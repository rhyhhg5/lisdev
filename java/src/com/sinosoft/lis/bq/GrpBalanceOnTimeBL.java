package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全定期结算处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class GrpBalanceOnTimeBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 团体合同号 */
    private String mGrpContNo = null;
    /** 工单号 */
    private String mDetailWorkNo = null;
    /** 转帐银行 */
    private String mBank = null;
    /** 转帐帐户 */
    private String mBankAccno = null;
    /** 帐户名 */
    private String mAccName = null;
    /** 转帐日期 */
    private String mPayDate = null;
    /**转帐方式*/
    private String mPayMode = null;
    /**最晚交费日期*/
    private String mPayEndDate = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    public LJAPaySet mLJAPaySet;
    public LJAGetSet mLJAGetSet;
    public LCGrpBalPlanDB mLCGrpBalPlanDB;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    //qulq 070117 add 8位机构
    private String bit8Managecom ="00000000";

    public GrpBalanceOnTimeBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");

        if (!checkData()) {
            return false;
        }
        // 根据业务逻辑对数据进行处理
        if (!this.dealData()) {
            return false;
        }

        //数据准备操作
        System.out.println("---End dealData---");
        if (!prepareOutputData()) {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean checkData() {
        mLCGrpBalPlanDB = new LCGrpBalPlanDB();
        mLCGrpBalPlanDB.setGrpContNo(mGrpContNo);
        if (!mLCGrpBalPlanDB.getInfo()) {
            this.mErrors.copyAllErrors(mLCGrpBalPlanDB.mErrors);
            return false;
        }
        if (!mLCGrpBalPlanDB.getState().equals("1")) {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保单有未结算的记录，不能进行新的结算!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLCGrpBalPlanDB.getBalIntv() == 0) {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保单的结算频次为随时结算，不需要做定期结算!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean prepareOutputData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            TransferData tTransferData = (TransferData) mInputData.
                                         getObjectByObjectName("TransferData",
                    0);
            mGrpContNo = (String) tTransferData.getValueByName("GrpNo");
            mDetailWorkNo = (String) tTransferData.getValueByName(
                    "DetailWorkNo");
            this.mBank = (String) tTransferData.getValueByName(
                    "Bank");
            this.mBankAccno = (String) tTransferData.getValueByName(
                    "BankAccno");
            this.mAccName = (String) tTransferData.getValueByName(
                    "AccName");
            this.mPayDate = (String) tTransferData.getValueByName(
                    "PayDate");
            this.mPayMode = (String) tTransferData.getValueByName(
                    "PayMode");
            this.mPayEndDate = (String) tTransferData.getValueByName(
                    "PayEndDate");
            if(mGlobalInput.ManageCom.length() == 2){
                mGlobalInput.ManageCom += "000000";
            } else if(mGlobalInput.ManageCom.length() == 4){
                mGlobalInput.ManageCom += "0000";
            }
            ExeSQL tExeSQL = new ExeSQL();
            String sql = "select managecom from lcgrpcont where grpcontno ='"+mGrpContNo+"'"
                    ;
            SSRS temp = tExeSQL.execSQL(sql);
            if(temp == null ||temp.getMaxRow()==0)
            {
                bit8Managecom = "00000000";
            }
            else
            {
                bit8Managecom = temp.GetText(1,1);
            }
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    private boolean dealData() {
        GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
        String balDate = mLCGrpBalPlanDB.getBaltoDate();
        //查询汇总金额
        Double tSumMoney = tGrpBalanceOnTime.getBalanceMoney(mGrpContNo,
                balDate, mCurrentDate);
        if (tSumMoney == null) {
            this.mErrors.copyAllErrors(tGrpBalanceOnTime.mErrors);
            return false;
        }
        this.mLJAGetSet = tGrpBalanceOnTime.mLJAGetSet;
        this.mLJAPaySet = tGrpBalanceOnTime.mLJAPaySet;
        double sumMoney = tSumMoney.doubleValue();
        if (sumMoney == 0 && this.mLJAGetSet.size() == 0 &&
            this.mLJAPaySet.size() == 0) {
            //已结算
            MMap tMap = tGrpBalanceOnTime.updateLCGrpBalPlanState(mGrpContNo,
                    "0", mGlobalInput.Operator, sumMoney);
            if (tMap == null) {
                this.mErrors.copyAllErrors(tGrpBalanceOnTime.mErrors);
                return false;
            }
            this.map.add(tMap);
            //工单任务结案
            if (!finishTask(sumMoney)) {
                return false;
            }

        } else if (sumMoney < 0) {
            //写应收
            LJSPaySchema tLJSPaySchema = setLJSPay(sumMoney);
            LJSPayBSchema tLJSPayBSchema = setLJSPayB(tLJSPaySchema);
            map.put(tLJSPaySchema, "INSERT");
            map.put(tLJSPayBSchema, "INSERT");
            //写LJTempFee,LJTempFeeClass
            if (this.mPayMode.equals("4")) {
                setTempInfo(map, tLJSPaySchema);
            }
            //写明细
            if (!setBalanceDetail("0", tLJSPaySchema.getGetNoticeNo())) {
                return false;
            }
            //结算待收费
            MMap tMap = tGrpBalanceOnTime.updateLCGrpBalPlanState(mGrpContNo,
                    "2", mGlobalInput.Operator, sumMoney);
            if (tMap == null) {
                this.mErrors.copyAllErrors(tGrpBalanceOnTime.mErrors);
                return false;
            }
            this.map.add(tMap);
            //修改结算方式'B'为'J'
            if (!(updatePayMode())) {
                return false;
            }
            //更改工单状态为待收费,9-待收费，0-保全确认。
            if (!waitingForFee("9", sumMoney)) {
                return false;
            }

        } else {
            //写实付
            LJAGetSchema tLJAGetSchema = setLJAGet(sumMoney);
            map.put(tLJAGetSchema, "INSERT");
            //写明细
            if (!setBalanceDetail("1", tLJAGetSchema.getActuGetNo())) {
                return false;
            }
            //已结算
            MMap tMap = tGrpBalanceOnTime.updateLCGrpBalPlanState(mGrpContNo,
                    "0", mGlobalInput.Operator, sumMoney);
            if (tMap == null) {
                this.mErrors.copyAllErrors(tGrpBalanceOnTime.mErrors);
                return false;
            }
            //
            this.map.add(tMap);
            //修改结算方式'B'为'J'
            if (!(updatePayMode())) {
                return false;
            }
            //工单任务结案
            if (!finishTask(sumMoney)) {
                return false;
            }
        }
        return true;
    }

    //写定期结算实收明细，flag表示本次结算汇总后，是收费还是付费，0--应收，1--实付
    //actNo表示应收/实付号码
    private boolean setBalanceDetail(String flag, String actNo) {
        for (int i = 1; i <= mLJAGetSet.size(); i++) {
            if (!setLJAEdorBalDetail(actNo, mLJAGetSet.get(i).getActuGetNo(),
                                     flag, "1")) { //白条是付费
                return false;
            }
        }
        for (int i = 1; i <= mLJAPaySet.size(); i++) {
            if (!setLJAEdorBalDetail(actNo, mLJAPaySet.get(i).getPayNo(), flag,
                                     "0")) { //白条是收费
                return false;
            }

        }
        return true;
    }

    //设置定期结算实收明细
    private boolean setLJAEdorBalDetail(String actuGetNo, String BTActuNo,
                                        String flag, String BTFlag) {
        LJAEdorBalDetailSchema tLJAEdorBalDetailSchema = new
                LJAEdorBalDetailSchema();
        tLJAEdorBalDetailSchema.setActuNo(actuGetNo);
        tLJAEdorBalDetailSchema.setBTActuNo(BTActuNo);
        tLJAEdorBalDetailSchema.setFlag(flag);
        tLJAEdorBalDetailSchema.setBTFlag(BTFlag);
        tLJAEdorBalDetailSchema.setOperator(mGlobalInput.Operator);
        tLJAEdorBalDetailSchema.setManageCom(mGlobalInput.ManageCom);
        tLJAEdorBalDetailSchema.setMakeDate(mCurrentDate);
        tLJAEdorBalDetailSchema.setMakeTime(mCurrentTime);
        tLJAEdorBalDetailSchema.setModifyDate(mCurrentDate);
        tLJAEdorBalDetailSchema.setModifyTime(mCurrentTime);
        map.put(tLJAEdorBalDetailSchema, "INSERT");
        return true;
    }


    /**
     * 设置应收表
     * @param sumMoney double
     */
    private LJSPaySchema setLJSPay(double sumMoney) {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(createPayNoticeNo());
        tLJSPaySchema.setOtherNo(this.mDetailWorkNo);
        tLJSPaySchema.setOtherNoType("13");

        if (this.mPayMode.equals("4")) {
            tLJSPaySchema.setBankCode(this.mBank);
            tLJSPaySchema.setAccName(this.mAccName);
            tLJSPaySchema.setBankAccNo(this.mBankAccno);
            tLJSPaySchema.setPayDate(this.mPayDate);
        } else {
            tLJSPaySchema.setPayDate(this.mPayEndDate);
        }
        tLJSPaySchema.setStartPayDate(PubFun.getCurrentDate());

        tLJSPaySchema.setManageCom(bit8Managecom);
        tLJSPaySchema.setSumDuePayMoney(Math.abs(sumMoney));
        tLJSPaySchema.setRiskCode(BQ.FILLDATA);
        tLJSPaySchema.setOperator(mGlobalInput.Operator);
        tLJSPaySchema.setMakeDate(mCurrentDate);
        tLJSPaySchema.setMakeTime(mCurrentTime);
        tLJSPaySchema.setModifyDate(mCurrentDate);
        tLJSPaySchema.setModifyTime(mCurrentTime);
        
    	String sql = "select LF_SaleChnl('2', '"+ mGrpContNo +"'),LF_MarketType('"+ mGrpContNo+"') from dual where 1=1";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow()>0)
        {
        	String tSaleChnl = tSSRS.GetText(1, 1);
            String tMarketType = tSSRS.GetText(1, 2);
            tLJSPaySchema.setSaleChnl(tSaleChnl);
            tLJSPaySchema.setMarketType(tMarketType);
        }
        
        return tLJSPaySchema;
    }

    private LJSPayBSchema setLJSPayB(LJSPaySchema aLJSPaySchema) {
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(aLJSPaySchema.getGetNoticeNo());
        tLJSPayBSchema.setOtherNo(aLJSPaySchema.getOtherNo());
        tLJSPayBSchema.setOtherNoType("13");
        tLJSPayBSchema.setBankCode(aLJSPaySchema.getBankCode());
        tLJSPayBSchema.setAccName(aLJSPaySchema.getAccName());
        tLJSPayBSchema.setBankAccNo(aLJSPaySchema.getBankAccNo());
        tLJSPayBSchema.setPayDate(aLJSPaySchema.getPayDate());
        tLJSPayBSchema.setStartPayDate(PubFun.getCurrentDate());

        tLJSPayBSchema.setManageCom(aLJSPaySchema.getManageCom());
        tLJSPayBSchema.setSumDuePayMoney(aLJSPaySchema.getSumDuePayMoney());
        tLJSPayBSchema.setRiskCode(BQ.FILLDATA);
        tLJSPayBSchema.setOperator(mGlobalInput.Operator);
        tLJSPayBSchema.setMakeDate(mCurrentDate);
        tLJSPayBSchema.setMakeTime(mCurrentTime);
        tLJSPayBSchema.setModifyDate(mCurrentDate);
        tLJSPayBSchema.setModifyTime(mCurrentTime);
        tLJSPayBSchema.setSaleChnl(aLJSPaySchema.getSaleChnl());
        tLJSPayBSchema.setMarketType(aLJSPaySchema.getMarketType());
        
        return tLJSPayBSchema;
    }


    /**
     * 设置实付表，对保全来说应付表(LJSGet)没有意义，已经不用
     * @return String
     */
    private LJAGetSchema setLJAGet(double sumMoney) {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(createGetNoticeNo());
        tLJAGetSchema.setOtherNo(this.mDetailWorkNo);
        tLJAGetSchema.setOtherNoType("13");
        tLJAGetSchema.setPayMode(this.mPayMode);

        tLJAGetSchema.setBankCode(this.mBank);
        tLJAGetSchema.setBankAccNo(this.mBankAccno);
        tLJAGetSchema.setAccName(this.mAccName);
        tLJAGetSchema.setStartGetDate(this.mPayDate);

        tLJAGetSchema.setManageCom(bit8Managecom);
        tLJAGetSchema.setSumGetMoney(Math.abs(sumMoney));
        tLJAGetSchema.setShouldDate(mCurrentDate);
        tLJAGetSchema.setOperator(mGlobalInput.Operator);
        tLJAGetSchema.setMakeDate(mCurrentDate);
        tLJAGetSchema.setMakeTime(mCurrentTime);
        tLJAGetSchema.setModifyDate(mCurrentDate);
        tLJAGetSchema.setModifyTime(mCurrentTime);
        return tLJAGetSchema;
    }

    /**
     * 产生退费记录号，如果原来LJAGet表中没有数据时才产生新的号
     * @return String
     */
    private String createGetNoticeNo() {
        return PubFun1.CreateMaxNo("GETNO", null);
    }

    /**
     * 产生交费记录号，如果原来LJSPay表中没有数据时才产生新的号
     * @return String
     */
    private String createPayNoticeNo() {
        return PubFun1.CreateMaxNo("PAYNOTICENO", null);
    }

    /**
     * 工单结案
     * @return boolean
     */
    private boolean finishTask(double sumMoney) {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setDetailWorkNo(mDetailWorkNo);
        tLGWorkSchema.setTypeNo("06");

        VData tTaskVData = new VData();
        tTaskVData.add(this.mGlobalInput);
        tTaskVData.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap tMap = tTaskAutoFinishBL.getSubmitData(tTaskVData, "");
        if (tMap == null) {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        this.map.add(tMap);
        //更改工单状态为待收费,9-待收费，0-保全确认。
        waitingForFee("0", sumMoney);
        return true;
    }

    /**
     * 更改保全申请主表状态,9-待收费，0-保全确认。
     * @param edorState String
     * @return boolean
     */
    private boolean waitingForFee(String edorState, double sumMoney) {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(this.mDetailWorkNo);
        if (!tLPEdorAppDB.getInfo()) {
            this.mErrors.addOneError("该工单没有生成lpedorapp记录");
            return false;
        }
        LPEdorAppSchema tLPEdorAppSchema = tLPEdorAppDB.getSchema();
        //待结算
        tLPEdorAppSchema.setEdorState(edorState);
        this.map.put(tLPEdorAppSchema, "UPDATE");
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setEdorAcceptNo(this.mDetailWorkNo);
        tLPGrpEdorMainSchema.setEdorNo(this.mDetailWorkNo);
        tLPGrpEdorMainSchema.setEdorAppNo(this.mDetailWorkNo);
        tLPGrpEdorMainSchema.setGrpContNo(mGrpContNo);
        tLPGrpEdorMainSchema.setEdorAppDate(tLPEdorAppSchema.getEdorAppDate());
        tLPGrpEdorMainSchema.setEdorValiDate(tLPEdorAppSchema.getEdorAppDate());
        tLPGrpEdorMainSchema.setEdorState(edorState);
        tLPGrpEdorMainSchema.setUWState("0");
        tLPGrpEdorMainSchema.setGetMoney(sumMoney * ( -1));
        tLPGrpEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPGrpEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
        tLPGrpEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
        tLPGrpEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
        tLPGrpEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPGrpEdorMainSchema, "DELETE&INSERT");

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo(mDetailWorkNo);
        tLPGrpEdorItemSchema.setEdorNo(mDetailWorkNo);
        tLPGrpEdorItemSchema.setEdorAppNo(mDetailWorkNo);
        tLPGrpEdorItemSchema.setEdorType("DJ");
        tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
        tLPGrpEdorItemSchema.setEdorState(edorState);
        tLPGrpEdorItemSchema.setGetMoney(sumMoney);
        tLPGrpEdorItemSchema.setEdorAppDate(tLPEdorAppSchema.getEdorAppDate());
        tLPGrpEdorItemSchema.setEdorValiDate(tLPEdorAppSchema.getEdorAppDate());
        tLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        tLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        tLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPGrpEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
        tLPGrpEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
        map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");

        return true;
    }

    //修改收费模式
    private boolean updatePayMode() {
        for (int i = 1; i <= mLJAGetSet.size(); i++) {
            LJAGetSchema tLJAGetSchema = this.mLJAGetSet.get(i);
            tLJAGetSchema.setPayMode("J");
            this.map.put(tLJAGetSchema, "UPDATE");
            String tSQL = "update LJFIGet set paymode='J' where ActuGetNo='"
                          + tLJAGetSchema.getActuGetNo()
                          + "' and paymode='B'";
            this.map.put(tSQL, "UPDATE");
        }
        for (int i = 1; i <= mLJAPaySet.size(); i++) {
            LJAPaySchema tLJAPaySchema = this.mLJAPaySet.get(i);
            tLJAPaySchema.setIncomeType("J");
            this.map.put(tLJAPaySchema, "UPDATE");
            String sql =
                    "update ljtempfeeclass set paymode='J' where tempfeeno in ( "
                    + "select distinct b.tempfeeno from ljtempfee b, ljapay c "
                    +
                    " where b.otherno=c.incomeno and b.othernotype='7' and c.payno='"
                    + tLJAPaySchema.getPayNo() + "')";
            this.map.put(sql, "UPDATE");
        }
        return true;
    }

    private void setTempInfo(MMap map, LJSPaySchema cLJSPaySchema) {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(cLJSPaySchema.getGetNoticeNo());
        tLJTempFeeSchema.setTempFeeType("4"); //保全
        tLJTempFeeSchema.setRiskCode(cLJSPaySchema.getRiskCode());
        tLJTempFeeSchema.setOtherNo(cLJSPaySchema.getOtherNo());
        tLJTempFeeSchema.setOtherNoType(cLJSPaySchema.getOtherNoType());
        tLJTempFeeSchema.setPayDate(mPayDate);
        tLJTempFeeSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeSchema.setManageCom(mGlobalInput.ManageCom);
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJTempFeeSchema, "INSERT");

        //设置暂交费分类表
        LJTempFeeClassSchema tLJTempFeeClassSchema = new
                LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(cLJSPaySchema.getGetNoticeNo());
        tLJTempFeeClassSchema.setPayMode(mPayMode); //4是银行转帐
        tLJTempFeeClassSchema.setPayDate(mPayDate);
        tLJTempFeeClassSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeClassSchema.setManageCom(mGlobalInput.ManageCom);
        tLJTempFeeClassSchema.setBankCode(mBank);
        tLJTempFeeClassSchema.setBankAccNo(mBankAccno);
        tLJTempFeeClassSchema.setAccName(mAccName);
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJTempFeeClassSchema, "INSERT");
    }


    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "pa0001";
        tGI.ComCode = "86";
        tGI.ManageCom = "86";

        String GrpContNo = "0000123201";
        String DetailWorkNo = "20060908000004";
        String PayMode ="1";
        String Bank = null;
        String BankAccno = null;
        String AccName = null;
        String PayDate = null;
        String PayEndDate = "2006-9-15";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpNo", GrpContNo);
        tTransferData.setNameAndValue("DetailWorkNo", DetailWorkNo);
        tTransferData.setNameAndValue("PayMode", PayMode);
        tTransferData.setNameAndValue("Bank", Bank);
        tTransferData.setNameAndValue("BankAccno", BankAccno);
        tTransferData.setNameAndValue("AccName", AccName);
        tTransferData.setNameAndValue("PayDate", PayDate);
        tTransferData.setNameAndValue("PayEndDate", PayEndDate);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        GrpBalanceOnTimeBL tGrpBalanceOnTimeBL = new GrpBalanceOnTimeBL();
        if (!tGrpBalanceOnTimeBL.submitData(tVData, "")) {
            System.out.println("end");
        }
    }
}
