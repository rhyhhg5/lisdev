package com.sinosoft.lis.bq;

import java.util.Date;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 补充A团体公共保额分配</p>
 * <p>Description: 项目明细录入</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author lzy
 * @version 1.0
 */

public class GEdorGDDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private static String mEdorType = BQ.EDORTYPE_GD;

    private String mGrpContNo = null;

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GEdorGDDetailBL(GlobalInput gi, String edorNo, String grpContNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到磁盘导入数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("找不到磁盘导入数据！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }

        changeEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkCustomers())
        {
            return false;
        }
        
        if(!checkCvalidate())
    	{
    		return false;
    	}
        
        if(!checkImportPerson())
        {
        	return false;
        }

        if (!checkMoney())
        {
            return false;
        }
        
        if (!checkImportMoney())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 校验保单生效日期
     * @author YangXingLei
     */
    private boolean checkCvalidate()
    {
    	for(int i = 1; i <= mLPDiskImportSet.size(); i++)
    	{
    		LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
    		
    		//取出contno
    		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
    		LCInsuredSet tLCInsuredSet = new LCInsuredSet();
    		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
    		tLCInsuredDB.setGrpContNo(mGrpContNo);
    		tLCInsuredDB.setName(tLPDiskImportSchema.getInsuredName());
    		tLCInsuredDB.setSex(tLPDiskImportSchema.getSex());
    		tLCInsuredDB.setBirthday(tLPDiskImportSchema.getBirthday());
    		tLCInsuredDB.setIDType(tLPDiskImportSchema.getIDType());
    		tLCInsuredDB.setIDNo(tLPDiskImportSchema.getIDNo());
    		tLCInsuredSet = tLCInsuredDB.query();
    		if(tLCInsuredSet == null)
    		{
    			mErrors.addOneError("没有查询到被保人信息！");
    			return false;
    		}
    		tLCInsuredSchema = tLCInsuredSet.get(1);
    		
    		
    		LCContSchema tLCContSchema = new LCContSchema();
    		LCContSet tLCContSet = new LCContSet();
    		LCContDB tLCContDB = new LCContDB();
    		tLCContDB.setGrpContNo(mGrpContNo);
    		tLCContDB.setContNo(tLCInsuredSchema.getContNo());
    		tLCContSet = tLCContDB.query();
    		if(tLCContSet == null)
    		{
    			mErrors.addOneError("没有查询到分单信息！");
    			return false;
    		}
    		tLCContSchema = tLCContSet.get(1);
    		
    		//取出保全生效日期
    		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    		LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
    		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
    		tLPGrpEdorItemDB.setEdorNo(mEdorNo);
    		tLPGrpEdorItemDB.setEdorType(mEdorType);
    		tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
    		if(tLPGrpEdorItemSet == null)
    		{
    			mErrors.addOneError("没有查询到保全项目信息！");
    			return false;
    		}
    		tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
    	            
    		//取出保单生效日期，与GD生效日期做对比
    		Date cvalidate = CommonBL.stringToDate(tLCContSchema.getCValiDate());
    		Date edorvalidate = CommonBL.stringToDate(tLPGrpEdorItemSchema.getEdorValiDate());
    		System.out.println("cvalidate="+cvalidate+";edorvalidate="+edorvalidate);
    		if(cvalidate!=null && edorvalidate!=null )
    		{
    			if(edorvalidate.before(cvalidate))
    			{
    				mErrors.addOneError("该分单"+tLCInsuredSchema.getContNo()+"还未生效，不能操作GD保全项目!");
    				return false;
    			}
    		}
    		else
    		{
    			mErrors.addOneError("保单或者保全生效日期为空!");
				return false;
    		}
    	}
    	return true;
    }

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkCustomers()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(
                    tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_FAIL).append("', ")
                        .append(" ErrorReason = '保单下不存在该客户，导入无效！', ")
                        .append(" Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append(" ModifyDate = '")
                        .append(mCurrentDate).append("', ")
                        .append(" ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
                flag = false;
            }
            else
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_SUCC).append("', ")
                        .append("InsuredNo = '")
                        .append(StrTool.cTrim(customerCheck.getCustomerNo()))
                        .append("', ")
                        .append("Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append("ModifyDate = '")
                        .append(mCurrentDate).append("', ")
                        .append("ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效客户！";
        }
        return true;
    }

    /**
     * 检查导入的资金是否超过团体公共保额
     * @return boolean
     */
    private boolean checkMoney()
    {
        double importMoney = getImportMoney();
        double grpAccountMeoney = CommonBL.getGrpAccountMoney(mGrpContNo);
        double interest = getInterest();
        if (importMoney > (grpAccountMeoney + interest))
        {
            mErrors.addOneError("团体公共保额不足！");
            return false;
        }
        return true;
    }

    /**
     * 得到磁盘导入的保额总数
     * @return double
     */
    private double getImportMoney()
    {
        double importMoney = 0.00;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            importMoney += mLPDiskImportSet.get(i).getMoney();
        }
        return importMoney;
    }

    /**
     * 得到利息
     * @return double
     */
    private double getInterest()
    {
        double interest = 0.00;
        return interest;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void changeEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }
    
    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkImportMoney()
    {
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
        	LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
        	double importMoney = 0.00;
        	importMoney = tLPDiskImportSchema.getMoney();
        	if(importMoney == 0.00)
        	{
        		mErrors.addOneError("被保人"+tLPDiskImportSchema.getInsuredName()+"的导入保额为0，请检查模板。");
                return false;
        	}
        	else if(importMoney<0.00)
        	{
        		mErrors.addOneError("被保人"+tLPDiskImportSchema.getInsuredName()+"的导入保额为负数，请检查模板。");
                return false;
        	}
        }
        return true;
    }
    
    //20091221 zhanggm 校验重复导入的被保人
    private boolean checkImportPerson()
    {
    	LPDiskImportSet tLPDiskImportSetCF = getLPDiskImportSetCF(mLPDiskImportSet);
        if(tLPDiskImportSetCF.size()>0)
        {
            for (int i = 1; i <= tLPDiskImportSetCF.size(); i++)
            {
                String errorReason = "";
                LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSetCF.get(i);
                System.out.println("重复人姓名："+tLPDiskImportSchema.getInsuredName());
                errorReason +="被保人" +tLPDiskImportSchema.getInsuredName()+"是重复导入的.";
                String sql = "update LPDiskImport " +
                                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                                        " ErrorReason = '" + errorReason + "', " +
                                        " Operator = '" + mGlobalInput.Operator + "', " +
                                        " ModifyDate = '" + mCurrentDate + "', " +
                                        " ModifyTime = '" + mCurrentTime + "' " +
                                        "where EdorNo = '" + mEdorNo + "' " +
                                        "and EdorType = '" + mEdorType + "' " +
                                        "and GrpContNo = '" + mGrpContNo + "' " +
                                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                  mMap.put(sql, "UPDATE");
                  mMessage = "有重复人被导入！";
          }
        }
        else
        {
            System.out.println("啥也没有,else");
            return true ;
        }
        return true ;
    }
    
//  20091221 zhanggm 得到重复导入的被保人:
    private LPDiskImportSet getLPDiskImportSetCF(LPDiskImportSet mLPDiskImportSet)
    {
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        LPDiskImportSet tLPDiskImportSet1 = new LPDiskImportSet(); //用来记录重复的人信息
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) 
        {
        	LPDiskImportSchema mLPDiskImportSchema = mLPDiskImportSet.get(i);
        	if(checkLPDiskImport(tLPDiskImportSet,mLPDiskImportSchema))
    		{
    			tLPDiskImportSet1.add(mLPDiskImportSet.get(i)); //重复人的记录
    		}
    		else
    		{
    			tLPDiskImportSet.add(mLPDiskImportSet.get(i));
    		}
        }
        return tLPDiskImportSet1;
    }
    
    //20091221 zhanggm 校验aLPDiskImportSchema是否在aLPDiskImportSet中,是:true,否：false
    private boolean checkLPDiskImport(LPDiskImportSet aLPDiskImportSet,LPDiskImportSchema aLPDiskImportSchema)
    {
    	if(aLPDiskImportSet.size()<1)
    	{
    		return false;
    	}
    	String mInsuredName = aLPDiskImportSchema.getInsuredName();
    	String mSex = aLPDiskImportSchema.getSex();
    	String mBirthday = aLPDiskImportSchema.getBirthday();
    	String mIDType = aLPDiskImportSchema.getIDType();
    	String mIDNo = aLPDiskImportSchema.getIDNo();
    	for (int i=1;i<=aLPDiskImportSet.size();i++)
    	{
			LPDiskImportSchema tLPDiskImportSchema = aLPDiskImportSet.get(i);
			String tInsuredName = tLPDiskImportSchema.getInsuredName();
        	String tSex = tLPDiskImportSchema.getSex();
        	String tBirthday = tLPDiskImportSchema.getBirthday();
        	String tIDType = tLPDiskImportSchema.getIDType();
        	String tIDNo = tLPDiskImportSchema.getIDNo();
        	if(mInsuredName.equals(tInsuredName) && mSex.equals(tSex) && mBirthday.equals(tBirthday) 
        			&& mIDType.equals(tIDType) && mIDNo.equals(tIDNo))
        	{
        		System.out.println("被保人"+tInsuredName+"是重复导入的！");
        		return true;
        	}
    	}
    	return false;
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
