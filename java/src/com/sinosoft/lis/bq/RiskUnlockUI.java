package com.sinosoft.lis.bq;
//程序名称：LJSUnlockUI.java
//程序功能：保全续期收付费信息修改
//创建日期：2009-4-15
//创建人  ：zhanggm 
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class RiskUnlockUI 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	public String mGetNoticeNo ;
	public RiskUnlockUI()
	{
	}

    public boolean submitData(VData cInputData, String paymode) 
    {
    	RiskUnlockBL tRiskUnlockBL =new RiskUnlockBL();
        if (!tRiskUnlockBL.submitData(cInputData,paymode)) 
        {
            this.mErrors.copyAllErrors(tRiskUnlockBL.getErrors());
            return false;
        }
        mGetNoticeNo = tRiskUnlockBL.getResult();
        return true;
    }
    public String getResult() {
        return this.mGetNoticeNo;
    }
}
