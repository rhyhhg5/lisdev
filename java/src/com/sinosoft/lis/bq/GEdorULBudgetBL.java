package com.sinosoft.lis.bq;

import utils.system;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.UliBQInsuAccBala;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 
 * @version 1.2
 */
public class GEdorULBudgetBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private String mGrpContNo = null;
    private String mOperator ;
    private EdorCalZTTestBL mEdorCalZTTestBL = new EdorCalZTTestBL();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //团体保全项目
    private LPEdorItemSchema mLPEdorItemSchema = null;  //个体保全项目

    public GEdorULBudgetBL()
    {
    }

    /**
     * 退保试算
     * @return
     */
    public boolean GULbudget(VData cInputData)
    {
    	VData mInputData = (VData) cInputData.clone();
    	TransferData mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
    	
    	String edorno=(String)mTransferData.getValueByName("EdorNo");
    	String edorType=(String)mTransferData.getValueByName("EdorType");
    	String grpCont=(String)mTransferData.getValueByName("GrpContNo");
    	String edorValiDate=(String)mTransferData.getValueByName("EdorValiDate");
        if(null == grpCont || "".equals(grpCont)){
        	mErrors.addOneError("解约生效日必须大于或者等于保单最后一次月结日期！");
        	return false;
        }
        //删除之前的数据
        clearPTrace(edorno,edorType);
        
        LCContSet tLCContSet=new LCContSet();
        LCContDB tLCContDB=new LCContDB();
        tLCContDB.setGrpContNo(grpCont);
        tLCContSet=tLCContDB.query();
        for(int i=1;i <= tLCContSet.size();i++){
        	LCContSchema tLCContSchema=tLCContSet.get(i);
        	LPEdorItemSchema tLPEdorItemSchema=new LPEdorItemSchema();
        	tLPEdorItemSchema.setEdorNo(edorno);
        	tLPEdorItemSchema.setGrpContNo(tLCContSchema.getGrpContNo());
        	tLPEdorItemSchema.setEdorValiDate(edorValiDate);
        	tLPEdorItemSchema.setContNo(tLCContSchema.getContNo());
        	tLPEdorItemSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        	tLPEdorItemSchema.setEdorType(edorType);
        	tLPEdorItemSchema.setOperator(mOperator);
        	InsuAccBala(tLPEdorItemSchema);
        }
        return true;
    }
    
    //团体万能退保试算
    private boolean InsuAccBala(LPEdorItemSchema pLPEdorItemSchema)
    {
    	LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
    	//查询保单信息
    	LCContSchema tLCContSchema = CommonBL.getLCCont(pLPEdorItemSchema.getContNo());
    	if(tLCContSchema==null){
        	return false;                		
    	}
    	
    	//保单上次月结与保全生效日期之间不能超过一个月
    	String edorValidate = pLPEdorItemSchema.getEdorValiDate();
    	//查询保单的最后一次月结时间
    	String lastBalaDate = new ExeSQL().getOneValue("select baladate from lcinsureaccclass where contno=( " +
    			" select contno from lcpol where grpcontno='"+pLPEdorItemSchema.getGrpContNo()+"' " +
    			" and poltypeflag='2' fetch first 1 rows only )  fetch first 1 rows only with ur ");
    	if(lastBalaDate!=null&&!"".equals(lastBalaDate)){
    		if(CommonBL.stringToDate(edorValidate).before(CommonBL.stringToDate(lastBalaDate))){
    			mErrors.addOneError("解约生效日必须大于或者等于保单最后一次月结日期！");
    			return false;   
    		}else{
    			if(CommonBL.changeMonth(lastBalaDate, 1).before(CommonBL.stringToDate(edorValidate))){
					CError tError = new CError();
					tError.moduleName = "PEdorCTAppConfirmBL";
					tError.functionName = "prepareData";
					tError.errorMessage = "解约生效日和保单最后一次月结日期的间隔不能超过1个月!";
					mErrors.addOneError(tError);    				
    				return false;   
    			}
    		}
    	}else{
    		mErrors.addOneError("查询保单最后一次月结信息失败！");
			return false;   
    	}
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = new LCPolSet();
    	tLCPolDB.setContNo(tLCContSchema.getContNo());
    	tLCPolSet = tLCPolDB.query();
    	for(int i=1;i<=tLCPolSet.size();i++){
    		//团险万能解约开始
    		pLPEdorItemSchema.setInsuredNo(tLCPolSet.get(i).getInsuredNo());
    		VData tVData = new VData();
    		GlobalInput mGI = new GlobalInput();
    		mGI.ManageCom = pLPEdorItemSchema.getManageCom();
    		mGI.Operator = pLPEdorItemSchema.getOperator();
    		mGI.ComCode = mGI.ManageCom;
    		tVData.add(mGI);
    		tVData.add(pLPEdorItemSchema);
//			对保单用保证利率进行月结    	
    		//针对农民工险种做解约，370301只有公共账户进行月结。20180412 lzj
    		if(!("370301".equals(tLCPolSet.get(i).getRiskCode())&&"0".equals(tLCPolSet.get(i).getPolTypeFlag()))&&!("370301".equals(tLCPolSet.get(i).getRiskCode())&&"1".equals(tLCPolSet.get(i).getPolTypeFlag()))){
    			UliBQInsuAccBala tUliBQInsuAccBala = new UliBQInsuAccBala();
    			if(!tUliBQInsuAccBala.submitData(tVData, "",tLCContSchema.getGrpContNo())){
    				mErrors.addOneError("保单:" + pLPEdorItemSchema.getContNo() + " 结算失败; ");
    				return false;
    			}
    		}
    	}
    	
    	return true;
    }
    private boolean clearPTrace(String edorNo,String edorType){
    	MMap mMap=new MMap();
    	mMap.put("delete from LPInsureAcc where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	mMap.put("delete from LPInsureAccClass where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	mMap.put("delete from LPInsureAccTrace where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	mMap.put("delete from LPInsureAccfee where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	mMap.put("delete from LPInsureAccClassFee where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	mMap.put("delete from LPInsureAccfeeTrace where edorno='" + edorNo + "' and EdorType='" + edorType +"'",SysConst.DELETE);
    	VData data = new VData();
        data.add(mMap);
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, ""))
        {
        	CError cError = new CError();

            cError.moduleName = "GEdorULBudgetBL";
            cError.functionName = "clearPTrace";
            cError.errorMessage = "提交数据失败";
            this.mErrors.addOneError(cError);
            return false;
        }
        return true;
    }

    /**
     * 设置是否需要试算结果标志
     * @param flag boolean
     */
    public void setGrpContNo(String grpContNo)
    {
        mGrpContNo = grpContNo;
    }

    public void setOperator(String operator)
    {
       this.mOperator = operator;
    }

    public static void main(String[] args)
    {
    }
}
