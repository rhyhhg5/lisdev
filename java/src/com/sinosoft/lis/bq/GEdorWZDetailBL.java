package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 名单增人明细保存
 * 保存人数，总交费，生效日期
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GEdorWZDetailBL
{
    /**错误的容器，保存错误信息*/
    public CErrors mErrors = new CErrors();

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //项目信息
    private TransferData mTransferData = null;  //传入的特殊数据的容器
    private GlobalInput mGlobalInput = null;

    private String mPrem = null;  //本次总交费
    private String mPeoples2 = null;  //增人人数
    private String mContPlanCode = null;  //被保人保障计划
    private String mContNo = null;
    private String mEdorValiDate = null;

    private MMap map = new MMap();

    public GEdorWZDetailBL()
    {
    }

    /**
     * 业务处理方法，对外的接口
     * @param data VData
     * 1、需要LPGrpEdorITemSchema 对象：包括edorNo, edorType, edorValiDate, grpContNo
     * 2、TransferData对象：包括总交费prem，人数peoples2, contPlanCode
     * 3、GlobalInput对象
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        if(!getInputData(data))
        {
            return false;
        }

        System.out.println(mGlobalInput.Operator + ", " + operate);

        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        VData tVData = new VData();
        tVData.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, operate))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("保存明细出错");
            return false;
        }

        return true;
    }

    /**
     * 获取传入的信息
     * @param data VData：包括：
     * 1、GlobalInput：操作员信息
     * 2、LPGrpEdorItemSchema：团单无名单增人项目信息
     * 3、TransferData：存储了收费prem，人数peoples2，保障计划contPlanCode，个单号contNo
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        if(mGlobalInput == null || mLPGrpEdorItemSchema == null
           || mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mPrem = (String) mTransferData.getValueByName("prem");
        mPeoples2 = (String) mTransferData.getValueByName("peoples2");
        mContPlanCode = (String) mTransferData.getValueByName("contPlanCode");
        mContNo = (String) mTransferData.getValueByName("contNo");
        if(mPrem == null || mPrem.equals("")
           || mPeoples2 == null || mPeoples2.equals(""))
        {
            mErrors.addOneError("增加被保险人数和本次应收保费均不能为空");
            return false;
        }
        if(mContPlanCode == null || mContPlanCode.equals(""))
        {
            mErrors.addOneError("请选择保障计划");
            return false;
        }
        if(mLPGrpEdorItemSchema.getEdorValiDate() == null
           || mLPGrpEdorItemSchema.getEdorValiDate().equals(""))
        {
            mErrors.addOneError("生效日期不能为空。");
            return false;
        }
        mEdorValiDate = mLPGrpEdorItemSchema.getEdorValiDate();

        return true;
    }

    /**
     * 校验传入的数据的合法性：
     * 1、项目状态不为未录入和录入完毕，先重复理算后再录入
     * 2、总保费应为非负数
     * 3、人数应为非负整数
     * @return boolean：合法true，否则false
     */
    private boolean checkData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet set = tLPGrpEdorItemDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询项目信息");
            return false;
        }
        if(set.get(1).getEdorState().equals(BQ.EDORSTATE_CAL)
           || set.get(1).getEdorState().equals(BQ.EDORSTATE_CONFIRM))
        {
            mErrors.addOneError("项目状态不为未录入和录入完毕，请先重复理算后再录入");
            return false;
        }
        String edorValiDate = mLPGrpEdorItemSchema.getEdorValiDate();
        mLPGrpEdorItemSchema.setSchema(set.get(1));
        mLPGrpEdorItemSchema.setEdorValiDate(edorValiDate);

        //校验总交费和人数的合法性
        try
        {
            double d = Double.parseDouble(mPrem);
        }
        catch(Exception e)
        {
            mErrors.addOneError("总保费应为数字");
            return false;
        }
        try
        {
            int i = Integer.parseInt(mPeoples2);
        }
        catch(Exception e)
        {
            mErrors.addOneError("人数应为整数");
            return false;
        }

        if(!checkItem())
        {
            return false;
        }

        return true;
    }

    /**
     * 同一保障计划不能同时做增减人操作
     * @return boolean
     */
    private boolean checkItem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 ")
            .append("from LCInsuredList a, LPCont b ")
            .append("where a.edorNo = b.edorNo ")
            .append("   and a.contNo = b.contNo")
            .append("   and a.edorNo = '")
            .append(this.mLPGrpEdorItemSchema.getEdorNo())
            .append("'   and a.contNo = '")
            .append(mContNo)
            .append("'   and b.edorType in ('WZ', 'WJ') ")
            .append("   and b.edorType != '")
            .append(this.mLPGrpEdorItemSchema.getEdorType())
            .append("' ");
        System.out.println(sql.toString());
        ExeSQL tExeSQL = new ExeSQL();
        String temp = tExeSQL.getOneValue(sql.toString());
        if(!temp.equals("") && !temp.equals("null"))
        {
            mErrors.addOneError("同一保障计划不能同时做增减人。");
            return false;
        }
        if(tExeSQL.mErrors.needDealError())
        {
            mErrors.addOneError("查询保障计划增减人信息出错。");
            return false;
        }

        return true;
    }

    /**
     * 进行业务逻辑处理
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        if(!prepareInputInfo())
        {
            return false;
        }
        if(!prepareLPEdorInfo())
        {
            return false;
        }
        if(!prepareContInfo())
        {
            return false;
        }
        if(!updateState())
        {
            return false;
        }

        return true;
    }

    /**
     * 存储录入的增人信息到LCInsuredList
     * @return boolean：成功true，否则false
     */
    private boolean prepareInputInfo()
    {
        String sql = "  select max(int(insuredID)) + 1 "
                     + "from LCInsuredList "
                     + "where grpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' ";
        String maxInsuredID = new ExeSQL().getOneValue(sql);
        if(maxInsuredID.equals("") || maxInsuredID.equals("null"))
        {
            maxInsuredID = "1";
        }

        sql = "  delete from LCInsuredList "
              + "where edorNo = '"
              + mLPGrpEdorItemSchema.getEdorNo() + "' "
              + "   and grpContNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + "   and contNo = '" + mContNo + "' "
              + "   and contPlanCode = '" + mContPlanCode + "' ";
        map.put(sql, "DELETE");

        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
        tLCInsuredListSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLCInsuredListSchema.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        tLCInsuredListSchema.setInsuredID(maxInsuredID);
        tLCInsuredListSchema.setContNo(this.mContNo);
        tLCInsuredListSchema.setContPlanCode(mContPlanCode);
        tLCInsuredListSchema.setState("0");
        tLCInsuredListSchema.setEmployeeName("无名单");
        tLCInsuredListSchema.setInsuredName(tLCInsuredListSchema
                                            .getEmployeeName());
        tLCInsuredListSchema.setRelation(BQ.MAININSURED);
        tLCInsuredListSchema.setNoNamePeoples(this.mPeoples2);
        tLCInsuredListSchema.setEdorPrem(this.mPrem);
        tLCInsuredListSchema.setEdorValiDate(this.mEdorValiDate);
        tLCInsuredListSchema.setOperator(mGlobalInput.Operator);

        PubFun.fillDefaultField(tLCInsuredListSchema);

        map.put(tLCInsuredListSchema, "INSERT");

        return true;
    }

    /**
     *生成无名单个单保全项目信息
     * @return boolean
     */
    private boolean prepareLPEdorInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if(!tLCContDB.getInfo())
        {
            System.out.println(tLCContDB.mErrors.getErrContent());
            mErrors.addOneError("没有查询到保单" + mContNo + "的信息");
            return false;
        }

        Reflections ref = new Reflections();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

        ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
        tLPEdorItemSchema.setContNo(mContNo);
        tLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        tLPEdorItemSchema.setEdorValiDate(mEdorValiDate);
        tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPEdorItemSchema.setUWFlag("0");
        tLPEdorItemSchema.setMakeDate(mCurDate);
        tLPEdorItemSchema.setMakeTime(mCurTime);
        tLPEdorItemSchema.setModifyDate(mCurDate);
        tLPEdorItemSchema.setModifyTime(mCurTime);
        map.put(tLPEdorItemSchema, "DELETE&INSERT");

        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        ref.transFields(tLPEdorMainSchema, tLPEdorItemSchema);
        tLPEdorMainSchema.setContNo(mContNo);
        tLPEdorMainSchema.setEdorValiDate(this.mEdorValiDate);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setUWState("0");
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INPUT);
        tLPEdorMainSchema.setMakeDate(mCurDate);
        tLPEdorMainSchema.setMakeTime(mCurTime);
        tLPEdorMainSchema.setModifyDate(mCurDate);
        tLPEdorMainSchema.setModifyTime(mCurTime);
        map.put(tLPEdorMainSchema, "DELETE&INSERT");

        return true;
    }

    /**
     * 项目状态变更为录入完毕
     * @return boolean：成功true，否则false
     */
    private boolean updateState()
    {
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPGrpEdorItemSchema.setModifyDate(mCurDate);
        mLPGrpEdorItemSchema.setModifyTime(mCurTime);

        map.put(mLPGrpEdorItemSchema, "UPDATE");

        return true;
    }

    /**
     * 生成保全临时数据
     * @return boolean：成功true，否则false
     */
    private boolean prepareContInfo()
    {
        String[] cTableP = {"LCCont", "LCInsured", "LCPol", "LCDuty", "LCPrem"};

        for(int i = 0; i < cTableP.length; i++)
        {
            //每个保障计划存储为一个无名单
            String sql = "delete from " + cTableP[i].replaceFirst("C", "P")
                         + " where edorNo = '"
                         + mLPGrpEdorItemSchema.getEdorNo() + "' "
                         + "   and edorType = '"
                         + mLPGrpEdorItemSchema.getEdorType() + "' "
                         + "   and contNo = '" + mContNo + "' ";
            map.put(sql, "DELETE");

            sql = "insert into " + cTableP[i].replaceFirst("C", "P")
                         + " (select '"
                         + mLPGrpEdorItemSchema.getEdorNo() + "', '"
                         + mLPGrpEdorItemSchema.getEdorType() + "', "
                         + cTableP[i] + ".* from " + cTableP[i] +
                         " where contNo = '" + mContNo + "') ";
            map.put(sql, "INSERT");

            sql = "  update " + cTableP[i].replaceFirst("C", "P")
                  + " set operator = '" + mGlobalInput.Operator + "', "
                  + "    modifyDate = '" + this.mCurDate + "', "
                  + "    modifyTime = '" + this.mCurTime + "', "
                  + "    makeDate = '" + this.mCurDate + "', "
                  + "    makeTime = '" + this.mCurTime + "' "
                  + "where contNO = '" + mContNo + "' "
                  + "   and edorNo = '"
                  + mLPGrpEdorItemSchema.getEdorNo() + "' "
                  + "   and edorType = '"
                  + mLPGrpEdorItemSchema.getEdorType() + "' ";
            map.put(sql, "UPDATE");
        }

        String[] cTableG = {"LCGrpCont", "LCGrpPol"};

        for(int i = 0; i < cTableG.length; i++)
        {
            //每个保障计划存储为一个无名单
            String sql = "delete from " + cTableG[i].replaceFirst("C", "P")
                         + " where edorNo = '"
                         + mLPGrpEdorItemSchema.getEdorNo() + "' "
                         + "   and edorType = '"
                         + mLPGrpEdorItemSchema.getEdorType() + "' "
                         + "   and grpContNo = '"
                         + mLPGrpEdorItemSchema.getEdorType() + "' ";
            map.put(sql, "DELETE");

            sql = "insert into " + cTableG[i].replaceFirst("C", "P")
                         + " (select '"
                         + mLPGrpEdorItemSchema.getEdorNo() + "', '"
                         + mLPGrpEdorItemSchema.getEdorType() + "', "
                         + cTableG[i] + ".* from " + cTableG[i] +
                         " where grpContNo = '"
                         + mLPGrpEdorItemSchema.getEdorType() + "') ";
            map.put(sql, "INSERT");

            sql = "  update " + cTableG[i].replaceFirst("C", "P")
                  + " set operator = '" + mGlobalInput.Operator + "', "
                  + "    modifyDate = '" + this.mCurDate + "', "
                  + "    modifyTime = '" + this.mCurTime + "', "
                  + "    makeDate = '" + this.mCurDate + "', "
                  + "    makeTime = '" + this.mCurTime + "' "
                  + "where grpContNo = '"
                  + mLPGrpEdorItemSchema.getEdorType() + "' "
                  + "   and edorNo = '"
                  + mLPGrpEdorItemSchema.getEdorNo() + "' "
                  + "   and edorType = '"
                  + mLPGrpEdorItemSchema.getEdorType() + "' ";
            map.put(sql, "UPDATE");
        }

        return true;
    }

    public static void main(String[] args)
    {
        LPGrpEdorItemSchema schema = new LPGrpEdorItemSchema();
        schema.setEdorNo("20060628000008");
        schema.setEdorType(BQ.EDORTYPE_WZ);
        schema.setGrpContNo("0000016701");
        schema.setEdorValiDate("2006-06-23");

        TransferData td = new TransferData();
        td.setNameAndValue("prem", "1000");
        td.setNameAndValue("peoples2", "10");
        td.setNameAndValue("contPlanCode", "A");
        td.setNameAndValue("contNo", "2300013409");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        VData data = new VData();
        data.add(schema);
        data.add(td);
        data.add(g);

        GEdorWZDetailBL gedorwzdetailbl = new GEdorWZDetailBL();
        if(!gedorwzdetailbl.submitData(data, ""))
        {
            System.out.println(gedorwzdetailbl.mErrors.getErrContent());
        }
    }
}
