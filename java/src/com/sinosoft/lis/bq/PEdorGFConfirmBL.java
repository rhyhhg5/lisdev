package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCContHangUpStateSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCContHangUpStateDB;
import com.sinosoft.lis.vschema.LCContHangUpStateSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PEdorGFConfirmBL  implements EdorConfirm {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LCContHangUpStateSchema mLCContHangUpStateSchema = new
            LCContHangUpStateSchema();
    StringBuffer sql = new StringBuffer();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public PEdorGFConfirmBL() {
    }


    public boolean submitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData))
        {
            return false ;
        }

        if (!prepareData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
            if (mLPEdorItemSchema== null || mGlobalInput== null)
            {
                CError tError = new CError();
                tError.moduleName = "PEdorGFConfirmBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获得信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
    }

    private boolean prepareData(){
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PEdorFCConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询保单信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
//        LCContSchema tLCContSchema = tLCContDB.getSchema();
        String SQL = "select * From lcconthangupstate where HangupType ='"+BQ.HANGUPTYPE_BQ+"' and State ='"+BQ.HANGUSTATE_ON+"' and ContNo ='"+mLPEdorItemSchema.getContNo()+"'" ;
        LCContHangUpStateDB tLCContHangUpStateDB = new LCContHangUpStateDB();
        LCContHangUpStateSet tLCContHangUpStateSet = tLCContHangUpStateDB.executeQuery(SQL);
        if (tLCContHangUpStateSet.size()==0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFCConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "没有查询到已经挂起保单信息！";
            this.mErrors.addOneError(tError);
        }
        mLCContHangUpStateSchema = tLCContHangUpStateSet.get(1).getSchema();
        mLCContHangUpStateSchema.setState(BQ.HANGUSTATE_OFF);
        mLCContHangUpStateSchema.setModifyDate(PubFun.getCurrentDate());
        mLCContHangUpStateSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLCContHangUpStateSchema,SysConst.UPDATE);
        return true;
    }


    public VData getResult() {
        VData tVData = new VData();
        tVData.add(map);
        return tVData;
    }

}
