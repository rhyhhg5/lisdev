package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.XmlExport;


/**
* <p>Title: 无名单实名化删除</p>
* <p>Description: 无名单实名化删除</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListWDUI
{
    PrtGrpInsuredListWDBL mPrtGrpInsuredListWDBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListWDUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListWDBL = new PrtGrpInsuredListWDBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListWDBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public XmlExport getInputStream()
    {
        return mPrtGrpInsuredListWDBL.getInputStream();
    }
}
