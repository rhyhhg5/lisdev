package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发保全确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class GrpEdorLRConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private GlobalInput mGlobalInput = new GlobalInput();

    public GrpEdorLRConfirmBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }
        System.out.println("---End prepareData---");

        //数据操作业务处理
//        PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//        if (!tPEdorConfirmBLS.submitData(mInputData, mOperate))
//        {
//            CError.buildErr(this, "数据提交失败", tPEdorConfirmBLS.mErrors);
//            return false;
//        }




        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorItemSchema",
                    0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        if (mLPGrpEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorNIAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        if (!tLPGrpEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }
        mLPGrpEdorMainSchema = tLPGrpEdorMainDB.getSchema();

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        mResult.clear();
        MMap map = new MMap();

//        String tSql =
//                "update LCGrpCont set lostTimes = lostTimes + 1 where GrpContNo='" +
//                mLPGrpEdorItemSchema.getGrpContNo() + "'";
//        String strSql =
//                "update LCGrpCont set printcount = 0 where GrpContNo = '" +
//                mLPGrpEdorItemSchema.getGrpContNo() + "'";

        mLPGrpEdorMainSchema.setEdorState("0");
        mLPGrpEdorMainSchema.setConfDate(PubFun.getCurrentDate());
        mLPGrpEdorMainSchema.setConfOperator(mGlobalInput.Operator);
        //当前日期为保全生效日期
        mLPGrpEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());

        mLPGrpEdorItemSchema.setEdorState("0");
        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCGrpContSchema=tLCGrpContDB.getSchema();
        tLCGrpContSchema.setLostTimes(tLCGrpContSchema.getLostTimes()+1);
        tLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        //简易单不修改打印次数 qulq 2007-11-28
        if(!("0".equals(tLCGrpContSchema.getCardFlag())))
        {
            tLCGrpContSchema.setPrintCount(0);
        }
        map.put(mLPGrpEdorItemSchema, "UPDATE");
        map.put(mLPGrpEdorMainSchema, "UPDATE");
        map.put(tLCGrpContSchema, "UPDATE");
        mResult.add(map);

        return true;
    }
}
