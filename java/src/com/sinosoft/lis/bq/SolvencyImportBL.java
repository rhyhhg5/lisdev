package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 偿付能力充足率导入类</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 20</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class SolvencyImportBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String fileName = null;

    private String configFileName = null;

    private String sheetName = "Sheet1";
    
    private String mCodeType=null;

    private static String mCurrentDate = PubFun.getCurrentDate();

    private static String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public SolvencyImportBL(String fileName, String configFileName)
    {
        this.fileName = fileName;
        this.configFileName = configFileName;
//        this.sheetName=sheetName;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到输入数据
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            LDRiskRateSchema tLDRiskRateSchema = (LDRiskRateSchema)
                    data.getObjectByObjectName("LDRiskRateSchema", 0);
            mCodeType=tLDRiskRateSchema.getCodeType();
            System.out.println("mCodeType:"+mCodeType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        deleteImportData();
        LDRiskRateSet tLDRiskRateSet = getLDRiskRateSet();
        if(tLDRiskRateSet.size()!=1){
        	return false;
        }
        setLDRiskRateSet(tLDRiskRateSet);
        mMap.put(tLDRiskRateSet, "INSERT");
        return true;
    }

    /**
     * 导入前先清空上次导入数据
     */
    private void deleteImportData()
    {
        String sql = "delete from LDRiskRate " +
        "where state = '2' " +
        "with ur ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 得到LDRiskRateSet，调用磁盘导入程序
     * @return LDRiskRateSet
     */
    private LDRiskRateSet getLDRiskRateSet()
    {
    	SolvencyDiskImporter importer = new SolvencyDiskImporter(fileName, configFileName,
                sheetName);
        if (!importer.doImport())
        {
            mErrors.copyAllErrors(importer.mErrrors);
            return null;
        }
        return (LDRiskRateSet) importer.getSchemaSet();
    }

    /**
     * 设置LDRiskRateSet
     * @param aLDRiskRateSet LDRiskRateSet
     */
    private void setLDRiskRateSet(LDRiskRateSet aLDRiskRateSet)
    {
    	System.out.println(aLDRiskRateSet.size());
        for (int i = 1; i <= aLDRiskRateSet.size(); i++)
        {
        	System.out.println(aLDRiskRateSet.size());
            LDRiskRateSchema tLDRiskRateSchema = aLDRiskRateSet.get(i);
            tLDRiskRateSchema.setIdNo("1");
            tLDRiskRateSchema.setCodeType(mCodeType);
            tLDRiskRateSchema.setState("2");  //模板导入
            tLDRiskRateSchema.setOperater(mGlobalInput.Operator);
            tLDRiskRateSchema.setMakeDate(mCurrentDate);
            tLDRiskRateSchema.setMakeTime(mCurrentTime);
            tLDRiskRateSchema.setModifyDate(mCurrentDate);
            tLDRiskRateSchema.setModifyTime(mCurrentTime);
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
