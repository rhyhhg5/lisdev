package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全批改项目处理类 </p>
 * <p>Description: 添加和删除保全项目 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * <p>rewrite by QiuYang 2005 </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */

public class PEdorItemBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();
    private TransferData tTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**全局变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 个人批改主表 */
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();

    /** 批改项目表 */
    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();

    public PEdorItemBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData 传入的数据
     * @param cOperate String  数据操作字符串
     * @return boolean
     */
     public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (getInputData() == false)
            return false;
        System.out.println("---getInputData---");

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");
        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PEdorItemBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");
        return true;
    }


    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPEdorMainSchema.setSchema( (LPEdorMainSchema) mInputData.
                                    getObjectByObjectName("LPEdorMainSchema", 0));
        mLPEdorItemSet.set( (LPEdorItemSet) mInputData.
                           getObjectByObjectName("LPEdorItemSet", 0));

        if (mLPEdorMainSchema == null || mLPEdorItemSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到个人批改主表或者批改项目表!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 判断是否是续期催收过程之中
     * @return boolean true 正在催收， false 已核销
     */
    private boolean checkDueFee(String contNo)
    {
        String sql = "select * from LJSPay " +
                "where OtherNoType = '2' " + //2是个单续期
                "and OtherNo = '" + contNo + "' ";
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0)
        {
            mErrors.addOneError("该保单处于续期待收费状态，续期核销之后才能处理保全业务！");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        if (!mOperate.equals("INSERT||EDORITEM")
            && !mOperate.equals("DELETE||EDORITEM"))
        {
            System.out.println("只能新增或者删除!");
            return false;
        }

        if (mOperate.equals("INSERT||EDORITEM"))
        {
            System.out.println("checkDueFee");
            if (!checkDueFee(mLPEdorMainSchema.getContNo()))
            {
                return false;
            }

            //判断该保单下是否存在未保全确认的该申请项目，防止重复申请
            for (int i = 0; i < mLPEdorItemSet.size(); i++)
            {
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setSchema(mLPEdorItemSet.get(i + 1));

                String sql = "select * from LPEdorItem" +
                             "where ContNo = '" + tLPEdorItemSchema.getContNo() + "' " +
                             "and   EdorType = '" + tLPEdorItemSchema.getEdorType() + "' " +
                             "and   EdorState in ('1', '2') ";
                LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
                LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
                if (tLPEdorItemSet.size() != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorItemBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "保单" + tLPEdorItemSchema.getContNo() +
                            "下已申请过" + tLPEdorItemSchema.getEdorType() +
                            "项目，保全受理号为" + tLPEdorItemSchema.getEdorAcceptNo();
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
        if (mOperate.equals("INSERT||EDORITEM"))
        {
            addEdorItem();
        }
        else if (mOperate.equals("DELETE||EDORITEM"))
        {
            delEdorItem();
        }

        return true;
    }

    /**
     * 添加保全批改项目
     */
    private void addEdorItem()
    {
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            mLPEdorItemSet.get(i).setEdorState("1");  //已申请，未申请确认
            mLPEdorItemSet.get(i).setEdorValiDate(PubFun.getCurrentDate());
            mLPEdorItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLPEdorItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLPEdorItemSet.get(i).setManageCom(mGlobalInput.ManageCom);
            mLPEdorItemSet.get(i).setOperator(mGlobalInput.Operator);
            mLPEdorItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
            mLPEdorItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
        }
        map.put(mLPEdorItemSet, "INSERT");
    }

    /**
     * 删除保全项目
     */
    private void delEdorItem()
    {
        map.put(mLPEdorItemSet, "DELETE");
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mResult.clear();
        mResult.add(mLPEdorItemSet);
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult()
    {
        return mResult;
    }

}
