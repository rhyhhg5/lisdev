package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:自动核保</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorAutoUWUI
{
    private PEdorAutoUWBL mPEdorAutoUWBL = null;


    public PEdorAutoUWUI(GlobalInput gi, String edorNo)
    {
        this.mPEdorAutoUWBL = new PEdorAutoUWBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorAutoUWBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回自核错误结果
     * @return String
     */
    public String getUWErrors()
    {
        return mPEdorAutoUWBL.getUWErrors();
    }

    /**
     * 返回核保标志
     * @return String
     */
    public String getUWState()
    {
        return mPEdorAutoUWBL.getUWState();
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorAutoUWBL.mErrors.getFirstError();
    }

    /**
      * 主函数，测试用
      * @param args String[]
      */
     public static void main(String[] args)
     {
         GlobalInput gi = new GlobalInput();
         gi.ManageCom = "86";
         gi.Operator = "endor";

         PEdorAutoUWUI tPEdorAutoUWUI = new PEdorAutoUWUI(gi, "20060324000005");
         if (!tPEdorAutoUWUI.submitData())
         {
             System.out.println(tPEdorAutoUWUI.getError());
         }
     }
}
