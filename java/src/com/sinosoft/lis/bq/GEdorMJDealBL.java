package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.operfee.FeeConst;
import com.sinosoft.task.TaskAutoExaminationBL;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 满期处理类
 * 两种满期处理：1，续保；2，满期终止
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJDealBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null; //存储外部传入的特殊数据

    private LGWorkSchema mLGWorkSchema = null;

    private Reflections ref = new Reflections();

    private MMap map = new MMap();

    private String mEdorNo = null; //保全受理好
    private String mEdorType = null; //保全项目
    private String mDealType = null; //满期操作类型：1，续保；2，终止
    private String mGetNo = null;  //给付号
    private String mGetNoticeNo = null;  //应收记录号
    private String mSerialNo = null;  //应收批次号
    private String mPayNo = null;  //收据号

    private String mManageCom = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private boolean mIsKeep = false;  //是否续保标志，false否

    private double mSumGetMoney = 0;

    private boolean mNeedConfirm = false;  //是否需送审批标准， false不需要，true需要

    public GEdorMJDealBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        LPGrpContDB tLPGrpContDB = new LPGrpContDB();
        tLPGrpContDB.setEdorNo(mEdorNo);
        tLPGrpContDB.setEdorType(mEdorType);
        LPGrpContSet tLPGrpContSet = tLPGrpContDB.query();
        if (tLPGrpContSet.size() == 0)
        {
            mErrors.addOneError("没有需要处理的保单。");
            return false;
        }

        if(mDealType.equals(BQ.ENDTIME_KEEP))
        {
            mIsKeep = true;
            if(!dealContKeep(tLPGrpContSet))
            {
                return false;
            }
        }
        else if(mDealType.equals(BQ.ENDTIME_STOP))
        {
            mIsKeep = false;
            if(!dealStop(tLPGrpContSet))
            {
                return false;
            }
        }

        if (!prepareOutputData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
        tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_MJSTATE);
        LPEdorEspecialDataSet tLPEdorEspecialDataSet =
                tLPEdorEspecialDataDB.query();
        if (tLPEdorEspecialDataSet.size() == 0)
        {
            mErrors.addOneError("满期结算状态有问题。");
            return false;
        }
        if (tLPEdorEspecialDataSet.get(1)
            .getEdorValue().equals(BQ.MJSTATE_FINISH))
        {
            mErrors.addOneError("满期结算已处理完毕，不能再进行业务操所。");
            return false;
        }

        //若帐户里的钱和理算结果不一致不可以结案，需要重复理算。
        //modify by fuxin 2008-5-21 IT要求修改满期流程
        if (!checkAccTrace()){
            return false ;
        }

        //若前面还有保全项目为完成，则不可做满期处理
        if (hasEdorItemDoing()) {
            return false;
        }

        //若正在理赔，则不可做满期处理
        if (checkClaiming()) {
            return false;
        }

        if (!autoExamination())
        {
            return false;
        }

        return true;
    }

    /**
     * 自动审批
     * @return boolean
     */
    private boolean autoExamination()
    {
        VData data = new VData();
        LGWorkSchema workSchema = new LGWorkSchema();
        workSchema.setWorkNo(mEdorNo);
        data.add(mGlobalInput);
        data.add(workSchema);
        TaskAutoExaminationBL exame = new TaskAutoExaminationBL();
        if (!exame.submitData(data, ""))
        {
            //送审批成功，则本类错误信息中保存送审批原因
            if (exame.sendConfirmSuccess())
            {
                mErrors.copyAllErrors(exame.getSendConfirmReason());
                mNeedConfirm = true;
            }
            else
            {
                mErrors.copyAllErrors(exame.mErrors);
                mNeedConfirm = false;
            }
            return false;
        }
        return true;
    }

    /**
     * 是否需要上级审批的标志
     * @return boolean:true需要，false不需要
     */
    public boolean needConfirm()
    {
        return mNeedConfirm;
    }

    private boolean prepareOutputData()
    {
        mLGWorkSchema.setStatusNo(com.sinosoft.task.Task.WORKSTATUS_DONE);
        mLGWorkSchema.setOperator(mGlobalInput.Operator);
        mLGWorkSchema.setModifyDate(PubFun.getCurrentDate());
        mLGWorkSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLGWorkSchema, "UPDATE");

        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        tLPEdorEspecialDataDB.setEdorNo(mEdorNo);
        tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_MJSTATE);
        LPEdorEspecialDataSet tLPEdorEspecialDataSet =
                tLPEdorEspecialDataDB.query();
        tLPEdorEspecialDataSet.get(1).setEdorValue(BQ.MJSTATE_FINISH);
        map.put(tLPEdorEspecialDataSet.get(1), "UPDATE");

        LPEdorEspecialDataDB especialDataDB = new LPEdorEspecialDataDB();
        especialDataDB.setEdorAcceptNo(mEdorNo);
        especialDataDB.setEdorType(mEdorType);
        especialDataDB.setEdorNo(mEdorNo);
        especialDataDB.setPolNo(BQ.FILLDATA);
        especialDataDB.setDetailType(BQ.DETAILTYPE_ENDTIME_DEAL);
        especialDataDB.setEdorValue(mDealType);
        map.put(especialDataDB.getSchema(), "DELETE&INSERT");

        return true;
    }

    /**
     * 得到传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) cInputData
                        .getObjectByObjectName("TransferData", 0);

        if (mGlobalInput == null || mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mEdorNo = (String) mTransferData.getValueByName("edorNo");
        mEdorType = (String) mTransferData.getValueByName("edorType");
        mDealType = (String) mTransferData.getValueByName("dealType");

        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorNo);
        if (!tLGWorkDB.getInfo())
        {
            tLGWorkDB = null;
        }

        if (mEdorNo == null || mEdorType == null
            || mDealType == null || tLGWorkDB == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mLGWorkSchema = tLGWorkDB.getSchema();

        return true;
    }

    /**
     * 满期续期处理
     * @return boolean
     */
    private boolean dealContKeep(LPGrpContSet tLPGrpContSet)
    {
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(this.mEdorNo, this.mEdorType);
        if (!tEdorItemSpecialData.query())
        {
            mErrors.addOneError("没有取到账户利率。");
            return false;
        }
        LPEdorEspecialDataSet set = tEdorItemSpecialData.getSpecialDataSet();
        for (int i = 1; i <= set.size(); i++)
        {
            if (set.get(i).getDetailType().equals(BQ.DETAILTYPE_ACCRATE))
            {
                mTransferData.setNameAndValue(BQ.DETAILTYPE_ACCRATE,
                                              set.get(i).getEdorValue());
                break;
            }
        }
        mTransferData.setNameAndValue(BQ.EDORNO, mEdorNo);

        VData data = new VData();
        for (int i = 1; i <= tLPGrpContSet.size(); i++)
        {
            mManageCom = tLPGrpContSet.get(i).getManageCom();

            data.clear();
            data.add(this.mTransferData);
            data.add(tLPGrpContSet.get(i).getSchema());
            data.add(this.mGlobalInput);

            GEdorMJXBBL tGEdorMJXBBL = new GEdorMJXBBL();
            if (!tGEdorMJXBBL.submitData(data, ""))
            {
                mErrors.copyAllErrors(tGEdorMJXBBL.mErrors);
                return false;
            }

            if(!setFianceKeep(tLPGrpContSet.get(i)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 续保财务数据处理：
     * 同时生成实退实收数据
     * @return boolean
     */
    private boolean setFianceKeep(LPGrpContSchema cLPGrpContSchema)
    {
        //生成实退数据
        LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
        tLPGrpPolDB.setEdorNo(mEdorNo);
        tLPGrpPolDB.setEdorType(mEdorType);
        tLPGrpPolDB.setGrpContNo(cLPGrpContSchema.getGrpContNo());
        LPGrpPolSet tLPGrpPolSet = tLPGrpPolDB.query();
        for(int i = 1; i <= tLPGrpPolSet.size(); i++)
        {
            setLJAGetEndorseInfo(tLPGrpPolSet.get(i));
            //实收
        }

        if (!setLJAGetInfo(cLPGrpContSchema))
        {
            return false;
        }

        return true;
    }

    /**
     * 满期终止处理
     * @return boolean
     */
    private boolean dealStop(LPGrpContSet tLPGrpContSet)
    {
        for (int i = 1; i <= tLPGrpContSet.size(); i++)
        {
            mManageCom = tLPGrpContSet.get(i).getManageCom();

            //做了变更的团单险种信息
            LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
            tLPGrpPolDB.setEdorNo(mEdorNo);
            tLPGrpPolDB.setEdorType(mEdorType);
            tLPGrpPolDB.setGrpContNo(tLPGrpContSet.get(i).getGrpContNo());
            LPGrpPolSet tLPGrpPolSet = tLPGrpPolDB.query();

            //团单所拥有的险种信息
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(tLPGrpContSet.get(i).getGrpContNo());
            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

            if (tLPGrpPolSet.size() == tLCGrpPolSet.size())
            {
                if (!dealContStop(tLPGrpContSet.get(i)))
                {
                    return false;
                }
            }

            for (int temp = 1; temp <= tLPGrpPolSet.size(); temp++)
            {
                if (!dealGrpPolStop(tLPGrpPolSet.get(i)))
                {
                    return false;
                }

                if (!setLJAGetEndorseInfo(tLPGrpPolSet.get(i)))
                {
                    return false;
                }
            }
        }

        if (!setLJAGetInfo(tLPGrpContSet.get(1)))
        {
            return false;
        }

        return true;
    }

    /**
     * 生成实退费信息
     * @param tLPGrpContSchema LPGrpContSchema
     * @return boolean
     */
    private boolean setLJAGetInfo(LPGrpContSchema tLPGrpContSchema)
    {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(getActuGetNo());
        tLJAGetSchema.setOtherNo(mEdorNo);
        tLJAGetSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJAGetSchema.setManageCom(tLPGrpContSchema.getManageCom());
        tLJAGetSchema.setApproveCode(tLPGrpContSchema.getApproveCode());
        tLJAGetSchema.setApproveDate(tLPGrpContSchema.getApproveDate());
        tLJAGetSchema.setAgentCode(tLPGrpContSchema.getAgentCode());
        tLJAGetSchema.setAgentGroup(tLPGrpContSchema.getAgentGroup());
        tLJAGetSchema.setAgentCom(tLPGrpContSchema.getAgentCom());
        tLJAGetSchema.setAgentType(tLPGrpContSchema.getAgentType());
        tLJAGetSchema.setAppntNo(mLGWorkSchema.getCustomerNo());
        tLJAGetSchema.setSumGetMoney(Math.abs(mSumGetMoney));
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setOperator(mGlobalInput.Operator);
        tLJAGetSchema.setMakeDate(this.mCurDate);
        tLJAGetSchema.setMakeTime(this.mCurTime);
        tLJAGetSchema.setModifyDate(this.mCurDate);
        tLJAGetSchema.setModifyTime(this.mCurTime);
        tLJAGetSchema.setPayMode(
                (String) mTransferData.getValueByName("payMode"));
        if (tLJAGetSchema.getPayMode().equals("4"))
        {
            tLJAGetSchema.setBankCode(
                    (String) mTransferData.getValueByName("bank"));
            tLJAGetSchema.setBankAccNo(
                    (String) mTransferData.getValueByName("bankAccno"));
            tLJAGetSchema.setShouldDate(
                    (String) mTransferData.getValueByName("payDate"));
        }

        map.put("delete from LJAGet where otherNo = '"
                + tLJAGetSchema.getOtherNo() + "' ",
                "DELETE");
        map.put(tLJAGetSchema, "INSERT");

        //实收
        if(mIsKeep)
        {
            tLJAGetSchema.setEnterAccDate(this.mCurDate);
            tLJAGetSchema.setConfDate(this.mCurDate);
            tLJAGetSchema.setPayMode("13");

            map.add(getLJAPayInfo(tLJAGetSchema, tLPGrpContSchema));
            map.add(getLJTempClass(tLJAGetSchema, tLPGrpContSchema));
        }

        return true;
    }

    /**
     * 生成暂收费分类表
     * @param tLJAGetSchema LJAGetSchema
     * @param tLPGrpContSchema LPGrpContSchema
     * @return MMap
     */
    private MMap getLJTempClass(LJAGetSchema tLJAGetSchema,
                                LPGrpContSchema tLPGrpContSchema)
    {
        MMap tMMap = new MMap();

        LJTempFeeClassSchema LJTempFeeClassSchema = new LJTempFeeClassSchema();
        LJTempFeeClassSchema.setTempFeeNo(this.getGetNoticeNo());
        LJTempFeeClassSchema.setPayMode("13");  //内部转帐
        LJTempFeeClassSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
        LJTempFeeClassSchema.setAppntName(tLPGrpContSchema.getGrpName());
        LJTempFeeClassSchema.setPayDate(this.mCurDate);
        LJTempFeeClassSchema.setConfDate(mCurDate);
        LJTempFeeClassSchema.setApproveDate(mCurDate);
        LJTempFeeClassSchema.setEnterAccDate(mCurDate);
        LJTempFeeClassSchema.setConfFlag("1");
        LJTempFeeClassSchema.setSerialNo(this.getSerialNo());
        LJTempFeeClassSchema.setManageCom(tLPGrpContSchema.getManageCom());
        LJTempFeeClassSchema.setConfMakeDate(mCurDate);
        LJTempFeeClassSchema.setConfMakeTime(this.mCurTime);
        LJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(LJTempFeeClassSchema);
        tMMap.put(LJTempFeeClassSchema, SysConst.INSERT);

        return tMMap;
    }

    //得到实收保费
    private MMap getLJAPayInfo(LJAGetSchema cLJAGetSchema,
                           LPGrpContSchema tLPGrpContSchema)
    {
        MMap tMMap = new MMap();

        //应收备份
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(getGetNoticeNo()); // 通知书号
        tLJSPayBSchema.setOtherNo(tLPGrpContSchema.getGrpContNo());
        tLJSPayBSchema.setOtherNoType("1");
        tLJSPayBSchema.setAppntNo(tLPGrpContSchema.getAppntNo());
        tLJSPayBSchema.setSumDuePayMoney(cLJAGetSchema.getSumGetMoney());
        tLJSPayBSchema.setPayDate(this.mCurDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPayBSchema.setStartPayDate(this.mCurDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPayBSchema.setBankOnTheWayFlag("0");
        tLJSPayBSchema.setBankSuccFlag("0");
        tLJSPayBSchema.setSendBankCount(0);
        tLJSPayBSchema.setApproveCode(tLPGrpContSchema.getApproveCode());
        tLJSPayBSchema.setApproveDate(tLPGrpContSchema.getApproveDate());
        tLJSPayBSchema.setRiskCode(BQ.FILLDATA);
        tLJSPayBSchema.setBankAccNo(tLPGrpContSchema.getBankAccNo());
        tLJSPayBSchema.setBankCode(tLPGrpContSchema.getBankCode());
        tLJSPayBSchema.setSerialNo(this.getSerialNo());
        tLJSPayBSchema.setOperator(mGlobalInput.Operator);
        tLJSPayBSchema.setManageCom(tLPGrpContSchema.getManageCom());
        tLJSPayBSchema.setAgentCom(tLPGrpContSchema.getAgentCom());
        tLJSPayBSchema.setAgentCode(tLPGrpContSchema.getAgentCode());
        tLJSPayBSchema.setAgentType(tLPGrpContSchema.getAgentType());
        tLJSPayBSchema.setAgentGroup(tLPGrpContSchema.getAgentGroup());
        tLJSPayBSchema.setMakeDate(mCurDate);
        tLJSPayBSchema.setMakeTime(mCurTime);
        tLJSPayBSchema.setModifyDate(mCurDate);
        tLJSPayBSchema.setModifyTime(mCurTime);
        tLJSPayBSchema.setDealState(FeeConst.DEALSTATE_URGESUCCEED);
        tLJSPayBSchema.setConfFlag("1");
        tMMap.put(tLJSPayBSchema, SysConst.INSERT);

        //实收
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(getPayNo()); //交费收据号码
        tLJAPaySchema.setIncomeNo(tLPGrpContSchema.getGrpContNo()); //应收/实收编号,集体合同号
        tLJAPaySchema.setIncomeType("1"); //应收/实收编号类型
        tLJAPaySchema.setAppntNo(tLPGrpContSchema.getAppntNo()); //  投保人客户号码
        tLJAPaySchema.setSumActuPayMoney(cLJAGetSchema.getSumGetMoney()); // 总实交金额
        tLJAPaySchema.setGetNoticeNo(getGetNoticeNo()); //交费收据号,暂时先取应收总表的通知号
        tLJAPaySchema.setEnterAccDate(this.mCurDate); // 到帐日期
        tLJAPaySchema.setPayDate(this.mCurDate); //交费日期,仍然取了最小日期
        tLJAPaySchema.setConfDate(this.mCurDate); //确认日期
        tLJAPaySchema.setApproveCode(tLPGrpContSchema.getApproveCode()); //复核人编码
        tLJAPaySchema.setApproveDate(tLPGrpContSchema.getApproveDate()); //  复核日期
        tLJAPaySchema.setSerialNo(getSerialNo()); //流水号
        tLJAPaySchema.setOperator(mGlobalInput.Operator); // 操作员
        tLJAPaySchema.setMakeDate(this.mCurDate); //入机时间
        tLJAPaySchema.setMakeTime(this.mCurTime); //入机时间
        tLJAPaySchema.setModifyDate(this.mCurDate); //最后一次修改日期
        tLJAPaySchema.setModifyTime(this.mCurTime); //最后一次修改时间
        tLJAPaySchema.setRiskCode(BQ.FILLDATA); // 险种编码
        tLJAPaySchema.setManageCom(tLPGrpContSchema.getManageCom());
        tLJAPaySchema.setAgentCom(tLPGrpContSchema.getAgentCom());
        tLJAPaySchema.setAgentType(tLPGrpContSchema.getAgentType());
        tLJAPaySchema.setBankCode(tLPGrpContSchema.getBankCode());
        tLJAPaySchema.setBankAccNo(tLPGrpContSchema.getBankAccNo());
        tLJAPaySchema.setAccName(tLPGrpContSchema.getAccName());
        tLJAPaySchema.setStartPayDate(this.mCurDate);
        tLJAPaySchema.setAgentCode(tLPGrpContSchema.getAgentCode());
        tLJAPaySchema.setAgentGroup(tLPGrpContSchema.getAgentGroup());
        tMMap.put(tLJAPaySchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 生成财务数据
     * 若为续保，则需要同时生成实付和实收数据
     * 若为满期终止，则只需要生成实付数据
     * @return boolean
     */
    private boolean setLJAGetEndorseInfo(LPGrpPolSchema tLPGrpPolSchema)
    {
        map.put("delete from LJAGetEndorse where endorsementNo = '" + mEdorNo
                + "'  and feeOperationType = '" + BQ.EDORTYPE_MJ
                + "'  and grpPolNo = '" + tLPGrpPolSchema.getGrpPolNo() + "' ",
                "DELETE");

        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setEndorsementNo(tLPGrpPolSchema.getEdorNo());
        tLJSGetEndorseDB.setFeeOperationType(tLPGrpPolSchema.getEdorType());
        tLJSGetEndorseDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
        if(tLJSGetEndorseSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "GEdorMJDealBL";
            tError.functionName = "setLJAGetEndorseInfo";
            tError.errorMessage = "没有查询到保单险种退费信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String actuGetNo = getActuGetNo();  //实付号

        double grpPolGetMoney = 0;
        
        
        
        int start = 1;
        int count = 5000; //每次取count条个险数据进行处理
        tLJSGetEndorseSet = new LJSGetEndorseSet();
        do
        {
	        String sql = "select * from LJSGetEndorse where EndorsementNo='"+mEdorNo+"' and FeeOperationType='"+tLPGrpPolSchema.getEdorType()+"' and GrpPolNo='"+tLPGrpPolSchema.getGrpPolNo()+"' ";      
	        tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(sql, start, count);
	        
	        
	        for(int i = 1; i <= tLJSGetEndorseSet.size(); i++)
	        {
	            LJAGetEndorseSchema schema = new LJAGetEndorseSchema();
	            ref.transFields(schema, tLJSGetEndorseSet.get(i));
	            schema.setActuGetNo(actuGetNo);
	            schema.setOperator(mGlobalInput.Operator);
	            schema.setMakeDate(this.mCurDate);
	            schema.setMakeTime(this.mCurTime);
	            schema.setModifyDate(this.mCurDate);
	            schema.setModifyTime(this.mCurTime);
	            if(mIsKeep)
	            {
	                schema.setEnterAccDate(this.mCurDate);
	                schema.setGetConfirmDate(this.mCurDate);
	            }
	            map.put(schema, SysConst.INSERT);

	            grpPolGetMoney += schema.getGetMoney();
	        }

	        
	        start=start+count;
        }while(tLJSGetEndorseSet.size() > 0);
        
        
        
        map.put("delete from LJSGetEndorse where endorsementNo = '" + mEdorNo
                + "'  and feeOperationType = '" + BQ.EDORTYPE_MJ
                + "'  and grpPolNo = '" + tLPGrpPolSchema.getGrpPolNo() + "' ",
                "DELETE");


        //实收
        if(mIsKeep)
        {
            map.add(getLJAPayGrp(grpPolGetMoney, tLPGrpPolSchema));
            map.add(getLJTempFee(grpPolGetMoney, tLPGrpPolSchema));
        }

        mSumGetMoney += Math.abs(grpPolGetMoney);

        return true;
    }

    /**
     * 生成险种收费记录LJSPayGrpB,LJAPayGrp
     * @param tLJAGetEndorseSchema LJAGetEndorseSchema
     * @param tLPGrpPolSchema LPGrpPolSchema
     * @return MMap
     */
    private MMap getLJAPayGrp(double grpPolGetMoney,
                              LPGrpPolSchema tLPGrpPolSchema)
    {
        MMap tMMap = new MMap();

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        if(!tLCGrpPolDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "GEdorMJDealBL";
            tError.functionName = "getLJAPayPerson";
            tError.errorMessage = "查询续保险种出错"
                                  + tLPGrpPolSchema.getRiskCode();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        //团险
        LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
        tLJSPayGrpBSchema.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        tLJSPayGrpBSchema.setGrpContNo(tLPGrpPolSchema.getGrpContNo());
        tLJSPayGrpBSchema.setAppntNo(mLGWorkSchema.getCustomerNo()); //随便取一个lcpol的投保人
        tLJSPayGrpBSchema.setPayCount(1); //交费次数
        tLJSPayGrpBSchema.setGetNoticeNo(this.getGetNoticeNo()); // 通知书号
        tLJSPayGrpBSchema.setSumDuePayMoney(Math.abs(grpPolGetMoney));
        tLJSPayGrpBSchema.setSumActuPayMoney(tLJSPayGrpBSchema.getSumDuePayMoney());
        tLJSPayGrpBSchema.setPayIntv(tLPGrpPolSchema.getPayIntv());
        tLJSPayGrpBSchema.setPayDate(this.mCurDate); //交费日期
        tLJSPayGrpBSchema.setPayType("ZC"); //交费类型=ZC 正常交费
        tLJSPayGrpBSchema.setLastPayToDate(tLPGrpPolSchema.getPaytoDate());
        tLJSPayGrpBSchema.setCurPayToDate(tLCGrpPolDB.getPaytoDate());
        tLJSPayGrpBSchema.setApproveCode(tLPGrpPolSchema.getApproveCode());
        tLJSPayGrpBSchema.setApproveDate(tLPGrpPolSchema.getApproveDate());
        tLJSPayGrpBSchema.setApproveTime(tLPGrpPolSchema.getApproveTime());
        tLJSPayGrpBSchema.setManageCom(tLCGrpPolDB.getManageCom());
        tLJSPayGrpBSchema.setAgentCom(tLPGrpPolSchema.getAgentCom());
        tLJSPayGrpBSchema.setAgentType(tLPGrpPolSchema.getAgentType());
        tLJSPayGrpBSchema.setRiskCode(tLPGrpPolSchema.getRiskCode());
        tLJSPayGrpBSchema.setSerialNo(this.getSerialNo());
        tLJSPayGrpBSchema.setInputFlag("1");
        tLJSPayGrpBSchema.setConfFlag("1");
        tLJSPayGrpBSchema.setOperator(mGlobalInput.Operator);
        tLJSPayGrpBSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
        tLJSPayGrpBSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
        PubFun.fillDefaultField(tLJSPayGrpBSchema);
        tLJSPayGrpBSchema.setDealState(FeeConst.DEALSTATE_URGESUCCEED); //催收状态：待核销
        tMMap.put(tLJSPayGrpBSchema, SysConst.INSERT);

        //团险实收
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        ref.transFields(tLJAPayGrpSchema, tLJSPayGrpBSchema);
        tLJAPayGrpSchema.setPayNo(getPayNo());
        tLJAPayGrpSchema.setEnterAccDate(this.mCurDate);
        tLJAPayGrpSchema.setConfDate(this.mCurDate);
        tMMap.put(tLJAPayGrpSchema, SysConst.INSERT);

        //团险下个险
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        for(int i = 1; i <= tLCInsureAccClassSet.size(); i++)
        {
            LCInsureAccClassSchema classchema = tLCInsureAccClassSet.get(i);

            //得到DutyCode与PlanPlanCode的关系
            LMDutyPayRelaDB tLMDutyPayRelaDB = new LMDutyPayRelaDB();
            tLMDutyPayRelaDB.setPayPlanCode(classchema.getPayPlanCode());
            LMDutyPayRelaSet set = tLMDutyPayRelaDB.query();

            //生成个人应收
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema.setPolNo(classchema.getPolNo()); // 保单号码
            tLJAPayPersonSchema.setPayCount("1"); // 第几次交费
            tLJAPayPersonSchema.setGrpContNo(classchema.getGrpContNo()); // 集体保单号码
            tLJAPayPersonSchema.setGrpPolNo(classchema.getGrpPolNo()); // 集体保单号码
            tLJAPayPersonSchema.setContNo(classchema.getContNo()); // 总单/合同号码
            tLJAPayPersonSchema.setAppntNo(tLJSPayGrpBSchema.getAppntNo()); // 投保人客户号码
            tLJAPayPersonSchema.setPayNo(getPayNo()); // 交费收据号码
            tLJAPayPersonSchema.setPayTypeFlag("1");
            tLJAPayPersonSchema.setPayAimClass("2"); // 交费目的分类
            tLJAPayPersonSchema.setDutyCode(set.get(1).getDutyCode()); // 责任编码
            tLJAPayPersonSchema.setPayPlanCode(classchema.getPayPlanCode()); // 交费计划编码
            tLJAPayPersonSchema.setSumDuePayMoney(classchema.getInsuAccBala()); // 总应交金额
            tLJAPayPersonSchema.setSumActuPayMoney(classchema.getInsuAccBala()); // 总实交金额
            tLJAPayPersonSchema.setPayIntv(tLPGrpPolSchema.getPayIntv()); // 交费间隔
            tLJAPayPersonSchema.setPayDate(this.mCurDate); // 交费日期
            tLJAPayPersonSchema.setPayType("ZC"); // 交费类型
            tLJAPayPersonSchema.setEnterAccDate(this.mCurDate); // 到帐日期
            tLJAPayPersonSchema.setConfDate(this.mCurDate); // 确认日期
            tLJAPayPersonSchema.setLastPayToDate(tLJSPayGrpBSchema.
                                             getLastPayToDate()); // 原交至日期
            tLJAPayPersonSchema.setCurPayToDate(tLJSPayGrpBSchema.
                                            getCurPayToDate()); // 现交至日期
            tLJAPayPersonSchema.setApproveCode(tLPGrpPolSchema.getApproveCode()); // 复核人编码
            tLJAPayPersonSchema.setApproveDate(tLPGrpPolSchema.getApproveDate()); // 复核日期
            tLJAPayPersonSchema.setApproveTime(tLPGrpPolSchema.getApproveTime()); // 复核时间
            tLJAPayPersonSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
            tLJAPayPersonSchema.setSerialNo(this.getSerialNo()); // 流水号
            tLJAPayPersonSchema.setOperator(mGlobalInput.Operator); // 实际截转为实收的操作员
            tLJAPayPersonSchema.setMakeDate(this.mCurDate); // 入机日期
            tLJAPayPersonSchema.setMakeTime(this.mCurTime); // 入机时间
            tLJAPayPersonSchema.setModifyDate(mCurDate); // 最后一次修改日期
            tLJAPayPersonSchema.setModifyTime(this.mCurTime); // 最后一次修改时间
            tLJAPayPersonSchema.setGetNoticeNo(this.getGetNoticeNo()); // 通知书号码
            tLJAPayPersonSchema.setManageCom(tLPGrpPolSchema.getManageCom()); // 管理机构
            tLJAPayPersonSchema.setAgentCom(tLPGrpPolSchema.getAgentCom()); // 代理机构
            tLJAPayPersonSchema.setAgentType(tLPGrpPolSchema.getAgentType()); // 代理机构内部分类
            tLJAPayPersonSchema.setRiskCode(tLPGrpPolSchema.getRiskCode()); // 险种编码
            tMMap.put(tLJAPayPersonSchema, SysConst.INSERT);
        }

        return tMMap;
    }

    /**
     * 生成暂收数据
     * @param tLJAGetEndorseSchema LJAGetEndorseSchema
     * @param tLPGrpPolSchema LPGrpPolSchema
     * @return MMap
     */
    private MMap getLJTempFee(double grpPolGetMoney,
                              LPGrpPolSchema tLPGrpPolSchema)
    {
        MMap tMMap = new MMap();

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(this.getGetNoticeNo());
        tLJTempFeeSchema.setTempFeeType("2");
        tLJTempFeeSchema.setRiskCode(tLPGrpPolSchema.getRiskCode());
        tLJTempFeeSchema.setPayIntv(tLPGrpPolSchema.getPayIntv());
        tLJTempFeeSchema.setOtherNo(tLPGrpPolSchema.getGrpPolNo());
        tLJTempFeeSchema.setOtherNoType("1");
        tLJTempFeeSchema.setPayMoney(Math.abs(grpPolGetMoney));
        tLJTempFeeSchema.setPayDate(this.mCurDate);
        tLJTempFeeSchema.setEnterAccDate(mCurDate);
        tLJTempFeeSchema.setConfDate(mCurDate);
        tLJTempFeeSchema.setConfMakeDate(mCurDate);
        tLJTempFeeSchema.setConfMakeTime(this.mCurTime);
        tLJTempFeeSchema.setSaleChnl(tLPGrpPolSchema.getSaleChnl());
        tLJTempFeeSchema.setManageCom(tLPGrpPolSchema.getManageCom());
        tLJTempFeeSchema.setAgentCom(tLPGrpPolSchema.getAgentCom());
        tLJTempFeeSchema.setAgentType(tLPGrpPolSchema.getAgentType());
        tLJTempFeeSchema.setAgentGroup(tLPGrpPolSchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLPGrpPolSchema.getAgentCode());
        tLJTempFeeSchema.setConfFlag("1");
        tLJTempFeeSchema.setSerialNo(this.getSerialNo());
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLJTempFeeSchema);
        tMMap.put(tLJTempFeeSchema, SysConst.INSERT);

        return tMMap;
    }

    private String getActuGetNo()
    {
        //added by huxl,解决LJAGetEndorse、LJAGet表中ActuGetNo 不一致问题。2006-11-28
        if (this.mGetNo == null || this.mGetNo.equals(""))
        {
            this.mGetNo = PubFun1.CreateMaxNo("GetNO", null);
        }

        return mGetNo;
    }

    /**
     * 得到应收记录号
     * @return String
     */
    private String getGetNoticeNo()
    {
        if(mGetNoticeNo == null)
        {
            mGetNoticeNo = PubFun1.CreateMaxNo("PayNoticeNo",
                                               PubFun.getNoLimit(mManageCom));
        }
        return mGetNoticeNo;
    }

    /**
     * 得到实收序号
     * @return String
     */
    private String getSerialNo()
    {
        if(mSerialNo == null)
        {
            mSerialNo = PubFun1.CreateMaxNo("SERIALNO",
                                            PubFun.getNoLimit(mManageCom));
        }
        return mSerialNo;
    }

    /**
     * 得到应收收据号
     * @return String
     */
    private String getPayNo()
    {
        if(mPayNo == null)
        {
            mPayNo = PubFun1.CreateMaxNo("PayNo", PubFun.getNoLimit(mManageCom));
        }
        return mPayNo;
    }

    /**
     * 团单满期终止
     * @param tLPGrpContSchema LPGrpContSchema
     * @return boolean
     */
    private boolean dealContStop(LPGrpContSchema tLPGrpContSchema)
    {
        tLPGrpContSchema.setState(BQ.CONTSTATE_MJED);

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tLPGrpContSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有查询到保单号为"
                                + tLPGrpContSchema.getGrpContNo()
                                + "的保单。");
            return false;
        }

        tLCGrpContDB.setState(BQ.CONTSTATE_MJED);
        tLCGrpContDB.setStateFlag(BQ.STATE_FLAG_TERMINATE);
        tLCGrpContDB.setOperator(mGlobalInput.Operator);
        tLCGrpContDB.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContDB.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCGrpContDB.getSchema(), "UPDATE");

        //生成保单状态
        String sql = "update LCGrpContState "
                     + "set EndDate = '" + mLGWorkSchema.getMakeDate() + "', "
                     + "   Operator = '" + mGlobalInput.Operator + "', "
                     + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                     + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                     + "where GrpContNo = '" + tLCGrpContDB.getGrpContNo() + "' "
                     + "   and (EndDate is null or EndDate > '"
                     + mLGWorkSchema.getMakeDate() + "') ";
        map.put(sql, SysConst.UPDATE);

        LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();
        tLCGrpContStateSchema.setGrpContNo(tLPGrpContSchema.getGrpContNo());
        tLCGrpContStateSchema.setGrpPolNo("000000");
        tLCGrpContStateSchema.setStateType("Terminate");
        tLCGrpContStateSchema.setStateReason(tLPGrpContSchema.getEdorType());
        tLCGrpContStateSchema.setState("1");
        tLCGrpContStateSchema.setOtherNo(tLPGrpContSchema.getEdorNo());
        tLCGrpContStateSchema.setOtherNoType(tLPGrpContSchema.getEdorType());
        tLCGrpContStateSchema.setOperator(mGlobalInput.Operator);
        tLCGrpContStateSchema.setStartDate(mLGWorkSchema.getMakeDate());
        tLCGrpContStateSchema.setEndDate("9999-12-31");
        tLCGrpContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setContPlanCode("00");
        map.put(tLCGrpContStateSchema, "DELETE&INSERT");

        sql = "update LCCont "
              + "set StateFlag = '" + BQ.STATE_FLAG_TERMINATE + "' "
              + "where GrpContNo = '" + tLPGrpContSchema.getGrpContNo() + "' ";
        map.put(sql, SysConst.UPDATE);

        return true;
    }

    /**
     * 团单险种满期终止
     * @param tLPGrpPolSchema LPGrpPolSchema
     * @return boolean
     */
    private boolean dealGrpPolStop(LPGrpPolSchema tLPGrpPolSchema)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        if (!tLCGrpPolDB.getInfo())
        {
            mErrors.addOneError("没有查询到险种"
                                + tLPGrpPolSchema.getRiskCode()
                                + "的信息。");
            return false;
        }

        tLCGrpPolDB.setState(BQ.CONTSTATE_MJED);
        tLCGrpPolDB.setStateFlag(BQ.STATE_FLAG_TERMINATE);
        tLCGrpPolDB.setOperator(mGlobalInput.Operator);
        tLCGrpPolDB.setModifyDate(PubFun.getCurrentDate());
        tLCGrpPolDB.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCGrpPolDB.getSchema(), "UPDATE");

        String sql = "update LCGrpContState "
                     + "set EndDate = '" + mLGWorkSchema.getMakeDate() + "', "
                     + "   Operator = '" + mGlobalInput.Operator + "', "
                     + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                     + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                     + "where GrpPolNo = '" + tLCGrpPolDB.getGrpPolNo() + "' "
                     + "   and (EndDate is null or EndDate > '"
                     + mLGWorkSchema.getMakeDate() + "') ";
        map.put(sql, SysConst.UPDATE);

        //更新保单状态
        LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();
        tLCGrpContStateSchema.setGrpContNo(tLPGrpPolSchema.getGrpContNo());
        tLCGrpContStateSchema.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        tLCGrpContStateSchema.setStateType("Terminate");
        tLCGrpContStateSchema.setStateReason(tLPGrpPolSchema.getEdorType());
        tLCGrpContStateSchema.setState("1");
        tLCGrpContStateSchema.setOtherNo(tLPGrpPolSchema.getEdorNo());
        tLCGrpContStateSchema.setOtherNoType(tLPGrpPolSchema.getEdorType());
        tLCGrpContStateSchema.setOperator(mGlobalInput.Operator);
        tLCGrpContStateSchema.setStartDate(mLGWorkSchema.getMakeDate());
        tLCGrpContStateSchema.setEndDate("9999-12-31");
        tLCGrpContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setContPlanCode("00");
        map.put(tLCGrpContStateSchema, "DELETE&INSERT");

        map.put("update LCPol set StateFlag = '"
                + BQ.STATE_FLAG_TERMINATE + "' "
                + "where GrpPolNo = '" + tLPGrpPolSchema.getGrpPolNo() + "' ",
                SysConst.UPDATE);


        //账户余额清0
        String[] tables = {" LCInsureAcc ", " LCInsureAccClass "};
        for(int i = 0; i < tables.length; i++)
        {
            sql = "update " + tables[i]
                         + "set InsuAccBala = 0 "
                         + "where GrpPolNo = '"
                         + tLPGrpPolSchema.getGrpPolNo() + "' ";
            map.put(sql, SysConst.UPDATE);
        }

        //添加一条轨迹轨迹表示当前退费
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();

        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setGrpPolNo(tLPGrpPolSchema.getGrpPolNo());
        LCInsureAccClassSet set = tLCInsureAccClassDB.query();
        for(int i = 1; i <= set.size(); i++)
        {
            double money = 0;
            tLCInsureAccTraceDB.setPolNo(set.get(i).getPolNo());
            tLCInsureAccTraceDB.setInsuAccNo(set.get(i).getInsuAccNo());
//            tLCInsureAccTraceDB.setPayPlanCode(set.get(i).getPayPlanCode());
            LCInsureAccTraceSet traceSet = tLCInsureAccTraceDB.query();
            if(traceSet.size() == 0)
            {
                continue;
            }
            for(int j = 1; j <= traceSet.size(); j++)
            {
                money += traceSet.get(j).getMoney();
            }

            LCInsureAccTraceSchema schema = new LCInsureAccTraceSchema();
            ref.transFields(schema, traceSet.get(1));
            schema.setOtherNo(this.mEdorNo);
            schema.setOtherType("3");
            schema.setPayDate(mLGWorkSchema.getMakeDate());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", PubFun.getNoLimit(schema.getManageCom()));
            schema.setSerialNo(serNo);
            schema.setMoney(-money);
            //20120130 理赔账户轨迹金额误差  账户表sumpaym问题 为monwytype 赋值为MJ
            schema.setMoneyType(BQ.EDORTYPE_MJ);
            //
            schema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(schema);

            map.put(schema, SysConst.INSERT);
        }

        return true;
    }

    /**
     * 保存数据
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.addOneError("保存数据失败");
            System.out.println(tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    /**
     * 校验是否有未结案的保全项目
     * @return boolean：有未结案的保全项目
     */
    private boolean hasEdorItemDoing() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPEdorApp a, LPGrpEdorItem b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("   and b.grpContNo = '")
                .append(mLGWorkSchema.getContNo())
                .append("'  and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM)
                .append("' ");

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(
                sql.toString());
        if (tLPGrpEdorItemSet.size() > 0) {
            String errMsg = "";
            for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
                errMsg += tLPGrpEdorItemSet.get(i).getEdorAcceptNo() + ", ";
            }
            mErrors.addOneError("该保单还有受理号为"
                                + errMsg.substring(
                                        0, errMsg.lastIndexOf(",") - 1)
                                + "的保全受理未处理完毕，请先处理以上受理。");
            return true;
        }

        return false;
    }

    /**
     * 校验保单险种是否正在理赔 fuxin modify 强校验
     * @return boolean,正在理赔: true
     */
    private boolean checkClaiming() {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLGWorkSchema.getWorkNo());
        tLGWorkDB.getInfo();

        String sql = "select rgtno from llregister where rgtobjno='" +
                     tLGWorkDB.getContNo()
                     +
                "' and rgtstate not in ('04','05') and declineflag is null ";
        ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if (rs == null || rs.getMaxRow() == 0) {
            LCInsureAccTraceDB db = new LCInsureAccTraceDB();
            db.setGrpContNo(tLGWorkDB.getContNo());
            db.setState("temp");
            LCInsureAccTraceSet tempSet = db.query();
            if (tempSet == null || tempSet.size() == 0) {
                return false;
            } else {
                mErrors.addOneError("帐户轨迹表中有理赔临时记录，不能操作");
                return true;
            }
        } else {
            StringBuffer tem = new StringBuffer();
            tem.append("该单正在理赔或有未撤件的理赔申请，不能操作,批次号");
            for (int i = 1; i <= rs.getMaxRow(); i++) {
                tem.append(rs.GetText(i, 1));
                tem.append("  ");
            }
            mErrors.addOneError(tem.toString());
            return true;
        }

    }

    private boolean checkAccTrace() {
        double LPDiskMoney ;
        double AccMoney ;
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        tLPDiskImportDB.setEdorNo(mEdorNo);
        tLPDiskImportDB.setEdorType(BQ.EDORTYPE_MJ);
        tLPDiskImportSet = tLPDiskImportDB.query();
        if(tLPDiskImportSet.size()==0){
            mErrors.addOneError("查询LPDiskImport表数据失败！");
        }
        LPDiskMoney = Double.parseDouble(new ExeSQL().getOneValue("select sum(money) From LPDiskImport where EdorNo ='"+mEdorNo+"'"));
        AccMoney = Double.parseDouble(new ExeSQL().getOneValue("select sum(money) From LCinsureaccTrace where  GrpContNo ='"+mLGWorkSchema.getContNo()+"'"));
        if(LPDiskMoney != AccMoney)
        {
            mErrors.addOneError("帐户金额和上次理算金额不一致，请重新理算！");
            return false ;
        }
        return true ;
    }


    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "pa1101";
        g.ComCode = "86";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("edorNo", "20061227000012");
        tTransferData.setNameAndValue("edorType", "MJ");
        tTransferData.setNameAndValue("dealType", "1");
        tTransferData.setNameAndValue("prtType", "MJDeal");
        tTransferData.setNameAndValue("payMode", "1");

        VData tVData = new VData();
        tVData.add(g);
        tVData.add(tTransferData);

        GEdorMJDealBL tGEdorMJDealBL = new GEdorMJDealBL();
        if (!tGEdorMJDealBL.submitData(tVData, ""))
        {
            System.out.println(tGEdorMJDealBL.mErrors.getErrContent());
        }
    }
}
