package com.sinosoft.lis.bq;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;

/**
 * <p>Title: Web业务系统保全人工核保保单申请功能部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorAppUWManuApplyChkBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
	private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mManageCom;

    /** 业务处理相关变量 */
    private String mEdorAcceptNo = "";
    /**轨迹锁表**/
    private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
    private LDSysTraceSet mAllLDSysTraceSet = new LDSysTraceSet();

    public PEdorAppUWManuApplyChkBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //由于业务逻辑不明确，现在直接返回true-----fanx

        //校验是不是已经有人申请此保单
//        if (!checkApply())
//        {
//            return false;
//        }
//
//        // 数据操作业务处理
//        if (!dealData())
//        {
//            return false;
//        }
//
//        //准备给后台的数据
//        prepareOutputData();

        //数据提交
//        PEdorUWManuApplyChkBLS tPEdorUWManuApplyChkBLS = new PEdorUWManuApplyChkBLS();
//        System.out.println("Start PEdorUWManuApplyChkBL Submit...");
//        if (!tPEdorUWManuApplyChkBLS.submitData(mInputData, mOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPEdorUWManuApplyChkBLS.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorUWManuApplyChkBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

//        PubSubmit tSubmit = new PubSubmit();
//        if (!tSubmit.submitData(mResult, "")) //数据提交
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tSubmit.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PEdorUWManuApplyChkBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (dealOnePol() == false)
        {
            return false;
        }

        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol()
    {
        // 健康信息
        if (prepareApply() == false)
        {
            return false;
        }

        LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
        tLDSysTraceSet.set(mLDSysTraceSet);
        mAllLDSysTraceSet.add(tLDSysTraceSet);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		if (tGlobalInput == null || tTransferData == null)
		{
			return false;
		}
        mOperate = tGlobalInput.Operator;
        mManageCom = tGlobalInput.ManageCom;
		mEdorAcceptNo = (String) tTransferData.getValueByName("EdorAcceptNo");
		return true;
    }

    /**
     * 准备信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkApply()
    {
        LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
        LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB();
        LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
        String tsql = "";

        //tsql = "select * from ldsystrace where ContNo = '" + mContNo + "' and operator <> '" + mOperate + "' and polstate = 2001"; //保全人工核保

        tLDSysTraceSet = tLDSysTraceDB.executeQuery(tsql);

        if (tLDSysTraceSet.size() > 0)
        {
            tLDSysTraceSchema = tLDSysTraceSet.get(1);

            CError tError = new CError();
            tError.moduleName = "PEdorUWManuApplyChkBL";
            tError.functionName = "checkApply";
            tError.errorMessage = "此保单已经被" + tLDSysTraceSchema.getOperator().trim() + "核保师申请，申请失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareApply()
    {
        LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();

        tLDSysTraceSchema.setCreatePos("个人保全人工核保");
        tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setManageCom(mManageCom);
        tLDSysTraceSchema.setOperator(mOperate);
        tLDSysTraceSchema.setOperator2(mOperate);
        tLDSysTraceSchema.setPolNo(mEdorAcceptNo);
        tLDSysTraceSchema.setPolState(2001);
        tLDSysTraceSchema.setRemark("U");

        mLDSysTraceSet.add(tLDSysTraceSchema);

        return true;
    }

    /**
     *准备需要保存的数据
     **/
    private void prepareOutputData()
    {
        mMap.put(mAllLDSysTraceSet, "DELETE&INSERT");
		mResult.clear();
		mResult.add(mMap);
    }
}
