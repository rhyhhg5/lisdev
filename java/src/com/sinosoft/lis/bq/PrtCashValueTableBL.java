package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成打印现金价值表所需要的XML数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtCashValueTableBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private LPPolSchema mLPPolSchema = null; //需要打印现金价值表的险种
    private LPEdorItemSchema mLPEdorItemSchema = null; //特殊数据
    private GlobalInput mGI = null;
    private int mAffixNo = 0;  //附件号
    private int mRowCount = 43;  //每栏显示的行数
    private String mTitle = null;

    private XmlExport mXmlexport = null;

    ListTable mtListTable = new ListTable();

    public PrtCashValueTableBL()
    {
    }

    /**
     * 出入了附件号的构造方法
     * @param affixNo int
     */
    public PrtCashValueTableBL(int affixNo)
    {
        mAffixNo = affixNo;
    }

    /**
     * 设计每一栏的最大行数
     * @param rowCount int：最大行数
     */
    public void setRowCount(int rowCount)
    {
        mRowCount = rowCount;
    }

    /**
     * 设计每一栏的最大行数
     * @param rowCount int：最大行数
     */
    public void setTitle(String title)
    {
        mTitle = title;
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        //校验数据合法性
        if(!checkData())
        {
            return null;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return null;
        }

        return this.mXmlexport;
    }

    /**
     * 得到传入的数据
     * @param cInputData VData：getXmlExport中传入的VData
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mLPPolSchema = (LPPolSchema) data
                       .getObjectByObjectName("LPPolSchema", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

        if(mLPPolSchema == null)
        {
            mErrors.addOneError("请传入险种信息");
            return false;
        }
        if(mGI == null)
        {
            mGI = new GlobalInput();
            mGI.Operator = "Server";
            mGI.ComCode = "86";
            mGI.ManageCom = "86";
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @return boolean：成功true，否则false
     */
    private boolean getPrintData()
    {
        mXmlexport = new XmlExport(); //新建一个XmlExport的实例
        mXmlexport.createDocument("PrtCashValueTable.vts", "printer"); //最好紧接着就初始化xml文档

        if(!getListTable())
        {
            return false;
        }

        if(!getHeadFoot())
        {
            return false;
        }

        mXmlexport.outputDocumentToFile("C:\\", "PrtCashValueTable");

        return true;
    }

    /**
     * 得到表头表尾
     * @return boolean：成功true，否则false
     */
    private boolean getHeadFoot()
    {
        TextTag tag = new TextTag();

        String edorNo = mLPPolSchema.getEdorNo();
        tag.add("Title", mTitle == null ?
                "现金价值表(批单号" + edorNo + "附件" + mAffixNo + ")" : mTitle);
        tag.add("ContNo", mLPPolSchema.getContNo());
        tag.add("InsuredName", mLPPolSchema.getInsuredName());
        tag.add("RiskName", CommonBL.getRiskName(mLPPolSchema.getRiskCode()));
        tag.add("RiskCode", mLPPolSchema.getRiskCode());
        tag.add("RiskSeqNo", StrTool.cTrim(mLPPolSchema.getRiskSeqNo()));
        tag.add("Unit", "人民币 元"); //货币单位
        tag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        tag.add("Printer", CommonBL.getUserName(mGI.Operator));

        mXmlexport.addTextTag(tag);

        return true;
    }

    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private boolean getListTable()
    {
        GetCashValueTableBL bl = new GetCashValueTableBL();

        bl.setRowCount(mRowCount);
        ListTable[] listTables = bl.getCashTable(mLPPolSchema);
        if(listTables == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        String[] title = {"保单年度", "保单周年日", "现金价值"};
        mXmlexport.addListTable(listTables[0], title);
        mXmlexport.addListTable(listTables[1], title);

        return true;
    }

    /**
     * 校验数据的合法性
     * @return boolean：合法true，否则false
     */
    private boolean checkData()
    {
//        LPPolDB tLPPolDB = new LPPolDB();
//        tLPPolDB.setEdorNo(mLPPolSchema.getEdorNo());
//        tLPPolDB.setEdorType(mLPPolSchema.getEdorType());
//        tLPPolDB.setPolNo(mLPPolSchema.getPolNo());
//
//        if(!tLPPolDB.getInfo())
//        {
//            mErrors.addOneError("没有查询到险种"
//                                + mLPPolSchema.getInsuredName() + " "
//                                + mLPPolSchema.getRiskCode()
//                                + "的保全备份记录");
//            return false;
//        }
//        mLPPolSchema = tLPPolDB.getSchema();

        return true;
    }

    public static void main(String[] args)
    {
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20061016000004");

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(schema.getEdorAcceptNo());
        tLPPolDB.setEdorType(BQ.EDORTYPE_NS);
        LPPolSet tLPPolSet = tLPPolDB.query();

        for(int i = 1; i <= tLPPolSet.size(); i++)
        {
            VData v = new VData();
            v.add(schema);
            v.add(tLPPolSet.get(i));

            PrtCashValueTableBL p = new PrtCashValueTableBL();
            if(p.getXmlExport(v, "") == null)
            {
                System.out.println(p.mErrors.getErrContent());
            }
            else
            {
                System.out.println("OK");
            }
        }
    }
}
