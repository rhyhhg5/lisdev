package com.sinosoft.lis.bq;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 简易保全逻辑处理类</p>
 * <p>Description: 主要用于新契约投保时修改已生效保单的基本信息 </p>
 * <p>Copyright: Copyright (c) 2006 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class EasyEdorTbBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    private String mChangeType = null;

    private String mEdorAcceptNo = null;

    private String mAppntNo = null;

    private String mInsuredNo = null;

    private String mContNo = null;

    private String mPolNo = null;

    private LCBnfSet mLCBnfSet = null;

    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        this.mChangeType = operate;
        if (!getInputData(data))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到输入参数
     * @param data VData
     * @return boolean
     */
    public boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput)
                data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData)
                data.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            mErrors.addOneError("必须传入TransferData参数！");
            return false;
        }

        mAppntNo = (String) mTransferData.getValueByName("AppntNo");
        mContNo = (String) mTransferData.getValueByName("ContNo");
        mInsuredNo = (String) mTransferData.getValueByName("InsuredNo");
        mLCBnfSet = (LCBnfSet) mTransferData.getValueByName("LCBnfSet");
        if ((mChangeType != null) && (mChangeType.equals(BQ.EASYEDORTYPE_APPNT)))
        {
            mInsuredNo = mAppntNo;
        }
        if ((mInsuredNo == null) || (mInsuredNo.equals("")))
        {
            mInsuredNo = BQ.FILLDATA;
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if ((mChangeType == null) || (mChangeType.equals("")))
        {
            mErrors.addOneError("需要传入操作的类型！");
            return false;
        }
        if (mChangeType.equals(BQ.EASYEDORTYPE_APPNT) ||
                mChangeType.equals(BQ.EASYEDORTYPE_INSURED))
        {
            if ((mAppntNo == null) || (mAppntNo.equals("")))
            {
                mErrors.addOneError("需要传入投保人客户号！");
                return false;
            }
            if ((mContNo == null) || (mContNo.equals("")))
            {
                mErrors.addOneError("需要传入保单号！");
                return false;
            }
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //生成工单受理
        mEdorAcceptNo = createTask();
        if ((mEdorAcceptNo == null) || (mEdorAcceptNo.equals("")))
        {
            return false;
        }

        //添加保全项目
        if (!addEdorItems())
        {
            return false;
        }

        //保存明细
        if (!saveDetail())
        {
            return false;
        }

        //保全理算
        if (!appConfirm())
        {
            return false;
        }
        //creatPrintVts();
        //保全确认
        if (!edorConfirm())
        {
            return false;
        }
        return true;
    }

    /**
     * 创建工单，添加工单受理
     * @return boolean
     */
    private String createTask()
    {
      /*
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mAppntNo);
        tLGWorkSchema.setTypeNo("00"); //00类型表明实际操作不走工单，是新契约产生的数据
        tLGWorkSchema.setDateLimit("");
        tLGWorkSchema.setApplyTypeNo("");
        tLGWorkSchema.setApplyName("");
        tLGWorkSchema.setPriorityNo("");
        tLGWorkSchema.setAcceptWayNo("");
        tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
        tLGWorkSchema.setAcceptCom(mGlobalInput.ManageCom);
        tLGWorkSchema.setAcceptorNo(mGlobalInput.Operator);
        tLGWorkSchema.setRemark("新契约变更客户保单信息生成的工单。");

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);
        TaskInputBL tTaskInputBL = new TaskInputBL();
        if (!tTaskInputBL.submitData(data, ""))
        {
            mErrors.addOneError("工单数据生成失败！");
            return null;
        }
       */
      //这里只需生成一个工单号就行了
        return com.sinosoft.task.CommonBL.createWorkNo();
    }

    /**
     * 添加保全项目
     * @return boolean
     */
    private boolean addEdorItems()
    {
        MMap map = new MMap();
        addEdorMain(map);
        if (mChangeType.equals(BQ.EASYEDORTYPE_APPNT)) //如果是投保人变更
        {
            addEdorItem(map, BQ.EDORTYPE_AD);
            addEdorItem(map, BQ.EDORTYPE_CM);
        }
        else if (mChangeType.equals(BQ.EASYEDORTYPE_INSURED)) //如果是被保人变更
        {
            addEdorItem(map, BQ.EDORTYPE_CM);
        }
        else if (mChangeType.equals(BQ.EASYEDORTYPE_BNF)) //如果是受益人变更
        {
            addEdorItem(map, BQ.EDORTYPE_BC);
        }
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 添加Main表信息
     * @return boolean
     */
    private void addEdorMain(MMap map)
    {
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorMainSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorMainSchema.setContNo(mContNo);
        tLPEdorMainSchema.setEdorAppDate(mCurrentDate);
        tLPEdorMainSchema.setEdorValiDate(mCurrentDate);
        tLPEdorMainSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorMainSchema.setUWState(BQ.UWFLAG_INIT);
        tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorMainSchema.setMakeDate(mCurrentDate);
        tLPEdorMainSchema.setMakeTime(mCurrentTime);
        tLPEdorMainSchema.setModifyDate(mCurrentDate);
        tLPEdorMainSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorMainSchema, "INSERT");
    }

    /**
     * 添加保全项目
     * @param map MMap
     * @param edorType String
     */
    private void addEdorItem(MMap map, String edorType)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorItemSchema.setEdorNo(mEdorAcceptNo);
        tLPEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
        tLPEdorItemSchema.setEdorType(edorType);
        tLPEdorItemSchema.setGrpContNo(BQ.GRPFILLDATA);
        tLPEdorItemSchema.setContNo(mContNo);
        tLPEdorItemSchema.setInsuredNo(mInsuredNo);
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INIT);
        tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorItemSchema.setEdorValiDate(mCurrentDate);
        tLPEdorItemSchema.setEdorAppDate(mCurrentDate);
        tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPEdorItemSchema.setMakeDate(mCurrentDate);
        tLPEdorItemSchema.setMakeTime(mCurrentTime);
        tLPEdorItemSchema.setModifyDate(mCurrentDate);
        tLPEdorItemSchema.setModifyTime(mCurrentTime);
        map.put(tLPEdorItemSchema, "INSERT");
        mLPEdorItemSet.add(tLPEdorItemSchema);
    }

    /**
     * 保存明保全细
     * @return boolean
     */
    private boolean saveDetail()
    {
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            if (edorType.equals(BQ.EDORTYPE_AD))  //地址变更
            {
                if (!saveADDetail(tLPEdorItemSchema))
                {
                    return false;
                }
            }
            else if (edorType.equals(BQ.EDORTYPE_CM)) //客户基本信息变更
            {
                if (!saveCMDetail(tLPEdorItemSchema))
                {
                    return false;
                }
            }
            else if (edorType.equals(BQ.EDORTYPE_BC)) //受益人变更
            {
                if (!saveBCDetail(tLPEdorItemSchema))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 保存AD项目的变更明细
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean saveADDetail(LPEdorItemSchema aLPEdorItemSchema)
    {
        String postalAddress = (String)
                mTransferData.getValueByName("PostalAddress");
        String zipCode = (String)
                mTransferData.getValueByName("ZipCode");
        String HomePhone=(String) mTransferData.getValueByName("HomePhone");
        String Mobile=(String) mTransferData.getValueByName("Mobile");

        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        tLPAppntSchema.setEdorNo(mEdorAcceptNo);
        tLPAppntSchema.setEdorType(BQ.EDORTYPE_AD);
        tLPAppntSchema.setGrpContNo(BQ.GRPFILLDATA);
        tLPAppntSchema.setContNo(mContNo);
        tLPAppntSchema.setAppntNo(mAppntNo);

        LCAddressSchema tLCAddressSchema = CommonBL.getLCAddress(mContNo);
        tLCAddressSchema.setCustomerNo(mAppntNo);
        tLCAddressSchema.setPostalAddress(postalAddress);
        tLCAddressSchema.setZipCode(zipCode);
        tLCAddressSchema.setHomePhone(HomePhone);
        tLCAddressSchema.setMobile(Mobile);

        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(mEdorAcceptNo);
        tLPContSchema.setEdorType(BQ.EDORTYPE_AD);
        tLPContSchema.setContNo(mContNo);
        tLPContSchema.setGrpContNo(BQ.GRPFILLDATA);
        LPContSet tLPContSet = new LPContSet();
        tLPContSet.add(tLPContSchema);

        //做地址变更
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLPAppntSchema);
        data.add(tLCAddressSchema);
        data.add(aLPEdorItemSchema);
        data.add(tLPContSet);
        PEdorADDetailBL tPEdorADDetailBL = new PEdorADDetailBL();
        if (!tPEdorADDetailBL.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPEdorADDetailBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 保存CM项目的变更明细
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean saveCMDetail(LPEdorItemSchema aLPEdorItemSchema)
    {
        //CM项目暂时用InsuredNo来存AppntNo，以后有时间加一个AppntNo字段
        aLPEdorItemSchema.setInsuredNo(mAppntNo);

        String customerNo = (String) mTransferData.getValueByName("CustomerNo");
        String name = (String) mTransferData.getValueByName("Name");
        String idType = (String) mTransferData.getValueByName("IDType");
        String idNo = (String) mTransferData.getValueByName("IDNo");
        String stateFlag = (String) mTransferData.getValueByName("stateFlag");

        LDPersonSchema tLDPersonSchema = CommonBL.getLDPerson(customerNo);
        tLDPersonSchema.setName(name);
        tLDPersonSchema.setIDType(idType);
        tLDPersonSchema.setIDNo(idNo);

        LCInsuredSchema tLCInsuredSchema = CommonBL.getLCInsured(mContNo, mInsuredNo);
        if (tLCInsuredSchema == null)
        {
            tLCInsuredSchema = new LCInsuredSchema();
        }
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("stateFlag", stateFlag);

        VData data = new VData();
        data.add("I"); //I表示是个单
        data.add("00"); //wdk于2013-06-14 修改 区别于保全万能标志Y
        data.add(tTransferData); //保单状态 0为未承保保单
        data.add(mGlobalInput);
        data.add(aLPEdorItemSchema);
        data.add(tLDPersonSchema);
        data.add(tLCInsuredSchema);
        PEdorCMDetailBL tPEdorCMDetailBL = new PEdorCMDetailBL();
        if (!tPEdorCMDetailBL.submitData(data))
        {
            mErrors.copyAllErrors(tPEdorCMDetailBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 保存BC项目的变更明细
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean saveBCDetail(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPBnfSet tLPBnfSet = new LPBnfSet();
        for (int i = 1; i <= mLCBnfSet.size(); i++)
        {
            LPBnfSchema tLPBnfSchema = new LPBnfSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPBnfSchema, mLCBnfSet.get(i));
            tLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            tLPBnfSet.add(tLPBnfSchema);
        }
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(aLPEdorItemSchema);
        data.add(tLPBnfSet);
        PEdorBCDetailBL tPEdorBCDetailBL = new PEdorBCDetailBL();
        if (!tPEdorBCDetailBL.submitData(data, "INSERT||MAIN"))
        {
            mErrors.copyAllErrors(tPEdorBCDetailBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 保全理算，更新核保标志等同于保全理算
     * @return boolean
     */
    private boolean appConfirm()
    {
        MMap map = new MMap();
        String sql;
        sql = "update LPEdorApp " +
                "set EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "    UWState = '" + BQ.UWFLAG_PASS + "' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorMain " +
                "set EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "    UWState = '" + BQ.UWFLAG_PASS + "' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LPEdorItem " +
                "set EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "    UWFlag = '" + BQ.UWFLAG_PASS + "' " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        map.put(sql, "UPDATE");
        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 保全确认
     * @param edorAcceptNo String
     * @return boolean
     */
    private boolean edorConfirm()
    {
        PEdorConfirmBL tPEdorConfirmBL =
                new PEdorConfirmBL(mGlobalInput, mEdorAcceptNo);
        if (!tPEdorConfirmBL.submitData())
        {
            mErrors.copyAllErrors(tPEdorConfirmBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
