package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;

/**
 * <p>Title: 保全明细录入</p>
 * <p>Description: 交费频次变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorPCDetailUI
{
    private PEdorPCDetailBL mPEdorPCDetailBL = null;

    public PEdorPCDetailUI(GlobalInput gi, String edorNo, String contNo)
    {
        mPEdorPCDetailBL = new PEdorPCDetailBL(gi, edorNo, contNo);
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
	{
        if (!mPEdorPCDetailBL.submitData(data))
        {
            return false;
        }
		return true;
	}

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
	{
		return mPEdorPCDetailBL.mErrors.getFirstError();
	}

	/**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        LCPolSet tLCPolSet = new LCPolSet();

        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("21000019852");
        tLCPolSchema.setPayIntv(3);
        tLCPolSet.add(tLCPolSchema);

        LCPolSchema tLCPolSchema2 = new LCPolSchema();
        tLCPolSchema2.setPolNo("21000019874");
        tLCPolSchema2.setPayIntv(3);
        tLCPolSet.add(tLCPolSchema2);

        LCPolSchema tLCPolSchema3 = new LCPolSchema();
        tLCPolSchema3.setPolNo("21000015357");
        tLCPolSchema3.setPayIntv(1);
        tLCPolSet.add(tLCPolSchema3);


        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";
        g.ManageCom = g.ComCode;

        VData data = new VData();
        data.add(tLCPolSet);
        PEdorPCDetailUI tPEdorPCDetailUI = new PEdorPCDetailUI(g,
            "20070215000002",
            "00002581101");
        if(!tPEdorPCDetailUI.submitData(data))
        {
            System.out.println(tPEdorPCDetailUI.getError());
        }

    }
}
