package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;



public class PrtGrpInsuredListGXUI
{
    PrtGrpInsuredListGXBL mPrtGrpInsuredListGXBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListGXUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListGXBL = new PrtGrpInsuredListGXBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListGXBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListGXBL.getInputStream();
    }
}
