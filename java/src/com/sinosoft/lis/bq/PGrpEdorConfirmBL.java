package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全确认逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PGrpEdorConfirmBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    private VData pInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    private LJAPaySchema mLJAPaySchema = null;

    /**  */
    LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    LPGrpEdorMainSet mLPGrpEdorMainSet = new LPGrpEdorMainSet();
    LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
    LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    String mStrTemplatePath = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private GlobalInput mGlobalInput2 = new GlobalInput();

    private MMap map = new MMap();
    public PGrpEdorConfirmBL()
    {}

    public MMap getSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return null;
        }

        //数据准备操作，检查团体保全主表数据，看是否通过了保全核保
        if (!prepareData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        //更新保单人数
        if(!reNewPeoples())
        {
            return null;
        }

        prepareOutputData();
        return map;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitData(cInputData, cOperate) == null)
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PGrpEdorConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 处理业务
     * @return boolean
     */
    public boolean dealData()
    {
        if (!validateFinanceData())
        {
            return false;
        }

        //数据操作业务处理
        if (mOperate.equals("INSERT||GRPEDORCONFIRM"))
        {
            pInputData = new VData();

            EdorConfirmBL aEdorConfirmBL = new EdorConfirmBL();
            //        mLPGrpEdorMainSchema.setSchema(mLPGrpEdorMainSet.get(i));

            GEdorConfirmBL aGEdorConfirmBL = new GEdorConfirmBL();
            LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
            tLPEdorMainDB.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
            mLPEdorMainSet = tLPEdorMainDB.query();
            if (tLPEdorMainDB.mErrors.needDealError())
            {
                CError.buildErr(this, "查询个人保全失败！");
            }
            for (int i = 1; i <= mLPEdorMainSet.size(); i++)
            {
                pInputData.clear();
                pInputData.addElement(mGlobalInput);
                pInputData.addElement("G");
                pInputData.addElement(mLPGrpEdorMainSchema);
                pInputData.addElement(mStrTemplatePath);

                System.out.println("处理个人保全主表");
                pInputData.addElement(mLPEdorMainSet.get(i));
                
             // -----------个人保全主表起始时间-----------------
				ExeSQL tExeSQL = new ExeSQL();
				String time = PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime();
				String insertSQL = "INSERT INTO ldtimetest VALUES ('CTTIME',current time,'"
						+ time
						+ "','个单:"
						+ mLPEdorMainSet.get(i).getContNo()
						+ "开始处理',NULL,NULL) ";
				tExeSQL.execUpdateSQL(insertSQL);
				
                if (!aEdorConfirmBL.submitData(pInputData, mOperate))
                {
                    this.mErrors.copyAllErrors(aEdorConfirmBL.mErrors);
                    return false;
                }
                else
                {
                    VData rVData = aEdorConfirmBL.getResult();
                    MMap tMap = new MMap();
                    tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                    if (tMap == null)
                    {
                        CError.buildErr(this,
                                "得到个人保单为:" +
                                mLPEdorMainSet.get(i).getContNo() +
                                "的保全确认结果时失败！");
                        return false;

                    }
                    else
                    {
                        map.add(tMap);
                    }
                }
                
                //-----------个人保全主表结束时间-----------------
				ExeSQL tExeSQL1 = new ExeSQL();
				String time1 = PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime();
				String insertSQL1 = "INSERT INTO ldtimetest VALUES ('CTTIME',current time,'"
						+ time1
						+ "','个单:"
						+ mLPEdorMainSet.get(i).getContNo()
						+ "结束处理',NULL,NULL) ";
				tExeSQL1.execUpdateSQL(insertSQL1);
				
            }

            pInputData.clear();
            pInputData.addElement(mGlobalInput);
            pInputData.addElement("G");
            pInputData.addElement(mStrTemplatePath);
            pInputData.addElement(mLPGrpEdorMainSchema);
            pInputData.addElement(mLJAPaySchema);

            System.out.println("处理团体保全主表，包括更新保全状态");
            

			// -----------团体保全主表起始时间-----------------
			ExeSQL tExeSQL2 = new ExeSQL();
			String time2 = PubFun.getCurrentDate() + " "
					+ PubFun.getCurrentTime();
			String insertSQL2 = "INSERT INTO ldtimetest VALUES ('CTTIME',current time,'"
					+ time2
					+ "','团单:"
					+ mLPGrpEdorMainSchema.getEdorAcceptNo()
					+ "开始处理',NULL,NULL) ";
			tExeSQL2.execUpdateSQL(insertSQL2);
			
            if (!aGEdorConfirmBL.submitData(pInputData, mOperate))
            {
                this.mErrors.copyAllErrors(aGEdorConfirmBL.mErrors);
                return false;
            }
            else
            {
                VData rVData = aGEdorConfirmBL.getResult();
                MMap tMap = new MMap();
                tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                if (tMap == null)
                {
                    CError.buildErr(this,
                            "得到团体保单的保全确认结果时失败！");
                    return false;

                }
                else
                {
                    map.add(tMap);
                }
            }
        
    		//-----------团体保全主表结束时间-----------------
 			ExeSQL tExeSQL3 = new ExeSQL();
 			String time3 = PubFun.getCurrentDate() + " "
 					+ PubFun.getCurrentTime();
 			String insertSQL3 = "INSERT INTO ldtimetest VALUES ('CTTIME',current time,'"
 					+ time3
 					+ "','团单:"
 					+ mLPGrpEdorMainSchema.getEdorAcceptNo()
 					+ "结束处理',NULL,NULL) ";
 			tExeSQL3.execUpdateSQL(insertSQL3);
         			
        }
        else
        {
            CError.buildErr(this,
                    "保全申请单更换号码失败！");
            return false;

        }

        //团险批改项目状态置为确认完毕
        String sql = " update LPGrpEdorItem "
                + "set edorState='0' "
                + "where EdorAcceptNo='"
                + mLPGrpEdorMainSchema.getEdorAcceptNo() + "' "
                + "    and GrpContNo='"
                + mLPGrpEdorMainSchema.getGrpContNo() + "' "
                + "    and (edorType='CT' or EdorType='WT') ";

        map.put(sql, "UPDATE");
        
        //根据数据校验添加修改SQL,将lpedoritem状态设置正确
        String sql1 = " update LPEdorItem "
            + "set edorState='0',operator='"
            + mGlobalInput.Operator+"',modifydate=current date,modifytime=current time "
            + "where EdorAcceptNo='"
            + mLPGrpEdorMainSchema.getEdorAcceptNo() + "' ";

    map.put(sql1, "UPDATE");

        return true;
    }

    /**
     * 使财务数据生效
     * @return boolean
     */
    private boolean validateFinanceData()
    {
        ValidateFinanceData vfd = new ValidateFinanceData(
                mGlobalInput, mLPGrpEdorMainSchema.getEdorAcceptNo(),
                BQ.NOTICETYPE_G);
        if (!vfd.submitData())
        {
            mErrors.copyAllErrors(vfd.mErrors);
            return false;
        }
        this.mLJAPaySchema = vfd.getLJAPay();
        System.out.println("PayNo:" + mLJAPaySchema.getPayNo());
        map.add(vfd.getMap());
        return true;
    }


    /**
     * 减去的个人账户余额总和
     * @return double
     */
    private double getSumInsuredAccBala()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select sum(b.insuAccBala) ")
            .append("from LCPol a, LCInsureAcc b ")
            .append("where a.polNo = b.polNo ")
            .append("   and a.grpContNo = b.grpContNo ")
            .append("and a.polTypeFlag != '")
            .append(BQ.POLTYPEFLAG_PUBLIC)
            .append("'  and b.InsuAccNo = '")
            .append(CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED))
            .append("'  and a.contNo in (")
            .append("      select contNo ")
            .append("      from LPEdorItem ")
            .append("      where edorNo = '")
            .append(mLPGrpEdorMainSchema.getEdorNo())
            .append("'         and edorType = '")
            .append(BQ.EDORTYPE_ZT)
            .append("') ");
        System.out.println(sql.toString());
        ExeSQL tExeSQL = new ExeSQL();
        String sumInsuredAccBala = tExeSQL.getOneValue(sql.toString());

        return Double.parseDouble(sumInsuredAccBala.equals("")
                                  ? "0" : sumInsuredAccBala);
    }

    /**
     * 更新保单人数
     * @return boolean
     */
    private boolean reNewPeoples()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
        LPGrpEdorItemSet set = tLPGrpEdorItemDB.query();
        if(set.size() > 0)
        {
            UpdatePeoplesBL tUpdatePeoplesBL =
                new UpdatePeoplesBL(set.get(1).getGrpContNo());
            MMap tMMap = tUpdatePeoplesBL.getSubmitData();
            if(tUpdatePeoplesBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tUpdatePeoplesBL.mErrors);
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            mStrTemplatePath = (String) mInputData.get(0);
            mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
            mLPGrpEdorMainSchema = (LPGrpEdorMainSchema) mInputData.getObjectByObjectName("LPGrpEdorMainSchema", 0);
            mGlobalInput2.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
            System.out.println(mGlobalInput2.ManageCom);
            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
            tLPEdorAppDB.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorNo());
            if (!tLPEdorAppDB.getInfo())
            {
                mErrors.addOneError("未找到保全受理号！");
                return false;
            }
            String edorState = tLPEdorAppDB.getEdorState();
            if ((edorState != null) && (edorState.equals(BQ.EDORSTATE_CONFIRM)))
            {
                mErrors.addOneError("保全确认已经成功，不能重复确认！");
                return false;
            }
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        mInputData.clear();
        String sql = "select * from LPGrpEdorMain where EdorState='2' and UWState ='9' "
                     + " and EdorNo='" + mLPGrpEdorMainSchema.getEdorNo() + "' ";
        System.out.println("----grpsql" + sql);
        mLPGrpEdorMainSet.clear();
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        mLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sql);
        if (tLPGrpEdorMainDB.mErrors.needDealError() || mLPGrpEdorMainSet.size() == 0)
        {
        }
        else
        {
            mLPGrpEdorMainSchema.setSchema(mLPGrpEdorMainSet.get(1));
        }
        mInputData.addElement(mGlobalInput);
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "endor";


        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
        tLPGrpEdorMainSchema.setEdorNo("20060710000004");
        tLPGrpEdorMainSchema.setGrpContNo("0000034701");
        tLPGrpEdorMainSchema.setEdorAcceptNo(tLPGrpEdorMainSchema.getEdorNo());

        String strTemplatePath = "xerox/printdata/";

        VData tInputData = new VData();
        tInputData.addElement(strTemplatePath);
        tInputData.addElement(tLPGrpEdorMainSchema);
        tInputData.addElement(tGlobalInput);

        PGrpEdorConfirmBL aPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
        if (!aPGrpEdorConfirmBL.submitData(tInputData, "INSERT||GRPEDORCONFIRM"))
        {
            System.out.println(aPGrpEdorConfirmBL.mErrors.getErrContent());
        }
    }

}
