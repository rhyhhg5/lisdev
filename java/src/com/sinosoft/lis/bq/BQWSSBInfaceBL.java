package com.sinosoft.lis.bq;

import java.util.List;

import org.jdom.Element;

import com.cbsws.obj.EdorItemInfo;
import com.cbsws.obj.EndorsementInfo;
import com.cbsws.xml.ctrl.complexpds.LCGrpContTable;
import com.cbsws.xml.ctrl.complexpds.LCInsuredTable;
import com.cbsws.obj.LGWorkTable;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LPDiskImportDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class BQWSSBInfaceBL {

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	private String mEdorAcceptNo;

	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
	public CErrors mErrors = new CErrors();

	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	public LCGrpContTable mLCGrpContTable;
	public LCInsuredTable mLCInsuredTable;
	private LCGrpContSchema mLCGrpContSchema;
	private LCInsuredSchema mLCInsuredSchema;
	private LPDiskImportSchema mLPDiskImportSchema;
	private EndorsementInfo mEndorsementInfo;
	private List tLCInsuredList;

	public BQWSSBInfaceBL() {
	}

	public BQWSSBInfaceBL(GlobalInput tGlobalInput,
			LCGrpContSchema tLCGrpContSchema, LCInsuredSchema tLCInsuredSchema) {
		this.mGlobalInput = tGlobalInput;
		this.mLCGrpContSchema = tLCGrpContSchema;
		this.mLCInsuredSchema = tLCInsuredSchema;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLCInsuredTable = (LCInsuredTable) cInputData.getObjectByObjectName(
				"LCInsuredTable", 0);
		mLCInsuredSchema = (LCInsuredSchema) cInputData.getObjectByObjectName(
				"LCInsuredSchema", 0);
		mLCGrpContTable = (LCGrpContTable) cInputData.getObjectByObjectName(
				"LCGrpContTable", 0);
		mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
				"LCGrpContSchema", 0);
		tLCInsuredList = (List) cInputData
				.getObjectByObjectName("ArrayList", 0);

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
		if (tLCGrpContSet.size() == 0) {
			mErrors.addOneError("查询团单信息失败！");
			return false;
		}
		mLCGrpContSchema = tLCGrpContSet.get(1);
		if (mGlobalInput == null) {
			mErrors.addOneError("请传入参数信息错误");
			return false;
		}
		mGlobalInput.ComCode = mLCGrpContSchema.getManageCom();
		mGlobalInput.ManageCom = mLCGrpContSchema.getManageCom();
		return true;
	}

	public boolean submit(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}

		return true;
	}

	private boolean deal() {

		try {
			// 申请工单
			if (!createWorkNo()) {
				return false;
			}
			// 添加保全
			if (!addEdorItem()) {
				// 撤销工单
				if (!cancelEdorItem("1", "G&EDORAPP")) {
					return false;
				}
				return false;
			}

			// 录入明细
			if (!saveDetail()) {
				// 撤销工单
				if (!cancelEdorItem("1", "G&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "G&EDORAPP")) {
						return false;
					}
				}
				return false;
			}

			// 理算确认
			if (!appConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "G&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "G&EDORAPP")) {
						return false;
					}
				}
				return false;
			}

			// 保全确认
			if (!edorConfirm()) {
				// 撤销工单
				if (!cancelEdorItem("1", "G&EDORMAIN")) {
					return false;
				} else {
					if (!cancelEdorItem("1", "G&EDORAPP")) {
						return false;
					}
				}
				return false;
			}
		} catch (Exception e) {
			System.out.println("无名单实名化接口程序处理错误：撤销工单");
			mErrors.addOneError("保全处理异常，请求失败。");
			cancelEdorItem("1", "G&EDORMAIN");
			cancelEdorItem("1", "G&EDORAPP");
			return false;
		}

		return true;
	}

	public LPGrpEdorItemSchema getEdorItem() {
		return mLPGrpEdorItemSchema;
	}

	// 撤销工单
	private boolean cancelEdorItem(String edorstate, String transact) {
		PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();

		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);

		if ("G&EDORAPP".equals(transact)) {
			tLPEdorAppSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPEdorAppSchema.setEdorState(edorstate);

			String delReason = "";
			String reasonCode = "002";

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPEdorAppSchema);
			// 准备传输数据 VData
			tVData.addElement(tTransferData);
		} else if ("G&EDORMAIN".equals(transact)) {
			tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
			tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
			tLPGrpEdorMainSchema.setEdorState(edorstate);
			tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			String delReason = "";
			String reasonCode = "002";
			System.out.println(delReason);

			tTransferData.setNameAndValue("DelReason", delReason);
			tTransferData.setNameAndValue("ReasonCode", reasonCode);
			tVData.addElement(tLPGrpEdorMainSchema);
			tVData.addElement(tTransferData);
		}
		try {
			// 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			System.out.println("A new Moon is rising");
			tPGrpEdorCancelUI.submitData(tVData, transact);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mErrors.addOneError("撤销工单失败" + e);
			return false;
		}
		return true;
	}

	// 申请团体工单
	private boolean createWorkNo() {

		// 得到工单信息
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
		tLGWorkSchema.setTypeNo("03");
		tLGWorkSchema.setApplyTypeNo("0");
		tLGWorkSchema.setAcceptWayNo("0");
		tLGWorkSchema.setAcceptDate(mCurrDate);
		tLGWorkSchema.setRemark("无名单实名化接口生成");

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskInputBL tTaskInputBL = new TaskInputBL();
		if (!tTaskInputBL.submitData(data, "")) {
			mErrors.addOneError("生成保全工单失败" + tTaskInputBL.mErrors);
			return false;
		}
		mEdorAcceptNo = tTaskInputBL.getWorkNo();

		return true;
	}

	private boolean addEdorItem() {
		// // 校验能否添加保全项目
		// if (!checkEdorItem()) {
		// return false;
		// }
		mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
		mLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		mLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);

		mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		mLPGrpEdorItemSchema.setEdorAcceptNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorAppNo(mEdorAcceptNo);
		mLPGrpEdorItemSchema.setEdorType(BQ.EDORTYPE_WS);
		mLPGrpEdorItemSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		mLPGrpEdorItemSchema.setEdorAppDate(mCurrDate);
		mLPGrpEdorItemSchema.setEdorValiDate(mCurrDate);
		mLPGrpEdorItemSchema.setManageCom(mLCGrpContSchema.getManageCom());
		mLPGrpEdorItemSet.add(mLPGrpEdorItemSchema);

		VData tVData = new VData();
		tVData.add(mLPGrpEdorItemSet);
		tVData.add(mLPGrpEdorMainSchema);
		tVData.add(mGlobalInput);
		GrpEdorItemUI tGrpEdorItemUI = new GrpEdorItemUI();
		if (!tGrpEdorItemUI.submitData(tVData, "INSERT||GRPEDORITEM")) {
			mErrors.addOneError("添加保全项目失败："
					+ tGrpEdorItemUI.mErrors.getFirstError());
			return false;
		}

		return true;

	}

	// 无名单项目明细
	private boolean saveDetail() {
		// 1、存储被保人数据save to lpdiskimport
		MMap map = new MMap();
		for (int i = 0; i < tLCInsuredList.size(); i++) {
			LCInsuredTable tLCInsuredTable = (LCInsuredTable) tLCInsuredList
					.get(i);
			mLPDiskImportSchema = new LPDiskImportSchema();
			mLPDiskImportSchema.setSerialNo("" + (i + 1));
			mLPDiskImportSchema.setEdorNo(mEdorAcceptNo);
			mLPDiskImportSchema.setEdorType(BQ.EDORTYPE_WS);
			mLPDiskImportSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			mLPDiskImportSchema.setState("1");
			mLPDiskImportSchema.setInsuredName(tLCInsuredTable.getName());
			mLPDiskImportSchema.setSex(tLCInsuredTable.getSex());
			mLPDiskImportSchema.setBirthday(tLCInsuredTable.getBirthday());
			mLPDiskImportSchema.setIDType(tLCInsuredTable.getIDType());
			mLPDiskImportSchema.setIDNo(tLCInsuredTable.getIDNo());
			mLPDiskImportSchema.setContPlanCode(tLCInsuredTable
					.getContPlanCode());
			mLPDiskImportSchema.setOccupationType(tLCInsuredTable
					.getOccupationType());
			mLPDiskImportSchema.setOperator(mGlobalInput.Operator);
			mLPDiskImportSchema.setMakeDate(mCurrDate);
			mLPDiskImportSchema.setMakeTime(mCurrTime);
			mLPDiskImportSchema.setModifyDate(mCurrDate);
			mLPDiskImportSchema.setModifyTime(mCurrTime);
			map.put(mLPDiskImportSchema, "INSERT");
		}
		// 将被保人清单提交数据库
		if (!submit(map)) {
			return false;
		}

		// 2、明细保存
		GrpEdorWSDetailUI tGrpEdorWSDetailUI = new GrpEdorWSDetailUI(
				mGlobalInput, mEdorAcceptNo, mLCGrpContSchema.getGrpContNo());
		if (!tGrpEdorWSDetailUI.submitData()) {
			mErrors.addOneError("添加保全项目失败：" + tGrpEdorWSDetailUI.getError());
			return false;
		}
		
		// 3、出现被保人重复，无名额等状况时，阻断后续，报错
		String mGrpContNo = mLCGrpContSchema.getGrpContNo();
		String errorSQL = " select ErrorReason from LPDiskImport"
				+ " where grpcontno='" + mGrpContNo + "' and edorno ='"
				+ mEdorAcceptNo + "' " + " and edortype = 'WS'"
				+ " and state = '0'" 
				+ " with ur  ";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tErrorInfoSSRS = null;
		int ErrorInfolen = 0;
		tErrorInfoSSRS = tExeSQL.execSQL(errorSQL);
		ErrorInfolen = tErrorInfoSSRS.getMaxRow();
		if (ErrorInfolen != 0) {
			String ErrorReason = "";
			for (int j = 1; j <= ErrorInfolen; j++) {
				ErrorReason = ErrorReason + tErrorInfoSSRS.GetText(j, 1);
			}
			mErrors.addOneError("无名单实名化失败：" +  ErrorReason );
			return false;
		}
		return true;
	}

	private boolean appConfirm() {
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tLPGrpEdorMainSchema.setEdorValiDate(mCurrDate);
		tLPGrpEdorMainSchema.setEdorAppDate(mCurrDate);

		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLPGrpEdorMainSchema);
		PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
		if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM")) {
			System.out
					.println(tPGrpEdorAppConfirmUI.getError().getErrContent());
			mErrors.addOneError("保全理算失败："
					+ tPGrpEdorAppConfirmUI.getError().getErrContent());
			return false;
		}
		return true;
	}

	private boolean checkEdorItem() {
		String mGrpContNo = mLCGrpContSchema.getGrpContNo();
		// 正在操作保全的无法添加犹豫期退保
		String edorSQL = " select edoracceptno from lpgrpedoritem where grpcontno='"
				+ mGrpContNo
				+ "' and edoracceptno!='"
				+ mEdorAcceptNo
				+ "' "
				+ " and exists (select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0') with ur  ";

		ExeSQL tExeSQL = new ExeSQL();
		String edorFlag = tExeSQL.getOneValue(edorSQL);
		if (edorFlag != null && !"".equals(edorFlag)) {
			mErrors.addOneError("团单" + mGrpContNo + "正在操作保全，工单号为：" + edorFlag
					+ "无法做无名单实名化");
			return false;
		}
		// 只有承保有效状态下的团单才能无名单实名化
		if ((!"1".equals(mLCGrpContSchema.getAppFlag()))
				|| "2".equals(mLCGrpContSchema.getStateFlag())
				|| "3".equals(mLCGrpContSchema.getStateFlag())) {
			mErrors.addOneError("团单只有在签单并承保有效的状态下才能无名单实名化");
			return false;
		}

		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.addOneError("提交数据库发生错误" + tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 保全确认
	 * 
	 * @param edorAcceptNo
	 *            String
	 * @return boolean
	 */
	private boolean edorConfirm() {
		MMap map = new MMap();

		// 生成财务数据
		FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput,
				mEdorAcceptNo, BQ.NOTICETYPE_G, "");
		if (!tFinanceDataBL.submitData()) {
			mErrors.addOneError("生成财务数据错误" + tFinanceDataBL.mErrors);
			return false;
		}

		String strTemplatePath = "";
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		tLPGrpEdorMainSchema.setEdorAcceptNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setEdorNo(mEdorAcceptNo);
		tLPGrpEdorMainSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		VData data = new VData();
		data.add(strTemplatePath);
		data.add(tLPGrpEdorMainSchema);
		data.add(mGlobalInput);
		PGrpEdorConfirmBL tPGrpEdorConfirmBL = new PGrpEdorConfirmBL();
		map = tPGrpEdorConfirmBL.getSubmitData(data, "INSERT||GRPEDORCONFIRM");
		if (map == null) {
			mErrors.addOneError("保全结案发生错误" + tFinanceDataBL.mErrors);
			return false;
		}

		// 工单结案
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setDetailWorkNo(mEdorAcceptNo);
		tLGWorkSchema.setTypeNo("03"); // 结案状态

		data = new VData();
		data.add(mGlobalInput);
		data.add(tLGWorkSchema);
		TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
		MMap tmap = tTaskAutoFinishBL.getSubmitData(data, "");
		if (tmap == null) {
			mErrors.addOneError("工单结案失败" + tFinanceDataBL.mErrors);
			return false;
		}
		map.add(tmap);
		if (!submit(map)) {
			return false;
		}
		return true;
	}

}
