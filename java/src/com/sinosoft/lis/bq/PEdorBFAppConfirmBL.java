package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保障调整理算类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorBFAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mInsuredNo = null;

    private double mGetMoney = 0.00;

    private double mChgPrem = 0.00;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private ReCalBL mReCalBL = null;

    private DetailDataQuery mQuery = null;

    private EdorItemSpecialData mSpecialData = null;

    /**
     * cal
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
            mInsuredNo = mLPEdorItemSchema.getInsuredNo();
            mSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        //当一个保单下有两个相同的险种时，第一次明细录入选择了两个险种，做到理算完成后又重新明细录入，选择一个险种，此时之前的数据不会删除导致错误，现进行修改。
        //cbs00050522  关于保障保费调整后续期缴费金额显示不正确
    	//2011-11-11 modify by whl
        mMap.put("delete from lpprem a where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"' and not exists (select 1 from lppol where edorno=a.edorno and edortype=a.edortype and polno=a.polno )", "DELETE");
        mMap.put("delete from lpduty a where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"' and not exists (select 1 from lppol where edorno=a.edorno and edortype=a.edortype and polno=a.polno )", "DELETE");
        mMap.put("delete from lpget  a where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"' and not exists (select 1 from lppol where edorno=a.edorno and edortype=a.edortype and polno=a.polno )", "DELETE");
        
        if (!dealPols())
        {
            return false;
        }
        setEdorItem();
        setLPCont();
        return true;
    }

    /**
     * 按险种处理保全数据
     * @return boolean
     */
    private boolean dealPols()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setContNo(mContNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            if (!dealOnePol(tLPPolSet.get(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema ==  null)
            {
                mErrors.addOneError("未找到序号" + tLCPolSchema.getRiskSeqNo()
                        + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false; 
            }
            double oldPrem = tLCPolSchema.getPrem();
            double newPrem = calNewPrem(aLPPolSchema);
            mSpecialData.setPolNo(tLCPolSchema.getPolNo());
            mSpecialData.query();
            String validateTime = mSpecialData.getEdorValue("ValidateTime");
            if (validateTime == null)
            {
                mErrors.addOneError("未录入生效时间！");
                return false;
            }
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(aLPPolSchema.getRiskCode());
            if ((!tLMRiskAppDB.getInfo()) || (tLMRiskAppDB.getRiskPeriod() == null))
            {
                mErrors.addOneError("未找到险种" + aLPPolSchema.getRiskCode() +
                        "在LMRiskApp表的定义！");
                return false;
            }
            double getMoney = 0;
            //长期险，减额或降档，即时生效
            if(tLMRiskAppDB.getRiskPeriod().equals("L") && validateTime.equals("1") && newPrem<oldPrem)
            {
            	String tEdorValidate = mLPEdorItemSchema.getEdorValiDate();
                getMoney = calLRiskGetMoney(oldPrem, newPrem, tLCPolSchema, tEdorValidate);
                if (!setLJSGetEndorse(aLPPolSchema, getMoney))
                {
                    return false;
                }
            }
             //即时生效或者是长期险
            else if (validateTime.equals("1") || tLMRiskAppDB.getRiskPeriod().equals("L"))
            {
                String tEdorValidate = mLPEdorItemSchema.getEdorValiDate();
                if (validateTime.equals("2"))
                {
                    tEdorValidate = aLPPolSchema.getPaytoDate();
                }
                getMoney = calGetMoney(oldPrem, newPrem, tLCPolSchema, aLPPolSchema, tEdorValidate);
                if (!setLJSGetEndorse(aLPPolSchema, getMoney))
                {
                    return false;
                }
            }
            setLPTables(newPrem - oldPrem);
            mGetMoney += getMoney;
            mChgPrem += newPrem - oldPrem;
            String tUpdateLCprem = " update lpprem a set (urgepayflag,paytimes,rate,needacc) = (select urgepayflag,paytimes,rate,needacc from lcprem where polno = a.polno and dutycode = a.dutycode and payplancode = a.payplancode) "
                                   + " WHERE a.EdorType = 'BF' and a.EdorNo = '"+aLPPolSchema.getEdorNo()+"' AND a.polno = '"+aLPPolSchema.getPolNo()+"'"
                                   ;
            String tUpdateLCduty = " update lpget a set (summoney,baladate,state) = (select summoney,baladate,state from lcget where polno = a.polno and dutycode = a.dutycode and getdutycode = a.getdutycode) "
                                   + " WHERE a.EdorType = 'BF' and a.EdorNo = '"+aLPPolSchema.getEdorNo()+"' AND a.polno = '"+aLPPolSchema.getPolNo()+"'"
                                   ;
            mMap.put(tUpdateLCprem,"UPDATE");
            mMap.put(tUpdateLCduty,"UPDATE");
        }
        catch (Exception ex)
        {
            mErrors.addOneError("保费计算出错！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 得到保全算费的计算代码
     * @param riskCode String
     * @return String
     */
    private LMPolDutyEdorCalSchema getEdorCal(String riskCode)
    {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(riskCode);
        tLMPolDutyEdorCalDB.setEdorType(mEdorType);
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if (tLMPolDutyEdorCalSet.size() == 0)
        {
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1);
    }

    /**
     * 重算保费
     * @param aLPPolSchema LPPolSchema
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private double calNewPrem(LPPolSchema aLPPolSchema) throws Exception
    {
        //重算保费
        mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
        if (!mReCalBL.recal())
        {
            mErrors.copyAllErrors(mReCalBL.mErrors);
            throw new Exception();
        }
        return mReCalBL.aftLPPolSet.get(1).getPrem();
    }

    /**
     * 得到交费起始日期
     * @param aLPPolSchema LPPolSchema
     * @return String
     */
    private String getLastPayToDate(LPPolSchema aLPPolSchema)
    {
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPolNo(aLPPolSchema.getPolNo());
        tLJAPayPersonDB.setCurPayToDate(aLPPolSchema.getPaytoDate());
        tLJAPayPersonDB.setPayType("ZC");
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if (tLJAPayPersonSet.size() == 0)
        {
            return null;
        }
        return tLJAPayPersonSet.get(1).getLastPayToDate();
    }
    
    /**
     * 得到保单回执客户签收日期 add by xp 08-09-23
     * @param String tContNo
     * @return String
     */
    private String getCustomgetpoldate(String tContNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() == 0)
        {
            return null;
        }
        return tLCContSet.get(1).getCustomGetPolDate();
    }
    
    /**
     * 得到保单签单日期 add  08-09-23
     * @param String tContNo
     * @return String
     */
    private String getSignDate(String tContNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() == 0)
        {
            return null;
        }
        return tLCContSet.get(1).getSignDate();
    }

    /**
     * 计算做过保全之后的保费差额
     * @param oldPrem double
     * @param newPrem double
     * @param aLPPolSchema LPPolSchema
     * @return double
     */
    private double calGetMoney(double oldPrem, double newPrem,
            LCPolSchema tLCPolSchema, LPPolSchema aLPPolSchema, String edorValidate) throws Exception
    {
        double getMoney = 0.0;
        String upPolYears =  getUpPolYears(aLPPolSchema.getContNo(), edorValidate);
        if (oldPrem != newPrem)
        {
            //得到交费起始日期
            String lastPayToDate = getLastPayToDate(aLPPolSchema);
            //得到保单回执客户签收日期 add  08-09-23
            String tCustomgetpoldate = getCustomgetpoldate(aLPPolSchema.getContNo());
            //得到保单签单日期 add  08-09-23
            String tSignDate = getSignDate(aLPPolSchema.getContNo());
            if (lastPayToDate == null)
            {
                mErrors.addOneError("未找到本期交费起始日期！");
                throw new Exception();
            }
            //按剩余日期计算保费差额
            BqCalBase tBqCalBase = new BqCalBase();
            tBqCalBase.setPrem(oldPrem);
            tBqCalBase.setNewPrem(newPrem);
            tBqCalBase.setEdorValiDate(edorValidate);
            tBqCalBase.setLastPayToDate(lastPayToDate);
            tBqCalBase.setPayToDate(aLPPolSchema.getPaytoDate());

            tBqCalBase.setPolNo(aLPPolSchema.getPolNo());
            tBqCalBase.setCValiDate(aLPPolSchema.getCValiDate());
            tBqCalBase.setUpPolYears(upPolYears);
            tBqCalBase.setGet(aLPPolSchema.getAmnt() - tLCPolSchema.getAmnt());
            tBqCalBase.setPayEndYear(aLPPolSchema.getPayEndYear());
            tBqCalBase.setSex(aLPPolSchema.getInsuredSex());
            tBqCalBase.setAppAge(PubFun.calInterval(
                    aLPPolSchema.getInsuredBirthday(),
                    aLPPolSchema.getPolApplyDate(), "Y"));
            tBqCalBase.setPayIntv(aLPPolSchema.getPayIntv());
            tBqCalBase.setInsuYear(aLPPolSchema.getInsuYear());
            tBqCalBase.setCustomgetpoldate(tCustomgetpoldate);
            tBqCalBase.setSignDate(tSignDate);
            LMPolDutyEdorCalSchema edorCalSchema =
                    getEdorCal(aLPPolSchema.getRiskCode());
            BqCalBL tBqCalBL = new BqCalBL();
            getMoney += tBqCalBL.calChgMoney(edorCalSchema.getChgPremCalCode(),
                    tBqCalBase);
            if (tBqCalBL.mErrors.needDealError())
            {
                mErrors.addOneError("计算补退费金额失败！");
                throw new Exception();
            }
        }
        return CommonBL.carry(getMoney);
    }

    /**
     * 得到本年保单年度
     * @param tLPEdorItemSchema LPEdorItemSchema
     */
    private String getUpPolYears(String contNo, String edorValiDate)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo())
        {
            mErrors.addOneError("未查询到保单" + tLCContDB.getContNo() + "的信息");
            return null;
        }
        String tCValiDate = tLCContDB.getCValiDate();
        String tEdorValiDate = edorValiDate;
        if (tCValiDate == null || tEdorValiDate == null)
        {
            return null;
        }

        int compared = tEdorValiDate.compareTo(tCValiDate);
        if (compared <= 0)
        {
            String errMsg = "保全生效日期" + tEdorValiDate
                    + "应晚于保单生效日期" + tCValiDate;
            mErrors.addOneError(errMsg);
            System.out.println(errMsg);
            return "1";
        }

        String sql = "select date('" + tEdorValiDate + "') - 1 days from dual ";
        ExeSQL e = new ExeSQL();
        tEdorValiDate = e.getOneValue(sql);
        if (tEdorValiDate == null || tEdorValiDate.equals(""))
        {
            return null;
        }
        int years = PubFun.calInterval(tCValiDate, tEdorValiDate, "Y");
        return String.valueOf(years + 1);
    }

    /**
     * 得到财务类型
     * @param edorType String
     * @param code String
     * @return String
     */
    private String getFeeType(String edorType, String code)
    {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType(edorType);
        tLDCode1DB.setCode(code);
        tLDCode1DB.setCode1("000000");
        if (!tLDCode1DB.getInfo())
        {
            mErrors.addOneError("财务类型表LDCode1中没有该项目的配置信息！");
            return null;
        }
        return tLDCode1DB.getCodeName();
    }

    /**
     *  往批改补退费表（应收/应付）新增数据
     * @param aLPPolSchema LPPolSchema
     * @param changePrem double
     * @return boolean
     */
    private boolean setLJSGetEndorse(LPPolSchema aLPPolSchema, double changePrem)
    {
        String feeType = null;
        if (changePrem > 0) //补费
        {
            feeType = getFeeType(mEdorType, "BF");
        }
        else if (changePrem <= 0) //退费
        {
            feeType = getFeeType(mEdorType, "TF");
        }
        if (feeType == null)
        {
            mErrors.addOneError("未找到财务代码类型！");
            return false;
        }

        BqCalBL bqCalBL = new BqCalBL();
        LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType, changePrem,
                mGlobalInput);
        tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置P表的保费
     */
    private void setLPTables(double chgPrem)
    {
        for (int i = 1; i <= mReCalBL.aftLPPolSet.size(); i++)
        {
            double sumPrem = mReCalBL.aftLPPolSet.get(i).getSumPrem() + chgPrem;
            mReCalBL.aftLPPolSet.get(i).setSumPrem(sumPrem);
        }
        mMap.put(mReCalBL.aftLPPolSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPDutySet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPPremSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPGetSet, "DELETE&INSERT");
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem()
    {
        mLPEdorItemSchema.setGetMoney(mGetMoney);
        //mLPEdorItemSchema.setChgPrem(mChgPrem);
        mLPEdorItemSchema.setChgAmnt(0); //只变保费，保额不变
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPEdorItemSchema, "DELETE&INSERT");
    }

    /**
     * 更新LPCont表的保费
     */
    private void setLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        tLPContSchema.setPrem(String.valueOf(tLPContSchema.getPrem() + mChgPrem));
        tLPContSchema.setSumPrem(String.valueOf(tLPContSchema.getSumPrem() + mChgPrem));
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setMakeDate(mCurrentDate);
        tLPContSchema.setMakeTime(mCurrentTime);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
    }
    
//  20081124 zhanggm 长期险减保额或降档按退保公式退费
    private double calLRiskGetMoney(double oldPrem, double newPrem, LCPolSchema aLCPolSchema, String edorValidate) 
    {
    	double getMoney = 0.0;
        EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
        tEdorCalZTTestBL.setEdorValiDate(edorValidate);
        tEdorCalZTTestBL.setCurPayToDateLongPol(aLCPolSchema.getPaytoDate());
        tEdorCalZTTestBL.setNeedBugetResultFlag(true);
        tEdorCalZTTestBL.setOperator(mGlobalInput.Operator);
        LJSGetEndorseSchema tLJSGetEndorseSchema = tEdorCalZTTestBL.budgetOnePol(aLCPolSchema);
        getMoney = tLJSGetEndorseSchema.getGetMoney();
        getMoney = getMoney*(oldPrem-newPrem)/oldPrem;
        System.out.print("险种"+ aLCPolSchema.getRiskCode() +"应退费"+CommonBL.carry(-getMoney)+"元");
        return CommonBL.carry(getMoney);
	}
}
