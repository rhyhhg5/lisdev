package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:预收保费理算类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorYSAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private AppAcc mAppAcc = new AppAcc();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                    getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
            mEdorType = mLPGrpEdorItemSchema.getEdorType();
            mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保客户信息！");
            return false;
        }
        String customerNo = tLCGrpAppntDB.getCustomerNo();
        LCAppAccTraceSchema tLCAppAccTraceSchema =
                mAppAcc.getLCAppAccTrace(customerNo, mEdorNo, BQ.NOTICETYPE_G);
        setGetEndorse(tLCAppAccTraceSchema);
        setEdorItem(tLCAppAccTraceSchema.getMoney());
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCAppAccTraceSchema tLCAppAccTraceSchema)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("未找到保单信息！");
            return false;
        }
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(BQ.FILLDATA);
        tLJSGetEndorseSchema.setGrpPolNo(BQ.GRPFILLDATA);
        tLJSGetEndorseSchema.setPolNo(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setAppntNo(tLCAppAccTraceSchema.getCustomerNo());
        tLJSGetEndorseSchema.setInsuredNo(BQ.FILLDATA);
        tLJSGetEndorseSchema.setGetDate(mLPGrpEdorItemSchema.getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(tLCAppAccTraceSchema.getMoney());
        tLJSGetEndorseSchema.setRiskCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setRiskVersion(BQ.FILLDATA);
        tLJSGetEndorseSchema.setAgentCom(tLCGrpContDB.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(tLCGrpContDB.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(tLCGrpContDB.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(tLCGrpContDB.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(tLCGrpContDB.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(tLCGrpContDB.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(tLCGrpContDB.getApproveTime());
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(tLCGrpContDB.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem(double getMoney)
    {
        mLPGrpEdorItemSchema.setGetMoney(getMoney);
        mLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPGrpEdorItemSchema.setModifyDate(mCurrentDate);
        mLPGrpEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPGrpEdorItemSchema, "DELETE&INSERT");
    }
}
