package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.HashMap;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorZTAppConfirmBL implements EdorAppConfirm {
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = null;
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private HashMap mSqlAndResultHashMap = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private MMap map = new MMap();

    public PEdorZTAppConfirmBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        //数据准备操作
        if (!prepareData()) {
            return false;
        }
        System.out.println("---End prepareData of PEdorZTAppConfirmBL---");

        prepareOutputData();

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData() {
        mResult = new VData();
        mResult.add(map);
    }


    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                                getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput = ((GlobalInput) mInputData.
                            getObjectByObjectName("GlobalInput", 0));
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    private LCInsuredSet getLPInsuredSet(LPEdorItemSchema tLPEdorItemSchema) {
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();

        if (tLPEdorItemSchema.getPolNo().equals("000000") &&
            tLPEdorItemSchema.getInsuredNo().equals("000000")) { //合同退保
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
        } else if (tLPEdorItemSchema.getPolNo().equals("000000")) { //被保险人退保
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
        }
        return tLCInsuredSet;
    }

    /**
     * 得到需要退保的保单（主附险），去除作犹豫期退保的保单
     * @return
     */
    private LPPolSet getLPPolSet(LPEdorItemSchema tLPEdorItemSchema) {
        //查找出所有的保单
        LPPolSet tLPPolSet = new LPPolSet();
        if (tLPEdorItemSchema.getPolNo().equals("000000") &&
            tLPEdorItemSchema.getInsuredNo().equals("000000")) { //合同退保
            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询被保险人失败！");
                return null;
            }
            for (int i = 1; i <= tLCInsuredSet.size(); i++) {
                LCPolSet tLCPolSet = new LCPolSet();
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setContNo(tLPEdorItemSchema.getContNo());
                tLCPolDB.setInsuredNo(tLCInsuredSet.get(i).getInsuredNo());
                tLCPolSet = tLCPolDB.query();
                if (tLCPolDB.mErrors.needDealError()) {
                    CError.buildErr(this, "查询保单失败！");
                    return null;
                }
                for (int k = 1; k <= tLCPolSet.size(); k++) {
                    LPPolBL tLPPolBL = new LPPolBL();
                    tLPEdorItemSchema.setPolNo(tLCPolSet.get(k).getPolNo());
                    tLPPolBL.queryLPPol(tLPEdorItemSchema);
                    tLPPolSet.add(tLPPolBL);
                }

            }

        } else if (tLPEdorItemSchema.getPolNo().equals("000000")) { //被保险人退保
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCPolDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            tLCPolSet = tLCPolDB.query();
            if (tLCPolDB.mErrors.needDealError()) {
                CError.buildErr(this, "查询保单失败！");
                return null;
            }
            for (int i = 1; i <= tLCPolSet.size(); i++) {
                LPPolBL tLPPolBL = new LPPolBL();
                tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());
                tLPPolBL.queryLPPol(tLPEdorItemSchema);
                tLPPolSet.add(tLPPolBL);
            }
        } else {
            LPPolBL tLPPolBL = new LPPolBL();
            if (tLPPolBL.queryLPPol(tLPEdorItemSchema)) {
                tLPPolSet.add(tLPPolBL);
            } else {
                CError.buildErr(this, "查询保单失败！");
                return null;
            }

        }

        System.out.println("需要退保的保单数:" + tLPPolSet.size());
        return tLPPolSet;
    }

    /**
     * 准备需要退保的数据，单个个人保单退保入口，LPEdorItemSchema中只需要PolNo、EdorNo和EdorType即可
     * @return
     */
    public boolean getZTData(LPEdorItemSchema pLPEdorItemSchema) {
        EdorCalZT tEdorCalZT = null;
        try {
            //得到序要退保的被保人
            LCInsuredSet tLCInsuredSet = getLPInsuredSet(pLPEdorItemSchema);
            for (int t = 1; t <= tLCInsuredSet.size(); t++) {
                LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet(); //被保人退费财务数据

                //得到被保人的险种
                LPPolSet tLPPolSet = getInsuredPol(pLPEdorItemSchema,
                        tLCInsuredSet.get(t));

                //循环每个险种，进行退保处理
                for (int i = 1; i <= tLPPolSet.size(); i++) {
                    //为保单制作个单保全主表数据
                    LPEdorItemSchema tLPEdorItemSchema = pLPEdorItemSchema.
                            getSchema();
                    tLPEdorItemSchema.setPolNo(tLPPolSet.get(i).getPolNo());

                    tEdorCalZT = new EdorCalZT(tLPPolSet.get(i), true, "1");
                    double getmoney = 0;
                    
                    //针对按非账户退保的多责任层减人时出现的精度的问题，现取保留五位小数的汇总退保金额 2018-8-1 lzj
                    String isACCTB = new ExeSQL().getOneValue("select 1 from ldcode where codetype = 'bqzt' and code = '"+tLPPolSet.get(i).getRiskCode()+"'");
                    if("1".equals(isACCTB)){
                    	getmoney = -tEdorCalZT.calZTData2(tLPEdorItemSchema);
                    }else{
                    	tEdorCalZT.calZTData(tLPEdorItemSchema);
                    }
                    if (tEdorCalZT.mErrors.needDealError()) {
                        mErrors.copyAllErrors(tEdorCalZT.mErrors);
                        CError.buildErr(this, "退保计算失败！");
                        return false;
                    }
                    //-------------
                    //为了处理增减人取相同算法时由于精度问题导致费用误差而将算费公式精度提高并将财务数据汇总为一条
                    LJSGetEndorseSet set = tEdorCalZT.mLJSGetEndorseSet;
                    if (set.size() > 0) {
                        double getMoney = 0;
                        for (int temp = 1; temp <= set.size(); temp++) {
                            getMoney += set.get(temp).getGetMoney();
                        }
                        LJSGetEndorseSchema schema = set.get(1);
                        schema.setDutyCode(BQ.FILLDATA);
                        schema.setPayPlanCode(BQ.FILLDATA);
                        if("1".equals(isACCTB)){
                            schema.setGetMoney(CommonBL.carry(getmoney));
                        }else{
                        	schema.setGetMoney(CommonBL.carry(getMoney));
                        }
                        tLJSGetEndorseSet.add(schema);
                    }
                    //--------------

                    //tLJSGetEndorseSet.add(tEdorCalZT.mLJSGetEndorseSet);
                }

                double importInsuredMoney = getImportMoney(pLPEdorItemSchema,
                        tLCInsuredSet.get(t)); //得到磁盘导入的保费
                //如果导入了退费金额，则按实收比例均摊退费
                if (importInsuredMoney != Double.MIN_VALUE) {
                    //客户除了特需险种外的实缴保费
                    double oneInsuredSumActuPayMoney
                            = getInsuredSumActuPayMoney(tLCInsuredSet.get(t));
                    if (!distributeMoney(importInsuredMoney,
                                         oneInsuredSumActuPayMoney,
                                         tLJSGetEndorseSet)) {
                        return false;
                    }
                }
                mLJSGetEndorseSet.add(tLJSGetEndorseSet);

                //若是医疗险减人，且退费选择了直接转入团体帐号的方式，则不需要生成财务数据
                if (!deleteEspecialPolFinalDataZT(pLPEdorItemSchema,
                                                  mLJSGetEndorseSet)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            if (tEdorCalZT != null) {
                mErrors.copyAllErrors(tEdorCalZT.mErrors);
            }
            mErrors.addOneError(ex.getMessage());
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 得到被保人的实交保费
     * @param tLCInsuredSchema LCInsuredSchema
     * @return double
     */
    private double getInsuredSumActuPayMoney(LCInsuredSchema tLCInsuredSchema) {
        double sumActuPayMoney = CommonBL.getCurSumActuPayMoneyOfCmnPol(
                tLCInsuredSchema.getContNo(), tLCInsuredSchema.getInsuredNo());

        return sumActuPayMoney;
    }

    /**
     * 按比例给每个交费项分配导入的退费
     * @param importInsuredMoney double：导入的被保人退费金额
     * @param oneInsuredSumActuPayMoney double：被保人的实交保费
     * @param tLJSGetEndorseSet LJSGetEndorseSet：按退保公式得到的退费财务数据
     */
    private boolean distributeMoney(double importInsuredMoney,
                                    double oneInsuredSumActuPayMoneyCmnPol,
                                    LJSGetEndorseSet tLJSGetEndorseSet) {
        if (CommonBL.carry(importInsuredMoney) - oneInsuredSumActuPayMoneyCmnPol > 0.001) {
            CError tError = new CError();
            tError.moduleName = "PEdorZTAppConfirmBL";
            tError.functionName = "distributeMoney";
            tError.errorMessage = "导入的被保人退费金额大于实交保费!";
            this.mErrors.addOneError(tError);
            return false;
        }
        ExeSQL tExeSQL = new ExeSQL();
        double sumDistributedMoney = 0;
        for (int i = 1; i < tLJSGetEndorseSet.size(); i++) {
            String sql = "  select riskType3 "
                         + "from LMRiskApp "
                         + "where riskCode = '"
                         + tLJSGetEndorseSet.get(i).getRiskCode() + "' ";
            String riskType3 = tExeSQL.getOneValue(sql);
            if (riskType3.equals(BQ.RISKTYPE3_SPECIAL)) {
                continue;
            }

            //---------------
            //为了处理增减人取相同算法时由于精度问题导致费用误差而将算费公式精度提高并将财务数据汇总为一条
            /*
                         sql = "  select sum(sumActuPayMoney) "
                         + "from LJAPayPerson "
                         + "where polNo = '"
                         + tLJSGetEndorseSet.get(i).getPolNo()
                         + "'  and dutyCode = '"
                         + tLJSGetEndorseSet.get(i).getDutyCode()
                         + "'  and panPlanCode = '"
                         + tLJSGetEndorseSet.get(i).getPayPlanCode() + "' ";
                         System.out.println(sql);
                         String moneyString = tExeSQL.getOneValue(sql);

                         double money = moneyString.equals("") ? 0
              : Double.parseDouble(moneyString);  //保费项实交保费
             */
            //-------------

            double money = CommonBL.getCurSumActuPayMoneyOfOnePol(
                    tLJSGetEndorseSet.get(i).getPolNo());
            //险种应退保费 = 被保人导入退费金额 * (险种实交保费 / 被保人实交保费)
            double actualGetMoney = 0.00;
            if ((Math.abs(oneInsuredSumActuPayMoneyCmnPol - 0) > 0.0001)) {
                actualGetMoney = PubFun.setPrecision(money /
                        oneInsuredSumActuPayMoneyCmnPol * importInsuredMoney,
                        "0.00");
            }
            tLJSGetEndorseSet.get(i).setGetMoney( -actualGetMoney);
            sumDistributedMoney += actualGetMoney;
        }
        tLJSGetEndorseSet.get(tLJSGetEndorseSet.size())
                .setGetMoney(PubFun.setPrecision(
                        -(importInsuredMoney - sumDistributedMoney), "0.00"));
        return true;
    }

    private double getImportMoney(LPEdorItemSchema pLPEdorItemSchema,
                                  LCInsuredSchema tLCInsuredSchema) {
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setGrpContNo(tLCInsuredSchema.getGrpContNo());
        tLPDiskImportDB.setEdorNo(pLPEdorItemSchema.getEdorNo());
        tLPDiskImportDB.setEdorType(pLPEdorItemSchema.getEdorType());
        tLPDiskImportDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        LPDiskImportSet set = tLPDiskImportDB.query();
        if (set.size() == 0) {
            return Double.MIN_VALUE;
        }
        //没有导入保费
        if (set.get(1).getMoney2() == null || set.get(1).getMoney2().equals("")) {
            return Double.MIN_VALUE;
        }

        return Double.parseDouble(set.get(1).getMoney2());
    }

    private LPPolSet getInsuredPol(LPEdorItemSchema pLPEdorItemSchema,
                                   LCInsuredSchema tLCInsuredSchema) {
        Reflections ref = new Reflections();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(tLCInsuredSchema.getContNo());
        tLCPolDB.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询保单失败！");
            return null;
        }

        LPPolSet tLPPolSet = new LPPolSet();
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema, tLCPolSet.get(i));
            tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPolSet.add(tLPPolSchema);
        }
        return tLPPolSet;
    }

    /**
     * 若是医疗险减人，且退费选择了直接转入团体帐号的方式，则不需要生成财务数据
     * @return boolean
     */
    private boolean deleteEspecialPolFinalDataZT(
            LPEdorItemSchema tLPEdorItemSchema,
            LJSGetEndorseSet tLJSGetEndorseSet) {
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
                tLPEdorItemSchema.getEdorAcceptNo(), BQ.EDORTYPE_ZT);
        tEdorItemSpecialData.query();
        String balaPayType = tEdorItemSpecialData
                             .getEdorValue(BQ.DETAILTYPE_BALAPAYTYPE);
        if (balaPayType == null
            || !balaPayType.equals(BQ.DETAILTYPE_BALAPAYTYPE1)) {
            return true;
        }

        for (int i = 1; i <= tLJSGetEndorseSet.size(); i++) {
            if (CommonBL.isEspecialPol(tLJSGetEndorseSet.get(i).getRiskCode())) 
            {
                //费用为0则不生成财务数据效果相同
                //tLJSGetEndorseSet.get(i).setGetMoney(0);
                tLJSGetEndorseSet.removeRange(i, i);
                i--;
            }
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData() {
        double aTotalReturn = 0; //总退费
        double tChgPrem = 0; //变动的保费

        //进行退保
        if (!getZTData(mLPEdorItemSchema.getSchema())) {
            System.out.println("退保计算失败");
            return false;
        }

        //若险种发生过理赔，则退费为0
        //在EdorCalZT中已进行了校验
//        GCheckClaimBL check = new GCheckClaimBL();
//        for(int i = 1; i <= mLJSGetEndorseSet.size(); i++)
//        {
//            if(check.checkClaimed(mLJSGetEndorseSet.get(i)))
//            {
//                mLJSGetEndorseSet.get(i).setGetMoney(0);
//            }
//            if(check.mErrors.needDealError())
//            {
//                mErrors.copyAllErrors(check.mErrors);
//                return false;
//            }
//        }

        //如果涉及补退费，则按照描述表中的财务类型进行修改
        LJSGetEndorseSchema tLJSGetEndorseSchema = null;
        LJSGetEndorseSet tempLJSGetEndorseSet = new LJSGetEndorseSet();
        LJSGetEndorseSet tLJSGetEndorseSet = mLJSGetEndorseSet;
        if (tLJSGetEndorseSet != null && tLJSGetEndorseSet.size() > 0) {
            for (int k = 1; k <= tLJSGetEndorseSet.size(); k++) {
                tLJSGetEndorseSchema = tLJSGetEndorseSet.get(k);
                tLJSGetEndorseSchema.setGetFlag("1");
                tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);

                //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                        getEdorType(), "TB", tLJSGetEndorseSchema.getPolNo());
                if (finType.equals("")) {
                    CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                    return false;
                }
                tLJSGetEndorseSchema.setFeeFinaType(finType);

                tempLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                //计算出总费用
                aTotalReturn = aTotalReturn + tLJSGetEndorseSchema.getGetMoney();
            }
        }

        /*
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(mLPEdorItemSchema.getPolNo());
                tLCPolDB.getInfo();
                LCPolSchema tLCPolSchema = tLCPolDB.getSchema();

                tChgPrem = tChgPrem + tLCPolSchema.getSumPrem();

                //处理挂帐问题
                //查找余额退还
                if (tLCPolSchema.getLeavingMoney() > 0)
                {
                    LJSGetEndorseSchema tLJS = new LJSGetEndorseSchema();
                    if (tempLJSGetEndorseSet.size() > 0)
                    {
                        tLJS.setSchema(tLJSGetEndorseSchema);

                        //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                        String finType = BqCalBL.getFinType(mLPEdorItemSchema.
         getEdorType(), "YE", tLJSGetEndorseSchema.getPolNo());
                        if (finType.equals(""))
                        {
                            CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                            return false;
                        }
                        tLJS.setFeeFinaType(finType);

                        tLJS.setGetMoney(tLCPolSchema.getLeavingMoney());
                        tLJS.setPolNo(tLCPolSchema.getPolNo());
                        tLJS.setPayPlanCode(BQ.FILLDATA);
                    }
                    else
                    {
                        BqCalBase tBqCalBase = new BqCalBase();
                        BqCalBL tBqCalBL = new BqCalBL(tBqCalBase, "", "");

                        //从描述表中获取财务接口类型，modify by Minim at 2003-12-23
                        String finType = BqCalBL.getFinType(mLPEdorItemSchema.
         getEdorType(), "YE", tLJSGetEndorseSchema.getPolNo());
                        if (finType.equals(""))
                        {
                            CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
                            return false;
                        }

         tLJS = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
                                                          tLCPolSchema,
                                                          null,
                                                          finType,
         tLCPolSchema.getLeavingMoney(),
                                                          mGlobalInput);
                        tLJS.setOperator(mGlobalInput.Operator);
                    }

         aTotalReturn = aTotalReturn + tLCPolSchema.getLeavingMoney();
                    tempLJSGetEndorseSet.add(tLJS);
                }
         */

        //暂时不处理帐户的平衡问题
        //得到当前LPEdorMain保单信息主表的状态，并更新状态为申请确认。
        mLPEdorItemSchema.setChgPrem(tChgPrem);
        //付款要用负数，打印团体变动清单时统计要用，by Minim at 2003-12-11
        mLPEdorItemSchema.setGetMoney(aTotalReturn);
        mLPEdorItemSchema.setEdorState("2");
        mLPEdorItemSchema.setUWFlag("0");

        mInputData.clear();
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(tempLJSGetEndorseSet, "DELETE&INSERT");
        mInputData.add(mLPEdorItemSchema);
        mInputData.add(tempLJSGetEndorseSet);

        return true;
    }

    public static void main(String[] args) {
        LPEdorItemDB mLPEdorItemDB = new LPEdorItemDB();
        mLPEdorItemDB.setEdorNo("20060606000013");
        mLPEdorItemDB.setContNo("2300156307");
        mLPEdorItemDB.setEdorType("ZT");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "pa9501";
        mGlobalInput.ManageCom = "86";

        VData tVata = new VData();
        tVata.add(mLPEdorItemDB.query().get(1));
        tVata.add(mGlobalInput);
        PEdorZTAppConfirmBL tPEdorZTAppConfirmBL = new PEdorZTAppConfirmBL();
        tPEdorZTAppConfirmBL.submitData(tVata, "");
        if (tPEdorZTAppConfirmBL.mErrors.needDealError()) {
            System.out.println(tPEdorZTAppConfirmBL.mErrors.getErrContent());
        }
    }

}
