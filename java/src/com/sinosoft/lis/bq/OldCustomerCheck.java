package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 老客户校验</p>
 * <p>Description: 根据条件判断是否是老客户</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class OldCustomerCheck
{
    /** 是老客户 */
    public static final int OLD = 1;

    /** 是新客户 */
    public static final int NEW = 0;

    /** 可能是老客户 */
    public static final int MAYBEOLD = 2;

    public CErrors mErrors = new CErrors();

    private LDPersonSchema mLDPersonSchema = null;

    private LCInsuredSchema mLCInsuredSchema = null;

    private LDPersonSchema mOldLDPersonSchema = null;

    private String mCustomerNo = null;

    public OldCustomerCheck()
    {
    }

    /**
     * 构造函数
     * @param aLDPersonSchema LDPersonSchema
     */
    public OldCustomerCheck(LDPersonSchema aLDPersonSchema)
    {
        this.mLDPersonSchema = aLDPersonSchema;
    }

    public OldCustomerCheck(LCInsuredSchema aLCInsuredSchema)
    {
        this.mLCInsuredSchema = aLCInsuredSchema;
    }

    public void setLDPerson(LDPersonSchema aLDPersonSchema)
    {
        this.mLDPersonSchema = aLDPersonSchema;
    }

    public void setLCInsured(LCInsuredSchema aLCInsuredSchema)
    {
        this.mLCInsuredSchema = aLCInsuredSchema;
    }

    public LCInsuredSchema getLCInsured()
    {
        return mLCInsuredSchema;
    }


    /**
     * 校验是否是老客户，不是老客户就是新客户
     * @return int
     */
    public int checkOldCustomer()
    {
        //根据客户号校验
        String customerNo = mLDPersonSchema.getCustomerNo();
        if (customerNo != null)
        {
            if (checkCustomerNo(customerNo))
            {
                return OLD;
            }
        }
        //根据客户信息校验
        mOldLDPersonSchema = getOldCustomer();
        if (mOldLDPersonSchema != null)
        {
            mCustomerNo = mOldLDPersonSchema.getCustomerNo();
            return OLD;
        }
        return NEW;
    }

    public int checkInsured()
    {
        String insuredNo = mLCInsuredSchema.getInsuredNo();
        if (insuredNo != null)
        {
            if (!checkInsuredNo(insuredNo))
            {
                return NEW;
            }
            return OLD;
        }
        if (!checkInsuredInfo())
        {
            return NEW;
        }
        return OLD;
    }

    private boolean checkInsuredNo(String insuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
        tLCInsuredDB.setInsuredNo(insuredNo);
        if (tLCInsuredDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    private boolean checkInsuredInfo()
    {
        String othIdNo = mLCInsuredSchema.getOthIDNo();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        if ((othIdNo == null) || (othIdNo.equals("")))
        {
            tLCInsuredDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
            tLCInsuredDB.setName(mLCInsuredSchema.getName());
            tLCInsuredDB.setSex(mLCInsuredSchema.getSex());
            tLCInsuredDB.setBirthday(mLCInsuredSchema.getBirthday());
            tLCInsuredDB.setIDType(mLCInsuredSchema.getIDType());
            tLCInsuredDB.setIDNo(mLCInsuredSchema.getIDNo());
        }
        else
        {
            tLCInsuredDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
            tLCInsuredDB.setName(mLCInsuredSchema.getName());
            tLCInsuredDB.setOthIDNo(mLCInsuredSchema.getOthIDNo());
        }
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0)
        {
            return false;
        }
        mCustomerNo = tLCInsuredSet.get(1).getInsuredNo();
        mLCInsuredSchema = tLCInsuredSet.get(1);
        return true;
    }

    /**
     * 执行校验，如果不肯定是老客户还需判断是否可能是老客户
     * @return int 返回校验结果
     */
    public int check()
    {
        String customerNo = mLDPersonSchema.getCustomerNo();
        if (customerNo != null)
        {
            if (checkCustomerNo(customerNo))
            {
                return OLD;
            }
        }
        mOldLDPersonSchema = getOldCustomer();
        if (mOldLDPersonSchema != null)
        {
            mCustomerNo = mOldLDPersonSchema.getCustomerNo();
            return OLD;
        }
        createNewCustomerNo();
        if (checkMaybeOldCustomer())
        {
            return MAYBEOLD;
        }
        return NEW;
    }

    /**
     * 根据客户号校验是否是老客户
     * @return boolean
     */
    private boolean checkCustomerNo(String customerNo)
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(customerNo);
        if (!tLDPersonDB.getInfo())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到老客户信息，如果是新客户则返回新客户号信息
     * @return LDPersonSchema
     */
    public LDPersonSchema getLDPerson()
    {
        if (mOldLDPersonSchema != null)
        {
            return mOldLDPersonSchema;
        }
        else
        {
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            tLDPersonSchema.setCustomerNo(mCustomerNo);
            return tLDPersonSchema;
        }
    }

    /**
     * 得到客户号，如果是老客户则返回老客户号，如果是新客户则返回新客户号
     * @return String
     */
    public String getCustomerNo()
    {
        System.out.println("VVV" + mCustomerNo);
        return mCustomerNo;
    }

    /**
     * 生成客户合并工单
     * @param gi GlobalInput
     * @return String
     */
    public String createCombineTask(GlobalInput gi)
    {
        String info = "系统生成的客户合并作业。客户" + mLDPersonSchema.getName() +
                "(客户号" + mCustomerNo + ")可能是老客户";
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mCustomerNo);
        tLGWorkSchema.setCustomerName(mLDPersonSchema.getName());
        tLGWorkSchema.setTypeNo("06");
        tLGWorkSchema.setDateLimit("");
        tLGWorkSchema.setApplyTypeNo("");
        tLGWorkSchema.setApplyName("");
        tLGWorkSchema.setPriorityNo("");
        tLGWorkSchema.setAcceptWayNo("");
        tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
        tLGWorkSchema.setAcceptCom(gi.ManageCom);
        tLGWorkSchema.setAcceptorNo(gi.Operator);
        tLGWorkSchema.setRemark(info);

        VData tVData = new VData();
        tVData.add(gi);
        tVData.add(tLGWorkSchema);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        if (!tTaskInputBL.submitData(tVData, "INSERT||MAIN"))
        {
            return null;
        }
        return tTaskInputBL.getWorkNo();
    }


    /**
     * 检查是否是老客户，姓名、性别、年龄、证件类型、证件号码都一致认为是老客户
     * @return String 返回老客户信息
     */
    private LDPersonSchema getOldCustomer()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        if(mLDPersonSchema.getIDType()!=null&&!"0".equals(mLDPersonSchema.getIDType())){
        	tLDPersonDB.setName(mLDPersonSchema.getName());
        	tLDPersonDB.setSex(mLDPersonSchema.getSex());
        	tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
        	tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
        	tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
        }else{
        	tLDPersonDB.setName(mLDPersonSchema.getName());
        	tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
        	tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
        }
        LDPersonSet tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 0)
        {
            return null;
        }
        return tLDPersonSet.get(1);
    }

    /**
     * 产生新客户号
     * @return String
     */
    private void createNewCustomerNo()
    {
        mCustomerNo = PubFun1.CreateMaxNo("CUSTOMERNO", null);
    }

    /**
     * 校验是否可能是老客户
     * @return boolean 返回false则表明不可能是老客户
     */
    private boolean checkMaybeOldCustomer()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
        tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
        LDPersonSet tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 0)
        {
            return false;
        }
        return true;
    }


    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }
}
