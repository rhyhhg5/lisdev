package com.sinosoft.lis.bq;

import sqlj.runtime.error.Errors;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

public class LMloanComSetUI {


    private LMloanComSetBL mLMloanComSetBL = null;

    public CErrors mErrors = new CErrors();
    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public LMloanComSetUI(GlobalInput gi, String aManageCom)
    {
    	mLMloanComSetBL = new LMloanComSetBL(gi, aManageCom);
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mLMloanComSetBL.submitData())
        {
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "设置错误！";
            this.mErrors.addOneError(tError);        	
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public CErrors getError()
    {
        return mLMloanComSetBL.mErrors;
    }


}
