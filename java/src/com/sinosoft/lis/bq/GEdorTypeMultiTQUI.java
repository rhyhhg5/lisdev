package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:减人业务处理</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class GEdorTypeMultiTQUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    public GEdorTypeMultiTQUI()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---GEdorTypeMultiTQUI BEGIN---");
        GEdorTypeMultiTQBL tGEdorTypeMultiTQBL = new GEdorTypeMultiTQBL();

        if (tGEdorTypeMultiTQBL.submitData(cInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGEdorTypeMultiTQBL.mErrors);
            mResult.clear();
            mResult.add(mErrors.getFirstError());
            return false;
        }
        else
        {
            mResult = tGEdorTypeMultiTQBL.getResult();
        }
        System.out.println("---GEdorZTDetailUI END---");

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }
}
