package com.sinosoft.lis.bq;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author 马传苏
 * 2015-03-09
 *	江苏中介平台接口对接
 */
public class CheckAgentComBL {
	/**添加日志*/
	private Logger cLogger = Logger.getLogger(getClass());
	
	/** 往工作流引擎中传输数据的容器 */
	private TransferData mTransferData = new TransferData(); 
	
	private VData mInputData = new VData();
	
	private String mContno = "";
	
	private String mEdorNo = "";
	
	private String mContType= "";
	
	private String mEdorType="";
	/**
	 * 中介机构校验-批单
	 * 开发时间：2015-03-09
	 * machuansu
	 */
    private TransferData mOutxml=new TransferData();
    
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    private ExeSQL mExeSQL = new ExeSQL();
    
	public boolean submitData(VData cInputData,String cOperate){
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("Start CheckAgentComBL Submit...");
		if(!getInputData(cInputData,cOperate)){
			return false;
		}
		System.out.println("Deal getInputData finish.....");
		if(!checkData()){
			return false;
		}
		try {
			if(!dealData()){
				return false;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println("End CheckAgentComBL Submit...");
        mInputData = null;
        return true;
	}
	
	private boolean getInputData(VData cInputData,String cOperate){
		//从输入数据中得到所有对象
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mInputData = cInputData;
        mContno=(String)mTransferData.getValueByName("ContNo");
        mEdorNo=(String)mTransferData.getValueByName("EdorNo");
        mEdorType=(String)mTransferData.getValueByName("EdorType");
        if (mContno == null || "".equals(mContno))
        {
            // @@错误处理
        	CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单号获取失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mContType=(String)mTransferData.getValueByName("ContType");
        if(null == mContType || "".equals(mContType)){
        	// @@错误处理
        	CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单类型获取失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(null == mEdorType || "".equals(mEdorType)){
        	// @@错误处理
        	CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "批改类型获取失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //处理发送基础数据封装
        String tcontsql="";
        if(mContType.equals(BQ.CONTTYPE_G)){
        	tcontsql=" select b.edoracceptno,(select AgentOrganCode from LAcom where agentcom=a.agentcom),"
        			+ " a.prtno,a.ManageCom,b.makedate,b.maketime from lcgrpcont a,lpedorapp b "
        			+ " where a.appntno=b.otherno and a.grpcontno='"+mContno+"' "
        			+ " and b.edoracceptno='"+mEdorNo+"' ";
        }else if(mContType.equals(BQ.CONTTYPE_P)){
        	tcontsql=" select b.edoracceptno,(select AgentOrganCode from LAcom where agentcom=a.agentcom),"
        		 	+ " a.prtno,a.ManageCom,b.makedate,b.maketime from lccont a,lpedorapp b "
        			+ " where a.appntno=b.otherno and a.contno='"+mContno+"' "
	        		+ " and b.edoracceptno='"+mEdorNo+"' "
	        		+ " union "
	        		+ " select b.edoracceptno,(select AgentOrganCode from LAcom where agentcom=a.agentcom),"
        		 	+ " a.prtno,a.ManageCom,b.makedate,b.maketime from lbcont a,lpedorapp b "
        			+ " where a.appntno=b.otherno and a.contno='"+mContno+"' "
	        		+ " and b.edoracceptno='"+mEdorNo+"' ";
        }else{
        	CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单类型错误！";
            this.mErrors.addOneError(tError);
            return false;
        }
        		        
        SSRS ssrs=mExeSQL.execSQL(tcontsql);
        if(null!=ssrs && ssrs.MaxRow > 0){
        	String mserNo = PubFun1.CreateMaxNo("SERIALNO", null).substring(0, 10);
        	String mEndorsementNo=ssrs.GetText(1, 1)+mEdorType;
        	mOutxml.setNameAndValue("SerialNo", mserNo);
        	mOutxml.setNameAndValue("EndorsementNo", mEndorsementNo);
        	mOutxml.setNameAndValue("AgencyCode", ssrs.GetText(1, 2));
        	mOutxml.setNameAndValue("PrtNo", ssrs.GetText(1,3)); 	
        	mOutxml.setNameAndValue("ComCode", ssrs.GetText(1, 4));
        	mOutxml.setNameAndValue("EndorsementApplyDate", ssrs.GetText(1, 5)+" "+ssrs.GetText(1, 6));
        }else{
        	CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单信息查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
		return true;
	}
	private boolean checkData(){
		
		return true;
	}
	private boolean dealData() throws FileNotFoundException{
		String tresult=checkEndorsement();	
		if(!"".equals(tresult) && tresult!=null){
			Document tresDocument=getDocument(tresult.toString());
			if(tresDocument!=null){
				Element tall=tresDocument.getRootElement();
				Element thead=tall.getChild("Head");
				System.out.println("tal:"+tall.getChild("Head").getChildText("ErrorCode"));
				Element tResponseCode=thead.getChild("ResponseCode");
				Element terrorMessage = thead.getChild("ErrorMessage");
				System.out.println("tResponseCode:"+tResponseCode.getText());
				if(tResponseCode!=null && !"".equals(tResponseCode.getText()) && "1".equals(tResponseCode.getText()) ){
					System.out.println("江苏中介平台返回成功！");
					return true;
				}else if(tResponseCode!=null && !"".equals(tResponseCode.getText()) && !"1".equals(tResponseCode.getText()) ){
					CError tError = new CError();
					tError.moduleName = "CheckAgentComBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = ""+terrorMessage.getText()+"";
		            this.mErrors.addOneError(tError);
		            return false;
				}else{
					CError tError = new CError();
		            tError.moduleName = "CheckAgentComBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "报文报错内容获取失败！";
		            this.mErrors.addOneError(tError);
		            return false;
				}
			}else{
				    CError tError = new CError();
		            tError.moduleName = "CheckAgentComBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "输入报文失败！";
		            this.mErrors.addOneError(tError);
		            return false;
			}
		}else{
			CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "dealData";
            tError.errorMessage = "获取返回报文失败！";
            this.mErrors.addOneError(tError);
			return false;
		}
	}
	/**使用WebService调取江苏中介接口*/
	public String checkEndorsement(){
		String tResponseXml = "";
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;
		
		try {
			RPCServiceClient tRPCServiceClient = new RPCServiceClient();
			ExeSQL tExeSQL=new ExeSQL();
			String str="select codename from ldcode where codetype='JSAgentComChk' "; 
			String url = tExeSQL.getOneValue(str+" and code='url' ");
			String qName = tExeSQL.getOneValue(str+" and code='qName' ");
			String method = tExeSQL.getOneValue(str+" and code='method' ");
			Options options = tRPCServiceClient.getOptions();
			EndpointReference targetEPR = new EndpointReference(url);
			options.setTo(targetEPR);
            options.setTimeOutInMilliSeconds(120000);//
			QName opAddEntry = new QName(qName, method);
			String inputXml =getOutputXml(); //这里是中介机构校验接口请求报文
			System.out.println("打印报文： "+inputXml);
			String serialNo = "PICCjs_86320000";//这里是外部交易流水号，
			Object[] opAddEntryArgs = new Object[] { inputXml,"HX","C01",serialNo };
			Class[] classes = new Class[] { String.class,String.class,String.class,String.class };
            mCurTimeMillis = System.currentTimeMillis();
    		cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
    		cLogger.info("开始调用WebService服务！" + url);
            tResponseXml = 
            (String) tRPCServiceClient.invokeBlocking(opAddEntry, opAddEntryArgs, classes)[0];
            mCurTimeMillis = System.currentTimeMillis();
    		cLogger.info("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
    		System.out.println("返回的报文："+tResponseXml);
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CheckAgentComBL";
            tError.functionName = "checkEndorsement";
            tError.errorMessage = "发送校验报文失败!";
            this.mErrors.addOneError(e.getMessage());
		}
		return tResponseXml;
	}
	/**请求报文  Dom解析*/
	private String getOutputXml(){
		//根
		Element Packet_Element = new Element("Packet");
		Packet_Element.addAttribute("type", "REQUEST");
		Packet_Element.addAttribute("version", "1.0");
		if(mContType.equals(BQ.CONTTYPE_G)){
			Packet_Element.addAttribute("channel","0");
		}else if(mContType.equals(BQ.CONTTYPE_P)){
			Packet_Element.addAttribute("channel","1");
		}
        //创建头
		Element Head = new Element("Head");
		addElement(Head, "RequestType", "C01");
		//创建体
		Element Body = new Element("Body");
		//保单层中的批单*/
		Element Endorsement = new Element("Endorsement");
		addElement(Endorsement, "SerialNo", (String)mOutxml.getValueByName("SerialNo"));;//序号
		addElement(Endorsement, "PrtNo", (String)mOutxml.getValueByName("PrtNo"));//印刷号
		addElement(Endorsement, "EndorsementNo", (String)mOutxml.getValueByName("EndorsementNo"));//工单号
		addElement(Endorsement, "AgencyCode", (String)mOutxml.getValueByName("AgencyCode"));//中介机构组织机构代码
		addElement(Endorsement, "ComCode", (String)mOutxml.getValueByName("ComCode"));//批单归属保险公司代码
		addElement(Endorsement, "EndorsementApplyDate", (String)mOutxml.getValueByName("EndorsementApplyDate"));//<!--批改申请日期；格式：精确到秒-->		
		
		Body.addContent(Endorsement);	
		Packet_Element.addContent(Head);
		Packet_Element.addContent(Body);
        Document tDocument = new Document(Packet_Element);
        JdomUtil.print(tDocument.getRootElement());//向控制台输出
        return DomtoString(tDocument); 
	}
	//向外输出XML
	private String DomtoString(Document mDoCument){
		XMLOutputter xmlout = new XMLOutputter();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			xmlout.output(mDoCument, bos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String xmlStr = bos.toString();
    	System.out.println("发送报文："+xmlStr);
		return xmlStr;
	}
	//读入XML
	private Document getDocument(String tstr){
		java.io.Reader in = new StringReader(tstr);  
        Document doc=null;
		try {
			doc = (new SAXBuilder()).build(in);
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}         
        return doc; 
	}

	//添加子元素 
    private static Element addElement(Element element, String elementName, String elementValue) {
    	Element tElementName = new Element(elementName);
    	tElementName.setText(elementValue);
    	element.addContent(tElementName);
		return element;
	}
    public static void main(String[] args) {
//    	CheckAgentComBL dealEdorDataBL = new CheckAgentComBL();
//	   	TransferData mTransferData = new TransferData();
//		mTransferData.setNameAndValue("ContNo", "0000000801");
//		mTransferData.setNameAndValue("EdorNo", "20060306000043");
//		VData cInputData = new VData();
//		cInputData.add(mTransferData);
//		dealEdorDataBL.submitData(cInputData,"");
//		
//    	String mserNo = "2123456789".substring(0, 10);
//		System.out.println(mserNo);
//    	dealEdorDataBL.submitData(cInputData, cOperate);
//    	
//    	  Date nowTime = new Date(System.currentTimeMillis());
//    	  SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd");
//    	  String retStrFormatNowDate = sdFormatter.format(nowTime);
//   	  System.out.println(retStrFormatNowDate);
//    	  System.out.println(System.currentTimeMillis());
    	}
}
