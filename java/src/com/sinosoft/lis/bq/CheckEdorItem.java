package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpContStateDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LLClaimPolicyDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.operfee.FeeConst;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCGrpContStateSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CheckEdorItem {
    /** 错误的容器 */
    public CErrors mErrors = new CErrors();

    private String mManageCom = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mContType = null;

    private String mContNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    public void setContType(String contType) {
        this.mContType = contType;
    }

    public void setManageCom(String manageCom) {
        this.mManageCom = manageCom;
    }

    public void setContNo(String contNo) {
        this.mContNo = contNo;
    }

    public void setEdorType(String edorType) {
        this.mEdorType = edorType;
    }

    public void setGrpContNo(String grpContNo) {
        this.mGrpContNo = grpContNo;
    }

    /**
     * 设置保全项目信息
     * @param tLPEdorItemSchema LPEdorItemSchema: 个单保全项目
     */
    public void setLPEdorItem(LPEdorItemSchema tLPEdorItemSchema) {
        mLPEdorItemSchema = tLPEdorItemSchema;
    }

    /**
     * 设置保全项目信息
     * @param tLPEdorItemSchema LPEdorItemSchema: 个单保全项目
     */
    public void setLPGrpEdorItem(LPGrpEdorItemSchema cLPGrpEdorItemSchema) {
        mLPGrpEdorItemSchema = cLPGrpEdorItemSchema;
    }

    private LCPolSet getLCPolSet() {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        return tLCPolDB.query();
    }

    public boolean submitData() {
        if (!dealData()) {
            return false;
        }
        return true;
    }

    private boolean dealData() {
        System.out.println("11111");


        if (!checkManageCom()) {
            return false;
        }


        //if(!checkCont())
        //{
        //    return false;
        //}
        
        //针对江苏分公司保单中介机构的有效性进行校验
        if(!checkAgentCom()){
        	return false;
        }

        if (!checkByEdorType()) {
            return false;
        }

        if (checkBack()) {
            return false;
        }

        if (!checkClaimed()) {
            return false;
        }
        // qulq 061220 add
        if (!checkDueFee()) {
            return false;
        }
        
        // zhanggm 20101011 校验满期给付
        if (!checkExpirBenefit()) 
        {
            return false;
        }
        return true;
    }

    /**
     * 判断是否是续期催收发盘中`
     * @return boolean false bankOnTheWayfalg = 1， true
     */
    private boolean checkDueFee() {
        String sql = "select * from LJSPay " +
                     " where OtherNoType = '2' " + //2是个单续期
//        + " and bankonthewayflag = '1' " +
                     "and OtherNo = '" + mContNo + "' ";

        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0) {
            for (int i = 1; i <= tLJSPaySet.size(); i++) {
                if (tLJSPaySet.get(i).getBankOnTheWayFlag() != null
                    && tLJSPaySet.get(i).getBankOnTheWayFlag().equals("1")) {
                    mErrors.addOneError("该保单的续期应收记录已经被发盘，当前不能处理保全业务！");
                    return false;
                }
                LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i);
                VData tVDada = new VData();
                MMap mMMap = new MMap();
                tLJSPaySchema.setCanSendBank("3");
                mMMap.put(tLJSPaySchema, "UPDATE");
                tVDada.add(mMMap);
                PubSubmit temp = new PubSubmit();
                if (!temp.submitData(tVDada, "")) {
                    mErrors.addOneError("设定阻止银行转帐状态失败！");
                    return false;
                }
            }

        }
        return true;
    }
    //判断保单是否有没有结案的电话催缴工单
    private boolean checkDueFeeOnTheWay()
    {
        String sql =" select b.workno From lgphonehasten a,lgwork b ,ljspay c where 1=1 "
                +" and a.workno = b.workno "
                + " and a.getnoticeno=c.getnoticeno "
                + " and b.statusno not in ('5','8') "
                + " and c.otherno = '" + mLPEdorItemSchema.getContNo() + "'"
                ;
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow()>0)
        {
            mErrors.addOneError("保单"+mLPEdorItemSchema.getContNo()+"有没有结案的电话催缴工单："+tSSRS.GetText(1,1));
            return false ;
        }
        return true ;
    }
    
    //万能续期补费的相关校验
    private boolean checkWX()
    {
    	ExeSQL tExeSQL =new ExeSQL();
//    	1.	仅有万能保单能添加该保全项目
        String tpolcount =tExeSQL.getOneValue(" select count(1) from lcpol where stateflag='1' and riskcode in (select riskcode from lmriskapp where risktype4='4') and contno='" + mLPEdorItemSchema.getContNo() + "'");
        if(!tpolcount.equals("1")){
        	// @@错误处理
			System.out.println("CheckEdorItem+checkWX++--");
			CError tError = new CError();
			tError.moduleName = "CheckEdorItem";
			tError.functionName = "checkWX";
			tError.errorMessage = "该保单下没有有效的万能保单!";
			mErrors.addOneError(tError);
			return false;
        }
//      2.	仅有存在成功续期的保单能添加该保全项目
        String txqcount =tExeSQL.getOneValue(" select count(1) from ljspayb where othernotype='2' and dealstate='1' and otherno='" + mLPEdorItemSchema.getContNo() + "'");
        if(txqcount.equals("0")){
        	// @@错误处理
			System.out.println("CheckEdorItem+checkWX++--");
			CError tError = new CError();
			tError.moduleName = "CheckEdorItem";
			tError.functionName = "checkWX";
			tError.errorMessage = "该保单下没有成功的续期记录!";
			mErrors.addOneError(tError);
			return false;
        }
//      3.	一个保单仅能操作一次该保全项目
        String tbqcount =tExeSQL.getOneValue(" select edorno from lpedoritem a where edortype='WX' and exists (select 1 from lpedorapp b where b.edoracceptno=a.edorno and b.edorstate='0') and contno='" + mLPEdorItemSchema.getContNo() + "'");
        if(!tbqcount.equals("")){
        	// @@错误处理
			System.out.println("CheckEdorItem+checkWX++--");
			CError tError = new CError();
			tError.moduleName = "CheckEdorItem";
			tError.functionName = "checkWX";
			tError.errorMessage = "一个保单只能操作一次该保全项目,该单之前的工单号为"+tbqcount+"!";
			mErrors.addOneError(tError);
			return false;
        }
//      4.	保全项目生效日期必须为保单当前已结算的截止日期
        String tbaladate =tExeSQL.getOneValue(" select baladate from lcinsureacc a where contno='" + mLPEdorItemSchema.getContNo() + "'");
        String tpaydate =tExeSQL.getOneValue(" select max(a.paydate) from lcinsureacctrace a,ljspayb b  where a.contno=b.otherno and a.otherno=b.getnoticeno and b.othernotype='2' and b.dealstate='1' and b.otherno='" + mLPEdorItemSchema.getContNo() + "' with ur");
        String tcomdate = PubFun.getLaterDate(tbaladate, tpaydate);
        String tlaterdate = PubFun.getLaterDate(tcomdate, mLPEdorItemSchema.getEdorValiDate());
        String tbeforedate = PubFun.getBeforeDate(tcomdate, mLPEdorItemSchema.getEdorValiDate());
        if((!tlaterdate.equals(tbeforedate))&&tcomdate.equals(tlaterdate)){
        	// @@错误处理
			System.out.println("CheckEdorItem+checkWX++--");
			CError tError = new CError();
			tError.moduleName = "CheckEdorItem";
			tError.functionName = "checkWX";
			tError.errorMessage = "保全的生效日期必须大于或者等于保单当前的月结截止日期和最后一次成功续期核销日期中较晚的一个，本保单对应的日期为"+tcomdate+"!";
			mErrors.addOneError(tError);
			return false;
        }
        return true ;
    }
    /**
     * 江苏分公司销售渠道为团险中介代理、银行代理、个险中介代理、互动中介的保单
     * 在添加保全项目要对中介机构进行验证
     * @return
     */
    private boolean checkAgentCom(){
    	ExeSQL tExeSQL=new ExeSQL();
    	//20150721 添加临时配置，特殊配置的保单不校验 
    	String tempCheck="";
    	//20150722 
    	String checkEdorType="select 1 from ldcode1 where codetype='JSZJEdorCode' and code='"+mEdorType+"' ";
    	String strSQL="";
    	String errorMes="";//错误信息
    	String edorNo="";
    	String checkContNo="";
    	String contType="";//保单类型
    	if (mContType.equals(BQ.CONTTYPE_P)) {
    		strSQL="select 1 from lccont where contno='"+this.mContNo+"' and salechnl in ('03','04','10','15') "
    			+ " and appflag='1' and managecom like '8632%' "
    			+ " union "
    			+ " select 1 from lbcont where contno='"+this.mContNo+"' and salechnl in ('03','04','10','15') "
    			+ " and managecom like '8632%'  ";
    		checkContNo=this.mContNo;
    		edorNo=mLPEdorItemSchema.getEdorAcceptNo();
    		errorMes="保单"+this.mContNo+"的中介机构校验失败:";
    		contType=BQ.CONTTYPE_P;
    		tempCheck="select 1 from JSAGENTDATA where contno='"+this.mContNo+"' and OperateType='BQ' ";
    		checkEdorType +=" and code1='I'";
    	}
    	else if (mContType.equals(BQ.CONTTYPE_G)) {
    		strSQL="select 1 from lcgrpcont where grpcontno='"+this.mGrpContNo+"' and salechnl in ('03','04','10','15') "
			+ " and appflag='1' and managecom like '8632%' ";
    		checkContNo=this.mGrpContNo;
    		edorNo=mLPGrpEdorItemSchema.getEdorAcceptNo();
    		errorMes="保单"+this.mGrpContNo+"的中介机构校验失败:";
    		contType=BQ.CONTTYPE_G;
    		tempCheck="select 1 from JSAGENTDATA where grpcontno='"+this.mGrpContNo+"' and OperateType='BQ' ";
    		checkEdorType =checkEdorType +" and code1='G'";
    	}
    	//20150721 添加临时配置，特殊配置的保单不校验 
    	String tempFlag = tExeSQL.getOneValue(tempCheck);
    	if(null!=tempCheck && !"".equals(tempFlag)){
    		System.out.println("保单有特殊配置，不走中介机构校验:"+this.mContNo+this.mGrpContNo);
    		return true;
    	}
    	//20150722涉及手续费结算的保全项才进行机构校验
    	String checkEdorFlag = tExeSQL.getOneValue(checkEdorType);
    	if(null==checkEdorFlag || "".equals(checkEdorFlag)){
    		return true;
    	}
    	String checkFlag = tExeSQL.getOneValue(strSQL);
    	if(null!=checkFlag && !"".equals(checkFlag)){
    		System.out.println("-----进行中介机构校验");
    		System.out.println("---edorno:"+edorNo+"  --contno:"+checkContNo);
    		
    		CheckAgentComBL tCheckAgentComBL = new CheckAgentComBL();
    	    TransferData mTransferData = new TransferData();
    		mTransferData.setNameAndValue("ContNo", checkContNo);
    		mTransferData.setNameAndValue("EdorNo", edorNo);
    		mTransferData.setNameAndValue("ContType", contType);
    		mTransferData.setNameAndValue("EdorType", mEdorType);
    		VData cInputData = new VData();
    		cInputData.add(mTransferData);
    		if(!tCheckAgentComBL.submitData(cInputData, "")){
    			CError tCError = new CError();
    			tCError.moduleName="CheckAgentComBL";
    			tCError.functionName="submitData";
    			errorMes=errorMes+" "+tCheckAgentComBL.mErrors.getFirstError();
    			tCError.errorMessage=errorMes;
    			this.mErrors.addOneError(tCError);
    			return false;
    		}
    		System.out.println("DealEdorDataUI 调用成功..."); 		
    	}
    	return true;
    }

    /**
     * 按保全项目进行校验
     * @return boolean: 校验通过true
     */
    private boolean checkByEdorType() {
        if (mContType.equals(BQ.CONTTYPE_P)) {
            //qulq 061220 LR 项目只能单独做
            if (!checkPEdorItem()) {
                return false;
            }

            if (!checkHangUpState()) {
                return false;
            }

            if (!chekHangUpOn()) {
                return false;
            }
            
//          万能续期补费的相关校验
            if (mLPEdorItemSchema.getEdorType().equals("WX")) {
                if (!checkWX()) {
                    return false;
                }
            }

            // add by fuxin ZF项目有银行转账且银行在途中的不允许添加保全项目
            if (mLPEdorItemSchema.getEdorType().equals("ZF")) {
                if (!checkDueFeeOnTheWay()) {
                    return false;
                }
            }
            // add by fuxin 2008-4-28
//            if(!checkBQState())
//            {
//                return false ;
//            }

            if(!checkPLR(mLPEdorItemSchema))
            {
                return false;
            }
            if (mEdorType.equals(BQ.EDORTYPE_NS)) {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mContNo);
                if (!tLCContDB.getInfo()) {
                    mErrors.addOneError("查询保单失败");
                    return false;
                }

                if (tLCContDB.getCardFlag() != null &&
                    !tLCContDB.getCardFlag().equals("0")) {
                	//add by lzy 20150824支持福满人生险种添加附加万能B--730201
                	//add by lc  20161209支持美满一生个人护理保险添加附加万能B--340401
                	String is730201="select 1 from lcpol where contno='"+mContNo+"' and riskcode in ('730201','340401','730503','730301','730302','730604') and appflag='1' and stateflag='1' ";
//                	String is730201="select 1 from lcpol where contno='"+mContNo+"' and riskcode='730201' and appflag='1' and stateflag='1' ";
                    String flag730201=new ExeSQL().getOneValue(is730201);
                	if(null ==flag730201 || "".equals(flag730201)){
                		mErrors.addOneError("简易保单不能新增险种");
                		return false;
                	}
                }

                System.out.println("EdorValiDate, CInValiDate："
                                   + mLPEdorItemSchema.getEdorValiDate() + " "
                                   + tLCContDB.getCInValiDate());
                int intv = PubFun.calInterval(mLPEdorItemSchema.getEdorValiDate(),
                                              tLCContDB.getCInValiDate(), "D");
                if (intv < 0) {
                    mErrors.addOneError("保全项目生效日期不能大于保单失效日期"); //长期险可为保单失效日
                    return false;
                }
            }
            //有失效的才能做，add by fuxin 2008-7-8
            if (mEdorType.equals(BQ.EDORTYPE_FX) || mEdorType.equals(BQ.EDORTYPE_TF)|| mEdorType.equals(BQ.EDORTYPE_MF)) {
                if (!checkFX()) {
                    mErrors.addOneError("该单没有失效并且该单下也没有需要复效的险种");
                    return false;
                }
            }
            //20140825不再单独校验
            /*
            //add by fuxin 2009-11-16 添加理赔案件状态校验-理赔要求添加。
            if (mEdorType.equals(BQ.EDORTYPE_CT))
            {
                String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
                             mLPEdorItemSchema.getContNo() + "')"
                             +
                             "  and rgtstate not in('11','12','14') with ur  "
                             ;
                String rgtNo = new ExeSQL().getOneValue(sql);
                if (rgtNo != null && !rgtNo.equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "GrpEdorWTDetailBL";
                    tError.functionName = "checkClaim";
                    tError.errorMessage = "保单未结案的理陪，不能做解约！";
                    mErrors.addOneError(tError);
                    return false;
                }
                return true;
            }
            */
            //协议退保校验
            if (mEdorType.equals(BQ.EDORTYPE_XT)) {
                //校验是否失效 061205 qulq 新需求不需要
                /*
                                if(!checkContState())
                                {
                                    return false;
                                }
                 */
                //校验生效日是否大于满期日
                if (!checkEdorDate()) {
                    return false;
                }
                //校验理赔，续期，保全
                //20140825不再单独校验理赔
//                if (!checkClaimXT()) {
//                    return false;
//                }
                if (!checkDueFeePXT()) {
                    return false;
                }
                if (!checkPBQ()) {
                    return false;
                }

            }
            //个单迁移
            if (mEdorType.equals("PR")) {
                //校验是否有续期
                if (checkFee()) {
                    mErrors.addOneError("该单有续期应收保费，请将此续期应收保费作废后，再进行迁移!");
                    return false;
                }
                //校验理赔，续期，保全
//                if (!checkClaim()) {
//                    mErrors.addOneError("该单有未结案的理赔不能做保单迁移!");
//                    return false;
//                }
                if (!checkOtherBQ()) {
                    mErrors.addOneError("该单有未结案的保全不能做保单迁移!");
                    return false;
                }
            }
            
//          常无忧B给付补费
            if (mEdorType.equals(BQ.EDORTYPE_CB)) 
            {
            	System.out.println("校验常无忧B给付补费");   
            	
            	String sql = "select count(1) from ljaget a ,ljsgetdraw d "
                    + "where a.actugetno = d.getnoticeno "
                    + "and a.othernotype='20' and a.confdate is not null "
                    + "and d.riskcode in ('330501','530301') and d.feefinatype != 'YEI' "
                    + "and d.ContNo = '" + this.mContNo + "' ";
            	
            	String tCount = new ExeSQL().getOneValue(sql);
                if ("0".equals(tCount)) 
                {
                	mErrors.addOneError("该保单不存在已经给付完成的满期给付记录，不能做给付补费!");
                    return false;
                }
                
                sql = "select a.edorNo from LPEdorItem a, LPEdorApp b "
                    + "where a.edorAcceptNo = b.edorAcceptNo and EdorType = '" + mEdorType 
                    + "' and ContNo = '" + this.mContNo + "' and b.edorState = '" + BQ.EDORSTATE_CONFIRM + "' ";
            	String edorNo = new ExeSQL().getOneValue(sql);
                if (edorNo != null && !edorNo.equals("")) 
                {
                	mErrors.addOneError("该保单已经做过给付补费，保全批单号："+edorNo+"，不能再次操作！");
                    return false;
                }
            	
//            	if (!checkClaim()) 
//            	{
//                    mErrors.addOneError("该保单有未结案的理赔，不能做给付补费!");
//                    return false;
//                }
            	if (!checkOtherBQ()) 
            	{
                    mErrors.addOneError("该保单有未结案的保全，不能做给付补费!");
                    return false;
                }
            	
            	
                return true;
            }
            if(mEdorType.equals(BQ.EDORTYPE_WT)){
            	if (!checkDueFeePWT()) 
            	{
                    mErrors.addOneError("该保单已经做过续期核销，不能申请犹豫期退保!");
                    return false;
                }
            	return true;
            }
            
        } else if (mContType.equals(BQ.CONTTYPE_G)) {

            if (!checkBQ()) {
                return false;
            }
            if(!checkGLR(mLPGrpEdorItemSchema))
            {
                return false;
            }


            if (mEdorType.equals(BQ.EDORTYPE_WT)) {
                if (!checkWTG()) {
                    return false;
                }
            }
            if (mEdorType.equals(BQ.EDORTYPE_CT)) {
                if (!checkCTG()) {
                    return false;
                }
            }
            if (mEdorType.equals(BQ.EDORTYPE_XT)) {
                if (!checkClaimG()) {
                    return false;
                }
            }
            //团险万能协议退保
            if (mEdorType.equals("TG")) {
                if (!checkTG()) {
                    return false;
                }
            }
            if(mEdorType.equals(BQ.EDORTYPE_ZG)){
            	if(!checkClaimG()){
            		return false;
            	}
            }
            if (mEdorType.equals(BQ.EDORTYPE_FP)) {
                if (!checkFPG()) {
                    return false;
                }
            }
            if(mEdorType.equals(BQ.EDORTYPE_GD)){
            	if(!checkGrpContState()){
            		return false;
            	}
            	if(!checkClaimG()){
            		return false;
            	}
            }
            
            if(mEdorType.equals(BQ.EDORTYPE_ZA)){
            	if(!checkGrpContState()){
            		return false;
            	}
            	if(!checkClaimG()){
            		return false;
            	}
            }
            if (mEdorType.equals(BQ.EDORTYPE_LQ)) {
                if (!checkClaimG()) {
                    return false;
                }
            }
            if (mEdorType.equals(BQ.EDORTYPE_ZB)) {
                if (!checkClaimG()) {
                    return false;
                }
            }
            if (mEdorType.equals(BQ.EDORTYPE_ZE)) {
            	//保单是否有未结案的保全
            	if(!checkGrpBQState()){
            		return false;
            	}
            	//保单是否有未结案的理赔
                if (!checkClaimG()) {
                    return false;
                }
            }
            
            //团单保全减人增加校验
            if (mEdorType.equals(BQ.EDORTYPE_ZT)) {
            	//保单是否有未结案的保全
            	if(!checkGrpBQState()){
            		return false;
            	}
            	//保单是否有未结案的理赔
                if (!checkClaimG()) {
                    return false;
                }
            }
            //qulq 061220 LR 项目只能单独做
            if (!checkGEdorItem()) {
                return false;
            }
            if (mEdorType.equals(BQ.EDORTYPE_TF))
            {
                if (!checkTF()) {
                    mErrors.addOneError("该单没有失效不需要做特权复效！");
                    return false;
                }
            }
            //lzj 20180503 农名工团体万能险种保障调整新增校验
            if (mEdorType.equals(BQ.EDORTYPE_BF))
            {
            	if (!checkGrpBQState()) {
            		return false;
            	}
            	//保单是否有未结案的理赔
            	if (!checkClaimG()) {
            		return false;
            	}
            }
        }

        return true;
    }
    /**
     * 卡单和撕单不能做保单补发
     * @return boolean
     * 返回值为真，表示可以做
     */
    private boolean checkPLR (LPEdorItemSchema aLPEdorItemSchema)
    {
        if(mEdorType.equals("LR"))
        {
            String sql = "select 1 from lccont b,LZCardNumber a "
                         + " where a.cardno = b.prtno "
                         + " and a.OperateType in ('0','1') "
                         +
                    " and b.conttype = '1' and b.cardflag in ('1','2','6') "
                         + " and contno = '" + aLPEdorItemSchema.getContNo() +
                         "'";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if (tSSRS.getMaxRow() > 0) {
                mErrors.addOneError("卡单或撕单不能做补发！");
                return false;
            }
        }
        return true;
    }
    /**
     * 卡单和撕单不能做保单补发
     * @return boolean
     * 返回值为真，表示可以做
     */

    private boolean checkGLR (LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        if(mEdorType.equals("LR"))
        {
            String sql = " select 1 from lcgrpcont b,LZCardNumber a "
                         + " where a.cardno = b.prtno "
                         + " and a.OperateType in ('0','1') "
                         + " and b.cardflag in ('0') "
                         + " and grpcontno = '" +
                         aLPGrpEdorItemSchema.getGrpContNo() + "'"
                         ;
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if (tSSRS.getMaxRow() > 0) {
                 mErrors.addOneError("卡单或撕单不能做补发！");
                return false;
            }
        }
        return true;
    }

    /**
     *校验是否有续期应收，有则真
     * @return boolean
     */
    private boolean checkFee() {
        String sql = "select * from LJSPay " +
                     " where OtherNoType = '2' " + //2是个单续期
                     "and OtherNo = '" + mLPEdorItemSchema.getContNo() + "' ";
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0) {
            return true;
        }
        return false;
    }

    private boolean checkOtherBQ() {
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and ContNo = '" + this.mContNo + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM +
                     "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (edorNo != null && !edorNo.equals("")) {
            return false;
        }
        return true;
    }

    private boolean checkClaim() {
        String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
                     mLPEdorItemSchema.getContNo() + "')"
                     + " and rgtstate not in('11','12','14') "
                     ;
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {

            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean checkPEdorItem() {
    	
    	
    	//个单保全非客户资料变更校验客户信息
    	if(!BQ.EDORTYPE_CM.equals(mEdorType)&&!"AD".equals(mEdorType)){
    		//校验投保人信息
    		String appntSQL = " select appntno from lcappnt where contno='"+mLPEdorItemSchema.getContNo()+"' " +
    				" and (nvl(appntname,'')='' or nvl(appntsex,'') ='' or nvl(appntbirthday,date('2999-01-01'))=date('2999-01-01') or nvl(idtype,'')=''  or nvl(idno,'')=''  " +
    				")  with ur ";
    		 String checkAppnt = new ExeSQL().getOneValue(appntSQL);
    		 
    		 if(checkAppnt!=null&&!"".equals(checkAppnt)){
                 mErrors.addOneError( "客户号为"+checkAppnt+"的客户信息缺失，请先对保单做“客户资料变更”补充完善后方能进行其他保全项目申请");
                 return false;
    		 }
    		 //校验被保人信息
    		 String insuredSQL = "select insuredno from lcinsured where contno ='" +mLPEdorItemSchema.getContNo() + "' " +
    		 		" and (nvl(name,'')='' or nvl(sex,'')='' or nvl(birthday,date('2999-01-01'))=date('2999-01-01') or nvl(idtype,'')='' or nvl(idno,'')='' or nvl(relationtoappnt,'')='') " +
    		 		" with ur  ";
    		 String checkInsured = new ExeSQL().getOneValue(insuredSQL);
    		 if(checkInsured!=null&&!"".equals(checkInsured)){
                 mErrors.addOneError( "客户号为"+checkInsured+"的客户信息缺失，请先对保单做“客户资料变更”补充完善后方能进行其他保全项目申请");
                 return false;
    		 }
    		 
    		 //校验是否为蚂蚁金服线上保全解约
    		 String checkMYJFSQL = "select 1 from ldcode a,lccont b where a.codetype = 'MYJFBQ' and a.code = b.salechnl and a.comcode = b.agentcode and b.contno = '"+mLPEdorItemSchema.getContNo()+"' and a.OtherSign = '"+mLPEdorItemSchema.getEdorType()+"'" 
    		 					 + " and exists (select 1 from lgwork where workno = '"+mLPEdorItemSchema.getEdorAcceptNo()+"' and remark = '线上解约接口生成')"; 
    		 //校验保单是否为深圳分公司保单
    		 String checkSZcontSQL = "select 1 from lccont where contno = '"+mLPEdorItemSchema.getContNo()+"' and managecom like '8695%' and cardflag = 'b'";
    		 if(!"1".equals(new ExeSQL().getOneValue(checkMYJFSQL))&&!"1".equals(new ExeSQL().getOneValue(checkSZcontSQL))){
    			 String addressSQL = " select a.customerno from lcaddress a,lcappnt b where a.customerno=b.appntno " +
    			 " and a.addressno=b.addressno and b.contno='"+ mLPEdorItemSchema.getContNo()+"' " +
    			 " and ((nvl(a.PostalAddress,'')='') or (nvl(a.phone,'')='' and nvl(a.mobile,'')='')) with ur ";
    			 String addressInfo = new ExeSQL().getOneValue(addressSQL);
    			 if(addressInfo!=null&&!"".equals(addressInfo)){
    				 mErrors.addOneError( "投保人号为"+addressInfo+"的联系电话或联系地址信息缺失，请通过“联系方式变更”补充完整后再申请其他保全项目");
    				 return false;
    			 }    		 
    		 }
    	} 

        //申请mEdorType时校验其他项目
        if (mEdorType.equals(BQ.EDORTYPE_LR)
            || mEdorType.equals(BQ.EDORTYPE_CC)
            || mEdorType.equals(BQ.EDORTYPE_CT)
            || mEdorType.equals(BQ.EDORTYPE_WT)
            || mEdorType.equals(BQ.EDORTYPE_XT)
            || mEdorType.equals(BQ.EDORTYPE_NS)
            || mEdorType.equals(BQ.EDORTYPE_ZF)
            || mEdorType.equals(BQ.EDORTYPE_PR)
            || mEdorType.equals(BQ.EDORTYPE_TF)  ) {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

            tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());

            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            if (tLPEdorItemSet != null && tLPEdorItemSet.size() > 0) {
                mErrors.addOneError(mEdorType + "项目不能和其它项目一起做！");
                return false;
            }
        }
        
        if(mEdorType.equals(BQ.EDORTYPE_TP)){
        	//暂时只支持康乐人生险种
        	/*String riskSQL = " select riskcode from lcpol where riskcode not in ( " +
        			"  select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode' union select riskcode from lmriskapp where risktype8='9' union select '121501' from dual  ) " +
        			" and contno='"+mLPEdorItemSchema.getContNo()+"'  with ur ";
        	String riskcode = new ExeSQL().getOneValue(riskSQL);
        	if(riskcode!=null&&!"".equals(riskcode)){
        		 mErrors.addOneError("保单存在不支持“保全转保”的险种：“"+riskcode+"”");
        		 return false;
        	}
        	*/
        	//支持含有其他传统险产品的康乐人生保单进行转保，除康乐人生及其附加险、附加豁免险以外的险种在转保前后不发生任何变化。
        	String riskSQL="select distinct 1 from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' " +
			" and riskcode in (select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode')";
        	String riskFlag = new ExeSQL().getOneValue(riskSQL);
        	if(null==riskFlag||!"1".equals(riskFlag)){
        		mErrors.addOneError("保单不存在支持“保全转保”的险种");
        		return false;
        	}
        	//理赔豁免责任的保单，不允申请作此保全项目
        	
        	//存在理赔记录的保单不允许操作转保
            LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
            tLLClaimPolicyDB.setContNo(mLPEdorItemSchema.getContNo());
            if (tLLClaimPolicyDB.query().size() > 0)
            {
                mErrors.addOneError("该保单发生过理赔，无法操作康乐人生转保！");
                return false;
            }
        	
        	//校验保全申请日期与保全生效日期是否为同一天
        	String appDateSQL = " select 1 from lpedorapp where edoracceptno = '"+mLPEdorItemSchema.getEdorAcceptNo()+"'" +
        			" and edorappdate = '"+mLPEdorItemSchema.getEdorValiDate() +"' with ur ";
        	String dateFlag =  new ExeSQL().getOneValue(appDateSQL);
        	if(!"1".equals(dateFlag)){
       		 mErrors.addOneError("保全生效日期必须为保全申请当天");
    		 return false;        		
        	}
        	
        }
        
        
        //校验辽宁医保卡是否结算
        if(mEdorType.equals(BQ.EDORTYPE_CT)){
        	LCContDB tLCContDB = new LCContDB();
        	tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        	if(!tLCContDB.getInfo()){
        		 mErrors.addOneError("退保保单不存在");
        		return false;
        	}
        	LCContSchema tLCContSchema = new LCContSchema();
        	tLCContSchema = tLCContDB.getSchema();
        	String riskcodeSQl = "select count(1) count from lcpol " +
        			"where contno = '"
        	+tLCContSchema.getContNo()
        	+"' and riskcode in (select code from ldcode where codetype='ybkriskcode') with ur";
        	String count = new ExeSQL().getOneValue(riskcodeSQl);
        	if("0".equals(count)){	//上海医保卡不走下面校验
        		if("8".equals(tLCContSchema.getPayMode())){
        			String docSQL = "select 1 from db2inst1.lysendtosettle where succflag='0' and dealstate in ('1','2') " +
        			" and polno ='"+tLCContSchema.getPrtNo()+"' with ur  ";
        			String medicare = new ExeSQL().getOneValue(docSQL);
        			if(medicare==null||!"1".equals(medicare)){
        				mErrors.addOneError("该保单的医保卡缴费并未结算，不能添加解约项目");
        				return false;
        			}
        		}
        	}
        }

        if (mEdorType.equals(BQ.EDORTYPE_PC)) {
            String sql = "select EdorType from LPEdorItem "
                         + "where EdorNo = '"
                         + mLPEdorItemSchema.getEdorAcceptNo() + "' "
                         + "   and EdorType not in('AD', 'CC') ";
            String edorType = new ExeSQL().getOneValue(sql);
            if (!edorType.equals("")) {
                mErrors.addOneError(mEdorType + "项目只能与联系方式变更、交费资料变更一起操作");
                return false;
            }
        }

        //申请其他项目时校验mEdorType
        String sql = "select EdorType from LPEdorItem "
                     + "where EdorNo = '"
                     + mLPEdorItemSchema.getEdorAcceptNo() + "' "
                     +
                     "   and EdorType in('LR', 'CT', 'WT', 'XT', 'NS','CC','PR','ZF','TF') ";
        String edorType = new ExeSQL().getOneValue(sql);
        if (!edorType.equals("")) {
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkPEdorItem";
            tError.errorMessage = edorType + "项目不能和其它项目一起做！";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if ("AD,CC".indexOf(mEdorType) < 0) {
            //申请其他项目时校验mEdorType
            sql = "select EdorType from LPEdorItem "
                  + "where EdorNo = '"
                  + mLPEdorItemSchema.getEdorAcceptNo() + "' "
                  + "   and EdorType in('PC') ";
            edorType = new ExeSQL().getOneValue(sql);
            if (!edorType.equals("")) {
                CError tError = new CError();
                tError.moduleName = "CheckEdorItem";
                tError.functionName = "checkPEdorItem";
                tError.errorMessage = "PC项目只能与联系方式变更、交费资料变更一起操作！";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }
        if (mEdorType.equals(BQ.EDORTYPE_LQ)) {
        	//含有附加万能险种的保单支持保全部分领取并且保全生效日期必须为当前日期  --hehongwei
        	String riskSQL="select distinct 1 from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' " 
			 		+ " and riskcode in (select riskcode from lmriskapp where risktype4='4' and  subriskflag='S') with ur";
        	String riskFlag = new ExeSQL().getOneValue(riskSQL);
        	if("1".equals(riskFlag)){	
        		FDate tFDate = new FDate();
        		if(tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))!=0){
        			mErrors.addOneError("保全生效日期必须当前日期");
       		 	return false;        		
        		}
        	}
    	}
        if (mEdorType.equals(BQ.EDORTYPE_HA)) {
        	//含有附加万能险种的保单支持保全部分领取并且保全生效日期必须为当前日期  --hehongwei
        	String riskSQL="select distinct 1 from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' " 
			 		+ " and riskcode in (select riskcode from lmriskapp where risktype4='2') and polstate='03060001' with ur";
        	String riskFlag = new ExeSQL().getOneValue(riskSQL);
        	if("1".equals(riskFlag)){	
        		FDate tFDate = new FDate();
        		if(tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))!=0){
        			mErrors.addOneError("保全生效日期必须当前日期");
       		 		return false;        		
        		}
        	}else{
        		mErrors.addOneError("分红险的保单只有满期处理状态才能申请分红险满期领取保全");
        		return false;
        	}
    	}
        if (mEdorType.equals(BQ.EDORTYPE_MF)) {
        	//免息复效保全生效日期必须为当前日期  --lichang
			String riskSQL = "select 1 from lpedorapp where edoracceptno = '"+mLPEdorItemSchema.getEdorAcceptNo()+"' with ur";
			String riskFlag = new ExeSQL().getOneValue(riskSQL);
			if ("1".equals(riskFlag)) {
				FDate tFDate = new FDate();
				if (tFDate.getDate(PubFun.getCurrentDate()).compareTo(
						tFDate.getDate(mLPEdorItemSchema.getEdorValiDate())) != 0) {
					mErrors.addOneError("保全生效日期必须为当前日期");
					return false;
				}
			}
		}
        if (mEdorType.equals(BQ.EDORTYPE_CM)) {
        	//税优保单客户资料变更时，保全生效日期必须小于当前日期并且大于保单生效日期  --hehongwei
        	String riskSQL="select distinct cvalidate from lcpol where contno='"+mLPEdorItemSchema.getContNo()+"' " 
			 		+ " and riskcode in (select riskcode from lmriskapp where taxoptimal='Y')  with ur";
			String cvalidate = new ExeSQL().getOneValue(riskSQL);
			if (!cvalidate.equals("") && cvalidate!=null ) {
				FDate tFDate = new FDate();
				if(tFDate.getDate(PubFun.getCurrentDate()).before(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))){
					mErrors.addOneError("保全生效日期不能大于当前日期");
					return false;
				}else if(tFDate.getDate(cvalidate).after(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))){
					mErrors.addOneError("保全生效日期不能小于当前保单年度的生效日期");
					return false;
				}
			}else {
		        return true;
			}
		}

        return true;

    }

    private boolean checkGEdorItem() {
        if (mEdorType.equals(BQ.EDORTYPE_LR)
                || mEdorType.equals(BQ.EDORTYPE_XT)
                || mEdorType.equals(BQ.EDORTYPE_CT)
                || mEdorType.equals(BQ.EDORTYPE_WT)
                || mEdorType.equals(BQ.EDORTYPE_LQ)
                || mEdorType.equals(BQ.EDORTYPE_GA)
                || mEdorType.equals(BQ.EDORTYPE_ZB)
                || mEdorType.equals(BQ.EDORTYPE_TF)
                || mEdorType.equals("TG") 
        		|| mEdorType.equals(BQ.EDORTYPE_ZG)){
                LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
                tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
                LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
                if (tLPGrpEdorItemSet != null && tLPGrpEdorItemSet.size() > 0) {
                    mErrors.addOneError(mEdorType + "项目不能和其它项目一起做！");
                    return false;
                }

                //发生其他工单的保全
                String sql = "select a.edorNo "
                             + "from LPGrpEdorItem a, LPEdorApp b "
                             + "where a.edorAcceptNo = b.edorAcceptNo "
                             + "   and grpContNo = '" + this.mGrpContNo + "' "
                             + "   and edorNo != '"
                             + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                             + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM
                             + "' ";
                String edorNo = new ExeSQL().getOneValue(sql);
                if (!edorNo.equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "GrpEdorWTDetailBL";
                    tError.functionName = "checkClaim";
                    tError.errorMessage = "保单正在做保全业务：" + edorNo + "，不能做"
                                          + CommonBL.getEdorName(mEdorType, "G");
                    mErrors.addOneError(tError);
                    return false;
                }
            } else {
                LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
                tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
                tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_LR);
                LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
                if (tLPGrpEdorItemSet != null && tLPGrpEdorItemSet.size() > 0) {
                    mErrors.addOneError("LR项目不能和其它项目一起做！");
                    return false;
                }
                
                
                //正在做团险万能部分领取的团单不能操作其他业务
                String sql1 = "select a.edorNo "
                    + "from LPGrpEdorItem a, LPEdorApp b "
                    + "where a.edorAcceptNo = b.edorAcceptNo "
                    + "   and grpContNo = '" + this.mGrpContNo + "' "
                    + "   and edorNo != '"
                    + this.mLPGrpEdorItemSchema.getEdorNo() + "' " +
            		"and a.edortype in ('TL','CT')  " 
            + " and exists (select 1 from lcgrppol where grpcontno = a.grpcontno and  " +
            		" exists (select 1 from lmriskapp where riskcode = lcgrppol.riskcode and risktype4='4')) "
                    + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM
                    + "' ";
    		   String edorNo = new ExeSQL().getOneValue(sql1);
    		   if (!edorNo.equals("")) {
    		       CError tError = new CError();
    		       tError.moduleName = "GrpEdorWTDetailBL";
    		       tError.functionName = "checkClaim";
    		       tError.errorMessage = " 保单在做团险万能的其他保全业务，工单号为：" + edorNo + "，不能做"
    		                             + CommonBL.getEdorName(mEdorType, "G");
    		       mErrors.addOneError(tError);
    		       return false;
    		   }            
            }
            if(!checkAddGEdorItem())
            {
            	return false;
            }
            return true;
        }

    /**
     * checkBQ
     * @return boolean
     */
    private boolean checkBQ() {
        //本工单正在做犹豫期退保，不能做其他保全业务
        String sql = "select EdorType "
                     + "from LPGrpEdorItem "
                     + "where grpContNo = '" + this.mGrpContNo + "' "
                     + "   and edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() +
                     "' "
                     + "   and edorType in ('" + BQ.EDORTYPE_WT
                     + "', '" + BQ.EDORTYPE_XT + "', '" + BQ.EDORTYPE_CT +
                     "') ";
        String edorType = new ExeSQL().getOneValue(sql);
        if (edorType != null && !edorType.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "本工单正在做"
                                  + CommonBL.getEdorName(edorType, "G")
                                  + "，不能申请其他保全业务";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验是否有正在发生或结案的理赔
     * @return boolean
     */
    private boolean checkClaimXT() {
        String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
                     mLPEdorItemSchema.getContNo() + "')"
                     + "  and rgtstate not in('11','12','14') with ur  "
                     ;
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单未结案的理陪，不能做协议退保";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验保全生效日期是否大于保单满期日期
     * 处理个单
     * @return boolean
     */
    private boolean checkEdorDate() {
        //得到保单满期日期
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mContNo);
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError("查找保单错误");
            return false;
        }
        String contEndDatet = tLCContDB.getSchema().getCInValiDate();
        String edorDate = mLPEdorItemSchema.getEdorValiDate();
        int i = PubFun.calInterval(edorDate, contEndDatet, "D");
        if (i <= 0) {
            mErrors.addOneError("保全生效日期不能大于保单满期日期");
            return false;
        }
        return true;
    }

    /**
     * 校验是否正在或发生过续期
     * @return boolean：正在或发生过续期false
     */
    private boolean checkDueFeePXT() {
        String sql = " select getNoticeNo from LJSPayB "
                     + "where otherNo = '"
                     + this.mLPEdorItemSchema.getContNo() + "' "
                     + "   and dealState in('"
                     + FeeConst.DEALSTATE_WAITCHARGE + "', '" //待收费
//                     + FeeConst.DEALSTATE_URGESUCCEED + "', '" //催收成功
                     + FeeConst.DEALSTATE_WAITCONFIRM + "') "; //待核销

        String result = new ExeSQL().getOneValue(sql);
        if (result != null && !result.equals("")) {
            CError tError = new CError();
            tError.moduleName = "PEdorXTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在续期续保业务，不能做协议退保"
                                  + result;
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验保单是否有其它保全业务，正在处理。
     * modify by fuxin 2007-12-20
     * 业务部门重新定义规则在做TB中产生的解约工单可以做犹豫期退保。
     * @return boolean
     */
    private boolean checkPBQ() {
        //发生其他工单的保全
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and ContNo = '" + this.mContNo + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM
                     + "' "
                     +" and edortype != 'TB' "
                     ;
        String edorNo = new ExeSQL().getOneValue(sql);
        if (edorNo != null && !edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在做其它保全业务：" + edorNo
                                  + "，不能做协议退保";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
        /*
                //本工单正在做其他保全业务
                sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                      + "where a.edorAcceptNo = b.edorAcceptNo "
                      + "   and a.ContNo = '" + this.mContNo + "' "
         + "   and a.edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' ";
                edorNo = new ExeSQL().getOneValue(sql);
                if (!edorNo.equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "GrpEdorWTDetailBL";
                    tError.functionName = "checkClaim";
                    tError.errorMessage = "本工单下有其它其他保全业务：" + edorNo
                                          + "，不能做犹豫期退保";
                    mErrors.addOneError(tError);
                    return false;
                }

                sql = " select otherNo from LJAGetEndorse "
                      + "where GrpContNo = '"
                      + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                      + "   and getmoney <> 0 ";
                edorNo = new ExeSQL().getOneValue(sql);
                if (!edorNo.equals("")) {
                    mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。" + edorNo);
                    return false;
                }

                sql = " select edorNo from LPGrpEdorItem a, LPEdorApp b "
                      + "where a.edorAcceptNo = b.edorAcceptNo "
                      + "   and a.EdorType in ( 'NI' ,'ZT')"
                      + "   and a.GrpContNo = '"
                      + mLPGrpEdorItemSchema.getGrpContNo() + "' "
         + "   and b.edorState = '" + BQ.EDORSTATE_CONFIRM + "' ";
                edorNo = new ExeSQL().getOneValue(sql);
                if (!edorNo.equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "CheckEdorItem";
                    tError.functionName = "checkBQ";
                    tError.errorMessage = "保单做过对保费有影响的保全业务，不能做犹豫期撤保"
                                          + edorNo;
                    mErrors.addOneError(tError);
                    return false;
                }
         */

    }
    /**
     * 校验是否正在或发生过续期
     * @return boolean：正在或发生过续期true
     */
    private boolean checkDueFeePWT() {
        String sql = " select getNoticeNo from LJSPayB "
                     + "where otherNo = '"+ mLPEdorItemSchema.getContNo() + "' "
                     + " and exists (select 1 from lcpol where contno='014087297000001' and riskcode in " 
                     +		"(select riskcode from lmriskapp where taxoptimal='Y' )" //现在只针对税优产品
                     + " and dealState in('" + FeeConst.DEALSTATE_URGESUCCEED + "') ";  //催收成功

        String result = new ExeSQL().getOneValue(sql);
        if (!result.equals("")) {
            CError tError = new CError();
            tError.moduleName = "PEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单已经做过续期核销，不能做犹豫期退保"
                                  + result;
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private boolean checkContState() {
        String sql = " select 1 from LCContState "
                     + "where contNo = '"
                     + mContNo + "' "
                     + "   and (endDate is null or endDate > '"
                     + PubFun.getCurrentDate() + "') "
                     ;
        String result = new ExeSQL().getOneValue(sql);
        if (result != null && !result.equals("")) {
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkContState";
            tError.errorMessage = "保单已暂停或终止，不能进行犹豫期退保";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验是否正在或发生过理赔(拒赔可以退)
     * @return boolean
     */
    private boolean checkClaimCT() {
        String sql = " select rgtNo from LLRegister a "
                     + "where RgtObjNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                     +
                     "and rgtstate not in ('04','05') and declineflag is null"
                     + "   and not exists "
                     + "      (select 1 from LLClaim "
                     + "       where rgtNo = a.rgtNo and giveType='3') "; //拒赔的可以撤保

        String rgtNo = new ExeSQL().getOneValue(sql);
        if (!rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在做理赔业务或发生过结案的理陪，不能做退保"
                                  + rgtNo;
            mErrors.addOneError(tError);
            return false;
        }

        return true;

    }
    
    /**
     * 团体保全项目校验理赔案件
     * @return
     */
    private boolean checkClaimG() {
    	 String tSQL = "SELECT 1 FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno "
			+ " AND b.rgtstate NOT IN ('11','12','14')"  //11	通知状态   12	给付状态 14	撤件状态
			+ " AND a.grpcontno='" + mLPGrpEdorItemSchema.getGrpContNo() + "'";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正做理赔业务，不能申请保全";
            mErrors.addOneError(tError);
            return false;
		}

        return true;

    }

    /**
     * 校验是否正在或发生过理赔
     * @return boolean
     */
    private boolean checkClaimWT() {
        String sql = " select rgtNo from LLRegister a "
                     + "where RgtObjNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                     +
                     " and rgtstate not in ('04','05') and declineflag is null"
                     + "   and not exists "
                     + "      (select 1 from LLClaim "
                     + "       where rgtNo = a.rgtNo and giveType='3') "; //拒赔的可以撤保

        String rgtNo = new ExeSQL().getOneValue(sql);
        if (!rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在做理赔业务或发生过结案的理陪，不能做犹豫期退保"
                                  + rgtNo;
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验是否正在或发生过续期
     * @return boolean：正在或发生过续期true
     */
    private boolean checkDueFeeWT() {
        String sql = " select getNoticeNo from LJSPayB "
                     + "where otherNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                     + "   and dealState in('"
                     + FeeConst.DEALSTATE_WAITCHARGE + "', '" //待收费
//                     + FeeConst.DEALSTATE_URGESUCCEED + "', '" //催收成功
                     + FeeConst.DEALSTATE_WAITCONFIRM + "') "; //待核销

        String result = new ExeSQL().getOneValue(sql);
        if (!result.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在发生续期续保业务，不能做犹豫期退保"
                                  + result;
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验保单正在发生其他保全业务
     * @return boolean：正在发生其他保全业务true
     */
    private boolean checkBQWT() {
        //发生其他工单的保全
        String sql = "select a.edorNo "
                     + "from LPGrpEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and grpContNo = '" + this.mGrpContNo + "' "
                     + "   and edorNo != '"
                     + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM +
                     "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正在做保全业务：" + edorNo
                                  + "，不能做犹豫期退保";
            mErrors.addOneError(tError);
            return false;
        }

        //本工单正在做其他保全业务
        sql = "select a.edorNo "
              + "from LPGrpEdorItem a, LPEdorApp b "
              + "where a.edorAcceptNo = b.edorAcceptNo "
              + "   and a.grpContNo = '" + this.mGrpContNo + "' "
              + "   and a.edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() +
              "' ";
        edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "本工单正在做其他保全业务：" + edorNo
                                  + "，不能做犹豫期退保";
            mErrors.addOneError(tError);
            return false;
        }

        sql = " select otherNo from LJAGetEndorse "
              + "where GrpContNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + "   and getmoney <> 0 ";
        edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            mErrors.addOneError("保单有过对保费有影响的保全业务，不能申请。" + edorNo);
            return false;
        }

        sql = " select edorNo from LPGrpEdorItem a, LPEdorApp b "
              + "where a.edorAcceptNo = b.edorAcceptNo "
              + "   and a.EdorType in ( 'NI' ,'ZT')"
              + "   and a.GrpContNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + "   and b.edorState = '" + BQ.EDORSTATE_CONFIRM + "' ";
        edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkBQ";
            tError.errorMessage = "保单做过对保费有影响的保全业务，不能做犹豫期撤保"
                                  + edorNo;
            mErrors.addOneError(tError);
            return false;
        }

        sql = " select 1 from LCGrpContState "
              + "where GrpcontNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + "   and (endDate is null or endDate > '"
              + PubFun.getCurrentDate() + "') "
              + "union "
              + "select 1 "
              + "from LCContState "
              + "where grpContNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + "   and (endDate is null or endDate > '"
              + PubFun.getCurrentDate() + "') ";
        String result = new ExeSQL().getOneValue(sql);
        if (!result.equals("")) {
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkBQ";
            tError.errorMessage = "保单已暂停或终止，不能进行犹豫期退保";
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     *
     * 校验团单退保
     * @return boolean
     */
    private boolean checkCTG() {
        if (!checkClaimCT()) {
            return false;
        }
        //校验规则一样
        if (!checkDueFeeWT()) {
            return false;
        }
        //
        if (!checkGrpContState()) {
            return false;
        }
        return true;
    }
    
   private boolean checkTG() {
       if (!checkClaimG()) {
           return false;
       }
       //校验规则一样
       if (!checkDueFeeWT()) {
           return false;
       }
       return true;
   }

    private boolean checkGrpContState() {
        //发生其他工单的保全
        String sql = "select a.edorNo "
                     + "from LPGrpEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and grpContNo = '" + this.mGrpContNo + "' "
                     + "   and edorNo != '"
                     + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM +
                     "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正做保全业务：" + edorNo
                                  + "，不能申请此保全";
            mErrors.addOneError(tError);
            return false;
        }

        //本工单正在做其他保全业务
        sql = "select a.edorNo "
              + "from LPGrpEdorItem a, LPEdorApp b "
              + "where a.edorAcceptNo = b.edorAcceptNo "
              + "   and a.grpContNo = '" + this.mGrpContNo + "' "
              + "   and a.edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() +
              "' ";
        edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "本工单正做其他保全业务：" + edorNo
                                  + "，不能申请此保全";
            mErrors.addOneError(tError);
            return false;
        }

        /*            sql = " select otherNo from LJAGetEndorse "
                          + "where GrpContNo = '"
                          + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                          + "   and getmoney <> 0 ";
                    edorNo = new ExeSQL().getOneValue(sql);
                    if (!edorNo.equals("")) {
         mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。" + edorNo);
                        return false;
                    }

         sql = " select edorNo from LPGrpEdorItem a, LPEdorApp b "
                          + "where a.edorAcceptNo = b.edorAcceptNo "
                          + "   and a.EdorType in ( 'NI' ,'ZT')"
                          + "   and a.GrpContNo = '"
                          + mLPGrpEdorItemSchema.getGrpContNo() + "' "
         + "   and b.edorState = '" + BQ.EDORSTATE_CONFIRM + "' ";
                    edorNo = new ExeSQL().getOneValue(sql);
                    if (!edorNo.equals("")) {
                        CError tError = new CError();
                        tError.moduleName = "CheckEdorItem";
                        tError.functionName = "checkBQ";
                        tError.errorMessage = "保单做过对保费有影响的保全业务，不能做犹豫期撤保"
                                              + edorNo;
                        mErrors.addOneError(tError);
                        return false;
                    }

                sql = " select 1 from lcgrpcontstate "
                             + "where grpcontno='"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                             +" and ((statetype ='Pause' and state ='1') or (statetype ='Terminate' and state ='1')) "
                             + "union "
                             + "select 1 "
                             + "from LCContState "
                             + "where grpContNo = '"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                             + " and ((statetype ='Available' and state ='1') or (statetype ='Terminate' and state ='1')) "
                             ;
         */
        sql = " select 1 from ldcode where codetype='tdbc' and code in (select riskcode from lcgrppol where grpcontno ='"
            + mLPGrpEdorItemSchema.getGrpContNo() + "') "
            ;
        String isTDBC = new ExeSQL().getOneValue(sql);
        if(!isTDBC.equals("")){
        	 return true;
        }
        sql = " select 1 from lcgrpcont where grpcontno ='"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' "
              + " and StateFlag in ('2','3')"
              ;
        String state = new ExeSQL().getOneValue(sql);
        if (!state.equals("")) {
            CError tError = new CError();
            tError.moduleName = "";
            tError.functionName = "";
            tError.errorMessage = "保单已暂停或终止，则不能进行此保全申请";
            mErrors.addOneError(tError);
            return false;
        }

        return true;

    }
    
    private boolean checkGrpBQState() {
        //发生其他工单的保全
        String sql = "select a.edorNo "
                     + "from LPGrpEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and grpContNo = '" + this.mGrpContNo + "' "
                     + "   and edorNo != '"
                     + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM +
                     "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTDetailBL";
            tError.functionName = "checkClaim";
            tError.errorMessage = "保单正做保全业务：" + edorNo
                                  + "，不能申请此保全";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验团单犹豫期退保
     * @return boolean
     */
    private boolean checkWTG() {
        if (!checkClaimWT()) {
            return false;
        }
        if (!checkDueFeeWT()) {
            return false;
        }
        if (!checkBQWT()) {
            return false;
        }

        return true;
    }

    /**
     * 校验是否有险种理赔
     * @return boolean
     */
    private boolean checkClaimed() {
    	//保单有正在处理的理赔案件时不能添加任何保全项目
    	 if (mContType.equals(BQ.CONTTYPE_P)) {
    	 String strSQL = " select 1 from LCInsured a "
             +" where exists (select * from LLCase "
             +" where CustomerNo = a.InsuredNo "
             +" and rgtstate not in ('11', '12', '14')) "
             +" and ContNo = '" +mLPEdorItemSchema.getContNo()+ "' ";
			SSRS tSSRS = new ExeSQL().execSQL(strSQL);
			if (tSSRS.getMaxRow() > 0) {
			  CError tError = new CError();
              tError.moduleName = "CheckEdorItem";
              tError.functionName = "checkClaim";
              tError.errorMessage = "当前保单正处理理赔不能受理任何保全项目！";
              mErrors.addOneError(tError);
			  return false;
			}
    	 }
    	
        if ((mEdorType.equals(BQ.EDORTYPE_WT))
            || (mEdorType.equals(BQ.EDORTYPE_CT))
            || (mEdorType.equals(BQ.EDORTYPE_XT))
            || (mEdorType.equals("ZF"))) {
            GCheckClaimBL bl = new GCheckClaimBL();
            if (mContType.equals(BQ.CONTTYPE_P)) {
            	/*不再单独校验
                if(mEdorType.equals("ZF"))
                {
                   String sqlZF = " select 1 from LCInsured a "
                                +" where exists (select * from LLCase "
                                +" where CustomerNo = a.InsuredNo "
                                +" and rgtstate not in ('11', '12', '14')) "
                                +" and ContNo = '" +mLPEdorItemSchema.getContNo()+ "' ";
                 SSRS tSSRS = new ExeSQL().execSQL(sqlZF);
                 if (tSSRS.getMaxRow() > 0) {
                     mErrors.addOneError("该保单有没有结案的理赔案件。");
                     return false;
                 }
                 return true;
                }
                */
            } else {
                LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                tLCGrpPolDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
                LCGrpPolSet set = tLCGrpPolDB.query();
                for (int i = 1; i <= set.size(); i++) {
                    if (bl.getLCPolClaimed(set.get(i).getGrpPolNo()).size() >
                        0) {
                        CError tError = new CError();
                        tError.moduleName = "CheckEdorItem";
                        tError.functionName = "checkClaimed";
                        tError.errorMessage = "保单正在理赔，不能申请项目";
                        mErrors.addOneError(tError);
                        System.out.println(tError.errorMessage);
                        return false;
                    }
                }
            }

            return true;
        }

        if (!mEdorType.equals(BQ.EDORTYPE_LR)) {
            return true;
        }
        String rgtNo;
        if (mContType.equals(BQ.CONTTYPE_P)) {
        	/*不再单独校验
            String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
                         mLPEdorItemSchema.getContNo() + "')"
                         + " and endcasedate is null and rgtstate<>'14' "
                         ;
            rgtNo = new ExeSQL().getOneValue(sql);
            if (rgtNo != null && !rgtNo.equals("")) {
                CError tError = new CError();
                tError.moduleName = "GrpEdorWTDetailBL";
                tError.functionName = "checkClaim";
                tError.errorMessage = "保单未结案的理陪，不能做遗失补发";
                mErrors.addOneError(tError);
                return false;
            }
            */
        } else {
            String sql = " select rgtNo from LLRegister a "
                         + "where RgtObjNo = '"
                         + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                         +
                         " and rgtstate not in ('04','05') and declineflag is null"
                         ;
            rgtNo = new ExeSQL().getOneValue(sql);
            if (rgtNo != null && !rgtNo.equals("")) {
                CError tError = new CError();
                tError.moduleName = "GrpEdorWTDetailBL";
                tError.functionName = "checkClaim";
                tError.errorMessage = "保单未结案的理陪，不能做遗失补发";
                mErrors.addOneError(tError);
                return false;
            }
        }

        /*
                LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
                if (mContType.equals(BQ.CONTTYPE_P))
                {
                    tLLClaimPolicyDB.setContNo(mContNo);
                }
                else
                {
                    tLLClaimPolicyDB.setGrpContNo(mGrpContNo);
                }
                if (tLLClaimPolicyDB.query().size() > 0)
                {
                    mErrors.addOneError("该保单发生过理赔！");
                    return false;
                }
         */
        return true;
    }

    /**
     * 得到保单管理机构
     * @return String
     */
    private String getManageCom() {
        if (mContType.equals(BQ.CONTTYPE_G)) {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLCGrpContDB.getInfo()) {
                return "";
            }
            return tLCGrpContDB.getManageCom();
        } else {
//            LCContDB tLCContDB = new LCContDB();
//            tLCContDB.setContNo(mContNo);
//            if (!tLCContDB.getInfo()) {
//                return "";
//            }
//            return tLCContDB.getManageCom();
            String tManageCom = "";
            if(!mLPEdorItemSchema.getEdorType().equals("RB"))
            {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mContNo);
                if (!tLCContDB.getInfo()) {
                    return "";
                }
                return tLCContDB.getManageCom();
            }else{
                String strCom=" select ManageCom from lccont where contno ='"+mLPEdorItemSchema.getContNo()+"' "
                             +" union "
                             +" select ManageCom from lbcont where contno ='"+mLPEdorItemSchema.getContNo()+"'"
                             ;
               tManageCom = new ExeSQL().getOneValue(strCom);
            }
            return tManageCom ;
        }
    }

    /**
     * 机构判断
     * @param needCom String
     * @param currentCom String
     * @return boolean
     */
    private boolean compareCom(String needCom, String currCom) {
        if ((currCom != null) && (currCom.equals(BQ.COMCODE_HEAD))) {
            return true;
        }
        if ((currCom.length() >= 4) && (needCom.length() >= 4)) {
        	//6位机构可以受理三级机构的保单--2015-1-26
        	if(currCom.length() == 6){
        		String subCom = needCom.substring(0, 6);
        		if(currCom.equals(subCom)){
        			return true;
        		}
        		return false;
        	}
        	
            //如果是三级机构，只能受理三级机构的保单。modify by fuxin 2008-9-1
           if(!"0000".equals(currCom.substring(4,currCom.length())) && currCom.length() > 4)
           {
               String needSubCom = needCom.substring(0, 8);
               String currSubCom = currCom.substring(0, 8);
               if (needSubCom.equals(currSubCom))
               {
                   return true ;
               }
           }
           else{
               String needSubCom = needCom.substring(0, 4);
               String currSubCom = currCom.substring(0, 4);
               if (needSubCom.equals(currSubCom)) {
                   return true;
               }
               return false ;
           }
        }
        return false;
    }

    /**
     * 判断当前机构是不是管理机构
     * @return boolean
     */
    private boolean checkManageCom() {
        String manageCom = getManageCom();
        if (!compareCom(manageCom, mManageCom)) {
            mErrors.addOneError("当前机构不是保单管理机构，没有操作权限！ 或者保单已经终止，只能做保全回退项目！");
            return false;
        }
        return true;
    }

    /**
     * 校验保单是否有效
     * @return boolean
     */
    private boolean checkCont() {
        if (mContType.equals(BQ.CONTTYPE_G)) {
            return checkLCGrpCont();
        } else if (mContType.equals(BQ.CONTTYPE_P)) {
            return checkLCCont();
        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean checkFX() {
        LCContStateDB tLCContStateDB = new LCContStateDB();
        tLCContStateDB.setContNo(this.mContNo);
        tLCContStateDB.setState("1");
        LCContStateSet set = tLCContStateDB.query();
        if (set == null || set.size() < 1) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean checkTF() {
        LCGrpContStateDB tLCGrpContStateDB = new LCGrpContStateDB();
        tLCGrpContStateDB.setGrpContNo(this.mGrpContNo);
        LCGrpContStateSet set = tLCGrpContStateDB.query();
        if (set == null || set.size() < 1) {
            return false;
        }
        return true;
    }

    /**
     * 校验团单是否有效
     * @return boolean
     */
    private boolean checkLCGrpCont() {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError("没有查询到保单信息");
            return false;
        }

        return checkDateValidate(tLCGrpContDB.getCInValiDate());
    }

    /**
     * 校验个单是否有效
     * @return boolean
     */
    private boolean checkLCCont() {
        String sql = "  select max(endDate) "
                     + "from LCPol "
                     + "where contNo = '" + mContNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String maxEndDate = tExeSQL.getOneValue(sql);

        return checkDateValidate(maxEndDate);
    }

    /**
     * 校验保单是否有效
     * @param date String
     * @return boolean, true: 有效, false: 终止
     */
    private boolean checkDateValidate(String date) {
        StringBuffer sql = new StringBuffer();
        sql.append("select * from Dual where days(current date) - days('")
                .append(date)
                .append("') < 0 ");
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(sql.toString());
        if (result.equals("")) {
            mErrors.addOneError("保单已终止，不能再做保全。");
            return false;
        }

        return true;
    }

    /**
     * 校验团单是否在进行保费转出
     * qulq create at 06-10-08
     * @param contNo String 团单号码
     * @return boolean 返回 true 是正在转出
     */
    private boolean checkGrpContBack(String contNo) {
        if (CommonBL.checkGrpBack(contNo)) {
            this.mErrors.addOneError("保单正在做实收保费转出,不能申请保全。");
            return true;
        }
        return false;
    }

    /**
     * 校验是否在进行保费转出
     * qulq create at 06-10-08
     *
     * @return boolean 返回 true 是正在转出
     */
    private boolean checkBack() {
        if (mContType.equals(BQ.CONTTYPE_G)) {
            return checkGrpContBack(this.mGrpContNo);
        }
        if (mContType.equals(BQ.CONTTYPE_P)) {
            return checkIndiContBack(mContNo);
        }

        return false;
    }

    /**
     * 校验团单是否在进行保费转出
     * @param contNo String 个单号码
     * @return boolean 返回 true 是正在转出
     */
    private boolean checkIndiContBack(String contNo) {
        if (CommonBL.checkIndiBack(contNo)) {
            this.mErrors.addOneError("保单正在做实收保费转出,不能申请保全。");
            return true;
        }
        return false;
    }

    // 控制存在待结算金额的保单,不能进行FP操作. added by huxl @ 2006-12-5
    private boolean checkFPG() {
        String strSQL = "select b.EdorAcceptNo from LJAPay a,LPGrpEdorItem b where a.IncomeNo = b.EdorAcceptNo and b.GrpContNo = '"
                        + this.mGrpContNo + "' and a.IncomeType = 'B' "
                        + " union "
                        + " select b.EdorAcceptNo from LJAGet a,LPGrpEdorItem b where a.OtherNo = b.EdorAcceptNo and b.GrpContNo = '"
                        + this.mGrpContNo + "' and a.OtherNoType = 'B' ";
        SSRS tSSRS = new ExeSQL().execSQL(strSQL);
        if (tSSRS.getMaxRow() > 0) {
            this.mErrors.addOneError("保单" + this.mGrpContNo +
                                     "存在待结算金额,不能进行服务批次变更!");
            return false;
        }
        return true;
    }

    private boolean checkBQState()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String sql =" select a.edorstate From lpedorapp a, lpedoritem b where contno ='"+mLPEdorItemSchema.getContNo()+"' "
                   +" and a.edoracceptno = b.edoracceptno "
                   ;
        String edorState = tExeSQL.getOneValue(sql);
        if("9".equals(mLPEdorItemSchema.getEdorState()))
        {
            this.mErrors.addOneError("保单" + mLPEdorItemSchema.getContNo() +
                                     "存在待结算金额,不能进行服务批次变更!");
            return false;

        }
        return true ;
    }

    //20080623 zhanggm 团体保全只能单独做的保全项目，添加完该项目后不能再添加其它项目。
    private boolean checkAddGEdorItem()
    {
        String sql = "select EdorType from LPGrpEdorItem "
                     + "where EdorNo = '" + mLPGrpEdorItemSchema.getEdorAcceptNo() + "' "
                     + "and EdorType in('LR', 'CT', 'WT', 'XT', 'GA','LQ','ZB','TF','TG') with ur";
        String edorType = new ExeSQL().getOneValue(sql);
        if (!edorType.equals("")) {
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkAddGEdorItem";
            tError.errorMessage = edorType + "项目不能和其它项目一起做！";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    private boolean checkHangUpState()
    {
        String SQL = "select 'X' From LCContHangUpState where ContNo ='"+mLPEdorItemSchema.getContNo()+"' and State ='"+BQ.HANGUSTATE_ON+"'";
        String CountX = new ExeSQL().getOneValue(SQL);
        if ((mEdorType.equals("AE")||mEdorType.equals("CT")
            ||mEdorType.equals("WT")||mEdorType.equals("XT")
            ||mEdorType.equals("PR")||mEdorType.equals("BC")
            ||mEdorType.equals("LR")||mEdorType.equals("LQ")
            ||mEdorType.equals("FC")) && CountX !="")
        {
        	SQL = "select 1 from loloan a where polno in (select polno from lcpol where contno='"+mContNo+"') and payoffflag='0' with ur";
        	CountX = new ExeSQL().getOneValue(SQL);
        	if(mEdorType.equals("CT")&&CountX !=""){
        		System.out.println("有贷款保全操作的保单允许申请解约");
        		return true;
        	}        	
            CError tError = new CError();
            tError.moduleName = "CheckEdorItem";
            tError.functionName = "checkAddGEdorItem";
            tError.errorMessage = "保单正处于保单挂起状态，不能做"+mEdorType+"项目！";
            mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    private boolean chekHangUpOn()
    {
        if (mEdorType.equals("GF"))
        {
            String SQL = "select 'X' From LCContHangUpState where ContNo ='"+mLPEdorItemSchema.getContNo()+"' and State ='"+BQ.HANGUSTATE_ON+"'";
            String CountX = new ExeSQL().getOneValue(SQL);
            if (CountX.equals("")||CountX == null)
            {
                CError tError = new CError();
                tError.moduleName = "CheckEdorItem";
                tError.functionName = "checkAddGEdorItem";
                tError.errorMessage = "不处于挂起的保单，不能做" + mEdorType + "项目！";
                mErrors.addOneError(tError);
                return false;
            }
        }
        return true ;
    }
    
    //如果保单处在满期抽档后，满期给付确认前，允许改动以下保全项目：AD，CM，LR
    private boolean checkExpirBenefit()
    {
    	if(mContType.equals(BQ.CONTTYPE_P))
    	{
    		try {
				String tEdorType = mLPEdorItemSchema.getEdorType();
				if(tEdorType.equals("AD")||tEdorType.equals("CM")||tEdorType.equals("LR"))
				{
					return true;
				}
				String tContNo = mLPEdorItemSchema.getContNo();
				String sql = "select sum(GetMoney) from LJSGetDraw a where ContNo = '" + tContNo + "' and feefinatype != 'YEI' "
				    + "and exists (select 1 from LJSGet where a.getnoticeno = getnoticeno and othernotype ='20' and paymode <> '5' ) "
				    + "and not exists (select 1 from ljaget where actugetno = a.getnoticeno) ";
				String tGetMoney = new ExeSQL().getOneValue(sql);
				if(!tGetMoney.equals("") && !tGetMoney.equals("null") && !tGetMoney.equals("0"))
				{
				    CError tError = new CError();
				    tError.moduleName = "CheckEdorItem";
				    tError.functionName = "checkExpirBenefit";
				    tError.errorMessage = "该保单已经给付抽档，不能添加该保全项目！";
				    mErrors.addOneError(tError);
				    return false;
				}
			} catch (RuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
            
        return true ;
    }

    public static void main(String[] args) {
        CheckEdorItem tCheckEdorItem = new CheckEdorItem();
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("20070522000262");

        tCheckEdorItem.setLPGrpEdorItem(tLPGrpEdorItemSchema);

        tCheckEdorItem.setManageCom("86");
        tCheckEdorItem.setContType(BQ.CONTTYPE_P);
        tCheckEdorItem.setGrpContNo("");
        tCheckEdorItem.setContNo("0000183302");
        tCheckEdorItem.setEdorType(BQ.EDORTYPE_FX);
        if (!tCheckEdorItem.submitData()) {
            System.out.println(tCheckEdorItem.mErrors.getFirstError());
        }
    }
}
