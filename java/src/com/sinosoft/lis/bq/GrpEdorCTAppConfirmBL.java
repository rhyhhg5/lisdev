package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 对保全明细录入的数据进行预算，得到客户交退费信息</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version
 */

public class GrpEdorCTAppConfirmBL implements EdorAppConfirm
{
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //保全项目信息
    private LJSGetEndorseSchema mLJSGetEndorseSchema = null;
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();  //保全部退费信息
    private LPContPlanRiskSet mLPContPlanRiskSet = null;
    private GlobalInput mGI = null;  //完整的操作员信息
    private BqCalBL mBqCalBL = new BqCalBL();  //生成财务数据的类
    private String mFeeFinaType = null;  //退费类型
    private String mFee =null;
    private String mCurDate = null;//当前日期
    private String mCurTime = null;//当前时间
    
    private boolean isULI=false;

    private MMap map = new MMap();

    public GrpEdorCTAppConfirmBL()
    {
    }

    /**
     * 操作的提交方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象，包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("qulq ctappconfirm");
        //得到外部传入的数据
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到处理后的数据集
     * @return VData：处理后的数据集，失败为null
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) cInputData.
                               getObjectByObjectName("GlobalInput", 0));
        if (mLPGrpEdorItemSchema == null || mGI == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            System.out.println(tLPGrpEdorItemDB.mErrors);
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));
        
        jugdeUliByGrpContNo();//这边判断该团单是团体万能保单还是其他保单 如果isULI为true则为团体万能保单
        if(!isULI){
        	//查找保障计划
        	
        	LPContPlanRiskDB tLPContPlanRiskDB = new LPContPlanRiskDB();
        	tLPContPlanRiskDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        	tLPContPlanRiskDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        	mLPContPlanRiskSet = tLPContPlanRiskDB.query();
        	if (mLPContPlanRiskSet == null || mLPContPlanRiskSet.size() <= 0)
        	{
        		mErrors.addOneError(new CError("查询保全项目保障计划信息失败！"));
        		System.out.println(mLPContPlanRiskSet.mErrors);
        		return false;
        	}
        }

        return true;
    }

    /**
     * 处理团单退保理算业务逻辑，对保全明细录入的数据进行预算，得到客户交退费信息：
     i.	按险种生成险种缴费信息LJSGetEndorse，若险种有加费，则同时生成加费的退费信息LJSGetEndore，为销售提数作准备。
     ii.生成工本费财务信息。
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
  {
      if(!checkData())
      {
          return false;
      }

      if(!setFee())
      {
          return false;
      }

      map.put(mLJSGetEndorseSet, "DELETE&INSERT");

      return true;
    }
    /**
     * -
     * @return boolean
     */
   private boolean setFee()
   {
       mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
       mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
       mLPGrpEdorItemSchema.setEdorState("2");
       mLPGrpEdorItemSchema.setUWFlag("0");
       map.put(mLPGrpEdorItemSchema, "UPDATE");
       String wherePart = "where EdorNo='" +
                          mLPGrpEdorItemSchema.getEdorAcceptNo() + "' and EdorType='" +
                          mLPGrpEdorItemSchema.getEdorType() + "'";
       String updateSql =
               "update LPGrpEdorItem set ChgPrem= nvl((select sum(ChgPrem) from LPEdorItem "
               + wherePart + "),0), "
               + "ChgAmnt= nvl((select sum(ChgAmnt) from LPEdorItem "
               + wherePart + "),0), "
               +
               "GetMoney= nvl((select sum(GetMoney) from LPEdorItem "
               + wherePart + "),0), "
               +
               "GetInterest= nvl((select sum(GetInterest) from LPEdorItem "
               + wherePart + "),0) "
               + wherePart;
       System.out.println(updateSql);
       map.put(updateSql, "UPDATE");
       if(mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_CT))
       {
           EdorItemSpecialData tEdorItemSpecialData =
                   new EdorItemSpecialData(mLPGrpEdorItemSchema.getEdorAcceptNo(),
                  mLPGrpEdorItemSchema.getEdorAcceptNo(),
                  mLPGrpEdorItemSchema.getEdorType() );
           if(tEdorItemSpecialData.query())
           {
               if(getEdorValue("RATEMONEY",tEdorItemSpecialData.getSpecialDataSet())!=null)
               {
                   String rateMoney = getEdorValue("RATEMONEY",
                       tEdorItemSpecialData.getSpecialDataSet());

                   //定额利息加入
                   String Sql =
                       "update LPGrpEdorItem set getmoney = getmoney - double('"
                       + rateMoney + "') " + wherePart;
                  map.put(Sql, "UPDATE");

                  LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                  tLCGrpPolDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
                  LCGrpPolSet set = tLCGrpPolDB.query();

                  LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
                  new Reflections().transFields(tLPGrpPolSchema, set.get(1));
                  tLPGrpPolSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                  tLPGrpPolSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());

                  LJSGetEndorseSchema schema
                      = new BqCalBL().initLJSGetEndorse(mLPGrpEdorItemSchema,
                      set.get(1), mLPGrpEdorItemSchema.getEdorType(),
                      "LX", -Math.abs(Double.parseDouble(rateMoney)), mGI);
                  schema.setContNo(BQ.FILLDATA);
                  schema.setGrpPolNo(BQ.FILLDATA);
                  schema.setPolNo(BQ.FILLDATA);
//                  schema.setRiskCode(BQ.FILLDATA);
                  schema.setPayPlanCode(BQ.FILLDATA);
                  schema.setMakeDate(PubFun.getCurrentDate());
                  schema.setMakeTime(PubFun.getCurrentTime());
                  PubFun.fillDefaultField(schema);

                  map.put(schema, SysConst.DELETE_AND_INSERT);

              }
           }

       }

       return true;
   }
    /**
     * 校验是否可以理算
     * @return boolean：成功true，否则false
     */
     private boolean checkData()
     {
        if(!mLPGrpEdorItemSchema.getEdorState().equals("1"))
        {
            this.mErrors.addOneError("当前状态无法进行理算!");
            return false;
        }
        return true;
	}
      private String getEdorValue(String detailType,LPEdorEspecialDataSet mSpecialDataSet)
      {
            for (int i = 1; i <= mSpecialDataSet.size(); i++)
            {
                LPEdorEspecialDataSchema tEspecialSchema = mSpecialDataSet.get(i);
                String dt = tEspecialSchema.getDetailType();
                if ((dt != null) && (dt.equalsIgnoreCase(detailType)))
                {
                    return tEspecialSchema.getEdorValue();
                }
            }
            return null;
      }
      
      //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
      public boolean jugdeUliByGrpContNo()
      {
          StringBuffer tSBql2 = new StringBuffer(256);
          tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
          tSBql2.append(this.mLPGrpEdorItemSchema.getGrpContNo());
          tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
          ExeSQL tExeSQL5 = new ExeSQL();
          SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
          if (tSSRS5.getMaxRow() < 1)
          {
          	System.out.println("此团单不是团体万能");
              return false;
          }
          System.out.println("此团单是团体万能");
          isULI=true;
          return true;
      }



    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo("20061107000006");
        tLPGrpEdorItemDB.setEdorType("CT");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemDB.query().get(1));

        GrpEdorCTAppConfirmBL tGrpEdorCTAppConfirmBL = new
                GrpEdorCTAppConfirmBL();
        if (!tGrpEdorCTAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorCTAppConfirmBL.mErrors.getErrContent());
        }

    }
}
