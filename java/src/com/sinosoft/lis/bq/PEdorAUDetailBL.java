package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPersonSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户信息授权</p>
 * <p>Copyright: Copyright (c) 2200</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite liuyang
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class PEdorAUDetailBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private MMap mMap = new MMap();

	private DetailDataQuery mQuery = null;

	private GlobalInput mGlobalInput = null;

	private String mTypeFlag = null;

	private String mGUFlag = null;

	private String stateFlag = null;

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mContNo = null;

	private String mCustomerNo = null;

	private String mName = null;

	private String mSex = null;

	private String mBirthday = null;

	private String mIdType = null;

	private String mIdNo = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String mPolno = null;

	private String mAuthorization = null;// 客户信息授权标识

	/**
	 * 提交数据
	 * @param cInputData VData
	 * @return boolean
	 */
	public boolean submitData(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到传入数据
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mTypeFlag = (String) cInputData.get(0);
			mGUFlag = (String) cInputData.get(1);
			TransferData mTransferData = new TransferData();
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			if (mTransferData != null) {
				stateFlag = (String) mTransferData.getValueByName("stateFlag");
			}
			mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0);
			LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema) cInputData
					.getObjectByObjectName("LPEdorItemSchema", 0);
			LDPersonSchema tLDPersonSchema = (LDPersonSchema) cInputData
					.getObjectByObjectName("LDPersonSchema", 0);

			this.mEdorNo = tLPEdorItemSchema.getEdorNo();
			this.mEdorType = tLPEdorItemSchema.getEdorType();
			this.mContNo = tLPEdorItemSchema.getContNo();
			this.mCustomerNo = tLDPersonSchema.getCustomerNo();
			this.mName = tLDPersonSchema.getName();
			this.mSex = tLDPersonSchema.getSex();
			this.mBirthday = tLDPersonSchema.getBirthday();
			this.mIdType = tLDPersonSchema.getIDType();
			this.mIdNo = tLDPersonSchema.getIDNo();
			this.mAuthorization = tLDPersonSchema.getAuthorization();

			this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 校验数据
	 * @return boolean
	 */
	private boolean checkData() {
		if ((mTypeFlag == null) || (mTypeFlag.equals(""))) {
			mErrors.addOneError("缺少个/团单处理标志！");
			return false;
		}
		if ((mCustomerNo == null) || (mCustomerNo.equals(""))) {
			mErrors.addOneError("客户号为空！");
			return false;
		}
		return true;
	}

	/**
	 * 处理业务数据
	 * @return boolean
	 */
	private boolean dealData() {
		// 个单和团单分开操作，个单按投保人操作，团单按被保人操作
		if (mTypeFlag.equals("G")) {
			return false;
		} else {
			if (!setLPPerson()) {
				mErrors.addOneError("客户信息缺失！");
				return false;
			}
		}
		setEdorState(BQ.EDORSTATE_INPUT);
		System.out.println("**********************************************");
		System.out.println("**********************************************");
		return true;
	}

	/**
	 * 设置LPPerson表
	 */
	private boolean setLPPerson() {
		if (mName == null && mName.equals("")) {

			mErrors.addOneError("客户姓名信息缺失");
			return false;
		}
		LPPersonSchema tLPPersonSchema = (LPPersonSchema) mQuery.getDetailData(
				"LDPerson", mCustomerNo);
		tLPPersonSchema.setName(mName);
		tLPPersonSchema.setSex(mSex);
		tLPPersonSchema.setBirthday(mBirthday);
		tLPPersonSchema.setIDType(mIdType);
		tLPPersonSchema.setIDNo(mIdNo);
		tLPPersonSchema.setAuthorization(mAuthorization);
		tLPPersonSchema.setOperator(mGlobalInput.Operator);
		tLPPersonSchema.setModifyDate(mCurrentDate);
		tLPPersonSchema.setModifyTime(mCurrentTime);

		/*
		 * 添加字段
		 */
		mMap.put(tLPPersonSchema, "DELETE&INSERT");

		return true;
	}

	/**
	 * 把保全状态设为已录入
	 * @param edorState String
	 */
	private void setEdorState(String edorState) {
		String w = "";
		if (!mTypeFlag.equals("G")) {
			if ((mPolno != null) && (!mPolno.equals(""))) {
				w = "    polno='" + mPolno + "', ";
			}
		} else {
			w = " ";
		}

		String sql = "update  LPEdorItem " + "set EdorState = '" + edorState
				+ "', " + "    Operator = '" + mGlobalInput.Operator + "', "
				+ w + "    ModifyDate = '" + mCurrentDate + "', "
				+ "    ModifyTime = '" + mCurrentTime + "' " +

				"where  EdorNo = '" + mEdorNo + "' " + "and EdorType = '"
				+ mEdorType + "' " + "and InsuredNo = '" + mCustomerNo + "'";
		mMap.put(sql, "UPDATE");
	}

	/**
	 * 提交数据到数据库
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
}
