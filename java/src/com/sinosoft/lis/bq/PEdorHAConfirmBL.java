package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 分红险红利领取方式变更保全确认处理类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Lanjun
 * @version 1.0
 */
public class PEdorHAConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

    /**  */

    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ValidateEdorData2 mValidateEdorData = null;

    public PEdorHAConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据操作
        if (!dealData())
        {
            return false;
        }
        //修改保单状态
        if(!dealContState()){
        	return false;
        }

        //输出数据准备
        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**数据查询业务处理(queryData())
     *
     */


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mLPEdorItemSchema.getEdorNo(),mLPEdorItemSchema.getEdorType(), mLPEdorItemSchema.getContNo(), "ContNo");
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
    	
        String[] addTables = {"LCInsureAccTrace"};
        mValidateEdorData.addData1(addTables);
        String[] chgTables = {"LCPol","LCGet","LCInsureAcc","LCInsureAccClass"};
        mValidateEdorData.changeData(chgTables);
        mMap.add(mValidateEdorData.getMap());
        //满期处理完成
    	String polState=" update lcpol set polstate='03060002',modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"'"
			+"where contno='"+mLPEdorItemSchema.getContNo()+"'";
    	mMap.put(polState, SysConst.UPDATE);	
        
        return true;
    }
    
    private boolean dealContState(){
    	String sql ="select b.* from lccont a,lcpol b where a.contno=b.contno and a.contno='"+mLPEdorItemSchema.getContNo()+"' and b.stateflag<>'3' with ur";
		LCPolSet getLCPolSet=new LCPolDB().executeQuery(sql);
    	if(getLCPolSet.size()>0){
    		//处理险种满期
    		for(int i=1;i<=getLCPolSet.size();i++){
    			LCPolSchema tLCPolSchema=getLCPolSet.get(i).getSchema();
                /*设置险种失效*/
                setPolAbate(tLCPolSchema, "Terminate", "1", "01");
    		}
    		//处理保单满期
        	String sql2 ="select * from lccont where  contno='"+mLPEdorItemSchema.getContNo()+"' and stateflag<>'3' with ur";
    		LCContSet getLCContSet=new LCContDB().executeQuery(sql2);
        	if(getLCContSet.size()>0){
        			LCContSchema tLCContSchema=getLCContSet.get(1).getSchema();
                    /*设置险种失效*/
        			setPolicyAbate(tLCContSchema, tLCContSchema.getCInValiDate(), "Terminate", "1", "01");
        	}
    	}
    	return true;
    }
    
    
    private boolean setPolAbate(LCPolSchema tLCPolSchema,String tStateType,String tState, String tStateReason){

        LCContStateSchema tLCContStateSchema = new LCContStateSchema();

        tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
        tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());
        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCContStateSchema.setStateType("Terminate");
        tLCContStateSchema.setOtherNoType("");
        tLCContStateSchema.setOtherNo("");
        tLCContStateSchema.setState("1");
        tLCContStateSchema.setStateReason(tStateReason);
        
        tLCContStateSchema.setStartDate(tLCPolSchema.getEndDate());
        tLCContStateSchema.setEndDate("");
        tLCContStateSchema.setRemark("");
        tLCContStateSchema.setOperator(mGlobalInput.Operator);
        tLCContStateSchema.setMakeDate(CurrentDate);
        tLCContStateSchema.setMakeTime(CurrentTime);
        tLCContStateSchema.setModifyDate(CurrentDate);
        tLCContStateSchema.setModifyTime(CurrentTime);
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
//        SetPolState(tLCPolSchema, "2");
        String updatepol = "update LCPol Set StateFlag = '"+BQ.STATE_FLAG_TERMINATE+ "',ModifyDate = '" + CurrentDate
            + "',ModifyTime = '" + CurrentTime +"' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        mMap.put(updatepol, "UPDATE");
        return true;

    }
    private boolean setPolicyAbate(LCContSchema tLCContSchema,String tEndDate,String tStateType,String tState, String tStateReason){
    	String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);
    	
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();
        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(tLCContSchema.getContNo());
        tLCContStateSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        tLCContStateSchema.setPolNo("000000");
        tLCContStateSchema.setStateType(tStateType);
        tLCContStateSchema.setStateReason(tStateReason);
        
        tLCContStateSchema.setState(tState);
        tLCContStateSchema.setOperator(mGlobalInput.Operator);
        tLCContStateSchema.setStartDate(tStartDate);
        tLCContStateSchema.setMakeDate(CurrentDate);
        tLCContStateSchema.setMakeTime(CurrentTime);
        tLCContStateSchema.setModifyDate(CurrentDate);
        tLCContStateSchema.setModifyTime(CurrentTime);
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
        
        String updatecont = "update LCCont Set StateFlag = '"+BQ.STATE_FLAG_TERMINATE+ "',ModifyDate = '" 
        + CurrentDate+ "',ModifyTime = '" + CurrentTime +"' where Contno = '" + tLCContSchema.getContNo() + "'";
        mMap.put(updatecont, "UPDATE");
        
        return true;
    }

    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
}
