package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class LCAppAccGetBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 业务处理相关变量 */
    private LCAppAccSchema mLCAppAccSchema = new LCAppAccSchema();
    private LCAppAccGetTraceSchema mLCAppAccGetTraceSchema = new LCAppAccGetTraceSchema();
    private String mNoticeNo;
    private GlobalInput mGlobalInput = new GlobalInput();
    // @Constructor
    public LCAppAccGetBL()
    {}
    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
            return false;

        if (this.checkData() == false)
            return false;

        if (this.dealData() == false)
            return false;

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");
        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start GrpEdorItemBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
           //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.errorMessage = "一分钟内不能连续操作两次。";                     
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("---commitData---");

        return true;
    }
    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //投保人帐户领取轨迹表
        mLCAppAccGetTraceSchema.setSchema((LCAppAccGetTraceSchema) mInputData.
                                   getObjectByObjectName("LCAppAccGetTraceSchema", 0));


        if (mLCAppAccGetTraceSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppAccGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到投保人帐户领取轨迹表!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData()
    {
        return true;
    }


    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {
//    	20110322 YangTianZheng 防止并发加锁
    	MMap tCekMap1 = null;
    	tCekMap1 = lockLJAGet(mLCAppAccGetTraceSchema);
        if (tCekMap1 == null)
        {
            return false;
        }
        map.add(tCekMap1);
//--------------------------
        
        mNoticeNo=createGetNoticeNo();
        setLJAGet();

        AppAcc tAppAcc=new AppAcc();
        LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCAppAccGetTraceSchema.getCustomerNo());

        tLCAppAccTraceSchema.setAccType("0");
        tLCAppAccTraceSchema.setOtherNo(mNoticeNo);
        if(this.mOperate.equals("0"))
        {
            tLCAppAccTraceSchema.setOtherType("6");
        }else
        {
            tLCAppAccTraceSchema.setOtherType("8");
        }
        tLCAppAccTraceSchema.setDestSource("04");
        tLCAppAccTraceSchema.setMoney(mLCAppAccGetTraceSchema.getAccGetMoney()*-1);
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);

        mLCAppAccGetTraceSchema.setNoticeNo(mNoticeNo);
        MMap tMap=tAppAcc.accTakeOutLQ(tLCAppAccTraceSchema,mLCAppAccGetTraceSchema);
        if(tMap==null)
        {
            this.mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        
        map.add(tMap);
        
        return true;
    }

    /**
     * 产生退费记录号，如果原来LJAGet表中没有数据时才产生新的号
     * @return String
     */
    private String createGetNoticeNo() {
    //    LJAGetDB tLJAGetDB = new LJAGetDB();
    //    tLJAGetDB.setOtherNo(mLCAppAccGetTraceSchema.getCustomerNo());
    //    LJAGetSet tLJAGetSet = tLJAGetDB.query();
    //    if (tLJAGetSet.size() > 0) {
    //        return tLJAGetSet.get(1).getActuGetNo();
    //    } else {
            return PubFun1.CreateMaxNo("GETNO", null);
    //    }
    }

    /**
     * 设置实付表
     * @return void
     */
    private void setLJAGet() {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo(mNoticeNo);
        tLJAGetSchema.setOtherNo(mLCAppAccGetTraceSchema.getCustomerNo());
        tLJAGetSchema.setOtherNoType("12");
        tLJAGetSchema.setPayMode(mLCAppAccGetTraceSchema.getAccGetMode());
        
        String com = getManageCom();
        
        tLJAGetSchema.setManageCom(com);
        tLJAGetSchema.setAccName(mLCAppAccGetTraceSchema.getAccName());
        tLJAGetSchema.setAppntNo(mLCAppAccGetTraceSchema.getCustomerNo());
        tLJAGetSchema.setSumGetMoney(Math.abs(mLCAppAccGetTraceSchema.getAccGetMoney()));
        tLJAGetSchema.setShouldDate(mLCAppAccGetTraceSchema.getTransferDate());
        tLJAGetSchema.setBankCode(mLCAppAccGetTraceSchema.getBankCode());
        tLJAGetSchema.setBankAccNo(mLCAppAccGetTraceSchema.getBankAccNo());
        tLJAGetSchema.setOperator(mGlobalInput.Operator);
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJAGetSchema, "DELETE&INSERT");
    }
    
    /**
     * 获取应付表的管理机构，
     * 1、先取登录机构下保单的对应机构。
     * 2、如果登录机构没有保单，再取C表第一个保单的机构。
     * @return String
     */
    
    private String getManageCom()
    {
    	String loginCom = mGlobalInput.ManageCom;
    	String com = null;
    	StringBuffer sql = new StringBuffer();
    	
    	sql.append("select 1 d , ManageCom from LCCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("and ManageCom = '"+loginCom +"' ")
    	.append("union select 2 d , ManageCom from LBCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("and ManageCom = '"+loginCom +"' ")
    	.append("union select 3 d , ManageCom from LCCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("and ManageCom like '"+loginCom+"%' ")
    	.append("union select 4 d , ManageCom from LBCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("and ManageCom like '"+loginCom+"%' ")
    	.append("union select 5 d , ManageCom from LCCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("union select 6 d , ManageCom from LBCont where AppntNo ='"+mLCAppAccGetTraceSchema.getCustomerNo()+"' ")
    	.append("order by d fetch first 1 row only with ur ");
    	
    	System.out.println(sql.toString());

    	SSRS temp = new ExeSQL().execSQL(sql.toString());

        if(temp == null ||temp.getMaxRow()==0)
        {
        	com = "00000000";
        }
        else
        {
        	com = temp.GetText(1,2);
        }
    	
    	return com;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mResult.clear();
        mInputData.clear();
        mInputData.add(map);

    }


    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult()
    {
        return mResult;
    }
    
    private MMap lockLJAGet(LCAppAccGetTraceSchema mLCAppAccGetTraceSchema)
    {
    	
    	
        MMap tMMap = null;
        /**锁定标志"AL"*/
        String tLockNoType = "AL";
        /**锁定时间*/
        String tAIS = "60";
        System.out.println("设置了60秒的解锁时间");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", mLCAppAccGetTraceSchema.getCustomerNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
       
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        
   
        if (tMMap == null)
        {           	
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
    
    
}
