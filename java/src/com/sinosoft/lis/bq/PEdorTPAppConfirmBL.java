package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.db.LPPremDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong,Lanjun
 * @version 1.0
 */

public class PEdorTPAppConfirmBL implements EdorAppConfirm
{
    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private  String mCalFlag = null;
    private ReCalBL mReCalBL = null;
    
    private double hmAmnt;
    
    private LPPolSet mLPPolSet=new LPPolSet();

    public PEdorTPAppConfirmBL()
    {}

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */ 
    public boolean submitData(VData cInputData, String cOperate)
    {

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        TransferData tTransferData = (TransferData) data
                                     .getObjectByObjectName("TransferData", 0);
        if(tTransferData != null)
        {
            mCalFlag = (String) tTransferData.getValueByName(BQ.EDORTYPE_UW);
        }

        mLPEdorItemSchema = (LPEdorItemSchema) data
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);

        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorNSAppComfirmBL: "
                           + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime() + " "
                           + mGlobalInput.Operator + " "
                           + mGlobalInput.ClientIP);

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	if(!checkData()){
    		return false;
    	}
        if(!dealPrem())
        {
            return false;
        }

        //将核保加费放入到C表中
        if(!dealPremAdd())
        {
            return false;
        }
        //处理豁免险
        if(!dealHMprem()){
        	return false;
        }       

        String sql = "update LPCont a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LPPrem "
                     + "   where contNo = a.contNo " +
                     		" and edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' ) "
                     + "where edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' "
                     + "   and edorType = '"
                     + mLPEdorItemSchema.getEdorType() + "' "
                     + "   and contNo = '"
                     + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);

        return true;
    }
    
    private boolean dealPrem(){
        LPPolDB tLPPolDB = new LPPolDB();
        String polSQL = "select * from lppol where edorno = '"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
        		" and edortype = '"+mLPEdorItemSchema.getEdorType()+"' and contno = '"+mLPEdorItemSchema.getContNo()+"' " +
        				" and exists (select 1 from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code1 )" +
        				" with ur  ";
        LPPolSet tLPPolSet = tLPPolDB.executeQuery(polSQL);
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            if (!dealOnePol(tLPPolSet.get(i)))
            {
                return false;
            }
        }
    	return true;
    }
    
    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("未找到序号" + tLCPolSchema.getRiskSeqNo()
                                    + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }
            
            double oldPrem = tLCPolSchema.getPrem();
            double newPrem = calNewPrem(aLPPolSchema);
 
            hmAmnt +=newPrem;
            setLPTables(newPrem - oldPrem);

        } catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }    
    
    private boolean dealHMprem(){
    	//判断是否有豁免险
    	String riskHM = "select * from lppol where contno='"+mLPEdorItemSchema.getContNo()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"'" +
    			" and exists (select 1 from lmriskapp where riskcode = lppol.riskcode and risktype8='9') with ur ";
    	LPPolDB tLPPolDB = new LPPolDB();
    	LPPolSet tLPPolSet = new LPPolSet();
    	tLPPolSet = tLPPolDB.executeQuery(riskHM);
    	if(tLPPolSet!=null&&tLPPolSet.size()>0){
    		for(int i =1;i<=tLPPolSet.size();i++){
    			LPPolSchema tLPPolSchema = tLPPolSet.get(i).getSchema();
    			//计算保费
    			double amnt = hmAmnt;
//    			String amntSQL = "SELECT SUM(PREM) FROM LPPREM WHERE contno='"+mLPEdorItemSchema.getContNo()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' " +
//    			" AND EXISTS (SELECT 1 FROM LPPOL WHERE POLNO = LPPREM.POLNO " +
//    			" AND NOT EXISTS (select 1 from lmriskapp where riskcode = lppol.riskcode and risktype8='9')) WITH UR ";
//    			amnt = Double.parseDouble(new ExeSQL().getOneValue(amntSQL));
    			tLPPolSchema.setAmnt(amnt);
    			
    			try {
					double oldPrem = tLPPolSchema.getPrem();
					 double newPrem = calNewPrem(tLPPolSchema);
			            setLPTables(newPrem - oldPrem);					 
					 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}else{
    		System.out.println("保单没有豁免险");
    		return true;
    	}
    	
    	return true;
    }
    
    private boolean checkData(){
    	return true;
    }
    
    /**
     * 设置P表的保费
     */
    private void setLPTables(double chgPrem)
    {
//        for (int i = 1; i <= mReCalBL.aftLPPolSet.size(); i++)
//        {
//            double sumPrem = mReCalBL.aftLPPolSet.get(i).getSumPrem() + chgPrem;
//            mReCalBL.aftLPPolSet.get(i).setSumPrem(sumPrem);
//        }
        mMap.put(mReCalBL.aftLPPolSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPDutySet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPPremSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPGetSet, "DELETE&INSERT");
    }   
    

    /**
     * 重算保费
     * @param aLPPolSchema LPPolSchema
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private double calNewPrem(LPPolSchema aLPPolSchema) throws Exception
    {
        //重算保费
        mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
        if (!mReCalBL.recal())
        {
            mErrors.copyAllErrors(mReCalBL.mErrors);
            throw new Exception();
        }
        for(int i=1;i<=mReCalBL.aftLPPolSet.size();i++)
        {
        	mLPPolSet.add(mReCalBL.aftLPPolSet.get(i));
        }
        
        return mReCalBL.aftLPPolSet.get(1).getPrem();
    }
    
    
    
    

    /**
     * 将核保加费放入到C表中
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealPremAdd()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from LPPrem ")
            .append("where edorNo = '")
            .append(mLPEdorItemSchema.getEdorNo()).append("' ")
            .append("   and edorType = '")
            .append(mLPEdorItemSchema.getEdorType()).append("' ")
            .append("   and contNo = '")
            .append(mLPEdorItemSchema.getContNo()).append("' ")
            .append("   and payPlanCode like '000000%' ");

        LPPremSet tLPPremSet = new LPPremDB().executeQuery(sql.toString());
        Reflections ref = new Reflections();
        for(int i = 1; i <= tLPPremSet.size(); i++)
        {
        	//查询保单的加费类型
        	String mAddFeeType = new ExeSQL().getOneValue("select 1 from LpPrem where PolNo = '" + tLPPremSet.get(i).getPolNo() + "' and PayPlanCode like '000000%' and SuppRiskScore > 0 and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"'");
        	 //保单总保费
            if (mAddFeeType!=null&&!"".equals(mAddFeeType))
            {
                //如果是评点加费
                System.out.println("评点加费");

                LPPremSchema tTmpPremInfo = tLPPremSet.get(i).getSchema();
                double cPrem = getAddPDFee(tTmpPremInfo);
                if (cPrem < 0)
                {
                	 mErrors.addOneError("计算加费信息错误");
                    return false;
                }
                hmAmnt += cPrem;

                //修改保费金额
               String sql1 = " update lpprem set prem="+cPrem+",standPrem="+cPrem+" where  polno='"+tTmpPremInfo.getPolNo()+"' and payplancode = '"+tTmpPremInfo.getPayPlanCode()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"'";
               mMap.put(sql1, SysConst.UPDATE);
               
            }else{
            	LPPremSchema tTmpPremInfo = tLPPremSet.get(i).getSchema();
                double cPrem =  getStandardAddFee(tTmpPremInfo);
                if (cPrem < 0)
                {
                	 mErrors.addOneError("计算加费信息错误");
                    return false;
                }
               
                hmAmnt += cPrem;
                //修改保费金额
                String sql1 = " update lpprem set prem="+cPrem+",standPrem="+cPrem+" where  polno='"+tTmpPremInfo.getPolNo()+"' and payplancode = '"+tTmpPremInfo.getPayPlanCode()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"'";
                mMap.put(sql1, SysConst.UPDATE);
            }
        }

        String sql2 = "update LpDuty a "
                      + "set (prem, sumPrem) = "
                      + "   (select sum(prem), sum(sumPrem) "
                      + "   from LpPrem "
                      + "   where polNo = a.polNo "
                      + "      and dutyCode = a.dutyCode " +
                      		" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' ) "
                      + "where contNo = '"
                      + mLPEdorItemSchema.getContNo() + "'" +
                      		" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' ";
        mMap.put(sql2, SysConst.UPDATE);

        sql2 = "update LpPol a "
               + "set (prem, sumPrem) = "
               + "   (select sum(prem), sum(sumPrem) "
               + "   from LpPrem "
               + "   where polNo = a.polNo " +
               		"and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' ) "
               + "where contNo = '"
               + mLPEdorItemSchema.getContNo() + "' " +
               		" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' ";
        mMap.put(sql2, SysConst.UPDATE);

        return true;
    }
    
    /**
     * 正常加费，相应金额
     * @param tLCPremSchema
     * @return
     */
    private double getStandardAddFee(LPPremSchema tLPPremSchema)
    {
        // 判断险种是否为万能行险种
        double tFeeValue = 0d;
        
        //查询加费险种信息
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setPolNo(tLPPremSchema.getPolNo());
        
        LPPolSet tLPPolSet = new LPPolSet();
        tLPPolSet = tLPPolDB.query();
        LPPolSchema tLPPolSchema = new LPPolSchema();
        if(tLPPolSet!=null&&tLPPolSet.size()>0){
        	tLPPolSchema = tLPPolSet.get(1);
        }else{
            String tStrErr = "险种信息缺失。";
            System.out.println(tStrErr);
            return -1;
        }
        
        if (CommonBL.isULIRisk(tLPPolSchema.getRiskCode()))
        {
        	tLPPremSchema.setPrem(0.0d);
        }
        else
        {
            // 现有正常加费
            if (tLPPremSchema.getRate() > 0)
            {
                tFeeValue = PubFun.setPrecision((tLPPolSchema.getPrem() * tLPPremSchema.getRate()), "0.00");
                tLPPremSchema.setPrem(PubFun.setPrecision((tLPPolSchema.getPrem() * tLPPremSchema.getRate()), "0.00"));
            }
            else
            {
                tFeeValue =  tLPPremSchema.getPrem();
            }
        }
        // -----------------------

        return tFeeValue;
    }
    
    private double getAddPDFee(LPPremSchema cAddFeeInfo)
    {
        double cPrem = 0;
        int cPDFee = 0;
        
        //查询加费险种信息
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setPolNo(cAddFeeInfo.getPolNo());
        
        LPPolSet tLPPolSet = new LPPolSet();
        tLPPolSet = tLPPolDB.query();
        LPPolSchema tLPPolSchema = new LPPolSchema();
        if(tLPPolSet!=null&&tLPPolSet.size()>0){
        	tLPPolSchema = tLPPolSet.get(1);
        }else{
            String tStrErr = "险种信息缺失。";
            System.out.println(tStrErr);
            return -1;
        }


        

        // 准备评点算法
        String tRiskCode = tLPPolSchema.getRiskCode();
        
        String tStrSql = "select * from LMCalMode where RiskCode = '" + tRiskCode + "' and Type = 'Q'";
        LMCalModeSet tCalModeSet = new LMCalModeDB().executeQuery(tStrSql);
        if (tCalModeSet == null || tCalModeSet.size() == 0)
        {
            String tStrErr = "险种不存在评点加费信息，不能进行评点加费。";
            System.out.println(tStrErr);
            return -1;
        }
        else if (tCalModeSet.size() >= 2)
        {
            String tStrErr = "险种评点加费描述信息出现多条，不能正确进行评点操作。";
            System.out.println(tStrErr);
            return -1;
        }
        LMCalModeSchema tPDCalMode = tCalModeSet.get(1);
        // --------------------
        
        double tGet = tLPPolSchema.getAmnt();
        int tPayEndYear = tLPPolSchema.getPayEndYear();
        String tPayEndYearFlag = tLPPolSchema.getPayEndYearFlag();
        String tSex = tLPPolSchema.getInsuredSex();
        int tAppAge = tLPPolSchema.getInsuredAppAge();
        int tInsuYear = tLPPolSchema.getInsuYear();
        String tInsuYearFlag = tLPPolSchema.getInsuYearFlag();
        int tPayIntv = tLPPolSchema.getPayIntv();
        String tPolNo = tLPPolSchema.getPolNo();
        double tPrem = tLPPolSchema.getPrem();

        double tSuppRiskScore = cAddFeeInfo.getSuppRiskScore();

        String tCalCode = tPDCalMode.getCalCode();
        
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(tCalCode);
        

        mCalculator.addBasicFactor("SuppRiskScore", String.valueOf(tSuppRiskScore));

        mCalculator.addBasicFactor("Get", String.valueOf(tGet));
        
        mCalculator.addBasicFactor("Sex", tSex);
        mCalculator.addBasicFactor("AppAge", String.valueOf(tAppAge));
        
        mCalculator.addBasicFactor("InsuYear", String.valueOf(tInsuYear));
        mCalculator.addBasicFactor("InsuYearFlag", tInsuYearFlag);

        mCalculator.addBasicFactor("PayIntv", String.valueOf(tPayIntv));
        mCalculator.addBasicFactor("PayEndYear", String.valueOf(tPayEndYear));
        mCalculator.addBasicFactor("PayEndYearFlag", tPayEndYearFlag);
        mCalculator.addBasicFactor("PolNo", tPolNo);
        mCalculator.addBasicFactor("Prem", String.valueOf(tPrem));

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals(""))
        {
            String tStrErr = "计算评点值对应加费金额失败。";
            System.out.println(tStrErr);
            return -1;
        }
        else
        {
            cPrem = Double.parseDouble(tStr);
        }

        return cPrem;

    }

    public static void main(String[] args)
    {}

}
