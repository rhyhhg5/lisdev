package com.sinosoft.lis.bq;


import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPPolSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class PEdorFCDetailBL {

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LPPolSet mLPPolSet = new LPPolSet();

    private MMap map = new MMap();
    private Reflections ref = new Reflections();
    TransferData tempTransferData = new TransferData();
    VData  mVData = new VData();

    private String mCurrDate = PubFun.getCurrentDate();
    private String mCurrTime = PubFun.getCurrentTime();



    public PEdorFCDetailBL() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareData()) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorFCDetailBL End PubSubmit");
        return true;
    }

    public boolean getInputData() {
        try {
            mGlobalInput = (GlobalInput) mInputData
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            tempTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        } catch (Exception e) {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mGlobalInput == null || mLPEdorItemSchema == null) {
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean checkData() {
        if (!checkLP()) {
            return false;
        }
        if (!checkDueFee()) {
            return false;
        }
        if (!checkContState())
        {
            return false;
        }
        return true;
    }

    public boolean dealData()
    {
        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询保单信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCContSchema = tLCContDB.getSchema();

        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPContSchema.setEdorType(mLPEdorItemSchema.getEdorType());
        ref.transFields(tLPContSchema,tLCContSchema);

        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCPolSet = tLCPolDB.query();
        if(tLCPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询险种信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1 ; i <= tLCPolSet.size() ; i++)
        {
            LCPolSchema tLCPolSchema =tLCPolSet.get(i).getSchema();
            LPPolSchema tLPPolSchema = new LPPolSchema();
            tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            ref.transFields(tLPPolSchema,tLCPolSchema);
            mLPPolSet.add(tLPPolSchema);
        }

        map.put(tLPContSchema,SysConst.DELETE_AND_INSERT);
        String upStateSQL = "UpDate LPEdorItem set EdorState ='1',ModifyDate = Current Date ,ModifyTime = Current Time where EdorAcceptNo ='"+mLPEdorItemSchema.getEdorAcceptNo()+"'" ;
        map.put(upStateSQL, SysConst.UPDATE);
        map.put(mLPPolSet,SysConst.DELETE_AND_INSERT);
        return true ;
    }

    public boolean prepareData()
    {
        mResult.clear();
        mResult.add(map);
        return true ;
    }

    //校验保单是否有未结案的理赔

    private boolean checkLP()
    {
        String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
                     mLPEdorItemSchema.getContNo() + "')"
                     + " and rgtstate not in('11','12','14') "
                     ;
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "有未结案的理赔，不能做该保全项目!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     *保单有续期应收并且已经处于银行发盘状态，不可以做保单质押冻结。
     * @return boolean
     */
    private boolean checkDueFee() {
        String sql = "select * from LJSPay "
                     + " where OtherNoType = '2' " //2是个单续期
                     + " and bankonthewayflag = '1' "
                     + "and OtherNo = '" + mLPEdorItemSchema.getContNo() + "' ";

        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0) {
            for (int i = 1; i <= tLJSPaySet.size(); i++) {
                if (tLJSPaySet.get(i).getBankOnTheWayFlag() != null
                    && tLJSPaySet.get(i).getBankOnTheWayFlag().equals("1")) {
                    CError tError = new CError();
                    tError.moduleName = "PEdorFCDetailBL";
                    tError.functionName = "checkLP";
                    tError.errorMessage = "该保单的续期应收记录已经被发盘，当前不能处理保全业务！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkContState()
    {
        String sql = "select stateflag from lccont  where contno ='"+mLPEdorItemSchema.getContNo()+"'";
        String StateFlag = new ExeSQL().getOneValue(sql);
        if (!StateFlag.equals("1") || StateFlag.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFCDetailBL";
            tError.functionName = "checkContState";
            tError.errorMessage = "保单为失效状态，不可以做此保全操作！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
        return mResult;
    }
}
