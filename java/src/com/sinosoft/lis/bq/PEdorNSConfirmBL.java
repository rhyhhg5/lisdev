package com.sinosoft.lis.bq;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.ProposalSignBL;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author MiNSm
 * @ReWrite: ZhangRong
 * @version 1.0
 * @author Lanjun
 */

public class PEdorNSConfirmBL implements EdorConfirm
{
    /** 全局基础数据 */
    private GlobalInput mGI = new GlobalInput();

    /** 传入的业务数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LCContSchema mLCContSchema = null;
    private LCPolSet mLCPolSetAdd = new LCPolSet();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private MMap mMap = new MMap();

    public PEdorNSConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，此可为空
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkDate()){
        	return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到保全确认处理后的数据集合
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) data
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mGI = (GlobalInput) data
                           .getObjectByObjectName("GlobalInput", 0);
        }
        catch(Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "GEdorNSDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("\n\n" + mGI.Operator + " "
                           + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime());

        return true;
    }
    /**
     * 新增险种，结案日期不能大于保全生效日期超过一天
     * 
     */
    private boolean checkDate(){
    	
      FDate tFDate = new FDate();
//
//      String edorvalidate = "select edorvalidate +1 days from lpedoritem where edorno='"+mLPEdorItemSchema.getEdorNo()+"' with ur";
//      ExeSQL e = new ExeSQL();
//      String tEdorValiDate = e.getOneValue(edorvalidate);
//      System.out.println(tEdorValiDate);
//      if(tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(tEdorValiDate))==0||tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))==0)
//      {
//      }else{
//      	mErrors.addOneError("金生无忧或者美满今生或福满人生或美满一生的保单新增附加个人护理险种时结案日期不能大于保全生效日期超过一天：保全生效日期为"+mLPEdorItemSchema.getEdorValiDate());
//          return false;
//      }
    	
    	return true;
    }
    
    /**
     * 新险种签单，P表换号，保全确认
     * @return：成功true，否则false；
     */
    private boolean dealData()
    {
        if(!getContInfo())
        {
            return false;
        }

        MMap tMMap = polSignC();
        if(tMMap == null)
        {
            return false;
        }
        mMap.add(tMMap);

        //得到签单后的险种信息，为P表换号做准备
        Object[] pols = tMMap.getAllObjectByObjectName("LCPolSchema", 0);
        pols = filterPols(pols);

        if(!dealFinace(pols))
        {
            return false;
        }

        if(!polSignP(pols))
        {
            return false;
        }

        if(!updateData(pols))
        {
            return false;
        }
        
        return true;
    }

    /**
     * 将费核保险种剔除
     * @param pols Object[]
     * @return boolean
     */
    private Object[] filterPols(Object[] pols)
    {
        LCPolSet tLCPolSet = new LCPolSet();
        for(int i = 0; i < pols.length; i++)
        {
            LCPolSchema schema = (LCPolSchema) pols[i];
            if(schema.getAppFlag().equals(BQ.APPFLAG_SIGN))
            {
                tLCPolSet.add(schema);
            }
        }
        Object[] re = new Object[tLCPolSet.size()];
        for(int i = 0; i < re.length; i++)
        {
            re[i] = tLCPolSet.get(i + 1);
        }

        return re;
    }

    /**
     * 换号，修改终止日，保费汇总
     * pols：签单后的险种信息
     * @return boolean：成功true
     */
    private boolean updateData(Object[] pols)
    {
        for(int i = 0; i < pols.length; i++)
        {
            //得到险种分类
            LCPolSchema schemaSigned = (LCPolSchema) pols[i];
            String sql = "select a.RiskType5 "
                         + "from LMRiskApp a "
                         + "where a.riskCode = '"
                         + schemaSigned.getRiskCode() + "' ";
            String riskType5 = new ExeSQL().getOneValue(sql);
            //QULQ 	070115 add
//            LPPolSchema temp = getPolNoBeforeSigned(schemaSigned);
//            if(temp==null)
//            {
//            	return false;
//            }
//            mLPEdorItemSchema.setPolNo(temp.getPolNo());
            //若为长期险，由于只能是保单周年日生效，所以不用修改截至日期
            if(riskType5.equals(BQ.RISKTYPE5_LONGTIME)
               || riskType5.equals(BQ.RISKTYPE5_FOREVER) || "232701".equals(schemaSigned.getRiskCode())|| "232901".equals(schemaSigned.getRiskCode()))
            {
                //20150821金生无忧|美满今生|福满人生可添加附加B款
                if("334801".equals(schemaSigned.getRiskCode()) || "340501".equals(schemaSigned.getRiskCode()) || "340601".equals(schemaSigned.getRiskCode()) || "232701".equals(schemaSigned.getRiskCode())|| "232901".equals(schemaSigned.getRiskCode())){
                	if(!update332301Date(schemaSigned)){
                		
                		return false;
                	}
                }
                else{
                	//更新险种
                    sql = "update LCPol a " +
                          " set (signCom, signDate, signTime) = "
                          + "    ('" + mGI.ComCode + "', '" + mCurDate
                          + "', '" + mCurTime + "'), "
                          + "   ModifyDate = '" + mCurDate + "', "
                          + "   ModifyTime = '" + mCurTime + "', "
                          + "   Operator = '" + mGI.Operator + "' " +
                          "where polNo = '" + schemaSigned.getPolNo() + "' ";
                    mMap.put(sql, "UPDATE");
                    mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

                }
                
            }
            //短期险可以是即时生效和下期生效，需要将险种终止日期更新为 本保单周年日
            else if(riskType5.equals(BQ.RISKTYPE5_LESSTHANYEAR)
                    || riskType5.equals(BQ.RISKTYPE5_YEAR))
            {
                String upPolYears = getUpContYears(mLPEdorItemSchema.getEdorValiDate(),mLCContSchema.getPaytoDate());
                if(upPolYears == null)
                {
                    mErrors.addOneError("计算保单年度出错");
                    return false;
                }
                sql = "select date('" + mLCContSchema.getCValiDate()
                      + "') + " + upPolYears + " year "
                      + "from dual ";
                String endDate = new ExeSQL().getOneValue(sql);

                if(!updateCValiDate(pols, schemaSigned, endDate))
                {
                    return false;
                }
            }
        }

        String sql = "update LCCont a "
              + " set (prem, sumPrem, amnt, cInValiDate) = "
              + "     (select sum(prem), sum(sumPrem), sum(amnt), "
              + "     max(endDate) "
              + "     from LCPol "
              + "     where contNo = a.contNo), "
              + "  ModifyDate = '" + mCurDate + "', "
              + "  ModifyTime = '" + mCurTime + "', "
              + "  Operator = '" + mGI.Operator + "' " +
              "where contNo = '" + mLCContSchema.getContNo() + "' ";
        mMap.put(sql, "UPDATE");
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        return true;
    }
    
    
    private boolean update332301Date(LCPolSchema polSchemaSigned){

    	
   	 String upPolYears = getUpContYearsE(mLPEdorItemSchema.getEdorValiDate(),mLCContSchema.getPaytoDate());
        if(upPolYears == null)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        String cvalisql = "select date('" + mLCContSchema.getCValiDate()
              + "') + " + upPolYears + " year "
              + "from dual ";
        String tcvalidate = new ExeSQL().getOneValue(cvalisql);
        
        String signsql = "select date('" + mLCContSchema.getSignDate()
        + "') + " + upPolYears + " year "
        + "from dual ";
        String tsigndate = new ExeSQL().getOneValue(signsql);
        
        String tpaytodate="";
        String tpayenddate="";
        String tenddate="";
        String mainpolno="";
        
        if("340501".equals(polSchemaSigned.getRiskCode()) || "340601".equals(polSchemaSigned.getRiskCode())|| "232701".equals(polSchemaSigned.getRiskCode())|| "232901".equals(polSchemaSigned.getRiskCode())){
        	String tpaytodatesql= "select date(payenddate) from lcpol" +
       		" where riskcode in ('340501','340601') and contno='"+mLCContSchema.getContNo()+"'";
        	System.out.println(tpaytodatesql);
        	
        	String cvalidate =  "select date(cvalidate)  from lcpol" +
       		" where riskcode in ('340501','340601') and contno='"+mLCContSchema.getContNo()+"'";
        	tcvalidate = new ExeSQL().getOneValue(cvalidate); 
        	if ("232701".equals(polSchemaSigned.getRiskCode())){
        		tpaytodate = new ExeSQL().getOneValue("select date(cvalidate)  from lcpol" +
        	       		" where riskcode ='232701' and contno='"+mLCContSchema.getContNo()+"'");
        		tsigndate = tpaytodate;
        		
        	   cvalidate =  "select date(cvalidate)  from lcpol" +
           		" where riskcode='232701' and contno='"+mLCContSchema.getContNo()+"'";
            	tcvalidate = new ExeSQL().getOneValue(cvalidate); 
        	}else if ("232901".equals(polSchemaSigned.getRiskCode())){
             	tpaytodate = new ExeSQL().getOneValue("select date(cvalidate)  from lcpol" +
            	       		" where riskcode ='232901' and contno='"+mLCContSchema.getContNo()+"'");
                tsigndate = tpaytodate;
                cvalidate =  "select date(cvalidate)  from lcpol" +
           		" where riskcode='232901' and contno='"+mLCContSchema.getContNo()+"'";
            	tcvalidate = new ExeSQL().getOneValue(cvalidate); 
        	}else {
        	tpaytodate = new ExeSQL().getOneValue(tpaytodatesql);
        	}
        	String tpayenddatesql= "select date(payenddate)  from lcpol" +
       		" where riskcode in ('340501','340601','232701','232901') and contno='"+mLCContSchema.getContNo()+"'";
        	tpayenddate = new ExeSQL().getOneValue(tpayenddatesql);
        	if ("232901".equals(polSchemaSigned.getRiskCode())){
        		tenddate = tpayenddate;
        	}else {
            	String tenddatesql= "select date(enddate)  from lcpol" +
           		" where riskcode in ('340501','340601','232701') and contno='"+mLCContSchema.getContNo()+"'";
            	tenddate = new ExeSQL().getOneValue(tenddatesql); 	
        	}
        	
        	mainpolno=polSchemaSigned.getMainPolNo();
        }else{
            String tpaytodatesql = "select date(paytodate)   from lcpol" +
       		" where riskcode in ('340101','340301','730201','340401') and contno='"+mLCContSchema.getContNo()+"'";
            //String tpaytodatesql = "select date('"+tcvalidate+"')+ 1 year from dual ";
            tpaytodate = new ExeSQL().getOneValue(tpaytodatesql);
    
            String tpayenddateesql = "select date(payenddate)   from lcpol" +
            	" where riskcode in ('340101','340301','730201','340401') and contno='"+mLCContSchema.getContNo()+"'";
            tpayenddate = new ExeSQL().getOneValue(tpayenddateesql);
    
            String tenddatesql = "select date(enddate)   from lcpol" +
            	" where riskcode in ('340101','340301','730201','340401') and contno='"+mLCContSchema.getContNo()+"'";
            tenddate = new ExeSQL().getOneValue(tenddatesql);
            //得到主险种号
            mainpolno=new ExeSQL().getOneValue("select polno from lcpol where contno ='"+mLCContSchema.getContNo()+"' "
                             +   " and riskcode in( select code1 from ldcode1 where code in ('334801')  and codetype='mainsubriskrela' ) with ur ");
        }

        //String tbalandatesql = "select year('"+tcvalidate+"')||'-'||month('"+tcvalidate+"')||'-1' from dual";
//        String tbalandatesql = "select date('"+tcvalidate+"') + 1 days from dual";
//        String tbalandate = new ExeSQL().getOneValue(tbalandatesql); // 2016-9-13 新增险种时，账户baladate改为险种生效日期
        

        
        String sql = "update LCPol a " +
	       " set (mainpolno,signCom, signDate, signTime,cvalidate,paytodate,payenddate,enddate," +
	       	"getstartdate,firstpaydate,approvedate,uwdate,lastrevdate,uwflag) = "
	       + "    ('"+mainpolno+"','" + mGI.ComCode + "', '" + tsigndate
	       + "', '" + mCurTime + "','"+tcvalidate+"','"+tpaytodate+"','"+tpayenddate+"','"+tenddate+"','"+tcvalidate+"','" + tsigndate
	       + "','" + tsigndate+ "','" + tsigndate+ "','"+tcvalidate+"','9'), "
	       + "   ModifyDate = '" + mCurDate + "', "
	       + "   ModifyTime = '" + mCurTime + "', "
	       + "   Operator = '" + mGI.Operator + "' " +
	       "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
      //更新缴费项
         sql = "update LCPrem a " +
                     "set payEndDate = '" + tpayenddate + "', "
                     + "   payToDate = '"+tpaytodate+"',payStartDate='"+tcvalidate+"', "
                     + "   ModifyDate = '" + mCurDate + "', "
                     + "   ModifyTime = '" + mCurTime + "', "
                     + "   Operator = '" + mGI.Operator + "' "
                     + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        //更新责任
        sql = "update LCDuty a " +
              " set (prem, sumPrem, PayToDate, payEndDate, endDate,firstpaydate,getstartdate) = "
              + "      (select sum(prem), sum(sumPrem), "
              + "          min(payToDate), min(payEndDate), '"+tenddate+"','" + tcvalidate
              + "', '" + tcvalidate+ "'"
              + "      from LCPrem "
              + "      where polNo = a.polNo and dutyCode = a.dutyCode), "
              + "   ModifyDate = '" + mCurDate + "', "
              + "   ModifyTime = '" + mCurTime + "', "
              + "   Operator = '" + mGI.Operator + "' " +
              "where PolNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, "UPDATE");
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        

        //更新给付责任
        sql = "update LCGet a " +
              "set getStartDate='"+tcvalidate+"',getEndDate = '" + tenddate + "', "
              + "   getToDate = '"+tcvalidate+"', "
              + "   ModifyDate = '" + mCurDate + "', "
              + "   ModifyTime = '" + mCurTime + "', "
              + "   Operator = '" + mGI.Operator + "' "
              + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        //2016-9-13 by hehongwei  保全新增险种，账户生效日期 调整为险种生效日期
        sql = "update lcinsureacc a " +
        "set baladate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureacctrace a " +
        "set paydate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' and othertype='1'";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureaccfee a " +
        "set baladate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureaccfeetrace a " +
        "set paydate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' and othertype='1' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureaccclass a " +
        "set baladate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureaccclassfee a " +
        "set baladate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
        
        sql = "update lcinsureaccbalance a " +
        "set duebaladate='"+tcvalidate+"',"
        + "   ModifyDate = '" + mCurDate + "', "
        + "   ModifyTime = '" + mCurTime + "', "
        + "   Operator = '" + mGI.Operator + "' "
        + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);
   	
   	return true;
   
    }
    /**
     * getUpContYears
     * @return String
     */
    private String getUpContYears(String edorValiDate,String minPayToDate)
    {
            //这里要减去一天，这样才能算出保单年度
       String sql = "select case when (date('"+edorValiDate+"') - 1 days) > (date('"+minPayToDate+"')-1 days ) "
                    +" then (date('"+minPayToDate+"')-1 days)"
                    +" else  (date('"+edorValiDate+"') - 1 days) end from dual ";
       ExeSQL e = new ExeSQL();
       String tEdorValiDate = e.getOneValue(sql);
       if(tEdorValiDate == null || tEdorValiDate.equals(""))
       {
           return null;
       }
       //因为这里计算出的保单年度是从0开始的，所以下面要加一
       int years = PubFun.calInterval(mLCContSchema.getCValiDate(), tEdorValiDate, "Y");

       return String.valueOf(years + 1);

    }
    
    private String getUpContYearsE(String edorValiDate,String minPayToDate)
    {
            //这里要减去一天，这样才能算出保单年度
       String sql = "select case when (date('"+edorValiDate+"') - 1 days) > (date('"+minPayToDate+"')-1 days ) "
                    +" then (date('"+edorValiDate+"')-1 days)"
                    +" else  (date('"+edorValiDate+"') - 1 days) end from dual ";
       ExeSQL e = new ExeSQL();
       String tEdorValiDate = e.getOneValue(sql);
       if(tEdorValiDate == null || tEdorValiDate.equals(""))
       {
           return null;
       }
       //因为这里计算出的保单年度是从0开始的，所以下面要加一
       int years = PubFun.calInterval(mLCContSchema.getCValiDate(), tEdorValiDate, "Y");

       return String.valueOf(years + 1);

    }
    /**
     * 更新险种相关信息
     * @param polNoSigned String：签单后的险种号
     * @param endDate String：终止日期
     * @return boolean
     */
    private boolean updateCValiDate(Object[] pols,
                                    LCPolSchema polSchemaSigned,
                                    String endDate)
    {
    	
//    	更新ljapayperson中的curPayToDate
    	String ljapaySql="update ljapayperson set curPayToDate='"+endDate+"' where polno='"+polSchemaSigned.getPolNo() +"'";
    	mMap.put(ljapaySql, SysConst.UPDATE);
    	
        //更新缴费项
        String sql = "update LCPrem a " +
                     "set payEndDate = '" + endDate + "', "
                     + "   payToDate = (select curPayToDate from LJAPayPerson "
                     + "             where polNo = a.polNo "
                     + "                and dutyCode = a.dutyCode "
                     + "                and payPlanCode = a.payPlanCode), "
                     + "   ModifyDate = '" + mCurDate + "', "
                     + "   ModifyTime = '" + mCurTime + "', "
                     + "   Operator = '" + mGI.Operator + "' "
                     + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        //更新责任
        sql = "update LCDuty a " +
              " set (prem, sumPrem, PayToDate, payEndDate, endDate) = "
              + "      (select sum(prem), sum(sumPrem), "
              + "          min(payToDate), min(payEndDate), min(payEndDate) "
              + "      from LCPrem "
              + "      where polNo = a.polNo and dutyCode = a.dutyCode), "
              + "   ModifyDate = '" + mCurDate + "', "
              + "   ModifyTime = '" + mCurTime + "', "
              + "   Operator = '" + mGI.Operator + "' " +
              "where PolNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, "UPDATE");
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        //更新险种
        sql = "update LCPol a " +
              " set (prem, sumPrem, amnt, PayToDate, payEndDate, endDate, "
              + "      signCom, signDate, signTime) = "
              + "      (select sum(prem), sum(sumPrem), sum(amnt), "
              + "          min(payToDate), min(payEndDate), min(endDate), '"
              + mGI.ComCode + "', '" + mCurDate + "', '" + mCurTime + "' "
              + "      from LCDuty "
              + "      where polNo = a.polNo), "
              + "  cvalidate='"+mLPEdorItemSchema.getEdorValiDate()+"',"
              + "   ModifyDate = '" + mCurDate + "', "
              + "   ModifyTime = '" + mCurTime + "', "
              + "   Operator = '" + mGI.Operator + "' " +
              "where PolNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, "UPDATE");
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        //更新给付责任
        sql = "update LCGet a " +
              "set getEndDate = '" + endDate + "', "
              + "   getToDate = (select min(lastPayToDate) from LJAPayPerson "
              + "             where polNo = a.polNo "
              + "                and dutyCode = a.dutyCode), "
              + "   ModifyDate = '" + mCurDate + "', "
              + "   ModifyTime = '" + mCurTime + "', "
              + "   Operator = '" + mGI.Operator + "' "
              + "where polNo = '" + polSchemaSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);
        mMap.put(sql.replaceFirst("LC", "LP"), SysConst.UPDATE);

        LPPolSchema tLPPolSchemaBeforeSigned
            = getPolNoBeforeSigned(polSchemaSigned);

        //保全补退费表换号
        sql = "update LJAGetEndorse a "
              + "set polNo = '" + polSchemaSigned.getPolNo() + "' "
              + "where endorsementNo = '"
              + mLPEdorItemSchema.getEdorNo() + "' "
              + "   and feeOperationType = '"
              + mLPEdorItemSchema.getEdorType() + "' "
              + "   and polNo = '" + tLPPolSchemaBeforeSigned.getPolNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);

        String[] pTables =
            {"LPUWMaster", "LPUWSub", "LPUWError",
            "LPEdorEspecialData"};

        for(int i = 0; i < pTables.length; i++)
        {
            sql = "update " + pTables[i]
                  + "   set polNo = '" + polSchemaSigned.getPolNo() + "' "
                  + "where polNo = '"
                  + tLPPolSchemaBeforeSigned.getPolNo() + "' "
                  + "   and edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' "
                  + "   and edorType = '"
                  + mLPEdorItemSchema.getEdorType() + "' ";
            mMap.put(sql, SysConst.UPDATE);
        }

        return true;
    }

    /**
     * 得到签单前险种
     * @param polSchemaSigned LCPolSchema
     * @return String
     */
    private LPPolSchema getPolNoBeforeSigned(LCPolSchema polSchemaSigned)
    {
        String sql = "select * from LPPol "
                     + "where contNo = '" + polSchemaSigned.getContNo() + "' "
                     + "   and insuredNo = '" + polSchemaSigned.getInsuredNo()
                     + "' "
                     + "   and riskCode = '" + polSchemaSigned.getRiskCode()
                     + "' ";
        LPPolSet tLPPolSet = new LPPolDB().executeQuery(sql);
        if(tLPPolSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSConfirmBL";
            tError.functionName = "updateCValiDate";
            tError.errorMessage = "查询保全险种信息失败";
            mErrors.addOneError(tError);
            return null;
        }

        return tLPPolSet.get(1);
    }

    /**
     * getContInfo
     *
     * @return boolean
     */
    private boolean getContInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        tLCContDB.getInfo();

        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * 生成个人实收，直接按签单后号码生成
     * @return boolean：成功true
     */
    private boolean dealFinace(Object[] pols)
    {
        //查询险种信息
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if(tLPPolSet.size() == 0)
        {
            mErrors.addOneError("查询险种失败");
            return false;
        }

        String payDate = null;
        String payNo = null;
        //查询应收信息
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mLPEdorItemSchema.getEdorNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if(tLJSPaySet.size() == 0)
        {
            payNo = PubFun1.CreateMaxNo("PAYNO", null);
            payDate = mCurDate;
        }
        else
        {
            payNo = "11";
            payDate = tLJSPaySet.get(1).getPayDate();
        }
        for(int t = 1; t <= tLPPolSet.size(); t++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(t);

            LCPolSchema tLCPolSchemaSigned
                = getLCPolSchemaSigned(pols, tLPPolSchema);
            if(tLCPolSchemaSigned == null)
            {
                return false;
            }

            LPPremDB tLPPremDB = new LPPremDB();
            tLPPremDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPPremDB.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPPremDB.setPolNo(tLPPolSchema.getPolNo());

            LPPremSet tLPPremSet = tLPPremDB.query();
            if(tLPPremSet.size() == 0)
            {
                mErrors.addOneError("查询缴费项失败");
                return false;
            }

            LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
            for(int i = 1; i <= tLPPremSet.size(); i++)
            {
                LPPremSchema schema = tLPPremSet.get(i);

                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
                tLJSGetEndorseDB.setEndorsementNo(tLPPolSchema.getEdorNo());
                tLJSGetEndorseDB.setFeeOperationType(tLPPolSchema.getEdorType());
                tLJSGetEndorseDB.setPolNo(tLPPolSchema.getPolNo());
                tLJSGetEndorseDB.setDutyCode(schema.getDutyCode());
                tLJSGetEndorseDB.setPayPlanCode(schema.getPayPlanCode());
                LJSGetEndorseSet set2 = tLJSGetEndorseDB.query();
                if(set2.size() == 0)
                {
                    mErrors.addOneError("查询不退费表失败");
                    return false;
                }
                LJSGetEndorseSchema tLJSGetEndorseSchema = set2.get(1);

                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                tLJAPayPersonSchema.setPolNo(tLCPolSchemaSigned.getPolNo());
                tLJAPayPersonSchema.setPayCount(1);
                tLJAPayPersonSchema.setGrpPolNo(tLPPolSchema.getGrpPolNo());
                tLJAPayPersonSchema.setContNo(schema.getContNo());
                tLJAPayPersonSchema.setGrpContNo(schema.getGrpContNo());
                tLJAPayPersonSchema.setAppntNo(schema.getAppntNo());
                tLJAPayPersonSchema.setPayNo(payNo);  //这里无法得到，在后面将再次更新
                tLJAPayPersonSchema.setPayAimClass("1");
                tLJAPayPersonSchema.setDutyCode(schema.getDutyCode());
                tLJAPayPersonSchema.setPayPlanCode(schema.getPayPlanCode());
                tLJAPayPersonSchema
                    .setSumDuePayMoney(tLJSGetEndorseSchema.getGetMoney());
                tLJAPayPersonSchema
                    .setSumActuPayMoney(tLJSGetEndorseSchema.getGetMoney());
                tLJAPayPersonSchema.setPayIntv(schema.getPayIntv());
                tLJAPayPersonSchema.setPayDate(payDate);
                tLJAPayPersonSchema.setPayType("ZC");
                tLJAPayPersonSchema.setEnterAccDate(CommonBL.getEnterAccDate(
                    mLPEdorItemSchema.getEdorNo()));
                tLJAPayPersonSchema.setConfDate(mCurDate);
                tLJAPayPersonSchema.setLastPayToDate(tLPPolSchema.getCValiDate());
                tLJAPayPersonSchema.setCurPayToDate(mLCContSchema.getPaytoDate()); //在最后统一更新
                tLJAPayPersonSchema.setOperator(mGI.Operator);
                tLJAPayPersonSchema.setMakeDate(mCurDate);
                tLJAPayPersonSchema.setMakeTime(mCurTime);
                tLJAPayPersonSchema.setModifyDate(mCurDate);
                tLJAPayPersonSchema.setModifyTime(mCurTime);
                tLJAPayPersonSchema.setManageCom(tLPPolSchema.getManageCom());
                tLJAPayPersonSchema.setAgentCom(tLPPolSchema.getAgentCom());
                tLJAPayPersonSchema.setAgentType(tLPPolSchema.getAgentType());
                tLJAPayPersonSchema.setRiskCode(tLPPolSchema.getRiskCode());
                tLJAPayPersonSchema.setAgentCode(tLPPolSchema.getAgentCode());
                tLJAPayPersonSchema.setAgentGroup(tLPPolSchema.getAgentGroup());

                tLJAPayPersonSet.add(tLJAPayPersonSchema);
            }

            mMap.put(tLJAPayPersonSet, SysConst.DELETE_AND_INSERT);
            if(tLJSPaySet.size() != 0)
            {
                //更新上面无法设置的payNo
                String sql = "update LJAPayPerson a "
                             + "set payNo = "
                             + "    (select payNo from LJAPay "
                             + "    where incomeNo = '"
                             + mLPEdorItemSchema.getEdorNo() + "') "
                             + "where polNo = '"
                             + tLCPolSchemaSigned.getPolNo() + "' ";
                mMap.put(sql, SysConst.UPDATE);
            }
        }

        return true;
    }

    /**
     * 得到tLPPolSchema对应的签单后险种
     * @param pols Object[]
     * @param tLPPolSchema LPPolSchema：签单前险种P
     * @return LCPolSchema：签单后险种
     */
    private LCPolSchema getLCPolSchemaSigned(Object[] pols,
                                             LPPolSchema tLPPolSchema)
    {
        LCPolSchema tLCPolSchemaSigned = null; //tLPPolSchema对应的签单后险种
        for(int m = 0; m < pols.length; m++)
        {
            LCPolSchema polSchema = (LCPolSchema) pols[m];
            if(polSchema.getInsuredNo().equals(tLPPolSchema.getInsuredNo())
               && polSchema.getRiskCode().equals(tLPPolSchema.getRiskCode()))
            {
                tLCPolSchemaSigned = polSchema;
            }
        }
        if(tLCPolSchemaSigned == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSConfirmBL";
            tError.functionName = "dealFinace";
            tError.errorMessage = "处理签单前后险种对应关系失败";
            mErrors.addOneError(tError);
            return null;
        }
        return tLCPolSchemaSigned;
    }

    /**
     * 将P表险种信息换号
     * @param pols Object[]：签单后的C表数据
     * @return boolean：成功true，否则false
     */
    private boolean polSignP(Object[] pols)
    {
        for(int i = 0; i < pols.length; i++)
        {
            LCPolSchema schema = (LCPolSchema) pols[i];
            if(schema.getRiskCode().equals("232701")){
            	
            	 ExeSQL tExSql = new ExeSQL();    
                 String str = "select a.curpaytodate from ljapayperson a, lcpol b where  a.contno= b.contno and a.curpaytodate = b.paytodate and b.contno ='"+mLPEdorItemSchema.getContNo() +"' and a.polno= b.mainpolno ";  
                 String spaytodate = tExSql.getOneValue(str);
                 System.out.println("HHHH"+spaytodate);
       
                 int year = schema.getInsuYear();
                 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                 Date  currdate = null;
                  try {
                 	 currdate = format.parse(spaytodate);
         			
         		} catch (ParseException e) {
         			// TODO Auto-generated catch block
         			e.printStackTrace();
         		}
         		Calendar ca = Calendar.getInstance();
         		ca.setTime(currdate);
         		ca.add(Calendar.YEAR, year);// 
         		currdate = ca.getTime();
                 format.format(currdate);
                 schema.setEndDate(format.format(currdate));
                 schema.setSignDate(spaytodate);
                 schema.setCValiDate(spaytodate);
                 schema.setSignTime(mCurTime); 
                 schema.setPayEndDate(format.format(currdate));
                 schema.setUWFlag("9");
            }

            if(!updatePPolInfo(schema))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * P表险种信息更新：删除P表险种信息，从C表复制
     * @param schema LPPolSchema：待处理的P表信息
     */
    private boolean updatePPolInfo(LCPolSchema schema)
    {
        String[] tables =
        {" LPPol ", " LPDuty ", " LPPrem "};
        for(int i = 0; i < tables.length; i++)
        {
            if(!"LP".equals(tables[i].trim().substring(0, 2)))
            {
                mErrors.addOneError("表明错误：" + tables[i]);
                return false;
            }

            //删除P表险种信息
            String sql = "delete from " + tables[i]
                         + "where contNo = '" + schema.getContNo() + "' "
                         + "   and edorNo = '"
                         + this.mLPEdorItemSchema.getEdorNo() + "' "
                         + "   and edorType = '"
                         + this.mLPEdorItemSchema.getEdorType() + "' ";
            mMap.put(sql, "DELETE");

            //从C表复制到p表
            String tableP = tables[i].replaceFirst("P", "C");
            sql = "insert into " + tables[i]
                  + " (select '" + mLPEdorItemSchema.getEdorNo()
                  + "','" + mLPEdorItemSchema.getEdorType()
                  + "', " + tableP + ".* from "+ tableP
                  + "where polNo = '" + schema.getPolNo() + "') ";
            mMap.put(sql, "INSERT");
        }

        //更新免责信息
        String sql = "update LPSpec "
                     + "set polNo = '" + schema.getPolNo() + "' "
                     + "where polNo = '" + schema.getProposalNo() + "' ";
        mMap.put(sql, "UPDATE");
        return true;
    }
    /**
     * 调用契约程序进行新险种签单
     * @return MMap: 签单后的险种信息集合
     */
    private MMap polSignC()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setAppFlag(BQ.APPFLAG_EDOR);
        tLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet.size() == 0)
        {
            mErrors.addOneError(new CError("获取保单数据失败"));
            return null;
        }
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            mLCPolSetAdd.add(tLCPolSet.get(i).getSchema());
        }

        //准备调用签单需要的数据
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("EdorNo",
                                      mLPEdorItemSchema.getEdorNo());
        tTransferData.setNameAndValue("EdorAcceptNo",
                                      mLPEdorItemSchema.getEdorAcceptNo());
        tTransferData.setNameAndValue("EdorType",
                                      mLPEdorItemSchema.getEdorType());

        VData vPolPub = new VData();

        vPolPub.add(mGI);
        vPolPub.add(tLCPolSet);
        vPolPub.add(mLPEdorItemSchema.getContNo());
        vPolPub.add(tTransferData);

        //准备签单数据
        System.out.println("Start 准备签单数据...");

        ProposalSignBL tProposalSignBL = new ProposalSignBL(); //调用个单签单程序
        if(!tProposalSignBL.submitData(vPolPub, "INSERT||ENDORSE"))
        {
            mErrors.copyAllErrors(tProposalSignBL.mErrors);
            mErrors.addOneError(new CError("新增附加险签单失败！"));
            return null;
        }

        VData tResultData = tProposalSignBL.getResult();
        MMap tMap = (MMap) tResultData.getObjectByObjectName("MMap",
            0);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tProposalSignBL.mErrors);
            mErrors.addOneError(new CError("获得签单程序返回数据失败!"));
        }

        return tMap;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";
        gi.ManageCom = gi.ComCode;

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo("20061020000004");
        tLPEdorItemDB.setEdorType(BQ.EDORTYPE_NS);
        tLPEdorItemDB.setContNo("00001526201");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorNSConfirmBL bl = new PEdorNSConfirmBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("All OK");
        }
    }
}
