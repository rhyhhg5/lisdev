package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 无名单实名化删除</p>
 * <p>Description: 无名单实名化删除理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorWDAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = BQ.EDORTYPE_WD;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 磁盘导入数据  */
    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mLPDiskImportSet = CommonBL.getLPDiskImportSet
                (mEdorNo, mEdorType, mGrpContNo, BQ.IMPORTSTATE_SUCC);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String insuredNo = mLPDiskImportSet.get(i).getInsuredNo();
            deleteOneInsured(insuredNo);
        }
        return true;
    }

    /**
     * 删除一个被保人
     * @param insuredNo String
     * @return boolean
     */
    private boolean deleteOneInsured(String insuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mGrpContNo);
        tLCInsuredDB.setInsuredNo(insuredNo);
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0)
        {
            mErrors.addOneError("未找到被保人" + insuredNo + "的信息！");
            return false;
        }
        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);
            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
            tLPInsuredSchema.setEdorNo(mEdorNo);
            tLPInsuredSchema.setEdorType(mEdorType);
            tLPInsuredSchema.setOperator(mGlobalInput.Operator);
            tLPInsuredSchema.setMakeDate(mCurrentDate);
            tLPInsuredSchema.setMakeTime(mCurrentTime);
            tLPInsuredSchema.setModifyDate(mCurrentDate);
            tLPInsuredSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        }
        return true;
    }

}

