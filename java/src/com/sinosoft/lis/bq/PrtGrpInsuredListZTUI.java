package com.sinosoft.lis.bq;

import java.io.InputStream;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.pubfun.GlobalInput;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtGrpInsuredListZTUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private VData mResult = null;
    PrtGrpInsuredListZTBL mPrtGrpInsuredListZTBL = new PrtGrpInsuredListZTBL();

    public PrtGrpInsuredListZTUI()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        if(!mPrtGrpInsuredListZTBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(mPrtGrpInsuredListZTBL.mErrors);
            return false;
        }
        mResult = mPrtGrpInsuredListZTBL.getResult();

        return true;
    }

    
    public VData getResult()
    {
        return this.mResult;
    }
    
    
    
    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListZTBL.getInputStream();
    }

    public static void main(String[] args)
    {
        String edorAcceptNo = "20050914000010";
        LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
        mLPEdorAppSchema.setEdorAcceptNo(edorAcceptNo);

        GlobalInput tG = new GlobalInput();
        tG.Operator = "endor";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(mLPEdorAppSchema);
        tVData.addElement(tG);

        PrtGrpInsuredListZTUI p = new PrtGrpInsuredListZTUI();
        if(p.submitData(tVData, "PRINT"))
        {
            System.out.println(p.mErrors.getFirstError());
        }
    }
}
