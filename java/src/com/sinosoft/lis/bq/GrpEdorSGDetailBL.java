package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LPBnfDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全减人存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorSGDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mInsuredNo = null;

    private Date mCValiDate = null;

    private Date mLastPayToDate = null;

    private Date mCurPayToDate = null;

    private String mMessage = "";

    private EdorItemSpecialData mSpecialData = null;

    private LPDiskImportSet mLPDiskImportSet = null;
    
    private LPBnfSet mLPBnfSet = null;
    
    private String mGrpContNo=null;
    
    private String mDeathDate=null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema) data.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            
            mSpecialData = (EdorItemSpecialData)data.
                    getObjectByObjectName("EdorItemSpecialData", 0);
            
            mEdorNo = tLPEdorItemSchema.getEdorNo();
            mEdorType = tLPEdorItemSchema.getEdorType();
            mInsuredNo = tLPEdorItemSchema.getInsuredNo();
            mDeathDate = mSpecialData.getEdorValue("DeathDate");
            mLPBnfSet = (LPBnfSet)data.
            getObjectByObjectName("LPBnfSet", 0);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkEdorValiDate())
        {
            return false;
        }
        if (!checkHealthPol())
        {
            return false;
        }
       
        return true;
    }

    /**
      * 获得当前函件的序号
      * @return String
      */
     private String getSerialNumber()
     {
         //得到序号
         String sql = "select * from LGLetter " +
                 "where EdorAcceptNo = '" + mEdorNo + "' " +
                 "order by int(SerialNumber) desc ";
         LGLetterDB db = new LGLetterDB();
         LGLetterSet set = db.executeQuery(sql);
         if (set.size() == 0)
         {
             return "1";
         }
         int sn = Integer.parseInt(set.get(1).getSerialNumber());
         return String.valueOf(sn + 1);
     }

     /**
      * 检查保全生效日期
      * @return boolean
      */
     private boolean checkEdorValiDate()
     {	  
    	  String sqlString = "select grpcontno from lpgrpedoritem where edoracceptno='"+mEdorNo+"' and edorno='"+mEdorNo+"' and edortype='"+mEdorType+"'";
    	  ExeSQL exeSQL = new ExeSQL();
    	  String grpcontno = exeSQL.getOneValue(sqlString);
    	  /*String mEdorValidate = mDeathDate.substring(0, 7) + "-01";
    	  String sql = "select count(*) from lcinsureacctrace a  where grpcontno='"+grpcontno+"' and exists (select 1 from lcinsureacc where grpcontno=a.grpcontno and polno=a.polno and acctype='001' and insuaccno=a.insuaccno ) and paydate =date('"+mEdorValidate+"') and insuaccno='671803' and MoneyType='MF' and othertype='6' ";
    	  ExeSQL tExeSQL = new ExeSQL();
    	  String resultString = tExeSQL.getOneValue(sql);
    	  if(resultString.equals("0")){
    	        mErrors.addOneError("该保单保全生效日(即身故日)的上个月还未月结，请先进行月结再操作该保全！");
    	        return false;
    	  }*/
    	  boolean flag = PubFun.isDoBQ(grpcontno, mDeathDate);
    	  if(!flag){
    		  mErrors.addOneError("该保单保全生效日(即身故日)的上个月还未月结，请先进行月结再操作该保全！");
  	        return false;
    	  }
    	  return true;
     }
     

    /**
     * 检查是不是有健管的产品
     * @return boolean
     */
    private boolean checkHealthPol()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String appflag = tLCGrpPolSchema.getAppFlag();
            String riskCode = tLCGrpPolSchema.getRiskCode();
            if ((appflag != null) && (appflag.equals("1")) &&
                    (riskCode != null) && (riskCode.equals("170101")))
            {
                mMessage = "本单有" + riskCode + "险种，请打印内部流转单，发送健管人员\n";
                LGLetterSchema tLGLetterSchema = new LGLetterSchema();
                tLGLetterSchema.setEdorAcceptNo(mEdorNo);
                tLGLetterSchema.setSerialNumber(getSerialNumber());
                tLGLetterSchema.setLetterType("1"); //核保函件
                tLGLetterSchema.setLetterSubType("8"); //内部流转
                tLGLetterSchema.setState("0"); //待下发
                tLGLetterSchema.setOperator(mGlobalInput.Operator);
                tLGLetterSchema.setMakeDate(mCurrentDate);
                tLGLetterSchema.setMakeTime(mCurrentTime);
                tLGLetterSchema.setModifyDate(mCurrentDate);
                tLGLetterSchema.setModifyTime(mCurrentTime);
                mMap.put(tLGLetterSchema, "DELETE&INSERT");
                return true;
            }
        }
        return true;
    }

    /**
     * 校验是否发生过理赔
     * @return boolean
     */
    private boolean checkClaim()
    {
        GCheckClaimBL check = new GCheckClaimBL();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                        get(i);
            String grpContNo = tLPDiskImportSchema.getGrpContNo();
            String insuredNo = tLPDiskImportSchema.getInsuredNo();
            String insuredName = tLPDiskImportSchema.getInsuredName();
            Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                        getEdorValiDate());
            if (edorValiDate != null)
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setGrpContNo(grpContNo);
                tLCPolDB.setInsuredNo(insuredNo);
                LCPolSet tLCPolSet = tLCPolDB.query();
                for (int j = 1; j <= tLCPolSet.size(); j++)
                {
                    String polNo = tLCPolSet.get(j).getPolNo();
                    if (check.checkClaimed(polNo))
                    {
                        mMessage += "被保人" + insuredName + tLCPolSet.get(j).getRiskCode()
                                + "险种发生理赔，（该险种期交保费" + tLCPolSet.get(j).getPrem()
                                + "元），请审核录入的退费。\n";
                    }
                }
            }
        }
        return true;
    }

    /**
     * 校验保全生效日期
     * @return boolean
     */
    private boolean checkLastPayToDate()
    {
        try
        {
            for (int i = 1; i <= mLPDiskImportSet.size(); i++)
            {
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                        get(i);
                Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                        getEdorValiDate());
                if ((edorValiDate != null) && (edorValiDate.before(mLastPayToDate)))
                {
                    mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                        "申请的生效日早于当期保费应交日，如有需要，请特殊处理。\n";
                }
            }
        }
        catch (Exception ex)
        {
            mErrors.addOneError("保全生效日期校验出错！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 检验结算频次
     * @return boolean
     */
    private boolean checkBalIntv()
    {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpBalPlanDB.getInfo())
        {
            return true;
        }
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到保全受理信息！");
            return false;
        }
        Date edorAppDate = CommonBL.stringToDate(tLPEdorAppDB.getEdorAppDate());
        if (edorAppDate == null)
        {
            mErrors.addOneError("未找到保全受理日期！");
            return false;
        }
        int balIntv = tLCGrpBalPlanDB.getBalIntv();
//        System.out.println("balIntv" + balIntv);
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.
                    get(i);
            Date edorValiDate = CommonBL.stringToDate(tLPDiskImportSchema.
                    getEdorValiDate());
            if ((edorValiDate != null) && (edorValiDate.before(edorAppDate)))
            {
                int interval = PubFun.calInterval(edorValiDate, edorAppDate,
                        "M");
                System.out.println("interval" + interval);
                if (balIntv == 0)
                {
                    if (interval > 3)
                    {
                        mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                                "的保全生效日期超过保全受理日期三个月！\n";
                    }
                }
                else if ((balIntv == 1) || (balIntv == 3) || (balIntv == 12))
                {
                    if (interval > balIntv)
                    {
                        mMessage += "被保人" + tLPDiskImportSchema.getInsuredName() +
                                "生效日期与受理日期间隔超过保单服务提交周期！";
                    }
                }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!checkEdorValiDate())
        {
            return false;
        }
        //修改批次等一些列表
        setEdorState(BQ.EDORSTATE_INPUT);
        //特殊表
        setSpecialData();
        //修改 lpperson中的 deathdate
        setLPPerson();
        //增加lpbnf
        setLPBnf();
       
        return true;
    }

    private void setLPBnf() {
		mMap.put(mLPBnfSet, "DELETE&INSERT");
	}

	private void setLPPerson() {
		LDPersonDB  tLDPersonDB = new LDPersonDB();
		tLDPersonDB.setCustomerNo(mInsuredNo);
		tLDPersonDB.setCustomerType("SG");
		LDPersonSet tLDPersonSet = tLDPersonDB.query();
		if(tLDPersonSet.size()==1){
			LPPersonSchema tLPPersonSchema = new LPPersonSchema();
			Reflections ref = new Reflections();
			ref.transFields(tLPPersonSchema, tLDPersonSet.get(1));
			tLPPersonSchema.setEdorNo(mEdorNo);
			tLPPersonSchema.setEdorType(mEdorType);
			tLPPersonSchema.setOperator(mGlobalInput.Operator);
			tLPPersonSchema.setModifyDate(PubFun.getCurrentDate());
			tLPPersonSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(tLPPersonSchema, "delete||UPDATE");
		}else {
			mErrors.addOneError("数据库中数据异常");
		}
	}

	/**
     * 把保全状态设为已申请状态
     * @return boolean
     */
    private void setEdorState(String edorState)
    {
        String sql;
        String sql2;
        String sql3;
        String sql4;
        String sql5;
        
        //更新main表
        sql = "update LPGrpEdorMain " +
                "set EdorState = '" + edorState + "', " +
                "    edorvalidate = '" + mDeathDate + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        
        sql2 = "update LPEdorMain " +
        "set EdorState = '" + edorState + "', " +
        "    edorvalidate = '" + mDeathDate + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
        "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
        "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql2, "UPDATE");
        

        //更新item表
        sql3 = "update LPGrpEdorItem  " +
                "set EdorState = '" + edorState + "', " +
                "    edorvalidate = '" + mDeathDate + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        
        mMap.put(sql3, "UPDATE");
        
        sql4 = "update LPEdorItem  " +
        "set EdorState = '" + edorState + "', " +
        "   edorvalidate = '" + mDeathDate + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
        "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
        "where EdorNo = '" + mEdorNo + "' " +
        "and EdorType = '" + mEdorType + "' ";

        mMap.put(sql4, "UPDATE");
        
        //修正lpedorapp
        sql5 = "update LPEdorapp  " +
        "set EdorState = '" + edorState + "', " +
        "    edorappdate = '" + mDeathDate + "', " +
        "    Operator = '" + mGlobalInput.Operator + "', " +
        "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
        "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
        "where edoracceptno = '" + mEdorNo + "' " ;
        mMap.put(sql5, "UPDATE");
    }

    /**
     * 设置保全项目特殊数据
     */
    public void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     * 重疾险种并且缴费频次为趸交的不受保全生效日期不能晚于交至日期的限制。
     * @return boolean
     */
    public static boolean has280101(String grpContNo, String InsuredNo)
    {
        
    	SSRS tSSRS = new ExeSQL().execSQL("select payintv,RISKCODE From lcpol where grpcontno ='"+grpContNo+"' and insuredno ='"+InsuredNo+"' ");
    	for( int i = 1 ; i<= tSSRS.getMaxRow() ; i++)
    	{
    		String payintv = tSSRS.GetText(i, 1);
    		String riskcode = tSSRS.GetText(i, 2);
    		if(payintv.equals("0")&& riskcode.equals("280101"))
    		{
    			return true;
    		}
    	}
        return false;
    }
}
