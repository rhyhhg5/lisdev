package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全交费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author  Lanjun
 * @version 1.0
 */

public class PEdorBFDetailUI
{
    private PEdorBFDetailBL mPEdorBFDetailBL = null;

    public PEdorBFDetailUI()
    {
        mPEdorBFDetailBL = new PEdorBFDetailBL();
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
        if (!mPEdorBFDetailBL.submitData(data, operator))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mPEdorBFDetailBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        String edorNo = "20070111000002";
        String edorType = "BF";

        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("21000019450");
        tLCPolSchema.setMult("2");
        tLCPolSchema.setAmnt("5000000");
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet.add(tLCPolSchema);

        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);
        tSpecialData.setPolNo(tLCPolSchema.getPolNo());
        tSpecialData.add("ValiDateTime", "1");

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo(edorNo);
        tLPEdorItemSchema.setContNo("00002906001");
        tLPEdorItemSchema.setEdorType("BF");

        GlobalInput gi = new GlobalInput();
        gi.Operator = "qq";
        VData data = new VData();
        data.add(gi);
        data.add(tLCPolSet);
        data.add(tLPEdorItemSchema);
        data.add(tSpecialData);
        PEdorBFDetailUI tPEdorBFDetailUI = new PEdorBFDetailUI();
        if (!tPEdorBFDetailUI.submitData(data, "SAVE"))
        {
            System.out.println(tPEdorBFDetailUI.getError());
        }
        else
        {
        }

    }
}
