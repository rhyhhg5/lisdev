package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单/团单下个单被保人重要资料变更项目明细</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @version 1.0
 */
public class PEdorLPDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /**  */
    private LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();

    public PEdorLPDetailBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据校验操作（checkdata)
        if (!checkData())
        {
            return false;
        }

        // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPDiskImportSchema = (LPDiskImportSchema) cInputData.getObjectByObjectName(
                "LPDiskImportSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        if (mLPDiskImportSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("数据传输不完全！"));
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        if(mOperate.equals("INSERT||MAIN"))
        {
            if (!checkBankInfo()) {
                return false;
            }
        }

        if(mOperate.equals("DELETE||MAIN"))
        {
            if(mLPDiskImportSchema.getSerialNo() == null || mLPDiskImportSchema.getSerialNo().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PEdorLPDetailBL";
                tError.functionName = "Preparedata";
                tError.errorMessage = "流水号为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        if(mLPDiskImportSchema.getGrpContNo() == null || mLPDiskImportSchema.getGrpContNo().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "团体合同号为空!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if(mLPDiskImportSchema.getEdorNo() == null || mLPDiskImportSchema.getEdorNo().equals("")
           ||mLPDiskImportSchema.getEdorType() == null || mLPDiskImportSchema.getEdorType().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "批单号或批改类型为空!";
            this.mErrors.addOneError(tError);
            return false;
        }else
        {
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(mLPDiskImportSchema.getEdorNo());
            tLPGrpEdorItemDB.setEdorType(mLPDiskImportSchema.getEdorType());
            if (tLPGrpEdorItemDB.query().size() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PEdorLPDetailBL";
                tError.functionName = "checkData";
                tError.errorMessage = "批单号不存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //通过查询判断客户是否是被保人
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mLPDiskImportSchema.getGrpContNo());
        tLCInsuredDB.setInsuredNo(mLPDiskImportSchema.getInsuredNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询被保险人信息错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCInsuredSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未查到被保险人信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCInsuredSchema = tLCInsuredSet.get(1);

        return true;
    }

    private boolean checkBankInfo()
    {
        String bankCode=mLPDiskImportSchema.getBankCode();
        String bankAccNo=mLPDiskImportSchema.getBankAccNo();
        String accName=mLPDiskImportSchema.getAccName();

        if (bankCode != null && !bankCode.equals("") &&
            bankAccNo != null && !bankAccNo.equals("") &&
            accName != null && !accName.equals(""))
        {
            LDBankDB tLDBankDB=new LDBankDB();
            tLDBankDB.setBankCode(mLPDiskImportSchema.getBankCode());
            if(tLDBankDB.query().size()==0){
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PEdorLPDetailBL";
                tError.functionName = "checkBankInfo";
                tError.errorMessage = "银行代码在系统中未定义!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        } else if ((bankCode == null || bankCode.equals("")) &&
                   (bankAccNo == null || bankAccNo.equals("")) &&
                   (accName == null || accName.equals("")))
        {
            return true;
        }else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorLPDetailBL";
            tError.functionName = "checkBankInfo";
            tError.errorMessage = "银行代码,户名,帐号填写不完整!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData()
    {
        if(mOperate.equals("INSERT||MAIN"))
        {
            if(mLPDiskImportSchema.getSerialNo()==null||
               mLPDiskImportSchema.getSerialNo().equals(""))
            {
                String strSql =
                        "select coalesce(max(cast(serialno as integer)),0)+1 from lpdiskimport" +
                        " where edorno='" + mLPDiskImportSchema.getEdorNo() +
                        "' and edortype='" + mLPDiskImportSchema.getEdorType() +
                        "'";
                ExeSQL tExeSQL = new ExeSQL();
                String serialNo = tExeSQL.getOneValue(strSql);
                mLPDiskImportSchema.setSerialNo(serialNo);
            }

            mLPDiskImportSchema.setInsuredName(mLCInsuredSchema.getName());
            mLPDiskImportSchema.setSex(mLCInsuredSchema.getSex());
            mLPDiskImportSchema.setBirthday(mLCInsuredSchema.getBirthday());
            mLPDiskImportSchema.setIDType(mLCInsuredSchema.getIDType());
            mLPDiskImportSchema.setIDNo(mLCInsuredSchema.getIDNo());
            mLPDiskImportSchema.setState("1");
            mLPDiskImportSchema.setMoney(0.0);
            mLPDiskImportSchema.setOperator(mGlobalInput.Operator);
            mLPDiskImportSchema.setMakeDate(PubFun.getCurrentDate());
            mLPDiskImportSchema.setMakeTime(PubFun.getCurrentTime());
            mLPDiskImportSchema.setModifyDate(PubFun.getCurrentDate());
            mLPDiskImportSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(mLPDiskImportSchema, "DELETE&INSERT");
        }
        if(mOperate.equals("DELETE||MAIN"))
        {
            LPDiskImportSchema tLPDiskImportSchema=new LPDiskImportSchema();
            tLPDiskImportSchema.setEdorNo(mLPDiskImportSchema.getEdorNo());
            tLPDiskImportSchema.setEdorType(mLPDiskImportSchema.getEdorType());
            tLPDiskImportSchema.setGrpContNo(mLPDiskImportSchema.getGrpContNo());
            tLPDiskImportSchema.setSerialNo(mLPDiskImportSchema.getSerialNo());
            mMap.put(tLPDiskImportSchema, "DELETE");
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mMap);
        return true;
    }
}
