//程序名称：BqDeleteRedundanceDataBL.java
//程序功能：保全增人被保人冗余数据维护
//创建日期：20190108
//创建人  ：ly

package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqDeleteRedundanceDataBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	// 录入
	private String mGrpContNo = null;

	private String mInsuredNo = null;

	private String mContNo = null;

	private MMap mMap = new MMap();

	public BqDeleteRedundanceDataBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		// 判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BqDeleteRedundanceDataBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败BqDeleteRedundanceDataBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start BqDeleteRedundanceDataBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BqDeleteRedundanceDataBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);

		mGrpContNo = (String) tTransferData.getValueByName("TGrpContNo");

		mInsuredNo = (String) tTransferData.getValueByName("TInsuredNo");

		return true;
	}

	private boolean dealData() {
		// 查询被保人是否存在
		String isInsuredSQL = "select 1 from lcinsured a where grpcontno  = '"
				+ mGrpContNo
				+ "' and insuredno = '"
				+ mInsuredNo
				+ "'"
				+ "and  exists(select 1 from lcget where grpcontno  = a.grpcontno and insuredno = a.insuredno)"
				+ "and  exists(select 1 from lcprem where grpcontno  = a.grpcontno )"
				+ "and  not exists(select 1 from lccont where grpcontno  = a.grpcontno and insuredno = a.insuredno)"
				+ "and  not exists(select 1 from lcpol where grpcontno  = a.grpcontno and insuredno = a.insuredno)"
				+ "and  exists (select grpcontno,contno from lcget b where a.grpcontno = b.grpcontno and not exists(select contno from lcduty where contno = b.contno) )";
		ExeSQL tExeSQL = new ExeSQL();
		String insuredResult = tExeSQL.getOneValue(isInsuredSQL);
		if (insuredResult == null || insuredResult == "") {
			return false;
		}
		// 对lcinsured,lcget,lcprem根据条件执行删除操作
		String deLcinsuredSQL = "delete from lcinsured where grpcontno  = '"
				+ mGrpContNo + "' and insuredno = '" + mInsuredNo + "'";
		String deLcgetSQL = "delete from lcget " + "where grpcontno  = '"
				+ mGrpContNo + "' and insuredno = '" + mInsuredNo + "'";

		String ContNoSQL = "select contno from lcinsured "
				+ "where grpcontno  = '" + mGrpContNo + "' and insuredno = '"
				+ mInsuredNo + "'";
		mContNo = tExeSQL.getOneValue(ContNoSQL);
		String deLcpremSQL = "delete from lcprem a where contno  = '" + mContNo
				+ "'";
		mMap.put(deLcpremSQL, SysConst.DELETE);
		mMap.put(deLcinsuredSQL, SysConst.DELETE);
		mMap.put(deLcgetSQL, SysConst.DELETE);
		return true;
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BqDeleteRedundanceDataBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
