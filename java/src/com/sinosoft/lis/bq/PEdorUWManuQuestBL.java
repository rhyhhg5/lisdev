package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 问题件录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuQuestBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPIssuePolSchema mLPIssuePolSchema = null;

    private String mOperate = null;

    private String mEdorNo = null;

    /** 核保问题件和EdorType无关，这里统一存UW */
    private String mEdorType = BQ.EDORTYPE_UW;

    private String mContNo = null;

    private String mPrtNo = null;

    private String mProposalContNo = null;

    private String mPrtSeq = null;

    private String mManageCom = null;

    private String mAgentCode = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        mOperate = operate;
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回体检通知书号
     * @return String
     */
    public String getPrtSeq()
    {
        return mPrtSeq;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPIssuePolSchema = (LPIssuePolSchema) data.
                    getObjectByObjectName("LPIssuePolSchema", 0);
            mEdorNo = mLPIssuePolSchema.getEdorNo();
            mContNo = mLPIssuePolSchema.getContNo();
            LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
            mProposalContNo = tLCContSchema.getProposalContNo();
            mPrtNo = tLCContSchema.getPrtNo();
            mManageCom = tLCContSchema.getManageCom();
            mAgentCode = tLCContSchema.getAgentCode();
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (mOperate.equals("INSERT||MAIN"))
        {
            setLPIssuePol();
        }
        else if (mOperate.equals("UPDATE||MAIN"))
        {
            updateLPIssuePol();
            setPrtManager(); //设置打印任务
        }
        else if (mOperate.equals("DELETE||MAIN"))
        {
            deleteLPIssuePol();
        }
        return true;
    }

    /**
     * 产生打印号
     * @return String
     */
    private String createPrtSeq()
    {
//        LPIssuePolDB tLPIssuePolDB = new LPIssuePolDB();
//        tLPIssuePolDB.setEdorNo(mEdorNo);
//        tLPIssuePolDB.setContNo(mContNo);
//        LPIssuePolSet tLPIssuePolSet = tLPIssuePolDB.query();
//        if (tLPIssuePolSet.size() != 0)
//        {
//            String prtNo = tLPIssuePolSet.get(1).getPrtSeq();
//            if (prtNo != null)
//            {
//                return prtNo;
//            }
//        }
        return PubFun1.CreateMaxNo("BQIPAYNOTICENO", mPrtNo);
//        return PubFun1.CreateMaxNo("PRTSEQNO","SN");
    }

    /**
     * 保存问题件
     */
    private void setLPIssuePol()
    {
//        String sql = "delete from LPPENotice " +
//                "where EdorNo = '" + mEdorNo + "' " +
//                "and CustomerNo = '" + mInsuredNo + "' ";
//        mMap.put(sql, "DELETE");
        mLPIssuePolSchema.setEdorNo(mEdorNo);
        mLPIssuePolSchema.setEdorType(mEdorType);
        mLPIssuePolSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPIssuePolSchema.setProposalContNo(mProposalContNo);
        mLPIssuePolSchema.setPrtSeq(mPrtSeq);
        mLPIssuePolSchema.setSerialNo(PubFun1.CreateMaxNo("BQQUEST", 10));
        mLPIssuePolSchema.setState("0");
        mLPIssuePolSchema.setManageCom(mManageCom);
        mLPIssuePolSchema.setOperator(mGlobalInput.Operator);
        mLPIssuePolSchema.setMakeDate(mCurrentDate);
        mLPIssuePolSchema.setMakeTime(mCurrentTime);
        mLPIssuePolSchema.setModifyDate(mCurrentDate);
        mLPIssuePolSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPIssuePolSchema, "DELETE&INSERT");
    }

    private void deleteLPIssuePol()
    {
        mLPIssuePolSchema.setEdorType(mEdorType);
        mLPIssuePolSchema.setProposalContNo(mProposalContNo);
        mMap.put(mLPIssuePolSchema, "DELETE");
    }

    private void updateLPIssuePol()
    {
        mPrtSeq = createPrtSeq();
        String sql = "update LPIssuePol " +
                "set PrtSeq = '" + mPrtSeq + "', " +
                "    State = '1', " +
//                "    MakeDate = '" + mCurrentDate + "', " +
//                "    MakeTime = '" + mCurrentTime + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and ContNo = '" + mContNo + "' " +
                "and State = '0'";
        mMap.put(sql, "UPDATE");
    }

    private void setPrtManager()
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(mPrtSeq);
        tLOPRTManagerSchema.setOtherNo(mEdorNo);
        tLOPRTManagerSchema.setOtherNoType("03");
        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PEdorISSUE);
        tLOPRTManagerSchema.setManageCom(mManageCom);
        tLOPRTManagerSchema.setAgentCode(mAgentCode);
        tLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag(BQ.PRINTFLAG_N); //未打印
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        tLOPRTManagerSchema.setPrintTimes(0);
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
