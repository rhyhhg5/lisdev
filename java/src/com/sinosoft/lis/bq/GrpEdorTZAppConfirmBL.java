package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.ulitb.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 新增被保人保全申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */
public class GrpEdorTZAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 新契约算费结果 */
    private LCPolSet mLCPolSet = new LCPolSet();

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = BQ.EDORTYPE_NI;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保单生效日期 */
    private String mCInValiDate = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    /** 保费计算方式 */
    private String mCalType = null;

    /** 按月算还是按日算 */
    private String mCalTime = null;

    /** 是否保费还原 */
    private String mRevertType = null;

    /** 补费金额 */
    private double mGetMoney = 0.00;

    /** 特需医疗险种标志 */
    private boolean mSpecialRiskFlag = false;

    /** 非特需普通险种标志 */
    private boolean mNotSpecialRiskFlag = true;

    /** 是否设置个人账户 */
    private String mCreateAccFlag = null;

    /** 个人账户资金来源 */
    private String mSource = null;

    private Set mInsuredNoSet = new HashSet();

    private Map mInsuredPremMap = new HashMap();

    private Map mInsuredNumMap = new HashMap();

    private Map mInsuredRemainRealPremMap = new HashMap();

    private ExeSQL mExeSQL = new ExeSQL();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                    getObjectByObjectName(
                    "LPGrpEdorItemSchema", 0);
            mEdorNo = edorItem.getEdorNo();
            mEdorType = edorItem.getEdorType();
            mGrpContNo = edorItem.getGrpContNo();
            mEdorValiDate = edorItem.getEdorValiDate();
            //得到保险终止日期
            mCInValiDate = getCInValiDate(mGrpContNo);

            mSpecialData = new EdorItemSpecialData(edorItem);
            mSpecialData.query();
            mCalType = mSpecialData.getEdorValue("CalType");
            mRevertType = mSpecialData.getEdorValue("RevertType");
            mCalTime = mSpecialData.getEdorValue("CalTime");
            mCreateAccFlag = mSpecialData.getEdorValue("CreateAccFlag");
            mSource = mSpecialData.getEdorValue("Source");
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入数据错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	RSWrapper rsWrapper = new RSWrapper();

    	String sql = "select distinct contno from lcinsuredlist where grpcontno = '" + mGrpContNo + "' "
    	           + "and edorno = '" + mEdorNo + "' and state = '0'";
    	System.out.println(sql);


        if (!rsWrapper.prepareData(null, sql))
        {
            System.out.println("处理数据准备失败! ");
            return false;
        }

        SSRS tSSRS = rsWrapper.getSSRS();
    	if (tSSRS.getMaxRow() > 0)
        {
	    	for (int i = 1 ; i<= tSSRS.getMaxRow(); i++)
	    	{
	    		mMap = new MMap();
	    		mLCPolSet.clear();
	            mInsuredNoSet.clear();
	            mInsuredPremMap.clear();
	            mInsuredNumMap.clear();
	            mInsuredRemainRealPremMap.clear();
	    		String tContNo = tSSRS.GetText(i, 1);
	    		if (!calPrem(tContNo))
	            {
	                System.out.println("1");
	                return false;

	            }
		        if(!submitMap(mMap))
		        {
		        	return false;
		        }
	    	}
        }
    	rsWrapper.close();
    	//校验数据完整性
    	if(!checkDataWanZheng())
    	{
    		return false;
    	}

    	mMap = new MMap();
    	/**
         * 处理特需医疗险种
         * @return boolean
         */
    	if (!dealSpecialRisk())
        {
            return false;
        }

        setGrpEdorItem();
 //       setInsuredList();
        setContFlag();
        setPolFlag();
        setEdorValiDate();

        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 处理特需医疗险种
     * @return boolean
     */
    private boolean dealSpecialRisk()
    {
        		mLCPolSet.clear();
            	LCPolDB tLCPolDB = new LCPolDB();
            	String sql = "select a.* from lcpol a,lcinsuredlist b where a.contno = b.contno "
            		       + "and a.insuredno = b.insuredno and b.edorno = '" + mEdorNo + "' "
            		       + "and b.grpcontno = '" + mGrpContNo + "' and b.state = '1' and (b.contplancode <> 'FM' or b.contplancode is null) with ur";
            	mLCPolSet.add(tLCPolDB.executeQuery(sql));
            	//追加保费
        		if ((mCreateAccFlag.equals("1")) && (mSource.equals("1")))
                {
                    if (!calSpecialEdorPrem())
                    {
                        return false;
                    }
                }
/*                //团体账户转入
                if (mCreateAccFlag.equals("1") && (mSource.equals("2")))
                {
                	 if (!calSpecialPrem())
                     {
                         return false;
                     }
                }
                //不建立个人账户
                if (this.mCreateAccFlag.equals("2"))
                {
                    for (int i = 1; i <= mLCPolSet.size(); i++)
                    {
                        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
                        double edorPrem = 0.0;
                        if (!setGetEndorse(edorPrem, tLCPolSchema))
                        {
                            return false;
                        }
                        if (!setLCPrem(edorPrem, tLCPolSchema))
                        {
                            return false;
                        }
                        mInsuredNoSet.add(tLCPolSchema.getInsuredNo());
                        mGetMoney += edorPrem;
                    }
                    //总管理费
                    double totalfee = 0.00;
                    EdorItemSpecialData specialData = new EdorItemSpecialData(mEdorNo,mEdorType);
                    specialData.add("ManageFee", String.valueOf(totalfee));
                    mMap.put(specialData.getSpecialDataSet(), "DELETE&INSERT");
                }*/

        
        return true;
    }

	/**
     * 得到增人导入的总保费
     * @return double
     */
    private double getImportMoney()
    {
        double importMoney = 0.00;
        String sql = "select sum(b.appntprem)+sum(b.personprem)+sum(b.personownprem) from lcpol a,lcinsuredlist b where a.contno = b.contno "
		       + "and a.insuredno = b.insuredno and b.edorno = '" + mEdorNo + "' "
		       + "and b.grpcontno = '" + mGrpContNo + "' and b.state = '1' and (b.contplancode <> 'FM' or b.contplancode is null) ";
        String sumPublicAcc = mExeSQL.getOneValue(sql);
        importMoney = CommonBL.carry(sumPublicAcc);
        return importMoney;
    }

    /**
     * 特需医疗保全算费-如果只含有特需险种-团体账户转入
     * @return boolean
     */
    private boolean calSpecialPrem()
    {
        //得到增人导入的总保费
        double importMoneyTotal = getImportMoney();
        //得到团体理赔帐户中的余额
        double grpAccountMoney = CommonBL.getGrpAccountMoney(mGrpContNo);
        //管理费率
        double rate = CommonBL.getManageFeeRate(mLCPolSet.get(1).getGrpPolNo(), BQ.FEECODE_INSURED);
        //总管理费
        double totalfee = 0.00;
        //如果团体理赔帐户中的保费不够
        if (grpAccountMoney < importMoneyTotal)
        {
            double sumEdorPrem = importMoneyTotal - grpAccountMoney;
            totalfee = CommonBL.carry(sumEdorPrem * rate);
            calEdorPrem(sumEdorPrem,totalfee);
        }
        else
        {
            for (int i = 1; i <= mLCPolSet.size(); i++)
            {
                LCPolSchema tLCPolSchema = mLCPolSet.get(i);
                double edorPrem = 0.0;
                if (!setGetEndorse(edorPrem, tLCPolSchema))
                {
                    return false;
                }
                String sql = "select PublicAcc from lcinsuredlist  where "
       		       + " edorno = '" + mEdorNo + "' and grpcontno = '" + mGrpContNo
       		       + "' and state = '1' and contplancode <> 'FM' "
       		       + "and insuredno = '" + tLCPolSchema.getInsuredNo() + "' ";
                String importMoney = mExeSQL.getOneValue(sql);
                double prem = CommonBL.carry(importMoney);
                if (!setLCPrem(prem, tLCPolSchema))
                {
                   return false;
                }
                mInsuredNoSet.add(tLCPolSchema.getInsuredNo());
            }
        }
        EdorItemSpecialData specialData = new EdorItemSpecialData(mEdorNo,mEdorType);
        specialData.add("ManageFee", String.valueOf(totalfee));
        mMap.put(specialData.getSpecialDataSet(), "DELETE&INSERT");
        return true;
    }

    /**
     * 特需医疗保全算费
     * @param sumPrem double
     * @param totalfee double
     * @return boolean
     */
    private boolean calEdorPrem(double sumEdorPrem,double totalfee)
    {
        //把应补费总额平均分配到每个人
        double averagePrem = CommonBL.carry(sumEdorPrem / mLCPolSet.size());
        //把管理费也总额平均分配到每个人
//        double averageFee = CommonBL.carry(totalfee / mLCPolSet.size());

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i);
            double edorPrem = averagePrem;
//            double manageFee = averageFee;
            if (i == mLCPolSet.size())
            { //对最后一个人作特殊处理，消除误差
                edorPrem = CommonBL.carry(sumEdorPrem -(averagePrem * (mLCPolSet.size() - 1)));
//                manageFee = CommonBL.carry(totalfee -(averageFee * (mLCPolSet.size() - 1)));
            }
            if (!setGetEndorse(edorPrem, tLCPolSchema))
            {
                return false;
            }
            String sql = "select PublicAcc from lcinsuredlist  where "
  		       + " edorno = '" + mEdorNo + "' and grpcontno = '" + mGrpContNo
  		       + "' and state = '1' and contplancode <> 'FM' "
  		       + "and insuredno = '" + tLCPolSchema.getInsuredNo() + "' ";
            String importMoney = mExeSQL.getOneValue(sql);
            double prem = CommonBL.carry(importMoney);// - manageFee;
            if (!setLCPrem(prem, tLCPolSchema))
            {
                return false;
            }
            mInsuredNoSet.add(tLCPolSchema.getInsuredNo());
            mGetMoney += edorPrem;
        }
        return true;
    }

    /**
     * 得到保险计划
     * @return String
     */
    private String getContPlanCode(LCPolSchema aLCPolSchema)
    {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(mEdorNo);
        tLCInsuredListDB.setGrpContNo(mGrpContNo);
        tLCInsuredListDB.setInsuredName(aLCPolSchema.getInsuredName());
        tLCInsuredListDB.setSex(aLCPolSchema.getInsuredSex());
        tLCInsuredListDB.setBirthday(aLCPolSchema.getInsuredBirthday());
        LCInsuredListSet tLCInsuredListSet = tLCInsuredListDB.query();
        if (tLCInsuredListSet.size() == 0)
        {
            return null;
        }
        return tLCInsuredListSet.get(1).getContPlanCode();
    }

    /**
     * 得到磁盘导入的计算方向
     * @param aLCPolSchema LCPolSchema
     * @return String
     */
    private String getImportCalRule(LCPolSchema aLCPolSchema)
    {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(mEdorNo);
        tLCInsuredListDB.setGrpContNo(aLCPolSchema.getGrpContNo());
        tLCInsuredListDB.setInsuredName(aLCPolSchema.getInsuredName());
        tLCInsuredListDB.setSex(aLCPolSchema.getInsuredSex());
        tLCInsuredListDB.setBirthday(aLCPolSchema.getInsuredBirthday());
        LCInsuredListSet tLCInsuredListSet = tLCInsuredListDB.query();
        if (tLCInsuredListSet.size() == 0)
        {
            return null;
        }
        return tLCInsuredListSet.get(1).getCalRule();
    }

    /**
     * 得到被保人清单
     * @return LCInsuredListSet
     */
    private LCInsuredListSet getInsuredList()
    {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(mEdorNo);
        tLCInsuredListDB.setGrpContNo(mGrpContNo);
        return tLCInsuredListDB.query();
    }

    /**
     * 计算保费，调用新契约程序
     * @return boolean
     */
    private boolean calPrem()
    {
        LCInsuredListSet tLCInsuredListSet = getInsuredList();
        if (tLCInsuredListSet.size() == 0)
        {
            mErrors.addOneError("未找到导入的被保人清单！");
            return false;
        }
        String batchNo = tLCInsuredListSet.get(1).getBatchNo();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("Flag", "BQ");
        tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
        tTransferData.setNameAndValue("EdorNo", mEdorNo);
        tTransferData.setNameAndValue("EdorType", mEdorType);
        tTransferData.setNameAndValue("FileName", batchNo);
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        ParseGuideIn tPGI = new ParseGuideIn();
        if (!tPGI.submitData(tVData, "INSERT||DATABASE"))
        {
            mErrors.copyAllErrors(tPGI.mErrors);
            return false;
        }
        mLCPolSet = tPGI.getEdorNiResult();
        if (mLCPolSet.size() == 0)
        {
            mErrors.addOneError("期交保费计算出错！");
            return false;
        }
        mMap.add(tPGI.getSubmitData());
        return true;
    }

    /**
     * 按险种分别算费
     * @return boolean
     */
    private boolean calEdorPrem()
    {
        try
        {
            for (int i = 1; i <= mLCPolSet.size(); i++)
            {
                LCPolSchema tLCPolSchema = mLCPolSet.get(i);
                String insuredNo = tLCPolSchema.getInsuredNo();
                String prem = String.valueOf(tLCPolSchema.getPrem());
                String number = "1";
                if (mInsuredPremMap.containsKey(insuredNo))
                {
                    prem = String.valueOf(Double.parseDouble((String)
                            mInsuredPremMap.get(insuredNo))
                            + tLCPolSchema.getPrem());
                    number = String.valueOf(Integer.parseInt((String)
                            mInsuredNumMap.get(insuredNo)) + 1);
                }
                mInsuredPremMap.put(insuredNo, prem);
                mInsuredNumMap.put(insuredNo, number);
            }

            for (int i = 1; i <= mLCPolSet.size(); i++)
            {
                double edorPrem =0.0;
                LCPolSchema tLCPolSchema = mLCPolSet.get(i);
                if(tLCPolSchema.getRiskCode().equals("280101"))
                {
                    edorPrem = tLCPolSchema.getStandPrem();
                }else
                {
                    edorPrem = CommonBL.carry(calculate(tLCPolSchema));
                }
                if (!setGetEndorse(edorPrem, tLCPolSchema))
                {
                    return false;
                }
                mInsuredNoSet.add(tLCPolSchema.getInsuredNo());
                mGetMoney += edorPrem;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 得到终止日期
     * @param grpContNo String
     * @return String
     */
    private String getCInValiDate(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            return null;
        }
        return tLCGrpContDB.getCInValiDate();
    }

    /**
     * 判断是否是保全导入保费
     * @param aLCPolSchema LCPolSchema
     * @return boolean 如果是导保费则返回true
     */
    private boolean isImportPrem(LCPolSchema aLCPolSchema)
    {
        String calRule = getImportCalRule(aLCPolSchema);
        if ((calRule != null) && (calRule.equals(BQ.CALRULE_IMPORT)))
        {
            return true;
        }
        return false;
    }

    /**
     * 计算导入保费，导入的是保险计划的总保费，现在则按比例分配到每个险种
     * @param aLCPolSchema LCPolSchema
     * @return String
     */
    private String calImportPrem(LCPolSchema aLCPolSchema)
    {
        double insuredPrem = 0.0;
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            String insuredNo = mLCPolSet.get(i).getInsuredNo();
            if (insuredNo.equals(aLCPolSchema.getInsuredNo()))
            {
                insuredPrem += mLCPolSet.get(i).getPrem();
            }
        }
        return String.valueOf(insuredPrem);
    }

    /**
     * 保全计算未满期保费
     * @param aLCPolSchema LCPolSchema
     * @return String
     */
    private String calculate(LCPolSchema aLCPolSchema)
            throws Exception
    {
        //得到计算方向
        String calRule = CommonBL.getCalRule(mGrpContNo,
                getContPlanCode(aLCPolSchema),
                aLCPolSchema.getRiskCode());
        if ((calRule == null) || (calRule.equals("")))
        {
            mErrors.addOneError("找不到" + aLCPolSchema.getRiskCode()
                    + "险种的保费计算方向！");
            throw new Exception();
        }

        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(mEdorNo);
        tLCInsuredListDB.setGrpContNo(mGrpContNo);
        tLCInsuredListDB.setInsuredName(aLCPolSchema.getInsuredName());
        tLCInsuredListDB.setSex(aLCPolSchema.getInsuredSex());
        tLCInsuredListDB.setBirthday(aLCPolSchema.getInsuredBirthday());
        LCInsuredListSet tLCInsuredListSet = tLCInsuredListDB.query();
        if (tLCInsuredListSet.size() == 0)
        {
            mErrors.addOneError("未找到导入的被保人" +
                    aLCPolSchema.getInsuredName() + "的信息!");
            throw new Exception();
        }
        //如果导入期交保费
        //double prem = tLCInsuredListSet.get(1).getPublicAcc();
        // if (prem != 0)
        //{
        //    calRule = "4"; //磁盘导入
        //}
        // else
        // {
        //     prem = aLCPolSchema.getPrem();
        // }
        //如果导入应收保费
        String edorPrem = tLCInsuredListSet.get(1).getEdorPrem();
        if ((edorPrem != null) && (!edorPrem.equals("")))
        {
            String insuredNo = aLCPolSchema.getInsuredNo();
            String sumPrem = (String) mInsuredPremMap.get(insuredNo);
            if (Double.parseDouble(sumPrem) == 0)
            {
                return edorPrem;
            }
            double realEdorPrem = 0.0;
            if (!mInsuredRemainRealPremMap.containsKey(insuredNo))
            {
                mInsuredRemainRealPremMap.put(insuredNo, edorPrem);
            }
            if (!((String) mInsuredNumMap.get(insuredNo)).equals("1"))
            {
                realEdorPrem = CommonBL.carry(Double.parseDouble(
                        edorPrem) * (aLCPolSchema.getPrem() /
                        Double.parseDouble(sumPrem)));
                String totalRealEdorPrem = String.valueOf(Double.parseDouble((
                        String)
                        mInsuredRemainRealPremMap.get(insuredNo))
                        - realEdorPrem);
                String number = String.valueOf(Integer.parseInt((String)
                        mInsuredNumMap.get(insuredNo))
                        - 1);
                mInsuredRemainRealPremMap.put(insuredNo, totalRealEdorPrem);
                mInsuredNumMap.put(insuredNo, number);
            }
            else
            {
                realEdorPrem = Double.parseDouble((String)
                        mInsuredRemainRealPremMap.get(insuredNo));
            }

            return String.valueOf(realEdorPrem);
        }
        String edorValiDate = tLCInsuredListSet.get(1).getEdorValiDate();
        if ((edorValiDate == null) || (edorValiDate.equals("")))
        {
            edorValiDate = mEdorValiDate;
        }
        String prem = tLCInsuredListSet.get(1).getPrem();
        if ((prem == null) || (prem.equals("")))
        {
            prem = String.valueOf(aLCPolSchema.getPrem());
        }
        LJAPayGrpSchema tLJAPayGrpSchema = CommonBL.getPayToDate(aLCPolSchema.
                getGrpPolNo());
        Calculator cal = new Calculator();
        cal.setCalCode(CommonBL.getCalCode(mEdorType, aLCPolSchema.getRiskCode()));
        cal.addBasicFactor("CalRule", calRule);
        cal.addBasicFactor("CValiDate", aLCPolSchema.getCValiDate());
        cal.addBasicFactor("CInValiDate", mCInValiDate);
        cal.addBasicFactor("InsuYear", String.valueOf(aLCPolSchema.getInsuYear()));
        cal.addBasicFactor("StandPrem",
                String.valueOf(aLCPolSchema.getStandPrem()));
        cal.addBasicFactor("Prem", prem);
        cal.addBasicFactor("CalType", mCalType);
        cal.addBasicFactor("RevertType", mRevertType);
        cal.addBasicFactor("CalTime", mCalTime);
        cal.addBasicFactor("LastPayToDate", tLJAPayGrpSchema.getLastPayToDate());
        cal.addBasicFactor("PayToDate", tLJAPayGrpSchema.getCurPayToDate());
        cal.addBasicFactor("EdorValiDate", edorValiDate);
        return cal.calculate();
    }

    /**
     * 设置合同状态，同时把保全生效日期设置为保单生效日期
     */
    private void setContFlag()
    {
        String sql = "update LCCont " +
                "set UWFlag = '9', " +
                "    ApproveFlag = '9', " +
                //        "    CValiDate = '" + mEdorValiDate + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
                "and ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null)) ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置险种状态，同时把保全生效日期设置为保单生效日期
     */
    private void setPolFlag()
    {
        String sql = "update LCPol " +
                "set UWFlag = '9', " +
                "    ApproveFlag = '9', " +
                //   "    CValiDate = '" + mEdorValiDate + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and AppFlag = '" + BQ.APPFLAG_EDOR + "' " +
                "and ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null)) ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 更新InsuredList表的保费
     * @param edorPrem String
     * @param aLCPolSchema LCPolSchema
     */
    private void setInsuredList()
    {
        for (Iterator iter = mInsuredNoSet.iterator(); iter.hasNext(); )
        {
            String insuredNo = (String) iter.next();
            String sql = "update LCInsuredList " +
                    "set PublicAcc = (select sum(GetMoney) from LJSGetEndorse " +
                    "    where EndorsementNo = '" + mEdorNo + "' " +
                    "    and FeeOperationType = '" + mEdorType + "'" +
                    "    and GrpContNo = '" + mGrpContNo + "' " +
                    "    and InsuredNo = '" + insuredNo + "') " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and GrpContNo = '" + mGrpContNo + "' " +
                    "and InsuredNo = '" + insuredNo + "'";
            mMap.put(sql, "UPDATE");
        }
    }

    /**
     * 设置item表中的费用和状态，其中ChgPrem 字段已经不用，所以用来存储特需的变动保费。
     */
    private void setGrpEdorItem()
    {
        String sql = "update LPGrpEdorItem " +
                "set GetMoney = " +
                "(select sum(GetMoney) from LJSGetEndorse where EndorsementNo = '" + mEdorNo +
                "' and FeeOperationType = '" + mEdorType + "' and GrpContNo = '" + mGrpContNo + "' ), " +
                "ChgPrem = " + String.valueOf(getImportMoney()) + ", " +
            //    "ChgPrem = (select sum(Prem) from LCPol ,lmriskapp" +
            //    "    where GrpContNo = '" + mGrpContNo + "' " +
            //    "    and AppFlag = '" + BQ.APPFLAG_EDOR +
            //    "' and LCPol.riskcode = LMRiskApp.riskcode and LMRiskApp.Risktype3 = '7' ), " +
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "UWFlag = '" + BQ.UWFLAG_PASS + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    private void setEdorValiDate()
    {
        String sql = "update LCCont set CValiDate = " +
                "  (select EdorValiDate from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and ContNo = LCCont.ContNo and relation = '00' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "    and (ContPlanCode <> 'FM' or ContPlanCode is null) and relation = '00') ";
        mMap.put(sql, "UPDATE");
        sql = "update LCPol set (CValiDate, GetStartDate) = " +
                "  (select EdorValiDate, EdorValiDate from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and ContNo = LCPol.ContNo " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) fetch first 1 row only ) " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) " +
                "and InsuredNo in (select InsuredNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) ";
        mMap.put(sql, "UPDATE");
        sql = "update LCDuty set GetStartDate = " +
                "  (select EdorValiDate from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and ContNo = LCDuty.ContNo " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null)  fetch first 1 row only ) " +
                "where ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) " +
                "and PolNo in (select a.polno from LCPol a,lcinsuredlist b "+
                "   where b.GrpContNo = '" + mGrpContNo + "' " +
                "   and b.EdorNo = '" + mEdorNo + "' " +
                "   and b.ContNo = a.ContNo and b.insuredno = a.insuredno " +
                "   and (b.ContPlanCode <> 'FM' or b.ContPlanCode is null) )";
        mMap.put(sql, "UPDATE");
        sql = "update LCGet set GetStartDate = " +
                "  (select EdorValiDate from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and ContNo = LCGet.ContNo " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) fetch first 1 row only ) " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and ContNo in (select ContNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) " +
                "   and InsuredNo in (select InsuredNo from LCInsuredList " +
                "   where GrpContNo = '" + mGrpContNo + "' " +
                "   and EdorNo = '" + mEdorNo + "' " +
                "   and (ContPlanCode <> 'FM' or ContPlanCode is null) ) ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(double edorPrem, LCPolSchema aLCPolSchema)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        //String finaType = BqCalBL.getFinType(BQ.EDORTYPE_NI,
        //        aLCPolSchema.getRiskCode());
        //if (finaType.equals(""))
        //{
        //    mErrors.addOneError("未找到财务类型编码！");
        //    return false;
        //}
        String finaType = "BF";
        tLJSGetEndorseSchema.setFeeFinaType(finaType);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "INSERT");
        System.out.println("insuredName:" + aLCPolSchema.getInsuredName());
        return true;
    }

    /**
     * 得到被保人清单
     * param String aContNo
     * @return LCInsuredListSet
     */
    private LCInsuredListSet getInsuredList(String aContNo)
    {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(mEdorNo);
        tLCInsuredListDB.setGrpContNo(mGrpContNo);
        if(!"null".equals(aContNo) && !"".equals(aContNo) && null!=aContNo)
        {
        	tLCInsuredListDB.setContNo(aContNo);
        }
        tLCInsuredListDB.setState("0");
        return tLCInsuredListDB.query();
    }

    /**
     * 计算保费，调用新契约程序
     * param String aContNo
     * @return boolean
     */
    private boolean calPrem(String aContNo)
    {
        LCInsuredListSet tLCInsuredListSet = getInsuredList(aContNo);
        if (tLCInsuredListSet.size() == 0)
        {
            mErrors.addOneError("未找到保单"+aContNo+"的被保人清单！");
            return false;
        }
        String batchNo = tLCInsuredListSet.get(1).getBatchNo();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("Flag", "BQ");
        tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
        tTransferData.setNameAndValue("EdorNo", mEdorNo);
        tTransferData.setNameAndValue("EdorType", mEdorType);
        tTransferData.setNameAndValue("FileName", batchNo);
        tTransferData.setNameAndValue("ContNo", aContNo);
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        ParseGuideIn tPGI = new ParseGuideIn();
        if (!tPGI.submitData(tVData, "INSERT||DATABASE"))
        {
            mErrors.copyAllErrors(tPGI.mErrors);
            return false;
        }
        mLCPolSet = tPGI.getEdorNiResult();
        if (mLCPolSet.size() == 0)
        {
            mErrors.addOneError("期交保费计算出错！");
            return false;
        }
        mMap.add(tPGI.getSubmitData());
        return true;
    }

    private boolean submitMap(MMap map)
    {
    	VData vd = new VData();
    	vd.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(vd,""))
        {
        	mErrors.addOneError("数据提交失败！");
                return false;
         }
        return true;
    }

    private boolean checkDataWanZheng()
    {
    	LCInsuredListSet tLCInsuredListSet = getInsuredList("");
    	if (tLCInsuredListSet.size() != 0)
        {
            mErrors.addOneError("数据校验：理算数据不完整，请重新理算！");
            return false;
        }
    	return true;
    }

    /**
     * 特需医疗保全算费-如果只含有特需险种-追加保费
     * @param sumPrem double
     * @return boolean
     */
    private boolean calSpecialEdorPrem()
    {
    	//管理费率
        double rate = CommonBL.getManageFeeRate(mLCPolSet.get(1).getGrpPolNo(),"671401");// BQ.FEECODE_INSURED);
        //增人导入的总保费
        double importMoneyTotal = getImportMoney();
        //总管理费
        double totalfee = CommonBL.carry(importMoneyTotal * rate);
        //临时数据，记录已经计算出的总管理费
//        double tempfee = 0.00;

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i);
            String sql = "select sum(appntprem)+sum(personprem)+sum(personownprem) from lcinsuredlist  where "
 		       + " edorno = '" + mEdorNo + "' and grpcontno = '" + mGrpContNo
 		       + "' and state = '1' and (contplancode <> 'FM' or contplancode is null) "
 		       + "and insuredno = '" + tLCPolSchema.getInsuredNo() + "' ";
            String importMoney = mExeSQL.getOneValue(sql);
            double edorPrem = CommonBL.carry(importMoney);
            if (!setGetEndorse(edorPrem, tLCPolSchema))
            {
                return false;
            }
            /*
	        double managefee = 0.00;
        	if(i==mLCPolSet.size())
        	{
        		managefee = totalfee - tempfee;
        	}
        	else
        	{
        		managefee = CommonBL.carry(edorPrem * rate);
        	}
        	double prem = edorPrem - managefee;
        	tempfee += managefee;
        	*/
        	if (!setLCPrem(edorPrem, tLCPolSchema))
            {
                return false;
            }
            mInsuredNoSet.add(tLCPolSchema.getInsuredNo());
            mGetMoney += edorPrem;
        }
        EdorItemSpecialData specialData = new EdorItemSpecialData(mEdorNo,mEdorType);
        specialData.add("ManageFee", String.valueOf(totalfee));
        mMap.put(specialData.getSpecialDataSet(), "DELETE&INSERT");
        return true;
    }

    private boolean setLCPrem(double aPrem, LCPolSchema tLCPolSchema)
    {
    	String tPolNo = tLCPolSchema.getPolNo();
    	String sql = "select dutycode from lcduty where polno = '" + tPolNo + "' with ur";
    	SSRS dutySSRS = mExeSQL.execSQL(sql);
		for(int j=1 ;j<=dutySSRS.getMaxRow();j++)
    	{
			sql = "select PayPlanCode from lcprem where polno = '" + tPolNo + "' and dutycode = '" + dutySSRS.GetText(j,1) + "' with ur";
    		SSRS premSSRS = mExeSQL.execSQL(sql);
    		for(int k=1 ;k<=premSSRS.getMaxRow();k++)
        	{
    			String tPrem = "0.00";
    			if(k==1)
    			{
    				tPrem = String.valueOf(aPrem);
    			}
    			String updatePrem = "update lcprem set prem = " + tPrem + " where polno = '" + tPolNo
    			                  + "' and dutycode = '" + dutySSRS.GetText(j, 1) + "' and PayPlanCode = '" + premSSRS.GetText(k, 1)+ "' ";
    			mMap.put(updatePrem, SysConst.UPDATE);
        	}
    		String updateDuty = "update lcduty a set prem = (select sum(prem) from lcprem where polno = a.polno and dutycode = a.dutycode) "
    		                  + " where polno = '" + tPolNo + "' and dutycode = '" + dutySSRS.GetText(j, 1) + "' ";
    		mMap.put(updateDuty, SysConst.UPDATE);
    	}
		String updatePol = "update lcpol a set prem = (select sum(prem) from lcduty where polno = a.polno) "
                         + " where polno = '" + tPolNo + "' ";
		mMap.put(updatePol, SysConst.UPDATE);
    	String updateCont = "update lccont a set prem = (select sum(prem) from lcpol where contno = a.contno) "
                         + " where contno = '" + tLCPolSchema.getContNo() + "' ";
    	mMap.put(updateCont, SysConst.UPDATE);
    	return true;
    }

    /**
     * 将团单险种层的缴费频次状态恢复
     */
    private void setPayIntv()
    {
        String tPayIntv = new ExeSQL().getOneValue(
                "select edorvalue From LPEdorEspecialData where edorno ='" +
                mEdorNo + "' and detailtype ='PAYINTV'");
        if (tPayIntv != "" || !tPayIntv.equals("")) {
            String updateSQL = "update  lcgrppol set payintv = " + tPayIntv +
                               " where grpcontno ='" + mGrpContNo +
                               "' and riskcode ='280101'";
            MMap tMMap = new MMap();
            tMMap.put(updateSQL, SysConst.UPDATE);

            VData data = new VData();
            data.add(tMMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(data, "")) {
                mErrors.copyAllErrors(tPubSubmit.mErrors);
            }
        }

    }


    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa5302";
        gi.ComCode = "8653";
        gi.ManageCom = gi.ComCode;

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo("20070213000069");
        tLPGrpEdorItemDB.setEdorType("NI");

        VData data = new VData();
        data.add(gi);
        data.add(tLPGrpEdorItemDB.query().get(1));

        GrpEdorTZAppConfirmBL bl = new GrpEdorTZAppConfirmBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
