package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全定期结算到帐处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class GrpBalancePayConfirmBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 团体合同号 */
    private String mGrpContNo = null;
    /** 工单号 */
    private String mDetailWorkNo = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();

    public LCGrpBalPlanDB mLCGrpBalPlanDB;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public GrpBalancePayConfirmBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        System.out.println("---End getInputData---");

        if (!checkData()) {
            return false;
        }
        // 根据业务逻辑对数据进行处理
        if (this.dealData() == false) {
            return false;
        }

        //数据准备操作
        System.out.println("---End dealData---");
        if (!prepareOutputData()) {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpBalancePayConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean checkData() {
        return true;
    }

    private boolean prepareOutputData() {
//        mResult.clear();
//        mResult.add(map);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
//        try {
//            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
//                    "GlobalInput", 0);
//
//        } catch (Exception e) {
//            CError.buildErr(this, "接收数据失败");
//            return false;
//        }

        return true;
    }

    private boolean dealData() {
        String strSql = "select a.* from lcgrpbalplan a ,lgwork lg where a.state='2' and exists (select tempfeeno from LJTempFee b where b.TempFeeNo in" +
                        "(select c.GetNoticeNo from ljspay c where a.grpcontno = (select contno from lgwork d where d.workno = c.otherno and typeno in ('0602','0601') and statusno ='3') and c.othernotype='13') and b.confmakedate is not null)"
                      + " and lg.contno=a.grpcontno"
                      + " and lg.typeno in ('0602','0601')"
                      + " and lg.statusno ='3'"
                      ;
        System.out.println(strSql);
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        LCGrpBalPlanSet tLCGrpBalPlanSet = tLCGrpBalPlanDB.executeQuery(strSql);
        if (tLCGrpBalPlanDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            return false;
        }
        System.out.println("定期结算交费结案:" + tLCGrpBalPlanSet.size() + "个集体合同。");
        GrpBalanceOnTime tGrpBalanceOnTime = new GrpBalanceOnTime();
        for (int i = 1; i <= tLCGrpBalPlanSet.size(); i++) {
        	//add by lzy 201507为防止并发生成多条实收数据，加个10分钟的锁
        	MMap tCekMap1 = null;
	    	tCekMap1 = lockLJAGet(tLCGrpBalPlanSet.get(i).getSchema());
	        if (null == tCekMap1)
	        {
	            return false;
	        }
	        map.add(tCekMap1);
        	
            LCGrpBalPlanSchema tLCGrpBalPlanSchema = tGrpBalanceOnTime.
                    updateLCGrpBalPlanState(tLCGrpBalPlanSet.get(i), "0",
                                            "",0);
            if(!setLJAPay(tLCGrpBalPlanSchema.getGrpContNo())){
                return false;
            }
            map.put(tLCGrpBalPlanSchema, "UPDATE");
//            String update="update LJTempFee b set b.confdate='"+mCurrentDate+"',b.modifydate='"+mCurrentDate+"',b.modifytime='"+mCurrentTime+"' "+
//                          "where b.TempFeeNo =(select c.GetNoticeNo from ljspay c where c.otherno='"+tLCGrpBalPlanSet.get(i).getGrpContNo()+"' and c.othernotype='13'"+
//                          ") and b.confmakedate is not null;";
//            map.put(update,"UPDATE");
        }
        return true;
    }

    /**
     * 生成实收号
     * @return String
     */
    private String createPayNo(String IncomeNo) {
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setIncomeNo(IncomeNo);
        tLJAPayDB.setIncomeType("13");
        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        if (tLJAPaySet.size() == 0) {
            return PubFun1.CreateMaxNo("PAYNO", null);
        } else {
            return tLJAPaySet.get(1).getPayNo();
        }
    }

    /**
     * 得到到帐确认日期
     * @return String
     */
    public static String getEnterAccDate(String grpContNo) {
        String strSql =
                "select enteraccdate from LJTempFee b where b.TempFeeNo =" +
                "(select c.GetNoticeNo from ljspay c where c.otherno='" +
                grpContNo + "' and c.othernotype='13')";
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(strSql);
        if (tLJTempFeeSet.size() == 0) {
            return null;
        }
        return tLJTempFeeSet.get(1).getEnterAccDate();
    }

    /**
     * 得到应收信息
     * @return LJSPaySet
     */
    private LJSPaySchema getLJSPay(String grpContNo) {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        String sql =
                "select a.* from ljspay a,ljtempfee c where a.othernotype = '13' "
                +
                "and a.getnoticeno = c.tempfeeno and c.confmakedate is not null "
                +
                " and a.otherno in (select workno from lgwork where lgwork.contno = '"
                + grpContNo + "')";
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() == 0) {
            return null;
        }
        return tLJSPaySet.get(1);
    }

    /**
     * 保全确认时把应收转为实收
     */
    private boolean setLJAPay(String grpContNo) {
        LJSPaySchema tLJSPaySchema = getLJSPay(grpContNo);
        if (tLJSPaySchema == null) {
            return true;
        }
        LGWorkDB tLGworkDB = new LGWorkDB();
        tLGworkDB.setWorkNo(tLJSPaySchema.getOtherNo());
        if (!tLGworkDB.getInfo()) {
            this.mErrors.addOneError("找不到该工单");
            return false;
        }
        LGWorkSchema tLGWorkSchema = tLGworkDB.getSchema();
        tLGWorkSchema.setTypeNo("06");
        //得到经办人和机构
        try {
            getOperatorInfo(tLJSPaySchema.getOtherNo());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        VData tTaskVData = new VData();
        tTaskVData.add(this.mGlobalInput);
        tTaskVData.add(tLGWorkSchema);
        TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
        MMap tMap = tTaskAutoFinishBL.getSubmitData(tTaskVData, "");
        if (tMap == null) {
            mErrors.copyAllErrors(tTaskAutoFinishBL.mErrors);
            return false;
        }
        this.map.add(tMap);

        //保全确认lpedorapp lpgrpedormain lpgrpedoritem
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(tLJSPaySchema.getOtherNo());
        if (!tLPEdorAppDB.getInfo()) {
            this.mErrors.addOneError("该工单没有生成lpedorapp记录");
            return false;
        }
        LPEdorAppSchema tLPEdorAppSchema = tLPEdorAppDB.getSchema();
        tLPEdorAppSchema.setEdorState("0");
        tLPEdorAppSchema.setConfDate(this.mCurrentDate);
        tLPEdorAppSchema.setOperator(mGlobalInput.Operator);
        tLPEdorAppSchema.setMakeDate(mCurrentDate);
        tLPEdorAppSchema.setMakeTime(mCurrentTime);
        tLPEdorAppSchema.setModifyDate(mCurrentDate);
        tLPEdorAppSchema.setModifyTime(mCurrentTime);
        this.map.put(tLPEdorAppSchema, "UPDATE");

        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(tLJSPaySchema.getOtherNo());
        tLPGrpEdorMainDB.setEdorNo(tLJSPaySchema.getOtherNo());
        if (!tLPGrpEdorMainDB.getInfo()) {
            this.mErrors.addOneError("该工单没有生成lpgrpedormain记录");
            return false;
        }
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainDB.getSchema();
        tLPGrpEdorMainSchema.setEdorState("0");
        tLPGrpEdorMainSchema.setConfDate(this.mCurrentDate);
        tLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPGrpEdorMainSchema.setMakeDate(mCurrentDate);
        tLPGrpEdorMainSchema.setMakeTime(mCurrentTime);
        tLPGrpEdorMainSchema.setModifyDate(mCurrentDate);
        tLPGrpEdorMainSchema.setModifyTime(mCurrentTime);
        this.map.put(tLPGrpEdorMainSchema, "UPDATE");

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(tLJSPaySchema.getOtherNo());
        tLPGrpEdorItemDB.setEdorNo(tLJSPaySchema.getOtherNo());
        tLPGrpEdorItemDB.setGrpContNo(grpContNo);
        tLPGrpEdorItemDB.setEdorType("DJ");
        if (!tLPGrpEdorItemDB.getInfo()) {
            this.mErrors.addOneError("该工单没有生成lpgrpedoritem记录");
            return false;
        }
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemDB.getSchema();
        tLPGrpEdorItemSchema.setEdorState("0");
        tLPGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPGrpEdorItemSchema.setMakeDate(mCurrentDate);
        tLPGrpEdorItemSchema.setMakeTime(mCurrentTime);
        tLPGrpEdorItemSchema.setModifyDate(mCurrentDate);
        tLPGrpEdorItemSchema.setModifyTime(mCurrentTime);
        this.map.put(tLPGrpEdorItemSchema, "UPDATE");

        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        Reflections ref = new Reflections();
        ref.transFields(tLJAPaySchema, tLJSPaySchema);

        String tPayNo = createPayNo(tLJSPaySchema.getOtherNo());
        tLJAPaySchema.setPayNo(tPayNo);
        tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());

        tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
        tLJAPaySchema.setSumActuPayMoney(tLJSPaySchema.getSumDuePayMoney());

        tLJAPaySchema.setEnterAccDate(getEnterAccDate(grpContNo));
        tLJAPaySchema.setConfDate(mCurrentDate);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setMakeDate(mCurrentDate);
        tLJAPaySchema.setMakeTime(mCurrentTime);
        tLJAPaySchema.setModifyDate(mCurrentDate);
        tLJAPaySchema.setModifyTime(mCurrentTime);

        map.put(tLJSPaySchema, "DELETE");
        map.put(tLJAPaySchema, "DELETE&INSERT");

        return true;
    }

    /**
     * 得到操作人员信息
     * @param edorAcceptNo String
     */
    private void getOperatorInfo(String edorAcceptNo) {
        String sql = "select a.Operator, b.ComCode " +
                     "from LGWork a, LDUser b " +
                     "where a.Operator = b.UserCode " +
                     "and a.DetailWorkNo = '" + edorAcceptNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() == 0) {
            System.out.println("未找到工单经办人信息!");
            return;
        }
        mGlobalInput.Operator = tSSRS.GetText(1, 1);
        mGlobalInput.ManageCom = tSSRS.GetText(1, 2);
    }
    
    private MMap lockLJAGet(LCGrpBalPlanSchema tLCGrpBalPlanSchema)
    {
    	
    	
        MMap tMMap = null;
        /**锁定标志"DJ"*/
        String tLockNoType = "DJ";
        /**锁定时间*/
        String tAIS = "600";
        System.out.println("设置10分钟的解锁时间");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", tLCGrpBalPlanSchema.getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
       
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        
        if (tMMap == null)
        {           	
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }


    public static void main(String[] args) {
//        GlobalInput tGlobalInput = new GlobalInput();
//        tGlobalInput.Operator = "001";
//        tGlobalInput.ManageCom = "86";
//
//        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
//        tLPGrpEdorItemSchema.setEdorAcceptNo("86110000000436");
//        tLPGrpEdorItemSchema.setEdorNo("430110000000143");
//        tLPGrpEdorItemSchema.setEdorAppNo("430110000000143");
//        tLPGrpEdorItemSchema.setGrpContNo("0000034201");
//        tLPGrpEdorItemSchema.setEdorType("AC");

        VData tVData = new VData();
//        tVData.add(tGlobalInput);
//        tVData.add(tLPGrpEdorItemSchema);

        GrpBalancePayConfirmBL tGrpEdorFPConfirmBL = new GrpBalancePayConfirmBL();
        if (!tGrpEdorFPConfirmBL.submitData(tVData, "")) {
            System.out.println(tGrpEdorFPConfirmBL.mErrors.getErrContent());
        }
    }
}
