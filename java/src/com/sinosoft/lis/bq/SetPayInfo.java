package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: </p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class SetPayInfo
{
    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    private static final String PAY_OPERATOR = "1";

    private static final String GET_OPERATOR = "0";

    private MMap mMap = new MMap();

    /** 全局数据 */
    private GlobalInput mGI;

    /** 操作类型，分交费和退费两种 */
    private String mOperator;

    /** 保全受理号 */
    private String mEdorAcceptNo;

    /** 交费方式 */
    private String mPayMode;

    /** 现金交付截止日期 */
    private String mEndDate;

    /** 银行转帐日期 */
    private String mPayDate;
    
    /** 银行转帐解止日期 */
    private String mEndPayDate = null;

    /** 转帐银行 */
    private String mBank;

    /** 转帐帐号 */
    private String mBankAccno;

    /** 帐户名 */
    private String mAccName;

    /** 管理机构*/
    private String mManageCom ;

    public SetPayInfo(String edorAcceptNo)
    {
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 处理业务数据
     * @param data VData
     * @param operator String
     * @return boolean
     */
    public boolean submitDate(VData data, String operator)
    {
        if (!getInputDate(data, operator))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }

        System.out.println("mOperator" + mOperator);
        //财务交费
        if (mOperator.equals(PAY_OPERATOR))
        {
            if (CommonBL.checkGetMoney(mEdorAcceptNo))
            {
                LJSPaySchema tLJSPaySchema = getLJSPay();
                setLJSPay(tLJSPaySchema);
                if (mPayMode.equals("4")) //如果是银行转帐设置暂收
                {
                    setTempInfo(tLJSPaySchema);
                }
            }
        }
        //财务退费
        else if (mOperator.equals(GET_OPERATOR))
        {
            LJAGetSchema tLJAGetSchema = getLJAGet();
            if (tLJAGetSchema != null)
            {
                setLJAGet(tLJAGetSchema);
            }
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if ((mPayMode == null) || (mPayMode.equals("")))
        {
            mErrors.addOneError("未选择收退费方式！");
            return false;
        }
        return true;
    }

    /**
     * 得到输入数据
     * @param data VData
     * @param operator String
     */
    private boolean getInputDate(VData data, String operator)
    {
        mOperator = operator;
        mGI = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData td = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);
        mPayMode = (String) td.getValueByName("payMode");
        mEndDate = (String) td.getValueByName("endDate");
        mPayDate = (String) td.getValueByName("payDate");
        mBank = (String) td.getValueByName("bank");
        mBankAccno = (String) td.getValueByName("bankAccno");
        mAccName = (String) td.getValueByName("accName");
        if(mPayDate!=null && !"null".equals(mPayDate) && !"".equals(mPayDate))
        {
        	String sql = "select date('" + mPayDate + "') + 3 days from dual";
            mEndPayDate = new ExeSQL().getOneValue(sql);
        }
        else{
        	
        	mErrors.addOneError("应付日期为空！");
        	return false;
        	
        }
        
        return true;
    }

    /**
     * 得到应付信息
     * @return double
     */
    private LJSPaySchema getLJSPay()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mEdorAcceptNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        return tLJSPaySet.get(1);
    }

    /**
     * 设置交费信息
     * @param cLJSPaySchema LJSPaySchema
     */
    private void setLJSPay(LJSPaySchema cLJSPaySchema)
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setSchema(cLJSPaySchema);
        if (mPayMode.equals("4")) //如果是银行转帐有StartPayDate和PayDate两个日期
        {
            //modified by huxl. 2006-11-28 与崔晓华沟通，缴费截止日期为转帐日期。
//            paydate为两个月之后的日期
//            FDate tFDate = new FDate();
//            Date payDate = tFDate.getDate(mPayDate);
//            GregorianCalendar calendar = new GregorianCalendar();
//            calendar.setTime(payDate);
//            calendar.add(Calendar.MONTH, 2);
        	
        	//20080815 modified by zhanggm 需求变更
        	//陈延禄邮件确认：Startpaydate由操作人员选择设置，paydate为Startpaydate + 1，cbs00017056
        	//20080918 modified by zhanggm 需求变更
        	//陈延禄邮件确认：Startpaydate由操作人员选择设置，paydate为Startpaydate + 3，cbs00019255
            tLJSPaySchema.setStartPayDate(mPayDate);
            tLJSPaySchema.setPayDate(mEndPayDate);
            tLJSPaySchema.setBankCode(mBank);
            tLJSPaySchema.setBankAccNo(mBankAccno);
            tLJSPaySchema.setAccName(mAccName);
        }
        else
        {
            tLJSPaySchema.setPayDate(mEndDate);
        }
        tLJSPaySchema.setOperator(mGI.Operator);
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        System.out.println("mPayMode" + mPayMode);
                System.out.println("mEndDate" + mEndDate);
        mMap.put(tLJSPaySchema, "DELETE&INSERT");
    }

    /**
     * 得到实付信息
     * @return LJAGetSchema
     */
    private LJAGetSchema getLJAGet()
    {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(mEdorAcceptNo);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() == 0)
        {
            return null;
        }
        return tLJAGetSet.get(1);
    }

    /**
     * 设置实付信息
     * @param cLJAGetSchema LJAGetSchema
     */
    private void setLJAGet(LJAGetSchema cLJAGetSchema)
    {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setSchema(cLJAGetSchema);
        tLJAGetSchema.setShouldDate(mPayDate);
        tLJAGetSchema.setPayMode(mPayMode);
        tLJAGetSchema.setBankCode(mBank);
        tLJAGetSchema.setBankAccNo(mBankAccno);
        tLJAGetSchema.setAccName(mAccName);
        tLJAGetSchema.setOperator(mGI.Operator);
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJAGetSchema, "DELETE&INSERT");
    }

    /**
     * 设置暂交费表
     * @param map MMap
     * @param cLJSPaySchema LJSPaySchema
     */
    private void setTempInfo(LJSPaySchema cLJSPaySchema)
    {
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(mEdorAcceptNo);
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

        //生成暂交费号
        String tempFeeNo;
        
        //修改ljtempfee中的tempfeeno有可能和ljspay中不一致的情况 add by xp 2010-2-10
//        if (tLJTempFeeSet.size() == 0)
//        {
            tempFeeNo = cLJSPaySchema.getGetNoticeNo();
//        }
        //已有暂交费记录
//        else
//        {
            String sql1 = "delete from LJTempFee " +
                    "where OtherNo = '" + mEdorAcceptNo + "' ";
            mMap.put(sql1, "DELETE");
//            tempFeeNo = tLJTempFeeSet.get(1).getTempFeeNo();
//        }

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(tempFeeNo);
        tLJTempFeeSchema.setTempFeeType("4"); //保全
        tLJTempFeeSchema.setRiskCode(cLJSPaySchema.getRiskCode());
        tLJTempFeeSchema.setOtherNo(mEdorAcceptNo);
        tLJTempFeeSchema.setOtherNoType(cLJSPaySchema.getOtherNoType());
        tLJTempFeeSchema.setPayDate(mEndPayDate);
        tLJTempFeeSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeSchema.setManageCom(getManageCom(mEdorAcceptNo));
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setOperator(mGI.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJTempFeeSchema, "INSERT");

        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(tempFeeNo);
        LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();
        if (tLJTempFeeClassSet.size() != 0)
        {
            String sql = "delete from LJTempFeeClass " +
                    "where TempFeeNo = '" + tempFeeNo + "' ";
            mMap.put(sql, "DELETE");
        }

        //设置暂交费分类表
        LJTempFeeClassSchema tLJTempFeeClassSchema = new
                LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tempFeeNo);
        tLJTempFeeClassSchema.setPayMode(mPayMode); //4是银行转帐
        tLJTempFeeClassSchema.setPayDate(mEndPayDate);
        tLJTempFeeClassSchema.setPayMoney(cLJSPaySchema.getSumDuePayMoney());
        tLJTempFeeClassSchema.setManageCom(getManageCom(mEdorAcceptNo));
        tLJTempFeeClassSchema.setBankCode(mBank);
        tLJTempFeeClassSchema.setBankAccNo(mBankAccno);
        tLJTempFeeClassSchema.setAccName(mAccName);
        tLJTempFeeClassSchema.setOperator(mGI.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJTempFeeClassSchema, "INSERT");
    }

    /**
     * 提交数据
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "SetPayInfo";
            tError.functionName = "submit";
            tError.errorMessage = "设置交费方式失败!";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得保单管理机构
     * add by fuxin 2008-7-9
     */
    private String getManageCom(String edorNo)
    {
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        String sql1 =" select * From LPEdorItem where EdorNo ='"+edorNo+"'";
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql1);
        tLPEdorItemSchema = tLPEdorItemSet.get(1);

        ExeSQL tExeSQL = new ExeSQL();
        String manageComSQL =" select managecom From lccont where contno ='"+tLPEdorItemSchema.getContNo()+"'"
                            +" union "
                            +" Select managecom from lbcont where contno ='"+tLPEdorItemSchema.getContNo()+"'"
                            ;
       return mManageCom = tExeSQL.getOneValue(manageComSQL);
    }
    
    public static void main(String[] args) {
    	SetPayInfo payinfo=new SetPayInfo("");
    	payinfo.mOperator ="";
	}
}
