package com.sinosoft.lis.bq;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>ClassName: LJFinaConfirm </p>
 * <p>Description: 财务业务核销转储 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @author: TJJ
 * @ReWrite: ZhangRong
 * @CreateDate：2002-10-15
 */
public class LJFinaConfirm
{
    private VData mResult = new VData();
    private MMap mMap = new MMap();

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /* 通知书号*/
    String mGetNoticeNo;
    /*给付类型标志 "I": 交费核销；"O":退费核销*/
    String mFlag;
    /*给付类型*/
    String mType;
    /*实付号码*/
    String mActuGetNo;
    /*交费收据号码*/
    String mPayNo;
    /*号码生成参数(set,get方法提供外部接口)*/
    String mLimit;
    /*操作员*/
    String mOperator;
    /*退费系列表*/
    LJSGetSet mLJSGetSet = new LJSGetSet();
    LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
    LJSGetOtherSet mLJSGetOtherSet = new LJSGetOtherSet();
    LJSGetTempFeeSet mLJSGetTempFeeSet = new LJSGetTempFeeSet();
    LJSGetDrawSet mLJSGetDrawSet = new LJSGetDrawSet();
    LJAGetSet mLJAGetSet = new LJAGetSet();
    LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
    LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet();
    LJAGetDrawSet mLJAGetDrawSet = new LJAGetDrawSet();
    LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
    /*交费系列表*/
    LJAPaySet mLJAPaySet = new LJAPaySet();
    LJSPaySet mLJSPaySet = new LJSPaySet();
    /*保全批改补退费表*/
    LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet();
    LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    /*保费相关系列表*/
    LPPremSet mLPPremSet = new LPPremSet();
    LPDutySet mLPDutySet = new LPDutySet();
    LPPolSet mLPPolSet = new LPPolSet();
    LPContSet mLPContSet = new LPContSet();
    LPGrpPolSet mLPGrpPolSet = new LPGrpPolSet();
    LPGrpContSet mLPGrpContSet = new LPGrpContSet();
    /*暂交费表*/
    LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    //财务交费到帐日期，add by Minim
    String mEnterAccDate = "";

    // @Constructor
    public LJFinaConfirm(String aGetNoticeNo)
    {
        mGetNoticeNo = aGetNoticeNo;
    }

    public LJFinaConfirm(String aGetNoticeNo, String aFlag)
    {
        mGetNoticeNo = aGetNoticeNo;
        mFlag = aFlag;
    }

    public LJFinaConfirm(String aGetNoticeNo, String aFlag, String aType)
    {
        mGetNoticeNo = aGetNoticeNo;
        mFlag = aFlag;
        mType = aType;
    }

    // @Method

    /**
     * 提交整体给付核销
     * @return
     */

    public boolean submitData()
    {
        System.out.println("\nStart LJFinaConfirm\n\n");
        //核销交退费
        if (!this.chkFinaConfirm())
        {
            return false;
        }

        if (mFlag.equals("O"))
        {
            if (!this.conLJAGetSerials())
            {
                return false;
            }
//            if (!this.saveLJAGetSerials())
//            {
//                return false;
//            }              //commented by zhangrong
//            return true;
        }
        else if (mFlag.equals("I"))
        {
            //准备转储交费数据
            if (!this.conLJAPaySerials())
            {
                return false;
            }
//            if (!this.saveLJAPaySerials())
//            {
//                return false;
//            }                //commented by zhangrong
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = "传入标志有误（mFlag）!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 按照给付类型提交给付核销
     * @return
     */
    public boolean submitDataByType()
    {
        String aType;
        aType = mType;

        if (!this.chkFinaConfirm())
        {
            return false;
        }
        if (mFlag.equals("O"))
        {
            if (!this.conLJAGetSerials(aType))
            {
                return false;
            }
            if (!this.saveLJAGetSerials())
            {
                return false;
            }
            return true;
        }
        else if (mFlag.equals("I"))
        {
            if (!this.conLJAPaySerials(aType))
            {
                return false;
            }
            if (!this.saveLJAPaySerials())
            {
                return false;
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = "传入标志有误（mFlag）!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     *准备转储数据
     * @return true or false
     */
    private boolean conLJAGetSerials()
    {
        String tActuGetNo;

        /*应付总表*/
        LJSGetSchema tLJSGetSchema = new LJSGetSchema();

        /*赔付应付表*/
        LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();

        /*其他退费应付表*/
        LJSGetOtherSchema tLJSGetOtherSchema = new LJSGetOtherSchema();

        /*暂交费退费应付表*/
        LJSGetTempFeeSchema tLJSGetTempFeeSchema = new LJSGetTempFeeSchema();

        /*给付表(生存领取_应付)*/
        LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();

        /*批改补退费表（应收/应付） */
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

        /*实付总表*/
        mLJAGetSet = new LJAGetSet();
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();

        /*赔付实付表*/
        LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
        mLJAGetClaimSet = new LJAGetClaimSet();

        /*其他退费实付表*/
        LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
        mLJAGetOtherSet = new LJAGetOtherSet();

        /*暂交费退费实付表*/
        LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
        mLJAGetTempFeeSet = new LJAGetTempFeeSet();

        /*给付表(生存领取_实付)*/
        LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
        mLJAGetDrawSet = new LJAGetDrawSet();

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        mLJAGetEndorseSet = new LJAGetEndorseSet();

        /*得到实付号码*/
        tActuGetNo = this.getActuGetNo();

        /*赔付应付核销转储*/
        LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
        tLJSGetClaimDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetClaimSet = tLJSGetClaimDB.query();
        if (mLJSGetClaimSet != null)
        {
            mLJAGetClaimSet.clear();
            for (int i = 1; i <= mLJSGetClaimSet.size(); i++)
            {
                tLJSGetClaimSchema = new LJSGetClaimSchema();
                tLJAGetClaimSchema = new LJAGetClaimSchema();
                tLJSGetClaimSchema = mLJSGetClaimSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetClaimSchema, tLJSGetClaimSchema);

                tLJAGetClaimSchema.setActuGetNo(tActuGetNo);
                tLJAGetClaimSchema.setOperator(this.getOperator());
                tLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetClaimSet.add(tLJAGetClaimSchema);
            }
        }

        /*其他退费核销转储*/
        LJSGetOtherDB tLJSGetOtherDB = new LJSGetOtherDB();
        tLJSGetOtherDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetOtherSet = tLJSGetOtherDB.query();
        if (mLJSGetOtherSet != null)
        {

            mLJAGetOtherSet.clear();
            for (int i = 1; i <= mLJSGetOtherSet.size(); i++)
            {
                tLJSGetOtherSchema = new LJSGetOtherSchema();
                tLJAGetOtherSchema = new LJAGetOtherSchema();
                tLJSGetOtherSchema = mLJSGetOtherSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetOtherSchema, tLJSGetOtherSchema);

                tLJAGetClaimSchema.setActuGetNo(tActuGetNo);
                tLJAGetClaimSchema.setOperator(this.getOperator());
                tLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());

                tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetOtherSet.add(tLJAGetOtherSchema);
            }
        }
        /*暂交费退费核销转储*/
        LJSGetTempFeeDB tLJSGetTempFeeDB = new LJSGetTempFeeDB();
        tLJSGetTempFeeDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetTempFeeSet = tLJSGetTempFeeDB.query();
        if (mLJSGetTempFeeSet != null)
        {

            mLJAGetTempFeeSet.clear();
            for (int i = 1; i <= mLJSGetTempFeeSet.size(); i++)
            {
                tLJSGetTempFeeSchema = new LJSGetTempFeeSchema();
                tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
                tLJSGetTempFeeSchema = mLJSGetTempFeeSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetTempFeeSchema, tLJSGetTempFeeSchema);

                tLJAGetTempFeeSchema.setActuGetNo(tActuGetNo);
                tLJAGetTempFeeSchema.setOperator(this.getOperator());
                tLJAGetTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetTempFeeSchema.setMakeTime(PubFun.getCurrentTime());

                tLJAGetTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
            }
        }
        /*给付表(生存领取_应付)核销转储*/
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetDrawSet = tLJSGetDrawDB.query();
        if (mLJSGetDrawSet != null)
        {

            mLJAGetDrawSet.clear();
            for (int i = 1; i <= mLJSGetDrawSet.size(); i++)
            {
                tLJSGetDrawSchema = new LJSGetDrawSchema();
                tLJAGetDrawSchema = new LJAGetDrawSchema();
                tLJSGetDrawSchema = mLJSGetDrawSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetDrawSchema, tLJSGetDrawSchema);

                tLJAGetDrawSchema.setActuGetNo(tActuGetNo);
                tLJAGetDrawSchema.setOperator(this.getOperator());
                tLJAGetDrawSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetDrawSchema.setMakeTime(PubFun.getCurrentTime());

                tLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetDrawSet.add(tLJAGetDrawSchema);
            }
        }

        /*批改补退费核销转储*/
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetEndorseSet = tLJSGetEndorseDB.query();
        if (mLJSGetEndorseSet != null)
        {

            mLJAGetEndorseSet.clear();
            for (int i = 1; i <= mLJSGetEndorseSet.size(); i++)
            {
                tLJSGetEndorseSchema = new LJSGetEndorseSchema();
                tLJAGetEndorseSchema = new LJAGetEndorseSchema();
                tLJSGetEndorseSchema = mLJSGetEndorseSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);

                tLJAGetEndorseSchema.setActuGetNo(tActuGetNo);
                tLJAGetEndorseSchema.setOperator(this.getOperator());
                tLJAGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());

                tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetEndorseSet.add(tLJAGetEndorseSchema);
            }

            //修改累计保费，add by Minim at 2003-10-17
            modifySumPrem(mLJAGetEndorseSet);
        }

        /*应付总表核销转储*/
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetSet = tLJSGetDB.query();
        if (mLJSGetSet != null)
        {
            mLJAGetSet.clear();
            for (int i = 1; i <= mLJSGetSet.size(); i++)
            {
                tLJSGetSchema = new LJSGetSchema();
                tLJAGetSchema = new LJAGetSchema();
                tLJSGetSchema = mLJSGetSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetSchema, tLJSGetSchema);

                tLJAGetSchema.setActuGetNo(tActuGetNo);
                tLJAGetSchema.setOperator(this.getOperator());
                tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetSet.add(tLJAGetSchema);
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "conData";
            tError.errorMessage = "主表数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 修改累计保费
     * @param tLJAGetEndorseSet
     * @return
     */
    private boolean modifySumPrem(LJAGetEndorseSet tLJAGetEndorseSet)
    {
        try
        {
            System.out.println("\nStart modify SumPrem");

            //modify lpprem
            LPPremDB tLPPremDB = new LPPremDB();
            tLPPremDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
            tLPPremDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
            tLPPremDB.setContNo(tLJAGetEndorseSet.get(1).getContNo());
            tLPPremDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
            LPPremSet tLPPremSet = tLPPremDB.query();

			if (tLPPremSet == null || tLPPremSet.size() <= 0)
			{
                LCPremDB tLCPremDB = new LCPremDB();
                tLCPremDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
                LCPremSet tLCPremSet = tLCPremDB.query();

                for (int j = 0; j < tLPPremSet.size(); j++)
                {
                    for (int i = 0; i < tLCPremSet.size(); i++)
                    {
                        if (tLPPremSet.get(j + 1).getContNo().equals(tLCPremSet.get(i + 1).getContNo())
                            && tLPPremSet.get(j + 1).getPolNo().equals(tLCPremSet.get(i + 1).getPolNo())
                            && tLPPremSet.get(j + 1).getDutyCode().equals(tLCPremSet.get(i + 1).getDutyCode())
                            && tLPPremSet.get(j + 1).getPayPlanCode().equals(tLCPremSet.get(i + 1).getPayPlanCode()))
                        {
                            tLPPremSet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCPremSet.get(j + 1).getSumPrem() +
                                (tLPPremSet.get(j + 1).getPrem() - tLCPremSet.get(j + 1).getPrem()), "0.00"));
                        }
                    }
                }
                mLPPremSet.add(tLPPremSet);
            }
            //modify lpduty
            LPDutyDB tLPDutyDB = new LPDutyDB();
            tLPDutyDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
            tLPDutyDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
            tLPDutyDB.setContNo(tLJAGetEndorseSet.get(1).getContNo());
            tLPDutyDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
            LPDutySet tLPDutySet = tLPDutyDB.query();

			if (tLPDutySet == null || tLPDutySet.size() <= 0)
			{
                LCDutyDB tLCDutyDB = new LCDutyDB();
                tLCDutyDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
                LCDutySet tLCDutySet = tLCDutyDB.query();

                for (int j = 0; j < tLPDutySet.size(); j++)
                {
                    for (int i = 0; i < tLCDutySet.size(); i++)
                    {
                        if (tLPDutySet.get(j + 1).getContNo().equals(tLCDutySet.get(i + 1).getContNo())
                            && tLPDutySet.get(j + 1).getPolNo().equals(tLCDutySet.get(i + 1).getPolNo())
                            && tLPDutySet.get(j + 1).getDutyCode().equals(tLCDutySet.get(i + 1).getDutyCode()))
                        {
                            tLPDutySet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCDutySet.get(i + 1).getSumPrem() +
                                (tLPDutySet.get(j + 1).getPrem() - tLCDutySet.get(j + 1).getPrem()), "0.00"));
                        }
                    }
                }
                mLPDutySet.add(tLPDutySet);
            }
            //modify lppol
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
            tLPPolDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
            tLPPolDB.setContNo(tLJAGetEndorseSet.get(1).getContNo());
            tLPPolDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
            LPPolSet tLPPolSet = tLPPolDB.query();

			if (tLPPolSet == null || tLPPolSet.size() <= 0)
			{
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLJAGetEndorseSet.get(1).getPolNo());
                LCPolSet tLCPolSet = tLCPolDB.query();

                for (int j = 0; j < tLPPolSet.size(); j++)
                {
                    for (int i = 0; i < tLCPolSet.size(); i++)
                    {
                        if (tLPPolSet.get(j + 1).getContNo().equals(tLCPolSet.get(i + 1).getContNo())
                            && tLPPolSet.get(j + 1).getPolNo().equals(tLCPolSet.get(i + 1).getPolNo()))
                        {
                            tLPPolSet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCPolSet.get(i + 1).getSumPrem() +
                                (tLPPolSet.get(j + 1).getPrem() - tLCPolSet.get(j + 1).getPrem()), "0.00"));
                        }
                    }
                }
                mLPPolSet.add(tLPPolSet);
            }

            //modify lpCont
            LPContDB tLPContDB = new LPContDB();
            tLPContDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
            tLPContDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
            tLPContDB.setContNo(tLJAGetEndorseSet.get(1).getContNo());
            LPContSet tLPContSet = tLPContDB.query();

			if (tLPContSet == null || tLPContSet.size() <= 0)
			{
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(tLJAGetEndorseSet.get(1).getContNo());
                LCContSet tLCContSet = tLCContDB.query();

                for (int j = 0; j < tLPContSet.size(); j++)
                {
                    for (int i = 0; i < tLCContSet.size(); i++)
                    {
                        if (tLPContSet.get(j + 1).getContNo().equals(tLCContSet.get(i + 1).getContNo()))
                        {
                            tLPContSet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCContSet.get(i + 1).getSumPrem() +
                                (tLPContSet.get(j + 1).getPrem() - tLCContSet.get(j + 1).getPrem()), "0.00"));
                        }
                    }
                }
                mLPContSet.add(tLPContSet);
            }

            //modify lpGrppol
            if (tLJAGetEndorseSet.get(1).getRiskCode() != null && !tLJAGetEndorseSet.get(1).getRiskCode().equals(""))
            {
                LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
                tLPGrpPolDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
                tLPGrpPolDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
                tLPGrpPolDB.setGrpPolNo(tLJAGetEndorseSet.get(1).getGrpPolNo());
                tLPGrpPolDB.setRiskCode(tLJAGetEndorseSet.get(1).getRiskCode());
                LPGrpPolSet tLPGrpPolSet = tLPGrpPolDB.query();

				if (tLPGrpPolSet == null || tLPGrpPolSet.size() <= 0)
				{
                    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                    tLCGrpPolDB.setGrpPolNo(tLJAGetEndorseSet.get(1).getGrpPolNo());
                    tLCGrpPolDB.setRiskCode(tLJAGetEndorseSet.get(1).getRiskCode());
                    LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

                    for (int j = 0; j < tLPGrpPolSet.size(); j++)
                    {
                        for (int i = 0; i < tLCGrpPolSet.size(); i++)
                        {
                            if (tLPGrpPolSet.get(j + 1).getGrpContNo().equals(tLCGrpPolSet.get(i + 1).getGrpContNo())
                                && tLPGrpPolSet.get(j + 1).getGrpPolNo().equals(tLCGrpPolSet.get(i + 1).getGrpPolNo()))
                            {
                                tLPGrpPolSet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCGrpPolSet.get(i + 1).getSumPrem() +
                                    (tLPGrpPolSet.get(j + 1).getPrem() - tLCGrpPolSet.get(j + 1).getPrem()), "0.00"));
                            }
                        }
                    }
                    mLPGrpPolSet.add(tLPGrpPolSet);
                }
            }

            //modify lpGrpCont
            LPGrpContDB tLPGrpContDB = new LPGrpContDB();
            tLPGrpContDB.setEdorNo(tLJAGetEndorseSet.get(1).getEndorsementNo());
            tLPGrpContDB.setEdorType(tLJAGetEndorseSet.get(1).getFeeOperationType());
            tLPGrpContDB.setGrpContNo(tLJAGetEndorseSet.get(1).getGrpContNo());
            LPGrpContSet tLPGrpContSet = tLPGrpContDB.query();

			if (tLPGrpContSet == null || tLPGrpContSet.size() <= 0)
			{
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(tLJAGetEndorseSet.get(1).getGrpContNo());
                LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();

                for (int j = 0; j < tLPGrpContSet.size(); j++)
                {
                    for (int i = 0; i < tLCGrpContSet.size(); i++)
                    {
                        if (tLPGrpContSet.get(j + 1).getGrpContNo().equals(tLCGrpContSet.get(i + 1).getGrpContNo()))
                        {
                            tLPGrpContSet.get(j + 1).setSumPrem(PubFun.setPrecision(tLCGrpContSet.get(i + 1).getSumPrem() +
                                (tLPGrpContSet.get(j + 1).getPrem() - tLCGrpContSet.get(j + 1).getPrem()), "0.00"));
                        }
                    }
                }
                mLPGrpContSet.add(tLPGrpContSet);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "modifySumPrem";
            tError.errorMessage = "修改累计保费有误!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 提交累计保费修改，共用一个数据库连接
     * @param conn
     * @throws Exception
     */
    private void commitModifySumPrem(Connection conn) throws Exception
    {
        HashMap map = new HashMap();
        map.put(mLPPremSet, "UPDATE");
        map.put(mLPDutySet, "UPDATE");
        map.put(mLPPolSet, "UPDATE");
        VData tVData = new VData();
        tVData.add(map);

        LJFinaConfirmBLS tLJFinaConfirmBLS = new LJFinaConfirmBLS();
        //不在LJFinaConfirmBLS中提交数据
        tLJFinaConfirmBLS.setCommitStatus(false);
        //共用一个数据库连接
        tLJFinaConfirmBLS.setConnection(conn);
        if (tLJFinaConfirmBLS.submitData(tVData, "") == false)
        {
            //@@错误处理
            this.mErrors.copyAllErrors(tLJFinaConfirmBLS.mErrors);
            throw new Exception("commitModifySumPrem Faile");
        }
    }

    /**
     *按照给付类型准备转储数据
     * @return true or false
     */
    private boolean conLJAGetSerials(String aType)
    {
        String tActuGetNo;

        /*应付总表*/
        LJSGetSchema tLJSGetSchema = new LJSGetSchema();

        /*赔付应付表*/
        LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();

        /*其他退费应付表*/
        LJSGetOtherSchema tLJSGetOtherSchema = new LJSGetOtherSchema();

        /*暂交费退费应付表*/
        LJSGetTempFeeSchema tLJSGetTempFeeSchema = new LJSGetTempFeeSchema();

        /*给付表(生存领取_应付)*/
        LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();

        /*批改补退费表（应收/应付） */
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

        /*实付总表*/
        mLJAGetSet = new LJAGetSet();
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();

        /*赔付实付表*/
        LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
        mLJAGetClaimSet = new LJAGetClaimSet();

        /*其他退费实付表*/
        LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
        mLJAGetOtherSet = new LJAGetOtherSet();

        /*暂交费退费实付表*/
        LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
        mLJAGetTempFeeSet = new LJAGetTempFeeSet();

        /*给付表(生存领取_实付)*/
        LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
        mLJAGetDrawSet = new LJAGetDrawSet();

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        mLJAGetEndorseSet = new LJAGetEndorseSet();

        /*得到实付号码*/
        tActuGetNo = this.getActuGetNo();

        /*赔付应付核销转储*/
        if (aType.equals("LP"))
        {
            LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
            tLJSGetClaimDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetClaimSet = tLJSGetClaimDB.query();
            if (mLJSGetClaimSet != null)
            {

                mLJAGetClaimSet.clear();
                for (int i = 1; i <= mLJSGetClaimSet.size(); i++)
                {
                    tLJSGetClaimSchema = new LJSGetClaimSchema();
                    tLJAGetClaimSchema = new LJAGetClaimSchema();
                    tLJSGetClaimSchema = mLJSGetClaimSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetClaimSchema, tLJSGetClaimSchema);

                    tLJAGetClaimSchema.setActuGetNo(tActuGetNo);
                    tLJAGetClaimSchema.setOperator(this.getOperator());
                    tLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetClaimSet.add(tLJAGetClaimSchema);
                }
            }
        }
        /*其他退费核销转储*/
        if (aType.equals("OT"))
        {
            LJSGetOtherDB tLJSGetOtherDB = new LJSGetOtherDB();
            tLJSGetOtherDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetOtherSet = tLJSGetOtherDB.query();
            if (mLJSGetOtherSet != null)
            {

                mLJAGetOtherSet.clear();
                for (int i = 1; i <= mLJSGetOtherSet.size(); i++)
                {
                    tLJSGetOtherSchema = new LJSGetOtherSchema();
                    tLJAGetOtherSchema = new LJAGetOtherSchema();
                    tLJSGetOtherSchema = mLJSGetOtherSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetOtherSchema, tLJSGetOtherSchema);

                    tLJAGetOtherSchema.setActuGetNo(tActuGetNo);
                    tLJAGetOtherSchema.setOperator(this.getOperator());
                    tLJAGetOtherSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetOtherSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetOtherSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetOtherSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetOtherSet.add(tLJAGetOtherSchema);
                }
            }
        }
        /*暂交费退费核销转储*/
        if (aType.equals("TP"))
        {
            LJSGetTempFeeDB tLJSGetTempFeeDB = new LJSGetTempFeeDB();
            tLJSGetTempFeeDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetTempFeeSet = tLJSGetTempFeeDB.query();
            if (mLJSGetTempFeeSet != null)
            {

                mLJAGetTempFeeSet.clear();
                for (int i = 1; i <= mLJSGetTempFeeSet.size(); i++)
                {
                    tLJSGetTempFeeSchema = new LJSGetTempFeeSchema();
                    tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
                    tLJSGetTempFeeSchema = mLJSGetTempFeeSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetTempFeeSchema, tLJSGetTempFeeSchema);

                    tLJAGetTempFeeSchema.setActuGetNo(tActuGetNo);
                    tLJAGetTempFeeSchema.setOperator(this.getOperator());
                    tLJAGetTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
                }
            }
        }

        /*给付表(生存领取_应付)核销转储*/
        if (aType.equals("DR"))
        {
            LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
            tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetDrawSet = tLJSGetDrawDB.query();
            if (mLJSGetDrawSet != null)
            {

                mLJAGetDrawSet.clear();
                for (int i = 1; i <= mLJSGetDrawSet.size(); i++)
                {
                    tLJSGetDrawSchema = new LJSGetDrawSchema();
                    tLJAGetDrawSchema = new LJAGetDrawSchema();
                    tLJSGetDrawSchema = mLJSGetDrawSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetDrawSchema, tLJSGetDrawSchema);

                    tLJAGetDrawSchema.setActuGetNo(tActuGetNo);
                    tLJAGetDrawSchema.setOperator(this.getOperator());
                    tLJAGetDrawSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetDrawSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetDrawSet.add(tLJAGetDrawSchema);
                }
            }
        }
        /*批改补退费核销转储*/
        if (aType.equals("PG"))
        {
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetEndorseSet = tLJSGetEndorseDB.query();
            if (mLJSGetEndorseSet != null)
            {

                mLJAGetEndorseSet.clear();
                for (int i = 1; i <= mLJSGetEndorseSet.size(); i++)
                {
                    tLJSGetEndorseSchema = new LJSGetEndorseSchema();
                    tLJAGetEndorseSchema = new LJAGetEndorseSchema();
                    tLJSGetEndorseSchema = mLJSGetEndorseSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);
                    tLJAGetEndorseSchema.setActuGetNo(tActuGetNo);
                    tLJAGetEndorseSchema.setOperator(this.getOperator());
                    tLJAGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetEndorseSet.add(tLJAGetEndorseSchema);
                }

                //修改累计保费，add by Minim at 2003-10-17
                modifySumPrem(mLJAGetEndorseSet);
            }
        }
        /*应付总表核销转储*/
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetSet = tLJSGetDB.query();
        if (mLJSGetSet != null)
        {

            mLJAGetSet.clear();
            for (int i = 1; i <= mLJSGetSet.size(); i++)
            {
                tLJSGetSchema = new LJSGetSchema();
                tLJAGetSchema = new LJAGetSchema();
                tLJSGetSchema = mLJSGetSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetSchema, tLJSGetSchema);

                tLJAGetSchema.setActuGetNo(tActuGetNo);
                tLJAGetSchema.setOperator(this.getOperator());
                tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetSet.add(tLJAGetSchema);
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "conData";
            tError.errorMessage = "主表数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 保存实付数据
     * @return
     */
    public boolean saveLJAGetSerials()
    {
        String tActuGetNo;
        /*实付总表*/
        LJAGetSet tLJAGetSet = new LJAGetSet();
        LJAGetDBSet tLJAGetDBSet;

        /*赔付实付表*/
        LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
        LJAGetClaimDBSet tLJAGetClaimDBSet;

        /*其他退费实付表*/
        LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
        LJAGetOtherDBSet tLJAGetOtherDBSet;

        /*暂交费退费实付表*/
        LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
        LJAGetTempFeeDBSet tLJAGetTempFeeDBSet;

        /*给付表(生存领取_实付)*/
        LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
        LJAGetDrawDBSet tLJAGetDrawDBSet;

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
        LJAGetEndorseDBSet tLJAGetEndorseDBSet;

        /*save data*/
        Connection conn = null;
        conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tActuGetNo = this.getActuGetNo();
        try
        {
            conn.setAutoCommit(false);

            tLJAGetClaimDBSet = new LJAGetClaimDBSet(conn);
            tLJAGetClaimDBSet.set(mLJAGetClaimSet);

            if (tLJAGetClaimDBSet != null && tLJAGetClaimDBSet.size() > 0)
            {
                if (!tLJAGetClaimDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "赔付纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(conn);
                tLJSGetClaimDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetClaimDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "赔付纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAGetOtherDBSet = new LJAGetOtherDBSet(conn);
            tLJAGetOtherDBSet.set(mLJAGetOtherSet);

            if (tLJAGetOtherDBSet != null && tLJAGetOtherDBSet.size() > 0)
            {
                if (!tLJAGetOtherDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "其他退费纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetOtherDB tLJSGetOtherDB = new LJSGetOtherDB(conn);
                tLJSGetOtherDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetOtherDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "其他退费纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAGetTempFeeDBSet = new LJAGetTempFeeDBSet(conn);
            tLJAGetTempFeeDBSet.set(mLJAGetTempFeeSet);

            if (tLJAGetTempFeeDBSet != null && tLJAGetTempFeeDBSet.size() > 0)
            {
                if (!tLJAGetTempFeeDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "暂交费退费纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetTempFeeDB tLJSGetTempFeeDB = new LJSGetTempFeeDB(conn);
                tLJSGetTempFeeDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetTempFeeDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "暂交费退费纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAGetDrawDBSet = new LJAGetDrawDBSet(conn);
            tLJAGetDrawDBSet.set(mLJAGetDrawSet);

            if (tLJAGetDrawDBSet != null && tLJAGetDrawDBSet.size() > 0)
            {
                if (!tLJAGetDrawDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "生存给付纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB(conn);
                tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetDrawDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "生存给付纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAGetEndorseDBSet = new LJAGetEndorseDBSet(conn);
            tLJAGetEndorseDBSet.set(mLJAGetEndorseSet);

            if (tLJAGetEndorseDBSet != null && tLJAGetEndorseDBSet.size() > 0)
            {
                if (!tLJAGetEndorseDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "保全交退费纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB(conn);
                tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetEndorseDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "保全交退费纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAGetDBSet = new LJAGetDBSet(conn);
            tLJAGetDBSet.set(mLJAGetSet);

            if (tLJAGetDBSet != null && tLJAGetDBSet.size() > 0)
            {
                if (!tLJAGetDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "实付保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetDB tLJSGetDB = new LJSGetDB(conn);
                tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "实付纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            commitModifySumPrem(conn);

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     *准备转储交费数据
     * @return true or false
     */
    private boolean conLJAPaySerials()
    {
        String tActuGetNo;

        /*应收总表*/
        LJSPaySet tLJSPaySet = new LJSPaySet();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();

        /*批改补退费表（应收/应付） */
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

        /*实收总表*/
        mLJAPaySet = new LJAPaySet();
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        mLJAGetEndorseSet = new LJAGetEndorseSet();

        /*得到实付号码*/
        String tPayNo = mPayNo;

        /*批改补退费核销转储*/
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
        mLJSGetEndorseSet = tLJSGetEndorseDB.query();
        if (mLJSGetEndorseSet != null)
        {
            mLJAGetEndorseSet.clear();
            for (int i = 1; i <= mLJSGetEndorseSet.size(); i++)
            {
                tLJSGetEndorseSchema = new LJSGetEndorseSchema();
                tLJAGetEndorseSchema = new LJAGetEndorseSchema();
                tLJSGetEndorseSchema = mLJSGetEndorseSet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);

                tLJAGetEndorseSchema.setActuGetNo(tPayNo);
                tLJAGetEndorseSchema.setEnterAccDate(mEnterAccDate); //add by Minim
                tLJAGetEndorseSchema.setGetConfirmDate(PubFun.getCurrentDate()); //add by Minim
                tLJAGetEndorseSchema.setOperator(this.getOperator());
                tLJAGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());

                mLJAGetEndorseSet.add(tLJAGetEndorseSchema);
            }

            //修改累计保费
            modifySumPrem(mLJAGetEndorseSet);
        }

        /*应付总表核销转储*/
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet != null)
        {
            mLJAPaySet.clear();
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                tLJSPaySchema = new LJSPaySchema();
                tLJAPaySchema = new LJAPaySchema();
                tLJSPaySchema = tLJSPaySet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAPaySchema, tLJSPaySchema);
                tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
                tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
                tLJAPaySchema.setSumActuPayMoney(tLJSPaySchema.getSumDuePayMoney());
                tLJAPaySchema.setPayNo(tPayNo);
                tLJAPaySchema.setEnterAccDate(mEnterAccDate); //add by Minim
                tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
                tLJAPaySchema.setOperator(this.getOperator());
                tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
                tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());

                mLJAPaySet.add(tLJAPaySchema);
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "conData";
            tError.errorMessage = "主表数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     *准备转储交费数据
     * @return true or false
     */
    private boolean conLJAPaySerials(String aType)
    {

        /*应付总表*/
        LJSPaySet tLJSPaySet = new LJSPaySet();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();

        /*批改补退费表（应收/应付） */
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

        /*实付总表*/
        mLJAPaySet = new LJAPaySet();
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        mLJAGetEndorseSet = new LJAGetEndorseSet();

        /*得到实付号码*/
        String tPayNo = mPayNo;

        if (aType.equals("PG"))
        {
            /*批改补退费核销转储*/
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
            mLJSGetEndorseSet = tLJSGetEndorseDB.query();
            if (mLJSGetEndorseSet != null)
            {
                mLJAGetEndorseSet.clear();
                for (int i = 1; i <= mLJSGetEndorseSet.size(); i++)
                {
                    tLJSGetEndorseSchema = new LJSGetEndorseSchema();
                    tLJAGetEndorseSchema = new LJAGetEndorseSchema();
                    tLJSGetEndorseSchema = mLJSGetEndorseSet.get(i);
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLJAGetEndorseSchema, tLJSGetEndorseSchema);

                    tLJAGetEndorseSchema.setActuGetNo(tPayNo);
                    tLJAGetEndorseSchema.setOperator(this.getOperator());
                    tLJAGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());

                    mLJAGetEndorseSet.add(tLJAGetEndorseSchema);
                }

                //修改累计保费，add by Minim at 2003-10-17
                modifySumPrem(mLJAGetEndorseSet);
            }
        }
        /*应付总表核销转储*/
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet != null)
        {
            mLJAPaySet.clear();
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                tLJSPaySchema = new LJSPaySchema();
                tLJAPaySchema = new LJAPaySchema();
                tLJSPaySchema = tLJSPaySet.get(i);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLJAPaySchema, tLJSPaySchema);

                tLJAPaySchema.setPayNo(tPayNo);
                tLJAPaySchema.setOperator(this.getOperator());

                tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
                tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());

                tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
                tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());

                mLJAPaySet.add(tLJAPaySchema);
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "conData";
            tError.errorMessage = "主表数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 保存实付数据
     * @return
     */
    public boolean saveLJAPaySerials()
    {
        String tActuPayNo;
        /*实付总表*/
        LJAPaySet tLJAPaySet = new LJAPaySet();
        LJAPayDBSet tLJAPayDBSet;

        /*批改补退费表（实收/实付） */
        LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
        LJAGetEndorseDBSet tLJAGetEndorseDBSet;

        /*save data*/
        Connection conn = null;
        conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tPayNo = mPayNo;

        try
        {
            conn.setAutoCommit(false);

            tLJAGetEndorseDBSet = new LJAGetEndorseDBSet(conn);
            tLJAGetEndorseDBSet.set(mLJAGetEndorseSet);

            if (tLJAGetEndorseDBSet != null && tLJAGetEndorseDBSet.size() > 0)
            {
                if (!tLJAGetEndorseDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "保全交退费纪录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB(conn);
                tLJSGetEndorseDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSGetEndorseDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "保全交退费纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            tLJAPayDBSet = new LJAPayDBSet(conn);
            tLJAPayDBSet.set(mLJAPaySet);

            if (tLJAPayDBSet != null && tLJAPayDBSet.size() > 0)
            {
                if (!tLJAPayDBSet.insert())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "实收保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LJSPayDB tLJSPayDB = new LJSPayDB(conn);
                tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
                if (!tLJSPayDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "saveLJAGetserials";
                    tError.errorMessage = "交费纪录删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            System.out.println("Start Modify ljtempfee");
            //更新暂交费表，add by Minim
            LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
            tLJTempFeeDBSet.set(mLJTempFeeSet);
            if (!tLJTempFeeDBSet.update())
            {
                CError tError = new CError();
                tError.moduleName = "LJFinaConfirm";
                tError.functionName = "saveLJAGetserials";
                tError.errorMessage = "暂交费表更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //更新暂交费子表,add by JL at 2004-11-19
            LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
            tLJTempFeeClassDBSet.set(mLJTempFeeClassSet);
            if (!tLJTempFeeClassDBSet.update())
            {
                CError.buildErr(this, "暂交费子表更新失败!");
                conn.rollback();
                conn.close();
                return true;
            }
            System.out.println("End Modify ljtempfee");

            commitModifySumPrem(conn);

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJFinaConfirm";
            tError.functionName = "saveData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 核销交退费处理
     * @return
     */
    private boolean chkFinaConfirm()
    {
        /*财务给付表*/
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();

        if (mFlag.equals("I"))
        {
            //获取应收总表数据
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
            if (!tLJSPayDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LJFinaConfirm";
                tError.functionName = "GetNoticeNO";
                tError.errorMessage = "没有财务应收交费纪录!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //生成实付号码
            String StrLimit = this.getLimit();
            mPayNo = PubFun1.CreateMaxNo("PAYNO", StrLimit);

            //如果需要财务收费
            if (tLJSPayDB.getSumDuePayMoney() != 0)
            {
                mLJSPaySet.add(tLJSPayDB.getSchema());

                //获取暂交费数据
                LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
                //Modify by Minim for check EnterAccDate
                String strSql = "select * from ljtempfee where tempfeeno='" + mGetNoticeNo
                                + "' and EnterAccDate is not null order by EnterAccDate";
                tLJTempFeeSet = tLJTempFeeDB.executeQuery(strSql);
                if (tLJTempFeeSet == null || tLJTempFeeSet.size() == 0)
                {
                    CError.buildErr(this, "没有财务交费纪录!");
                    return false;
                }

                double tPayMoney = 0;
                double tPayMoneyClass;
                for (int i = 1; i <= tLJTempFeeSet.size(); i++)
                {
                    tPayMoney = tPayMoney + tLJTempFeeSet.get(i).getPayMoney();
                    //核销暂交费数据,add by Minim
                    tLJTempFeeSet.get(i).setConfFlag("1");
                    tLJTempFeeSet.get(i).setConfDate(PubFun.getCurrentDate());
                    //核销暂交费子表数据,add by JL at 2004-11-18
                    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
                    strSql = "select * from ljtempfeeclass where tempfeeno='"
                             + tLJTempFeeSet.get(i).getTempFeeNo()
                             + "' and EnterAccDate is not null";
                    tLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(strSql);
                    if (tLJTempFeeClassSet == null || tLJTempFeeClassSet.size() == 0)
                    {
                        CError.buildErr(this, "查找暂交费分类表记录失败!");
                        return false;
                    }
                    tPayMoneyClass = 0;
                    for (int j = 1; j <= tLJTempFeeClassSet.size(); j++)
                    {
                        tPayMoneyClass += tLJTempFeeClassSet.get(j).getPayMoney();
                        //核销暂交费子表
                        tLJTempFeeClassSet.get(j).setConfFlag("1");
                        tLJTempFeeClassSet.get(j).setConfDate(PubFun.getCurrentDate());
                    }
                    //校验暂交费子表金额是否与暂交费表相同
                    if (tPayMoneyClass == tLJTempFeeSet.get(i).getPayMoney())
                    {
                        mLJTempFeeClassSet.add(tLJTempFeeClassSet);
                    }
                    else
                    {
                        CError.buildErr(this, "暂交费主表记录和暂交费子表记录金额不符!");
                        return false;
                    }
                }

                //判断应收与暂收是否相等
                if (tPayMoney == tLJSPayDB.getSumDuePayMoney())
                {
                    //获取最晚的到帐日期,add by Minim
                    mEnterAccDate = tLJTempFeeSet.get(1).getEnterAccDate();
                    mLJTempFeeSet = tLJTempFeeSet;
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "LJFinaConfirm";
                    tError.functionName = "GetNoticeNO";
                    tError.errorMessage = "应交与暂交不符!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            //add by JL at 2004-11-24,修改tLJSPayDB.getSumDuePayMoney()==0
            //mEnterAccDate为空的情况。
            if (mEnterAccDate == null || mEnterAccDate == "")
            {
                mEnterAccDate = PubFun.getCurrentDate();
            }
        }
        else
        {
            if (mFlag.equals("O"))
            {
                String strLimit = this.getLimit();
                mActuGetNo = PubFun1.CreateMaxNo("GETNO", strLimit);
                this.setActuGetNo(mActuGetNo);
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "LJFinaConfirm";
                tError.functionName = "GetNoticeNO";
                tError.errorMessage = "标志传入有误!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        mMap.put(mLJSGetEndorseSet, "DELETE");
        mMap.put(mLJAGetEndorseSet, "INSERT");

        if (mFlag.equals("I"))
        {
            mMap.put(mLJTempFeeSet, "UPDATE");
            mMap.put(mLJTempFeeClassSet, "UPDATE");
            mMap.put(mLJSPaySet, "DELETE");
            mMap.put(mLJAPaySet, "INSERT");
        }
        else if (mFlag.equals("O"))
        {
            mMap.put(mLJSGetClaimSet, "DELETE");
            mMap.put(mLJAGetClaimSet, "INSERT");
            mMap.put(mLJSGetOtherSet, "DELETE");
            mMap.put(mLJAGetOtherSet, "INSERT");
            mMap.put(mLJSGetTempFeeSet, "DELETE");
            mMap.put(mLJAGetTempFeeSet, "INSERT");
            mMap.put(mLJSGetDrawSet, "DELETE");
            mMap.put(mLJAGetDrawSet, "INSERT");
            mMap.put(mLJSGetSet, "DELETE");
            mMap.put(mLJAGetSet, "INSERT");
        }

        // 更新保费变更数据
        mMap.put(mLPPremSet, "UPDATE");
        mMap.put(mLPDutySet, "UPDATE");
        mMap.put(mLPPolSet, "UPDATE");
        mMap.put(mLPContSet, "UPDATE");
        mMap.put(mLPGrpPolSet, "UPDATE");
        mMap.put(mLPGrpContSet, "UPDATE");

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    public void setLimit(String aLimit)
    {
        mLimit = aLimit;
    }

    public String getLimit()
    {
        return mLimit;
    }

    public void setOperator(String aOperator)
    {
        mOperator = aOperator;
    }

    public String getOperator()
    {
        return mOperator;
    }

    public void setActuGetNo(String aActuGetNo)
    {
        mActuGetNo = aActuGetNo;
    }

    public String getActuGetNo()
    {
        return mActuGetNo;
    }

    public static void main(String[] args)
    {
        int f = PubFun.calInterval("2003-12-23", "2004-04-02", "D");
        String aGetNoticeNo = "86110020030310000276";
        LJFinaConfirm tLJFinaConfirm = new LJFinaConfirm(aGetNoticeNo, "I");
        tLJFinaConfirm.setOperator("001");
        tLJFinaConfirm.setLimit(PubFun.getNoLimit("86"));
        System.out.println("start LJFinaConfirm...");
        if (!tLJFinaConfirm.submitData())
        {

        }
    }
}
