package com.sinosoft.lis.bq;

//程序名称：PEdorLQDetailBL.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class PEdorLQConfirmBL implements EdorConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private ValidateEdorData2 mValidateEdorData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo,mEdorType, mContNo, "ContNo");
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateEdorData())
        {
            return false;
        }
        return true;
    }

    /**
     * 使保全数据生效
     * @return boolean
     */
    private boolean validateEdorData()
    {
        String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
        mValidateEdorData.addData(addTables);
        String[] chgTables = {"LCInsureAcc", "LCInsureAccFee","LCInsureAccClass","LCInsureAccClassFee"};
        mValidateEdorData.changeData(chgTables);
        mMap.add(mValidateEdorData.getMap());
        return true;
    }
}
