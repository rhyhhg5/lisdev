package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorEWConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
	private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作（preparedata())
        if (!prepareData())
        {
            return false;
        }

        return true;
    }
    /**
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitInsuData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作（preparedata())
//        if (!prepareInsuData())
        if (!prepareData())
        {
            return false;
        }

        return true;
    }



    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        return true;
        
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {

    	LPContSubSet saveLPContSubSet = new LPContSubSet();
    	LCContSubSet  saveLCContSubSet = new LCContSubSet();
;
    	LCContSubSet tLCContSubSet = new LCContSubSet();
    	LCContSubSchema aLCContSubSchema = new LCContSubSchema();
    	LPContSubSet tLPContSubSet = new LPContSubSet();
    	LPContSubSchema aLPContSubSchema = new LPContSubSchema();
		Reflections tRef = new Reflections();
		LPContSubDB   tLPContSubDB = new LPContSubDB();
		LPContSubSchema tLPContSubSchema = new LPContSubSchema();
		tLPContSubSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPContSubSchema.setEdorType(mLPEdorItemSchema.getEdorType());

		   tLPContSubDB.setSchema(tLPContSubSchema);
		   tLPContSubSet= tLPContSubDB.query();
		   for(int i = 1; i <= tLPContSubSet.size(); i++){
			   aLPContSubSchema = tLPContSubSet.get(i);
			   
				LCContSubSchema tLCContSubSchema = new LCContSubSchema();
				tLPContSubSchema = new LPContSubSchema();
				
	
				LCContSubDB aLCContSubDB = new LCContSubDB();
				aLCContSubDB.setPrtNo(aLPContSubSchema.getPrtNo());

	            if (!aLCContSubDB.getInfo())
	            {
	            	
	            }else{
	                aLCContSubSchema = aLCContSubDB.getSchema();
	                tRef.transFields(tLPContSubSchema, aLCContSubSchema);
	                tLPContSubSchema.setEdorNo(aLPContSubSchema.getEdorNo());
	                tLPContSubSchema.setEdorType(aLPContSubSchema.
	                                             getEdorType());
	//
//	                //转换成保单个人信息。
	                tRef.transFields(tLCContSubSchema, aLPContSubSchema);
	                tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
	                tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
	//
	                saveLPContSubSet.add(tLPContSubSchema);
	                tLCContSubSet.add(tLCContSubSchema);
	                saveLCContSubSet.add(tLCContSubSchema);
	            }
		   }
		   
//		
		//得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
		mLPEdorItemSchema.setEdorState("0");

		mMap.put(mLPEdorItemSchema, "UPDATE");

		mMap.put(saveLCContSubSet, "UPDATE");
		mMap.put(saveLPContSubSet, "UPDATE");
		

         mResult.add(mMap);
		return true;
    }
    
    
    /**
     * 被保人联系方式变更 li caiyan
     * 
     */
//    private boolean prepareInsuData()
//    {}
}
