package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全个单新收费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Lanjun, QiuYang
 * @version 1.0
 */

public class PEdorCCDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private DetailDataQuery mQuery = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_CC;

    private String mContNo = null;

    private LPContSchema mLPContSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        
        /**
         * 医保个人账户校验
         * 目前在程序中付费方式
         */
        if(!checked()){
        	return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 医保卡校验
     * wujun
     * @return
     */
    public boolean checked(){
    	//校验保单是否已过犹豫期
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(mLPContSchema.getContNo());
    	if(!tLCContDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "您操作的保单不存在！";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	LCContSchema tLCContSchema = new LCContSchema();
    	tLCContSchema = tLCContDB.getSchema();
    	
    	LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
    	tLPEdorMainDB.setEdorAcceptNo(mLPContSchema.getEdorNo());
    	tLPEdorMainDB.setContNo(mLPContSchema.getContNo());
    	tLPEdorMainDB.setEdorNo(mLPContSchema.getEdorNo());
    	if(!tLPEdorMainDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保全项目不存在！";
            this.mErrors.addOneError(tError);
    		return false;    		
    	}
    	LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
    	tLPEdorMainSchema = tLPEdorMainDB.getSchema();
    	
    	//首期付费方式为医保卡账户，校验保单是否在犹豫期
    	if("8".equals(tLCContSchema.getPayMode())&&!"8".equals(mLPContSchema.getPayMode())){
    		if(tLCContSchema.getCustomGetPolDate()!=null&&!"".equals(tLCContSchema.getCustomGetPolDate())&&!"null".equals(tLCContSchema.getCustomGetPolDate())){
    			//获取保单的犹豫期
    			int WTPeriod = CommonBL.getWTPeriod(tLCContSchema.getContNo());
    			if(PubFun.calInterval(tLCContSchema.getCustomGetPolDate(), tLPEdorMainSchema.getEdorValiDate(), "D")<=WTPeriod){
    	            CError tError = new CError();
    	            tError.moduleName = "PEdorItemBL";
    	            tError.functionName = "checkData";
    	            tError.errorMessage = "保单仍在犹豫期内，不能修改“医保个人账户”为其他交费方式!";
    	            this.mErrors.addOneError(tError);
    				return false;
    			}
    		}else {
    			if(PubFun.calInterval(tLCContSchema.getCValiDate(), tLPEdorMainSchema.getEdorValiDate(), "D")<=30){
    	            CError tError = new CError();
    	            tError.moduleName = "PEdorItemBL";
    	            tError.functionName = "checkData";
    	            tError.errorMessage = "保单仍在犹豫期内，不能修改“医保个人账户”为其他交费方式!";
    	            this.mErrors.addOneError(tError);
    				return false;
    			}
    		}
    	}
    	
    	//将收付费方式修改为医保个人账户的校验
    	boolean riskflag = false;
    	//2017-02-16 yukun新增riskcodeSQl以及上海医保卡不走下面的校验
    	String riskcodeSQl = "select count(1) count from lcpol " 
    						+"where contno = '"
    						+tLCContSchema.getContNo()
    						+"' and riskcode in (select code from ldcode where codetype='ybkriskcode') with ur";
    	String count = new ExeSQL().getOneValue(riskcodeSQl);
    	if("0".equals(count)){	//上海医保卡不走下面校验
    	if("8".equals(mLPContSchema.getPayMode())){
    		String mSQL = "select MedicalComCode from LDMedicalCom where CanSendFlag='1' and  comcode like '" +
    		tLCContSchema.getManageCom()+
			"%' fetch first 1 rows only with ur";
    		if(new ExeSQL().getOneValue(mSQL)!=null&&!"".equals(new ExeSQL().getOneValue(mSQL))){
				//校验险种是否是支持医疗个人账户的险种
				String riskSQL= " select code from ldcode where codetype='medicaloutrisk' ";
				
				String strSQL1 = "select 1 from lcpol where contno = '"+mLPContSchema.getContNo()+"'" +
				"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
				"and code1 in ("+riskSQL+")) " +
				"and riskcode not in ("+riskSQL+") with ur";
				String haveFlag = new ExeSQL().getOneValue(strSQL1);
				
				if(haveFlag!=null&&!"".equals(haveFlag)){
					String wrapSql=" select 1 from lccont a,lcriskdutywrap b where a.contno=b.contno " +
					" and a.contno='"+tLCContSchema.getContNo()+"' "+
					" and exists (select 1 from ldcode where codetype ='medicaloutwrap' and code=b.riskwrapcode) " +
					" with ur";
			
					String wrapFlag=new ExeSQL().getOneValue(wrapSql);
					if(wrapFlag==null||"".equals(wrapFlag)){
						riskflag=true;
					}
				}
				
    		}else{
    			riskflag = true;
    		}
    		if(riskflag){
	            CError tError = new CError();
	            tError.moduleName = "PEdorItemBL";
	            tError.functionName = "checkData";
	            tError.errorMessage = "保单所属机构或险种不符合使用“医保个人账户”的要求!";
	            this.mErrors.addOneError(tError);
    			return false;    				
    		}
    	}
    	}
    	return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPContSchema = (LPContSchema) cInputData.getObjectByObjectName(
                    "LPContSchema", 0);
            mEdorNo = mLPContSchema.getEdorNo();
            mContNo = mLPContSchema.getContNo();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入参数错误！";
            this.mErrors.addOneError(tError);            
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        setPayInfo();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
   }

   /**
    * 更新交费资料
    */
   private boolean setPayInfo()
   {
       LPContSchema tLPContSchema = (LPContSchema)
               mQuery.getDetailData("LCCont", mContNo);
       tLPContSchema.setPayMode(mLPContSchema.getPayMode());
       tLPContSchema.setBankCode(mLPContSchema.getBankCode());
       tLPContSchema.setBankAccNo(mLPContSchema.getBankAccNo());
       tLPContSchema.setAccName(mLPContSchema.getAccName());
       tLPContSchema.setOperator(mGlobalInput.Operator);
       tLPContSchema.setMakeDate(mCurrentDate);
       tLPContSchema.setMakeTime(mCurrentTime);
       tLPContSchema.setModifyDate(mCurrentDate);
       tLPContSchema.setModifyTime(mCurrentTime);
       mMap.put(tLPContSchema, "DELETE&INSERT");
       setLPPol();
       return true;
   }

   /**
    * 把保全状态设为已录入
    * @param edorState String
    */
   private void setEdorState(String edorState)
   {
       String sql = "update  LPEdorItem " +
               "set EdorState = '" + edorState + "', " +
               "    Operator = '" + mGlobalInput.Operator + "', " +
               "    ModifyDate = '" + mCurrentDate + "', " +
               "    ModifyTime = '" + mCurrentTime + "' " +
               "where  EdorNo = '" + mEdorNo + "' " +
               "and EdorType = '" + mEdorType + "' " +
               "and ContNo = '" + mContNo + "'";
       mMap.put(sql, "UPDATE");
   }


   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }
   
   //20090817 zhanggm cont表变，pol表跟着一起变，变更字段：paymode
   private void setLPPol()
   {
	   String sql = "select polno from lcpol where contno = '" + mContNo + "' ";
       SSRS tSSRS = new ExeSQL().execSQL(sql);
       for(int i=1;i<=tSSRS.MaxRow;i++)
       {
    	   LPPolSchema tLPPolSchema =  (LPPolSchema) mQuery.getDetailData("LCPol", tSSRS.GetText(i, 1));
    	   tLPPolSchema.setPayMode(mLPContSchema.getPayMode());
    	   tLPPolSchema.setOperator(mGlobalInput.Operator);
    	   tLPPolSchema.setMakeDate(mCurrentDate);
    	   tLPPolSchema.setMakeTime(mCurrentTime);
    	   tLPPolSchema.setModifyDate(mCurrentDate);
    	   tLPPolSchema.setModifyTime(mCurrentTime);
    	   mMap.put(tLPPolSchema, SysConst.DELETE_AND_INSERT);
       }
   }
   
	   public static void main(String[] args) {
		   System.out.println(PubFun.calInterval("2013-09-10", "2013-09-01", "D"));
	   }
}
