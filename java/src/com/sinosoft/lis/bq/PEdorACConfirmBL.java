package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong
 * @version 1.0
 */
public class PEdorACConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
	private MMap mMap = new MMap();

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private GlobalInput mGlobalInput = new GlobalInput();

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();

        getInputData(cInputData);

        if (!prepareData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError(new CError("传入数据不完全！"));
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPEdorItemDB.setPolNo(mLPEdorItemSchema.getPolNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询批改项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));
        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
		LCContSet aLCContSet = new LCContSet();
		LCPolSet aLCPolSet = new LCPolSet();
		LCAppntSet aLCAppntSet = new LCAppntSet();
		LCAddressSet aLCAddressSet = new LCAddressSet();
		LDPersonSet aLDPersonSet = new LDPersonSet();

		LPContSet aLPContSet = new LPContSet();
		LPPolSet aLPPolSet = new LPPolSet();
		LPAppntSet aLPAppntSet = new LPAppntSet();
		LPAddressSet aLPAddressSet = new LPAddressSet();
		LPPersonSet aLPPersonSet = new LPPersonSet();

		Reflections tRef = new Reflections();

		// 得到投保人资料信息表
		LPPolSet tLPPolSet = new LPPolSet();
		LPPolDB tLPPolDB = new LPPolDB();
		tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());

		String strSql = "select * from LPPol where EdorNo = '" + mLPEdorItemSchema.getEdorNo() +
						"' and EdorType = '" + mLPEdorItemSchema.getEdorType() + "'";

		tLPPolSet = tLPPolDB.executeQuery(strSql);

		for (int i = 1; i <= tLPPolSet.size(); i++)
		{
			LPPolSchema tLPPolSchema = new LPPolSchema();
			LCPolSchema tLCPolSchema = new LCPolSchema();
			tRef.transFields(tLCPolSchema, tLPPolSet.get(i));

			LCPolDB tLCPolDB = new LCPolDB();
			tLCPolDB.setPolNo(tLCPolSchema.getPolNo());
			if (!tLCPolDB.getInfo())
			{
				mErrors.copyAllErrors(tLCPolDB.mErrors);
				mErrors.addOneError(new CError("查询险种保单表失败！"));
				return false;
			}

			tRef.transFields(tLPPolSchema, tLCPolDB.getSchema());
			tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
			tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
			aLPPolSet.add(tLPPolSchema);
			aLCPolSet.add(tLCPolSchema);

		}

		LPContSchema tLPContSchema = new LPContSchema();
		LCContSchema tLCContSchema = new LCContSchema();
		LPContSchema aLPContSchema = new LPContSchema();
		LCContSchema aLCContSchema = new LCContSchema();
		LCContSet tLCContSet = new LCContSet();
		LPContSet tLPContSet = new LPContSet();

		tLPContSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPContSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		//tLPContSchema.setContNo(mLPEdorItemSchema.getContNo());

		LPContDB tLPContDB = new LPContDB();
		tLPContDB.setSchema(tLPContSchema);
		tLPContSet = tLPContDB.query();
		for (int j = 1; j <= tLPContSet.size(); j++)
		{
			aLPContSchema = tLPContSet.get(j);
			tLCContSchema = new LCContSchema();
			tLPContSchema = new LPContSchema();

			LCContDB aLCContDB = new LCContDB();
			aLCContDB.setContNo(aLPContSchema.getContNo());
			aLCContDB.setInsuredNo(aLPContSchema.getInsuredNo());
			if (!aLCContDB.getInfo())
			{
				mErrors.copyAllErrors(aLCContDB.mErrors);
				mErrors.addOneError(new CError("查询被保人信息失败！"));
				return false;
			}
			aLCContSchema = aLCContDB.getSchema();
			tRef.transFields(tLPContSchema, aLCContSchema);
			tLPContSchema.setEdorNo(aLPContSchema.getEdorNo());
			tLPContSchema.setEdorType(aLPContSchema.getEdorType());

			//转换成保单个人信息。
			tRef.transFields(tLCContSchema, aLPContSchema);
			tLCContSchema.setModifyDate(PubFun.getCurrentDate());
			tLCContSchema.setModifyTime(PubFun.getCurrentTime());

			aLPContSet.add(tLPContSchema);
			tLCContSet.add(tLCContSchema);
			aLCContSet.add(tLCContSchema);
		}

		//得到当前保单的投保人资料
		LCAppntSchema aLCAppntSchema = new LCAppntSchema();
		LPAppntSchema aLPAppntSchema = new LPAppntSchema();
		LCAppntSet tLCAppntSet = new LCAppntSet();
		LPAppntSet tLPAppntSet = new LPAppntSet();

		LPAppntSchema tLPAppntSchema = new LPAppntSchema();
		tLPAppntSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPAppntSchema.setEdorType(mLPEdorItemSchema.getEdorType());
	//	tLPAppntSchema.setContNo(mLPEdorItemSchema.getContNo());

		LPAppntDB tLPAppntDB = new LPAppntDB();
		tLPAppntDB.setSchema(tLPAppntSchema);
		tLPAppntSet = tLPAppntDB.query();
		for (int j = 1; j <= tLPAppntSet.size(); j++)
		{
			aLPAppntSchema = tLPAppntSet.get(j);
			LCAppntSchema tLCAppntSchema = new LCAppntSchema();
			tLPAppntSchema = new LPAppntSchema();

			LCAppntDB aLCAppntDB = new LCAppntDB();
			aLCAppntDB.setContNo(aLPAppntSchema.getContNo());
			aLCAppntDB.setAppntNo(aLPAppntSchema.getAppntNo());
			if (!aLCAppntDB.getInfo())
			{
				mErrors.copyAllErrors(aLCAppntDB.mErrors);
				mErrors.addOneError(new CError("查询投保人信息失败！"));
				return false;
			}
			aLCAppntSchema = aLCAppntDB.getSchema();
			tRef.transFields(tLPAppntSchema, aLCAppntSchema);
			tLPAppntSchema.setEdorNo(aLPAppntSchema.getEdorNo());
			tLPAppntSchema.setEdorType(aLPAppntSchema.getEdorType());

			//转换成保单个人信息。
			tRef.transFields(tLCAppntSchema, aLPAppntSchema);
			tLCAppntSchema.setModifyDate(PubFun.getCurrentDate());
			tLCAppntSchema.setModifyTime(PubFun.getCurrentTime());

			aLPAppntSet.add(tLPAppntSchema);
			tLCAppntSet.add(tLCAppntSchema);
			aLCAppntSet.add(tLCAppntSchema);
		}

		//得到当前保单的投保人资料
		boolean tNewAdd = false;
		LCAddressSchema aLCAddressSchema = new LCAddressSchema();
		LPAddressSchema aLPAddressSchema = new LPAddressSchema();
		LCAddressSet tLCAddressSet = new LCAddressSet();
		LPAddressSet tLPAddressSet = new LPAddressSet();

		LPAddressSchema tLPAddressSchema = new LPAddressSchema();
		tLPAddressSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPAddressSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPAddressSchema.setCustomerNo(tLPAppntSchema.getAppntNo());

		LPAddressDB tLPAddressDB = new LPAddressDB();
		tLPAddressDB.setSchema(tLPAddressSchema);
		tLPAddressSet = tLPAddressDB.query();
		for (int j = 1; j <= tLPAddressSet.size(); j++)
		{
			aLPAddressSchema = tLPAddressSet.get(j);
			LCAddressSchema tLCAddressSchema = new LCAddressSchema();
			tLPAddressSchema = new LPAddressSchema();

			LCAddressDB aLCAddressDB = new LCAddressDB();
			aLCAddressDB.setCustomerNo(aLPAddressSchema.getCustomerNo());
			aLCAddressDB.setAddressNo(aLPAddressSchema.getAddressNo());
			if (!aLCAddressDB.getInfo())
			{
				if (aLCAddressDB.mErrors.needDealError())
				{
                    mErrors.copyAllErrors(aLCAddressDB.mErrors);
                    mErrors.addOneError(new CError("查询投保人地址信息失败！"));
                    return false;
                }
				tNewAdd = true;
				tRef.transFields(tLCAddressSchema, aLPAddressSchema);
				tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
				tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());
				tLCAddressSet.add(tLCAddressSchema);
				aLCAddressSet.add(tLCAddressSchema);
			}
			else
			{
				tNewAdd = false;
                aLCAddressSchema = aLCAddressDB.getSchema();
                tRef.transFields(tLPAddressSchema, aLCAddressSchema);
                tLPAddressSchema.setEdorNo(aLPAddressSchema.getEdorNo());
                tLPAddressSchema.setEdorType(aLPAddressSchema.getEdorType());

                //转换成保单个人信息。
                tRef.transFields(tLCAddressSchema, aLPAddressSchema);
                tLCAddressSchema.setModifyDate(PubFun.getCurrentDate());
                tLCAddressSchema.setModifyTime(PubFun.getCurrentTime());

                aLPAddressSet.add(tLPAddressSchema);
                tLCAddressSet.add(tLCAddressSchema);
                aLCAddressSet.add(tLCAddressSchema);
            }
		}

		//得到当前保单的投保人资料
		LDPersonSchema aLCPersonSchema = new LDPersonSchema();
		LPPersonSchema aLPPersonSchema = new LPPersonSchema();
		LDPersonSet tLDPersonSet = new LDPersonSet();
		LPPersonSet tLPPersonSet = new LPPersonSet();

		LPPersonSchema tLPPersonSchema = new LPPersonSchema();
		tLPPersonSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPPersonSchema.setEdorType(mLPEdorItemSchema.getEdorType());

		LPPersonDB tLPPersonDB = new LPPersonDB();
		tLPPersonDB.setSchema(tLPPersonSchema);
		tLPPersonSet = tLPPersonDB.query();
		for (int j = 1; j <= tLPPersonSet.size(); j++)
		{
			aLPPersonSchema = tLPPersonSet.get(j);
			LDPersonSchema tLDPersonSchema = new LDPersonSchema();
			tLPPersonSchema = new LPPersonSchema();

			LDPersonDB aLDPersonDB = new LDPersonDB();
			aLDPersonDB.setCustomerNo(aLPPersonSchema.getCustomerNo());
			if (!aLDPersonDB.getInfo())
			{
				mErrors.copyAllErrors(aLDPersonDB.mErrors);
				mErrors.addOneError(new CError("查询投保人信息失败！"));
				return false;
			}
			aLCPersonSchema = aLDPersonDB.getSchema();
			tRef.transFields(tLPPersonSchema, aLCPersonSchema);
			tLPPersonSchema.setEdorNo(aLPPersonSchema.getEdorNo());
			tLPPersonSchema.setEdorType(aLPPersonSchema.getEdorType());

			//转换成保单个人信息。
			tRef.transFields(tLDPersonSchema, aLPPersonSchema);
			tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
			tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());

			aLPPersonSet.add(tLPPersonSchema);
			tLDPersonSet.add(tLDPersonSchema);
			aLDPersonSet.add(tLDPersonSchema);
		}
		
		if(!setLCInsured())
		{
			return false;
		}

		//得到当前LPEdorItem保单信息主表的状态，并更新状态为申请确认。
		mLPEdorItemSchema.setEdorState("0");

		mMap.put(mLPEdorItemSchema, "UPDATE");
		mMap.put(aLCContSet, "UPDATE");
		mMap.put(aLCPolSet, "UPDATE");
		mMap.put(aLCAppntSet, "UPDATE");
		mMap.put(aLDPersonSet, "UPDATE");
		mMap.put(aLPContSet, "UPDATE");
		mMap.put(aLPPolSet, "UPDATE");
		mMap.put(aLPAppntSet, "UPDATE");
		mMap.put(aLPPersonSet, "UPDATE");
		if (tNewAdd)
		{
			mMap.put(aLCAddressSet, "INSERT");
		}
		else
		{
			mMap.put(aLCAddressSet, "UPDATE");
			mMap.put(aLPAddressSet, "UPDATE");
		}
                mResult.add(mMap);
		return true;

    }
    //20081212 zhanggm 交换被保人信息，该投保人是其它保单被保人时用到
    public boolean setLCInsured()
    {
    	Reflections tRef = new Reflections();
    	LPInsuredSet tLPInsuredSet = new LPInsuredSet();
    	LCInsuredSet tLCInsuredSet = new LCInsuredSet();
    	LPInsuredDB aLPInsuredDB = new LPInsuredDB();
    	aLPInsuredDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
    	aLPInsuredDB.setEdorType(mLPEdorItemSchema.getEdorType());
    	LPInsuredSet aLPInsuredSet = aLPInsuredDB.query();
    	for (int i = 1; i <= aLPInsuredSet.size(); i++)
		{
    		LPInsuredSchema aLPInsuredSchema = aLPInsuredSet.get(i);
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
			LCInsuredDB aLCInsuredDB = new LCInsuredDB();
			aLCInsuredDB.setInsuredNo(aLPInsuredSchema.getInsuredNo());
			aLCInsuredDB.setContNo(aLPInsuredSchema.getContNo());
			if (!aLCInsuredDB.getInfo())
			{
				mErrors.copyAllErrors(aLCInsuredDB.mErrors);
				mErrors.addOneError(new CError("查询投保人"+aLCInsuredDB.getInsuredNo()+"信息失败！"));
				return false;
			}
			LCInsuredSchema aLCInsuredSchema = aLCInsuredDB.getSchema();
			tRef.transFields(tLPInsuredSchema, aLCInsuredSchema);
			tLPInsuredSchema.setEdorNo(aLPInsuredSchema.getEdorNo());
			tLPInsuredSchema.setEdorType(aLPInsuredSchema.getEdorType());
			
			tRef.transFields(tLCInsuredSchema, aLPInsuredSchema);
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
			if("".equals(tLCInsuredSchema.getRelationToMainInsured())||null==tLCInsuredSchema.getRelationToMainInsured())
			{
				tLCInsuredSchema.setRelationToMainInsured("00");
			}
			tLPInsuredSet.add(tLPInsuredSchema);
			tLCInsuredSet.add(tLCInsuredSchema);
		}
    	mMap.put(tLPInsuredSet, "UPDATE");
		mMap.put(tLCInsuredSet, "UPDATE");
    	return true;
    }

}
