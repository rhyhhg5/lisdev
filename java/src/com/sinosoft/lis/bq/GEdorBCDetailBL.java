package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class GEdorBCDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    private LPEdorItemSet saveLPEdorItemSet = new LPEdorItemSet();
    private LPEdorMainSet saveLPEdorMainSet = new LPEdorMainSet();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    public GEdorBCDetailBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //数据校验操作
        if (!checkData())
            return false;
        System.out.println("---End checkData---");

        //数据准备操作
        if (mOperate.equals("INSERT||EDOR"))
        {
            if (!prepareData())
                return false;
            System.out.println("---End prepareData---");
        }
        //数据准备操作
        if (mOperate.equals("DELETE||EDOR"))
        {
            if (!deleteData())
                return false;
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start GEdorBCDetailBL Submit...sc");

        if (!tPubSubmit.submitData(mResult, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSet = (LPEdorItemSet) mInputData.getObjectByObjectName(
                    "LPEdorItemSet", 0);
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorItemSchema",
                    0);

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GEdorDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return
     */
    private boolean checkData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);
        if (!tLPGrpEdorItemDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PInsuredBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "无保全申请数据!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        //将查询出来的团体保全主表数据保存至模块变量中，省去其它的重复查询
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemDB.getSchema());
        if (tLPGrpEdorItemDB.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PInsuredBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "该保全已经申请确认不能修改!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     * @return
     */
    private boolean prepareData()
    {
        //按个人保全主表进行处理
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            LPPolSchema tLPPolSchema = new LPPolSchema();
            tLPEdorItemSchema = mLPEdorItemSet.get(i);

            ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
            if (tLPEdorItemSchema.getPolNo()!=null)
            {
              tLPEdorItemSchema.setPolNo(tLPEdorItemSchema.getPolNo());
              LCPolDB tLCPolDB = new LCPolDB();
              tLCPolDB.setPolNo(tLPEdorItemSchema.getPolNo());
              if(tLCPolDB.getInfo()){
                  ref.transFields(tLPPolSchema,tLCPolDB.getSchema());
                  tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
                  tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
                  map.put(tLPPolSchema,"DELETE&INSERT");
              }
            }
            else
              tLPEdorItemSchema.setPolNo("000000");
            tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
            tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
            tLPEdorItemSchema.setUWFlag("0");
            tLPEdorItemSchema.setMakeDate(currDate);
            tLPEdorItemSchema.setMakeTime(currTime);
            tLPEdorItemSchema.setModifyDate(currDate);
            tLPEdorItemSchema.setModifyTime(currTime);
            saveLPEdorItemSet.add(tLPEdorItemSchema);

            LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
            tLPEdorMainDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPEdorMainDB.setContNo(tLPEdorItemSchema.getContNo());
            LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
            tLPEdorMainSet = tLPEdorMainDB.query();
            if (tLPEdorMainDB.mErrors.needDealError())
            {
                CError.buildErr(this, "查询个人保全主表失败!");
                return false;
            }
            if (tLPEdorMainSet.size() == 0)
            {
                LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
                ref.transFields(tLPEdorMainSchema, tLPEdorItemSchema);
                tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
                tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
                tLPEdorMainSchema.setUWState("0");
                tLPEdorMainSchema.setMakeDate(currDate);
                tLPEdorMainSchema.setMakeTime(currTime);
                tLPEdorMainSchema.setModifyDate(currDate);
                tLPEdorMainSchema.setModifyTime(currTime);
                saveLPEdorMainSet.add(tLPEdorMainSchema);
            }
        }
        map.put(saveLPEdorItemSet, "INSERT");
        map.put(saveLPEdorMainSet, "INSERT");
        mResult.clear();
        mResult.add(map);

        return true;
    }

    /**
     * 准备需要保存的数据
     * @return
     */
    private boolean deleteData()
    {

        String cotnNoStr = "";
        //按个人保全主表进行处理
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            if (i != mLPEdorItemSet.size())
            {
                cotnNoStr = cotnNoStr + "'" + mLPEdorItemSet.get(i).getContNo() +
                            "',";
            }
            else
                cotnNoStr = cotnNoStr + "'" + mLPEdorItemSet.get(i).getContNo() +
                            "'";

            String deleteSql = "delete from lpedoritem where contno='"
                               + mLPEdorItemSet.get(i).getContNo()
                               + "' and edorno='" +
                               mLPEdorItemSet.get(i).getEdorNo() + "'"
                               + " and edortype='" +
                               mLPEdorItemSet.get(i).getEdorType() + "'";
            if (mLPEdorItemSet.get(i).getInsuredNo() != null &&
                !mLPEdorItemSet.get(i).getInsuredNo().equals("") &&
                !mLPEdorItemSet.get(i).getInsuredNo().equals("000000"))
            {
                cotnNoStr = cotnNoStr + "' and InsuredNo='" +
                            mLPEdorItemSet.get(i).getContNo() + "'";
                if (mLPEdorItemSet.get(i).getPolNo() != null &&
                    !mLPEdorItemSet.get(i).getPolNo().equals("") &&
                    !mLPEdorItemSet.get(i).getPolNo().equals("000000"))
                {
                    cotnNoStr = cotnNoStr + "' and PolNo='" +
                                mLPEdorItemSet.get(i).getPolNo() + "'";
                }
            }
            //删除个人批改项目
            map.put(deleteSql, "DELETE");

        }
//         //删除个人批改项目
//         String sql="delete from LPEdorItem where edorno='"
//                    + mLPGrpEdorItemSchema.getEdorNo()
//                    +"' and edortype='"+mLPGrpEdorItemSchema.getEdorType()
//                    + "' and ContNo in ("
//                    + cotnNoStr + ")";
//         map.put(sql,"UPDATE");
        //当个人批单主表没有批改项目时需要删掉个人批改主表
        String sql1 = "delete from LPEdorMain a where a.edorno='"
                      + mLPGrpEdorItemSchema.getEdorNo()
                      + "' and a.contno in ("
                      + cotnNoStr +
                ") and 0=(select count(1) from lpedoritem b where b.contno=a.contno "
                      + "and b.edorNo='" + mLPGrpEdorItemSchema.getEdorNo() +
                      "') ";
        map.put(sql1, "UPDATE");
        mResult.clear();
        mResult.add(map);

        return true;
    }

}
