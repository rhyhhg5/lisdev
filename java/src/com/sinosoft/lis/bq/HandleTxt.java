package com.sinosoft.lis.bq;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class HandleTxt {

	private MMap mMap = new MMap();
	private String txtName=new String();
	private String path;

    public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	// 获取一个文件夹下的所有文件 要求：后缀名为txt (可自己修改)     
	private List getFileList(File file) {  
        List result = new ArrayList();  
        if (!file.isDirectory()) {  
            result.add(file.getAbsolutePath());  
        } else {  
            // 内部匿名类，用来过滤文件类型  
            File[] directoryList = file.listFiles(new FileFilter() {  
                public boolean accept(File file) {  
                    if (file.isFile() && file.getName().indexOf("crstxt") > -1) {  
                        return true;  
                    } else {                    	
                        return false;  
                    }  
                }  
            });  
            for (int i = 0; i < directoryList.length; i++) {  
                result.add(directoryList[i].getAbsolutePath());  
            }  
        }  
        return result;  
    }  
	
    //根据文件的路径获取文件名称
    private String getTxtName(String interFilePath){
    	if ((interFilePath != null) && (interFilePath.length() > 0)) {  
            int a = interFilePath.lastIndexOf('.');  
            int b = interFilePath.lastIndexOf('\\');
            if ((a > -1) && (a < (interFilePath.length()))) {  
            	txtName =interFilePath.substring((b+1), a);  
            }  
       }  
	System.out.println("文件名:"+txtName);
	return txtName;		
    }
    
    //读取TXT内容，并且根据（,号）读取
	private boolean readTextDate(String interFilePath) throws java.io.FileNotFoundException,
	java.io.IOException {
		
		List tResult = new ArrayList();
		LDCode1Set tLDCode1Set=new LDCode1Set();
		FileReader fr = new FileReader(interFilePath);				
		BufferedReader br = new BufferedReader(fr);
		LDCode1DB tLdCode1DB=new LDCode1DB();
		
		String Line = "";
		int max=0;
		int intFileNum = 0;
		while ((Line =br.readLine())!= null) {
			tResult.add(Line);
			
			intFileNum = intFileNum + 1;
			if (intFileNum <= 5000){
				tLDCode1Set=new LDCode1Set();
				for (int i = 0; i < intFileNum; i++) {
					String[] SingleData = ((String) tResult.get(i)).split("\\,");
					LDCode1Schema tLDCode1Schema=new LDCode1Schema();					
					tLDCode1Schema.setCodeType("crs");										
					tLDCode1Schema.setCode("CS");;
					tLDCode1Schema.setCode1(SingleData[0]); //合同号
					tLDCode1Schema.setCodeName(SingleData[1]);//中介机构
					tLDCode1Schema.setCodeAlias(SingleData[2]);//身份证号
					tLDCode1Schema.setComCode(SingleData[3]);//业务员代码
					tLDCode1Schema.setOtherSign(SingleData[4]);//业务员姓名
					
					tLDCode1Set.add(tLDCode1Schema);
				}
				mMap = new MMap();
				mMap.put(tLDCode1Set, "DELETE&INSERT");
				if (!SubmitMap()){
					br.close();
					return false;					
				}
				intFileNum = 0;
				tResult = new ArrayList();
			}							
			
		}
			File f2=new File(interFilePath);
			f2.delete();
			br.close();
			fr.close();
			return true;
		
	}	
	
	/*数据处理*/
	private boolean SubmitMap() {
		PubSubmit tPubSubmit = new PubSubmit();
		VData mResult = new VData();
		mResult.add(mMap);
		if (!tPubSubmit.submitData(mResult, "")) {
			System.out.println("提交数据失败！");
			return false;
		}
		return true;
	}

	
		
	

	
	
	
	public boolean deal() {		
		File fl=new File(path);
		List list=getFileList(fl);
		String a = null;
		for(int i = 0; i < list.size(); i++)  
	        {  
	            a=list.get(i).toString();	           
	            System.out.println(a);  
	            try {
	    			if(!readTextDate(a)){
	    				return false;
	    			};
	    		} catch (FileNotFoundException e) {
	    			e.printStackTrace();
	    			return false;
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    			return false;
	    		}

	        } 
		return true;
		
	}
	public static void main(String args[]) {
		HandleTxt ht = new HandleTxt();
		ht.deal();
	}
}
