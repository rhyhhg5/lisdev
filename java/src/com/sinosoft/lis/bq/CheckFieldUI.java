package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import java.util.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:校验功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class CheckFieldUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public CheckFieldUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    CheckFieldBL tCheckFieldBL = new CheckFieldBL();
    System.out.println("---UI BEGIN---"+mOperate);
    if (tCheckFieldBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tCheckFieldBL.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "CheckFieldUI";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据查询失败!";
//      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tCheckFieldBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    System.out.println("-------test...");
	/**GlobalInput tG = new GlobalInput();
	tG.Operator ="001";
	tG.ComCode  ="001";
	LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
	LPEdorItemSet tLPEdorItemSet=new LPEdorItemSet();
	tLPEdorItemSchema.setPolNo("86110020030210034951");
	tLPEdorItemSchema.setEdorNo("");
	tLPEdorItemSchema.setInsuredName("李虹");
	tLPEdorItemSchema.setEdorType("ZT");
	//tLPEdorMainSchema.setEdorValiDate("2004-04-08");
	tLPEdorItemSchema.setChgPrem("1000.0");
	tLPEdorItemSchema.setChgAmnt("");
	tLPEdorItemSchema.setOperator("001");
	//tLPEdorMainSchema.setEdorAppDate();
	tLPEdorItemSet.add(tLPEdorItemSchema);

	VData tVCheckData = new VData();
	tVCheckData.addElement(tG);
	tVCheckData.addElement(tLPEdorItemSchema);
	tVCheckData.addElement("VERIFY||BEGIN");
	tVCheckData.addElement("PEDORINPUT#EDORTYPE");

    CheckFieldUI tCheckFieldUI = new CheckFieldUI();
	if (tCheckFieldUI.submitData(tVCheckData,"CHECK||FIELD")) {
	 }*/
  }
}
