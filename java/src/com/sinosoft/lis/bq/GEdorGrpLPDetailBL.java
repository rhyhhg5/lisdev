package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpAppntDB;


public class GEdorGrpLPDetailBL
{
        /** 往后面传输数据的容器 */
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private static String mEdorType = "LP";

    private LCGrpAppntSchema mLCGrpAppntSchema;

    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GEdorGrpLPDetailBL(GlobalInput gi, String edorNo, String grpContNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        try {
            mLCGrpAppntSchema = (LCGrpAppntSchema) mInputData.
                                   getObjectByObjectName("LCGrpAppntSchema",
                    0);
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean dealData()
    {
        // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
        mMap.put("delete from LPGrpAppnt where edorno='"+this.mEdorNo+"' and edortype='"+this.mEdorType+"'"
                 , "DELETE");
        LCGrpAppntDB tLCGrpAppntDB=new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(this.mGrpContNo);
        LCGrpAppntSchema tLCGrpAppntSchema=null;
        if(!tLCGrpAppntDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "GEdorGrpLPDetailBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询团体投保人表出错！";
            this.mErrors.addOneError(tError);
            return false;

        }
        tLCGrpAppntSchema=tLCGrpAppntDB.getSchema();
        LPGrpAppntSchema tLPGrpAppntSchema=new LPGrpAppntSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPGrpAppntSchema, tLCGrpAppntSchema);
        tLPGrpAppntSchema.setClaimBankCode(mLCGrpAppntSchema.getClaimBankCode());
        tLPGrpAppntSchema.setClaimBankAccNo(mLCGrpAppntSchema.getClaimBankAccNo());
        tLPGrpAppntSchema.setClaimAccName(mLCGrpAppntSchema.getClaimAccName());
        tLPGrpAppntSchema.setEdorNo(this.mEdorNo);
        tLPGrpAppntSchema.setEdorType(this.mEdorType);
        tLPGrpAppntSchema.setModifyDate(mCurrentDate);
        tLPGrpAppntSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPGrpAppntSchema, "INSERT");

        changeEdorState(BQ.EDORSTATE_INPUT);

        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void changeEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
