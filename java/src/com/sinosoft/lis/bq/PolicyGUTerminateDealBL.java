package com.sinosoft.lis.bq;

import java.util.ArrayList;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PolicyTerminateDealBL.java
//程序功能：失效满2年的保单设置“终止”状态
//创建日期：2010-03-26
//创建人  ：zhanggm
//更新记录：

public class PolicyGUTerminateDealBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private LLCaseCommon mLLCaseCommon = new LLCaseCommon();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL mExeSQL = new ExeSQL();
    private String mGetStartDate = "";
    private String mGrpContNo = "";
    private FDate tFDate = new FDate();
    private String GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
    private String GGBalaCoutn = "";
    private LCInsureAccClassSet mLCInsureAccClassSet = null;//记录子账户结息后的信息
    private LCInsureAccSchema mLCInsureAccSchema = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度
    private String mBalanceSeq = null;
    private String mStartDate = null;
    private Reflections ref = new Reflections();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public PolicyGUTerminateDealBL() {}


    private boolean getInputData(VData cInputData) 
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mManageCom = mG.ManageCom;
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        cInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        try
        {
        	callGUTerminate();
        }
        catch(Exception ex)
        {
            System.out.print(ex.toString());
        }
        return true;
    }


    /***************************团险万能分单满期终止处理********************************/
    private boolean callGUTerminate() 
    {
        System.out.println("团险万能分单满期批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        String strSql = "select * from LCPol a where  "
                      + " conttype = '2' and " 
                      +" exists ( select 1 from lcget where polno=a.polno and getdutykind is not null and livegettype='0' and getstartdate <= '" + mCurrentDate + "') " //cbs00040508 应缴日到可恢复有效的时间中间跨度是2年60天,跟条款保持一致
                      + "and exists (select 1 from LMRiskApp where risktype4='4' and riskprop ='G' and riskcode = a.riskCode)  " //长期险满期终止
                      + subSql + " and stateflag = '1'  and poltypeflag<>'2'  and polstate not in ('03060001','03060002') with ur ";
        System.out.println("Sql ：" + strSql);
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolSet = tLCPolDB.executeQuery(strSql);
        
        try
        {
//        	逐条调用险种保单终止处理
        	SingleTerminateDeal(tLCPolSet);
        }
        catch(Exception ex)
        {
            System.out.print(ex.toString());
        }
        
      
        
        //查询保单
//        String Sql = "select * from LCCont a where  1=1  "
//                   + "and conttype = '1' and a.paytodate + 2 years + 60 days <= '" + mCurrentDate + "' "
//                   + "and stateflag = '2'  "
//                   + "and exists (select 1 from lccontstate where statereason = '09' and state = '1' " 
//                   + "and statetype = 'Terminate' and contno = a.contno and polno <> '" + BQ.FILLDATA + "')"
//                   + subSql + " with ur";
//        System.out.println("Sql ：" + Sql);
//        LCContSet tLCContSet = new LCContSet();
//        LCContDB tLCContDB = new LCContDB();
//        tLCContSet = tLCContDB.executeQuery(Sql);
//        
//        try
//        {
////        	保单永久失效处理
//        	SinglePolicyTerminateDeal(tLCContSet);
//        }
//        catch(Exception ex)
//        {
//            System.out.print(ex.toString());
//        }
//        System.out.println("团险万能分单满期批处理运行结束。。。。");

        return true;
    }

    private boolean SingleTerminateDeal(LCPolSet aLCPolSet) 
    {
        for (int i = 1; i <= aLCPolSet.size(); i++) 
        {
        	
            MMap tMap = new MMap();
            VData tData = new VData();
            PubSubmit tPubSubmit = new PubSubmit();
            
            
            
            // 判断是否满足剩余的条件
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema.setSchema(aLCPolSet.get(i));
            String tstartdate = "select distinct getstartdate from lcget where " +
            		"polno = '"+tLCPolSchema.getPolNo()+"' " +
            				"and  getdutykind is not null and livegettype='0' fetch first 1 rows only with ur ";
            SSRS tSSRS = new ExeSQL().execSQL(tstartdate);
            
			
        	if(tSSRS == null || tSSRS.getMaxRow()<=0){
        		mErrors.addOneError("获取起领时间出错！");
                return false;
        	}
        	
        	mGetStartDate = tSSRS.GetText(1, 1);
        	String tCheckDate = PubFun.calDate(tLCPolSchema.getCValiDate(), 1, "M", null);
            if(tFDate.getDate(mGetStartDate).before
            		(tFDate.getDate(tCheckDate))||
            		tFDate.getDate(mGetStartDate).equals(tFDate.getDate(tCheckDate)))
            {
            	String tFlagDate = PubFun.calDate(tLCPolSchema.getCValiDate(), 1, "Y", null);
            	if(tFDate.getDate(mCurrentDate).equals(tFDate.getDate(tFlagDate))||
            			tFDate.getDate(mCurrentDate).after(tFDate.getDate(tFlagDate)))
            		mGetStartDate=PubFun.calDate(tLCPolSchema.getCValiDate(), 1, "Y", null);
            	else
            		continue;
            }
            if (!canTerminate(tLCPolSchema)) 
            {
                continue;
            }
            
            if(!calFee(tLCPolSchema)){
            	continue;
            }
          
            
            setLCPol(tLCPolSchema,tMap);
            System.out.println(tLCPolSchema.getPolNo() + "-满期终止-" + tLCPolSchema.getRiskCode());
            tData.clear();
            tData.add(tMap);
            tPubSubmit.submitData(tData, "");
        }
        return true;
    }
    
    
    /**
     * getInsuAccRateUnBalance
     * 查询未结算的月收益利率
     * @param cLCInsureAccClassSchema LCInsureAccClassSchema，子账户
     * @return LMInsuAccRateSet，月收益率
     */
    private Interest000001Set getInterest000001(String tDate,LCPolSchema mLCPol)
    {
        String sql = " select a.* from Interest000001 a "
                     + "where 1 = 1 and "
                     + "   '"+tDate+"'  >= startdate "
                     +" and '"+tDate+"' < enddate "
                     + "and (grpcontno = '000000' or grpcontno = '"+mGrpContNo+"') "
                     + " and riskcode='"+mLCPol.getRiskCode()+"' "//20150629 新增产品，需要关联险种代码
                     + "order by a.startdate ";
        System.out.println(sql+"====获取待结算利率信息");
        Interest000001DB tInterest000001DB = new Interest000001DB();
        Interest000001Set tInterest000001Set = tInterest000001DB.executeQuery(sql);
        String sqlByGrpContNo = " select a.* from Interest000001 a " +
        		"where 1 = 1  and " 
        		 		+ "   '"+tDate+"'  >= startdate "
        		 		+" and '"+tDate+"' < enddate "
        		 		+" and GrpContNo = '"+mGrpContNo+"'"
        		 		+ "order by a.startdate ";
        Interest000001Set tInterest000001Set1 = tInterest000001DB.executeQuery(sqlByGrpContNo);//用以保存个别团单录入的利率
        for(int i=1;i<=tInterest000001Set1.size();i++){
        	for(int j=1;j<=tInterest000001Set.size();j++){
        		if(tInterest000001Set1.get(i).getStartDate().equals(tInterest000001Set.get(j).getStartDate()) 
        		&& tInterest000001Set1.get(i).getEndDate().equals(tInterest000001Set.get(j).getEndDate())
        		&& tInterest000001Set1.get(i).getRiskCode().equals(tInterest000001Set.get(j).getRiskCode())
        		&& !tInterest000001Set1.get(i).getGrpContNo().equals(tInterest000001Set.get(j).getGrpContNo())){
        			tInterest000001Set.remove(tInterest000001Set.get(j));
        			j--;
        		}
        	}
        }
        if (tInterest000001Set.mErrors.needDealError())
        {
            System.out.println(tInterest000001DB.mErrors.getErrContent());
            mErrors.addOneError("查询符合该账户的月收益率发生异常");
            return null;
        }
        if (tInterest000001Set.size() == 0)
        {
        	String tempDate = PubFun.calDate(tDate, -1, "M", null);
        	tInterest000001Set = getInterest000001(tempDate,mLCPol);
        	if(tInterest000001Set.size() == 0){
        		
        		System.out.println(tInterest000001DB.mErrors.getErrContent());
                mErrors.addOneError("查询符合该账户的月收益率发生异常，满期日前一个月的利率未获取到");
                return null;
        	}
        }

        return tInterest000001Set;
    }
    
    private LCInsureAccClassSet getAccClass(LCInsureAccSchema
            tLCInsureAccSchema)
	{
	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
	tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
	tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
	LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
	if (tLCInsureAccClassDB.mErrors.needDealError())
	{
	CError.buildErr(this, "子帐户信息查询失败");
	return null;
	}
	if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1)
	{
	CError.buildErr(this, "没有子帐户信息");
	return null;
	}
	return tLCInsureAccClassSet;
	}
    
    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth()
    {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }
    
    private MMap dealOneAccClass(LCInsureAccClassSchema tLCInsureAccClassSchema,
            String cRateIntv, String cRatetype,String tRate )
	{
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", mStartDate);
		tTransferData.setNameAndValue("EndDate", mEndDate);
		tTransferData.setNameAndValue("RiskCode",
		                 tLCInsureAccClassSchema.getRiskCode());
		tTransferData.setNameAndValue("RateIntv", cRateIntv); //利率间隔
		tTransferData.setNameAndValue("RateType", cRatetype); //利率类型
		tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
		tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
		
		tTransferData.setNameAndValue("Rate", tRate);//利率  根据cBalaType，利率对应结算利率和保证利率
		
		
		VData data = new VData();
		data.add(mG);
		data.add(tLCInsureAccClassSchema);
		data.add(tTransferData);
		
		MMap tMMapAV = balanceOneAccRate(data);
		return tMMapAV;
	}
    
    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate)
    {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);
        mG = (GlobalInput) cInputDate
              .getObjectByObjectName("GlobalInput", 0);
        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        if (tTransferData == null || mG == null
            || tLCInsureAccClassSchema == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return null;
        }

        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mG);

        //查询上一次结算信息
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(tLCInsureAccClassSchema.
                                             getBalaDate());
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        if (tLCInsureAccBalanceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到上一次结算信息");
            return null;
        }
        double tLastBala = tLCInsureAccBalanceSet.get(1).getInsuAccBalaAfter();
        tTransferData.setNameAndValue("LastBala", "" + tLastBala);

        //计算账户收益
        AccountManage tAccountManage = new AccountManage();
        MMap tMMapLX = tAccountManage.getUliBalaAccInterest(tTransferData);
        if (tMMapLX == null)
        {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }

        LCInsureAccClassSchema accClassDelt
                = (LCInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        accClassDelt.setInsuAccBala(PubFun.setPrecision(accClassDelt.
                getInsuAccBala(), "0.00"));
        LCInsureAccTraceSchema accTraceDelt
                = (LCInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccTraceSchema", 0);
        accTraceDelt.setOtherType("6"); //月结算
        accTraceDelt.setOtherNo(getBalanceSequenceNo());
        accTraceDelt.setMoney(PubFun.setPrecision(accTraceDelt.getMoney(),
                                                  "0.00"));
        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);

        return tMMapAll;
    }
	   
    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        if (mBalanceSeq == null)
        {
            String[] dateArr = PubFun.getCurrentDate().split("-");
            String date = dateArr[0] + dateArr[1] + dateArr[2];
            mBalanceSeq = date
                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
        }

        return mBalanceSeq;
    }
    
    //根据子账户获取账户总金额
    public double getAccBalaByAccClass(LCInsureAccClassSet tLCInsureAccClassSet){
    	double accBala = 0;
    	for(int i=1;i<=tLCInsureAccClassSet.size();i++){
    		accBala = Arith.add(accBala, tLCInsureAccClassSet.get(i).getInsuAccBala());
    	}
    	return accBala;
    }
    
    //根据账户在全局变量中获取子账户
    public LCInsureAccClassSet getAccClassSetFromM(LCInsureAccSchema tLCInsureAccSchema){
    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    		for(int i=1;i<=mLCInsureAccClassSet.size();i++){
        		if(mLCInsureAccClassSet.get(i).getPolNo().equals(tLCInsureAccSchema.getPolNo()) 
        				&& mLCInsureAccClassSet.get(i).getInsuAccNo().equals(tLCInsureAccSchema.getInsuAccNo())){
        			tLCInsureAccClassSet.add(mLCInsureAccClassSet.get(i));
        		}
        	}
    	return tLCInsureAccClassSet;
    }
    
//  获取当前人的管理费
   private double getGlFeeByPolno(String aPolno,double aGlFee,String aBcStartDate){
   	String maxPaydateSql = "select min(paydate) from LCInsureAcctrace " +
   			"where grpcontno = '"+mGrpContNo+"' " +
   			"and polno = '"+aPolno+"' ";
		String minPaydate = new ExeSQL().getOneValue(maxPaydateSql);
		FDate fDate = new FDate();
		if(fDate.getDate(minPaydate).after(fDate.getDate(aBcStartDate))){
			int tIntv = PubFun.calInterval(minPaydate, mEndDate, "D");
			int monthLength = PubFun.monthLength(minPaydate);
//			System.out.println(Arith.div(aGlFee, monthLength,2));
//			System.out.println(Arith.mul(tIntv, Arith.div(aGlFee, monthLength,2)));
			return Arith.round(Arith.mul(tIntv, Arith.div(aGlFee, monthLength)),2);
		}else{
			return aGlFee;
		}
   }
   
 //管理费收取轨迹
   public MMap getTrace(LCInsureAccClassSchema tLCInsureAccClassSchema,double fee,String aAIPolNo,String FeeCode){
   	MMap tMMap = new MMap();
//		管理费轨迹
//		String RiskFeeSql = "select feecode,feevalue from LMRiskFee where insuaccno = '"+tLCInsureAccClassSchema.getInsuAccNo()+"' and payplancode = '"+tLCInsureAccClassSchema.getPayPlanCode()+"'";
//		SSRS RiskFeeSSRS = new ExeSQL().execSQL(RiskFeeSql);
		LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
		ref.transFields(schema, tLCInsureAccClassSchema);
       String tLimit = PubFun.getNoLimit(schema.getManageCom());
       String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
       schema.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
       schema.setSerialNo(serNo);
       schema.setFeeCode(FeeCode);
       schema.setFee(fee);
       schema.setOtherNo(this.getBalanceSequenceNo());
       schema.setOtherType("6"); //月结算
       schema.setFeeRate("0");
       schema.setPayDate(tLCInsureAccClassSchema.getBalaDate());
       schema.setState("0");
       PubFun.fillDefaultField(schema);
       String moneyType = "MF";
       schema.setMoneyType(moneyType);
       tMMap.put(schema, SysConst.INSERT);
       
//     管理费的账户轨迹
       LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
       ref.transFields(traceSchema, schema);

       traceSchema.setSerialNo(serNo);
       traceSchema.setAIPolNo(aAIPolNo);
       traceSchema.setMoney( -Math.abs(schema.getFee()));
       tMMap.put(traceSchema, SysConst.INSERT);
       return tMMap;
   }
   
   /**
    * getLCInsureAccBalance
    * 生成结算轨迹
    * @param cAccValue double
    * @param cAccValueGurat double
    * @return Object
    */
   private LCInsureAccBalanceSchema getLCInsureAccBalance1(LCInsureAccSchema glLCInsureAccSchema,Interest000001Schema
           cInterest000001Schema,
           double cAccValue,String flag,String isgg)
   {
       LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
       ref.transFields(schema, glLCInsureAccSchema);

//       schema.setSequenceNo(getBalanceSequenceNo());
       if("1".equals(isgg) ){//isgg 1 为公共账户
       	if(!"".equals(GGSequenceno)){
       		schema.setSequenceNo(GGSequenceno);
       	}else{
       		schema.setSequenceNo(getBalanceSequenceNo());
       		GGSequenceno = schema.getSequenceNo();
       	}
       }else{
       	schema.setSequenceNo(getBalanceSequenceNo());
       }
       schema.setInsuAccBalaBefore(glLCInsureAccSchema.getInsuAccBala());
       schema.setInsuAccBalaAfter(cAccValue);
       //这里由于不再计算保证帐户价值，所以直接置InsuAccBalaGurat ＝ InsuAccBalaAfter,
       if("370101".equals(glLCInsureAccSchema.getRiskCode())){
       	schema.setInsuAccBalaGurat("");
       }else{
       	schema.setInsuAccBalaGurat(cAccValue);
       }
       schema.setDueBalaDate(mEndDate);
       schema.setDueBalaTime("00:00:00");
       schema.setRunDate(PubFun.getCurrentDate());
       schema.setRunTime(PubFun.getCurrentTime());
       schema.setPolYear(mPolYear);
       schema.setPolMonth(mPolMonth);
       schema.setPrintCount(0);
       schema.setOperator(mG.Operator);
       PubFun.fillDefaultField(schema);
       if("".equals(flag)){
       	String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
               + "where PolNo = '"
               + glLCInsureAccSchema.getPolNo() + "' "
               + "   and InsuAccNo = '"
               + glLCInsureAccSchema.getInsuAccNo() + "' ";
       	String maxBalaCount = new ExeSQL().getOneValue(sql);
       	if (maxBalaCount == null || maxBalaCount.equals("") ||
       			maxBalaCount.equals("null"))
       	{
       		maxBalaCount = "1";
       	}
       	schema.setBalaCount(maxBalaCount);
       	if("1".equals(isgg)){
       		GGBalaCoutn = maxBalaCount;
           }
       }else{
       	schema.setBalaCount(GGBalaCoutn);
       }
       return schema;
   }
   //生成结算信息
   public MMap getBalance(LCInsureAccSchema glLCInsureAccSchema,Interest000001Schema tInterest000001Schema,double tAccValue,String flag,String isGG){
   	MMap BaMMap = new MMap();
   	LCInsureAccBalanceSchema tLCInsureAccBalanceSchema
   	  = getLCInsureAccBalance1(glLCInsureAccSchema,tInterest000001Schema,
   	                          tAccValue,flag,isGG);
   	    if (tLCInsureAccBalanceSchema == null)
   	    {
   	        return null;
   	    }
   	    if("".equals(flag)){// 公共账户未存库及所有个人账户处理
   	    	BaMMap.put(tLCInsureAccBalanceSchema, SysConst.INSERT);
   	    }else{
   	    	tLCInsureAccBalanceSchema.setSequenceNo(flag);
   	    	BaMMap.put(tLCInsureAccBalanceSchema, SysConst.UPDATE);
   	    }
   	    
   	    return BaMMap;
   }
   
   /**
    * updateAccInfo
    * 更新总账户信息
    * @return MMap
    */
   private MMap updateAccInfo(LCInsureAccSchema aLCInsureAccSchema)
   {
       MMap tMMap = new MMap();

       String sql = "update LCInsureAccClass a "
                    + "set (InsuAccBala, LastAccBala) "
                    + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                    + "where PolNo = '"
                    + aLCInsureAccSchema.getPolNo() + "' "
                    + "   and InsuAccNo = '"
                    + aLCInsureAccSchema.getInsuAccNo() + "' ";
       tMMap.put(sql, SysConst.UPDATE);
       //更新账户信息
       sql = "update LCInsureAcc a "
             + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
             + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
             " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
             + " group by Operator, ModifyDate, ModifyTime) "
             + "where PolNo = '"
             + aLCInsureAccSchema.getPolNo() + "' "
             + "   and InsuAccNo = '"
             + aLCInsureAccSchema.getInsuAccNo() + "' ";
       tMMap.put(sql, SysConst.UPDATE);
       
       //按轨迹更新账户管理费信息
       sql = "update LCInsureAccClassFee a " +
             "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
             + "where PolNo = '"
             + aLCInsureAccSchema.getPolNo() + "' "
             + "   and InsuAccNo = '"
             + aLCInsureAccSchema.getInsuAccNo() + "' ";
       tMMap.put(sql, SysConst.UPDATE);

       //更新账户管理费信息
       sql = "update LCInsureAccFee a " +
             "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
             + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
             " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
             + " group by Operator, ModifyDate, ModifyTime) "
             + "where PolNo = '"
             + aLCInsureAccSchema.getPolNo() + "' "
             + "   and InsuAccNo = '"
             + aLCInsureAccSchema.getInsuAccNo() + "' ";
       tMMap.put(sql, SysConst.UPDATE);

       return tMMap;
   }
    
    private boolean calFee(LCPolSchema tLCPolSchema){

        //查询出当前账户最后结算日期比已公布利率日期晚的账户，由于理赔或保全等原因，一个账户可能需要结算多次
    	mGrpContNo = tLCPolSchema.getGrpContNo();
    	
    	String FeeSql = "select feevalue,riskfeemode,feecode from lcgrpfee where grpcontno = '"+mGrpContNo+"' and insuaccno = '000000'";
    	SSRS FeeSSRS = new ExeSQL().execSQL(FeeSql);
    	if(FeeSSRS == null || FeeSSRS.getMaxRow()<=0 || FeeSSRS.getMaxRow()>1){
    		mErrors.addOneError("获取管理费收取方式及金额失败！");
            return false;
    	}
    	double glFee = Double.parseDouble(FeeSSRS.GetText(1, 1));//管理费金额
    	String glFeeMode = FeeSSRS.GetText(1, 2);
    	String FeeCode = FeeSSRS.GetText(1, 3);
    	String Accsql = "select a.* from LCInsureAcc a where a.grpcontno = '"+mGrpContNo+"' order by baladate fetch first 1 rows only";
    	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    	LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(Accsql);
    	
    	
    	Interest000001Set tInterest000001Set = getInterest000001(mGetStartDate,tLCPolSchema);
        if (tInterest000001Set == null)
        {
            return false;
        }
        //根据团单号获取利率
//        String RateByGrpContNosql = "select * from ";
        ArrayList tlist = new ArrayList(); //用以记录扣除管理费时余额不足的被保人客户号
        for (int m = 1; m <= tInterest000001Set.size(); m++)
        {
           LCInsureAccSchema mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
           LCInsureAccClassSet mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;
           double GGAccValue = 0;
           boolean getGG = false;//公共账户是否已从数据库获取，false 未获取，true 已获取
           GGSequenceno = "";//公共账户是否已处理完存库，""为未存库，非""已存库。
           GGBalaCoutn = "";
        	String InsuredNoSql = "select distinct insuredno,polno,acctype from LCInsureAcc where grpcontno = '"+mGrpContNo+"' and polno='"+tLCPolSchema.getPolNo()+"' and baladate<='"+tInterest000001Set.get(m).getEndDate()+"' order by acctype ";
        	SSRS InsuredNoSSRS = new ExeSQL().execSQL(InsuredNoSql);
        	if(InsuredNoSSRS == null || InsuredNoSSRS.getMaxRow()<=0){
        		mErrors.addOneError("获取需结息的被保人客户号码失败！");
                return false;
        	}
        	String mBcStartDate = tInterest000001Set.get(m).getStartDate();//本次月结的开始时间
            //mEndDate = tInterest000001Set.get(m).getEndDate();
        	mEndDate = PubFun.calDate(mGetStartDate, -1, "D", null);
            String tRate =  String.valueOf(tInterest000001Set.get(m).getRate());
            String tRateIntv = tInterest000001Set.get(m).getRateIntv();
            String tRateType = tInterest000001Set.get(m).getRateType();
        	int tCount = InsuredNoSSRS.getMaxRow(); //记录每个保单的总人数
        	for(int b=1;b<=tCount;b++){
        		if(tlist.contains(InsuredNoSSRS.GetText(b, 1))){//如果该客户在上月月结时余额不足，则本月不进行月结。
        			System.out.println("由于被保人"+InsuredNoSSRS.GetText(b, 1)+"上月月结余额不足，本月不进行月结。");
        			continue;
        		}
        		long t0 = System.currentTimeMillis();
        		String aAIPolNo = InsuredNoSSRS.GetText(b, 2);
//        		mGGLCInsureAccSchema = new LCInsureAccSchema();//公共账户信息
//        	    mGGLCInsureAccClassSet = new LCInsureAccClassSet() ;
        	    mLCInsureAccClassSet = new LCInsureAccClassSet();//记录子账户结息后的信息
//        	    double GGAccValue = 0;
        		Accsql = "select a.* from LCInsureAcc a where a.grpcontno = '"+mGrpContNo+"' and insuredno = '"+InsuredNoSSRS.GetText(b, 1)+"' order by insuaccno";
        		tLCInsureAccDB = new LCInsureAccDB();
            	tLCInsureAccSet = tLCInsureAccDB.executeQuery(Accsql);
            	MMap tPerMMap = new MMap();
            	for(int a=1;a<=tLCInsureAccSet.size();a++){
            		mLCInsureAccSchema = tLCInsureAccSet.get(a);
            		if (mLCInsureAccSchema == null)
                    {
                        return false;
                    }
            		if("001".equals(mLCInsureAccSchema.getAccType())){
            			mGGLCInsureAccSchema = tLCInsureAccSet.get(a);
            			aAIPolNo="";
            		}
            		System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                            + ", 险种" + mLCInsureAccSchema.getRiskCode()
                            + ", 结算起始日期"
                            + tInterest000001Set.get(m).getStartDate() + "的结算");
            		mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
//            		查询帐户子表信息
            		LCInsureAccClassSet tLCInsureAccClassSet = getAccClass(
            				mLCInsureAccSchema);
                    if (tLCInsureAccClassSet == null)
                    {
                        return false;
                    }
                    LCInsureAccClassSchema schemaAV = null; //处理后的账户分类价值
//                    LCInsureAccTraceSchema schemaTraceOne = null; //本次产生的任一个轨迹
                    MMap tMMap = new MMap();
                    for (int n = 1; n <= tLCInsureAccClassSet.size(); n++)
                    {
                        LCInsureAccClassSchema tLCInsureAccClassSchema
                                = tLCInsureAccClassSet.get(n);
                        mStartDate = tLCInsureAccClassSchema.getBalaDate();
                        if (!calPolYearMonth())
                        {
                            return false;
                        }
                        System.out.println("计算账户价值");
                        MMap tMMapAV = dealOneAccClass(tLCInsureAccClassSchema,
                        		tRateIntv, tRateType,tRate);
                        if (tMMapAV == null)
                        {
                            return false;
                        }
                        tMMap.add(tMMapAV);
                        schemaAV = (LCInsureAccClassSchema) tMMapAV
                                   .getObjectByObjectName("LCInsureAccClassSchema", 0);

                        if (schemaAV == null)
                        {
                            mErrors.addOneError("没有计算出账户价值");
                            return false;
                        }
                        mLCInsureAccClassSet.add(schemaAV);//将结息后的子账户信息存入全局变量
                        if("001".equals(schemaAV.getAccType())){
                        	mGGLCInsureAccClassSet.add(schemaAV);
                        	GGAccValue += schemaAV.getInsuAccBala();
                        }
                    }
                    tPerMMap.add(tMMap);
            	}//对个人的所有账户处理完毕
            	System.out.println("第"+b+"个人结息处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t0));
            	long t1 = System.currentTimeMillis();
//            	以下开始收取管理费
            	if("2".equals(glFeeMode)&& !getGG){// 从公共账户收取
            		if(mGGLCInsureAccClassSet==null ||mGGLCInsureAccClassSet.size()<=0){//公共账户已存库。
            			String GGAccSql = "select * from lcinsureacc where grpcontno = '"+mGrpContNo+"' and acctype = '001'";
            			LCInsureAccDB tGGLCInsureAccDB = new LCInsureAccDB();
            			mGGLCInsureAccSchema = tGGLCInsureAccDB.executeQuery(GGAccSql).get(1);
            			if(mGGLCInsureAccSchema==null){
            				mErrors.addOneError("没有获取到公共账户信息！");
                            return false;
            			}
            			mGGLCInsureAccClassSet= getAccClass(mGGLCInsureAccSchema);
            			GGAccValue = getAccBalaByAccClass(mGGLCInsureAccClassSet);
            			String BalaSql ="select Sequenceno,Balacount,InsuAccBalaAfter from LCInsureAccBalance where polno = '"+mGGLCInsureAccSchema.getPolNo()+"' order by balacount desc fetch first 1 rows only";
            			SSRS BalaSSRS = new ExeSQL().execSQL(BalaSql);
            			GGSequenceno = BalaSSRS.GetText(1, 1);
            			GGBalaCoutn = BalaSSRS.GetText(1, 2);
            		}
//            		查看公共账户是否足够，需考虑月结不足满月的情况            		
            		double sumGlFee = 0;
            		for(int i=1;i<=InsuredNoSSRS.getMaxRow();i++){
        				sumGlFee = Arith.add(sumGlFee, getGlFeeByPolno(InsuredNoSSRS.GetText(i, 2),glFee,mBcStartDate));
            		}
            		if(sumGlFee>GGAccValue){
            			System.out.println("公共账户在扣除管理费时余额不足！");
            			glFeeMode = "1";
            		}
            		getGG = true;
            	}
            	double temp = getGlFeeByPolno(InsuredNoSSRS.GetText(b, 2),glFee,mBcStartDate);//获取当前被保人需缴纳的管理费用
            	boolean flag = true;
        		if("2".equals(glFeeMode)){//从公共账户扣除
        	    	for(int j=1;j<=mGGLCInsureAccClassSet.size();j++){
        	    		LCInsureAccClassSchema tGGLCInsureAccClassSchema = mGGLCInsureAccClassSet.get(j);//公共账户只有一个，子账户也有一个
        	    		tGGLCInsureAccClassSchema.setBalaDate(mEndDate);
        	    		MMap MMapTrace = getTrace(tGGLCInsureAccClassSchema,temp,aAIPolNo,FeeCode);//生成管理费轨迹
            			tPerMMap.add(MMapTrace);
        	    	}
        	    	double tAccValue=0;
    	    		tAccValue = Arith.sub(GGAccValue, temp);
//        			String BalaSql ="select Sequenceno,InsuAccBalaAfter from LCInsureAccBalance where polno = '"+mGGLCInsureAccSchema.getPolNo()+"' order by balacount desc fetch first 1 rows only";
//        			SSRS BalaSSRS = new ExeSQL().execSQL(BalaSql);
//        			GGSequenceno = BalaSSRS.GetText(1, 1);
//        	    	MMap MMapBalance = getBalance(mGGLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,GGSequenceno,"1");
    	    		GGAccValue =Arith.sub(GGAccValue, temp);
//        			tPerMMap.add(MMapBalance);
        			tPerMMap.add(updateAccInfo(mGGLCInsureAccSchema));
        			for(int i=1;i<=tLCInsureAccSet.size();i++){
        				mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
                		LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
	        			if(!"001".equals(tLCInsureAccSet.get(i).getAccType())){
	        				mBalanceSeq = null;
	        				LCInsureAccClassSet grLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));
	        				tAccValue = getAccBalaByAccClass(grLCInsureAccClassSet);
		        			MMap MMapBalance1 = getBalance
		        			(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,"","0");
		        			tPerMMap.add(MMapBalance1);
		        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
	        			}
        			}
        		}else{//从个人账户收取
        			double accbala = getAccBalaByAccClass(mLCInsureAccClassSet);//一个人所有账户的总额
        			if(temp>accbala){
        				System.out.println("处理被保人客户号码为"+InsuredNoSSRS.GetText(b, 1)+"的数据时余额不足！");
        				tlist.add(InsuredNoSSRS.GetText(b, 1));
                		continue;
                	}
        			for(int i=1;i<=tLCInsureAccSet.size();i++){
	        			mBalanceSeq = null; //由于月度不同，需要重新生成结算序号
	                	LCInsureAccSchema glLCInsureAccSchema = tLCInsureAccSet.get(i);
	        			LCInsureAccClassSet tLCInsureAccClassSet = getAccClassSetFromM(tLCInsureAccSet.get(i));//从全局变量中获取当前账户的子账户
	        			if(flag){
		        			double accbalacur = getAccBalaByAccClass(tLCInsureAccClassSet);//当前账户的余额
		        			if(accbalacur>temp){
		        				double accTemp = temp;
		        				for(int j=1;j<=tLCInsureAccClassSet.size();j++){
		        					double accTemp1 = accTemp;//本次扣除费用
		                			accTemp = Arith.sub(tLCInsureAccClassSet.get(j).getLastAccBala(), accTemp);
		                			if(accTemp>=0){
		                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),accTemp1,"",FeeCode);//生成管理费轨迹
		                				tLCInsureAccClassSet.get(j).setInsuAccBala(accTemp);//本账户余额够，将剩余钱放入账户
		                				tPerMMap.add(MMapTrace);
		                				flag = false;
		                			}else{
		                				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
		                				tLCInsureAccClassSet.get(j).setInsuAccBala(0);//本账户不够 全部扣除
		                				tPerMMap.add(MMapTrace);
		                				accTemp = Arith.sub(accTemp,tLCInsureAccClassSet.get(j).getLastAccBala());
		                			}
		        				}
		        			}else{//当前账户余额不够扣除管理费
		            			for(int j=1;j<=tLCInsureAccClassSet.size();j++){
		            				MMap MMapTrace = getTrace(tLCInsureAccClassSet.get(j),tLCInsureAccClassSet.get(j).getLastAccBala(),"",FeeCode);//生成管理费轨迹
		            				temp = Arith.sub(temp, tLCInsureAccClassSet.get(j).getLastAccBala());
		            				tLCInsureAccClassSet.get(j).setInsuAccBala(0);
		            				tPerMMap.add(MMapTrace);
		                		}
		        			}
		        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
		        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,"","0");
		        			tPerMMap.add(MMapBalance);
	        			}else{
		        			double tAccValue = getAccBalaByAccClass(tLCInsureAccClassSet);
		        			MMap MMapBalance = getBalance(glLCInsureAccSchema,tInterest000001Set.get(m),tAccValue,"","0");
		        			tPerMMap.add(MMapBalance);
	        			}
	        			tPerMMap.add(updateAccInfo(glLCInsureAccSchema));
        			}
        		}
	        	//保存管理费数据
	        	VData data = new VData();
	            data.add(tPerMMap);
	            PubSubmit tSubmit = new PubSubmit();
	            if (!tSubmit.submitData(data, ""))
	            {
	                System.out.println("PolicyGUTerminateDealBL数据提交失败");
	                return false;
	            }
	            System.out.println("第"+b+"个人管理费处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t1));
	            System.out.println("第"+b+"个人处理完成，共"+tCount+"人，用时："+(System.currentTimeMillis() - t0));
//	            System.out.println(PubFun.getCurrentDate()+PubFun.getCurrentTime()+"===第"+b+"个人结束时间");
        	}
        }
        return true;
    
    }
    
//    private boolean SinglePolicyTerminateDeal(LCContSet tLCContSet) 
//    {
//        for (int i = 1; i <= tLCContSet.size(); i++) 
//        {
//        	MMap tMap = new MMap();
//            LCContSchema tLCContSchema = new LCContSchema();
//            tLCContSchema.setSchema(tLCContSet.get(i));
////            if(!canTerminate(tLCContSchema))
////            {
////            	continue;
////            }
//            
//            PubSubmit tPubSubmit = new PubSubmit();
//            VData tData = new VData();
//                        
//            setLCCont(tLCContSchema,tMap);
//            
//            tData.clear();
//            tData.add(tMap);
//            tPubSubmit.submitData(tData,"");
//            
//            
//        }
//        return true;
//    }
    

  

    /**
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean canTerminate(LCPolSchema tLCPolSchema) 
    {
    	String tPolno = tLCPolSchema.getPolNo();
        //正在做保全不终止
        if (edorseState(tLCPolSchema.getGrpContNo())) 
        {
        	System.out.println(tPolno + "——在做保全项目，不能终止。");
            return false;
        }
        
//      正在做理赔不终止
        String claimState = mLLCaseCommon.getClaimState(tLCPolSchema.getPolNo());
        if (claimState.equals("1") || claimState.equals("2")) 
        {
            System.out.println(tPolno + "——在做理赔项目，不能终止。");
            return false;
        }
//        
        //已经终止或永久失效的保单不参加永久失效
//        String sql = "select count(1) from lccontstate where state = '1' and statetype = 'Terminate' and polno = '" + tPolno + "' ";
//        String tCount = mExeSQL.getOneValue(sql);
//        if (!"0".equals(tCount))
//        {
//        	System.out.println(tPolno + "——已经终止或永久失效的保单不参加终止处理。");
//            return false;
//        }
        
        String tBalanSql = "select max(baladate) from lcinsureacc where  polno = '" + tPolno + "' with ur";
        String tBalanDate = mExeSQL.getOneValue(tBalanSql);
        tBalanDate = PubFun.calDate(tBalanDate, 1, "M", null);
        if(tFDate.getDate(mGetStartDate).
        		after(tFDate.getDate(tBalanDate)))
        {
        	System.out.println(tPolno + "——满期日当月未进行月结算。");
            return false;
        }
        
        
        return true;
    }
    


    /*保全状态*/
    public boolean edorseState(String aContNo) 
    {
        String tableName = " LPGrpEdorMain ";
        String contType = " grpContNo ";
        

        StringBuffer sql = new StringBuffer();
        sql.append("select a.edorAcceptNo ")
                .append("from LPEdorApp a, ")
                .append(tableName).append(" b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("    and b.").append(contType).append(" = '")
                .append(aContNo).append("' ")
                .append("    and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM).append("' ")
                .append("   and not exists(select 1 from LGwork ")
                .append("           where WorkNo = a.EdorAcceptNo and StatusNo = '8') "); //撤销
        System.out.println(sql.toString());
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql.toString());
        if (tSSRS != null && tSSRS.getMaxRow() > 0) 
        {
            return true;
        }

        return false;
    }

    /*SQL中取管理结构工具*/
    private String getSQLManageCom() 
    {
        String mSQL = "";
        if (mManageCom != null && !mManageCom.trim().equals("")) 
        {
            if (mManageCom.length() >= 4) 
            {

                mSQL = " and ManageCom like '" + mManageCom.substring(0, 4) +
                       "%'";
            } 
            else 
            {
                mSQL = " and ManageCom like '86%'";
            }
        } 
        else 
        {
            mSQL = " ";
        }
        return mSQL;
    }


    /*险种状态设置*/
    private boolean setLCPol(LCPolSchema tLCPolSchema,MMap tMap) 
    {
        String updatepol = "update LCPol Set polstate = '"
                           + BQ.POLSTATE_UAENDDEAL + "',ModifyDate = '" + mCurrentDate
                           + "',ModifyTime = '" + mCurrentTime +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        tMap.put(updatepol, "UPDATE");
        return true;
    }



    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
//        LCGrpContSchema ttLCGrpContSchema = new LCGrpContSchema();
//        ttLCGrpContSchema.setGrpContNo("0000486001");
//        LCContSchema tLCContSchema = new LCContSchema();
//        tLCContSchema.setContNo("00001490205");
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

        PolicyGUTerminateDealBL mPolicyGUTerminateDealBL = new PolicyGUTerminateDealBL();

//        mPolicyAbateDealBL.submitData(mVData,"Available");
        mPolicyGUTerminateDealBL.submitData(mVData, "Terminate");
//       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
//         mPolicyAbateDealBL.edorseState("0000126302");
    }


}
