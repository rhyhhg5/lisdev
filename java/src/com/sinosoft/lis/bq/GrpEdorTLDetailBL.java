package com.sinosoft.lis.bq;

import java.util.Date;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


public class GrpEdorTLDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private EdorItemSpecialData mSpecialData = null;

    private String mEdorNo = null;

    private static String mEdorType = "TL";

    private String mGrpContNo = null;
    
    private String fmtransact = null;    

    /** 团险万能险种号 */
    private String mGrpPolNo = null;
    
    private double uligrpleft = Double.parseDouble(new ExeSQL().getOneValue("select code from ldcode where codetype='uligrpleft'"));

    private LPDiskImportSet mLPDiskImportSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    //本次部分领取保全的保单年度
    private int mPolYears=0;
    
    //团险万能险种的生效日期
    private String mCValiDate="";

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GrpEdorTLDetailBL(GlobalInput gi, String edorNo,
            String grpContNo, String fmtransact,EdorItemSpecialData specialData)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
        this.mSpecialData = specialData;
        this.fmtransact = fmtransact;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,
                mGrpContNo, BQ.IMPORTSTATE_SUCC);
        //if (mLPDiskImportSet.size() == 0)
        //{
        //    mErrors.addOneError("找不到磁盘导入数据！");
        //    return false;
        //}
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
		if (!checkData())
		{
			return false;
		}
    	//计算费用值
    	if(fmtransact!=null&&"CALFEE".equals(fmtransact.toUpperCase())){
    		//计算公共账户的最大可领取金额
    		double maxCangetFee  = 0;
    		LCPolDB tLCPolDB = new LCPolDB();
    		LCPolSet tLCPolSet = new LCPolSet();
    		LCPolSchema tLCPolSchema = new LCPolSchema();
    		String querypolSQL = " select * from lcpol where grpcontno='"+mGrpContNo+"' and poltypeflag='2' ";
    		tLCPolSet = tLCPolDB.executeQuery(querypolSQL);
    		
    		
    		//查询保全生效日期
    		String edorvalidate = new ExeSQL().getOneValue("select edorvalidate from lpgrpedoritem where edoracceptno='"+mEdorNo+"' ");
    		if(tLCPolSet!=null&&tLCPolSet.size()>=1){
    			tLCPolSchema = tLCPolSet.get(1);
//              解约管理费率
            	System.out.println("解约管理费率开始计算");
            	int tpolyear=PubFun.calPolYear(tLCPolSchema.getCValiDate(), edorvalidate);
            	String tfee = "";
            	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLCPolSchema.getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
            	tfee = new ExeSQL().getOneValue(polRateSQL);
                if ((tfee == null) || (tfee.equals(""))){
                	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLCPolSchema.getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
                	if ((tfee == null) || (tfee.equals(""))){
                		mErrors.addOneError("查询管理费为空！");
                		return false;
                	}
                }
            	double feeRate = Double.parseDouble(tfee); 
            	
            	//得到实际领取金额
            String  grpLQmoney = this.mSpecialData.getEdorValue("GrpLQMoney");
			if (grpLQmoney == null || "".equals(grpLQmoney)) {
				grpLQmoney = "0";
			}
            
			//特殊数据表，如果万能部分领取不领取公共账户，则grpLQmoney=0
         	//grpLQmoney!=0对公共账户 领取金额做校验
	            if(!"0".equals(grpLQmoney)){
	            	//查询费用
	                String calFEESQL="select  b.insuaccbala as 公共账户余额," +
	                		" (case when b.insuaccbala*0.85 < "+uligrpleft+" " +
	                		" then (case when b.insuaccbala*0.15>(b.insuaccbala-"+uligrpleft+") then (b.insuaccbala-"+uligrpleft+") else  b.insuaccbala*0.15 end) " +
	                		" else (b.insuaccbala*0.15 +(b.insuaccbala*0.85-"+uligrpleft+")/(1+"+feeRate+")) end) as 公共账户最大可领取金额, " +
	                		" '"+grpLQmoney+"' as 公共账户部分领取金额, " +
	                		" (case when '"+grpLQmoney+"' ='0' then '0' " +
	                		" when decimal('"+grpLQmoney+"')<= b.insuaccbala*0.15 then '0' " +
	                		" else  char((double('"+grpLQmoney+"') - b.insuaccbala*0.15 )* "+feeRate+") end ) as 退保费用  " +
	                		" from lcpol a ,lcinsureacc b  " +
	                		" where a.grpcontno='"+mGrpContNo+"'  and a.poltypeflag='2' " +
	                		" and a.grpcontno=b.grpcontno  and a.polno=b.polno with ur  ";
	                SSRS tSSRS =new ExeSQL().execSQL(calFEESQL); 
	                if (tSSRS.getMaxRow() > 0){
	//                	String insuaccbala = tSSRS.GetText(1, 1);
	                	 maxCangetFee = CommonBL.carry(tSSRS.GetText(1, 2));
	                	 double grpZTFEE = Math.floor(Double.parseDouble(tSSRS.GetText(1, 4))*100)/100;
	                	 mSpecialData.setGrpPolNo(tLCPolSchema.getPolNo());
	                	 mSpecialData.add("MAXCANGETFEE", maxCangetFee+"");
	                	 mSpecialData.add("GRPZTFEE", grpZTFEE+"");         
	                	 if(Double.parseDouble(grpLQmoney)>maxCangetFee){
	                     	mErrors.addOneError("公共账户的部分领取金额"+grpLQmoney+"不能大于最大可领取金额"+maxCangetFee);
	                    	return false;
	                	 }
	                }else{
	                	mErrors.addOneError("查询团体公共账户失败！");
	                	return false;
	                }
	            }
//            
    		}else{
                mErrors.addOneError("查询团体公共账户失败！");
                return false;
    		}
    		
    		
    	}else if(fmtransact!=null&&"SAVEVALUE".equals(fmtransact.toUpperCase())){
    		setEdorState(BQ.EDORSTATE_INPUT);
    	}
    	setSpecialData();
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkRiskCoede())
        {
            return false;
        }
        if (!checkGrpMoney())
        {
            return false;
        }
        if (!checkGCustomers())
        {
            return false;
        }
//        if (!checkPCustomers())
//        {
//        	return false;
//        }
        return true;
    }
    
    private LPDiskImportSchema calfee(LCPolSchema tLCPolSchema,String grpLQmoney,double feeRate,String tInsuNo,String peopleFlag ){
    	
    	LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
    	tLPDiskImportSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
 
    	double MaxAccfee  = 0;
    	if(peopleFlag!=null&&"1".equals(peopleFlag)){
    		MaxAccfee = 0;
    	}else{
    		MaxAccfee = uligrpleft;
    	}
        //查询费用
        String calFEESQL="select  b.insuaccbala as 账户余额," +
        		" (case when b.insuaccbala*0.85 < "+MaxAccfee+
        		" then (case when b.insuaccbala*0.15>(b.insuaccbala-"+MaxAccfee+") then (b.insuaccbala-"+MaxAccfee+") else  b.insuaccbala*0.15 end) " +
        		" else (b.insuaccbala*0.15 +(b.insuaccbala*0.85-"+MaxAccfee+")/(1+"+feeRate+")) end) as 账户最大可领取金额, " +
        		" '"+grpLQmoney+"' as 账户部分领取金额, " +
        		" (case when '"+grpLQmoney+"' ='0' then '0' " +
        		" when decimal('"+grpLQmoney+"')<= b.insuaccbala*0.15 then '0' " +
        		" else  char((double('"+grpLQmoney+"') - b.insuaccbala*0.15 )* "+feeRate+") end ) as 退保费用  " +
        		" from lcpol a ,lcinsureacc b  " +
        		" where a.grpcontno='"+mGrpContNo+"'  and b.polno='"+tLCPolSchema.getPolNo()+"' and b.insuaccno='"+tInsuNo+"'" +
        		" and a.grpcontno=b.grpcontno  and a.polno=b.polno with ur  ";
        
        SSRS tSSRS =new ExeSQL().execSQL(calFEESQL); 
        if (tSSRS.getMaxRow() > 0){
       	 double maxCanPgetFee = CommonBL.carry(tSSRS.GetText(1, 2));
       	 
//       	 if(Double.parseDouble(grpLQmoney)>maxCanPgetFee){
//          	mErrors.addOneError("个人账户有领取金额大于最大领取金额的记录");
//        	return null;
//       	 }
    	 double grpPZTFEE = Math.floor(Double.parseDouble(tSSRS.GetText(1, 4))*100)/100;
    	 tLPDiskImportSchema.setMoney2(maxCanPgetFee+"");
    	 tLPDiskImportSchema.setInsuAccBala(CommonBL.carry(tSSRS.GetText(1, 1))+"");
    	 tLPDiskImportSchema.setInsuAccInterest(grpPZTFEE+"");
        } 
        
    	return tLPDiskImportSchema;
    }
    
    

    /**
     * 检查特需险种
     * @return boolean
     */
    private boolean checkRiskCoede()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        if (tLCGrpPolSet.size() == 0)
        {
            mErrors.addOneError("未找到集体险种信息，请检查LCGrpPol表的数据！");
            return false;
        }
        int n = 0;
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String riskCode = tLCGrpPolSchema.getRiskCode();
            if (CommonBL.isULIRisk(riskCode))
            {
                mGrpPolNo = tLCGrpPolSchema.getGrpPolNo();
                mCValiDate=tLCGrpPolSchema.getCValiDate();
                n++;
            }
        }
        if (!(n == 1))
        {
            mErrors.addOneError("此保单含有多个万能险种，不能处理！");
            return false;
        }
        
        LPGrpEdorItemDB tLPGrpEdorItemDB=new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setGrpContNo(mGrpContNo);
        tLPGrpEdorItemDB.setEdorType("TL");
        if (!tLPGrpEdorItemDB.getInfo())
        {
            mErrors.addOneError("未找到本次团单万能领取保全项目信息！");
            return false;
        }
        LPGrpEdorItemSchema tLPGrpEdorItemSchema= new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema=tLPGrpEdorItemDB.getSchema();
        String edorValidateDate=tLPGrpEdorItemSchema.getEdorValiDate();
        mPolYears=PubFun.calPolYear(mCValiDate, edorValidateDate);

        return true;
    }

    private boolean checkGrpMoney()
    {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++)
        {
            String grpPolNo = grpPolNos[i];

            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(grpPolNo);
            tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
            LCPolSet tLCPolSet = tLCPolDB.query();
            if (tLCPolSet.size() == 0)
            {
                throw new RuntimeException();
            }
            String polNo = tLCPolSet.get(1).getPolNo(); 
            LCInsureAccSchema tLCInsureAccSchema =
                    CommonBL.getLCInsureAcc(polNo,
                    CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSet.get(1).getRiskCode()));
            if (tLCInsureAccSchema == null)
            {
                mErrors.addOneError("找不到公共账户信息！");
                return false;
            }
            mSpecialData.setGrpPolNo(grpPolNo);
            double grpMoney = Double.parseDouble(mSpecialData.getEdorValue("GrpLQMoney")); //应领金额
            if (grpMoney > tLCInsureAccSchema.getInsuAccBala())
            {
                mErrors.addOneError("公共账户领取金额:"+grpMoney+",不能多于账户余额"+tLCInsureAccSchema.getInsuAccBala()+"！");
                return false;
            }
            if (grpMoney <0)
            {
                mErrors.addOneError("公共账户领取金额"+grpMoney+"不能为负数！");
                return false;
            }
            
            //查询上一次结算信息
            LCInsureAccBalanceDB tLCInsureAccBalanceDB = new LCInsureAccBalanceDB();
            tLCInsureAccBalanceDB.setPolNo(polNo);
            tLCInsureAccBalanceDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
            tLCInsureAccBalanceDB.setDueBalaDate(PubFun.getCurrentDate().split("-")[0]+"-"+PubFun.getCurrentDate().split("-")[1]+"-01");
            LCInsureAccBalanceSet tLCInsureAccBalanceSet
                    = tLCInsureAccBalanceDB.query();
            if (tLCInsureAccBalanceSet.size() == 0)
            {
                mErrors.addOneError("该保单上月没有月结数据，不能做部分领取");
                return false;
            }
//            if(grpMoney>0)
//            {
//            	String tcheck=new ExeSQL().getOneValue("select 1 from lcinsureacctrace where polno='"+polNo+"' and paydate>'"+mCValiDate+"' and insuaccno='"+CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSet.get(1).getRiskCode())+"' and state!='temp' ");
//        		if(tcheck.equals("1"))
//        		{
//        			mErrors.addOneError("保全的生效日期必须在公共账户最后一次变动之后！");
//                    return false;
//        		}
//            	String sql="select paydate from lcinsureacctrace a where  othertype='3' and exists (select 1 from lpgrpedoritem where edortype='TL' and edorno=a.otherno) and  polno='"+polNo+"'   and insuaccno='"+CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSet.get(1).getRiskCode())+"' ";
//                SSRS getDataSqlSSRS =new ExeSQL().execSQL(sql); 
//                if(getDataSqlSSRS!= null && getDataSqlSSRS.getMaxRow() > 0 )
//        		{
//        			for(int j=1;j< getDataSqlSSRS.getMaxRow()+1;j++)
//        			{
//        				if(mPolYears==PubFun.calPolYear(mCValiDate, getDataSqlSSRS.GetText(j, 1)))
//        				{
//        					 mErrors.addOneError("该团单公共账户在本保单年度["+getDataSqlSSRS.GetText(j, 1)+"]已经进行过部分领取，不能再本年度再次申请！");
//        			         return false;
//        				}
//        				
//        			}
//        		}
//            }
        }
        return true;
    }
    
    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkGCustomers()
    {
        boolean flag = true;
        
        //查询保全生效日期
        String edorvalidate = new ExeSQL().getOneValue("select edorvalidate from lpgrpedoritem where edoracceptno='"+mEdorNo+"' ");
        
        
        
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LPDiskImportSchema ttLPDiskImportSchema = new LPDiskImportSchema();
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(
                    tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                errorReason += "保单中不存在被保人" + tLCInsuredSchema.getName() +
                        "，导入无效！";
                flag = false;
            }
            else
            {
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(mGrpPolNo, customerCheck.getCustomerNo(),
                        BQ.ACCTYPE_INSURED);
                double leftMoney = tLCInsureAccSchema.getInsuAccBala();
                double importMoney = tLPDiskImportSchema.getMoney();
            	//为导入的客户设置最大领取金额及退保费用
                String peopleFlag = "1";
//            	获取个人账户个人部分保险账户号码
                String tInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLCInsureAccSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
               
                //查询被保险人险种信息
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLCInsureAccSchema.getPolNo());
                tLCPolDB.getInfo();
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema = tLCPolDB.getSchema();
                

//              解约管理费率
            	System.out.println("解约管理费率开始计算");
            	int tpolyear=PubFun.calPolYear(tLCPolSchema.getCValiDate(), edorvalidate);
            	String tfee = "";
            	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLCPolSchema.getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
            	tfee = new ExeSQL().getOneValue(polRateSQL);
                if ((tfee == null) || (tfee.equals(""))){
                	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLCPolSchema.getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
                	if ((tfee == null) || (tfee.equals(""))){
                		mErrors.addOneError("查询管理费为空！");
                		return false;
                	}
                }
                	double feeRate = Double.parseDouble(tfee); 
                	
                	//得到实际领取金额
                String  grpLQmoney = importMoney+"";
                if(grpLQmoney==null||"".equals(grpLQmoney)){
                	grpLQmoney = "0";
                }
                
                //计算每一个被保险人的个人账户的最大可领取金额以及退保费用
                ttLPDiskImportSchema = calfee(tLCPolSchema,grpLQmoney,feeRate,tInsuNo,peopleFlag);
                
                if(ttLPDiskImportSchema==null){
                	return false;
                }
                if (importMoney <= 0)
                {
                    errorReason += "被保人领取金额不正确！";
                    flag = false;
                }
                if(Double.parseDouble(grpLQmoney)>Double.parseDouble(ttLPDiskImportSchema.getMoney2())){
                    errorReason += "被保人账户余额不足，当前最大可领取余额为" + ttLPDiskImportSchema.getMoney2() + "元,部分领取金额为："+grpLQmoney+"！";
                    flag = false;
                }
                else if (importMoney > leftMoney)
                {
                    errorReason += "被保人余额不足，当前余额为" + leftMoney + "元！";
                    flag = false;
                }
                if (!errorReason.equals(""))
                {
                	StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                	sql.append("Set State = '")
                	.append(BQ.IMPORTSTATE_FAIL).append("', ")
                	.append(" ErrorReason = '" + errorReason + "', ")
                	.append(" Operator = '")
                	.append(mGlobalInput.Operator).append("', ")
                	.append(" ModifyDate = '")
                	.append(mCurrentDate ).append("', ")
                	.append(" ModifyTime = '")
                	.append(mCurrentTime).append("' ")
                	.append("where EdorNo = '")
                	.append(mEdorNo).append("' ")
                	.append("and EdorType = '")
                	.append(mEdorType).append("' ")
                	.append("and GrpContNo = '")
                	.append(mGrpContNo).append("' ")
                	.append("and SerialNo = '")
                	.append(tLPDiskImportSchema.getSerialNo())
                	.append("' ");
                	mMap.put(sql.toString(), "UPDATE");
                	flag = false;
                }
                else
                {            	
                	StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                	sql.append("Set State = '")
                	.append(BQ.IMPORTSTATE_SUCC).append("', ")
                	.append("InsuredNo = '")
                	.append(StrTool.cTrim(customerCheck.getCustomerNo()))
                	.append("', ")
                	.append("money2='")
                	.append(ttLPDiskImportSchema.getMoney2())
                	.append("', ")
                	.append("InsuAccBala='")
                	.append(ttLPDiskImportSchema.getInsuAccBala())
                	.append("',")
                	.append("InsuAccInterest='")
                	.append(ttLPDiskImportSchema.getInsuAccInterest())
                	.append("',")
                	.append("Operator = '")
                	.append(mGlobalInput.Operator).append("', ")
                	.append("ModifyDate = '")
                	.append(mCurrentDate).append("', ")
                	.append("ModifyTime = '")
                	.append(mCurrentTime).append("' ")
                	.append("where EdorNo = '")
                	.append(mEdorNo).append("' ")
                	.append("and EdorType = '")
                	.append(mEdorType).append("' ")
                	.append("and GrpContNo = '")
                	.append(mGrpContNo).append("' ")
                	.append("and SerialNo = '")
                	.append(tLPDiskImportSchema.getSerialNo())
                	.append("' ");
                	mMap.put(sql.toString(), "UPDATE");
                }
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
        }
        return true;
    }    

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkPCustomers()
    {
        boolean flag = true;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setGrpContNo(mGrpContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            OldCustomerCheck customerCheck = new OldCustomerCheck(
                    tLCInsuredSchema);
            if (customerCheck.checkInsured() != OldCustomerCheck.OLD)
            {
                errorReason += "保单中不存在被保人" + tLCInsuredSchema.getName() +
                        "，导入无效！";
                flag = false;
            }
            else
            {
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(mGrpPolNo,
                		customerCheck.getCustomerNo());
                if (tLCPolSchema == null)
                {
                    mErrors.addOneError("找不到客户" + customerCheck.getCustomerNo() + "的险种信息！");
                    return false;
                }
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setGrpPolNo(mGrpPolNo);
                tLCInsureAccDB.setInsuredNo(customerCheck.getCustomerNo());
                String insuaccno=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLCPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
                tLCInsureAccDB.setInsuAccNo(insuaccno);//个人账户个人缴费部分的账户代码
                LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
                if (tLCInsureAccSet.size() == 0)
                {
                	mErrors.addOneError("找不到客户" + customerCheck.getCustomerNo() + "的个人账户总信息！");
                    return false;
                }
                LCInsureAccSchema tLCInsureAccSchema =tLCInsureAccSet.get(1);
                if (tLCInsureAccSchema == null)
                {
                    mErrors.addOneError("找不到客户" + customerCheck.getCustomerNo() + "的个人账户信息！");
                    return false;
                }
                double leftMoney = tLCInsureAccSchema.getInsuAccBala();
                double importMoney = tLPDiskImportSchema.getMoney();
                
                
                
                
                if (importMoney <= 0)
                {
                    errorReason += "被保人领取金额不正确！";
                    flag = false;
                }
                if (importMoney > 0)
                {
                	String tcheck=new ExeSQL().getOneValue("select 1 from lcinsureacctrace where polno='"+tLCPolSchema.getPolNo()+"' and paydate>'"+mCValiDate+"' and insuaccno='"+CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSchema.getRiskCode())+"' and state!='temp' ");
            		if(tcheck.equals("1"))
            		{
            			errorReason += "保全的生效日期必须在保单号为:"+tLCPolSchema.getContNo()+"的个人账户最后一次变动之后！";
                        flag = false;
            		}
            		String sql="select paydate from lcinsureacctrace a where  othertype='3' and exists (select 1 from lpgrpedoritem where edortype='TL' and edorno=a.otherno) and  polno='"+tLCPolSchema.getPolNo()+"'   and insuaccno='"+CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSchema.getRiskCode())+"' ";
                    SSRS getDataSqlSSRS =new ExeSQL().execSQL(sql); 
                    if(getDataSqlSSRS!= null && getDataSqlSSRS.getMaxRow() > 0 )
            		{
            			for(int j=1;j< getDataSqlSSRS.getMaxRow()+1;j++)
            			{
            				if(mPolYears==PubFun.calPolYear(mCValiDate, getDataSqlSSRS.GetText(j, 1)))
            				{
            					 mErrors.addOneError("保单"+tLCPolSchema.getContNo()+"个人账户在本保单年度["+getDataSqlSSRS.GetText(j, 1)+"]已经进行过部分领取，不能再本年度再次申请！");
            			         return false;
            				}
            				
            			}
            		}                	
                }
                if (importMoney > leftMoney)
                {
                    errorReason += "被保人余额不足，当前余额为" + leftMoney + "元！";
                    flag = false;
                }
            }
            if (!errorReason.equals(""))
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_FAIL).append("', ")
                        .append(" ErrorReason = '" + errorReason + "', ")
                        .append(" Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append(" ModifyDate = '")
                        .append(mCurrentDate ).append("', ")
                        .append(" ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
                flag = false;
            }
            else
            {
                StringBuffer sql = new StringBuffer("Update LPDiskImport ");
                sql.append("Set State = '")
                        .append(BQ.IMPORTSTATE_SUCC).append("', ")
                        .append("InsuredNo = '")
                        .append(StrTool.cTrim(customerCheck.getCustomerNo()))
                        .append("', ")
                        .append("Operator = '")
                        .append(mGlobalInput.Operator).append("', ")
                        .append("ModifyDate = '")
                        .append(mCurrentDate).append("', ")
                        .append("ModifyTime = '")
                        .append(mCurrentTime).append("' ")
                        .append("where EdorNo = '")
                        .append(mEdorNo).append("' ")
                        .append("and EdorType = '")
                        .append(mEdorType).append("' ")
                        .append("and GrpContNo = '")
                        .append(mGrpContNo).append("' ")
                        .append("and SerialNo = '")
                        .append(tLPDiskImportSchema.getSerialNo())
                        .append("' ");
                mMap.put(sql.toString(), "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
        }
        return true;
    }

    /**
     * 保存团单追加金额
     */
    private void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        System.out.println(sql);
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
