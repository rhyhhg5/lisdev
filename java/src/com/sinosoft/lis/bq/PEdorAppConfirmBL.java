package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 保全理算,以前叫保全受理申请确认</p>
 * <p>Description: 对保全变更进行计算,并把计算结果放入P表,保全确认后生效</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorAppConfirmBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorAcceptNo = null;

    private LPEdorItemSet mLPEdorItemSet = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorAcceptNo String
     */
    public PEdorAppConfirmBL(GlobalInput gi, String edorAcceptNo)
    {
        this.mGlobalInput = gi;
        this.mEdorAcceptNo = edorAcceptNo;
        this.mLPEdorItemSet = getEdorItems();
    }

    /**
     * 调用业务逻辑并提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 检查数据的正确性
     * @return boolean
     */
    private boolean checkData()
    {
    	if ((this.mEdorAcceptNo == null) || (this.mEdorAcceptNo =="")|| (this.mEdorAcceptNo =="null"))
        {
            mErrors.addOneError( "受理号为空，请录入受理号！");
            return false;
        }
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            String edorState = mLPEdorItemSet.get(i).getEdorState();
            String edorType = mLPEdorItemSet.get(i).getEdorType();
            if ((edorState == null) || (!edorState.equals(BQ.EDORSTATE_INPUT)))
            {
                mErrors.addOneError(edorType + "项目未录入保全明细！");
                return false;
            }
            
            
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!confirmAllItems())
        {
            return false;
        }
        updateMainMoney();
        updateAppMoney();
        setEdorState(BQ.EDORSTATE_CAL);
        setStatusNo(Task.WORKSTATUS_DOING);
        return true;
    }

    /**
     * 得到Main表中的数据，Main表中一条记录对应一个ContNo
     * @return LPEdorMainSet
     */
    private LPEdorMainSet getEdorMains()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorAcceptNo);
        return tLPEdorMainDB.query();
    }

    /**
     * 得到保全项目,为了能多个项目同时做而不冲突,这里必须加上order by
     * 而且是后保存的项目要后理算
     * @return LPEdorItemSet
     */
    private LPEdorItemSet getEdorItems()
    {
        String sql = "select * from LPEdorItem " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' " +
                "order by ModifyDate, ModifyTime ";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        return tLPEdorItemDB.executeQuery(sql);
    }

    /**
     * 对该保全受理下的所有项目依次理算
     * @return boolean
     */
    private boolean confirmAllItems()
    {
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
        	System.out.print("***************************"+mLPEdorItemSet.size()+"88888888888888");
        	
            if (!confirmOneItem(mLPEdorItemSet.get(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 理算一个项目
     * @param edorItem LPEdorItemSchema
     * @return boolean
     */
    private boolean confirmOneItem(LPEdorItemSchema edorItem)
    {
        String edorType = edorItem.getEdorType();
        try
        {
            //按项目编码调用不同的理算类
            String className = "com.sinosoft.lis.bq.PEdor" +
                    edorType + "AppConfirmBL";
            Class confirmClass = Class.forName(className);
            EdorAppConfirm tPEdorAppConfirm = (EdorAppConfirm)
                    confirmClass.newInstance();
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(edorItem);
            if (!tPEdorAppConfirm.submitData(data, "APPCONFIRM||" + edorType))
            {
                mErrors.copyAllErrors(tPEdorAppConfirm.mErrors);
                return false;
            }
            VData ret = tPEdorAppConfirm.getResult();
            MMap map = (MMap) ret.getObjectByObjectName("MMap", 0);
            if (map == null)
            {
                mErrors.addOneError(edorType + "项目理算失败!");
                return false;
            }
            mMap.add(map);
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println(edorItem.getEdorType() + "项目没有保全理算类!");
        }
        catch (ClassCastException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("找不到" + edorType + "项目理算类中的函数!");
            return false;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError(edorType + "项目理算失败!");
            return false;
        }
        return true;
    }

    /**
     * 统计Item表中的保费变化,同步更新Main,表其中Main表按ContNo和Item表对应
     */
    private void updateMainMoney()
    {
        LPEdorMainSet tLPEdorMainSet = getEdorMains();
        for (int i = 1; i <= tLPEdorMainSet.size(); i++)
        {
            String contNo = tLPEdorMainSet.get(i).getContNo();
            StringBuffer sql = new StringBuffer("update LPEdorMain ");
            sql.append("set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = ")
                    .append("(select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), ")
                    .append("sum(GetInterest) from LPEdorItem ")
                    .append("where EdorAcceptNo = '").append(mEdorAcceptNo).append("' ")
                    .append("and ContNo = '").append(contNo).append("') ")
                    .append("where EdorAcceptNo = '").append(mEdorAcceptNo).append("' ")
                    .append("and ContNo = '").append(contNo).append("'");
            mMap.put(sql.toString(), "UPDATE");
        }
    }

    /**
     * 统计Main表中的保费变化,同步更新App表
     */
    private void updateAppMoney()
    {
        String sql = "update LPEdorApp " +
                "set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = " +
                "    (select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), " +
                "    sum(GetInterest) from LPEdorMain " +
                "    where EdorAcceptNo = '" + mEdorAcceptNo + "') " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置保全状态,"2"为理算完成状态
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "  update LPEdorApp "
              + "set EdorState = '" + edorState + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorMain "
              + "set EdorState = '" + edorState + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorItem "
              + "set EdorState = '" + edorState + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
    }
    
    private void setStatusNo(String workstatus) 
    {
    	 String sql = null;
         sql = "update lgwork set statusno = '" + workstatus + "', Operator = '" + mGlobalInput.Operator 
             + "', ModifyDate = '" + PubFun.getCurrentDate() + "', ModifyTime = '" + PubFun.getCurrentTime() 
             + "' where workno = '" + mEdorAcceptNo + "' ";
         mMap.put(sql, "UPDATE");
	}

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

}
