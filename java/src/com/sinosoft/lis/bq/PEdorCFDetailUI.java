package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;


public class PEdorCFDetailUI
{
    private PEdorCFDetailBL mPEdorCFDetailBL = null;

     public PEdorCFDetailUI(String edorNo,String appntno,String contno)
     {
         mPEdorCFDetailBL = new PEdorCFDetailBL(edorNo,appntno,contno);
     }

     /**
      * 调用业务逻辑
      * @param data VData
      * @return boolean
      */
     public boolean submitData(VData data, String operator)
     {
         if (!mPEdorCFDetailBL.submitData(data, operator))
         {
             return false;
         }
         return true;
     }

     /**
      * 返回错误信息
      * @return String
      */
     public String getError()
     {
         return mPEdorCFDetailBL.mErrors.getFirstError();
     }
}
