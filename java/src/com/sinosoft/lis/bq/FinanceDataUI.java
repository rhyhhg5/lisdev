package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全处理财务数据</p>
 * <p>Description:把保全补退费数据放入财务接口</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class FinanceDataUI
{
    private FinanceDataBL mFinanceDataBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public FinanceDataUI(GlobalInput gi, String edorNo, String noticeType,
                         String balanceMethodValue)
    {
        mFinanceDataBL = new FinanceDataBL(gi, edorNo, noticeType, balanceMethodValue);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mFinanceDataBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mFinanceDataBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "endor0";
        String edorNo = "20051104000023";
        FinanceDataUI tFinanceDataUI = new FinanceDataUI(gi, edorNo, BQ.NOTICETYPE_P, "1");
        if (!tFinanceDataUI.submitData())
        {
            System.out.println(tFinanceDataUI.getError());
        }
    }

}
