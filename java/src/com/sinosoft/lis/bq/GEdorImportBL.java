package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 保全特需医疗团体帐户余额分配磁盘导入类</p>
 * <p>Description: 导入被保人分配金额 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorImportBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private String fileName = null;

    private String configFileName = null;

    private String sheetName = "";

    private static String mCurrentDate = PubFun.getCurrentDate();

    private static String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public GEdorImportBL(String fileName, String configFileName, String sheetName)
    {
        this.fileName = fileName;
        this.configFileName = configFileName;
        this.sheetName=sheetName;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        checkInsured();
        
        return true;
    }

    /**
     * 得到输入数据
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            LPDiskImportSchema tLPDiskImportSchema = (LPDiskImportSchema)
                    data.getObjectByObjectName("LPDiskImportSchema", 0);
            mEdorNo = tLPDiskImportSchema.getEdorNo();
            mEdorType = tLPDiskImportSchema.getEdorType();
            mGrpContNo = tLPDiskImportSchema.getGrpContNo();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        deleteImportData();
        LPDiskImportSet tLPDiskImportSet = getLPDiskImportSet();
        setLPDiskImportSet(tLPDiskImportSet);
        mMap.put(tLPDiskImportSet, "INSERT");
        return true;
    }

    /**
     * 导入前先清空上次导入数据
     */
    private void deleteImportData()
    {
        String sql = "delete from LPDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 得到LPDiskImportSet，调用磁盘导入程序
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getLPDiskImportSet()
    {
        BqDiskImporter importer = new BqDiskImporter(fileName, configFileName,
                sheetName);
        if (!importer.doImport())
        {
            mErrors.copyAllErrors(importer.mErrrors);
            return null;
        }
        return (LPDiskImportSet) importer.getSchemaSet();
    }

    /**
     * 设置LPDiskImportSet
     * @param aLPDiskImportSet LPDiskImportSet
     */
    private void setLPDiskImportSet(LPDiskImportSet aLPDiskImportSet)
    {
        for (int i = 1; i <= aLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = aLPDiskImportSet.get(i);
            tLPDiskImportSchema.setEdorNo(mEdorNo);
            tLPDiskImportSchema.setEdorType(mEdorType);
            tLPDiskImportSchema.setGrpContNo(mGrpContNo);
            tLPDiskImportSchema.setState(BQ.IMPORTSTATE_SUCC);  //有效
            tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
            tLPDiskImportSchema.setMakeDate(mCurrentDate);
            tLPDiskImportSchema.setMakeTime(mCurrentTime);
            tLPDiskImportSchema.setModifyDate(mCurrentDate);
            tLPDiskImportSchema.setModifyTime(mCurrentTime);
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     *客户资料变更 需要导入联系电话，且在明细时立马要用，
     *无奈,,,只能在这先加上客户的校验了
     * @return
     */
    private boolean checkInsured(){
    	if(BQ.EDORTYPE_CM.equals(mEdorType) && null!=mGrpContNo && !"".equals(mGrpContNo)){
    		ImportCheckInsuredBL mImportCheckInsuredBL =new ImportCheckInsuredBL();
    		TransferData transferData=new TransferData();
    		transferData.setNameAndValue("EdorNo", mEdorNo);
    		transferData.setNameAndValue("GrpContNo", mGrpContNo);
    		transferData.setNameAndValue("EdorType", mEdorType);
    		VData data = new VData();
    		data.add(mGlobalInput);
    		data.add(transferData);
    		mImportCheckInsuredBL.submitData(data);
    	}
    	return true;
    }
}
