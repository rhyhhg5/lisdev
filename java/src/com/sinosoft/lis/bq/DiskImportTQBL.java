package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.HashMap;
import com.sinosoft.lis.pubfun.diskimport.BqDiskImporter;
import java.io.File;

/**
 * <p>Title: </p>
 *
 * <p>Description:以磁盘导入方式进行减人操作</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class DiskImportTQBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;
    private int importPersons = 0;   //减人明细录入处理后成功导入的人数
    private int mImportPersonsForTQ = 0;  //传入到减人类里面的人数

    private String edorNo = null;
    private String grpContNo = null;
    private String edorType = null;

    public int mImportPersonsForTQCF=0;    //重复人数  ******
    public LPDiskImportSet mLPDiskImportSetForTQCF=null;   //重复人记录 ******

    private LPEdorItemSet mLPEdorItemSet = null;
    private LPInsuredSet mLPInsuredSet = null;
    private LPDiskImportSet mLPDiskImportSet = null;

    private OldCustomerCheck check = new OldCustomerCheck();  //校验客户信息类

    public DiskImportTQBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator
                          + (String) mTransferData.getValueByName("fileName");

        String configFileName = path + File.separator + BQ.FILENAME_IMPORT;
        //从磁盘导入数据
        BqDiskImporter importer = new BqDiskImporter(fileName, configFileName,
                BQ.EDORTYPE_TQ);
        if (!importer.doImport())
        {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        mLPDiskImportSet = (LPDiskImportSet)importer
                                           .getSchemaSet();
        //过滤重复记录
        LPDiskImportSet t = getIn(mLPDiskImportSet);
        mLPDiskImportSet.clear();
        mLPDiskImportSet.add(t);
        //若被保人在保单中没有记录，则剔出
        //2008-11-28增加限制录入补退费金额为负数的校验
        if (!checkData())
        {
            return false;
        }

        //进入个人保全
        if(mLPDiskImportSet.size() != 0
           && !createEdorDetailTQ())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);

        if (mTransferData == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }

        edorNo = (String) mTransferData.getValueByName("edorNo");
        grpContNo = (String) mTransferData.getValueByName("grpContNo");
        edorType = (String) mTransferData.getValueByName("edorType");
        if(edorNo == null || grpContNo == null || edorType == null)
        {
            System.out.println("传入的的数据不完整: edorNo == null "
                               + "|| grpContNo == null || edorType == null");
            mErrors.addOneError("传入的的数据不完整。");
            return false;
        }

        return true;
    }

    /**
     * 校验导入数据的合法性，若被保人在保单中没有记录，则剔除
     * @param tLCInsuredListSet LCInsuredListSet
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData()
    {
        if(mLPDiskImportSet == null)
        {
            mErrors.addOneError("导入被保人有误。");
            return false;
        }
        try
        {
            //checkCustomer();
            checkInsured();
        }
        catch(Exception e)
        {
            mErrors.addOneError("校验被保人出错");
            e.printStackTrace();
            return false;
        }
        if (!checkMoney())
        {
        	mErrors.addOneError("减人模板中<补/退费>录入不能为负数,请检查模板");
            return false;
        }
        return true;
    }
    
    /**
     * 检查减人补退费是否有录入为负数的情况,如有返回FALSE
     * @param mLPDiskImportSet
     * @return boolean，成功校验 true
     */
    private boolean checkMoney()
            
    {

        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            System.out.println("..................i="+i+"时money为"+mLPDiskImportSet.get(i).getMoney2());
        	String money=mLPDiskImportSet.get(i).getMoney2();
        	if(money.startsWith("-")){
        		mErrors.addOneError("减人模板序号为["+i+"]的人员录入补退费为负数!");
        		return false;
        	}

        }
        return true;
    }
    

    /**
     * 检查每个客户是否是本公司的客户，若不是则剔除
     * @param tLCInsuredListSet LCInsuredListSet
     * @return boolean，成功校验 trun
     */
    private void checkCustomer()
            throws Exception
    {
        //是否出错标志
        boolean flag = false;
        //不是本公司客户时的错误信息
        StringBuffer errMsg = new StringBuffer();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            tLDPersonSchema.setName(mLPDiskImportSet.get(i).getInsuredName());
            tLDPersonSchema.setSex(mLPDiskImportSet.get(i).getSex());
            tLDPersonSchema.setBirthday(mLPDiskImportSet.get(i).getBirthday());
            tLDPersonSchema.setIDType(mLPDiskImportSet.get(i).getIDType());
            tLDPersonSchema.setIDNo(mLPDiskImportSet.get(i).getIDNo());

            int oldFlag = check.check();
            if(check.mErrors.needDealError())
            {
                mErrors.copyAllErrors(check.mErrors);
                System.out.println(check.mErrors.getErrContent());
                throw new Exception();
            }
            if (oldFlag != OldCustomerCheck.OLD)
            {
                //不是本公司客户，剔除
                errMsg.append("序号为" + mLPDiskImportSet.get(i).getSerialNo())
                        .append(" 被保人姓名为")
                        .append(mLPDiskImportSet.get(i).getInsuredName() + "，");
                mLPDiskImportSet.removeRange(i, i);
                i--;
                flag = true;
            }
        }
        if(flag)
        {
            mErrors.addOneError(errMsg.toString() + "不是本公司客户；");
        }
    }

    /**
     * 检查每个客户是否是本单的被保人，若不是则剔除
     * @param tLCInsuredListSet LCInsuredListSet
     * @return boolean
     */
    private void checkInsured()
    {
        //是否出错标志
        boolean flag = false;

        mLPEdorItemSet = new LPEdorItemSet();
        mLPInsuredSet = new LPInsuredSet();

        //客户不是本保单的被保人时的错误信息
        StringBuffer errMsg = new StringBuffer();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LCInsuredDB db = new LCInsuredDB();
            db.setGrpContNo((String) mTransferData.getValueByName("grpContNo"));
            db.setName(mLPDiskImportSet.get(i).getInsuredName());
            db.setSex(mLPDiskImportSet.get(i).getSex());
            db.setBirthday(mLPDiskImportSet.get(i).getBirthday());
            db.setIDType(mLPDiskImportSet.get(i).getIDType());
            db.setIDNo(mLPDiskImportSet.get(i).getIDNo());
            db.setOthIDType(mLPDiskImportSet.get(i).getOthIDType());
            db.setOthIDNo(mLPDiskImportSet.get(i).getOthIDNo());

            check.setLCInsured(db.getSchema());
            int isInsured = check.checkInsured();
            if (isInsured != check.OLD)
            {
                //被保人在保单中没有记录，剔除
                errMsg.append("序号为" + mLPDiskImportSet.get(i).getSerialNo())
                        .append(" 被保人姓名为")
                        .append(mLPDiskImportSet.get(i).getInsuredName() + "，");
                mLPDiskImportSet.removeRange(i, i);
                i--;
                flag = true;
            }
            else
            {
                LCInsuredSchema tLCInsuredSchema = check.getLCInsured();
                mLPDiskImportSet.get(i).setEdorNo(this.edorNo);
                mLPDiskImportSet.get(i).setEdorType(this.edorType);
                mLPDiskImportSet.get(i).setGrpContNo(this.grpContNo);
                mLPDiskImportSet.get(i).setInsuredNo(tLCInsuredSchema.getInsuredNo());
                mLPDiskImportSet.get(i).setState("1");
                mLPDiskImportSet.get(i).setContPlanCode(
                    tLCInsuredSchema.getContPlanCode());
                mLPDiskImportSet.get(i).setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(mLPDiskImportSet.get(i));

                //个人保单批改项目表信息
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setEdorAcceptNo(edorNo);
                tLPEdorItemSchema.setEdorNo(edorNo);
                tLPEdorItemSchema.setContNo(tLCInsuredSchema.getContNo());
                tLPEdorItemSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                tLPEdorItemSchema.setEdorValiDate(mLPDiskImportSet.get(i)
                                                  .getEdorValiDate());
                mLPEdorItemSet.add(tLPEdorItemSchema);

                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                tLPInsuredSchema.setContNo(tLCInsuredSchema.getContNo());
                tLPInsuredSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                mLPInsuredSet.add(tLPInsuredSchema);
            }
        }
        if(flag)
        {
            mErrors.addOneError(errMsg.toString() + "不是本保单客户；");
        }
    }

    /**
     * 进入个人保全
     * @param tLCInsuredListSet LCInsuredListSet
     * @return boolean
     */
    private boolean createEdorDetailTQ()
    {
        //团单批改项目表信息

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo(edorNo);
        tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
        tLPGrpEdorItemSchema.setEdorNo(edorNo);
        tLPGrpEdorItemSchema.setEdorType(edorType);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tLPGrpEdorItemSchema);
        tVData.add(mLPEdorItemSet);
        tVData.add(mLPInsuredSet);
        tVData.add(mLPDiskImportSet);

        mImportPersonsForTQ = mLPDiskImportSet.size();

        //调用团单项目信息录入类
        GEdorTQDetailBL tGEdorTQDetailBL = new GEdorTQDetailBL();
        if(!tGEdorTQDetailBL.submitData(tVData, "INSERT||EDOR"))
        {
            mErrors.copyAllErrors(tGEdorTQDetailBL.mErrors);
            return false;
        }
        else
        {
            importPersons = mLPDiskImportSet.size();
        }

        return true;
    }

    /**
     * 得到减人明细处理后成功导入的人数
     * @return int
     */
    public int getImportPersons()
    {
        return importPersons;
    }

    /**
     * 传给减人明细处理类的人数
     * @return int
     */
    public int getImportPersonsForTQ()
    {
        return mImportPersonsForTQ;
    }
    /**
     *
     * @去掉LPDiskImpot表中重复的人
     */
    public LPDiskImportSet getIn(LPDiskImportSet mLPDiskImportSet){
        HashMap hashmap = new HashMap();
        HashMap hashmap1= new HashMap();
        LPDiskImportSet tLPDiskImportSet =new LPDiskImportSet();
        LPDiskImportSet tLPDiskImportSet1=new LPDiskImportSet();   //用来记录重复的人信息
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            if (!hashmap.containsKey(mLPDiskImportSet.get(i).getIDNo()))
            {
                hashmap.put(mLPDiskImportSet.get(i).getIDNo(), "");
                tLPDiskImportSet.add(mLPDiskImportSet.get(i));
            }
            else
            {
                if (!hashmap1.containsKey(mLPDiskImportSet.get(i).getIDNo()))
                        {
                         hashmap1.put(mLPDiskImportSet.get(i).getIDNo(), "");
                         tLPDiskImportSet1.add(mLPDiskImportSet.get(i)); //重复人的记录
                        }

            }

        }

        mImportPersonsForTQCF=tLPDiskImportSet1.size();  //重复人数
        mLPDiskImportSetForTQCF=tLPDiskImportSet1; //重复人记录

        return tLPDiskImportSet;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();
        t.setNameAndValue("edorNo", "20060508000004");
        t.setNameAndValue("grpContNo", "0000165301");
        t.setNameAndValue("edorType", "TQ");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "CC.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportTQBL d = new DiskImportTQBL();
        d.submitData(v, "INSERT||EDOR");

        if(d.mErrors.needDealError())
        {
            System.out.println(d.mErrors.getErrContent());
        }
    }
}

