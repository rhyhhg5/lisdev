package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite Luomin
 * @version 2.0
 */
public class PEdorAcceptConfirmBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    private VData pInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /**  */
    LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
    LPEdorAppSet mLPEdorAppSet = new LPEdorAppSet();
    LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
    LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    String mStrTemplatePath = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String FlagStr;
    private static String Content;
    private String tWorkNo = "";
    private String tAccpetNo = "";
    private String tDetailWorkNo = "";

    private MMap map = new MMap();
    public PEdorAcceptConfirmBL() {}

    /**
     * 数据提交到数据库
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitData(cInputData, cOperate) == null)
        {
            return false;
        }
        //　数据提交
        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PEdorAcceptConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 处理数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public MMap getSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return null;
        }
        checkCont();
        if (!checkData())
        {
            return null;
        }

        //数据准备操作，检查团体保全主表数据，看是否通过了保全核保
        if (!prepareData())
        {
            return null;
        }

        //数据操作业务处理
        if (cOperate.equals("INSERT||EDORACPTCONFIRM"))
        {
            if (!dealData())
            {
                return null;
            }

        }
        mInputData.add(map);
        mResult.add(map);
        return map;
    }


    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult() {
        return mResult;
    }

    //added by luomin   --@2005-8-3
    private void checkCont() {
        //LGWorkDB tLGWorkDB = new LGWorkDB();
        //LGWorkSet tLGWorkSet = new LGWorkSet();
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
        tLPEdorAppSet = tLPEdorAppDB.query();
        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        tLPEdorAppSchema = tLPEdorAppSet.get(1);
        String sql = "select workno from LGWork where CustomerNo = '" +
                     tLPEdorAppSchema.getOtherNo() + "' " +
                     " and TypeNo ='0300003' and AcceptWayNo='8' " +
                     "and acceptno ='" + mLPEdorAppSchema.getEdorAcceptNo() +
                     "' " +
                     "and InnerSource is not null";
        //tLGWorkSet = tLGWorkDB.executeQuery(sql);
        //if(tLGWorkSet.size()==0){
        //    createCont();
        //}
        ExeSQL tExeSQL = new ExeSQL();
        String strWorkno = tExeSQL.getOneValue(sql);
        if (strWorkno == "") {
            createCont();
        } else {}
    }

    //added by luomin   --@2005-8-3   多保单关联被保人修改，如不在同一被保人下做CM生成新的工单
    private void createCont() {
        System.out.println("now in checkCont:========================");
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        tLPInsuredDB.setEdorNo(mLPEdorAppSchema.getEdorAcceptNo());
        tLPInsuredDB.setEdorType("CM");
        //tLPInsuredDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
        tLPInsuredSet = tLPInsuredDB.query();
        if (tLPInsuredSet.size() >= 1) {
            for (int k = 1; k <= tLPInsuredSet.size(); k++) {
                LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(k);
                LGWorkSchema tLGWorkSchema = new LGWorkSchema();
                LGWorkDB tLGWorkDB = new LGWorkDB();
                LGWorkSet tLGWorkSet = new LGWorkSet();
                //当修改一被保人时，搜索在其他保单中有无次被保人
                String strSQL =
                        "select distinct a.AppntNo " +
                        "from LCCont a , LCInsured b " +
                        "where a.ContNo = b.ContNo " +
                        "and a.AppFlag = '1' " +
                        //"and a.Conttype = '1' " +        //限定关联变化只限于个单
                        "and b.AppntNo <> '" + tLPInsuredSchema.getAppntNo() +
                        "' " +
                        "and b.InsuredNo = '" +
                        tLPInsuredSchema.getInsuredNo() + "' " +
                        "UNION " +
                        "select distinct a.AppntNo " +
                        "from LCCont a , LCAppnt b " +
                        "where a.ContNo = b.ContNo " +
                        "and a.AppFlag = '1' " +
                        //"and a.Conttype = '1' " +        //限定关联变化只限于个单
                        "and b.AppntNo <> '" + tLPInsuredSchema.getAppntNo() +
                        "' " +
                        "and b.AppntNo = '" +
                        tLPInsuredSchema.getInsuredNo() + "' ";

                ExeSQL tExeSQL = new ExeSQL();
                String strAppntno = tExeSQL.getOneValue(strSQL);

                if (strAppntno == "") {
                    break;
                }

                tLGWorkDB.setCustomerNo(strAppntno);
                tLGWorkDB.setStatusNo("2");
                tLGWorkDB.setTypeNo("0300003");
                tLGWorkDB.setInnerSource(mLPEdorAppSchema.getEdorAcceptNo());
                tLGWorkDB.setAcceptWayNo("8");
                tLGWorkSet = tLGWorkDB.query();
                if (tLGWorkSet.size() <= 0) {
                    tLGWorkSchema.setCustomerNo(strAppntno);
                    tLGWorkSchema.setStatusNo("2");
                    tLGWorkSchema.setTypeNo("0300003");
                    tLGWorkSchema.setInnerSource(mLPEdorAppSchema.
                                                 getEdorAcceptNo());
                    tLGWorkSchema.setAcceptWayNo("8");
                    VData tVData = new VData();
                    tVData.add(tLGWorkSchema);
                    tVData.add(mGlobalInput);
                    TaskInputBL tTaskInputBL = new TaskInputBL();
                    if (tTaskInputBL.submitData(tVData, "") == false) {
                        FlagStr = "Fail";
                        Content = "数据保存失败！";
                    } else {
                        //设置显示信息
                        VData tRet = tTaskInputBL.getResult();
                        LGWorkSchema mLGWorkSchema = new LGWorkSchema();
                        mLGWorkSchema.setSchema((LGWorkSchema) tRet.
                                                getObjectByObjectName(
                                "LGWorkSchema", 0));

                        tWorkNo = mLGWorkSchema.getWorkNo();
                        tAccpetNo = mLGWorkSchema.getAcceptNo();
                        tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();

                        FlagStr = "Succ";
                        Content = "数据保存成功，录入工单的受理号为：" + tAccpetNo;
                    }
                    System.out.println("End checkCont:========================");
                }
            }
        } else {
            LPAppntDB tLPAppntDB = new LPAppntDB();
            LPAppntSet tLPAppntSet = new LPAppntSet();
            tLPAppntDB.setEdorNo(mLPEdorAppSchema.getEdorAcceptNo());
            tLPAppntDB.setEdorType("CM");
            //tLPInsuredDB.setInsuredNo(mLPEdorItemSchema.getInsuredNo());
            tLPAppntSet = tLPAppntDB.query();

            for (int k = 1; k <= tLPAppntSet.size(); k++) {
                LPAppntSchema tLPAppntSchema = tLPAppntSet.get(k);
                LGWorkSchema tLGWorkSchema = new LGWorkSchema();
                LGWorkDB tLGWorkDB = new LGWorkDB();
                LGWorkSet tLGWorkSet = new LGWorkSet();
                String strSQL =
                        "select distinct a.AppntNo " +
                        "from LCCont a , LCInsured b " +
                        "where a.ContNo = b.ContNo " +
                        "and a.AppFlag = '1' " +
                        "and b.AppntNo <> '" + tLPAppntSchema.getAppntNo() +
                        "' " +
                        "and b.InsuredNo = '" + tLPAppntSchema.getAppntNo() +
                        "' ";
                ExeSQL tExeSQL = new ExeSQL();
                String strAppntno = tExeSQL.getOneValue(strSQL);
                if (strAppntno == "") {
                    break;
                }

                tLGWorkDB.setCustomerNo(strAppntno);
                tLGWorkDB.setStatusNo("2");
                tLGWorkDB.setTypeNo("0300003");
                tLGWorkDB.setInnerSource(mLPEdorAppSchema.getEdorAcceptNo());
                tLGWorkDB.setAcceptWayNo("8");
                tLGWorkSet = tLGWorkDB.query();
                if (tLGWorkSet.size() <= 0) {
                    tLGWorkSchema.setCustomerNo(strAppntno);
                    tLGWorkSchema.setStatusNo("2");
                    tLGWorkSchema.setTypeNo("0300003");
                    tLGWorkSchema.setInnerSource(mLPEdorAppSchema.
                                                 getEdorAcceptNo());
                    tLGWorkSchema.setAcceptWayNo("8");
                    VData tVData = new VData();
                    tVData.add(tLGWorkSchema);
                    tVData.add(mGlobalInput);
                    TaskInputBL tTaskInputBL = new TaskInputBL();
                    if (tTaskInputBL.submitData(tVData, "") == false) {
                        FlagStr = "Fail";
                        Content = "数据保存失败！";
                    } else {
                        //设置显示信息
                        VData tRet = tTaskInputBL.getResult();
                        LGWorkSchema mLGWorkSchema = new LGWorkSchema();
                        mLGWorkSchema.setSchema((LGWorkSchema) tRet.
                                                getObjectByObjectName(
                                "LGWorkSchema", 0));

                        tWorkNo = mLGWorkSchema.getWorkNo();
                        tAccpetNo = mLGWorkSchema.getAcceptNo();
                        tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();

                        FlagStr = "Succ";
                        Content = "数据保存成功，录入工单的受理号为：" + tAccpetNo;
                    }
                    System.out.println("End checkCont:========================");
                }
            }

        }
    }

    /**
     * 业务逻辑处理方法
     * @return boolean
     */
    public boolean dealData() {
        pInputData = new VData();

        EdorConfirmBL aEdorConfirmBL = new EdorConfirmBL();
        //        mLPEdorAppSchema.setSchema(mLPEdorAppSet.get(i));
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
        mLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询个人保全失败！");
        }
        for (int i = 1; i <= mLPEdorMainSet.size(); i++) {
            pInputData.clear();
            pInputData.addElement(mGlobalInput);
            pInputData.addElement("I");
            pInputData.addElement(mLPEdorAppSchema);
            pInputData.addElement(mStrTemplatePath);

            System.out.println("处理个人保全主表");
            pInputData.addElement(mLPEdorMainSet.get(i));
            if (!aEdorConfirmBL.submitData(pInputData, mOperate)) {
                this.mErrors.copyAllErrors(aEdorConfirmBL.mErrors);
                return false;
            } else {
                VData rVData = aEdorConfirmBL.getResult();
                MMap tMap = new MMap();
                tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                if (tMap == null) {
                    CError.buildErr(this,
                                    "得到个人保单为:" +
                                    mLPEdorMainSet.get(i).getContNo() +
                                    "的保全确认结果时失败！");
                    return false;

                } else {
                    map.add(tMap);
                }
            }
        }
        String tEdorAcceptNo = mLPEdorAppSchema.getEdorAcceptNo();
        //下面要增加处理保全申请主表的逻辑
        mLPEdorAppSchema.setEdorState("0");
        mLPEdorAppSchema.setConfOperator(mGlobalInput.Operator);
        mLPEdorAppSchema.setConfDate(PubFun.getCurrentDate());
        mLPEdorAppSchema.setConfTime(PubFun.getCurrentTime());
        mLPEdorAppSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorAppSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPEdorAppSchema, "UPDATE");
        String wherePart = "where EdorAcceptNo='" +
                           tEdorAcceptNo + "'";
        map.put(
                "update LPEdorApp set ChgPrem= (select sum(ChgPrem) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set ChgAmnt= (select sum(ChgAmnt) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set GetMoney= (select sum(GetMoney) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");
        map.put(
                "update LPEdorApp set GetInterest= (select sum(GetInterest) from LPEdorMain "
                + wherePart + ") " + wherePart, "UPDATE");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mStrTemplatePath = (String) mInputData.get(0);
            mLPEdorAppSchema = (LPEdorAppSchema) mInputData.
                               getObjectByObjectName("LPEdorAppSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData() {

        mInputData.clear();

        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setSchema(mLPEdorAppSchema);
        if (!tLPEdorAppDB.getInfo()) {
            CError.buildErr(this, "查询保全申请主表时失败！");
            return false;
        } else {
            mLPEdorAppSchema.setSchema(tLPEdorAppDB.getSchema());
        }
        mInputData.addElement(mGlobalInput);
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData() {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = new LJSPaySet();

        tLJSPayDB.setOtherNo(mLPEdorAppSchema.getEdorAcceptNo());
        tLJSPayDB.setOtherNoType("10");
        tLJSPaySet = tLJSPayDB.query();

        if (tLJSPaySet.size() > 0) {
            String aGetNoticeNo = tLJSPaySet.get(1).getGetNoticeNo();
            LJFinaConfirm tLJFinaConfirm = new LJFinaConfirm(aGetNoticeNo,
                    "I");
            tLJFinaConfirm.setOperator(mGlobalInput.Operator);
            tLJFinaConfirm.setLimit(PubFun.getNoLimit(mGlobalInput.
                    ManageCom));
            System.out.println("start LJFinaConfirm...");
            if (!tLJFinaConfirm.submitData()) {

                //需要缴费，将保全申请状态转换为待缴费
                String updateSql = "update LPEdorApp "
                                   + "set edorState='9' "
                                   + "where edorAcceptNo='"
                                   + mLPEdorAppSchema.getEdorAcceptNo() + "' ";
                MMap map = new MMap();
                VData inputData = new VData();
                PubSubmit tPubSubmit = new PubSubmit();

                map.put(updateSql, "UPDATE");
                inputData.add(map);

                if (!tPubSubmit.submitData(inputData, "UPDATE||MAIN"))
                {
                    this.mErrors.addOneError("需要缴费，但是在修改保全申请状态为\"待缴费\"时出错");
                    return false;
                }

                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EdorConfirmBL";
                tError.functionName = "checkData";
                tError.errorMessage = "保全交费核销失败！请确认是否已交费！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        VData tInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        PEdorAcceptConfirmBL aPEdorAcceptConfirmBL = new
                PEdorAcceptConfirmBL();
        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "endor";

        tLPEdorAppSchema.setEdorAcceptNo("20050919000022");
//        tLPEdorAppSchema.setEdorNo("410110000000157");
        String strTemplatePath = "xerox/printdata/";

        tInputData.addElement(strTemplatePath);
        tInputData.addElement(tLPEdorAppSchema);
        tInputData.addElement(tGlobalInput);

        if (!aPEdorAcceptConfirmBL.submitData(tInputData,
                                              "INSERT||EDORACPTCONFIRM")) {
            System.out.println(aPEdorAcceptConfirmBL.mErrors.getErrContent());
        }
    }

}
