package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体医疗追加保费</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorZAAppConfirmBL implements EdorAppConfirm {
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private LPInsureAccClassSet mLPInsureAccClassSet = new LPInsureAccClassSet();
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult() {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                                       getObjectByObjectName(
                                               "LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mVtsData = new EdorItemSpecialData(edorItem);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
        if (!setGrpAcc()) {
            return false;
        }
        if (!setInsuredAcc()) {
            return false;
        }
        
        setEdorInfo();
        setGrpEdorItem();

        return true;
    }

    /**
     * 设置团体账户追加保费金额
     */
    private boolean setGrpAcc() {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++) {
            String grpPolNo = grpPolNos[i];

            String sqlEspecialRiskcode =
                    "select a.riskcode from lcgrppol a,lmriskapp b where a.grppolno = '"
                    + grpPolNo + "' and a.riskcode = b.riskcode  ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
            if (tSSRS.getMaxRow() != 0 && CommonBL.isTDBCPol(tSSRS.GetText(1, 1)) &&!"162401".equals(tSSRS.GetText(1, 1))) {
                String polNo = CommonBL.getPubliAccPolNo(grpPolNo);
                
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(polNo,
                                                CommonBL.getInsuAccNo(BQ.
                        ACCTYPE_GROUP,tSSRS.GetText(1, 1)));
                if (tLCInsureAccSchema == null) {
                    mErrors.addOneError("找不到团体账户信息！");
                    return false;
                }
                mSpecialData.setGrpPolNo(grpPolNo);
                double getMoney =
                        Double.parseDouble(mSpecialData.getEdorValue(
                                "GroupMoney"));
                System.out.println("getMoney" + getMoney);
                if (getMoney == 0) {
                    continue;
                }

                String tchgpremcalcode= "select chgpremcalcode from LMPolDutyEdorCal where riskcode='"+tLCInsureAccSchema.getRiskCode()+"'" 
                					+ "and edortype='"+mEdorType+"' and dutycode = (select dutycode from lcduty where polno='"+polNo+"')";
                String  chgpremcalcode= tExeSQL.getOneValue(tchgpremcalcode);
                System.out.println(chgpremcalcode);
                Calculator tCalculator = new Calculator();
                tCalculator.setCalCode(chgpremcalcode);
                tCalculator.addBasicFactor("Prem", String.valueOf(getMoney));

                String tMult= "select calfactorvalue from LCContPlanDutyParam where grpcontno='"+mGrpContNo+"'"
                			  +" and  mainriskcode = '"+tLCInsureAccSchema.getRiskCode()+"' and calfactor='Mult' fetch first 1 rows only";
                String  mult= new ExeSQL().getOneValue(tMult);
                tCalculator.addBasicFactor("Mult",String.valueOf(mult));
                String amnt=tCalculator.calculate();
                
                if(amnt.equals(""))
                {
                    mErrors.addOneError("公共部分保额计算失败！");
                    return false;
                }
                double addAmnt=Double.parseDouble(amnt);
                System.out.println("addAmnt:"+addAmnt);
                
                double leftMoney = CommonBL.carry(tLCInsureAccSchema.
                        getInsuAccBala())  + addAmnt;
                double sumMoney = CommonBL.carry(tLCInsureAccSchema.
                        getSumPay() ) + addAmnt;
                LPInsureAccSchema tLPInsureAccSchema = new
                        LPInsureAccSchema();
                
                double chgMoney = CommonBL.carry(getMoney);
                Reflections ref = new Reflections();
                ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                tLPInsureAccSchema.setEdorNo(mEdorNo);
                tLPInsureAccSchema.setEdorType(mEdorType);
                tLPInsureAccSchema.setInsuAccBala(leftMoney);
                tLPInsureAccSchema.setSumPay(sumMoney);
                tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                tLPInsureAccSchema.setModifyDate(mCurrentDate);
                tLPInsureAccSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
                if (tLCPolSchema == null) {
                    mErrors.addOneError("找不到团体公共账户的险种信息！");
                    return false;
                }
                setGetEndorse(chgMoney, tLCPolSchema);
                mGetMoney += getMoney;
                //设置批单显示数据
                LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                tLCInsureAccClassDB.setGrpContNo(mGrpContNo);
                tLCInsureAccClassDB.setInsuAccNo(tLPInsureAccSchema.getInsuAccNo());
                tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
                LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                if(tLCInsureAccClassSet.size()>=1)
                {
                	LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
                	LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
                	ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
                	tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
                	tLPInsureAccClassSchema.setEdorNo(mEdorNo);
                	tLPInsureAccClassSchema.setEdorType(mEdorType);
                	tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                	tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
                	tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
                }
                mVtsData.setGrpPolNo(grpPolNos[i]);
                mVtsData.add("GrpGetMoney", CommonBL.bigDoubleToCommonString(getMoney,"0.00"));
                mVtsData.add("GrpLeftMoney", CommonBL.bigDoubleToCommonString(leftMoney,"0.00"));
                setLPInsureAccTrace(tLCInsureAccSchema, addAmnt);


            }
        }
        return true;
    }
    /**
     * 设置帐户追加轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.FEEFINATYPE_BF);
        tLPInsureAccTraceSchema.setMoney(Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }



    /**
     * 设置个人账户追加保费金额
     * @return boolean
     */
    private boolean setInsuredAcc() {
        LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            
            ExeSQL tExeSQL = new ExeSQL();

                LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet
                        (mEdorNo, mEdorType, mGrpContNo, null,
                         BQ.IMPORTSTATE_SUCC);
                double insuredGetMoney = 0.0;
                for (int j = 1; j <= tLPDiskImportSet.size(); j++) {
                    LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.
                            get(j);
                    String insuredNo = tLPDiskImportSchema.getInsuredNo();
                    
                    LCInsureAccSchema tLCInsureAccSchema =
                            CommonBL.getLCInsureAcc(grpPolNo, insuredNo,
                            BQ.ACCTYPE_INSURED);
                    if (tLCInsureAccSchema == null) {
                        mErrors.addOneError("找不到客户" + insuredNo + "的个人账户信息！");
                        return false;
                    }
                    double getMoney = tLPDiskImportSchema.getMoney();
                    LCPolSchema tLCPol=CommonBL.getLCPol(grpPolNo,insuredNo);
                    String tchgpremcalcode= "select chgpremcalcode from LMPolDutyEdorCal where riskcode='"+tLCPol.getRiskCode()+"'" 
                    					+ "and edortype='"+mEdorType+"' fetch first row only";
                    String  chgpremcalcode= tExeSQL.getOneValue(tchgpremcalcode);
                    System.out.println(chgpremcalcode);
                    Calculator tCalculator = new Calculator();
                    tCalculator.setCalCode(chgpremcalcode);
                    tCalculator.addBasicFactor("Prem", String.valueOf(getMoney));

                    String tMult= "select calfactorvalue from LCContPlanDutyParam where grpcontno='"+mGrpContNo+"'"
                    			  +" and  mainriskcode = '"+tLCGrpPolSchema.getRiskCode()+"' and calfactor='Mult' fetch first 1 rows only";
                    String  mult= new ExeSQL().getOneValue(tMult);
                    tCalculator.addBasicFactor("Mult",String.valueOf(mult));
                    String amnt=tCalculator.calculate();
                    
                    if(amnt.equals(""))
                    {
                        mErrors.addOneError("客户" + insuredNo + "的保额计算失败！");
                        return false;
                    }
                    double addAmnt=Double.parseDouble(amnt);
                    System.out.println("addAmnt:"+addAmnt);
                    
                    double leftMoney = CommonBL.carry(tLCInsureAccSchema.
                            getInsuAccBala())  + addAmnt;
                    double sumMoney = CommonBL.carry(tLCInsureAccSchema.
                            getSumPay() ) + addAmnt;
                    LPInsureAccSchema tLPInsureAccSchema = new
                            LPInsureAccSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                    tLPInsureAccSchema.setEdorNo(mEdorNo);
                    tLPInsureAccSchema.setEdorType(mEdorType);
                    tLPInsureAccSchema.setInsuAccBala(leftMoney);
                    tLPInsureAccSchema.setSumPay(sumMoney);
                    tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                    tLPInsureAccSchema.setModifyDate(mCurrentDate);
                    tLPInsureAccSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                    LCPolSchema tLCPolSchema = CommonBL.getLCPol(grpPolNo,
                            insuredNo);
                    if (tLCPolSchema == null) {
                        mErrors.addOneError("找不到客户" + insuredNo + "的险种信息！");
                        return false;
                    }
                    setGetEndorse(getMoney, tLCPolSchema);
                    mGetMoney += getMoney;
                    insuredGetMoney += getMoney;
                    setDiskImport(tLPDiskImportSchema, leftMoney, addAmnt);
                    
                    LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                    tLCInsureAccClassDB.setGrpContNo(mGrpContNo);
                    tLCInsureAccClassDB.setInsuAccNo(tLPInsureAccSchema.getInsuAccNo());
                    tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
                    LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
                    if(tLCInsureAccClassSet.size()>=1)
                    {
                    	LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
                    	LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
                    	ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
                    	tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
                    	tLPInsureAccClassSchema.setEdorNo(mEdorNo);
                    	tLPInsureAccClassSchema.setEdorType(mEdorType);
                    	tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                    	tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
                    	tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
                        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
                    }
                    setLPInsureAccTrace(tLCInsureAccSchema, addAmnt);
                }
                //设置批单显示数据
                mVtsData.setGrpPolNo(grpPolNo);
                mVtsData.add("InsuredGetMoney", CommonBL.bigDoubleToCommonString(insuredGetMoney,"0.00"));


            
        }
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private void setGetEndorse(double edorPrem, LCPolSchema aLCPolSchema) {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }


    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem() {
        String sql = "update LPGrpEdorItem " +
                     "set GetMoney = " + mGetMoney + ", " +
                     "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                     "UWFlag = '" + BQ.UWFLAG_PASS + "', " +
                     "ModifyDate = '" + mCurrentDate + "', " +
                     "ModifyTime = '" + mCurrentTime + "' " +
                     "where EdorNo = '" + mEdorNo + "' " +
                     "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema,
                               double leftMoney ,double addAmnt) {
        tLPDiskImportSchema.setGetMoney(addAmnt);
        tLPDiskImportSchema.setMoney2(String.valueOf(leftMoney));
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }

    /**
     * 保存要在批单中显示的数据
     */
    private void setEdorInfo() {
        mMap.put(mVtsData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
//        GlobalInput gi = new GlobalInput();
//        gi.Operator = "endor0";
//        gi.ComCode = "86";
//
//        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
//        tLPGrpEdorItemDB.setEdorNo("20070213000061");
//
//        VData d = new VData();
//        d.add(gi);
//        d.add(tLPGrpEdorItemDB.query().get(1));
//
//        GrpEdorZBAppConfirmBL bl = new GrpEdorZBAppConfirmBL();
//        if(!bl.submitData(d, ""))
//        {
//            System.out.println(bl.mErrors.getErrContent());
//        }
    	String grpPolNo = "2201169996"; 
    	ExeSQL ES = new ExeSQL();
        String strSQL = " select b.* from ldcode a ,lcgrppol b where a.code=b.riskcode " +
		" and a.codetype='ChargeFeeRateType' " +
		" and b.grppolno='"+grpPolNo+"' " +
		" with ur ";
        String insuredNo= "00156397" ;
        String StrSQL = " select b.* from ldcode a ,lcgrppol b where a.code=b.riskcode " +
		" and a.codetype='ChargeFeeRateType' " +
		" and b.grppolno='"+grpPolNo+"' " +
		" with ur ";
        String chargeFeeType=ES.getOneValue(StrSQL);
        System.out.println(chargeFeeType);
    }
}
