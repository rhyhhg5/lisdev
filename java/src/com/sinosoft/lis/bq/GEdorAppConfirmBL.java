package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.task.Task;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统保全申请确认变动功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author Tjj
 * @version 1.0
 */
public class GEdorAppConfirmBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    //操作符
    public String mOperate;

    /** 全局数据 */
    private MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
    private LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
    private LJSGetEndorseSchema mLJSGetEndorseSchema = new LJSGetEndorseSchema();
    String mPayPrintParams = "";

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (!prepareData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 普通保全项目对集体单操作
     * @return
     */
    private boolean prepareData()
    {
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();

        LPGrpEdorItemDB aLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorMainSchema = (LPGrpEdorMainSchema) mInputData.
                getObjectByObjectName("LPGrpEdorMainSchema",
                0);
        mLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainSchema);
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setSchema(tLPGrpEdorMainSchema);
        tLPGrpEdorMainDB.getInfo();
        if (tLPGrpEdorMainDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询团体批单主表时失败");
            return false;
        }
        tLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainDB);
        mLPGrpEdorMainSchema.setSchema(tLPGrpEdorMainDB);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput",
                0);

        mLPGrpEdorItemSet.clear();
        aLPGrpEdorItemDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
        aLPGrpEdorItemDB.setEdorNo(tLPGrpEdorMainSchema.getEdorNo());
        aLPGrpEdorItemDB.setEdorState("1");

        mLPGrpEdorItemSet = aLPGrpEdorItemDB.query();
        if (aLPGrpEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询团体批单项目表时失败");
            return false;
        }
        return true;
    }

    /**
     * 普通保全项目对集体单操作
     * @return
     */
    private boolean dealData()
    {
        ExeSQL aExeSQL = new ExeSQL();
        String tEdorNo = mLPGrpEdorMainSchema.getEdorNo();
        String tGrpContNo = mLPGrpEdorMainSchema.getGrpContNo();
        //按照保全项目来进行确认（tjj chg 1024）
        String tEdorType="";
        for (int i = 1; i <= mLPGrpEdorItemSet.size(); i++)
        {

            LPGrpEdorItemSchema iLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            iLPGrpEdorItemSchema.setSchema(mLPGrpEdorItemSet.get(i));

            tEdorType = iLPGrpEdorItemSchema.getEdorType();
            //检查个单是否全部通过计算
            String sql = "select count(*) from LPEdorItem where GrpContNo='" +
                    tGrpContNo + "' and EdorNo='" +
                    tEdorNo + "' and EdorType='" + tEdorType +
                    "' and EdorState ='1'";
            String aReturn = aExeSQL.getOneValue(sql);
            if (aExeSQL.mErrors.needDealError())
            {
                CError.buildErr(this,
                        "查询批单号为" + tEdorNo + ",项目为" + tEdorType + "下个单保全项目时失败");
                return false;
            }

            int aCount;
            aCount = 100;

            if ((aReturn == null) || aReturn.trim().equals("0"))
            {
                aCount = 0;
            }
            else
            {
                aCount = Integer.parseInt(aReturn);
            }

            System.out.println("aCount :" + aCount);

            if (aCount == 0)
            {

                iLPGrpEdorItemSchema.setEdorState("2");
                iLPGrpEdorItemSchema.setUWFlag("0");

                //按照保全项目对集体单进行特殊处理
                System.out.println("edortype:" + tEdorType);
                try
                {
                    Class tClass = Class.forName("com.sinosoft.lis.bq.GrpEdor" +
                            tEdorType +
                            "AppConfirmBL");
                    System.out.println(tClass);
                    EdorAppConfirm tGrpEdorAppConfirm = (EdorAppConfirm)
                            tClass.newInstance();
                    System.out.println(tClass);
                    
                    VData tVData = new VData();

                    tVData.add(iLPGrpEdorItemSchema);
                    tVData.add(mLJSGetEndorseSchema);
                    tVData.add(mGlobalInput);
                    tVData.add(tLPGrpEdorMainSet);

                    if (!tGrpEdorAppConfirm.submitData(tVData,
                            "APPCONFIRM||" + tEdorType))
                    {
                        //add by fuxin 2010-6-23 异常时 将险种的缴费频次置回原来的状态
                        String tPayIntv = new ExeSQL().getOneValue(
                                "select payintv from lcgrppol where grpcontno ='" +
                                mLPGrpEdorMainSchema.getGrpContNo() +
                                "' and riskcode ='280101' with ur");
                        if (tPayIntv != "" || !tPayIntv.equals("")) {
                            setPayIntv();
                        }
                        mErrors.copyAllErrors(tGrpEdorAppConfirm.mErrors);
                        return false;
                    }
                    else
                    {
                        VData rVData = tGrpEdorAppConfirm.getResult();
                        MMap tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                        if (tMap == null)
                        {
                            CError.buildErr(this,
                                            "得到保全项目为:" + tEdorType +
                                            "的保全申请确认结果时失败！");
                            return false;

                        }
                        else
                        {
                            map.add(tMap);
                        }
                    }

                }
                catch (ClassNotFoundException ex)
                {
                	System.out.println("ClassNotFoundException  这里抛异常了");
                    map.put(iLPGrpEdorItemSchema, "UPDATE");
                    String wherePart = "where EdorNo='" +
                            tEdorNo + "' and EdorType='" +
                            tEdorType + "'";
                    String updateSql =  "update LPGrpEdorItem set ChgPrem= nvl((select sum(ChgPrem) from LPEdorItem "
                            + wherePart + "),0), "
                            + "ChgAmnt= nvl((select sum(ChgAmnt) from LPEdorItem "
                            + wherePart + "),0), "
                            +
                            "GetMoney= nvl((select sum(GetMoney) from LPEdorItem "
                            + wherePart + "),0), "
                            +
                            "GetInterest= nvl((select sum(GetInterest) from LPEdorItem "
                            + wherePart + "),0) "
                            + wherePart;
                    System.out.println(updateSql);
                    //liuxin 20110531
                    String updateSql2="";
                    if(tEdorType.equals("TQ")){
                    	updateSql2 =  "update LPGrpEdorItem set edorvalidate= (select edorvalidate from LPEdorItem "
                            + wherePart + ") "
                            + wherePart;
                    	map.put(updateSql2, "UPDATE");
                    }
                    map.put(updateSql, "UPDATE");
                    
                }
                catch (Exception ex)
                {
                    CError.buildErr(this, "保全项目" + tEdorType + "申请确认时失败！");
                    return false;
                }
                System.out.println("ClassNotFoundException  这里执行了没有");
            }
        }
        System.out.println("ClassNotFoundException  开始执行lpedorapp");
        //更新app表中的edorstate
        String sql = "update LPEdorApp " +
                     "set EdorState = '2', " +
                     "Operator = '" + mGlobalInput.Operator + "', " +
                     "ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                     "ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                     "where EdorAcceptNo = '" + tEdorNo +"' ";
        map.put(sql, "UPDATE");

        mLPGrpEdorMainSchema.setEdorState("2");
        mLPGrpEdorMainSchema.setUWState("0");
        mLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
        mLPGrpEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
        tLPGrpEdorMainSet.add(mLPGrpEdorMainSchema);
        map.put(mLPGrpEdorMainSchema, "UPDATE");
        String wherePart = "where EdorNo='" +
                tEdorNo + "' and GrpContNo='" + tGrpContNo + "'";
        map.put(
                "update LPGrpEdorMain set ChgPrem= (select sum(ChgPrem) from LPGrpEdorItem "
                + wherePart + "), "
                + "ChgAmnt= (select sum(ChgAmnt) from LPGrpEdorItem "
                + wherePart + "), "
                + "GetMoney= (select sum(GetMoney) from LPGrpEdorItem "
                + wherePart + "), "
                + "GetInterest= (select sum(GetInterest) from LPGrpEdorItem "
                + wherePart + ") "
                + wherePart, "UPDATE");
        //liuxin 20110531
        if(tEdorType.equals("TQ")){
        	map.put(
                    "update LPGrpEdorMain set edorvalidate= (select edorvalidate from LPGrpEdorItem "
        			+ wherePart + ")"
                    + wherePart, "UPDATE");
        }
        updateAppMoney();

        setEdorState();
        setStatusNo(Task.WORKSTATUS_DOING);

        return true;
    }


    /**
     * 设置状态
     * qiuyang 2006 add
     */
    private void setEdorState()
    {
        String sql = "  update LPEdorItem "
                + "set EdorState = '2', "
                + "    Operator = '" + mGlobalInput.Operator + "', "
                + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where EdorAcceptNo = '"
                + mLPGrpEdorMainSchema.getEdorAcceptNo() + "' ";
        map.put(sql, "UPDATE");
    }


    /**
     * 统计Main表中的保费变化,同步更新App表
     */
    private void updateAppMoney()
    {
        String sql = "update LPEdorApp " +
                "set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = " +
                "    (select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), " +
                "    sum(GetInterest) from LPGrpEdorMain " +
                "    where EdorAcceptNo = '" +
                mLPGrpEdorMainSchema.getEdorAcceptNo() + "') " +
                "where EdorAcceptNo = '" + mLPGrpEdorMainSchema.getEdorAcceptNo() +
                "' ";
        map.put(sql, "UPDATE");
    }

    private boolean prepareOutputData()
    {

        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(map);

        return true;
    }

    private void setStatusNo(String workstatus)
    {
    	 String sql = null;
         sql = "update lgwork set statusno = '" + workstatus + "', Operator = '" + mGlobalInput.Operator
             + "', ModifyDate = '" + PubFun.getCurrentDate() + "', ModifyTime = '" + PubFun.getCurrentTime()
             + "' where workno = '" + mLPGrpEdorMainSchema.getEdorAcceptNo() + "' ";
         map.put(sql, "UPDATE");
	}

        /**
  * 将团单险种层的缴费频次状态恢复
  */
 private void setPayIntv()
 {
     String tPayIntv = new ExeSQL().getOneValue(
             "select edorvalue From LPEdorEspecialData where edorno ='" +
             mLPGrpEdorMainSchema.getEdorAcceptNo() + "' and detailtype ='PAYINTV'");
     if (tPayIntv != "" || !tPayIntv.equals("")) {
         String updateSQL = "update  lcgrppol set payintv = " + tPayIntv +
                            " where grpcontno ='" + mLPGrpEdorMainSchema.getGrpContNo() +
                            "' and riskcode ='280101'";
         MMap tMMap = new MMap();
         tMMap.put(updateSQL, SysConst.UPDATE);
         VData data = new VData();
         data.add(tMMap);
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(data, "")) {
             mErrors.copyAllErrors(tPubSubmit.mErrors);
         }
     }
 }
}
