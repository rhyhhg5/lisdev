/**
 * <p>ClassName: LAContReceiveUI </p>
 * <p>Description: LAContReceive类文件 </p>
 * <p>Copyright: Copyright (c) 2007.7</p>
 * @Database: 代理人管理
 * <p>Company: sinosoft</p>
 * @author xiongxin
 */

package com.sinosoft.lis.bq;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BqUpdateDScardflagUI {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public BqUpdateDScardflagUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;// 这是空的
		// 用于获得一个现有的载体的另一个副本.该副本将有一个参考的内部数据数组的克隆而不是参考的原始内部数据数组
		this.mInputData = (VData) cInputData.clone();
		BqUpdateDScardflagBL updateDScardflagBL = new BqUpdateDScardflagBL();
		try {
			if (!updateDScardflagBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(updateDScardflagBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "BqUpdateDScardflagUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
