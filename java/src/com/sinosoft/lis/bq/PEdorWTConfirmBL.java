package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorWTConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private LPEdorItemSchema mLPEdorItemSchema = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private Reflections ref=new Reflections();

    public PEdorWTConfirmBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public void setOperate(String cOperate)
    {
        this.mOperate = cOperate;
    }

    public String getOperate()
    {
        return this.mOperate;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.setOperate(cOperate);

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据准备操作（preparedata())
        if (!prepareData())
        {
            return false;
        }

        this.setOperate("CONFIRM||WT");
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorNIAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
        {
            mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            return false;
        }
        mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLPEdorMainDB.getInfo())
        {
            mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
            mErrors.addOneError(new CError("查询保全信息失败！"));
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
   	   	mResult.clear();
        MMap map = new MMap();

        String aEdorNo = mLPEdorItemSchema.getEdorNo();
        ContCancel tContCancel = new ContCancel();
        tContCancel.setEdorType(mLPEdorItemSchema.getEdorType());

        LPContSet tLPContSet = getLPCont(); //查询整单退保信息
        LPInsuredSet tLPInsuredSet = getLPInsured(); //查询被保险人退保信息
        LPPolSet tLPolSet = getLPPol(); //查询险种退保信息
        if(tLPContSet == null || tLPInsuredSet == null || tLPolSet == null)
        {
            return false;
        }
        //若为银保通申请工单保全确认成功，则修改银保通表信息。
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLPEdorItemSchema.getContNo());
        LCAppntSet tLCAppntSet = tLCAppntDB.query();
        if(tLCAppntSet==null||tLCAppntSet.size()<1){
        		System.out.println("修改银保通数据失败");
        }else{
        	LCAppntSchema tLCAppntSchema = tLCAppntSet.get(1).getSchema();
        	
        	//查询银保通犹豫期退保数据
        	LPYBTAppWTDB tLPYBTAppWTDB = new LPYBTAppWTDB();
        	tLPYBTAppWTDB.setContNo(mLPEdorItemSchema.getContNo());
        	tLPYBTAppWTDB.setAppntName(tLCAppntSchema.getAppntName());
        	tLPYBTAppWTDB.setAppntSex(tLCAppntSchema.getAppntSex());
        	tLPYBTAppWTDB.setAppntBirthday(tLCAppntSchema.getAppntBirthday());
        	tLPYBTAppWTDB.setAppntIDType(tLCAppntSchema.getIDType());
        	tLPYBTAppWTDB.setAppntIDNo(tLCAppntSchema.getIDNo());
        	LPYBTAppWTSet tLPYBTAppWTSet = tLPYBTAppWTDB.query();
        	if(tLPYBTAppWTSet!=null&&tLPYBTAppWTSet.size()>0){
        		LPYBTAppWTSchema tLPYBTAppWTSchema = tLPYBTAppWTSet.get(1).getSchema();
        		tLPYBTAppWTSchema.setAppState("2");
        		tLPYBTAppWTSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
        		tLPYBTAppWTSchema.setModifyDate(PubFun.getCurrentDate());
        		tLPYBTAppWTSchema.setModifyTime(PubFun.getCurrentTime());
        		map.put(tLPYBTAppWTSchema, SysConst.UPDATE);
        	}
        	
        }
        
        if (tLPContSet.size() == 0
            && tLPInsuredSet.size() == 0
            && tLPolSet.size() == 0)
        {
            CError.buildErr(this, "没有查到任何准备退保的数据！");
            return false;
        }

        if (tLPContSet.size()>0) //合同退保
        {
            MMap tMMap = new MMap();
            tMMap = tContCancel.prepareContData(mLPEdorItemSchema.getContNo(),
                                                aEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个人保单数据失败！");
                return false;
            }
            map.add(tMMap);
        }
        else
        {
             //被保险人退保
             MMap tMMap = new MMap();
             for (int i = 1; i <= tLPInsuredSet.size(); i++)
             {
                 LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(i);

                 //已被保人退保的险种不需要再进行险种退保
                 if(!dealLPPolOfInsured(tLPolSet, tLPInsuredSchema))
                 {
                     return false;
                 }

                 tMMap = tContCancel.prepareInsuredData(
                     mLPEdorItemSchema.getContNo(),
                     tLPInsuredSchema.getInsuredNo(),
                     aEdorNo);
                 if (tContCancel.mErrors.needDealError())
                 {
                     CError.buildErr(this, "准备被保险人数据失败！");
                     return false;
                 }
                 map.add(tMMap);
             }
             //险种退保
             for (int i = 1; i <= tLPolSet.size(); i++)
             {
                 tMMap = tContCancel.preparePolData(tLPolSet.get(i).getPolNo(),
                     aEdorNo);
                 if (tContCancel.mErrors.needDealError())
                 {
                     CError.buildErr(this, "准备个人险种数据失败！");
                     return false;
                 }
                 map.add(tMMap);
             }
             
//           20101220 zhanggm 处理万能附加重疾犹豫期退保，将月结扣除的风险保费返回到账户中
             if(!dealULIAppendFee(tLPolSet,map))
             {
             	return false;
             }
        }
        
        //添加保费留存的数据修改
        map.add(getRemainMap());

        //若发生扣除体检费操作，则将相应的保单体检通知状态置为已扣除体检费（6）
        map.add(getTJInfo());

        mResult.add(map);

        return true;
    }
    
    private MMap getRemainMap(){
    	MMap tMMap = new MMap();
    	
    	String updateSQL = "";
    	
    	String remainSQL = "select 1 from lccontremain where remainstate='1' " +
    			" and exists (select 1 from lpcont where contno=lccontremain.contno " +
    			" and (cvalidate + 2 years) > '"+mLPEdorItemSchema.getEdorValiDate()+"' " +
    			" and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"') " +
    			" and applyyears='2' " +
    			" and contno='"+mLPEdorItemSchema.getContNo()+"' ";
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	String remainFlag = tExeSQL.getOneValue(remainSQL);
    	
    	if(remainFlag!=null&&"1".equals(remainFlag)){
    		 updateSQL = " update  lccontremain set remainstate='3',edorZTdate='"+PubFun.getCurrentDate()+"',edorZTfee="+mLPEdorItemSchema.getGetMoney()+", " +
    				" modifydate='"+PubFun.getCurrentDate()+"',modifytime='"+PubFun.getCurrentTime()+"' where contno='"+mLPEdorItemSchema.getContNo()+"' and applyyears='2' ";
    		 tMMap.put(updateSQL, SysConst.UPDATE);
    	}
    	
    	
    	return tMMap;
    }

    /**
     * 已被保人退保的险种不需要再进行险种退保
     * @param tLPolSet LPPolSet
     * @param tLPInsuredSchema LPInsuredSchema
     * @return boolean
     */
    private boolean dealLPPolOfInsured(LPPolSet cLPolSet,
                                       LPInsuredSchema tLPInsuredSchema)
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPPolDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
        tLPPolDB.setContNo(tLPInsuredSchema.getContNo());

        LPPolSet tLPPolSet = tLPPolDB.query();
        if(tLPPolDB.mErrors.needDealError())
        {
            mErrors.addOneError("查询退保险种信息出错。");
            System.out.println("PEdorWTConfirmBL查询退保险种信息出错。");
            return false;
        }

        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            boolean loop = true;
            for (int t = 1; loop && t <= cLPolSet.size(); t++)
            {
                if (tLPPolSet.get(i).getPolNo().equals(cLPolSet.get(t).getPolNo()))
                {
                    cLPolSet.removeRange(t, t);
                    t--;
                    loop = false;
                }
            }
        }

        return true;
    }

    /**
     * 查询整单退保信息
     * @return LPContSet
     */
    private LPContSet getLPCont()
    {
        LPContDB tLPContDB=new LPContDB();
        tLPContDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPContDB.setContNo(mLPEdorItemSchema.getContNo());
        LPContSet set = tLPContDB.query();

        if (tLPContDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询个人保单数据失败！");
            return null;
        }
        return set;
    }

    /**
     * 查询被保人退保信息
     * @return LPInsuredSet
     */
    private LPInsuredSet getLPInsured()
    {
        LPInsuredDB tLPInsuredDB=new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPInsuredDB.setContNo(mLPEdorItemSchema.getContNo());
        LPInsuredSet tLPInsuredSet=tLPInsuredDB.query();

        if (tLPInsuredDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询个人保单被保险人数据失败！");
            return null;
        }
        return tLPInsuredSet;
    }

    /**
     * 查询险种退保信息
     * @return LPPolSet
     */
    private LPPolSet getLPPol()
    {
        LPPolDB tLPPolDB=new LPPolDB();
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LPPolSet tLPolSet=tLPPolDB.query();

        if (tLPPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询个人保单险种数据失败！");
                return null;
        }
        return tLPolSet;
    }

    private MMap getTJInfo()
    {
        MMap map = new MMap();

        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
        tLPPENoticeDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPPENoticeDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPENoticeDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPPENoticeSet tLPPENoticeSet = tLPPENoticeDB.query();
        if(tLPPENoticeSet.size() > 0)
        {
            for(int i = 1; i <= tLPPENoticeSet.size(); i++)
            {
                tLPPENoticeSet.get(i).setPEState("06");  //已扣除体检费
                Reflections r = new Reflections();
                LCPENoticeSchema schema = new LCPENoticeSchema();
                r.transFields(schema, tLPPENoticeSet.get(i));
                map.put(schema, "UPDATE");
            }
            map.put(tLPPENoticeSet, "UPDATE");
        }
        
        return map;
    }
    
//  20101220 zhanggm 处理万能附加重疾犹豫期退保，将月结扣除的风险保费返回到账户中
    private boolean dealULIAppendFee(LPPolSet cLPPolSet, MMap map)
    {
    	MMap tMap = new MMap();
    	
    	String tContNo = mLPEdorItemSchema.getContNo();
    	String tEdorNo = mLPEdorItemSchema.getEdorNo();
    	String tEdorType = mLPEdorItemSchema.getEdorType();
    	
    	if (tContNo == null || tContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTConfirmBL";
            tError.functionName = "dealULIAppendFee";
            tError.errorMessage = "保单号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	if (tEdorNo == null || tEdorNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTConfirmBL";
            tError.functionName = "dealULIAppendFee";
            tError.errorMessage = "受理号查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	if (tEdorType == null || tEdorType.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorWTConfirmBL";
            tError.functionName = "dealULIAppendFee";
            tError.errorMessage = "保全类型查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	
//    	保单不包含万能险，不需要处理
    	if(!CommonBL.hasULIRisk(tContNo))
    	{
    		return true;
    	}
    	
    	// 退保的险种,含有万能主险，整单退保，不需要处理
    	for(int i=1; i<=cLPPolSet.size(); i++)
    	{
    		if(CommonBL.isULIRisk(cLPPolSet.get(i).getRiskCode()))
    		{
    			return true ;
    		}
    	}
    	
    	//不含主险，则只能是附加重疾单独退保 C表P表互换数据
    	for(int j=1; j<=cLPPolSet.size(); j++)
    	{
    		LPPolSchema tLPPolSchema = cLPPolSet.get(j);
            String tRiskCode = tLPPolSchema.getRiskCode();
            String tPolNo = tLPPolSchema.getPolNo();
            String tMainPolNo = tLPPolSchema.getMainPolNo();//主险PolNo
           
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if(tLMRiskAppDB.getInfo())
            {
            	String tRiskType3 = tLMRiskAppDB.getRiskType3();
            	if(tRiskType3.equals("3")) //重疾
				{
            		LPInsureAccDB tLPInsureAccDB = new LPInsureAccDB();
            		tLPInsureAccDB.setEdorNo(tEdorNo);
            		tLPInsureAccDB.setEdorType(tEdorType);
            		tLPInsureAccDB.setContNo(tContNo);
            		tLPInsureAccDB.setPolNo(tMainPolNo);
            		LPInsureAccSet tLPInsureAccSet = tLPInsureAccDB.query();
            		if(tLPInsureAccSet == null || tLPInsureAccSet.size()==0)
			        {
            			continue;
			        }
            		LPInsureAccSchema tLPInsureAccSchema = tLPInsureAccSet.get(1).getSchema();
            		
            		LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            		tLCInsureAccDB.setContNo(tContNo);
            		tLCInsureAccDB.setPolNo(tMainPolNo);
            		LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
            		if(tLCInsureAccSet == null || tLCInsureAccSet.size()==0)
			        {
						CError tError = new CError();
						tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LCAcc!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
            		LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(1).getSchema();
            		
            		double newInsuAccBala = tLPInsureAccSchema.getInsuAccBala(); //新余额
            		double oldInsuAccBala = tLCInsureAccSchema.getInsuAccBala(); //老余额
            		
            		tLPInsureAccSchema.setInsuAccBala(oldInsuAccBala);
					tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccSchema.setModifyDate(mCurrentDate);
					tLPInsureAccSchema.setModifyTime(mCurrentTime);
					
					tLCInsureAccSchema.setInsuAccBala(newInsuAccBala);
					tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
					tLCInsureAccSchema.setModifyDate(mCurrentDate);
					tLCInsureAccSchema.setModifyTime(mCurrentTime);
            		
					
					LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();
					tLPInsureAccClassDB.setEdorNo(tEdorNo);
					tLPInsureAccClassDB.setEdorType(tEdorType);
					tLPInsureAccClassDB.setContNo(tContNo);
					tLPInsureAccClassDB.setPolNo(tMainPolNo);
					LPInsureAccClassSet tLPInsureAccClassSet = tLPInsureAccClassDB.query();
					if(tLPInsureAccClassSet == null || tLPInsureAccClassSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LPClass!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LPInsureAccClassSchema tLPInsureAccClassSchema = tLPInsureAccClassSet.get(1).getSchema();
					tLPInsureAccClassSchema.setInsuAccBala(oldInsuAccBala);
					tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
					tLCInsureAccClassDB.setContNo(tContNo);
					tLCInsureAccClassDB.setPolNo(tMainPolNo);
					LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
					if(tLCInsureAccClassSet == null || tLCInsureAccClassSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败Class!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccClassSchema tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema();
					tLCInsureAccClassSchema.setInsuAccBala(newInsuAccBala);
					tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
					tLCInsureAccClassSchema.setModifyDate(mCurrentDate);
					tLCInsureAccClassSchema.setModifyTime(mCurrentTime);
					
					
					LPInsureAccFeeDB tLPInsureAccFeeDB = new LPInsureAccFeeDB();
					tLPInsureAccFeeDB.setEdorNo(tEdorNo);
					tLPInsureAccFeeDB.setEdorType(tEdorType);
					tLPInsureAccFeeDB.setContNo(tContNo);
					tLPInsureAccFeeDB.setPolNo(tMainPolNo);
					LPInsureAccFeeSet tLPInsureAccFeeSet = tLPInsureAccFeeDB.query();
					if(tLPInsureAccFeeSet == null || tLPInsureAccFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LPFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LPInsureAccFeeSchema tLPInsureAccFeeSchema = tLPInsureAccFeeSet.get(1).getSchema();

					LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
					tLCInsureAccFeeDB.setContNo(tContNo);
					tLCInsureAccFeeDB.setPolNo(tMainPolNo);
					LCInsureAccFeeSet tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
					if(tLCInsureAccFeeSet == null || tLCInsureAccFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LCFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccFeeSchema tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1).getSchema();
					
					double oldFee = tLCInsureAccFeeSchema.getFee();
					double newFee = tLPInsureAccFeeSchema.getFee();
					
					tLPInsureAccFeeSchema.setFee(oldFee);
					tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
					
					tLCInsureAccFeeSchema.setFee(newFee);
					tLCInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
					tLCInsureAccFeeSchema.setModifyDate(mCurrentDate);
					tLCInsureAccFeeSchema.setModifyTime(mCurrentTime);
					
					LPInsureAccClassFeeDB tLPInsureAccClassFeeDB = new LPInsureAccClassFeeDB();
					tLPInsureAccClassFeeDB.setEdorNo(tEdorNo);
					tLPInsureAccClassFeeDB.setEdorType(tEdorType);
					tLPInsureAccClassFeeDB.setContNo(tContNo);
					tLPInsureAccClassFeeDB.setPolNo(tMainPolNo);
					LPInsureAccClassFeeSet tLPInsureAccClassFeeSet = tLPInsureAccClassFeeDB.query();
					if(tLPInsureAccClassFeeSet == null || tLPInsureAccClassFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LPClassFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = tLPInsureAccClassFeeSet.get(1).getSchema();
					tLPInsureAccClassFeeSchema.setFee(newFee);
					tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
					tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
					tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
					
					LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
					tLCInsureAccClassFeeDB.setContNo(tContNo);
					tLCInsureAccClassFeeDB.setPolNo(tMainPolNo);
					LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
					if(tLCInsureAccClassFeeSet == null || tLCInsureAccClassFeeSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LCClassFee!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(1).getSchema();
					tLCInsureAccClassFeeSchema.setFee(newFee);
					tLCInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
					tLCInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
					tLCInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
					
					LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
					tLPInsureAccTraceDB.setEdorNo(tEdorNo);
					tLPInsureAccTraceDB.setEdorType(tEdorType);
					LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
					if(tLPInsureAccTraceSet == null || tLPInsureAccTraceSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LCFeeTrace!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					
					LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
					for(int i=1; i<= tLPInsureAccTraceSet.size() ;i++)
					{
						LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
						LPInsureAccTraceSchema tLPInsureAccTraceSchema = tLPInsureAccTraceSet.get(i).getSchema();
						tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
						tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
						tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
						ref.transFields(tLCInsureAccTraceSchema, tLPInsureAccTraceSchema);
						tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
						tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
						tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
						tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
						tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
						tLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
					}
					
					LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new LPInsureAccFeeTraceDB();
					tLPInsureAccFeeTraceDB.setEdorNo(tEdorNo);
					tLPInsureAccFeeTraceDB.setEdorType(tEdorType);
					LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB.query();
					if(tLPInsureAccFeeTraceSet == null || tLPInsureAccFeeTraceSet.size()==0)
			        {
						CError tError = new CError();
			            tError.moduleName = "PEdorWTConfirmBL";
			            tError.functionName = "dealULIAppendFee";
			            tError.errorMessage = "查询账户信息失败LPFeeTrace!";
			            this.mErrors.addOneError(tError);
			            return false;
			        }
					
					LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
					for(int i=1; i<= tLPInsureAccFeeTraceSet.size() ;i++)
					{
						LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
						LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = tLPInsureAccFeeTraceSet.get(i).getSchema();
						tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
						tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
						tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
						ref.transFields(tLCInsureAccFeeTraceSchema, tLPInsureAccFeeTraceSchema);
						tLCInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
						tLCInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
						tLCInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
						tLCInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
						tLCInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
						tLCInsureAccFeeTraceSet.add(tLCInsureAccFeeTraceSchema);
					}
					
					tMap.put(tLPInsureAccSchema, SysConst.UPDATE);
					tMap.put(tLCInsureAccSchema, SysConst.UPDATE);
					tMap.put(tLPInsureAccClassSchema, SysConst.UPDATE);
					tMap.put(tLCInsureAccClassSchema, SysConst.UPDATE);
					tMap.put(tLPInsureAccFeeSchema, SysConst.UPDATE);
					tMap.put(tLCInsureAccFeeSchema, SysConst.UPDATE);
					tMap.put(tLPInsureAccClassFeeSchema, SysConst.UPDATE);
					tMap.put(tLCInsureAccClassFeeSchema, SysConst.UPDATE);
					
					tMap.put(tLCInsureAccTraceSet, SysConst.DELETE_AND_INSERT);
					tMap.put(tLCInsureAccFeeTraceSet, SysConst.DELETE_AND_INSERT);
					
					map.add(tMap);
					
				}
				else if(tRiskType3.equals("4")) //意外
				{
					continue;
				}
            }
    	}
    	
    	return true;
    }
}
