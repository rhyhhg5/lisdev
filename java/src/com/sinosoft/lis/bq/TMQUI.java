package com.sinosoft.lis.bq;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */

public class TMQUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public TMQUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    TMQBL tTMQBL = new TMQBL();
    System.out.println("-----UI BEGIN-----"+cOperate);
        if(!tTMQBL.submitData(cInputData,cOperate)){
        	System.out.println("UI---false---");
            this.mErrors.copyAllErrors(tTMQBL.mErrors);
            return false;
        } else {
        	System.out.println("true");
            mResult = tTMQBL.getResult();
        }
        return true;
    }

  public VData getResult()
  {
    return mResult;
  }

/*  public static void main(String[] args)
  {
    System.out.println("-------test...");

    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.Operator = "001";
    tGlobalInput.ComCode = "86";
    tGlobalInput.ManageCom = "86";

    LCContStateSchema tLCContStateSchema = new LCContStateSchema();
    tLCContStateSchema.setContNo("86110020040430000258");
    //tLCContStateSchema.setEdorType("AC");

    VData tVData = new VData();
    tVData.addElement(tGlobalInput);
    tVData.addElement(tLCContStateSchema);

    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    TMQUI tTMQUI = new TMQUI();
    tTMQUI.submitData(tVData, "DELETE||EDOR");


  }*/
}