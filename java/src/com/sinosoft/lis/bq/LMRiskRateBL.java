package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskRateSchema;
import com.sinosoft.lis.schema.LICertifyImportLogSchema;
import com.sinosoft.lis.vschema.LDRiskRateSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class LMRiskRateBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /* 业务相关的数据 */
    private LDRiskRateSet mLDRiskRateSet = new LDRiskRateSet();
    private LDRiskRateSchema tLDRiskRateSchema=new LDRiskRateSchema();
    private VData mResult = new VData();
    private GlobalInput globalInput = new GlobalInput();
    private String mszOperate = "";


    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

    public LMRiskRateBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = cOperate;
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
        	return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        boolean flag = true;
        MMap tMMap = new MMap();
        String tErrorInfo = "";
        System.out.println(mLDRiskRateSet.size());
        for(int i=1;i<=mLDRiskRateSet.size();i++){
        	LDRiskRateSchema mLDRiskRateSchema = mLDRiskRateSet.get(i);
        	String tSQL = "";
        	int mIdNo;
        	String tIdNoSql = "select max(idno) from LDRiskRate where codetype='FXDJ' with ur ";
        	String tIdNo = new ExeSQL().getOneValue(tIdNoSql);
    		if("INSERT||MAIN".equals(mszOperate)){

            	System.out.println(tIdNo);
            	if("".equals(tIdNo) || null == tIdNo){
            		mIdNo=1;
            	}else{
            		mIdNo=Integer.parseInt(tIdNo)+1;
            	}
            	tLDRiskRateSchema.setIdNo(mIdNo);
            	tLDRiskRateSchema.setCodeType("FXDJ");
            	tLDRiskRateSchema.setState("1");
            	tLDRiskRateSchema.setYear(mLDRiskRateSchema.getYear());
            	tLDRiskRateSchema.setQuarter(mLDRiskRateSchema.getQuarter());
            	tLDRiskRateSchema.setRiskRate(mLDRiskRateSchema.getRiskRate());
            	tLDRiskRateSchema.setOperater(globalInput.Operator);
            	tLDRiskRateSchema.setMakeDate(PubFun.getCurrentDate());
            	tLDRiskRateSchema.setMakeTime(PubFun.getCurrentTime());
            	tLDRiskRateSchema.setModifyDate(PubFun.getCurrentDate());
            	tLDRiskRateSchema.setModifyTime(PubFun.getCurrentTime());
            	tMMap.put(tLDRiskRateSchema, SysConst.INSERT);
            	
            	
            }else if("UPDATE||MAIN".equals(mszOperate)){
            	
            	tSQL = "update ldriskrate set state = '0',modifydate=current date,modifytime=current time where codetype='FXDJ' and year='"+mLDRiskRateSchema.getYear()+"' and quarter ='"+mLDRiskRateSchema.getQuarter()+"' and state='1' ";
            	tMMap.put(tSQL, SysConst.UPDATE);
            	
            	mIdNo=Integer.parseInt(tIdNo)+1;
            	tLDRiskRateSchema.setIdNo(mIdNo);
            	tLDRiskRateSchema.setCodeType("FXDJ");
            	tLDRiskRateSchema.setState("1");
            	tLDRiskRateSchema.setYear(mLDRiskRateSchema.getYear());
            	tLDRiskRateSchema.setQuarter(mLDRiskRateSchema.getQuarter());
            	tLDRiskRateSchema.setRiskRate(mLDRiskRateSchema.getRiskRate());
            	tLDRiskRateSchema.setOperater(globalInput.Operator);
            	tLDRiskRateSchema.setMakeDate(PubFun.getCurrentDate());
            	tLDRiskRateSchema.setMakeTime(PubFun.getCurrentTime());
            	tLDRiskRateSchema.setModifyDate(PubFun.getCurrentDate());
            	tLDRiskRateSchema.setModifyTime(PubFun.getCurrentTime());
            	tMMap.put(tLDRiskRateSchema, SysConst.INSERT);

            }
    	}
//		保存数据
        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }
        tMMap = null;
    	return flag;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
    	
    	globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0));
    	mLDRiskRateSet.set((LDRiskRateSet) vData.getObjectByObjectName("LDRiskRateSet",0));
        if(mLDRiskRateSet == null || mLDRiskRateSet.size()<=0){
        	buildError("getInputData","获取数据失败！");
        	return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LMSolvencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
