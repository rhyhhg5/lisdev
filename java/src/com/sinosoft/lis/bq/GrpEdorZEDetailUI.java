package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 提前领取</p>
 * <p>Description:提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorZEDetailUI
{
    private GrpEdorZEDetailBL mGrpEdorZEDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GrpEdorZEDetailUI(GlobalInput gi, String edorNo,
            String grpContNo,EdorItemSpecialData tSpecialData)
    {
        mGrpEdorZEDetailBL = new GrpEdorZEDetailBL(gi, edorNo, grpContNo, tSpecialData);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGrpEdorZEDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGrpEdorZEDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorZEDetailBL.getMessage();
    }
}
