package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 提前领取</p>
 * <p>Description:提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorTLDetailUI
{
    private GrpEdorTLDetailBL mGrpEdorTLDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GrpEdorTLDetailUI(GlobalInput gi, String edorNo,
            String grpContNo,String fmtransact, EdorItemSpecialData specialData)
    {
        mGrpEdorTLDetailBL = new GrpEdorTLDetailBL(gi, edorNo, grpContNo,fmtransact, specialData);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGrpEdorTLDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGrpEdorTLDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorTLDetailBL.getMessage();
    }
}
