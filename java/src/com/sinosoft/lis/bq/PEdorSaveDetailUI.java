package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保存明细</p>
 * <p>Description: 个单保存明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorSaveDetailUI
{
    private PEdorSaveDetailBL mPEdorSaveDetailBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public PEdorSaveDetailUI(GlobalInput gi, LPEdorItemSchema edorItem)
    {
        mPEdorSaveDetailBL = new PEdorSaveDetailBL(gi, edorItem);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorSaveDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorSaveDetailBL.mErrors.getFirstError();
    }
}
