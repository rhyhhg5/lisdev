package com.sinosoft.lis.bq;

import java.util.List;

import com.cbsws.obj.EdorInsuredInfo;
import com.cbsws.obj.GrpEdorItemInfo;
import com.cbsws.xml.ctrl.complexpds.LCInsuredTable;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LPNIZTInsuredSchema;
import com.sinosoft.lis.schema.LBInsuredListSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.task.TaskAutoFinishBL;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class BQNIZTInfaceBL {

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public CErrors mErrors = new CErrors();
	public String mBatchNo="";//批次号
	public GrpEdorItemInfo mGrpEdorItemInfo;
	
	private LCGrpContSchema mLCGrpContSchema;
	private List tLCInsuredList;
	private TransferData mTransferData = null;
	private LPNIZTInsuredSchema mLPNIZTInsuredSchema = new LPNIZTInsuredSchema();
	private LBInsuredListSchema mLBInsuredListSchema = new LBInsuredListSchema();
	private LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
	
	private MMap map = new MMap();
	private VData mResult = new VData();
	
	private String mCurrDate = PubFun.getCurrentDate();
	private String mCurrTime = PubFun.getCurrentTime();
	
	public BQNIZTInfaceBL() {
	}

	public BQNIZTInfaceBL(GlobalInput tGlobalInput, LCGrpContSchema tLCGrpContSchema) {
		this.mGlobalInput = tGlobalInput;
		this.mLCGrpContSchema = tLCGrpContSchema;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);

		// 请求批次号
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			mErrors.addOneError("请传入请求批次号");
			return false;
		}
		mBatchNo = (String) mTransferData.getValueByName("BatchNo");
		if (mBatchNo == null) {
			mErrors.addOneError("请传入请求批次号");
			return false;
		}

		mGrpEdorItemInfo = (GrpEdorItemInfo) cInputData.getObjectByObjectName(
				"GrpEdorItemInfo", 0);
		mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
				"LCGrpContSchema", 0);
		
		tLCInsuredList = (List) cInputData.getObjectByObjectName("ArrayList", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("传入参数信息错误");
			return false;
		}
		return true;
	}

	public boolean submit(VData cInputData) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!deal()) {
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) {
			return false;
		}

		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, "")) { // 数据提交
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BQNIZTInfaceBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("BQNIZTInfaceBL End PubSubmit");
		return true;
	}

	private boolean deal() {
		
//		String ManageSql = "";
//		ExeSQL tExeSQL = new ExeSQL();
//		String ManageCom = tExeSQL.getOneValue(ManageSql);
				
		mLPNIZTInsuredSchema.setBatchNo(mBatchNo); // 请求报文批次号
		mLPNIZTInsuredSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo()); //团单号
		mLPNIZTInsuredSchema.setManageCom(mLCGrpContSchema.getManageCom());
		mLPNIZTInsuredSchema.setEdorType(mGrpEdorItemInfo.getEdorType()); // 增人：NI，减人：ZT
		mLPNIZTInsuredSchema.setDealState("1"); // 处理状态，1：申请，2：工单已结案
		mLPNIZTInsuredSchema.setAcceptDate(mGrpEdorItemInfo.getAcceptDate());//申请日期
		mLPNIZTInsuredSchema.setDealResult("0"); // 处理结果，0：未完成，1：成功，2：失败
		mLPNIZTInsuredSchema.setOperator(mGlobalInput.Operator);
		mLPNIZTInsuredSchema.setMakeDate(mCurrDate);
		mLPNIZTInsuredSchema.setMakeTime(mCurrTime);
		mLPNIZTInsuredSchema.setModifyDate(mCurrDate);
		mLPNIZTInsuredSchema.setModifyTime(mCurrTime);
		map.put(mLPNIZTInsuredSchema, "INSERT");

		// 增人
		if (BQ.EDORTYPE_NI.equals(mGrpEdorItemInfo.getEdorType())) {
			for (int i = 0; i < tLCInsuredList.size(); i++) {
				EdorInsuredInfo tEdorInsuredInfo = (EdorInsuredInfo) tLCInsuredList.get(i);
				mLBInsuredListSchema = new LBInsuredListSchema();
				mLBInsuredListSchema.setSerialNo(mBatchNo);
				mLBInsuredListSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
				mLBInsuredListSchema.setContNo(tEdorInsuredInfo.getContID());
				mLBInsuredListSchema.setInsuredID(tEdorInsuredInfo.getInsuredID());
				mLBInsuredListSchema.setState(tEdorInsuredInfo.getInsuredState());
				mLBInsuredListSchema.setEmployeeName(tEdorInsuredInfo.getEmployeeName());
				mLBInsuredListSchema.setInsuredName(tEdorInsuredInfo.getName());
				mLBInsuredListSchema.setRelation(tEdorInsuredInfo.getRelationToMainInsured());
				mLBInsuredListSchema.setSex(tEdorInsuredInfo.getSex());
				mLBInsuredListSchema.setBirthday(tEdorInsuredInfo.getBirthday());
				mLBInsuredListSchema.setIDType(tEdorInsuredInfo.getIDType());
				mLBInsuredListSchema.setIDNo(tEdorInsuredInfo.getIDNo());
				mLBInsuredListSchema.setContPlanCode(tEdorInsuredInfo.getContPlanCode());
				mLBInsuredListSchema.setOccupationType(tEdorInsuredInfo.getOccupationType());
				mLBInsuredListSchema.setBankCode(tEdorInsuredInfo.getBankCode());
				mLBInsuredListSchema.setAccName(tEdorInsuredInfo.getAccName());
				mLBInsuredListSchema.setBankAccNo(tEdorInsuredInfo.getBankAccno());
				mLBInsuredListSchema.setPrem(tEdorInsuredInfo.getPrem());
				mLBInsuredListSchema.setEdorValiDate(tEdorInsuredInfo.getCValidate());
				mLBInsuredListSchema.setPhone(tEdorInsuredInfo.getPhone());
				mLBInsuredListSchema.setOperator(mGlobalInput.Operator);
				mLBInsuredListSchema.setMakeDate(mCurrDate);
				mLBInsuredListSchema.setMakeTime(mCurrTime);
				mLBInsuredListSchema.setModifyDate(mCurrDate);
				mLBInsuredListSchema.setModifyTime(mCurrTime);
				map.put(mLBInsuredListSchema, "INSERT");
			}
		}
		// 减人
		else {
			for (int i = 0; i < tLCInsuredList.size(); i++) {
				EdorInsuredInfo tEdorInsuredInfo = (EdorInsuredInfo) tLCInsuredList.get(i);
				mLPDiskImportSchema = new LPDiskImportSchema();
				mLPDiskImportSchema.setEdorNo(mBatchNo);
				mLPDiskImportSchema.setEdorType(BQ.EDORTYPE_ZT);
				mLPDiskImportSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
				mLPDiskImportSchema.setSerialNo(tEdorInsuredInfo.getInsuredID());
				mLPDiskImportSchema.setInsuredNo(tEdorInsuredInfo.getContID());
				mLPDiskImportSchema.setState(tEdorInsuredInfo.getInsuredState());
				mLPDiskImportSchema.setEmployeeName(tEdorInsuredInfo.getEmployeeName());
				mLPDiskImportSchema.setInsuredName(tEdorInsuredInfo.getName());
				mLPDiskImportSchema.setRelation(tEdorInsuredInfo.getRelationToMainInsured());
				mLPDiskImportSchema.setSex(tEdorInsuredInfo.getSex());
				mLPDiskImportSchema.setBirthday(tEdorInsuredInfo.getBirthday());
				mLPDiskImportSchema.setIDType(tEdorInsuredInfo.getIDType());
				mLPDiskImportSchema.setIDNo(tEdorInsuredInfo.getIDNo());
				mLPDiskImportSchema.setContPlanCode(tEdorInsuredInfo.getContPlanCode());
				mLPDiskImportSchema.setOccupationType(tEdorInsuredInfo.getOccupationType());
				mLPDiskImportSchema.setBankCode(tEdorInsuredInfo.getBankCode());
				mLPDiskImportSchema.setAccName(tEdorInsuredInfo.getAccName());
				mLPDiskImportSchema.setBankAccNo(tEdorInsuredInfo.getBankAccno());
				mLPDiskImportSchema.setMoney(tEdorInsuredInfo.getGetMoney());
				mLPDiskImportSchema.setEdorValiDate(tEdorInsuredInfo.getCValidate());
				mLPDiskImportSchema.setPhone(tEdorInsuredInfo.getPhone());
				mLPDiskImportSchema.setOperator(mGlobalInput.Operator);
				mLPDiskImportSchema.setMakeDate(mCurrDate);
				mLPDiskImportSchema.setMakeTime(mCurrTime);
				mLPDiskImportSchema.setModifyDate(mCurrDate);
				mLPDiskImportSchema.setModifyTime(mCurrTime);
				map.put(mLPDiskImportSchema, "INSERT");
			}
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareData() {
		mResult.clear();
		mResult.add(map);
		return true;
	}
}
