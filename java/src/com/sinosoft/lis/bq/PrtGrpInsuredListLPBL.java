package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要修改理赔金帐户的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtGrpInsuredListLPBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private LPEdorAppSchema mLPEdorAppSchema = null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
//    private BankCodeQuery mBankCodeQuery = new BankCodeQuery();
//    private IdTypeQuery mIdTypeQuery = new IdTypeQuery();
    ListTable mtListTable = new ListTable();

    public PrtGrpInsuredListLPBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTransferData = (TransferData) cInputData.
                            getObjectByObjectName("TransferData", 0);
            mLPEdorAppSchema = (LPEdorAppSchema) cInputData.
                               getObjectByObjectName("LPEdorAppSchema", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtGrpInsuredListLP.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        tag.add("GrpName", getCompanyName());
        String edorType = (String) mTransferData.getValueByName("edorType");
        tag.add("EdorName", CommonBL.getEdorInfo(edorType, "G").getEdorName());
        tag.add("EdorNo", mLPEdorAppSchema.getEdorAcceptNo());
        xmlexport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }
        String[] title = {"序号", "客户号","被保人姓名" ,
                         "证件类型", "证件号码", "变更改理赔金转帐银行", "户名",
                         "帐号"};

        xmlexport.addListTable(tListTable, title);
        xmlexport.outputDocumentToFile("C:\\", "PrtGrpInsuredListLPBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    //查询公司名称
    private String getCompanyName()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPEdorAppSchema.getEdorAcceptNo());
        if(!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("没有查询到受理信息。");
            return null;
        }
        return tLGWorkDB.getCustomerName();
    }

    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getListTable()
    {
//        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
//        tLPDiskImportDB.setEdorNo(mLPEdorAppSchema.getEdorAcceptNo());
//        tLPDiskImportDB.setEdorType("LP");
//        LPDiskImportSet tLPDiskImportSet = tLPDiskImportDB.query();
//
//        for (int i = 1; i <= tLPDiskImportSet.size(); i++)
//        {
//            getInsuredInfo(i,tLPDiskImportSet.get(i));
//        }
//
//        LBDiskImportDB tLBDiskImportDB = new LBDiskImportDB();
//        tLBDiskImportDB.setEdorNo(mLPEdorAppSchema.getEdorAcceptNo());
//        tLBDiskImportDB.setEdorType("LP");
//        LBDiskImportSet tLBDiskImportSet = tLBDiskImportDB.query();
//
//        for (int i = 1; i <= tLBDiskImportSet.size(); i++) {
//            getInsuredInfoFromLB(i, tLBDiskImportSet.get(i));
//        }
    	String tSQL= "select insuredNo,insuredname,codename('idtype',idtype),IDNo,"                               
	    	+ " (select bankname from ldbank where bankcode=LPDiskImport.bankcode fetch first 1 rows only),"
	    	+ " AccName,BankAccNo "                                                                          
	    	+ " from LPDiskImport where EdorNo='"+ mLPEdorAppSchema.getEdorAcceptNo() +"' and EdorType='LP' "
	    	+ " union "
	    	+ " select insuredNo,insuredname,codename('idtype',idtype),IDNo,"                               
	    	+ " (select bankname from ldbank where bankcode=LBDiskImport.bankcode fetch first 1 rows only),"
	    	+ " AccName,BankAccNo"                                                                          
	    	+ " from LBDiskImport where EdorNo='"+ mLPEdorAppSchema.getEdorAcceptNo() +"' and EdorType='LP' "
	    	+ " with ur";
    	SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSQL);
        for (int i=1;i<=tSSRS.getMaxRow();i++)
        {
        	String[] info = new String[8];
            info[0] = StrTool.cTrim(""+i);
            info[1] = StrTool.cTrim(tSSRS.GetText(i, 1));
            info[2] = StrTool.cTrim(tSSRS.GetText(i, 2));
            info[3] = StrTool.cTrim(tSSRS.GetText(i, 3));
            info[4] = StrTool.cTrim(tSSRS.GetText(i, 4));
            info[5] = StrTool.cTrim(tSSRS.GetText(i, 5));
            info[6] = StrTool.cTrim(tSSRS.GetText(i, 6));
            info[7] = StrTool.cTrim(tSSRS.GetText(i, 7));

            mtListTable.add(info);
        }

        mtListTable.setName("LP");
        return mtListTable;
    }
/*
    private void getInsuredInfo(int t,
                                LPDiskImportSchema tLPDiskImportSchema)
    {
        System.out.println("insured index:" + t);
        String[] info = new String[8];
        info[0] = "" + t;
        info[1] = tLPDiskImportSchema.getInsuredNo();
        info[2] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
        info[3] = mIdTypeQuery.getIdTypeName(tLPDiskImportSchema.getIDType());
        info[4] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
        info[5] = mBankCodeQuery.getBankName(tLPDiskImportSchema.getBankCode());
        info[6] = StrTool.cTrim(tLPDiskImportSchema.getAccName());
        info[7] = StrTool.cTrim(tLPDiskImportSchema.getBankAccNo());

        mtListTable.add(info);
    }

    private void getInsuredInfoFromLB(int t,
                                    LBDiskImportSchema tLBDiskImportSchema)
        {
            System.out.println("insured index:" + t);
            String[] info = new String[8];
            info[0] = "" + t;
            info[1] = tLBDiskImportSchema.getInsuredNo();
            info[2] = StrTool.cTrim(tLBDiskImportSchema.getInsuredName());
            info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName("IDType",
                    tLBDiskImportSchema.getIDType()));
            info[4] = StrTool.cTrim(tLBDiskImportSchema.getIDNo());
            info[5] = mBankCodeQuery.getBankName(tLBDiskImportSchema.getBankCode());
            info[6] = StrTool.cTrim(tLBDiskImportSchema.getAccName());
            info[7] = StrTool.cTrim(tLBDiskImportSchema.getBankAccNo());

            mtListTable.add(info);
    }
*/
    /**
     * 得到保全受理信息
     * @param edorAcceptNo String
     * @return LPEdorAppSchema
     */
    private LPEdorAppSchema getLPEdorAppInfo(String edorAcceptNo)
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(edorAcceptNo);
        if(!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("没有查询到保全受理信息。");
            return null;
        }

        return tLPEdorAppDB.getSchema();
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    private boolean checkData()
    {
        mLPEdorAppSchema = getLPEdorAppInfo(mLPEdorAppSchema.getEdorAcceptNo());
        if(mLPEdorAppSchema == null)
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PrtGrpInsuredListLPBL p = new PrtGrpInsuredListLPBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20051227000003");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("edorType", "CT");

        VData v = new VData();
        v.add(schema);
        v.add(tTransferData);
        if(!p.submitData(v, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
