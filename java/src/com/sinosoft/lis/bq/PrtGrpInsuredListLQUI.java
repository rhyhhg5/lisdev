package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 余额领取清单</p>
* <p>Description:余额领取清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListLQUI
{
    PrtGrpInsuredListLQBL mPrtGrpInsuredListLQBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListLQUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListLQBL = new PrtGrpInsuredListLQBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListLQBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListLQBL.getInputStream();
    }
}
