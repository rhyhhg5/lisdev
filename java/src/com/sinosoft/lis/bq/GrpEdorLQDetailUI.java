package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 提前领取</p>
 * <p>Description:提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorLQDetailUI
{
    private GrpEdorLQDetailBL mGrpEdorLQDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GrpEdorLQDetailUI(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        mGrpEdorLQDetailBL = new GrpEdorLQDetailBL(gi, edorNo, grpContNo, specialData);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGrpEdorLQDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGrpEdorLQDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorLQDetailBL.getMessage();
    }
}
