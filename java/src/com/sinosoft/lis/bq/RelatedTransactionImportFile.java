package com.sinosoft.lis.bq;

import java.util.HashMap;

import com.sinosoft.lis.db.LDRelatedPartyDB;
import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.vschema.LCBlackListSet;
import com.sinosoft.lis.vschema.LDRelatedPartySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

public class RelatedTransactionImportFile {

    public RelatedTransactionImportFile() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 从xls文件读入Sheet的开始行*/
    private static final int STARTROW = 2;
    /** 使用默认的导入方式 */
    private MultiSheetImporter importer;
    /** 错误处理 */
    public CErrors mErrors = new CErrors();
    /** Sheet Name */
    private String[] mSheetName;
    /** Schema 的名字 */
    private LDRelatedPartySet mLDRelatedPartySet;
    /** Sheet对应table的名字*/
    private static final String[] mTableNames = {"LDRelatedParty"};

    public RelatedTransactionImportFile(String fileName, String configFileName,
                         String[] sheetName) {
        mSheetName = sheetName;
        importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
      System.out.println(" Into---- doImport...");
        importer.setTableName(mTableNames);
        importer.setMStartRows(STARTROW);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "MultiSheetImporter";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public HashMap getResult() {
        return importer.getResult();
    }

    /**
     * 用于获取导入文件版本号
     * @return String
     */
    public String getVersion() {
        HashMap tHashMap = importer.getResult();
        String version = "";
        version = (String) tHashMap.get(mSheetName[1]);
        return version;
    }

    /**
     * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
     *
     * @return LMProtocolFeeRateSet
     */
    public LDRelatedPartySet getLDRelatedPartySet()
    {
    	LDRelatedPartySet tLDRelatedPartySet = new LDRelatedPartySet();
        HashMap tHashMap = importer.getResult();
        tLDRelatedPartySet.add((LDRelatedPartySet) tHashMap.get(mSheetName[0]));
        return tLDRelatedPartySet;
    }

    private void jbInit() throws Exception {
    }
}
