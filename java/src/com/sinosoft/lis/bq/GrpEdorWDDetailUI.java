package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: 保全无名单实名化删除</p>
 * <p>Description: 保全无名单实名化删除</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorWDDetailUI
{
    private GrpEdorWDDetailBL mGrpEdorWDDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GrpEdorWDDetailUI()
    {
        mGrpEdorWDDetailBL = new GrpEdorWDDetailBL();
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data, String operator)
    {
                    System.out.println("operator：" + operator);
        if (!mGrpEdorWDDetailBL.submitData(data, operator))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mGrpEdorWDDetailBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGrpEdorWDDetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }
}
