package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全处理财务数据</p>
 * <p>Description:把保全补退费数据放入财务接口</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorTADetailUI
{
    private GEdorTADetailBL mGEdorTADetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GEdorTADetailUI(GlobalInput gi, String edorNo, String grpContNo)
    {
        mGEdorTADetailBL = new GEdorTADetailBL(gi, edorNo, grpContNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGEdorTADetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGEdorTADetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGEdorTADetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ManageCom = "86";
        GEdorTADetailUI tGEdorTADetailUI = new GEdorTADetailUI(gi,
                "20051227000005", "0000031001");
        if (!tGEdorTADetailUI.submitData())
        {
            System.out.println(tGEdorTADetailUI.getError());
        }
    }
}
