package com.sinosoft.lis.newpeople;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 删除被保人清单中的数据</p>
 * <p>Description: 可以彻底删除也可以只设为无效状态 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class BqUpdatePolStateBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = null;

    /** 数据操作类型 */
    private String mOperate = null;

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();

    /** 修改清单 */
    private LCPolSet mLCPolSet = new LCPolSet();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public BqUpdatePolStateBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData())
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        System.out.println("Start BqUpdatePolStateBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "BqUpdatePolStateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCPolSet = (LCPolSet) mInputData.getObjectByObjectName(
                "LCPolSet", 0);

        return true;
    }


    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	if(!update()){
    		return false;
    	}
        return true;
    }

    private boolean update(){
    	//修改数据
        if (mOperate.equals("UPDATE"))
        {
        	for (int i = 1; i <= mLCPolSet.size(); i++)
            {
    			LCPolSchema tLCPolSchema = mLCPolSet.get(i);
    			String sql = "update LCPol set Polstate = '00019999' , "
        				+ " modifydate= '"+ mCurrentDate +"' ,modifytime= '"+ mCurrentTime +"'"
        				+ " where polno = '"+ tLCPolSchema.getContNo() +"'";
    			System.out.println(sql);
        		mMap.put(sql, "UPDATE");
            }
        	mInputData.add(mMap);
        }else{
        	return false;
        }
        return true;
    }
    /**
     * 根据业务逻辑对数据进行处理
     */
    private void prepareOutputData()
    {
        mInputData.add(mMap);
    }
}
