package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
  被保险人资料变更功能类
*/

public class PEdorBCDetailUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public PEdorBCDetailUI() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("Operate : "+cOperate);
    PEdorBCDetailBL tPEdorBCDetailBL = new PEdorBCDetailBL();
    System.out.println("   ----- UI BEGIN");
    if (tPEdorBCDetailBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPEdorBCDetailBL.mErrors);
      return false;
    }
    else
      mResult = tPEdorBCDetailBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    VData tInputData=new VData();
    GlobalInput tGlobalInput = new GlobalInput();
    PEdorBCDetailUI aPEdorBCDetailUI = new PEdorBCDetailUI();
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPBnfSet tLPBnfSet = new LPBnfSet();
    LPBnfSchema tLPBnfSchema = new LPBnfSchema();
    tGlobalInput.ManageCom="86";
    tGlobalInput.Operator="001";

    tLPEdorItemSchema.setPolNo("000000");
    tLPEdorItemSchema.setEdorNo("410000000000517");
    tLPEdorItemSchema.setEdorType("BC");
    tLPEdorItemSchema.setContNo("230110000000555");
    tLPEdorItemSchema.setInsuredNo("0000494300 ");
    tLPEdorItemSchema.setEdorAcceptNo("86000000000527");

    tLPBnfSchema.setBnfGrade("1");
    tLPBnfSchema.setBnfLot("1.0");
    tLPBnfSchema.setBnfType("0");
    tLPBnfSchema.setCustomerNo("");
    tLPBnfSchema.setEdorNo("410000000000517");
    tLPBnfSchema.setEdorType("BC");
    tLPBnfSchema.setIDNo("1415");
    tLPBnfSchema.setIDType("1");
    tLPBnfSchema.setName("cccc");
    tLPBnfSchema.setPolNo("210110000001136");
    tLPBnfSchema.setRelationToInsured("06");
    tLPBnfSet.add(tLPBnfSchema);

    tInputData.addElement(tLPBnfSet);
    tInputData.addElement(tLPEdorItemSchema);
    tInputData.addElement(tGlobalInput);
    aPEdorBCDetailUI.submitData(tInputData,"INSERT||PERSON");

    System.out.println("  ----- test ...");
  }
}
