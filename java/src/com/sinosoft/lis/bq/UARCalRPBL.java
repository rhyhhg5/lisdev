package com.sinosoft.lis.bq;


import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;




public class UARCalRPBL {
	
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private String tEndDate ="";
    private String tStartDate ="";
    private FDate tFDate = new FDate();
    private String tFeeFlag = "";
    private Reflections ref = new Reflections();
    private String mBalanceSeq = null;
    private String mDealFlag = null;
    private String mOtherno = null;

	 public boolean submitData(LCPolSchema aLCPolSchema,String tCurrentDate,String tFlag) 
	    {
		 	
		 			mDealFlag = tFlag;
		            String aPolNo = aLCPolSchema.getPolNo();
		            String aContNo = aLCPolSchema.getContNo();
		            String contType = aLCPolSchema.getContType();//保单类型（团、个）
		                                  
		           		            
		            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		            LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
		            tLCInsureAccClassDB.setContNo(aContNo);
		                       
		            LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
		            if (tLCInsureAccClassDB.mErrors.needDealError())
		            {
		                System.out.println(tLCInsureAccClassDB.mErrors.getErrContent());
		                mErrors.addOneError("查询月结信息时出错");
		                return false;
		            }
		            tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema(); 
		            if(tFDate.getDate(tLCInsureAccClassSchema.getBalaDate()).
		            		after(tFDate.getDate(tCurrentDate)))
		            {
//		            	tEndDate = PubFun.calDate(tLCInsureAccClassSchema.getBalaDate(), -1, "D", null);
//		            	tStartDate = tCurrentDate;
		            	tEndDate = tCurrentDate;
		            	tStartDate = tCurrentDate.substring(0,7)+"-01";
		            	tFeeFlag = "0";
		            }
		            else{
		            	tEndDate = tCurrentDate;
		            	tStartDate = tLCInsureAccClassSchema.getBalaDate();
		            	tFeeFlag = "1";
		            }
		           
		        		
			        //计算管理费
			        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			        tLMRiskFeeDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
			        tLMRiskFeeDB.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
			        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
			        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
			        tLMRiskFeeDB.setFeeItemType("07");
			        tLMRiskFeeDB.setFeeCode("000012");
			        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			        if (tLMRiskFeeDB.mErrors.needDealError())
			        {
			            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
			            mErrors.addOneError("查询管理费时出现异常");
			            return false;
			        }
		
			        
			        if (tEndDate == null||tStartDate==null)
			        {
			            mErrors.addOneError("获取万能重疾风险扣费起期止期错误");
			            return false;
			        }
		
			      
			       
		
			       
			        double fee = 0.00;
		
			        //计算管理费
			        for (int t = 1; t <= tLMRiskFeeSet.size(); t++)
			        {
			            Calculator tCalculator = new Calculator();
			            tCalculator.setCalCode(tLMRiskFeeSet.get(t).getFeeCalCode());
			           
			            tCalculator.addBasicFactor("EndDate", tEndDate);
			            tCalculator.addBasicFactor("StartDate", tStartDate);
			            tCalculator.addBasicFactor("Sex", aLCPolSchema.getInsuredSex());
			            String subRiskCode = ""; 
			            subRiskCode = tLMRiskFeeSet.get(t).getFeeNoti();
			            String tSubAmnt = String.valueOf(aLCPolSchema.getAmnt());
						tCalculator.addBasicFactor("SubAmnt",tSubAmnt);
						String sql = "select Rate from LCPrem where PolNo = '" + aLCPolSchema.getPolNo() 
									+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
						String tRiskRate = new ExeSQL().getOneValue(sql);
						if(tRiskRate==null || tRiskRate.equals(""))
						{
								tCalculator.addBasicFactor("RiskRate", "0");
							}
						else
							{
								tCalculator.addBasicFactor("RiskRate", tRiskRate);
							}
						 AccountManage tAccountManage = new AccountManage();
				            String tInsuredAppAge = tAccountManage
				                                    .getInsuredAppAge(tCurrentDate,
				                    tLCInsureAccClassSchema);
				            if (tInsuredAppAge == null)
				            {
				                mErrors.copyAllErrors(tAccountManage.mErrors);
				                return false;
				            }
				            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
				            fee = Math.abs(PubFun.setPrecision(
				                    Double.parseDouble(tCalculator.calculate()), "0.00"));
				            if (tCalculator.mErrors.needDealError())
				            {
				                System.out.println(tCalculator.mErrors.getErrContent());
				                mErrors.addOneError("计算管理费出错");
				                return false;
				            }
						}
			        	mOtherno = aLCPolSchema.getPolNo();
			        		        
			        
			        	 if("11".equals(mDealFlag)){
			        		 if("0".equals(tFeeFlag)){
			     	        	String sql = "select -sum(money) from lcinsureacctrace where PolNo = '" + tLCInsureAccClassSchema.getPolNo() 
			     				+ "' and PayPlanCode ='"+aLCPolSchema.getRiskCode()+"' and othertype='6' " +
			     						"  and moneytype='RP' and insuaccno='"+tLCInsureAccClassSchema.getInsuAccNo()+"' and paydate between '"+PubFun.calDate(tStartDate, 1, "M", null)+"' " +
			     						"and '"+tLCInsureAccClassSchema.getBalaDate()+"' " ;
			     				String tSumBlan = new ExeSQL().getOneValue(sql);
			     				if(tSumBlan!=null && !tSumBlan.equals(""))
			     				{
			     					fee = Double.parseDouble(tSumBlan) - fee;
			     				}
			        		 }
			        		 mMap.add(insertAccTrace(aLCPolSchema,tLCInsureAccClassSchema,fee));
			        	 }
					        	
			        	 
			        	 mMap.add(updateAccInfo(tLCInsureAccClassSchema));
			         
				        
			        
			        
			        
			        if (!SubmitMap()) 
		            {
		                return false;
		            }
			    
			    
		 	return true;
	    }
	 
	 public boolean submitDataP(LPPolSchema aLPPolSchema,LCPolSchema tLCPolSchema,String tCurrentDate,String tFlag) 
	    {
		 	
		 			mDealFlag = tFlag;
		            String aPolNo = aLPPolSchema.getPolNo();
		            String aContNo = aLPPolSchema.getContNo();
		            String contType = aLPPolSchema.getContType();//保单类型（团、个）
		                                  
		           		            
		            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		            LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
		            tLCInsureAccClassDB.setContNo(aContNo);
		                       
		            LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
		            if (tLCInsureAccClassDB.mErrors.needDealError())
		            {
		                System.out.println(tLCInsureAccClassDB.mErrors.getErrContent());
		                mErrors.addOneError("查询月结信息时出错");
		                return false;
		            }
		            tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1).getSchema(); 
		            
		            tStartDate = tCurrentDate;
		            tEndDate = tLCInsureAccClassSchema.getBalaDate();
//		            if(tFDate.getDate(tLCInsureAccClassSchema.getBalaDate()).
//		            		after(tFDate.getDate(tCurrentDate)))
//		            {
////		            	tEndDate = PubFun.calDate(tLCInsureAccClassSchema.getBalaDate(), -1, "D", null);
////		            	tStartDate = tCurrentDate;
////		            	tEndDate = tCurrentDate;
////		            	tStartDate = tCurrentDate.substring(0,7)+"-01";
//		            	tFeeFlag = "0";
//		            }
//		            else{
//		            	
//		            	tFeeFlag = "1";
//		            }
		        
		
			        //计算管理费
			        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			        tLMRiskFeeDB.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
			        tLMRiskFeeDB.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
			        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
			        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
			        tLMRiskFeeDB.setFeeItemType("07");
			        tLMRiskFeeDB.setFeeCode("000012");
			        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			        if (tLMRiskFeeDB.mErrors.needDealError())
			        {
			            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
			            mErrors.addOneError("查询管理费时出现异常");
			            return false;
			        }
		
			        
			        if (tEndDate == null||tStartDate==null)
			        {
			            mErrors.addOneError("获取万能重疾风险扣费起期止期错误");
			            return false;
			        }
		
			      
			       
		
			       
			        double fee = 0.00;
		
			        //计算管理费
			        for (int t = 1; t <= tLMRiskFeeSet.size(); t++)
			        {
			            Calculator tCalculator = new Calculator();
			            tCalculator.setCalCode(tLMRiskFeeSet.get(t).getFeeCalCode());
			           
			            tCalculator.addBasicFactor("EndDate", tEndDate);
			            tCalculator.addBasicFactor("StartDate", tStartDate);
			            tCalculator.addBasicFactor("Sex", aLPPolSchema.getInsuredSex());
			            String subRiskCode = ""; 
			            subRiskCode = tLMRiskFeeSet.get(t).getFeeNoti();
			            String tSubAmnt = String.valueOf(aLPPolSchema.getAmnt());
						tCalculator.addBasicFactor("SubAmnt",tSubAmnt);
						String sql = "select Rate from LCPrem where PolNo = '" + aLPPolSchema.getPolNo() 
									+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
						String tRiskRate = new ExeSQL().getOneValue(sql);
						if(tRiskRate==null || tRiskRate.equals(""))
						{
								tCalculator.addBasicFactor("RiskRate", "0");
							}
						else
							{
								tCalculator.addBasicFactor("RiskRate", tRiskRate);
							}
						 AccountManage tAccountManage = new AccountManage();
				            String tInsuredAppAge = tAccountManage
				                                    .getInsuredAppAge(tCurrentDate,
				                    tLCInsureAccClassSchema);
				            if (tInsuredAppAge == null)
				            {
				                mErrors.copyAllErrors(tAccountManage.mErrors);
				                return false;
				            }
				            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);
//				            仅计算风险保费的情况没必要用绝对值 20140110
				            fee = PubFun.setPrecision(
				                    Double.parseDouble(tCalculator.calculate()), "0.00");
				            if (tCalculator.mErrors.needDealError())
				            {
				                System.out.println(tCalculator.mErrors.getErrContent());
				                mErrors.addOneError("计算管理费出错");
				                return false;
				            }
						}
//			        风险保费是扣的负值 20140110
			        fee=-fee;
			        
			        mOtherno = aLPPolSchema.getEdorNo();
			        
			         if("12P".equals(mDealFlag)){
			        	mDealFlag = "12"; 
//			        	if("0".equals(tFeeFlag)){
//		     	        	String sql = "select -sum(money) from lcinsureacctrace where PolNo = '" + tLCInsureAccClassSchema.getPolNo() 
//		     				+ "' and PayPlanCode ='"+aLPPolSchema.getRiskCode()+"' and othertype='6' " +
//		     						"  and moneytype='RP' and insuaccno='"+tLCInsureAccClassSchema.getInsuAccNo()+"' and paydate between '"+PubFun.calDate(tStartDate, 1, "M", null)+"' " +
//		     						"and '"+tLCInsureAccClassSchema.getBalaDate()+"' " ;
//		     				String tSumBlan = new ExeSQL().getOneValue(sql);
//		     				if(tSumBlan!=null && !tSumBlan.equals(""))
//		     				{
//		     					fee = Double.parseDouble(tSumBlan) - fee;
//		     				}
//		        		 }
			        	mMap.add(insertAccTraceP(aLPPolSchema,tLCInsureAccClassSchema,fee));
			        	mMap.add(updateAccInfoP(aLPPolSchema,tLCInsureAccClassSchema));
			        }
			          else if("12".equals(mDealFlag)){
//			        	  if("0".equals(tFeeFlag)){
//			     	        	String sql = "select -sum(money) from lcinsureacctrace where PolNo = '" + tLCInsureAccClassSchema.getPolNo() 
//			     				+ "' and PayPlanCode ='"+aLPPolSchema.getRiskCode()+"' and othertype='6' " +
//			     						"  and moneytype='RP' and insuaccno='"+tLCInsureAccClassSchema.getInsuAccNo()+"' and paydate between '"+PubFun.calDate(tStartDate, 1, "M", null)+"' " +
//			     						"and '"+tLCInsureAccClassSchema.getBalaDate()+"' " ;
//			     				String tSumBlan = new ExeSQL().getOneValue(sql);
//			     				if(tSumBlan!=null && !tSumBlan.equals(""))
//			     				{
//			     					fee = Double.parseDouble(tSumBlan) - fee;
//			     				}
//			        		 }
//			        	  原始程序逻辑混乱+全局变量滥用,改起来及其费劲,为避免对附加重疾失效造成影响,只能新建方法
//						mMap.add(insertAccTrace(tLCPolSchema,tLCInsureAccClassSchema,-fee));
			        	mMap.add(insertAccTraceC(tLCPolSchema,tLCInsureAccClassSchema,fee));
						mMap.add(updateAccInfo(tLCInsureAccClassSchema));
			         }
				        
			        
			        
			        
			        if (!SubmitMap()) 
		            {
		                return false;
		            }
			    
			    
		 	return true;
	    }
	 /*数据处理*/
	    private boolean SubmitMap() {
	        PubSubmit tPubSubmit = new PubSubmit();
	        mResult.clear();
	        mResult.add(mMap);
	        if (!tPubSubmit.submitData(mResult, "")) 
	        {
	            System.out.println("提交数据失败！");
//	            return false;
	        }
	        this.mMap = new MMap();
	        return true;
	    }
	    
	    /**
	     * 得到结算序号
	     * @return String
	     */
	    public String getBalanceSequenceNo()
	    {
	        if (mBalanceSeq == null)
	        {
	            String[] dateArr = PubFun.getCurrentDate().split("-");
	            String date = dateArr[0] + dateArr[1] + dateArr[2];
	            mBalanceSeq = date
	                          + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
	        }

	        return mBalanceSeq;
	    }
	    
	    private MMap insertAccTrace(LCPolSchema aLCPolSchema,LCInsureAccClassSchema tLCInsureAccClassSchema,double fee){
	    	
	    	MMap tMMap = new MMap();
	    	LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
            String tLimit = PubFun.getNoLimit(tLCInsureAccClassSchema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
	        if("0".equals(tFeeFlag)){
	        	
	        	tLCInsureAccTraceSchema.setMoney(fee);
	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
	        	tLCInsureAccTraceSchema.setPayDate(tLCInsureAccClassSchema.getBalaDate());
	        }
	        else if("1".equals(tFeeFlag)){
	        	tLCInsureAccTraceSchema.setMoney(-fee);
	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null); //将其日期+1天
	        	tLCInsureAccTraceSchema.setPayDate(tEndDate);
	        }
	        tLCInsureAccTraceSchema.setMoneyType("RP");
	        tLCInsureAccTraceSchema.setGrpContNo(tLCInsureAccClassSchema.getGrpContNo());
	        tLCInsureAccTraceSchema.setGrpPolNo(tLCInsureAccClassSchema.getGrpPolNo());
	        tLCInsureAccTraceSchema.setContNo(tLCInsureAccClassSchema.getContNo());
	        tLCInsureAccTraceSchema.setPolNo(tLCInsureAccClassSchema.getPolNo());
	        tLCInsureAccTraceSchema.setSerialNo(serNo);
	        tLCInsureAccTraceSchema.setRiskCode(tLCInsureAccClassSchema.getRiskCode());
	        tLCInsureAccTraceSchema.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
	        tLCInsureAccTraceSchema.setPayPlanCode(aLCPolSchema.getRiskCode());
	        tLCInsureAccTraceSchema.setManageCom(tLCInsureAccClassSchema.getManageCom());
	        //tLCInsureAccTraceSchema.setOtherNo(getBalanceSequenceNo());
	        tLCInsureAccTraceSchema.setOtherNo(mOtherno);
	        tLCInsureAccTraceSchema.setOtherType(mDealFlag);//万能附加险失效标记
	        tLCInsureAccTraceSchema.setAccAscription(tLCInsureAccClassSchema.getAccAscription());
	        tLCInsureAccTraceSchema.setUnitCount(tLCInsureAccClassSchema.getUnitCount());
	        tLCInsureAccTraceSchema.setState(tLCInsureAccClassSchema.getState());
	        tLCInsureAccTraceSchema.setOperator("Server");
	        tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
	        tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
	        tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
	        tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
	        tMMap.put(tLCInsureAccTraceSchema, "DELETE&INSERT");
	        
	        return tMMap;
	    }
	    
	    
	    private MMap insertAccTraceC(LCPolSchema aLCPolSchema,LCInsureAccClassSchema tLCInsureAccClassSchema,double fee){
	    	
	    	MMap tMMap = new MMap();
	    	LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
            String tLimit = PubFun.getNoLimit(tLCInsureAccClassSchema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
//	        if("0".equals(tFeeFlag)){
	        	
	        	tLCInsureAccTraceSchema.setMoney(fee);
	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
	        	tLCInsureAccTraceSchema.setPayDate(tLCInsureAccClassSchema.getBalaDate());
//	        }
//	        else if("1".equals(tFeeFlag)){
//	        	tLCInsureAccTraceSchema.setMoney(-fee);
//	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null); //将其日期+1天
//	        	tLCInsureAccTraceSchema.setPayDate(tEndDate);
//	        }
	        tLCInsureAccTraceSchema.setMoneyType("RP");
	        tLCInsureAccTraceSchema.setGrpContNo(tLCInsureAccClassSchema.getGrpContNo());
	        tLCInsureAccTraceSchema.setGrpPolNo(tLCInsureAccClassSchema.getGrpPolNo());
	        tLCInsureAccTraceSchema.setContNo(tLCInsureAccClassSchema.getContNo());
	        tLCInsureAccTraceSchema.setPolNo(tLCInsureAccClassSchema.getPolNo());
	        tLCInsureAccTraceSchema.setSerialNo(serNo);
	        tLCInsureAccTraceSchema.setRiskCode(tLCInsureAccClassSchema.getRiskCode());
	        tLCInsureAccTraceSchema.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
	        tLCInsureAccTraceSchema.setPayPlanCode(aLCPolSchema.getRiskCode());
	        tLCInsureAccTraceSchema.setManageCom(tLCInsureAccClassSchema.getManageCom());
	        //tLCInsureAccTraceSchema.setOtherNo(getBalanceSequenceNo());
	        tLCInsureAccTraceSchema.setOtherNo(mOtherno);
	        tLCInsureAccTraceSchema.setOtherType(mDealFlag);//万能附加险失效标记
	        tLCInsureAccTraceSchema.setAccAscription(tLCInsureAccClassSchema.getAccAscription());
	        tLCInsureAccTraceSchema.setUnitCount(tLCInsureAccClassSchema.getUnitCount());
	        tLCInsureAccTraceSchema.setState(tLCInsureAccClassSchema.getState());
	        tLCInsureAccTraceSchema.setOperator("Server");
	        tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
	        tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
	        tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
	        tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
	        tMMap.put(tLCInsureAccTraceSchema, "DELETE&INSERT");
	        
	        return tMMap;
	    }
	    

	    private MMap insertAccTraceP(LPPolSchema aLPPolSchema,LCInsureAccClassSchema tLCInsureAccClassSchema,double fee){
	    	
	    	MMap tMMap = new MMap();
	    	LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
            String tLimit = PubFun.getNoLimit(tLCInsureAccClassSchema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
//	        if("0".equals(tFeeFlag)){
	        	
//	        	tLPInsureAccTraceSchema.setMoney(-fee);
	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null);
	        	tLPInsureAccTraceSchema.setPayDate(tLCInsureAccClassSchema.getBalaDate());
//	        }
//	        else if("1".equals(tFeeFlag)){
	        	tLPInsureAccTraceSchema.setMoney(fee);
//	        	//tEndDate = PubFun.calDate(tEndDate, 1, "D", null); //将其日期+1天
//	        	tLPInsureAccTraceSchema.setPayDate(tEndDate);
//	        }
	        tLPInsureAccTraceSchema.setEdorNo(aLPPolSchema.getEdorNo());
	        tLPInsureAccTraceSchema.setEdorType(aLPPolSchema.getEdorType());
	        tLPInsureAccTraceSchema.setMoneyType("RP");
	        tLPInsureAccTraceSchema.setGrpContNo(tLCInsureAccClassSchema.getGrpContNo());
	        tLPInsureAccTraceSchema.setGrpPolNo(tLCInsureAccClassSchema.getGrpPolNo());
	        tLPInsureAccTraceSchema.setContNo(tLCInsureAccClassSchema.getContNo());
	        tLPInsureAccTraceSchema.setPolNo(tLCInsureAccClassSchema.getPolNo());
	        tLPInsureAccTraceSchema.setSerialNo(serNo);
	        tLPInsureAccTraceSchema.setRiskCode(tLCInsureAccClassSchema.getRiskCode());
	        tLPInsureAccTraceSchema.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
	        tLPInsureAccTraceSchema.setPayPlanCode(aLPPolSchema.getRiskCode());
	        tLPInsureAccTraceSchema.setManageCom(tLCInsureAccClassSchema.getManageCom());
	        //tLPInsureAccTraceSchema.setOtherNo(getBalanceSequenceNo());
	        tLPInsureAccTraceSchema.setOtherNo(aLPPolSchema.getEdorNo());
	        tLPInsureAccTraceSchema.setOtherType(mDealFlag);//万能附加险失效标记
	        tLPInsureAccTraceSchema.setAccAscription(tLCInsureAccClassSchema.getAccAscription());
	        tLPInsureAccTraceSchema.setUnitCount(tLCInsureAccClassSchema.getUnitCount());
	        tLPInsureAccTraceSchema.setState(tLCInsureAccClassSchema.getState());
	        tLPInsureAccTraceSchema.setOperator("Server");
	        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
	        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
	        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
	        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
	        tMMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
	        
	        return tMMap;
	    }
	    
	    /**
	     * updateAccInfo
	     * 更新总账户信息
	     * @return MMap
	     */
	    private MMap updateAccInfo(LCInsureAccClassSchema tLCInsureAccClassSchema)
	    {
	        MMap tMMap = new MMap();

	        String sql = "update LCInsureAccClass a "
	                     + "set (InsuAccBala, LastAccBala) "
	                     + "   =(select sum(Money), "+tLCInsureAccClassSchema.getInsuAccBala()+" from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
	                     + "where PolNo = '"
	                     + tLCInsureAccClassSchema.getPolNo() + "' "
	                     + "   and InsuAccNo = '"
	                     + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
	        tMMap.put(sql, "UPDATE");
	        //更新账户信息
	        sql = "update LCInsureAcc a "
	              + "set (InsuAccBala, LastAccBala,Operator, ModifyDate, ModifyTime) "
	              + " =(select sum(InsuAccBala), "+tLCInsureAccClassSchema.getInsuAccBala()+", Operator, ModifyDate, ModifyTime " +
	              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
	              + " group by Operator, ModifyDate, ModifyTime) "
	              + "where PolNo = '"
	              + tLCInsureAccClassSchema.getPolNo() + "' "
	              + "   and InsuAccNo = '"
	              + tLCInsureAccClassSchema.getInsuAccNo() + "' ";
	        tMMap.put(sql, "UPDATE");
	        
	      
	        

	        return tMMap;
	    }
	    /**
	     * updateAccInfo
	     * 更新总账户信息P表
	     * @return MMap
	     */
	    private MMap updateAccInfoP(LPPolSchema aLPPolSchema,LCInsureAccClassSchema tLCInsureAccClassSchema)
	    {
	        MMap tMMap = new MMap();

	        String sql = "update LPInsureAccClass a "
	                     + "set (InsuAccBala, LastAccBala) "
	                     + "   =(select sum(Money), "+tLCInsureAccClassSchema.getInsuAccBala()+" from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
	                     + "where PolNo = '"
	                     + tLCInsureAccClassSchema.getPolNo() + "' "
	                     + "   and InsuAccNo = '"
	                     + tLCInsureAccClassSchema.getInsuAccNo() + "' and edorno='"+aLPPolSchema.getEdorNo()+"'";
	        tMMap.put(sql, "UPDATE");
	        //更新账户信息
	        sql = "update LPInsureAcc a "
	              + "set (InsuAccBala, LastAccBala,Operator, ModifyDate, ModifyTime) "
	              + " =(select sum(InsuAccBala), "+tLCInsureAccClassSchema.getInsuAccBala()+", Operator, ModifyDate, ModifyTime " +
	              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
	              + " group by Operator, ModifyDate, ModifyTime) "
	              + "where PolNo = '"
	              + tLCInsureAccClassSchema.getPolNo() + "' "
	              + "   and InsuAccNo = '"
	              + tLCInsureAccClassSchema.getInsuAccNo() + "' and edorno='"+aLPPolSchema.getEdorNo()+"' ";
	        tMMap.put(sql, "UPDATE");
	        
	      
	        

	        return tMMap;
	    }
}
