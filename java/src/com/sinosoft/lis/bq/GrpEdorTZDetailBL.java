package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: 保全团险新增被保人保存申请</p>
 * <p>Description: 把保全状态置为申请确认状态</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GrpEdorTZDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private Date mCValiDate = null;

    private Date mLastPayToDate = null;

    private Date mCurPayToDate = null;

    private String mMessage = "";

    private EdorItemSpecialData mSpecialData = null;

    private LCInsuredListSet mLCInsuredListSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        try
		{
			mGlobalInput = (GlobalInput) data.getObjectByObjectName(
					"GlobalInput", 0);
			LPGrpEdorItemSchema tLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data.
					getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mSpecialData = (EdorItemSpecialData)data.
					getObjectByObjectName("EdorItemSpecialData", 0);
            mEdorNo = tLPGrpEdorItemSchema.getEdorNo();
            mEdorType = tLPGrpEdorItemSchema.getEdorType();
            mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();
            LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
            tLCInsuredListDB.setEdorNo(mEdorNo);
            tLCInsuredListDB.setGrpContNo(mGrpContNo);
            tLCInsuredListDB.setState(BQ.IMPORTSTATE_FAIL);
            mLCInsuredListSet = tLCInsuredListDB.query();
            LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
            if (tLCGrpPolSet.size() == 0)
            {
                mErrors.addOneError("未找到团体险种信息！");
                return false;
            }
            String cValiDate = tLCGrpPolSet.get(1).getCValiDate();
            System.out.println("生效日期" + cValiDate);
            if (cValiDate == null)
            {
                mErrors.addOneError("未找到保单生效日期，请检保单数据！");
                return false;
            }
            this.mCValiDate = CommonBL.stringToDate(cValiDate);
            LJAPayGrpSchema tLJAPayGrpSchema =
                    CommonBL.getPayToDate(tLCGrpPolSet.get(1).getGrpPolNo());
            this.mLastPayToDate = CommonBL.stringToDate(tLJAPayGrpSchema.
                    getLastPayToDate());
            System.out.println(tLJAPayGrpSchema.getLastPayToDate());
            this.mCurPayToDate = CommonBL.stringToDate(tLJAPayGrpSchema.
                    getCurPayToDate());
            System.out.println(tLJAPayGrpSchema.getCurPayToDate());
		}
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据的合法性
     * @return boolean
     */
    private boolean checkData()
	{
        //检查缴费期间
        if (!checkPayToDate())
        {
            return false;
        }

        //检查保全生效日期
        if (!checkEdorValiDate())
        {
            return false;
        }

        //检查新增的被保人是否和已有客户重复
        if (!checkInsured())
        {
            return false;
        }
        //检查新增被保人的省份证号是否符合校验
        if(checkInsuredIdNo()){
        	return false;
        }

        if (!checkContPlan())
        {
            return false;
        }

        if (!checkHealthPol())
        {
            return false;
        }

        if (!checkLastPayToDate())
        {
            return false;
        }

        if (!checkBalIntv())
        {
            return false;
        }
		return true;
	}

    private boolean checkInsured()
    {
        return true;
    }

    private boolean checkContPlan()
    {
        return true;
    }

    /**
     * 检查特需医疗数据
     * @return boolean
     */
    private boolean checkSpecialRisk()
    {
        return true;
    }
    
    private boolean checkInsuredIdNo(){
        try
        {
            for (int i = 1; i <= mLCInsuredListSet.size(); i++)
            {
                LCInsuredListSchema tSchema = mLCInsuredListSet.
                        get(i);
                String errorInfo=PubFun.CheckIDNo(tSchema.getIDType(), tSchema.getIDNo(), tSchema.getBirthday(), tSchema.getSex());
                if (!"".equals(errorInfo) && null != errorInfo)
                {
                    mErrors.addOneError("被保人" + tSchema.getInsuredName()
                    		+ "的身份证信息录入有误，" + errorInfo);
                    return false;
                }

            }
        }
        catch (Exception ex)
        {
            mErrors.addOneError("被保人身份证信息校验出错！");
            ex.printStackTrace();
            return false;
        }
    	
    	return true ;
    }

    /**
     * 获得当前函件的序号
     * @return String
     */
    private String getSerialNumber()
    {
        //得到序号
        String sql = "select * from LGLetter " +
                "where EdorAcceptNo = '" + mEdorNo + "' " +
                "order by int(SerialNumber) desc ";
        LGLetterDB db = new LGLetterDB();
        LGLetterSet set = db.executeQuery(sql);
        if (set.size() == 0)
        {
            return "1";
        }
        int sn = Integer.parseInt(set.get(1).getSerialNumber());
        return String.valueOf(sn + 1);
    }

    /**
     * 检查是不是有健管的产品
     * @return boolean
     */
    private boolean checkHealthPol()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String appflag = tLCGrpPolSchema.getAppFlag();
            String riskCode = tLCGrpPolSchema.getRiskCode();
            if ((appflag != null) && (appflag.equals("1")) &&
                    (riskCode != null) && (riskCode.equals("170101")))
            {
                mMessage = "本单有" + riskCode + "险种，请打印内部流转单，发送健管人员\n";
                LGLetterSchema tLGLetterSchema = new LGLetterSchema();
                tLGLetterSchema.setEdorAcceptNo(mEdorNo);
                tLGLetterSchema.setSerialNumber(getSerialNumber());
                tLGLetterSchema.setLetterType("1"); //核保函件
                tLGLetterSchema.setLetterSubType("8"); //内部流转
                tLGLetterSchema.setState("0"); //待下发
                tLGLetterSchema.setOperator(mGlobalInput.Operator);
                tLGLetterSchema.setMakeDate(mCurrentDate);
                tLGLetterSchema.setMakeTime(mCurrentTime);
                tLGLetterSchema.setModifyDate(mCurrentDate);
                tLGLetterSchema.setModifyTime(mCurrentTime);
                mMap.put(tLGLetterSchema, "DELETE&INSERT");
                return true;
            }
        }
        return true;
    }

    /**
     * 检查交费期间
     * @return boolean
     */
    private boolean checkPayToDate()
    {
        String calType = mSpecialData.getEdorValue("CalType");
        if ((calType != null) && (calType.equals("1")))
        {
            LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
            for (int i = 1; i <= tLCGrpPolSet.size(); i++)
            {
                LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
                LJAPayGrpSchema tLJAPayGrpSchema =
                        CommonBL.getPayToDate(tLCGrpPolSchema.getGrpPolNo());
                String lastPayToDate = tLJAPayGrpSchema.getLastPayToDate();
                String curPayToDate = tLJAPayGrpSchema.getCurPayToDate();
                if ((lastPayToDate == null) || (lastPayToDate.equals("")))
                {
                    mErrors.addOneError("未找到交费起始日期!");
                    return false;
                }
                if ((curPayToDate == null) || (curPayToDate.equals("")))
                {
                    mErrors.addOneError("未找到交费截止日期!");
                    return false;
                }
                Date date = CommonBL.changeYear(lastPayToDate, 1);
                if (!date.equals(new FDate().getDate(curPayToDate)))
                {
                    mErrors.addOneError("险种" + tLCGrpPolSchema.getRiskCode() +
                            "的交费期间为非一年期，算费方式不能使用月份保费表!");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 检查保全生效日期
     * @return boolean
     */
    private boolean checkEdorValiDate()
    {
        try
        {
            for (int i = 1; i <= mLCInsuredListSet.size(); i++)
            {
                LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.
                        get(i);
                Date edorValiDate = CommonBL.stringToDate(tLCInsuredListSchema.
                        getEdorValiDate());
                if (edorValiDate.before(mCValiDate))
                {
                    mErrors.addOneError("被保人" + tLCInsuredListSchema.getInsuredName()
                            + "的保全生效日期不能早于保单生效日期！");
                    return false;
                }
                if (edorValiDate.after(mCurPayToDate))
                {
                    mErrors.addOneError("被保人" + tLCInsuredListSchema.getInsuredName()
                            + "保全生效日期不能晚于交至日期！");
                    return false;
                }
                //20080522 add by zhanggm 增加增人的生效日期的校验，增人的生效日期不能晚于保单满期日的24时
                if (edorValiDate.after(getCInValiDate()))
                {
                    mErrors.addOneError("被保人" + tLCInsuredListSchema.getInsuredName()
                            + "保全生效日期不能晚于合同终止日期！");
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            mErrors.addOneError("保全生效日期校验出错！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 生效日早于当期保费应交日提示导入保费
     * @return boolean
     */
    private boolean checkLastPayToDate()
    {
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.
                    get(i);
            Date edorValiDate = CommonBL.stringToDate(tLCInsuredListSchema.
                    getEdorValiDate());
            String edorPrem = tLCInsuredListSchema.getEdorPrem();
            if (edorValiDate.before(mLastPayToDate) &&
                    ((edorPrem == null) || (edorPrem.equals(""))))
            {
                mMessage += "被保人" + tLCInsuredListSchema.getInsuredName() +
                        "申请的生效日早于当期保费应交日，如有需要，请特殊处理。\n";
            }
        }
        return true;
    }

    /**
     * 检验结算频次
     * @return boolean
     */
    private boolean checkBalIntv()
    {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpBalPlanDB.getInfo())
        {
            return true;
        }
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到保全受理信息！");
            return false;
        }
        Date edorAppDate = CommonBL.stringToDate(tLPEdorAppDB.getEdorAppDate());
        if (edorAppDate == null)
        {
            mErrors.addOneError("未找到保全受理日期！");
            return false;
        }
        int balIntv = tLCGrpBalPlanDB.getBalIntv();
        System.out.println("balIntv" + balIntv);
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.
                    get(i);
            Date edorValiDate = CommonBL.stringToDate(tLCInsuredListSchema.
                    getEdorValiDate());
            if ((edorValiDate != null) && (edorValiDate.before(edorAppDate)))
            {
                int interval = PubFun.calInterval(edorValiDate, edorAppDate, "M");
                        System.out.println("interval" + interval);
                if (balIntv == 0)
                {
                    if (interval > 3)
                    {
                        mMessage += "被保人" + tLCInsuredListSchema.getInsuredName() +
                                "的保全生效日期超过保全受理日期三个月！\n";
                    }
                }
                else if ((balIntv == 1) || (balIntv == 3) || (balIntv == 12))
                {
                    if (interval > balIntv)
                    {
                        mMessage += "被保人" + tLCInsuredListSchema.getInsuredName() +
                                "生效日期与受理日期间隔超过保单服务提交周期！";
                    }
                }
            }
        }
        return true;
    }

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * @return boolean 如果在处理过程中出错，则返回false,否则返回true
	 */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
            if (!dealSpecialRisk())
            {
                return false;
            }
        setEdorState(BQ.EDORSTATE_INPUT);
        setSpecialData();
        return true;
    }

    private boolean dealSpecialRisk()
    {
        if (!checkSpecialRisk())
        {
            return false;
        }
        setSpecialPrem();
        return true;
    }

    /**
     * 设置特需保费
     * @return boolean
     */
    private void setSpecialPrem()
    {
        String createAccFlag = mSpecialData.getEdorValue("CreateAccFlag");
        String inputType = mSpecialData.getEdorValue("InputType");
        String prem = mSpecialData.getEdorValue("Prem");
        if (createAccFlag.equals("1") && inputType.equals("2")) //磁盘导入
        {
            if (!CommonBL.hasNotSpecialRisk(mGrpContNo)) //如果不含有非特需的一般险种
            {
                String sql = "update LCInsuredList " +
                        "set PublicAcc = decimal(EdorPrem), " +
                        "    CalRule = '" + BQ.CALRULE_IMPORT + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and GrpContNo = '" + mGrpContNo + "'";
                mMap.put(sql, "UPDATE");
            }
            return;
        }
        if (createAccFlag.equals("1") && inputType.equals("1")) //统一设置
        {
            if ((prem == null) || (prem.equals("")))
            {
                return ; // qulq 2007-4-1 16:33 可以不导入保费
//                prem = "0";
            }
            String sql = "update LCInsuredList " +
                    "set PublicAcc = " + Double.parseDouble(prem)+ ", " +
                    "    EdorPrem = '" + prem + "', " +
                    "    CalRule = '" + BQ.CALRULE_IMPORT + "' " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and GrpContNo = '" + mGrpContNo + "'";
            mMap.put(sql, "UPDATE");
        }
    }

	/**
	 * 把保全状态设为已申请状态
	 * @return boolean
	 */
	private void setEdorState(String edorState)
	{
		String sql;
		//更新main表
		sql = "update LPGrpEdorMain " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
				"where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");

		//更新item表
        sql = "update LPGrpEdorItem  " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + PubFun.getCurrentDate() + "', " +
                "    ModifyTime = '" + PubFun.getCurrentTime() + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 设置保全项目特殊数据
     */
    public void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
//  20080522 add by zhanggm 获取保单满期日
    private Date getCInValiDate()
    {
    	String tCInValiDate = null;
    	String sql = "select CInValiDate from LCGrpCont where GrpContNo = '" + mGrpContNo + "' with ur";
        tCInValiDate = new ExeSQL().getOneValue(sql);
        System.out.println("合同终止日期为：" + tCInValiDate);
    	return CommonBL.stringToDate(tCInValiDate);
    }
}
