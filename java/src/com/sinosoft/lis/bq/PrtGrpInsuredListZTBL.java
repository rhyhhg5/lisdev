package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.io.InputStream;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要被撤销的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtGrpInsuredListZTBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private LPEdorAppSchema mLPEdorAppSchema = null;
    private boolean isULI=false;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    ListTable mtListTable = new ListTable();
    private ExeSQL mExeSQL = new ExeSQL();
    private HashMap mIDType = new HashMap();
    private boolean isTDBC = false;

    public PrtGrpInsuredListZTBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }
    
    
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xmlexport.getInputStream();
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTransferData = (TransferData) cInputData.
                            getObjectByObjectName("TransferData", 0);
            mLPEdorAppSchema = (LPEdorAppSchema) cInputData.
                               getObjectByObjectName("LPEdorAppSchema", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例

        //判断是普通险种解约还是团险万能解约
        jugdeUliByGrpContNo();
        if(this.isULI){
        	xmlexport.createDocument("PrtGrpInsuredListULIZT.vts", "printer"); //最好紧接着就初始化xml文档
        	
        	//得到公司名
        	tag.add("GrpName", getCompanyName());
        	String edorType = (String) mTransferData.getValueByName("edorType");
        	tag.add("EdorName", CommonBL.getEdorInfo(edorType, "G").getEdorName());
        	tag.add("EdorNo", mLPEdorAppSchema.getEdorAcceptNo());
        	xmlexport.addTextTag(tag);
        	//打印团险万能清单
        	ListTable tListTableULI = getULIListTable();
        	if (tListTableULI == null)
        	{
        		return false;
        	}
        	String[] titleULI = {"序号", "被保险人姓名", "证件号码", "客户号码", "解约时个人账户总金额",
        			"个人账户个人交费金额", "个人账户单位交费金额", "解约管理费合计", "个人交费解约管理费",
        			"单位交费解约管理费", "合计退费","个人交费部分退费","单位交费部分退费"};
        	xmlexport.addListTable(tListTableULI, titleULI);
        	
        	
        }else if(isTDBC){//补充团体医疗保单被保人清单
        	xmlexport.createDocument("PrtGrpInsuredListTDBCZT.vts", "printer"); //最好紧接着就初始化xml文档
        	
        	//得到公司名
        	tag.add("GrpName", getCompanyName());
        	String edorType = (String) mTransferData.getValueByName("edorType");
        	tag.add("EdorName", CommonBL.getEdorInfo(edorType, "G").getEdorName());
        	tag.add("EdorNo", mLPEdorAppSchema.getEdorAcceptNo());
        	xmlexport.addTextTag(tag);
        	ListTable tListTable = getListTableTDBC();
        	if (tListTable == null)
        	{
        		return false;
        	}
        	String[] title = {"序号", "姓名", "客户号", "性别", "出生日期",
        			"证件类型", "证件号码", "保险计划", "解约时保险金额(元)","退还保险金额(元)"};
        	xmlexport.addListTable(tListTable, title);
        }else{
        	xmlexport.createDocument("PrtGrpInsuredListZT.vts", "printer"); //最好紧接着就初始化xml文档
        	
        	//得到公司名
        	tag.add("GrpName", getCompanyName());
        	String edorType = (String) mTransferData.getValueByName("edorType");
        	tag.add("EdorName", CommonBL.getEdorInfo(edorType, "G").getEdorName());
        	tag.add("EdorNo", mLPEdorAppSchema.getEdorAcceptNo());
        	xmlexport.addTextTag(tag);
        	ListTable tListTable = getListTable();
        	if (tListTable == null)
        	{
        		return false;
        	}
        	String[] title = {"序号", "姓名", "客户号", "性别", "出生日期",
        			"证件类型", "证件号码", "保险计划", "期交保费",
        			"变更生效日期", "个人账户余额", "退还账户余额",
        			"其他险种退费", "本次应退保费","其他号码","备注"};
        	xmlexport.addListTable(tListTable, title);
        }

//        xmlexport.outputDocumentToFile("C:\\", "PrtGrpInsuredListZTBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    //查询公司名称
    private String getCompanyName()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPEdorAppSchema.getEdorAcceptNo());
        if(!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("没有查询到受理信息。");
            return null;
        }
        return tLGWorkDB.getCustomerName();
    }

    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getListTable()
    {
        //通过LPEdorMain得到保单号，然后得到公司名称
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();

        queryIDType();

        for (int i = 1; i <= tLPGrpEdorMainSet.size(); i++)
        {
            //查询团单下的个人保单信息
            StringBuffer sql = new StringBuffer();
            sql.append("select * from LPInsured ")
                .append("where edorNo = '")
                .append(tLPGrpEdorMainSet.get(i).getEdorNo())
                .append("'  and edorType = '")
                .append((String) mTransferData.getValueByName("edorType"))
                .append("'  and grpContNo = '")
                .append(tLPGrpEdorMainSet.get(i).getGrpContNo()).append("'")
                .append("order by name ");
            System.out.println(sql.toString());
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.executeQuery(sql.toString());

            //账户险种余额返还方式
            String accPayType = getAccPayType(tLPGrpEdorMainSet.get(i));
            String payWay = "";
            //团险公共账户
            LPInsuredSet publicAccInsuredSet = getPublicAccInsured(
                tLPGrpEdorMainSet.get(i).getGrpContNo());
            for (int t = 1; t <= tLPInsuredSet.size(); t++)
            {
                //若该被保人有特许险种,则显示帐户余额的退还方式
                String riskInfo = "  select b.* "
                                  + "from LPPol a, LMRiskApp b "
                                  + "where a.riskCode = b.riskCode "
                                  + "   and a.edorNo = '"
                                  + tLPInsuredSet.get(t).getEdorNo()
                                  + "'  and a.edorType = '"
                                  + tLPInsuredSet.get(t).getEdorType()
                                  + "'  and a.contNo = '"
                                  + tLPInsuredSet.get(t).getContNo()
                                  + "'  and a.insuredNo = '"
                                  + tLPInsuredSet.get(t).getInsuredNo()
                                  + "' ";
                System.out.println(riskInfo);
                LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.executeQuery(riskInfo);

                for (int count = 1; count <= tLMRiskAppSet.size(); count++)
                {
                    if(tLMRiskAppSet.get(count).getRiskType3()
                       .equals(BQ.RISKTYPE3_SPECIAL))
                    {
                        payWay = accPayType;
                    }
                }

                boolean isNotPublicAcc = true;
                for (int temp = 1; temp <= publicAccInsuredSet.size(); temp++)
                {
                    if (tLPInsuredSet.get(t).getContNo()
                        .equals(publicAccInsuredSet.get(temp).getContNo()))
                    {
                        isNotPublicAcc = false;
                        tLPInsuredSet.removeRange(t, t);
                        t--;
                        break;
                    }
                }
                if (isNotPublicAcc)
                {
                    getInsuredInfo(t, tLPInsuredSet.get(t), payWay,
                                   BQ.POLTYPEFLAG_INSURED);
                }
            }

            //公共账户
            for (int t = 1; t <= publicAccInsuredSet.size(); t++)
            {
            	String riskcode = new ExeSQL().getOneValue("select riskcode from lcgrppol where grpcontno = '"+tLPGrpEdorMainSet.get(1).getGrpContNo()+"'");
            	if(riskcode!=null && !"".equals(riskcode) && "370301".equals(riskcode) ){
            		getInsuredInfo(tLPInsuredSet.size() + t,
            				publicAccInsuredSet.get(t),
            				payWay,
            				BQ.POLTYPEFLAG_PUBLIC);
            	}
            }
        }
        mtListTable.setName("ZT");
        return mtListTable;
    }
    
    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getListTableTDBC()
    {
        //通过LPEdorMain得到保单号，然后得到公司名称
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();

        queryIDType();

        for (int i = 1; i <= tLPGrpEdorMainSet.size(); i++)
        {
            //查询团单下的个人保单信息
            StringBuffer sql = new StringBuffer();
            sql.append("select * from LPInsured ")
                .append("where edorNo = '")
                .append(tLPGrpEdorMainSet.get(i).getEdorNo())
                .append("'  and edorType = '")
                .append((String) mTransferData.getValueByName("edorType"))
                .append("'  and grpContNo = '")
                .append(tLPGrpEdorMainSet.get(i).getGrpContNo()).append("'")
                .append("order by name ");
            System.out.println(sql.toString());
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.executeQuery(sql.toString());

            for (int t = 1; t <= tLPInsuredSet.size(); t++)
            {
            	LPInsuredSchema tLPInsuredSchema=tLPInsuredSet.get(t);
            	System.out.println("insured index:" + t);
                String[] info = new String[10];
                info[0] = "" + t;
                info[1] = "" + StrTool.cTrim(tLPInsuredSchema.getName());
                info[2] = tLPInsuredSchema.getInsuredNo();
                info[3] = StrTool.cTrim(getSex(tLPInsuredSchema.getSex()));
                info[4] = StrTool.cTrim(tLPInsuredSchema.getBirthday());
                info[5] = StrTool.cTrim(getIDType(tLPInsuredSchema.getIDType()));
                info[6] = StrTool.cTrim(tLPInsuredSchema.getIDNo());
                info[7] = StrTool.cTrim(tLPInsuredSchema.getContPlanCode());
                double getMoney = getGetMoney(tLPInsuredSchema);
                info[8] = String.valueOf(PubFun.setPrecision(getMoney, "0.00"));
                info[9] = String.valueOf(PubFun.setPrecision(getMoney, "0.00"));
                mtListTable.add(info);
            }

        }
        mtListTable.setName("ZT");
        return mtListTable;
    }
    
    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getULIListTable()
    {

    	//查询万能险种及团单号
    	String queryRiskInfo = " select * from lppol where grpcontno = ( " +
    			" select grpcontno from lpgrpedoritem  where edoracceptno='"+mLPEdorAppSchema.getEdorAcceptNo()+"'  ) " +
    					" and poltypeflag='2' and edorno= '"+mLPEdorAppSchema.getEdorAcceptNo()+"'";
    	LPPolDB tLPPolDB = new LPPolDB();
    	LPPolSet tLPPolSet = new LPPolSet();
    	LPPolSchema tLPPolSchema = new LPPolSchema();
    	tLPPolSet = tLPPolDB.executeQuery(queryRiskInfo);
    	if(tLPPolSet!=null&&tLPPolSet.size()>0){
    		tLPPolSchema = tLPPolSet.get(1);
    	}else{
    		return null;
    	}

        //公共账户账户编码
        String inGsuaccno = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='3')) ");
      //获取个人账户单位部分保险账户号码
        String tGrpInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')) ");
        //个人账户
        String tInsuNo =  new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");

    	String queryULIInfo = " select a.NAME,a.IDNO,a.INSUREDNO,b.sumGMoney,b.sumPMoney,b.sumGRPMoney, " +
    			" b.sumTMGMoney,b.sumTMPMoney,b.sumTMGRPMoney,b.sumCTGMoney,b.sumCTPMoney,b.sumCTGRPMoney " +
    			" from LPINSURED a,(SELECT c.insuredno, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE IN ('TM','CT') and otherno=c.edorno and insuaccno in ('"+tGrpInsuNo+"','"+tInsuNo+"') then money else 0 end) " +
    			" from lpinsureacctrace where polno=c.polno and edorno=c.edorno ),0) AS sumGMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE IN ('TM','CT') and otherno=c.edorno and insuaccno='"+tInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumPMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE IN ('TM','CT') and otherno=c.edorno and insuaccno='"+tGrpInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumGRPMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='TM' and otherno=c.edorno and insuaccno in ('"+tGrpInsuNo+"','"+tInsuNo+"') then money else 0 end ) " +
    			" from lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumTMGMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='TM' and otherno=c.edorno and insuaccno='"+tInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumTMPMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='TM' and otherno=c.edorno and insuaccno='"+tGrpInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumTMGRPMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='CT' and otherno=c.edorno and insuaccno in ('"+tGrpInsuNo+"','"+tInsuNo+"') then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumCTGMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='CT' and otherno=c.edorno and insuaccno='"+tInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumCTPMoney, " +
    			" nvl((select sum(CASE WHEN MONEYTYPE='CT' and otherno=c.edorno and insuaccno='"+tGrpInsuNo+"' then money else 0 end) " +
    			" from  lpinsureacctrace where polno=c.polno and edorno=c.edorno),0) AS sumCTGRPMoney " +
    			" FROM LPINSUREACC c WHERE EDORNO='"+mLPEdorAppSchema.getEdorAcceptNo()+"' " +
    			" and polno!=(select polno from lppol where grpcontno='"+tLPPolSchema.getGrpContNo()+"' and poltypeflag='2' and edorno='"+mLPEdorAppSchema.getEdorAcceptNo()+"' ) " +
    			" group by edorno,polno,insuredno) b " +
    			" where a.insuredno=b.insuredno and a.EDORNO='"+mLPEdorAppSchema.getEdorAcceptNo()+"' and a.grpcontno = '"+tLPPolSchema.getGrpContNo()+"' WITH UR ";

    	SSRS tSSRS = mExeSQL.execSQL(queryULIInfo);
    	for(int i=1;i<=tSSRS.getMaxRow();i++){
    		String[] info = new String[13];
    		info[0] = i+"";
    		info[1] = tSSRS.GetText(i,1);
    		info[2] = tSSRS.GetText(i,2);
    		info[3] = tSSRS.GetText(i,3);
    		info[4] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,4)), "0.00")));
    		info[5] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,5)), "0.00")));
    		info[6] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,6)), "0.00")));
    		info[7] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,7)), "0.00")));
    		info[8] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,8)), "0.00")));
    		info[9] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,9)), "0.00")));
    		info[10] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,10)), "0.00")));
    		info[11] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,11)), "0.00")));
    		info[12] = Double.toString(Math.abs(PubFun.setPrecision(Double.parseDouble(tSSRS.GetText(i,12)), "0.00")));
    		mtListTable.add(info);
    		mtListTable.setName("ZT");
    	}
        return mtListTable;
    }    
    
    
    

    private void getInsuredInfo(int t,
                                LPInsuredSchema tLPInsuredSchema,
                                String accPayType,
                                String insuredType)
    {
        System.out.println("insured index:" + t);
        String[] info = new String[16];
        info[0] = "" + t;
        if(insuredType.equals(BQ.POLTYPEFLAG_PUBLIC))
        {
            for(int i = 1; i < info.length - 2; i++)
            {
                info[i] = "";
            }
            info[1] = StrTool.cTrim(tLPInsuredSchema.getName());
            info[13] = String.valueOf(PubFun.setPrecision(
                getGetMoney(tLPInsuredSchema), "0.00"));
            info[14] = accPayType + getRemark(tLPInsuredSchema);
        }
        else
        {
            info[1] = StrTool.cTrim(tLPInsuredSchema.getName());
            info[2] = tLPInsuredSchema.getInsuredNo();
            info[3] = StrTool.cTrim(getSex(tLPInsuredSchema.getSex()));
            info[4] = StrTool.cTrim(tLPInsuredSchema.getBirthday());
            info[5] = StrTool.cTrim(getIDType(tLPInsuredSchema.getIDType()));
            info[6] = StrTool.cTrim(tLPInsuredSchema.getIDNo());
            info[7] = StrTool.cTrim(tLPInsuredSchema.getContPlanCode());
            info[8] = StrTool.cTrim(getSumPrem(tLPInsuredSchema));
            info[9] = getEdorValidate(tLPInsuredSchema);
            info[10] = StrTool.cTrim(getInsureAccBala(tLPInsuredSchema.
                getContNo(),
                tLPInsuredSchema.getInsuredNo()));
            double getMoney = getGetMoney(tLPInsuredSchema);
            double insureAccBack = getInsureAccBack(tLPInsuredSchema);
            info[11] = String.valueOf(PubFun.setPrecision(insureAccBack, "0.00"));
            info[12] = String.valueOf(PubFun.setPrecision(
                getMoney - insureAccBack, "0.00"));
            info[13] = String.valueOf(PubFun.setPrecision(getMoney, "0.00"));
            info[14] = StrTool.cTrim(getOthIDNo(tLPInsuredSchema));
            info[15] =  (hasEspecalPol(tLPInsuredSchema) ? accPayType : "")
                        + getRemark(tLPInsuredSchema);
        }
        mtListTable.add(info);
    }

    private String getSex(String sex)
    {
        if(sex.equals("0"))
        {
            return "男";
        }
        else if(sex.equals("1"))
        {
            return "女";
        }
        else
        {
            return "其他";
        }
    }

    private String getOthIDNo(LPInsuredSchema tLPInsuredSchema)
    {
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPDiskImportDB.setEdorType(tLPInsuredSchema.getEdorType());
        tLPDiskImportDB.setGrpContNo(tLPInsuredSchema.getGrpContNo());
        tLPDiskImportDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
        LPDiskImportSet set = tLPDiskImportDB.query();
        if(set.size() == 0)
        {
            return "";
        }
        return set.get(1).getOthIDNo();
    }

    private void queryIDType()
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("idtype");
        LDCodeSet set = tLDCodeDB.query();
        for(int i = 1; i <= set.size(); i++)
        {
            mIDType.put(set.get(i).getCode(), set.get(i).getCodeName());
        }
    }

    private String getIDType(String code)
    {
        Object o = mIDType.get(code);
        if(o == null)
        {
            return "";
        }
        else
        {
            return String.valueOf(o);
        }
    }

    private boolean hasEspecalPol(LPInsuredSchema tLPInsuredSchema)
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPPolDB.setEdorType(tLPInsuredSchema.getEdorType());
        tLPPolDB.setContNo(tLPInsuredSchema.getContNo());
        tLPPolDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
        tLPPolDB.setRiskCode("1605");
        LPPolSet set = tLPPolDB.query();
        if(set.size() > 0)
        {
            return true;
        }
        return false;
    }

    private LPInsuredSet getPublicAccInsured(String grpContNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct a.* from LPInsured a, LPPol b ")
            .append("where a.grpContNo = b.grpContNo ")
            .append("   and a.edorNo = b.edorNo ")
            .append("   and a.edorType = b.edorType ")
            .append("   and a.insuredNo = b.insuredNo ")
            .append("  and a.edorNo = '")
            .append(mLPEdorAppSchema.getEdorAcceptNo())
            .append("'  and a.edorType = '")
            .append((String) mTransferData.getValueByName("edorType"))
            .append("'   and a.grpContNo = '").append(grpContNo)
            .append("'  and b.polTypeFlag = '").append(BQ.POLTYPEFLAG_PUBLIC)
            .append("'  ");
        System.out.println(sql.toString());
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.executeQuery(sql.toString());

        return tLPInsuredSet;
    }

    /**
     * 特需险种应退余额
     * @param tLPInsuredSchema LPInsuredSchema
     * @return String
     */
    private double getInsureAccBack(LPInsuredSchema tLPInsuredSchema)
    {
        String edorState = mLPEdorAppSchema.getEdorState();
        StringBuffer sql = new StringBuffer();
        sql.append("  select round(sum(a.getMoney), 2) ")
            .append("from ").append((edorState.equals(BQ.EDORSTATE_CONFIRM)
                                     ? "LJAGetEndorse " : "LJSGetEndorse "))
            .append("a, LMRiskApp b ")
            .append("where a.riskCode = b.riskCode ")
            .append("   and a.endorsementNo = '")
            .append(mLPEdorAppSchema.getEdorAcceptNo())
            .append("'  and a.feeOperationType = '")
            .append((String) mTransferData.getValueByName("edorType"))
            .append("'  and insuredNo = '")
            .append(tLPInsuredSchema.getInsuredNo())
            .append("'  and riskType3 = '").append(BQ.RISKTYPE3_SPECIAL)
            .append("' ");
        ExeSQL tExeSQL = new ExeSQL();
        String sumGetMoney = StrTool.cTrim(tExeSQL.getOneValue(sql.toString()));

        return sumGetMoney.equals("")
            ? 0 : Math.abs(Double.parseDouble(sumGetMoney));
    }

    private String getOtherPolGetMoney(double insureAccBack)
    {
        return "";
    }

    /**
     * 得到被保人的期交保费之和
     * @param tLPInsuredSchema LPInsuredSchema
     * @return String
     */
    public String getSumPrem(LPInsuredSchema tLPInsuredSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select sum(prem) ")
            .append("from LPPol ")
            .append("where edorNo = '").append(tLPInsuredSchema.getEdorNo())
            .append("'  and edorType = '").append(tLPInsuredSchema.getEdorType())
            .append("'  and contNo = '").append(tLPInsuredSchema.getContNo())
            .append("'  and insuredNo = '")
            .append(tLPInsuredSchema.getInsuredNo()).append("' ");
        ExeSQL tExeSQL = new ExeSQL();
        String sumPrem = tExeSQL.getOneValue(sql.toString());
        return sumPrem;
    }

    /**
     * 得到保全生效日期
     * @param tLPInsuredSchema LPInsuredSchema
     * @return String
     */
    private String getEdorValidate(LPInsuredSchema tLPInsuredSchema)
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPEdorItemDB.setContNo(tLPInsuredSchema.getContNo());
        tLPEdorItemDB.setEdorType(tLPInsuredSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

        return StrTool.cTrim(tLPEdorItemSet.get(1).getEdorValiDate());
    }
    
    
    

    /**
     * 得到被保人的账户余额
     * @param insuredNo String
     * @return String
     */
    private String getInsureAccBala(String contNo,
                                    String insuredNo)
    {

        String sql = null;
        if(mLPEdorAppSchema.getEdorState().equals(BQ.EDORSTATE_CONFIRM))
        {
            sql = "  select sum(insuAccBala) "
                  + "from LBInsureAcc "
                  + "where edorNo = '"
                  + mLPEdorAppSchema.getEdorAcceptNo() + "' "
                  + "    and contNo = '" + contNo + "' "
                  + "    and insuredNo = '" + insuredNo + "' ";
        }
        else
        {
            sql = " select sum(insuAccBala) "
                  + "from LCInsureAcc "
                  + "where contNo = '" + contNo + "' "
                  + "    and insuredNo = '" + insuredNo + "' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String insureAccBala = tExeSQL.getOneValue(sql);
        return insureAccBala == null ? "" : insureAccBala;
    }

    //查询投保人的保险计划
    private String getContPlanCode(String contNo, String insuredNo)
    {
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mLPEdorAppSchema.getEdorAcceptNo());
        tLPInsuredDB.setEdorType((String) mTransferData.getValueByName("edorType"));
        tLPInsuredDB.setContNo(contNo);
        tLPInsuredDB.setInsuredNo(insuredNo);
        if(!tLPInsuredDB.getInfo())
        {
            return "";
        }
        return tLPInsuredDB.getContPlanCode();
    }

    /**
     * 查询该保单的退费
     * @param schema LCContSchema
     * @return String
     */
    private double getGetMoney(LPInsuredSchema schema)
    {
//        String tableName = " LJSGetEndorse ";
//        if(mLPEdorAppSchema.getEdorState().equals(BQ.EDORSTATE_CONFIRM))
//        {
//            tableName = " LJAGetEndorse ";
//        }
//        String sql = "  select sum(getMoney) "
//                     + "from " + tableName
//                     + "where endorsementNo = '"
//                     + mLPEdorAppSchema.getEdorAcceptNo()
//                     + "'  and feeOperationType = '" + schema.getEdorType()
//                     + "'  and contNo = '" + schema.getContNo()
//                     + "'  and insuredNo = '" + schema.getInsuredNo() + "' ";

        String sql = "  select sum(money) from "
                     + "(select sum(getMoney) money "
                     + "from LJSGetEndorse "
                     + "where endorsementNo = '"
                     + mLPEdorAppSchema.getEdorAcceptNo()
                     + "'  and feeOperationType = '" + schema.getEdorType()
                     + "'  and contNo = '" + schema.getContNo()
                     + "'  and insuredNo = '" + schema.getInsuredNo() + "' "
                     + "union all "
                     + "select sum(getMoney) money "
                     + "from LJAGetEndorse "
                     + "where endorsementNo = '"
                     + mLPEdorAppSchema.getEdorAcceptNo()
                     + "'  and feeOperationType = '" + schema.getEdorType()
                     + "'  and contNo = '" + schema.getContNo()
                     + "'  and insuredNo = '" + schema.getInsuredNo() + "') t ";

        String getMoney = mExeSQL.getOneValue(sql);
        if(getMoney.equals("") || getMoney.equals("null"))
        {
            return 0;
        }

        return Math.abs(Double.parseDouble(getMoney));
    }

    /**
     * 生成对一条被保人清单的记录的备注
     * @param schema Schema
     * @return String
     */
    private String getRemark(LPInsuredSchema tLPInsuredSchema)
    {
        String claimed = checkLCClaimed(tLPInsuredSchema);
        return claimed;
    }

    /**
     * 该被保人发生理赔的信息
     * @param tLPInsuredSchema LPInsuredSchema
     * @return String
     */
    private String checkLCClaimed(LPInsuredSchema tLPInsuredSchema)
    {
        StringBuffer polNos = new StringBuffer();

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPPolDB.setEdorType(tLPInsuredSchema.getEdorType());
        tLPPolDB.setContNo(tLPInsuredSchema.getContNo());
        tLPPolDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        for(int i = 1; i <= tLPPolSet.size(); i++)
        {
             polNos.append("'" + tLPPolSet.get(i).getPolNo() + "', ");
        }

        //查询这些险种是否发生过理赔
        if(polNos.toString().equals(""))
        {
            return "";
        }
        String polNos2 = polNos.substring(0, polNos.length() - 2); //去掉最后一个，号
        String sql = "select distinct riskCode "
                     + "from LLClaimPolicy "
                     + "where polNo in (" + polNos2 + ")"; ;
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS == null)
        {
            return "";
        }

        polNos2 = "";
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            polNos2 += tSSRS.GetText(1, 1) + ",";
        }

        return polNos2.equals("") ? polNos2 : "险种" + polNos2 + "发生理赔";
    }

    /**
     * 账户余额退还方式
     * @param tLPGrpEdorMainSchema LPGrpEdorMainSchema
     * @return String
     */
    private String getAccPayType(LPGrpEdorMainSchema tLPGrpEdorMainSchema)
    {
        String edorType = (String) mTransferData.getValueByName("edorType");
        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(tLPGrpEdorMainSchema.getEdorNo(), edorType);
        tEdorItemSpecialData.query();
        String balaPayType = tEdorItemSpecialData.getEdorValue("BALAPAYTYP");
        balaPayType = balaPayType == null ? "" : balaPayType;
        return balaPayType.equals("1") ? "账户余额转入团体账户 " : "";
    }

    /**
     * 得到保全受理信息
     * @param edorAcceptNo String
     * @return LPEdorAppSchema
     */
    private LPEdorAppSchema getLPEdorAppInfo(String edorAcceptNo)
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(edorAcceptNo);
        if(!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("没有查询到保全受理信息。");
            return null;
        }

        return tLPEdorAppDB.getSchema();
    }
    
    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = ");
        tSBql2.append(" (select max(distinct grpcontno) from lpgrpedoritem where edoracceptno='"+mLPEdorAppSchema.getEdorAcceptNo()+"') ");
        tSBql2.append(" and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G') and riskcode <> '370301'");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
        	StringBuffer tSBql3 = new StringBuffer(256);
        	tSBql3.append("select 1 from lbgrppol where GrpContNo = ");
        	tSBql3.append(" (select max(distinct grpcontno) from lpgrpedoritem where edoracceptno='"+mLPEdorAppSchema.getEdorAcceptNo()+"') ");
        	tSBql3.append(" and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G') and riskcode <> '370301'");
        	ExeSQL tExeSQL6 = new ExeSQL();
        	SSRS tSSRS6 = tExeSQL6.execSQL(tSBql3.toString());
        	
            if (tSSRS6.getMaxRow() < 1)
            {
	        	System.out.println("此团单不是团体万能");
	        	//蹭一下这个方法，判断一下补充团体医疗的保单
	            String str="select 1 from lcgrppol a,lpgrpedoritem b "
	            	 	+ " where a.grpcontno=b.grpcontno "
	            	 	+ " and a.riskcode in (select code from ldcode where codetype='tdbc')"
	            	 	+ " and b.edorno='"+mLPEdorAppSchema.getEdorAcceptNo()+"'"
	            	 	+ " union "
	            	 	+ " select 1 from lbgrppol a,lpgrpedoritem b "
	            	 	+ " where a.grpcontno=b.grpcontno "
	            	 	+ " and a.riskcode in (select code from ldcode where codetype='tdbc')"
	            	 	+ " and b.edorno='"+mLPEdorAppSchema.getEdorAcceptNo()+"'";
	            
	            String istdbc=tExeSQL5.getOneValue(str);
	            if(null !=istdbc && !"".equals(istdbc)){
	            	System.out.println("补充a");
	            	isTDBC = true ;
	            }
            return false;
            }
        }
        System.out.println("此团单是团体万能");
        isULI=true;
        
        return true;
    }    

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    private boolean checkData()
    {
        mLPEdorAppSchema = getLPEdorAppInfo(mLPEdorAppSchema.getEdorAcceptNo());
        if(mLPEdorAppSchema == null)
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PrtGrpInsuredListZTBL p = new PrtGrpInsuredListZTBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20060302000039");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("edorType", "ZT");

        VData v = new VData();
        v.add(schema);
        v.add(tTransferData);
        if(!p.submitData(v, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
