package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 被保人重要资料变更保全申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong,QiuYang
 * @version 1.0
 */
public class PEdorMCConfirmBL implements EdorConfirm
{
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    /** 客户号，可能是投保人也可能是被保人 */
    private String mCustomerNo = null;

    /** 提示信息 */
    private String mMessage = null;

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        getInputData();

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        data.add(mMessage);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        mCustomerNo = mLPEdorItemSchema.getInsuredNo();
    }

    /**
     * 判断是否是被保人信息变更
     * @return boolean
     */
    private boolean isInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mContNo);
        tLCInsuredDB.setInsuredNo(mCustomerNo);
        if (tLCInsuredDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        if (isInsured())
        {
            if (!confirmInsured())
            {
                return false;
            }
        }
        else
        {
            if (!confirmAppnt())
            {
                return false;
            }
        }
        if (!createRelationEdor())
        {
            return false;
        }
        //团单保全已结案，但LPEdorItem状态不为结案状态 1030 数据校验
       updateItem();
        
        return true;
    }

    /**
     * 被保险人信息变更保全确认
     * @return void
     */ 
    private void updateItem()
    {
    	 String updateItem = " update LPEdorItem "
				           + "set edorState='0', modifydate=current date,modifytime=current time "
				           + "where EdorAcceptNo='"
				           + mEdorNo + "' ";
    	 mMap.put(updateItem, "UPDATE");
    }
    
    
    /**
     * 被保险人信息变更保全确认
     * @return boolean
     */
    private boolean confirmInsured()
    {
        PEdorICConfirmBL tPEdorICConfirmBL = new PEdorICConfirmBL();
        if (!tPEdorICConfirmBL.submitData(mInputData, mOperate))
        {
            mErrors.copyAllErrors(tPEdorICConfirmBL.mErrors);
            return false;
        }
        MMap map = (MMap) tPEdorICConfirmBL.getResult().
                getObjectByObjectName("MMap", 0);
        mMap.add(map);
        return true;
    }

    /**
     * 投保人信息变更保全确认
     * @return boolean
     */
    private boolean confirmAppnt()
    {
        PEdorACConfirmBL tPEdorACConfirmBL = new PEdorACConfirmBL();
        if (!tPEdorACConfirmBL.submitData(mInputData, mOperate))
        {
            mErrors.copyAllErrors(tPEdorACConfirmBL.mErrors);
            return false;
        }
        MMap map = (MMap) tPEdorACConfirmBL.getResult().
                getObjectByObjectName("MMap", 0);
        mMap.add(map);
        return true;
    }

    /**
     * 判断是否是团单CM
     * @return boolean 如果是团单CM返回true
     */
    private boolean isGrpEdor()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType(mEdorType);
        if (tLPGrpEdorItemDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 产生相关联的保全受理，CM项目根据投保人的不同来产生不同的保全受理
     * @return boolean
     */
    private boolean createRelationEdor()
    {
        //如果是团单CM则不用考虑关联关系
        if (isGrpEdor())
        {
            return true;
        }
        //如果本身就是关联产生的保全则不用再关联
        if (isRelationEdor())
        {
            return true;
        }

        SSRS conts = getRelationConts();
        for (int i = 1; i <= conts.getMaxRow(); i++)
        {
            String contNo = conts.GetText(i, 1);
            String appntNo = conts.GetText(i, 2);
            String workNo = createTask(contNo, appntNo);
            setMessage(contNo, workNo);
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean isRelationEdor()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setDetailWorkNo(mEdorNo);
        LGWorkSet tLGWorkSet = tLGWorkDB.query();
        if (tLGWorkSet.size() == 0)
        {
            return false;
        }
        String acceptWayNo = tLGWorkSet.get(1).getAcceptWayNo();
        String innerSource = tLGWorkSet.get(1).getInnerSource();
        if ((acceptWayNo == null) || (!acceptWayNo.equals("8")))
        {
            return false;
        }
        if ((innerSource == null) || (innerSource.equals("")))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到保单的投保人号
     * @return String
     */
    private String getAppntNo()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mContNo);
        if (!tLCAppntDB.getInfo())
        {
            return null;
        }
        return tLCAppntDB.getAppntNo();
    }

    /**
     * 得到相关联的保单
     * @return SSRS
     */
    private SSRS getRelationConts()
    {
        String appntNo = getAppntNo();
        String sql = "select a.ContNo, a.AppntNo " +
                "from LCAppnt a, LCCont b " +
                "where a.ContNo = b.ContNo " +
                "and b.AppFlag = '" + BQ.APPFLAG_SIGN + "' " +
                "and a.AppntNo <> '" + appntNo +  "' " +
                "and a.AppntNo = '" + mCustomerNo + "' " +
                "union " +
                "select a.ContNo, a.AppntNo " +
                "from LCAppnt a, LCCont b, LCInsured c " +
                "where a.ContNo = b.ContNo " +
                "and a.ContNO = c.ContNo " +
                "and a.AppntNo <> '" + appntNo + "' " +
                "and b.AppFlag = '" + BQ.APPFLAG_SIGN + "' " +
                "and c.InsuredNo = '" + mCustomerNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.execSQL(sql);
    }

    /**
     * 创建工单
     * @return boolean
     */
    private String createTask(String contNo, String appntNo)
    {
        String info = "系统生成的客户资料变更作业。保单" + contNo +
                "，客户号为" + mCustomerNo + "的客户资料需变更。" +
                "与工单号为" + mEdorNo + "的工单相关联。";
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(appntNo);
        tLGWorkSchema.setTypeNo("03");
        tLGWorkSchema.setDateLimit("");
        tLGWorkSchema.setApplyTypeNo("");
        tLGWorkSchema.setApplyName("");
        tLGWorkSchema.setPriorityNo("");
        tLGWorkSchema.setAcceptWayNo("8"); //与其他工单关联产生
        tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
        tLGWorkSchema.setAcceptCom(mGlobalInput.ManageCom);
        tLGWorkSchema.setAcceptorNo(mGlobalInput.Operator);
        tLGWorkSchema.setRemark(info);
        tLGWorkSchema.setInnerSource(mEdorNo); //和当前保全号相关联

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGWorkSchema);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap map = tTaskInputBL.getSubmitData(data, "INSERT||MAIN");
        if (map == null)
        {
            mErrors.copyAllErrors(tTaskInputBL.mErrors);
            return null;
        }
        mMap.add(map);
        return tTaskInputBL.getWorkNo();
    }

    /**
     * 设置提示信息
     */
    private void setMessage(String contNo, String workNo)
    {
        if (mMessage == null)
        {
            mMessage = "";
        }
        mMessage += "因客户资料变更，需关联变更保单" + contNo +
                "的客户信息，生成工单" + workNo + "。\n";
    }
}
