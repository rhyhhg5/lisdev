package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 保全处理团体理赔金帐户</p>
 * <p>Description:处理团体投保人理赔金帐户</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorGrpLPDetailUI
{
    private GEdorGrpLPDetailBL mGEdorGrpLPDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GEdorGrpLPDetailUI(GlobalInput gi, String edorNo, String grpContNo)
    {
        mGEdorGrpLPDetailBL = new GEdorGrpLPDetailBL(gi, edorNo, grpContNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!mGEdorGrpLPDetailBL.submitData(cInputData))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGEdorGrpLPDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGEdorGrpLPDetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ManageCom = "86";
        GEdorGADetailUI tGEdorGADetailUI = new GEdorGADetailUI(gi,
                "20051227000005", "0000031001");
        if (!tGEdorGADetailUI.submitData())
        {
            System.out.println(tGEdorGADetailUI.getError());
        }
    }
}
