package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PEdorFCAppConfirmBL  implements EdorAppConfirm {

    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private GlobalInput mGlobalInput = null;

    private LJSGetEndorseSchema mLJSGetEndorseSchema = new LJSGetEndorseSchema();

    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();

    private LPEdorEspecialDataSet mLPEdorEspecialDataSet = new LPEdorEspecialDataSet();

    public PEdorFCAppConfirmBL() {
    }

    public CErrors mErrors= new CErrors();

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //处理数据
        if (!dealData()) {
            return false;
        }
        System.out.println("after dealData data....");

        //数据准备操作
        if (!prepareData()) {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) mInputData
                       .getObjectByObjectName("GlobalInput", 0);
        return true;
    }

    private boolean dealData()
    {
        EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
        LCPolDB tLCPolDB = new LCPolDB();

        LCPolSet tLCPolSet = new LCPolSet();
        String sqlPOL = "select * From LCPol where ContNo ='"+mLPEdorItemSchema.getContNo()+"'";
        tLCPolSet = tLCPolDB.executeQuery(sqlPOL);
        LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
        for ( int i = 1 ; i <= tLCPolSet.size() ; i ++)
        {
            LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
            LCPolSchema tLCPolSchema = tLCPolSet.get(i).getSchema();
            tEdorCalZTTestBL.setEdorValiDate(mLPEdorItemSchema.getEdorValiDate());
            mLJSGetEndorseSchema = tEdorCalZTTestBL.budgetOnePol(tLCPolSchema);
            tLPEdorEspecialDataSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPEdorEspecialDataSchema.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
            tLPEdorEspecialDataSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            tLPEdorEspecialDataSchema.setDetailType("FC");
            double x = mLJSGetEndorseSchema.getGetMoney();
            String y = Double.toString(Math.abs(x));
            tLPEdorEspecialDataSchema.setEdorValue(y);
            tLPEdorEspecialDataSchema.setPolNo(tLCPolSchema.getPolNo());
            mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
//            mLJSGetEndorseSet.add(mLJSGetEndorseSchema);
        }

        return true;
    }

    private boolean prepareData() {
        mResult.clear();
        MMap map = new MMap();
//        map.put(mLJSGetEndorseSet, "DELETE&INSERT");
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(mLPEdorEspecialDataSet,SysConst.DELETE_AND_INSERT);
        mResult.add(map);
        return true;
    }


    public VData getResult() {
        return mResult;
    }

}
