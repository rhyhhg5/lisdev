package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BqUpdateUnlockBL {
	public CErrors mErrors = new CErrors();
	private VData Result = new VData();
	/** 往后面传输数据的容器 */
	private VData tVData;
	private String edorno;
	// 数据库查询对象
	public ExeSQL exeSql = new ExeSQL();
	TransferData tTransferData = new TransferData();
	// PubSubmit tPubSubmit = new PubSubmit();
	MMap tmap = new MMap();

	/**
	 * 数据提交：外部数据通过该接口传递进来进行业务处理
	 * 
	 * @param cInputData
	 * @return
	 */
	public boolean submitData(VData cInputData) {
		this.tVData = cInputData;
		// 1、获取数据
		if (!getInputData()) {
			return false;
		}
		// 2、处理数据
		if (!dealData()) {
			return false;
		}
		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 获取传递过来的数据
	 * 
	 * @return
	 */
	private boolean getInputData() {
		System.out.println("开始获取数据...");
		tTransferData = (TransferData) tVData.getObjectByObjectName(
				"TransferData", 0);
		edorno = (String) tTransferData.getValueByName("edorno");
		String sql = "select * from lpedorapp where edoracceptno = '" + edorno
				+ "' and edorstate != '0'";
		SSRS ssrs = exeSql.execSQL(sql);
		if (ssrs == null || ssrs.getMaxRow() <= 0) {
			CError tError = new CError();
			tError.moduleName = "BqUpdateUnlockBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取数据失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		String sql = "delete from lcurgeverifylog where serialno = '" + edorno
				+ "'";
		tmap.put(sql, "DELETE");
		Result.clear();
		System.out.println("sql:" + sql);
		Result.add(tmap);
		// if(tPubSubmit.submitData(Result, "") == false){
		// System.out.println("数据出错");
		// return false;
		// }
		return true;
	}

	private boolean submit() {
		// VData data = new VData();
		// data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(Result, "")) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
