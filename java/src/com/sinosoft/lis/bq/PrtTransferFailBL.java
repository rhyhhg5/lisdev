package com.sinosoft.lis.bq;

import java.io.*;
import java.util.*;
import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Tjj
 * @version 1.0
 */

public class PrtTransferFailBL {
    private Document myDocument;

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String mEdorAcceptNo = "";

    //批单号
    private String agentGroup = "";
    private String agentCode = "";
    private int mSameCM = 0;

    //业务处理相关变量
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private MMap mMap = new MMap();

    //LJAget 的实付号
    private String mActuGetNo;
    private LJAGetDB mLJAGetDB = new LJAGetDB();

    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
    public PrtTransferFailBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }
        return true;
    }

    /**
     * 生成数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean getSubmitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 得到MMap
     * @return MMap
     */
    public MMap getMMap() {
        return mMap;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData temp = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mActuGetNo = (String) temp.getValueByName("actuGetNo");

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("\n\nStart Write Print Data\n\n");
        //最好紧接着就初始化xml文档
        xmlexport.createDocuments("TransferFailPrint.vts", mGlobalInput);
        mLJAGetDB.setActuGetNo(this.mActuGetNo);
        LJAGetSet tLJAGetSet = mLJAGetDB.query();
        if (tLJAGetSet == null || tLJAGetSet.size() < 1) {
            buildError("dealData", "在LJAGet中无相关的数据");
            return false;
        }
        textTag.add("AppntName", tLJAGetSet.get(1).getAccName());
        textTag.add("BankCon", tLJAGetSet.get(1).getBankAccNo());
        String sql = " select codename from ldcode  where codetype = 'bank' "
                     + " and code = '"
                     + tLJAGetSet.get(1).getBankCode()
                     + "'"
                     ;
        ExeSQL tExeSQL = new ExeSQL();
        String bankName = tExeSQL.getOneValue(sql);
        textTag.add("BankName", bankName);


        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        mEdorAcceptNo = tLJAGetSet.get(1).getOtherNo();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
        tLPEdorAppSet.set(tLPEdorAppDB.query());
        if (tLPEdorAppSet == null || tLPEdorAppSet.size() < 1) {
            buildError("dealData", "在LPEdorApp中无相关保全受理号的数据");
            return false;
        }
        mLPEdorAppSchema = tLPEdorAppSet.get(1);
        textTag.add("EdorDate", mLPEdorAppSchema.getEdorAppDate());
        textTag.add("PayNo", tLJAGetSet.get(1).getActuGetNo());

        //3 团单 10 个单

        if (tLJAGetSet.get(1).getOtherNoType().equals("3")) {
            String tsql = "select * from LPGrpEdorItem where edorno='"
                          + tLJAGetSet.get(1).getOtherNo()
                          + "' order by MakeDate,MakeTime";
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
            tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(tsql);
            if (tLPGrpEdorItemSet.size() == 0) {
                buildError("dealData", "团险保全项目表中无相关批单号的数据");
                return false;
            }
            LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
            tLPGrpEdorMainDB.setEdorNo(mEdorAcceptNo);
            LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
            tLPGrpEdorMainSet.set(tLPGrpEdorMainDB.query());
            if (tLPGrpEdorMainSet.size() == 0) {
                buildError("dealData", "在LPGrpEdorMain中无相关批单号的数据");
                return false;
            }
            LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);

            String tGrpContNo = tLPGrpEdorMainSchema.getGrpContNo(); //取得保全记录的合同号
            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tGrpContNo);
            if (!tLCGrpContDB.getInfo()) {
                LBGrpContDB tLBGrpContDB = new LBGrpContDB();
                tLBGrpContDB.setGrpContNo(tGrpContNo);
                if (!tLBGrpContDB.getInfo()) {
                    return false;
                }
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLCGrpContSchema,
                                         tLBGrpContDB.getSchema());
            } else {
                tLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
            }

            LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
            tLCGrpAppntDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
            if (!tLCGrpAppntDB.getInfo()) {
                buildError("getDetailAD", "团单投保人表LCGrpAppnt中无相关集体合同号的记录");
                return false;
            }
            LCGrpAppntSchema tLCGrpAppntSchema = tLCGrpAppntDB.getSchema();

            //判断是否含有AC项目
            boolean hasAC = false;
            for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
                if (tLPGrpEdorItemSet.get(i).getEdorType().equals("AC")) {
                    hasAC = true;
                    break;
                }
            }

            //得到单位地址信息，如果是AC项目则取P表，否则取C表
            if (hasAC == true) {
                LPGrpAddressDB tLPGrpAddressDB = new LPGrpAddressDB();
                tLPGrpAddressDB.setEdorNo(this.mEdorAcceptNo);
                tLPGrpAddressDB.setEdorType("AC");
                LPGrpAddressSet tLPGrpAddressSet = tLPGrpAddressDB.query();
                LPGrpAddressSchema tLPGrpAddressSchema = tLPGrpAddressSet.get(1);
                tLPGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
                LPGrpContDB tLPGrpContDB = new LPGrpContDB();
                tLPGrpContDB.setEdorNo(mEdorAcceptNo);
                tLPGrpContDB.setEdorType("AC");
                tLPGrpContDB.setGrpContNo(tLPGrpEdorMainSchema.getGrpContNo());
                tLPGrpContDB.getInfo();

                textTag.add("EdorAppZipCode", tLPGrpAddressSchema.getGrpZipCode());
                textTag.add("EdorAppAddress", tLPGrpAddressSchema.getGrpAddress());
                textTag.add("EdorAppName", tLPGrpAddressSchema.getLinkMan1());
                textTag.add("Name", tLPGrpAddressSchema.getLinkMan1());
                textTag.add("AppntName", tLPGrpContDB.getGrpName());
                textTag.add("AppntNo", tLPGrpContDB.getAppntNo());
            } else {
                LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
                tLCGrpAddressDB.setCustomerNo(tLCGrpAppntSchema.getCustomerNo());
                tLCGrpAddressDB.setAddressNo(tLCGrpAppntSchema.getAddressNo());
                if (!tLCGrpAddressDB.getInfo()) {
                    buildError("", "未查到单位地址信息！");
                    return false;
                }
                LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressDB.
                        getSchema();
                textTag.add("EdorAppZipCode", tLCGrpAddressSchema.getGrpZipCode());
                textTag.add("EdorAppAddress", tLCGrpAddressSchema.getGrpAddress());
                textTag.add("EdorAppName", tLCGrpAddressSchema.getLinkMan1());
                textTag.add("Name", tLCGrpAddressSchema.getLinkMan1());
                textTag.add("AppntName", tLCGrpContSchema.getGrpName());
                textTag.add("AppntNo", tLCGrpContDB.getAppntNo());
            }

            LPGrpContDB tLPGrpContDB = new LPGrpContDB();
            tLPGrpContDB.setEdorNo(tLJAGetSet.get(1).getOtherNo());
            LPGrpContSet temp = tLPGrpContDB.query();
            if (temp == null && temp.size() == 0) {
                buildError("dealData", "查询保全保单失败");
                return false;
            }
            agentCode = temp.get(1).getAgentCode();
            agentGroup = temp.get(1).getAgentGroup();
        } else if (tLJAGetSet.get(1).getOtherNoType().equals("10")) {
            String tCustomerNo = mLPEdorAppSchema.getOtherNo();

            if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
                sql = "select * from LPAppnt where edorno in (select edorno from lpedormain where edoracceptno='" +
                      mLPEdorAppSchema.getEdorAcceptNo() + "')";
                System.out.println("sql 1 : " + sql);
                LPAppntDB tLPAppntDB = new LPAppntDB();
                LPAppntSet tLPAppntSet = tLPAppntDB.executeQuery(sql);
                if (tLPAppntSet != null && tLPAppntSet.size() > 0) {
                    tCustomerNo = tLPAppntSet.get(1).getAppntNo();
                }

            } else
            if (mLPEdorAppSchema.getOtherNoType().equals("3")) {
                sql = "select * from lpcont where contno='" +
                      mLPEdorAppSchema.getOtherNo() +
                      "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                      mLPEdorAppSchema.getEdorAcceptNo() + "')";
                System.out.println("sql 2 : " + sql);
                LPContDB tLPContDB = new LPContDB();
                LPContSet tLPContSet = tLPContDB.executeQuery(sql);
                if (tLPContSet == null || tLPContSet.size() < 1) {
                    LCContDB tLCContDB = new LCContDB();
                    tLCContDB.setContNo(mLPEdorAppSchema.getOtherNo());
                    if (!tLCContDB.getInfo()) {
                        buildError("dealData", "保全申请的合同不存在");
                        return false;
                    }

                    tCustomerNo = tLCContDB.getAppntNo();
                } else {
                    tCustomerNo = tLPContSet.get(1).getAppntNo();
                }
            }
            textTag.add("AppntNo", tCustomerNo);
            sql = "select * from LdPerson where CustomerNo='"
                  + tCustomerNo
                  +"'"
                  ;
            System.out.println("sql 3 : " + sql);
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            LDPersonDB tLDPersonDB = new LDPersonDB();
            LDPersonSet tLDPersonSet = tLDPersonDB.executeQuery(sql);
            if (tLDPersonSet == null || tLDPersonSet.size() < 1) {
                LBPersonDB tLBPersonDB = new LBPersonDB();
                tLBPersonDB.setCustomerNo(tCustomerNo);
                if (!tLBPersonDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "PrtTransferFailBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询客户失败（C，B）不存在相应记录!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LBPersonSchema tLBPersonSchema = tLBPersonDB.getSchema();
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLDPersonSchema, tLBPersonSchema);
            } else {
                tLDPersonSchema = tLDPersonSet.get(1).getSchema();
            }
            textTag.add("EdorAppName",
                        tLDPersonSchema.getName() +
                        CommonBL.decodeSex(tLDPersonSchema.getSex()));
            textTag.add("Name", tLDPersonSchema.getName());

            sql = "select * from LPAddress where CustomerNo='"
                  + tCustomerNo
                  +"' and edorno in (select edorno from lpedormain where edoracceptno='"
                  + mLPEdorAppSchema.getEdorAcceptNo() + "')"
                  ;
            System.out.println("sql 4 : " + sql);
            LPAddressSchema tLPAddressSchema = new LPAddressSchema();
            LPAddressDB tLPAddressDB = new LPAddressDB();
            LPAddressSet tLPAddressSet = tLPAddressDB.executeQuery(sql);
            if (tLPAddressSet == null || tLPAddressSet.size() < 1) {
                //得到第一个保全申请保单的合同号
                LPEdorMainDB db = new LPEdorMainDB();
                db.setEdorAcceptNo(mEdorAcceptNo); ;
                LPEdorMainSet set = db.query();
                if (set.size() <= 0) {
                    mErrors.addOneError("没有查到受理号" + mEdorAcceptNo
                                        + "所对应的保全申请LPEdorApp记录");
                    return false;
                }

                //得到保单的地址信息(地址号)
                LCAppntSchema tLCAppntSchema = new LCAppntSchema();
                LCAppntDB tLCAppntDB = new LCAppntDB();
                tLCAppntDB.setContNo(set.get(1).getContNo());
                LCAppntSet tLCAppntSet = tLCAppntDB.query();
                tLCAppntSchema = tLCAppntSet.get(1);
                if(!tLCAppntDB.getInfo())
                {
                    tLCAppntSchema = new LCAppntSchema();
                    LBAppntDB tLBAppntDB = new LBAppntDB();
                    tLBAppntDB.setContNo(set.get(1).getContNo());
                    tLBAppntDB.setEdorNo(mEdorAcceptNo);
                    if (!tLBAppntDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "PrtTransferFailBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "查询投保人失败（C，B）不存在相应记录!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }

                    LBAppntSchema tLBAppntSchema = tLBAppntDB.getSchema();
                    Reflections aReflections = new Reflections();
                    aReflections.transFields(tLCAppntSchema, tLBAppntSchema);

                }

                //得到该合同该地址号的地址信息
                sql = "select * from LCAddress where customerNo = '"
                      + tCustomerNo
                      + "' and addressNo ='" + tLCAppntSchema.getAddressNo()
                      + "' "
                      ;
                System.out.println("sql 5 : " + sql);
                LCAddressDB tLCAddressDB = new LCAddressDB();

                LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
                if (tLCAddressSet == null || tLCAddressSet.size() < 1) {
                    CError tError = new CError();
                    tError.moduleName = "PrtAppEndorsementBL";
                    tError.functionName = "getInsuredCM";
                    tError.errorMessage = "未找到投保人地址信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LCAddressSchema tLCAddressSchema =
                        tLCAddressSet.get(1).getSchema();
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLPAddressSchema, tLCAddressSchema);
            } else {
                tLPAddressSchema = tLPAddressSet.get(1).getSchema();
            }
            textTag.add("EdorAppZipCode", tLPAddressSchema.getZipCode());
            textTag.add("EdorAppAddress", tLPAddressSchema.getPostalAddress());

        }

        else {
            buildError("dealData", "判断团个单失败");
            return false;
        }

        setFixedInfo();
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(tLJAGetSet.get(1).getOtherNo());
        LPContSet temp = tLPContDB.query();
        if (temp == null && temp.size() == 0) {
            buildError("dealData", "查询保全保单失败");
            return false;
        }
        agentCode = temp.get(1).getAgentCode();
        agentGroup = temp.get(1).getAgentGroup();

//        mEdorNo = tLJAGetSet.get(1).getOtherNo();
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(agentCode);
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentNO", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", tLABranchGroupDB.getName());

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }


    mResult.clear();

    //生成主打印批单schema
    xmlexport.outputDocumentToFile("D:\\", "bqxml_ZHJ");

    mResult.add(xmlexport);

    return true;

}


/**
 * 设置批单显示的固定信息
 */
private void setFixedInfo() {
    //查询受理机构
    LDUserDB tLDUserDB = new LDUserDB();
    tLDUserDB.setUserCode(mGlobalInput.Operator);
    tLDUserDB.getInfo();

    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(tLDUserDB.getComCode());
    tLDComDB.getInfo();

    textTag.add("ServicePhone", tLDComDB.getServicePhone());
    textTag.add("ServiceFax", tLDComDB.getFax());
    textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
    textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
    textTag.add("ComName", tLDComDB.getLetterServiceName());
    textTag.add("Com", tLDComDB.getLetterServiceName());
    textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    textTag.add("MakeDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    textTag.add("BackDate",
                CommonBL.decodeDate(PubFun.calDate(PubFun.getCurrentDate(), 10,
            "D", "")));

}

//查询客户名称
private String getCustomerName(String customerNo) {
    LCAddressSchema tLCAddressSchema = null;
    LDPersonDB tLDPersonDB = new LDPersonDB();
    tLDPersonDB.setCustomerNo(customerNo);
    if (!tLDPersonDB.getInfo()) {
        mErrors.addOneError("没有查询到受理信息。");
        return "";
    }
    return tLDPersonDB.getName();
}

private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "PrtTransferFailBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

public static void main(String[] args) {
    GlobalInput gi = new GlobalInput();
    gi.Operator = "endor0";
    gi.ManageCom = "86";
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("actuGetNo", "37000000031");

    VData tVData = new VData();
    tVData.add(tTransferData);
    tVData.add(gi);
    PrtTransferFailBL tPrtTransferFailBL =
            new PrtTransferFailBL();
    if (!tPrtTransferFailBL.submitData(tVData, "")) {
        System.out.println(tPrtTransferFailBL.mErrors.getErrContent());
    }

}
}
