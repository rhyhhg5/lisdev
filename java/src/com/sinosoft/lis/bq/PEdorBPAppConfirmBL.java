package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

//程序名称：PEdorBPAppConfirmBL.java
//程序功能：万能保费变更理算类
//创建日期：2009-07-03
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorBPAppConfirmBL implements EdorAppConfirm
{
    private MMap map = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 合同号 */
    private String mContNo = null;

    /** 保单险种号 */
    private String mPolNo = null;
    
    private String mNewPrem = null;

    private Reflections ref = new Reflections();

    private DetailDataQuery mQuery = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;
    
    private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator;
    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            System.out.println("PEdorBPAppConfirmBL.java->getInputData(cInputData)失败");
        	return false;
        }

        if (!dealData())
        {
        	System.out.println("PEdorBPAppConfirmBL.java->dealData()失败");
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema)cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mContNo = mLPEdorItemSchema.getContNo();
        System.out.println("mEdorNo="+mEdorNo+",mEdorType="+mEdorType+",mContNo="+mContNo);
        mOperator=mGlobalInput.Operator;
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mEdorNo);
        tLPEdorEspecialDataDB.setEdorType(mEdorType);
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,mEdorType);
        if(tSpecialData.query())
        {
        	mNewPrem = tSpecialData.getEdorValue("NEWPREM");
            mPolNo = tSpecialData.getEdorValue("POLNO");
        }
        else
        {
        	System.out.println("PEdorBPAppConfirmBL.java->line103行出错");
        	return false;
        }
        
        System.out.println("mNewPrem="+mNewPrem+",mPolNo="+mPolNo);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() 
    {
    	LPContSchema tLPContSchema =  (LPContSchema) mQuery.getDetailData("LCCont", mContNo);
    	tLPContSchema.setPrem(mNewPrem);
    	tLPContSchema.setOperator(mOperator);
    	tLPContSchema.setMakeDate(mCurrentDate);
    	tLPContSchema.setMakeTime(mCurrentTime);
    	tLPContSchema.setModifyDate(mCurrentDate);
    	tLPContSchema.setModifyTime(mCurrentTime);
    	map.put(tLPContSchema, SysConst.DELETE_AND_INSERT);
    	
    	LPPolSchema tLPPolSchema =  (LPPolSchema) mQuery.getDetailData("LCPol", mPolNo);
    	tLPPolSchema.setPrem(mNewPrem);
    	tLPPolSchema.setStandPrem(mNewPrem);
    	tLPPolSchema.setOperator(mOperator);
    	tLPPolSchema.setMakeDate(mCurrentDate);
    	tLPPolSchema.setMakeTime(mCurrentTime);
    	tLPPolSchema.setModifyDate(mCurrentDate);
    	tLPPolSchema.setModifyTime(mCurrentTime);
    	map.put(tLPPolSchema, SysConst.DELETE_AND_INSERT);
    	
    	LPDutySet tLPDutySet = new LPDutySet();
    	LPPremSet tLPPremSet = new LPPremSet();
    	String tRiskCode = tLPPolSchema.getRiskCode();
    	LCDutyDB tLCDutyDB = new LCDutyDB();
    	tLCDutyDB.setPolNo(mPolNo);
    	LCDutySet tLCDutySet = tLCDutyDB.query();
    	LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
    	tLMRiskDutyDB.setRiskCode(tRiskCode);
    	LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
    	boolean haveSetDuty = false;
    	for(int i=1;i<=tLCDutySet.size();i++)
    	{
    		LCDutySchema tLCDutySchema = new LCDutySchema();
    		tLCDutySchema.setSchema(tLCDutySet.get(i));
    		LPDutySchema tLPDutySchema = new LPDutySchema();
    		ref.transFields(tLPDutySchema, tLCDutySchema);
    		boolean canSetDuty = false;
    		String newprem = "0";
    		for(int j=1;j<=tLMRiskDutySet.size();j++)
    		{
    			LMRiskDutySchema tLMRiskDutySchema = new LMRiskDutySchema();
    			tLMRiskDutySchema.setSchema(tLMRiskDutySet.get(j));
    			if(tLPDutySchema.getDutyCode().equals(tLMRiskDutySchema.getDutyCode()))
    			{
    				canSetDuty = true;
    			}
    		}
    		if(canSetDuty)
    		{
    			if(!haveSetDuty)
    			{
    				newprem = mNewPrem;
    				haveSetDuty = true;
    			}
    		}
    		tLPDutySchema.setEdorNo(mEdorNo);
    		tLPDutySchema.setEdorType(mEdorType);
    		tLPDutySchema.setPrem(newprem);
    		tLPDutySchema.setStandPrem(newprem);
    		tLPDutySchema.setOperator(mOperator);
    		tLPDutySchema.setMakeDate(mCurrentDate);
    		tLPDutySchema.setMakeTime(mCurrentTime);
    		tLPDutySchema.setModifyDate(mCurrentDate);
    		tLPDutySchema.setModifyTime(mCurrentTime);
    		tLPDutySet.add(tLPDutySchema);
    		
    		LCPremDB tLCPremDB = new LCPremDB();
    		tLCPremDB.setPolNo(tLPDutySchema.getPolNo());
    		tLCPremDB.setDutyCode(tLPDutySchema.getDutyCode());
    		LCPremSet tLCPremSet = tLCPremDB.query();
    		LMDutyPayRelaDB tLMDutyPayRelaDB = new LMDutyPayRelaDB();
    		tLMDutyPayRelaDB.setDutyCode(tLPDutySchema.getDutyCode());
    		LMDutyPayRelaSet tLMDutyPayRelaSet = tLMDutyPayRelaDB.query();
    		boolean haveSetPrem = false;
        	for(int k=1;k<=tLCPremSet.size();k++)
        	{
        		LCPremSchema tLCPremSchema = new LCPremSchema();
        		tLCPremSchema.setSchema(tLCPremSet.get(k));
        		LPPremSchema tLPPremSchema = new LPPremSchema();
        		ref.transFields(tLPPremSchema, tLCPremSchema);
        		boolean canSetPrem = false;
        		double newprem1 = 0.0;
        		for(int l=1;l<=tLMDutyPayRelaSet.size();l++)
        		{
        			LMDutyPayRelaSchema tLMDutyPayRelaSchema = new LMDutyPayRelaSchema();
        			tLMDutyPayRelaSchema.setSchema(tLMDutyPayRelaSet.get(l));
        			if(tLPPremSchema.getPayPlanCode().equals(tLMDutyPayRelaSchema.getPayPlanCode()))
        			{
        				canSetPrem = true;
        			}
        		}
        		if(canSetPrem)
        		{
        			if(!haveSetPrem)
        			{
        				newprem1 = tLPDutySchema.getPrem();
            			haveSetPrem = true;
        			}
        		}
        		tLPPremSchema.setEdorNo(mEdorNo);
        		tLPPremSchema.setEdorType(mEdorType);
        		tLPPremSchema.setPrem(newprem1);
        		tLPPremSchema.setStandPrem(newprem1);
        		tLPPremSchema.setOperator(mOperator);
        		tLPPremSchema.setMakeDate(mCurrentDate);
        		tLPPremSchema.setMakeTime(mCurrentTime);
        		tLPPremSchema.setModifyDate(mCurrentDate);
        		tLPPremSchema.setModifyTime(mCurrentTime);
        		tLPPremSet.add(tLPPremSchema);
        	}
    	}
    	map.put(tLPDutySet, SysConst.DELETE_AND_INSERT);
    	map.put(tLPPremSet, SysConst.DELETE_AND_INSERT);
        return true;
    }
 }
