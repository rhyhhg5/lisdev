package com.sinosoft.lis.bq;

import java.io.*;
import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.GetMemberInfo;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Tjj
 * @version 1.0
 */

public class PrtInsuEndorsementBL {
    private Document myDocument;

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //保全受理号 By WangJH
    private String mEdorAcceptNo = "";

    //批单号
    private String mEdorNo = "";
    private String mContNo = "";
    private String sqlSelectAddress ="" ;
    //业务处理相关变量
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private ListTable tlistTable;
    private MMap mMap = new MMap();
    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    //By WangJH
    public PrtInsuEndorsementBL(String aEdorAcceptNo) {
        mEdorAcceptNo = aEdorAcceptNo;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }
        return true;
    }

    /**
     * 生成数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean getSubmitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 得到MMap
     * @return MMap
     */
    public MMap getMMap() {
        return mMap;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("\n\nStart Write Print Data\n\n");
        boolean tFlag = false;

        //最好紧接着就初始化xml文档
        xmlexport.createDocuments("PrtAppEndorsementApp.vts", mGlobalInput);

        //从2层查询变更为3层查询--By WangJH
        //增加了保全受理号，提升到定层的保全申请主表LPEdorApp开始查找
        //保全申请主表LPEdorApp->个险保全批改表LPEdorMain->个险保全项目表LPEdorItem
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
        tLPEdorAppSet.set(tLPEdorAppDB.query());
        if (tLPEdorAppSet == null || tLPEdorAppSet.size() < 1) {
            buildError("dealData", "在LPEdorApp中无相关保全受理号的数据");
            return false;
        }
        mLPEdorAppSchema = tLPEdorAppSet.get(1);

        xmlexport.addDisplayControl("displayHead");

        String tCustomerNo = "";
        textTag.add("EdorAcceptNo", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCode1", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getMakeDate()));
        setFixedInfo();

        
        
        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            tCustomerNo = mLPEdorAppSchema.getOtherNo();
            String sql = "select * from lpInsured where edorno in (select edorno from lpedormain where edoracceptno='" +
                         mLPEdorAppSchema.getEdorAcceptNo() + "')";
            System.out.println("sql 1 :------------------------ " + sql);
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.executeQuery(sql);
            if (tLPInsuredSet != null && tLPInsuredSet.size() > 0) {
                tCustomerNo = tLPInsuredSet.get(1).getInsuredNo();
            }
            System.out.println("客户号---------" + tCustomerNo);
            textTag.add("CustomerNo", tCustomerNo);
        } 

        String sql = "select * from LPPerson where CustomerNo='" + tCustomerNo +
                     "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                     mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 3 : " + sql);
        LPPersonSchema tLPPersonSchema = new LPPersonSchema();
        LPPersonDB tLPPersonDB = new LPPersonDB();
        LPPersonSet tLPPersonSet = tLPPersonDB.executeQuery(sql);
        if (tLPPersonSet == null || tLPPersonSet.size() < 1) {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tCustomerNo);
            if (!tLDPersonDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "PrtInsuEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LDPersonDB不存在相应记录!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPPersonSchema, tLDPersonSchema);
        } else {
            tLPPersonSchema = tLPPersonSet.get(1).getSchema();
        }
        textTag.add("EdorAppName", tLPPersonSchema.getName());
        textTag.add("EdorAppSex", CommonBL.decodeSex(tLPPersonSchema.getSex()));
        textTag.add("AppntName", tLPPersonSchema.getName());

        sql = "select * from LPAddress where CustomerNo='" + tCustomerNo +
              "' and edorno in (select edorno from lpedormain where edoracceptno='" +
              mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 4 : " + sql);
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        LPAddressDB tLPAddressDB = new LPAddressDB();
        LPAddressSet tLPAddressSet = tLPAddressDB.executeQuery(sql);
        if (tLPAddressSet == null || tLPAddressSet.size() < 1) {
            //得到第一个保全申请保单的合同号
            LPEdorMainDB db = new LPEdorMainDB();
            db.setEdorAcceptNo(mEdorAcceptNo); ;
            LPEdorMainSet set = db.query();
            if (set.size() <= 0) {
                mErrors.addOneError("没有查到受理号" + mEdorAcceptNo
                                    + "所对应的保全申请LPEdorApp记录");
                return false;
            }

           
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(set.get(1).getContNo());
            tLCInsuredDB.getInfo();

            //得到该合同该地址号的地址信息
             sqlSelectAddress =
                    "select * "
                    + "from LCAddress "
                    + "where customerNo = '" + tCustomerNo + "' "
                    + "    and addressNo ='" + tLCInsuredDB.getAddressNo() + "' ";
            
            System.out.println("sql 5 : " + sqlSelectAddress);
            LCAddressDB tLCAddressDB = new LCAddressDB();

            LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(
                    sqlSelectAddress);
            if (tLCAddressSet == null || tLCAddressSet.size() < 1) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "未找到投保人地址信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCAddressSchema tLCAddressSchema =
                    tLCAddressSet.get(1).getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPAddressSchema, tLCAddressSchema);
        } else {
            tLPAddressSchema = tLPAddressSet.get(1).getSchema();
        }
        textTag.add("EdorAppZipCode", tLPAddressSchema.getZipCode());
        textTag.add("EdorAppAddress", tLPAddressSchema.getPostalAddress());

        //查询个险保全项目表中保全受理号为mEdorAcceptNo的记录
        String AppSQL = "select * from LPEdorMain where EdorAcceptNo ='" +
                        mEdorAcceptNo +
                        "'order by MakeDate,MakeTime";
        System.out.println("sql 6 : " + AppSQL);
        //--End

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.executeQuery(AppSQL); //By WangJH
        if (0 == tLPEdorMainSet.size()) {
            buildError("dealData", "在LPEdorMain中无相关批单号的数据");
            return false;
        }

        //增加了一层循环 By WangJH
        for (int i = 1; i <= tLPEdorMainSet.size(); i++) {
            LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(i);
            mContNo = tLPEdorMainSchema.getContNo(); //取得保全记录的合同号
            mEdorNo = tLPEdorMainSchema.getEdorNo(); //取得保全记录的批单号 By WangJH
            LCContSchema tLCContSchema = new LCContSchema();
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo()) {
                LBContDB tLBContDB = new LBContDB();
                tLBContDB.setContNo(mContNo);
                if (!tLBContDB.getInfo()) {
                    return false;
                }
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLCContSchema, tLBContDB.getSchema());
            } else {
                tLCContSchema.setSchema(tLCContDB.getSchema());
            }
            //业务员信息
            String agntPhone = "";
            String temPhone = "";
            String agentCode = null;
            LPContDB tLPContDB = new LPContDB();
            tLPContDB.setContNo(mContNo);
            tLPContDB.setEdorNo(mEdorNo);
            tLPContDB.setEdorType("PR");//个单迁移
            if (tLPContDB.getInfo()) {
                agentCode = tLPContDB.getAgentCode();
            }
            if (agentCode == null) {
                agentCode = tLCContSchema.getAgentCode();
            }
            LAAgentDB tLaAgentDB = new LAAgentDB();
            tLaAgentDB.setAgentCode(agentCode);
            tLaAgentDB.getInfo();
            textTag.add("AgentName", tLaAgentDB.getName());
            textTag.add("AgentCode", tLaAgentDB.getAgentCode());
            temPhone = tLaAgentDB.getMobile();
            if (temPhone == null || temPhone.equals("") ||
                temPhone.equals("null")) {
                agntPhone = tLaAgentDB.getPhone();
            } else {
                agntPhone = temPhone;
            }
            textTag.add("Phone", agntPhone);

            //机构信息
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContSchema.getAgentGroup());
            tLABranchGroupDB.getInfo();
            String branchSQL =
                    " select * from LABranchGroup where agentgroup = "
                    +
                    " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                    + " (select agentgroup from laagent where agentcode ='"
                    + tLaAgentDB.getAgentCode() + "'))"
                    ;
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                    branchSQL);
            if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
                CError.buildErr(this, "查询业务员机构失败");
                return false;
            }

            textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            tLPEdorItemDB.setContNo(mContNo);
            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            for (int j = 1; j <= tLPEdorItemSet.size(); j++) {
                LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(j);
               if (tLPEdorItemSchema.getEdorType().equals("AD")) {
                    xmlexport.addDisplayControl("displayAD");
                    if (!this.getDetailAD(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                else { //对没有的保全项目类型生成空打印数据
                    if (!this.getDetailForBlankType(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
            }
        } //增加的一层循环 By WangJH
        sql = "select * from ljsgetendorse where 1=1 and EndorsementNo ='" +
              this.mLPEdorAppSchema.getEdorAcceptNo() + "'";
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        System.out.println("SQL 电风扇  " + sql);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(sql);
        System.out.println(tLJSGetEndorseSet.size());
        if (tLJSGetEndorseSet == null || tLJSGetEndorseSet.size() < 1) {
            System.out.println("没有收退费记录");
        } else {
            this.xmlexport.addDisplayControl("displayMN");
            double tmoney = this.mLPEdorAppSchema.getGetMoney();
            if (tmoney < 0.0) {
                this.textTag.add("MN_Type", "应退");
                this.textTag.add("MN_Money", Math.abs(tmoney));
            } else if (tmoney > 0.0) {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", tmoney);
            } else {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", 0);
            }
        }
        //   if ((this.mLPEdorAppSchema.getGetMoney()-0.0)>-0.000001&&(this.mLPEdorAppSchema.getGetMoney()-0.0)<0.00001)
        //   {
        //       this.xmlexport.addDisplayControl("displayMN");
        //   }
        AppAcc tAppAcc = new AppAcc();
        String noticeType = "";
        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            noticeType = BQ.NOTICETYPE_P;
        }
        if (mLPEdorAppSchema.getOtherNoType().equals("2")) {
            noticeType = BQ.NOTICETYPE_G;
        }
        LCAppAccTraceSchema tLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(
                mLPEdorAppSchema.
                getOtherNo(), mLPEdorAppSchema.getEdorAcceptNo(), noticeType);
        if (tLCAppAccTraceSchema != null) {
            if (tLCAppAccTraceSchema.getAccType().equals("0")) {
                if (!this.getDetailAppAccTakeOut(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
            if (tLCAppAccTraceSchema.getAccType().equals("1")) {
                if (!this.getDetailAppAccShiftTo(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
        } else {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }

        if (!tFlag) {
            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }

        mResult.clear();

        //生成主打印批单schema
        LPEdorAppPrintSchema tLPEdorAppPrintSchemaMain = new
                LPEdorAppPrintSchema();
        tLPEdorAppPrintSchemaMain.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorAppPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorAppPrintSchemaMain.setPrtFlag("N");
        tLPEdorAppPrintSchemaMain.setPrtTimes(0);
        tLPEdorAppPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorAppPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorAppPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("D:\\", "bqxml_ZHJ");
        tLPEdorAppPrintSchemaMain.setEdorInfo(ins);
        sql = "delete from LPEdorAppPrint " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "'";
        System.out.println(sql);
        mMap.put(sql, "DELETE");
        mMap.put(tLPEdorAppPrintSchemaMain, "BLOBINSERT");
        mResult.addElement(mMap);
        try {
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true ;
        }

  
    /**
     * 设置批单显示的固定信息
     */
    private void setFixedInfo() {
        //查询受理人
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorAcceptNo);
        tLGWorkDB.getInfo();
        textTag.add("ApplyName", tLGWorkDB.getApplyName());

        //查询受理渠道
        if (tLGWorkDB.getAcceptWayNo() != null) {
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("acceptwayno");
            tLDCodeDB.setCode(tLGWorkDB.getAcceptWayNo());
            tLDCodeDB.getInfo();
            textTag.add("AcceptWay", tLDCodeDB.getCodeName());
        }

        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLGWorkDB.getAcceptorNo());
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));

        textTag.add("Operator",
                    GetMemberInfo.getMemberName(mGlobalInput.Operator));
        textTag.add("Acceptor",
                    GetMemberInfo.getMemberName(tLGWorkDB.getAcceptorNo()));
    }


    //投保人联系方式变更
    private boolean getDetailAD(LPEdorItemSchema aLPEdorItemSchema) {

        //取（保全团单被保人表）和（团单被保人表）的相关记录，生称相应schema
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsuredDB.setEdorType(aLPEdorItemSchema.getEdorType());
        LPInsuredSet tLPInsuredSet = new LPInsuredSet();
        tLPInsuredSet.set(tLPInsuredDB.query());
        if (tLPInsuredDB.mErrors.needDealError() || tLPInsuredSet.size() == 0) {
            buildError("getDetailAD", "保全投保人表LPInsured中无相关批单号的记录");
            return false;
        }
        LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(1);

        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(tLPInsuredSchema.getEdorNo());
        tLPAddressDB.setEdorType(tLPInsuredSchema.getEdorType());
        tLPAddressDB.setCustomerNo(tLPInsuredSchema.getInsuredNo());
        tLPAddressDB.setAddressNo(tLPInsuredSchema.getAddressNo());
        if (!tLPAddressDB.getInfo()) {
            buildError("getDetailAD",
                       "查询客户号为" + tLPInsuredSchema.getInsuredNo() + "的保全地址信息时失败!");
            return false;

        }
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        tLPAddressSchema.setSchema(tLPAddressDB.getSchema());

        //取（保全团体客户地址表）和（团体客户地址表）的相关记录，生称相应schema
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(tLPInsuredSchema.getInsuredNo());
        tLCInsuredDB.setContNo(tLPInsuredSchema.getContNo());
        if (!tLCInsuredDB.getInfo()) {
            buildError("getDetailAD",
                       "查询保单号为" + tLPInsuredSchema.getContNo() + "的被保人表失败!");
            return false;
        }

        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCInsuredDB.getInsuredNo());
        tLCAddressDB.setAddressNo(tLCInsuredDB.getAddressNo());
        if (!tLCAddressDB.getInfo()) {
            buildError("getDetailAD",
                       "查询客户号为" + tLPInsuredSchema.getInsuredNo() + "的地址信息时失败!");
            return false;
        }
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());

        tlistTable = new ListTable();
        tlistTable.setName("AD");
        if (!StrTool.cTrim(tLCAddressSchema.getPostalAddress()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getPostalAddress()))) {
            String[] strArr = new String[1];
            strArr[0] = "联系地址：" +
                        CommonBL.notNull(tLCAddressSchema.getPostalAddress()) +
                        "\n变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getPostalAddress());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getZipCode()).equals(StrTool.
                cTrim(tLPAddressSchema.getZipCode()))) {
            String[] strArr = new String[1];
            strArr[0] = "邮政编码：" + CommonBL.notNull(tLCAddressSchema.getZipCode()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getZipCode());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getPhone()).equals(StrTool.
                cTrim(tLPAddressSchema.getPhone()))) {
            String[] strArr = new String[1];
            strArr[0] = "联系电话：" + CommonBL.notNull(tLCAddressSchema.getPhone()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getPhone());
            tlistTable.add(strArr);
        }
        if (!StrTool.cTrim(tLCAddressSchema.getEMail()).equals(StrTool.
                cTrim(tLPAddressSchema.getEMail()))) {
            String[] strArr = new String[1];
            strArr[0] = "电子邮件地址：" + CommonBL.notNull(tLCAddressSchema.getEMail()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getEMail());
            tlistTable.add(strArr);
        }
        if (!StrTool.cTrim(tLCAddressSchema.getMobile()).equals(StrTool.cTrim(
                tLPAddressSchema.getMobile()))) {
            String[] strArr = new String[1];
            strArr[0] = "移动电话：" + CommonBL.notNull(tLCAddressSchema.getMobile()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getMobile());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getFax()).equals(StrTool.cTrim(
                tLPAddressSchema.getFax()))) {
            String[] strArr = new String[1];
            strArr[0] = "通讯传真：" + CommonBL.notNull(tLCAddressSchema.getFax()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getFax());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getHomeAddress()).equals(StrTool.
                cTrim(tLPAddressSchema.getHomeAddress()))) {
            String[] strArr = new String[1];
            strArr[0] = "家庭住址：" +
                        CommonBL.notNull(tLCAddressSchema.getHomeAddress()) +
                        "\n变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getHomeAddress());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getHomeZipCode()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getHomeZipCode()))) {
            String[] strArr = new String[1];
            strArr[0] = "家庭邮政编码：" +
                        CommonBL.notNull(tLCAddressSchema.getHomeZipCode()) +
                        " 变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getHomeZipCode());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getHomePhone()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getHomePhone()))) {
            String[] strArr = new String[1];
            strArr[0] = "家庭电话：" +
                        CommonBL.notNull(tLCAddressSchema.getHomePhone()) +
                        " 变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getHomePhone());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getHomeFax()).equals(StrTool.cTrim(
                tLPAddressSchema.getHomeFax()))) {
            String[] strArr = new String[1];
            strArr[0] = "家庭传真：" + CommonBL.notNull(tLCAddressSchema.getHomeFax()) +
                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getHomeFax());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getCompanyAddress()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getCompanyAddress()))) {
            String[] strArr = new String[1];
            strArr[0] = "单位地址：" +
                        CommonBL.notNull(tLCAddressSchema.getCompanyAddress()) +
                        "\n变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getCompanyAddress());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getCompanyZipCode()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getCompanyZipCode()))) {
            String[] strArr = new String[1];
            strArr[0] = "单位邮政编码：" +
                        CommonBL.notNull(tLCAddressSchema.getCompanyZipCode()) +
                        " 变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getCompanyZipCode());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getCompanyPhone()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getCompanyPhone()))) {
            String[] strArr = new String[1];
            strArr[0] = "单位电话：" +
                        CommonBL.notNull(tLCAddressSchema.getCompanyPhone()) +
                        " 变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getCompanyPhone());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCAddressSchema.getCompanyFax()).equals(StrTool.
                cTrim(
                        tLPAddressSchema.getCompanyFax()))) {
            String[] strArr = new String[1];
            strArr[0] = "单位传真：" +
                        CommonBL.notNull(tLCAddressSchema.getCompanyFax()) +
                        " 变更为：" +
                        CommonBL.notNull(tLPAddressSchema.getCompanyFax());
            tlistTable.add(strArr);
        }

        String[] strArrHead = new String[1];
        strArrHead[0] = "地址变更";
        xmlexport.addListTable(tlistTable, strArrHead);

        String contNo = "";
        for (int i = 1; i <= tLPInsuredSet.size(); i++) {
            if (i != tLPInsuredSet.size()) {
                contNo += tLPInsuredSet.get(i).getContNo() + "，";
            } else {
                contNo += tLPInsuredSet.get(i).getContNo();
            }
        }

        textTag.add("New_Cont_Info", contNo);
        textTag.add("AD_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        return true;
    
    }

    /**
     * 查询特殊数据信息
     * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema：保全项目
     * @param polNo String：险种号
     * @param detailType String：类型
     * @return String
     */
    private String getSpecialData(LPEdorItemSchema tLPEdorItemSchema,
                                  String polNo, String detailType) {
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(tLPEdorItemSchema);
        tEdorItemSpecialData.setPolNo(polNo);
        if (!tEdorItemSpecialData.query()) {
            return null;
        }
        return tEdorItemSpecialData.getEdorValue(detailType);
    }
    
    private boolean getDetailAppAccShiftTo(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccShiftTo");
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        return true;
    }

    private boolean getDetailAppAccTakeOut(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccTakeOut");
        textTag.add("AppAcc_Money", pLCAppAccTraceSchema.getMoney());
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        textTag.add("AppAcc_RealPay",
                    mLPEdorAppSchema.getGetMoney() +
                    pLCAppAccTraceSchema.getMoney());

        return true;
    }

    //查询客户名称
    private String getCustomerName(String customerNo) {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(customerNo);
        if (!tLDPersonDB.getInfo()) {
            mErrors.addOneError("没有查询到受理信息。");
            return "";
        }
        return tLDPersonDB.getName();
    }

    //对没有的保全项目类型生成空打迎数据
    private boolean getDetailForBlankType(LPEdorItemSchema
                                          aLPEdorItemSchema) {
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrtEndorsementBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ManageCom = "86";

        VData tVData = new VData();
        tVData.add(gi);
        PrtInsuEndorsementBL tPrtAppEndorsementBL =
                new PrtInsuEndorsementBL("20061226000008");
        if (!tPrtAppEndorsementBL.submitData(tVData, "")) {
            System.out.println(tPrtAppEndorsementBL.mErrors.getErrContent());
        }

    }
}
