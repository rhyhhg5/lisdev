package com.sinosoft.lis.bq.mm.face;

import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * 邮件接口
 * @realize SendEmailImpl.java
 * @author hyy
 *
 */
public interface ISendEmail {
	/**
	 * 发送邮件
	 * @param address
	 * @param content
	 * @return 成功或者失败
	 */
	public boolean sendEmail(String address,String content,GlobalInput cGlobalInput);

}
