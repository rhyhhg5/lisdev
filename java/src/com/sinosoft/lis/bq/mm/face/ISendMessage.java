package com.sinosoft.lis.bq.mm.face;
/**
 * 短信接口
 * @Realize SendMessageImpl.java
 * @author hyy
 *
 */
public interface ISendMessage {
	/**
	 * 发送短信
	 * @param mobileNumber
	 * @param content
	 * @return 成功或者失败
	 */
	public boolean sendMessage(String mobileNumber,String content);

}
