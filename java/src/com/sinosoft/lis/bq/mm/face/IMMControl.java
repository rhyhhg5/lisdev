package com.sinosoft.lis.bq.mm.face;
/**
 * @Realize MMControlImpl.java
 * 短邮功能响应的控制类
 * @author hyy
 *
 */
public interface IMMControl {
	/**
	 * 短邮功能控制方法
	 * @param FunctionCode
	 * @param ModelCode
	 * @param state
	 * @return 如果返回true 则更新成功 否则失败
	 */
	public boolean functionControl(String FunctionCode,String ModelCode,String state);
	/**
	 * 判断当前模块的当前功能点是否打开了短邮功能
	 * @param FunctionCode
	 * @param ModelCode
	 * @param state
	 * @return true：打开 false：关闭
	 */
	public boolean isOpen(String FunctionCode,String ModelCode);
}
