package com.sinosoft.lis.bq.mm.face;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LIWaitSendMessageSchema;

/**
 * 这个短信邮件的通用接口
 * 调用这个接口直接可以同时发送短信和邮件信息
 * @realize CommonMMImpl.java
 * @author hyy
 *
 */
public interface ICommonMM {
	/**
	 * 同时发送短信和邮件功能的方法
	 * @param waitSchema
	 * @return 短信或者邮件一个发送失败，返回都将失败
	 */
	public boolean sendMM(GlobalInput cGlobalInput);
	/**
	 * 查询待发表中的记录
	 * @param cGlobalInput
	 * @return
	 */
	public boolean queryWaitMessage();
	/**
	 * 同时发送短信和邮件功能 同上边的区别是 
	 * 马上发送短信 不会向待发表插入记录 直接向
	 * serviceRecord插入记录 并且发送短信
	 * @param cGlobalInput
	 * @return
	 */
	public boolean sendMMSoon(GlobalInput cGlobalInput,LIWaitSendMessageSchema soonSchema) ;
	/**
	 * 普通业务发送短信的时 调用的方法
	 * 将会向待发记录表中插入一条记录 
	 * @param cGlobalInput
	 * @param soonSchema
	 * @return true 、false
	 */
	public boolean sendMMLate(GlobalInput cGlobalInput,LIWaitSendMessageSchema lateSchema);
}
