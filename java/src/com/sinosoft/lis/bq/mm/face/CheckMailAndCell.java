package com.sinosoft.lis.bq.mm.face;
/**
 * 这个类主要是检查mail地址和移动电话地址以及号码
 * 的合法性
 * @realize CheckMailAndCellImpl.java
 * @author hyy
 *
 */
public interface CheckMailAndCell {
	/**
	 * 检查邮件地址是否合法
	 * @param mailAddress
	 * @return true，false
	 */
	public boolean checkMail(String mailAddress) ;
	/**
	 * 检查电话号码是否合法
	 * @param cellNumber
	 * @return true，false
	 */
	public boolean checkCellNumber(String cellNumber) ;

}
