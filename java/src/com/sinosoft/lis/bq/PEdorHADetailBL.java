package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全个单分红险红利变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Lanjun, QiuYang
 * @version 1.0
 */

public class PEdorHADetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private DetailDataQuery mQuery = null;

    private String mEdorNo = null;
    private String mPolNo =null;
    private String mInsuredNo=null;
    private String mEdorType = BQ.EDORTYPE_HA;
    //给付对象，0-投保人，1-被保人
    private String HLGetPerson=null;

    private String mContNo = null;

    private LPPolSchema mLPPolSchema = null;

    private LPEdorItemSchema mLPEdorItemSchema=null;
    
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema=null;
    
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("来到了BL层");
        if (!dealData())
        {
            return false;
        }
        System.out.println("完成了BL层");
        
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                    "LPEdorItemSchema", 0);
            mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) cInputData.getObjectByObjectName(
                    "LPEdorEspecialDataSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mContNo = mLPEdorItemSchema.getContNo();
            HLGetPerson=mLPEdorEspecialDataSchema.getEdorValue();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
// 	   String getPolnoSql="select polno from lcpol a where contno='"+mContNo+"' " +
//  		"and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='2')";
// 	   ExeSQL tExeSQL =new ExeSQL();
// 	   String tPolNo=tExeSQL.getOneValue(getPolnoSql);
// 	   System.out.println("险种号："+tPolNo);
// 	   mPolNo=tPolNo;
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
   }


   /**
    * 把保全状态设为已录入
    * @param edorState String
    */
   private void setEdorState(String edorState)
   {
       if(mLPEdorEspecialDataSchema != null)
       {

           mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");
       }
       String sql = "update  LPEdorItem " +
               "set EdorState = '" + edorState + "', " +
//               "    polno  = '"+mPolNo+"', "+
               "    Operator = '" + mGlobalInput.Operator + "', " + 
               "    ModifyDate = '" + mCurrentDate + "', " +
               "    ModifyTime = '" + mCurrentTime + "' " +
               "where  EdorNo = '" + mEdorNo + "' " +
               "and EdorType = '" + mEdorType + "' " +
               "and ContNo = '" + mContNo + "'";
       mMap.put(sql, "UPDATE");
   }


   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }
}
