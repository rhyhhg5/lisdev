package com.sinosoft.lis.bq;

//程序名称：BalanceListDownloadBL.java
//程序功能：万能保单月度结算清单下载
//创建日期：2007-12-17
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class BalanceDownloadBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String[] mTitle = null;
    private String mOutXmlPath = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    private String TITLE = "TITLE";

    public BalanceDownloadBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BalanceListDownloadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            return false;
        }

        //设置标题
        String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][15];
        if(this.mTitle == null)
        {
            mToExcel[0][0] = "管理机构";
            mToExcel[0][1] = "保单号";
            mToExcel[0][2] = "投保人";
            mToExcel[0][3] = "结算年度";
            mToExcel[0][4] = "结息月度";
            mToExcel[0][5] = "本次结息金";
            mToExcel[0][6] = "管理费扣除";
            mToExcel[0][7] = "风险保费扣除";
            mToExcel[0][8] = "持续奖金";
            mToExcel[0][9] = "结算后金额";
            mToExcel[0][10] = "结息日期";
            mToExcel[0][11] = "业务员";
            mToExcel[0][12] = "业务员代码";
            mToExcel[0][13] = "业务员部门";
            mToExcel[0][14] = "投保人手机号";
        }
        else
        {
            mToExcel[0] = mTitle;
        }

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "BalanceListDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("Sql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");

        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "BalanceDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mTitle = (String[])tf.getValueByName(TITLE);

        return true;
    }
}
