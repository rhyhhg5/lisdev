package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.NIDiskImport;
import com.sinosoft.lis.tb.GrpDiskImport;
import com.sinosoft.lis.tb.SIDiskImport;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>增加功能，处理从磁盘导入的多Sheet文档</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @rewrite by Wulg
 * @version 1.0
 */

public class SIAddInsuredList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 批次号 */
    private String mBatchNo = null;

    
    
    
    private LCIllnessInsuredListSchema mLCIllnessInsuredListSchema = null;
    
    
    
    
    /** 团体合同号信息 */
    private LCGrpContSchema mLCGrpContSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 节点名 */
    private String[] sheetName = {"InsuredInfo", "Ver", "RiskInfo"};
    /** 配置文件名 */
    private String configName = "SIDiskImport.xml";

    /**
     * 构造函数，保全入口
     * @param GrpContNo String
     * @param gi GlobalInput
     * @param edorNo StringD:\wuligang\workspace\doc
     */
    public SIAddInsuredList(String GrpContNo, GlobalInput gi, String edorNo,
                          String configName, String[] sheetName) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
        this.mEdorNo = edorNo;
        this.configName = configName;
        this.sheetName = sheetName;
        if ((edorNo != null) && (!edorNo.equals(""))) {
        	LCIllnessInsuredListDB tLPGrpEdorItemDB = new LCIllnessInsuredListDB();
        	
            tLPGrpEdorItemDB.setEdorNo(mEdorNo);
            tLPGrpEdorItemDB.setGrpContNo(GrpContNo);
            LCIllnessInsuredListSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
            if (tLPGrpEdorItemSet.size() > 0) {
            	mLCIllnessInsuredListSchema = tLPGrpEdorItemSet.get(1);
            }
        }
    }

    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public SIAddInsuredList(String GrpContNo, GlobalInput gi) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
    }

    /**
     * 添加传入的多个Sheet数据
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        SIDiskImport importFile = new SIDiskImport(path + fileName,
                path + configName,
                sheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }
        LCIllnessInsuredListSet tLCIllnessInsuredListSet = (LCIllnessInsuredListSet) importFile.
                                             getSchemaSet();
        LCIllnessInsuredListSet tLCInsuredListPolSet = (LCIllnessInsuredListSet)
                importFile.
                getLCInsuredListPolSet();
        this.mResult.add(tLCIllnessInsuredListSet);
        this.mResult.add(tLCInsuredListPolSet);

        //存放Insert Into语句的容器
        MMap map = new MMap();
        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
            deleteInsuredList(map);
        }
        for (int i = 1; i <= tLCIllnessInsuredListSet.size(); i++) {
            //添加一个被保人
            addOneInsured(map, tLCIllnessInsuredListSet.get(i), i);
        }
        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
                                 tLCInsuredListPolSet.get(i).getBankCode());
            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
        }

        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }

    /**
     * 保全增人导入。单sheet added by huxl @ 2006-12-8
     * 保全差异增人。多sheet 导入 modify by qulq 2007-7-24
     * @param path String
     * @param fileName String
     * @param edortype String
     * @return boolean
     */
    public boolean doAdd(String path, String fileName, String edorType) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        SIDiskImport importFile = new SIDiskImport(path + fileName,path + configName,
                sheetName);

        if (!importFile.doImport()) {
            //this.mErrors.copyAllErrors(importFile.mErrrors);
            return false;
        }

        String localVersion = importFile.getVersion();
        String strSql = "select 1 from lccontplandutyparam where calfactor ='CalRule' and calfactorvalue ='4' "
											+ " and contplancode not in ('00','11') and grpcontno ='"+mGrpContNo+"'";
        ExeSQL exeSql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSql.execSQL(strSql);
        if(ssrs.getMaxRow()!=0)
        {
            String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("该保单下有险种为差异算费，请使用差异增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }
        else
        {
        	  String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer2.2'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("导入模板错误请使用普通增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }

        LCIllnessInsuredListSet tLCInsuredListSet = (LCIllnessInsuredListSet) importFile.
                                             getSchemaSet();
        LCIllnessInsuredListSet tLCInsuredListPolSet = (LCIllnessInsuredListSet)
                importFile.getLCInsuredListPolSet();
        this.mResult.add(tLCInsuredListSet);
				this.mResult.add(tLCInsuredListPolSet);
        //存放Insert Into语句的容器
        MMap map = new MMap();
        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
            deleteInsuredList(map);
        }
        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            //添加一个被保人
            addOneInsured(map, tLCInsuredListSet.get(i), i);
        }
        map.put("delete from LCIllnessInsuredList where grpcontno ='"+mGrpContNo+"'","DELETE");
        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
                                 tLCInsuredListPolSet.get(i).getBankCode());
            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
        }
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }


    /**
     * 先删除相关的数据
     * @param map MMap
     */
    private void deleteInsuredList(MMap map) {
        String sql = "delete from LCInsuredList " +
                     "where GrpContNo = '" + mGrpContNo + "' " +
                     "and EdorNo = '" + mEdorNo + "' ";
        map.put(sql, "DELETE");
    }

    private void deleteInsuredListPol(MMap map, String InsuredID,
                                      String RiskCode) {
        String sql = "delete from LCInsuredListPol where GrpContNo = '" + mGrpContNo + "' "
                     + " and InsuredID = '" + InsuredID + "' "
                     +	" and RiskCode = '" + RiskCode + "'"
                     ;
        map.put(sql, "DELETE");
    }

    /**
     * 校验导入数据
     * @param cLCInsuredListSet LCInsuredListSet
     * @return boolean
     */
    private boolean checkData(LCIllnessInsuredListSet cLCInsuredListSet) {
        for (int i = 1; i <= cLCInsuredListSet.size(); i++) {
        	LCIllnessInsuredListSchema schema = cLCInsuredListSet.get(i);
//            String occupationType = schema.getOccupationType();
//            if ((occupationType == null) || (occupationType.equals(""))) {
//                mErrors.addOneError("导入错误，缺少职业类别！");
//                return false;
//            }

            String contPlanCode = schema.getContPlanCode();
            if ((contPlanCode == null) || (contPlanCode.equals(""))) {
                mErrors.addOneError("导入错误，缺少保险计划！");
                return false;
            }
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private void addOneInsured(MMap map,
    		LCIllnessInsuredListSchema cLCInsuredListSchema, int i) {
    	LCIllnessInsuredListSchema tLCInsuredListSchema = cLCInsuredListSchema;
        if (tLCInsuredListSchema.getContNo() == null ||
            tLCInsuredListSchema.getContNo().equals("")) {
            return;
        }
        tLCInsuredListSchema.setGrpContNo(mGrpContNo);
        tLCInsuredListSchema.setState("0"); //0为未生效，1为有效
        tLCInsuredListSchema.setBatchNo(this.mBatchNo);
        //在磁盘投保时合同号码存储合同id
        if (StrTool.cTrim(tLCInsuredListSchema.getContNo()).equals("")) {
            tLCInsuredListSchema.setContNo(String.valueOf(i));
        }
        String edorValiDate = tLCInsuredListSchema.getEdorValiDate();
        if ((edorValiDate == null) || (edorValiDate.equals(""))) {
            if (mLPGrpEdorItemSchema != null) {
                tLCInsuredListSchema.setEdorValiDate(
                        mLPGrpEdorItemSchema.getEdorValiDate());
            }
        }
        tLCInsuredListSchema.setEdorNo(mEdorNo);
        tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListSchema.setMakeDate(mCurrentDate);
        tLCInsuredListSchema.setMakeTime(mCurrentTime);
        tLCInsuredListSchema.setModifyDate(mCurrentDate);
        tLCInsuredListSchema.setModifyTime(mCurrentTime);
        String prem = tLCInsuredListSchema.getPrem();
        if ((prem != null) && (!prem.equals("")) &&
            (Double.parseDouble(prem) > 0)) {
            tLCInsuredListSchema.setPublicAcc(Double.parseDouble(prem));
        }
        //设置计算方向
        if (tLCInsuredListSchema.getPublicAcc() > 0) {
            tLCInsuredListSchema.setCalRule("4");
        }
        map.put(tLCInsuredListSchema, "DELETE&INSERT");
    }

    private void addOneInsuredPol(MMap map,
    		LCIllnessInsuredListSchema
                                  cLCInsuredListPolSchema, int i) {
    	LCIllnessInsuredListSchema tLCInsuredListPolSchema =
                cLCInsuredListPolSchema;
        if (tLCInsuredListPolSchema.getContNo() == null ||
            tLCInsuredListPolSchema.getContNo().equals("")) {
            return;
        }
        if (tLCInsuredListPolSchema.getPrem() == null ||
            tLCInsuredListPolSchema.getPrem().equals("")) {
            tLCInsuredListPolSchema.setPrem("0");
        }
        if (tLCInsuredListPolSchema.getAccName() == null ||
            tLCInsuredListPolSchema.getAccName().equals("")) {
            //tLCInsuredListPolSchema.getAppFlag("0");
        }
        String strPrem =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getPrem()), 2));
        String strAmnt =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getAccName()), 2));

        tLCInsuredListPolSchema.setPrem(strPrem);
       // tLCInsuredListPolSchema.getAccName(strAmnt);
        tLCInsuredListPolSchema.setGrpContNo(
                mGrpContNo);
        tLCInsuredListPolSchema.setMakeDate(mCurrentDate);
        tLCInsuredListPolSchema.setMakeTime(mCurrentTime);
        tLCInsuredListPolSchema.setModifyDate(mCurrentDate);
        tLCInsuredListPolSchema.setModifyTime(mCurrentTime);
        tLCInsuredListPolSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListPolSchema.setGrpContNo(i + "");
        map.put(tLCInsuredListPolSchema, "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.Operator = "endor";
        String grpContNo = "1400003904";
        String path = "Y:\\ui\\temp\\";
        String fileName = "18061117009.xls";
        AddInsuredList tAddInsuredList = new AddInsuredList(grpContNo, tGI);
        if (!tAddInsuredList.doAdd(path, fileName)) {
            System.out.println(tAddInsuredList.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
}
