package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保全申请确认功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PEdorAcceptAppConfirmUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */

    private String mPayPrintParams = "";

    private String mOperate;

    public PEdorAcceptAppConfirmUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        PEdorAcceptAppConfirmBL tPEdorAcceptAppConfirmBL = new
                PEdorAcceptAppConfirmBL();

        if (tPEdorAcceptAppConfirmBL.submitData(cInputData, mOperate) == false)
        {
            System.out.println("错误原因：" +
                    tPEdorAcceptAppConfirmBL.mErrors.getFirstError());
            this.mErrors.copyAllErrors(tPEdorAcceptAppConfirmBL.mErrors);
            return false;
        }
        else
        {
            mResult = tPEdorAcceptAppConfirmBL.getResult();
            mPayPrintParams = tPEdorAcceptAppConfirmBL.getPrtParams();
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public String getPrtParams()
    {
        return mPayPrintParams;
    }

    public static void main(String[] args)
    {
    }
}
