package com.sinosoft.lis.bq;

import com.certicom.net.ssl.a.d;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMComLoanRateDB;
import com.sinosoft.lis.db.LMInterestRateDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LMComLoanRateSchema;
import com.sinosoft.lis.schema.LMInterestRateSchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LMComLoanRateSet;
import com.sinosoft.lis.vschema.LMInterestRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LMCanloanComSetBL {


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;
    
    private String mManageCom = "";
    
    private String mOperater = "";
    
	/** 数据操作字符串 */
	private String mOperate = "";
    
    private LMComLoanRateSchema mLMComLoanRateSchema = new LMComLoanRateSchema();
    
    private LMComLoanRateSchema dLMComLoanRateSchema = new LMComLoanRateSchema();
    
    
    public LMCanloanComSetBL(){
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {   
    	this.mOperate = cOperate;
    	if(!getInputDate(cInputData)){
    		return false;
    	}
        if(!checked()){
        	return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }
    
    /**
     * 医保单贷款校验
     * wujun
     * @return
     */
    public boolean checked(){
    	if(mGlobalInput==null||"".equals(mGlobalInput)){
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "设置错误！";
            this.mErrors.addOneError(tError);        	
            return false;
    	}
    	//查询该管理机构下相同险种现有贷款比例中存在记录
    	String tSql = "";
    	if("INSERT||MAIN".equals(this.mOperate)){
    	tSql = " select * from LMComLoanRate where managecom='"+mLMComLoanRateSchema.getManageCom()+"' and riskcode='"+mLMComLoanRateSchema.getRiskCode()+"' " +
    	" order by int(MinPremLimit) desc fetch first 1 rows only with ur ";
    	}
    	if("DELETE||MAIN".equals(this.mOperate)){
        	tSql = " select * from LMComLoanRate where managecom='"+dLMComLoanRateSchema.getManageCom()+"' and riskcode='"+dLMComLoanRateSchema.getRiskCode()+"' " +
        	" order by int(MinPremLimit) desc fetch first 1 rows only with ur ";    		
    	}
    	LMComLoanRateDB tLMComLoanRateDB = new LMComLoanRateDB();
    	LMComLoanRateSet tLMComLoanRateSet = new LMComLoanRateSet();
    	LMComLoanRateSchema tLMComLoanRateSchema = new LMComLoanRateSchema();
    	tLMComLoanRateSet = tLMComLoanRateDB.executeQuery(tSql);
    	//最小保费金额必须从小往上添加
    	 if("INSERT||MAIN".equals(this.mOperate)){
    		 if(tLMComLoanRateSet!=null&&tLMComLoanRateSet.size()>0){
    			 tLMComLoanRateSchema = tLMComLoanRateSet.get(1);
    			 if(tLMComLoanRateSchema.getMaxPremLimit()!=null&&!"".equals(tLMComLoanRateSchema.getMaxPremLimit())){
    				 if(!mLMComLoanRateSchema.getMinPremLimit().equals(tLMComLoanRateSchema.getMaxPremLimit())){
     		            CError tError = new CError();
    		            tError.moduleName = "LMCanloanComSetBL";
    		            tError.functionName = "checkData";
    		            tError.errorMessage = "最小保费金额必须等于相同机构险种下的最大保费金额！";
    		            this.mErrors.addOneError(tError);        	
    		            return false;     					 
    				 }
    			 }else{
  		            CError tError = new CError();
		            tError.moduleName = "LMCanloanComSetBL";
		            tError.functionName = "checkData";
		            tError.errorMessage = "该机构下的该险种已经录入过贷款比例！";
		            this.mErrors.addOneError(tError);        	
		            return false;      				 
    			 }
    		 }else{
    			 if(!"0".equals(mLMComLoanRateSchema.getMinPremLimit())){
    		            CError tError = new CError();
    		            tError.moduleName = "LMCanloanComSetBL";
    		            tError.functionName = "checkData";
    		            tError.errorMessage = "最小保费金额初始值必须为0！";
    		            this.mErrors.addOneError(tError);        	
    		            return false;    				 
    			 }
    		 }
    	 }
    	 if("DELETE||MAIN".equals(this.mOperate)){
    		 //只能冲最大保费一条一条往下删除
    		 if(tLMComLoanRateSet!=null&&tLMComLoanRateSet.size()>0){
    			 tLMComLoanRateSchema = tLMComLoanRateSet.get(1);
    			 if(tLMComLoanRateSchema.getMaxPremLimit()!=null&&!"".equals(tLMComLoanRateSchema.getMaxPremLimit())){
    				 if(!dLMComLoanRateSchema.getMinPremLimit().equals(tLMComLoanRateSchema.getMinPremLimit())||!dLMComLoanRateSchema.getMaxPremLimit().equals(tLMComLoanRateSchema.getMaxPremLimit())){
    			            CError tError = new CError();
    			            tError.moduleName = "LMCanloanComSetBL";
    			            tError.functionName = "checkData";
    			            tError.errorMessage = "请按照保费从大往小删除该机构下的该险种的贷款比例！";
    			            this.mErrors.addOneError(tError);        	
    			            return false;       					 
    				 }
    			 }else{
    				 if(!dLMComLoanRateSchema.getMinPremLimit().equals(tLMComLoanRateSchema.getMinPremLimit())){
    					 CError tError = new CError();
    					 tError.moduleName = "LMCanloanComSetBL";
    					 tError.functionName = "checkData";
    					 tError.errorMessage = "请按照保费从大往小删除该机构下的该险种的贷款比例！";
    					 this.mErrors.addOneError(tError);        	
    					 return false;          				 
    				 }
    			 }
    		 }else{
		            CError tError = new CError();
		            tError.moduleName = "LMCanloanComSetBL";
		            tError.functionName = "checkData";
		            tError.errorMessage = "您选择的记录不存在！";
		            this.mErrors.addOneError(tError);        	
		            return false;       			 
    		 }
    	 }

    	
    	return true;
    }
    
    private boolean getInputDate(VData cInputData){
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    this.mLMComLoanRateSchema = (LMComLoanRateSchema)tTransferData.getValueByName("tLMComLoanRateSchema");
	    this.dLMComLoanRateSchema = (LMComLoanRateSchema)tTransferData.getValueByName("dLMComLoanRateSchema");
	    if( this.mGlobalInput==null){
            CError tError = new CError();
            tError.moduleName = "LMCanloanComSetBL";
            tError.functionName = "checkData";
            tError.errorMessage = "获取数据错误！";
            this.mErrors.addOneError(tError);        	
            return false;    	    	
	    }
    	return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	if("INSERT||MAIN".equals(this.mOperate)){
    		//赋值LMComLoanRate
    		if(!setLMComLoanRate()){
    			return false;
    		}
    	}
    	if("DELETE||MAIN".equals(this.mOperate)){
    		//删除LMComLoanRate
    		if(!deleteLMComLoanRate()){
    			return false;
    		}
    	}
        return true;
   }

    private boolean deleteLMComLoanRate(){
    	mMap.put(dLMComLoanRateSchema, SysConst.DELETE);
    	return true;
    }
    
    private boolean setLMComLoanRate(){
    	mLMComLoanRateSchema.setOperator(mGlobalInput.Operator);
    	mLMComLoanRateSchema.setMakeDate(PubFun.getCurrentDate());
    	mLMComLoanRateSchema.setMakeTime(PubFun.getCurrentTime());
    	mLMComLoanRateSchema.setModifyDate(PubFun.getCurrentDate());
    	mLMComLoanRateSchema.setModifyTime(PubFun.getCurrentTime());
    	mMap.put(mLMComLoanRateSchema,SysConst.DELETE_AND_INSERT);
    	return true;
    }
    
   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }


}
