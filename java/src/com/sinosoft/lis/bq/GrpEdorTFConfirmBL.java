package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpEdorTFConfirmBL implements EdorConfirm {

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap mMMap = new MMap();
    private CErrors mErrors = new CErrors();
    /**传入容器*/
    private VData mInputData = new VData();
    /**全局参数 */
    private GlobalInput mGlobalInput = null;
    private String mOperate ;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpPolSchema mLCGrpPolSchema = new  LCGrpPolSchema();



    public GrpEdorTFConfirmBL() {
    }

    /**
     * 进行团单复效保全确认业务逻辑的处理并提交数据库。
     * @param cInputData VData：包含：
      A.	GlobalInput对象，完整的登陆用户信息。
      B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate String:此为“”
     * @return boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData())
        {
            return false ;
        }

        if (!dealData())
        {
            return false ;
        }

        return true;
    }

    public VData getResult() {
        VData d = new VData();
        d.add(mMMap);
        return d;
    }

    private boolean getInputData()
    {
        try {
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorItemSchema", 0);
            if (mLPGrpEdorItemSchema == null || mLPGrpEdorItemSchema.equals("") ||
                mGlobalInput == null) {
                mErrors.addOneError("接受数据失败！");
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "GrpEdorTFConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        //设置保单状态
        changeStateFlag();
        delprtmanager();
        dellcgrpcontstate();
        return true ;
    }

    private void changeStateFlag() {
        String updateGrpCont = " update LCGrpCont set StateFlag ='1' ,modifydate=current date,modifytime = current time where grpContno='"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "'";
        String updateGrpPol  = " update LCGrpPol set StateFlag ='1',modifydate=current date ,modifytime=current time where grpContNo ='"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "'";
        String updateLCcont  = " update LCCont set StateFlag ='1' ,modifydate=current date,modifytime = current time where grpContno='"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "'";
        String updateLCPol   = " update LCPol set StateFlag ='1' ,modifydate=current date,modifytime = current time where grpContno='"
                             + mLPGrpEdorItemSchema.getGrpContNo() + "'";
        mMMap.put(updateGrpCont,SysConst.UPDATE);
        mMMap.put(updateGrpPol,SysConst.UPDATE);
        mMMap.put(updateLCcont,SysConst.UPDATE);
        mMMap.put(updateLCPol,SysConst.UPDATE);
    }
    /**
     * 删除打印管理表的数据。
     */
    private void delprtmanager()
    {
        String deleteSQL = " delete from loprtmanager where otherno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"' and code in('21','42')";
        mMMap.put(deleteSQL,SysConst.DELETE);
    }
    /**
     * 删除保单状态表
     */
    private void dellcgrpcontstate()
    {
        String delSQL =" Delete From lcgrpcontstate where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"' ";
        mMMap.put(delSQL,SysConst.DELETE);
    }
}
