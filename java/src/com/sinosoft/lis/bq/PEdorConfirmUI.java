package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全确认</p>
 * <p>Description: 使保全操作生效，把P表的数据放入C表</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorConfirmUI
{
    private PEdorConfirmBL mPEdorConfirmBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorAcceptNo String
     */
    public PEdorConfirmUI(GlobalInput gi, String edorAcceptNo)
    {
        mPEdorConfirmBL = new PEdorConfirmBL(gi, edorAcceptNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorConfirmBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorConfirmBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mPEdorConfirmBL.getMessage();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "endor";
        String edorAcceptNo = "20060223000006";
        PEdorConfirmUI tPEdorConfirmUI = new PEdorConfirmUI(gi,
                edorAcceptNo);
        if (!tPEdorConfirmUI.submitData())
        {
            System.out.println(tPEdorConfirmUI.getError());
        }

    }
}
