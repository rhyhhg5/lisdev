package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * 保全个单新增险种
 * 得到PEdorNSDetailUI.java传入的数据，进行新增险种业务逻辑的处理，
 * 生成险种保全备份数据LPPol，LCDuty，LCPrem。
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Yang Yalin
 * @version 1.3
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PEdorNSDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput(); //完整的用户信息
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema(); //项目信息
    private LPEdorEspecialDataSet mLPEdorEspecialDataSet = null; //险种生效日期

    private LPContSchema mLPContSchema = new LPContSchema(); //保单信息
    private LCPolSet mLCPolSetAdd = null; //新增的险种信息
    private LPPolSet mLPPolSetAdd = null; //新增的险种信息

    private Reflections ref = new Reflections();

    /** 传出数据的容器 */
    private MMap map = new MMap();

    public PEdorNSDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，进行新增险种明细录入逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LPPolSet：新增的险种信息，需要PolNo，InsuredNo，RiskCode，CValiDate即可。
     * @param cOperate 数据操作字符串，可为空""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PGrpEdorAutoUWBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("End PEdorNSDetail PubSubmit OK...");

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data
                           .getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) data
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mLPEdorEspecialDataSet = (LPEdorEspecialDataSet) data
                                     .getObjectByObjectName(
                                         "LPEdorEspecialDataSet", 0);
        }
        catch(Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "PEdorNSDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if(mGlobalInput == null || mLPEdorItemSchema == null
           || mLPEdorEspecialDataSet == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSDetailBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "输入数据有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("Time " + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime() + ": "
                           + mGlobalInput.Operator + " ");

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 将P表原数据删除后，复制C表数据到P表，目的在于为核保铺平道路
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!dealCont())
        {
            return false;
        }
        //先删除原P表数据
        if(!deleteOldPolAdd())
        {
            return false;
        }

        if(!dealCValiDate())
        {
            return false;
        }


        if(!dealPolAdd())
        {
            return false;
        }

        if(!dealDutyAdd())
        {
            return false;
        }

        if(!dealPremAdd())
        {
            return false;
        }

        mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT); //录入完成
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mLPEdorItemSchema);
        
        map.put(mLPEdorItemSchema, "DELETE&INSERT");
        
        if( mLCPolSetAdd.get(1).getRiskCode().equals("232701")){
            ExeSQL tExSql = new ExeSQL();    
            String str = "select a.curpaytodate from ljapayperson a, lcpol b where  a.contno= b.contno and a.curpaytodate = b.paytodate and b.contno ='"+mLPEdorItemSchema.getContNo() +"' and a.polno= b.mainpolno ";  
            String spaytodate = tExSql.getOneValue(str);
            System.out.println("HHHH"+spaytodate);
            
            LCPolDB aLCPolDB = new LCPolDB();
            aLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
            aLCPolDB.setAppFlag(BQ.APPFLAG_EDOR); //保全增加附加险
            LCPolSet tLCPolSetAdd = new LCPolSet();
            tLCPolSetAdd = aLCPolDB.query();
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema  = tLCPolSetAdd.get(1);
            tLCPolSchema.setCValiDate(spaytodate);
            int tappntAge = PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),
            		spaytodate, "Y");
            tLCPolSchema.setInsuredAppAge(tappntAge);
            tLCPolSchema.setInsuredNo(tLCPolSchema.getAppntNo());
            tLCPolSchema.setInsuredName(tLCPolSchema.getAppntName());
            int year = tLCPolSchema.getInsuYear();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date  currdate = null;
             try {
            	 currdate = format.parse(spaytodate);
    			
    		} catch (ParseException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		Calendar ca = Calendar.getInstance();
    		ca.setTime(currdate);
    		ca.add(Calendar.YEAR, year);// 
    		currdate = ca.getTime();
            format.format(currdate);
            tLCPolSchema.setEndDate(format.format(currdate));
            tLCPolSchema.setPayEndDate(format.format(currdate));
            map.put(tLCPolSchema, "UPDATE");
            
              LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
              
              LCAppntDB aLCAppntDB = new LCAppntDB();
              aLCAppntDB.setContNo(mLPEdorItemSchema.getContNo());
              aLCAppntDB.setAppntNo(tLCPolSchema.getAppntNo());
              LCAppntSchema tLCAppntSchema = new LCAppntSchema();
              LCAppntSet tLCAppntSet = new LCAppntSet();
              tLCAppntSet = aLCAppntDB.query();
              tLCAppntSchema = tLCAppntSet.get(1);
              tLCInsuredSchema.setContNo(tLCAppntSchema.getContNo());
              tLCInsuredSchema.setGrpContNo(tLCAppntSchema.getGrpContNo());
              tLCInsuredSchema.setAppntNo(tLCAppntSchema.getAppntNo());
              tLCInsuredSchema.setInsuredNo(tLCAppntSchema.getAppntNo());  //客户号
              tLCInsuredSchema.setAddressNo(tLCAppntSchema.getAddressNo()); //客户号
               tLCInsuredSchema.setName(tLCAppntSchema.getAppntName());              //姓名
               tLCInsuredSchema.setSex(tLCAppntSchema.getAppntSex());                //性别
               tLCInsuredSchema.setBirthday(tLCAppntSchema.getAppntBirthday());      //出生日期
         //年龄，不提交
               tLCInsuredSchema.setIDType(tLCAppntSchema.getIDType());          //证件类型
               tLCInsuredSchema.setIDNo(tLCAppntSchema.getIDNo());              //证件号码
               tLCInsuredSchema.setNativePlace(tLCAppntSchema.getNativePlace());//国籍
               tLCInsuredSchema.setNativeCity(tLCAppntSchema.getNativeCity());//
               tLCInsuredSchema.setRgtAddress(tLCAppntSchema.getRgtAddress());  //户口所在地
               tLCInsuredSchema.setMarriage(tLCAppntSchema.getMarriage());      //婚姻状况
               tLCInsuredSchema.setNationality(tLCAppntSchema.getNationality());//民族
               tLCInsuredSchema.setDegree(tLCAppntSchema.getDegree());          //学历
               tLCInsuredSchema.setSmokeFlag(tLCAppntSchema.getSmokeFlag());    //是否吸烟
               ExeSQL tExSql2 = new ExeSQL();    
               String strRelationToMainInsured = "select RelationToAppnt from  lcinsured  where  contno ='"+mLPEdorItemSchema.getContNo() +"' and insuredno =(select distinct insuredno from lcpol where polno=mainpolno and  contno  ='"+mLPEdorItemSchema.getContNo() +"'  and riskcode <>'232701' fetch first 1 rows only )";  
               String sRelationToMainInsured = tExSql2.getOneValue(strRelationToMainInsured);
                tLCInsuredSchema.setRelationToMainInsured(sRelationToMainInsured);
                tLCInsuredSchema.setRelationToAppnt("00");
                tLCInsuredSchema.setPrtNo(tLCAppntSchema.getPrtNo());
                tLCInsuredSchema.setManageCom(tLCAppntSchema.getManageCom());
                tLCInsuredSchema.setExecuteCom(tLCAppntSchema.getManageCom());
                tLCInsuredSchema.setOperator(tLCAppntSchema.getOperator());
  //               LCAddressDB aLCAddressDB = new LCAddressDB();
//               aLCAddressDB.setCustomerNo(tLCAppntSchema.getAppntNo());
//               aLCAddressDB.setAddressNo(tLCAppntSchema.getAddressNo());
//               LCAddressSchema tLCAddressSchema = new LCAddressSchema();
//               LCAddressSet tLCAddressSet = new LCAddressSet();
//               tLCAddressSet = aLCAddressDB.query();
//               tLCAddressSchema = tLCAddressSet.get(0);
              
               tLCInsuredSchema.setPosition(tLCAppntSchema.getPosition());         //
          //     tLCInsuredSchema.setZipCode(tLCAddressSchema.getZipCode());        //邮政编码
           //    tLCInsuredSchema.setHomeAddress("");//住址
         //      tLCInsuredSchema.setHomeZipCode(request.getParameter("HomeZipCode"));//住址邮政编码
         //      tLCInsuredSchema.setPhone(request.getParameter("Phone"));            //联系电话（1）
         //      tLCInsuredSchema.setPhone2(request.getParameter("Phone2"));          //联系电话（2）
         //        tLCInsuredSchema.setMobile(request.getParameter("Mobile"));          //移动电话
         //      tLCInsuredSchema.setEMail(request.getParameter("EMail"));            //电子邮箱
         //      tLCInsuredSchema.setGrpName(request.getParameter("GrpName"));        //工作单位
         //      tLCInsuredSchema.setGrpPhone(request.getParameter("GrpPhone"));      //单位电话
         //      tLCInsuredSchema.setGrpAddress(request.getParameter("GrpAddress"));  //单位地址
               tLCInsuredSchema.setIDStartDate(tLCAppntSchema.getIDStartDate());  //
               tLCInsuredSchema.setIDEndDate(tLCAppntSchema.getIDEndDate()); 
               
               tLCInsuredSchema.setWorkType(tLCAppntSchema.getWorkType());      //职业（工种）
               tLCInsuredSchema.setPluralityType(tLCAppntSchema.getPluralityType());         //兼职（工种）
               tLCInsuredSchema.setOccupationType(tLCAppntSchema.getOccupationType());       //职业类别
               tLCInsuredSchema.setOccupationCode(tLCAppntSchema.getOccupationCode());       //职业代码
               tLCInsuredSchema.setMakeDate(PubFun.getCurrentDate());
               tLCInsuredSchema.setMakeTime(PubFun.getCurrentTime());
               tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
               tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
               tLCInsuredSchema.setHealth(tLCAppntSchema.getHealth());
               map.put(tLCInsuredSchema, "DELETE&INSERT");
            }

        return true;
    }

    /**
     * 存储险种生效日期
     * @return boolean:成功true
     */
    private boolean dealCValiDate()
    {
        map.put(this.mLPEdorEspecialDataSet, "INSERT");

        String cValiDate = null;
        for(int i = 1; i <= mLPEdorEspecialDataSet.size(); i++)
        {
            String endDate = null;  //险种终止时间，取保本单年度末，若险种为长期险，则不需要处理

            LPEdorEspecialDataSchema schema = mLPEdorEspecialDataSet.get(i);
            String sql = "select a.RiskType5 "
                         + "from LMRiskApp a, LCPol b "
                         + "where a.riskCode = b.riskCode "
                         + "   and b.polNo = '" + schema.getPolNo() + "'  ";
            String riskType5 = new ExeSQL().getOneValue(sql);

            //长期险只能下一周年日生效
            if(riskType5.equals(BQ.RISKTYPE5_LONGTIME)
               || riskType5.equals(BQ.RISKTYPE5_FOREVER))
            {
                //计算当前保单年度
                String upPolYears = getUpContYears(mLPEdorItemSchema.getEdorValiDate(),getLastPayToDate());
                //new EdorCalZT()
                  //                  .getUpPolYears(mLPEdorItemSchema);
                if(upPolYears == null)
                {
                    mErrors.addOneError("计算保单年度出错");
                    return false;
                }
                sql = "select date('" + mLPContSchema.getCValiDate()
                      + "') + " + upPolYears + " year "
                      + "from dual ";
                cValiDate = new ExeSQL().getOneValue(sql);
                //新增340601险种，即时生效
                if(schema.getEdorValue().equals("1"))
                {
                    cValiDate = mLPEdorItemSchema.getEdorValiDate(); //即刻生效取保全生效日期

                }
            }

            //短期险
            if(riskType5.equals(BQ.RISKTYPE5_LESSTHANYEAR)
               || riskType5.equals(BQ.RISKTYPE5_YEAR))
            {
                if(schema.getEdorValue().equals("1"))
                {
                    cValiDate = mLPEdorItemSchema.getEdorValiDate(); //即刻生效取保全生效日期
                    sql = "select max(curPayToDate) "
                          + "from LJAPayPerson a, LCPol b "
                          + "where a.polNo = b.polNo "
                          + "   and a.curPayToDate = b.payToDate "
                          + "   and a.contNo = '"
                          + mLPEdorItemSchema.getContNo() + "' ";
                    String curPayToDate = new ExeSQL().getOneValue(sql);
                    if(curPayToDate.equals("") || curPayToDate.equals("null"))
                    {
                        CError tError = new CError();
                        tError.moduleName = "PEdorNSDetailBL";
                        tError.functionName = "dealCValiDate";
                        tError.errorMessage = "查询保单应缴出错";
                        mErrors.addOneError(tError);
                        return false;
                    }

                    int intv = PubFun.calInterval(
                        curPayToDate, mLPEdorItemSchema.getEdorValiDate(), "D");
                    if(intv > 0)
                    {
                        mErrors.addOneError("险种生效日期为即刻生效，生效日不能大于交至日期"); //长期险可为保单失效日
                        return false;
                    }
                }
                else if(schema.getEdorValue().equals("2"))
                {
                    cValiDate = getLastPayToDate();  //下期生效取其他险种的缴至日期
                    if(cValiDate == null)
                    {
                        return false;
                    }
                }

                endDate = getRealEndDate();
            }

            if(!updateCValiDate(schema.getPolNo(), cValiDate))
            {
                return false;
            }

            if(endDate != null)
            {
                if(!updateEndDate(schema.getPolNo(), endDate))
                {
                    return false;
                }
            }
        }

        if(!getLCPolAdd())
        {
            return false;
        }

        return true;
    }

    /**
     * updateEndDate
     * 更新险种截止时间，此处直接提交，为后续查询险种做准备
     * @param polNo String
     * @param endDate String
     */
    private boolean updateEndDate(String polNo, String endDate)
    {
        MMap tMMap = new MMap();

        String sql = "update LCPrem a " +
                     "set payEndDate = '" + endDate + "' "
                     + "where polNo = '" + polNo + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新责任
        sql = "update LCDuty a " +
              " set (payEndDate, endDate) = "
              + "      (select min(payEndDate), min(payEndDate) "
              + "      from LCPrem "
              + "      where polNo = a.polNo and dutyCode = a.dutyCode) "
              + "where PolNo = '" + polNo + "' ";
        tMMap.put(sql, "UPDATE");

        //更新险种
        sql = "update LCPol a " +
              " set (payEndDate, endDate) = "
              + "      (select min(payEndDate), min(endDate) "
              + "      from LCDuty "
              + "      where polNo = a.polNo) "
              + "where PolNo = '" + polNo + "' ";
        tMMap.put(sql, "UPDATE");

        VData data = new VData();
        data.add(tMMap);

        PubSubmit tSubmit = new PubSubmit();
        if(!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PGrpEdorAutoUWBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * getRealEndDate
     * 得到险种终止日期，取本保单年度末日期
     * @return String：本保单年度末日期
     */
    private String getRealEndDate()
    {
        String upPolYears = getUpContYears(mLPEdorItemSchema.getEdorValiDate(),getLastPayToDate());
        if(upPolYears == null)
        {
            mErrors.addOneError("计算保单年度出错");
            return null;
        }

        String sql = "select date('" + mLPContSchema.getCValiDate()
              + "') + " + upPolYears + " year "
              + "from dual ";
        return new ExeSQL().getOneValue(sql);
    }
    /**
     * getUpContYears
     * @return String
     */
    private String getUpContYears(String edorValiDate,String minPayToDate)
    {
            //这里要减去一天，这样才能算出保单年度
       String sql = "select case when (date('"+edorValiDate+"') - 1 days) > (date('"+minPayToDate+"')-1 days ) "
                    +" then (date('"+minPayToDate+"')-1 days)"
                    +" else  (date('"+edorValiDate+"') - 1 days) end from dual ";
       ExeSQL e = new ExeSQL();
       String tEdorValiDate = e.getOneValue(sql);
       if(tEdorValiDate == null || tEdorValiDate.equals(""))
       {
           return null;
       }
       //因为这里计算出的保单年度是从0开始的，所以下面要加一
       int years = PubFun.calInterval(mLPContSchema.getCValiDate(), tEdorValiDate, "Y");

       return String.valueOf(years + 1);

    }
    /**
     * 计算本保单其他险种的交至日期
     * @return String：本保单其他险种的交至日期
     */
    private String getLastPayToDate()
    {
        String sql = "select min(payToDate) "
                     + "from LCPol "
                     + "where contNo = '" + mLPContSchema.getContNo() + "' "
                     + "   and appFlag = '" + BQ.APPFLAG_SIGN + "' "
                     + " and StateFlag != '"+BQ.STATE_FLAG_TERMINATE+"' ";
        String curPayToDate = new ExeSQL().getOneValue(sql);
        if(curPayToDate.equals("") || curPayToDate.equals("null"))
        {
            mErrors.addOneError("查询原保单险种交至日期失败");
            return null;
        }
        return curPayToDate;
    }

    /**
     * 使用cValiDate修改新险种信息（传入cValiDate调用契约修改类）
     * 立即提交，在生成P表数据时使用
     * @param polNo：险种号；cValiDate：险种生效日期
     * @return boolean：成功true，否认false
     */
    private boolean updateCValiDate(String polNo, String cValiDate)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPContSchema.getContNo());
        tLCContDB.getInfo();

        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContDB.getContNo());
        tLCAppntDB.getInfo();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        tLCPolDB.getInfo();
        String scvilidate =tLCPolDB.getCValiDate();

        //计算保障期间
        int intvDays = PubFun.calInterval(scvilidate,
                       tLCPolDB.getEndDate(), "D");
        if( tLCPolDB.getRiskCode().equals("232701")|| tLCPolDB.getRiskCode().equals("232901")){
          //tLCPolDB.setCValiDate(cValiDate);
        	if (null != cValiDate && !cValiDate.equals("")){
        		tLCPolDB.setCValiDate(cValiDate);
        	}
        }else {
        	tLCPolDB.setCValiDate(cValiDate);
        }

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(tLCPolDB.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();

        for(int i = 1; i <= tLCDutySet.size(); i++)
          {
           String endDate = PubFun.calDate(tLCPolDB.getCValiDate(),
                                 intvDays, "D", null);
           tLCDutySet.get(i).setEndDate(endDate);
          }

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCContDB.getContNo());
        if( tLCPolDB.getRiskCode().equals("232701") ){
            String sql = "select InsuredNo "
                + "from LCPol "
                + "where contNo = '" + mLPContSchema.getContNo() + "' "
                + "   and appFlag = '" + BQ.APPFLAG_SIGN + "' "
                + " and StateFlag != '"+BQ.STATE_FLAG_TERMINATE+"'  and riskcode<>'232901' ";
            String sinsuredno = new ExeSQL().getOneValue(sql);
        	tLCInsuredDB.setInsuredNo(sinsuredno);
      
        } else {
        tLCInsuredDB.setInsuredNo(tLCPolDB.getInsuredNo());
        }
        tLCInsuredDB.getInfo();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("EdorType", "NS");
        tTransferData.setNameAndValue("SavePolType", "2");

//        tTransferData.setNameAndValue("getIntv","1"); //领取间隔（方式）
//        tTransferData.setNameAndValue("GetDutyKind", ""); //年金开始领取年龄
//        tTransferData.setNameAndValue("samePersonFlag", "0"); //投保人同被保人标志
//        tTransferData.setNameAndValue("deleteAccNo", "0");
//        tTransferData.setNameAndValue("ChangePlanFlag", "0");


        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tLCContDB.getSchema());
        tVData.add(tLCAppntDB.getSchema());
        tVData.addElement(tLCInsuredDB.getSchema());
        tVData.add(tLCPolDB.getSchema());
        tVData.add(tLCDutySet);
        tVData.addElement(new LCInsuredRelatedSet());
        tVData.add(tTransferData);

        ProposalBL tProposalBL1 = new ProposalBL();
        if(!tProposalBL1.submitData(tVData, "UPDATE||PROPOSAL"))
        {
            mErrors.copyAllErrors(tProposalBL1.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 删除原P表险种数据
     * @return boolean：成功true
     */
    private boolean deleteOldPolAdd()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LPPolSet set = tLPPolDB.query();

        if(set.size() > 0)
        {
            LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            tLPEdorItemSchema.setEdorType(mLPEdorItemSchema.getEdorType());

            for(int i = 1; i <= set.size(); i++)
            {
                tLPEdorItemSchema.setPolNo(set.get(i).getPolNo());
                MMap tMMap = CommonBL.delPolInfoFromP(tLPEdorItemSchema);
                if(tMMap == null)
                {
                    mErrors.addOneError("准备删除保全险种信息出错:"
                                        + tLPEdorItemSchema.getPolNo());
                    return false;
                }
                map.add(tMMap);
            }
        }

        return true;
    }

    /**
     * dealDutyAdd
     *将责任信息放到P表
     * @return boolean：成功true
     */
    private boolean dealPremAdd()
    {
        for(int i = 1; i <= mLPPolSetAdd.size(); i++)
        {
            LCPremDB tLCPremDB = new LCPremDB();
            tLCPremDB.setPolNo(mLPPolSetAdd.get(i).getPolNo());
            LCPremSet set = tLCPremDB.query();

            LPPremSet tLPPremSet = new LPPremSet();
            for(int t = 1; t <= set.size(); t++)
            {
                LPPremSchema schema = new LPPremSchema();

                ref.transFields(schema, set.get(t));
                schema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                schema.setEdorType(mLPEdorItemSchema.getEdorType());
                schema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(mLPContSchema);

                tLPPremSet.add(schema);
            }
            map.put(tLPPremSet, "INSERT");
        }

        return true;
    }

    /**
     * dealDutyAdd
     *将责任信息放到P表
     * @return boolean：成功true
     */
    private boolean dealDutyAdd()
    {
        for(int i = 1; i <= mLPPolSetAdd.size(); i++)
        {
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(mLPPolSetAdd.get(i).getPolNo());
            LCDutySet set = tLCDutyDB.query();

            LPDutySet tLPDutySet = new LPDutySet();
            for(int t = 1; t <= set.size(); t++)
            {
                LPDutySchema schema = new LPDutySchema();

                ref.transFields(schema, set.get(t));
                schema.setEdorNo(mLPEdorItemSchema.getEdorNo());
                schema.setEdorType(mLPEdorItemSchema.getEdorType());
                schema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(mLPContSchema);

                tLPDutySet.add(schema);
            }
            map.put(tLPDutySet, "INSERT");
        }

        return true;
    }

    /**
     * 将保单信息放到P表
     * @return boolean：成功true
     */
    private boolean dealCont()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if(tLCContDB.getInfo())
        {
            ref.transFields(mLPContSchema, tLCContDB.getSchema());
            mLPContSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
            mLPContSchema.setEdorType(mLPEdorItemSchema.getEdorType());
            mLPContSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(mLPContSchema);
        }
        else
        {
            mErrors.addOneError("没有查询到保单信息");
            return false;
        }

        map.put(mLPContSchema, "DELETE&INSERT");

        return true;
    }

    /**
     * 处理新增的险种，将险种信息放到P表，为核保做准备
     * @return boolean：处理成功true
     */
    private boolean dealPolAdd()
    {
        //将险种信息放到P表
        mLPPolSetAdd = new LPPolSet();
        for(int i = 1; i <= mLCPolSetAdd.size(); i++)
        {
            LPPolSchema schema = new LPPolSchema();

            ref.transFields(schema, mLCPolSetAdd.get(i));
            schema.setEdorNo(mLPEdorItemSchema.getEdorNo());
         //   schema.setCValiDate(mLPEdorItemSchema.getEdorNo());
            schema.setEdorType(mLPEdorItemSchema.getEdorType());
            schema.setOperator(mGlobalInput.Operator);
            if (mLCPolSetAdd.get(i).getRiskCode().equals("232701"))
            {
            schema.setInsuredNo(mLCPolSetAdd.get(i).getAppntNo());
            schema.setInsuredName(mLCPolSetAdd.get(i).getAppntName());
            LCAppntDB aLCAppntDB = new LCAppntDB();
            aLCAppntDB.setContNo(mLPEdorItemSchema.getContNo());
       //     LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            LCAppntSet tLCAppntSet = new LCAppntSet();
            tLCAppntSet = aLCAppntDB.query();
            
            schema.setInsuredBirthday(tLCAppntSet.get(i).getAppntBirthday());
            schema.setInsuredSex(tLCAppntSet.get(i).getAppntSex());

            }
            PubFun.fillDefaultField(schema);

            mLPPolSetAdd.add(schema);
        }
        map.put(mLPPolSetAdd, "INSERT");
        
        

        return true;
    }

    /**
     * 得到新增的险种
     */
    private boolean getLCPolAdd()
    {
        LCPolDB aLCPolDB = new LCPolDB();
        aLCPolDB.setContNo(mLPEdorItemSchema.getContNo());
        aLCPolDB.setAppFlag(BQ.APPFLAG_EDOR); //保全增加附加险
        mLCPolSetAdd = aLCPolDB.query();
        if(mLCPolSetAdd.size() == 0)
        {
            mErrors.addOneError("请先录入新险种信息。");
            return false;
        }

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        if(!checkState())
        {
            return false;
        }

        if(!checkHeathImp())
        {
            return false;
        }

        if(!getLCPolAdd())
        {
            return false;
        }

        if(mLPEdorEspecialDataSet.size() != mLCPolSetAdd.size())
        {
            mErrors.addOneError("请录入各险种的生效日期");
            return false;
        }

        return true;
    }

    /**
     * 校验是否录入了健康告知
     * @return boolean：录入了健康告知true
     */
    private boolean checkHeathImp()
    {
        String sql = "select distinct insuredNo "
                     + "from LCPol a "
                     + "where appFlag = '" + BQ.APPFLAG_EDOR + "' "
                     + "   and contNo = '"
                     + mLPEdorItemSchema.getContNo() + "' "
                     + "   and insuredNo not in "
                     + "      (select customerNo from LPCustomerImpart "
                     + "      where contNo = a.contNo "
                     + "         and edorNo = '"
                     + mLPEdorItemSchema.getEdorNo() + "' "
                     + "         and edorType = '"
                     + mLPEdorItemSchema.getEdorType() + "') "
                     + "   and not exists "
                     + "      (select 1 from LMRiskApp "
                     + "      where riskCode = a.riskCode "
                     + "         and RiskType = 'M') ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() > 0)
        {
            String insuredNos = "";
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                insuredNos += tSSRS.GetText(i, 1) + ", ";
            }
            mErrors.addOneError("以下被保人还未录入健康告知，请录入:" + insuredNos);
            return false;
        }

        return true;
    }

    /**
     * 校验项目状态
     * @return boolean：若不能录入明细，返回false
     */
    private boolean checkState()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if(tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLPEdorItemDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorNSDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "无保全申请数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPEdorItemSchema = tLPEdorItemSet.get(1);

        if(BQ.EDORSTATE_CAL.equals(mLPEdorItemSchema.getEdorState())
           || BQ.EDORSTATE_CONFIRM.equals(mLPEdorItemSchema.getEdorState()))
        {
            // @@错误处理
            mErrors.copyAllErrors(mLPEdorItemSchema.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorNSDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该保全已经理算或确认不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("20070110000002");
        tLPEdorItemSchema.setContNo("00001287301");
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_NS);

        LPEdorEspecialDataSet mLPEdorEspecialDataSet = new
            LPEdorEspecialDataSet();
        String[] pols = {"11000097454"};

        for(int i = 0; i < pols.length; i++)
        {
            LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new
                LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema.setEdorAcceptNo(tLPEdorItemSchema.
                getEdorNo());
            tLPEdorEspecialDataSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPEdorEspecialDataSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_CVALIDATETYPE);
            tLPEdorEspecialDataSchema.setEdorValue("1");
            tLPEdorEspecialDataSchema.setPolNo(pols[i]);
            mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
        }
        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemSchema);
        tVData.add(mLPEdorEspecialDataSet);

        PEdorNSDetailBL tGrPEdorNSDetailBL = new PEdorNSDetailBL();
        if(!tGrPEdorNSDetailBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrPEdorNSDetailBL.mErrors.getErrContent());
        }

    }
}
