package com.sinosoft.lis.bq;

import java.io.*;
import java.util.*;
import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.GetMemberInfo;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Tjj
 * @version 1.0
 */

public class PrtAppEndorsementBL {
    private Document myDocument;

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //保全受理号 By WangJH
    private String mEdorAcceptNo = "";

    //批单号
    private String mEdorNo = "";
    private String mContNo = "";
    private int mSameCM = 0;
    private int mSameAD = 0;
    private int mSameEW = 0;
    private int mSameAU = 0;
    private String sqlSelectAddress ="" ;
    //业务处理相关变量
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private ListTable tlistTable;
    private MMap mMap = new MMap();
    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    //By WangJH
    public PrtAppEndorsementBL(String aEdorAcceptNo) {
        mEdorAcceptNo = aEdorAcceptNo;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }
        return true;
    }

    /**
     * 生成数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean getSubmitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 得到MMap
     * @return MMap
     */
    public MMap getMMap() {
        return mMap;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("\n\nStart Write Print Data\n\n");
        boolean tFlag = false;

        //最好紧接着就初始化xml文档
        xmlexport.createDocuments("PrtAppEndorsementApp.vts", mGlobalInput);

        //从2层查询变更为3层查询--By WangJH
        //增加了保全受理号，提升到定层的保全申请主表LPEdorApp开始查找
        //保全申请主表LPEdorApp->个险保全批改表LPEdorMain->个险保全项目表LPEdorItem
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
        tLPEdorAppSet.set(tLPEdorAppDB.query());
        if (tLPEdorAppSet == null || tLPEdorAppSet.size() < 1) {
            buildError("dealData", "在LPEdorApp中无相关保全受理号的数据");
            return false;
        }
        mLPEdorAppSchema = tLPEdorAppSet.get(1);

        xmlexport.addDisplayControl("displayHead");

        String tCustomerNo = "";
        textTag.add("EdorAcceptNo", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCode1", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getMakeDate()));
        setFixedInfo();

        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            tCustomerNo = mLPEdorAppSchema.getOtherNo();
            String sql = "select * from LPAppnt where edorno in (select edorno from lpedormain where edoracceptno='" +
                         mLPEdorAppSchema.getEdorAcceptNo() + "')";
            System.out.println("sql 1 : " + sql);
            LPAppntDB tLPAppntDB = new LPAppntDB();
            LPAppntSet tLPAppntSet = tLPAppntDB.executeQuery(sql);
            if (tLPAppntSet != null && tLPAppntSet.size() > 0) {
                tCustomerNo = tLPAppntSet.get(1).getAppntNo();
            }
            textTag.add("CustomerNo", tCustomerNo);
        } else if (mLPEdorAppSchema.getOtherNoType().equals("3")) {
            String sql = "select * from lpcont where contno='" +
                         mLPEdorAppSchema.getOtherNo() +
                         "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                         mLPEdorAppSchema.getEdorAcceptNo() + "')";
            System.out.println("sql 2 : " + sql);
            LPContDB tLPContDB = new LPContDB();
            LPContSet tLPContSet = tLPContDB.executeQuery(sql);
            if (tLPContSet == null || tLPContSet.size() < 1) {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mLPEdorAppSchema.getOtherNo());
                if (!tLCContDB.getInfo()) {
                    buildError("dealData", "保全申请的合同不存在");
                    return false;
                }
                tCustomerNo = tLCContDB.getAppntNo();
            } else {
                tCustomerNo = tLPContSet.get(1).getAppntNo();
            }
            textTag.add("CustomerNo", tCustomerNo);
        }

        String sql = "select * from LPPerson where CustomerNo='" + tCustomerNo +
                     "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                     mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 3 : " + sql);
        LPPersonSchema tLPPersonSchema = new LPPersonSchema();
        LPPersonDB tLPPersonDB = new LPPersonDB();
        LPPersonSet tLPPersonSet = tLPPersonDB.executeQuery(sql);
        if (tLPPersonSet == null || tLPPersonSet.size() < 1) {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tCustomerNo);
            if (!tLDPersonDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LDPersonDB不存在相应记录!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPPersonSchema, tLDPersonSchema);
        } else {
            tLPPersonSchema = tLPPersonSet.get(1).getSchema();
        }
        textTag.add("EdorAppName", tLPPersonSchema.getName());
        textTag.add("EdorAppSex", CommonBL.decodeSex(tLPPersonSchema.getSex()));
        textTag.add("AppntName", tLPPersonSchema.getName());

        sql = "select * from LPAddress where CustomerNo='" + tCustomerNo +
              "' and edorno in (select edorno from lpedormain where edoracceptno='" +
              mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 4 : " + sql);
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        LPAddressDB tLPAddressDB = new LPAddressDB();
        LPAddressSet tLPAddressSet = tLPAddressDB.executeQuery(sql);
        if (tLPAddressSet == null || tLPAddressSet.size() < 1) {
            //得到第一个保全申请保单的合同号
            LPEdorMainDB db = new LPEdorMainDB();
            db.setEdorAcceptNo(mEdorAcceptNo); ;
            LPEdorMainSet set = db.query();
            if (set.size() <= 0) {
                mErrors.addOneError("没有查到受理号" + mEdorAcceptNo
                                    + "所对应的保全申请LPEdorApp记录");
                return false;
            }

            //得到保单的地址信息(地址号)
            String sqledortyp = " select edortype From lpedoritem where edoracceptno ='"+mLPEdorAppSchema.getEdorAcceptNo()+"'";
            String edorType = new ExeSQL().getOneValue(sqledortyp);
            if(edorType.equals("RB"))
            {
                 sqlSelectAddress =
                   "select * "
                   + "from LCAddress "
                   + "where customerNo = '" + tCustomerNo + "' "
//                   + "    and addressNo ='" + tLCAppntDB.getAddressNo() + "' "
                   ;

            }else
            {
                LCAppntDB tLCAppntDB = new LCAppntDB();
                tLCAppntDB.setContNo(set.get(1).getContNo());
                tLCAppntDB.getInfo();

                //得到该合同该地址号的地址信息
                 sqlSelectAddress =
                        "select * "
                        + "from LCAddress "
                        + "where customerNo = '" + tCustomerNo + "' "
                        + "    and addressNo ='" + tLCAppntDB.getAddressNo() + "' ";
            }
            
            if(edorType.equals("CF"))
            {
            	LPAppntDB tLPAppntDB = new LPAppntDB();
            	LPAppntSet tLPAppntSet = new LPAppntSet();
            	tLPAppntDB.setEdorNo(set.get(1).getEdorNo());
            	tLPAppntSet=tLPAppntDB.query();
            	
            	sqlSelectAddress =
                    "select * "
                    + "from LCAddress "
                    + "where customerNo = '" + tCustomerNo + "' "
                    + "    and addressNo ='" + tLPAppntSet.get(1).getAddressNo() + "' ";
            }
            
            System.out.println("sql 5 : " + sqlSelectAddress);
            LCAddressDB tLCAddressDB = new LCAddressDB();

            LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(
                    sqlSelectAddress);
            if (tLCAddressSet == null || tLCAddressSet.size() < 1) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "未找到投保人地址信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCAddressSchema tLCAddressSchema =
                    tLCAddressSet.get(1).getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPAddressSchema, tLCAddressSchema);
        } else {
            tLPAddressSchema = tLPAddressSet.get(1).getSchema();
        }
        textTag.add("EdorAppZipCode", tLPAddressSchema.getZipCode());
        textTag.add("EdorAppAddress", tLPAddressSchema.getPostalAddress());

        //查询个险保全项目表中保全受理号为mEdorAcceptNo的记录
        String AppSQL = "select * from LPEdorMain where EdorAcceptNo ='" +
                        mEdorAcceptNo +
                        "'order by MakeDate,MakeTime";
        System.out.println("sql 6 : " + AppSQL);
        //--End

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        //tLPEdorMainDB.setEdorNo(mEdorNo);    //By WangJH
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.executeQuery(AppSQL); //By WangJH
        if (0 == tLPEdorMainSet.size()) {
            buildError("dealData", "在LPEdorMain中无相关批单号的数据");
            return false;
        }

        //增加了一层循环 By WangJH
        for (int i = 1; i <= tLPEdorMainSet.size(); i++) {
            LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(i);
            mContNo = tLPEdorMainSchema.getContNo(); //取得保全记录的合同号
            mEdorNo = tLPEdorMainSchema.getEdorNo(); //取得保全记录的批单号 By WangJH
            LCContSchema tLCContSchema = new LCContSchema();
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo()) {
                LBContDB tLBContDB = new LBContDB();
                tLBContDB.setContNo(mContNo);
                if (!tLBContDB.getInfo()) {
                    return false;
                }
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLCContSchema, tLBContDB.getSchema());
            } else {
                tLCContSchema.setSchema(tLCContDB.getSchema());
            }
            //业务员信息
            String agntPhone = "";
            String temPhone = "";
            String agentCode = null;
            LPContDB tLPContDB = new LPContDB();
            tLPContDB.setContNo(mContNo);
            tLPContDB.setEdorNo(mEdorNo);
            tLPContDB.setEdorType("PR");//个单迁移
            if (tLPContDB.getInfo()) {
                agentCode = tLPContDB.getAgentCode();
            }
            if (agentCode == null) {
                agentCode = tLCContSchema.getAgentCode();
            }
            LAAgentDB tLaAgentDB = new LAAgentDB();
            tLaAgentDB.setAgentCode(agentCode);
            tLaAgentDB.getInfo();
            textTag.add("AgentName", tLaAgentDB.getName());
            //textTag.add("AgentCode", tLaAgentDB.getAgentCode());
            textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
            temPhone = tLaAgentDB.getMobile();
            if (temPhone == null || temPhone.equals("") ||
                temPhone.equals("null")) {
                agntPhone = tLaAgentDB.getPhone();
            } else {
                agntPhone = temPhone;
            }
            textTag.add("Phone", agntPhone);

            //机构信息
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContSchema.getAgentGroup());
            tLABranchGroupDB.getInfo();
            String branchSQL =
                    " select * from LABranchGroup where agentgroup = "
                    +
                    " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                    + " (select agentgroup from laagent where agentcode ='"
                    + tLaAgentDB.getAgentCode() + "'))"
                    ;
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                    branchSQL);
            if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
                CError.buildErr(this, "查询业务员机构失败");
                return false;
            }

            textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            tLPEdorItemDB.setContNo(mContNo);
            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            for (int j = 1; j <= tLPEdorItemSet.size(); j++) {
                LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(j);
                //遗失补发--By WangJH
                if (tLPEdorItemSchema.getEdorType().equals("LR")) {
                    xmlexport.addDisplayControl("displayLR");
                    if (!this.getDetailLR(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //犹豫退保--By WangJH
                else if (tLPEdorItemSchema.getEdorType().equals("WT")) {
                    xmlexport.addDisplayControl("displayWT");
                    if (!this.getDetailWT(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //受益人资料变更
                else if (tLPEdorItemSchema.getEdorType().equals("BC")) {
                    xmlexport.addDisplayControl("displayBC");
                    if (!this.getDetailBC(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //保障保费调整
                else if (tLPEdorItemSchema.getEdorType().equals("BF")) {
                    xmlexport.addDisplayControl("displayBF");
                    if (!this.getDetailBF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                } 
                else if (tLPEdorItemSchema.getEdorType().equals("EW")) {
                	if(mSameEW==0){
                		xmlexport.addDisplayControl("displayEW");
                		if (!this.getDetailEW(tLPEdorItemSchema)) {
                			return false;
                		}
                		tFlag = true;
                		mSameEW++;
                	}
                }
                else if (tLPEdorItemSchema.getEdorType().equals("AD")) {
                	if(mSameAD==0){
                		xmlexport.addDisplayControl("displayAD");
                		if (!this.getDetailAD(tLPEdorItemSchema)) {
                			return false;
                		}
                		tFlag = true;
                		mSameAD++;
                	}
                }
                //更换投保人
                else if (tLPEdorItemSchema.getEdorType().equals("AE")) {
                    xmlexport.addDisplayControl("displayAE");
                    if (!this.getDetailAE(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                } else if (tLPEdorItemSchema.getEdorType().equals("CC")) {
                    xmlexport.addDisplayControl("displayCC");
                    if (!getDetailCC(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                } else if (tLPEdorItemSchema.getEdorType().equals(BQ.
                        EDORTYPE_NS)) {
                    xmlexport.addDisplayControl("displayNS");
                    if (!this.getDetailNS(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                    mSameCM++;
                } else if (tLPEdorItemSchema.getEdorType().equals("CF")) {
                    xmlexport.addDisplayControl("displayCF");
                    if (!getDetailCF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //客户基本资料变更
                else if (tLPEdorItemSchema.getEdorType().equals("CM")) {
                    if (mSameCM == 0) {
                        //有多条CM项目时只进入getDetailCM一次
                        xmlexport.addDisplayControl("displayCM");
                        if (!this.getDetailCM(tLPEdorItemSchema)) {
                            return false;
                        }
                        tFlag = true;
                        mSameCM++;
                    }
                }
             // 客户信息授权变更
				else if (tLPEdorItemSchema.getEdorType().equals("AU")) {
					if (mSameAU == 0) {
						// 有多条AU项目时只进入getDetailAU一次
						xmlexport.addDisplayControl("displayAU");
						if (!this.getDetailAU(tLPEdorItemSchema)) {
							return false;
						}
						tFlag = true;
						mSameAU++;
					}
				}
                //中止合同
                else if (tLPEdorItemSchema.getEdorType().equals("CT")) {
                    xmlexport.addDisplayControl("displayCT");
                    if (!this.getDetailCT(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //保全回退
                else if(tLPEdorItemSchema.getEdorType().equals("RB")){
                    xmlexport.addDisplayControl("displayRB");
                    if(!this.getDetailRB(tLPEdorItemSchema))
                    {
                        return false ;
                    }
                    tFlag = true ;
                }
                //投保事项变更
                else if (tLPEdorItemSchema.getEdorType().equals("TB")) {
                    xmlexport.addDisplayControl("displayTB");
                    if (!this.getDetailTB(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //协议退保
                else if (tLPEdorItemSchema.getEdorType().equals("XT")) {
                    xmlexport.addDisplayControl("displayXT");
                    if (!this.getDetailXT(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //预收保费
                else if (tLPEdorItemSchema.getEdorType().equals("YS")) {
                    xmlexport.addDisplayControl("displayYS");
                    if (!this.getDetailYS(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //交费频次变更
                else if (tLPEdorItemSchema.getEdorType().equals("PC")) {
                    xmlexport.addDisplayControl("displayPC");
                    if (!this.getDetailPC(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //减额
                else if (tLPEdorItemSchema.getEdorType().equals("PT")) {
                    xmlexport.addDisplayControl("displayPT");
                    if (!this.getDetailPT(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //2006-10-24 qulq添加保单复效
                else if (tLPEdorItemSchema.getEdorType().equals("FX")) {
                    xmlexport.addDisplayControl("displayFX");
                    if (!this.getDetailFX(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;

                }
                //2017-1-16 qulq添加保单复效
                else if (tLPEdorItemSchema.getEdorType().equals("MF")) {
                    xmlexport.addDisplayControl("displayMF");
                    if (!this.getDetailMF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;

                }
                //2008-06-06 caozeng添加特权保单复效
                else if (tLPEdorItemSchema.getEdorType().equals("TF")) {
                    xmlexport.addDisplayControl("displayTF");
                    if (!this.getDetailTF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;

                }
                 else if (tLPEdorItemSchema.getEdorType().equals("PR")) {
                    xmlexport.addDisplayControl("displayPR");
                    if (!this.getDetailPR(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                // 增加各险满期终止项目
                //add by hyy
                }else if (tLPEdorItemSchema.getEdorType().equals("ZF")) {
					   xmlexport.addDisplayControl("displayZF");
					   if (!this.getDetailZF(tLPEdorItemSchema)) {
						           return false;
					   }
                                           tFlag = true;
				}
                //万能险部分领取 080414 zhanggm
                else if (tLPEdorItemSchema.getEdorType().equals("LQ")) {
                    xmlexport.addDisplayControl("displayLQ");
                    if (!this.getDetailLQ(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //个险万能险保障调整 080527 pst
                else if (tLPEdorItemSchema.getEdorType().equals("BA")) {
                    xmlexport.addDisplayControl("displayBA");
                    if (!this.getDetailBA(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //个险万能险保费追交调整 080528 pst
                else if (tLPEdorItemSchema.getEdorType().equals("ZB")) {
                    xmlexport.addDisplayControl("displayZB");
                    if (!this.getDetailZB(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //保单冻结2009-7-1 fuxin
                else if (tLPEdorItemSchema.getEdorType().equals("FC")){
                    xmlexport.addDisplayControl("displayFC");
                    if (!this.getDetailFC(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }

                //万能期交保费调整 090706 zhanggm
                else if (tLPEdorItemSchema.getEdorType().equals("BP")) {
                    xmlexport.addDisplayControl("displayBP");
                    if (!this.getDetailBP(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }

                //保单解冻 2009-7-13 fuxin
                else if (tLPEdorItemSchema.getEdorType().equals("GF")) {
                    xmlexport.addDisplayControl("displayGF");
                    if (!this.getDetailGF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }

//              万能续期补费 20110808 xp
                else if (tLPEdorItemSchema.getEdorType().equals("WX")) {
                    xmlexport.addDisplayControl("displayWX");
                    if (!this.getDetailWX(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //保单贷款 20111103 xp
                else if (tLPEdorItemSchema.getEdorType().equals("LN")) {
                    xmlexport.addDisplayControl("displayLN");
                    if (!this.getDetailLN(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
//              保单还款 20111103 xp
                else if (tLPEdorItemSchema.getEdorType().equals("RF")) {
                    xmlexport.addDisplayControl("displayRF");
                    if (!this.getDetailRF(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                else if (tLPEdorItemSchema.getEdorType().equals("DD")) {
                    xmlexport.addDisplayControl("displayDD");
                    if (!getDetailDD(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //康乐人生转保
                else if (tLPEdorItemSchema.getEdorType().equals("TP")) {
                    xmlexport.addDisplayControl("displayTP");
                    if (!getDetailTP(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                //分红险满期领取
                else if (tLPEdorItemSchema.getEdorType().equals("HA")) {
//                    xmlexport.addDisplayControl("displayHA02");
                    if (!getDetailHA(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
                else { //对没有的保全项目类型生成空打印数据
                    if (!this.getDetailForBlankType(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }
            }
        } //增加的一层循环 By WangJH
        sql = "select * from ljsgetendorse where 1=1 and EndorsementNo ='" +
              this.mLPEdorAppSchema.getEdorAcceptNo() + "'";
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        System.out.println("SQL 电风扇  " + sql);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(sql);
        System.out.println(tLJSGetEndorseSet.size());
        if (tLJSGetEndorseSet == null || tLJSGetEndorseSet.size() < 1) {
            System.out.println("没有收退费记录");
        } else {
            this.xmlexport.addDisplayControl("displayMN");
            double tmoney = this.mLPEdorAppSchema.getGetMoney();
            if (tmoney < 0.0) {
                this.textTag.add("MN_Type", "应退");
                this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(Math.abs(tmoney),"0.00"));
            } else if (tmoney > 0.0) {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", CommonBL.bigDoubleToCommonString(tmoney,"0.00"));
            } else {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", 0);
            }
        }
        //   if ((this.mLPEdorAppSchema.getGetMoney()-0.0)>-0.000001&&(this.mLPEdorAppSchema.getGetMoney()-0.0)<0.00001)
        //   {
        //       this.xmlexport.addDisplayControl("displayMN");
        //   }
        AppAcc tAppAcc = new AppAcc();
        String noticeType = "";
        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            noticeType = BQ.NOTICETYPE_P;
        }
        if (mLPEdorAppSchema.getOtherNoType().equals("2")) {
            noticeType = BQ.NOTICETYPE_G;
        }
        LCAppAccTraceSchema tLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(
                mLPEdorAppSchema.
                getOtherNo(), mLPEdorAppSchema.getEdorAcceptNo(), noticeType);
        if (tLCAppAccTraceSchema != null) {
            if (tLCAppAccTraceSchema.getAccType().equals("0")) {
                if (!this.getDetailAppAccTakeOut(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
            if (tLCAppAccTraceSchema.getAccType().equals("1")) {
                if (!this.getDetailAppAccShiftTo(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
        } else {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }

        if (!tFlag) {
            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }

        mResult.clear();

        //生成主打印批单schema
        LPEdorAppPrintSchema tLPEdorAppPrintSchemaMain = new
                LPEdorAppPrintSchema();
        tLPEdorAppPrintSchemaMain.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorAppPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorAppPrintSchemaMain.setPrtFlag("N");
        tLPEdorAppPrintSchemaMain.setPrtTimes(0);
        tLPEdorAppPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorAppPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorAppPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("D:\\", "bqxml_ZHJ");
        tLPEdorAppPrintSchemaMain.setEdorInfo(ins);
        sql = "delete from LPEdorAppPrint " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "'";
        System.out.println(sql);
        mMap.put(sql, "DELETE");
        mMap.put(tLPEdorAppPrintSchemaMain, "BLOBINSERT");
        mResult.addElement(mMap);
        try {
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true ;
        }

        /**
         * 工单回退 add by fuxin
         * @param aLPEdorItemSchema LPEdorItemSchema
         * @return boolean
         */

        private boolean getDetailRB(LPEdorItemSchema aLPEdorItemSchema)
        {
            String edorNo = aLPEdorItemSchema.getEdorAcceptNo();
            String edorType = aLPEdorItemSchema.getEdorType();
            String contNo = aLPEdorItemSchema.getContNo();
            String edorAppDate = aLPEdorItemSchema.getEdorAppDate();
            String edorValidate = aLPEdorItemSchema.getEdorValiDate();
            tlistTable = new ListTable();
            tlistTable.setName("RB");
            String[] strArray = new String[5] ;
            String sql =" select c.innersource,a.insuredname,d.polno,d.getmoney ,(select edorvalidate from lpedoritem where edorno = c.innersource) from lppol a,lpedorapp b,lgwork c , ljsgetendorse d "
                       +" where a.edorno = b.edoracceptno "
                       +" and b.edoracceptno = c.workno  "
                       +" and a.polno = d.polno "
                       +" and edorno='"+edorNo+"' order by a.insuredname";
            SSRS result = new ExeSQL().execSQL(sql);
            if (result == null || result.getMaxRow()==0)
            {
                mErrors.addOneError("查询数据失败！");
                return false;
            }
            for (int i=1; i<=result.getMaxRow() ; i++)
            {
                strArray = new String[5];
                strArray[0] = result.GetText(i, 1);
                strArray[1] = result.GetText(i, 2);
                strArray[2] = result.GetText(i, 3);
                strArray[3] = result.GetText(i, 4);
                strArray[4] = result.GetText(i, 5);
                tlistTable.add(strArray);
            }
            String odlsql = " select edorValidate from lpedoritem where  edoracceptno =(select innersource from lgwork where workno ='"+edorNo+"') ";
            String odlEdorValidate = new ExeSQL().getOneValue(odlsql);
            textTag.add("Money",100);
            textTag.add("EdorNo",edorNo);
            textTag.add("EdorValidate",CommonBL.decodeDate(PubFun.calDate(edorValidate,0,"D",null)));  //本次保全生效日期
            textTag.add("odlEdorValidate",odlEdorValidate);
            xmlexport.addListTable(tlistTable,strArray);
        return true;
        }
        
        //康乐人生转保
    private boolean getDetailTP(LPEdorItemSchema aLPEdorItemSchema){
        tlistTable = new ListTable();
        tlistTable.setName("TP");
        
        String riskSQL = "select (select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code1 ), " +
        		" (select riskname from lmriskapp where riskcode = (select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code1 )), " +
        		" (select prem from lcpol where polno=lppol.polno and riskcode  =(select code from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code1 ) ), " +
        		"  riskcode ," +
        		" (select riskname from lmriskapp where riskcode=lppol.riskcode), " +
        		" prem " +
        		" from lppol " +
        		" where edorno='"+aLPEdorItemSchema.getEdorAcceptNo()+"' " +
        		" and exists (select 1 from ldcode1 where codetype = 'riskedortp' and codename='riskcode' and lppol.riskcode=code1 ) ";

        System.out.println(riskSQL);
        SSRS tSSRS = new ExeSQL().execSQL(riskSQL);
        if (tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailNS";
            tError.errorMessage = "查询险种信息失败";
            mErrors.addOneError(tError);
            return false;
        }
        for(int i =1;i<=tSSRS.getMaxRow();i++){
        	String[] info = new String[1];
        	info[0] = "保单" + aLPEdorItemSchema.getContNo()
        	+ "下的原【"+tSSRS.GetText(i, 1)+"-"+tSSRS.GetText(i, 2)+"】险种已转保为" 
        	+ "【"+tSSRS.GetText(i, 4)+"-"+tSSRS.GetText(i, 5)+"】，保费由" + tSSRS.GetText(i, 3)
        	+ "元变更为" + tSSRS.GetText(i, 6)
        	+ "元，后续续期费用按转保后的险种费率缴纳。";
        	
        	tlistTable.add(info);
        	
        }
        
        //查询保单是否存在豁免险
        String hmRiskSQL = "select riskcode, " +
        		" (select riskname from lmriskapp where riskcode=lppol.riskcode) , " +
        		" (select prem from lcpol where contno=lppol.contno and riskcode=lppol.riskcode), " +
        		" (select amnt from lcpol where contno=lppol.contno and riskcode=lppol.riskcode), " +
        		" prem,amnt from lppol where riskcode='231401' and edortype='TP'  " +
        		" and edorno='"+aLPEdorItemSchema.getEdorAcceptNo()+"' with ur  ";
        
        System.out.println(hmRiskSQL);
        SSRS tSSRS1 = new ExeSQL().execSQL(hmRiskSQL);
        if (tSSRS1.getMaxRow() > 0) {
        	for(int i =1;i<=tSSRS1.getMaxRow();i++){
        		String[] info1 = new String[1];
        		info1[0] = "保单" + aLPEdorItemSchema.getContNo()
        		+ "下的原【"+tSSRS1.GetText(i, 1)+"-"+tSSRS1.GetText(i, 2)+"】险种由于主险转保，保额由" 
        		+tSSRS1.GetText(i, 4)+"元变为"+tSSRS1.GetText(i, 6)+"元，保费由" + tSSRS1.GetText(i, 3)
        		+ "元变更为" + tSSRS1.GetText(i, 5)
        		+ "元，后续续期费用按转保后的险种费率缴纳。";
        		
        		tlistTable.add(info1);
        	}
        }
        
        textTag.add("TP_ContNo", aLPEdorItemSchema.getContNo());
    	
        xmlexport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);
    	return true;
    }

    // 各险终止缴费
	// add by hyy
	private boolean getDetailZF(LPEdorItemSchema aLPEdorItemSchema) {
		String edorNo = aLPEdorItemSchema.getEdorNo();
		String edorType = aLPEdorItemSchema.getEdorType();
		String contNo = aLPEdorItemSchema.getContNo();
		String date = aLPEdorItemSchema.getEdorAppDate();
                tlistTable = new ListTable();
                tlistTable.setName("ZF");
                String[] strArray = new String[4];
		// 险种号码
		// 查询出表格要显示的内容
		String sql2 = "select riskseqno,lp.RiskCode ,(select lm.RiskName from lmrisk lm where lm.RiskCode = lp.RiskCode ) ,lp.PaytoDate "
				+ "from LPPol lp "
				+ "where lp.edorNo ='"
				+ edorNo
				+ "'"
				+ "and lp.edortype ='"
				+ edorType
				+ "'"
				+ "and lp.contNo ='"
				+ contNo + "'";
		SSRS result = new ExeSQL().execSQL(sql2);
		if (result == null || result.getMaxRow() == 0) {
			mErrors.addOneError("为找到查询信息！");
			return false;
		}
		for (int i = 1; i <= result.getMaxRow(); i++) {
                    strArray = new String[4];
			strArray[0] = result.GetText(i, 1);
			strArray[1] = result.GetText(i, 2);
			strArray[2] = result.GetText(i, 3);
			strArray[3] = result.GetText(i, 4);
                        tlistTable.add(strArray);
		}
		String sql = "select AppntName from LCAppnt where contNo = '" + contNo
				+ "'";
		String AppntName = new ExeSQL().getOneValue(sql);
		if (AppntName == null) {
			mErrors.addOneError("为找到查询信息！");
			return false;
		}
		textTag.add("ZFnumber", contNo);
		textTag.add("ZFdate", date);
		textTag.add("ZFclientName", AppntName);
                xmlexport.addListTable(tlistTable,strArray);

		return true;
	}
    /**
     * 设置批单显示的固定信息
     */
    private void setFixedInfo() {
        //查询受理人
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorAcceptNo);
        tLGWorkDB.getInfo();
        textTag.add("ApplyName", tLGWorkDB.getApplyName());

        //查询受理渠道
        if (tLGWorkDB.getAcceptWayNo() != null) {
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("acceptwayno");
            tLDCodeDB.setCode(tLGWorkDB.getAcceptWayNo());
            tLDCodeDB.getInfo();
            textTag.add("AcceptWay", tLDCodeDB.getCodeName());
        }

        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLGWorkDB.getAcceptorNo());
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));

        textTag.add("Operator",
                    GetMemberInfo.getMemberName(mGlobalInput.Operator));
        textTag.add("Acceptor",
                    GetMemberInfo.getMemberName(tLGWorkDB.getAcceptorNo()));
    }

    //保单遗失补发
    private boolean getDetailLR(LPEdorItemSchema aLPEdorItemSchema) {
        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("相关的个人保单表中没有记录！"));
            return false;
        }
        tLCContSchema = tLCContDB.getSchema();
        textTag.add("LR_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("LR_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        textTag.add("LR_LostTimes", tLCContSchema.getLostTimes() + 1);
        textTag.add("LR_GB",aLPEdorItemSchema.getGetMoney());
        if (aLPEdorItemSchema.getGetMoney() > 0) {
            textTag.add("LR_GetMoney",
                        "本次变更，投保人需补费" + aLPEdorItemSchema.getGetMoney() + "元。");
        }

        return true;
    }
    
//  保单贷款 20111103 xp
    private boolean getDetailLN(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String tPolNo=new ExeSQL().getOneValue("select polno from lpedorespecialdata where edorno='" + edorNo + "' and detailtype='LOAN'");
        
      EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,edorType);
      if(!tSpecialData.query())
      {
      	mErrors.addOneError("未找到本次保全信息！");
          return false;
      }
      textTag.add("CashValue", tSpecialData.getEdorValue("CASHVALUE"));
      textTag.add("CanLoan", tSpecialData.getEdorValue("CANLOAN"));
      tSpecialData.setPolNo(tPolNo);
      textTag.add("Loan", tSpecialData.getEdorValue("LOAN"));
      textTag.add("LastLoan", tSpecialData.getEdorValue("LOAN"));
        
        LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setPolNo(tPolNo);
    	if(!tLCPolDB.getInfo())
    	{
    		CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailBA";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() + "下无相应的险种信息!";
            this.mErrors.addOneError(tError);
    	}
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema=tLCPolDB.getSchema();
        textTag.add("ContNo", tLCPolSchema.getContNo());
        textTag.add("Appnt", tLCPolSchema.getAppntName());
        textTag.add("AppntNo", tLCPolSchema.getAppntNo());
        textTag.add("Insured", tLCPolSchema.getInsuredName());
        textTag.add("InsuredNo", tLCPolSchema.getInsuredNo());
        
        String checkSql = "select 1 from ldcode where codetype = 'loancom' and code = " +
        " (select case when managecom = '86' then managecom else substr(managecom,1,4) end " +
         " from lccont where contno='"+tLCPolSchema.getContNo()+"') with ur  " ; 
       String  checkResult = new ExeSQL().getOneValue(checkSql);
       String sql = "";
       if(checkResult!=null&&!"".equals(checkResult)){
    	    sql = "  select round(rate*100,2) from lminterestrate where startdate<=current date " +
    	   " and enddate>=current date  and busstype='L' " +
    	   " and comcode = (select case when managecom = '86' then managecom else substr(managecom,1,4) end "
    	   + "  from  lccont where contno='"+tLCPolSchema.getContNo()+"') " +
    	   " and int(minpremlimit)<= (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"' ) " +
    	   " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"') or maxpremlimit is null or maxpremlimit='') " +
    	   " and riskcode =(select riskcode from lcpol where polno='" + tPolNo + "' ) ";
       }else{
    	   sql = "  select round(rate*100,2) from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
    		   + " and a.comcode = '86' and a.riskcode =(select riskcode from lcpol where contno='" + tLCPolSchema.getContNo() + "' and stateflag='1' " 
    		   + " and int(minpremlimit)<= (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"' ) "
    		   + " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"') or maxpremlimit is null or maxpremlimit='') "
    		   + " and riskcode in (select riskcode from LMLoan) order by riskcode fetch first 1 row only) ";
       }

  	  	String rate=new ExeSQL().getOneValue(sql);
  	  	if(rate==null||"".equals(rate)){
			// @@错误处理
			System.out.println("PEdorLNAppConfirmBL+getLPLoan++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNAppConfirmBL";
			tError.functionName = "getLPLoan";
			tError.errorMessage = "获取贷款利率失败";
			mErrors.addOneError(tError);
			return false;  	  		
  	  	}
  	  	textTag.add("Rate", CommonBL.carry(rate));
  	  	
		LPLoanDB tLPLoanDB=new LPLoanDB();
		tLPLoanDB.setEdorNo(edorNo);
		tLPLoanDB.setEdorType(edorType);
		tLPLoanDB.setPolNo(tPolNo);
		if(!tLPLoanDB.getInfo()){
			// @@错误处理
			System.out.println("PEdorLNAppConfirmBL+getLPLoan++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNAppConfirmBL";
			tError.functionName = "getLPLoan";
			tError.errorMessage = "获取贷款表失败";
			mErrors.addOneError(tError);
			return false;
		}
		LPLoanSchema tLPLoanSchema=tLPLoanDB.getSchema();
		textTag.add("LoanDate", CommonBL.decodeDate(tLPLoanSchema.getLoanDate()));
		textTag.add("PayOffDate", CommonBL.decodeDate(tLPLoanSchema.getPayOffDate()));
        return true;
    }
    
//  保单还款 20111103 xp
    private boolean getDetailRF(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        
        LPReturnLoanDB tLPReturnLoanDB = new LPReturnLoanDB();
		LPReturnLoanSet tLPReturnLoanSet = new LPReturnLoanSet();
		LPReturnLoanSchema tLPReturnLoanSchema = new LPReturnLoanSchema();
		tLPReturnLoanDB.setEdorNo(edorNo);
		tLPReturnLoanDB.setEdorType(edorType);
		tLPReturnLoanSet = tLPReturnLoanDB.query();
		if (tLPReturnLoanSet.size() != 1) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorLNConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询保全贷款记录信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		tLPReturnLoanSchema = tLPReturnLoanSet.get(1);
        
        LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setPolNo(tLPReturnLoanSchema.getPolNo());
    	if(!tLCPolDB.getInfo())
    	{
    		CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailBA";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() + "下无相应的险种信息!";
            this.mErrors.addOneError(tError);
    	}
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema=tLCPolDB.getSchema();
        textTag.add("ContNo", tLCPolSchema.getContNo());
        textTag.add("Appnt", tLCPolSchema.getAppntName());
        textTag.add("AppntNo", tLCPolSchema.getAppntNo());
        textTag.add("Insured", tLCPolSchema.getInsuredName());
        textTag.add("InsuredNo", tLCPolSchema.getInsuredNo());
        
        String checkSql = "select 1 from ldcode where codetype = 'loancom' and code = " +
        " (select case when managecom = '86' then managecom else substr(managecom,1,4) end " +
         " from lccont where contno='"+tLCPolSchema.getContNo()+"') with ur  " ; 
       String  checkResult = new ExeSQL().getOneValue(checkSql);
       String sql = "";
       if(checkResult!=null&&!"".equals(checkResult)){
    	    sql = "  select round(rate*100,2) from lminterestrate where startdate<=current date " +
    	   " and enddate>=current date  and busstype='L' " +
    	   " and comcode = (select case when managecom = '86' then managecom else substr(managecom,1,4) end "
    	   + "  from  lccont where contno='"+tLCPolSchema.getContNo()+"') " +
    	   " and int(minpremlimit)<= (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"' ) " +
    	   " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"') or maxpremlimit is null or maxpremlimit='') " +
    	   " and riskcode =(select riskcode from lcpol where polno='" + tLPReturnLoanSchema.getPolNo() + "' ) ";
       }else{
    	   sql = "  select round(rate*100,2) from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
    		   + " and a.comcode = '86' and a.riskcode =(select riskcode from lcpol where contno='" + tLCPolSchema.getContNo() + "' and stateflag='1' " 
    		   + " and int(minpremlimit)<= (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"' ) "
    		   + " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+tLCPolSchema.getContNo()+"') or maxpremlimit is null or maxpremlimit='') "
    		   + " and riskcode in (select riskcode from LMLoan) order by riskcode fetch first 1 row only) ";
       }         	
  	  	
  	  	String rate=new ExeSQL().getOneValue(sql);
  	  	if(rate==null||"".equals(rate)){
  	   		CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailBA";
            tError.errorMessage = "贷款利率获取失败!";
            this.mErrors.addOneError(tError);
            return false;
  	  	}
  	  	textTag.add("Rate", CommonBL.carry(rate));
  	  	
		textTag.add("LoanMoney", tLPReturnLoanSchema.getSumMoney());
		textTag.add("LoanDate", CommonBL.decodeDate(tLPReturnLoanSchema.getLoanDate()));
		textTag.add("PayOffDate", CommonBL.decodeDate(PubFun.calDate(tLPReturnLoanSchema.getLoanDate(), 6, "M", null)));
		textTag.add("TruePayOffDate", CommonBL.decodeDate(tLPReturnLoanSchema.getPayOffDate()));
		textTag.add("Interest", tLPReturnLoanSchema.getReturnInterest());
		String tSumMoney=String.valueOf(CommonBL.carry(tLPReturnLoanSchema.getReturnInterest()+tLPReturnLoanSchema.getReturnMoney()));
		textTag.add("SumMoney", tSumMoney);
        return true;
    }
    


    /**
     * 受益人变更
     * 论何种情况下，均显示被变更险种（相同被保人）的所有受益人的信息。
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailBC(LPEdorItemSchema aLPEdorItemSchema) {
        //得到险种名称
        tlistTable = new ListTable();
        tlistTable.setName("BC");

        String sql;
        String insuredHasNoBnf = "";
        ExeSQL tExeSQL = new ExeSQL();

        textTag.add("BC_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("BC_InsuredNo", aLPEdorItemSchema.getInsuredNo());

        //查询保全受益人表
        //目前dealData中不支持对同一个保单的同一项目的LPEdorItem循环，故在此做循环
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(aLPEdorItemSchema.getEdorAcceptNo());
        tLPEdorItemDB.setEdorType(aLPEdorItemSchema.getEdorType());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int temp = 1; temp <= tLPEdorItemSet.size(); temp++) {
            aLPEdorItemSchema = tLPEdorItemSet.get(temp);
            LPBnfDB tLPBnfDB = new LPBnfDB();
            tLPBnfDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPBnfDB.setEdorType(aLPEdorItemSchema.getEdorType());
            tLPBnfDB.setContNo(aLPEdorItemSchema.getContNo());
            tLPBnfDB.setInsuredNo(aLPEdorItemSchema.getInsuredNo());
            LPBnfSet tLPBnfSet = tLPBnfDB.query();
            if (tLPBnfDB.mErrors.needDealError()) {
                buildError("getDetailBC", "查询变更后的受益人出错");
                return false;
            }

            if (tLPBnfSet.size() == 0) {
                insuredHasNoBnf += getInsuredName(aLPEdorItemSchema)
                        + "(" + aLPEdorItemSchema.getInsuredNo() + ") ";
                getInsuredName(aLPEdorItemSchema);
                continue;
            }

            for (int i = 1; i <= tLPBnfSet.size(); i++) {
                LPBnfSchema tLPBnfSchema = tLPBnfSet.get(i).getSchema();
                String[] strArr = new String[11];
                strArr[0] = String.valueOf(i);
                String tInsured = "";
                sql = "select Name from LCInsured where InsuredNo='" +
                      tLPBnfSchema.getInsuredNo() + "' and ContNo='" +
                      tLPBnfSchema.getContNo() + "'";
                tInsured = tExeSQL.getOneValue(sql);
                sql = "select RiskCode from LCPol " +
                      "where PolNo = '" + tLPBnfSchema.getPolNo() + "'";
                String riskCode = tExeSQL.getOneValue(sql);
                strArr[1] = riskCode;
                strArr[2] = tInsured;
                strArr[3] = ChangeCodeBL.getCodeName("BnfType",
                        tLPBnfSchema.getBnfType());
                strArr[4] = tLPBnfSchema.getName();
                strArr[5] = ChangeCodeBL.getCodeName("Sex", tLPBnfSchema.getSex());
                strArr[6] = ChangeCodeBL.getCodeName("IDType",
                        tLPBnfSchema.getIDType());
                strArr[7] = StrTool.cTrim(tLPBnfSchema.getIDNo());
                strArr[8] = ChangeCodeBL.getCodeName("Relation",
                        tLPBnfSchema.
                        getRelationToInsured());
                strArr[9] = ChangeCodeBL.getCodeName("BnfGrade",
                        tLPBnfSchema.getBnfGrade());
                strArr[10] = String.valueOf(tLPBnfSchema.getBnfLot());
                tlistTable.add(strArr);
            }
        }
        String edorName = "§受益人变更";
        String contNotice = " 保单" + aLPEdorItemSchema.getContNo()
                            + "资料变更如下:\n";

        if (!insuredHasNoBnf.equals("")) {
            textTag.add("BC_HasNoBnf", edorName + "\n" + contNotice
                        + "以下被保人变更为法定：" + insuredHasNoBnf);
        }
        if (tlistTable.size() > 0) {
            xmlexport.addDisplayControl("displayBC");
            if (insuredHasNoBnf.equals("")) {
                textTag.add("BC_EdorName", edorName);
                textTag.add("BC_ContNotice", contNotice);
            } else {
                textTag.add("BC_ContNotice", "其他变更如下：");
            }
        }

        String[] strArrHead = new String[11];
        strArrHead[0] = "序号";
        strArrHead[1] = "险种代码";
        strArrHead[2] = "被保人";
        strArrHead[3] = "受益人类别";
        strArrHead[4] = "受益人姓名";
        strArrHead[5] = "受益人性别";
        strArrHead[6] = "证件类型";
        strArrHead[7] = "证件号码";
        strArrHead[8] = "与被保人关系";
        strArrHead[9] = "受益顺序";
        strArrHead[10] = "受益份额";
        xmlexport.addListTable(tlistTable, strArrHead);
        return true;
    }

    /**
     * 保障保费调整
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailBF(LPEdorItemSchema aLPEdorItemSchema) {
        EdorItemSpecialData specialData = new EdorItemSpecialData(
                aLPEdorItemSchema);
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        System.out.println("getDetailBF" + contNo);
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(edorNo);
        tLPPolDB.setEdorType(edorType);
        tLPPolDB.setContNo(contNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        tlistTable = new ListTable();
        tlistTable.setName("BF");
        for (int i = 1; i <= tLPPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(tLPPolSchema.getPolNo());
            if (tLCPolSchema == null) {
                mErrors.addOneError("找不到" + tLPPolSchema.getRiskSeqNo() + "险种" +
                                    "的险种信息！");
                return false;
            }
            double changPrem;
            LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
            tLJSGetEndorseDB.setEndorsementNo(edorNo);
            tLJSGetEndorseDB.setFeeOperationType(edorType);
            tLJSGetEndorseDB.setPolNo(tLPPolSchema.getPolNo());
            LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
            System.out.println("tLJSGetEndorseSet.size()" +
                               tLJSGetEndorseSet.size());
            if (tLJSGetEndorseSet.size() == 0) {
                changPrem = 0;
            } else {
                changPrem = tLJSGetEndorseSet.get(1).getGetMoney();
                System.out.println("changPrem" + String.valueOf(changPrem));
            }
            String changPremStr = String.valueOf(Math.abs(changPrem));
            String content = "序号" + tLPPolSchema.getRiskSeqNo()
                             + "，险种名称" +
                             CommonBL.getRiskName(tLPPolSchema.getRiskCode())
                             + "，代码" + tLPPolSchema.getRiskCode()
                             + "（被保险人" + tLPPolSchema.getInsuredName() + "）\n";
            if (tLCPolSchema.getMult() == 0) {
                content += "保额由" + String.valueOf(tLCPolSchema.getAmnt())
                        + "元调整为" + String.valueOf(tLPPolSchema.getAmnt()) +
                        "元，";
            } else {
                content += "档次由" + String.valueOf(tLCPolSchema.getMult()) +
                        "档调整为" + String.valueOf(tLPPolSchema.getMult()) + "档，";
            }
            String edorValidate = " ";
            try {
                //得到生效日期
                specialData.setPolNo(tLPPolSchema.getPolNo());
                specialData.query();
                String validateTime = specialData.getEdorValue("ValidateTime");
                if ((validateTime == null) || (validateTime.equals("1"))) { //即时生效
                    edorValidate = CommonBL.decodeDate(new FDate().toString(
                            CommonBL.changeDate(aLPEdorItemSchema.
                                                getEdorValiDate(), 1)));
                } else if (validateTime.equals("2")) { //下期生效
                    edorValidate = CommonBL.decodeDate(tLPPolSchema.
                            getPaytoDate());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            content += "从" + edorValidate + "生效，\n本险种期交保费变为" +
                    tLPPolSchema.getPrem() + "元，";
            if ((tLPPolSchema.getAmnt() > tLCPolSchema.getAmnt()) ||
                (tLPPolSchema.getMult() > tLCPolSchema.getMult())) {
                content += "本次应交保费" + changPremStr + "元";
            }
            if ((tLPPolSchema.getAmnt() < tLCPolSchema.getAmnt()) ||
                (tLPPolSchema.getMult() < tLCPolSchema.getMult())) {
                content += "本次应退保费" + changPremStr + "元";
            }
            String[] column = new String[1];
            column[0] = content;
            tlistTable.add(column);
        }
        xmlexport.addListTable(tlistTable, new String[1]);
        String fee = "本次变更合计应";
        if (aLPEdorItemSchema.getGetMoney() >= 0) {
            fee += "收";
        } else {
            fee += "退";
        }
        fee += "保费" + Math.abs(aLPEdorItemSchema.getGetMoney())
                + "元";
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(edorNo);
        tLPContDB.setEdorType(edorType);
        tLPContDB.setContNo(contNo);
        if (tLPContDB.getInfo()) {
            fee += "，保单期交保费变更为" + tLPContDB.getPrem() + "元";
        }
        textTag.add("BF_ContNo", contNo);
        textTag.add("BF_Fee", fee);
        return true;
    }

    /**
     * 投保事项变更
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailTB(LPEdorItemSchema aLPEdorItemSchema) {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCContSchema tLCContSchema = CommonBL.getLCCont(contNo);
        if (tLCContSchema == null) {
            mErrors.addOneError("未找到保单信息！");
            return false;
        }
        textTag.add("TB_ContNo", contNo);
        LPTbInfoDB tLPTbInfoDB = new LPTbInfoDB();
        tLPTbInfoDB.setEdorNo(edorNo);
        tLPTbInfoDB.setEdorType(edorType);
        tLPTbInfoDB.setContNo(contNo);
        if (!tLPTbInfoDB.getInfo()) {
            mErrors.addOneError("未找到保全变更信息！");
            return false;
        }
        //理算结果显示投保告知补遗和重审承保条件内容，但批单中不显示
        String content = "";
        if (tLPTbInfoDB.getSignName() != null) {
            content += "投/被保人补签名：" +
                    tLPTbInfoDB.getSignName() + "\n";
        }
        if (tLPTbInfoDB.getImpart() != null) {
            content += "投保告知补遗：" + tLPTbInfoDB.getImpart() + "\n";
        }
        if (tLPTbInfoDB.getCondition() != null) {
            content += "重审承保条件：" + tLPTbInfoDB.getCondition() + "\n";
        }
        if ((tLPTbInfoDB.getRemarkFlag() != null)
            && (tLPTbInfoDB.getRemarkFlag().equals("1"))) {
            content += "保单特别约定由：" + CommonBL.notNull(tLCContSchema.getRemark()) +
                    "\n变为：" + CommonBL.notNull(tLPTbInfoDB.getRemark());
        }
        String sugUWIdea = getSugUWIdea(aLPEdorItemSchema);
        content += sugUWIdea;
        tlistTable = new ListTable();
        tlistTable.setName("TB");
        String[] column = new String[1];
        column[0] = content;
        tlistTable.add(column);
        xmlexport.addListTable(tlistTable, new String[1]);

        String confirmcontent = "";
        if (tLPTbInfoDB.getSignName() != null) {
            confirmcontent += "投/被保人补签名：" +
                    tLPTbInfoDB.getSignName() + "\n";
        }
        if (tLPTbInfoDB.getImpart() != null) {
            confirmcontent += "投保告知补遗：保单" + contNo + "投保告知补遗\n";
        }
        if (tLPTbInfoDB.getCondition() != null) {
            confirmcontent += "重审承保条件：保单" + contNo + "重审承保条件\n";
        }
        if ((tLPTbInfoDB.getRemarkFlag() != null)
            && (tLPTbInfoDB.getRemarkFlag().equals("1"))) {
            confirmcontent += "保单特别约定由：" +
                    CommonBL.notNull(tLCContSchema.getRemark()) +
                    "\n变为：" + CommonBL.notNull(tLPTbInfoDB.getRemark());
        }
        confirmcontent += sugUWIdea;
        tlistTable = new ListTable();
        tlistTable.setName("TB_Confirm");
        String[] confirmColumn = new String[1];
        confirmColumn[0] = confirmcontent;
        tlistTable.add(confirmColumn);
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    private String getInsuredName(LPEdorItemSchema itemSchema) {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(itemSchema.getInsuredNo());
        tLCInsuredDB.setContNo(itemSchema.getContNo());
        LCInsuredSet set = tLCInsuredDB.query();
        if (set.size() == 0) {
            return "";
        }
        return StrTool.cTrim(set.get(1).getName());
    }

    //犹豫期退保 By WangJH
    private boolean getDetailWT(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("WT_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("WT_EdorValiDate", aLPEdorItemSchema.getEdorValiDate());

        ListTable tlistTable = new ListTable();
        tlistTable.setName("WT");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        String msql =
                "select sum(getmoney) from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "'";
        ExeSQL aExeSQL = new ExeSQL();
        String tReturn = aExeSQL.getOneValue(msql);
        System.out.println("msql:" + msql);
        if (tReturn == null || StrTool.cTrim(tReturn).equals("")) {
            mErrors.addOneError(new CError("没有相关的犹豫期退保记录！"));
            return false;
        }
        String yeSQL = msql + " and FeeFinaType = 'YE' ";
        String yeReturn = aExeSQL.getOneValue(yeSQL);
        if (yeReturn.equals("")) {
            yeReturn = "0";
        }
        String tfSQL = msql + " and FeeFinaType = 'TF' ";
        String tfReturn = aExeSQL.getOneValue(tfSQL);
        if (tfReturn.equals("")) {
            tfReturn = "0";
        }
        String gbSQL = msql + " and FeeFinaType = 'GB' ";
        String gbReturn = aExeSQL.getOneValue(gbSQL);
        if (gbReturn.equals("")) {
            gbReturn = "0";
        }
        String tjReturn = aExeSQL.getOneValue(msql + " and FeeFinaType = 'TJ' ");
        if (tjReturn.equals("")) {
            tjReturn = "0";
        }
        
        double tfReturn2 = Math.abs(Double.parseDouble(tfReturn));
        double gbReturn2 = Math.abs(Double.parseDouble(gbReturn));
        double tjReturn2 = Math.abs(Double.parseDouble(tjReturn));
        
        textTag.add("WT_Total", CommonBL.bigDoubleToCommonString(tfReturn2,"0.00"));
        textTag.add("WT_TJ", CommonBL.bigDoubleToCommonString(tjReturn2,"0.00"));
        textTag.add("WT_GB", CommonBL.bigDoubleToCommonString(gbReturn2,"0.00"));
        
        double total = Math.abs(Double.parseDouble(tfReturn)
                                + Double.parseDouble(yeReturn)
                                + Double.parseDouble(gbReturn)
                                + Double.parseDouble(tjReturn));
        textTag.add("WT_GetMoney", CommonBL.bigDoubleToCommonString(total,"0.00"));

        String[] edorValiDate = (aLPEdorItemSchema.getEdorValiDate()).split("-");
        String WT_edorValiDate = edorValiDate[0] + "年"
                                 + edorValiDate[1] + "月"
                                 + edorValiDate[2] + "日";
        textTag.add("WT_edorValiDate", WT_edorValiDate);

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet == null || tLPPolSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "PrtEndorsementBL";
            tError.functionName = "getDetailWT";
            tError.errorMessage = "LCPol不存在相应的记录!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int i = 0; i < tLPPolSet.size(); i++) {
            String printContent[] = new String[4];
            for (int j = 0; j < printContent.length; j++) {
                printContent[j] = "";
            }
            LPPolSchema tLPPolSchema = tLPPolSet.get(i + 1);

            //得到各个险种的变更费用
            msql =
                    "select abs(sum(GetMoney)) from LJSGetEndorse where EndorsementNo='" +
                    aLPEdorItemSchema.getEdorNo() + "' " +
                    "and FeeOperationType='"
                    + aLPEdorItemSchema.getEdorType() + "' "
                    + "    and PolNo='" + tLPPolSchema.getPolNo() + "' "
                    + "    and feeFinaType = '"
                    + BqCalBL.getFinType(aLPEdorItemSchema.getEdorType(), "TF",
                                         tLPPolSet.get(i + 1).getPolNo()) +
                    "' ";
            aExeSQL = new ExeSQL();
            String chgPrem = CommonBL.bigDoubleToCommonString(Double.parseDouble(aExeSQL.getOneValue(msql)),"0.00");

            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(tLPPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "PrtEndorsementBL";
                tError.functionName = "getDetailWT";
                tError.errorMessage = "LMRisk不存在相应的记录!";
                this.mErrors.addOneError(tError);
                tLMRiskDB.setRiskName("");
            } else {
                printContent[0] = tLPPolSchema.getInsuredName();
                printContent[1] = StrTool.cTrim(tLPPolSchema.getRiskSeqNo());
                printContent[2] = tLMRiskDB.getRiskName();
                printContent[3] = chgPrem + "元";
            }
            tlistTable.add(printContent);
        }
        String strhead[] = new String[4];
        strhead[0] = "被保人";
        strhead[1] = "保险序号";
        strhead[2] = "险种名称";
        strhead[3] = "退险金额";
        xmlexport.addListTable(tlistTable, strhead);
        createAppAccList(aLPEdorItemSchema);
        return true;
    }
    
    //add fr 上海医保卡EW
    private boolean getDetailEW(LPEdorItemSchema aLPEdorItemSchema){
	 
        String sql = "select * from LPEdorItem " +
        "where EdorAcceptNo = '" +
        aLPEdorItemSchema.getEdorAcceptNo() + "' " +
        "and EdorType='" +
        aLPEdorItemSchema.getEdorType() + "' " +
        "order by edorno,insuredno";
 		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
 		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
 		if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
 			CError tError = new CError();
 			tError.moduleName = "PrtAppEndorsementBL";
 			tError.functionName = "getLCContSubEW";
 			tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
 			                     "下无相应的保全项目信息!";
 			this.mErrors.addOneError(tError);
 			return false;
 		}

    	LPContSubDB tLPContSubDB =null;
     LPContSubSchema tlPContSubSchema = null;
     LCContSubDB tLCContSubDB=null;
     LCContSubSchema tlCContSubSchema = null;

    	tlistTable = new ListTable(); 
    	tlistTable.setName("EW");
 		for(int i=1;i<=tLPEdorItemSet.size();i++){
 			
 			LPEdorItemSchema tLPEdorItemSchema= tLPEdorItemSet.get(i);

 			   tLPContSubDB = new LPContSubDB();
 			   tLPContSubDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
 			   tLPContSubDB.setEdorType(tLPEdorItemSchema.getEdorType());
 			   ExeSQL tExeSQL = new ExeSQL();
 			   String prtno = tExeSQL.getOneValue("select prtno from lccont where contno ='" +tLPEdorItemSchema.getContNo()+"'  and appflag='1' ");
 			   tLPContSubDB.setPrtNo(prtno);
 		        if (!tLPContSubDB.getInfo()) {
 		            buildError("getDetailEW",
 		                       "查询失败!");
 		            return false;
 		        }
 		        tlPContSubSchema = new LPContSubSchema();
 		        tlPContSubSchema.setSchema(tLPContSubDB.getSchema());
 			
 		        
 		        
 		 	   tLCContSubDB = new LCContSubDB();
 			   tLCContSubDB.setPrtNo(tlPContSubSchema.getPrtNo());
 		        if (!tLCContSubDB.getInfo()) {
 		            buildError("getDetailEW",
 		                       "查询失败!");
 		            return false;
 		        }
 		        tlCContSubSchema = new LCContSubSchema();
 		        tlCContSubSchema.setSchema(tLCContSubDB.getSchema());
 			
 	        //-------------
  
 		      if (!StrTool.cTrim(tlCContSubSchema.getMedicalCode()).equals(StrTool.
 		                cTrim(
 		                		tlPContSubSchema.getMedicalCode()))) {
 		            String[] strArr  = new String[1];
 		            strArr[0] = "医保账户：" +
 		                        CommonBL.notNull(tlCContSubSchema.getMedicalCode()) +
 		                        "  变更为：" +
 		                        CommonBL.notNull(tlPContSubSchema.getMedicalCode());
 		            tlistTable.add(strArr);
 		        }
 		        
 		     if (!StrTool.cTrim(tlCContSubSchema.getRenemalPayMethod()).equals(StrTool.
 		                cTrim(
 		                		tlPContSubSchema.getRenemalPayMethod()))) {
 		            String[] strArr = new String[1];
 		            strArr[0] = "连续投保方式：" +
// 		            tExeSQL.getOneValue("select codename from ldcode where codetype ='paymode' and code='"+CommonBL.notNull(tlCContSubSchema.getRenemalPayMethod())+"'")
// 		                         +
// 		                        "  变更为：" +
// 		    		            tExeSQL.getOneValue("select codename from ldcode where codetype ='paymode' and code='"+CommonBL.notNull(tlPContSubSchema.getRenemalPayMethod())+"'")
// 		    			        ;
					tExeSQL.getOneValue("select codename from ldcode where codetype ='paymodesh' and code='"+CommonBL.notNull(tlCContSubSchema.getRenemalPayMethod())+"'")
                     				+
                     				"  变更为：" +
                     				tExeSQL.getOneValue("select codename from ldcode where codetype ='paymodesh' and code='"+CommonBL.notNull(tlPContSubSchema.getRenemalPayMethod())+"'")
			        ;
 		            tlistTable.add(strArr);
 		        }
 		        
 	        if (!StrTool.cTrim(tlCContSubSchema.getIfAutoPay()).equals(StrTool.
 	                cTrim(
 	                		tlPContSubSchema.getIfAutoPay()))) {
 	            String[] strArr = new String[1];
 	            strArr[0] = "是否续保：" +
 	                        ("0".equals(CommonBL.notNull(tlCContSubSchema.getIfAutoPay()))?"否":"是") +
 	                        "  变更为：" +
 	                        ("0".equals(CommonBL.notNull(tlPContSubSchema.getIfAutoPay()))?"否":"是");
 	            tlistTable.add(strArr);
 	        }
 //

 	        if(i!=tLPEdorItemSet.size()){
 	        	 String[] strSpe = new String[1];
 	        	 strSpe[0]="\n\n";
 	        	 tlistTable.add(strSpe);
 	        }
 		}
 		 String[] strArrHead = new String[1];
 	        strArrHead[0] = "上海医保账户微信平台保全项目变更 ";
 	        xmlexport.addListTable(tlistTable, strArrHead);
        String contNo = "";
//        for (int i = 1; i <= tLPAppntSet.size(); i++) {
//            if (i != tLPAppntSet.size()) {
//                contNo += tLPAppntSet.get(i).getContNo() + "，";
//            } else {
//                contNo += tLPAppntSet.get(i).getContNo();
//            }
//        }
        String strCont=" select contno from lccont where prtno in (select distinct prtno from lpcontsub " 
        		+" where edorno='"+aLPEdorItemSchema.getEdorNo()+"' " 
        		+" and edortype='"+aLPEdorItemSchema.getEdorType()+"') and appflag='1'  " 
        		+" with ur";
        SSRS tSSRS=new SSRS();
        tSSRS=new ExeSQL().execSQL(strCont);
        for (int i = 1; i <= tSSRS.MaxRow; i++) {
          if (i != tSSRS.MaxRow) {
              contNo += tSSRS.GetText(i, 1)+ "，";
          } else {
              contNo += tSSRS.GetText(i, 1);
          }
      }
        
        textTag.add("EW_Cont_Info", contNo);
        textTag.add("EW_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        return true;
    
    }
    //投保人联系方式变更
    private boolean getDetailAD(LPEdorItemSchema aLPEdorItemSchema) {
    	
        //20141014联系方式变更保全批单增加对被保险人变更内容的显示
        String sql = "select * from LPEdorItem " +
        "where EdorAcceptNo = '" +
        aLPEdorItemSchema.getEdorAcceptNo() + "' " +
        "and EdorType='" +
        aLPEdorItemSchema.getEdorType() + "' " +
        "order by edorno,insuredno";
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
		if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "PrtAppEndorsementBL";
			tError.functionName = "getInsuredCM";
			tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
			                     "下无相应的保全项目信息!";
			this.mErrors.addOneError(tError);
			return false;
		}
		LPAppntDB tLPAppntDB = null;
    	LPAppntSchema tLPAppntSchema =null;
    	LPAppntSet tLPAppntSet =null;
    	LPInsuredSet tLPInsuredSet =null;
    	LPAddressDB tLPAddressDB=null;
    	LPAddressSchema tLPAddressSchema=null;
    	LCAddressDB tLCAddressDB=null;
    	LCAddressSchema tLCAddressSchema=null;
    	LPInsuredSchema tLPInsuredSchema=null;
    	LCInsuredDB tLCInsuredDB=null;
    	
    	tlistTable = new ListTable();
    	tlistTable.setName("AD");
		for(int i=1;i<=tLPEdorItemSet.size();i++){
			
			LPEdorItemSchema tLPEdorItemSchema= tLPEdorItemSet.get(i);
			boolean isInusred=false;
		    //--------------------
		  //取（保全团单投保人表）和（团单投保人表）的相关记录，生称相应schema
			tLPAppntDB = new LPAppntDB();
	        tLPAppntDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
	        tLPAppntDB.setEdorType(tLPEdorItemSchema.getEdorType());
	        if(!BQ.FILLDATA.equals(tLPEdorItemSchema.getInsuredNo())){
	        	tLPAppntDB.setAppntNo(tLPEdorItemSchema.getInsuredNo());
	        }
	        tLPAppntSet = new LPAppntSet();
	        tLPAppntSet.set(tLPAppntDB.query());
	        if (tLPAppntDB.mErrors.needDealError() || tLPAppntSet.size() == 0) {
	        	//查询不到投保人保全信息时去查询被保人保全信息
	        	isInusred=true;		//被保人联系方式变更
		    	LPInsuredDB tLPInsuredDB = new LPInsuredDB();
		    	tLPInsuredDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
		    	tLPInsuredDB.setEdorType(tLPEdorItemSchema.getEdorType());
		    	tLPInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
		        tLPInsuredSet = new LPInsuredSet();
		        tLPInsuredSet.set(tLPInsuredDB.query());
		        if (tLPAppntDB.mErrors.needDealError() || tLPInsuredSet.size() == 0) {
		            buildError("getDetailAD", "保全被保人表LPInsured中无相关批单号的记录");
		            return false;
		        }
		        tLPInsuredSchema=tLPInsuredSet.get(1);//只有一条
		        
		        //查询被保险人联系方式信息
		        tLPAddressDB = new LPAddressDB();
		        tLPAddressDB.setEdorNo(tLPInsuredSchema.getEdorNo());
		        tLPAddressDB.setEdorType(tLPInsuredSchema.getEdorType());
		        tLPAddressDB.setCustomerNo(tLPInsuredSchema.getInsuredNo());
		        tLPAddressDB.setAddressNo(tLPInsuredSchema.getAddressNo());
		        if (!tLPAddressDB.getInfo()) {
		            buildError("getDetailAD",
		                       "查询客户号为" + tLPInsuredSchema.getInsuredNo() + "的保全地址信息时失败!");
		            return false;
		        }
		        tLPAddressSchema = new LPAddressSchema();
		        tLPAddressSchema.setSchema(tLPAddressDB.getSchema());
		        
		        tLCInsuredDB=new LCInsuredDB();
		        tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
		        tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
		        if(!tLCInsuredDB.getInfo()){
		        	 buildError("getDetailAD",
		                       "查询保单号为" + tLPEdorItemSchema.getContNo() + "的被保人信息时失败!");
		            return false;
		        }
		        
		        tLCAddressDB = new LCAddressDB();
		        tLCAddressDB.setCustomerNo(tLCInsuredDB.getInsuredNo());
		        tLCAddressDB.setAddressNo(tLCInsuredDB.getAddressNo());
		        if (!tLCAddressDB.getInfo()) {
		            buildError("getDetailAD",
		                       "查询客户号为" + tLPInsuredSchema.getAppntNo() + "的地址信息时失败!");
		            return false;
		        }
		        tLCAddressSchema = new LCAddressSchema();
		        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
		        
	        }else{
		        tLPAppntSchema = tLPAppntSet.get(1);
	
		        tLPAddressDB = new LPAddressDB();
		        tLPAddressDB.setEdorNo(tLPAppntSchema.getEdorNo());
		        tLPAddressDB.setEdorType(tLPAppntSchema.getEdorType());
		        tLPAddressDB.setCustomerNo(tLPAppntSchema.getAppntNo());
		        tLPAddressDB.setAddressNo(tLPAppntSchema.getAddressNo());
		        if (!tLPAddressDB.getInfo()) {
		            buildError("getDetailAD",
		                       "查询客户号为" + tLPAppntSchema.getAppntNo() + "的保全地址信息时失败!");
		            return false;
	
		        }
		        tLPAddressSchema = new LPAddressSchema();
		        tLPAddressSchema.setSchema(tLPAddressDB.getSchema());
	
		        //取（保全团体客户地址表）和（团体客户地址表）的相关记录，生称相应schema
		        LCAppntDB tLCAppntDB = new LCAppntDB();
		        tLCAppntDB.setAppntNo(tLPAppntSchema.getAppntNo());
		        tLCAppntDB.setContNo(tLPAppntSchema.getContNo());
		        if (!tLCAppntDB.getInfo()) {
		            buildError("getDetailAD",
		                       "查询保单号为" + tLPAppntSchema.getContNo() + "的投保人表失败!");
		            return false;
		        }
	
		        tLCAddressDB = new LCAddressDB();
		        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
		        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
		        if (!tLCAddressDB.getInfo()) {
		            buildError("getDetailAD",
		                       "查询客户号为" + tLPAppntSchema.getAppntNo() + "的地址信息时失败!");
		            return false;
		        }
		        tLCAddressSchema = new LCAddressSchema();
		        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());
	        }
	        //-------------
		    String[] strCus = new String[1];
		    if(isInusred){
		    	strCus[0] = "被保人：" + tLPInsuredSchema.getName() +
	            " 客户号：" + tLPInsuredSchema.getInsuredNo() +
	            "\n";
		    }else{
		    	strCus[0] = "投保人：" + tLPAppntSchema.getAppntName() +
	            " 客户号：" + tLPAppntSchema.getAppntNo() +
	            "\n";
		    }
		    tlistTable.add(strCus);
		
		  //--------------------------------------
	        if (!StrTool.cTrim(tLCAddressSchema.getPostalAddress()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getPostalAddress()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "联系地址：" +
	                        CommonBL.notNull(tLCAddressSchema.getPostalAddress()) +
	                        "\n变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getPostalAddress());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getZipCode()).equals(StrTool.
	                cTrim(tLPAddressSchema.getZipCode()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "邮政编码：" + CommonBL.notNull(tLCAddressSchema.getZipCode()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getZipCode());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getPhone()).equals(StrTool.
	                cTrim(tLPAddressSchema.getPhone()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "联系电话：" + CommonBL.notNull(tLCAddressSchema.getPhone()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getPhone());
	            tlistTable.add(strArr);
	        }
	        if (!StrTool.cTrim(tLCAddressSchema.getEMail()).equals(StrTool.
	                cTrim(tLPAddressSchema.getEMail()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "电子邮件地址：" + CommonBL.notNull(tLCAddressSchema.getEMail()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getEMail());
	            tlistTable.add(strArr);
	        }
	        if (!StrTool.cTrim(tLCAddressSchema.getMobile()).equals(StrTool.cTrim(
	                tLPAddressSchema.getMobile()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "移动电话：" + CommonBL.notNull(tLCAddressSchema.getMobile()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getMobile());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getFax()).equals(StrTool.cTrim(
	                tLPAddressSchema.getFax()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "通讯传真：" + CommonBL.notNull(tLCAddressSchema.getFax()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getFax());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getHomeAddress()).equals(StrTool.
	                cTrim(tLPAddressSchema.getHomeAddress()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "家庭住址：" +
	                        CommonBL.notNull(tLCAddressSchema.getHomeAddress()) +
	                        "\n变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getHomeAddress());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getHomeZipCode()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getHomeZipCode()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "家庭邮政编码：" +
	                        CommonBL.notNull(tLCAddressSchema.getHomeZipCode()) +
	                        " 变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getHomeZipCode());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getHomePhone()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getHomePhone()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "家庭电话：" +
	                        CommonBL.notNull(tLCAddressSchema.getHomePhone()) +
	                        " 变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getHomePhone());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getHomeFax()).equals(StrTool.cTrim(
	                tLPAddressSchema.getHomeFax()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "家庭传真：" + CommonBL.notNull(tLCAddressSchema.getHomeFax()) +
	                        " 变更为：" + CommonBL.notNull(tLPAddressSchema.getHomeFax());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getCompanyAddress()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getCompanyAddress()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "单位地址：" +
	                        CommonBL.notNull(tLCAddressSchema.getCompanyAddress()) +
	                        "\n变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getCompanyAddress());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getCompanyZipCode()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getCompanyZipCode()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "单位邮政编码：" +
	                        CommonBL.notNull(tLCAddressSchema.getCompanyZipCode()) +
	                        " 变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getCompanyZipCode());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getCompanyPhone()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getCompanyPhone()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "单位电话：" +
	                        CommonBL.notNull(tLCAddressSchema.getCompanyPhone()) +
	                        " 变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getCompanyPhone());
	            tlistTable.add(strArr);
	        }
	
	        if (!StrTool.cTrim(tLCAddressSchema.getCompanyFax()).equals(StrTool.
	                cTrim(
	                        tLPAddressSchema.getCompanyFax()))) {
	            String[] strArr = new String[1];
	            strArr[0] = "单位传真：" +
	                        CommonBL.notNull(tLCAddressSchema.getCompanyFax()) +
	                        " 变更为：" +
	                        CommonBL.notNull(tLPAddressSchema.getCompanyFax());
	            tlistTable.add(strArr);
	        }
	        
	        if(i!=tLPEdorItemSet.size()){
	        	 String[] strSpe = new String[1];
	        	 strSpe[0]="\n\n";
	        	 tlistTable.add(strSpe);
	        }
		}
		 String[] strArrHead = new String[1];
	        strArrHead[0] = "地址变更";
	        xmlexport.addListTable(tlistTable, strArrHead);
        String contNo = "";
//        for (int i = 1; i <= tLPAppntSet.size(); i++) {
//            if (i != tLPAppntSet.size()) {
//                contNo += tLPAppntSet.get(i).getContNo() + "，";
//            } else {
//                contNo += tLPAppntSet.get(i).getContNo();
//            }
//        }
        String strCont="select distinct contno from lpappnt " 
        		+" where edorno='"+aLPEdorItemSchema.getEdorNo()+"' " 
        		+" and edortype='"+aLPEdorItemSchema.getEdorType()+"' " 
        		+" union "
        		+" select distinct contno from lpinsured "
        		+" where edorno='"+aLPEdorItemSchema.getEdorNo()+"' " 
        		+" and edortype='"+aLPEdorItemSchema.getEdorType()+"' "
        		+" and contno='"+aLPEdorItemSchema.getContNo()+"' "
        		+" with ur";
        SSRS tSSRS=new SSRS();
        tSSRS=new ExeSQL().execSQL(strCont);
        for (int i = 1; i <= tSSRS.MaxRow; i++) {
          if (i != tSSRS.MaxRow) {
              contNo += tSSRS.GetText(i, 1)+ "，";
          } else {
              contNo += tSSRS.GetText(i, 1);
          }
      }
        
        textTag.add("New_Cont_Info", contNo);
        textTag.add("AD_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        return true;
    }

    /**
     * 更换投保人
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailAE(LPEdorItemSchema aLPEdorItemSchema) {
    	LPInsuredSchema tLPInsuredSchema =new LPInsuredSchema ();
    	LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("找不到原投保人信息!");
            return false;
        }
        textTag.add("AE_ContNo", tLCAppntDB.getContNo());
        textTag.add("AE_AppntName", tLCAppntDB.getAppntName());
        textTag.add("AE_AppntNo", tLCAppntDB.getAppntNo());

        LPAppntDB tLPAppntDB = new LPAppntDB();
        tLPAppntDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPAppntDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPAppntDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLPAppntDB.getInfo()) {
            mErrors.addOneError("找不到新投保人信息!");
            return false;
        }
        String newContNo = PubFun1.CreateMaxNo("ContNo", tLPAppntDB.getAppntNo());
        textTag.add("AE_NewContNo", newContNo);
        textTag.add("AE_NewAppntName", tLPAppntDB.getAppntName());
        textTag.add("AE_NewAppntNo", tLPAppntDB.getAppntNo());
        
        
        tlistTable = new ListTable();
        tlistTable.setName("AE");
        
        
        LCInsuredSet  tLCInsuredSet = new  LCInsuredSet();
        LCInsuredDB tLCInsuredDB  = new LCInsuredDB();
        tLCInsuredDB.setContNo(aLPEdorItemSchema.getContNo());
        tLCInsuredSet=tLCInsuredDB.query();
        LPInsuredDB tLPInsuredDB  = new LPInsuredDB();
        
        for(int index=1;index<=tLCInsuredSet.size();index++)
        {       	
        	String printInsured[] =new String[3];
        	System.out.println("Begin the PrtAppEndorsementBL." +
        			"getDetailAE(LPEdorItemSchema" +
        			" aLPEdorItemSchema).for()"+index);
        	  tLPInsuredDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
              tLPInsuredDB.setEdorType(aLPEdorItemSchema.getEdorType());
              tLPInsuredDB.setContNo(aLPEdorItemSchema.getContNo());
              tLPInsuredDB.setInsuredNo(tLCInsuredSet.get(index).getInsuredNo());
              
              if(!tLPInsuredDB.getInfo())
              {
            	mErrors.addOneError("找不到原来的被保人信息!");
         	    return  false;
              }
              LDCodeDB tLDCodeDB =new LDCodeDB();
              tLDCodeDB.setCodeType("relation");
              tLDCodeDB.setCode(tLPInsuredDB.getRelationToAppnt());
              tLDCodeDB.getInfo();
              
        	System.out.println("被保人被查到啦");
        	System.out.println(tLPInsuredDB.getName());
        	System.out.println("与投保人的关系"+tLPInsuredDB.getRelationToAppnt()+tLDCodeDB.getCodeName());

            printInsured[0] = "被保人"+ tLPInsuredDB.getName();
            printInsured[1] = "与投保人的关系变更为"+tLDCodeDB.getCodeName();
            printInsured[2] = tLPInsuredDB.getRelationToAppnt(); 
            
//            printInsured[index-1] = "被保人"+tLPInsuredDB.getName()+"与投保人的关系变更为"+tLPInsuredDB.getRelationToAppnt()+tLDCodeDB.getCodeName();
//            printInsured[1] = tLPInsuredDB.getRelationToAppnt(); 
//            printInsured[2] = tLDCodeDB.getCodeName();
            //System.out.println(printInsured[0]+printInsured[1]+printInsured[2]);
            //textTag.add("AE_CodeName",tLDCodeDB.getCodeName());
            //textTag.add("AE_INSUREDNAME", tLPInsuredDB.getName());
            //textTag.add("AE_INSUREDRELATIONTOAPPNT", tLPInsuredDB.getRelationToAppnt());
            //textTag.add("AE_print", printInsured[0]+printInsured[1]+printInsured[2]);
            tlistTable.add(printInsured);     
        }
          String[] strArrHead = new String[3];       
        strArrHead[0]="被保人";
        strArrHead[1]="变更后的代码";
        strArrHead[2]="与投保人的关系变更后的名字";
        
        xmlexport.addListTable(tlistTable, strArrHead);
    	return true;
    }

    /**
     * 新增险种
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailNS(LPEdorItemSchema aLPEdorItemSchema) {
        tlistTable = new ListTable();
        tlistTable.setName("NS");

        String sql = "select a.riskName, a.riskCode, b.insuredName, "
                     + "   cValiDate, prem, "
                     + "   (select sum(getMoney ) from LJSGetEndorse "
                     + "   where otherNo = b.edorNo "
                     + "      and feeOperationType = b.edorType "
                     + "      and polNo = B.polNo ) "
                     + "from LMRisk a, LPPol b "
                     + "where a.riskCode = b.riskCode "
                     + "   and b.edorNo = '"
                     + aLPEdorItemSchema.getEdorNo() + "' "
                     + "   and b.edorType = '"
                     + aLPEdorItemSchema.getEdorType() + "' "
                     + "   and b.contNo = '"
                     + aLPEdorItemSchema.getContNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailNS";
            tError.errorMessage = "查询险种信息失败";
            mErrors.addOneError(tError);
            return false;
        }

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[1];
            info[0] = "增加险种名称：" + tSSRS.GetText(i, 1)
                      + "，代码：" + tSSRS.GetText(i, 2)
                      + "（被保险人" + tSSRS.GetText(i, 3)
                      + "），从" + CommonBL.decodeDate(tSSRS.GetText(i, 4))
                      + "生效，本险种期交保费为" + tSSRS.GetText(i, 5)
                      + "元，本次应交保费" + tSSRS.GetText(i, 6) + "元。";

            tlistTable.add(info);
        }

        textTag.add("NS_ContNo", aLPEdorItemSchema.getContNo());

        sql = "select sum(a.prem), b.payToDate from LCPol a, LCCont b "
              + "where a.contNo = b.contNo "
              + "   and a.contNo = '" + aLPEdorItemSchema.getContNo() + "' "
              + "   and a.CValiDate <= b.payToDate "
              + "group by b.payToDate ";
        System.out.println(sql);
        SSRS aSSRS = new ExeSQL().execSQL(sql);
        textTag.add("NS_Prem", aSSRS.GetText(1, 1));
        textTag.add("NS_PayToDate", CommonBL.decodeDate(aSSRS.GetText(1, 2)));
        xmlexport.addListTable(tlistTable, new String[1]);

        return true;
    }


    /**
     * 保单复效
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    //qulq create at 2006-10-25
    private boolean getDetailFX(LPEdorItemSchema aLPEdorItemSchema) {
        tlistTable = new ListTable();
        tlistTable.setName("FX");
        String sql = "select b.riskseqno,(select riskName from LMRisk a where a.riskCode = b.riskCode),"
                     + "   b.riskCode,b.insuredName,b.prem, sum(c.getmoney), "
                     + "(select sum(GetMoney) from LJSGetEndorse where EndorsementNo = c.EndorsementNo "
                     + "    and PolNo = c.PolNo and FeeFinaType = 'BF'), "
                     + "(select sum(GetMoney) from LJSGetEndorse where EndorsementNo = c.EndorsementNo "
                     + "    and PolNo = c.PolNo and FeeFinaType = 'FXLX') "
                     + "from LPPol b,LJSGetEndorse c "
                     + "where b.contno='" + aLPEdorItemSchema.getContNo() +
                     "' "
                     + "   and b.EdorType = '"
                     + aLPEdorItemSchema.getEdorType() + "' "
                     + "   and b.polno = c.polno "
                     + "   and b.edorno = c.EndorsementNo "
                     + "   and b.edorno ='"
                     + aLPEdorItemSchema.getEdorAcceptNo() + "' "
                     +"group by b.prem, b.riskCode, b.insuredName, b.riskseqno, c.polno,c.EndorsementNo";
        System.out.println(sql);

        SSRS tSSRS = new ExeSQL().execSQL(sql);

        if (tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailFX";
            tError.errorMessage = "查询复效险种信息失败";
            mErrors.addOneError(tError);
            return false;
        }
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(aLPEdorItemSchema.getEdorNo()
                                          , aLPEdorItemSchema.getEdorType());
        if (!tEdorItemSpecialData.query()) {
            this.mErrors.addOneError("复效日期查询错误！");
            return false;
        }

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[1];
            info[0] = "序号 " + tSSRS.GetText(i, 1)
                      + " 险种名称 " + tSSRS.GetText(i, 2)
                      + " 代码 " + tSSRS.GetText(i, 3)
                      + " （被保险人 " + tSSRS.GetText(i, 4)
                      + " ），"
                      + "\n从" +
                      PubFun.calDate(tEdorItemSpecialData.getEdorValue("FX_D"),
                                     1, "D", "")
                      + "日复效，本险种期交保费为" + tSSRS.GetText(i, 5)
                      + "元，本次应交保费" + tSSRS.GetText(i, 6)
                      + "元。"
                      + "（保费：" + tSSRS.GetText(i, 7) + "元，"
                      + " 利息："+ tSSRS.GetText(i, 8) + "元）"
                      ;

            tlistTable.add(info);
        }

        textTag.add("FX_Year", tEdorItemSpecialData.getEdorValue("FX_D"));
        textTag.add("FX_ContNo", aLPEdorItemSchema.getContNo());
        xmlexport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);

        return true;

    }

    
    /**
     * 保单免息复效
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    //LC create at 2017-1-16
    private boolean getDetailMF(LPEdorItemSchema aLPEdorItemSchema) {
        tlistTable = new ListTable();
        tlistTable.setName("MF");
        String sql = "select b.riskseqno,(select riskName from LMRisk a where a.riskCode = b.riskCode),"
                     + "   b.riskCode,b.insuredName,b.prem, sum(c.getmoney), "
                     + "(select sum(GetMoney) from LJSGetEndorse where EndorsementNo = c.EndorsementNo "
                     + "    and PolNo = c.PolNo and FeeFinaType = 'BF'), "
                     + "(select sum(GetMoney) from LJSGetEndorse where EndorsementNo = c.EndorsementNo "
                     + "    and PolNo = c.PolNo and FeeFinaType = 'MFLX') "
                     + "from LPPol b,LJSGetEndorse c "
                     + "where b.contno='" + aLPEdorItemSchema.getContNo() +
                     "' "
                     + "   and b.EdorType = '"
                     + aLPEdorItemSchema.getEdorType() + "' "
                     + "   and b.polno = c.polno "
                     + "   and b.edorno = c.EndorsementNo "
                     + "   and b.edorno ='"
                     + aLPEdorItemSchema.getEdorAcceptNo() + "' "
                     +"group by b.prem, b.riskCode, b.insuredName, b.riskseqno, c.polno,c.EndorsementNo";
        System.out.println(sql);

        SSRS tSSRS = new ExeSQL().execSQL(sql);

        if (tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailMF";
            tError.errorMessage = "查询复效险种信息失败";
            mErrors.addOneError(tError);
            return false;
        }
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(aLPEdorItemSchema.getEdorNo()
                                          , aLPEdorItemSchema.getEdorType());
        if (!tEdorItemSpecialData.query()) {
            this.mErrors.addOneError("复效日期查询错误！");
            return false;
        }

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[1];
            info[0] = "序号 " + tSSRS.GetText(i, 1)
                      + " 险种名称 " + tSSRS.GetText(i, 2)
                      + " 代码 " + tSSRS.GetText(i, 3)
                      + " （被保险人 " + tSSRS.GetText(i, 4)
                      + " ），"
                      + "\n从" +
                      PubFun.calDate(tEdorItemSpecialData.getEdorValue("MF_D"),
                                     1, "D", "")
                      + "日复效，本险种期交保费为" + tSSRS.GetText(i, 5)
                      + "元，本次应交保费" + tSSRS.GetText(i, 6)
                      + "元。"
                      + "（保费：" + tSSRS.GetText(i, 7) + "元，"
                      + " 利息："+ tSSRS.GetText(i, 8) + "元）"
                      ;

            tlistTable.add(info);
        }

        textTag.add("MF_Year", tEdorItemSpecialData.getEdorValue("MF_D"));
        textTag.add("MF_ContNo", aLPEdorItemSchema.getContNo());
        xmlexport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);

        return true;

    }
    
    
    
    /**
     * 保单特权复效
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    //caozeng create at 2008-06-06
    private boolean getDetailTF(LPEdorItemSchema aLPEdorItemSchema) {
        tlistTable = new ListTable();
        tlistTable.setName("TF");
        String sql = "select b.riskseqno,(select riskName from LMRisk a where a.riskCode = b.riskCode),"
                     + "   b.riskCode,b.insuredName "
                     + "from LPPol b,LJSGetEndorse c "
                     + "where b.contno='" + aLPEdorItemSchema.getContNo() +
                     "' "
                     + "   and b.EdorType = '"
                     + aLPEdorItemSchema.getEdorType() + "' "
                     + "   and b.edorno ='"
                     + aLPEdorItemSchema.getEdorAcceptNo() + "' "
                     +
                     "group by b.riskCode, b.insuredName, b.riskseqno";
        System.out.println(sql);

        SSRS tSSRS = new ExeSQL().execSQL(sql);

        if (tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailTF";
            tError.errorMessage = "查询复效险种信息失败";
            mErrors.addOneError(tError);
            return false;
        }
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(aLPEdorItemSchema.getEdorNo()
                                          , aLPEdorItemSchema.getEdorType());
        if (!tEdorItemSpecialData.query()) {
            this.mErrors.addOneError("复效日期查询错误！");
            return false;
        }

        Calendar   calendar   =   Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(new Date());
        String   date;
        date   =   String.valueOf(calendar.get(Calendar.YEAR))   +   "-";
        date   +=   String.valueOf(calendar.get(Calendar.MONTH)   +   1)   +   "-";
        date   +=   String.valueOf(calendar.get(Calendar.DATE));
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[1];
            info[0] = "序号 " + tSSRS.GetText(i, 1)
                      + " 险种名称 " + tSSRS.GetText(i, 2)
                      + " 代码 " + tSSRS.GetText(i, 3)
                      + " （被保险人 " + tSSRS.GetText(i, 4)
                      + " ），"
                      + "\n从" +
                      date
                      + "日复效。";
            tlistTable.add(info);
        }
        textTag.add("TF_Year", tEdorItemSpecialData.getEdorValue("TF_D"));
        textTag.add("TF_ContNo", aLPEdorItemSchema.getContNo());
        xmlexport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);

        return true;

    }

    /**
     * 交费资料变更
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailCC(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("CC_ContNo", aLPEdorItemSchema.getContNo());

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        tLCContDB.getInfo();

        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPContDB.setContNo(aLPEdorItemSchema.getContNo());
        tLPContDB.getInfo();

        ListTable tlistTable = new ListTable();
        tlistTable.setName("CC");
        if (!StrTool.cTrim(tLCContDB.getPayMode()).equals(StrTool.
                cTrim(tLPContDB.getPayMode()))) {
            String[] strArr = new String[1];
            strArr[0] = "交费方式：" +
                        CommonBL.notNull(ChangeCodeBL.getCodeName("PayMode",
                    tLCContDB.getPayMode())) +
                        "，变更为：" +
                        CommonBL.notNull(ChangeCodeBL.getCodeName("PayMode",
                    tLPContDB.getPayMode()));
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCContDB.getBankCode()).equals(StrTool.
                cTrim(tLPContDB.getBankCode()))) {
            String[] strArr = new String[1];
            if(tLPContDB.getPayMode()!=null&&"8".equals(tLPContDB.getPayMode())){
            	String querySql = "select MedicalComName from LDMedicalCom where CanSendFlag='1' and  comcode like '"+tLPContDB.getManageCom()+ "%' order by MedicalComCode with ur";
            	String bankname = new ExeSQL().getOneValue(querySql);
            	if(bankname==null||"".equals(bankname)){
            		bankname = "空白";
            	}
            	strArr[0] = "转帐银行：" +
            	CommonBL.notNull(ChangeCodeBL.getCodeName("Bank",
            			tLCContDB.getBankCode(), "BankCode")) +
            			"，变更为：" +bankname;
            	tlistTable.add(strArr);
            }else{
            	strArr[0] = "转帐银行：" +
            	CommonBL.notNull(ChangeCodeBL.getCodeName("Bank",
            			tLCContDB.getBankCode(), "BankCode")) +
            			"，变更为：" +
            			CommonBL.notNull(ChangeCodeBL.getCodeName("Bank",
            					tLPContDB.getBankCode(), "BankCode"));
            	tlistTable.add(strArr);
            }
        }

        if (!StrTool.cTrim(tLCContDB.getBankAccNo()).equals(StrTool.
                cTrim(tLPContDB.getBankAccNo()))) {
            String[] strArr = new String[1];
            strArr[0] = "转帐帐号：" +
                        CommonBL.notNull(tLCContDB.getBankAccNo()) +
                        "，变更为：" +
                        CommonBL.notNull(tLPContDB.getBankAccNo());
            tlistTable.add(strArr);
        }

        if (!StrTool.cTrim(tLCContDB.getAccName()).equals(StrTool.
                cTrim(tLPContDB.getAccName()))) {
            String[] strArr = new String[1];
            strArr[0] = "转帐帐户名：" +
                        CommonBL.notNull(tLCContDB.getAccName()) +
                        "，变更为：" +
                        CommonBL.notNull(tLPContDB.getAccName());
            tlistTable.add(strArr);
        }
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }
    /**
     * 保单拆分保全项目
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */

    private boolean getDetailCF(LPEdorItemSchema aLPEdorItemSchema) {
    	
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCContSchema tLCContSchema = CommonBL.getLCCont(contNo);
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(edorNo);
        tLPContDB.setEdorType(edorType);
        LPContSet tLPContSet = tLPContDB.query();
        if (tLPContSet.size() == 0) {
            mErrors.addOneError("未找到新保单信息！");
            return false;
        }
        LPContSchema tLPContSchema = tLPContSet.get(1);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("CF1");
        String[] oldContArr = new String[8];
        oldContArr[0] = tLCContSchema.getContNo();
        oldContArr[1] = tLCContSchema.getAppntName();
        oldContArr[2] = tLCContSchema.getAppntNo();
        oldContArr[3] = CommonBL.decodeDate(tLCContSchema.getCValiDate());
        oldContArr[4] = CommonBL.decodeDate(tLCContSchema.getPaytoDate());
        oldContArr[5] = String.valueOf(tLCContSchema.getPrem()- tLPContSchema.getPrem());
        oldContArr[6] = tLCContSchema.getManageCom();
        oldContArr[7] = ChangeCodeBL.getCodeName("paymode",tLCContSchema.getPayMode());
        tlistTable.add(oldContArr);
        xmlexport.addListTable(tlistTable, new String[8]);
        textTag.add("CF_ContNoOld", tLCContSchema.getContNo());

        tlistTable = new ListTable();
        tlistTable.setName("CF2");
        String sql = "select * from LCPol a " +
                     "where ContNo = '" + contNo + "' " +
                     "and not exists (select PolNo from " +
                     "    LPPol where edorNo = '" + edorNo + "' " +
                     "    and EdorType = '" + edorType + "' " +
                     "    and PolNo = a.PolNo)";
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
        for (int i = 1; i <= tLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            String[] oldPolArr = new String[9];
            oldPolArr[0] = String.valueOf(i);
            oldPolArr[1] = tLCPolSchema.getInsuredNo();
            oldPolArr[2] = tLCPolSchema.getInsuredName();
            oldPolArr[3] = ChangeCodeBL.getCodeName("riskcode",tLCPolSchema.getRiskCode(), "riskcode");
            oldPolArr[4] = tLCPolSchema.getRiskCode();
            oldPolArr[5] = String.valueOf(tLCPolSchema.getAmnt());
            oldPolArr[6] = String.valueOf(tLCPolSchema.getPrem());
            oldPolArr[7] = CommonBL.decodeDate(tLCPolSchema.getCValiDate());
            oldPolArr[8] = CommonBL.decodeDate(tLCPolSchema.getEndDate());
            tlistTable.add(oldPolArr);
        }
        xmlexport.addListTable(tlistTable, new String[9]);

        
        tlistTable = new ListTable();
        tlistTable.setName("CF3");
        String[] newContArr = new String[8];
        
        newContArr[0] = tLPContSchema.getContNo();
        newContArr[1] = tLPContSchema.getAppntName();
        newContArr[2] = tLPContSchema.getAppntNo();
        newContArr[3] = CommonBL.decodeDate(tLPContSchema.getCValiDate());
        newContArr[4] = CommonBL.decodeDate(tLPContSchema.getPaytoDate());
        newContArr[5] = String.valueOf(tLPContSchema.getPrem());
        newContArr[6] = tLPContSchema.getManageCom();
        newContArr[7] = ChangeCodeBL.getCodeName("paymode",tLPContSchema.getPayMode());
 
        tlistTable.add(newContArr);
        xmlexport.addListTable(tlistTable, new String[8]);
        textTag.add("CF_ContNoNew", tLPContSchema.getContNo());

        tlistTable = new ListTable();
        tlistTable.setName("CF4");
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(edorNo);
        tLPPolDB.setEdorType(edorType);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            String[] newPolArr = new String[9];
            
            newPolArr[0] = String.valueOf(i);
            newPolArr[1] = tLPPolSchema.getInsuredNo();
            newPolArr[2] = tLPPolSchema.getInsuredName();
            newPolArr[3] = ChangeCodeBL.getCodeName("riskcode",tLPPolSchema.getRiskCode(), "riskcode");
            newPolArr[4] = tLPPolSchema.getRiskCode();
            newPolArr[5] = String.valueOf(tLPPolSchema.getAmnt());
            newPolArr[6] = String.valueOf(tLPPolSchema.getPrem());
            newPolArr[7] = CommonBL.decodeDate(tLPPolSchema.getCValiDate());
            newPolArr[8] = CommonBL.decodeDate(tLPPolSchema.getEndDate());
            
            tlistTable.add(newPolArr);
        }
        xmlexport.addListTable(tlistTable, new String[9]);
        return true;
    }

    /**
     *  客户基本资料变更--add by luomin
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailCM(LPEdorItemSchema aLPEdorItemSchema) {
        //有CM相关记录，分别按投保人、被保人分别查询相关修改记录
        boolean isChangeInsured = false;
        String tCustomerNo = "";
        String tFormerCustomerNo = "";

        String sql = "select * from LPEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPEdorItemSchema.getEdorType() + "' " +
                     "order by edorno,insuredno";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredCM";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        textTag.add("CM_SIZE", tLPEdorItemSet.size());
        for (int i = 0; i < tLPEdorItemSet.size(); i++) {
            Reflections ref = new Reflections();
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i + 1);
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPInsuredDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLPInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            LPInsuredSet tLPInsuredSet = new LPInsuredSet();
            tLPInsuredSet = tLPInsuredDB.query();

            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
            tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            LCInsuredSet tLCInsuredSet = new LCInsuredSet();
            tLCInsuredSet = tLCInsuredDB.query();

            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            LPAppntSchema tLPAppntSchema = new LPAppntSchema();
            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            
            String prtNo = new ExeSQL().getOneValue("select prtno from lccont where contno = '"+tLPEdorItemSchema.getContNo()+"' union select prtno from lccont where contno = '"+tLPEdorItemSchema.getContNo()+"'");
            LPContSubDB lpContSubDB = new LPContSubDB();
            lpContSubDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            lpContSubDB.setEdorType(tLPEdorItemSchema.getEdorType());
            lpContSubDB.setPrtNo(prtNo);
            LPContSubSet lpContSubSet = new LPContSubSet();
            lpContSubSet = lpContSubDB.query();
            
            LCContSubDB lcContSubDB = new LCContSubDB();
            lcContSubDB.setPrtNo(prtNo);
            LCContSubSet lcContSubSet = new LCContSubSet();
            lcContSubSet = lcContSubDB.query();

            LPContSubSchema lpContSubSchema = new LPContSubSchema();
            LCContSubSchema lcContSubSchema = new LCContSubSchema();
            
            if(lpContSubSet.size()>=1){
            	lpContSubSchema = lpContSubSet.get(1);
            }
            
            if(lcContSubSet.size()>=1){
            	lcContSubSchema = lcContSubSet.get(1);
            }
            
            if (tLCInsuredSet.size() >= 1) {
                tLCInsuredSchema = tLCInsuredSet.get(1);
            }
            if (tLPInsuredSet.size() >= 1) {
                tLPInsuredSchema = tLPInsuredSet.get(1);
                //ref.transFields(tLCInsuredSchema,tLPInsuredSchema);
            }
            if (tLPInsuredDB.getInfo()) {
                //P表中有相应数据表示没有对被保人的信息进行修改
                isChangeInsured = true;
                tCustomerNo = tLPInsuredDB.getInsuredNo();
            }
            //否则查保全投保人表
            else {
                LPAppntDB tLPAppntDB = new LPAppntDB();
                tLPAppntDB.setEdorType(tLPEdorItemSchema.getEdorType());
                tLPAppntDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                tLPAppntDB.setContNo(tLPEdorItemSchema.getContNo());
                LPAppntSet tLPAppntSet = new LPAppntSet();
                tLPAppntSet = tLPAppntDB.query();
                LCAppntDB tLCAppntDB = new LCAppntDB();
                tLCAppntDB.setAppntNo(tLPEdorItemSchema.getInsuredNo());
                tLCAppntDB.setContNo(tLPEdorItemSchema.getContNo());
                LCAppntSet tLCAppntSet = new LCAppntSet();
                tLCAppntSet = tLCAppntDB.query();
                if (tLCAppntSet.size() >= 1) {
                    tLCAppntSchema = tLCAppntSet.get(1);
                }
                //LPAppntSchema tLPAppntSchema = new LPAppntSchema();
                //LCAppntSchema tLCAppntSchema = new LCAppntSchema();
                if (tLPAppntSet.size() >= 1) {
                    tLPAppntSchema = tLPAppntSet.get(1);
                    //ref.transFields(tLCAppntSchema,tLPAppntSchema);
                }
                if (tLPAppntDB.getInfo()) {
                    //P表中有相应数据表示没有对投保人的信息进行修改
                    isChangeInsured = false;
                    tCustomerNo = tLPAppntDB.getAppntNo();
                } else {
                    CError tError = new CError();
                    tError.moduleName = "PrtAppEndorsementBL";
                    tError.functionName = "getInsuredCM";
                    tError.errorMessage = "保全投保人表和被保人表中都不存在相应记录!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            if (!tFormerCustomerNo.equals(tCustomerNo)) {
                LPPersonDB tLPPersonDB = new LPPersonDB();
                tLPPersonDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                tLPPersonDB.setEdorType(tLPEdorItemSchema.getEdorType());
                tLPPersonDB.setCustomerNo(tCustomerNo);
                if (!tLPPersonDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "PrtAppEndorsementBL";
                    tError.functionName = "getInsuredCM";
                    tError.errorMessage = "LPPersonDB不存在相应记录!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LPPersonSchema tLPPersonSchema = tLPPersonDB.getSchema();

                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(tCustomerNo);
                if (!tLDPersonDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "PrtAppEndorsementBL";
                    tError.functionName = "getInsuredCM";
                    tError.errorMessage = "LDPersonDB不存在相应记录!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();

                String printCustomer;
                if (isChangeInsured) {
                    // printCustomer = "被保人：" + tLDPersonSchema.getName() +
                    //                 " 客户号：" + tLDPersonSchema.getCustomerNo() +
                    //                 "\n";
                    printCustomer = "被保人：" + tLPInsuredSchema.getName() +
                                    " 客户号：" + tLPInsuredSchema.getInsuredNo() +
                                    "\n";

                } else {
                    printCustomer = "投保人：" + tLPAppntSchema.getAppntName() +
                                    " 客户号：" + tLPAppntSchema.getAppntNo() +
                                    "\n";
                }
                textTag.add("CM_CUSTOMER" + i, printCustomer);

                tlistTable = new ListTable();
                tlistTable.setName("CM" + i);

                if (isChangeInsured) {
                    //姓名变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getName()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getName()))) {
                        String[] change = new String[1];
                        change[0] = "姓名：" +
                                    CommonBL.notNull(tLCInsuredSchema.getName()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getName());
                        tlistTable.add(change);
                    }
                    //生日变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getBirthday()).equals(
                            StrTool.cTrim(tLPInsuredSchema.getBirthday()))) {
                        String[] change = new String[1];
                        change[0] = "生日：" +
                                    CommonBL.decodeDate(tLCInsuredSchema.
                                getBirthday()) +
                                    "  变更为：" +
                                    CommonBL.decodeDate(tLPInsuredSchema.
                                getBirthday());
                        tlistTable.add(change);
                    }

                    //性别变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getSex()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getSex()))) {
                        String[] change = new String[1];
                        change[0] = "性别：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLCInsuredSchema.getSex()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLPInsuredSchema.getSex());
                        tlistTable.add(change);
                    }

                    //证件类型变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getIDType()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getIDType()))) {
                        String[] change = new String[1];
                        change[0] = "证件类型：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLCInsuredSchema.getIDType()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLPInsuredSchema.getIDType());
                        tlistTable.add(change);
                    }

                    //证件号码变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getIDNo()).equals(
                            StrTool.
                            cTrim(
                                    tLPInsuredSchema.getIDNo()))) {
                        String[] change = new String[1];
                        change[0] = "证件号码：" +
                                    CommonBL.notNull(tLCInsuredSchema.getIDNo()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getIDNo());
                        tlistTable.add(change);
                    }
                    
                    //证件有效时间变更
                    if(!StrTool.cTrim(tLCInsuredSchema.getIDStartDate()).equals(
                            StrTool.
                            cTrim(
                            		tLPInsuredSchema.getIDStartDate()))){
                        String[] change = new String[1];
                        change[0] = "证件生效时间：" +
                                    CommonBL.notNull(tLCInsuredSchema.getIDStartDate()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getIDStartDate());
                        tlistTable.add(change);
                    }
                    
                    //证件失效时间变更
                    if(!StrTool.cTrim(tLCInsuredSchema.getIDEndDate()).equals(
                            StrTool.
                            cTrim(
                            		tLPInsuredSchema.getIDEndDate()))){
                        String[] change = new String[1];
                        change[0] = "证件失效时间：" +
                                    CommonBL.notNull(tLCInsuredSchema.getIDEndDate()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getIDEndDate());
                        tlistTable.add(change);
                    } 

                    //职业(Code)+职业等级变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getOccupationCode()).
                        equals(
                                StrTool.cTrim(tLPInsuredSchema.
                                              getOccupationCode()))) {
                        String[] change = new String[1];
                        change[0] = "职业：" +
                                    ChangeCodeBL.getCodeName("OccupationCode",
                                tLCInsuredSchema.getOccupationCode(),
                                "OccupationCode")
                                    + "(风险" +
                                    tLCInsuredSchema.getOccupationType()
                                    + "级)"
                                    + "\n变更为：" +
                                    ChangeCodeBL.getCodeName("OccupationCode",
                                tLPInsuredSchema.getOccupationCode(),
                                "OccupationCode")
                                    + "(风险" +
                                    tLPInsuredSchema.getOccupationType()
                                    + "级)"
                                    ;

                        tlistTable.add(change);
                    }

                    //婚姻状况变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getMarriage()).equals(
                            StrTool.cTrim(tLPInsuredSchema.getMarriage()))) {
                        String[] change = new String[1];
                        change[0] = "婚姻状况：" +
                                    ChangeCodeBL.getCodeName("Marriage",
                                tLCInsuredSchema.getMarriage())
                                    + "  变更为：" +
                                    ChangeCodeBL.getCodeName("Marriage",
                                tLPInsuredSchema.getMarriage());
                        tlistTable.add(change);
                    }

                    //在职状态变更
                    if (!StrTool.cTrim(tLCInsuredSchema.getInsuredStat()).
                        equals(
                                StrTool.cTrim(tLPInsuredSchema.getInsuredStat()))) {
                        String[] change = new String[1];
                        change[0] = "在职状态：" +
                                    ChangeCodeBL.getCodeName("workstate",
                                tLCInsuredSchema.getInsuredStat())
                                    + "  变更为：" +
                                    ChangeCodeBL.getCodeName("workstate",
                                tLPInsuredSchema.getInsuredStat());
                        tlistTable.add(change);
                    }

                    //理赔金转帐银行变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getBankCode()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getBankCode()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐银行：" +
                                    ChangeCodeBL.getCodeName("bank",
                                tLCInsuredSchema.getBankCode(), "BankCode") +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("bank",
                                tLPInsuredSchema.getBankCode(), "BankCode");

                        tlistTable.add(change);
                    }

//理赔金转帐帐号变更
                    if (!StrTool.cTrim(tLPInsuredSchema.getBankAccNo()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getBankAccNo()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐帐号：" +
                                    StrTool.cTrim(tLCInsuredSchema.getBankAccNo()) +
                                    "  变更为：" +
                                    StrTool.cTrim(tLPInsuredSchema.getBankAccNo());
                        tlistTable.add(change);
                    }

                    if (!StrTool.cTrim(tLPInsuredSchema.getAccName()).equals(
                            StrTool.
                            cTrim(
                                    tLCInsuredSchema.getAccName()))) {
                        String[] change = new String[1];
                        change[0] = "理赔金转帐帐户：" +
                                    StrTool.cTrim(tLCInsuredSchema.getAccName()) +
                                    "  变更为：" +
                                    StrTool.cTrim(tLPInsuredSchema.getAccName());
                        tlistTable.add(change);
                    }

//与主被保人关系变更
                    String relation = "";
                    String relationtoappnt = "";
//LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                    tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
                    tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                    if (tLCInsuredDB.getInfo()) {
                        relation = tLPInsuredDB.getRelationToMainInsured();
                        relationtoappnt=tLPInsuredDB.getRelationToAppnt();
                    }

                    if (!StrTool.cTrim(relation).equals(
                            StrTool.cTrim(tLCInsuredDB.getRelationToMainInsured()))) {
                        String[] change = new String[1];
                        change[0] = "与主被保人关系：" +
                                    ChangeCodeBL.getCodeName("Relation",
                                tLCInsuredDB.getRelationToMainInsured()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("Relation",
                                relation);
                        tlistTable.add(change);
                    }
                  //  与投保人的关系：
                    
                    
                    if (!StrTool.cTrim(relationtoappnt).equals(
                            StrTool.cTrim(tLCInsuredDB.getRelationToAppnt()))) {
                        String[] change = new String[1];
                        change[0] = "与投保人的关系：" +
                                    ChangeCodeBL.getCodeName("Relation",
                                tLCInsuredDB.getRelationToAppnt()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("Relation",
                                    		relationtoappnt);
                        System.out.println(relationtoappnt+"**********************************************");
                       
                        System.out.println(change[0]+"**********************************************");
                        tlistTable.add(change);
                    }
                    
                    /*
                     * 添加字段 CM by 【OoO?】
                     */
                    if (!StrTool.cTrim(tLCInsuredSchema.getNativePlace()).equals(
                    		StrTool.cTrim(tLPInsuredSchema.getNativePlace()))) {
                    	String cNativePlace = null;
                    	String pNativePlace = null;
                    	
                    	 if(tLCInsuredSchema.getNativePlace()==null)
                     	{                    		                    		
                     		cNativePlace="空白";
                     	}else
                     	if(tLCInsuredSchema.getNativePlace().equals("HK"))
                     	{                    		
                     		cNativePlace="港澳台人士";
                     	}else
                        if(tLCInsuredSchema.getNativePlace().equals("ML"))
                        {
                     	   cNativePlace="中国大陆";
                     	}else
                        if(tLCInsuredSchema.getNativePlace().equals("OT"))
                        {
                     	   cNativePlace="其它";
                        }else
                        if(tLCInsuredSchema.getNativePlace().equals("OS"))
                        {
                     	   cNativePlace="外籍人士";
                        }
                     	
                  	  if(tLPInsuredSchema.getNativePlace()==null)
                   	  {                    		
                  		 pNativePlace="空白";
                   	  }else
                        if(tLPInsuredSchema.getNativePlace().equals("HK"))
                        {
                     	   pNativePlace="港澳台人士";
                        }else
                        if(tLPInsuredSchema.getNativePlace().equals("ML"))
                        {
                     	   pNativePlace="中国大陆";
                        }else
                        if(tLPInsuredSchema.getNativePlace().equals("OT"))
                        {
                     	   pNativePlace="其它";
                        }else
                        if(tLPInsuredSchema.getNativePlace().equals("OS"))
                        {
                     	   pNativePlace="外籍人士";
                        }
                     	
                  	System.out.println(cNativePlace+"**********************************************");
                  	System.out.println(pNativePlace+"**********************************************");	
                  	
                        String[] change = new String[1];
                        change[0] = "国籍：" +
                        CommonBL.notNull(cNativePlace) +
                                    "  变更为：" +
                                    CommonBL.notNull(pNativePlace);
                        tlistTable.add(change);
                    }
                    //----------年薪-------
                    if (tLCInsuredSchema.getSalary()!=tLPInsuredSchema.getSalary())
                    {
                        String[] change = new String[1];
                        change[0] = "年薪：" +
                                    tLCInsuredSchema.getSalary() + "万元"+
                                    "  变更为：" +
                                    tLPInsuredSchema.getSalary()+"万元";
                        tlistTable.add(change);
                    }
                    //--------岗位---------
                    if (!StrTool.cTrim(tLCInsuredSchema.getPosition()).equals(
                            StrTool.
                            cTrim(tLPInsuredSchema.getPosition()))) {
                        String[] change = new String[1];
                        change[0] = "岗位：" +
                        CommonBL.notNull(tLCInsuredSchema.getPosition()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPInsuredSchema.getPosition());
                        tlistTable.add(change);
                    }
                    //新增税优变更社会信用代码和税务登记证代码 2018-2-23 liuzhenjiang
                    if(lcContSubSchema.getTaxFlag()!=null&&lcContSubSchema.getTaxFlag().equals("1")){
                    	//-------个税征收方式-------
                    	if(!StrTool.cTrim(lcContSubSchema.getTaxPayerType()).equals(StrTool.cTrim(lpContSubSchema.getTaxPayerType()))){
                    		String[] change = new String[1];
                    		change[0] = "个税征收方式：" +
                    		ChangeCodeBL.getCodeName("taxpayertype",lcContSubSchema.getTaxPayerType()) +
                    		"  变更为：" +
                    		ChangeCodeBL.getCodeName("taxpayertype",lpContSubSchema.getTaxPayerType());
                    		tlistTable.add(change);
                    	}
                    	//-------- 个人税务登记号-----
                    	if(!StrTool.cTrim(lcContSubSchema.getTaxNo()).equals(StrTool.cTrim(lpContSubSchema.getTaxNo()))){
                    		String[] change = new String[1];
                    		change[0] = "个人税务登记号：" +
                    		CommonBL.notNull(lcContSubSchema.getTaxNo()) +
                    		"  变更为：" +
                    		CommonBL.notNull(lpContSubSchema.getTaxNo());
                    		tlistTable.add(change);
                    	}
                    	//-------个人社会信用代码---------
                    	if(!StrTool.cTrim(lcContSubSchema.getCreditCode()).equals(StrTool.cTrim(lpContSubSchema.getCreditCode()))){
                    		String[] change = new String[1];
                    		change[0] = "个人社会信用代码：" +
                    		CommonBL.notNull(lcContSubSchema.getCreditCode()) +
                    		"  变更为：" +
                    		CommonBL.notNull(lpContSubSchema.getCreditCode());
                    		tlistTable.add(change);
                    	}
                    	//--------单位税务登记证代码-------
                    	if(!StrTool.cTrim(lcContSubSchema.getGTaxNo()).equals(StrTool.cTrim(lpContSubSchema.getGTaxNo()))){
                    		String[] change = new String[1];
                    		change[0] = "单位税务登记证代码：" +
                    		CommonBL.notNull(lcContSubSchema.getGTaxNo()) +
                    		"  变更为：" +
                    		CommonBL.notNull(lpContSubSchema.getGTaxNo());
                    		tlistTable.add(change);
                    	}
                    	//--------单位社会信用代码-------
                    	if(!StrTool.cTrim(lcContSubSchema.getGOrgancomCode()).equals(StrTool.cTrim(lpContSubSchema.getGOrgancomCode()))){
                    		String[] change = new String[1];
                    		change[0] = "单位社会信用代码：" +
                    		CommonBL.notNull(lcContSubSchema.getGOrgancomCode()) +
                    		"  变更为：" +
                    		CommonBL.notNull(lpContSubSchema.getGOrgancomCode());
                    		tlistTable.add(change);
                    	}
                    }
                    //--------END---------
                    
                } else {
                    //姓名变更
                    if (!StrTool.cTrim(tLCAppntSchema.getAppntName()).equals(
                            StrTool.
                            cTrim(tLPAppntSchema.getAppntName()))) {
                        String[] change = new String[1];
                        change[0] = "姓名：" + tLCAppntSchema.getAppntName() +
                                    "  变更为：" + tLPAppntSchema.getAppntName();
                        tlistTable.add(change);
                    }
                    //生日变更
                    if (!StrTool.cTrim(tLCAppntSchema.getAppntBirthday()).
                        equals(
                                StrTool.cTrim(tLPAppntSchema.getAppntBirthday()))) {
                        String[] change = new String[1];
                        change[0] = "生日：" +
                                    CommonBL.decodeDate(tLCAppntSchema.
                                getAppntBirthday()) +
                                    "  变更为：" +
                                    CommonBL.decodeDate(tLPAppntSchema.
                                getAppntBirthday());
                        tlistTable.add(change);
                    }

                    //性别变更
                    if (!StrTool.cTrim(tLCAppntSchema.getAppntSex()).equals(
                            StrTool.
                            cTrim(tLPAppntSchema.getAppntSex()))) {
                        String[] change = new String[1];
                        change[0] = "性别：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLCAppntSchema.getAppntSex()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("Sex",
                                tLPAppntSchema.getAppntSex());
                        tlistTable.add(change);
                    }
                    
      
                    
                    //证件类型变更
                    if (!StrTool.cTrim(tLCAppntSchema.getIDType()).equals(
                            StrTool.
                            cTrim(tLPAppntSchema.getIDType()))) {
                        String[] change = new String[1];
                        change[0] = "证件类型：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLCAppntSchema.getIDType()) +
                                    "  变更为：" +
                                    ChangeCodeBL.getCodeName("IDType",
                                tLPAppntSchema.getIDType());
                        tlistTable.add(change);
                    }

                    //证件号码变更
                    if (!StrTool.cTrim(tLCAppntSchema.getIDNo()).equals(
                            StrTool.
                            cTrim(
                                    tLPAppntSchema.getIDNo()))) {
                        String[] change = new String[1];
                        change[0] = "证件号码：" +
                                    CommonBL.notNull(tLCAppntSchema.getIDNo()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPAppntSchema.getIDNo());
                        tlistTable.add(change);
                    }
                    
                    //证件有效时间变更
                    if(!StrTool.cTrim(tLCAppntSchema.getIDStartDate()).equals(
                            StrTool.
                            cTrim(
                                    tLPAppntSchema.getIDStartDate()))){
                        String[] change = new String[1];
                        change[0] = "证件生效时间：" +
                                    CommonBL.notNull(tLCAppntSchema.getIDStartDate()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPAppntSchema.getIDStartDate());
                        tlistTable.add(change);
                    }
                    
                    //证件失效时间变更
                    if(!StrTool.cTrim(tLCAppntSchema.getIDEndDate()).equals(
                            StrTool.
                            cTrim(
                                    tLPAppntSchema.getIDEndDate()))){
                        String[] change = new String[1];
                        change[0] = "证件失效时间：" +
                                    CommonBL.notNull(tLCAppntSchema.getIDEndDate()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPAppntSchema.getIDEndDate());
                        tlistTable.add(change);
                    }                    
                    

                    //职业(Code)+职业等级变更
                    if (!StrTool.cTrim(tLCAppntSchema.getOccupationCode()).
                        equals(
                                StrTool.cTrim(tLPAppntSchema.getOccupationCode()))) {
                        String[] change = new String[1];
                        change[0] = "职业：" +
                                    ChangeCodeBL.getCodeName("OccupationCode",
                                tLCAppntSchema.getOccupationCode(),
                                "OccupationCode")
                                    + "(风险" +
                                    tLCAppntSchema.getOccupationType()
                                    + "级)"
                                    + "\n变更为：" +
                                    ChangeCodeBL.getCodeName("OccupationCode",
                                tLPAppntSchema.getOccupationCode(),
                                "OccupationCode")
                                    + "(风险" +
                                    tLPAppntSchema.getOccupationType()
                                    + "级)"
                                    ;

                        tlistTable.add(change);
                    }

                    //婚姻状况变更
                    if (!StrTool.cTrim(tLCAppntSchema.getMarriage()).equals(
                            StrTool.cTrim(tLPAppntSchema.getMarriage()))) {
                        String[] change = new String[1];
                        change[0] = "婚姻状况：" +
                                    ChangeCodeBL.getCodeName("Marriage",
                                tLCAppntSchema.getMarriage()) + "  变更为：" +
                                    ChangeCodeBL.getCodeName("Marriage",
                                tLPAppntSchema.getMarriage());
                        tlistTable.add(change);
                    }
                    
                    /*
                     * 添加字段 CM by 【OoO?】
                     */
                    //--------国籍---------
                    if (!StrTool.cTrim(tLCAppntSchema.getNativePlace()).equals(
                    		StrTool.cTrim(tLPAppntSchema.getNativePlace()))) {
                    	String xNativePlace = null;
                    	String tNativePlace = null;
                    	
                    	 if(tLCAppntSchema.getNativePlace()==null)
                     	{                    		
                    		 xNativePlace="空白";
                     	}else
                     	if(tLCAppntSchema.getNativePlace().equals("HK"))
                     	{                    		
                     		xNativePlace="港澳台人士";
                     	}else
                        if(tLCAppntSchema.getNativePlace().equals("ML"))
                        {
                        	xNativePlace="中国大陆";
                     	}else
                        if(tLCAppntSchema.getNativePlace().equals("OT"))
                        {
                        	xNativePlace="其它";
                        }else
                        if(tLCAppntSchema.getNativePlace().equals("OS"))
                        {
                        	xNativePlace="外籍人士";
                        }
                     	
                  	  if(tLPAppntSchema.getNativePlace()==null)
                   	  {                    		
                  		tNativePlace="空白";
                   	  }else
                        if(tLPAppntSchema.getNativePlace().equals("HK"))
                        {
                        	tNativePlace="港澳台人士";
                        }else
                        if(tLPAppntSchema.getNativePlace().equals("ML"))
                        {
                        	tNativePlace="中国大陆";
                        }else
                        if(tLPAppntSchema.getNativePlace().equals("OT"))
                        {
                        	tNativePlace="其它";
                        }else
                        if(tLPAppntSchema.getNativePlace().equals("OS"))
                        {
                        	tNativePlace="外籍人士";
                        }
                     	
                    	
                  	System.out.println(xNativePlace+"**********************************************");
                  	System.out.println(tNativePlace+"**********************************************");	
                  	  
                        String[] change = new String[1];
                        change[0] = "国籍：" +
                        CommonBL.notNull(xNativePlace) +
                                    "  变更为：" +
                                    CommonBL.notNull(tNativePlace);                    	                       
                        tlistTable.add(change);
                    }
                    //----------年薪-------
                    if (tLCAppntSchema.getSalary()!=tLPAppntSchema.getSalary())
                    {
                        String[] change = new String[1];
                        change[0] = "年薪：" +
                        tLCAppntSchema.getSalary() + "万元"+
                                    "  变更为：" +
                                    tLPAppntSchema.getSalary()+ "万元";
                        tlistTable.add(change);
                    }
                    //--------岗位---------
                    if (!StrTool.cTrim(tLCAppntSchema.getPosition()).equals(
                            StrTool.
                            cTrim(tLPAppntSchema.getPosition()))) {
                        String[] change = new String[1];
                        change[0] = "岗位：" +
                        CommonBL.notNull(tLCAppntSchema.getPosition()) +
                                    "  变更为：" +
                                    CommonBL.notNull(tLPAppntSchema.getPosition());
                        tlistTable.add(change);
                    }
                    //--------END---------

                }

                String[] strhead = new String[1];
                strhead[0] = "批改内容";
                xmlexport.addListTable(tlistTable, strhead);

                //资料更新导致的费用变更
                sql = "select sum(GetMoney) from LJSGetEndorse " +
                      "where EndorsementNo = '" +
                      tLPEdorItemSchema.getEdorNo() + "' " +
                      "and FeeOperationType = '" +
                      tLPEdorItemSchema.getEdorType() + "' " +
                      "and InsuredNo = '" +
                      tLPEdorItemSchema.getInsuredNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String chgPrem = tExeSQL.getOneValue(sql);
                
                String fenHongXian="select 1 from lcpol a where insuredno='"+tLPEdorItemSchema.getInsuredNo()+"' and  contno='"+tLPEdorItemSchema.getContNo()
                                  +"' and exists (select 1 from lppol where amnt!=a.amnt and edorno ='"+tLPEdorItemSchema.getEdorNo()+"' and polno=a.polno and insuredno=a.insuredno and edortype='"+tLPEdorItemSchema.getEdorType()+"')"
                                  ;
                String fenHongXianFlag=tExeSQL.getOneValue(fenHongXian);
                if ((chgPrem != null && !"".equals(chgPrem) &&
                    Double.parseDouble(chgPrem) != 0)||(fenHongXianFlag!=null&&!fenHongXianFlag.equals(""))) { //保费变更，需要添加费用变更打印内容
                    /* String printContent = "由于上述资料变更 ，保单" +
                                           tLPEdorItemSchema.getContNo() +
                                           "相关险种承保内容变更如下：";
                     textTag.add("CM_FEE" + i, printContent);
                     */
                    sql = "select distinct(contno) from LJSGetEndorse " +
                          "where EndorsementNo = '" +
                          tLPEdorItemSchema.getEdorNo() + "' " +
                          "and FeeOperationType = '" +
                          tLPEdorItemSchema.getEdorType() + "' " +
                          "and InsuredNo = '" +
                          tLPEdorItemSchema.getInsuredNo() + "' " +
                          //此处需要过滤万能险种和附加重疾
                          		" and riskcode not in (select riskcode from lmriskapp where risktype4='4') " +
                          		" and riskcode not in ('231001') ";
                    tExeSQL = new ExeSQL();
                    SSRS g = tExeSQL.execSQL(sql);
                    String[][] strContno = g.getAllData();

                    tlistTable = new ListTable();
                    tlistTable.setName("FEE" + i);
                    for (int k = 0; k < strContno.length; k++) {
                        sql = "select * from lcpol where polno in " +
                              "(select polno from lppol where edorno= '" +
                              tLPEdorItemSchema.getEdorNo() + "' and " +
                              "edortype='" + tLPEdorItemSchema.getEdorType() +
                              "' and insuredno='" +
                              tLPEdorItemSchema.getInsuredNo() + "' and " +
                              "contno ='" + strContno[k][0] + "')";
                        LCPolDB tLCPolDB = new LCPolDB();
                        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
                        if (tLCPolSet == null || tLCPolSet.size() < 1) {
                            CError tError = new CError();
                            tError.moduleName = "PrtAppEndorsementBL";
                            tError.functionName = "getInsuredCM";
                            tError.errorMessage = "LCPol不存在相应的记录!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                        String[] change = new String[1];
                        change[0] = "由于上述资料变更 ，保单" +
                                    strContno[k][0] +
                                    "相关险种承保内容变更如下：";

                        tlistTable.add(change);

                        /*String printContent = "由于上述资料变更 ，保单" +
                                              strContno[k][0] +
                                              "相关险种承保内容变更如下：";
                         textTag.add("CM_FEE" + k, printContent);
                         */

                        for (int j = 0; j < tLCPolSet.size(); j++) {
                            LCPolSchema tLCPOLSchema = tLCPolSet.get(j + 1);
                            //得到各个险种的变更费用
                            /* sql = "select sum(GetMoney) from LJSGetEndorse " +
                                   "where EndorsementNo = '" +
                                   tLPEdorItemSchema.getEdorNo() + "' " +
                                   "and FeeOperationType = '" +
                                   tLPEdorItemSchema.getEdorType() + "' " +
                                   "and InsuredNo = '" +
                                   tLPEdorItemSchema.getInsuredNo() + "' " +
                                   "and RiskCode = '" +
                                   tLCPOLSchema.getRiskCode() + "' ";
                             */
                            sql = "select sum(GetMoney) from LJSGetEndorse " +
                                  "where EndorsementNo = '" +
                                  tLPEdorItemSchema.getEdorNo() + "' " +
                                  "and FeeOperationType = '" +
                                  tLPEdorItemSchema.getEdorType() + "' " +
                                  "and InsuredNo = '" +
                                  tLPEdorItemSchema.getInsuredNo() + "' " +
                                  "and PolNo = '" +
                                  tLCPOLSchema.getPolNo() + "' " +
                                  "and contno = '" + tLCPOLSchema.getContNo() +
                                  "'";

                            tExeSQL = new ExeSQL();
                            chgPrem = tExeSQL.getOneValue(sql);
                            if ((chgPrem == null) || (chgPrem.equals(""))) {
                                continue;
                            }

                            LPPolDB tLPPolDB = new LPPolDB();
                            tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                            tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
                            tLPPolDB.setPolNo(tLCPOLSchema.getPolNo());
                            if (!tLPPolDB.getInfo()) {
                                CError tError = new CError();
                                tError.moduleName = "PrtAppEndorsementBL";
                                tError.functionName = "getInsuredCM";
                                tError.errorMessage = "LPPol不存在相应的记录!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                            LMRiskDB tLMRiskDB = new LMRiskDB();
                            tLMRiskDB.setRiskCode(tLCPOLSchema.getRiskCode());
                            if (!tLMRiskDB.getInfo()) {
                                CError tError = new CError();
                                tError.moduleName = "PrtAppEndorsementBL";
                                tError.functionName = "getInsuredCM";
                                tError.errorMessage = "LMRisk不存在相应的记录!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }

                            String[] fee = new String[1];
                            fee[0] = "序号" + tLCPOLSchema.getRiskSeqNo() +
                            "险种（" +
                            tLMRiskDB.getRiskName() + "）保额变更为："+tLPPolDB.getAmnt()+",保费变更为：" +
                            tLPPolDB.getPrem();

                   if (Double.parseDouble(chgPrem) < 0) {
                       fee[0] += ", 应退费" +
                               Math.abs(Double.
                                        parseDouble(
                               chgPrem)) +
                               "元。\n";

                   } else if (Double.parseDouble(chgPrem) > 0) {
                       fee[0] += ", 应补费" +
                               Double.parseDouble(
                                       chgPrem) +
                               "元。\n";
                   }
                   else
                   {
                   	 fee[0] += "。";
                   }
                            tlistTable.add(fee);
                        }
                    }
                }
                String[] feeHead = new String[1];
                strhead[0] = "费用";
                xmlexport.addListTable(tlistTable, feeHead);
            }
            tFormerCustomerNo = tCustomerNo;
            
//          万能和万能附加重疾部分批单
            String feeInfo="";
            String WNsql = "select distinct contno from LPInsureAccTrace where EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS WNssrs = tExeSQL.execSQL(WNsql);
            String[][] WNContno = WNssrs.getAllData();
            for (int l = 0; l < WNContno.length; l++) {
            	if(CommonBL.hasULIRisk(WNContno[l][0])){//如果是万能险
//            		除了附加重疾外的万能费用
            		String tmoney=tExeSQL.getOneValue("select sum(money) from LPInsureAccTrace where payplancode not in ('231001') and ContNo='"+WNContno[l][0]+"' and EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ");
//					万能附加重疾的客户资料变更费用            	
            		String tEXmoney=tExeSQL.getOneValue("select sum(money) from LPInsureAccTrace where payplancode in ('231001') and ContNo='"+WNContno[l][0]+"' and EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ");
            		if(tmoney != null && !"".equals(tmoney) &&
                            Double.parseDouble(tmoney) != 0){
            			if(Double.parseDouble(tmoney)>0){//退费情况
                			feeInfo+="保单号码"+WNContno[l][0]+"的万能保单主险因客户资料变更需要退回风险保费"+tmoney+"元，本次退回的风险保费将全部进入万能个人帐户。\n";
                		}
                		else{//补费情况
                			feeInfo+="保单号码"+WNContno[l][0]+"的万能保单主险因客户资料变更需要补交风险保费"+Math.abs(Double.parseDouble(tmoney))+"元，本次补交的风险保费将全部从万能个人帐户中扣除。\n";
                		}
            		}
            		if(tEXmoney != null && !"".equals(tEXmoney) &&
                            Double.parseDouble(tEXmoney) != 0){
            			if(Double.parseDouble(tEXmoney)>0){//退费情况
                			feeInfo+="保单号码"+WNContno[l][0]+"的万能保单附加重疾险种因客户资料变更需要退回风险保费"+tEXmoney+"元，本次退回的风险保费将全部进入万能个人帐户。\n";
                		}
                		else{//补费情况
                			feeInfo+="保单号码"+WNContno[l][0]+"的万能保单附加重疾险种因客户资料变更需要补交风险保费"+Math.abs(Double.parseDouble(tEXmoney))+"元，本次补交的风险保费将全部从万能个人帐户中扣除。\n";
                		}
            		}
            	}
            }
            //税优客户资料变更
            String isTPHIsql = "select distinct contno from LPInsureAccTrace a where edorno ='"+tLPEdorItemSchema.getEdorNo()+"'  and edortype='"+tLPEdorItemSchema.getEdorType()+"' "
            				+" and exists (select 1 from lppol where edorno ='"+tLPEdorItemSchema.getEdorNo()+"' and polno=a.polno and edortype='"+tLPEdorItemSchema.getEdorType()+"')"
            				+" and exists (select 1 from lmriskapp where riskcode=a.riskcode and taxoptimal ='Y') ";
            				
            tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(isTPHIsql);
            String[][] SYContno = tSSRS.getAllData();
            for (int k = 0; k < SYContno.length; k++) {
                sql = "select * from lcpol where polno in " +
                      "(select polno from lppol where edorno= '" +
                      tLPEdorItemSchema.getEdorNo() + "' and " +
                      "edortype='" + tLPEdorItemSchema.getEdorType() +
                      "' and insuredno='" +
                      tLPEdorItemSchema.getInsuredNo() + "' and " +
                      "contno ='" + SYContno[k][0] + "')";
                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
                if (tLCPolSet == null || tLCPolSet.size() < 1) {
                    CError tError = new CError();
                    tError.moduleName = "PrtAppEndorsementBL";
                    tError.functionName = "getInsuredCM";
                    tError.errorMessage = "LCPol不存在相应的记录!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                String[] change = new String[1];
                change[0] = "由于上述资料变更 ，保单" +
                			SYContno[k][0] +
                            "相关险种承保内容变更如下：";
                tlistTable.add(change);
                for (int j = 0; j < tLCPolSet.size(); j++) {
                    LCPolSchema tLCPOLSchema = tLCPolSet.get(j + 1);
                    sql = "select 1 from lcpol lc,lppol lp " +
                          " where lc.polno=lp.polno " +
                          " and lp.edorno= '" +
                          tLPEdorItemSchema.getEdorNo() + "' " +
                          " and lp.edortype = '" +
                          tLPEdorItemSchema.getEdorType() + "' " +
                          " and lp.InsuredNo = '" +
                          tLPEdorItemSchema.getInsuredNo() + "' " +
                          " and lp.PolNo = '" +
                          tLCPOLSchema.getPolNo() + "' " +
                          " and lp.contno = '" + tLCPOLSchema.getContNo() +
                          "' and lc.prem<>lp.prem ";

                    tExeSQL = new ExeSQL();
                    String chgPrem = tExeSQL.getOneValue(sql);


                    LPPolDB tLPPolDB = new LPPolDB();
                    tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                    tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
                    tLPPolDB.setPolNo(tLCPOLSchema.getPolNo());
                    if (!tLPPolDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "PrtAppEndorsementBL";
                        tError.functionName = "getInsuredCM";
                        tError.errorMessage = "LPPol不存在相应的记录!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    LMRiskDB tLMRiskDB = new LMRiskDB();
                    tLMRiskDB.setRiskCode(tLCPOLSchema.getRiskCode());
                    if (!tLMRiskDB.getInfo()) {
                        CError tError = new CError();
                        tError.moduleName = "PrtAppEndorsementBL";
                        tError.functionName = "getInsuredCM";
                        tError.errorMessage = "LMRisk不存在相应的记录!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if ((chgPrem != null) && (!chgPrem.equals(""))) {
                        String[] fee = new String[1];
                        fee[0] = "序号" + tLCPOLSchema.getRiskSeqNo() +
                        "险种（" +
                        tLMRiskDB.getRiskName() + "）保费变更为：" +
                        tLPPolDB.getPrem();

                        fee[0] += "。";
                        tlistTable.add(fee);
                        String[] feeHead = new String[1];
                        xmlexport.addListTable(tlistTable, feeHead);
                    }

            		String tmoney=tExeSQL.getOneValue("select sum(getmoney) from LJSGetEndorse where  ContNo='"+tLCPOLSchema.getContNo()+"' and endorsementno = '" +tLPEdorItemSchema.getEdorNo() + "' and FeeOperationType = '" + tLPEdorItemSchema.getEdorType() + "' ");

            		if(tmoney != null && !"".equals(tmoney) &&
                            Double.parseDouble(tmoney) != 0){
            			if(Double.parseDouble(tmoney)<0){//退费情况
                			feeInfo+="保单号码"+tLCPOLSchema.getContNo()+"的万能保单主险因客户资料变更需要退回风险保费"+Math.abs(Double.parseDouble(tmoney))+"元。\n";
                		}
                		else{//补费情况
                			feeInfo+="保单号码"+tLCPOLSchema.getContNo()+"的万能保单主险因客户资料变更需要补交风险保费"+tmoney+"元。\n";
                		}
            		}
           }
         }
            textTag.add("feeInfo", feeInfo);
        }
        return true;
    }
    
 // 客户信息授权变更
 	private boolean getDetailAU(LPEdorItemSchema aLPEdorItemSchema) {

 		// 20141014联系方式变更保全批单增加对被保险人变更内容的显示
 		String sql = "select * from LPEdorItem " + "where EdorAcceptNo = '"
 				+ aLPEdorItemSchema.getEdorAcceptNo() + "' " + "and EdorType='"
 				+ aLPEdorItemSchema.getEdorType() + "' "
 				+ "order by edorno,insuredno";
 		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
 		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
 		if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
 			CError tError = new CError();
 			tError.moduleName = "PrtAppEndorsementBL";
 			tError.functionName = "getInsuredCM";
 			tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo()
 					+ "下无相应的保全项目信息!";
 			this.mErrors.addOneError(tError);
 			return false;
 		}
 		LPPersonDB tLPPersonDB = null;
 		LPPersonSchema tLPPersonSchema = null;
 		LPPersonSet tLPPersonSet = null;
 		LDPersonDB tLDPersonDB = null;
 		LDPersonSchema tLDPersonSchema = null;
 		LDPersonSet tLDPersonSet = null;

 		tlistTable = new ListTable();
 		tlistTable.setName("AU");
 		for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
 			LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
 			tLPPersonDB = new LPPersonDB();
 			tLPPersonDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
 			tLPPersonDB.setEdorType(tLPEdorItemSchema.getEdorType());
 			tLPPersonDB.setCustomerNo(tLPEdorItemSchema.getInsuredNo());
 			tLPPersonSet = new LPPersonSet();
 			tLPPersonSet.set(tLPPersonDB.query());
 			if (tLPPersonDB.mErrors.needDealError() || tLPPersonSet.size() == 0) {
 				buildError("getDetailAU", "保全客户表LPPerson中无相关批单号的记录");
 				return false;
 			}
 			tLPPersonSchema = tLPPersonSet.get(1);// 只有一条

 			tLDPersonDB = new LDPersonDB();
 			tLDPersonDB.setCustomerNo(tLPEdorItemSchema.getInsuredNo());
 			if (!tLDPersonDB.getInfo()) {
 				buildError("getDetailAU",
 						"查询客户号为" + tLPEdorItemSchema.getInsuredNo()
 								+ "的客户信息时失败!");
 				return false;
 			}
 			tLDPersonSet = new LDPersonSet();
 			tLDPersonSet.set(tLDPersonDB.query());
 			tLDPersonSchema = tLDPersonSet.get(1);
 			
 			// -------------
 			String[] strCus = new String[1];
 			strCus[0] = "客户：" + tLPPersonSchema.getName() + " 客户号："
 					+ tLPPersonSchema.getCustomerNo() + "\n";
 			tlistTable.add(strCus);

 			// --------------------------------------
 			String dAuthorization = tLDPersonSchema.getAuthorization();
			String pAuthorization = tLPPersonSchema.getAuthorization();
			String[] mStrings = { "否", "是" };
			if (dAuthorization == null || dAuthorization == " ") {
				if (!StrTool.cTrim(dAuthorization).equals(
						StrTool.cTrim(pAuthorization))) {
					String[] strArr = new String[1];
					strArr[0] = "授权标识："
							+ CommonBL.notNull(dAuthorization)
							+ "\n变更为："
							+ CommonBL
									.notNull(pAuthorization
											+ "--"
											+ mStrings[Integer
													.parseInt(pAuthorization)]);
					tlistTable.add(strArr);
				}
			} else {
				if (!StrTool.cTrim(dAuthorization).equals(
						StrTool.cTrim(pAuthorization))) {
					String[] strArr = new String[1];
					strArr[0] = "授权标识："
							+ CommonBL
									.notNull(dAuthorization
											+ "--"
											+ mStrings[Integer
													.parseInt(dAuthorization)])
							+ "\n变更为："
							+ CommonBL
									.notNull(pAuthorization
											+ "--"
											+ mStrings[Integer
													.parseInt(pAuthorization)]);
					tlistTable.add(strArr);
				}
			}

 			if (i != tLPEdorItemSet.size()) {
 				String[] strSpe = new String[1];
 				strSpe[0] = "\n\n";
 				tlistTable.add(strSpe);
 			}
 		}
 		String[] strArrHead = new String[1];
 		strArrHead[0] = "客户信息授权变更";
 		xmlexport.addListTable(tlistTable, strArrHead);
 		String contNo = "";
 		String strCont = "select distinct contno from LPEdorItem "
 				+ " where edorno='" + aLPEdorItemSchema.getEdorNo() + "' "
 				+ " and edortype='" + aLPEdorItemSchema.getEdorType() + "' "
 				+ " with ur";
 		SSRS tSSRS = new SSRS();
 		tSSRS = new ExeSQL().execSQL(strCont);
 		for (int i = 1; i <= tSSRS.MaxRow; i++) {
 			if (i != tSSRS.MaxRow) {
 				contNo += tSSRS.GetText(i, 1) + "，";
 			} else {
 				contNo += tSSRS.GetText(i, 1);
 			}
 		}

 		textTag.add("New_Cont_Info", contNo);
 		textTag.add("AU_EdorValiDate",
 				CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
 		return true;
 	}

    //合同终止
    private boolean getDetailCT(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("CT_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("CT_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        ListTable tlistTable = new ListTable();
        tlistTable.setName("CT");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        String msql =
                "select sum(getmoney)  from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "'";
        ExeSQL aExeSQL = new ExeSQL();
        String tReturn = CommonBL.bigDoubleToCommonString(Math.abs(Double.parseDouble(aExeSQL.getOneValue(msql))),"0.00");
        System.out.println("msql:" + msql);
        if (tReturn == null || StrTool.cTrim(tReturn).equals("")) {
            mErrors.addOneError(new CError("没有相关的合同期退保记录！"));
            return false;
        }
        textTag.add("CT_Total", tReturn);

        //这里对万能险退保进行特殊处理
        if(CommonBL.hasULIRisk(aLPEdorItemSchema.getContNo()))
        {
        	boolean isOnlyIll = true; //只有附加重疾单独退保为true
        	boolean isTaxoptimal = true; //税优退保为true
//        	#1298针对万能解约数据金额处理
        	String mulisql ="select sum(getmoney) from LJSGetendorse where endorsementno ='"+aLPEdorItemSchema.getEdorNo() + "'"
        			+ " and FeeOperationType ='" + aLPEdorItemSchema.getEdorType() + "' and riskcode in (select riskcode from lmriskapp where risktype4='4')";
        	ExeSQL tExeSQL = new ExeSQL();
        	String Result = tExeSQL.getOneValue(mulisql);
        	if(null == Result || "".equals(Result)){
        		Result = "0";
        	}
        	String tULIReturn = CommonBL.bigDoubleToCommonString(Double.parseDouble(Result),"0.00");
        	
            LPPolDB  tLPPolDB=new LPPolDB();
            tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
            tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
            LPPolSet tLPPolSet = tLPPolDB.query();
            
            if (tLPPolDB.mErrors.needDealError())
            {
                CError.buildErr(this, "查询待退保险种时失败！");
                return false;
            }
            for(int i=1; i<=tLPPolSet.size() ;i++)
            {
            	String tRiskCode = tLPPolSet.get(i).getRiskCode();
            	LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                tLMRiskAppDB.setRiskCode(tRiskCode);
                if(tLMRiskAppDB.getInfo())
                {
                	String tRiskType3 = tLMRiskAppDB.getRiskType3();
                 	if(!tRiskType3.equals("3")) //重疾
                 	{
                 		isOnlyIll = false;
                 	}
                 	
                	String mTaxoptimal = tLMRiskAppDB.getTaxOptimal();
                	if(null == mTaxoptimal || !"Y".equals(mTaxoptimal)) //税优
                 	{
                 		isTaxoptimal = false;
                 	}
                }
            }
			
            if(isOnlyIll)
            {
            	String sql = "select  nvl(abs(sum(Money)), 0) from LPInsureAccTrace "
                    + "where EdorNo = '" + aLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + aLPEdorItemSchema.getEdorType() + "' "
                    + "and ContNo = '" + aLPEdorItemSchema.getContNo() + "' and MoneyType = 'RP' ";

            	SSRS tSSRS = new ExeSQL().execSQL(sql);
                if(tSSRS.GetText(1, 1).equals("0"))
                {
                    System.out.println("没有结算信息！");
                    String ULIInfo = "补扣风险保费:0元";
                    sql = "select sum(insuaccbala) from LCinsureacc where contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                    String tBala= CommonBL.bigDoubleToCommonString(Double.parseDouble(new ExeSQL().getOneValue(sql)),"0.00");
                    if(tSSRS.getMaxRow()>0)
                    {
                    	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。" + "解约手续费:0元，";
                    }
                    textTag.add("CT_ULIInfo", ULIInfo);
                }
                else
                {
                    String ULIInfo = "补扣风险保费:"+tSSRS.GetText(1, 1) + "元";
                    sql = "select sum(insuaccbala) from lpinsureacc where edorno = '" + aLPEdorItemSchema.getEdorNo() 
                    	+ "' and edortype = '" + aLPEdorItemSchema.getEdorType() + "' and contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                    String tBala= CommonBL.bigDoubleToCommonString(Double.parseDouble(new ExeSQL().getOneValue(sql)),"0.00");
                    if(tSSRS.getMaxRow()>0)
                    {
                    	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。" + "解约手续费:0元，";
                    }
                    textTag.add("CT_ULIInfo", ULIInfo);
                }
            }
            else if(isTaxoptimal)
            {
                String sql =
                        "select MoneyType, nvl(abs(sum(Money)), 0) from LPInsureAccTrace "
                        + "where OtherType = '10' "
                        + "   and OtherNo = '"
                        + aLPEdorItemSchema.getEdorNo() + "' "
                        + "   and ContNo = '" + aLPEdorItemSchema.getContNo() + "' "
                        + "   and MoneyType in('LX', 'RP') "
                        + "group by MoneyType ";
                SSRS tSSRS = new ExeSQL().execSQL(sql);
                if(tSSRS.getMaxRow()==0){
                     System.out.println("查询结算信息失败！");
                }else{

                     String ULIInfo = "补计利息：" + tSSRS.GetText(1, 2) +
                                         "元，补扣保单管理费:0元，补扣风险保费:0元，应退未满期净风险保费："+tSSRS.GetText(2, 2) +
                                         "元";
                     sql = "select sum(insuaccbala)  from lpinsureacc where edorno = '" + aLPEdorItemSchema.getEdorNo() 
                        	+ "' and edortype = '" + aLPEdorItemSchema.getEdorType() + "' and contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                     String tBala= CommonBL.bigDoubleToCommonString(Double.parseDouble(new ExeSQL().getOneValue(sql)),"0.00");
                     if(tSSRS.getMaxRow()>0)
                     {
                        	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。"
                            		+"解约手续费:"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tBala)+Double.parseDouble(tULIReturn)),"0.00")
                            		+"元，";
                     }
                     textTag.add("CT_ULIInfo", ULIInfo);
                 }
            }
            else
            {
            	String sql =
                    "select MoneyType, nvl(abs(sum(Money)), 0) from LPInsureAccTrace "
                    + "where OtherType = '10' "
                    + "   and OtherNo = '"
                    + aLPEdorItemSchema.getEdorNo() + "' "
                    + "   and ContNo = '" + aLPEdorItemSchema.getContNo() + "' "
                    + "   and MoneyType in('LX', 'MF', 'RP') "
                    + "group by MoneyType ";
            	SSRS tSSRS = new ExeSQL().execSQL(sql);
                if(tSSRS.getMaxRow()==0){
                    System.out.println("查询结算信息失败！");
                }else{
                    String ULIInfo = "补计利息：" + tSSRS.GetText(1, 2) +
                                     "元，补扣保单管理费:" +tSSRS.GetText(2, 2)+ "元，补扣风险保费:"+tSSRS.GetText(3, 2) +
                                     "元";
                    sql = "select sum(insuaccbala) from lpinsureacc where edorno = '" + aLPEdorItemSchema.getEdorNo() 
                    	+ "' and edortype = '" + aLPEdorItemSchema.getEdorType() + "' and contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                    String tBala= CommonBL.bigDoubleToCommonString(Double.parseDouble(new ExeSQL().getOneValue(sql)),"0.00");
                    if(tSSRS.getMaxRow()>0)
                    {
                    	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。"
                        		+"解约手续费:"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tBala)+Double.parseDouble(tULIReturn)),"0.00")
                        		+"元，";
                    }
                    textTag.add("CT_ULIInfo", ULIInfo);
                }
            }
        }

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet == null || tLPPolSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "PrtEndorsementBL";
            tError.functionName = "getDetailCT";
            tError.errorMessage = "LCPol不存在相应的记录!";
            this.mErrors.addOneError(tError);
            return false;

        }

        //险种退费情况
        StringBuffer sql = new StringBuffer();
        sql.append("  select polNo, riskCode, insuredNo, sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and feefinatype not in ('RFLX','RFBF','HLLX','HLBF','EB','EBLX') and contNo = '")
                .append(aLPEdorItemSchema.getContNo())
                .append("' group by polNo, riskCode, insuredNo");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql.toString());

        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String printContent[] = new String[1];

                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
                if (!tLCPolDB.getInfo()) {
                    mErrors.addOneError("没有查到险种号" + tSSRS.GetText(i, 1)
                                        + "所对应的险种");
                    return false;
                }
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(tSSRS.GetText(i, 2));
                if (!tLMRiskDB.getInfo()) {
                    mErrors.addOneError("没有查到险种编码" + tSSRS.GetText(i, 1)
                                        + "所对应的险种信息");
                    return false;
                }
                printContent[0] =
                        "被保人" + CommonBL.getInsuredName(tSSRS.GetText(i, 3))
                        + "，序号" + tLCPolDB.getRiskSeqNo() + "险种（"
                        + tLMRiskDB.getRiskName() + "）退保，" + "应退退保金"
                        + CommonBL.bigDoubleToCommonString(Math.abs(CommonBL.carry(tSSRS.GetText(i, 4))),"0.00")
                        + "元。";
                tlistTable.add(printContent);
            }
        }
                
        //金生无忧保单满期及利息信息显示
        StringBuffer ebsql = new StringBuffer();
        ebsql.append("  select sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and feefinatype ='EB' ");
        StringBuffer ebLxsql = new StringBuffer();
        ebLxsql.append("  select sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and feefinatype ='EBLX' ");
        String tEbMoney=tExeSQL.getOneValue(ebsql.toString());
        String tEbLxMoney=tExeSQL.getOneValue(ebLxsql.toString());
        if(null!=tEbMoney && (!tEbMoney.equals(""))&&(!tEbMoney.equals("null"))){
        	String printEB[] = new String[1];
        	String printEbLx[] = new String[1];
        	printEB[0] ="保单下未领取的健康维护保险金或老年护理保险金： "+CommonBL.bigDoubleToCommonString(Math.abs(CommonBL.carry(tEbMoney)),"0.00")+" 元。";
        	printEbLx[0] ="保单下未领取的健康维护保险金或老年护理保险金之利息： "+CommonBL.bigDoubleToCommonString(Math.abs(CommonBL.carry(tEbLxMoney)),"0.00")+"元。";
        	tlistTable.add(printEB);
        	tlistTable.add(printEbLx);
        }
        //*********金生无忧满期金打印处理完毕
        
        //解约保全增加保单交费信息变更功能
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPContDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPContDB.setContNo(aLPEdorItemSchema.getContNo());
        tLPContDB.getInfo();
        
		if ("4".equals(tLCContDB.getPayMode())) {
			if (!StrTool.cTrim(tLCContDB.getBankCode()).equals(
					StrTool.cTrim(tLPContDB.getBankCode()))
					|| !StrTool.cTrim(tLCContDB.getAccName()).equals(
							StrTool.cTrim(tLPContDB.getAccName()))
					|| !StrTool.cTrim(tLCContDB.getBankAccNo()).equals(
							StrTool.cTrim(tLPContDB.getBankAccNo()))) {
				String print1[] = new String[1];
				String print2[] = new String[1];
				String print3[] = new String[1];
				String print4[] = new String[1];
               
				print1[0] = "  ";
				tlistTable.add(print1);
				print2[0] = "§缴费资料变更";
				tlistTable.add(print2);
				print3[0] = "保单号" + tLCContDB.getContNo() + ",原";
				print4[0] = "现修改";

				if (!StrTool.cTrim(tLCContDB.getBankCode()).equals(
						StrTool.cTrim(tLPContDB.getBankCode()))) {
						print3[0] = print3[0] + "交易银行为："+CommonBL.notNull(ChangeCodeBL.getCodeName("Bank",tLCContDB.getBankCode(), "BankCode"))+"   ";
                 		print4[0] = print4[0] + "交易银行为："+CommonBL.notNull(ChangeCodeBL.getCodeName("Bank",tLPContDB.getBankCode(), "BankCode"))+"   ";
				}
				if (!StrTool.cTrim(tLCContDB.getBankAccNo()).equals(
						StrTool.cTrim(tLPContDB.getBankAccNo()))) {
						print3[0] = print3[0] + "交易账号为：" +CommonBL.notNull(tLCContDB.getBankAccNo())+"   ";
                 		print4[0] = print4[0] + "交易账号为：" +CommonBL.notNull(tLPContDB.getBankAccNo())+"   ";
				}
				if (!StrTool.cTrim(tLCContDB.getAccName()).equals(
						StrTool.cTrim(tLPContDB.getAccName()))) {
						print3[0] = print3[0] + "户名为："+CommonBL.notNull(tLCContDB.getAccName());
						print4[0] = print4[0] + "户名为："+CommonBL.notNull(tLPContDB.getAccName());
				}
				tlistTable.add(print3);
				tlistTable.add(print4);
			}

		}
        
        String strhead[] = new String[1];
        strhead[0] = "合同中止";
        xmlexport.addListTable(tlistTable, strhead);

        StringBuffer loansql = new StringBuffer();
        loansql.append("  select sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and feefinatype in ('RFBF','RFLX') ");
        String tLoanMoney=tExeSQL.getOneValue(loansql.toString());
        StringBuffer bonussql = new StringBuffer();
        bonussql.append("  select sum(-getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and feefinatype in ('HLBF','HLLX') ");
        String tbonusMoney=tExeSQL.getOneValue(bonussql.toString());
        String CT_Loan="";
        if(tLoanMoney!=null&&(!tLoanMoney.equals(""))&&(!tLoanMoney.equals("null"))){
        	CT_Loan="保单下存在未还贷款，贷款的本息合计为"+CommonBL.bigDoubleToCommonString(Double.parseDouble(tLoanMoney),"0.00")+"元。";
        }
        if(tbonusMoney!=null&&(!tbonusMoney.equals(""))&&(!tbonusMoney.equals("null"))){
        	CT_Loan+="保单退保分配红利合计为"+CommonBL.bigDoubleToCommonString(Double.parseDouble(tbonusMoney),"0.00")+"元。";
        }
        if(!CT_Loan.equals("")){
        	textTag.add("CT_Loan",CT_Loan);
        }
        //如果客户无有效保单显示余额信息
        createAppAccList(aLPEdorItemSchema);
        return true;
    }

    /**
     * qulq 061130 modify
     * 协议退保
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailXT(LPEdorItemSchema aLPEdorItemSchema) {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        //协议退费额度
        String msql =
                "select sum(getmoney)  from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "' ";
        ExeSQL aExeSQL = new ExeSQL();
        String tReturn = aExeSQL.getOneValue(msql);
        if (tReturn == null || StrTool.cTrim(tReturn).equals("")) {
            mErrors.addOneError(new CError("没有相关的协议退保记录！"));
            return false;
        }

        //判断是险种退保还是整单退
        String contPol = "select count(1) from lcpol"
                         + " where contno = '"
                         + aLPEdorItemSchema.getContNo() + "' ";
        String rs = new ExeSQL().getOneValue(contPol); //保单险种数
        if (rs == null || rs.equals("") || rs.equals("null")) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailXT";
            tError.errorMessage = "查询保单下险种失败";
            mErrors.addOneError(tError);
            return false;
        }
        int contRiskCount = Integer.parseInt(rs);

        //保全险种信息
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();

        StringBuffer xtInfor;
        StringBuffer syInfor;
        String str="select 1 from lcpol where contno='"+tLPPolSet.get(1).getContNo()+"' and riskcode in (select riskcode from lmriskapp where  taxoptimal='Y')";
        String sy=new ExeSQL().getOneValue(str);
        if(null != sy && !"".equals(sy)){
        	msql =
                " select nvl(sum(case when feefinatype <> 'BS' then getmoney end),0)," +
                " nvl(sum(case when feefinatype = 'BS' then getmoney end),0)"+
                "  from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "' ";
        	aExeSQL = new ExeSQL();
        	SSRS tSSRS2 = aExeSQL.execSQL(msql);
//        	String tSyReturn = aExeSQL.getOneValue(msql);
        	if (tSSRS2 == null ) {
        		mErrors.addOneError(new CError("没有相关的协议退保记录！"));
        		return false;
        	}
            xtInfor = new StringBuffer(100);
            xtInfor.append("保单号").append(aLPEdorItemSchema.getContNo())
                    .append("办理协议解约，生效日期")
                    .append(CommonBL.decodeDate(aLPEdorItemSchema.
                                                getEdorValiDate()))
                    .append("，保单效力自始无效，应退协议解约金额")
                    .append(CommonBL.bigDoubleToCommonString(Math.abs(Double.parseDouble(tSSRS2.GetText(1, 1))),"0.00")).append("元。");
            textTag.add("XT_infor", xtInfor.toString());
            syInfor = new StringBuffer(100);
            syInfor.append("补交税收优惠额度：").append(CommonBL.bigDoubleToCommonString(Math.abs(Double.parseDouble(tSSRS2.GetText(1, 2))),"0.00")).append("元");
            textTag.add("XT_syInfor", syInfor.toString());
        }
        else if (contRiskCount == tLPPolSet.size()) {
            xtInfor = new StringBuffer(100);
            xtInfor.append("保单号").append(aLPEdorItemSchema.getContNo())
                    .append("办理协议解约，生效日期")
                    .append(CommonBL.decodeDate(aLPEdorItemSchema.
                                                getEdorValiDate()))
                    .append("，保单效力自始无效，应退协议解约金额")
                    .append(CommonBL.bigDoubleToCommonString(Math.abs(Double.parseDouble(tReturn)),"0.00")).append("元。");
            textTag.add("XT_infor", xtInfor.toString());
        } else {
            xtInfor = new StringBuffer(100);
            xtInfor.append("保单号").append(aLPEdorItemSchema.getContNo())
                    .append("以下险种办理协议解约，生效日期")
                    .append(CommonBL.decodeDate(aLPEdorItemSchema.
                                                getEdorValiDate()))
                    .append("，险种效力自始无效，应退协议解约金额")
                    .append(CommonBL.bigDoubleToCommonString(Math.abs(Double.parseDouble(tReturn)),"0.00")).append("元。");
            textTag.add("XT_infor", xtInfor.toString());
        }

        if (!getXTPolInfo(aLPEdorItemSchema, tLPPolSet)) {
            return false;
        }
        createAppAccList(aLPEdorItemSchema);
        return true;
    }

    /**
     * 生成退保的险种信息
     * @return boolean
     */
    private boolean getXTPolInfo(LPEdorItemSchema aLPEdorItemSchema,
                                 LPPolSet tLPPolSet) {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("XT");

        ExeSQL tExeSQL = new ExeSQL();
        for (int i = 1; i <= tLPPolSet.size(); i++) {
            //取得试算金额
            String sql = "select abs(sum(GetMoney)) "
                         + "from LPBudgetResult "
                         + "where EdorNo = '"
                         + aLPEdorItemSchema.getEdorNo() + "' "
                         + "   and EdorType = '"
                         + aLPEdorItemSchema.getEdorType() + "' "
                         + "   and PolNo = '"
                         + tLPPolSet.get(i).getPolNo() + "' ";
            System.out.println(sql);
            String getMoney = tExeSQL.getOneValue(sql);
            if (getMoney.equals("") || getMoney.equals("null")) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getXTPolInfo";
                tError.errorMessage = "没有查询到正常退保金额";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            String[] printContent = new String[7];
            printContent[0] = String.valueOf(tLPPolSet.get(i).getRiskSeqNo());
            printContent[1] = String.valueOf(tLPPolSet.get(i).getRiskCode());
            printContent[2] = String.valueOf(CommonBL.getRiskName(tLPPolSet.get(
                    i).getRiskCode()));
            printContent[3] = CommonBL.bigDoubleToCommonString(
                    Double.parseDouble(getMoney), "0.00");

            String xtMoney = getSpecialData(aLPEdorItemSchema,
                                            tLPPolSet.get(i).getPolNo(),
                                            BQ.DETAILTYPE_XTFEE);
            xtMoney = xtMoney.substring(xtMoney.indexOf("-") + 1);
            printContent[4] = CommonBL.bigDoubleToCommonString(
                    Double.parseDouble(xtMoney), "0.00");

            String rate = getSpecialData(aLPEdorItemSchema,
                                         tLPPolSet.get(i).getPolNo(),
                                         BQ.XTFEERATEP);
            printContent[5] = rate;
            if (!printContent[5].equals("-")) {
                printContent[5] = CommonBL.bigDoubleToCommonString(
                        Double.parseDouble(rate), "0.0000");
            }
            printContent[6] = String.valueOf(tLPPolSet.get(i).getInsuredName());//hyx

            tlistTable.add(printContent);
        }
        xmlexport.addListTable(tlistTable, new String[tLPPolSet.size()]);
        return true;
    }

    /**
     * 查询特殊数据信息
     * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema：保全项目
     * @param polNo String：险种号
     * @param detailType String：类型
     * @return String
     */
    private String getSpecialData(LPEdorItemSchema tLPEdorItemSchema,
                                  String polNo, String detailType) {
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(tLPEdorItemSchema);
        tEdorItemSpecialData.setPolNo(polNo);
        if (!tEdorItemSpecialData.query()) {
            return null;
        }
        return tEdorItemSpecialData.getEdorValue(detailType);
    }

    /**
     * 交费频次变更
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPC(LPEdorItemSchema aLPEdorItemSchema) {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PC");

        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(edorNo);
        tLPPolDB.setEdorType(edorType);
        tLPPolDB.setContNo(contNo);
        LPPolSet tLPPolSet = tLPPolDB.query();

        for (int i = 1; i <= tLPPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
            if (!tLCPolDB.getInfo()) {
                mErrors.addOneError("未找到险种信息！");
                return false;
            }
            int oldPayIntv = tLCPolDB.getPayIntv();
            int newPayIntv = tLPPolSchema.getPayIntv();
            if (oldPayIntv != newPayIntv) {
                String content = "序号" + tLPPolSchema.getRiskSeqNo() +
                                 "险种" + tLPPolSchema.getRiskCode() +
                                 "交费频次从" +
                                 ChangeCodeBL.getCodeName("payintv",
                        String.valueOf(oldPayIntv)) +
                                 "变为" +
                                 ChangeCodeBL.getCodeName("payintv",
                        String.valueOf(newPayIntv)) +
                                 "，期交保费变更为" + tLPPolSchema.getPrem() +
                                 "元，从下期生效。";
//                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
//                tLJSGetEndorseDB.setEndorsementNo(edorNo);
//                tLJSGetEndorseDB.setFeeOperationType(edorType);
//                tLJSGetEndorseDB.setContNo(contNo);
//                tLJSGetEndorseDB.setPolNo(tLPPolSchema.getPolNo());
//                LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
//                if (tLJSGetEndorseSet.size() > 0)
//                {
//                    double getMoney = tLJSGetEndorseSet.get(1).getGetMoney();
//                    if (getMoney < 0)
//                    {
//                        content += "，应退费" + Math.abs(getMoney) + "元\n";
//                    }
//                    else if (getMoney > 0)
//                    {
//                        content += "，应补费" + getMoney + "元\n";
//                    }
//                }

                String[] column = new String[1];
                column[0] = content;
                tlistTable.add(column);
            }
        }
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    /**
     * 减少保额
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPT(LPEdorItemSchema aLPEdorItemSchema) {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        textTag.add("PT_ContNo", aLPEdorItemSchema.getContNo());
        tlistTable = new ListTable();
        tlistTable.setName("PT");
        //查找相应的保全项目
        String sql = "select * from LPEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPEdorItemSchema.getEdorType() + "' " +
                     "order by edorno,insuredno";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getInsuredPT";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i); ;
            //取险种名称
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPPolDB.setContNo(tLPEdorItemSchema.getContNo());
            tLPPolDB.setPolNo(tLPEdorItemSchema.getPolNo());

            LPPolSet tLPPolSet = tLPPolDB.query();
            LPPolSchema tLPPolSchema = tLPPolSet.get(1);

            sql = "select GetMoney from LJSGetEndorse " +
                  "where EndorsementNo = '" +
                  tLPEdorItemSchema.getEdorNo() + "' " +
                  "and FeeOperationType = '" +
                  tLPEdorItemSchema.getEdorType() + "' " +
                  "and InsuredNo = '" +
                  tLPPolSchema.getInsuredNo() + "' " +
                  "and RiskCode = '" +
                  tLPPolSchema.getRiskCode() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tExeSQL = new ExeSQL();
            String chgPrem = tExeSQL.getOneValue(sql);

            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(tLPPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LMRisk不存在相应的记录!";
                this.mErrors.addOneError(tError);
                return false;
            }

            String[] fee = new String[1];
            fee[0] = "保单" + tLPEdorItemSchema.getContNo() + "序号" +
                     tLPPolSchema.getRiskSeqNo() +
                     "险种（" + tLMRiskDB.getRiskName() + "）" +
                     "保额变更为" + CommonBL.carry(tLPPolSchema.getAmnt()) + "元。\n从" +
                     CommonBL.decodeDate(tLPEdorItemSchema.getEdorValiDate()) +
                     "起，期交保费变更为：" + tLPPolSchema.getPrem() +
                     "元，应退费" + CommonBL.carry(chgPrem) + "元。";
            tlistTable.add(fee);
        }

        String[] feeHead = new String[1];
        feeHead[0] = "费用";
        xmlexport.addListTable(tlistTable, feeHead);
        return true;
    }

    /**
     * 预收保费
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailYS(LPEdorItemSchema aLPEdorItemSchema) {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("未找到投保人信息！");
            return false;
        }
        AppAcc appAcc = new AppAcc();
        LCAppAccTraceSchema tLCAppAccTraceSchema =
                appAcc.getLCAppAccTrace(tLCAppntDB.getAppntNo(), edorNo,
                                        BQ.NOTICETYPE_P);
        textTag.add("YS_Money", String.valueOf(tLCAppAccTraceSchema.getMoney()));
        textTag.add("YS_AccBala",
                    String.valueOf(tLCAppAccTraceSchema.getAccBala()));
        textTag.add("YS_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        return true;
    }

    /**
     * 个单迁移
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPR(LPEdorItemSchema aLPEdorItemSchema) {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("未找到投保人信息！");
            return false;
        }
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
                aLPEdorItemSchema);
        tEdorItemSpecialData.query();
        String inCom = tEdorItemSpecialData.getEdorValue("incom");
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(inCom);
        if (!tLDComDB.getInfo()) {
            CError.buildErr(this, "查询转移机构失败");
            return false;
        }
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType(edorType);
        LPAddressSet tLPAddressSet = tLPAddressDB.query();

        textTag.add("PR_CONTNO", contNo);
        textTag.add("PR_INCOM", tLDComDB.getName());
        if (tLPAddressSet.size() > 0) {
            textTag.add("PR_ADDRESS", tLPAddressSet.get(1).getPostalAddress());
            textTag.add("PR_ZIPCODE", tLPAddressSet.get(1).getZipCode());
        }
        if (!inCom.substring(0,
            4).equals(tEdorItemSpecialData.getEdorValue("outcom").substring(0,
                4))) {
            textTag.add("PR_COM", "迁移后本保单的指定医院清单将发生变化，敬请留意");
        }

        String tsql = "select count(0) from lmriskapp where risktype ='M' "
                      +
                "and riskcode in(select riskcode from lcpol where contno ='" +
                      contNo + "')"
                      ;

        String re = new ExeSQL().getOneValue(tsql);
        if (!re.equals("0")) {
            textTag.add("PR_HEL",
                    "“您所享受的健康管理服务计划所包含的服务项目和服务标准，将根据保单迁移有关规则进行调整，具体调整情况可向您的业务员咨询。”");
        }

        return true;
    }
    
    //      万能续期补费 20110808 xp
    private boolean getDetailWX(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("WX_ContNo", contNo);
//        textTag.add("WX_EdorValiDate", CommonBL.decodeDate(edorValiDate));
       
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,edorType);
        if(!tSpecialData.query())
        {
        	mErrors.addOneError("未找到本次保全信息！");
            return false;
        }
        String [] polnos = tSpecialData.getGrpPolNos();
        String polno = polnos[0];
        tSpecialData.setPolNo(polno);

        textTag.add("WX_Fee", tSpecialData.getEdorValue("FEE"));
        textTag.add("WX_FeeLX", tSpecialData.getEdorValue("FEELX"));
        textTag.add("WX_GetNoticeNo", tSpecialData.getEdorValue("GETNOTICENO"));
//        textTag.add("WX_EdorValiDate", tSpecialData.getEdorValue("FEEPAYDATE"));
		//在此处添加利息的后续计算日期 20110914 by xp
        textTag.add("WX_EdorValiDate", tSpecialData.getEdorValue("FEELXDATE"));

        String FEEMODE = tSpecialData.getEdorValue("FEEMODE");
        if(FEEMODE.equals("1")){
            textTag.add("WX_Amnt", "续期核销时保额");
        }else if(FEEMODE.equals("0")){
            textTag.add("WX_Amnt", "投保时保额");
        }
        return true;
    }
    
    

    //  万能部分领取 080414 zhanggm
    private boolean getDetailLQ(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("LQ_ContNo", contNo);
        textTag.add("LQ_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String content = "";
        String appMoney = "";
        String manageMoney = "";
        String leftMoney = "";
        String getMoney = "";
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,edorType);
        if(!tSpecialData.query())
        {
        	mErrors.addOneError("未找到本次领取金额！");
            return false;
        }
        String [] polnos = tSpecialData.getGrpPolNos();
        String polno = polnos[0];
        tSpecialData.setPolNo(polno);

        appMoney = tSpecialData.getEdorValue("AppMoney");
    	manageMoney = tSpecialData.getEdorValue("ManageMoney");
    	leftMoney = tSpecialData.getEdorValue("LeftMoney");
    	getMoney = tSpecialData.getEdorValue("GetMoney");

        if (appMoney != null && !("0").equals(appMoney) && !("").equals(appMoney))
        {
            content += "万能险账户：本次领取金额" + appMoney + "元，"
                     + "领取后帐户余额：" + leftMoney + "元，\n"
                     + "扣除手续费金额：" + manageMoney + "元，"
                     + "本次实际领取金额：" + getMoney + "元。\n\n";
        }
        else
        {
        	mErrors.addOneError("领取信息出错，请重新理算！");
            return false;
        }

        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("LQ");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }
    //   个险万能保障调整 080514 pst
    private boolean getDetailBA(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("BA_ContNo", contNo);
        textTag.add("BA_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String content = "\n";
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(edorNo);
        tLPEdorEspecialDataDB.setEdorType(edorType);
        tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.query();
        
        for(int i=1; i<=tLPEdorEspecialDataSet.size(); i++)
        {
        	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        	tLPEdorEspecialDataSchema.setSchema(tLPEdorEspecialDataSet.get(i).getSchema());
        	
        	LCPolDB tLCPolDB = new LCPolDB();
        	tLCPolDB.setPolNo(tLPEdorEspecialDataSchema.getPolNo());
        	if(!tLCPolDB.getInfo())
        	{
        		CError tError = new CError();
                tError.moduleName = "PrtAppEndorsementBL";
                tError.functionName = "getDetailBA";
                tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() + "下无相应的保单信息!";
                this.mErrors.addOneError(tError);
        	}
            
        	double appMoney = 0.0;
        	try
            {
            	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());
            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
                appMoney = 0;
            }
            if (appMoney != 0 && appMoney != tLCPolDB.getAmnt())
            {
            	String riskcode = tLCPolDB.getRiskCode();
            	String riskname = "";
            	LMRiskDB tLMRiskDB = new LMRiskDB();
            	tLMRiskDB.setRiskCode(riskcode);
            	if(tLMRiskDB.getInfo())
            	{
            		riskname = tLMRiskDB.getRiskName();
            	}
                content += "险种代码："+riskcode+"，险种名称："+riskname
                	+"\n原基本保额:" + tLCPolDB.getAmnt() + "元，调整后基本保额：" + appMoney + "元；\n";
            }
        }

        content += "\n本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效。";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("BA");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }

    //   个险万能保障调整 080514 pst
    private boolean getDetailZB(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        String typeName="";
        textTag.add("ZB_ContNo", contNo);
        String polNo = null;
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        LCContSchema tLCContSchema= tLCContDB.query().get(1);
        if(tLCContSchema==null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtAppEndorsementBL";
            tError.functionName = "getDetailZB";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保单信息!";
            this.mErrors.addOneError(tError);
        }

        String content = "";
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(edorNo);
        tLPEdorEspecialDataDB.setEdorType(edorType);
        tLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
        double appMoney = 0.0;
        double tSumFee=0.0;
        //本次是补费
        if("BF".equals(tLPEdorEspecialDataSchema.getDetailType()))
        {
        	typeName="补交保费";
            try
            {
            	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());
            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
                appMoney = 0;
            }
        }
        //追加保费
        else if("ZF".equals(tLPEdorEspecialDataSchema.getDetailType()))
        {
        	typeName="追加保费";
            try
            {
            	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());

            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
                appMoney = 0;
            }
        }
        //扣费金额
        String tSql="select value(sum(money),0) from lpinsureacctrace  where edorno='"+edorNo+"' and edortype='ZB' and moneytype in('GL','KF') ";
        ExeSQL tExeSQL=new ExeSQL();
        tSumFee=Double.parseDouble(tExeSQL.getOneValue(tSql));
        tSql="select value(insuaccbala,0) from lcinsureacc where contno='"+contNo+"'";
        //当前帐户价值
        String accBala=tExeSQL.getOneValue(tSql);
        //追加后的帐户价值
        tSql="select value(insuaccbala,0) from lpinsureacc where  edorno='"+edorNo+"' and edortype='ZB'";
        String afterBala=tExeSQL.getOneValue(tSql);
        //持续奖励
        tSql="select value(sum(money),0) from lpinsureacctrace  where edorno='"+edorNo+"' and edortype='ZB' and moneytype in('B')";
        String JL=tExeSQL.getOneValue(tSql);

        if (appMoney != 0)
        {
            content += "个险万能追加保费/补交保费：交费金额" + CommonBL.bigDoubleToCommonString(appMoney,"0.00") + "元\n" +
            		"    扣除费用累计:" + CommonBL.bigDoubleToCommonString(tSumFee,"0.00")  +"";
        }
        textTag.add("TypeName", typeName);
        textTag.add("SumFee", CommonBL.bigDoubleToCommonString(tSumFee,"0.00"));
        textTag.add("tSumMoney", CommonBL.bigDoubleToCommonString(appMoney,"0.00"));
        textTag.add("AccBala", CommonBL.bigDoubleToCommonString(Double.parseDouble(accBala),"0.00"));
        textTag.add("AfterBala", CommonBL.bigDoubleToCommonString(Double.parseDouble(afterBala),"0.00"));
        textTag.add("JL", JL);
        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("ZB");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);

        return true;
    }
    
    
    private boolean getDetailAppAccShiftTo(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccShiftTo");
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        return true;
    }

    private boolean getDetailAppAccTakeOut(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccTakeOut");
        textTag.add("AppAcc_Money", pLCAppAccTraceSchema.getMoney());
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        textTag.add("AppAcc_RealPay",
                    mLPEdorAppSchema.getGetMoney() +
                    pLCAppAccTraceSchema.getMoney());

        return true;
    }

    //查询客户名称
    private String getCustomerName(String customerNo) {
        LCAddressSchema tLCAddressSchema = null;
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(customerNo);
        if (!tLDPersonDB.getInfo()) {
            mErrors.addOneError("没有查询到受理信息。");
            return "";
        }
        return tLDPersonDB.getName();
    }

    //对没有的保全项目类型生成空打迎数据
    private boolean getDetailForBlankType(LPEdorItemSchema
                                          aLPEdorItemSchema) {
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrtEndorsementBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 生成帐户余额清单
     */
    private boolean createAppAccList(LPEdorItemSchema aLPEdorItemSchema) {
        //判断客户是否有有效保单
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("查询投保人信息出错！");
            return false;
        }

        //若本次保全后投保人无有效保单，则需要提醒客户领取投保人账户余额
        //这种情况只可能出像在保全项目为CT,XT,WT且选择了全部险种的时候
        String sql = "select * from LCCont a " +
                     "where Appflag = '1' " +
                     "and ContType = '1' " +
                     "and AppntNo = '" + tLCAppntDB.getAppntNo() + "' " +
                     "and not exists "
                     + "  (select * from LPCont b " +
                     "    where b.EdorNo = '" + aLPEdorItemSchema.getEdorNo() +
                     "' " +
                     "      and b.ContNo = a.ContNo " +
                     "      and b.EdorType in ('CT', 'XT', 'WT') "
                     + "    and (select count(1) from LPPol " //选择全部险种
                     + "      where edorNo = b.edorNo "
                     + "         and edorType = b.edorType "
                     + "         and contNo = b.contNo)  "
                     + "      = "
                     +
                     "      (select count(1) from LCPol where contNo = b.contNo) "
                     + " ) ";
        System.out.println("acc sql:" + sql);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        if (tLCContSet.size() == 0) {
            String customerNo = tLCAppntDB.getAppntNo();
            String customerName = tLCAppntDB.getAppntName();
            textTag.add("CustomerNo", customerNo);
            textTag.add("CustomerName", customerName);
            tlistTable = new ListTable();
            LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
            tLCAppAccTraceDB.setCustomerNo(customerNo);
            LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
            if (tLCAppAccTraceSet.size() > 0) {
                xmlexport.addDisplayControl("displayAcc");
            }
            for (int i = 1; i <= tLCAppAccTraceSet.size(); i++) {
                LCAppAccTraceSchema tLCAppAccTraceSchema = tLCAppAccTraceSet.
                        get(i);
                String[] info = new String[6];
                info[0] = String.valueOf(i);
                info[1] = CommonBL.decodeDate(tLCAppAccTraceSchema.
                                              getConfirmDate());
                info[2] = CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getMoney(),"0.00");
                info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName(
                        "appaccdestsource",
                        tLCAppAccTraceSchema.getDestSource()));
                info[4] = StrTool.cTrim(tLCAppAccTraceSchema.getOtherNo());
                info[5] = CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getAccBala(),"0.00");
                tlistTable.add(info);
            }
            tlistTable.setName("APPACC");
            xmlexport.addListTable(tlistTable, new String[6]);
        }
        return true;
    }

//  20070717 zhanggm TB批单显示增加人工核保意见
    private String getSugUWIdea(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String idea = "";
    	LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
    	tLPUWMasterDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
    	tLPUWMasterDB.setEdorType(aLPEdorItemSchema.getEdorType());
    	LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
    	for(int i=1;i<=tLPUWMasterSet.size();i++)
    	{
    		LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(i).getSchema();
    		LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(tLPUWMasterSchema.getEdorNo());
//            tLPPolDB.setEdorType(tLPUWMasterSchema.getEdorType());
            tLPPolDB.setPolNo(tLPUWMasterSchema.getPolNo());
            LPPolSet tLPPolSet = new LPPolSet();
            tLPPolSet = tLPPolDB.query();
            if(tLPPolSet.size() >= 1)
            {
            	for(int j=1;j<=tLPPolSet.size();j++)
            	{
            		LPPolSchema tLPPolSchema = tLPPolSet.get(j);
        		    String tSugUWIdea = null;
        	    	tSugUWIdea = tLPUWMasterSchema.getSugUWIdea().trim();
        	    	if((null != tSugUWIdea) && (!"".equals(tSugUWIdea)) && (!"null".equals(tSugUWIdea)))
        	    	{
        		    	idea += "序号" + tLPPolSchema.getRiskSeqNo()
        		    	      + "，险种名称 "  + CommonBL.getRiskName(tLPPolSchema.getRiskCode())
                              + "，代码" + tLPPolSchema.getRiskCode()
                              + "（被保险人" + tLPPolSchema.getInsuredName() + "）的核保意见是：" + tSugUWIdea + "\n";
        		    }
            	}
            }
    	}
    	LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i).getSchema();
    		String sql = "select mult from LCPol where PolNo = '" + tLPPolSchema.getPolNo() + "' "
	                   + "union all "
	                   + "select mult from LBPol where PolNo = '" + tLPPolSchema.getPolNo() + "' ";
    		double oldMult = Double.parseDouble((new ExeSQL().getOneValue(sql).trim()));
    		double newMult = tLPPolSchema.getMult();
            if(oldMult!=newMult)
            {
            	idea += "序号" + tLPPolSchema.getRiskSeqNo()
			         + "，险种名称 "  + CommonBL.getRiskName(tLPPolSchema.getRiskCode())
                     + "，代码" + tLPPolSchema.getRiskCode()
                     + "（被保险人" + tLPPolSchema.getInsuredName()
                     + "）档次由" + String.valueOf(oldMult) + "档调整为" + String.valueOf(newMult) + "档，"
                     + "保费调整为" + tLPPolSchema.getPrem() + "元\n";
            }
        }
        /*
    	LPContDB tLPContDB = new LPContDB();
    	tLPContDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
    	tLPContDB.setEdorType(aLPEdorItemSchema.getEdorType());
    	tLPContDB.setContNo(aLPEdorItemSchema.getContNo());
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
    	if(tLPContDB.getInfo()&&tLCContDB.getInfo()&&(tLPContDB.getPrem()!=tLCContDB.getPrem()))
    	{
    		idea += "保单期交保费变更为" + tLPContDB.getPrem() + "元";
    	}
    	*/
    	return idea;
    }

    private boolean getDetailFC(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorAcceptNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String ContNo = aLPEdorItemSchema.getContNo();
        String edorAppDate = aLPEdorItemSchema.getEdorAppDate();
        String edorValidate = aLPEdorItemSchema.getEdorValiDate();
        tlistTable = new ListTable();
        tlistTable.setName("FC");
        String[] strArray = new String[4];
        String sql = " select AppntName,b.polno,(select riskname from lmrisk where b.riskcode = riskcode),edorvalue "
                   + " From lpedorespecialdata a,lppol b where b.edorno ='"+edorNo+"' "
                   + "  and a.polno = b.polno and a.edorno = b.edorno "
                   ;
        SSRS result = new ExeSQL().execSQL(sql);
        if (result == null || result.getMaxRow() == 0) {
            mErrors.addOneError("查询数据失败！");
            return false;
        }
        for (int i = 1; i <= result.getMaxRow(); i++) {
            strArray = new String[4];
            strArray[0] = result.GetText(i, 1);
            strArray[1] = result.GetText(i, 2);
            strArray[2] = result.GetText(i, 3);
            strArray[3] = result.GetText(i, 4);
//            strArray[4] = result.GetText(i, 5);
            tlistTable.add(strArray);
        }
        String odlsql = " select sum(cast(edorvalue as decimal (12,2))) "
                      + " from lpedorespecialdata where edorno ='"+edorNo+"' and polno !='000000' ";
//        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema
        String sumMoney = new ExeSQL().getOneValue(odlsql);
        textTag.add("ContNo", ContNo);
        textTag.add("EdorNo", edorNo);
        textTag.add("sumMoney",Math.abs(Double.parseDouble(sumMoney)));
        textTag.add("EdorValidate",CommonBL.decodeDate(PubFun.calDate(edorValidate, 0, "D", null))); //本次保全生效日期
        xmlexport.addListTable(tlistTable, strArray);
        return true;

    }
    
    /**
     * 红利领取方式变更
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailDD(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("DD_ContNo", aLPEdorItemSchema.getContNo());

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCPolDB.getInfo();

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLPPolDB.getInfo();

        System.out.println(tLCPolDB.getBonusGetMode());
        System.out.println( CommonBL.notNull(ChangeCodeBL.getCodeName("BonusGetMode",
                tLCPolDB.getBonusGetMode())));
               
        ListTable tlistTable = new ListTable();
        tlistTable.setName("DD");
        if (!StrTool.cTrim(tLCPolDB.getBonusGetMode()).equals(StrTool.
                cTrim(tLPPolDB.getBonusGetMode()))) {
            String[] strArr = new String[1];
            strArr[0] = "红利领取方式由:" +
                        CommonBL.notNull(ChangeCodeBL.getCodeName("BonusGetMode",
                        tLCPolDB.getBonusGetMode())) +
                        "，变更为：" +
                        CommonBL.notNull(ChangeCodeBL.getCodeName("BonusGetMode",
                        tLPPolDB.getBonusGetMode()));
//            判断本次是否会产生费用
            String sql = "select sum(-getmoney) from ljsgetendorse where endorsementno= '" + aLPEdorItemSchema.getEdorNo() + "' ";
            String tHLGet = new ExeSQL().getOneValue(sql);
            if(tHLGet!=null&&(!tHLGet.equals(""))&&(!tHLGet.equals("null"))){
                strArr[0]+="，累积生息帐户中余额的本息合计为"+tHLGet+"。";
            }
            tlistTable.add(strArr);
        }
        else {
        	String[] strArr = new String[1];
        	strArr[0] ="您没有变更红利领取方式的信息。";
        	tlistTable.add(strArr);
        }
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    private boolean getDetailBP(LPEdorItemSchema aLPEdorItemSchema)
    {
        String tEdorNo = aLPEdorItemSchema.getEdorNo();
        String tEdorType = aLPEdorItemSchema.getEdorType();
        String tContNo = aLPEdorItemSchema.getContNo();
        String tEdorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("BP_ContNo", tContNo);
        textTag.add("BP_EdorValiDate", CommonBL.decodeDate(tEdorValiDate));
        String tNewPrem = "";
        String content = "";
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(tEdorNo,tEdorType);
        if(tSpecialData.query())
        {
        	tNewPrem = tSpecialData.getEdorValue("NEWPREM");
        }
        String sql = "select prem from lccont where contno = '" + tContNo + "' ";
        String tOldPrem = new ExeSQL().getOneValue(sql);

        content += "万能险期交保费变更：由" + tOldPrem + "元，变更为：" + tNewPrem + "元。\n\n";
        content += "本变更自" + CommonBL.decodeDate(tEdorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("BP");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }
    
    /**
     * 续保核保
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    

    private boolean getDetailGF(LPEdorItemSchema aLPEdorItemSchema){

        String ContNo = aLPEdorItemSchema.getContNo();
        tlistTable = new ListTable();
        tlistTable.setName("GF");
        String[] strArray = new String[3];
        String sql = " select AppntName,polno,(select riskname from lmrisk where a.riskcode = riskcode)  From lcpol a where contno ='"+aLPEdorItemSchema.getContNo()+"'"
                   ;
        SSRS result = new ExeSQL().execSQL(sql);
        if (result == null || result.getMaxRow() == 0) {
            mErrors.addOneError("查询数据失败！");
            return false;
        }
        for (int i = 1; i <= result.getMaxRow(); i++) {
            strArray = new String[3];
            strArray[0] = result.GetText(i, 1);
            strArray[1] = result.GetText(i, 2);
            strArray[2] = result.GetText(i, 3);
            tlistTable.add(strArray);
        }
        textTag.add("ContNo", ContNo);
        xmlexport.addListTable(tlistTable, strArray);
        return true;
    }
    
    private boolean getDetailHA(LPEdorItemSchema aLPEdorItemSchema){
//      LCPolDB tLCPolDB = new LCPolDB();
//      tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());
//      tLCPolDB.getInfo();
  	
      String tRiskCodeSQL = " select a.bonusgetmode,b.riskname from lcpol a, lmriskapp b where a.riskcode=b.riskcode " 
      					 +" and a.contno='"+aLPEdorItemSchema.getContNo()+"' and b.risktype4='2' with ur ";
      SSRS result = new ExeSQL().execSQL(tRiskCodeSQL);
      if (result.GetText(1, 1).equals("1")) {
      	xmlexport.addDisplayControl("displayHA01");
      	if(!getDetailHA01(aLPEdorItemSchema)){
      		return false;
      	}
      }else{
      	xmlexport.addDisplayControl("displayHA02");
      	if(!getDetailHA02(aLPEdorItemSchema)){
      		return false;
      	}
      }
  	return true;
  }
  
  private boolean getDetailHA01(LPEdorItemSchema aLPEdorItemSchema){

      tlistTable = new ListTable();
      tlistTable.setName("HA01");
      String[] strArray = new String[3];
      String tEdorNo = aLPEdorItemSchema.getEdorNo();
      String tEdorType = aLPEdorItemSchema.getEdorType();
      String tContNo = aLPEdorItemSchema.getContNo();
      String tEdorValiDate = aLPEdorItemSchema.getEdorValiDate();
		String tSql = "select AppntName from LCAppnt where contNo = '" + tContNo + "'";
		String AppntName = new ExeSQL().getOneValue(tSql);
		if (AppntName == null) {
			mErrors.addOneError("为找到查询信息！");
			return false;
		}
		textTag.add("clientName", AppntName);
		
		String mSql = "select enddate from LCPol where contNo = '" + tContNo + "' and riskcode in (select riskcode from lmriskapp where risktype4='2')";
		String endDate = new ExeSQL().getOneValue(mSql);
		
      textTag.add("ContNo", tContNo);
      textTag.add("EndDate", endDate);
      

//    满期金金额
      String sql2 = "select sum(-getmoney) from ljsgetendorse where endorsementno= '" + aLPEdorItemSchema.getEdorNo() + "' and feefinatype='EB' ";
      String tMJGet = new ExeSQL().getOneValue(sql2);
      if(tMJGet==null || (tMJGet.equals("")) || (tMJGet.equals("null"))){
//      	return  false;
      }
          
      String sql3 = " select a.riskcode,b.riskname from lcpol a, lmriskapp b where a.riskcode=b.riskcode " 
			 +" and a.contno='" + tContNo + "' and b.risktype4='2' with ur  ";
      SSRS result = new ExeSQL().execSQL(sql3);
      if (result == null || result.getMaxRow() == 0) {
          mErrors.addOneError("查询数据失败！");
      return false;
      }
      for (int i = 1; i <= result.getMaxRow(); i++) {
          strArray = new String[3];
          strArray[0] = result.GetText(i, 1);
          strArray[1] = result.GetText(i, 2);
          strArray[2] = tMJGet;

          tlistTable.add(strArray);
      }
      xmlexport.addListTable(tlistTable, strArray);   
          
          
//        判断本次是否有还款         
			String loanSql = " select sum(getMoney) from LJSGetEndorse where endorsementNo = '"
					+ aLPEdorItemSchema.getEdorNo() + "'  "
					+ " and feeOperationType = '"
					+ aLPEdorItemSchema.getEdorType()
					+ "' and feefinatype in ('RFBF','RFLX') ";
			String tLoanMoney = new ExeSQL().getOneValue(loanSql);

			String mLoan = "";
			if (tLoanMoney != null && (!tLoanMoney.equals(""))
					&& (!tLoanMoney.equals("null"))) {
				mLoan = "保单下存在未还贷款，贷款的本息合计为" + tLoanMoney + "元。";
			}
			if (!mLoan.equals("")) {
				textTag.add("Loan", mLoan);
			}
			
  	return true;
  }
  private boolean getDetailHA02(LPEdorItemSchema aLPEdorItemSchema){

      tlistTable = new ListTable();
      tlistTable.setName("HA02");
      String[] strArray = new String[4];
      
      String tEdorNo = aLPEdorItemSchema.getEdorNo();
      String tEdorType = aLPEdorItemSchema.getEdorType();
      String tContNo = aLPEdorItemSchema.getContNo();
      String tEdorValiDate = aLPEdorItemSchema.getEdorValiDate();
		String tSql = "select AppntName from LCAppnt where contNo = '" + tContNo + "'";
		String AppntName = new ExeSQL().getOneValue(tSql);
		if (AppntName == null) {
			mErrors.addOneError("未找到查询信息！");
			return false;
		}
		textTag.add("clientName", AppntName);
      
//      LCPolDB tLCPolDB = new LCPolDB();
//      tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());
//      tLCPolDB.getInfo();
		String mSql = "select enddate from LCPol where contNo = '" + tContNo + "' and riskcode in (select riskcode from lmriskapp where risktype4='2')";
		String endDate = new ExeSQL().getOneValue(mSql);
		
      textTag.add("ContNo", tContNo);
      textTag.add("EndDate", endDate);
      
      
//    判断本次红利及利息总和
      String sql = "select sum(-getmoney) from ljsgetendorse where endorsementno= '" + aLPEdorItemSchema.getEdorNo() + "' and feefinatype='HL' ";
      String tHLGet = new ExeSQL().getOneValue(sql);
      if(tHLGet==null || (tHLGet.equals(""))||(tHLGet.equals("null"))){
//      		textTag.add("HLGet", tHLGet);
      }

//    满期金金额
      String sql2 = "select sum(-getmoney) from ljsgetendorse where endorsementno= '" + aLPEdorItemSchema.getEdorNo() + "' and feefinatype='EB' ";
      String tMJGet = new ExeSQL().getOneValue(sql2);
      if(tMJGet==null || (tMJGet.equals("")) || (tMJGet.equals("null"))){
//      	return  false;
      }
      
      String sql3 = " select a.riskcode,b.riskname from lcpol a, lmriskapp b where a.riskcode=b.riskcode " 
      			 +" and a.contno='" + tContNo + "' and b.risktype4='2' with ur  ";
      
      SSRS result = new ExeSQL().execSQL(sql3);
      if (result == null || result.getMaxRow() == 0) {
      	mErrors.addOneError("查询数据失败！");
      	return false;
      }
      for (int i = 1; i <= result.getMaxRow(); i++) {
      	strArray = new String[4];
      	strArray[0] = result.GetText(i, 1);
      	strArray[1] = result.GetText(i, 2);
      	strArray[2] = tHLGet;
      	strArray[3] = tMJGet;

      	tlistTable.add(strArray);
      }
      xmlexport.addListTable(tlistTable, strArray);
          
//    判断本次是否有还款         
      String loanSql = " select sum(getMoney) from LJSGetEndorse where endorsementNo = '"
					+ aLPEdorItemSchema.getEdorNo()+ "' and feeOperationType = '"+ aLPEdorItemSchema.getEdorType()+"'"
					+ " and feefinatype in ('RFBF','RFLX') ";
		String tLoanMoney = new ExeSQL().getOneValue(loanSql);

		String mLoan = "";
		if (tLoanMoney != null && (!tLoanMoney.equals(""))
				&& (!tLoanMoney.equals("null"))) {
			mLoan = "保单下存在未还贷款，贷款的本息合计为" + tLoanMoney + "元。";
		}
		if (!mLoan.equals("")) {
				textTag.add("Loan", mLoan);
		}

  	return true;
  }

    public static void main(String[] args) {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ManageCom = "86";

        VData tVData = new VData();
        tVData.add(gi);
        PrtAppEndorsementBL tPrtAppEndorsementBL =
                new PrtAppEndorsementBL("20061226000008");
        if (!tPrtAppEndorsementBL.submitData(tVData, "")) {
            System.out.println(tPrtAppEndorsementBL.mErrors.getErrContent());
        }

    }
}
