package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保全免责</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class AddSpec
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private VData mInputData = new VData();

    private MMap mMap = new MMap();

    private LPSpecSet mLPSpecSet = null;



    /**
     * 处理数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitData(VData cInputData)
    {
        this.mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (getSubmitData(cInputData) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到外部传入的数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPSpecSet = (LPSpecSet) mInputData.getObjectByObjectName(
                "LPSpecSet", 0);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        deleteSpec();
        setLPSpec();
        return true;
    }

    private void deleteSpec()
    {
        LPSpecSchema tLPSpecSchema = mLPSpecSet.get(1);
        String sql = "delete from LPSpec " +
                     "where EdorNo = '" + tLPSpecSchema.getEdorNo() + "' " +
                     "and EdorType = '" + tLPSpecSchema.getEdorType() + "' " +
                     "and PolNo = '" + tLPSpecSchema.getPolNo() + "' ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 为添加免责信息准备数据
     */
    private void setLPSpec()
    {
        for (int i = 1; i <= mLPSpecSet.size(); i++)
        {
            LPSpecSchema tLPSpecSchema = mLPSpecSet.get(i);
            tLPSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPSpecSchema.setProposalNo(tLPSpecSchema.getPolNo());
            tLPSpecSchema.setSerialNo(PubFun1.CreateMaxNo("SpecCode",
                    PubFun.getNoLimit(mGlobalInput.ComCode)));
            tLPSpecSchema.setOperator(mGlobalInput.Operator);
            tLPSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLPSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLPSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLPSpecSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLPSpecSchema, "INSERT");
        }
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
