package com.sinosoft.lis.bq;

//程序名称：PEdorBPDetailBL.java
//程序功能：万能保费变更明细录入
//创建日期：2009-07-03
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorBPDetailBL 
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局基础数据 */
	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private MMap map = new MMap();

	private String mPolNo = null;
	
	private String mContNo = null;
	
	private String mNewPrem = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String mEdorNo = null;

	private static String mEdorType = "BP";
	
	public PEdorBPDetailBL() 
	{
	}

	public boolean submitData(VData cInputData, String cOperate) 
	{
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) 
		{
			return false;
		}

		if (!checkData()) 
		{
			return false;
		}

		// 进行业务处理
		if (!dealData()) 
		{
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) 
		{
			return false;
		}
		
		//提交
		if (!submitData()) 
		{
			return false;
		}
		
		return true;
	}

	private boolean getInputData() 
	{
		try 
		{
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
			TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
			mEdorNo = mLPEdorItemSchema.getEdorNo();
			mEdorType = mLPEdorItemSchema.getEdorType();
			mContNo = mLPEdorItemSchema.getContNo();
			mPolNo = (String) tTransferData.getValueByName("PolNo");
			mNewPrem = (String) tTransferData.getValueByName("NewPrem");
		} 
		catch (Exception e) 
		{
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "PEdorBPDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mGlobalInput == null || mLPEdorItemSchema == null) 
		{
			CError tError = new CError();
			tError.moduleName = "PEdorBPDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "输入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private boolean dealData() 
	{
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,mEdorType);
		tSpecialData.add("NEWPREM", mNewPrem);
		tSpecialData.add("POLNO", mPolNo);
		map.put(tSpecialData.getSpecialDataSet(), "DELETE&INSERT");
		setEdorState(BQ.EDORSTATE_INPUT);
		return true;
	}

	private boolean checkData() 
	{
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
		LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
		if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) 
		{
			CError tError = new CError();
			tError.moduleName = "PEdorBPDetailBL";
			tError.functionName = "checkData";
			tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
			this.mErrors.addOneError(tError);
			return false;
		}

		mLPEdorItemSchema = tLPEdorItemSet.get(1);
		if (mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL)) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorBPDetailBL";
			tError.functionName = "checkData";
			tError.errorMessage = "该保全已经保全理算，不能修改!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
    	String isTPHISQL = " SELECT 1 FROM LCPOL a WHERE a.POLNO='"+mPolNo+"' " +
    			" and riskcode in (select riskcode from lmriskapp where taxoptimal ='Y') " +
    			" with ur  ";
       ExeSQL tExeSQL = new ExeSQL();
       String isTPHIRisk = tExeSQL.getOneValue(isTPHISQL);
       if(null!=isTPHIRisk && "1".equals(isTPHIRisk)){
    	   
    	   //判断税优保单变更后的保费是否超过10000+风险保费（下个保单周年日的风险保费）

           String  tSql ="select distinct insuredsex,insuredbirthday,payenddate,payintv from lcpol where  polno='"+mPolNo+"'";
           SSRS tSSRS = new ExeSQL().execSQL(tSql);
           String tInsuSex = tSSRS.GetText(1, 1);
           String tInsuBirthDay = tSSRS.GetText(1, 2);
           String tPayEndDate = tSSRS.GetText(1, 3);
           String tPayIntv = tSSRS.GetText(1, 4);
           int mPayIntv=Integer.parseInt(tPayIntv); 
           int tInsuredAge = PubFun.calInterval(tInsuBirthDay,tPayEndDate, "Y");
           double riskPrem =getRiskPremSY(mPolNo,mPayIntv,tInsuredAge,tInsuSex);//下个年度的风险保费
           double tNewPrem=Double.parseDouble(mNewPrem); //变更的期缴保费
           double mNewPrem=0;//年度保费（期缴保费*缴费频次）
           double m=1;
           if(mPayIntv==12){
        	   m=1;
        	   mNewPrem=tNewPrem;
           }else{
        	   m=12;
        	   mNewPrem=tNewPrem*12;
           }
           double maxPrem=10000+riskPrem*m;   //最大可变更的保费
           System.out.println("maxPrem"+maxPrem);
           System.out.println("riskPrem"+riskPrem);
           if(mNewPrem>maxPrem){
				mErrors.addOneError("变更后的年度保费不能超过10000元加年度风险保费"+riskPrem*m+"元！");
                return false;
           }else if(tNewPrem<riskPrem){
				mErrors.addOneError("变更后的保费不能小于风险保费："+riskPrem+"！");
                return false;
           }
   		String edorSql = "update  LPEdorItem " + "set EdorValidate = '"+ tPayEndDate
				+ "' where  EdorNo = '" + mEdorNo + "' " + "and EdorType = '"
				+ mEdorType + "' " + "and ContNo = '" + mContNo + "'";
		map.put(edorSql, "UPDATE");
       }
		return true;

	}
    /**
     * 税优计算风险保费
     * */
    private double getRiskPremSY(String aPolno,int payIntv,int aInsuredAge,String aInsuSex)
    {  
    	double rpPrem=0.0;
        String tInsuSex=aInsuSex;
        int tInsuredAge=aInsuredAge;
        int tpayIntv=payIntv;
        SSRS tSSRS = new SSRS();
        try
        {   

            
			String strSQL="select insuaccno from lcinsureacc  where  polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取账户信息失败！");
                return 1;
			}
			
            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
            tLCInsureAccClassDB.setPolNo(aPolno);
            tLCInsureAccClassDB.setInsuAccNo(tSSRS.GetText(1, 1));
            LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.
                    query();
            if(tLCInsureAccClassSet.size()!=1){
            	// @@错误处理
				System.out.println("PEdorICAppConfirmBL+getRiskPrem2++--");
				CError tError = new CError();
				tError.moduleName = "PEdorICAppConfirmBL";
				tError.functionName = "getRiskPrem2";
				tError.errorMessage = "获取万能账户信息失败!";
				this.mErrors.addOneError(tError);
				return 1;
            }
//            mLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);
 
        	//税优风险保费计算
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(tSSRS.GetText(1, 1));
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("02");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			if (tLMRiskFeeDB.mErrors.needDealError()) 
			{
				CError.buildErr(this, "账户管理费查询失败!");
				return 1;
			}
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(1).getFeeCalCode());
            tCalculator.addBasicFactor("Sex", tInsuSex);
            tCalculator.addBasicFactor("PolNo",aPolno);
			tCalculator.addBasicFactor("PayIntv",""+ tpayIntv);
			tCalculator.addBasicFactor("AppAge",""+ tInsuredAge);
            
			strSQL="select a.payintv,a.insuredappage,b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取保单详细信息失败！");
                return 1;
			}
			//增加折扣因子
			if(null != tSSRS.GetText(1, 3) && !"".equals(tSSRS.GetText(1, 3))){
				tCalculator.addBasicFactor("Totaldiscountfactor", tSSRS.GetText(1, 3));
			}else
			{
				tCalculator.addBasicFactor("Totaldiscountfactor", "1");
			}
			
            double fee = Math.abs(PubFun.setPrecision(
                        Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算风险保费出错");
                return 1;
            }
            rpPrem += fee;

        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
            return 1;
        }
        return rpPrem;
    }

	
	private void setEdorState(String edorState) 
	{
		String sql = "update  LPEdorItem " + "set EdorState = '" + edorState
				+ "', " + "    Operator = '" + mGlobalInput.Operator + "', "
				+ "    ModifyDate = '" + mCurrentDate + "', "
				+ "    ModifyTime = '" + mCurrentTime + "' "
				+ "where  EdorNo = '" + mEdorNo + "' " + "and EdorType = '"
				+ mEdorType + "' " + "and ContNo = '" + mContNo + "'";
		map.put(sql, "UPDATE");
	}

	private boolean prepareData() 
	{
		mResult.clear();
		mResult.add(map);
		return true;
	}

	public VData getResult() 
	{
		return mResult;
	}
	
	private boolean submitData() 
	{
		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(mResult, "")) 
		{
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorBPDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("PEdorBPDetailBL End PubSubmit");
		return true;
	}
	
}
