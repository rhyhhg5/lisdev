package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 编码转换类</p>
 * <p>Description: 把代码转换为名称</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class ChangeCodeBL
{
    private ChangeCodeBL()
    {
    }

    /**
     * 得到Code对应的CodeName，用于编码在自定义表的情况
     * @param  String codeType  编码类型
     * @param  String codeId    编码
     * @param  String conditionField  条件查询字段
     * @return String    编码名称
     */
    public static String getCodeName(String codeType, String code, String conditionField)
    {
        String codeName = "";
        try
        {
            if (code == null)
            {
                return "";
            }
            LDCodeSchema tLDCodeSchema = new LDCodeSchema();
            tLDCodeSchema.setCodeType(codeType);
            tLDCodeSchema.setCode(code);

            GlobalInput tGlobalInput = new GlobalInput();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("conditionField", conditionField);
            tTransferData.setNameAndValue("codeCondition", code);

            VData tData = new VData();
            tData.add(tLDCodeSchema);
            tData.add(tGlobalInput);
            tData.add(tTransferData);

            CodeQueryBL tCodeQueryBL = new CodeQueryBL();
            tCodeQueryBL.submitData(tData, "");
            String result = (String) tCodeQueryBL.getResult().getObject(0);
            String[] record = PubFun.split(result, "^");
            String[] feild = PubFun.split(record[1], "|");
            codeName = feild[1];
        }
        catch (Exception ex)
        {
        }
        return codeName;
    }

    /**
     * 得到Code对应的CodeName，用于编码在LDCode的情况
     * @param  String codeType  编码类型
     * @param  String codeId    编码
     * @return String          编码名称
     */
    public static String getCodeName(String codeType, String code)
    {
        String codeName = ChangeCodeBL.getCodeName(codeType, code, "code");
        return codeName;
    }

    public static void main(String[] args)
    {

        System.out.println(ChangeCodeBL.getCodeName("Sex", "1"));
        System.out.println(ChangeCodeBL.getCodeName("OccupationCode", "00702", "OccupationCode"));
        System.out.println(ChangeCodeBL.getCodeName("BnfType", "0"));
        System.out.println(ChangeCodeBL.getCodeName("IDType", "0"));
        System.out.println(ChangeCodeBL.getCodeName("Relation", "00"));
        System.out.println(ChangeCodeBL.getCodeName("BnfGrade", "1"));
        System.out.println(ChangeCodeBL.getCodeName("Bank", "0701"));
    }
}
