package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 人工核保保费重算类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorUWAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mInsuredNo = null;

    private double mGetMoney = 0.00;

    private double mChgPrem = 0.00;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private ReCalBL mReCalBL = null;

    private DetailDataQuery mQuery = null;


    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                    getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                    getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
            mInsuredNo = mLPEdorItemSchema.getInsuredNo();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!dealPols())
        {
            return false;
        }
        setEdorItem();
        setLPCont();
        return true;
    }

    /**
     * 按险种处理保全数据
     * @return boolean
     */
    private boolean dealPols()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setContNo(mContNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            if (!dealOnePol(tLPPolSet.get(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema ==  null)
            {
                mErrors.addOneError("未找到序号" + tLCPolSchema.getRiskSeqNo()
                        + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }
            double oldPrem = tLCPolSchema.getPrem();
            double newPrem = calNewPrem(aLPPolSchema);
            double getMoney = calGetMoney(oldPrem, newPrem, aLPPolSchema);
            if (getMoney != 0)
            {
                if (!setLJSGetEndorse(aLPPolSchema, getMoney))
                {
                    return false;
                }
            }
            setLPTables(newPrem - oldPrem);
            mGetMoney += getMoney;
            mChgPrem += newPrem - oldPrem;
        }
        catch (Exception ex)
        {
            mErrors.addOneError("保费计算出错！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 得到保全算费的计算代码
     * @param riskCode String
     * @return String
     */
    private LMPolDutyEdorCalSchema getEdorCal(String riskCode)
    {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(riskCode);
        tLMPolDutyEdorCalDB.setEdorType(mEdorType);
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if (tLMPolDutyEdorCalSet.size() == 0)
        {
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1);
    }

    /**
     * 重算保费
     * @param aLPPolSchema LPPolSchema
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private double calNewPrem(LPPolSchema aLPPolSchema) throws Exception
    {
        //重算保费
        mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
        if (!mReCalBL.recal())
        {
            mErrors.copyAllErrors(mReCalBL.mErrors);
            throw new Exception();
        }
        return mReCalBL.aftLPPolSet.get(1).getPrem();
    }

    /**
     * 得到交费起始日期
     * @param aLPPolSchema LPPolSchema
     * @return String
     */
    private String getLastPayToDate(LPPolSchema aLPPolSchema)
    {
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPolNo(aLPPolSchema.getPolNo());
        tLJAPayPersonDB.setCurPayToDate(aLPPolSchema.getPaytoDate());
        tLJAPayPersonDB.setPayType("ZC");
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if (tLJAPayPersonSet.size() == 0)
        {
            return null;
        }
        return tLJAPayPersonSet.get(1).getLastPayToDate();
    }

    /**
     * 计算做过保全之后的保费差额
     * @param oldPrem double
     * @param newPrem double
     * @param aLPPolSchema LPPolSchema
     * @return double
     */
    private double calGetMoney(double oldPrem, double newPrem,
            LPPolSchema aLPPolSchema) throws Exception
    {
        double getMoney = 0.0;
        if (oldPrem != newPrem)
        {
            //得到交费起始日期
            String lastPayToDate = getLastPayToDate(aLPPolSchema);
            if (lastPayToDate == null)
            {
                mErrors.addOneError("未找到" + aLPPolSchema.getRiskSeqNo() + "险种本期交费起始日期！");
                throw new Exception();
            }
            String payToDate = aLPPolSchema.getPaytoDate();
            if (payToDate == null)
            {
                mErrors.addOneError("未找到" + aLPPolSchema.getRiskSeqNo() + "险种本期交至日期！");
                throw new Exception();
            }
            String edorValiDate = mLPEdorItemSchema.getEdorValiDate();
            getMoney = (newPrem - oldPrem) * PubFun.calInterval(edorValiDate, payToDate, "D")
                    / PubFun.calInterval(lastPayToDate, payToDate, "D");
        }
        return CommonBL.carry(getMoney);
    }

    /**
     * 得到财务类型
     * @param edorType String
     * @param code String
     * @return String
     */
    private String getFeeType(String edorType, String code)
    {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType(edorType);
        tLDCode1DB.setCode(code);
        tLDCode1DB.setCode1("000000");
        if (!tLDCode1DB.getInfo())
        {
            mErrors.addOneError("财务类型表LDCode1中没有该项目的配置信息！");
            return null;
        }
        return tLDCode1DB.getCodeName();
    }

    /**
     *  往批改补退费表（应收/应付）新增数据
     * @param aLPPolSchema LPPolSchema
     * @param changePrem double
     * @return boolean
     */
    private boolean setLJSGetEndorse(LPPolSchema aLPPolSchema, double changePrem)
    {
        String feeType = null;
        ExeSQL tExeSql= new ExeSQL();
    	String ISLongRisk=tExeSql.getOneValue("select riskperiod from lmriskapp where riskcode='"+aLPPolSchema.getRiskCode()+"'");
        if (changePrem > 0) //补费
        {
            feeType = "BF";
        }
        else if (changePrem <= 0) //退费
        {
        	if(ISLongRisk!=null&&ISLongRisk=="L")
            {
        		feeType = "TB";
            }
        	else
        	{
        		feeType = "TF";
        	}
        }
        if (feeType == null)
        {
            mErrors.addOneError("未找到财务代码类型！");
            return false;
        }
        BqCalBL bqCalBL = new BqCalBL();
        LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType, changePrem,
                mGlobalInput);
        tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置P表的保费
     */
    private void setLPTables(double chgPrem)
    {
        for (int i = 1; i <= mReCalBL.aftLPPolSet.size(); i++)
        {
            double sumPrem = mReCalBL.aftLPPolSet.get(i).getSumPrem() + chgPrem;
            mReCalBL.aftLPPolSet.get(i).setSumPrem(sumPrem);
        }
        mMap.put(mReCalBL.aftLPPolSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPDutySet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPPremSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPGetSet, "DELETE&INSERT");
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem()
    {
        mLPEdorItemSchema.setGetMoney(mGetMoney);
        //mLPEdorItemSchema.setChgPrem(mChgPrem);
        mLPEdorItemSchema.setChgAmnt(0); //只变保费，保额不变
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPEdorItemSchema, "DELETE&INSERT");
    }

    /**
     * 更新LPCont表的保费
     */
    private void setLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        tLPContSchema.setPrem(String.valueOf(tLPContSchema.getPrem() + mChgPrem));
        tLPContSchema.setSumPrem(String.valueOf(tLPContSchema.getSumPrem() + mChgPrem));
        tLPContSchema.setOperator(mGlobalInput.Operator);
        tLPContSchema.setMakeDate(mCurrentDate);
        tLPContSchema.setMakeTime(mCurrentTime);
        tLPContSchema.setModifyDate(mCurrentDate);
        tLPContSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPContSchema, "DELETE&INSERT");
    }
}
