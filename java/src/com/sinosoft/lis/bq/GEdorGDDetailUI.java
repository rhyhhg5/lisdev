package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全处理财务数据</p>
 * <p>Description:把保全补退费数据放入财务接口</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class GEdorGDDetailUI
{
    private GEdorGDDetailBL mGEdorGDDetailBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public GEdorGDDetailUI(GlobalInput gi, String edorNo, String grpContNo)
    {
        mGEdorGDDetailBL = new GEdorGDDetailBL(gi, edorNo, grpContNo);
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mGEdorGDDetailBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public  String getError()
    {
        return mGEdorGDDetailBL.mErrors.getFirstError();
    }

    /**
     * 得到需显示的提示信息
     * @return String
     */
    public String getMessage()
    {
        return mGEdorGDDetailBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ManageCom = "86";
        GEdorGDDetailUI tGEdorGADetailUI = new GEdorGDDetailUI(gi,
                "20051227000005", "0000031001");
        if (!tGEdorGADetailUI.submitData())
        {
            System.out.println(tGEdorGADetailUI.getError());
        }
    }
}
