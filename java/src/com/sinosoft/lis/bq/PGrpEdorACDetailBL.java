package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class PGrpEdorACDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();

    private LPGrpContSchema mLPGrpContSchema = new LPGrpContSchema();
    private LPGrpAppntSchema mLPGrpAppntSchema = new LPGrpAppntSchema();
    private LPGrpSchema mLPGrpSchema = new LPGrpSchema();
    private LPGrpAddressSchema mLPGrpAddressSchema = new LPGrpAddressSchema();
    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    //qulq	修改为一次更新所有保单的投保单位资料。
    private LPGrpContSet mLPGrpContSet = new LPGrpContSet();
    private LPGrpAppntSet mLPGrpAppntSet = new LPGrpAppntSet();

    String addrFlag = "";
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    MMap map = new MMap();

    boolean newaddrFlag = false;
    public PGrpEdorACDetailBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        getInputData(cInputData);

        //数据查询业务处理(queryData())
        if (mOperate.equals("QUERY||MAIN") || mOperate.equals("QUERY||DETAIL"))
        {
            if (!queryData())
                return false;
            else
                return true;
        }

        //数据校验操作（checkdata)
        if (!checkData())
            return false;
        //数据准备操作（preparedata())
        if (!prepareData())
            return false;

        System.out.println("---oper" + cOperate);

        // 数据操作业务处理(新增insertData();修改updateData();删除deletedata())
        if (mOperate.equals("INSERT||EDORAC"))
        {
            if (!insertData())
                return false;
            System.out.println("---insertData---");
        }
        if (mOperate.equals("UPDATE||EDORAC"))
        {
            if (!updateData())
                return false;
            System.out.println("---updateData---");
        }
        PubSubmit tPubSubmit = new PubSubmit();
        //tPubSubmit.submitData(mInputData, cOperate);
        if (tPubSubmit.submitData(mInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**数据查询业务处理(queryData())
     *
     */
    private boolean queryData()
    {
        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean insertData()
    {
        map.put(mLPGrpContSet, "DELETE&INSERT");
        map.put(mLPGrpAppntSet, "DELETE&INSERT");
        map.put(mLPGrpSchema, "DELETE&INSERT");
        map.put(mLPGrpEdorItemSchema, "UPDATE");
        mLPGrpAddressSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        map.put(mLPGrpAddressSchema, "DELETE&INSERT");
        mInputData.clear();
        mInputData.add(map);

        mResult.clear();
        mResult.add(mLPGrpAddressSchema);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData)
    {
        addrFlag = (String) cInputData.get(0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mLPGrpAppntSchema = (LPGrpAppntSchema) cInputData.getObjectByObjectName(
                "LPGrpAppntSchema", 0);
        mLPGrpSchema = (LPGrpSchema) cInputData.getObjectByObjectName(
                "LPGrpSchema", 0);
        mLPGrpAddressSchema = (LPGrpAddressSchema) cInputData.
                getObjectByObjectName("LPGrpAddressSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
    }

    /**
     * 更新集体保单保全信息
     * @return
     */
    private boolean updateData()
    {

        return true;
    }

    /**
     * 校验传入的数据的合法性
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);
        if (!tLPGrpEdorItemDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PAppntGrpBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "无保全项目数据!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        mLPGrpEdorItemSchema = tLPGrpEdorItemDB.getSchema();
        if (tLPGrpEdorItemDB.getEdorState().trim().equals("2"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PAppntGrpBL";
            tError.functionName = "Preparedata";
            tError.errorMessage = "该保全项目已经申请确认不能修改!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果地址号不为空，需要校验该地址号是否为本次保全申请产生，如果不是本次保全申请产生，则不让修改
        return flag;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
        Reflections tReflections = new Reflections();
        //准备团体投保人信息
        LPGrpAppntBL tLPGrpAppntBL = new LPGrpAppntBL();
        if (!tLPGrpAppntBL.queryLPGrpAppnt(mLPGrpEdorItemSchema))
        {
            CError.buildErr(this, "查询投保人信息失败！");
            return false;
        }
/*
        tLPGrpAppntBL.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        tLPGrpAppntBL.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpAppntBL.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpAppntBL.setEdorNo(mLPGrpAppntSchema.getEdorNo());
        tLPGrpAppntBL.setEdorType(mLPGrpAppntSchema.getEdorType());
        tLPGrpAppntBL.setCustomerNo(mLPGrpAppntSchema.getCustomerNo()); //客户号码
        tLPGrpAppntBL.setName(mLPGrpAppntSchema.getName());
        tLPGrpAppntBL.setPostalAddress(mLPGrpAppntSchema.getPostalAddress());
        tLPGrpAppntBL.setZipCode(mLPGrpAppntSchema.getZipCode());
        tLPGrpAppntBL.setAddressNo(mLPGrpAppntSchema.getAddressNo());
        tLPGrpAppntBL.setPhone(mLPGrpAppntSchema.getPhone());
*/
        //qulq
        LPGrpAppntSet tLPGrpAppntSet = new LPGrpAppntSet();
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        LCGrpAppntSet tLCGrpAppntSet = tLCGrpAppntDB.executeQuery("select a.* from LCGrpAppnt a where CustomerNo ='"+mLPGrpAppntSchema.getCustomerNo()+"'");
        if(tLCGrpAppntSet==null||tLCGrpAppntSet.size()==0)
        {
            CError.buildErr(this, "查询投保人信息失败！");
            System.out.println("123");
            return false;
        }
        for(int i=1 ; i<=tLCGrpAppntSet.size();i++)
        {
            LPGrpAppntSchema temp = new LPGrpAppntSchema();
            tReflections.transFields(temp,tLCGrpAppntSet.get(i));
            temp.setEdorNo(mLPGrpAppntSchema.getEdorNo());
            temp.setEdorType(mLPGrpAppntSchema.getEdorType());
            temp.setCustomerNo(mLPGrpAppntSchema.getCustomerNo()); //客户号码
            temp.setName(mLPGrpAppntSchema.getName());
            temp.setPostalAddress(mLPGrpAppntSchema.getPostalAddress());
            temp.setZipCode(mLPGrpAppntSchema.getZipCode());
            temp.setAddressNo(mLPGrpAppntSchema.getAddressNo());
            temp.setPhone(mLPGrpAppntSchema.getPhone());
            temp.setTaxNo(mLPGrpAppntSchema.getTaxNo());
            temp.setLegalPersonName(mLPGrpAppntSchema.getLegalPersonName());
            temp.setLegalPersonIDNo(mLPGrpAppntSchema.getLegalPersonIDNo());
            //添加联系人相关信息
            temp.setIDType(mLPGrpAppntSchema.getIDType());
            temp.setIDNo(mLPGrpAppntSchema.getIDNo());
            temp.setIDStartDate(mLPGrpAppntSchema.getIDStartDate());
            temp.setIDEndDate(mLPGrpAppntSchema.getIDEndDate());
            temp.setIDLongEffFlag(mLPGrpAppntSchema.getIDLongEffFlag());
            tLPGrpAppntSet.add(temp);
            
//            System.out.println("税务登记证号码:"+mLPGrpAppntSchema.getTaxNo());
//            System.out.println("法人姓名:"+mLPGrpAppntSchema.getLegalPersonName());
//            System.out.println("法人身份证号:"+mLPGrpAppntSchema.getLegalPersonIDNo());
        }

        LPGrpContBL tLPGrpContBL = new LPGrpContBL();
        if (!tLPGrpContBL.queryLPGrpCont(mLPGrpEdorItemSchema))
        {
            CError.buildErr(this, "查询投保人信息失败！");
            return false;
        }
/*
        tLPGrpContBL.setEdorNo(mLPGrpSchema.getEdorNo());
        tLPGrpContBL.setEdorType(mLPGrpSchema.getEdorType());
        tLPGrpContBL.setAppntNo(mLPGrpSchema.getCustomerNo()); //客户号码
        tLPGrpContBL.setGrpName(mLPGrpSchema.getGrpName()); //单位名称
        tLPGrpContBL.setGrpNature(mLPGrpSchema.getGrpNature()); //单位性质
        tLPGrpContBL.setBusinessType(mLPGrpSchema.getBusinessType()); //行业类别
        tLPGrpContBL.setPeoples(mLPGrpSchema.getPeoples()); //总人数
        tLPGrpContBL.setOnWorkPeoples(mLPGrpSchema.getOnWorkPeoples());
        tLPGrpContBL.setOffWorkPeoples(mLPGrpSchema.getOffWorkPeoples());
        tLPGrpContBL.setOtherPeoples(mLPGrpSchema.getOtherPeoples());
        tLPGrpContBL.setPhone(mLPGrpSchema.getPhone()); //总机
*/
        LPGrpContSet tLPGrpContSet = new LPGrpContSet();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery("select a.* from LCGrpCont a Where appntno ='"+mLPGrpAppntSchema.getCustomerNo()+"'");
        if(tLCGrpContSet==null||tLCGrpContSet.size()==0)
        {
            CError.buildErr(this, "查询投保人信息失败！");
            return false;
        }

        for(int i=1 ; i<=tLCGrpContSet.size();i++)
        {
            LPGrpContSchema tLPGrpContSchema = new LPGrpContSchema();
            tReflections.transFields(tLPGrpContSchema,tLCGrpContSet.get(i));
            tLPGrpContSchema.setEdorNo(mLPGrpSchema.getEdorNo());
            tLPGrpContSchema.setEdorType(mLPGrpSchema.getEdorType());
            tLPGrpContSchema.setAppntNo(mLPGrpSchema.getCustomerNo()); //客户号码
            tLPGrpContSchema.setGrpName(mLPGrpSchema.getGrpName()); //单位名称
            tLPGrpContSchema.setGrpNature(mLPGrpSchema.getGrpNature()); //单位性质
            tLPGrpContSchema.setBusinessType(mLPGrpSchema.getBusinessType()); //行业类别
            tLPGrpContSchema.setPeoples(mLPGrpSchema.getPeoples()); //总人数
            tLPGrpContSchema.setOnWorkPeoples(mLPGrpSchema.getOnWorkPeoples());
            tLPGrpContSchema.setOffWorkPeoples(mLPGrpSchema.getOffWorkPeoples());
            tLPGrpContSchema.setOtherPeoples(mLPGrpSchema.getOtherPeoples());
            tLPGrpContSchema.setPhone(mLPGrpSchema.getPhone()); //总机
            tLPGrpContSet.add(tLPGrpContSchema);
        }


        //准备团体客户信息
        LPGrpBL tLPGrpBL = new LPGrpBL();
        if (!tLPGrpBL.queryLPGrp(mLPGrpSchema))
        {
            CError.buildErr(this, "查询投团体客户信息！");
            return false;
        }

        tLPGrpBL.setEdorNo(mLPGrpSchema.getEdorNo());
        tLPGrpBL.setEdorType(mLPGrpSchema.getEdorType());
        tLPGrpBL.setCustomerNo(mLPGrpSchema.getCustomerNo()); //客户号码
        tLPGrpBL.setGrpName(mLPGrpSchema.getGrpName()); //单位名称
        tLPGrpBL.setGrpNature(mLPGrpSchema.getGrpNature()); //单位性质
        tLPGrpBL.setBusinessType(mLPGrpSchema.getBusinessType()); //行业类别
        tLPGrpBL.setPeoples(mLPGrpSchema.getPeoples()); //总人数
        tLPGrpBL.setOnWorkPeoples(mLPGrpSchema.getOnWorkPeoples());
        tLPGrpBL.setOffWorkPeoples(mLPGrpSchema.getOffWorkPeoples());
        tLPGrpBL.setOtherPeoples(mLPGrpSchema.getOtherPeoples());
        tLPGrpBL.setPhone(mLPGrpSchema.getPhone()); //总机
        
        //团体客户地址  LPGrpAddress
        LPGrpAddressBL tLPGrpAddressBL = new LPGrpAddressBL();
        String sql =
                "select max(int(AddressNo)) + 1 from LCGrpAddress " +
                "where CustomerNo = '" + mLPGrpAddressSchema.getCustomerNo() +
                "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String AddressNo = tExeSQL.getOneValue(sql);

        tLPGrpAddressBL.setAddressNo(AddressNo);
//        tLPGrpAppntBL.setAddressNo(AddressNo);
//        tLPGrpContBL.setAddressNo(AddressNo);
        if ("MOD".equals(addrFlag))
        {
            tLPGrpAddressBL.setEdorNo(mLPGrpAddressSchema.getEdorNo());
            tLPGrpAddressBL.setEdorType(mLPGrpAddressSchema.getEdorType());
            tLPGrpAddressBL.setCustomerNo(mLPGrpAddressSchema.getCustomerNo()); //客户号码
            tLPGrpAddressBL.setGrpAddress(mLPGrpAddressSchema.getGrpAddress()); //单位地址
            tLPGrpAddressBL.setGrpZipCode(mLPGrpAddressSchema.getGrpZipCode()); //单位邮编
            tLPGrpAddressBL.setPostalProvince(mLPGrpAddressSchema.getPostalProvince());
            tLPGrpAddressBL.setPostalCity(mLPGrpAddressSchema.getPostalCity());
            tLPGrpAddressBL.setPostalCounty(mLPGrpAddressSchema.getPostalCounty());
            tLPGrpAddressBL.setDetailAddress(mLPGrpAddressSchema.getDetailAddress());
            
            //保险联系人一
            tLPGrpAddressBL.setLinkMan1(mLPGrpAddressSchema.getLinkMan1());
            tLPGrpAddressBL.setPhone1(mLPGrpAddressSchema.getPhone1());
            tLPGrpAddressBL.setE_Mail1(mLPGrpAddressSchema.getE_Mail1());
            tLPGrpAddressBL.setFax1(mLPGrpAddressSchema.getFax1());
            tLPGrpAddressBL.setMobile1(mLPGrpAddressSchema.getMobile1());
        }
//        tLPGrpAppntBL.setDefaultFields();
//        tLPGrpAppntBL.setOperator(mGlobalInput.Operator);
//        tLPGrpContBL.setDefaultFields();
//        tLPGrpContBL.setOperator(mGlobalInput.Operator);
        for(int i=1 ; i<=tLPGrpAppntSet.size();i++)
	{
		tLPGrpAppntSet.get(i).setAddressNo(AddressNo);
		tLPGrpAppntSet.get(i).setOperator(mGlobalInput.Operator);
		tLPGrpAppntSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLPGrpAppntSet.get(i).setModifyTime(PubFun.getCurrentTime());
	}

        for(int i=1 ; i<=tLPGrpContSet.size();i++)
        {
        	tLPGrpContSet.get(i).setAddressNo(AddressNo);
        	tLPGrpContSet.get(i).setOperator(mGlobalInput.Operator);
        	tLPGrpContSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLPGrpContSet.get(i).setModifyTime(PubFun.getCurrentTime());
       	}
        tLPGrpBL.setDefaultFields();
        tLPGrpBL.setOperator(mGlobalInput.Operator);
        tLPGrpAddressBL.setDefaultFields();
        tLPGrpAddressBL.setOperator(mGlobalInput.Operator);
//        mLPGrpContSchema.setSchema(tLPGrpContBL);
//        mLPGrpAppntSchema.setSchema(tLPGrpAppntBL);
        mLPGrpContSet.clear();
        mLPGrpContSet.add(tLPGrpContSet);
        mLPGrpAppntSet.clear();
        mLPGrpAppntSet.add(tLPGrpAppntSet);
        mLPGrpSchema.setSchema(tLPGrpBL);
        mLPGrpAddressSchema.setSchema(tLPGrpAddressBL);
        mLPGrpEdorItemSchema.setEdorState("1");

        return true;
    }


}
