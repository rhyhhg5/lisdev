package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 团体特需医疗追加保费
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorZGAppConfirmBL implements EdorAppConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 团体合同号 */
	private String mGrpContNo = null;

	/** 保全生效日期 */
	private String mEdorValiDate = null;

	/** 保全交费总额 */
	private double manageMoney = 0.00;

	/** 保全项目特殊数据 */
	private EdorItemSpecialData mSpecialData = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		getInputData(cInputData);

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private void getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData
				.getObjectByObjectName("LPGrpEdorItemSchema", 0);
		mEdorNo = edorItem.getEdorNo();
		mEdorType = edorItem.getEdorType();
		mGrpContNo = edorItem.getGrpContNo();
		mEdorValiDate = edorItem.getEdorValiDate();
		mSpecialData = new EdorItemSpecialData(edorItem);
		mSpecialData.query();
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		if (!setGrpAcc()) {
			return false;
		}
		setGrpEdorItem();
		return true;
	}

	/**
	 * 设置团体账户追加保费金额
	 */
	private boolean setGrpAcc() {
		String[] grpPolNos = mSpecialData.getGrpPolNos();
		for (int i = 0; i < grpPolNos.length; i++) {
			String grpPolNo = grpPolNos[i];

			String sqlEspecialRiskcode = "select b.risktype3,a.riskcode,b.RiskType8 from lcgrppol a,lmriskapp b where a.grppolno = '"
					+ grpPolNo + "' and a.riskcode = b.riskcode  ";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
			if (tSSRS.getMaxRow() != 0
					&& (tSSRS.GetText(1, 1).equals("7") || tSSRS.GetText(1, 3).equals(BQ.RISKTYPE8_TPA))) {
				String polNo = CommonBL.getPubliAccPolNo(grpPolNo);
				
				LCInsureAccSchema tLCInsureAccSchema = CommonBL.getLCInsureAcc(
						polNo, CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, tSSRS
								.GetText(1, 2)));
				if (tLCInsureAccSchema == null) {
					mErrors.addOneError("找不到团体账户信息！");
					return false;
				}
				mSpecialData.setGrpPolNo(grpPolNo);
				manageMoney = Double.parseDouble(mSpecialData
						.getEdorValue("GroupMoney"));

				System.out.println("manageMoney" + manageMoney);
				LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
				if (tLCPolSchema == null) {
					mErrors.addOneError("找不到团体公共账户的险种信息！");
					return false;
				}
				setGetEndorseMF(manageMoney, tLCPolSchema);
				setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
				setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney,
						CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP));
				setLPInsureAccFeeTrace(tLCInsureAccSchema, manageMoney,
						CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP),
						BQ.FEECODE_GROUP);

			}
		}
		return true;
	}

	/**
	 * 设置管理费
	 * 
	 * @param aLCInsureAccSchema
	 *            LCInsureAccSchema
	 * @param manageMoney
	 *            double
	 */
	private void setLPInsureAccFee(LCInsureAccSchema aLCInsureAccSchema,
			double manageMoney) {
		String[] keys = new String[2];
		DetailDataQuery mQuery = new DetailDataQuery(mEdorNo, mEdorType);
		keys[0] = aLCInsureAccSchema.getPolNo();
		keys[1] = aLCInsureAccSchema.getInsuAccNo();
		LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema) mQuery
				.getDetailData("LCInsureAccFee", keys);

		// 若没有管理费，则生成一条
		if (tLPInsureAccFeeSchema.getGrpContNo() == null) {
			Reflections ref = new Reflections();
			ref.transFields(tLPInsureAccFeeSchema, aLCInsureAccSchema);
			tLPInsureAccFeeSchema.setFeeRate(0);
			tLPInsureAccFeeSchema.setFee(0);
			tLPInsureAccFeeSchema.setFeeUnit(0);
			tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
			PubFun.fillDefaultField(tLPInsureAccFeeSchema);

			LCInsureAccFeeSchema schemaC = new LCInsureAccFeeSchema();
			ref.transFields(schemaC, tLPInsureAccFeeSchema);
			mMap.put(schemaC, SysConst.INSERT);
		}

		tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee()+ manageMoney);
		tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
		tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
	}

	/**
	 * 设置管理费分类 轨迹
	 * 
	 * @param aLCInsureAccSchema
	 *            LCInsureAccSchema
	 * @param manageMoney
	 *            double
	 * @param feeRate
	 *            double
	 */
	private void setLPInsureAccClassFee(LCInsureAccSchema aLCInsureAccSchema,
			double manageMoney, double feeRate) {
		
		LCInsureAccClassFeeDB mLCInsureAccClassFeeDB=new LCInsureAccClassFeeDB();
		mLCInsureAccClassFeeDB.setPolNo(aLCInsureAccSchema.getPolNo());
		mLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
		mLCInsureAccClassFeeDB.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
		LCInsureAccClassFeeSet mLCInsureAccClassFeeSet=mLCInsureAccClassFeeDB.query();
		
		if(mLCInsureAccClassFeeSet.size()>0){
			manageMoney+=mLCInsureAccClassFeeSet.get(1).getFee();
		}
		
		LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
		tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
		tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
		tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccSchema.getPolNo());
		tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccSchema
				.getInsuAccNo());
		tLPInsureAccClassFeeSchema.setPayPlanCode(BQ.FILLDATA);
		tLPInsureAccClassFeeSchema.setOtherNo(mEdorNo);
		tLPInsureAccClassFeeSchema.setOtherType("3"); // 3是保全
		tLPInsureAccClassFeeSchema.setAccAscription("0");
		tLPInsureAccClassFeeSchema
				.setRiskCode(aLCInsureAccSchema.getRiskCode());
		tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccSchema.getContNo());
		tLPInsureAccClassFeeSchema
				.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
		tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccSchema
				.getGrpContNo());
		tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccSchema
				.getManageCom());
		tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccSchema
				.getInsuredNo());
		tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccSchema.getAppntNo());
		tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccSchema.getAccType());
		tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccSchema
				.getAccComputeFlag());
		tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccSchema
				.getAccFoundDate());
		tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccSchema
				.getAccFoundTime());
		tLPInsureAccClassFeeSchema
				.setBalaDate(aLCInsureAccSchema.getBalaDate());
		tLPInsureAccClassFeeSchema
				.setBalaTime(aLCInsureAccSchema.getBalaTime());
		tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
		tLPInsureAccClassFeeSchema.setFee(manageMoney);
		tLPInsureAccClassFeeSchema.setFeeUnit(0);
		tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
		tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
		tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
		tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
	}

	/**
	 * 设置批改补退费表
	 * 
	 * @return boolean
	 */
	private void setGetEndorseMF(double edorPrem, LCPolSchema aLCPolSchema) {
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); // 给付通知书号码，没意义
		tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
		tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
		tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
		tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
		tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
		tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
		tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
		tLJSGetEndorseSchema.setGetMoney(edorPrem);
		tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
		tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
		tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
		tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
		tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
		tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
		tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
		tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
		tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
		tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_MF);
		tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
		tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
		tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
		tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
		tLJSGetEndorseSchema.setOtherNo(mEdorNo);
		tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
		tLJSGetEndorseSchema.setGetFlag("0");
		tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
		tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
		tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
		mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
	}

	/**
	 * 设置item表中的费用和状态
	 */
	private void setGrpEdorItem() {
		String sql = "update LPGrpEdorItem " + "set GetMoney = " + manageMoney
				+ ", " + "EdorState = '" + BQ.EDORSTATE_CAL + "', "
				+ "UWFlag = '" + BQ.UWFLAG_PASS + "', " + "ModifyDate = '"
				+ mCurrentDate + "', " + "ModifyTime = '" + mCurrentTime + "' "
				+ "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '"
				+ mEdorType + "' ";
		mMap.put(sql.toString(), "UPDATE");
	}

	/**
	 * 
	 * 管理费轨迹
	 * 
	 * @param aLCInsureAccSchema
	 *            LCInsureAccSchema
	 * @param grpFee
	 *            double
	 * @param feeRate
	 *            double
	 * @param tFeeCode
	 *            String
	 */
	private void setLPInsureAccFeeTrace(LCInsureAccSchema aLCInsureAccSchema,
			double grpFee, double feeRate, String tFeeCode) {
		String serialNo;
		LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new LPInsureAccFeeTraceDB();
		tLPInsureAccFeeTraceDB.setEdorNo(mEdorNo);
		tLPInsureAccFeeTraceDB.setEdorType(mEdorType);
		tLPInsureAccFeeTraceDB.setGrpContNo(mGrpContNo);
		tLPInsureAccFeeTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
		tLPInsureAccFeeTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
		LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB
				.query();
		if (tLPInsureAccFeeTraceSet.size() > 0) {
			serialNo = tLPInsureAccFeeTraceSet.get(1).getSerialNo();
			String sql = "delete from LPInsureAccFeeTrace "
					+ "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '"
					+ mEdorType + "' " + "and SerialNo = '" + serialNo + "' ";
			mMap.put(sql, "DELETE");
		} else {
			serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
		}
		LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
		tLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
		tLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
		tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccSchema
				.getGrpContNo());
		tLPInsureAccFeeTraceSchema
				.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
		tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
		tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
		tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
		tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccSchema
				.getInsuAccNo());
		tLPInsureAccFeeTraceSchema
				.setRiskCode(aLCInsureAccSchema.getRiskCode());
		tLPInsureAccFeeTraceSchema.setPayPlanCode(BQ.FILLDATA);
		tLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
		tLPInsureAccFeeTraceSchema.setOtherType("3"); // 3是保全
		tLPInsureAccFeeTraceSchema.setAccAscription("0");
		tLPInsureAccFeeTraceSchema.setMoneyType(BQ.FEEFINATYPE_MF);
		tLPInsureAccFeeTraceSchema.setFeeRate(feeRate);
		tLPInsureAccFeeTraceSchema.setFee(Math.abs(grpFee)); 
		tLPInsureAccFeeTraceSchema.setFeeUnit("0");
		tLPInsureAccFeeTraceSchema.setPayDate(mEdorValiDate);
		tLPInsureAccFeeTraceSchema.setState("0");
		tLPInsureAccFeeTraceSchema.setFeeCode(tFeeCode);
		tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccSchema
				.getManageCom());
		tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
		tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
		tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
		tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
	}

	/**
	 * 调试方法
	 */
	public static void main(String[] args) {
		GlobalInput gi = new GlobalInput();
		gi.Operator = "endor0";
		gi.ComCode = "86";

		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		tLPGrpEdorItemDB.setEdorNo("20070213000061");

		VData d = new VData();
		d.add(gi);
		d.add(tLPGrpEdorItemDB.query().get(1));

		GrpEdorZGAppConfirmBL bl = new GrpEdorZGAppConfirmBL();
		if (!bl.submitData(d, "")) {
			System.out.println(bl.mErrors.getErrContent());
		}
	}
}
