package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全确认</p>
 * <p>Description: 使保全操作生效，把P表的数据放入C表</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PInsuEdorConfirmBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 提示信息 **/
    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorAcceptNo = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorAcceptNo String
     */
    public PInsuEdorConfirmBL(GlobalInput gi, String edorAcceptNo)
    {
        this.mGlobalInput = gi;
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 调用业务逻辑但不提交
     * @return boolean
     */
    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        return mMap;
    }

    /**
     * 调用业务逻辑并提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (getSubmitData() == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        LBContDB tLBCont = new LBContDB();
        //查询该工单的保全项目和对应的保单号。
        String sql = "select edortype,contno from lpedoritem where edoracceptno='"+mEdorAcceptNo+"' with ur";
        SSRS relu = new ExeSQL().execSQL(sql);
       if(relu.getMaxNumber()>0){
           if(relu.GetText(1,1).equals("TB")){
             tLBCont.setContNo(relu.GetText(1,2));
           }else{
              tLBCont.setContNo("");
           }
       }
       //判断此工单为TB且已解约，则强制结案。
        if(!tLBCont.getInfo()){
            if (!validateFinanceData()) {
                return false;
            }
            if (!confirmAllItems()) {
                return false;
            }
            updateContMoney();
        }
        setEdorState(BQ.EDORSTATE_CONFIRM); //保全结案
        return true;
    }

    /**
     * 得到Main表中的数据，Main表中一条记录对应一个ContNo
     * @return LPEdorMainSet
     */
    private LPEdorMainSet getEdorMains()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorAcceptNo);
        return tLPEdorMainDB.query();
    }

    /**
     * 得到保全项目
     * @return LPEdorItemSet
     */
    private LPEdorItemSet getEdorItems()
    {
        String sql = "select * from LPEdorItem " +
                "where EdorAcceptNo = '" + mEdorAcceptNo + "' " +
                "order by ModifyDate, ModifyTime ";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        return tLPEdorItemDB.executeQuery(sql);
    }

    /**
     * 对该保全受理下的所有项目依次确认
     */
    private boolean confirmAllItems()
    {
        LPEdorItemSet tLPEdorItemSet = getEdorItems();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            if (!confirmOneItem(tLPEdorItemSet.get(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 确认一个项目
     * @return boolean
     */
    private boolean confirmOneItem(LPEdorItemSchema edorItem)
    {
        try
        {
            //按项目编码调用不同的理算类
            String edorType = edorItem.getEdorType();
            PEdorADConfirmBL tPEdorConfirm = new PEdorADConfirmBL();
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(edorItem);
            if (!tPEdorConfirm.submitInsuData(data, "CONFIRM||" + edorType))
            {
                mErrors.copyAllErrors(tPEdorConfirm.mErrors);
                return false;
            }
            VData retData = tPEdorConfirm.getResult();
            MMap map = (MMap) retData.getObjectByObjectName("MMap", 0);
            if (map == null)
            {
                mErrors.copyAllErrors(tPEdorConfirm.mErrors);
                mErrors.addOneError(edorType + "项目保全确认失败!");
                return false;
            }
            mMap.add(map);
            mMessage = (String) retData.getObjectByObjectName("String", 0);
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 更新LCCont表使其保费和LCPol表保费之和一致
     */
    private void updateContMoney()
    {
        LPEdorMainSet tLPEdorMainSet = getEdorMains();
        for (int i = 1; i <= tLPEdorMainSet.size(); i++)
        {
            String contNo = tLPEdorMainSet.get(i).getContNo();
            String sql = "update LCCont " +
                    "set Prem = (select sum(Prem) from LCPol " +
                    "    where ContNo = '" + contNo + "'), " +
                    "Amnt = (select sum(Amnt) from LCPol " +
                    "    where ContNo = '" + contNo + "'), " +
                    "SumPrem = (select sum(SumPrem) from LCPol " +
                    "    where ContNo = '" + contNo + "'), " +
                    "CInValiDate = (select max(EndDate) from LCPol "+
                    "    where appFlag = '1' and ContNo = '"+ contNo + "')" +
                    "where ContNo = '" + contNo + "' ";
            mMap.put(sql, "UPDATE");
        }
    }

    /**
     * 使财务数据生效
     * @return boolean
     */
    private boolean validateFinanceData()
    {
        ValidateFinanceData vfd = new ValidateFinanceData(
                mGlobalInput, mEdorAcceptNo, BQ.NOTICETYPE_P);
        if (!vfd.submitData())
        {
            return false;
        }
        mMap.add(vfd.getMap());
        return true;
    }


    /**
     * 设置保全状态,"0"为保全结案状态
     */
    private void setEdorState(String edorState)
    {
        String sql;
        sql = "  update LPEdorApp  "
              + "set EdorState = '" + edorState + "', "
              + "    ConfDate = '" + PubFun.getCurrentDate() + "', "
//              + "    ConfTime = '" + PubFun.getCurrentTime() + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorMain "
              + "set EdorState = '" + edorState + "', "
              + "    ConfDate = '" + PubFun.getCurrentDate() + "', "
              + "    ConfTime = '" + PubFun.getCurrentTime() + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
        sql = "  update LPEdorItem "
              + "set EdorState = '" + edorState + "', "
              + "    Operator = '" + mGlobalInput.Operator + "', "
              + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
              +  "where EdorAcceptNo = '" + mEdorAcceptNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
