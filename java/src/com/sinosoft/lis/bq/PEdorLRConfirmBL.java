package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PEdorLRConfirmBL implements EdorConfirm
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private GlobalInput mGlobalInput = new GlobalInput();

    public PEdorLRConfirmBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //数据准备操作
        if (!prepareData())
        {
            return false;
        }
        System.out.println("---End prepareData---");

        //数据操作业务处理
//        PEdorConfirmBLS tPEdorConfirmBLS = new PEdorConfirmBLS();
//        if (!tPEdorConfirmBLS.submitData(mInputData, mOperate))
//        {
//            CError.buildErr(this, "数据提交失败", tPEdorConfirmBLS.mErrors);
//            return false;
//        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema",
                    0);
            mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        if (mLPEdorItemSchema == null || mGlobalInput == null)
  {
      CError tError = new CError();
      tError.moduleName = "GrpEdorNIAppConfirmBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
  }

  LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
  tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
  tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
  tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
  LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
  if (tLPEdorItemSet == null || tLPEdorItemSet.size() != 1)
  {
      LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
      tLPGrpEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
      tLPGrpEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
      tLPGrpEdorItemDB.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
      LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
      if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1) //团单项目表也没有LR项目，说明保全项目查询失败
      {
          mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
          mErrors.addOneError(new CError("查询保全项目信息失败！"));
          return false;
      }
      mResult.clear();
      MMap map=new MMap();
      mResult.add(map);
            return true;//存在团单LR项目，此时是团单LR确认调用了个单LR确认，添加空map并返回true
  }
  mLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1));

  LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
  tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSchema.getEdorAcceptNo());
  tLPEdorMainDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
  tLPEdorMainDB.setContNo(mLPEdorItemSchema.getContNo());
  if (!tLPEdorMainDB.getInfo())
  {
      mErrors.copyAllErrors(tLPEdorMainDB.mErrors);
      mErrors.addOneError(new CError("查询保全信息失败！"));
      return false;
  }
  mLPEdorMainSchema = tLPEdorMainDB.getSchema();


        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareData()
    {
//        LPEdorMainBL tLPEdorMainBL = new LPEdorMainBL();
//
//        //得到当前LPEdorMain保单信息主表的状态，并更新状态为申请确认。
//        tLPEdorMainBL.setSchema(mLPEdorMainSchema);
//        tLPEdorMainBL.setUpdateFields();
//        mLPEdorMainSchema = tLPEdorMainBL.getSchema();
//        mLPEdorMainSchema.setEdorState("0");
//        mLPEdorMainSchema.setConfDate(PubFun.getCurrentDate());
//        mLPEdorMainSchema.setConfOperator(mGlobalInput.Operator);
//
//        //生效日期为保全生效日期
//        mLPEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());
//
//        mInputData.clear();
//        mInputData.add(mLPEdorMainSchema);
        mResult.clear();
        MMap map = new MMap();
        mLPEdorMainSchema.setEdorState("0");
        mLPEdorMainSchema.setConfDate(PubFun.getCurrentDate());
        mLPEdorMainSchema.setConfOperator(mGlobalInput.Operator);
        //当前日期为保全生效日期
        mLPEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());

        mLPEdorItemSchema.setEdorState("0");
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);

        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            mErrors.addOneError(new CError("相关的集体保单表中没有记录！"));
            return false;
        }
        tLCContSchema=tLCContDB.getSchema();
        tLCContSchema.setLostTimes(tLCContSchema.getLostTimes()+1);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());
        //简易单不修改打印次数 qulq 2007-11-28
//        if(!("1".equals(tLCContSchema.getCardFlag())||"2".equals(tLCContSchema.getCardFlag())||"6".equals(tLCContSchema.getCardFlag())))
//        {
            tLCContSchema.setPrintCount(0);
//        }
        map.put(mLPEdorItemSchema, "UPDATE");
        map.put(mLPEdorMainSchema, "UPDATE");
        map.put(tLCContSchema, "UPDATE");
        mResult.add(map);

        return true;
    }
}
