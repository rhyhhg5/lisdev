package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;

public class EdorFinishUI
{
    private EdorFinishBL mEdorFinishBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public EdorFinishUI(GlobalInput gi, String edorNo)
    {
        mEdorFinishBL = new EdorFinishBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData(String operator)
    {
        if (!mEdorFinishBL.submitData(operator))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mEdorFinishBL.getError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mEdorFinishBL.getMessage();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String args[])
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor";
        g.ComCode = "86";
        EdorFinishBL bl = new EdorFinishBL(g, "20051104000023");
        if (!bl.submitData(BQ.CONTTYPE_P))
        {
            System.out.println("保全确认未通过！原因是：" + bl.getError());
        }
    }
}
