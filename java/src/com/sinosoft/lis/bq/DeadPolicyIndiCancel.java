package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.operfee.*;
/**
 * <p>Title:失效保单作废应收 </p>
 *
 * <p>Description: 由失效批处理程序调用</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author qulq
 * @version 1.0
 */
public class DeadPolicyIndiCancel {
    CErrors mErrors = new CErrors();
    GlobalInput mGlobalInput = new GlobalInput();
    MMap mMap = null;

    public DeadPolicyIndiCancel() {
        mGlobalInput.ManageCom ="86";
        mGlobalInput.Operator = "Server";
    }

    /**
     * 当前机构
     * @param gi GlobalInput
     */

    public DeadPolicyIndiCancel(GlobalInput gi) {
        this.mGlobalInput = gi;
    }

    public void dealData()
    {
        dealDataP();
//        dealDataG();
        if(mErrors.needDealError())
        {
            System.out.println(mErrors.getErrContent());
        }
    }
    private void dealDataP()
    {
        String sql = "select getnoticeno from ljspay a where othernotype = '2' "
                     +" and exists (select 1 from ljspayperson b , lcpol c "
                     +" where b.polno = c. polno and c.stateflag in ('2','3') "
                     +" and b.getnoticeno = a.getnoticeno )"
                     +" and a.managecom like '"+mGlobalInput.ManageCom+"%'"
                     ;
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();
        for(int i =1 ;i<=tSSRS.getMaxRow() ;i++)
        {
            try {
                LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                tLJSPaySchema.setGetNoticeNo(tSSRS.GetText(i,1));

                TransferData tTransferData = new TransferData();
                //作废原因为收费失败
                tTransferData.setNameAndValue("CancelMode", "4");
                //zhangm 20090317 保单失效作废应收时催收状态设置为"已撤销" cbs00024530 
                tTransferData.setNameAndValue("IsRepeal", "2");

                VData tVData = new VData();
                tVData.addElement(tLJSPaySchema);
                tVData.addElement(mGlobalInput);
                tVData.addElement(tTransferData);

                if(!tIndiLJSCancelUI.submitData(tVData, "INSERT"))
                {
                    mErrors.addOneError("个单失效保单作废应收"+tSSRS.GetText(i,1)+"失败");
                }
            } catch (Exception ex) {
                mErrors.addOneError(ex.toString());
            }
        }
    }
    private void dealDataG()
    {
        String sql = "select getnoticeno from ljspay a where othernotype = '1' and "
                     +" exists (select 1 from ljspaygrp b ,lcgrppol c "
                     +" where b.grppolno = c.grppolno and c.stateflag in ('2','3')"
                     +" and b.getnoticeno = a.getnoticeno )"
                     +" and a.managecom like '"+mGlobalInput.ManageCom+"%'"
                     ;
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        GrpLJSCancelUI tGrpLJSCancelUI = new GrpLJSCancelUI();
        for(int i =1 ;i<=tSSRS.getMaxRow() ;i++)
        {
            try {
                LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                tLJSPaySchema.setGetNoticeNo(tSSRS.GetText(i,1));

                TransferData tTransferData = new TransferData();
                //作废原因为收费失败
                tTransferData.setNameAndValue("CancelMode", "4");

                VData tVData = new VData();
                tVData.addElement(tLJSPaySchema);
                tVData.addElement(mGlobalInput);
                tVData.addElement(tTransferData);

                if(!tGrpLJSCancelUI.submitData(tVData, "INSERT"))
                {
                    mErrors.addOneError("团单失效保单作废应收"+tSSRS.GetText(i,1)+"失败");
                }
            } catch (Exception ex) {
                mErrors.addOneError(ex.toString());
            }
        }
    }
   public static void main(String []args)
   {
       DeadPolicyIndiCancel temp = new DeadPolicyIndiCancel();
       temp.dealData();
   }

}
