package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 无名单实名化</p>
* <p>Description: 无名单实名化</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListWSUI
{
    PrtGrpInsuredListWSBL mPrtGrpInsuredListWSBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListWSUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListWSBL = new PrtGrpInsuredListWSBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListWSBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListWSBL.getInputStream();
    }
}
