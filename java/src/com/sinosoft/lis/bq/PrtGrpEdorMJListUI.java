package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpEdorMJListUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = null;

    private PrtGrpEdorMJListBL mPrtGrpEdorMJListBL = null;

    public PrtGrpEdorMJListUI()
    {
    }

    /**
     * 操作提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        mPrtGrpEdorMJListBL =
            new PrtGrpEdorMJListBL();

        if (!mPrtGrpEdorMJListBL.submitData(cInputData, operate))
        {
            mErrors = mPrtGrpEdorMJListBL.mErrors;
            return false;
        }

        return true;
    }

    /**
     * 得到批单信息
     * @return XmlExport
     */
    public XmlExport getDealXmlExport()
    {
        return mPrtGrpEdorMJListBL.getDealXmlExport();
    }


    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "pa1101";
        tGlobalInput.ComCode = "8611";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("edorAcceptNo", "20060125000007");
        tTransferData.setNameAndValue("prtType", "MJDeal");

        VData data = new VData();
        data.add(tGlobalInput);
        data.add(tTransferData);

        PrtGrpEdorMJListBL tPrtGrpEdorMJListBL = new PrtGrpEdorMJListBL();
        if (!tPrtGrpEdorMJListBL.submitData(data, "")) {
            System.out.println(tPrtGrpEdorMJListBL.mErrors.getErrContent());
        }

    }
}
