package com.sinosoft.lis.bq;

/**
 * @author 张彦梅 2008年6月20日
 * 检测万能险个单是否应该进入宽限期，对于该进入宽限期的保单，设置为进入宽限期状态
 *
 */
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PolInGracePeriodBL
{
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    public CErrors mErrors = new CErrors();
    private LCContStateSet mLCContStateSet = new LCContStateSet();

    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("GracePeriod"))
        {
            try
            {
                setToGracePeriod();
            } catch (Exception ex)
            {
                System.out.print(ex.toString());
            }
        }

        VData data = new VData();
        data.add(mMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "PolicyAbateDealBL";
            tError.functionName = "callGrpPauseJS";
            tError.errorMessage = tPubSubmit.mErrors.getErrContent();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
        }
        return true;
    }

    // 得到输入参数
    private boolean getInputData(VData cInputData)
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom; // 得到管理机构
        return true;
    }

    // 万能险个单进入宽限期
    private void setToGracePeriod()
    {
        String subSql = getSQLManageCom();
        String sql = "";
        sql = " select * from LCInsureAcc  ,lcpol,lmriskapp "
              + " where LCInsureAcc.insuAccBala<=0"
              + " and  lmriskapp.risktype4='4' "
              + " and lmriskapp.riskcode=LCInsureAcc.riskcode"
              + "	and lcpol.AppFlag='1' and lcpol.payintv > 0 "
              + " and lcpol.stateFlag not in('2','3') "
              + " and  lcpol.polno=LCInsureAcc.polno"
              + " and not exists(select * from lccontstate a where  a.contno=LCInsureAcc.contNo and a.insuredNo=LCInsureAcc.insuredNo and a.stateType='GracePeriod' )"
              + subSql;
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        tLCInsureAccSet = tLCInsureAccDB.executeQuery(sql);
        System.out.println("得到的LCInsureAccSet 长度为" + tLCInsureAccSet.size());
        if (tLCInsureAccSet == null || tLCInsureAccSet.size() == 0)
        {
            System.out.println("没有要进入宽限期的保单");
        }
        if (tLCInsureAccSet.size() > 0)
        {
            flagContsInGracePeriod(tLCInsureAccSet);
        }
    }

    private void flagContsInGracePeriod(LCInsureAccSet cLCInsureAccSet)
    {
        for (int i = 1; i < cLCInsureAccSet.size() + 1; i++)
        {
            LCInsureAccSchema tLCInsureAccSchema = cLCInsureAccSet.get(i);
            LCContStateSchema tLCContStateSchema = new LCContStateSchema();

            String tStartDate = getPayToDateByPolNo(tLCInsureAccSchema
                    .getPolNo());
            String tEndDate = PubFun.calDate(tStartDate,60,"D",null);

            tLCContStateSchema.setContNo(tLCInsureAccSchema.getContNo());
            tLCContStateSchema.setPolNo("000000");
            tLCContStateSchema.setInsuredNo(tLCInsureAccSchema.getInsuredNo());
            tLCContStateSchema.setGrpContNo("000000");
            tLCContStateSchema.setStateType("GracePeriod");
            tLCContStateSchema.setState("0");
            tLCContStateSchema.setStateReason("01");
            tLCContStateSchema.setEndDate(tEndDate);
            tLCContStateSchema.setStartDate(tStartDate);

            tLCContStateSchema.setOperator(mG.Operator);
            tLCContStateSchema.setMakeDate(mCurrentDate);
            tLCContStateSchema.setMakeTime(mCurrentTime);
            tLCContStateSchema.setModifyDate(mCurrentDate);
            tLCContStateSchema.setModifyTime(mCurrentTime);

            mLCContStateSet.add(tLCContStateSchema);
        }
        System.out.println("加入mLCContStateSet后");
        mMap.put(mLCContStateSet, "DELETE&INSERT");
    }

    // 根据险种号获得该险种的PayToDate
    private String getPayToDateByPolNo(String cPolNo)
    {
        String tPayToDate = "";
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(cPolNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet == null || tLCPolSet.size() < 0)
        {
            System.out.println("错误的险种号码");
        }
        tPayToDate = tLCPolSet.get(1).getPaytoDate();
        return tPayToDate;
    }

    /* SQL中取管理结构工具 */
    private String getSQLManageCom()
    {
        String mSQL = "";
        if (mManageCom != null && !mManageCom.trim().equals(""))
        {
            if (mManageCom.length() >= 4)
            {

                mSQL = " and lcpol.ManageCom like '" +
                       mManageCom.substring(0, 4)
                       + "%'";
            } else
            {
                mSQL = " and lcpol.ManageCom like '86%'";
            }
        } else
        {
            mSQL = " ";
        }
        return mSQL;
    }

    public static void main(String[] args)
    {
        VData mVData = new VData();
        GlobalInput mG = new GlobalInput();
        mG.Operator = "task";
        mG.ManageCom = "86";
        mVData.add(mG);

        PolInGracePeriodBL tPolInGracePeriodBL = new PolInGracePeriodBL();
        tPolInGracePeriodBL.submitData(mVData, "GracePeriod");
    }
}
