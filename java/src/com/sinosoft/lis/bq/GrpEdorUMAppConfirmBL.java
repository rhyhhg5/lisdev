package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体特需医疗提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorUMAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    private DetailDataQuery mQuery = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setInsuredAcc())
        {
            return false;
        }
        setGrpEdorItem();
        return true;
    }

    /**
     * 设置账户余额
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private void setLPInsureAcc(LCInsureAccClassSchema aLCInsureAccClassSchema, double leftMoney)
    {
    	
    	LCInsureAccSchema aLCInsureAccSchema = new LCInsureAccDB().executeQuery("select * from lcinsureacc where polno='"+aLCInsureAccClassSchema.getPolNo()+"' and insuaccno='"+aLCInsureAccClassSchema.getInsuAccNo()+"' ").get(1);
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccSchema, aLCInsureAccSchema);
        tLPInsureAccSchema.setEdorNo(mEdorNo);
        tLPInsureAccSchema.setEdorType(mEdorType);
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccSchema.setModifyDate(mCurrentDate);
        tLPInsureAccSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
        LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
        ref.transFields(tLPInsureAccClassSchema, aLCInsureAccClassSchema);
        tLPInsureAccClassSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassSchema.setEdorType(mEdorType);
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccClassSchema aLCInsureAccClassSchema, double grpMoney)
    {
    	Reflections ref = new Reflections();
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        ref.transFields(tLPInsureAccTraceSchema, aLCInsureAccClassSchema);
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setSerialNo(PubFun1.CreateMaxNo("BQACCTRACE", 9));
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(mEdorType);
        tLPInsureAccTraceSchema.setMoney(grpMoney);  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(aLCInsureAccClassSchema.getBalaDate());
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }
    
    
    /**
     * 获取账户
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private LCInsureAccClassSchema getAccClass(LPPolSchema tLPPolSchema, String tInsuAccNo)
    {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLPPolSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tInsuAccNo);
        if(tLCInsureAccClassDB.query().size()!=1){
        	return null;
        }
        return tLCInsureAccClassDB.query().get(1);
    }

    /**
     * 设置个人账户追加保费金额
     * @return boolean
     */
    private boolean setInsuredAcc()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        LPPolSet tLPPolSet = tLPPolDB.query();
        if(tLPPolSet.size()!=1){
        	CError.buildErr(this, "line238-获取个人险种信息失败！");
            return false;
        }
        LPPolSchema tLPPolSchema=new LPPolSchema();
        tLPPolSchema=tLPPolSet.get(1);
        
        //获取个人账户单位部分保险账户号码
        String tGrpInsuNo=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')) ");
        LCInsureAccClassSchema tLCGrpAccClassSchema=getAccClass(tLPPolSchema,tGrpInsuNo);
    	if(tLCGrpAccClassSchema==null){
    		CError.buildErr(this, "line238-获取个人账户单位缴费账户失败！");
            return false;
    	}
        setLPInsureAcc(tLCGrpAccClassSchema, 0);
        setLPInsureAccTrace(tLCGrpAccClassSchema, -tLCGrpAccClassSchema.getInsuAccBala());
    	
//        	获取个人账户个人部分保险账户号码
        String tInsuNo = new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLPPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
        LCInsureAccClassSchema tLCInsuredAccClassSchema=getAccClass(tLPPolSchema,tInsuNo);
    	if(tLCInsuredAccClassSchema==null){
    		CError.buildErr(this, "line238-获取个人账户个人缴费账户失败！");
            return false;
    	}
        setLPInsureAcc(tLCInsuredAccClassSchema, 0);
        setLPInsureAccTrace(tLCInsuredAccClassSchema, -tLCInsuredAccClassSchema.getInsuAccBala());
        
        mGetMoney =Double.parseDouble(new ExeSQL().getOneValue("select edorvalue from lpedorespecialdata where edorno='"+mEdorNo+"' and detailtype='SUM' "));
        setGetEndorse(tLPPolSchema, mGetMoney);
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LPPolSchema aLPPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLPPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLPPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLPPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        tLJSGetEndorseSchema.setRiskCode(aLPPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLPPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLPPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLPPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLPPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLPPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLPPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLPPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLPPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TB);
        tLJSGetEndorseSchema.setAppntNo(aLPPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLPPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem()
    {
        System.out.println(mGetMoney);
        String sql = "update LPGrpEdorItem " +
                "set GetMoney = -" + mGetMoney + ", " + //这里是负数
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + mCurrentDate + "', " +
                "ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema, double leftMoney, double getMoney)
    {
        tLPDiskImportSchema.setMoney2(String.valueOf(getMoney));
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }
 }
