package com.sinosoft.lis.bq;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SpecialModifyCinalidateBL {

    public SpecialModifyCinalidateBL() {
    }

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = null;
    /***/
    private TransferData tempTransferData = new TransferData();

    private String mGrpContNo = "";

    private String mCinValiDate ="";
    
    /** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 操作员 */
	private String mOperater ;
	
	private MMap map = new MMap();

    private SSRS mSSRS = new SSRS();
    
    /** 变更次数 */
    private String mCount = "0";
    
    /** 工单号 */
    private String mWorkNo = null;
    
    /** 工单类型-修改特需终止日期 */
    private String mWorkType = BQ.CINVALIDATE_CHANGE;
    
    
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        
        PubSubmit tSubmit = new PubSubmit();
        mResult.add(map);

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "SpecialModifyCinalidateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("SpecialModifyCinalidateBL End PubSubmit");
        return true;
    }

    public boolean getInputData() {
       try{
           mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
           mOperater = mGlobalInput.Operator;

           tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);

           mGrpContNo = (String)tempTransferData.getValueByName("tGtpContNo");
           if (mGrpContNo == null || "".equals(mGrpContNo) || "null".equals(mGrpContNo)) 
           {
        	   CError tError = new CError();
               tError.moduleName = "SpecialModifyCinalidateBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "传入的mGrpContNo为空!";
               this.mErrors.addOneError(tError);
               return false;
           }
           LCGrpContDB tLCGrpContDB = new LCGrpContDB();
           tLCGrpContDB.setGrpContNo(mGrpContNo);
           if(!tLCGrpContDB.getInfo())
           {
        	   CError tError = new CError();
               tError.moduleName = "SpecialModifyCinalidateBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "查询LCGrpCont失败!";
               this.mErrors.addOneError(tError);
               return false;
           }
           mLCGrpContSchema = tLCGrpContDB.getSchema();

           mCinValiDate = (String)tempTransferData.getValueByName("tCinvaliDate");
           if (mCinValiDate == null || "".equals(mCinValiDate) || "null".equals(mCinValiDate)) 
           {
           	CError tError = new CError();
               tError.moduleName = "SpecialModifyCinalidateBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "传入的mCinValiDate为空!";
               this.mErrors.addOneError(tError);
               return false;
           }

           if(mGlobalInput==null ||tempTransferData==null)
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "PEdorRBDetailBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "接收数据失败!!";
               this.mErrors.addOneError(tError);
               return false;
           }
           mCount = getCount();

       }catch(Exception ex)
       {
           ex.getMessage();
       }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
        return mResult;
    }

    private boolean checkData()
    {
        if (!CheckState()) 
        {
            return false;
        }
        if (!CheckRiskAmount())
        {
            return false;
        }
        if (!CheckBQ())
        {
            return false;
        }
        if (!CheckPayToDate())
        {
            return false;
        }
        return true ;
    }

    /**
     * 校验是否有未结案的保全。
     * @return boolean
     */
    private boolean CheckBQ()
    {
        String sqlBQ=" select 1 From lpedorapp a,lpgrpedoritem b where a.edoracceptno = b.edoracceptno "
                    +" and b.grpcontno ='"+mGrpContNo+"'"
                    +" and a.edorState !='0'"
                    ;
        mSSRS = new ExeSQL().execSQL(sqlBQ);
        if(mSSRS.getMaxRow()>0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SpecialModifyCinalidateBL";
            tError.functionName = "CheckBQ";
            tError.errorMessage = "保单有未结案的工单，不能做终止日期变更！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 查看保单是否只含有特选险种，如果不是返回false;
     * @return boolean
     */

    private boolean CheckRiskAmount()
    {
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        tLCGrpPolSet = tLCGrpPolDB.query();

        if (tLCGrpPolSet.size()>1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SpecialModifyCinalidateBL";
            tError.functionName = "CheckRiskAmount";
            tError.errorMessage = "保单含有多个险种不能作此变更！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCGrpPolSchema = tLCGrpPolSet.get(1).getSchema();
        if (!getSpecialRisk(tLCGrpPolSchema.getGrpContNo()))
        {
            return false;
        }
        return true;
    }


    /*
     * 判断险种是否是特需险种。
     *  @param grpContNo String：团体客户号码
     *  @return String 险种号码
     */
    private  boolean getSpecialRisk(String grpContNo) {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "SpecialModifyCinalidateBL";
                tError.functionName = "getSpecialRisk";
                tError.errorMessage = "获得险种信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String riskType = tLMRiskAppDB.getRiskType3();
            if ((riskType != null) && (riskType.equals(BQ.RISKTYPE3_SPECIAL))) {
                return true;
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "SpecialModifyCinalidateBL";
                tError.functionName = "getSpecialRisk";
                tError.errorMessage = "获得险种信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 只有趸交保单才可以做此变更
     * @return boolean
     */

    private boolean CheckPayToDate()
    {
        String SQL = "select paytodate,payenddate From lcgrppol where GrpContNo ='"+mGrpContNo+"' ";
        mSSRS = new ExeSQL().execSQL(SQL);
        String tPayToDate = mSSRS.GetText(1,1);
        String tPayenddate = mSSRS.GetText(1,2);
        if(!tPayToDate.equals(tPayenddate))
        {
            CError tError = new CError();
            tError.moduleName = "SpecialModifyCinalidateBL";
            tError.functionName = "CheckPayToDate";
            tError.errorMessage = "保单缴费不是趸交方式，不能修改！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    /**
     * 只有有效的保单才可以做此变更
     * @return boolean
     */
    private boolean CheckState()
    {
        if (!mLCGrpContSchema.getStateFlag().equals(BQ.STATE_FLAG_SIGN))
        {
            CError tError = new CError();
            tError.moduleName = "SpecialModifyCinalidateBL";
            tError.functionName = "CheckState";
            tError.errorMessage = "只有有效的保单才可以作此变更！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
    	if(!createWork())
    	{
    		return false;
    	}
    	if(!bakP())
    	{
    		return false;
    	}
    	if(!updateC())
    	{
    		return false;
    	}
    	return true;
    }
    
    //计算是第几次修改特需保单终止日期
    private String getCount()
    {
    	String tCount = null;
    	StringBuffer sql = new StringBuffer();
    	sql.append("select count(1)+1 from lgwork where ")
		   .append("typeno = '").append(mWorkType).append("' ")
		   .append("and contno = '").append(mGrpContNo).append("' with ur");
    	tCount = new ExeSQL().getOneValue(sql.toString());
    	System.out.println("第"+tCount+"次修改特需满期终止日。");
    	return tCount ;
    }
    //生成工单操作轨迹
    private boolean createWork()
    {
    	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLGWorkSchema.setInnerSource(mCount);
        tLGWorkSchema.setContNo(mGrpContNo);
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo(mWorkType);
        StringBuffer tRemark = new StringBuffer();
        tRemark.append("自动批注：修改特需保单终止日期，由[").append(mLCGrpContSchema.getCInValiDate())
               .append("] 变更为 [").append(mCinValiDate).append("]")
               .append("本次是第").append(mCount).append("次变更。");
        tLGWorkSchema.setRemark(tRemark.toString());
        String workBoxNo = getWorkBox();
        if(workBoxNo == null)
        {
            return false;
        }
        
        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGlobalInput);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("生成工单信息失败:\n"+ tTaskInputBL.mErrors.getFirstError());
            return false;
        }
        map.add(tMMap);
        mWorkNo = tTaskInputBL.getWorkNo();
        System.out.println("生成工单号:mWorktype = "+mWorkNo);
    	return true;
    }
    //备份C表到P表
    private boolean bakP()
    {
    	Reflections ref = new Reflections();
    	LPGrpContSchema tLPGrpContSchema = new LPGrpContSchema();
    	ref.transFields(tLPGrpContSchema, mLCGrpContSchema);
    	tLPGrpContSchema.setEdorNo(mWorkNo);
    	tLPGrpContSchema.setEdorType(mWorkType);
    	map.put(tLPGrpContSchema, SysConst.DELETE_AND_INSERT);
    	
    	LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    	tLCGrpPolDB.setGrpContNo(mGrpContNo);
    	LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
    	LPGrpPolSet tLPGrpPolSet = new LPGrpPolSet();
    	for(int i=1;i<=tLCGrpPolSet.size();i++)
    	{
    		LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
    		ref.transFields(tLPGrpPolSchema, tLCGrpPolSet.get(i));
    		tLPGrpPolSchema.setEdorNo(mWorkNo);
    		tLPGrpPolSchema.setEdorType(mWorkType);
    		tLPGrpPolSet.add(tLPGrpPolSchema);
    	}
    	map.put(tLPGrpPolSet, SysConst.DELETE_AND_INSERT);
    	
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setGrpContNo(mGrpContNo);
    	LCContSet tLCContSet = tLCContDB.query();
    	LPContSet tLPContSet = new LPContSet();
    	for(int i=1;i<=tLCContSet.size();i++)
    	{
    		LPContSchema tLPContSchema = new LPContSchema();
    		ref.transFields(tLPContSchema, tLCContSet.get(i));
    		tLPContSchema.setEdorNo(mWorkNo);
    		tLPContSchema.setEdorType(mWorkType);
    		tLPContSet.add(tLPContSchema);
    	}
    	map.put(tLPContSet, SysConst.DELETE_AND_INSERT);
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolDB.setGrpContNo(mGrpContNo);
    	LCPolSet tLCPolSet = tLCPolDB.query();
    	LPPolSet tLPPolSet = new LPPolSet();
    	for(int i=1;i<=tLCPolSet.size();i++)
    	{
    		LPPolSchema tLPPolSchema = new LPPolSchema();
    		ref.transFields(tLPPolSchema, tLCPolSet.get(i));
    		tLPPolSchema.setEdorNo(mWorkNo);
    		tLPPolSchema.setEdorType(mWorkType);
    		tLPPolSet.add(tLPPolSchema);
    	}
    	map.put(tLPPolSet, SysConst.DELETE_AND_INSERT);
    	
    	LCPremDB tLCPremDB = new LCPremDB();
    	tLCPremDB.setGrpContNo(mGrpContNo);
    	LCPremSet tLCPremSet = tLCPremDB.query();
    	LPPremSet tLPPremSet = new LPPremSet();
    	for(int i=1;i<=tLCPremSet.size();i++)
    	{
    		LPPremSchema tLPPremSchema = new LPPremSchema();
    		ref.transFields(tLPPremSchema, tLCPremSet.get(i));
    		tLPPremSchema.setEdorNo(mWorkNo);
    		tLPPremSchema.setEdorType(mWorkType);
    		tLPPremSet.add(tLPPremSchema);
    	}
    	map.put(tLPPremSet, SysConst.DELETE_AND_INSERT);
    	
    	LCGetDB tLCGetDB = new LCGetDB();
    	tLCGetDB.setGrpContNo(mGrpContNo);
    	LCGetSet tLCGetSet = tLCGetDB.query();
    	LPGetSet tLPGetSet = new LPGetSet();
    	for(int i=1;i<=tLCGetSet.size();i++)
    	{
    		LPGetSchema tLPGetSchema = new LPGetSchema();
    		ref.transFields(tLPGetSchema, tLCGetSet.get(i));
    		tLPGetSchema.setEdorNo(mWorkNo);
    		tLPGetSchema.setEdorType(mWorkType);
    		tLPGetSet.add(tLPGetSchema);
    	}
    	map.put(tLPGetSet, SysConst.DELETE_AND_INSERT);
    	
    	
    	StringBuffer sql = new StringBuffer();
    	sql.append("select * from lcduty a where exists (select 1 from lcpol ")
    	   .append("where polno = a.polno and grpcontno = '").append(mGrpContNo).append("') with ur");
    	System.out.println("SQL: "+sql.toString());
    	LCDutyDB tLCDutyDB = new LCDutyDB();
    	LCDutySet tLCDutySet = tLCDutyDB.executeQuery(sql.toString());
    	LPDutySet tLPDutySet = new LPDutySet();
    	for(int i=1;i<=tLCDutySet.size();i++)
    	{
    		LPDutySchema tLPDutySchema = new LPDutySchema();
    		ref.transFields(tLPDutySchema, tLCDutySet.get(i));
    		tLPDutySchema.setEdorNo(mWorkNo);
    		tLPDutySchema.setEdorType(mWorkType);
    		tLPDutySet.add(tLPDutySchema);
    	}
    	map.put(tLPDutySet, SysConst.DELETE_AND_INSERT);
    	return true;
    }
    
    //更新C表
    private boolean updateC()
    {
    	StringBuffer sqlLCGrpCont = new StringBuffer();
    	sqlLCGrpCont.append("update lcgrpcont set ")
		   .append("cinvalidate = date('").append(mCinValiDate).append("') - 1 day, ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCGrpPol = new StringBuffer();
    	sqlLCGrpPol.append("update LCGrpPol set ")
		   .append("Paytodate = '").append(mCinValiDate).append("', ")
		   .append("Payenddate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCCont = new StringBuffer();
    	sqlLCCont.append("update LCCont set ")
		   .append("cinvalidate = '").append(mCinValiDate).append("', ")
		   .append("Paytodate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCPol = new StringBuffer();
    	sqlLCPol.append("update LCPol set ")
		   .append("enddate = '").append(mCinValiDate).append("', ")
		   .append("payenddate = '").append(mCinValiDate).append("', ")
		   .append("Paytodate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCGet = new StringBuffer();
    	sqlLCGet.append("update LCGet set ")
		   .append("getenddate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCPrem = new StringBuffer();
    	sqlLCPrem.append("update LCPrem set ")
		   .append("paytodate = '").append(mCinValiDate).append("', ")
		   .append("payenddate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where grpcontno = '").append(mGrpContNo).append("' ");
    	StringBuffer sqlLCDuty = new StringBuffer();
    	sqlLCDuty.append("update LCDuty a set ")
		   .append("paytodate = '").append(mCinValiDate).append("', ")
		   .append("payenddate = '").append(mCinValiDate).append("', ")
		   .append("Enddate = '").append(mCinValiDate).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where exists (select 1 from lcpol where polno = a.polno and grpcontno = '").append(mGrpContNo).append("') ");
    	map.put(sqlLCGrpCont.toString(), SysConst.UPDATE);
    	map.put(sqlLCGrpPol.toString(), SysConst.UPDATE);
    	map.put(sqlLCCont.toString(), SysConst.UPDATE);
    	map.put(sqlLCPol.toString(), SysConst.UPDATE);
    	map.put(sqlLCGet.toString(), SysConst.UPDATE);
    	map.put(sqlLCPrem.toString(), SysConst.UPDATE);
    	map.put(sqlLCDuty.toString(), SysConst.UPDATE);
    	return true;
    }
    
    private String getWorkBox()
    {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + mOperater + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if(tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null"))
        {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }
        return tWorkBox;
    }
    
    public static void main (String args[])
    {
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "zgm";
    	TransferData cInputData = new TransferData();
    	cInputData.setNameAndValue("tGtpContNo","00000009000005" );
    	cInputData.setNameAndValue("tCinvaliDate","2010-6-19");
    	SpecialModifyCinalidateBL bl = new SpecialModifyCinalidateBL();
    	VData tVData = new VData();  	
    	tVData.add(tGlobalInput);
    	tVData.add(cInputData);
    	bl.submitData(tVData, "zgm");
    }
}
