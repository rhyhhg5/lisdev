package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: 删除被保人清单中的数据</p>
 * <p>Description: 可以彻底删除也可以只设为无效状态 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class DeleteInsuredListBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = null;

    /** 数据操作类型 */
    private String mOperate = null;

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();

    /** 被保人清单 */
    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public DeleteInsuredListBL()
    {
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData())
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        System.out.println("Start DeleteInsuredListBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "DeleteInsuredListBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @return boolean
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCInsuredListSet = (LCInsuredListSet) mInputData.getObjectByObjectName(
                "LCInsuredListSet", 0);

        return true;
    }


    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //设置为无效
        if (mOperate.equals("INVALID"))
        {
            setInvalid();
        }
        //删除数据
        if (mOperate.equals("DELETE"))
        {
            delete();
        }

        return true;
    }

    /**
     * 把被包人清单中的数据设为无效
     */
    private void setInvalid()
    {
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.get(i);
            String sql = "update LCInsuredList " +
                    "set State = '2', " +
                    "Operator = '" + mGlobalInput.Operator + "', " +
                    "ModifyDate = '" + mCurrentDate + "', " +
                    "ModifyTime = '" + mCurrentTime + "' " +
                    "where GrpContNo = '" + tLCInsuredListSchema.getGrpContNo() + "' " +
                    "and InsuredId = '" + tLCInsuredListSchema.getInsuredID() + "' ";
            mMap.put(sql, "UPDATE");
        }
    }

    /**
     * 删除导入清单中的被保人数据
     */
    private void delete()
    {
        for (int i = 1; i <= mLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = mLCInsuredListSet.get(i);
            String sql = "delete from LCInsuredList " +
                    "where GrpContNo = '" + tLCInsuredListSchema.getGrpContNo() +"' " +
                    " and EdorNo = '" + tLCInsuredListSchema.getEdorNo() + "' " +
                    " and Insuredname = '" + tLCInsuredListSchema.getInsuredName() +"'" +
                    " and idno = '"+ tLCInsuredListSchema.getIDNo() + "' ";
            mMap.put(sql, "DELETE");
        }
    }

    /**
     * 根据业务逻辑对数据进行处理
     */
    private void prepareOutputData()
    {
        mInputData.add(mMap);
    }
}
