package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.1
 */
public class GEdorMJReturnCalBL
{
    public CErrors mErrors = new CErrors();
    private String mEdorNo = null;
    private String mEdorType = null;
    private String mGrpPolNo = null;
    private GlobalInput mGlobalInput = null;
    private MMap map = new MMap();

    public GEdorMJReturnCalBL(String edorNo, String edorType, String grpPolNo)
    {
        mEdorNo = edorNo;
        mEdorType = edorType;
        mGrpPolNo = grpPolNo;
    }

    /**
     * 公共的提交方法
     * @return boolean
     */
    public boolean submitData()
    {
        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        if(this.mEdorNo == null || this.mEdorType == null
            || this.mGrpPolNo == null)
         {
             mErrors.addOneError("传入的数据不完整。");
             return false;
         }

         EdorItemSpecialData eSpecialData
             = new EdorItemSpecialData(this.mEdorNo, this.mEdorType);
         if(!eSpecialData.query())
         {
             mErrors.addOneError("满期处理数据有误，不能进行本操作，请重新进行满期理算。");
             return false;
         }
         LPEdorEspecialDataSet set = eSpecialData.getSpecialDataSet();
         for (int i = 1; i <= set.size(); i++)
         {
             if (!set.get(i).getDetailType().equals(BQ.DETAILTYPE_MJSTATE))
             {
                 continue;
             }
             if (set.get(i).getEdorValue().equals(BQ.MJSTATE_FINISH))
             {
                 mErrors.addOneError("满期结算任务已结案，不能再进行本操作。");
                 return false;
             }
         }

         return true;
    }

    private boolean dealData()
    {
        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(this.mEdorNo, this.mEdorType);

        if(!tEdorItemSpecialData.query())
        {
            mErrors.addOneError("准备数据出错。");
            return false;
        }
        LPEdorEspecialDataSet tLPEdorEspecialDataSet
            = tEdorItemSpecialData.getSpecialDataSet();
        map.put(tLPEdorEspecialDataSet, "DELETE");
        for(int i = 1; i <= tLPEdorEspecialDataSet.size(); i++)
        {
            if(tLPEdorEspecialDataSet.get(i).getDetailType()
               .equals(BQ.DETAILTYPE_MJSTATE))
            {
                LPEdorEspecialDataSchema schema
                    = tLPEdorEspecialDataSet.get(i).getSchema();
                schema.setEdorValue(BQ.MJSTATE_INIT);
                map.put(schema, "INSERT");
            }
            if(tLPEdorEspecialDataSet.get(i).getDetailType()
               .equals(BQ.DETAILTYPE_ACCRATE))
            {
                map.put(tLPEdorEspecialDataSet.get(i).getSchema(), "INSERT");
            }
            if(tLPEdorEspecialDataSet.get(i).getDetailType()
               .equals(BQ.DETAILTYPE_ENDTIME_RATETYPE))
            {
                map.put(tLPEdorEspecialDataSet.get(i).getSchema(), "INSERT");
            }
        }

        String sql = "delete from LJSGetEndorse "
                     + "where EndorsementNo = '" + mEdorNo + "' "
                     + "   and FeeOperationType = '" + mEdorType + "' "
                     + "   and GrpPolNo = '" + mGrpPolNo + "' ";
        map.put(sql, SysConst.DELETE);

        return true;
    }

    public static void main(String[] args)
    {
        GEdorMJReturnCalBL gedormjreturncalbl = new GEdorMJReturnCalBL("", "", "");
    }
}
