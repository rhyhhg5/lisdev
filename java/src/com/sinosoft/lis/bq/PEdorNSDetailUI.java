package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 新保加人保全项目</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * ReWrite ZhangRong
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;

public class PEdorNSDetailUI
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public PEdorNSDetailUI()
    {
    }

    /**
     * 数据提交的公共方法，进行新增险种明细录入逻辑处理并提交数据库，
     * 提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象：包括：
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LPPolSet：新增的险种信息，需要PolNo，InsuredNo，RiskCode，CValiDate即可。
     * @param cOperate 数据操作字符串，可为空""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---GEdorNSDetailBL BEGIN---");
        PEdorNSDetailBL tPEdorNSDetailBL = new PEdorNSDetailBL();

        if(tPEdorNSDetailBL.submitData(cInputData, cOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPEdorNSDetailBL.mErrors);
            return false;
        }
        System.out.println("---GEdorNSDetailUI BL END---");

        return true;
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("20060926000002");
        tLPEdorItemSchema.setContNo("00002581101");
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_NS);

        LPEdorEspecialDataSet mLPEdorEspecialDataSet= new LPEdorEspecialDataSet();
        String[] pols = {"11000094815", "11000094817", "11000094826"};

        for(int i = 0; i < pols.length; i++)
        {
            LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new
                LPEdorEspecialDataSchema();
            tLPEdorEspecialDataSchema.setEdorAcceptNo(tLPEdorItemSchema.getEdorNo());
            tLPEdorEspecialDataSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPEdorEspecialDataSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_CVALIDATETYPE);
            tLPEdorEspecialDataSchema.setEdorValue("1");
            tLPEdorEspecialDataSchema.setPolNo(pols[i]);
            mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
        }
        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemSchema);
        tVData.add(mLPEdorEspecialDataSet);

        PEdorNSDetailUI tGrpEdorNSDetailUI = new PEdorNSDetailUI();
        if(!tGrpEdorNSDetailUI.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorNSDetailUI.mErrors.getErrContent());
        }
    }
}
