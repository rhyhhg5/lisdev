package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class GrpEdorTYConfirmBL implements EdorConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateEdorData())
        {
            return false;
        }
        updatePrem();
        setLBDiskImport();
        return true;
    }

    /**
     * 使保全数据生效
     * @return boolean
     */
    private boolean validateEdorData()
    {
/*
        String[] tables = {"LCInsureAcc"};
        ValidateEdorData validate = new ValidateEdorData(tables,
                mEdorNo, mEdorType);
        if (!validate.submitData())
        {
            mErrors.copyAllErrors(validate.mErrors);
            return false;
        }
        mMap.add(validate.getMap());
        return true;
 */
//    	将保全生效日期和追加保费进账户时间都修改为进账户次日,即使整体确认出错此处也可重复进行提交 20120503
    	MMap map = new MMap();
    	map.put("update LPInsureAccTrace set paydate=current date + 1 day where edorno='"+mEdorNo+"' and edortype='TY'", "UPDATE");
    	map.put("update LPInsureAccFeeTrace set paydate=current date + 1 day where edorno='"+mEdorNo+"' and edortype='TY'", "UPDATE");
    	map.put("update lpgrpedoritem set edorvalidate=current date + 1 day where edorno='"+mEdorNo+"' ", "UPDATE");
    	map.put("update lpgrpedormain set edorvalidate=current date + 1 day where edorno='"+mEdorNo+"' ", "UPDATE");
//    	map.put("update LpInsureAccClassFee a " +
//                "set Fee =(select sum(Fee) from LpInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
//                + "where edorno = '" +mEdorNo+ "' ", SysConst.UPDATE);
        VData tdate = new VData();
        tdate.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(tPubSubmit.submitData(tdate, "") == false)
        {
            // @@错误处理
			System.out.println("GrpEdorTYConfirmBL+validateEdorData++--");
			CError tError = new CError();
			tError.moduleName = "GrpEdorTYConfirmBL";
			tError.functionName = "validateEdorData";
			tError.errorMessage = "处理生效日期出错!";
			mErrors.addOneError(tError);
			return false;
        }
    	
		ValidateEdorData2 validate = new ValidateEdorData2(mGlobalInput, mEdorNo,
		        mEdorType, mGrpContNo, "GrpContNo");
		String[] addTables = {"LCInsureAccTrace","LCInsureAccFeeTrace"};
//		String[] addTables = {"LCInsureAccTrace", "LCInsureAccFeeTrace"};
		validate.addData(addTables);
		String[] chgTables = {"LCInsureAcc", "LCInsureAccFee","LCInsureAccClass", "LCInsureAccClassFee",};
		validate.changeData(chgTables);
		mMap.add(validate.getMap());
		return true;
    }

    private void updatePrem()
    {
        String sql = "update LCPol set Prem = Prem + " +
                " (select sum(GetMoney) from LJSGetEndorse " +
                "  where EndorseMentNo = '" + mEdorNo + "' " +
                "  and FeeOperationType = '" + mEdorType + "' " +
                "  and GrpContNo = '" + mGrpContNo + "' " +
                "  and PolNo = LCPol.PolNo) " +
                "where GrpContNo = '" + mGrpContNo + "' " +
                "and PolNo in (select PolNo from LJSGetEndorse " +
                "    where EndorseMentNo = '" + mEdorNo + "' " +
                "    and FeeOperationType = '" + mEdorType + "' " +
                "    and GrpContNo = '" + mGrpContNo + "') ";
        mMap.put(sql, "UPDATE");

        sql = "update LCCont set Prem = " +
                " (select sum(Prem) from LCPol " +
                "  where ContNo = LCCont.ContNo) " +
                "where GrpContNo = '" + mGrpContNo + "' ";
        mMap.put(sql, "UPDATE");

        sql = "update LCGrpPol set Prem = " +
                " (select sum(Prem) from LCPol " +
                "  where GrpPolNo = LCGrpPol.GrpPolNo) " +
                "where GrpContNo = '" + mGrpContNo + "' ";
        mMap.put(sql, "UPDATE");

        sql = "update LCGrpCont set Prem = " +
                " (select sum(Prem) from LCPol " +
                "  where GrpContNo = LCGrpCont.GrpContNo) " +
                "where GrpContNo = '" + mGrpContNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 把LPDiskImport的数据转到LBDiskImport
     * @return boolean
     */
    private void setLBDiskImport()
    {
        String sql = "insert into LBDiskImport " +
                "(select * from LPDiskImport " +
                " where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "') ";
        mMap.put(sql, "INSERT");
        sql = "delete from LPDiskImport " +
                " where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "DELETE");
    }
}
