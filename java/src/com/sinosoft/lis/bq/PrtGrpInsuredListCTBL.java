package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PrtGrpInsuredListCTBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private LPEdorAppSchema mLPEdorAppSchema = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorAcceptNo = null;

    public PrtGrpInsuredListCTBL(String edorAcceptNo)
    {
        mEdorAcceptNo = edorAcceptNo;
    }

    public boolean submitData(VData inputData, String operator)
    {
        if(!getInputData(inputData))
        {
            return false;
        }

        mLPGrpEdorItemSchema = getLPGrpEdorItemInfo(mEdorAcceptNo);
        mLPEdorAppSchema = getLPEdorAppInfo();

        if(!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到项目信息
     * @param edorAcceptNo String
     * @return LPGrpEdorItemSchema
     */
    private LPGrpEdorItemSchema getLPGrpEdorItemInfo(String edorAcceptNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(edorAcceptNo);
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_CT);
        return tLPGrpEdorItemDB.query().get(1);
    }

    private LPEdorAppSchema getLPEdorAppInfo()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        return tLPEdorAppDB.query().get(1);
    }

    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        if(mGlobalInput == null)
        {
            mErrors.addOneError("请传入用户信息。");
            return false;
        }

        return true;
    }

    private boolean getPrintData()
    {
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtGrpInsuredListCT.vts", "printer"); //最好紧接着就初始化xml文档

        TextTag tag = new TextTag(); //得到公司名
        String grpName = getCompanyName();
        if (grpName == null)
        {
            return false;
        }
        tag.add("GrpName", "参保团体名称：" + grpName + "。");
        tag.add("EdorName", "变更类型：团险解约。");
        tag.add("EdorNo", "本清单为受理号为" + mEdorAcceptNo + "批单的附件。");
        xmlexport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }

        String[] title =
                         {"序号", "姓名", "客户号", "性别", "出生日期",
                         "证件类型", "证件号码", "保险计划", "期交保费", "理赔银行",
                         "帐号", "户名", "本次应退保费", "状态", "备注"};
        xmlexport.addListTable(tListTable, title);
        xmlexport.outputDocumentToFile("C:\\", "PrtGrpInsuredListCTBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;

    }

    /**
     * 查询公司名称
     */
    private String getCompanyName()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorAcceptNo);
        if(!tLGWorkDB.getInfo())
        {
            mErrors.addOneError("没有查询到公司信息。");
            return null;
        }

        return tLGWorkDB.getCustomerName();
    }

    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();

        //通过LPEdorMain得到保单号，然后得到公司名称
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mEdorAcceptNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();

        for(int i = 1; i <= tLPGrpEdorMainSet.size(); i++)
        {
            LPInsuredDB tLPInsuredDB = new LPInsuredDB();
            tLPInsuredDB.setEdorNo(mEdorAcceptNo);
            tLPInsuredDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
            LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();
            for(int t = 1; t <= tLPInsuredSet.size(); t++)
            {
                LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(t);
                String[] info = new String[15];
                info[0] = "" + t;
                info[1] = StrTool.cTrim(tLPInsuredSchema.getName());
                info[2] = tLPInsuredSchema.getInsuredNo();
                info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName("Sex",
                    tLPInsuredSchema.getSex()));
                info[4] = CommonBL.decodeDate(
                    tLPInsuredSchema.getBirthday());
                info[5] = StrTool.cTrim(ChangeCodeBL.getCodeName("IDType",
                    tLPInsuredSchema.getIDType()));
                info[6] = StrTool.cTrim(tLPInsuredSchema.getIDNo());
            }
        }

        return null;
    }
}

















