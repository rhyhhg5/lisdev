package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 维护合同表中的人数字段，主要用在增人之后 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft< /p>
 * @author QiuYang
 * @version 1.0
 */

public class UpdatePeoplesBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mGrpContNo = null;

    private MMap map = new MMap();
    
    private MMap map2 = new MMap();
    
    private MMap mapAll = new MMap();

    /**
     * 构造函数
     * @param grpContNo String
     */
    public UpdatePeoplesBL(String grpContNo)
    {
        this.mGrpContNo = grpContNo;
    }

    /**
     * 执行操作
     * @return boolean
     */
    public boolean submitData()
    {
        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到MMap
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!dealData())
        {
            return null;
        }
        
        mapAll.add(map);
        mapAll.add(map2);
        
        return mapAll;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        boolean isNoNameCont = CommonBL.isNoNameGrpCont(mGrpContNo);

        if(isNoNameCont)
        {
            if(!updatePeoplesNoNameGrpCont())
            {
                return false;
            }
        }
        else
        {
            if (!updatePeoples())
            {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean updatePeoplesNoNameGrpCont()
    {
        System.out.println("无名单");
        //更新lccont保单人数
        String contpeoples2 = "  (select sum(peoples) "
                          + "from LCCont "
                          + "where polType = '1' "
                          + "   and grpContNo = '" + mGrpContNo + "') ";
        
//      更新lcpol保单人数
        String polpeoples2 = "  (select sum(InsuredPeoples) "
                          + " from LCpol "
                          + " where POLTYPEFLAG = '1' "
                          + "       and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
                          + "   and grpContNo = '" + mGrpContNo + "' and riskcode=a.riskcode ) ";
        
        //更新保单人数
        String sqlcontsum = "  update LCGrpCont "
                     + "set peoples2 = " + contpeoples2
                     + " where grpContNo = '" + mGrpContNo + "' ";
        
        System.out.println(sqlcontsum);
    
       
        
//      更新保单人数
        String sqlpolsum = "  update LCGrpPol a "
                     + "set peoples2 = " + polpeoples2
                     + " where grpContNo = '" + mGrpContNo + "' ";
        
        System.out.println(sqlpolsum);
        System.out.println("*************************************************");
           
        //更新保障计划人数
       String sqlXXX = "  update LCContPlan a "
              + "set a.Peoples2 = "
              + "      (   select  sum(InsuredPeoples)   "
              + "          from LCPol "
              + "          where GrpContNo = '" + mGrpContNo + "' "
              + "          and PolTypeFlag= '1' "
              + "          and contPlanCode = a.contPlanCode "
              + "          and AppFlag = '" + BQ.APPFLAG_SIGN + "') " +
              		"/" 
              	   + "      (   select  count(1)   "
                   + "          from LCPol "
                   + "          where GrpContNo = '" + mGrpContNo + "' "
                   + "          and PolTypeFlag= '1' "
                   + "          and contPlanCode = a.contPlanCode "
                   + "          and AppFlag = '" + BQ.APPFLAG_SIGN + "') " 
              + "where a.grpContNo = '" + mGrpContNo + "' ";
        
        System.out.println(sqlXXX);
        
        String sqlOnexxx = "  update LCContPlan a "
            + "set a.Peoples2 = 0 "
            + "where a.grpContNo = '" + mGrpContNo + "' " +
            		" and contPlanCode = '11' ";
        System.out.println(sqlOnexxx);
        
        map.put(sqlpolsum, "UPDATE");
        map.put(sqlcontsum, "UPDATE");
        
        map2.put(sqlOnexxx, "UPDATE");
        map2.put(sqlXXX, "UPDATE");
     
        
        return true;
    }

    /**
     * 维护LCGrpCont表中的被保人数
     */
    private boolean updatePeoples()
    {
        System.out.println("非无名单");

        //更新lccont保单人数
        String contpeoples2 = "  (select sum(peoples) "
                          + "from LCCont "
                          + "where polType = '0' "
                          + "   and grpContNo = '" + mGrpContNo + "') ";
        
//      更新lcpol保单人数
        String polpeoples2 = "  (select sum(InsuredPeoples) "
                          + "from LCpol "
                          + "where POLTYPEFLAG = '0' "
                          + "       and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
                          + "   and grpContNo = '" + mGrpContNo + "' and riskcode=a.riskcode ) ";
             
//      更新保单人数
        String sqlpolsum = "  update LCGrpPol a "
                     + "set peoples2 = " + polpeoples2
                     + " where grpContNo = '" + mGrpContNo + "' ";
        map.put(sqlpolsum, "UPDATE");
        
        String onWorkPeoples = "(select count(1) from LCInsured a, LCCont b " +
                "where a.ContNo = b.ContNo " +
                "and a.GrpContNo = '" + mGrpContNo + "' " +
                "and a.InsuredStat = '1' " +
                "and b.AppFlag = '" + BQ.APPFLAG_SIGN + "') ";
        String offWorkPeoples = "(select count(1) from LCInsured a, LCCont b " +
                "where a.ContNo = b.ContNo " +
                "and a.GrpContNo = '" + mGrpContNo + "' " +
                "and a.InsuredStat = '2' " +
                "and b.AppFlag = '" + BQ.APPFLAG_SIGN + "') ";
        String sql = "update LCGrpCont set Peoples2 = " + contpeoples2 + ", " +
                "OnWorkPeoples = " + onWorkPeoples + ", " +
                "OffWorkPeoples = " + offWorkPeoples + ", " +
                "OtherPeoples = " + contpeoples2 + "-" + onWorkPeoples + "-" + offWorkPeoples + " " +
                "where GrpContNo = '" + mGrpContNo + "'";
        
        
        map.put(sql, "UPDATE");
        System.out.println(sql);
        sql = "  update LCContPlan a "
              + "set a.Peoples2 = "
              + "    (select count(distinct InsuredNo) from LCPol "
              + "    where GrpContNo = '" + mGrpContNo + "' "
              + "       and PolTypeFlag = '" + BQ.POLTYPEFLAG_INSURED + "' "
              + "       and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
              + "       and contPlanCode = a.contPlanCode) "
              + "where a.grpContNo = '" + mGrpContNo + "' ";
        map.put(sql, "UPDATE");
        System.out.println(sql);
        return true;
    }

    /**
      * 提交数据到数据库
      * @param map MMap
      * @return boolean
      */
     private boolean submit()
     {
         VData data = new VData();
         data.add(map);
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(data, ""))
         {
             mErrors.copyAllErrors(tPubSubmit.mErrors);
             return false;
         }
         return true;
     }


     /**
      * 主函数，测试用
      * @param args String[]
      */
     public static void main(String[] args)
     {
         UpdatePeoplesBL bl = new UpdatePeoplesBL("00023094000001");
         bl.submitData();
     }

}
