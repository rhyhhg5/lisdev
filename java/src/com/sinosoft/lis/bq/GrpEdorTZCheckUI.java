package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QIuYang
 * @version 1.0
 */

public class GrpEdorTZCheckUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private GrpEdorTZCheckBL mGrpEdorTZCheckBL = new GrpEdorTZCheckBL();

    public GrpEdorTZCheckUI()
    {}

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!mGrpEdorTZCheckBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(mGrpEdorTZCheckBL.mErrors);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mGrpEdorTZCheckBL.getResult();
    }

    public static void main(String[] args)
    {
        String sql = "select * from LJAPayGrp a " +
                "where PayCount = (select max(PayCount) from LJAPayGrp " +
                "  where GrpPolNo = a.GrpPolNo and PayType = a.PayType) " +
                "and GrpPolNo = '2200000250' " +
                "and PayType = 'ZC'";

        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(sql);
        LJAPayGrpSchema tLJAPayGrpSchema = tLJAPayGrpSet.get(1).getSchema();
        System.out.print(sql);
        System.out.print(tLJAPayGrpSchema.getLastPayToDate());
        System.out.print(tLJAPayGrpSchema.getCurPayToDate());
/*
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";

        //输入参数
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add("0000005905");

        GrpEdorTZCheckUI tGrpEdorTZCheckUI = new GrpEdorTZCheckUI();
        if (!tGrpEdorTZCheckUI.submitData(tVData, ""))
        {
            VData result = tGrpEdorTZCheckUI.getResult();
            System.out.println((String)result.get(0));
        }
 */
    }
}
