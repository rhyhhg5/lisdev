package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 2.0
 */
public class PGrpEdorAppConfirmBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    private LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();

    public PGrpEdorAppConfirmBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())return false;

        //数据准备操作,校验该团体保全申请的明细信息是否完整
        if (!prepareData())return false;

        VData pInputData = new VData();
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
        tLPEdorMainDB.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
        mLPEdorMainSet = tLPEdorMainDB.query();
        if (tLPEdorMainDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询个人保全失败！");
        }

        for (int i = 1; i <= mLPEdorMainSet.size(); i++)
        {
            if (BQ.EDORSTATE_INIT.equals(mLPEdorMainSet.get(i).getEdorState()))
            {
                CError.buildErr(this,
                        "受理号为" + mLPEdorMainSet.get(i).getEdorNo()
                        + "的保全没有录入！");
                return false;
            }
        }

        for (int i = 1; i <= mLPEdorMainSet.size(); i++)
        {
            System.out.println("处理个人保全主表" + i+mLPEdorMainSet.get(i).getContNo());
            pInputData.clear();
            pInputData.addElement(mGlobalInput);
            pInputData.addElement(mLPGrpEdorMainSchema);
            pInputData.addElement(mLPEdorMainSet.get(i));
            EdorAppConfirmBL tEdorAppConfirmBL = new EdorAppConfirmBL();
            if (!tEdorAppConfirmBL.submitData(pInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tEdorAppConfirmBL.mErrors);
                return false;
            }
        }

        //处理团单
        GEdorAppConfirmBL tGEdorAppConfirmBL = new GEdorAppConfirmBL();
        pInputData.clear();
        pInputData.addElement(mGlobalInput);
        pInputData.addElement(mLPGrpEdorMainSchema);

        if (!tGEdorAppConfirmBL.submitData(pInputData,
                "INSERT||EDORAPPCONFIRM"))
        {
            this.mErrors.copyAllErrors(tGEdorAppConfirmBL.mErrors);
            return false;
        }
        
        
        
//		增加冲抵增减人时总金额小于0.1元的金额 2008-12-9 xiepan
    	GrpNIZTMoneyCheckBL tGrpNIZTMoneyCheckBL = new GrpNIZTMoneyCheckBL();
    	tGrpNIZTMoneyCheckBL.checkMoney(mLPGrpEdorMainSchema.getEdorNo());

        //生成批单
        if(!createPrt())
        {
            return false;
        }

        return true;
    }

    /**
     * 生成批单
     * @return boolean
     */
    private boolean createPrt()
    {
        LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
        tLPEdorAppSchema.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorAcceptNo());

        //生成保全批单
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        PrtGrpEndorsementBL tPrtGrpEndorsementBL = new PrtGrpEndorsementBL(
            tLPEdorAppSchema.getEdorAcceptNo());
        if(!tPrtGrpEndorsementBL.submitData(tVData, ""))
        {
            String errMsg = "保全理算成功。但生成理算结果预览失败";
            mErrors.addOneError(errMsg + "！原因是："
                      + tPrtGrpEndorsementBL.mErrors.getLastError());
            System.out.println(errMsg);
            return false;
        }
        return true;
    }

    /**
     * 得到Main表中的数据，Main表中一条记录对应一个ContNo
     * @return LPEdorMainSet
     */
    private LPGrpEdorMainSet getEdorMains()
    {
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mEdorNo);
        return tLPGrpEdorMainDB.query();
    }

    /**
     * 得到保全项目
     * @return LPEdorItemSet
     */
    private LPGrpEdorItemSet getEdorItems()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo(mEdorNo);
        return tLPGrpEdorItemDB.query();
    }


    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData()
    {
        try
        {
            //得到保单号（避免依靠前台）
            mLPGrpEdorMainSchema = (LPGrpEdorMainSchema) mInputData.
                    getObjectByObjectName("LPGrpEdorMainSchema",
                    0);
            if(mLPGrpEdorMainSchema==null)
            {
            	CError.buildErr(this, "接收保全主表数据失败");
            	System.out.println("接收保全主表数据失败,mLPGrpEdorMainSchema为null，PGrpEdorAppConfirmBL-L194");
                return false;
            }
            String tEdorNo = null;
            tEdorNo = mLPGrpEdorMainSchema.getEdorAcceptNo();
            if(tEdorNo==null || "".equals(tEdorNo) || "null".equals(tEdorNo))
            {
            	CError.buildErr(this, "接收保全主表数据失败");
            	System.out.println("接收保全主表数据失败,mLPGrpEdorMainSchema的EdorAcceptNo为"+tEdorNo+"，PGrpEdorAppConfirmBL-L203");
                return false;
            }
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 校验该团体保全申请的明细信息是否完整
     * @param aLPGrpEdorMainSchema
     * @return
     */
    private boolean checkData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        VerifyEndorsement tVer = new VerifyEndorsement();
        VData tVData = new VData();
        tVData.addElement(aLPGrpEdorItemSchema);

        if (!tVer.verifyEdorDetail(tVData, "VERIFY||GRPDETAIL"))
        {
            this.mErrors.copyAllErrors(tVer.mErrors);
            return false;
        }
        else
        {
            if (tVer.getResult() != null && tVer.getResult().size() > 0)
            {
                String tErr = "";
                for (int i = 0; i < tVer.getResult().size(); i++)
                {
                    tErr = tErr + (String) tVer.getResult().get(i);
                }

                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "VerifyEndorsement";
                tError.functionName = "verifyEdorDetail";
                tError.errorMessage = tErr;
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        
        if(!checkZTAndNI(aLPGrpEdorItemSchema)){
        	return false; 
        }
        
        return true;
    }
    
    
    private boolean checkZTAndNI(LPGrpEdorItemSchema aLPGrpEdorItemSchema){
    	if(aLPGrpEdorItemSchema.getEdorType().equals("ZT")){
    		for (int j = 1;j <= tLPGrpEdorItemSet.size(); j++) {
    			LPGrpEdorItemSchema bLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(j);
    			if(bLPGrpEdorItemSchema.getEdorType().equals("NI")){
    		        String checkSql = "select c.name  from lpedoritem e, lcinsured c where e.edorno = '"+bLPGrpEdorItemSchema.getEdorNo()+"'   and e.edortype = 'ZT'   and c.contno = e.contno   and c.relationtomaininsured = '00' and exists (select 1 from lpdiskimport m  where m.edorno = e.edorno and m.insuredno = c.insuredno)   and exists (select 1 from ldperson per, lcinsuredlist lci where grpcontno = '"+bLPGrpEdorItemSchema.getGrpContNo()+"' and lci.edorno = e.edorno and lower(contplancode) = 'fm' and per.customerno = c.insuredno and per.name = lci.insuredname and per.sex = lci.sex and per.birthday = lci.birthday and per.idtype = lci.idtype and per.idno = lci.idno ) with ur";
    		        String checkSqlresult = new ExeSQL().getOneValue(checkSql);
    		        
    		        if(checkSqlresult!=null && !checkSqlresult.equals("")){
    		        		String tErr = "同时增人减人有误，主被保人："+checkSqlresult+" 既存在于减人清单，也存在于增人清单，请查看！";
    		        	 	CError tError = new CError();
    		                tError.moduleName = "增减人模板";
    		                tError.functionName = "checkZTAndNI";
    		                tError.errorMessage = tErr;
    		                this.mErrors.addOneError(tError);
    		                return false;
    		        }
    		        
    			}
			}
    	}
    	return true;
    }
    

    /**
     * 准备需要保存的数据
     * @return
     */
    private boolean prepareData()
    {
        //查询团体保全主表中，该保单处于申请状态的保全数据
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        String strSql = "select * from LPGrpEdorItem "
                + " where  GrpContNo='" +
                mLPGrpEdorMainSchema.getGrpContNo()
                + "' and EdorNo='" + mLPGrpEdorMainSchema.getEdorNo()
                + "' and ManageCom like '" + mGlobalInput.ManageCom
                + "%' order by EdorNo, MakeDate, MakeTime"; //去掉按EdorValiDate排序
         tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(
                strSql);
        if (tLPGrpEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this,
                    "查询批单号为" + mLPGrpEdorMainSchema.getEdorNo()
                    + "的保全项目时失败！");
            return false;
        }
        //判断所有项目是否已经录入,如果未录入,退出重新录入.
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
        {
            //"3"未录入,"1"录入完成 只有录入完成后才能进行理算.
            if ("3".equals(tLPGrpEdorItemSet.get(i).getEdorState()))
            {
                CError.buildErr(this,
                        "受理号为" + mLPGrpEdorMainSchema.getEdorNo()
                        + "的保全下" + tLPGrpEdorItemSet.get(i).getEdorType() +
                        "项目未录入，请录入后再进行保全理算。");
                return false;
            }
        }

        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
        {
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(i);
            //校验该团体保全申请的明细信息是否完整
            if (!checkData(tLPGrpEdorItemSchema))
                return false;
        }
        return true;
    }
}
