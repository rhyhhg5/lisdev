package com.sinosoft.lis.bq;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCAscriptionRuleFactoryDB;
import com.sinosoft.lis.db.LCAscriptionRuleParamsDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpBalPlanDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpFeeDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUrgeVerifyLogDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LMEdorItemDB;
import com.sinosoft.lis.db.LMPolDutyEdorCalDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.lis.db.LPDiskImportDB;
import com.sinosoft.lis.db.LPEdorAppDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.operfee.FeeConst;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAscriptionRuleFactorySchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpBalPlanSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LMEdorItemSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCAscriptionRuleFactorySet;
import com.sinosoft.lis.vschema.LCAscriptionRuleParamsSet;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCGrpBalPlanSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUrgeVerifyLogSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMPolDutyEdorCalSet;
import com.sinosoft.lis.vschema.LMRiskInsuAccSet;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorMainSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: 保全公共类</p>
 * <p>Description: 保全公共类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class CommonBL
{
    private static ExeSQL mExeSQL = new ExeSQL();

    private CommonBL()
    {
    }

    /**
     * 把型如"2005-01-01"格式的日期转为"2005年01月01日"格式
     * @param date String
     * @return String
     */
    static public String decodeDate(String date)
    {
        if ((date == null) || (date.equals("")))
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        String data[] = date.split("-");
        if (data.length != 3)
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        return (data[0] + "年" + data[1] + "月" + data[2] + "日");
    }

    /**
     * 把性别转换为成为称谓
     * @param sex String
     * @return String
     */
    static public String decodeSex(String sex)
    {
        if (sex == null)
        {
            System.out.println("性别编码不正确！");
            return "";
        }

        String retSex = sex;
        if (sex.equals("0"))
        {
            retSex = "先生";
        }
        else if (sex.equals("1"))
        {
            retSex = "女士";
        }
        else
        {
            retSex = "先生/女士";
        }
        return retSex;
    }

    /**
     * 状态转换
     * @param state String
     * @return String
     */
    static public String decodeState(String state)
    {
        if ((state == null) || (state.equals(BQ.IMPORTSTATE_FAIL)))
        {
            return "无效";
        }
        return "有效";
    }

    /**
     * 按四舍五入保留两位小数
     * @param value double
     * @return double
     */
    static public double carry(double value)
    {
        BigDecimal b = new BigDecimal(String.valueOf(value));
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 按四舍五入保留两位小数，接受String类型的参数
     * @param value double
     * @return double
     */
    static public double carry(String value)
    {
        if ((value == null) || (value.equals("")))
        {
            return 0.0;
        }
        BigDecimal b = new BigDecimal(value);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 消除空值，批单显示专用
     * @param value String
     * @return String
     */
    static public String notNull(String value)
    {
        if ((value == null) || (value.trim().equals("")))
        {
            return "空白";
        }
        else
        {
            return value.trim();
        }
    }

    /**
     * 把String转为Date
     * @param dateString String
     * @return Date
     */
    static public Date stringToDate(String dateString)
    {
        if ((dateString == null) || (dateString.equals("")))
        {
            return null;
        }
        try
        {
            if (dateString.indexOf("-") != -1)
            {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                return df.parse(dateString);
            }
            else
            {
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                return df.parse(dateString);
            }
        }
        catch (ParseException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 对日期按天加减
     * @param oldDate Date
     * @param changeNum int
     * @return Date
     */
    static public Date changeDate(Date oldDate, int changeNum)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(oldDate);
        calendar.add(Calendar.DATE, changeNum);
        return calendar.getTime();
    }

    /**
     * 对日期按天加减
     * @param oldDate String
     * @param changeNum int
     * @return Date
     */
    static public Date changeDate(String oldDate, int changeNum)
    {
        return changeDate(stringToDate(oldDate), changeNum);
    }

    /**
     * 对日期按月加减
     * @param oldDate Date
     * @param changeNum int
     * @return Date
     */
    static public Date changeMonth(Date oldDate, int changeNum)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(oldDate);
        calendar.add(Calendar.MONTH, changeNum);
        return calendar.getTime();
    }

    /**
     * 对日期按月加减
     * @param oldDate String
     * @param changeNum int
     * @return Date
     */
    static public Date changeMonth(String oldDate, int changeNum)
    {
        return changeMonth(stringToDate(oldDate), changeNum);
    }

    /**
     * 对日期按年加减
     * @param oldDate Date
     * @param changeNum int
     * @return Date
     */
    static public Date changeYear(Date oldDate, int changeNum)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(oldDate);
        calendar.add(Calendar.YEAR, changeNum);
        return calendar.getTime();
    }

    /**
     * 对日期按年加减
     * @param oldDate String
     * @param changeNum int
     * @return Date
     */
    static public Date changeYear(String oldDate, int changeNum)
    {
        return changeYear(stringToDate(oldDate), changeNum);
    }

    /**
     * 通过投保人号得到投保人名
     * @param appntNo String
     * @return String
     */
    static public String getAppntName(String appntNo)
    {
        LDPersonDB db = new LDPersonDB();
        db.setCustomerNo(appntNo);
        if (!db.getInfo())
        {
            return "";
        }
        return db.getName();
    }

    /**
     * 通过被保人号得到被保人名
     * @param insuredNo String
     * @return String
     */
    public static String getInsuredName(String insuredNo)
    {
        return getAppntName(insuredNo);
    }

    /**
     * 查询险种名称
     * @param riskCode String
     * @return String
     */
    public static String getRiskName(String riskCode)
    {
        LMRiskDB db = new LMRiskDB();
        db.setRiskCode(riskCode);
        if (!db.getInfo())
        {
            return "";
        }
        return db.getRiskName();
    }

    /**
     * 得到用户名
     * @param userCode String
     * @return String
     */
    public static String getUserName(String userCode)
    {
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(userCode);
        if (!tLDUserDB.getInfo())
        {
            return null;
        }
        return tLDUserDB.getUserName();
    }

    /**
     * 根据账户类型编码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    public static String getInsuAccNo(String accType)
    {
        String sql = "select * from LMRiskInsuAcc " + "where AccType = '"
                + accType + "' " + "order by InsuAccNo ";
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        LMRiskInsuAccSet tLMRiskInsuAccSet = tLMRiskInsuAccDB.executeQuery(sql);
        if (tLMRiskInsuAccSet.size() == 0)
        {
            return null;
        }
        return tLMRiskInsuAccSet.get(1).getInsuAccNo();
    }

    /**
     * 根据账户类型和险种编码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    public static String getInsuAccNo(String accType, String riskCode)
    {
        String sql = "select distinct a.InsuAccNo from "
                + "LMRiskInsuAcc a, LMRiskToAcc b "
                + "where a.InsuAccNo = b.InsuAccNo " + "and a.AccType = '"
                + accType + "' " + "and b.riskCode = '" + riskCode + "' ";
        return (new ExeSQL()).getOneValue(sql);
    }

    /**
     * 判断团体保单下是否含有特需医疗或TPA的险种
     * @return boolean
     */
    public static boolean hasSpecialRisk(String grpContNo)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType = tLMRiskAppDB.getRiskType3();
            String riskType8 = tLMRiskAppDB.getRiskType8(); //TPA产品
            if ((riskType != null && riskType.equals(BQ.RISKTYPE3_SPECIAL)) 
            		|| (riskType8 != null && riskType8.equals(BQ.RISKTYPE8_TPA)))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断保单下是否含有万能险种
     * @return boolean
     */
    public static boolean hasULIRisk(String ContNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(ContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE1_ULI)))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 判断保单下是否含有附加万能险种
     * @return boolean
     */
    public static boolean hasSubULIRisk(String ContNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(ContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            String subriskflag = tLMRiskAppDB.getSubRiskFlag();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE1_ULI))&&(subriskflag != null) && (subriskflag.equals("S")))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 判断保单下是否有万能附加险
     * @param ContNo
     * @return
     */
    public static boolean hasAttachULIRisk(String ContNo){
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(ContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            
            String strSQL = "select * from lcpol where  contno='"+ContNo+"' and  riskcode in "
            			   +" (select lm.riskcode from lmriskapp lm,ldcode1 ld  where lm.risktype4='4' and  lm.riskcode=ld.code1 "
            			   +" and ld.CodeType='mainsubriskrela' and ld.Code='"+tLCPolSet.get(i).getRiskCode()+"') with ur ";
            SSRS tssrs = new SSRS();
            tssrs = new ExeSQL().execSQL(strSQL);
            
            if(tssrs != null&&tssrs.MaxRow>0){
            	return true;
            }
        }        
    	return false;
    }

    /**
     * 判断保单下是否含有分红险种
     * @return boolean
     */
    public static boolean hasBonusRisk(String ContNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(ContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE4_BONUS)))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 判断团体保单下是否含有非特需且非TPA的险种
     * @param grpContNo String
     * @return boolean
     */
    public static boolean hasNotSpecialRisk(String grpContNo)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType = tLMRiskAppDB.getRiskType3();
            String riskType8 = tLMRiskAppDB.getRiskType8(); //TPA产品
            if (riskType == null || !riskType.equals(BQ.RISKTYPE3_SPECIAL))
            {
            	if(riskType8==null || !riskType8.equals(BQ.RISKTYPE8_TPA))
            	{
            		return true;
            	}
            }
					            	
        }
        return false;
    }

    /**
     * 判断某险种是否为万能类险种
     * @return boolean
     */
    public static boolean isULIRisk(String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        tLMRiskAppDB.setRiskType4(BQ.RISKTYPE1_ULI);

        return tLMRiskAppDB.query().size() >= 1;
    }
    
    /**
     * 得到保单的犹豫期天数
     * @return
     */
    public static int getWTPeriod(String contNo)
    {
//    	int WTPeriod = 10;
//        LCContDB tLCContDB = new LCContDB();
//        LCContSchema tLCContSchema = new LCContSchema();
//        tLCContDB.setContNo(contNo);
//        if (!tLCContDB.getInfo())
//        {
//            return WTPeriod;
//        }
//        tLCContSchema = tLCContDB.getSchema();
//        if("04".equals(tLCContSchema.getSaleChnl())||"13".equals(tLCContSchema.getSaleChnl())){
//        	return 15;
//        }else{
//        	String  wtSQL = " select max(hesitateend) from lmedorwt  " +
//        			" where riskcode in (select riskcode from lcpol where contno='"+contNo+"') with ur ";
//        	String WTPeriod1 = new ExeSQL().getOneValue(wtSQL);
//        	if(WTPeriod1==null||"".equals(WTPeriod1)){
//        		return 10;
//        	}else{
//        		WTPeriod = Integer.parseInt(WTPeriod1);
//        	}
//        }
    	int WTPeriod =15;
    		
        return WTPeriod;
    }    
    
    
    
    /**
     * 通过账户和保全生效日期来获取团单的归属比例
     * @param tLCGrpAccClassSchema LCInsureAccClassSchema
     * @param tEdorValiDate String 
     * @return double
     */
    public static double getULIRate(LCInsureAccClassSchema tLCGrpAccClassSchema,String tEdorValiDate)
    {
    	double tRate=0;
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPolNo(tLCGrpAccClassSchema.getPolNo());
		if(!tLCPolDB.getInfo()){
			System.out.println("获取险种失败line-444");
			return -1;
		}
//		得到归属规则编码
		String arCode = StrTool.cTrim(tLCPolDB.getSchema().getAscriptionRuleCode());
    	
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setInsuredNo(tLCGrpAccClassSchema.getInsuredNo());
		tLCInsuredDB.setContNo(tLCGrpAccClassSchema.getContNo());
		if(!tLCInsuredDB.getInfo()){
			System.out.println("获取被保人失败line-454");
			return -1;
		}
		LCInsuredSchema tLCInsuredSchema =tLCInsuredDB.getSchema();
		String jcDate = tLCInsuredSchema.getJoinCompanyDate();
		System.out.println("客户:入司时间:"+jcDate);
    	String sql = "select * from LCAscriptionRuleFactory where GrpPolNo = '"
			+ tLCGrpAccClassSchema.getGrpPolNo()
			+ "' and OtherNo = '"
			+ tLCGrpAccClassSchema.getPayPlanCode()
			+ "' and AscriptionRuleCode = '" + arCode + "'";
    	System.out.println("规则查询SQL:"+sql);
    	LCAscriptionRuleFactoryDB tLCAscriptionRuleFactoryDB=new LCAscriptionRuleFactoryDB();
    	LCAscriptionRuleFactorySet tLCAscriptionRuleFactorySet = tLCAscriptionRuleFactoryDB.executeQuery(sql);
    	
    	//判断计算出了几个归属比例
    	int n=0;
    	// 计算归属比例
		for (int i = 1; i <= tLCAscriptionRuleFactorySet.size(); i++) {
			Calculator mCalculator = new Calculator();
			LCAscriptionRuleFactorySchema tLCAscriptionRuleFactorySchema = tLCAscriptionRuleFactorySet.get(i);
			//获取计算代码
			String tCalCode = tLCAscriptionRuleFactorySchema.getFactoryCode();
	        mCalculator.setCalCode(tCalCode);
	        
	        //获取计算名称
	        String tFactoryname=tLCAscriptionRuleFactorySchema.getFactoryName();
	        
			LCAscriptionRuleParamsDB tLCAscriptionRuleParamsDB = new LCAscriptionRuleParamsDB();
			tLCAscriptionRuleParamsDB.setGrpPolNo(tLCAscriptionRuleFactorySchema.getGrpPolNo());
			tLCAscriptionRuleParamsDB.setAscriptionRuleCode(tLCAscriptionRuleFactorySchema.getAscriptionRuleCode());
			tLCAscriptionRuleParamsDB.setOtherNo(tLCAscriptionRuleFactorySchema.getOtherNo());
			tLCAscriptionRuleParamsDB.setFactoryCode(tLCAscriptionRuleFactorySchema.getFactoryCode());
			tLCAscriptionRuleParamsDB.setFactoryType(tLCAscriptionRuleFactorySchema.getFactoryType());
			tLCAscriptionRuleParamsDB.setInerSerialNo(tLCAscriptionRuleFactorySchema.getInerSerialNo());
			tLCAscriptionRuleParamsDB.setFactorySubCode(tLCAscriptionRuleFactorySchema.getFactorySubCode());
			LCAscriptionRuleParamsSet tLCAscriptionRuleParamsSet = tLCAscriptionRuleParamsDB.query();

			for (int k = 1; k <= tLCAscriptionRuleParamsSet.size(); k++) {
				mCalculator.addBasicFactor(tLCAscriptionRuleParamsSet.get(k).getParamName(), tLCAscriptionRuleParamsSet.get(k).getParam());
			}

			if (tFactoryname.startsWith("ServiceYear")) {// 如果是服务年限
				if("".equals(StrTool.cTrim(jcDate))) {
					System.out.println("客户"+tLCInsuredSchema.getName()+":没有入司时间！");
					continue;
				}
				String tServiceYear = Integer.toString(PubFun
						.calInterval(jcDate, tEdorValiDate, "Y"));
				System.out.println("ServiceYear:" + tServiceYear);
				mCalculator.addBasicFactor("ServiceYear", tServiceYear);
			}
			if (tFactoryname.startsWith("Position")) {
				if("".equals(StrTool.cTrim(tLCInsuredDB.getPosition()))) {
					System.out.println("客户"+tLCInsuredSchema.getName()+":没有职位！");
					continue;
				}
				mCalculator.addBasicFactor("Position", tLCInsuredDB.getPosition());
			}
			if (tFactoryname.equals("ServiceYearAndPosition")) {
				if("".equals(StrTool.cTrim(jcDate))) {
					System.out.println("客户"+tLCInsuredSchema.getName()+":没有入司时间！");
					continue;					
				}
				if("".equals(StrTool.cTrim(tLCInsuredDB.getPosition()))) {
					System.out.println("客户"+tLCInsuredSchema.getName()+":没有职位！");
					continue;
				}
				String tServiceYear = Integer.toString(PubFun
						.calInterval(jcDate, tEdorValiDate, "Y"));
				System.out.println("ServiceYear:" + tServiceYear);
				String tPosition = tLCInsuredDB.getPosition();
				mCalculator.addBasicFactor("ServiceYear", tServiceYear);
				mCalculator.addBasicFactor("Position", tPosition);
			}
			
	        String tStr = "";
	        tStr = mCalculator.calculate();
	        System.out.println("---str" + tStr);
	        if (tStr == null || tStr.trim().equals(""))
	        {
	        	System.out.println(".........查询归属比例失败");
	            continue;
	        }
	        else
	        {
	        	System.out.println(".........查询归属比例完成");
	        	tRate=Double.parseDouble(tStr);
	        	n++;
	        }
		}
		
		if(!(n==1)){
        	System.out.println("查询归属比例出错,查出了"+n+"条归属比例");
        	return -1;
        }
        System.out.println("查询归属比例:"+tRate);
        return tRate;
    }

    /**
     * 得到交费期间，取LJAPayGrpSchema中的CurPayToDate和LastPayToDate
     * @param grpPolNo String
     * @return LJAPayGrpSchema
     */
    // modify by fuxin 2007-11-24 取得一个险种的最新交至日期 关联到lcgrppol
    public static LJAPayGrpSchema getPayToDate(String grpPolNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * from LJAPayGrp a,LCGRPpol b ").append(
                "where PayCount = ").append(
                "(select max(PayCount) from LJAPayGrp ").append(
                " where GrpPolNo = a.GrpPolNo ")
                .append(" and PayType = 'ZC') ").append(" and a.GrpPolNo = '")
                .append(grpPolNo).append("' ").append(
                        " and a.curpaytodate = b.paytodate ").append(
                        " and a.grppolno = b.grppolno ").append(
                        "and PayType = 'ZC'");
        System.out.println(sql.toString());
        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(sql.toString());
        if (tLJAPayGrpSet.size() == 0)
        {
            return null;
        }
        return tLJAPayGrpSet.get(1);
    }

    /**
     * 得到当前的交至日期
     * @param grpPolNo String
     * @return String
     */
    public static String getLastPayToDate(String grpPolNo)
    {
        LJAPayGrpSchema tLJAPayGrpSchema = getPayToDate(grpPolNo);
        if (tLJAPayGrpSchema == null)
        {
            return null;
        }
        return tLJAPayGrpSchema.getLastPayToDate();
    }

    /**
     * 得到交费起始日期
     * @param aLPPolSchema LPPolSchema
     * @return String
     */
    public static String getLastPayToDate(String polNo, String payToDate)
    {
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPolNo(polNo);
        tLJAPayPersonDB.setCurPayToDate(payToDate);
        tLJAPayPersonDB.setPayType("ZC");
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if (tLJAPayPersonSet.size() == 0)
        {
            return null;
        }
        return tLJAPayPersonSet.get(1).getLastPayToDate();
    }

    /**
     * 得到计算方向
     * @param grpContNo String
     * @param contPlanCode String
     * @param riskCode String
     * @return String
     */
    public static String getCalRule(String grpContNo, String contPlanCode,
            String riskCode)
    {
        if (contPlanCode == null)
        {
            return null;
        }
        LCContPlanDutyParamDB tContPlanDB = new LCContPlanDutyParamDB();
        tContPlanDB.setGrpContNo(grpContNo);
        tContPlanDB.setContPlanCode(contPlanCode);
        tContPlanDB.setRiskCode(riskCode);
        tContPlanDB.setCalFactor("CalRule");
        LCContPlanDutyParamSet tContPlanSet = tContPlanDB.query();
        if (tContPlanSet.size() == 0)
        {
            return null;
        }
        return tContPlanSet.get(1).getCalFactorValue();
    }

    /**
     * 得到到帐确认日期
     * @return String
     */
    public static String getEnterAccDate(String edorNo)
    {
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(edorNo);
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeSet.size() == 0)
        {
            return null;
        }
        return tLJTempFeeSet.get(1).getEnterAccDate();
    }

    /**
     * 得到客户信息
     * @param customerNo String
     * @return LDPersonSchema
     */
    public static LDPersonSchema getLDPerson(String customerNo)
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(customerNo);
        if (!tLDPersonDB.getInfo())
        {
            return null;
        }
        return tLDPersonDB.getSchema();
    }

    /**
     * 得到投保人地址信息
     * @param contNo String
     * @return LCAddressSchema
     */
    public static LCAddressSchema getLCAddress(String contNo)
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo())
        {
            return null;
        }
        String customerNo = tLCAppntDB.getAppntNo();
        String addressNo = tLCAppntDB.getAddressNo();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(customerNo);
        tLCAddressDB.setAddressNo(addressNo);
        if (!tLCAddressDB.getInfo())
        {
            return null;
        }
        return tLCAddressDB.getSchema();
    }

    /**
     * 得到被保人信息
     * @param contNo String
     * @param insuredNo String
     * @return LCInsuredSchema
     */
    public static LCInsuredSchema getLCInsured(String contNo, String insuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(contNo);
        tLCInsuredDB.setInsuredNo(insuredNo);
        if (!tLCInsuredDB.getInfo())
        {
            return null;
        }
        return tLCInsuredDB.getSchema();
    }

    /**
     * 得到保全应收信息
     * @param edorNo String
     * @param edorType String
     * @return LJAPaySchema
     */
    public static LJSPaySchema getLJSPay(String edorNo, String noticeType)
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(edorNo);
        tLJSPayDB.setOtherNoType(noticeType);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() == 0)
        {
            return null;
        }
        return tLJSPaySet.get(1);
    }

    /**
     * 得到保全实收信息
     * @param edorNo String
     * @param edorType String
     * @return LJAPaySchema
     */
    public static LJAPaySchema getLJAPay(String edorNo, String noticeType)
    {
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setIncomeNo(edorNo);
        tLJAPayDB.setIncomeType(noticeType);
        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        if (tLJAPaySet.size() == 0)
        {
            return null;
        }
        return tLJAPaySet.get(1);
    }


    /**
     * 得到实收子表信息,主要是为了在保全给LJSPayPerson 赋值,慎用!
     * @param PolNo String
     * @return LJAPayPersonSchema
     */
    public static LJAPayPersonSchema getLJAPayPerson(String aPolNo)
    {
    	LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
    	tLJAPayPersonDB.setPolNo(aPolNo);
    	LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if (tLJAPayPersonSet.size() == 0)
        {
            return null;
        }
        return tLJAPayPersonSet.get(1);
    }
    /**
     * 得到保全实付信息
     * @param edorNo String
     * @param noticeType String
     * @return LJAGetSchema
     */
    public static LJAGetSchema getLJAGet(String edorNo, String noticeType)
    {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setOtherNo(edorNo);
        tLJAGetDB.setOtherNoType(noticeType);
        LJAGetSet tLJAGetSet = tLJAGetDB.query();
        if (tLJAGetSet.size() == 0)
        {
            return null;
        }
        return tLJAGetSet.get(1);
    }

    /**
     * 得到保全计算代码，保全算费按险种分
     * @param riskCode String
     * @return String
     */
    public static String getCalCode(String edorType, String riskCode)
    {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(riskCode);
        tLMPolDutyEdorCalDB.setEdorType(edorType);
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if (tLMPolDutyEdorCalSet.size() == 0)
        {
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1).getChgPremCalCode();
    }

    /**
     * 得到特需或TPA险种团体理赔帐户余额
     * @param grpContNo String
     * @throws Exception
     * @return double
     */
    public static double getGrpAccountMoney(String grpContNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(grpContNo);
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
        tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            throw new RuntimeException();
        }

        String polNo = tLCPolSet.get(1).getPolNo();
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(polNo);
        tLCInsureAccDB.setInsuAccNo(getInsuAccNo(BQ.ACCTYPE_GROUP, tLCPolSet
                .get(1).getRiskCode()));
        if (!tLCInsureAccDB.getInfo())
        {
            throw new RuntimeException();
        }
        return tLCInsureAccDB.getInsuAccBala();
    }

    /**
     * 计算团单险种grpPolNo个人账户的余额和
     * @param grpPolNo String
     * @return double
     */
    public static double getInsuredAccountMoney(String grpPolNo)
    {
        String sql = "  select sum(insuAccBala) "
                + "from LCInsureAcc a, LCPol b " + "where a.polNo = b.polNo "
                + "   and b.polTypeFlag != '" + BQ.POLTYPEFLAG_PUBLIC + "' "
                + "   and a.grpPolNo = '" + grpPolNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String sumInsuAccBala = tExeSQL.getOneValue(sql);
        return Double.parseDouble(sumInsuAccBala.equals("") ? "0"
                : sumInsuAccBala);
    }

    /**
     * 得到团体公共帐户
     * @param grpPolNo String
     * @return String
     */
    public static String getPubliAccPolNo(String grpPolNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpPolNo(grpPolNo);
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
        tLCPolDB.setAccType(BQ.ACCTYPE_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            throw new RuntimeException();
        }
        return tLCPolSet.get(1).getPolNo();
    }

    /**
     * 得到磁盘导入xml配置文件路径
     * @return String
     */
    public static String getXmlPath()
    {
        String sql = "select SysvarValue from LDSysVar "
                + "where SysVar = 'XmlPath'";
        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.getOneValue(sql);
    }

    /**
     * 得到团体客户号
     * @param grpcontNo String
     * @return String
     */
    public static String getGrpCustomerNo(String grpcontNo)
    {
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(grpcontNo);
        if (!tLCGrpAppntDB.getInfo())
        {
            return null;
        }
        return tLCGrpAppntDB.getCustomerNo();
    }

    /**
     * 检查是否需要财务交费
     * @return boolean true为需要交费，false为不需交费
     */
    public static boolean checkGetMoney(String edorAcceptNo)
    {
        int ret = queryLCGrpBalPlan(edorAcceptNo);
        if (ret == 1)
        {
            System.out.println("该保单是定期结算！");
            return false;
        }
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(edorAcceptNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() == 0)
        {
            return false;
        }
        if (tLJSPaySet.get(1).getSumDuePayMoney() > 0)
        {
            return true;
        }
        return false;
    }

    /**
     * 得到保全状态
     * @param edorAcceptNo String
     * @return String
     */
    public static String getEdorState(String edorAcceptNo)
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(edorAcceptNo);
        if (!tLPEdorAppDB.getInfo())
        {
            return null;
        }
        return tLPEdorAppDB.getEdorState();
    }

    /**
     * 校验险种是否是特需或TPA险种
     * @param polNo String
     * @return boolean，是特需险种：true， 否则：false
     */
    public static boolean isEspecialPol(String riskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(riskCode);
        if (tLMRiskAppDB.getInfo())
        {
            String riskType = tLMRiskAppDB.getRiskType3();
            String riskType8 = tLMRiskAppDB.getRiskType8();
            if ((riskType != null && riskType.equals(BQ.RISKTYPE3_SPECIAL)) 
            		|| (riskType8 != null && riskType8.equals(BQ.RISKTYPE8_TPA)))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 校验险种是否是补充团体医疗产品
     * @param polNo String
     * @return boolean，是：true， 否则：false
     */
    public static boolean isTDBCPol(String riskCode)
    {
    	String str="select 1 from ldcode where codetype='tdbc' and code='"+riskCode+"'";
    	String isflag=mExeSQL.getOneValue(str);
    	if(null != isflag && !"".equals(isflag)){
    		return true;
    	}
        return false;
    }
    
    /**
     * 校验险种是否是税优产品
     * @param polNo String
     * @return boolean，是：true， 否则：false
     */
    public static boolean isTPHIPol(String riskCode)
    {
    	String str="select 1 from lmriskapp where taxoptimal ='Y' and riskcode='"+riskCode+"'";
    	String isflag=mExeSQL.getOneValue(str);
    	if(null != isflag && !"".equals(isflag)){
    		return true;
    	}
        return false;
    }

    /**
     * 得到保单信息
     * @param contNo String
     * @return LCContSchema
     */
    public static LCContSchema getLCCont(String contNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo())
        {
            return null;
        }
        return tLCContDB.getSchema();
    }

    /**
     * 得到险种信息
     * @param polNo String
     * @return LCPolSchema
     */
    public static LCPolSchema getLCPol(String polNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        if (!tLCPolDB.getInfo())
        {
            return null;
        }
        return tLCPolDB.getSchema();
    }

    /**
     * 根据团体险种号和被保人客户号得到个人险种号
     * @param grpPolNo String
     * @param insuredNo String
     * @return LCPolSchema
     */
    public static LCPolSchema getLCPol(String grpPolNo, String insuredNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpPolNo(grpPolNo);
        tLCPolDB.setInsuredNo(insuredNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            return null;
        }
        return tLCPolSet.get(1);
    }

    /**
     * 根据保单号得到团体险种号
     * @param grpContNo String
     * @return LCGrpPolSet
     */
    public static LCGrpPolSet getLCGrpPolSet(String grpContNo)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        return tLCGrpPolDB.query();
    }

    /**
     * 得到团体保单信息
     * @param contNo String
     * @return LCContSchema
     */
    public static LCGrpContSchema getLCGrpCont(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            return null;
        }
        return tLCGrpContDB.getSchema();
    }

    /**
     * 得到账户信息
     * @param insuredNo String
     * @return LCInsureAccSchema
     */
    public static LCInsureAccSchema getLCInsureAcc(String grpPolNo,
            String insuredNo, String accType)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(grpPolNo);
        if (!tLCGrpPolDB.getInfo())
        {
            return null;
        }
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setGrpPolNo(grpPolNo);
        tLCInsureAccDB.setInsuredNo(insuredNo);
        tLCInsureAccDB.setInsuAccNo(getInsuAccNo(accType, tLCGrpPolDB
                .getRiskCode()));
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
        if (tLCInsureAccSet.size() == 0)
        {
            return null;
        }
        return tLCInsureAccSet.get(1);
    }

    /**
     * 得到账户信息
     * @param insuredNo String
     * @return LCInsureAccSchema
     */
    public static LCInsureAccSchema getLCInsureAcc(String polNo,
            String insuAccNo)
    {
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(polNo);
        tLCInsureAccDB.setInsuAccNo(insuAccNo);
        if (!tLCInsureAccDB.getInfo())
        {
            return null;
        }
        return tLCInsureAccDB.getSchema();
    }

    /**
     * 得到特需医疗管理费率
     * @param grpPolNo String
     * @param accType String
     * @return double
     */
    public static double getManageFeeRate(String grpPolNo, String feeCode)
    {
        //modify by hyy 08.1.25 因为新老特需险种的 feecode 不一致 所以需要改动
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        //根据grppolnocha出险种号码
        tLCGrpPolDB.setGrpPolNo(grpPolNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

        //System.out.println(grpSet.get(1).getRiskCode()+"~~~~~~");
        //如果险种号码为170301 把feeCode设置为000003
        if ((tLCGrpPolSet.size() != 0)
                && (tLCGrpPolSet.get(1).getRiskCode().equals("170301")||tLCGrpPolSet.get(1).getRiskCode().equals("170401"))
                && "000006".equals(feeCode))
        {
            feeCode = "000003";
        }

        LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
        tLCGrpFeeDB.setGrpPolNo(grpPolNo);
        tLCGrpFeeDB.setFeeCode(feeCode);
        LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.query();
        if (tLCGrpFeeSet.size() == 0)
        {
            return 0;
        }
        return tLCGrpFeeSet.get(1).getFeeValue();
    }
    
    /**
     * 得到特需医疗管理费率
     * @param grpPolNo String
     * @param accType String
     * @return double
     */
    public static double getManageFeeRateByCont(String aGrpContNo, String aFeeCode)
    {
    	LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(aGrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
        	String tRiskCode = tLCGrpPolSet.get(i).getRiskCode();
            if(isEspecialPol(tRiskCode))
            {
            	if (("170301".equals(tRiskCode)||"170401".equals(tRiskCode)) && BQ.FEECODE_CT.equals(aFeeCode))
                {
            		aFeeCode = "000003";
                }

                LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
                tLCGrpFeeDB.setGrpPolNo(tLCGrpPolSet.get(i).getGrpPolNo());
                tLCGrpFeeDB.setFeeCode(aFeeCode);
                LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.query();
                if (tLCGrpFeeSet.size() == 0)
                {
                    return 0;
                }
                return tLCGrpFeeSet.get(1).getFeeValue();
            }
            else
            {
            	return 0;
            }
        }
        return 0;
    }

    //一下是测试方法
    public static void main(String[] args)
    {

        CommonBL.test();
    }

    /**
     * 保全项目信息
     * @param edorType String
     * @param appObj String I: 个险, G:团险
     * @return LMEdorItemSchema
     */
    public static LMEdorItemSchema getEdorInfo(String edorType, String appObj)
    {
        LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
        tLMEdorItemDB.setEdorCode(edorType);
        tLMEdorItemDB.setAppObj(appObj);
        tLMEdorItemDB.getInfo();
        return tLMEdorItemDB.getSchema();
    }

    /**
     * 得到保全磁盘导入的数据
     * @param edorNo String
     * @param edorType String
     * @param state String
     * @return LPDiskImportSet
     */
    public static LPDiskImportSet getLPDiskImportSet(String edorNo,
            String edorType, String grpContNo, String state)
    {
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setEdorNo(edorNo);
        tLPDiskImportDB.setEdorType(edorType);
        tLPDiskImportDB.setGrpContNo(grpContNo);
        tLPDiskImportDB.setState(state);
        return tLPDiskImportDB.query();
    }

    /**
     * 得到保全磁盘导入的数据
     * @param edorNo String
     * @param edorType String
     * @param grpContNo String
     * @param riskSeqNo String
     * @param state String
     * @return LPDiskImportSet
     */
    public static LPDiskImportSet getLPDiskImportSet(String edorNo,
            String edorType, String grpContNo, String riskSeqNo, String state)
    {
        if ((riskSeqNo == null) || (riskSeqNo.equals("")))
        {
            LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
            tLPDiskImportDB.setEdorNo(edorNo);
            tLPDiskImportDB.setEdorType(edorType);
            tLPDiskImportDB.setGrpContNo(grpContNo);
            tLPDiskImportDB.setState(state);
            return tLPDiskImportDB.query();
        }
        else
        {
            LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
            tLPDiskImportDB.setEdorNo(edorNo);
            tLPDiskImportDB.setEdorType(edorType);
            tLPDiskImportDB.setGrpContNo(grpContNo);
            tLPDiskImportDB.setRiskSeqNo(riskSeqNo);
            tLPDiskImportDB.setState(state);
            return tLPDiskImportDB.query();
        }
    }

    /**
     * 得到财务类型
     * @param edorType String
     * @param code String
     * @return String
     */
    public static String getFeeType(String edorType, String code)
    {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType(edorType);
        tLDCode1DB.setCode(code);
        tLDCode1DB.setCode1(BQ.FILLDATA);
        if (!tLDCode1DB.getInfo())
        {
            return null;
        }
        return tLDCode1DB.getCodeName();
    }

    /**
     * 得到补退费金额
     * @param edorNo String
     * @param edorType String
     * @param polNo String
     * @return double
     */
    public static double getChgMoney(String edorNo, String edorType,
            String polNo)
    {
        String sql = "select sum(GetMoney) from LJSGetEndorse "
                + "where EndorseMentNo = '" + edorNo + "' "
                + "and FeeOperationType = '" + edorType + "' "
                + "and PolNo = '" + polNo + "' ";
        String chgMoney = (new ExeSQL()).getOneValue(sql);
        if (chgMoney.equals(""))
        {
            return 0.0;
        }
        return Double.parseDouble(chgMoney);
    }

    /**
     * 检验保单是否已生成满期理算任务并已满期理算
     * 本类没有容错能力，需保证传入的团单号是正确的
     * @param grpContNo String: 团单号
     * @return boolean，true：已生成满期理算任务并已满期理算
     */
    public static boolean afterMJCalculate(String grpContNo)
    {
        //通过保单的满期结算工单号去查询特殊数据表的满期结算状态，
        //若保单状态为正满期结算，工单未处理完毕，满期结算状态为1，则可认为保单没有进行满期理算
        boolean flag = true;
        StringBuffer sql = new StringBuffer();
        sql.append("select c.* ").append(
                "from LCGrpCont a, LGWork b, LPEdorEspecialData c ").append(
                "where a.grpContNo = b.contNo ").append(
                "   and b.workNo = c.edorAcceptNo ").append("   and polNo = '")
                .append(BQ.FILLDATA).append(
                        "'  and b.statusNo not in ('5', '6', '7', '8') ")
                .append("  and c.edorType = '").append(BQ.EDORTYPE_MJ).append(
                        "'  and c.detailType = '")
                .append(BQ.DETAILTYPE_MJSTATE).append("'  and edorValue != '")
                .append(BQ.MJSTATE_INIT).append("'  and edorValue != '")
                .append(BQ.MJSTATE_FINISH).append("'  and a.state = '").append(
                        BQ.CONTSTATE_MJING).append("'  and a.grpContNo = '")
                .append(grpContNo).append("' ");

        System.out.println(sql.toString());
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = tLPEdorEspecialDataDB
                .executeQuery(sql.toString());
        if (tLPEdorEspecialDataSet.size() == 0)
        {
            flag = false;
        }
        //qulq 061226
        sql = new StringBuffer();
        sql
                .append(
                        "select 1 from lpgrpedoritem a ,lpedorapp b  where b.edorstate !='0' ")
                .append(
                        "and a.edoracceptno = b.edoracceptno and a.edortype in ('ZB','LQ','GA') AND a.grpcontno ='")
                .append(grpContNo + "'");
        String rs = (new ExeSQL()).getOneValue(sql.toString());
        if (rs != null && !rs.equals(""))
        {
            flag = true;
        }

        return flag;
    }

    /**
     * 得到被保人insuredNo在保单contNo中除了特需险的当期实交保费
     * 若险种的缴费频次不一样的话，个险的当期保费有效期不一样长，且缴费次数也不一样
     * 所以需对每个险种分别计算当期实交保费
     * @param contNo String 保单号
     * @param insuredNo String 被保人号
     * @return double
     */
    public static double getCurSumActuPayMoneyOfCmnPol(String contNo,
            String insuredNo)
    {
        double sumActuPayMoney = 0;
        ExeSQL tExeSQL = new ExeSQL();

        //得到被保人的所有险种
        String sql = "  select distinct a.polNo "
                + "from LCPol a, LMRiskApp b "
                + "where a.riskCode = b.riskCode " + "   and b.riskType3 != '"
                + BQ.RISKTYPE3_SPECIAL + "'  and contNo = '" + contNo
                + "'  and insuredNo = '" + insuredNo + "' ";
        System.out.println(sql);
        SSRS tSSRS = tExeSQL.execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            //险种的最大缴费次数，取最大缴费次数的实交保费
            sumActuPayMoney += getCurSumActuPayMoneyOfOnePol(tSSRS
                    .GetText(i, 1));
        }

        return sumActuPayMoney;
    }

    /**
     * 得到险种polno的当期实交保费
     * @param polNo String
     * @return double
     */
    public static double getCurSumActuPayMoneyOfOnePol(String polNo)
    {
        String sql = "  select max(payCount) " + "from LJAPayPerson "
                + "where polNo = '" + polNo + "' ";
        String maxPayCount = mExeSQL.getOneValue(sql);
        if (maxPayCount.equals("") || maxPayCount.equals("null"))
        {
            return 0;
        }

        sql = "  select sum(sumActuPayMoney) " + "from LJAPayPerson a "
                + "where payCount = " + maxPayCount + "   and polNo = '"
                + polNo + "' ";
        String money = mExeSQL.getOneValue(sql);
        if (money.equals("") || money.equals("null"))
        {
            return 0;
        }
        return Double.parseDouble(money);
    }

    /**
     * 若数据太大，double数据会以科学计数法的新式显示
     * 本方法用来将值value转换成字符串表示的pattern精度的正常显示
     * @param value Double
     * @param pattern String
     * @return String
     */
    public static String bigDoubleToCommonString(double value, String pattern)
    {
        try
        {
            DecimalFormat tDecimalFormat = new DecimalFormat(pattern);
            String valueString = tDecimalFormat.format(value);
            return valueString;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 判断保单是否是无名单
     * @param grpContNo String
     * @return boolean
     */
    public static boolean isNoNameGrpCont(String grpContNo)
    {
        String sql = "  select 1 " + "from LCCont " + "where polType = '1' "
                + "   and grpContNo = '" + grpContNo + "' ";
        String flag = mExeSQL.getOneValue(sql);
        if (flag.equals("") || flag.equals("null"))
        {
            return false;
        }

        return true;
    }

    /**
     * 得到交费期数，即当前是第几期交费
     * @return int
     */
    public static int getPayNum(LPPolSchema tLPPolSchema)
    {
        if (tLPPolSchema.getPayIntv() == 0)
        {
            return 1;
        }
        else
        {
            return PubFun.calInterval(tLPPolSchema.getCValiDate(), tLPPolSchema
                    .getPaytoDate(), "M")
                    / tLPPolSchema.getPayIntv();
        }
    }

    //查询是否是定期结算的保单
    public static int queryLCGrpBalPlan(String edorAcceptNo)
    {
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(edorAcceptNo);
        tLPGrpEdorMainDB.setEdorNo(edorAcceptNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet == null)
        {
            if (tLPGrpEdorMainDB.mErrors.needDealError())
            {
                return -1;
            }
        }
        if (tLPGrpEdorMainSet.size() == 0)
        {
            return 0;
        }
        //获得团体合同号
        String grpContNo = tLPGrpEdorMainSet.get(1).getGrpContNo();

        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        LCGrpBalPlanSet tLCGrpBalPlanSet = tLCGrpBalPlanDB.query();
        //查询定期结算计划出错
        if (tLCGrpBalPlanSet == null)
        {
            if (tLCGrpBalPlanDB.mErrors.needDealError())
            {
                return -1;
            }
        }
        //不存在定期结算计划或者为随时结算
        if (tLCGrpBalPlanSet.size() == 0)
        {
            return 0;
        }
        LCGrpBalPlanSchema tLCGrpBalPlanSchema = tLCGrpBalPlanSet.get(1);
        //        if (!tLCGrpBalPlanSchema.getState().equals("0")) {
        //            CError tError = new CError();
        //            tError.moduleName = "ValidateFinanceData";
        //            tError.functionName = "queryLCGrpBalPlan";
        //            tError.errorMessage = "该保单正处于结算过程当中，不能进行保全操作!";
        //            this.mErrors.addOneError(tError);
        //            return 2;
        //        }
        //为随时结算
        if (tLCGrpBalPlanSchema.getBalIntv() == 0)
        {
            return 0;
        }
        //如果选择了延期计算
        if (queryEspecialDate(edorAcceptNo) == 1)
        {
            return 0;
        }

        return 1;
    }

    /**
     * 返回值1，表示即时计算。
     * @return int
     */
    private static int queryEspecialDate(String edorAcceptNo)
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(edorAcceptNo);
        tLPEdorEspecialDataDB.setEdorNo(edorAcceptNo);
        tLPEdorEspecialDataDB.setEdorType("DJ");
        tLPEdorEspecialDataDB.setDetailType("BALTYPE");
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = tLPEdorEspecialDataDB
                .query();

        if (tLPEdorEspecialDataSet == null)
        {
            if (tLPEdorEspecialDataSet.mErrors.needDealError())
            {
                return -1;
            }
        }
        if (tLPEdorEspecialDataSet.size() == 0)
        {
            return 0;
        }
        else if (tLPEdorEspecialDataSet.get(1).getEdorValue().equals("1"))
        {
            return 1;
        }
        return 0;
    }

    /**
     * 判断团单是否正在做实收保费转出
     * @param GrpContNo String 团单号
     * @return boolean 返回 true 是正在进行保费转出
     */
    public static boolean checkGrpBack(final String GrpContNo)
    {
        if (GrpContNo == null || GrpContNo.equals(""))
        {
            System.out.println("保单号错误！");
            return true;
        }
        String sql = "select 1 from ljspayb where OtherNo = '" + GrpContNo
                + "' and otherNoType = '1' and dealstate = '"
                + FeeConst.DEALSTATE_BACK + "' ";
        String flag = mExeSQL.getOneValue(sql);
        if (flag.equals("") || flag.equals("null"))
        {
            return false;
        }

        return true;
    }

    /**
     * 判断团单是否正在做实收保费转出
     * @param GrpContNo String 团单号
     * @return boolean 返回 true: 正在进行保费转出
     */
    public static boolean checkIndiBack(final String contNo)
    {
        if (contNo == null || contNo.equals(""))
        {
            System.out.println("保单号错误！");
            return true;
        }

        //状态为正转出
        String sql = "select 1 from LJSPayB " + "where OtherNo = '" + contNo
                + "' " + "   and otherNoType = '2' " + "   and dealstate = '"
                + FeeConst.DEALSTATE_BACK + "' ";
        String flag = mExeSQL.getOneValue(sql);
        if (!flag.equals("") && !flag.equals("null"))
        {
            return true;
        }

        //有未处理完成的日志信息
        StringBuffer sqls2 = new StringBuffer();
        sqls2.append("select * from LCUrgeVerifyLog ").append(
                "where RiskFlag = '2' ").append("   and ContNo = '").append(
                contNo).append("'  and OperateType = '3' ").append(
                "   and OperateFlag = '1' ").append("   and DealState != '3' ");
        System.out.println(sqls2.toString());
        LCUrgeVerifyLogSet set = new LCUrgeVerifyLogDB().executeQuery(sqls2
                .toString());
        if (set.size() > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * 删除tLPEdorItemSchema所代表险种的所有P表记录：
     * LPPol,LPInsuredRelated,LPBnf,LPDuty,LPPrem,LPGet,LPPremToAcc,LPGetToAcc,LPSpec
     * LPInsureAcc,LPInsureAccClassFee,LPInsureAccFee,LPInsureAccClass,LPInsureAccTrace
     * 若有与险种相关但本方法没有的保全表，请添加
     * @param tLPEdorItemSchema LPEdorItemSchema：需要EdorNo,EdorType,PolNo
     * @return MMap:处理的sql
     */
    public static MMap delPolInfoFromP(LPEdorItemSchema schema)
    {
        if (schema.getEdorNo() == null || schema.getEdorNo().equals("")
                || schema.getEdorType() == null
                || schema.getEdorType().equals("") || schema.getPolNo() == null
                || schema.getPolNo().equals(""))
        {
            System.out.println("必须传入EdorNo,EdorType,PolNo");
            return null;
        }

        //LP表名
        String[] tables = { "LPPol", "LPInsuredRelated", "LPBnf", "LPDuty",
                "LPPrem", "LPGet", "LPSpec", "LPPremToAcc", "LPGetToAcc",
                "LPInsureAcc", "LPInsureAccClassFee", "LPInsureAccFee",
                "LPInsureAccClass", "LPInsureAccTrace", "LPEdorEspecialData" };

        MMap tMMap = new MMap();
        for (int i = 0; i < tables.length; i++)
        {
            if (!"LP".equals(tables[i].substring(0, 2)))
            {
                System.out.println("删除P表险种时，表名错误：" + tables[i]);
                return null;
            }

            StringBuffer sql = new StringBuffer();
            sql.append("delete from ").append(tables[i]).append(
                    " where edorNo = '").append(schema.getEdorNo()).append(
                    "'   and edorType = '").append(schema.getEdorType())
                    .append("'   and polNo = '").append(schema.getPolNo())
                    .append("' ");

            tMMap.put(sql.toString(), SysConst.DELETE);
        }
        return tMMap;
    }

    /**
     * 查询codeType/code对应的CodeName
     * @param codeType String
     * @param code String
     * @return String
     */
    public static String getCodeName(String codeType, String code)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType(codeType);
        tLDCodeDB.setCode(code);
        if (!tLDCodeDB.getInfo())
        {
            return null;
        }

        return tLDCodeDB.getCodeName();
    }

    /**
     * 数组对应位置字段相加,主要处理以字符串存储的数字数组
     * @param left String[]
     * @param right String[]
     * @param start: 从该位置开始相加
     * @return String[]
     */
    public static String[] add(String[] left, String[] right, int start)
    {
        String[] result = null;
        if (left == null && right == null)
        {
            System.out.println("相加的两数均不能为空");
            return null;
        }

        if (lengthExceptNull(left) == 0)
        {
            result = copyString(right);
            return result;
        }
        if (lengthExceptNull(right) == 0)
        {
            result = copyString(left);
            return result;

        }

        result = new String[left.length];
        for (int i = start; i < left.length; i++)
        {
            //字符串转换为double后求和
            if (left[i] != null && left[i].indexOf(".") >= 0
                    || right[i] != null && right[i].indexOf(".") >= 0)
            {
                result[i] = CommonBL.bigDoubleToCommonString(Double
                        .parseDouble(left[i])
                        + Double.parseDouble(right[i]), "0.00");
            }
            //字符串转换为int后求和
            else if (left[i] != null && right[i] != null)
            {
                result[i] = String.valueOf(Integer.parseInt(left[i])
                        + Integer.parseInt(right[i]));
            }
        }

        return result;
    }

    /**
     * 复制字符串数组
     * @param cString String[]
     * @return String[]
     */
    public static String[] copyString(String[] cString)
    {
        if (cString == null)
        {
            return null;
        }

        String[] string = new String[cString.length];
        for (int i = 0; i < cString.length; i++)
        {
            if (cString[i] == null)
            {
                string[i] = null;
                continue;
            }
            string[i] = new String(cString[i]);
        }

        return string;
    }

    /**
     * 计算去除字符串内容为null的字符串数组长度
     * @param string String[]
     * @return int
     */
    public static int lengthExceptNull(String[] string)
    {
        if (string == null)
        {
            return 0;
        }

        int length = 0;
        for (int i = 0; i < string.length; i++)
        {
            if (string[i] != null)
            {
                length++;
            }
        }

        return length;
    }

    /**
     * 查询项目类型名
     * @param cEdorCode String
     * @param cAppObj String
     * @return String
     */
    public static String getEdorName(String cEdorCode, String cAppObj)
    {
        LMEdorItemSchema schema = getEdorInfo(cEdorCode, cAppObj);
        if (schema == null)
        {
            return null;
        }

        return schema.getEdorName();
    }

    /**
     * 得到补退费金额总计
     * @param containYE boolean：求和是否包括余额抵扣的数据，
     * true：总求和，false：除了余额抵扣外的求和
     * @return double
     */
    public static double getSumGetMoney(String edorNo, boolean containYE)
    {
        String yePart = containYE ? ""
                : "   and FeeOperationType not in('YEI', 'YEO') ";

        String sql = "select sum(GetMoney) " + "from LJSGetEndorse "
                + "where EndorsementNo = '" + edorNo + "' " + yePart;
        ExeSQL tExeSQL = new ExeSQL();
        String money = tExeSQL.getOneValue(sql);
        if (money.equals(""))
        { //如果没有记录则设money为0
            money = "0";
        }

        return Double.valueOf(money).doubleValue();
    }

    /**
     * 得到补退费金额总计
     * @param EdorNo 工单号
     * @return double
     * add by fuxin 2007-6-25
     */
    public static double getSumGetMoney(String edorNo) {

        String sql = "select sum(GetMoney) " + "from LJSGetEndorse "
                     + "where EndorsementNo = '" + edorNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String money = tExeSQL.getOneValue(sql);
        if (money.equals("")) { //如果没有记录则设money为0
            money = "0";
        }
        return Double.valueOf(money).doubleValue();
    }


    /*
     * 得到该团单下的特需或TPA险种
     *  @param grpContNo String：团体客户号码
     *  @return String 险种号码
     */
    static public String getSpecialRisk(String grpContNo)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return null;
            }
            String riskType = tLMRiskAppDB.getRiskType3();
            String riskType8= tLMRiskAppDB.getRiskType8();
            if ((riskType != null && riskType.equals(BQ.RISKTYPE3_SPECIAL)) 
            		|| (riskType8 != null && riskType8.equals(BQ.RISKTYPE8_TPA)))
            {
                return tLCGrpPolSet.get(i).getRiskCode();
            }
        }
        return null;
    }

    /**
     * 查询保单的可能会影响保费的未结案保全项目
     * @param cContNo String：个单号
     * @return LPEdorItemSet：个单保全项目
     */
    public static LPEdorItemSet getEdorItemMayChangePrem(String cContNo)
    {
        String sql = "select a.* " + "from LPEdorItem a, LPEdorApp b "
                + "where a.EdorAcceptNo = b.EdorAcceptNo "
                + "   and a.ContNo = '" + cContNo + "' "
                + "   and b.EdorState != '0' "
                + "   and a.EdorType not in(select Code from LDCode "
                + "               where CodeType = 'edortypechangeprem') ";
        System.out.println(sql);
        LPEdorItemSet set = new LPEdorItemDB().executeQuery(sql);
        return set;
    }

    /**
     * 查询险种的满期日期，保单正常、续保、解约、协议退保、减人、犹豫期退保。
     * @param polno String 险种号码
     * @return String 险种满期日期。
     */
    public static String getPolInvalidate(String polno)
    {
        //查询正常单
        String queryEndDate = " select enddate from lcpol where appflag ='1' and polno ='"
                + polno
                + "' "
                + " union " //查询续保前保单。
                + " select enddate from lbpol a where polno ='"
                + polno
                + "' and "
                + " exists ( select 1 from lcrnewstatelog lcr where lcr.grpcontno = a.grpcontno and  lcr.newpolno = a.polno ) "
                + " union " //查询团单的退保减人、个单的退保，协议退保，犹豫期退保的险种截至日期。
                + " select (case lp.edortype when 'WT' then lb.cvalidate else lp.edorvalidate end) "
                + " from lpedoritem lp,lbpol lb where lp.edorno = lb.edorno "
                + " and lp.contno = lb.contno and lb.polno ='"
                + polno
                + "' "
                + " and exists ( select 1 from lpedorapp where edoracceptno = lp.edorno and edorstate ='0') "
                + " and lp.edortype in ('CT','XT','ZT','WT') "
                + " union " //查询团单的协议退保、由于期退保险种的截至日期。
                + " select (case lp.edortype when 'WT' then lb.cvalidate else lp.edorvalidate end) "
                + " from lpgrpedoritem lp,lbpol lb where lp.edorno = lb.edorno "
                + " and lp.grpcontno = lb.grpcontno and lb.polno ='"
                + polno
                + "' and lb.conttype ='2' "
                + " and exists ( select 1 from lpedorapp where edoracceptno = lp.edorno and edorstate ='0' and othernotype ='2') "
                + " and lp.edortype in ('XT','WT') ";
        return (new ExeSQL()).getOneValue(queryEndDate);
    }

    /**
     * 判断万能险能否进行保费缓交，
     * @param contno String 险种号码
     * @return boolean  true：可缓交，false：不可缓交
     */
    public static boolean checkULIHuan(String contno)
    {
    	try
    	{
    		double accbala = 0.0;
        	double money_mf = 0.0;
        	double money_rp = 0.0;
        	ExeSQL tExeSQL = new ExeSQL();
        	String sql = "select insuaccbala from lcinsureacc where contno = '"+contno+"' ";
        	String temp = tExeSQL.getOneValue(sql);

        	if(!"".equals(temp))
        	{
        		accbala = Double.parseDouble(temp);
        	}
        	sql = "select nvl(min(case moneytype when 'MF' then money else 0 end),0), "
        		+ "nvl(min(case moneytype when 'RP' then money else 0 end),0) "
        		+ "from lcinsureacctrace where contno = '"+contno+"' and moneytype in ('MF','RP') ";
        	SSRS tSSRS = new SSRS();
        	tSSRS = tExeSQL.execSQL(sql);
        	String temp1 = tSSRS.GetText(1, 1);
        	String temp2 = tSSRS.GetText(1, 2);
        	if(!"".equals(temp1))
        	{
        		money_mf = Double.parseDouble(temp1);
        	}
        	if(!"".equals(temp2))
        	{
        		money_rp = Double.parseDouble(temp2);
        	}
        	if(accbala <= Math.abs(money_mf+money_rp))
        	{
        		return false;
        	}

    	}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
        	return true;
    	}
    	return true;
    }
    
    /**
     * 得到投保时保额
     * @param aLCPolSchema LCPolSchema 
     * @return double
     * 2010-5-25 计算初始保费要用投保时保额 CQ号：
     */
    public static double getTBAmnt(LCPolSchema aLCPolSchema)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	double tAmnt = aLCPolSchema.getAmnt();
    	
//    	添加对于湖南相关保单的配置
    	String sqlcheck = "select count(1) from lpedorespecialdata where edorvalue='1' and edortype = 'WN' and detailtype = 'ULIFEE' and edorno = '" + aLCPolSchema.getContNo() + "' with ur";
     	String tCount = tExeSQL.getOneValue(sqlcheck);
     	if(!"0".equals(tCount))
     	{
     		return tAmnt;
     	}	     	
    	
    	try 
    	{
			String tContNo = aLCPolSchema.getContNo();
			String sql = "select edoracceptno from LPEdorItem a where ContNo = '" + tContNo + "' and EdorType = 'BA' "
				       + "and exists (select 1 from LPEdorApp where EdorAcceptNo = a.EdorAcceptNo and EdorState = '0') " 
				       + "order by edoracceptno ";
			String firstEdorNo = tExeSQL.getOneValue(sql);
			if(!firstEdorNo.equals(""))
			{
				sql = "select Amnt from LPPol where EdorNo = '" + firstEdorNo  + "' and PolNo = '" + aLCPolSchema.getPolNo() 
				    + "' and EdorType = 'BA' ";
				String sAmnt = tExeSQL.getOneValue(sql);
				tAmnt = Double.parseDouble(sAmnt);
			}
			
		} 
    	catch (RuntimeException e)
    	{
			e.printStackTrace();
			return tAmnt;
		}
    	return tAmnt;
    }
    
    /**
     * 判断保全项目生效日不应早于公共账户或者个人账户的最后一次账户资金变化日。
     * @return boolean
     */
    public static String ISBeforeLastAccBala(String polNo,String EdorValiDate)
    {
    
		//加上和保单生效日的比较，从保单生效日开始收取管理费
		String sqlli2 = " select max(a.paydate) from lcinsureacctrace a, lcpol b"
		              + " where a.polno = '"+polNo+"' and moneytype = 'LX' and a.paydate >= b.cvalidate "
		              + " and a.polno = b.polno ";
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli2 = new SSRS();
		InsuRSSRSli2 = exesql.execSQL(sqlli2);
		if("".equals(InsuRSSRSli2.GetText(1, 1)) || null == InsuRSSRSli2.GetText(1, 1)){
			String sqlliPol = " select cvalidate from lcpol "
	              + " where polno = '"+polNo+"' ";
			InsuRSSRSli2 = new SSRS();
			InsuRSSRSli2 = exesql.execSQL(sqlliPol);
		}	
		if (InsuRSSRSli2 != null && InsuRSSRSli2.getMaxRow() > 0
				&& !("".equals(InsuRSSRSli2.GetText(1, 1)))
				&& EdorValiDate != null && !("".equals(EdorValiDate))) {
			
			String sqlli5 = "SELECT date('" + EdorValiDate
			+ "')-date('"
			+ InsuRSSRSli2.GetText(1, 1)
			+ "') FROM dual";
	        SSRS InsuRSSRSli5 = new SSRS();
	    	InsuRSSRSli5 = exesql.execSQL(sqlli5);
			if (InsuRSSRSli5 != null
					&& InsuRSSRSli5.getMaxRow() > 0
					&& (Integer.parseInt(InsuRSSRSli5.GetText(1, 1)) - 0) < 0) {
				return "1"+InsuRSSRSli2.GetText(1, 1);
			}
		}
	return "0";
    }
    /**
     * 判断保全项目生效日的上一个月应月结完成。
     * @return boolean
     */
    public static boolean ISLastMonthAccBala(String polNo,String EdorValiDate)
    {
    
		//加上和保单生效日的比较，从保单生效日开始收取管理费
		String sqlli2 = " select max(a.paydate) from lcinsureacctrace a, lcpol b"
		              + " where a.polno = '"+polNo+"' and moneytype = 'LX' and a.paydate >= b.cvalidate "
		              + " and a.polno = b.polno ";
		ExeSQL exesql = new ExeSQL();
		SSRS InsuRSSRSli2 = new SSRS();
		InsuRSSRSli2 = exesql.execSQL(sqlli2);
		if("".equals(InsuRSSRSli2.GetText(1, 1)) || null == InsuRSSRSli2.GetText(1, 1)){
			String sqlliPol = " select cvalidate from lcpol "
	              + " where polno = '"+polNo+"' ";
			InsuRSSRSli2 = new SSRS();
			InsuRSSRSli2 = exesql.execSQL(sqlliPol);
		}	
		if (InsuRSSRSli2 != null && InsuRSSRSli2.getMaxRow() > 0
				&& !("".equals(InsuRSSRSli2.GetText(1, 1)))
				&& EdorValiDate != null && !("".equals(EdorValiDate)))
		{			
			String sqlli3 = "SELECT date('" + EdorValiDate
			+ "')-(date('"
			+ InsuRSSRSli2.GetText(1, 1)
			+ "')+1 month) FROM dual";
	   SSRS InsuRSSRSli3 = new SSRS();
	  InsuRSSRSli3 = exesql.execSQL(sqlli3);
	     if (InsuRSSRSli3 != null
			&& InsuRSSRSli3.getMaxRow() > 0
			&& (Integer.parseInt(InsuRSSRSli3.GetText(1, 1)) - 0) > 0)
	      {					
		   return false;
	      }
		}
	  return true;
     }
    
    /**
     * 判断某险种是否为健管类险种
     * @return boolean
     */
    public static boolean is62Risk(String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        tLMRiskAppDB.setRiskType("M");
        tLMRiskAppDB.setRiskProp("I");

        return tLMRiskAppDB.query().size() >= 1;
    }
    
    /**
     * 查询未选择的险种信息
     * @param tLPPolSet LPPolSet
     * @return LCPolSet
     */
    public static LCPolSet getLeavingPolNoInfo(LPPolSet tLPPolSet)
    {
        String polNos = "";
        for(int i = 1; i <= tLPPolSet.size(); i++)
        {
            polNos += "'" + tLPPolSet.get(i).getPolNo() + "', ";
        }

        String sql = "  select * "
                     + "from LCPol "
                     + "where contNo = '" + tLPPolSet.get(1).getContNo() + "' "
                     + "    and polNo not in ("
                     + polNos.substring(0, polNos.lastIndexOf(",")) + ")";
        System.out.println(sql);

        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

        return tLCPolSet;
    }

    public static void test()
    {
        String edorNo = "20080407000004";
        String edorType = "CT";
        String ContNo = "000917521000001";

        //得到帐户信息
        String tPolApplyDate = "";
        String tBalaMonth = null;
        String tRate = null;
        String tInsuAccBala = null;
        String tCTFee = null;

        String tSQL = "select * from LCPol a where ContNo = '" + ContNo
                + "' and exists (select 1 from LMRiskApp where RiskType4 = '"
                + BQ.RISKTYPE1_ULI + "' and riskcode = a.RiskCode) ";
        LCPolSchema tLCPolSchema = new LCPolDB().executeQuery(tSQL).get(1);
        tPolApplyDate = tLCPolSchema.getPolApplyDate(); //获取投保日期
        String cPolNo = tLCPolSchema.getPolNo();
        String cInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
                tLCPolSchema.getRiskCode());

        tSQL = "select a.InsuAccBala,ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day))))"
                + ",a.BalaDate from LCInsureAcc a , LCInsureAccBalance b  where a.PolNo = '"
                + cPolNo
                + "' and  a.InsuAccNo = '"
                + cInsuAccNo
                + "' and  b.PolNo=a.PolNo and b.InsuAccNo = a.InsuAccNo order by b.DueBalaDate desc ";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        tInsuAccBala = tSSRS.GetText(1, 1); //当前账户金额
        tBalaMonth = tSSRS.GetText(1, 2); //最近一次结算月度
        String tBalaDate = tSSRS.GetText(1, 3);

        tSQL = "select Rate from lmInsuAccRate where RateType = 'C' and Rateintv = 1 and RateIntvUnit = 'Y' and RiskCode = '"
                + tLCPolSchema.getRiskCode()
                + "' and InsuAccNo = '"
                + cInsuAccNo
                + "' and BalaDate <= '"
                + tBalaDate
                + "' order by BalaDate desc";
        tRate = new ExeSQL().getOneValue(tSQL); //最近公布收益率

        tSQL = "select year(EdorValidate-'" + tLCPolSchema.getCValiDate()
                + "')+1 from LPEdorItem where EdorNo = '" + edorNo
                + "' and EdorType = '" + edorType + "' and ContNo = '" + ContNo
                + "'";
        String tPolYear = new ExeSQL().getOneValue(tSQL);
        int tIntPolYear = Integer.parseInt(tPolYear) >= 6 ? 0 : 6 - Integer
                .parseInt(tPolYear);
        tCTFee = tIntPolYear + "%";
    }
    //        public static void main(String arg[])
    //        {
    //        String payPlanCode = "00000008";
    //        String code = String.valueOf(Integer.parseInt(payPlanCode.substring(payPlanCode.length() - 2, payPlanCode.length())) + 1);
    //        if (code.length() < 2)
    //        {
    //            code =  "0" + code;
    //        }
    //        String str = "000000" + code;
    //        System.out.println(str);
    //        System.out.println(getCurSumActuPayMoneyOfCmnPol("00000090301", "000000903"));
    //        String date = decodeDate("2005-4-05");
    //        System.out.println(date);
    //        String sex = decodeSex("0");
    //        System.out.println(sex);
    //        System.out.println(notNull(null));
    //        System.out.println(notNull(""));
    //        System.out.println(carry("2.225"));
    //        System.out.println(carry(2.224));
    //        double t = 2.225;
    //        System.out.println(String.valueOf(t));
    //         int d = PubFun.calInterval("2006-1-1", "2006-2-1", "D");
    //         System.out.println(d);
    //                CommonBL t = new CommonBL();
    //                t.test();
    //
    //        }

}
