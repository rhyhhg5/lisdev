package com.sinosoft.lis.bq;


import org.apache.poi35.hssf.usermodel.HSSFCell;
import org.apache.poi35.hssf.usermodel.HSSFRow;
import org.apache.poi35.xssf.usermodel.XSSFCell;
import org.apache.poi35.xssf.usermodel.XSSFRow;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.utility.TransferData;

/**
 * 团单excel行解析处理类
 * 
 */
public class LCGrpInputSchemaDeal {

	/**
	 * 转换excel行为Schema
	 * 
	 * @param hRow 行对象
	 * @param rowNo 行数
	 * @param tTransferData
	 * @return
	 */
	/**
	 * @param hRow
	 * @param rowNo
	 * @param tTransferData
	 * @return
	 */
	public static LCInsuredListSchema setScheml(HSSFRow hRow, int rowNo, TransferData tTransferData) {

		try {
			String tGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
			String tPrtNo = (String) tTransferData.getValueByName("PrtNo");
			String tBatchNo = (String) tTransferData.getValueByName("BatchNo");
			String tSeqNo = (String) tTransferData.getValueByName("SeqNo");
			GlobalInput tGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");

			LCInsuredListSchema lcinsuredlistSchema = new LCInsuredListSchema();

			lcinsuredlistSchema.setGrpContNo(tGrpContNo);
			
			lcinsuredlistSchema.setInsuredID(getVal(hRow.getCell(2)));
			lcinsuredlistSchema.setState("0");
			lcinsuredlistSchema.setContNo(getVal(hRow.getCell(1)));
			lcinsuredlistSchema.setBatchNo(tBatchNo);
			lcinsuredlistSchema.setRetire(getVal(hRow.getCell(3)));
			lcinsuredlistSchema.setEmployeeName(getVal(hRow.getCell(4)));
			lcinsuredlistSchema.setInsuredName(getVal(hRow.getCell(5)));
			lcinsuredlistSchema.setRelation(getVal(hRow.getCell(6)));
			lcinsuredlistSchema.setSex(getVal(hRow.getCell(7)));
			lcinsuredlistSchema.setBirthday(getVal(hRow.getCell(8)));
			lcinsuredlistSchema.setIDType(getVal(hRow.getCell(9)));
			lcinsuredlistSchema.setIDNo(getVal(hRow.getCell(10)));
			lcinsuredlistSchema.setContPlanCode(getVal(hRow.getCell(11)));
			lcinsuredlistSchema.setOccupationType(getVal(hRow.getCell(12)));
			lcinsuredlistSchema.setBankCode(getVal(hRow.getCell(13)));
			lcinsuredlistSchema.setBankAccNo(getVal(hRow.getCell(15)));
			lcinsuredlistSchema.setAccName(getVal(hRow.getCell(14)));
			lcinsuredlistSchema.setOperator(tGlobalInput.Operator);
			lcinsuredlistSchema.setMakeDate(PubFun.getCurrentDate());
			lcinsuredlistSchema.setMakeTime(PubFun.getCurrentTime());
			lcinsuredlistSchema.setModifyDate(PubFun.getCurrentDate());
			lcinsuredlistSchema.setModifyTime(PubFun.getCurrentTime());
			lcinsuredlistSchema.setPublicAcc(getVal(hRow.getCell(16)));
			lcinsuredlistSchema.setOthIDType(getVal(hRow.getCell(17)));
			lcinsuredlistSchema.setOthIDNo(getVal(hRow.getCell(18)));
			lcinsuredlistSchema.setEnglishName(getVal(hRow.getCell(19)));
			lcinsuredlistSchema.setPhone(getVal(hRow.getCell(20)));
			lcinsuredlistSchema.setNoNamePeoples(0);
			lcinsuredlistSchema.setGetYear(0);
			lcinsuredlistSchema.setAppntPrem(0.0);
			lcinsuredlistSchema.setPersonPrem(0.0);
			lcinsuredlistSchema.setPersonOwnPrem(0.0);

			
			return lcinsuredlistSchema;
		} catch (RuntimeException e) {

			e.printStackTrace();

			return null;
		}
	}

	/**
	 * 解析单元格对象
	 * 
	 * @param hssfCell 单元格对象
	 * @return
	 */
	private static String getVal(HSSFCell hssfCell) {
		String tVal = null;

		if (hssfCell == null) {
			return null;
		}

		if (hssfCell.getCellType() == XSSFCell.CELL_TYPE_BOOLEAN) {
			tVal = String.valueOf(hssfCell.getBooleanCellValue());
		} else if (hssfCell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
			tVal = String.valueOf(hssfCell.getNumericCellValue());
		} else {
			tVal = String.valueOf(hssfCell.getStringCellValue());
		}

		return tVal;
	}
}
