package com.sinosoft.lis.bq;

import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:客户信息授权
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite liuyang
 * @version 1.0
 */
public class PEdorAUDetailUI {
	private PEdorAUDetailBL mPEdorAUDetailBL = null;

	public PEdorAUDetailUI() {
		mPEdorAUDetailBL = new PEdorAUDetailBL();
	}

	/**
	 * 调用业务逻辑
	 * @param data VData
	 * @return boolean
	 */
	public boolean submitData(VData data) {
		if (!mPEdorAUDetailBL.submitData(data)) {
			return false;
		}
		return true;
	}

	/**
	 * 返回错误信息
	 * @return String
	 */
	public String getError() {
		return mPEdorAUDetailBL.mErrors.getFirstError();
	}
}
