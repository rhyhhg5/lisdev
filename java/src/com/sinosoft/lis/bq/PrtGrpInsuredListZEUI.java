package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;

/**
* <p>Title: 团体医疗追加保费清单</p>
* <p>Description:团体医疗追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListZEUI
{
    PrtGrpInsuredListZEBL mPrtGrpInsuredListZEBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListZEUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListZEBL = new PrtGrpInsuredListZEBL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListZEBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public VData getResult()
    {
        return mPrtGrpInsuredListZEBL.getResult();
    }
}
