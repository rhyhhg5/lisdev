package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.RSWrapper;
import java.sql.Connection;
import com.sinosoft.utility.DBConnPool;
import java.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PPolicyAbateDealBL {

    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private LJSPaySchema mLJSPaySchema = null;

    public PPolicyAbateDealBL() {
    }

    //获得传入数据
    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();
        //qulq add 作废失效保单的应收记录
        DeadPolicyIndiCancel tDeadPolicyIndiCancel = new DeadPolicyIndiCancel();
        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("Available")) {
            try {
                // 发现程序有时执行不到作废应收的部分,所以现在每个独立的
                // 批处理都单独try起来.
                try {
                    /*个单险种失效批处理*/
                    CallSingleAvailable();
                } catch (Exception e) {
                    System.out.println(e.getMessage() + "个险失效批处理出现错误!");
                }
                //处理作废应收..
//                tDeadPolicyIndiCancel.dealData();
            } catch (Exception ex) {
                ex.getMessage();
                System.out.print(ex.toString());
            }
        } else if (cOperate != null && cOperate.trim().equals("Terminate")) {
            /*个单险种满期终止批处理*/
            try{
                callSingleTerminate();
            }catch(Exception ex)
            {
                System.out.println(ex.getMessage()+"个险终止批处理出错");
            }
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }

        return true;
    }

    private boolean CallSingleAvailable()
    {
        String subSql = getSQLManageCom();
        StringBuffer tStringBuffer = new StringBuffer();
        RSWrapper rsWrapper = new RSWrapper();

        LCPolSet tLCPolSet = new LCPolSet();

            //险种失效只根据险种的交至日期来判断和主附险没有直接关系。
        tStringBuffer.append(" select * from lcpol a where 1 = 1 and StateFlag ='1' and appflag = '1' ")
                .append(" and conttype = '1'  and paytodate  < '" +mCurrentDate + "' ")
                .append(" and riskcode not in (select riskcode from lmriskapp where risktype4 = '4') ")
                .append(" and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4 = '4')) ")
                //                .append(" and exists(select 1 from lmriskapp where riskcode = a.riskcode and riskperiod != 'L') ")
                .append(" and not exists (select 1 from lpedorapp lpapp,lpedoritem lpitem where lpapp.edoracceptno = lpitem.edoracceptno and lpapp.edorstate !='0' and a.contno = lpitem.contno) ") //不存在未结案的保全
                //                .append(" and not exists(select 1 from ljspay , ljspayb  where a.contno = ljspay.otherno and ljspay.getnoticeno=ljspayb.getnoticeno  and dealstate ='4' and ljspay.bankonthewayflag = '1') ") //不存在续期待核销（dealstate ='4'）和银行在途（ljspay.bankonthewayflag = '1'）的。
                .append(" and not exists (select 1 from ljspayb,ljspay where a.contno = ljspay.otherno and ljspayb.getnoticeno = ljspay.getnoticeno and dealstate = '4')")
                .append(" and not exists (select 1 from ljspay  where a.contno = ljspay.otherno and bankonthewayflag = '1') ")
                //                    .append(" and a.ContNo ='000009808000001'")
                .append("" + subSql + " with ur");
        System.out.println("个险失效批处理查询SQL:" + tStringBuffer.toString());
        try {
            if (!rsWrapper.prepareData(tLCPolSet, tStringBuffer.toString())) {
                System.out.println("处理数据准备失败! ");
                return false;
            }
            do {
                rsWrapper.getData();
                if (tLCPolSet.size() > 0 || tLCPolSet != null) {
                    for (int i = 1; i <= tLCPolSet.size(); i++) {
                        LCPolSchema tLCPolSchema = tLCPolSet.get(i).
                                                   getSchema();
                        /*
                         * 宽限期校验：
                          1、无续期，过了宽限期，则置失效
                         2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
                         （zhanggm IT要求作废校验2 cbs00024142 cbs00024143 cbs00024144）
                         */
                        if (!checkLapseDate(tLCPolSchema)) {
                            System.out.println(tLCPolSchema.getPolNo() +
                                               "未过宽限期");
                            continue;
                        }
                        //险种失效
                        if (!PolcyPolDeal(tLCPolSchema)) {
                            System.out.println("个单险种失效处理失败！");
                            continue;
                        }
                        if (!SubmitMap()) {
                            return false;
                        }
                    }

                }
            } while (tLCPolSet != null && tLCPolSet.size() > 0);
        } catch (Exception ex) {
            ex.getMessage();
        } finally {
            rsWrapper.close();
        }
        /*个单保单失效批处理
        根据保单下险种失效情况，设置保单的失效状态：若所有险种失效，则整单失效
        */
       System.out.println("个单保单失效批处理开始……");

       RSWrapper XrsWrapper = new RSWrapper();

       LCContSet tLCContSet = new LCContSet();

       //再处理保单状态表中无数据的保单
       StringBuffer subString = new StringBuffer();
       subString.append(" select * from lccont a where 1=1")
                .append(" and not exists(select 'X' from lccontstate b where b.statetype in ('Available','Terminate') and b.state = '1' ")
                .append(" and b.contno = a.contno and b.polno ='000000' )")
                .append(" and appflag ='1' and conttype ='1' ")
                .append(" and stateflag ='1' and paytodate <'"+mCurrentDate+"'")
//                .append(" and a.ContNo ='000009808000001'")
                .append(""+subSql+" with ur ") ;

       System.out.println(subString.toString());
       try{
           if (!XrsWrapper.prepareData(tLCContSet, subString.toString())) {
               System.out.println("处理数据准备失败! ");
               return false;
           }
           do {
               XrsWrapper.getData();
               for (int y = 1; y <= tLCContSet.size(); y++) {
                   LCContSchema tLCContSchema = tLCContSet.get(y).getSchema();
                   if (!"0".equals(SinglePolicyDeal(tLCContSchema))) {
                       continue;
                   }
                   if (!setPolicyAbate(tLCContSchema,
                                       tLCContSchema.getPaytoDate(),
                                       "Available", "1", "02")) {
                       System.out.println("保单" + tLCContSchema.getContNo() +
                                          "设置失效失败");
                   } else {
                       sendInvaliNotice(tLCContSchema, mManageCom, "42", null);
                   }
                   if (!SubmitMap()) {
                       return false;
                   }
               }
           } while (tLCContSet != null && tLCContSet.size() > 0);
       } catch(Exception ex)
       {
           ex.getMessage();
       }finally{
           XrsWrapper.close();
       }
       return true ;
    }

    /*SQL中取管理结构工具*/
    private String getSQLManageCom() {
        String mSQL = "";
        if (mManageCom != null && !mManageCom.trim().equals("")) {
            if (mManageCom.length() >= 4) {

                mSQL = " and ManageCom like '" + mManageCom.substring(0, 4) +
                       "%'";
            } else {
                mSQL = " and ManageCom like '86%'";
            }
        } else {
            mSQL = " ";
        }
        return mSQL;
    }

    private boolean PolcyPolDeal(LCPolSchema tLCPolSchema)
    {
        setPolAbate(tLCPolSchema, tLCPolSchema.getPaytoDate(),"Available", "1", "02");
        return true;
    }

    /**
     * 宽限期校验：
     * 过了宽限期，则置失效。根据产品描述，取险种的宽限期。
     * @param tLCPolSchema LCPolSchema
     * @return boolean：过可以置失效true
     */
    private boolean checkLapseDate(LCPolSchema tLCPolSchema) {
        FDate tFDate = new FDate();

       //查询宽限期
       String sql = "select max(a.GracePeriod) "
                    + "from LMRiskPay a, LCPol b "
                    + "where a.RiskCode = b.RiskCode "
                    + "   and b.PolNo = '" + tLCPolSchema.getPolNo() + "' "
                    + " and (b.stateflag = '1' or b.StateFlag is null) and b.AppFlag = '1'"
                    ;
       String maxGracePeriod = new ExeSQL().getOneValue(sql);
       if(maxGracePeriod.equals("") || maxGracePeriod.equals("null"))
       {
           System.out.println(tLCPolSchema.getPolNo() + "计算宽限期失败");
               return false;
       }

       LJSPayDB tLJSPayDB = new LJSPayDB();
       tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
       LJSPaySet set = tLJSPayDB.query();

       //1、无续期，过了宽限期止期，则置失效
       if(set.size() == 0)
       {
           String tLapseDate = PubFun.calDate(tLCPolSchema.getPaytoDate(),
                          Integer.parseInt(maxGracePeriod), "D", null);

           if(tFDate.getDate(tLapseDate).after(tFDate.getDate(mCurrentDate)))
           {
               System.out.println(tLCPolSchema.getPolNo() + "无续期未到宽限期止期");
               return false;
           }

           return true;
       }

       //2、有续期，缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个超过当天的，置为失效
       for(int i = 1; i <= set.size(); i++)
       {
           LJSPaySchema schema = set.get(i);

           //若正电话催缴、正转账，不置状态
           if("2".equals(schema.getBankOnTheWayFlag())
               || "1".equals(schema.getBankOnTheWayFlag()))
           {
               return false;
           }

           if(tFDate.getDate(schema.getPayDate()).after(tFDate.getDate(mCurrentDate)))
           {
               System.out.println(tLCPolSchema.getPolNo() + "续期未到缴费截止期");
               return false;
           }

           String maxDate = PubFun.calDate(schema.getStartPayDate(),
                                           Integer.parseInt(maxGracePeriod),
                                           "D", null);
           //缴至日期和首次缴费日+宽限期(长期险60天，短期险30天)比较大的一个
           if(tFDate.getDate(PubFun.calDate(tLCPolSchema.getPaytoDate(),
                                           Integer.parseInt(maxGracePeriod),
                                           "D", null)).after(tFDate.getDate(maxDate)))
           {
              // maxDate = tLCPolSchema.getPaytoDate();
              maxDate = PubFun.calDate(tLCPolSchema.getPaytoDate(),
                                           Integer.parseInt(maxGracePeriod),
                                           "D", null);

           }
           //超过当天的，置为失效
           if(tFDate.getDate(maxDate).after(tFDate.getDate(mCurrentDate)))
           {
               System.out.println(tLCPolSchema.getPolNo() + "续期未到宽限期止期");
               return false;
           }
       }
       return true;

    }


    /*数据处理*/
    private boolean SubmitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) {
            System.out.println(mLCPolSchema.getPolNo() + "提交数据失败！");
        }
        this.mMap = new MMap();
        return true;
    }

    /*设置个人险种失效*/
    public boolean setPolAbate(LCPolSchema tLCPolSchema,
                               String tEndDate,
                               String tStateType,
                               String tState, String tStateReason
            ) {
        if (tLCPolSchema == null || tEndDate == null ||
            tStateType == null || tState == null ||
            tEndDate.trim().equals("") ||
            tStateType.trim().equals("") ||
            tState.trim().equals("")) {
            return false;
        }
        //新状态起始日期为旧状态结束日期次日
        String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);

        LCContStateSchema tLCContStateSchema = new LCContStateSchema();

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() != 0) {
            tLCContStateSchema
                    .setOtherNoType(tLJSPaySet.get(1).getOtherNoType());
            tLCContStateSchema
                    .setOtherNo(tLJSPaySet.get(1).getGetNoticeNo());
        }

        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
        tLCContStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());
        tLCContStateSchema.setStateType(tStateType);
        tLCContStateSchema.setStateReason(tStateReason);
        tLCContStateSchema.setState(tState);
        tLCContStateSchema.setOperator("000");
        tLCContStateSchema.setStartDate(tStartDate);
        tLCContStateSchema.setMakeDate(mCurrentDate);
        tLCContStateSchema.setMakeTime(mCurrentTime);
        tLCContStateSchema.setModifyDate(mCurrentDate);
        tLCContStateSchema.setModifyTime(mCurrentTime);
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
        SetPolState(tLCPolSchema, "2");

        return true;
    }

    /*个单险种设置状态*/
    private boolean SetPolState(LCPolSchema tLCPolSchema, String StateFlag) {
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + mCurrentDate
                           + "',ModifyTime = '" + mCurrentTime +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        mMap.put(updatepol, "UPDATE");
        return true;
    }

    /*个单保单失效批处理*/
    private String SinglePolicyDeal(LCContSchema tLCContSchema) {
            //若所有险种都终止或失效，则保单失效
            String sql = "select count(1) from LCPol "
                         + "where StateFlag ='1'"
                         + "   and ContNo ='" + tLCContSchema.getContNo() +
                         "' ";
            String countStr = tExeSQL.getOneValue(sql);
//        if (!SubmitMap()) {
//            return false;
//        }
        return countStr;
    }

    /*设置个险保单失效*/
    public boolean setPolicyAbate(LCContSchema tLCContSchema, String tEndDate,
                                  String tStateType, String tState,
                                  String tStateReason) {
        if (tLCContSchema == null || tEndDate == null ||
            tStateType == null || tState == null ||
            tEndDate.trim().equals("") ||
            tStateType.trim().equals("") ||
            tState.trim().equals("")) {
            return false;
        }
        //新状态起始日期为旧状态结束日期次日
        String tStartDate = PubFun.calDate(tEndDate, 1, "D", null);
        LCContStateDB tLCContStateDB = new LCContStateDB();
        LCContStateSet tLCContStateSet = new LCContStateSet();
        LCContStateSchema oldLCContStateSchema = new LCContStateSchema();
        PubSubmit tPubSubmit = new PubSubmit();
        VData tData = new VData();
        if (!tStateType.equals("Available")) {
            String strSql =
                    " select * from lccontstate where statetype = 'Available' " +
                    " and enddate is null and contno = '" +
                    tLCContSchema.getContNo() +
                    "' and polno = '000000'";
            tLCContStateSet = tLCContStateDB.executeQuery(strSql);
            if (!tLCContStateDB.mErrors.needDealError()) {
                if (tLCContStateSet != null && tLCContStateSet.size() > 0) {
                    oldLCContStateSchema = tLCContStateSet.get(1).getSchema();
                    oldLCContStateSchema.setEndDate(tEndDate);
                    oldLCContStateSchema.setStateReason(tStateReason);
                    oldLCContStateSchema.setModifyDate(mCurrentDate);
                    oldLCContStateSchema.setModifyTime(mCurrentTime);
                    mMap.put(oldLCContStateSchema, "DELETE&INSERT");
                }
            } else {
                System.out.println("保单状态查询失败");
                return false;
            }
        }
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();
        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(tLCContSchema.getContNo());
        tLCContStateSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        tLCContStateSchema.setPolNo("000000");
        tLCContStateSchema.setStateType(tStateType);
        tLCContStateSchema.setStateReason(tStateReason);
        if (mLJSPaySchema != null) {
            tLCContStateSchema.setOtherNo(mLJSPaySchema.getGetNoticeNo());
            tLCContStateSchema.setOtherNoType(mLJSPaySchema.
                                              getOtherNoType());
        }
        tLCContStateSchema.setState(tState);
        tLCContStateSchema.setOperator("000");
        tLCContStateSchema.setStartDate(tStartDate);
        tLCContStateSchema.setMakeDate(mCurrentDate);
        tLCContStateSchema.setMakeTime(mCurrentTime);
        tLCContStateSchema.setModifyDate(mCurrentDate);
        tLCContStateSchema.setModifyTime(mCurrentTime);
        mMap.put(tLCContStateSchema, "DELETE&INSERT");
        if (tStateType.equals("Available")) {
            SetContState(tLCContSchema, "2");
        }
        return true;
    }

    /*个单保单设置状态*/
    private boolean SetContState(LCContSchema tLCContSchema, String StateFlag) {
        String updatecont = "update LCCont Set StateFlag = '"
                            + StateFlag + "',ModifyDate = '" + mCurrentDate
                            + "',ModifyTime = '" + mCurrentTime +
                            "' where Contno = '" + tLCContSchema.getContNo() +
                            "'";

        mMap.put(updatecont, "UPDATE");

        return true;
    }

    //发失效通知书
    private boolean sendInvaliNotice(LCContSchema tLCContSchema, String tCom,
                                     String tCode, String tPayDate) {
//        String tStartDate = PubFun.calDate(mEnd, 1, "D", null);

        if (tPayDate == null) {
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setOtherNo(tLCContSchema.getContNo());
            LJSPaySet set = tLJSPayDB.query();
            if (set.size() > 0) {
                mLJSPaySchema = set.get(1);
                tPayDate = mLJSPaySchema.getPayDate();
            }
        }

        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("XXNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //42为失效通知书的代码
        tLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        tLOPRTManagerSchema.setStandbyFlag1(tLCContSchema.getPaytoDate()); //失效日期=交至日期
        tLOPRTManagerSchema.setStandbyFlag2(tPayDate);
        if (mLJSPaySchema != null) {
            tLOPRTManagerSchema.setStandbyFlag3(mLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag4(mLJSPaySchema.getOtherNoType());
        }
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }

    /***************************个单满期终止处理********************************/
    private boolean callSingleTerminate() {
        System.out.println("个单险种满期终止批处理运行开始。。。。");
        String subSql = getSQLManageCom();
        StringBuffer strSql = new StringBuffer();
        LCPolSet tLCPolSet = new LCPolSet();


        RSWrapper rsWrapper = new RSWrapper();

        strSql.append(" select * from LCPol a where  1=1 ")
              .append(" and not exists(select 'X' from lccontstate where  enddate is null and state = '1' and  statetype = 'Terminate' and polno = a.polno) ") //失效状态下不参与满期终止
              .append(" and a.AppFlag = '1' and conttype = '1' and a.EndDate <= '"+mCurrentDate+"'")
              .append(" and riskcode not in (select riskcode from lmriskapp where risktype4 = '4')")
              .append(" and riskcode not in (select code from ldcode1 where exists (select 1 from lmriskapp where riskcode = ldcode1.code1 and risktype4= '4')) ")
              .append(" and not exists (select 1 from lpedorapp lpapp,lpedoritem lpitem where lpapp.edoracceptno = lpitem.edoracceptno and lpapp.edorstate !='0' and a.contno = lpitem.contno) ") //不存在未结案的保全
              .append(" and not exists (select 1 from ljspayb,ljspay where a.contno = ljspay.otherno and ljspayb.getnoticeno = ljspay.getnoticeno and dealstate = '4')")
              .append(" and not exists (select 1 from ljspay  where a.contno = ljspay.otherno and bankonthewayflag = '1') ")
              .append(""+subSql+" with ur");

        System.out.println("Sql ：" + strSql.toString());
        try{
            if (!rsWrapper.prepareData(tLCPolSet, strSql.toString())) {
                System.out.println("处理数据准备失败! ");
                return false;
            }

            do {
                rsWrapper.getData();
                //逐条调用保单失效处理
                for (int i = 1; i <= tLCPolSet.size(); i++) {
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i).getSchema();
                    if (!canTerminate(tLCPolSchema)) {
                        continue;
                    }
                    if (!SingleTerminateDeal(tLCPolSchema)) {
                        continue;
                    }
                    if (!SubmitMap()) {
                        return false;
                    }
                }
            } while (tLCPolSet != null || tLCPolSet.size() > 0);
        }catch(Exception ex)
        {
            ex.getMessage();
        }
        finally{
            rsWrapper.close(); //关闭rsWrapper
        }
        System.out.println("个单保单满期终止批处理运行开始。。。。");


        LCContSet tLCContSet = new LCContSet();
        RSWrapper xrsWrapper = new RSWrapper();
        //查询未失效、无应收的个单
        StringBuffer SqlCont = new StringBuffer();

        SqlCont.append("select * from LCCont a where  1=1 " )
               .append(" and not exists(select 'X' from lccontstate where  enddate is null and state = '1' and  statetype = 'Terminate' and contno = a.contno and polno = '000000') " ) //失效状态下不参与满期终止
               .append(" and a.AppFlag = '1' and conttype = '1' and a.cinvalidate <= '"+mCurrentDate + "' ")
               .append(" and not exists(select 'X' from ljspayperson where contno = a.contno)" )
               .append(""+subSql + " with ur");

        System.out.println("Sql ：" + SqlCont.toString());
        try{
            if (!xrsWrapper.prepareData(tLCContSet, SqlCont.toString())) {
                System.out.println("处理数据准备失败! ");
                return false;
            }

            do {
                xrsWrapper.getData();
                //保单满期终止处理
                for (int j = 1; j <= tLCContSet.size(); j++) {
                    LCContSchema tLCContSchema = tLCContSet.get(j).getSchema();
                    if (SinglePolicyTerminateDeal(tLCContSchema)) {
                        return false;
                    }
                    if (!SubmitMap()) {
                        return false;
                    }
                }
            } while (tLCContSet != null || tLCContSet.size() > 0);
        }catch(Exception ex)
        {
            ex.getMessage();
        }
        finally{
            xrsWrapper.close();
        }
        System.out.println("满期终止批处理运行结束。。。。");

        return true;
    }

    private boolean SingleTerminateDeal(LCPolSchema tLCPolSchema) {
        LCContStateDB tLCContStateDB = new LCContStateDB();
        LCContStateSet tLCContStateSet = new LCContStateSet();
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();

//        boolean mainPolFlag = false;

            tLCContStateDB.setContNo(tLCPolSchema.getContNo());
            tLCContStateDB.setPolNo(tLCPolSchema.getPolNo());
            tLCContStateDB.setStateType("Available");
            tLCContStateSet = tLCContStateDB.query();
            if (!tLCContStateDB.mErrors.needDealError()) {
                if (tLCContStateSet != null && tLCContStateSet.size() > 0) {
                    tLCContStateSchema = tLCContStateSet.get(1).getSchema();
                    tLCContStateSet.get(1).setEndDate(tLCPolSchema.getEndDate());
                    mMap.put(tLCContStateSet.get(1), "DELETE&INSERT");
                } else {
                    tLCContStateSchema = new LCContStateSchema();
                    tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
                    tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());

                    tLCContStateSchema.setGrpContNo("000000");
                    String mInsuredNo = "";
                    if (tLCPolSchema.getInsuredNo().trim().equals("")) {
                        mInsuredNo = "000000";
                    } else {
                        mInsuredNo = tLCPolSchema.getInsuredNo();
                    }
                    tLCContStateSchema.setInsuredNo(mInsuredNo);
                    tLCContStateSchema.setStateType("Terminate");
                    tLCContStateSchema.setOtherNoType("");
                    tLCContStateSchema.setOtherNo("");
                    tLCContStateSchema.setState("1");
                    tLCContStateSchema.setStateReason("01");
                    tLCContStateSchema.setStartDate(tLCPolSchema.getEndDate());
                    tLCContStateSchema.setEndDate("");
                    tLCContStateSchema.setRemark("");
                    tLCContStateSchema.setOperator("000");
                    tLCContStateSchema.setMakeDate(mCurrentDate);
                    tLCContStateSchema.setMakeTime(mCurrentTime);
                    tLCContStateSchema.setModifyDate(mCurrentDate);
                    tLCContStateSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLCContStateSchema, "DELETE&INSERT");
                    SetLCPolState(tLCPolSchema, "3", mMap);
                }
                System.out.println(tLCPolSchema.getPolNo() + "满期终止");
        }
        return true;
    }

    /**
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean canTerminate(LCPolSchema tLCPolSchema) {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLCPolSchema.getContNo());
        LJSPaySet set = tLJSPayDB.query();
        for (int i = 1; i <= set.size(); i++) {
            LJSPaySchema schema = set.get(i);

            //若正电话催缴、正转账，不置状态
            if ("2".equals(schema.getBankOnTheWayFlag())
                || "1".equals(schema.getBankOnTheWayFlag())) {
                return false;
            }
        }

        /*个单一年期险种只有以下情况才会终止，其中1和2由续保自动核保程序校验，若不能续保，将险种预标记为续保终止（此标记非保单状态），满期后批处理程序置为满期终止。
         * 1)、由于年龄限制不可能再续保，即到达续保年龄上限。此种情况在满期失效程序中不进行判断，由续保自动核保程序校验，若不能续保，将险种预标记为续保终止（此标记非保单状态），满期后批处理程序置为满期终止。
         * 2)、险种（提前）已被置了续保终止标示，如客户已经提了续保终止申请，或抽档后的应收记录已被置为应收撤销。
         */
        String sql = "select RiskPeriod from LMRiskApp where riskCode = '"
                     + tLCPolSchema.getRiskCode() + "' ";
        String riskPeriod = tExeSQL.getOneValue(sql);
        if (!"L".equals(riskPeriod)) {
            //若险种PolState状态为续保终止，则需要置终止
            if (BQ.POLSTATE_XBEND.equals(tLCPolSchema.getPolState())) {
                return true;
            }
            return false;
        }

        return true;
    }

    /*险种状态设置*/
    private boolean SetLCPolState(LCPolSchema tLCPolSchema, String StateFlag,
                                  MMap tMap) {
        String updatepol = "update LCPol Set StateFlag = '"
                           + StateFlag + "',ModifyDate = '" + mCurrentDate
                           + "',ModifyTime = '" + mCurrentTime +
                           "' where PolNo = '" + tLCPolSchema.getPolNo() + "'";
        mMap.put(updatepol, "UPDATE");
        return true;
    }

    private boolean SinglePolicyTerminateDeal(LCContSchema tLCContSchema) {

            //若保单没有未终止的险种，则保单终止
            String sql = "select count(1) from LCPol a "
                         + "where ContNo = '"
                         + tLCContSchema.getContNo() + "' "
                         + "   and (StateFlag is null or StateFlag != '"
                         + BQ.STATE_FLAG_TERMINATE + "') ";
            String available = tExeSQL.getOneValue(sql);
            if ("0".equals(available)) {
                if (!setPolicyAbate(tLCContSchema,
                                    tLCContSchema.getCInValiDate(),
                                    "Terminate", "1", "01")) {
                    System.out.println("保单" + tLCContSchema.getContNo() +
                                       "设置失效失败");
                } else {
                    sendTerminateNotice(tLCContSchema, mManageCom, "21");
                }
        }

        return true;
    }

    /*发送满期终止通知书*/
    private boolean sendTerminateNotice(LCContSchema tLCContSchema, String tCom,
                                        String tCode) {
        String tLimit = PubFun.getNoLimit(tCom);
        String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(tCode); //42为失效通知书的代码
        tLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(tCom);
        tLOPRTManagerSchema.setReqOperator("000");
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setStandbyFlag1(tLCContSchema.getCInValiDate()); //tLCContSchema.getCInValiDate()=保单保障满期日+1天
        tLOPRTManagerSchema.setStandbyFlag2(tLCContSchema.getCInValiDate());
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }



    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("00001328901");
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

        PPolicyAbateDealBL tPPolicyAbateDealBL = new PPolicyAbateDealBL();

        tPPolicyAbateDealBL.submitData(mVData, "Available");
//        tPPolicyAbateDealBL.submitData(mVData, "Terminate");
    }
}
