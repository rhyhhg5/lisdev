package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 客户资料变更</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class PEdorJMDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private DetailDataQuery mQuery = null;

    private GlobalInput mGlobalInput = null;

    private String mTypeFlag = null;

    private String stateFlag = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mCustomerNo = null;
    
    private String mName = null;

    private String mSex = null;

    private String mBirthday = null;

//    private String mCValiDate = null;

    private String mIdNo = null;
    
    private String mMobile = null;
    
    private String mPhone = null;
    
    private String mPostalAddress = null;
    
    private String mZipCode = null;
    
    private String mEMail = null;
    
    private String mIDType = null;
    
    private String mOccupationCode = null;
    
    private String mOccupationType = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
    	System.out.println("Begin --------PEdorJMDetailBL");
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())//检验数据未处理
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTypeFlag = (String) cInputData.get(0);
            mCustomerNo = (String) cInputData.get(1);
            TransferData mTransferData = new TransferData();
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
            if(mTransferData != null)
            {
            	stateFlag = (String) mTransferData.getValueByName("stateFlag");
            }           
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            LICardActiveInfoListSchema tLICardActiveInfoListSchema = (LICardActiveInfoListSchema)
                    cInputData.getObjectByObjectName("LICardActiveInfoListSchema", 0);

            this.mEdorNo = tLPEdorItemSchema.getEdorNo();
            this.mEdorType = tLPEdorItemSchema.getEdorType();
            this.mContNo = tLPEdorItemSchema.getContNo();
            this.mCustomerNo = mCustomerNo;
            this.mName = tLICardActiveInfoListSchema.getName();
            this.mSex = tLICardActiveInfoListSchema.getSex();
            this.mBirthday = tLICardActiveInfoListSchema.getBirthday();          
//            this.mCValiDate=tLICardActiveInfoListSchema.getCValidate();
            this.mIdNo=tLICardActiveInfoListSchema.getIdNo();
            this.mMobile=tLICardActiveInfoListSchema.getMobile();
            this.mPhone=tLICardActiveInfoListSchema.getPhone();           
            this.mPostalAddress=tLICardActiveInfoListSchema.getPostalAddress();
            this.mZipCode=tLICardActiveInfoListSchema.getZipCode();
            this.mEMail=tLICardActiveInfoListSchema.getEMail();
            this.mIDType = tLICardActiveInfoListSchema.getIdType();
            this.mOccupationCode= tLICardActiveInfoListSchema.getOccupationCode();
            this.mOccupationType = tLICardActiveInfoListSchema.getOccupationType();
            
            this.mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    /*
     * 
     * */
    private String getOccupationType(String cOccupationCode){
    	String sql="select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"+cOccupationCode+"'  fetch first 2000 rows only ";
    	ExeSQL tExeSQL = new ExeSQL();     
    	String tOccupationType= tExeSQL.getOneValue(sql);
    	return tOccupationType;
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if ((mCustomerNo == null) || (mCustomerNo.equals("")))
        {
            mErrors.addOneError("客户号为空！");
            return false;
        } 
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	setLPEdoreSpecialData();
        setLPPerson();
        setGrpLPCont();
        setGrpLPInsured();
        setGrpLPPol();
//        setLICardActiveInfoList();
        setLICertifyInsured();
//        setLICertify();
//        setOtherLP();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }
    
    /**
     * 校验是否为激活卡
     * @return boolean
     */
    private boolean IsCard(){
    	ExeSQL texesql=new ExeSQL();
    	String tcard=texesql.getOneValue("select 1 from lccont where  contno='"+mContNo+"' and exists (select 1 from lcgrpcont where grpcontno=lccont.grpcontno and cardflag='2') and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02') ");
    	if ((tcard == null) || (tcard.equals(""))) {
			return false;
		}else
		{
    	return true;}
    }
   
    private void setLPEdoreSpecialData()
    {
    	setLPEdoreSpecialData("Name",mName);
    	setLPEdoreSpecialData("Sex",mSex);
    	setLPEdoreSpecialData("Birthday",mBirthday);
    	//setLPEdoreSpecialData("CValiDate",mCValiDate);
    	setLPEdoreSpecialData("IdNo",mIdNo);
    	setLPEdoreSpecialData("Mobile",mMobile);
    	setLPEdoreSpecialData("Phone",mPhone);
    	setLPEdoreSpecialData("PostalAddress",mPostalAddress);
    	setLPEdoreSpecialData("ZipCode",mZipCode);
    	setLPEdoreSpecialData("EMail",mEMail);
    	setLPEdoreSpecialData("IDType",mIDType);
    	setLPEdoreSpecialData("OccupationCode",mOccupationCode);
    	setLPEdoreSpecialData("OccupationType",mOccupationType);
	
    }
    private void setLPEdoreSpecialData(String ZiDuanName,String value)
    {
    	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=new LPEdorEspecialDataSchema();
    	tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
    	tLPEdorEspecialDataSchema.setEdorType(mEdorType);
    	tLPEdorEspecialDataSchema.setPolNo("000000");
    	tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
    	tLPEdorEspecialDataSchema.setDetailType(ZiDuanName);
    	tLPEdorEspecialDataSchema.setEdorValue(value);
    	mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
    }
    

    /**
     * 设置LPPerson表
     */
    private void setLPPerson()
    {
    	LPPersonSchema tLPPersonSchema = (LPPersonSchema)
        mQuery.getDetailData("LDPerson", mCustomerNo);
    	
    	if(tLPPersonSchema!=null)
    	{
 
            tLPPersonSchema.setEdorNo(mEdorNo);
            tLPPersonSchema.setEdorType(mEdorType);
            tLPPersonSchema.setName(mName);
            tLPPersonSchema.setSex(mSex);
            tLPPersonSchema.setBirthday(mBirthday);
            tLPPersonSchema.setIDType(mIDType);
            tLPPersonSchema.setOccupationType(mOccupationType);
            tLPPersonSchema.setOccupationCode(mOccupationCode);
            tLPPersonSchema.setIDNo(mIdNo);          
            tLPPersonSchema.setOperator(mGlobalInput.Operator);
            tLPPersonSchema.setModifyDate(mCurrentDate);
            tLPPersonSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPersonSchema, "DELETE&INSERT");
 
    	}

        
    } 
    /**
     * 设置LPCont表
     */
    private void setGrpLPCont()
    {
        LPContSchema tLPContSchema = (LPContSchema)
                mQuery.getDetailData("LCCont", mContNo);
        //如果是连带被保人则不操作
        if(tLPContSchema!=null)
        {
	        if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
	        {
	            return;
	        }
	        tLPContSchema.setEdorNo(mEdorNo);
	        tLPContSchema.setEdorType(mEdorType);
	        tLPContSchema.setInsuredName(mName);
	        tLPContSchema.setInsuredSex(mSex);
	        tLPContSchema.setInsuredBirthday(mBirthday);
//	        tLPContSchema.setCValiDate(mCValiDate);
	        tLPContSchema.setInsuredIDNo(mIdNo);
	        tLPContSchema.setInsuredIDType(mIDType);
	        tLPContSchema.setOperator(mGlobalInput.Operator);
	        tLPContSchema.setModifyDate(mCurrentDate);
	        tLPContSchema.setModifyTime(mCurrentTime);
	        mMap.put(tLPContSchema, "DELETE&INSERT");
        }
    }

    /**
     * 设置LPInsured表
     */
    private void setGrpLPInsured()
    {
        String[] keys = new String[2];
        keys[0] = mContNo;
        keys[1] = mCustomerNo;
        LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)mQuery.getDetailData("LCInsured", keys);
        if(tLPInsuredSchema!=null)
        {
	        tLPInsuredSchema.setName(mName);
	        tLPInsuredSchema.setSex(mSex);
	        tLPInsuredSchema.setBirthday(mBirthday);
	        tLPInsuredSchema.setIDNo(mIdNo);
	        tLPInsuredSchema.setIDType(mIDType);
	        tLPInsuredSchema.setOccupationCode(mOccupationCode);
	        tLPInsuredSchema.setOccupationType(mOccupationType);
	        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
	        tLPInsuredSchema.setModifyDate(mCurrentDate);
	        tLPInsuredSchema.setModifyTime(mCurrentTime);
	        mMap.put(tLPInsuredSchema, "DELETE&INSERT");
        }
    }

    /**
     * 设置LPPol表
     */
    private void setGrpLPPol()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        tLCPolDB.setInsuredNo(mCustomerNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = (LPPolSchema) mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
            if(tLPPolSchema!=null)
            {
            	tLPPolSchema.setEdorNo(mEdorNo);
            	tLPPolSchema.setEdorType(mEdorType);
	            tLPPolSchema.setInsuredName(mName);
	            tLPPolSchema.setInsuredSex(mSex);
	            tLPPolSchema.setInsuredBirthday(mBirthday);
//	            tLPPolSchema.setCValiDate(mCValiDate);
	            tLPPolSchema.setOperator(mGlobalInput.Operator);
	            tLPPolSchema.setModifyDate(mCurrentDate);
	            tLPPolSchema.setModifyTime(mCurrentTime);
	            mMap.put(tLPPolSchema, "DELETE&INSERT");
            }
        }
    }
   /* 
    private void setLICardActiveInfoList()
    {
    	    String[] keys = new String[2];
    	    keys[0] = mContNo;
    	    keys[1] = "1";
    	    //还未建表  
    	    LPCardActiveInfoListSchema tLPCardActiveInfoListSchema = (LPCardActiveInfoListSchema)mQuery.getDetailData("LICardActiveInfoList", keys);  
    	    if(tLPCardActiveInfoListSchema!=null)
    	    {
    	    	tLPCardActiveInfoListSchema.setName(mName);
    	    	tLPCardActiveInfoListSchema.setSex(mSex);
    	    	tLPCardActiveInfoListSchema.setBirthday(mBirthday);
    	    	tLPCardActiveInfoListSchema.setIdNo(mIdNo);
    	    	tLPCardActiveInfoListSchema.setCValidate(mCValiDate);
    	    	tLPCardActiveInfoListSchema.setMobile(mMobile);
    	    	tLPCardActiveInfoListSchema.setPhone(mPhone);
    	    	tLPCardActiveInfoListSchema.setPostalAddress(mPostalAddress);
    	    	tLPCardActiveInfoListSchema.setEMail(mEMail);
    	    	tLPCardActiveInfoListSchema.setZipCode(mZipCode);
    	    	tLPCardActiveInfoListSchema.setOperator(mGlobalInput.Operator);
    	    	tLPCardActiveInfoListSchema.setModifyDate(mCurrentDate);
    	    	tLPCardActiveInfoListSchema.setModifyTime(mCurrentTime);
    	        mMap.put(tLPCardActiveInfoListSchema, "DELETE&INSERT");
    	    }
    
    }
*/
    private void setLICertifyInsured()
    {
    	    String[] keys = new String[2];
//    	    添加多被保人的支持
    	    String seqno=new ExeSQL().getOneValue("select sequenceno from licertifyinsured where cardno='"+mContNo+"' and customerno='"+mCustomerNo+"'");
//    	    keys[0] = "1";
    	    keys[0] = seqno;
    	    keys[1] =mContNo;
    	    LPCertifyInsuredSchema tLPCertifyInsuredSchema = (LPCertifyInsuredSchema)mQuery.getDetailData("LICertifyInsured", keys);  
    	    if(tLPCertifyInsuredSchema!=null)
    	    {
    	    	tLPCertifyInsuredSchema.setName(mName);
    	    	tLPCertifyInsuredSchema.setSex(mSex);
    	    	tLPCertifyInsuredSchema.setBirthday(mBirthday);
    	    	tLPCertifyInsuredSchema.setIdNo(mIdNo);
    	    	tLPCertifyInsuredSchema.setIdType(mIDType);
    	    	tLPCertifyInsuredSchema.setOccupationCode(mOccupationCode);
    	    	tLPCertifyInsuredSchema.setOccupationType(mOccupationType);
    	    	tLPCertifyInsuredSchema.setMobile(mMobile);
    	    	tLPCertifyInsuredSchema.setPhone(mPhone);
    	    	tLPCertifyInsuredSchema.setPostalAddress(mPostalAddress);
    	    	tLPCertifyInsuredSchema.setEMail(mEMail);
    	    	tLPCertifyInsuredSchema.setZipCode(mZipCode);
    	    	tLPCertifyInsuredSchema.setOperator(mGlobalInput.Operator);
    	    	tLPCertifyInsuredSchema.setModifyDate(mCurrentDate);
    	    	tLPCertifyInsuredSchema.setModifyTime(mCurrentTime);
    	        mMap.put(tLPCertifyInsuredSchema, "DELETE&INSERT");
    	    }
    }
    
//    private void setLICertify()
//    {
//    	String updateLICertify="update LICertify set CValidate='"+mCValiDate
//    	                      +"',modifydate=current date,modifytime=current time where cardno='"+mContNo+"'"; 
//    	mMap.put(updateLICertify, "UPDATE");
//    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sqlitem= "update  LPEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and InsuredNo = '" + mCustomerNo + "'";
        mMap.put(sqlitem, "UPDATE");
        String sqlgrpitem = "update  LPgrpEdorItem " +
		        "set EdorState = '" + edorState + "', " +
		        "    Operator = '" + mGlobalInput.Operator + "', " +
		        "    ModifyDate = '" + mCurrentDate + "', " +
		        "    ModifyTime = '" + mCurrentTime + "' " +
		        "where  EdorNo = '" + mEdorNo + "' " +
		        "and EdorType = '" + mEdorType + "' " ;
        mMap.put(sqlgrpitem, "UPDATE");
        String sqlgrpmain = "update  LPgrpEdormain " +
	        "set EdorState = '" + edorState + "', " +
	        "    Operator = '" + mGlobalInput.Operator + "', " +
	        "    ModifyDate = '" + mCurrentDate + "', " +
	        "    ModifyTime = '" + mCurrentTime + "' " +
	        "where  EdorNo = '" + mEdorNo + "' " ;
        mMap.put(sqlgrpmain, "UPDATE");
        String sqlapp = "update  LPEdorapp " +
	        "set EdorState = '" + edorState + "', " +
	        "    Operator = '" + mGlobalInput.Operator + "', " +
	        "    ModifyDate = '" + mCurrentDate + "', " +
	        "    ModifyTime = '" + mCurrentTime + "' " +
	        "where  EdoracceptNo = '" + mEdorNo + "' "  ;
         mMap.put(sqlapp, "UPDATE");
        String sqlmain = "update  LPEdormain " +
	         "set EdorState = '" + edorState + "', " +
	         "    Operator = '" + mGlobalInput.Operator + "', " +
	         "    ModifyDate = '" + mCurrentDate + "', " +
	         "    ModifyTime = '" + mCurrentTime + "' " +
	         "where  EdorNo = '" + mEdorNo + "' " ;
        mMap.put(sqlmain, "UPDATE");
    }
  
/**
 * 同时处理该客户作为其它团单被保人的资料变更
 * 20090320 zhanggm cbs00024868 
 */
private void setOtherLP()
{
	StringBuffer sql = new StringBuffer();
	sql.append("select contno from lcinsured a where insuredno = '" +mCustomerNo + "' ");
	sql.append("and grpcontno <> '" + BQ.GRPFILLDATA + "' ");
	sql.append("and contno <> '" + mContNo + "'");
	sql.append("and exists (select 1 from lccont where contno = a.contno and appflag = '1') ");
	System.out.println("查询关联保单："+sql);
	SSRS tSSRS = new SSRS();
	tSSRS = new ExeSQL().execSQL(sql.toString());
	
	if(tSSRS.MaxRow==0)
	{
		System.out.println("被保人"+mCustomerNo+"没有其它关联团单需要变更");
		return;
	}
	else
	{
		for(int i=1;i<=tSSRS.MaxRow;i++)
		{
			String tContNo = tSSRS.GetText(i, 1);
			System.out.println("PEdorCMDetailBL.java-L613->保单："+tContNo);
			setOtherGrpLPCont(tContNo);
			setOtherGrpLPInsured(tContNo);
			setOtherGrpLPPol(tContNo);
		}
	}
}

/**
 * 该客户作为其它团单被保人的团单设置LPCont表
 */
private void setOtherGrpLPCont(String aContNo)
{
	LPContSchema tLPContSchema = (LPContSchema)mQuery.getDetailData("LCCont", aContNo);
    //如果是连带被保人则不操作
    if (!mCustomerNo.equals(tLPContSchema.getInsuredNo()))
    {
        return;
    }
    if(tLPContSchema!=null)
    {
    	tLPContSchema.setEdorNo(mEdorNo);
    	tLPContSchema.setEdorType(mEdorType);
		tLPContSchema.setInsuredName(mName);
	    tLPContSchema.setInsuredSex(mSex);
	    tLPContSchema.setInsuredBirthday(mBirthday);
	    tLPContSchema.setInsuredIDNo(mIdNo);
	    tLPContSchema.setInsuredIDType(mIDType);
//	    tLPContSchema.setCValiDate(mCValiDate);
	    tLPContSchema.setOperator(mGlobalInput.Operator);
	    tLPContSchema.setModifyDate(mCurrentDate);
	    tLPContSchema.setModifyTime(mCurrentTime);
	    mMap.put(tLPContSchema, "DELETE&INSERT");
    }
}

/**
 * 该客户作为其它团单被保人的团单设置LPInsured表
 */
private void setOtherGrpLPInsured(String aContNo)
{
    String[] keys = new String[2];
    keys[0] = aContNo;
    keys[1] = mCustomerNo;
    LPInsuredSchema tLPInsuredSchema = (LPInsuredSchema)mQuery.getDetailData("LCInsured", keys);  
    if(tLPInsuredSchema!=null)
    {
        tLPInsuredSchema.setName(mName);
        tLPInsuredSchema.setSex(mSex);
        tLPInsuredSchema.setBirthday(mBirthday);
        tLPInsuredSchema.setIDNo(mIdNo);
        tLPInsuredSchema.setIDType(mIDType);
        tLPInsuredSchema.setOccupationCode(mOccupationCode);
        tLPInsuredSchema.setOccupationType(mOccupationType);
        tLPInsuredSchema.setOperator(mGlobalInput.Operator);
        tLPInsuredSchema.setModifyDate(mCurrentDate);
        tLPInsuredSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsuredSchema, "DELETE&INSERT");
    }
}

/**
 * 该客户作为其它团单被保人的团单设置LPPol表
 */
private void setOtherGrpLPPol(String aContNo)
{
	LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(aContNo);
    tLCPolDB.setInsuredNo(mCustomerNo);
    LCPolSet tLCPolSet = tLCPolDB.query();
    for (int i = 1; i <= tLCPolSet.size(); i++)
    {
        LCPolSchema tLPolSchema = tLCPolSet.get(i);
        LPPolSchema tLPPolSchema = (LPPolSchema)mQuery.getDetailData("LCPol", tLPolSchema.getPolNo());
        if(tLPPolSchema!=null)
        {
        	tLPPolSchema.setEdorNo(mEdorNo);
        	tLPPolSchema.setEdorType(mEdorType);
            tLPPolSchema.setInsuredName(mName);
            tLPPolSchema.setInsuredSex(mSex);
            tLPPolSchema.setInsuredBirthday(mBirthday);
//            tLPPolSchema.setCValiDate(mCValiDate);
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");
        }
    }
}

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
