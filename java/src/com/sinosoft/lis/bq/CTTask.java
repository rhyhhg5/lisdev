package com.sinosoft.lis.bq;

import java.util.Set;
import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;

/**
 * <p>Title: 解约任务处理</p>
 * <p>Description: 解约任务处理 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class CTTask
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mRemark = "";

    private int mPolSize = 0;

    private boolean mHasCreateTask = false;

    private Map mContNoMap = new HashMap();

    private List mWorkList = new ArrayList();

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public CTTask(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
    }

    /**
     * 添加一个需解约的险种
     * @param contNo String
     * @param polNo String
     */
    public void addCTPol(LCPolSchema aLCPolSchema)
    {
        String contNo = aLCPolSchema.getContNo();
        if (!mContNoMap.containsKey(contNo))
        {
            LCPolSet tLCPolSet = new LCPolSet();
            tLCPolSet.add(aLCPolSchema);
            mContNoMap.put(contNo, tLCPolSet);
        }
        else
        {
            LCPolSet tLCPolSet = (LCPolSet) mContNoMap.get(contNo);
            tLCPolSet.add(aLCPolSchema);
        }
        mPolSize ++;
    }

    /**
     * 生成解约工单任务
     * @return boolean
     */
    public boolean createCTTask()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setInnerSource(mEdorNo);
        LGWorkSet tLGWorkSet = tLGWorkDB.query();
        if (tLGWorkSet.size() > 0)
        {
            mHasCreateTask = true; //已生成关联工单则不在生成新的
        }
        for (int i = 1; i <= tLGWorkSet.size(); i++)
        {
            mWorkList.add(tLGWorkSet.get(i).getWorkNo());
        }
        Set contNoSet = mContNoMap.keySet();
        for (Iterator iter = contNoSet.iterator(); iter.hasNext();)
        {
            String contNo = (String) iter.next();
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(contNo);
            if (!tLCAppntDB.getInfo())
            {
                mErrors.addOneError("未找到保单" + contNo + "投保人信息！");
                return false;
            }
            String appntNo = tLCAppntDB.getAppntNo();
            String info = "因人工核保未通过，需做保单" + contNo + "序号";
            LCPolSet tLCPolSet = (LCPolSet) mContNoMap.get(contNo);
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                info += tLCPolSchema.getRiskSeqNo();
                if (i < tLCPolSet.size())
                {
                    info += "，";
                }
            }
            info += "险种的解约操作，关联工单" + mEdorNo + "\n";
            mRemark += info;
            if (mHasCreateTask == false)
            {
                LGWorkSchema tLGWorkSchema = new LGWorkSchema();
                tLGWorkSchema.setCustomerNo(appntNo);
                tLGWorkSchema.setTypeNo("0300006"); //退保
                tLGWorkSchema.setDateLimit("");
                tLGWorkSchema.setApplyTypeNo("");
                tLGWorkSchema.setApplyName("");
                tLGWorkSchema.setPriorityNo("");
                tLGWorkSchema.setAcceptWayNo("");
                tLGWorkSchema.setInnerSource(mEdorNo); //相关联的工单
                tLGWorkSchema.setAcceptDate(PubFun.getCurrentDate());
                tLGWorkSchema.setAcceptCom(mGlobalInput.ManageCom);
                tLGWorkSchema.setAcceptorNo(mGlobalInput.Operator);
                tLGWorkSchema.setRemark(info);
                tLGWorkSchema.setInnerSource(mEdorNo); //和当前保全号相关联

                VData data = new VData();
                data.add(mGlobalInput);
                data.add(tLGWorkSchema);

                TaskInputBL tTaskInputBL = new TaskInputBL();
                if (!tTaskInputBL.submitData(data, "INSERT||MAIN"))
                {
                    mErrors.copyAllErrors(tTaskInputBL.mErrors);
                    return false;
                }
                mWorkList.add(tTaskInputBL.getWorkNo());
            }
        }
        return true;
    }

    public String[] getWorks()
    {
        return (String[]) mWorkList.toArray(new String[0]);
    }

    public String getRemark()
    {
        return mRemark;
    }

    public int getPolSize()
    {
        return mPolSize;
    }
}
