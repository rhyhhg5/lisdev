package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 协议退保项目明细</p>
 * <p>Copyright: Copyright (c) 2005.3.31</p>
 * <p>Company: Sinosoft</p>
 * @author LHS
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class GrpEdorXTDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局基础数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private EdorItemSpecialData mEdorItemSpecialData = null;
    private LPGrpContSchema tLPGrpContSchema = null;
    private LPContPlanRiskSet mLPContPlanRiskSet = new LPContPlanRiskSet();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    public GrpEdorXTDetailBL()
    {
    }


    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(mMap);
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("GrpEdorXTDetailBL End PubSubmit");
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {

        if(!getLPContPlanRisk())
        {
            return false;
        }

        //若选择了特需险的公共账户保障计划，则需选择特需被保人的所有保障计划
        //若现在了特需险的所有被保人保障计划，则需选择公共账户的保障计划
        for(int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            if(!CommonBL.isEspecialPol(mLPContPlanRiskSet.get(i).getRiskCode()))
            {
                continue;
            }
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mLPContPlanRiskSet.get(i)
                                           .getGrpContNo());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i)
                                          .getRiskCode());
            LCContPlanRiskSet set = tLCContPlanRiskDB.query(); //特需险保障计划数
            int selectedPlanCount = 0; //选择了的当前保障计划险种下的保障计划数
            //本保障是否公共帐户保障
            boolean isPublicAccPlan = mLPContPlanRiskSet.get(i).getContPlanCode()
                                      .equals("11");
            boolean hasPublicAccPlan = false;  //当前选择的保障计划是否包含公共帐户保障计划
            //得到险种下所有被选择的保障计划数，并判断当前保障计划是否公共帐户保障计划
            for(int t = 1; t <= mLPContPlanRiskSet.size(); t++)
            {
                if(!mLPContPlanRiskSet.get(i).getRiskCode()
                   .equals(mLPContPlanRiskSet.get(t).getRiskCode())) //只处理当前险种
                {
                    continue;
                }
                if(mLPContPlanRiskSet.get(t).getContPlanCode().equals("11"))
                {
                    hasPublicAccPlan = true;
                }
                selectedPlanCount++;
            }
            if (isPublicAccPlan && selectedPlanCount != set.size())
            {
                mErrors.addOneError("您选择了特需险种公共账户保障计划，"
                                    + "请同时选择所有被保人保障计划。");
                return false;
            }
            //特需险种保障计划数 = 被保人保障计划数 + 1(公共帐户保障计划数)
            if(!isPublicAccPlan && !hasPublicAccPlan
               && set.size() == selectedPlanCount + 1)
            {
                mErrors.addOneError("您选择了所有特需险种的被保人保障计划，"
                                    + "请同时选择公共账户保障计划。");
                return false;
            }
        }

        if(!checkMainRisk())
        {
            return false;
        }

        return true;
    }

    /**
     * checkMainRisk
     *若选择了主险，则附加险也需选择
     * @return boolean
     */
    private boolean checkMainRisk()
    {
        //若选择了主险，则附加险也需选择
        for(int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            LPContPlanRiskSchema schema = mLPContPlanRiskSet.get(i);
            if(!schema.getRiskCode().equals(schema.getMainRiskCode()))
            {
                continue;
            }

            //选择了主险，查询该主险是否有附加险，若有则需要处理附加险
            String sql =
                "select * from LCContPlanRisk "
                + "where GrpContNo = '" + schema.getGrpContNo() + "' "
                + "   and MainRiskCode = '" + schema.getRiskCode() + "' "
                + "   and RiskCode != MainRiskCode ";
            System.out.println(sql);
            LCContPlanRiskSet set = new LCContPlanRiskDB().executeQuery(sql);

            //无附加险
            if(set.size() == 0)
            {
                continue;
            }

            int sumRiskCount = 0;  //附加险数据
            for(int j = 1; j <= set.size(); j++)
            {
                LCContPlanRiskSchema cSchema = set.get(j);

                for(int t = 1; t <= mLPContPlanRiskSet.size(); t++)
                {
                    //选择了附加险
                    if(cSchema.getRiskCode()
                       .equals(mLPContPlanRiskSet.get(t).getRiskCode()) && cSchema.getContPlanCode().equals(mLPContPlanRiskSet.get(t).getContPlanCode()))
                    {
                        sumRiskCount++;
                    }
                }
            }

            if(sumRiskCount != set.size())
            {
                CError tError = new CError();
                tError.moduleName = "GrpEdorXTDetailBL";
                tError.functionName = "checkMainRisk";
                tError.errorMessage = "险种" + schema.getRiskCode()
                                      + "有附加险，请选择附件险一起处理";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        LPGrpEdorItemSchema grpEdorItem = null;
        try
        {
            mGlobalInput = (GlobalInput) data.
                           getObjectByObjectName("GlobalInput", 0);
            grpEdorItem = (LPGrpEdorItemSchema) data.
                          getObjectByObjectName("LPGrpEdorItemSchema", 0);
            mEdorItemSpecialData = (EdorItemSpecialData) data
                                   .getObjectByObjectName("EdorItemSpecialData", 0);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError("接收数据失败!!");
            return false;
        }

        if (mGlobalInput == null || grpEdorItem == null
            || mEdorItemSpecialData == null)
        {
            this.mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        System.out.println(mGlobalInput.Operator + " "
            + PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + " "
            + "协议退保明细录入");

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(grpEdorItem.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(grpEdorItem.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(grpEdorItem.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() < 1)
        {
            this.mErrors.addOneError("输入数据有误,没有查询到保全项目信息。");
            return false;
        }

        mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        mLPGrpEdorItemSchema.setReasonCode(grpEdorItem.getReasonCode());
        mLPGrpEdorItemSchema.setEdorState("1");

        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        if (!tLPGrpEdorMainDB.getInfo())
        {
            this.mErrors.addOneError("输入数据有误,LPGrpEdorMain中没有相关数据!");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!dealLPGrpCont())
        {
            return false;
        }

        if(!dealLPPol())
        {
            return false;
        }

        mMap.put(mLPGrpEdorItemSchema, "UPDATE");

        String sql = "delete from LPEdorEspecialData "
                     + "where EdorNo = '"
                     + mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + mLPGrpEdorItemSchema.getEdorType() + "' ";
        mMap.put(sql, SysConst.DELETE);

        mMap.put(mEdorItemSpecialData.getSpecialDataSet(), "INSERT");

        return true;
    }

    /**
     * dealLPPol
     *
     * @return boolean
     */
    private boolean dealLPPol()
    {
        mMap.put("  delete from LPPol "
                 + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                 + "   and edorType = '"
                 + mLPGrpEdorItemSchema.getEdorType() + "' "
                 + "   and grpContNo = '"
                 + mLPGrpEdorItemSchema.getGrpContNo() + "' ",
                 "DELETE");

        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            mMap.put("insert into LPPol "
                     + "(select '" + mLPGrpEdorItemSchema.getEdorNo()
                     + "', '" + mLPGrpEdorItemSchema.getEdorType()
                     + "', LCPol.* "
                     + "from LCPol "
                     + "where grpContNo = '"
                     + mLPContPlanRiskSet.get(i).getGrpContNo()
                     + "'  and contPlanCode = '"
                     + mLPContPlanRiskSet.get(i).getContPlanCode()
                     + "'  and riskCode = '"
                     + mLPContPlanRiskSet.get(i).getRiskCode() + "') ", "INSERT");
        }

        updateDefaultFields("LPPol");

        return true;
    }

    private void updateDefaultFields(String tableName)
    {
        mMap.put("  update " + tableName
              + " set operator = '" + mGlobalInput.Operator + "', "
              + "    makeDate = '" + PubFun.getCurrentDate() + "', "
              + "    makeTime = '" + PubFun.getCurrentTime() + "', "
              + "    modifyDate = '" + PubFun.getCurrentDate() + "', "
              + "    modifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
              + "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
              + "   and grpContNo = '"
              + mLPGrpEdorItemSchema.getGrpContNo() + "' ",
              "INSERT");
    }

    /**
     * dealLPContPlanRisk
     *
     * @return boolean
     */
    private boolean getLPContPlanRisk()
    {
        String sql = "delete from LPContPlanRisk "
                     + "where EdorNo = '"
                     + mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + mLPGrpEdorItemSchema.getEdorType() + "' "
                     + "   and GrpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' ";
        mMap.put(sql, SysConst.DELETE);

        Reflections ref = new Reflections();
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        tLCContPlanRiskDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());

        LPEdorEspecialDataSet set = mEdorItemSpecialData.getSpecialDataSet();
        for(int i = 1; i <= set.size(); i++)
        {
            LPEdorEspecialDataSchema schema = set.get(i);

            if(schema.getDetailType().equals(BQ.DETAILTYPE_XTFEEG))
            {
                String[] temp = schema.getPolNo().split(",");
                String contPlanCode = temp[0];
                String riskCode = temp[1];

                tLCContPlanRiskDB.setContPlanCode(contPlanCode);
                tLCContPlanRiskDB.setRiskCode(riskCode);

                LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
                if(tLCContPlanRiskSet.size() == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "GrpEdorXTDetailBL";
                    tError.functionName = "dealLPContPlanRisk";
                    tError.errorMessage = "没有查询到保障计划信息" + contPlanCode
                                          + riskCode;
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
                }

                LPContPlanRiskSchema tLPContPlanRiskSchema
                    = new LPContPlanRiskSchema();
                ref.transFields(tLPContPlanRiskSchema,
                                tLCContPlanRiskSet.get(1));
                tLPContPlanRiskSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                tLPContPlanRiskSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
                tLPContPlanRiskSchema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(tLPContPlanRiskSchema);

                mLPContPlanRiskSet.add(tLPContPlanRiskSchema);
            }
        }
        mMap.put(mLPContPlanRiskSet, SysConst.INSERT);

        return true;
    }

    /**
     * getLPGrpCont
     * 若选择的保障计划下险种数=LCPol的保障计划下险种数，则整单退保，存储LPGrpCont
     * @return boolean
     */
    private boolean dealLPGrpCont()
    {
        int polSelected = 0;  //选择的保障计划下险种数

        String sqDelete = "delete from LPGrpCont "
                     + "where EdorNo = '"
                     + mLPGrpEdorItemSchema.getEdorNo() + "' "
                     + "   and Edortype = '"
                     + mLPGrpEdorItemSchema.getEdorType() + "' "
                     + "   and GrpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' ";

        LPEdorEspecialDataSet set = mEdorItemSpecialData.getSpecialDataSet();
        for(int i = 1; i <= set.size(); i++)
        {
            if(set.get(i).getDetailType().equals(BQ.DETAILTYPE_XTFEEG))
            {
                polSelected++;
            }
        }

        if(polSelected == 0)
        {
            mMap.put(sqDelete, SysConst.DELETE);
            return true;
        }

        String sql = "select distinct ContPlanCode, RiskCode "
                     + "from LCPol "
                     + "where GrpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' ";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() == polSelected)
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
            tLCGrpContDB.getInfo();

            tLPGrpContSchema = new LPGrpContSchema();
            new Reflections().transFields(tLPGrpContSchema,
                                          tLCGrpContDB.getSchema());
            tLPGrpContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
            tLPGrpContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
            tLPGrpContSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLPGrpContSchema);
            mMap.put(tLPGrpContSchema, SysConst.DELETE_AND_INSERT);
        }
        else
        {
            mMap.put(sqDelete, SysConst.DELETE);
        }

        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "test";
        gi.ComCode = "86";

        String edorNo = "20070313000001";
        String edorType = "XT";
        String grpContNo = "0000222801";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo(edorNo);
        tLPGrpEdorItemSchema.setEdorType(edorType);
        tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
        tLPGrpEdorItemSchema.setReasonCode("010");

        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(edorNo, edorType);
        tEdorItemSpecialData.setGrpPolNo("A,1602");
        tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-10");
        tEdorItemSpecialData.add(BQ.XTFEERATEG, "0.1729");

        tEdorItemSpecialData.setGrpPolNo("A,5601");
        tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-10");
        tEdorItemSpecialData.add(BQ.XTFEERATEG, "0.1729");


        VData tVData = new VData();
        tVData.addElement(gi);
        tVData.addElement(tLPGrpEdorItemSchema);
        tVData.add(tEdorItemSpecialData);

        GrpEdorXTDetailBL bl = new GrpEdorXTDetailBL();
        if(!bl.submitData(tVData, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
