package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListTAUI
{
    PrtGrpInsuredListTABL mPrtGrpInsuredListTABL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListTAUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListTABL = new PrtGrpInsuredListTABL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListTABL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListTABL.getInputStream();
    }
}
