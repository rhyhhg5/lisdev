package com.sinosoft.lis.bq;

import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PContTerminateQueryPrintBL implements PrintService {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private LCContSchema mLCContSchema = new LCContSchema();

    private SSRS mSSRS = new SSRS();

    private String mOperate = "";

    private String mContNo = "";

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    public PContTerminateQueryPrintBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("==PContTerminateQueryPrintBL start==");

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (!getListData()) {
            return false;
        }


        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        //加入到打印列表
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("==PContTerminateQueryPrintBL end==");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                              getObjectByObjectName(
                                      "LOPRTManagerSchema", 0);
        if (mGlobalInput == null || mLOPRTManagerSchema == null) {
            CError tError = new CError();
            tError.moduleName = "PolicyAbatePrintBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo()) {
            buildError("getPrintData", "获得保单信息失败！");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();
        return true;
    }

    private boolean getPrintData() {

        String sql = "";

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='" +
              mLCContSchema.getContNo() + "') AND AddressNo = '" + tLCAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).
                           getSchema();


        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolicyAbatePrintBL.vts", "printer");
        textTag.add("JetFormType", "210");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }
        setFixedInfo();
        textTag.add("GrpZipCode", mLCAddressSchema.getZipCode());
        System.out.println("GrpZipCode=" + mLCAddressSchema.getZipCode());
        textTag.add("GrpAddress", mLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if (mLCAddressSchema.getPhone() != null &&
            !mLCAddressSchema.getPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getPhone() + "、";
        }
        if (mLCAddressSchema.getHomePhone() != null &&
            !mLCAddressSchema.getHomePhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getHomePhone() + "、";
        }
        if (mLCAddressSchema.getCompanyPhone() != null &&
            !mLCAddressSchema.getCompanyPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getCompanyPhone() + "、";
        }
        if (mLCAddressSchema.getMobile() != null &&
            !mLCAddressSchema.getMobile().equals("")) {
            appntPhoneStr += mLCAddressSchema.getMobile() + "、";
        }
        
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentDB tLAAgentDB=new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        if(tLAAgentDB.getInfo())
        {
        	tLAAgentSchema = tLAAgentDB.getSchema();;
        }
        else
        {
        	 buildError("getPrintData", "查询业务员信息失败！");
             return false;
        }
        appntPhoneStr = appntPhoneStr.substring(0, appntPhoneStr.length() - 1);
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", mLCContSchema.getAppntName());
        textTag.add("AppntNo", mLCContSchema.getAppntNo());

        textTag.add("ContNo", mLCContSchema.getContNo());

        textTag.add("PayToDate",
                    CommonBL.decodeDate(PubFun.calDate(mLCContSchema.getPaytoDate(), 2, "Y", null))); //转换格式，显示为24时
        
        String sumPrem="0";
        String getsumPremSQL ="select sum(a.prem) from lcpol a ,lmriskapp c where  c.riskperiod = 'L' and c.riskcode=a.riskcode and a.ContNo='"+mLCContSchema.getContNo()+"'";
        SSRS getsumPremSqlSSRS =new ExeSQL().execSQL(getsumPremSQL); 
        if(getsumPremSqlSSRS!= null && getsumPremSqlSSRS.getMaxRow() > 0 )
		{
        	sumPrem=getsumPremSqlSSRS.GetText(1, 1);
		}
        
        textTag.add("SumMoney",sumPrem);//合计收费
        String PolicyDate = new ExeSQL().getOneValue("select max(MakeDate) From  loprtmanager where OtherNo ='"+mLCContSchema.getContNo()+"' and Code ='42' with ur");
        textTag.add("MakeDate",CommonBL.decodeDate(PubFun.calDate(PolicyDate, 0, "D", null)));
        String tOperator = mGlobalInput.Operator;//得到经办人
        textTag.add("Operator", tOperator);//经办人
        textTag.add("AgentCom", tLAAgentSchema.getOldCom());//业务员部门
        
        String tPolApplyDate = mLCContSchema.getPolApplyDate();//投保单申请日
        String tPaytoDate = mLCContSchema.getPaytoDate();//交至日
        String tPauseDate = new ExeSQL().getOneValue("select makedate from lccontstate where state = '1' and statetype = 'Terminate' and contno = '"+mLCContSchema.getContNo()+"' ");//失效日
        String printStr="特别提醒您，您于"+toDate(tPolApplyDate)+"投保的"+mLCContSchema.getContNo()+"号保单因我公司未收到保单的续期保费，该保单已于"
                       +toDate(tPauseDate)+"失效";
        textTag.add("printStr", printStr);               
        String sexCode = tLCAppntDB.getAppntSex();
        String sex;
        if (sexCode == null) {
            sex = "先生/女士";
        } else if (sexCode.equals("0")) {
            sex = "先生";
        } else if (sexCode.equals("1")) {
            sex = "女士";
        } else {
            sex = "先生/女士";
        }
        textTag.add("LinkManSex", sex);

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                         +" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                         +" (select agentgroup from laagent where agentcode ='"
                         + tLaAgentDB.getAgentCode() + "'))";
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL.toString());
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
        
        String sqlPrtSeq="select max(prtSeq) from LOPRTManager where otherNo ='"+mLCContSchema.getContNo()+"' and code = '210'";
        ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS= tExeSQL.execSQL(sqlPrtSeq);
        if(tSSRS==null||tSSRS.getMaxRow()<=0)
        {
        	 CError.buildErr(this, "查询永久失效通知书号失败");
             return false;
        }
        textTag.add("BarCode1", tSSRS.GetText(1, 1));
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("StandFlag1", "");
        textTag.add("StandFlag2", "");
        textTag.add("StandFlag3", "");
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        String[] title = {"险种代码", "险种名称", "保额（元）",  "保险期间（年）",
                "缴费年期（年）", "保费（元）"};
        xmlexport.addListTable(getListTable(), title);
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);

        String currentTimeMillis=System.currentTimeMillis()+"";
        xmlexport.outputDocumentToFile("C:\\", currentTimeMillis);
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    private boolean getListData()
    {
    	String getRiskSQL ="select a.riskcode,(select b.riskname from lmrisk b where b.riskcode=a.riskcode),a.amnt,(case when a.insuyearflag='Y' then a.insuyear when a.insuyearflag='D' then a.insuyear/365 when a.insuyearflag='M' then a.insuyear/12 when a.insuyearflag = 'A' then a.years end),(case when a.payendyearflag='Y' then a.payendyear when a.payendyearflag='M' then a.payendyear/12 when a.payendyearflag='D' then a.payendyear/365 end)  ,a.prem from lcpol a ,lmriskapp c where  c.riskperiod = 'L' and c.riskcode=a.riskcode and a.ContNo='"+mLCContSchema.getContNo()+"'";

        mSSRS = new ExeSQL().execSQL(getRiskSQL);
        if (mSSRS.getMaxRow()==0 || mSSRS== null)
        {
            buildError("getListData","查询失效险种信息失败！");
            return false;
        }
        return true;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {

        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("02");
        mLOPRTManagerSchema.setCode("210");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);

        return true;
    }


    public VData getResult() {
        return this.mResult;
    }


    public CErrors getErrors()
    {
        return this.mErrors;
    }
//  转化日期：将第一个-转为年，第二个-转为月
    private String toDate(String date)
    {
 	   String[] dDate=date.split("-");
 	   
 	   return dDate[0]+"年"+dDate[1]+"月"+dDate[2]+"日";
    }
    //转化日期：将第一个-转为年，第二个-转为月,得到复效日，复效日为终止日加两年
    private String addTwoYear(String date)
    {
 	   String[] dDate=date.split("-");
 	   
 	   return Integer.parseInt(dDate[0])+2+"年"+dDate[1]+"月"+dDate[2]+"日";
    }

}
