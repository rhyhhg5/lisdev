package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 卡折增加连带被保人磁盘导入类</p>
 * <p>Description: 导入连带被保人 </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Sinosoft </p>
 * @author Zhanggm 20101005
 * @version 1.0
 */

public class LLWSImportBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String fileName = null;

    private String configFileName = null;

    private String sheetName = "";
    
    private LPDiskImportSet mLPDiskImportSet = null;
    
    public LPDiskImportSet mLPDiskImportSetCF = null;   //重复人记录 ******
    
    private String mMessage = null;

    private static String mCurrentDate = PubFun.getCurrentDate();

    private static String mCurrentTime = PubFun.getCurrentTime();
    
    private ExeSQL mExeSQL = new ExeSQL();

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public LLWSImportBL(String fileName, String configFileName, String sheetName)
    {
        this.fileName = fileName;
        this.configFileName = configFileName;
        this.sheetName=sheetName;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到输入数据
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            LPDiskImportSchema tLPDiskImportSchema = (LPDiskImportSchema)
                    data.getObjectByObjectName("LPDiskImportSchema", 0);
            mEdorNo = tLPDiskImportSchema.getEdorNo();
            mEdorType = tLPDiskImportSchema.getEdorType();
            mContNo = tLPDiskImportSchema.getGrpContNo();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        deleteImportData();
        mLPDiskImportSet = getLPDiskImportSet();
        setLPDiskImportSet(mLPDiskImportSet);
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        tLPDiskImportSet.set(mLPDiskImportSet);
        mMap.put(tLPDiskImportSet, "INSERT");
        
        //校验与主被保人关系是否正确
        checkInsuredRelation();
        
//      校验被保人是否是重复导入的
        checkInsuredCF();
        
        //校验卡单下是否已经有该被保人
        checkInsuredCZ();

        return true;
    }

    /**
     * 导入前先清空上次导入数据
     */
    private void deleteImportData()
    {
        String sql = "delete from LPDiskImport " 
        	       + "where EdorNo = '" + mEdorNo + "' and EdorType = '" + mEdorType 
        	       + "' and GrpContNo = '" + mContNo + "' ";
        mMap.put(sql, "DELETE");
    }

    /**
     * 得到LPDiskImportSet，调用磁盘导入程序
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getLPDiskImportSet()
    {
        BqDiskImporter importer = new BqDiskImporter(fileName, configFileName,
                sheetName);
        if (!importer.doImport())
        {
            mErrors.copyAllErrors(importer.mErrrors);
            return null;
        }
        return (LPDiskImportSet) importer.getSchemaSet();
    }

    /**
     * 设置LPDiskImportSet
     * @param aLPDiskImportSet LPDiskImportSet
     */
    private void setLPDiskImportSet(LPDiskImportSet aLPDiskImportSet)
    {
        for (int i = 1; i <= aLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = aLPDiskImportSet.get(i);
            tLPDiskImportSchema.setEdorNo(mEdorNo);
            tLPDiskImportSchema.setEdorType(mEdorType);
            tLPDiskImportSchema.setGrpContNo(mContNo);
            tLPDiskImportSchema.setState(BQ.IMPORTSTATE_SUCC);  //有效
            tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
            tLPDiskImportSchema.setMakeDate(mCurrentDate);
            tLPDiskImportSchema.setMakeTime(mCurrentTime);
            tLPDiskImportSchema.setModifyDate(mCurrentDate);
            tLPDiskImportSchema.setModifyTime(mCurrentTime);
        }
    }
    
    public String getError()
    {
        return mErrors.getFirstError();
    }
    
    private void checkInsuredRelation()
    {
    	LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        boolean flag = true;
    	for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            String relation = tLPDiskImportSchema.getRelation();
            if("00".equals(relation))
            {
            	errorReason += "新增被保人"+tLPDiskImportSchema.getInsuredName()+"不能是主被保人本人。";
            	flag = false;
            }
            String sql = "select count(1) from ldcode where codetype = 'relation' and code = '" + relation + "' with ur";
            if("0".equals(mExeSQL.getOneValue(sql)))
            {
            	errorReason += "新增被保人" + tLPDiskImportSchema.getInsuredName() + "与主被保人关系录入错误。";
            	flag = false;
            }

            if (!errorReason.equals(""))
            {
                sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '" + errorReason + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
            else
            {
            	tLPDiskImportSet.add(tLPDiskImportSchema);
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
            System.out.println(mMessage);
        }
        mLPDiskImportSet.clear();
        mLPDiskImportSet.add(tLPDiskImportSet);
    }

    private void checkInsuredCF()
    {
        LPDiskImportSet t = getIn();
        mLPDiskImportSet.clear();
        mLPDiskImportSet.add(t);
        
        if(mLPDiskImportSetCF.size()>0)
        {
            for (int i = 1; i <= mLPDiskImportSetCF.size(); i++)
            {
                String errorReason = "";
                LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSetCF.get(i);
                System.out.println("重复人姓名："+tLPDiskImportSchema.getInsuredName());
                errorReason +="被保人" +tLPDiskImportSchema.getInsuredName()+"是重复导入的.";
                String sql = "update LPDiskImport " +
                                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                                        " ErrorReason = '" + errorReason + "', " +
                                        " Operator = '" + mGlobalInput.Operator + "', " +
                                        " ModifyDate = '" + mCurrentDate + "', " +
                                        " ModifyTime = '" + mCurrentTime + "' " +
                                        "where EdorNo = '" + mEdorNo + "' " +
                                        "and EdorType = '" + mEdorType + "' " +
                                        "and GrpContNo = '" + mContNo + "' " +
                                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() + "' ";
                  mMap.put(sql, "UPDATE");
                  mMessage = "有重复人被导入！";
          }
        }
    }
    
    private void checkInsuredCZ()
    {
    	boolean flag = true;
    	for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            String errorReason = "";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema.setContNo(mContNo);
            tLCInsuredSchema.setName(tLPDiskImportSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLPDiskImportSchema.getSex());
            tLCInsuredSchema.setBirthday(tLPDiskImportSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLPDiskImportSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLPDiskImportSchema.getIDNo());
            
            if (checkInsuredInfo(tLCInsuredSchema))
            {
            	errorReason += "保单中已存在被保人" + tLCInsuredSchema.getName() + "，不能重复导入！";
            	flag = false;
            }

            if (!errorReason.equals(""))
            {
                String sql = "update LPDiskImport " +
                        "set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '" + errorReason + "', " +
                        " Operator = '" + mGlobalInput.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mContNo + "' " +
                        "and SerialNo = '" + tLPDiskImportSchema.getSerialNo() +
                        "' ";
                mMap.put(sql, "UPDATE");
            }
        }
        if (flag == false)
        {
            mMessage = "导入数据中有无效的被保人信息！";
            System.out.println(mMessage);
        }
    }
    
        //20080821 add zhanggm 校验重复人的方法:
        public LPDiskImportSet getIn()
        {
            LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
            LPDiskImportSet tLPDiskImportSet1 = new LPDiskImportSet(); //用来记录重复的人信息
            for (int i = 1; i <= mLPDiskImportSet.size(); i++) 
            {
            	LPDiskImportSchema mLPDiskImportSchema = mLPDiskImportSet.get(i);
            	if(checkLPDiskImport(tLPDiskImportSet,mLPDiskImportSchema))
        		{
        			tLPDiskImportSet1.add(mLPDiskImportSet.get(i)); //重复人的记录
        		}
        		else
        		{
        			tLPDiskImportSet.add(mLPDiskImportSet.get(i));
        		}
            }
            
            mLPDiskImportSetCF=tLPDiskImportSet1; //重复人记录
            return tLPDiskImportSet;
        }
        //20080821 add zhanggm 校验aLPDiskImportSchema是否在aLPDiskImportSet中,是:true,否：false
        private boolean checkLPDiskImport(LPDiskImportSet aLPDiskImportSet,LPDiskImportSchema aLPDiskImportSchema)
        {
        	if(aLPDiskImportSet.size()<1)
        	{
        		return false;
        	}
        	String mInsuredName = aLPDiskImportSchema.getInsuredName();
        	String mSex = aLPDiskImportSchema.getSex();
        	String mBirthday = aLPDiskImportSchema.getBirthday();
        	String mIDType = aLPDiskImportSchema.getIDType();
        	String mIDNo = aLPDiskImportSchema.getIDNo();
        	for (int i=1;i<=aLPDiskImportSet.size();i++)
        	{
    			LPDiskImportSchema tLPDiskImportSchema = aLPDiskImportSet.get(i);
    			String tInsuredName = tLPDiskImportSchema.getInsuredName();
            	String tSex = tLPDiskImportSchema.getSex();
            	String tBirthday = tLPDiskImportSchema.getBirthday();
            	String tIDType = tLPDiskImportSchema.getIDType();
            	String tIDNo = tLPDiskImportSchema.getIDNo();
            	if(mInsuredName.equals(tInsuredName) && mSex.equals(tSex) && mBirthday.equals(tBirthday) 
            			&& mIDType.equals(tIDType) && mIDNo.equals(tIDNo))
            	{
            		System.out.println("被保人"+tInsuredName+"是重复导入的！");
            		return true;
            	}
        	}
        	return false;
        }
        
        private boolean checkInsuredInfo(LCInsuredSchema aLCInsuredSchema)
        {
            String othIdNo = aLCInsuredSchema.getOthIDNo();
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            if ((othIdNo == null) || (othIdNo.equals("")))
            {
                tLCInsuredDB.setContNo(mContNo);
                tLCInsuredDB.setName(aLCInsuredSchema.getName());
                tLCInsuredDB.setSex(aLCInsuredSchema.getSex());
                tLCInsuredDB.setBirthday(aLCInsuredSchema.getBirthday());
                tLCInsuredDB.setIDType(aLCInsuredSchema.getIDType());
                tLCInsuredDB.setIDNo(aLCInsuredSchema.getIDNo());
            }
            else
            {
                tLCInsuredDB.setContNo(mContNo);
                tLCInsuredDB.setName(aLCInsuredSchema.getName());
                tLCInsuredDB.setOthIDNo(aLCInsuredSchema.getOthIDNo());
            }
            LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
            if (tLCInsuredSet.size() == 0)
            {
                return false;
            }
            return true;
        }
        

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
