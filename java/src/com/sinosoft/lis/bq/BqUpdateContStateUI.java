//程序名称：BqUpdateContStateUI.java
	//程序功能：保单状态维护
	//创建日期：20170426
	//创建人  ：wxd
	//程序功能：保单状态维护
	//创建日期：20170426
	//创建人  ：wxd
package com.sinosoft.lis.bq;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BqUpdateContStateUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public BqUpdateContStateUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();// ?把数据复制到了自己定义的变量中,不操作直接传过来的参数,为啥?
		BqUpdateContStateBL updateContStateBL = new BqUpdateContStateBL();
		try {
			if (!updateContStateBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(updateContStateBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "BqUpdateContStateUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
