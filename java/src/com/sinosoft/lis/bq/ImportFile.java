package com.sinosoft.lis.bq;

import java.io.*;
import org.w3c.dom.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;

/**
 * <p>Title: 磁盘导入类</p>
 * <p>Description: 把数据从磁盘中的XML文件导入，存到相应的类中 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class ImportFile {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 保单信息在xml中的结点 */
    private static final String PARSE_PATH = "/DATASET/CONTTABLE/ROW";

    /** 配置文件名 */
    private static final String CONFIG_FILE_NAME = "Import.xml";

    /** 文件路径 */
    private String path;

    /** Excel文件名 */
    private String fileName;
    private String mBatchNo = "";
    /** 存储全局的被保人 */
    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();


    /**
     * 构造函数
     * @param path String
     * @param fileName String
     */
    public ImportFile(String path, String fileName) {
        this.path = path;
        this.fileName = fileName;
        this.mBatchNo = fileName.substring(0,fileName.lastIndexOf("."));
    }

    /**
     * 从Excel文件导入数据，保存存到List中<br>
     * 先把数据保存到多个xml文件中，然后从xml读出存到VData中
     * @return VData 保存数据的容器
     */
    public VData doImport() {
        String[] xmlFiles = parseVts(); //解析Excel到xml
        if (xmlFiles == null) {
            return null;
        }

        VData data = new VData();
        for (int i = 0; i < xmlFiles.length; i++) {
            parseXml(data, xmlFiles[i]); //解析xml到VData
        }
        return data;
    }

    /**
     * 得到Excel文件路径
     * @return String
     */
    private String getFilePath() {
        return path + File.separator + fileName;
    }

    /**
     * 得到配置文件路径
     * @return String
     */
    private String getConfigFilePath() {
        return path + File.separator + CONFIG_FILE_NAME;
    }

    /**
     * 解析excel并转换成多个xml文件
     * @return String[] 存放xml文件名的数组
     */
    private String[] parseVts() {
        //检查文件是否存在
        File file = new File(getFilePath());
        if (!file.exists()) {
            CError tError = new CError();
            tError.moduleName = "ImportFile";
            tError.functionName = "parseVts";
            tError.errorMessage = "未上传文件到指定路径" + path + "!";
            mErrors.addOneError(tError);
            return null;
        }

        //检查导入配置文件是否存在
        File configFile = new File(getConfigFilePath());
        if (!configFile.exists()) {
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + path + "!";
            mErrors.addOneError(tError);
            return null;
        }

        GrpPolVTSParser gpvp = new GrpPolVTSParser();
        gpvp.setFileName(getFilePath());
        gpvp.setConfigFileName(getConfigFilePath());
        //转换excel到xml
        if (!gpvp.transform()) {
            mErrors.copyAllErrors(gpvp.mErrors);
            return null;
        }
        return gpvp.getDataFiles();
    }

    /**
     * 解析xml，把数据保存到VData中
     * @param xmlFileName String  解析的xml文件名
     * @param data VData  存放解析结果的容器
     */
    private void parseXml(VData data, String xmlFileName) {
        GrpPolParser tGrpPolParser = new GrpPolParser();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
        try {
            //解析保单信息
            XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
            NodeList nodeList = xmlPT.parseN(PARSE_PATH);
            System.out.println("长度 ： " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                NodeList contNodeList = node.getChildNodes();
                if (contNodeList.getLength() <= 0)
                    continue;
                for (int j = 0; j < contNodeList.getLength(); j++) {

                    Node contNode = contNodeList.item(j);
                    String nodeName = contNode.getNodeName();
                    if (nodeName.equals("#text")) {
                    }
                    //解析保单序号
                    else if (nodeName.equals("CONTID")) {
                    }
                    //解析被保险人
                    else if (nodeName.equals("INSUREDTABLE")) {
                        tLCInsuredSet.add(tGrpPolParser.getLCInsuredSet(
                                contNode));
                        mLCInsuredListSet.add(tGrpPolParser.getLCInsuredListSet(
                                contNode));
                    }
                    //解析受益人
                    else if (nodeName.equals("BNFTABLE")) {
                        tLCBnfSet.add(tGrpPolParser.getLCBnfSet(contNode));
                    }
                    //解析连身被保
                    else if (nodeName.equals("INSURED_RELA_TABLE")) {
                        tLCInsuredRelatedSet.add(
                                tGrpPolParser.getLCInsuredRelatedSet(contNode));
                    }
                }
            }

            //解析完成删除xml临时文件
            File xmlFile = new File(xmlFileName);
            xmlFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //保存解析结果
        if (tLCInsuredSet.size() != 0) {
            data.add(tLCInsuredSet);
        }
        if (mLCInsuredListSet.size() != 0) {
            data.add(mLCInsuredListSet);
        }
        if (tLCBnfSet.size() != 0) {
            data.add(tLCBnfSet);
        }
        if (tLCInsuredRelatedSet.size() != 0) {
            data.add(tLCInsuredRelatedSet);
        }
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        ImportFile importFile = new ImportFile(
                "D:\\picch\\ui\\temp", "10人A计划.xls");
        VData data = importFile.doImport();
        LCInsuredSet tLCInsuredSet = (LCInsuredSet)
                                     data.getObjectByObjectName("LCInsuredSet", 0);
        System.out.println(tLCInsuredSet.size());
    }
}
