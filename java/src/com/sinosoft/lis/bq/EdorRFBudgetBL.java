package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.PubFun;

import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.BqCalBase;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>
 * Title:
 * </p>
 *
 * <p>
 * Description: 本类用来实现还款试算功能 调用正常还款核心程序进行算费
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2018
 * </p>
 *
 * <p>
 * Company: sinosoft
 * </p>
 *
 * @author fengzhihang
 * @version 1.1
 */
public class EdorRFBudgetBL {
	/** 错误的容器 */
	private static final String LCPOLSCHEMA = LCPolSchema.class.getName();
	private static final String STRING = java.lang.String.class.getName();
	private Reflections mReflections = new Reflections();
	public CErrors mErrors = new CErrors();
	private LPEdorItemSchema mLPEdorItemSchema = null; // 个体保全项目
	private Reflections ref = new Reflections();
	private LPLoanSchema mLPLoanSchema = new LPLoanSchema();
	private String mEdorValiDate = null; // 试算生效日期
	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();
	private String operator = null;
	private FDate tFDate = new FDate();

	private HashMap mHashMap = new HashMap();
	private boolean mNeedBugetResult = false;
	private LGErrorLogSet mLGErrorLogSet = new LGErrorLogSet();
	private LPBudgetResultSchema mLPBudgetResultSchema = null; // 试算结果

	public EdorRFBudgetBL() {
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	/**
	 * 传入个单保全项目信息，为走正常退保流程做准备
	 */
	public void setLPEdorItemInfo(LPEdorItemSchema item) {
		this.mLPEdorItemSchema = item;
	}

	/**
	 * 个人险种还款试算 由于还款试算走正常还款的流程，需要构造保全项目信息
	 * @return LJSGetEndorseSet
	 */
	public LJSGetEndorseSet BudgetOnePol(Object obj) {
		// 构造保全项目信息
		if (this.mLPEdorItemSchema == null) {
			this.mLPEdorItemSchema = createCmnLPEdorItemInfo();
			if (this.mLPEdorItemSchema == null) {
				return null;
			}
		}

		LCPolSchema tLCPolSchema = getLCPolInfo(obj);
		if(tLCPolSchema.equals("") || tLCPolSchema == null){
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取险种信息失败!";
			mErrors.addOneError(tError);
			return null;
		}
		mLPEdorItemSchema.setContNo(tLCPolSchema.getContNo());
		mLPEdorItemSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
		mLPEdorItemSchema.setPolNo(tLCPolSchema.getPolNo());
		mLPEdorItemSchema.setOperator(operator);
		if (!checkRF(tLCPolSchema)) {
			return null;
		} else {
//			 查找保单
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(tLCPolSchema.getContNo());
			if (!tLCContDB.getInfo()) {
				mErrors.addOneError("查询保单失败。");
				return null;
			}
			// 查询保费以及保单管理机构
			LCContSchema tLCContSchema = new LCContSchema();
			tLCContSchema = tLCContDB.getSchema();
			// 开始调用核心正常还款流程
			mLGErrorLogSet.clear();
//			LCPolDB tLCPolDB = new LCPolDB();
//			tLCPolDB.setPolNo(mLPLoanSchema.getPolNo());
//			if (!tLCPolDB.getInfo()) {
//				// @@错误处理
//				System.out.println("PEdorRFAppConfirmBL+dealData++--");
//				CError tError = new CError();
//				tError.moduleName = "PEdorRFAppConfirmBL";
//				tError.functionName = "dealData";
//				tError.errorMessage = "获取险种信息失败!";
//				mErrors.addOneError(tError);
//				return null;
//			}
			LPPolSchema tLPPolSchema = new LPPolSchema();
			tLPPolSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
			tLPPolSchema.setEdorType(mLPEdorItemSchema.getEdorType());
			mReflections.transFields(tLPPolSchema, tLCPolSchema);
			String aRiskCode = tLPPolSchema.getRiskCode();
			if (aRiskCode == null || aRiskCode.equals("")
					|| aRiskCode.equals("null")) {
				// @@错误处理
				System.out.println("PEdorRFAppConfirmBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "PEdorRFAppConfirmBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取险种失败!";
				mErrors.addOneError(tError);
				return null;
			}
			double tInterest = 0;
			String countdate = mLPEdorItemSchema.getEdorValiDate();
			// 获取贷款利息
			AccountManage tAccountManage = new AccountManage();
			tInterest = CommonBL.carry(tAccountManage.getLoanInterest(
					mLPLoanSchema.getLoanDate(), mLPLoanSchema.getSumMoney(),
					tLCContSchema.getManageCom(), tLCContSchema.getPrem() + "",
					countdate, aRiskCode));
			if (tInterest == -1) {
					// @@错误处理
				System.out.println("试算利息失败");
				CError tError = new CError();
				tError.moduleName = "PEdorRFBudgetBL";
				tError.functionName = "getLoanInterest()";
				tError.errorMessage = "计算贷款利息失败!";
				mErrors.addOneError(tError);
				return null;
			}
			// 生成财务数据
			LJSGetEndorseSet set = new LJSGetEndorseSet();
			LJSGetEndorseSchema RFBFSchema = new LJSGetEndorseSchema();
//			贷款本金
			RFBFSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo()); //给付通知书号码
			RFBFSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
			RFBFSchema.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
			RFBFSchema.setContNo(mLPEdorItemSchema.getContNo());
//			RFBFSchema.setGrpPolNo(mLPEdorItemSchema.getGrpPolNo());
			RFBFSchema.setPolNo(mLPLoanSchema.getPolNo());
			RFBFSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
			RFBFSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
			RFBFSchema.setGetMoney(mLPLoanSchema.getSumMoney());
			System.out.println("本金是："+mLPLoanSchema.getSumMoney());
			RFBFSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType()); //补退费业务类型
			RFBFSchema.setFeeFinaType("RFBF"); //补退费财务类型
			set.add(RFBFSchema);
//			利息
			LJSGetEndorseSchema RFLXSchema = new LJSGetEndorseSchema();
			RFLXSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorNo()); //给付通知书号码
			RFLXSchema.setEndorsementNo(mLPEdorItemSchema.getEdorNo());
			RFLXSchema.setGrpContNo(mLPEdorItemSchema.getGrpContNo());
			RFLXSchema.setContNo(mLPEdorItemSchema.getContNo());
//			RFLXSchema.setGrpPolNo(mLPEdorItemSchema.getGrpPolNo());
			RFLXSchema.setPolNo(mLPLoanSchema.getPolNo());
			RFLXSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
			RFLXSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
			RFLXSchema.setGetMoney(tInterest);
			System.out.println("利息是："+tInterest);
			RFLXSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType()); //补退费业务类型
			RFLXSchema.setFeeFinaType("RFLX"); //补退费财务类型
			set.add(RFLXSchema);
			if (set == null || set.size() == 0) {
				return null;
			}
			return set;
		}
	}

	/**
	 * 设置需要试算结果标志
	 * 
	 * @param flag
	 *            boolean
	 */
	public void setNeedBugetResultFlag(boolean flag) {
		this.mNeedBugetResult = flag;
	}

	/**
	 * 得到险种试算的结果
	 * 
	 * @return LPBudgetResultSchema
	 */
	public LPBudgetResultSchema getOnePolLPBudgetResult() {
		return mLPBudgetResultSchema;
	}

	public LGErrorLogSet getOnePolLGErrorLogSet() {
		return mLGErrorLogSet;
	}

	/**
	 * 得到需要退保试算的险种信息
	 * 
	 * @param obj
	 *            Object：可能以险种号或LCPolSchema两种方式传入 若传入险种号，则需查询得到险种信息
	 * @return LCPolSchema
	 */
	private LCPolSchema getLCPolInfo(Object obj) {
		String className = obj.getClass().getName();
		if (className.equals(LCPOLSCHEMA)) {
			return (LCPolSchema) obj;
		} else if (className.equals(STRING)) {
			LCPolDB db = new LCPolDB();
			db.setPolNo((String) obj);
			if (!db.getInfo()) {
				mErrors.addOneError("没有查询到险种信息。");
				System.out.println("没有查询到险种信息" + (String) obj);
				return null;
			}
			return db.getSchema();
		}
		return null;
	}

	/**
	 * @param polNo
	 *            String
	 * @return LPEdorItemSchema
	 */
	private LPEdorItemSchema createCmnLPEdorItemInfo() {
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

		tLPEdorItemSchema.setEdorAcceptNo("000000"); // 这么写是为了数据的完整性，试算过程中不EdorAcceptNo没有实际意义
		tLPEdorItemSchema.setEdorAppNo(tLPEdorItemSchema.getEdorAcceptNo());
		tLPEdorItemSchema.setEdorNo(tLPEdorItemSchema.getEdorAcceptNo());
		tLPEdorItemSchema.setEdorType("RF");// 保单还款
		tLPEdorItemSchema.setEdorAppDate(mCurDate);
		tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		if (mEdorValiDate == null) {
			tLPEdorItemSchema.setEdorValiDate(mCurDate);
		} else {
			tLPEdorItemSchema.setEdorValiDate(mEdorValiDate);
		}
		return tLPEdorItemSchema;
	}

	public void setEdorValiDate(String date) {
		mEdorValiDate = date;
	}
/*
 * add by fengzhihang
 * 校验所选预计还款时间是否符合要求，并且构建lploan表为后边计算准备
 * 预计还款时间的要求是：
 * min:贷款起息日期：loloan.loandate
 * max:贷款险种满期日期：lcpol.enddate
 */
	public boolean checkRF(LCPolSchema tLCPolSchema) {
		// 查询即往借款信息,取得上次借款的本金额度和利息
		LOLoanDB tLOLoanDB = new LOLoanDB();
		LOLoanSet tLOLoanSet = tLOLoanDB
				.executeQuery("select * from loloan where polno in (select polno from lcpol where contno='"
						+ mLPEdorItemSchema.getContNo()
						+ "') and loantype='0' and payoffflag='0' with ur");
		// //loanType 1-自垫 0-借款
		// tLOLoanDB.setLoanType("0");
		// //1-还清
		// tLOLoanDB.setPayOffFlag("0");

		if (tLOLoanSet != null && tLOLoanSet.size() != 1) {
			// @@错误处理
			System.out.println("PEdorRFBudgetBL+checkRF++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFBudgetBL";
			tError.functionName = "checkRF";
			tError.errorMessage = "获取贷款表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mReflections.transFields(mLPLoanSchema, tLOLoanSet.get(1));
		// actugetno为还款工单号
		mLPLoanSchema.setActuGetNo(mLPEdorItemSchema.getEdorNo());
		mLPLoanSchema.setEdorType(mLPEdorItemSchema.getEdorType());

		String tloanDate = new ExeSQL()
				.getOneValue("select loandate from loloan where polno in (select polno from lcpol where contno='"
						+ mLPEdorItemSchema.getContNo()
						+ "') and payoffflag='0' and loantype='0'");
		if (tloanDate != null && (!tloanDate.equals(""))
				&& (!tloanDate.equals("null"))) {
			FDate chgdate = new FDate();
			Date rTloanDate = chgdate.getDate(tloanDate);
			Date RFDate = chgdate.getDate(mLPEdorItemSchema
					.getEdorValiDate());
			if (rTloanDate.compareTo(RFDate) > 0) {

				System.out.println("EdorRFBudgetBL+checRF++--");
				CError tError = new CError();
				tError.moduleName = "PEdorRFAppConfirmBL";
				tError.functionName = "checkData";
				tError.errorMessage = "预计保单还款日期不能早于贷款起息日期" + tloanDate;
				mErrors.addOneError(tError);
				return false;
			}
//			最大还款日期应该小于贷款险种满期日期
			Date LNRiskEndDate = chgdate.getDate(tLCPolSchema.getEndDate());
			if(LNRiskEndDate.compareTo(RFDate) < 0){
				System.out.println("EdorRFBudgetBL+checRF++--");
				CError tError = new CError();
				tError.moduleName = "PEdorRFAppConfirmBL";
				tError.functionName = "checkData";
				tError.errorMessage = "预计保单还款日期不能晚于贷款险种满期日期" + tLCPolSchema.getEndDate();
				mErrors.addOneError(tError);
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		EdorRFBudgetBL bl = new EdorRFBudgetBL();

		bl.setEdorValiDate("2035-10-28");
		bl.setNeedBugetResultFlag(false);
		bl.setOperator("test");

		LJSGetEndorseSet set = bl.BudgetOnePol("21042792107");
		if(set != null){
			for(int i = 1;i<=set.size();i++){
				if((set.get(i).getFeeFinaType()).equals("RFBF")){
					System.out.println("贷款本金是："+set.get(i).getGetMoney());
				}else if((set.get(i).getFeeFinaType()).equals("RFLX")){
					System.out.println("贷款利息是："+set.get(i).getGetMoney());
				}
			}
		}else{
			System.out.println("还款计算失败");
		}
		

		System.out.println((System.currentTimeMillis() - startTime) / 1000
				+ "秒");
	}
}
