package com.sinosoft.lis.bq;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 客户资料变更
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Tjj
 * @ReWrite ZhangRong,Luomin
 * @author rewrite by QiuYang 2005
 * @version 1.0
 */

public class GEdorRSDetailBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private MMap mMap = new MMap();

	private DetailDataQuery mQuery = null;

	private GlobalInput mGlobalInput = null;

	private String mTypeFlag = null;

	private String stateFlag = null;

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mStopDate = null;

	private String mGrpContNo = null;

	private LCGrpContStateSchema mLCGrpContStateSchema = new LCGrpContStateSchema();

	private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	public boolean submitData(VData cInputData) {
		System.out.println("Begin --------PEdorJMDetailBL");
		if (!getInputData(cInputData)) {
			return false;
		}
        
		if(!insertLCGrpEdor())
		{
			System.out.println("配置失败！");
		}
		if (!checkData())// 检验数据未处理
		{
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到传入数据
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mLCGrpContStateSchema = (LCGrpContStateSchema) cInputData
					.getObjectByObjectName("LCGrpContStateSchema", 0);
			TransferData mTransferData = new TransferData();
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0);

			this.mEdorNo = mLCGrpContStateSchema.getOtherNo();
			this.mEdorType = mLCGrpContStateSchema.getOtherNoType();
			this.mStopDate = mLCGrpContStateSchema.getStartDate();
			this.mGrpContNo = mLCGrpContStateSchema.getGrpContNo();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/*
     * 
     * */
	private String getOccupationType(String cOccupationCode) {
		String sql = "select OccupationType from LDOccupation where '1218610553000'='1218610553000' and  OccupationCode = '"
				+ cOccupationCode + "'  fetch first 2000 rows only ";
		ExeSQL tExeSQL = new ExeSQL();
		String tOccupationType = tExeSQL.getOneValue(sql);
		return tOccupationType;
	}

	/**
	 * 校验数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		if (!CanStop()) {
			ExeSQL texesql = new ExeSQL();
			String jianGongXian = texesql.getOneValue("select 1 from lcgrppol where  grpcontno='"+ mGrpContNo + "'  and riskcode not in ('190106','590206','5901') ");
			if ((jianGongXian == null) || (jianGongXian.equals(""))) {
				mErrors.addOneError("此保单已经暂停期满180天,无法再进行暂停操作！");
				return false;
			}
			else
			{
				mErrors.addOneError("此保单已经暂停期满90天,无法再进行暂停操作！");
				return false;
			}
			
		}
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		mLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		mLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		mLPEdorEspecialDataSchema.setEdorType(mEdorType);
		mLPEdorEspecialDataSchema.setDetailType("STOPDATE");
		mLPEdorEspecialDataSchema.setEdorValue(mStopDate);
		mLPEdorEspecialDataSchema.setPolNo(mGrpContNo);

		mMap.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");

		setEdorState("1");
		return true;
	}

	/**
	 * 校验是否暂停期满
	 * 
	 * @return boolean
	 */
	private boolean CanStop() {
		ExeSQL texesql = new ExeSQL();
		String tIsEnd = texesql
				.getOneValue("select 1 from lcgrpedor where  grpcontno='"
						+ mGrpContNo + "' and ValidFlag='1' and state='0' ");
		if ((tIsEnd == null) || (tIsEnd.equals(""))) {
			return false;
		}
		return true;
	}

	private void setLPEdoreSpecialData(String ZiDuanName, String value) {
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchema.setEdorType(mEdorType);
		tLPEdorEspecialDataSchema.setPolNo("000000");
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchema.setDetailType(ZiDuanName);
		tLPEdorEspecialDataSchema.setEdorValue(value);
		mMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
	}

	/**
	 * 把保全状态设为已录入
	 * 
	 * @param edorState
	 *            String
	 */
	private void setEdorState(String edorState) {
		String sqlgrpitem = "update  LPgrpEdorItem " + "set EdorState = '"
				+ edorState + "', " + "    Operator = '"
				+ mGlobalInput.Operator + "', " + "    ModifyDate = '"
				+ mCurrentDate + "', " + "    ModifyTime = '" + mCurrentTime
				+ "' " + "where  EdorNo = '" + mEdorNo + "' "
				+ "and EdorType = '" + mEdorType + "' ";
		mMap.put(sqlgrpitem, "UPDATE");
		String sqlgrpmain = "update  LPgrpEdormain " + "set EdorState = '"
				+ edorState + "', " + "    Operator = '"
				+ mGlobalInput.Operator + "', " + "    ModifyDate = '"
				+ mCurrentDate + "', " + "    ModifyTime = '" + mCurrentTime
				+ "' " + "where  EdorNo = '" + mEdorNo + "' ";
		mMap.put(sqlgrpmain, "UPDATE");
		String sqlapp = "update  LPEdorapp " + "set EdorState = '" + edorState
				+ "', " + "    Operator = '" + mGlobalInput.Operator + "', "
				+ "    ModifyDate = '" + mCurrentDate + "', "
				+ "    ModifyTime = '" + mCurrentTime + "' "
				+ "where  EdoracceptNo = '" + mEdorNo + "' ";
		mMap.put(sqlapp, "UPDATE");
	}
	
	/**
	 * 建工险在此生成LCGrpEdor表中数据
	 * 
	 * @param edorState
	 *            String
	 */
	private boolean insertLCGrpEdor()
	{
//    	对于保单暂停来说 state:"0"表示未暂停满180天,"1"表示暂停已满180天
		LCGrpEdorSchema tLCGrpEdorSchema=new LCGrpEdorSchema();
		tLCGrpEdorSchema.setGrpContNo(mGrpContNo);
		tLCGrpEdorSchema.setEdorType("RS");
		tLCGrpEdorSchema.setState("0");
//    	ValidFlag:"0"表示无效,"1"表示有效
		tLCGrpEdorSchema.setValidFlag("1");
		tLCGrpEdorSchema.setMakeDate(mCurrentDate);
		tLCGrpEdorSchema.setMakeTime(mCurrentTime);
		tLCGrpEdorSchema.setModifyDate(mCurrentDate);
		tLCGrpEdorSchema.setModifyTime(mCurrentTime);
		tLCGrpEdorSchema.setOperator(mGlobalInput.Operator);
	    MMap tMap = new MMap();
	    tMap.put(tLCGrpEdorSchema, "DELETE&INSERT"); //插入
        VData data = new VData();
		data.add(tMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
}
