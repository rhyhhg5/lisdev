package com.sinosoft.lis.bq;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;

import com.cbsws.obj.DealInfo;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.LPNIZTInsuredSchema;
import com.sinosoft.lis.db.LPNIZTInsuredDB;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理返回报文业务逻辑
 * a)	创建返回报文
 * b)	保存处理结果

 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author LC
 * @version 1.2
 */
public class GedorNIZTDealBL {

    public CErrors mCErrors = new CErrors();

    private VData mInputData = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private MMap map = new MMap();
    
    
    //new define
	// MsgHead信息
	public String mSendDate = "";
	public String mSendTime = "";
	public String mBranchCode = "";
	public String mSendOperator = "";
	public String mMsgType = "";
	
    private DealInfo mDealInfo;
    
    private Document tOutXmlDoc = null;
    
    private LPNIZTInsuredSchema mLPNIZTInsuredSchema = new LPNIZTInsuredSchema();
    
    private String mBatchNo = "";
    
    private String mGrpContNo = "";

    private String mDealResult = "";

    private String mEdorNo = "";
    
    private String mRemark = "";
    
    private String mCurrDate = PubFun.getCurrentDate();
    
	private String mCurrTime = PubFun.getCurrentTime();

    public GedorNIZTDealBL() {
    }

    /**
     * 为外部调用提供的接口，进行逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

       System.out.println("===============GedorNIZTDealBL Start==========");
       if(getSubmitMap(cInputData, cOperate) == null)
       {
           return false;
       }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            System.out.println("delete fail!");
            return false;
        }

        return true;
    }

    /**
     * 为外部调用提供的接口，进行作废逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
	public MMap getSubmitMap(VData cInputData, String cOperate) {

		mOperate = cOperate;

		mInputData = (VData) cInputData;

		if (!mOperate.equals("UPDATE")) {

			return null;
		}

		if (!this.getInputData(mInputData)) {
			return null;
		}

		if (!this.checkData()) {
			return null;
		}

		if (!this.dealData()) {
			return null;
		}

		if (!prepareData()) {
			this.bulidError("prepareData", "存数据到MAP错误");
			return null;
		}
		return map;
	}

    
    /**
     * 得到外部传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mGlobalInput == null || mLPNIZTInsuredSchema == null) {
                this.bulidError("getInputData", "数据不完整");
                return false;
            }
            mLPNIZTInsuredSchema = ((LPNIZTInsuredSchema) mInputData.getObjectByObjectName(
                    "LPNIZTInsuredSchema", 0));
            LPNIZTInsuredDB tLPNIZTInsuredDB = new LPNIZTInsuredDB();
            mBatchNo = mLPNIZTInsuredSchema.getBatchNo();
            mGrpContNo = mLPNIZTInsuredSchema.getGrpContNo();
            tLPNIZTInsuredDB.setBatchNo(mBatchNo);
            tLPNIZTInsuredDB.setGrpContNo(mGrpContNo);
            if (tLPNIZTInsuredDB.getInfo() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GedorNIZTDealBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有查询到批次号数据，请您确认!";
                this.mCErrors.addOneError(tError);
                return false;
            }
            mLPNIZTInsuredSchema.setSchema(tLPNIZTInsuredDB.getSchema());
                        
        	mDealResult = (String) mTransferData.getValueByName("DealResult");
            mEdorNo = (String) mTransferData.getValueByName("EdorNo");
            mRemark = (String) mTransferData.getValueByName("Remark");
        } catch (Exception ex) {
            this.mCErrors.addOneError("数据不完整");
            return false;
        }

        return true;
    }

    /**
     *校验操作的合法性
     * @return boolean
     */
    private boolean checkData() {

        if ("".equals(mDealResult) || mDealResult.equals(null)) {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }
        
        String ResultSql = "select dealresult from LPNIZTInsured " 
        				+  " where batchno='" + mBatchNo + "'";
        
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(ResultSql);
        if(!"".equals(result) && result != null && !"0".equals(result)){
        	this.bulidError("dealresult", "批次号："+ mBatchNo +" 已处理完毕，请勿重复录入处理结果");
            return false;
        }
        
        if("1".equals(mDealResult)){
        	if("".equals(mEdorNo) || mEdorNo.equals(null)){
        		this.bulidError("dealresult", "处理结果为成功，请录入工单号信息");
                return false;
        	}
        	String EdorSql = "select edorstate from lpedorapp " 
				+  " where edoracceptno='" + mEdorNo + "'";
        	String edorstate = tExeSQL.getOneValue(EdorSql);
        	if(!"".equals(edorstate) || edorstate != null || !"0".equals(edorstate)){
        		this.bulidError("dealresult", "工单尚未结案，无法录入处理成功信息");
                return false;
        	}
        }
        return true;
        
    }

    /**
     * 进行处理
     * @return boolean
     */
    private boolean dealData() {

//    	//返回报文信息
//        if(!dealXml()){
//            return false;
//        }

        //存储处理结果
		mLPNIZTInsuredSchema.setDealState("2"); // 处理状态，1：审核中，2：已处理
		mLPNIZTInsuredSchema.setEdorNo(mEdorNo); //工单号
		mLPNIZTInsuredSchema.setDealResult(mDealResult); // 处理结果，0：未完成，1：成功，2：失败
		mLPNIZTInsuredSchema.setRemark(mRemark); //备注
		mLPNIZTInsuredSchema.setConfDate(mCurrDate); //备注
		mLPNIZTInsuredSchema.setOperator(mGlobalInput.Operator);
		mLPNIZTInsuredSchema.setModifyDate(mCurrDate);
		mLPNIZTInsuredSchema.setModifyTime(mCurrTime);
        return true;
    }


	/**
	 * 返回报文数据
	 * 
	 * @return boolean
	 */
	private boolean dealXml() {
		try {
			tOutXmlDoc = getWrapParmList(mLPNIZTInsuredSchema, "success");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
    
	private Document getWrapParmList(LPNIZTInsuredSchema LPNIZTInsured, String ztFlag) throws Exception {
		// 判断失败还是成功，此处默认为成功
		if ("success".equals(ztFlag) && LPNIZTInsured != null) {
			mDealInfo = new DealInfo();
			mDealInfo.setDealResult(mDealResult);
			mDealInfo.setEdorNo(mEdorNo);
			mDealInfo.setRemark(mRemark);
			return createResultXML();
		} 
		//错误的返回报文
//		else if ("fail".equals(ztFlag)) {
//			mDealInfo = new DealInfo();
//			return createFalseXML();
//		}	
		return null;
	}
	

	/**
	 * 生成正常返回报文
	 * 
	 * @return Document
	 */
	private Document createResultXML() throws Exception {
		// Request部分
		Element tRootData = new Element("DataSet");

		// Head部分
		Element tMsgResHead = new Element("MsgResHead");
		Element tItem = new Element("Item");

		//批次号
		Element tBatchNo = new Element("BatchNo");
		tBatchNo.addContent(mBatchNo);
		tItem.addContent(tBatchNo);

		Element tSendDate = new Element("SendDate");
		tSendDate.addContent(mCurrDate);
		tItem.addContent(tSendDate);

		Element tSendTime = new Element("SendTime");
		tSendTime.addContent(mCurrTime);
		tItem.addContent(tSendTime);

		//报送单位
//		Element tBranchCode = new Element("BranchCode");
//		tBranchCode.addContent(mBranchCode);
//		tItem.addContent(tBranchCode);

		//报送员，当前操作员
		Element tSendOperator = new Element("SendOperator");
		tSendOperator.addContent(mOperate);
		tItem.addContent(tSendOperator);

		Element tMsgType = new Element("MsgType");
		tMsgType.addContent("BQ_NIZT001");
		tItem.addContent(tMsgType);

		Element tState = new Element("State");
		tState.addContent("00");
		tItem.addContent(tState);
		
		Element tErrCode = new Element("ErrCode");
		tErrCode.addContent("");
		tItem.addContent(tErrCode);

		Element tErrInfo = new Element("ErrInfo");
		tErrInfo.addContent("");
		tItem.addContent(tErrInfo);

		tMsgResHead.addContent(tItem);

		// body部分：DealInfo
		Element tDealInfo = new Element("DealInfo");
		Element tItem2 = new Element("Item");

		Element tDealResult = new Element("DealResult");
		tDealResult.addContent(mDealResult);
		tItem2.addContent(tDealResult);		
		
		Element tEdorNo = new Element("EdorNo");
		tEdorNo.addContent(mEdorNo);
		tItem2.addContent(tEdorNo);

		Element tRemark = new Element("Remark");
		tRemark.addContent(mRemark);
		tItem2.addContent(tRemark);

		tDealInfo.addContent(tItem2);

		tRootData.addContent(tMsgResHead);
		tRootData.addContent(tDealInfo);
		Document tDocument = new Document(tRootData);

		return tDocument;
	}
	
    /**
     * 准备数据
     * @return boolean
     */
    public boolean prepareData() {
    	map.put(mLPNIZTInsuredSchema, "UPDATE");
        mInputData.add(map);
        return true;
    }


    /**
     *
     * @param cFunctionName String
     * @param cErrorsMsg String
     */
    private void bulidError(String cFunctionName, String cErrorsMsg) {

        CError tCError = new CError();
        tCError.moduleName = "GedorNIZTDealBL";
        tCError.functionName = cFunctionName;
        tCError.errorMessage = cErrorsMsg;

        this.mCErrors.addOneError(tCError);
    }

    public static void main(String[] args) {

//        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
//        tLJSPaySchema.setGetNoticeNo("31000003372");
//
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("CancelMode", "3");
//
//        GlobalInput g = new GlobalInput();
//        g.ComCode = "86";
//        g.Operator = "endor0";
//
//        VData tVData = new VData();
//        tVData.addElement(tLJSPaySchema);
//        tVData.addElement(g);
//        tVData.addElement(tTransferData);
//
//        IndiLJSCancelBL bl = new IndiLJSCancelBL();
//        if(!bl.submitData(tVData, "INSERT"))
//        {
            System.out.println(Integer.parseInt("1439.41"));
//        }
    }
}
