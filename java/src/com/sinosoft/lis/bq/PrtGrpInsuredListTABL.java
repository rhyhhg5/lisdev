package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListTABL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_TA;

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListTABL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
    	MMap map = getSubmitData();
        if (map == null)
        {
            return false;
        }

        if (!submit(map))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 通过contno and  insuredno  得到某人的保全前个人账户个人交费部分余额
     */
    
    private String getPerAcc(LPDiskImportSchema tLPDiskImportSchema){
    	String sql = "select nvl(sum(money), 0) from LCInsureAccTrace a,lcinsured b " +
    			"where polno in (select polno from lcpol where ContNo = b.ContNo and InsuredNo = b.InsuredNo) " +
    			"and payplancode in (select payplancode from lmdutypay where accpayclass in ('5','6')) " +
    			"and b.grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"'  and b.insuredno ='"+tLPDiskImportSchema.getInsuredNo()+"'";
    	ExeSQL exeSQL = new ExeSQL();
    	String moneyString = exeSQL.getOneValue(sql);
    	System.out.println(moneyString);
    	return moneyString;
    }
    /**
     * 通过contno and  insuredno  得到某人的保全前个人账户单位交费余额
     */
    
    private String getComAcc(LPDiskImportSchema tLPDiskImportSchema){
    	String sql = "select nvl(sum(money), 0) from LCInsureAccTrace a,lcinsured b " +
		"where polno in (select polno from lcpol where ContNo = b.ContNo and InsuredNo = b.InsuredNo) " +
		"and payplancode in (select payplancode from lmdutypay where accpayclass in ('4')) " +
		"and b.grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"'  and b.insuredno ='"+tLPDiskImportSchema.getInsuredNo()+"'";
    	ExeSQL exeSQL = new ExeSQL();
    	String moneyString = exeSQL.getOneValue(sql);
    	System.out.println(moneyString);
    	return moneyString;
    }
    
    /**
     * 得到个人账户总金额  保全前
     */
    private String getAcc(LPDiskImportSchema tLPDiskImportSchema){
    	String sql = "select nvl(sum(money), 0) from LCInsureAccTrace a,lcinsured b " +
		"where polno in (select polno from lcpol where ContNo = b.ContNo and InsuredNo = b.InsuredNo) " +
		"and payplancode in (select payplancode from lmdutypay where accpayclass in ('4','5','6')) " +
		"and b.grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"'  and b.insuredno ='"+tLPDiskImportSchema.getInsuredNo()+"'";
    	ExeSQL exeSQL = new ExeSQL();
    	String moneyString = exeSQL.getOneValue(sql);
    	System.out.println(moneyString);
    	return moneyString;
    	
    }
    
    /**
     * 得到理赔金账户信息
     */
    private String getBankNo(LPDiskImportSchema tLPDiskImportSchema){
    	String sql = "select bankaccno from LCInsured  where grpcontno='"+tLPDiskImportSchema.getGrpContNo()+"'  and insuredno ='"+tLPDiskImportSchema.getInsuredNo()+"'";
    	ExeSQL exeSQL = new ExeSQL();
    	String bankNoString = exeSQL.getOneValue(sql);
    	System.out.println(bankNoString);
    	return bankNoString;
    	
    }
    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_TA);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getInsuredList()
    {
        String sql = "select * from LPDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "order by int(SerialNo) ";
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        return tLPDiskImportDB.executeQuery(sql);
    }
    public static void main(String[] args) {
        /*GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "pa0005";
        String edorno = "20061101000010";
        PrtGrpInsuredListTABL tPrtGrpInsuredListTABL = new PrtGrpInsuredListTABL(tGlobalInput,edorno);
        tPrtGrpInsuredListTABL.submitData();*/
    	PrtGrpInsuredListTABL tPrtGrpInsuredListTABL = new PrtGrpInsuredListTABL();
    	 LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
    	 tLPDiskImportSchema.setGrpContNo("00003554000011");
    	 tLPDiskImportSchema.setInsuredNo("000965677");
    	tPrtGrpInsuredListTABL.getPerAcc(tLPDiskImportSchema);
    	
    }
    /**
     * 无参构造
     */
	public PrtGrpInsuredListTABL() {
		super();
	}
	
	/**
     * 得到要提交的数据，不执行submit操作
     * @return MMap
     */
    public MMap getSubmitData()
    {
        if (!prepareData())
        {
            return null;
        }
        xml = createXML();
        return insertXML(xml);
    }
    /**
     * 为生成XML准备数据
     * @return boolean
     */
    private boolean prepareData()
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mEdorNo);
        tLPGrpEdorItemDB.setEdorType("TA"); //资金分配
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            mErrors.addOneError("未找到保全部分领取项目信息！");
            return false;
        }
        //得到团体合同号
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        this.mGrpContNo = tLPGrpEdorItemSchema.getGrpContNo();

        //得到团体客户名称
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("未查到团体保单信息！");
            return false;
        }
        return true;
    }
    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private XmlExport createXML()
    {
    	xml = new XmlExport();
        xml.createDocument("PrtGrpInsuredListTA.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("TA");
        LPDiskImportSet tLPDiskImportSet = getInsuredList();
            
            
        for (int i = 1; i <= tLPDiskImportSet.size(); i++)
          {
              LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
              String[] column = new String[13];
              column[0] = StrTool.cTrim(tLPDiskImportSchema.getSerialNo());
              column[1] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
              column[2] = StrTool.cTrim(tLPDiskImportSchema.getInsuredNo());
              column[3] = ChangeCodeBL.getCodeName("Sex",
                      tLPDiskImportSchema.getSex());
              column[4] = CommonBL.decodeDate(tLPDiskImportSchema.getBirthday());
              column[5] = ChangeCodeBL.getCodeName("IDType",
                      tLPDiskImportSchema.getIDType());
              column[6] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
              column[7] = StrTool.cTrim(String.valueOf(tLPDiskImportSchema.
                      getMoney()));
              column[8] = StrTool.cTrim(getPerAcc(tLPDiskImportSchema));
              column[9] = Arith.add(CommonBL.carry(getComAcc(tLPDiskImportSchema)), CommonBL.carry(StrTool.cTrim(String.valueOf(tLPDiskImportSchema.getMoney()))))+"";
              column[10] =Arith.add(CommonBL.carry(getAcc(tLPDiskImportSchema)), CommonBL.carry(StrTool.cTrim(String.valueOf(tLPDiskImportSchema.getMoney()))))+"";
              column[11] =getBankNo(tLPDiskImportSchema);
              column[12] =CommonBL.decodeState(tLPDiskImportSchema.getState());
  
              listTable.add(column);
          }
        xml.addListTable(listTable, new String[14]);
        xml.outputDocumentToFile("c:\\", "InsuredList");
        return xml;
    }
    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit(MMap map)
    {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 把产生的BolbXML插入数据库
     * @param xmlExport XmlExport
     */
    private MMap insertXML(XmlExport xml)
    {
        LPEdorPrint2Schema tLPEdorPrint2Schema = new LPEdorPrint2Schema();
        tLPEdorPrint2Schema.setEdorNo(mEdorNo);
        tLPEdorPrint2Schema.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrint2Schema.setPrtFlag("N");
        tLPEdorPrint2Schema.setPrtTimes(0);
        tLPEdorPrint2Schema.setEdorInfo(xml.getInputStream());
        tLPEdorPrint2Schema.setOperator(mGlobalInput.Operator);
        tLPEdorPrint2Schema.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrint2Schema.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrint2Schema.setModifyTime(PubFun.getCurrentTime());

        MMap map = new MMap();
        map.put(tLPEdorPrint2Schema, "BLOBINSERT");
        return map;
    }

}
