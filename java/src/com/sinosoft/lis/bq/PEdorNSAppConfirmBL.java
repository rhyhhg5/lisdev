package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.tb.ProposalBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全申请确认处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong,Lanjun
 * @version 1.0
 */

public class PEdorNSAppConfirmBL implements EdorAppConfirm
{
    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private  String mCalFlag = null;
    
    private  boolean mRiskFlag = true;

    public PEdorNSAppConfirmBL()
    {}

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */ 
    public boolean submitData(VData cInputData, String cOperate)
    {

        //得到外部传入的数据,将数据备份到本类中
        if(!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData data)
    {
        TransferData tTransferData = (TransferData) data
                                     .getObjectByObjectName("TransferData", 0);
        if(tTransferData != null)
        {
            mCalFlag = (String) tTransferData.getValueByName(BQ.EDORTYPE_UW);
        }

        mLPEdorItemSchema = (LPEdorItemSchema) data
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);

        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSAppConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("PEdorNSAppComfirmBL: "
                           + PubFun.getCurrentDate() + " "
                           + PubFun.getCurrentTime() + " "
                           + mGlobalInput.Operator + " "
                           + mGlobalInput.ClientIP);

        return true;
    }

    /**
     * 往批改补退费表（应收/应付）新增数据
     * @return：成功true，否则false
     */
    private boolean setLJSGetEndorse()
    {
        //获取保单表数据
        LPPolSet tLPPolSet = getLPPolAdd();
        if(tLPPolSet == null)
        {
            return false;
        }

        double sumGetMoney = 0;   //总收费
        double sumPrem = 0;  //新险种总保费
        double sumAmnt = 0;  //新险种总保额
        BqCalBL tBqCalBL = new BqCalBL(); //调用其方法生成财务数据
        for(int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);

            LPDutySet tLPDutySet = getLPDuty(tLPPolSchema);
            if(tLPDutySet == null)
            {
                return false;
            }

            for(int t = 1; t <= tLPDutySet.size(); t++)
            {
                sumAmnt += tLPDutySet.get(t).getAmnt();

                //查询责任下缴费项
                LPPremDB db = new LPPremDB();
                db.setEdorNo(tLPDutySet.get(t).getEdorNo());
                db.setEdorType(tLPDutySet.get(t).getEdorType());
                db.setPolNo(tLPDutySet.get(t).getPolNo());
                db.setDutyCode(tLPDutySet.get(t).getDutyCode());

                LPPremSet set = db.query();
                if(set.size() == 0)
                {
                    mErrors.addOneError("查询新增险种责任信息失败");
                    return false;
                }

                for(int m = 1; m <= set.size(); m++)
                {
                    LPPremSchema premSchema = set.get(m);

                    sumPrem += premSchema.getPrem();
                    double getMoney = 0;
                    if(mRiskFlag){
                    	
                    	getMoney = calGetMoney(tLPPolSchema,
                                tLPDutySet.get(t),
                                premSchema);
                    }
                    
                    if(getMoney == Double.MIN_VALUE)
                    {
                        return false;
                    }
                    System.out.println("getMoney: polNo "
                                       + tLPDutySet.get(t).getPolNo()
                                       + ", riskCode " + tLPPolSchema.getRiskCode()
                                       + ", duty " + tLPDutySet.get(t).getDutyCode()
                                       + ": " + getMoney);

                    sumGetMoney += getMoney;

                    //设置批改补退费表
                    LJSGetEndorseSchema tLJSGetEndorseSchema
                        = tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema,
                                                     tLPPolSchema,
                                                     tLPDutySet.get(t), "BF",
                                                     getMoney, mGlobalInput);

                    //从描述表中获取财务接口类型
                    String finType = BqCalBL.getFinType(mLPEdorItemSchema.
                                                        getEdorType(),
                                                        "BF", tLPPolSchema.getPolNo());
                    if(finType.equals(""))
                    {
                        mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码");
                        return false;
                    }
                    tLJSGetEndorseSchema.setFeeFinaType(finType);
                    tLJSGetEndorseSchema
                        .setPayPlanCode(premSchema.getPayPlanCode());
                    mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
                }
            }
        }
        mMap.put(mLJSGetEndorseSet, SysConst.DELETE_AND_INSERT);

        mLPEdorItemSchema.setGetMoney(sumGetMoney);
        mLPEdorItemSchema.setChgPrem(sumPrem);
        mLPEdorItemSchema.setChgAmnt(sumAmnt);
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(mLPEdorItemSchema, SysConst.DELETE_AND_INSERT);

        return true;
    }

    /**
     * 计算缴费
     * @param tLPPolSchema LPPolSchema：需要计算收费的险种
     * @return double
     */
    private double calGetMoney(LPPolSchema polSchema,
                               LPDutySchema dutySchema,
                               LPPremSchema premSchema)
    {
        String calCode = getCalCode(polSchema, premSchema);
        if(calCode == null || calCode.equals(""))
        {
            return Double.MIN_VALUE;
        }

        //生效日期类型
        String cValiDateType = getCValiDateType(polSchema);
        if(cValiDateType == null)
        {
            return Double.MIN_VALUE;
        }

        //原险种交至日期
        String payToDate = getPayToDate();
        if(payToDate == null)
        {
            return Double.MIN_VALUE;
        }

        //原险种应缴日期
        String lastPayToDate = getLastPayToDate(payToDate);
        if(lastPayToDate == null)
        {
            return Double.MIN_VALUE;
        }

        Calculator cal = new Calculator();

        cal.setCalCode(calCode);
        cal.addBasicFactor("EdorNo", dutySchema.getEdorNo());
        cal.addBasicFactor("EdorType", dutySchema.getEdorType());
        cal.addBasicFactor("ContNo", dutySchema.getContNo());
        cal.addBasicFactor("PolNo", dutySchema.getPolNo());
        cal.addBasicFactor("RiskCode", polSchema.getRiskCode());
        cal.addBasicFactor("DutyCode", premSchema.getDutyCode());
        cal.addBasicFactor("PayPlanCode", premSchema.getPayPlanCode());
        cal.addBasicFactor("Amnt", CommonBL.bigDoubleToCommonString(
            dutySchema.getAmnt(), "0.00"));
        cal.addBasicFactor("Prem", CommonBL.bigDoubleToCommonString(
            premSchema.getPrem(), "0.00"));
        cal.addBasicFactor("CValiDateType", cValiDateType);
        cal.addBasicFactor("PayToDate", payToDate);
        cal.addBasicFactor("LastPayToDate", lastPayToDate);
        cal.addBasicFactor("EdorAppDate", mLPEdorItemSchema.getEdorAppDate());
        cal.addBasicFactor("EdorValiDate", mLPEdorItemSchema.getEdorValiDate());

        try
        {
            //计算
            String getMoneyStr = cal.calculate();
            if(getMoneyStr == null || getMoneyStr.equals("")
               || getMoneyStr.equals("null"))
            {
                mErrors.addOneError("计算收费失败");
                return Double.MIN_VALUE;
            }
            if(getMoneyStr.indexOf("-") >= 0)
            {
                mErrors.addOneError("险种选择即刻生效，但计算出的收费为负："
                    + CommonBL.carry(getMoneyStr)
                    + "，请检查项目生效日是否在本缴费期内");
                return Double.MIN_VALUE;
            }

            return CommonBL.carry(getMoneyStr);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return Double.MIN_VALUE;
        }
    }

    /**
     * 得到原险种的最小LasyPayToDate
     * @return String：本保单原险种的最小LastPayToDate
     */
    private String getLastPayToDate(String payToDate)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select lastPayToDate ")
            .append("from LJAPayPerson ")
            .append("where sumDuePayMoney >= 0 ")
            .append("   and contNo = '")
            .append(mLPEdorItemSchema.getContNo()).append("' ")
            .append("   and curPayToDate = '").append(payToDate).append("' ")
            .append("order by curPayToDate desc, makeDate desc, makeTime desc ");
        String lastPayToDate = new ExeSQL().getOneValue(sql.toString());
        if(lastPayToDate.equals("") || lastPayToDate.equals("null"))
        {
            mErrors.addOneError("没有查询到保单险种的应缴日");
            return null;
        }
        return lastPayToDate;
    }

    /**
     * 得到P原险种的最小PayToDate
     * @return String：本保单原险种的最小PayToDate
     */
    private String getPayToDate()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select min(payToDate) ")
            .append("from LCPol ")
            .append("where appFlag = '").append(BQ.APPFLAG_SIGN).append("' ")
            .append("   and contNo = '")
            .append(mLPEdorItemSchema.getContNo()).append("' ");
        String payToDate = new ExeSQL().getOneValue(sql.toString());
        if(payToDate.equals("") || payToDate.equals("null"))
        {
            mErrors.addOneError("没有查询到保单险种的交至日");
            return null;
        }

        return payToDate;
    }

    /**
     * 得到生效日期类型
     * @param polSchema LPPolSchema
     * @return String
     */
    private String getCValiDateType(LPPolSchema polSchema)
    {
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPEdorItemSchema.getEdorNo(),
                                      mLPEdorItemSchema.getEdorType());
        if(!tEdorItemSpecialData.query())
        {
            mErrors.addOneError("没有查询到险种的生效时间编码："
                                + polSchema.getRiskCode());
            return null;
        }

        tEdorItemSpecialData.setPolNo(polSchema.getPolNo());
        return tEdorItemSpecialData.getEdorValue(BQ.DETAILTYPE_CVALIDATETYPE);
    }

    /**
     * 得到保全算费的计算代码
     * @param schema LPPolSchema：险种编码
     * @return String：calCode
     */
    private String getCalCode(LPPolSchema polSchema, LPPremSchema premSchema)
    {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(polSchema.getRiskCode());
        tLMPolDutyEdorCalDB.setDutyCode(premSchema.getDutyCode());
        tLMPolDutyEdorCalDB.setEdorType(polSchema.getEdorType());
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if(tLMPolDutyEdorCalSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorNSAppConfirmBL";
            tError.functionName = "getCalCode";
            tError.errorMessage = "没有查询到算费公式代码";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1).getChgPremCalCode();
    }

    /**
     * 查询新增险种险种责任项
     * @param schema LPPolSchema
     * @return LPDutySet：险种信息
     */
    private LPDutySet getLPDuty(LPPolSchema schema)
    {
        LPDutyDB db = new LPDutyDB();
        db.setEdorNo(schema.getEdorNo());
        db.setEdorType(schema.getEdorType());
        db.setPolNo(schema.getPolNo());

        LPDutySet set = db.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("查询新增险种责任信息失败");
            return null;
        }

        return set;
    }

    /**
     * 得到新增的险种信息
     * @return LCPolSet：新增的险种信息
     */
    private LPPolSet getLPPolAdd()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(mLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = new LPPolSet();
        tLPPolSet = tLPPolDB.query();
        if(tLPPolSet == null || tLPPolSet.size() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "getLPPol";
            tError.functionName = "setLJSGetEndorse";
            tError.errorMessage = "查询新增险种信息失败! ";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tLPPolSet;
    }

    /**
     * 处理核保险种，处理核保结论为将挡获减额的险种信息
     * 先调用契约进行保费重算，在备份到p表
     * @return boolean
     */
    private boolean dealUW()
    {
        if(mCalFlag == null || !mCalFlag.equals(BQ.EDORTYPE_UW))
        {
            return true;
        }

        String sql = "select b.* "
                     + "from LPUWMaster a, LPPol b "
                     + "where a.edorNo = b.edorNo "
                     + "   and a.edorType = b.edorType "
                     + "   and a.polNo = b.polNo "
                     + "   and a.edorNo = '"
                     + mLPEdorItemSchema.getEdorNo() + "' "
                     + "   and a.edorType = '"
                     + mLPEdorItemSchema.getEdorType() + "' "
                     + "   and a.contNo = '"
                     + mLPEdorItemSchema.getContNo() + "' "
                     + "   and a.passFlag = '" + BQ.PASSFLAG_CONDITION + "' "
                     + "   and (a.SubMultFlag = '1' or a.SubAmntFlag = '1' ) ";
        System.out.println(sql);
        LPPolSet tLPPolSet = new LPPolDB().executeQuery(sql);

        MMap tMMap = new MMap();
        Reflections ref = new Reflections();
        for(int i = 1; i <= tLPPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            ref.transFields(tLCPolSchema, tLPPolSet.get(i).getSchema());

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mLPEdorItemSchema.getContNo());
            tLCContDB.getInfo();

            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(tLCContDB.getContNo());
            tLCAppntDB.getInfo();

            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            LCDutySet tLCDutySet = tLCDutyDB.query();

            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(tLCContDB.getContNo());
            tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCInsuredDB.getInfo();

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("EdorType", "NS");
            tTransferData.setNameAndValue("SavePolType", "2");

            // 准备传输数据 VData
            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(tLCContDB.getSchema());
            tVData.add(tLCAppntDB.getSchema());
            tVData.addElement(tLCInsuredDB.getSchema());
            tVData.add(tLCPolSchema);
            tVData.add(tLCDutySet);
            tVData.addElement(new LCInsuredRelatedSet());
            tVData.add(tTransferData);

            ProposalBL tProposalBL1 = new ProposalBL();
            if(!tProposalBL1.submitData(tVData, "UPDATE||PROPOSAL"))
            {
                mErrors.copyAllErrors(tProposalBL1.mErrors);
                return false;
            }


            //备份到P表
            String[] tables = {"LCPol", "LCDuty", "LCPrem"};
            for(int t = 0; t < tables.length; t++)
            {
                String tableP = tables[t].replaceFirst("LC", "LP");
                sql = "delete from " + tableP
                      + " where edorNo = '"
                      + tLPPolSet.get(i).getEdorNo() + "' "
                      + "   and edorType = '"
                      + tLPPolSet.get(i).getEdorType() + "' "
                      + "   and polNo = '"
                      + tLPPolSet.get(i).getPolNo() + "' ";
                tMMap.put(sql, SysConst.DELETE);

                sql = "insert into " + tableP
                      + " (select '" + tLPPolSet.get(i).getEdorNo()
                      + "', '" + tLPPolSet.get(i).getEdorType() + "',"
                      + tables[t] + ".* from " + tables[t] +
                      " where PolNo='" + tLPPolSet.get(i).getPolNo() + "')";
                tMMap.put(sql, "INSERT");
            }
        }

        VData data = new VData();
        data.add(tMMap);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            mErrors.addOneError("处理险种核保信息出错");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	if(!checkData()){
    		return false;
    	}
        if(!dealUW())
        {
            return false;
        }

        //往批改补退费表（应收/应付）新增数据
        if(!setLJSGetEndorse())
        {
            return false;
        }
        //将核保加费放入到C表中
        if(!dealPremAdd())
        {
            return false;
        }
        
        if(!mRiskFlag){
        	
        	
        	String sqlcvalidate = "select cvalidate from lccont where contno='"+mLPEdorItemSchema.getContNo()+"' ";
            ExeSQL e = new ExeSQL();
            
            String tCvilidate = e.getOneValue(sqlcvalidate);
        	       	
          	 	String upPolYears = getUpContYearsE(mLPEdorItemSchema.getEdorValiDate(),tCvilidate);
               if(upPolYears == null)
               {
                   mErrors.addOneError("计算保单年度出错");
                   return false;
               }
               String cvalisql = "select date('" + tCvilidate
                     + "') + " + upPolYears + " year "
                     + "from dual ";
               String tcvalidate = new ExeSQL().getOneValue(cvalisql);
               String edorSql = "select edorvalue "
                   + " from LPEdorEspecialData  "
                   + " where edorno = '" +mLPEdorItemSchema.getEdorNo()+ "'" 
                   + " and detailtype='CVALIDATETYPE' ";
               String edorvalue = new ExeSQL().getOneValue(edorSql);
               if(edorvalue.equals("1")){
            	   tcvalidate = mLPEdorItemSchema.getEdorValiDate();
               }
               
               String sql = "update LPPol a " +
       	       " set (cvalidate) = "
       	       + "    ('"+tcvalidate+"') "
//       	       + "   ModifyDate = '" + mCurDate + "', "
//       	       + "   ModifyTime = '" + mCurTime + "', "
//       	       + "   Operator = '" + mGI.Operator + "' " +
       	       +"where CONTNO = '" + mLPEdorItemSchema.getContNo() + "' AND EDORNO='"+mLPEdorItemSchema.getEdorNo()+"' ";
               mMap.put(sql, SysConst.UPDATE);
               
               sql = "update LCPol a " +
       	       " set (cvalidate) = "
       	       + "    ('"+tcvalidate+"') "
//       	       + "   ModifyDate = '" + mCurDate + "', "
//       	       + "   ModifyTime = '" + mCurTime + "', "
//       	       + "   Operator = '" + mGI.Operator + "' " +
       	       +"where CONTNO = '" + mLPEdorItemSchema.getContNo() + "' AND RISKCODE ='332301' AND APPFLAG='2' ";
               mMap.put(sql, SysConst.UPDATE);
               
                 	
          	
          
           
        }

        String sql = "update LPCont a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LCPrem "
                     + "   where contNo = a.contNo) "
                     + "where edorNo = '" + mLPEdorItemSchema.getEdorNo() + "' "
                     + "   and edorType = '"
                     + mLPEdorItemSchema.getEdorType() + "' "
                     + "   and contNo = '"
                     + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql, SysConst.UPDATE);

        return true;
    }
    
    private String getUpContYearsE(String edorValiDate,String tCvilidate)
    {
            //这里要减去一天，这样才能算出保单年度
        String sql = "select case when (date('"+edorValiDate+"') - 1 days) > (date('"+getPayToDate()+"')-1 days ) "
        +" then (date('"+getPayToDate()+"')-1 days)"
        +" else  (date('"+edorValiDate+"') - 1 days) end from dual ";
        ExeSQL e = new ExeSQL();
        String tEdorValiDate = e.getOneValue(sql);
        System.out.println(tEdorValiDate);
      
       if(tEdorValiDate == null || tEdorValiDate.equals(""))
       {
           return null;
       }
       //因为这里计算出的保单年度是从0开始的，所以下面要加一
       int years = PubFun.calInterval(tCvilidate, tEdorValiDate, "Y");

       return String.valueOf(years + 1);

    }
    
    private boolean checkData(){
    	
//    	String tSQL2 = "SELECT 1 FROM lcpol WHERE contno='"+mLPEdorItemSchema.getContNo()+"' and riskcode in ('340101','340301','730201','340401','335301') and appflag='1' and stateflag='1' ";
    	String tSQL2 = "SELECT 1 FROM lcpol WHERE contno='"+mLPEdorItemSchema.getContNo()+"' and riskcode in ('340301','340401','335301','335302','730503') and appflag='1' and stateflag='1' ";
		ExeSQL exesql = new ExeSQL();
//		boolean t340101Flag = false;
		//by_hehongwei 新增险种只能追加健康一生个人护理保险（万能型）340501
		boolean t730201Falg = false; //福满人生、今生、美满今生、美满一生可添加附件万能B
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL2);
		if (tSSRS.getMaxRow() > 0) {
			t730201Falg = true;
		}
    	if(t730201Falg){
    		StringBuffer sql = new StringBuffer();
            sql.append("select * ")
                .append("from LPPol ")
                .append("where edorNo = '")
                .append(mLPEdorItemSchema.getEdorNo()).append("' ")
                .append("   and edorType = '")
                .append(mLPEdorItemSchema.getEdorType()).append("' ")
                .append("   and contNo = '")
                .append(mLPEdorItemSchema.getContNo()).append("' ");

            LPPolSet tLPPolSet = new LPPolDB().executeQuery(sql.toString());
            Reflections ref = new Reflections();
            for(int i = 1; i <= tLPPolSet.size(); i++)
            {
            	LPPolSchema tLPPolSchema = new LPPolSchema();
                ref.transFields(tLPPolSchema, tLPPolSet.get(i));
                if(!"334801".equals(tLPPolSchema.getRiskCode())&& !"340501".equals(tLPPolSchema.getRiskCode()) && !"340602".equals(tLPPolSchema.getRiskCode())  ){
                	
//                	 mErrors.addOneError("美满今生或美满一生或美满尊享的保单下不能添加非健康一生个人护理保险（万能型）以外的险种："+tLPPolSchema.getRiskCode());
                	mErrors.addOneError("新增险种不能添加非健康一生个人护理保险（万能型,A款）以外的险种："+tLPPolSchema.getRiskCode());
                     return false;
                }
                
            }
            FDate tFDate = new FDate();
//            if(!tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()).equals(tFDate.getDate(PubFun.getCurrentDate())))
//            {
//            	mErrors.addOneError("金生无忧或者美满今生或福满人生或美满一生的保单新增附加个人护理险种时理算日期需要与保全生效日期一致：保全生效日期为"+mLPEdorItemSchema.getEdorValiDate());
//                return false;
//            }

//            String edorvalidate = "select edorvalidate +1 days from lpedoritem where edorno='"+mLPEdorItemSchema.getEdorNo()+"' with ur";
//            ExeSQL e = new ExeSQL();
//            String tEdorValiDate = e.getOneValue(edorvalidate);
//            if(tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(tEdorValiDate))==0||tFDate.getDate(PubFun.getCurrentDate()).compareTo(tFDate.getDate(mLPEdorItemSchema.getEdorValiDate()))==0)
//            {
//            }else{
//            	mErrors.addOneError("金生无忧或者美满今生或福满人生或美满一生的保单新增附加个人护理险种时理算日期不能大于保全生效日期超过一天：保全生效日期为"+mLPEdorItemSchema.getEdorValiDate());
//                return false;
//            }

    	}
    	mRiskFlag = false;
    	return true;
    }

    /**
     * 将核保加费放入到C表中
     * @return boolean 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealPremAdd()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from LPPrem ")
            .append("where edorNo = '")
            .append(mLPEdorItemSchema.getEdorNo()).append("' ")
            .append("   and edorType = '")
            .append(mLPEdorItemSchema.getEdorType()).append("' ")
            .append("   and contNo = '")
            .append(mLPEdorItemSchema.getContNo()).append("' ")
            .append("   and payPlanCode like '000000%' ");

        LPPremSet tLPPremSet = new LPPremDB().executeQuery(sql.toString());
        Reflections ref = new Reflections();
        for(int i = 1; i <= tLPPremSet.size(); i++)
        {
            LCPremSchema schema = new LCPremSchema();
            ref.transFields(schema, tLPPremSet.get(i));

            schema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(schema);
            mMap.put(schema, SysConst.DELETE_AND_INSERT);
        }

        String sql2 = "update LCDuty a "
                      + "set (prem, sumPrem) = "
                      + "   (select sum(prem), sum(sumPrem) "
                      + "   from LCPrem "
                      + "   where polNo = a.polNo "
                      + "      and dutyCode = a.dutyCode) "
                      + "where contNo = '"
                      + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql2, SysConst.UPDATE);

        sql2 = "update LCPol a "
               + "set (prem, sumPrem) = "
               + "   (select sum(prem), sum(sumPrem) "
               + "   from LCPrem "
               + "   where polNo = a.polNo) "
               + "where contNo = '"
               + mLPEdorItemSchema.getContNo() + "' ";
        mMap.put(sql2, SysConst.UPDATE);

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("20060926000002");
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_NS);
        tLPEdorItemSchema = (tLPEdorItemSchema.getDB().query()).get(1);

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPEdorItemSchema);

        PEdorNSAppConfirmBL tPEdorNSAppConfirmBL = new PEdorNSAppConfirmBL();
        if(!tPEdorNSAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tPEdorNSAppConfirmBL.mErrors.getErrContent());
        }
        else
        {
            System.out.println("Submit Succed!");
        }
    }

}
