package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class GrpEdorTLConfirmBL implements EdorConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mGrpContNo = null;

    private ValidateEdorData2 mValidateEdorData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo,
                mEdorType, mGrpContNo, "GrpContNo");
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateEdorData())
        {
            return false;
        }
        return true;
    }

    /**
     * 使保全数据生效
     * @return boolean
     */
    private boolean validateEdorData()
    {
        String[] addTables = {"LCInsureAccTrace"};
        mValidateEdorData.addData(addTables);
        String[] chgTables = {"LCInsureAcc","LCInsureAccClass"};
        mValidateEdorData.changeData(chgTables);
        mMap.add(mValidateEdorData.getMap());
        return true;
    }

}
