package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
* <p>Title: 余额领取清单</p>
* <p>Description:余额领取清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListLQBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_LQ;

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListLQBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!createXML())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PrtGrpInsuredListLQ.vts", "printer");

        //设置批单中TextTag要显示的信息
        if(mGrpContNo==null ||mGrpContNo.equals(""))
        {
            return false;
        }
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("LQ");
        LPDiskImportSet tLPDiskImportSet = getInsuredList();
        for (int i = 1; i <= tLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String[] column = new String[10];
            column[0] = StrTool.cTrim(tLPDiskImportSchema.getSerialNo());
            column[1] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
            column[2] = StrTool.cTrim(tLPDiskImportSchema.getInsuredNo());
            column[3] = ChangeCodeBL.getCodeName("Sex",
                    tLPDiskImportSchema.getSex());
            column[4] = StrTool.cTrim(tLPDiskImportSchema.getBirthday());
            column[5] = ChangeCodeBL.getCodeName("IDType",
                    tLPDiskImportSchema.getIDType());
            column[6] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
            column[7] = StrTool.cTrim(String.valueOf(tLPDiskImportSchema.
                    getMoney2()));
            column[8] = StrTool.cTrim(String.valueOf(tLPDiskImportSchema.getGetMoney()));
            column[9] = CommonBL.decodeState(tLPDiskImportSchema.getState());
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[9]);
        //xml.outputDocumentToFile("c:\\", "ZBInsuredList");
        return true;
    }

    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_LQ); //增人
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getInsuredList()
    {
        String sql = "select * from LPDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "union " +
                "select * from LBDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "order by SerialNo ";
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        return tLPDiskImportDB.executeQuery(sql);
    }
}
