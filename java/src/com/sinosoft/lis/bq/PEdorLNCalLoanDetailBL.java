package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发项目明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PEdorLNCalLoanDetailBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局基础数据 */
	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private TransferData mTransferData = null;

	private String mXQFlag = "";

	private String mPrem = "";
	
	private String mRiskCode = "";

	private MMap map = new MMap();

	private Reflections ref = new Reflections();

	private String currDate = PubFun.getCurrentDate();

	private String currTime = PubFun.getCurrentTime();

	private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

	public PEdorLNCalLoanDetailBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) {
			return false;
		}
		PubSubmit tSubmit = new PubSubmit();

		if (!tSubmit.submitData(mResult, "")) { // 数据提交
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("PEdorCTDetailBL End PubSubmit");
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
			mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mGlobalInput == null || mLPEdorItemSchema == null || mTransferData == null) {
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "输入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mXQFlag = (String) mTransferData.getValueByName("XQFlag");
		mPrem = (String) mTransferData.getValueByName("Prem");
		mRiskCode = (String)mTransferData.getValueByName("riskcode");
		if (mXQFlag == null || mXQFlag.equals("") || mPrem == null || mPrem.equals("")||mRiskCode==null||"".equals(mRiskCode)) {
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {

		mLPEdorItemSchema.setEdorAppDate(currDate);
		mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		mLPEdorItemSchema.setEdorValiDate(currDate);

		double tCashValue = 0;

		EdorCalZT tEdorCalZT = new EdorCalZT("1");
		try {
			tEdorCalZT.calZTData(mLPEdorItemSchema);
		} catch (Exception ex) {
			// throw ex;
			// @@错误处理
			System.out.println("PEdorLNCalLoanDetailBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNCalLoanDetailBL";
			tError.functionName = "dealData";
			tError.errorMessage = "计算现金价值失败";
			mErrors.addOneError(tError);
			return false;
		}

		for (int i = 1; i <= tEdorCalZT.mLJSGetEndorseSet.size(); i++) {
			tCashValue += -tEdorCalZT.mLJSGetEndorseSet.get(i).getGetMoney();
		}

		tCashValue = PubFun.setPrecision(tCashValue, "0.00");

		LPEdorEspecialDataSchema tCASHVALUELPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tCASHVALUELPEdorEspecialDataSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorNo());
		tCASHVALUELPEdorEspecialDataSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tCASHVALUELPEdorEspecialDataSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tCASHVALUELPEdorEspecialDataSchema.setDetailType("CASHVALUE");
		tCASHVALUELPEdorEspecialDataSchema.setPolNo("000000");
		tCASHVALUELPEdorEspecialDataSchema.setEdorValue(Double.toString(tCashValue));

		//查询保单机构是否有特殊配置
		ExeSQL exc = new ExeSQL();
//		String checkComSql = " select 1 from ldcode where codetype = 'loancom' and code =" +
//				" (select case when managecom = '86' then managecom else substr(managecom,1,4) end " +
//				" from lccont where contno='"+mLPEdorItemSchema.getContNo()+"') fetch first 1 rows only with ur ";
		String loanrateSql = "";
			
			loanrateSql = "select canloanrate from LMComLoanRate where managecom = (" +
					" select case when managecom = '86' then managecom else substr(managecom,1,4) end "
              + "  from  lccont where contno='"+mLPEdorItemSchema.getContNo()+"' ) "
              + " and riskcode = '"+mRiskCode+"' and int(minpremlimit)<="+mPrem+" "
              + " and (int(nvl(maxpremlimit,0))>"+mPrem+" or maxpremlimit is null or maxpremlimit = '')" 
              + " fetch first 1 rows only with ur ";
			
		if(exc.getOneValue(loanrateSql)==null||"".equals(exc.getOneValue(loanrateSql))){
			loanrateSql = "select canrate from lmloan where riskcode = (select riskcode from lcpol where polno='" + mLPEdorItemSchema.getPolNo() + "')";
		}
		String loanrate = exc.getOneValue(loanrateSql);
		double canloan = tCashValue;
		if (mXQFlag.equals("1")) {
			canloan = tCashValue - Double.parseDouble(mPrem);
		}
		if (loanrate == null || loanrate.equals("")) {
			// @@错误处理
			System.out.println("PEdorLNCalLoanDetailBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNCalLoanDetailBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取可贷款额度比例失败";
			mErrors.addOneError(tError);
			return false;
		}
		canloan = PubFun.setPrecision(canloan * Double.parseDouble(loanrate), "0.00");
		if (canloan < 0) {
			canloan = 0;
		}
		LPEdorEspecialDataSchema tCANLOANLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tCANLOANLPEdorEspecialDataSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorNo());
		tCANLOANLPEdorEspecialDataSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tCANLOANLPEdorEspecialDataSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tCANLOANLPEdorEspecialDataSchema.setDetailType("CANLOAN");
		tCANLOANLPEdorEspecialDataSchema.setPolNo("000000");
		tCANLOANLPEdorEspecialDataSchema.setEdorValue(Double.toString(canloan));

		map.put(tCASHVALUELPEdorEspecialDataSchema, "DELETE&INSERT");
		map.put(tCANLOANLPEdorEspecialDataSchema, "DELETE&INSERT");
		return true;
	}

	/**
	 * 根据前面的输入数据，进行校验处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareData() {
		mResult.clear();
		mResult.add(map);
		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}

}
