package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import java.util.*;

public class BqConfirmUI
{
    private BqConfirmBL mBqConfirmBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public BqConfirmUI(GlobalInput gi, String edorNo, String contType,String balanceMethodValue)
    {
        mBqConfirmBL = new BqConfirmBL(gi, edorNo, contType,balanceMethodValue);
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mBqConfirmBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mBqConfirmBL.mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mBqConfirmBL.getMessage();
    }

    /**
     * 自动审批是否成功标记
     * @return String
     */
    public String getFailType()
    {
        return mBqConfirmBL.getFailType();
    }

    /**
     * 判断自动核保是否成功
     * 成功返回"1"，失败返回"0"
     * @return String
     */
    public String autoUWFail()
    {
        return mBqConfirmBL.autoUWFail();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String args[])
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "pa0001";
        gi.ComCode = "86";
        gi.ManageCom = "86";
        BqConfirmUI tBqConfirmBL = new BqConfirmUI(gi, "20070402000002",
                BQ.CONTTYPE_P, "1");
        if (!tBqConfirmBL.submitData())
        {
            System.out.println("保全确认未通过！原因是：" + tBqConfirmBL.getError());
        }

    }
}
