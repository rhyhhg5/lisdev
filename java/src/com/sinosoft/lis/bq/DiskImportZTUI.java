package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPDiskImportSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class DiskImportZTUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportZTBL mDiskImportZTBL = null;

    public DiskImportZTUI()
    {
        mDiskImportZTBL = new DiskImportZTBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportZTBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportZTBL.mErrors);
            return false;
        }
        //执行成功当时部分人员不是本保单客户
        if(mDiskImportZTBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportZTBL.mErrors);
        }

        return true;
    }

    /**
     * 得到减人明细处理后成功导入的人数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportZTBL.getImportPersons();
    }

    /**
     * 得到减人明细处理后成功导入的人数
     * @return int
     */
     public int getNotImportPersons()//*******
    {
        return mDiskImportZTBL.mImportPersonsForZTCF;
    }
    /**
     * 得到重复人的信息
     * @return LPDiskImportSet
     */
    public LPDiskImportSet getLPDiskImportSetForZTCF(){
        return mDiskImportZTBL.mLPDiskImportSetForZTCF;
    }

    /**
     * 传给减人明细处理类的人数
     * @return int
     */
    public int getImportPersonsForZT()
    {
        return mDiskImportZTBL.getImportPersonsForZT();
    }
    /**
     * 获得重复人信息,姓名,证件号
     * @return String
     */
    public String getDiskImportName(){
        String message=" ";

        for(int i=1;i<=mDiskImportZTBL.mLPDiskImportSetForZTCF.size();i++)
        {
          message +="姓名: "+ mDiskImportZTBL.mLPDiskImportSetForZTCF.get(i).getInsuredName()
                  +" , "
                  + "证件号: "+mDiskImportZTBL.mLPDiskImportSetForZTCF.get(i).getIDNo()+"<br>";

        }
        return message;
    }

    public static void main(String[] args)
    {
        DiskImportZTUI ztdiskimportui = new DiskImportZTUI();
    }
}
