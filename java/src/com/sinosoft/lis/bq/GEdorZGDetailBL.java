package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsureAccTraceDB;


public class GEdorZGDetailBL
{
    public CErrors mErrors = new CErrors();

    private String mMessage = null;

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private EdorItemSpecialData mSpecialData = null;

    private String mEdorNo = null;

    private static String mEdorType = BQ.EDORTYPE_ZG;

    private String mGrpContNo = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     * @param grpContNo String
     */
    public GEdorZGDetailBL(GlobalInput gi, String edorNo,
            String grpContNo, EdorItemSpecialData specialData)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = grpContNo;
        this.mSpecialData = specialData;
    }

    /**
     * 提交数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private boolean getInputData()
    {
    	if(null==mGlobalInput){
    		mErrors.addOneError("输入数据有误!");
    	    return false;
    	}
    	if(null==mEdorNo || "".equals(mEdorNo)){
    		mErrors.addOneError("没有接收到工单信息!");
    	    return false;
    	}
    	if(null==mGrpContNo || "".equals(mGrpContNo)){
    		mErrors.addOneError("获取保单信息失败!");
    	    return false;
    	}
    	
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!checkData())
        {
            return false;
        }
        setSpecialData();
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkClaiming())
        {
            return false ;
        }
        return true;
    }

    /**
     * 保存团单追加金额
     */
    private void setSpecialData()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 把保全状态设为已录入
     * @param edorState String
     */
    private void setEdorState(String edorState)
    {
        String sql = "update  LPGrpEdorItem " +
                "set EdorState = '" + edorState + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where  EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    /**
     *
     * @return boolean
     */

    private boolean checkClaiming()
    {
        String sql = " select rgtno from llregister where rgtobjno='"+mGrpContNo+"' and rgtstate not in ('04','05') and declineflag is null ";
        ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if(rs==null||rs.getMaxRow()==0)
        {
            LCInsureAccTraceDB db = new LCInsureAccTraceDB();
            db.setGrpContNo(mGrpContNo);
            db.setState("temp");
            LCInsureAccTraceSet tempSet = db.query();
            if(tempSet ==null || tempSet.size()==0)
            {
                return true;
            }
            else
            {
                mErrors.addOneError("帐户轨迹表中有理赔临时记录，不能操作");
                return false;
            }
        }
        else
        {
            StringBuffer tem = new StringBuffer();
            tem.append("该单正在理赔或有未撤件的理赔申请，不能操作,批次号");
            for(int i = 1;i<=rs.getMaxRow();i++)
            {
                tem.append(rs.GetText(i,1));
                tem.append("  ");
            }
            mErrors.addOneError(tem.toString());
            return false;
        }
    }
}
