package com.sinosoft.lis.bq;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

//程序名称：PEdorWXDetailUI.java
//程序功能：
//创建日期：2008-05-20
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容

public class PEdorWXDetailUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public PEdorWXDetailUI() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		PEdorWXDetailBL tPEdorWXDetailBL = new PEdorWXDetailBL();
		if (!tPEdorWXDetailBL.submitData(cInputData, mOperate)) {
			// @@错误处理
			mErrors.copyAllErrors(tPEdorWXDetailBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorWXDetailUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败!";
			mErrors.addOneError(tError);
			mResult.clear();
			return false;
		} else
			mResult = tPEdorWXDetailBL.getResult();
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
	}
}
