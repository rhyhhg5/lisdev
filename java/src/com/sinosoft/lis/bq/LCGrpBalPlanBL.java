package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCGrpBalPlanDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCGrpBalPlanSchema;

/**
 * <p>Title: 定期结算计划表的维护的公共程序</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCGrpBalPlanBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap map = new MMap();

    /** 往后面传输数据的容器 */
    private VData mInputData = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;

    //统一更新日期，时间
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LGWorkSchema mLGWorkSchema = null;
    /** 小组信箱号*/
    private String mWorkBoxNo = null;

    public LCGrpBalPlanBL() {
    }

    /**
     * 数据传书方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //数据校验
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LCGrpBalPlanBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");

        mInputData = null;
        return true;
    }

    /**
     * 获取从UI传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mInputData = cInputData;
        if (this.mInputData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->getInputData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mLGWorkSchema = (LGWorkSchema) mInputData.getObjectByObjectName(
                "LGWorkSchema", 0);
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mWorkBoxNo = (String) mInputData.getObjectByObjectName("String",0);
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData() {
        //检查计划表里
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(mLGWorkSchema.getContNo());
        if (!tLCGrpBalPlanDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLCGrpBalPlanDB.getState().equals("0")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCGrpBalPlanDB.getBalIntv() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpBalPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
//      针对保单失效，以后进行控制 2006-10-24
//        if (!tLCGrpBalPlanDB.getFlag().equals("0")) {
//           // @@错误处理
//           CError tError = new CError();
//           tError.moduleName = "LCGrpBalPlanBL";
//           tError.functionName = "checkData";
//           tError.errorMessage = "数据处理失败LCGrpBalPlanBL-->checkData!该保单已经失效";
//           this.mErrors.addOneError(tError);
//           return false;
//       }


        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData() {
        if (!(updateLCGrpBalPlan("1"))) {
            return false;
        }
        VData tVData = new VData();
        tVData.add(mLGWorkSchema);
        tVData.add(this.mGlobalInput);
        tVData.add(this.mWorkBoxNo);
        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMap = tTaskInputBL.getSubmitData(tVData, "");
        if (tMap == null) {
            this.mErrors.copyAllErrors(tTaskInputBL.mErrors);
            return false;
        }
        this.map.add(tMap);
        return true;
    }

    /**
     * 准备向后台传输的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        this.mInputData.clear();
        this.mInputData.add(map);
        this.mResult.clear();
        return true;
    }

    /**
     * 获取结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    //修改团体保单定期结算状态
    private boolean updateLCGrpBalPlan(String state) {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(mLGWorkSchema.getContNo());
        if (!tLCGrpBalPlanDB.getInfo()) {
            mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTimeBL";
            tError.functionName = "updateLCGrpBalPlan";
            tError.errorMessage = "查询团体保单结算计划信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCGrpBalPlanDB.setState(state);
        tLCGrpBalPlanDB.setModifyDate(this.mCurrentDate);
        tLCGrpBalPlanDB.setModifyTime(this.mCurrentTime);
        tLCGrpBalPlanDB.setOperator(mGlobalInput.Operator);
        LCGrpBalPlanSchema tLCGrpBalPlanSchema=tLCGrpBalPlanDB.getSchema();
        this.map.put(tLCGrpBalPlanSchema, "UPDATE");
        return true;
    }
}
