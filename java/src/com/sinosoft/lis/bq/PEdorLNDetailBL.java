package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发项目明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorLNDetailBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局基础数据 */
	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private MMap map = new MMap();

	private Reflections ref = new Reflections();

	private String currDate = PubFun.getCurrentDate();

	private String currTime = PubFun.getCurrentTime();

	private TransferData mTransferData = new TransferData();

	private String mEdorNo = new String();

	private String mEdorType = new String();

	private String mLoanMoney = new String();

	private String mPolNo = new String();

	private LPLoanSchema mLPLoanSchema = new LPLoanSchema();

	public PEdorLNDetailBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) {
			return false;
		}
		PubSubmit tSubmit = new PubSubmit();

		if (!tSubmit.submitData(mResult, "")) { // 数据提交
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("PEdorCTDetailBL End PubSubmit");
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mGlobalInput == null || mTransferData == null) {
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "输入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mEdorNo = (String) mTransferData.getValueByName("EdorNo");
		mEdorType = (String) mTransferData.getValueByName("EdorType");
		mLoanMoney = (String) mTransferData.getValueByName("LoanMoney");
		mPolNo = (String) mTransferData.getValueByName("PolNo");
		if (mEdorNo == null || mEdorNo.equals("") || mEdorType == null || mEdorType.equals("") || mLoanMoney == null || mLoanMoney.equals("") || mPolNo == null || mPolNo.equals("")) {
			CError tError = new CError();
			tError.moduleName = "PEdorCTDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setEdorNo(mEdorNo);
		tLPEdorItemDB.setEdorType(mEdorType);
		if (tLPEdorItemDB.query().size() != 1) {
			// @@错误处理
			System.out.println("PEdorLNDetailBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorLNDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全表信息失败";
			mErrors.addOneError(tError);
			return false;
		}
		mLPEdorItemSchema = tLPEdorItemDB.query().get(1);

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		// 首先更新其后的保全项目的状态
		mLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		mLPEdorItemSchema.setModifyDate(currDate);
		mLPEdorItemSchema.setModifyTime(currTime);
		map.put(mLPEdorItemSchema, "UPDATE");

		mLPLoanSchema.setPolNo(mPolNo);
		mLPLoanSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		mLPLoanSchema.setSerialNo(""); // 现在不用了
		mLPLoanSchema.setActuGetNo(mLPEdorItemSchema.getEdorAcceptNo()); // 暂时填这个，确认时从实付总表里取
		mLPLoanSchema.setLoanType("0"); // 贷款
		// 获得本次贷款是第几次
		String tSql = "SELECT max(int(OrderNo))+1 FROM LOLoan WHERE polno='" + mPolNo + "' and LoanType='0'";
		int tOrderNo = 1;
		try {
			String aOrderNo = new ExeSQL().getOneValue(tSql);
			if (aOrderNo != null && (!aOrderNo.equals(""))) {
				tOrderNo = Integer.parseInt(aOrderNo);
			}
		} catch (Exception e) {
			tOrderNo = 1;
		}
		mLPLoanSchema.setOrderNo(String.valueOf(tOrderNo));

		// 起息日和还款日暂时都从当天进行设置,待理算后重新设置
		mLPLoanSchema.setLoanDate(PubFun.calDate(currDate, 3, "D", currDate));
		mLPLoanSchema.setPayOffDate(PubFun.calDate(mLPLoanSchema.getLoanDate(), 6, "M", mLPLoanSchema.getLoanDate()));
		// 贷款金额
		mLPLoanSchema.setSumMoney(mLoanMoney);
		mLPLoanSchema.setInputFlag("1"); // 按照描述进行利息计算
		// mLPLoanSchema.setInterestType("");
		// mLPLoanSchema.setInterestRate("");
		// mLPLoanSchema.setInterestMode("");
		// mLPLoanSchema.setRateCalType("");
		// mLPLoanSchema.setRateCalCode("");
		mLPLoanSchema.setSpecifyRate("1");
		mLPLoanSchema.setLeaveMoney(mLoanMoney);
		// 还清标志 0 －－ 未还清 1 －－ 还清
		mLPLoanSchema.setPayOffFlag("0");
		mLPLoanSchema.setOperator(this.mGlobalInput.Operator);
		mLPLoanSchema.setMakeDate(currDate);
		mLPLoanSchema.setMakeTime(currTime);
		mLPLoanSchema.setModifyDate(currDate);
		mLPLoanSchema.setModifyTime(currTime);
		mLPLoanSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		map.put(mLPLoanSchema, "DELETE&INSERT");

		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorAcceptNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorEspecialDataSchema.setEdorNo(mLPEdorItemSchema.getEdorNo());
		tLPEdorEspecialDataSchema.setEdorType(mLPEdorItemSchema.getEdorType());
		tLPEdorEspecialDataSchema.setDetailType("LOAN");
		tLPEdorEspecialDataSchema.setPolNo(mPolNo);
		tLPEdorEspecialDataSchema.setEdorValue(mLoanMoney);
		map.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");

		return true;
	}

	/**
	 * 根据前面的输入数据，进行校验处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareData() {
		mResult.clear();
		mResult.add(map);
		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}

}
