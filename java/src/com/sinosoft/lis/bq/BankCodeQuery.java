package com.sinosoft.lis.bq;

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

public class BankCodeQuery
{
    private Map mBankMap = new HashMap();

    public BankCodeQuery()
    {
        LDBankSet tLDBankSet = new LDBankDB().query();
        for (int i = 1; i < tLDBankSet.size(); i++)
        {
            LDBankSchema tLDBankSchema = tLDBankSet.get(i);
            mBankMap.put(tLDBankSchema.getBankCode(), tLDBankSchema.getBankName());
        }
    }

    public String getBankName(String bankCode)
    {
        System.out.println("bankCode:" + bankCode);
        if ((bankCode == null) || (bankCode.equals("")))
        {
            return "";
        }
        String bankName = (String) mBankMap.get(bankCode);
        if (bankName == null)
        {
            return "";
        }
        return bankName;
    }
}
