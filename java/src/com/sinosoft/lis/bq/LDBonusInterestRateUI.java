package com.sinosoft.lis.bq;
//程序名称：LMInsuAccRateUI.java
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：zhanggm 
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.LDBonusInterestRateBL;

public class LDBonusInterestRateUI 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	public LDBonusInterestRateUI()
	{
	}

  public boolean submitData(VData cInputData, String cOperate) 
  {
	  LDBonusInterestRateBL tLDBonusInterestRateBL = new LDBonusInterestRateBL();
      if (!tLDBonusInterestRateBL.submitData(cInputData, cOperate)) 
      {
          this.mErrors.copyAllErrors(tLDBonusInterestRateBL.mErrors);
          return false;
      }
      return true;
  }
}
