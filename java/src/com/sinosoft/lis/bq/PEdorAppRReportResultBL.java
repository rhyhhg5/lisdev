package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LPAppRReportDB;
import com.sinosoft.lis.vdb.LPAppRReportDBSet;
import com.sinosoft.lis.db.LPCUWMasterDB;
import com.sinosoft.lis.vschema.LPAppRReportResultSet;
import com.sinosoft.lis.vschema.LPAppRReportSet;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class PEdorAppRReportResultBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData;
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 业务操作类 */
    private LPAppRReportResultSet mLPAppRReportResultSet = new LPAppRReportResultSet();
    private LPAppRReportSchema mLPAppRReportSchema = new LPAppRReportSchema();

    private LPCUWMasterSchema mLPCUWMasterSchema = new LPCUWMasterSchema();

    /** 业务数据 */
    private String mName = "";
    private String mDelSql = "";

    public PEdorAppRReportResultBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata");

        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start PEdorAppRReportResultBL Submit...");

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("PEdorAppRReportResultBL end");

        return true;
    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        map.put(mDelSql, "DELETE");
        map.put(mLPAppRReportResultSet, "INSERT");
        map.put(mLPAppRReportSchema,"UPDATE");
          map.put(mLPCUWMasterSchema,"UPDATE");


        mResult.add(map);

        return true;
    }


    /**
     * dealData
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {

      if (prepareReport() == false)
			return false;
        //先删除已经存过的数据
        mDelSql = "delete from LPAppRReportresult where 1=1 "
                  + " and ProposalContNo= '" +
                  mLPAppRReportResultSet.get(1).getProposalContNo() + "'"
                  + " and prtseq = '" + mLPAppRReportResultSet.get(1).getPrtSeq() +
                  "'"
                  ;
        int tSerialNo = 1;
        for (int i = 1; i <= mLPAppRReportResultSet.size(); i++)
        {
            mLPAppRReportResultSet.get(i).setName(mName);
            mLPAppRReportResultSet.get(i).setSerialNo("" + tSerialNo);
            mLPAppRReportResultSet.get(i).setOperator(mOperator);
            mLPAppRReportResultSet.get(i).setMakeDate(PubFun.getCurrentDate());
            mLPAppRReportResultSet.get(i).setMakeTime(PubFun.getCurrentTime());
            mLPAppRReportResultSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLPAppRReportResultSet.get(i).setModifyTime(PubFun.getCurrentTime());
            tSerialNo++;
        }
        return true;
    }


    /**
     * checkData
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonDB.setCustomerNo(mLPAppRReportResultSet.get(1).getCustomerNo());
        if (!tLDPersonDB.getInfo())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorAppRReportResultBL";
            tError.functionName = "checkData";
            tError.errorMessage = "客户信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLDPersonSchema = tLDPersonDB.getSchema();
        mName = tLDPersonSchema.getName();
        return true;
    }

    private boolean prepareReport()
            {
              //取回复内容
              String tReplyContent = mLPAppRReportSchema.getReplyContent();
              System.out.println(tReplyContent);
              //取生调记录
              String tsql = "select * from LPAppRReport where EdorAcceptNo = '"+mLPAppRReportSchema.getEdorAcceptNo()+"' and prtseq =  '"+mLPAppRReportSchema.getPrtSeq()+"' and replyflag = '0'";

              System.out.println("sql:"+tsql);
              LPAppRReportDB tLPAppRReportDB = new LPAppRReportDB();
              LPAppRReportSet tLPAppRReportSet = new LPAppRReportSet();

              tLPAppRReportSet = tLPAppRReportDB.executeQuery(tsql);

              if(tLPAppRReportSet.size() == 0)
              {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWRReportReplyBL";
                tError.functionName = "prepareReport";
                tError.errorMessage = "此单已经回复!";
                this.mErrors .addOneError(tError) ;
                return false;
              }
              else
              {
                mLPAppRReportSchema = tLPAppRReportSet.get(1);
              }

              mLPAppRReportSchema.setReplyContent(tReplyContent);
              mLPAppRReportSchema.setReplyFlag("1");
              mLPAppRReportSchema.setReplyOperator(mOperate);
              mLPAppRReportSchema.setReplyDate(PubFun.getCurrentDate());
              mLPAppRReportSchema.setReplyTime(PubFun.getCurrentTime());
              mLPAppRReportSchema.setModifyDate(PubFun.getCurrentDate());
              mLPAppRReportSchema.setModifyTime(PubFun.getCurrentTime());

              //核保主表信息
              LPCUWMasterDB tLPCUWMasterDB = new LPCUWMasterDB();
              tLPCUWMasterDB.setEdorNo(mLPAppRReportSchema.getEdorNo());

              if(tLPCUWMasterDB.getInfo() == false)
              {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWRReportBL";
                tError.functionName = "prepareReport";
                tError.errorMessage = "无核保主表信息!";
                this.mErrors .addOneError(tError) ;
                return false;
              }

              mLPCUWMasterSchema = tLPCUWMasterDB.getSchema();


              mLPCUWMasterSchema.setReportFlag("2");

              return true;
	}
    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLPAppRReportResultSet = (LPAppRReportResultSet) cInputData.
                              getObjectByObjectName("LPAppRReportResultSet", 0);
       mLPAppRReportSchema.setSchema((LPAppRReportSchema)cInputData.getObjectByObjectName("LPAppRReportSchema",0));

        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorAppRReportResultBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //取得疾病信息
        if (mLPAppRReportResultSet == null || mLPAppRReportResultSet.size() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorAppRReportResultBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据LPAppRReportResultSet失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

}
