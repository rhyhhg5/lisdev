package com.sinosoft.lis.bq;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BqUpdateUnlockUI {
	public CErrors mErrors = new CErrors();

	private BqUpdateUnlockBL bqBL;

	public BqUpdateUnlockUI() {
		this.bqBL = new BqUpdateUnlockBL();
	}

	public boolean submiteData(VData cInputData) {
		if (!bqBL.submitData(cInputData)) {
			this.mErrors.copyAllErrors(bqBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "BqUpdateUnlockUI";
			tError.functionName = "submitDat";
			tError.errorMessage = "BL�ദ��ʧ��!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
