package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务
 * 团单犹豫项目明细
 * 得到GrpEdorWTDetailUI.java传入的数据，进行犹豫期车保明细录入业务逻辑的处理。
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Yang Yalin
 * @version 1.3
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorWTDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局基础数据 */
    private GlobalInput mGI = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private LPPENoticeSet mLPPENoticeSet = null;
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;  //存储工本费
    private LPContPlanRiskSet mLPContPlanRiskSet = null;

    private String mEdorNo = null;
    private String mEdorType = null;
    private String mGrpContNo = null;
    private String mCrrDate = PubFun.getCurrentDate();
    private String mCrrTime = PubFun.getCurrentTime();

    /** 传出数据的容器 */
    private MMap mMap = new MMap();

    public GrpEdorWTDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(mMap);

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            System.out.println(tSubmit.mErrors.getErrContent());
            return false;
        }
        System.out.println("GrpEdorWTDetailBL End PubSubmit");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                                  .getObjectByObjectName(
                                      "LPGrpEdorItemSchema", 0);
        mLPPENoticeSet = (LPPENoticeSet) cInputData
                         .getObjectByObjectName("LPPENoticeSet", 0);
        mLPContPlanRiskSet = (LPContPlanRiskSet) cInputData
                             .getObjectByObjectName("LPContPlanRiskSet", 0);
        mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) cInputData
                                    .getObjectByObjectName(
                                        "LPEdorEspecialDataSchema", 0);
        if (mGI == null || mLPGrpEdorItemSchema == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }
        if(mLPContPlanRiskSet == null || mLPContPlanRiskSet.size() == 0)
        {
            mErrors.addOneError( "请选择保障计划下险种进行退保。");
            return false;
        }

        mEdorNo = mLPGrpEdorItemSchema.getEdorNo();
        mEdorType = mLPGrpEdorItemSchema.getEdorType();
        mGrpContNo = mLPGrpEdorItemSchema.getGrpContNo();

        System.out.println(getClass().getName() + " "
                           + PubFun.getCurrentDate() + " "
                           + mGI.Operator + " "
                           + mGI.ComCode);

        return true;
    }

    /**
     * 校验当前状态是否可进行明细录入
     * @return boolean
     */
    private boolean checkItem()
    {
        String reasonCode = mLPGrpEdorItemSchema.getReasonCode();  //退保原因

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();

        if (tLPGrpEdorItemSet.size() < 1)
        {
            mErrors.addOneError("输入数据有误,LPGrpEdorItem中没有相关数据");
            return false;
        }
        else
        {
            mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        }
        mLPGrpEdorItemSchema.setReasonCode(reasonCode);

        if(mLPGrpEdorItemSchema.getEdorState().equals(BQ.EDORSTATE_CAL)
           || mLPGrpEdorItemSchema.getEdorState().equals(BQ.EDORSTATE_CONFIRM))
        {
            mErrors.addOneError("保全已理算，不能再进行录入。");
            return false;
        }

        return true;
    }



    /**
     * 必须是处于“未录入”和“录入完毕”的项目才可录入项目信息
     * @return boolean
     */
    private boolean checkData()
    {
        if(!checkItem())
        {
            return false;
        }

        //若选择了特需险的公共账户保障计划，则需选择特需被保人的所有保障计划
        //若现在了特需险的所有被保人保障计划，则需选择公共账户的保障计划
        for(int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            if(!CommonBL.isEspecialPol(mLPContPlanRiskSet.get(i).getRiskCode()))
            {
                continue;
            }
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mLPContPlanRiskSet.get(i)
                                           .getGrpContNo());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i)
                                          .getRiskCode());
            LCContPlanRiskSet set = tLCContPlanRiskDB.query(); //特需险保障计划数
            int selectedPlanCount = 0; //选择了的当前保障计划险种下的保障计划数
            //本保障是否公共帐户保障
            boolean isPublicAccPlan = mLPContPlanRiskSet.get(i).getContPlanCode()
                                      .equals("11");
            boolean hasPublicAccPlan = false;  //当前选择的保障计划是否包含公共帐户保障计划
            //得到险种下所有被选择的保障计划数，并判断当前保障计划是否公共帐户保障计划
            for(int t = 1; t <= mLPContPlanRiskSet.size(); t++)
            {
                if(!mLPContPlanRiskSet.get(i).getRiskCode()
                   .equals(mLPContPlanRiskSet.get(t).getRiskCode())) //只处理当前险种
                {
                    continue;
                }
                if(mLPContPlanRiskSet.get(t).getContPlanCode().equals("11"))
                {
                    hasPublicAccPlan = true;
                }
                selectedPlanCount++;
            }
            if (isPublicAccPlan && selectedPlanCount != set.size())
            {
                mErrors.addOneError("您选择了特需险种公共账户保障计划，"
                                    + "请同时选择所有被保人保障计划。");
                return false;
            }
            //特需险种保障计划数 = 被保人保障计划数 + 1(公共帐户保障计划数)
            if(!isPublicAccPlan && !hasPublicAccPlan
               && set.size() == selectedPlanCount + 1)
            {
                mErrors.addOneError("您选择了所有特需险种的被保人保障计划，"
                                    + "请同时选择公共账户保障计划。");
                return false;
            }
        }

        return true;
    }

    /**
     * 进行业务逻辑处理，存储页面传入的信息，同时将需要退保的保障计划下险种备份到LPPol表。
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
//        mReflections = new Reflections();
        //体检费信息
//        if(!getTestInfo())
//        {
//            return false;
//        }
    	
        dealGBInfo();
   	 if (!jugdeUliByGrpContNo())
	 {
   	    if(!dealContPlan())
        {
           return false;
        }
   	   
   	  if(!dealLPPol())
       {
         return false;
        }
           
	  }else{
		  if(!dealULILPPol())
	        {
	            return false;
	        } 
	  }
     
        

        changeItemState();

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean dealContPlan()
    {
        mMap.put("  delete from LPContPlanRisk "
                 + "where edorNo = '" + mEdorNo + "' "
                 + "  and edorType = '" + mEdorType + "' "
                 + "  and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        String sql = "";
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        for (int i = 1; i <= this.mLPContPlanRiskSet.size(); i++)
        {
            tLCContPlanRiskDB.setGrpContNo(mGrpContNo);
            tLCContPlanRiskDB.setContPlanCode(
                mLPContPlanRiskSet.get(i).getContPlanCode());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i).getRiskCode());

            LCContPlanRiskSet set = tLCContPlanRiskDB.query();
            if(set.size() == 0)
            {
                mErrors.addOneError("没有查询到相应的保障计划。"
                    + tLCContPlanRiskDB.getContPlanCode() + " "
                    + tLCContPlanRiskDB.getRiskCode());
                return false;
            }
            sql = "  insert into LPContPlanRisk "
                  + "   ( select '" + mEdorNo + "', "
                  + "   '" + mEdorType + "', "
                  + "   LCContPlanRisk.* "
                  + "   from LCContPlanRisk "
                  + "   where grpContNo = '" + set.get(1).getGrpContNo()
                  + "'     and contPlanCode = '"
                  + set.get(1).getContPlanCode()
                  + "'     and riskCode = '" + set.get(1).getRiskCode()
                  + "') ";
            mMap.put(sql, "INSERT");
        }
        updateDefaultFields("LPContPlanRisk");
        return true;
    }

    private void updateDefaultFields(String tableName)
    {
        mMap.put("  update " + tableName
              + " set operator = '" + mGI.Operator + "', "
              + "    makeDate = '" + mCrrDate + "', "
              + "    makeTime = '" + mCrrTime + "', "
              + "    modifyDate = '" + mCrrDate + "', "
              + "    modifyTime = '" + mCrrTime + "' "
              + "where edorNo = '" + mEdorNo + "' "
              + "   and edorType = '" + mEdorType + "' "
              + "   and grpContNo = '" + mGrpContNo + "' ", "INSERT");
    }

    /**
     * 生成保全险种信息
     * @return boolean
     */
    private boolean dealLPPol()
    {
        mMap.put("  delete from LPPol "
                 + "where edorNo = '" + mEdorNo + "' "
                 + "   and edorType = '" + mEdorType + "' "
                 + "   and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            mMap.put("insert into LPPol "
                     + "(select '" + mEdorNo
                     + "', '" + mEdorType + "', LCPol.* "
                     + "from LCPol "
                     + "where grpContNo = '"
                     + mLPContPlanRiskSet.get(i).getGrpContNo()
                     + "'  and contPlanCode = '"
                     + mLPContPlanRiskSet.get(i).getContPlanCode()
                     + "'  and riskCode = '"
                     + mLPContPlanRiskSet.get(i).getRiskCode() + "') ", "INSERT");
        }
        updateDefaultFields("LPPol");

        return true;
    }
    /**
     * 生成保全险种信息
     * @return boolean
     */
    private boolean dealULILPPol()
    {
        mMap.put("  delete from LPPol "
                 + "where edorNo = '" + mEdorNo + "' "
                 + "   and edorType = '" + mEdorType + "' "
                 + "   and grpContNo = '" + mGrpContNo + "' ", "DELETE");
        
            mMap.put("insert into LPPol "
                     + "(select '" + mEdorNo
                     + "', '" + mEdorType + "', LCPol.* "
                     + "from LCPol "
                     + "where grpContNo = '"       
                     + mGrpContNo + "') ", "INSERT");
  
        updateDefaultFields("LPPol");

        return true;
    }

    /**
     * 存储工本费信息
     */
    private void dealGBInfo()
    {
        mMap.put("delete from LPEdorEspecialData "
                 + "where edorAcceptNo = '"
                 + mLPGrpEdorItemSchema.getEdorAcceptNo()
                 + "'  and edorType ='" + mLPGrpEdorItemSchema.getEdorType()
                 + "'  and detailType = '" + BQ.DETAILTYPE_GB + "' ",
                 "DELETE");

        if(mLPEdorEspecialDataSchema == null)
        {
            return;
        }
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPGrpEdorItemSchema);
        tEdorItemSpecialData.add(BQ.DETAILTYPE_GB,
                                 mLPEdorEspecialDataSchema.getEdorValue());
        mMap.put(tEdorItemSpecialData.getSpecialDataSet(), "INSERT");
    }

    /**
     * 得到完整的体检通知信息
     * @return boolean
     */
    private boolean getTestInfo()
    {
        //若没有录入体检费信息，则删除之前可能存储了的体检费信息
        deleteTestInfo();
        setLPPENoticeInfo();

        return true;
    }

    /**
     * 处理体检通知信息
     */
    private void setLPPENoticeInfo()
    {
        Reflections tReflections = new Reflections();
        for(int i = 1; i <= mLPPENoticeSet.size(); i++)
        {
            LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
            tLCPENoticeDB.setProposalContNo(
                    mLPPENoticeSet.get(i).getProposalContNo());
            tLCPENoticeDB.setPrtSeq(mLPPENoticeSet.get(i).getPrtSeq());
            if(!tLCPENoticeDB.getInfo())
            {
                continue;
            }
            tReflections.transFields(
                    mLPPENoticeSet.get(i), tLCPENoticeDB.getSchema());
            mLPPENoticeSet.get(i).setGrpContNo(
                    mLPGrpEdorItemSchema.getGrpContNo());
        }

        mMap.put(mLPPENoticeSet, "INSERT");
    }


    /**
     * 删除体检费信息
     */
    private void deleteTestInfo()
    {
        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
        tLPPENoticeDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPPENoticeDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPPENoticeDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());

        LPPENoticeItemDB tLPPENoticeItemDB = new LPPENoticeItemDB();
        tLPPENoticeItemDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        tLPPENoticeItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPPENoticeItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());

        mMap.put(tLPPENoticeDB.query(), "DELETE");
        mMap.put(tLPPENoticeItemDB.query(), "DELETE");
    }

    /**
     * 项目状态为录入完毕
     */
    private void changeItemState()
    {
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPGrpEdorItemSchema.setOperator(mGI.Operator);
        mLPGrpEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
        mMap.put(mLPGrpEdorItemSchema, "UPDATE");
    }
    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(mGrpContNo);
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            return false;
        }
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        String edorNo = "2006";
        String edorType = "WT";
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo(edorNo);
        tLPGrpEdorItemSchema.setEdorNo(edorNo);
        tLPGrpEdorItemSchema.setGrpContNo("0000006202");
        tLPGrpEdorItemSchema.setEdorType(edorType);
        tLPGrpEdorItemSchema.setReasonCode("1");

        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData data = new VData();
        data.add(gi);


        GrpEdorWTDetailBL bl = new GrpEdorWTDetailBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("All OK");
        }
    }
}
