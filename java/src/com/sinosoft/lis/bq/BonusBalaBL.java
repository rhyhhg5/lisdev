package com.sinosoft.lis.bq;

import java.util.HashMap;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMInsuAccRateDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsureAccBalanceSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LMInsuAccRateSchema;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 分红险累积生息处理 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company: sinosoft</p>
 * @author XP
 * @version 1.0
 * @CreateDate：2012-08-02
 */

public class BonusBalaBL {
	
	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGI = new GlobalInput();

	public CErrors mErrors = new CErrors();
	
	public Reflections ref = new Reflections();
	
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度
    
//    结算序号
    private String mBalanceSeq="";

	private LCContSchema mLCContSchema = new LCContSchema();
	
	private LCPolSchema mLCPolSchema = new LCPolSchema();
	
	private LCInsureAccSchema mLCInsureAccSchema = new LCInsureAccSchema();
	private LCInsureAccClassSchema mLCInsureAccClassSchema = new LCInsureAccClassSchema();
	
//	仅仅用来储存结算区间
	private LMInsuAccRateSet mLMInsuAccRateSet=new LMInsuAccRateSet();

	private String mPolNo = "";// 处理红利险种号
	
	private String YXCalCode="";//有效
	private String ZZCalCode="";//失效
	private String FXCalCode="";//复效
	private String JYLPCalCode="";//解约理赔
	private String MQCalCode="";//满期
	
	private ExeSQL mExeSQL=new ExeSQL();
	
    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
//    	获取前台传入的数据
        if (!getInputData(cInputData))
        {
            return false;
        }
        
//    	执行业务处理
        if (!dealData())
        {
            return false;
        }
        return true;
    }
    
//    获取各类算法的calcode以备使用
    public boolean getcalcode(){
    	LMRiskBonusDB tLMRiskBonusDB= new LMRiskBonusDB();
    	tLMRiskBonusDB.setRiskCode(mLCPolSchema.getRiskCode());
    	if(!tLMRiskBonusDB.getInfo()){
    		return false;
    	}
    	Calculator tCalculator=new Calculator();
    	tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
    	tCalculator.addBasicFactor("caltype1","YX");
    	YXCalCode=tCalculator.calculate();
    	tCalculator=new Calculator();
    	tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
    	tCalculator.addBasicFactor("caltype1","ZZ");
    	ZZCalCode=tCalculator.calculate();
    	tCalculator=new Calculator();
    	tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
    	tCalculator.addBasicFactor("caltype1","FX");
    	FXCalCode=tCalculator.calculate();
    	tCalculator=new Calculator();
    	tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
    	tCalculator.addBasicFactor("caltype1","JY");
    	JYLPCalCode=tCalculator.calculate();
    	tCalculator=new Calculator();
    	tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
    	tCalculator.addBasicFactor("caltype1","MQ");
    	MQCalCode=tCalculator.calculate();
    	return true;
    }
    
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        mGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCInsureAccSchema = (LCInsureAccSchema) mInputData.getObjectByObjectName("LCInsureAccSchema", 0);

        if ((mGI == null) || (mLCInsureAccSchema == null) ) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mPolNo=mLCInsureAccSchema.getPolNo();
        if(mPolNo==null||mPolNo.equals("")||mPolNo.equals("null")){
        	// @@错误处理
			System.out.println("BonusBalaBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取险种号失败";
			mErrors.addOneError(tError);
			return false;
        }

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mPolNo);
        if (!tLCPolDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCPolSchema.setSchema(tLCPolDB.getSchema());
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPolSchema.getContNo());
        if (tLCContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        return true;
    }

    
    private boolean SubmitData(MMap tMMap){
        VData data = new VData();
        data.clear();
        data.add(tMMap);
        if (data == null) {
            // @@错误处理
			System.out.println("BonusGetBL+enclosing_method++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "enclosing_method";
			tError.errorMessage = "提交数据为空";
			mErrors.addOneError(tError);
			return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成数据失败!");
            return false;
        }
        return true;
    }
    
    
    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
    	System.out.println("进入---------------BonusBalaBL.java---------------");
    	
/*    	校验是否存在累积生息帐户结算表,如果不存在则生成,并且同时设置baladate为第一笔非0数据进账户的时间.
    	这里会有一个数据库提交,提交失败则返回false
   */
        if (!HaveBalance())
        {
        	// @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "生成帐户结算轨迹表失败!";
			mErrors.addOneError(tError);
			return false;
        }

//      获取需要结算的区间
        if(!GetSumBalaDate()){
        	// @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "分配红利失败!";
			mErrors.addOneError(tError);
			return false;
        }
        if(!getcalcode()){
        	// @@错误处理
			System.out.println("BonusBalaBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取计算代码失败";
			mErrors.addOneError(tError);
			return false;
        }
        for (int i = 1; i <= mLMInsuAccRateSet.size(); i++) {        	
        	MMap tMMap =new MMap();
        	if(!initAcc()){
        		// @@错误处理
				System.out.println("BonusBalaBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "BonusBalaBL";
				tError.functionName = "dealData";
				tError.errorMessage = "初始化保单账户信息失败";
				mErrors.addOneError(tError);
				return false;
        	}
        	double tInterest=0;
//        	若区间经过保单周年日,则进行拆分
        	LMInsuAccRateSet tLMInsuAccRateSet=getRateSet(mLMInsuAccRateSet.get(i));
//        	此处不用4舍5入
        	tInterest=getinterest(tLMInsuAccRateSet);
        	if(tInterest<0){
        		// @@错误处理
				System.out.println("BonusBalaBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "BonusBalaBL";
				tError.functionName = "dealData";
				tError.errorMessage = "计算利息出错";
				mErrors.addOneError(tError);
				return false;
        	}
            if (!calPolYearMonth(mLMInsuAccRateSet.get(i).getStartBalaDate()))
            {
                // @@错误处理
				System.out.println("BonusBalaBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "BonusBalaBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取保单年度出错";
				mErrors.addOneError(tError);
				return false;
            }
        	tMMap.add(getAcc(mLMInsuAccRateSet.get(i),tInterest));
        	
//        	添加并发锁
        	String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                + "where PolNo = '"
                + mLCInsureAccSchema.getPolNo() + "' "
                + "   and InsuAccNo = '"
                + mLCInsureAccSchema.getInsuAccNo() + "' ";
            String maxBalaCount = new ExeSQL().getOneValue(sql);
        	MMap tCekMap = null;
        	tCekMap = lockBalaCount(mLCInsureAccSchema.getPolNo()+maxBalaCount);
            if (tCekMap == null)
            {
                return false;
            }
            tMMap.add(tCekMap);
            
        	if(!SubmitData(tMMap)){
        		return false;
        	}
		}
        return true;
    }
    
//    计算利息
    private double getinterest(LMInsuAccRateSet tLMInsuAccRateSet){
    	String rate1=GetRate(tLMInsuAccRateSet.get(1).getStartBalaDate());
    	double interest=0;
    	if(rate1==null||rate1.equals("")||rate1.equals("null")){
    		return -1;
    	}
		String lastriskbonus = mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='"+mPolNo+"' and paydate<='"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"'");
		if(lastriskbonus==null||lastriskbonus.equals("")||lastriskbonus.equals("null")){
			return -1;
		}
//		当前失效,失效日期在结算之前
		String tSXDate=mExeSQL.getOneValue("select startdate from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='1' " +
				" and startdate<'"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and StateReason!='02' " +
						" and (enddate is null or enddate='' ) " +
						" union all " +
						" select makedate from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='1' " +
				" and makedate<'"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and StateReason='02' " +
						" and (enddate is null or enddate='' ) ");
		if(tSXDate!=null&&(!tSXDate.equals(""))&&(!tSXDate.equals("null"))){
			return 0;
		}
		
//		当前失效,失效日期在第一个结算区间
		tSXDate=mExeSQL.getOneValue("select makedate from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='1' " +
				" and makedate>='"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and makedate<'"+tLMInsuAccRateSet.get(1).getBalaDate()+"' and StateReason='02' " +
				" and (enddate is null or enddate='' ) " +
				" union all " +
				" select startdate from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='1' " +
				" and startdate>='"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and startdate<'"+tLMInsuAccRateSet.get(1).getBalaDate()+"' and StateReason!='02' " + 
				" and (enddate is null or enddate='' ) ");
		if(tSXDate!=null&&(!tSXDate.equals(""))&&(!tSXDate.equals("null"))){
			Calculator tCalculator=new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			return interest;
		}
    	if(tLMInsuAccRateSet.size()==1){
    		try {
        		LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
        		ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
        		interest= getAccInterset(rate1,lastriskbonus,ttLMInsuAccRateSet);
        		return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return  -1;
			}
//    		String tSXDate1=mExeSQL.getOneValue("select min(startdate) from lccontstate " +
//    				" where  polno='"+mPolNo+"' and statetype='Available' and state='0' " +
//						" and startdate between '"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and '"+tLMInsuAccRateSet.get(1).getBalaDate()+"' and enddate is not null and enddate!='' ");
//    		String tFXDate1=mExeSQL.getOneValue("select max(enddate) from lccontstate where state='0' and polno='"+mPolNo+"' and statetype='Available' and enddate between '"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and '"+tLMInsuAccRateSet.get(1).getBalaDate()+"' ");
//    		if((tSXDate1==null||tSXDate1.equals("")||tSXDate1.equals("null"))&&(tFXDate1==null||tFXDate1.equals("")||tFXDate1.equals("null"))){
//    			Calculator tCalculator=new Calculator();
//    			tCalculator.setCalCode(YXCalCode);
//    			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
//    			tCalculator.addBasicFactor("rate", rate1);
//    			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
//    			tCalculator.addBasicFactor("accountdate",  tLMInsuAccRateSet.get(1).getBalaDate());
//    			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
//    		}else if(tSXDate1.equals("")&&(!tFXDate1.equals(""))){
//    			Calculator tCalculator=new Calculator();
//    			tCalculator.setCalCode(FXCalCode);
//    			tCalculator.addBasicFactor("reinstatedate", tFXDate1);
//    			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
//    			tCalculator.addBasicFactor("rate", rate1);
//    			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
//    			tCalculator.addBasicFactor("accountdate",  tLMInsuAccRateSet.get(1).getBalaDate());
//    			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
//    		}else if(tFXDate1.equals("")&&(!tSXDate1.equals(""))){
//    			Calculator tCalculator=new Calculator();
//    			tCalculator.setCalCode(ZZCalCode);
//    			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
//    			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
//    			tCalculator.addBasicFactor("EndDate", tSXDate1);
//    			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
//    			tCalculator.addBasicFactor("enddate", tSXDate1);
//    			tCalculator.addBasicFactor("rate", rate1);
//    			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
//    			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
//    		}else if((!tFXDate1.equals(""))&&(!tSXDate1.equals(""))){
//    			if(PubFun.calInterval(tSXDate1,tFXDate1, "D")<0){
////    				复效日期小于失效日期，按正常来计算，重新设置结算区间
//    				Calculator tCalculator=new Calculator();
//        			tCalculator.setCalCode(YXCalCode);
//        			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
//        			tCalculator.addBasicFactor("rate", rate1);
//        			tCalculator.addBasicFactor("lastaccountdate",  tFXDate1);
//        			tCalculator.addBasicFactor("accountdate",  tSXDate1);
//        			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
//    			}else{
////    				复效日期大于失效日期，分两段来计算
//    				Calculator tCalculatorzz=new Calculator();
//    				tCalculatorzz.setCalCode(YXCalCode);
//    				tCalculatorzz.addBasicFactor("lastriskbonus", lastriskbonus);
//    				tCalculatorzz.addBasicFactor("rate", rate1);
//    				tCalculatorzz.addBasicFactor("lastaccountdate",tLMInsuAccRateSet.get(1).getStartBalaDate());
//    				tCalculatorzz.addBasicFactor("accountdate",tSXDate1);
////    				第二段复效的
//    				Calculator tCalculatorfx=new Calculator();
//    				tCalculatorfx.setCalCode(YXCalCode);
//    				tCalculatorfx.addBasicFactor("lastriskbonus", tCalculatorzz.calculate());
//    				tCalculatorfx.addBasicFactor("rate", rate1);
//    				tCalculatorfx.addBasicFactor("lastaccountdate",tFXDate1);
//    				tCalculatorfx.addBasicFactor("accountdate",tLMInsuAccRateSet.get(1).getBalaDate());
//    				interest=Double.parseDouble(tCalculatorfx.calculate()) - Double.parseDouble(lastriskbonus);
//    			}
//    		}
//        	return interest;
    	}else if(tLMInsuAccRateSet.size()==2){
    		String rate2=GetRate(PubFun.calDate(tLMInsuAccRateSet.get(2).getBalaDate(),-1,"D",null));
    		try {
        		LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
        		ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
        		double interest1= getAccInterset(rate1,lastriskbonus,ttLMInsuAccRateSet);
        		ttLMInsuAccRateSet = new LMInsuAccRateSet();
        		ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(2));
        		lastriskbonus = mExeSQL.getOneValue("select sum(money)+"+interest1+" from lcinsureacctrace where polno='"+mPolNo+"' and paydate<='"+tLMInsuAccRateSet.get(2).getStartBalaDate()+"'");
        		if(lastriskbonus==null||lastriskbonus.equals("")||lastriskbonus.equals("null")){
        			return -1;
        		}
        		double interest2= getAccInterset(rate2,lastriskbonus,ttLMInsuAccRateSet);
        		interest=interest1+interest2;
        		return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return  -1;
			}
    	}else{
    		return -1;
    	}
    }
    
    private double getAccInterset(String rate1,String lastriskbonus,LMInsuAccRateSet tLMInsuAccRateSet){
    	double interest=0;
    	
    	
    	
		String tSXDate1=mExeSQL.getOneValue("select min(startdate) from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='0' " +
					" and startdate between '"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and '"+tLMInsuAccRateSet.get(1).getBalaDate()+"' and StateReason!='02' " +
							" and enddate is not null and enddate!='' " +
							" union all " +
							" select min(makedate) from lccontstate " +
				" where  polno='"+mPolNo+"' and statetype='Available' and state='0' " +
					" and makedate between '"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and '"+tLMInsuAccRateSet.get(1).getBalaDate()+"' and StateReason='02' " +
							" and enddate is not null and enddate!='' ");
		
		
		String tFXDate1=mExeSQL.getOneValue("select max(enddate) from lccontstate where state='0' and polno='"+mPolNo+"' and statetype='Available' and enddate between '"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and '"+tLMInsuAccRateSet.get(1).getBalaDate()+"' ");
		if((tSXDate1==null||tSXDate1.equals("")||tSXDate1.equals("null"))&&(tFXDate1==null||tFXDate1.equals("")||tFXDate1.equals("null"))){
			Calculator tCalculator=new Calculator();
			tCalculator.setCalCode(YXCalCode);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate",  tLMInsuAccRateSet.get(1).getBalaDate());
			System.out.println(tCalculator.calculate());
			System.out.println(lastriskbonus);
			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		}else if(tSXDate1.equals("")&&(!tFXDate1.equals(""))){
			Calculator tCalculator=new Calculator();
			tCalculator.setCalCode(FXCalCode);
			tCalculator.addBasicFactor("reinstatedate", tFXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate",  tLMInsuAccRateSet.get(1).getBalaDate());
			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		}else if(tFXDate1.equals("")&&(!tSXDate1.equals(""))){
			Calculator tCalculator=new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate1);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate",  tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		}else if((!tFXDate1.equals(""))&&(!tSXDate1.equals(""))){
			if(PubFun.calInterval(tSXDate1,tFXDate1, "D")<0){
//				复效日期小于失效日期，按正常来计算，重新设置结算区间
				Calculator tCalculator=new Calculator();
    			tCalculator.setCalCode(YXCalCode);
    			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
    			tCalculator.addBasicFactor("rate", rate1);
    			tCalculator.addBasicFactor("lastaccountdate",  tFXDate1);
    			tCalculator.addBasicFactor("accountdate",  tSXDate1);
    			interest=Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			}else{
//				复效日期大于失效日期，分两段来计算
				Calculator tCalculatorzz=new Calculator();
				tCalculatorzz.setCalCode(YXCalCode);
				tCalculatorzz.addBasicFactor("lastriskbonus", lastriskbonus);
				tCalculatorzz.addBasicFactor("rate", rate1);
				tCalculatorzz.addBasicFactor("lastaccountdate",tLMInsuAccRateSet.get(1).getStartBalaDate());
				tCalculatorzz.addBasicFactor("accountdate",tSXDate1);
//				第二段复效的
				Calculator tCalculatorfx=new Calculator();
				tCalculatorfx.setCalCode(YXCalCode);
				tCalculatorfx.addBasicFactor("lastriskbonus", tCalculatorzz.calculate());
				tCalculatorfx.addBasicFactor("rate", rate1);
				tCalculatorfx.addBasicFactor("lastaccountdate",tFXDate1);
				tCalculatorfx.addBasicFactor("accountdate",tLMInsuAccRateSet.get(1).getBalaDate());
				interest=Double.parseDouble(tCalculatorfx.calculate()) - Double.parseDouble(lastriskbonus);
			}
		}
//		当前保单做复效操作后有效，但结算区间在复效之前
		String tFXDate = mExeSQL.getOneValue("select enddate from lccontstate " +
				" where polno='"+mPolNo+"' and statetype='Available' and state='0' " +
				" and  startdate<'"+tLMInsuAccRateSet.get(1).getStartBalaDate()+"' and enddate>'"+tLMInsuAccRateSet.get(1).getBalaDate()+"' " +
				" and enddate is not null with ur  ");
		if(tFXDate!=null&&(!tFXDate.equals(""))&&(!tFXDate.equals("null"))){
			return 0;
		}
    	return interest;
	
    }
    
//    拆分结算月度经过保单周年日的情况
    private LMInsuAccRateSet getRateSet(LMInsuAccRateSchema tLMInsuAccRateSchema){
    	LMInsuAccRateSet tLMInsuAccRateSet =new  LMInsuAccRateSet();
    	int i=1;
    	boolean haveDate=false;
    	do {
//    		结算开始日期减去该次保单周年日的日期
    		int bigthanstart=PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null),tLMInsuAccRateSchema.getStartBalaDate(), "D");
//    		结算结束日期减去该次保单周年日的日期
    		int bigthanend=PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null),tLMInsuAccRateSchema.getBalaDate(), "D");
    		if(bigthanstart>0){
    			i++;
    		}else if(bigthanstart==0){
    			break;
    		}else if(bigthanstart<0&&bigthanend>0){
    			haveDate=true;
    			break;
    		}else if(bigthanend<=0){
    			break;
    		}
		} while (PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null),tLMInsuAccRateSchema.getBalaDate(), "D")>0);
    	if(haveDate){
    		String midDate=PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null);
    		LMInsuAccRateSchema tLM1=new LMInsuAccRateSchema();
    		tLM1.setStartBalaDate(tLMInsuAccRateSchema.getStartBalaDate());
    		tLM1.setBalaDate(midDate);
    		LMInsuAccRateSchema tLM2=new LMInsuAccRateSchema();
    		tLM2.setStartBalaDate(midDate);
    		tLM2.setBalaDate(tLMInsuAccRateSchema.getBalaDate());
    		tLMInsuAccRateSet.add(tLM1);
    		tLMInsuAccRateSet.add(tLM2);
    	}else{
    		tLMInsuAccRateSet =new  LMInsuAccRateSet();
    		tLMInsuAccRateSet.add(tLMInsuAccRateSchema);
    	}
    	return tLMInsuAccRateSet;
    }
    
//    每次结算时初始化查询保单账户信息
    private boolean initAcc(){
    	mPolYear=0;
    	mPolMonth=0;
    	LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
    	tLCInsureAccDB.setPolNo(mPolNo);
    	if(tLCInsureAccDB.query().size()==1){
    		mLCInsureAccSchema=tLCInsureAccDB.query().get(1);
    	}else{
    		return false;
    	}
    	LCInsureAccClassDB tLCInsureAccClassDB=new LCInsureAccClassDB();
    	tLCInsureAccClassDB.setPolNo(mPolNo);
    	if(tLCInsureAccClassDB.query().size()==1){
    		mLCInsureAccClassSchema=tLCInsureAccClassDB.query().get(1);
    	}else{
    		return false;
    	}
    	return true;
    }
 
    /**
     * 处理红利分配
     * @return boolean
     */
    private boolean GetSumBalaDate()
    {
//    	仅仅用LMInsuAccRate来储存每一段结算区间
		LMInsuAccRateSchema tLMInsuAccRateSchema =new LMInsuAccRateSchema();
		int i=0;
    	do{
    		if(mLMInsuAccRateSet.size()>0){
	    		tLMInsuAccRateSchema =new LMInsuAccRateSchema();
	    		tLMInsuAccRateSchema.setStartBalaDate(mLMInsuAccRateSet.get(i).getBalaDate());
	    		String baladate=PubFun.calDate(mLMInsuAccRateSet.get(i).getBalaDate(), 1, "M", null);
	    		String trate=GetRate(PubFun.calDate(baladate, -1, "D", null));
	    			if(trate==null||trate.equals("")||trate.equals("null")){
		    			break;
		    		}else{
		    			tLMInsuAccRateSchema.setBalaDate(baladate);
		    			mLMInsuAccRateSet.add(tLMInsuAccRateSchema);
		    		}
    		}else{
    			tLMInsuAccRateSchema =new LMInsuAccRateSchema();
        		tLMInsuAccRateSchema.setStartBalaDate(mLCInsureAccSchema.getBalaDate());
        		String baladate=mExeSQL.getOneValue("select date('"+mLCInsureAccSchema.getBalaDate()+"') - (day('"+mLCInsureAccSchema.getBalaDate()+"')-1) day + 1 month from dual");
        		if(PubFun.calInterval(baladate,CurrentDate, "D")<0){
        			break;
        		}
        		String trate=GetRate(PubFun.calDate(baladate, -1, "D", null));
        			if(trate==null||trate.equals("")||trate.equals("null")){
    	    			break;
    	    		}else{
    	    			tLMInsuAccRateSchema.setBalaDate(baladate);
    	    			mLMInsuAccRateSet.add(tLMInsuAccRateSchema);
    	    		}
    		}
    		i++;
    		}while(PubFun.calInterval(PubFun.calDate(tLMInsuAccRateSchema.getBalaDate(), 1, "M", null),CurrentDate, "D")>=0);
    	return true;
    }
    
//    返回该日期对应的利率
    public String GetRate(String tdate){
    	String tPolYear=String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),tdate, "Y"));
    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mPolNo+"' " +
    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
    	return trate;
    }
    
    /**
     * 得到结算序号
     * @return String
     */
    public String getBalanceSequenceNo()
    {
        String[] dateArr = PubFun.getCurrentDate().split("-");
        String date = dateArr[0] + dateArr[1] + dateArr[2];
        mBalanceSeq = date
                      + PubFun1.CreateMaxNo("BalaSeq" + date, 10);

        return mBalanceSeq;
    }
    
    /**
     * calPolYearMonth
     */
    private boolean calPolYearMonth(String tStartDate)
    {
        String sql = "select year(date('" + tStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + tStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));
        
        return true;
    }
    
    private LCInsureAccBalanceSchema getLCInsureAccBalance(LMInsuAccRateSchema
            cLMInsuAccRateSchema,
            double cAccValue)
    {
        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        ref.transFields(schema, mLCInsureAccSchema);

        schema.setSequenceNo(getBalanceSequenceNo());
        schema.setInsuAccBalaBefore(mLCInsureAccSchema.getInsuAccBala());
        schema.setInsuAccBalaAfter(cAccValue);
        schema.setInsuAccBalaGurat(cAccValue);
        schema.setDueBalaDate(cLMInsuAccRateSchema.getBalaDate());
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(PubFun.getCurrentDate());
        schema.setRunTime(PubFun.getCurrentTime());
        schema.setPolYear(mPolYear);
        schema.setPolMonth(mPolMonth);
        schema.setPrintCount(0);
        schema.setOperator(mGI.Operator);
        PubFun.fillDefaultField(schema);

        String sql = "select max(BalaCount + 1) from LCInsureAccBalance "
                     + "where PolNo = '"
                     + mLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLCInsureAccSchema.getInsuAccNo() + "' ";
        String maxBalaCount = new ExeSQL().getOneValue(sql);
        if (maxBalaCount == null || maxBalaCount.equals("") ||
            maxBalaCount.equals("null"))
        {
            maxBalaCount = "1";
        }
        schema.setBalaCount(maxBalaCount);

        return schema;
    }
    
//	设置累积生息的帐户数据
	private MMap getAcc(LMInsuAccRateSchema tLMInsuAccRateSchema,double interest){
		MMap tMMap=new MMap();    
		double tmoney=CommonBL.carry(mLCInsureAccSchema.getInsuAccBala()+interest);
//        设置账户余额
		mLCInsureAccSchema.setBalaDate(tLMInsuAccRateSchema.getBalaDate());
        mLCInsureAccSchema.setInsuAccBala(tmoney);
        mLCInsureAccSchema.setLastAccBala(mLCInsureAccSchema.getInsuAccBala());
        mLCInsureAccSchema.setOperator(mGI.Operator);
        mLCInsureAccSchema.setModifyDate(CurrentDate);
        mLCInsureAccSchema.setModifyTime(CurrentTime);
        tMMap.put(mLCInsureAccSchema, SysConst.UPDATE);
        
        mLCInsureAccClassSchema.setBalaDate(tLMInsuAccRateSchema.getBalaDate());
        mLCInsureAccClassSchema.setInsuAccBala(tmoney);
        mLCInsureAccClassSchema.setLastAccBala(mLCInsureAccClassSchema.getInsuAccBala());
        mLCInsureAccClassSchema.setOperator(mGI.Operator);
        mLCInsureAccClassSchema.setModifyDate(CurrentDate);
        mLCInsureAccClassSchema.setModifyTime(CurrentTime);
        tMMap.put(mLCInsureAccClassSchema, SysConst.UPDATE);
		
        tMMap.put(getLCInsureAccBalance(tLMInsuAccRateSchema, tmoney), SysConst.INSERT);
        tMMap.put(getLCInsureAccTrace(tLMInsuAccRateSchema,interest),SysConst.INSERT);
		return tMMap;
	}
	
    /**
     * 设置帐户利息轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     */
    private LCInsureAccTraceSchema getLCInsureAccTrace(LMInsuAccRateSchema tLMInsuAccRateSchema,double interest)
    {
        String serialNo = PubFun1.CreateMaxNo("SERIALNO",
                                       mLCInsureAccClassSchema.getManageCom());

        LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
        tLCInsureAccTraceSchema.setGrpContNo(mLCInsureAccClassSchema.
                                             getGrpContNo());
        tLCInsureAccTraceSchema.setGrpPolNo(mLCInsureAccClassSchema.getGrpPolNo());
        tLCInsureAccTraceSchema.setContNo(mLCInsureAccClassSchema.getContNo());
        tLCInsureAccTraceSchema.setPolNo(mLCInsureAccClassSchema.getPolNo());
        tLCInsureAccTraceSchema.setSerialNo(serialNo);
        tLCInsureAccTraceSchema.setInsuAccNo(mLCInsureAccClassSchema.
                                             getInsuAccNo());
        tLCInsureAccTraceSchema.setRiskCode(mLCInsureAccClassSchema.getRiskCode());
        tLCInsureAccTraceSchema.setPayPlanCode(mLCInsureAccClassSchema.
                                               getPayPlanCode());
        tLCInsureAccTraceSchema.setManageCom(mLCInsureAccClassSchema.
                                             getManageCom());
        tLCInsureAccTraceSchema.setAccAscription("0");
        tLCInsureAccTraceSchema.setMoneyType("LX");
        tLCInsureAccTraceSchema.setMoney(CommonBL.carry(interest));
        tLCInsureAccTraceSchema.setUnitCount("0");
        tLCInsureAccTraceSchema.setPayDate(tLMInsuAccRateSchema.getBalaDate());
        tLCInsureAccTraceSchema.setState("0");
        tLCInsureAccTraceSchema.setOtherNo(mBalanceSeq);
        tLCInsureAccTraceSchema.setOtherType("6");
        tLCInsureAccTraceSchema.setOperator(mGI.Operator);
        tLCInsureAccTraceSchema.setMakeDate(CurrentDate);
        tLCInsureAccTraceSchema.setMakeTime(CurrentTime);
        tLCInsureAccTraceSchema.setModifyDate(CurrentDate);
        tLCInsureAccTraceSchema.setModifyTime(CurrentTime);
        return tLCInsureAccTraceSchema;
    }
	
    /**
     * 锁定动作
     * @param polBalaCount
     * @return MMap
     */
    private MMap lockBalaCount(String polBonus)
    {
        MMap tMMap = null;
        /**万能险月结算"UJ"*/
        String tLockNoType = "BA";
        /**锁定有效时间（秒）*/
        String tAIS = "36000";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", polBonus);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
	
    /**
     * 处理帐户结算表初步生成
     * !!!!!!这里有分步提交数据库!!!!!!
     * @return boolean
     */
    private boolean HaveBalance()
    {
    	String Haveacc = mExeSQL.getOneValue("select 1 from lcinsureaccbalance where polno='"+mPolNo+"' fetch first 1 row only with ur");
    	if(Haveacc!=null&&Haveacc.equals("1")){
    		System.out.println("已经生成帐户了!");
    		return true;
    	}
    	String tbaladate=mExeSQL.getOneValue("select min(paydate) from lcinsureacctrace where polno='"+mPolNo+"' and money<>0 with ur");
    	if(tbaladate==null||tbaladate.equals("")||tbaladate.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取最早结算日期出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	String tmoney=mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='"+mPolNo+"' and paydate<='"+tbaladate+"' with ur");
    	if(tmoney==null||tmoney.equals("")||tmoney.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取帐户金额出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	
    	VData tVData = new VData();
    	MMap tMMap =new MMap();
    	
    	tMMap.put("update lcinsureacc set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);
    	tMMap.put("update lcinsureaccclass set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);

        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        ref.transFields(schema, mLCInsureAccSchema);
        schema.setSequenceNo(mLCInsureAccSchema.getPolNo()+ mLCInsureAccSchema.getInsuAccNo());
        schema.setBalaCount(0);
        schema.setDueBalaDate(tbaladate);
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(schema.getMakeDate());
        schema.setRunTime(schema.getMakeTime());
        schema.setPolYear(1);
        schema.setPolMonth(1);
        schema.setInsuAccBalaBefore(tmoney);
        schema.setInsuAccBalaAfter(tmoney);
        schema.setInsuAccBalaGurat(tmoney);
        schema.setPrintCount(0);
        tMMap.put(schema,SysConst.INSERT);

        tVData.add(tMMap);
        //计算并填充账户结构
        if (tVData == null)
        {
        	// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "创建账户结构失败!";
			mErrors.addOneError(tError);
			return false;
        }
//      执行业务数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(tVData, ""))
        {
        	// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "帐户结算创建数据提交失败!";
			mErrors.addOneError(tError);
			return false;
        }
//		提交完成后置全局变量结算日期
        mLCInsureAccSchema.setBalaDate(tbaladate);
    	return true;    	
    }

    public static void main(String[] args) {
//		System.out.println(PubFun.calInterval(PubFun.calDate("2010-01-15", 1, "Y", null),"2011-01-15", "D"));
//		System.out.println(PubFun.calInterval(PubFun.calDate("2010-01-15", 1, "Y", null),"2011-02-01", "D"));
	System.out.println(PubFun.calInterval("2010-01-15","2011-01-15", "Y"));
    }
}

