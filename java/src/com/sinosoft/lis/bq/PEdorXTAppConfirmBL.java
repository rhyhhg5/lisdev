package com.sinosoft.lis.bq;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 协议退保业务处理类
 * 为了支持作帐分离，特调用解约程序算出正常解约时的费用，
 * 并与协议解约实际退费作差，生成财务数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PEdorXTAppConfirmBL implements EdorAppConfirm
{
    /**错误的容器*/
    CErrors mErrors = new CErrors();
    private VData mResult = null;  //存储处理后的数据
    private MMap map = new MMap();
    private LPEdorItemSchema mLPEdorItemSchema = null;
    private GlobalInput mGlobalInput = null;
    private LPPolSet mLPPolSet = null;

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象mErrors中
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData(operate))
        {
            return false;
        }

        prepareOuputData();

        return true;
    }

    /**
     * 接收传入到本类的数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData
                                .getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput = (GlobalInput) cInputData
                           .getObjectByObjectName("GlobalInput", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }

        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }

        return true;
    }
    private boolean checkData()
    {
        if(!mLPEdorItemSchema.getEdorState().equals(BQ.EDORSTATE_INPUT))
        {
            mErrors.addOneError("当前状态不能理算");
           return false;
        }
        return true;
    }

    /**
     * 处理业务逻辑，生成所需的数据记录
     * @return boolean
     */
    private boolean dealData(String operate)
    {
        mLPPolSet = getLPPol();
        if(mLPPolSet ==null)
        {
            return false;
        }

        if(!setXTFee())
        {
            return false;
        }

        return true;
    }
    private LPPolSet getLPPol()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setContNo(mLPEdorItemSchema.getContNo());
        tLPPolDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
        tLPPolDB.setEdorType(mLPEdorItemSchema.getEdorType());
        LPPolSet tLLPolSet = tLPPolDB.query();
        if(tLLPolSet==null||tLLPolSet.size()<1)
        {
            mErrors.addOneError("查询保全险种错误");
            return null;
        }

        return tLLPolSet;
    }
    /**
     * 生成协议退保的退费记录
     * @return boolean
     */
    private boolean setXTFee()
    {
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPEdorItemSchema.getEdorNo(),
                                      mLPEdorItemSchema.getEdorType());
        tEdorItemSpecialData.query();

        double xtFee = Double.MIN_VALUE; //协议退费金
        for(int i = 1; i <= mLPPolSet.size(); i++)
        {
            tEdorItemSpecialData.setPolNo(mLPPolSet.get(i).getPolNo());
            try
            {
                xtFee = Double.parseDouble(tEdorItemSpecialData.getEdorValue(BQ.
                    DETAILTYPE_XTFEE));
            }
            catch(Exception e)
            {
                e.printStackTrace();
                mErrors.addOneError("没有查询到退费记录");
                return false;
            }

            BqCalBL tBqCalBL = new BqCalBL();

            String feeFinaType = PEdorXTAppConfirmBL.getFinType(mLPPolSet.get(i).getRiskCode());
            if (feeFinaType.equals("")) {
                mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码!");
                return false;
            }

            LJSGetEndorseSchema tLJSGetEndorseSchema =
                tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema, mLPPolSet.get(i),
                                           null, feeFinaType, xtFee, this.mGlobalInput);
          /**add by hhw 20170519处理税优保单补交税收优惠额度*/
          String str="select 1 from lmriskapp where riskcode='" +mLPPolSet.get(i).getRiskCode()+ "' and taxoptimal='Y'";
          String sy=new ExeSQL().getOneValue(str);
          if(null != sy && !"".equals(sy)){
        	String taxmoney = tEdorItemSpecialData.getEdorValue("TAXMoney");
        	double  taxMoney = 0;
			if(taxmoney!=null&&!"".equals(taxmoney)){
				taxMoney = Double.parseDouble(taxmoney);
			}
			
			LJSGetEndorseSchema tTaxLJSGetEndorseSchema =
				tBqCalBL.initLJSGetEndorse(mLPEdorItemSchema, mLPPolSet.get(i),
						null, "BS", taxMoney, this.mGlobalInput);
			map.put(tTaxLJSGetEndorseSchema, SysConst.DELETE_AND_INSERT);
			xtFee+=taxMoney;
			if(xtFee>0){
				mErrors.addOneError("补交税收优惠额度大于协议退费金额，请重新录入!");
				return false;
			}
          }
            map.put(tLJSGetEndorseSchema, SysConst.DELETE_AND_INSERT);
            mLPEdorItemSchema.setGetMoney(mLPEdorItemSchema.getGetMoney()
                                          + xtFee);
        }

        map.put(mLPEdorItemSchema, "UPDATE");

        return true;
    }

    /**
     * 准备需要向外输出的数据，
     */
    private void prepareOuputData()
    {
        mResult = new VData();
        mResult.add(map);
    }

    public VData getResult()
    {
        return mResult;
    }

    public PEdorXTAppConfirmBL()
    {
    }


    /**
     * 本函数中polNo不能为"000000"
     * 1、	一年期及以内险种 对应 冲退保费 TB。
     * 2、	一年期以上险种 对应 冲退保金 TF。
     *      * @param polNo String
     * @return String
     */
     static String getFinType(String riskCode) {

            //通过险种代码查询险种期限
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(riskCode);
            if (!tLMRiskAppDB.getInfo()) {
                return "";
            }
            System.out.println("说长险短险:::"+tLMRiskAppDB.getRiskPeriod());
            //一年期及以内险种冲退保费 TF。
            if (tLMRiskAppDB.getRiskPeriod().equals("M")
                || tLMRiskAppDB.getRiskPeriod().equals("S")) {
                return BQ.FEEFINATYPE_TF;
            }
            //一年期以上险种冲退保金 TB
            else if (tLMRiskAppDB.getRiskPeriod().equals("L")) {
                return BQ.FEEFINATYPE_TB;
            }
        return "";
    }


    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LPEdorItemDB db = new LPEdorItemDB();
        db.setEdorAcceptNo("20061226000008");
        LPEdorItemSchema schema = db.query().get(1);

        VData v = new VData();
        v.add(g);
        v.add(schema);

        PEdorXTAppConfirmBL bl = new PEdorXTAppConfirmBL();
        if(!bl.submitData(v, "INSERT||GEDORAPPCONFIRM"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
