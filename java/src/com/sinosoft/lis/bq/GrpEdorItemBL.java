package com.sinosoft.lis.bq;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class GrpEdorItemBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 往前面传输数据的容器 */
  private VData mResult = new VData();
  private MMap map = new MMap();
  TransferData tTransferData = new TransferData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /** 业务处理相关变量 */
  private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
  private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
  private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
  private GlobalInput mGlobalInput = new GlobalInput();
  private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

  // @Constructor
  public GrpEdorItemBL() {}

  /**
   * 数据提交的公共方法
   * @param: cInputData 传入的数据
   *		  cOperate 数据操作字符串
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false) {
      return false;
    }

    if (this.checkData() == false) {
      return false;
    }

    // 根据业务逻辑对数据进行处理
    // 根据业务逻辑对数据进行处理
    if (mOperate.equals("INSERT||GRPEDORITEM")) {
      if (this.dealData() == false) {
        return false;
      }
    }
    else if (mOperate.equals("DELETE||GRPEDORITEM")) {
      if (this.deleteData() == false) {
        return false;
      }
    }
    else if (mOperate.equals("UPDATE||GRPEDORITEM")) {
      if (!updateData()) {
        return false;
      }
    }
    // 装配处理好的数据，准备给后台进行保存
    this.prepareOutputData();
    System.out.println("---prepareOutputData---");
    //　数据提交、保存
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start GrpEdorItemBL Submit...");

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);

      CError tError = new CError();
      tError.moduleName = "GrpEdorItemBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";

      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---commitData---");

    return true;
  }
  
  /**
   * 获取提交数据的公共方法
   * @param: cInputData 传入的数据
   *		 cOperate 数据操作字符串
   * @return:MMap
   */
  public MMap getsubmitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false) {
      return null;
    }

    if (this.checkData() == false) {
      return null;
    }

    // 根据业务逻辑对数据进行处理
    // 根据业务逻辑对数据进行处理
    if (mOperate.equals("INSERT||GRPEDORITEM")) {
      if (this.dealData() == false) {
        return null;
      }
    }
    else if (mOperate.equals("DELETE||GRPEDORITEM")) {
      if (this.deleteData() == false) {
        return null;
      }
    }
    else if (mOperate.equals("UPDATE||GRPEDORITEM")) {
      if (!updateData()) {
        return null;
      }
    }
    return map;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {

    //全局变量
    mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0));
    //团体批改主表
    mLPGrpEdorMainSchema.setSchema( (LPGrpEdorMainSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorMainSchema",
        0));

    //批改项目表
    mLPGrpEdorItemSet.set( (LPGrpEdorItemSet) mInputData.
                          getObjectByObjectName("LPGrpEdorItemSet", 0));
//        tTransferData = (TransferData) mInputData.
//                        getObjectByObjectName("TransferData", 0);

    if (mLPGrpEdorMainSchema == null || mLPGrpEdorItemSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpEdorItemBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在接受数据时没有得到团体批改主表或者批改项目表!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLPGrpEdorItemSchema = mLPGrpEdorItemSet.get(1);
    return true;
  }

  /**
   * 判断是否是续期催收过程之中
   * @return boolean true 正在催收， false 已核销
   */
  private boolean checkDueFee(String grpContNo) {
    String sql = "select * from LJSPay " +
        "where OtherNoType = '1' " + //1是团单续期
        "and OtherNo = '" + grpContNo + "' ";
    LJSPayDB tLJSPayDB = new LJSPayDB();
    LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
    if (tLJSPaySet.size() > 0) {
      mErrors.addOneError("该保单处于续期待收费状态，续期核销之后才能处理保全业务！");
      return false;
    }
    return true;
  }

  /**
   * 判断同保单是否有未结案的相同项目
   * @return boolean
   */
  private boolean checkOtherEdor() {
    String sql = "select b.* from LPEdorApp a, LPGrpEdorItem b " +
        "where  a.EdorAcceptNo = b.EdorAcceptNo " +
//        "and a.EdorState in ('1', '3', '2', '9') " +
        "and a.EdorState != '0' " +
        "and b.EdorType = '" + mLPGrpEdorItemSchema.getEdorType() +
        "' " +
        "and b.GrpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() +
        "' ";
    LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
    LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(sql);
    if (tLPGrpEdorItemSet.size() > 0) {
      mErrors.addOneError("保单" + mLPGrpEdorItemSchema.getGrpContNo() +
                          "下有未结案的" + tLPGrpEdorItemSet.get(1).getEdorType() +
                          "项目受理，受理号为" + tLPGrpEdorItemSet.get(1).getEdorNo() +
                          "，该工单结案后才能继续受理。");
      return false;
    }
    return true;
  }

  /**
   * 新契约保单打印之后才能做保全
   * @return boolean
   */
  private boolean checkPrintCount() {
	  if(!mLPGrpEdorItemSchema.getEdorType().equals("RB"))
      {
          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
          tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
          if (!tLCGrpContDB.getInfo()) {
              mErrors.addOneError("未找到保单信息！");
              return false;
          }
          if (tLCGrpContDB.getPrintCount() == 0 &&
              !mLPGrpEdorItemSchema.getEdorType().equals("WT")) {
              mErrors.addOneError("此保单未完成保单打印，保单打印之后才能处理保全业务！");
              return false;
          }
//              return true;
      }else
      {    //modify by fuxin 保全回退需要查询C表和B表。这个校验其实没用。
          String sql = " select 1 From lcgrpcont where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"'"
                     + " union "
                     + " select 1 from lbgrpcont where grpcontno ='"+mLPGrpEdorItemSchema.getGrpContNo()+"'"
                     ;
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS.getMaxRow()==0)
         {
             mErrors.addOneError("未查到保单信息！");
             return false ;
         }
      }
      return true ;
  }

  private boolean checEdorItem() {
        if (!checkPrintCount())
       {
            return false;
        }
        if (!checkDueFee(mLPGrpEdorItemSchema.getGrpContNo()))
        {
            return false;
        }
        if (!checkOtherEdor())
        {
            return false;
        }
        // 业务在保单失效后的30天内可以做增减人业务.30天则不可以添加.
        // 中间可能出现定期结算的状保不失效,只有失效的保团才走这个地方.
        //qulq modify 与和或的优先级。注意
        if((mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_ZT)
           ||mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_NI)
           ||mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_WZ)
           ||mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_WJ))
            &&checkContState(mLPGrpEdorItemSchema.getGrpContNo()).equals("3")   )
        {
            if (!checkDate(mLPGrpEdorItemSchema.getGrpContNo()))
            {
                return false;
            }
        }
    for (int i = 1; i <= mLPGrpEdorItemSet.size(); i++) {
      LPGrpEdorItemSchema tLPGrpEdorItemSchema = mLPGrpEdorItemSet.get(i);
      String grpContNo = tLPGrpEdorItemSchema.getGrpContNo();
      String edorType = tLPGrpEdorItemSchema.getEdorType();

      CheckEdorItem tCheckEdorItem = new CheckEdorItem();
      tCheckEdorItem.setContType(BQ.CONTTYPE_G);
      tCheckEdorItem.setManageCom(mGlobalInput.ManageCom);
      tCheckEdorItem.setGrpContNo(grpContNo);
      tCheckEdorItem.setEdorType(edorType);
      tCheckEdorItem.setLPGrpEdorItem(tLPGrpEdorItemSchema);
      if (!tCheckEdorItem.submitData()) {
        mErrors.copyAllErrors(tCheckEdorItem.mErrors);
        return false;
      }
    }
    return true;
  }

  /**
   * 校验传入的数据
   * @param: 无
   * @return: boolean
   */
  private boolean checkData() {
    if (!checEdorItem()) {
      return false;
    }
    
    if (mOperate.equals("INSERT||GRPEDORITEM")) {
      for (int i = 0; i < mLPGrpEdorItemSet.size(); i++) {
        VData inputCheckData = new VData();
        inputCheckData.add(mGlobalInput);
        inputCheckData.add(mLPGrpEdorItemSet.get(i + 1));
        inputCheckData.add("VERIFY||BEGIN");
        inputCheckData.add("GEDORINPUT#EDORTYPE");

        CheckFieldBL tCheckFieldBL = new CheckFieldBL();
        if (!tCheckFieldBL.submitData(inputCheckData, "")) {
          this.mErrors.copyAllErrors(tCheckFieldBL.mErrors);
          return false;
        }
      }
      
//      //校验该保单保全生效日的上个月还未月结，请先进行月结再申请该保全
//      for(int i=0;i<mLPGrpEdorItemSet.size();i++){
//    	  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
//          tLPGrpEdorItemSchema.setSchema(mLPGrpEdorItemSet.get(i + 1));
//          if(tLPGrpEdorItemSchema.getEdorType().equals("TQ")||tLPGrpEdorItemSchema.getEdorType().equals("TA") || tLPGrpEdorItemSchema.getEdorType().equals("HL")){
//            String tValiDate = PubFun.getCurrentDate();
//            tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
//            /*tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
//            System.out.println("保全生效日期mEdorValidate"+tValiDate);
//            
//            String mEdorValidate = tValiDate.substring(0, 7) + "-01";
//
//            System.out.println("校验日期："+mEdorValidate);
//            
//            String sql = "select count(*) from lcinsureacctrace a  where grpcontno='"+tLPGrpEdorItemSchema.getGrpContNo()+"' and exists (select 1 from lcinsureacc where grpcontno=a.grpcontno and polno=a.polno and acctype='001' and insuaccno=a.insuaccno ) and paydate =date('"+mEdorValidate+"') and insuaccno='671803' and MoneyType='MF' and othertype='6' ";
//            ExeSQL tExeSQL = new ExeSQL();
//            String resultString = tExeSQL.getOneValue(sql);
//            if(resultString.equals("0")){
//            	CError.buildErr(this, "该保单保全生效日的上个月还未月结，请先进行月结再申请该保全！");
//            	return false;
//            }*/
//            boolean flag = PubFun.isDoBQ(tLPGrpEdorItemSchema.getGrpContNo(), tValiDate);
//            if (!flag) {
//            	CError.buildErr(this, "该保单保全生效日的上个月还未月结，请先进行月结再申请该保全！");
//            	return false;
//			}
//          }
//      }
      
      //校验在保单失效及满期终止状态下保全项目能否添加   add by luomin
      for (int i = 0; i < mLPGrpEdorItemSet.size(); i++) {
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setSchema(mLPGrpEdorItemSet.get(i + 1));
        if (!tDisabledManageBL.dealDisabledcont(tLPGrpEdorItemSchema.getGrpContNo(),
                                                tLPGrpEdorItemSchema.getEdorType(),
                                                2)) {

          CError tError = new CError();
          tError.moduleName = "PEdorItemBL";
          tError.functionName = "checkData";
          tError.errorMessage = "保单" + tLPGrpEdorItemSchema.getGrpContNo() +
              "下" + tLPGrpEdorItemSchema.getEdorType() +
              "项目在"+CommonBL.getCodeName("stateflag",
                tDisabledManageBL.getState())+"状态下不能添加!";
          this.mErrors.addOneError(tError);
          return false;

        }

      }
      //校验在其它保单已存在保全项目状态下该保全项目能否添加 080515 zhanggm 
      if(!checkAddItem())
      {
    	  CError tError = new CError();
	      tError.moduleName = "GrpEdorItemBL";
	      tError.functionName = "checkData";
	      tError.errorMessage = "checkAddItem()校验失败！";
	      this.mErrors.addOneError(tError);
	      return false;
      }

      LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
      tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorMainSchema.getGrpContNo());
      tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorMainSchema.getEdorNo());
      tLPGrpEdorItemDB.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
      tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSet.get(1).getEdorType());
      int tCount = tLPGrpEdorItemDB.getCount();
      if (tLPGrpEdorItemDB.mErrors.needDealError()) {
        CError tError = new CError();
        tError.moduleName = "GrpEdorItemBL";
        tError.functionName = "checkData";
        tError.errorMessage = "查询保全项目失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      if (tCount > 0) {
        CError tError = new CError();
        tError.moduleName = "GrpEdorItemBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该批单下已经添加过该项目!";
        this.mErrors.addOneError(tError);
        return false;
      }

    }

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean dealData() {
    LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
    tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
    tLPGrpEdorMainDB.setGrpContNo(mLPGrpEdorMainSchema.getGrpContNo());
    LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
    if (tLPGrpEdorMainSet.size() == 0) {
      mLPGrpEdorMainSchema.setEdorAppNo(mLPGrpEdorMainSchema.
                                        getEdorAcceptNo());
      mLPGrpEdorMainSchema.setEdorNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
      //mLPGrpEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());
      //mLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setEdorState("1");
      mLPGrpEdorMainSchema.setUWState("0");
      mLPGrpEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
      mLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
      mLPGrpEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
      mLPGrpEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLPGrpEdorMainSchema, "DELETE&INSERT");
    }

    //产生集体投保单号码
    if (mOperate.equals("INSERT||GRPEDORITEM")) {
      for (int i = 1; i <= mLPGrpEdorItemSet.size(); i++) {
        String tEdorNo = mLPGrpEdorItemSet.get(i).getEdorNo();
        if (tEdorNo == null || tEdorNo.equals("")) {
          mLPGrpEdorItemSet.get(i).setEdorNo(mLPGrpEdorMainSchema.getEdorAppNo());
        }

        mLPGrpEdorItemSet.get(i).setEdorState("3"); ////未录入
        //mLPGrpEdorItemSet.get(i).setEdorAppDate(PubFun.getCurrentDate());
        //mLPGrpEdorItemSet.get(i).setEdorValiDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
        mLPGrpEdorItemSet.get(i).setOperator(mGlobalInput.Operator);
        mLPGrpEdorItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
      }
      map.put(mLPGrpEdorItemSet, "INSERT");
    }

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean updateData() {
    //产生集体投保单号码
    if (mOperate.equals("UPDATE||GRPEDORITEM")) {
      String tEdorAcceptNo = mLPGrpEdorItemSet.get(1).getEdorAcceptNo();
      String tEdorNo = mLPGrpEdorItemSet.get(1).getEdorNo();
      String tEdorType = mLPGrpEdorItemSet.get(1).getEdorType();
      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      tLPEdorItemDB.setEdorAcceptNo(tEdorAcceptNo);
      tLPEdorItemDB.setEdorNo(tEdorNo);
      tLPEdorItemDB.setEdorType(tEdorType);
      tLPEdorItemDB.setEdorState("3");
      LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
      if (tLPEdorItemSet == null || tLPEdorItemSet.size() <= 0) {
        map.put(
            "update LPGrpEdorItem set edorstate='1' where edoracceptno='" +
            tEdorAcceptNo + "' and edorno='" + tEdorNo +
            "' and edortype='" + tEdorType + "'", "UPDATE");
      }
      else {
        CError tError = new CError();
        tError.moduleName = "GrpEdorItemBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该保全项目下存在装态为3（未录入）的个单!";
        this.mErrors.addOneError(tError);
        return false;
      }
    }

    return true;
  }

  /**
   * 删除传入的险种
   * @param: 无
   * @return: void
   */
  private boolean deleteData() {

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: void
   */
  private void prepareOutputData() {
    mResult.clear();
    mResult.add(mLPGrpEdorItemSet);
    mInputData.clear();
    mInputData.add(map);
  }

  /**
   * 得到处理后的结果集
   * @return 结果集
   */

  public VData getResult() {
    return mResult;
  }

  private boolean checkDate(String grpContNo)
  {
      String sql = "select Cinvalidate From lcgrpcont where grpcontno ='"+grpContNo+"'";
      ExeSQL tExeSQL = new ExeSQL();
      String Cinvalidate = tExeSQL.getOneValue(sql);
      String tEdorappDate = mLPGrpEdorMainSchema.getEdorAppDate();
      PubFun tPubFun = new PubFun();
      int i = tPubFun.calInterval(Cinvalidate,tEdorappDate,"D");
      System.out.println("保全申请时间和保单满期日期间隔:" + i);
      if(i>30)
      {
          mErrors.addOneError("保单已经失效超过满期日30天,不能再添加保全项目!");
          return false;
      }
      return true;
  }
  // 获得保单状态的方法.
  private String checkContState(String grpContNo)
  {
      String sql ="select stateflag From lcgrpcont where grpcontno ='"+ grpContNo +"'";
      ExeSQL tExeSQL = new ExeSQL();
      String stateflag = tExeSQL.getOneValue(sql);

      return stateflag ;
  }
  
//校验在其它保单已存在保全项目状态下该保全项目能否添加 071206 zhanggm
  private boolean checkAddItem()
  {
	  LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
	  tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
	  tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
	 // LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
	 // tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
	  if(!tLPGrpEdorMainDB.getInfo())
	  {
		  return true;
	  }
	  else
	  {
		  if(!mLPGrpEdorItemSchema.getGrpContNo().equals(tLPGrpEdorMainDB.getGrpContNo()))
		  {
		      CError tError = new CError();
		      tError.moduleName = "GrpEdorItemBL";
		      tError.functionName = "checkAddItem";
		      tError.errorMessage = "一个工单只能对一个保单进行保全操作！";
		      this.mErrors.addOneError(tError);
		      return false;
		  }
	  }
	  return true;
  }
}
