package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import java.util.GregorianCalendar;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体定期结算公共处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author mojiao
 * @version 1.0
 */
public class GrpBalanceOnTime{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    public LJAPaySet mLJAPaySet;
    public LJAGetSet mLJAGetSet;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    public GrpBalanceOnTime() {}

    //建立团体定期计划记录
    public MMap addLCGrpBalPlan(String grpContNo, String cValiDate,
                                String balIntrv, String manageCom,
                                String operator) {
        int intBalIntrv=0;
        if (grpContNo == null || grpContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "保单号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (cValiDate == null || cValiDate.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "生效日期为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(balIntrv==null||balIntrv.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "结算频次为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(manageCom==null||manageCom.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "管理机构为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(operator==null||operator.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        LCGrpBalPlanSchema tLCGrpBalPlanSchema=new LCGrpBalPlanSchema();
        tLCGrpBalPlanSchema.setGrpContNo(grpContNo);
        tLCGrpBalPlanSchema.setBalTimes(0);
        tLCGrpBalPlanSchema.setBalStartDate(cValiDate);
        //tLCGrpBalPlanSchema.setPayEndDate();
        tLCGrpBalPlanSchema.setBaltoDate(cValiDate);
        if (balIntrv.equals("1"))
            intBalIntrv = 1;
        else if (balIntrv.equals("2"))
            intBalIntrv = 3;
        else if (balIntrv.equals("3"))
            intBalIntrv = 6;
        else if (balIntrv.equals("4"))
            intBalIntrv =0;
        else if (balIntrv.equals("5"))
            intBalIntrv =12;
        else {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "结算频次不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(intBalIntrv!=0)
        {

            tLCGrpBalPlanSchema.setNextDate(getNextDate(cValiDate,
                    intBalIntrv, "M", mCurrentDate));
        }
        tLCGrpBalPlanSchema.setBalIntv(intBalIntrv);
        tLCGrpBalPlanSchema.setBalMoney(0);
        //此处判断保单是否生效，如果生效lcgrpbalplan flag设置为0-生效，否则设置为1-失效
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet =  tLCGrpContDB.query();
        if(tLCGrpContSet.size()==0){
            tLCGrpBalPlanSchema.setFlag("1");
        }else if(tLCGrpContSet.get(1).getAppFlag().equals("1")){
            tLCGrpBalPlanSchema.setFlag("0");
        }else{
            tLCGrpBalPlanSchema.setFlag("1");
        }
        tLCGrpBalPlanSchema.setState("0");
        tLCGrpBalPlanSchema.setSumBal(0);
        tLCGrpBalPlanSchema.setManageCom(manageCom);
        tLCGrpBalPlanSchema.setOperator(operator);
        tLCGrpBalPlanSchema.setMakeDate(mCurrentDate);
        tLCGrpBalPlanSchema.setMakeTime(mCurrentTime);
        tLCGrpBalPlanSchema.setModifyDate(mCurrentDate);
        tLCGrpBalPlanSchema.setModifyTime(mCurrentTime);
        MMap map=new MMap();
        map.put(tLCGrpBalPlanSchema,"DELETE&INSERT");
        return map;
    }

    /**结算当前日期之后的下一个对应日
     * 根据新的需求，下一个对应日提前一天 2006-10-24  */
    public String getNextDate(String baseDate,int interval, String unit,String compareDate)
    {
        GregorianCalendar mCalendar = new GregorianCalendar();
        GregorianCalendar cCalendar = new GregorianCalendar();
        String tDate=baseDate;
        String tCompareDate = PubFun.calDate(compareDate,1, "D", null);
        do
        {
            tDate=PubFun.calDate(tDate,interval, unit, null);
            FDate tFDate = new FDate();
            Date bDate = tFDate.getDate(tDate);
            Date cDate = tFDate.getDate(tCompareDate);
            mCalendar.setTime(bDate);
            cCalendar.setTime(cDate);
        }while(!mCalendar.after(cCalendar));

        return PubFun.calDate(tDate,-1, "D", null);
    }

    //修改团体保单定期结算状态
    public MMap updateLCGrpBalPlanState(String grpContNo, String state,String operator,double sumMoney) {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        if (!tLCGrpBalPlanDB.getInfo()) {
            mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "updateLCGrpBalPlanState";
            tError.errorMessage = "查询团体保单结算计划信息失败!";
            this.mErrors.addOneError(tError);
            return null;
        }

        LCGrpBalPlanSchema tLCGrpBalPlanSchema=updateLCGrpBalPlanState(tLCGrpBalPlanDB.getSchema(),state,operator,sumMoney);
        MMap map=new MMap();
        map.put(tLCGrpBalPlanSchema, "UPDATE");
        return map;
    }

    //修改团体保单定期结算状态
    public LCGrpBalPlanSchema updateLCGrpBalPlanState(LCGrpBalPlanSchema tLCGrpBalPlanSchema, String state,String operator,double sumMoney) {
        if (state.equals("2")||state.equals("0")&&tLCGrpBalPlanSchema.getState().equals("1")) {
            tLCGrpBalPlanSchema.setBaltoDate(mCurrentDate);
            tLCGrpBalPlanSchema.setNextDate(getNextDate(tLCGrpBalPlanSchema.
                    getBalStartDate(),
                    tLCGrpBalPlanSchema.getBalIntv(), "M", mCurrentDate));
            tLCGrpBalPlanSchema.setBalTimes(tLCGrpBalPlanSchema.getBalTimes()+1);
        }
        if (state.equals("2")){
            tLCGrpBalPlanSchema.setBalMoney(sumMoney);
        }
        tLCGrpBalPlanSchema.setState(state);
        tLCGrpBalPlanSchema.setModifyDate(this.mCurrentDate);
        tLCGrpBalPlanSchema.setModifyTime(this.mCurrentTime);
        if(operator==null||operator.equals("")){
            tLCGrpBalPlanSchema.setOperator("pa0001");
        }else{
            tLCGrpBalPlanSchema.setOperator(operator);
        }
        return tLCGrpBalPlanSchema;
    }

    //修改团体定期结算频次
    public MMap changeLCGrpBalIntrv(String grpContNo,
                                    String balIntrv, String manageCom,
                                String operator)
    {
        if (grpContNo == null || grpContNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "保单号为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(balIntrv==null||balIntrv.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "结算频次为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(manageCom==null||manageCom.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "管理机构为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(operator==null||operator.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "addLCGrpBalPlan";
            tError.errorMessage = "操作员为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        LCGrpBalPlanDB tLCGrpBalPlanDB=new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        if(!tLCGrpBalPlanDB.getInfo())
        {
            mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            return null;
        }
        LCGrpBalPlanSchema tLCGrpBalPlanSchema=tLCGrpBalPlanDB.getSchema();
        return changeLCGrpBalIntrv(tLCGrpBalPlanSchema,balIntrv,manageCom,operator);
    }

    //修改团体定期结算频次
    public MMap changeLCGrpBalIntrv(LCGrpBalPlanSchema tLCGrpBalPlanSchema,
                                    String balIntrv, String manageCom,
                                    String operator)
    {
        int intBalIntrv=0;
        if(tLCGrpBalPlanSchema.getBalIntv()!=0&&!tLCGrpBalPlanSchema.getState().equals("0"))
        {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "changeLCGrpBalIntrv";
            tError.errorMessage = "当前保单结算状态不允许修改频次!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (balIntrv.equals("1"))
            intBalIntrv = 1;
        else if (balIntrv.equals("2"))
            intBalIntrv = 3;
        else if (balIntrv.equals("3"))
            intBalIntrv = 6;
        else if (balIntrv.equals("4"))
            intBalIntrv =0;
        else if (balIntrv.equals("5"))
            intBalIntrv =12;
        else {
            CError tError = new CError();
            tError.moduleName = "GrpBalanceOnTime";
            tError.functionName = "changeLCGrpBalIntrv";
            tError.errorMessage = "结算频次不正确!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(intBalIntrv!=0)
            tLCGrpBalPlanSchema.setNextDate(getNextDate(tLCGrpBalPlanSchema.
                    getBalStartDate(),
                    intBalIntrv, "M", mCurrentDate));
        else
            tLCGrpBalPlanSchema.setNextDate("");
        tLCGrpBalPlanSchema.setBalIntv(intBalIntrv);
        tLCGrpBalPlanSchema.setManageCom(manageCom);
        tLCGrpBalPlanSchema.setOperator(operator);
        tLCGrpBalPlanSchema.setModifyDate(mCurrentDate);
        tLCGrpBalPlanSchema.setModifyTime(mCurrentTime);
        MMap map=new MMap();
        map.put(tLCGrpBalPlanSchema,"UPDATE");
        return map;
    }

    //判断是否需要先定期结算-1--出错，0--不需要，1--需要
    public int ifNeedBalance(String grpContNo)
    {
        LCGrpBalPlanDB tLCGrpBalPlanDB=new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        LCGrpBalPlanSet tLCGrpBalPlanSet=tLCGrpBalPlanDB.query();
        if(tLCGrpBalPlanDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            return -1;
        }
        //如果不存在
        if(tLCGrpBalPlanSet.size()==0)
        {
            return 0;
        }
        LCGrpBalPlanSchema tLCGrpBalPlanSchema=tLCGrpBalPlanSet.get(1);
        if(!tLCGrpBalPlanSchema.getState().equals("0"))//如果在结算过程中
        {
            return 1;
        }
        if(tLCGrpBalPlanSchema.getBalIntv()==0)//如果是随时结算方式
        {
            return 0;
        }
        LJAPaySet tLJAPaySet=getLJAPay(grpContNo,tLCGrpBalPlanSchema.getBaltoDate(),this.mCurrentDate);
        if (tLJAPaySet == null) {
            return -1;
        }
        LJAGetSet tLJAGetSet=getLJAGet(grpContNo,tLCGrpBalPlanSchema.getBaltoDate(),this.mCurrentDate);
        if(tLJAPaySet==null)
        {
            return -1;
        }
        if (tLJAPaySet.size() + tLJAGetSet.size() == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    //获得定期结算汇总金额
    public Double getBalanceMoney(String grpContNo,String startDate,String endDate)
    {
        double sumPay=0.0;
        double sumGet=0.0;
        double sumMoney=0.0;
        LJAPaySet tLJAPaySet=getLJAPay(grpContNo,startDate,endDate);
        if(tLJAPaySet==null)
        {
            return null;
        }
        for (int i = 1; i <= tLJAPaySet.size(); i++) {
            LJAPaySchema tLJAPaySchema = tLJAPaySet.get(i);
            sumPay+=tLJAPaySchema.getSumActuPayMoney();
        }
        LJAGetSet tLJAGetSet=getLJAGet(grpContNo,startDate,endDate);
        if(tLJAPaySet==null)
        {
            return null;
        }
        for (int i = 1; i <= tLJAGetSet.size(); i++) {
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i);
            sumGet+=tLJAGetSchema.getSumGetMoney();
        }
        sumMoney=sumGet-sumPay;
        return new Double(sumMoney);
    }

    private LJAPaySet getLJAPay(String grpContNo,String startDate,String endDate)
    {
        LJAPayDB tLJAPayDB=new LJAPayDB();
        String strSql="select distinct a.* from LJAPay a,LJAGetEndorse b "+
                      "where a.PayNo=b.ActuGetNo and b.GrpContNo='"+grpContNo+"' and a.IncomeType='B'";
        LJAPaySet tLJAPaySet=tLJAPayDB.executeQuery(strSql);
        if(tLJAPayDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLJAPayDB.mErrors);
            return null;
        }
        this.mLJAPaySet=tLJAPaySet;
        return tLJAPaySet;
    }

    private LJAGetSet getLJAGet(String grpContNo,String startDate,String endDate)
    {
        LJAGetDB tLJAGetDB=new LJAGetDB();
        String strSql="select DISTINCT a.* from LJAGet a,LJAGetEndorse b where a.actugetno=b.ActuGetNo"+
                     " and  b.grpcontno='"+grpContNo+"' and a.paymode='B'";
        LJAGetSet tLJAGetSet=tLJAGetDB.executeQuery(strSql);
        if(tLJAGetDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLJAGetDB.mErrors);
            return null;
        }
        this.mLJAGetSet=tLJAGetSet;
        return tLJAGetSet;
    }

    public static void main(String[] args) {
        GrpBalanceOnTime t = new GrpBalanceOnTime();
        LCGrpBalPlanDB tLCGrpBalPlanDB=new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo("3");
        if(!tLCGrpBalPlanDB.getInfo())
        {
            System.out.println("aaaa");
        }
        MMap m=t.addLCGrpBalPlan("1400001437","2006-08-24","1","86","endor");
        VData data = new VData();
       data.add(m);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
       }
    }
}
