package com.sinosoft.lis.bq;

/**
 * <p>Title: 个单保全重复理算</p>
 * <p>Description: 个单保全重复理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorReCalUI
{
    private PEdorReCalBL mPEdorReCalBL = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public PEdorReCalUI(String edorAcceptNo)
    {
        mPEdorReCalBL = new PEdorReCalBL(edorAcceptNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorReCalBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorReCalBL.mErrors.getFirstError();
    }
}
