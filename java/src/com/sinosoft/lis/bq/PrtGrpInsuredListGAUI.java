package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListGAUI
{
    PrtGrpInsuredListGABL mPrtGrpInsuredListGABL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListGAUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListGABL = new PrtGrpInsuredListGABL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListGABL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPrtGrpInsuredListGABL.getInputStream();
    }
}
