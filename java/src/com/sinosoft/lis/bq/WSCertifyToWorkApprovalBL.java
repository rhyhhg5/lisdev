package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class WSCertifyToWorkApprovalBL {
	public CErrors mErrors = new CErrors();

	private String mMessage = null;

	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	private EdorItemSpecialData mSpecialData = null;

	private String mEdorNo = null;

	private String mEdorType = "JM";

	private String mCardNo = null;
	
	private String mInsuredNo = null;
	
    private TransferData mTransferData = null;
    
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/**
	 * 构造函数
	 * 
	 * @param gi
	 *            GlobalInput
	 * @param edorNo
	 *            String
	 * @param grpContNo
	 *            String
	 */
	public WSCertifyToWorkApprovalBL() {
	}

	public String getmEdorNo() {
		return mEdorNo;
	}

	public void setmEdorNo(String mEdorNo) {
		this.mEdorNo = mEdorNo;
	}

	/**
	 * 提交数据
	 * 
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}
		return true;
	}

	/**
	 * 得到提示信息
	 * 
	 * @return String
	 */
	public String getMessage() {
		return mMessage;
	}

	/**
	 * 得到输入数据
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {

		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null||mGlobalInput == null)
        {
        	CError tError = new CError();
			tError.moduleName = "WSCertifyToWorkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "所需参数不完整。";
			this.mErrors.addOneError(tError);
			return false;
        }
		mCardNo = (String) mTransferData.getValueByName("CardNo");
		mEdorNo = (String) mTransferData.getValueByName("EdorNo");
		mInsuredNo = (String) mTransferData.getValueByName("InsuredNo");
		if (mCardNo.equals("") || mCardNo == null||mEdorNo.equals("") || mEdorNo == null||mInsuredNo.equals("") || mInsuredNo == null) {
			
			CError.buildErr(this, "line130:数据获取失败!");
			System.out
					.println("WSCertifyToWorkApprovalBL+getInputData++--");
			return false;
		}
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		LPEdorItemSet tLPEdorItemSet=new LPEdorItemSet();
		LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
		tLPEdorItemDB.setEdorNo(mEdorNo);
		tLPEdorItemDB.setInsuredNo(mInsuredNo);
		tLPEdorItemDB.setContNo(mCardNo);
		tLPEdorItemSet=tLPEdorItemDB.query();
		if(tLPEdorItemSet.size()!=1){
			CError.buildErr(this, "line154-获取保全工单表失败");
			System.out.println("WSCertifyToWorkApprovalBL+dealData++--");
			return false;
		}
		mLPEdorItemSchema=tLPEdorItemSet.get(1);
		//现在设置一个默认的值,为99
		mLPEdorItemSchema.setEdorReasonNo("99");
		mMap.put(mLPEdorItemSchema, "UPDATE");
		
		return true;
	}

	/**
	 * 校验数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		String checkClaim="";
		checkClaim=new ExeSQL().getOneValue("select 1 from llcase where customerno in (select insuredno from lcpol where contno='"+mCardNo+"') and rgtstate not in ('11','12','14') with ur");
	
		if(checkClaim.equals("1"))
		{
		CError.buildErr(this, "工单生成处理失败，原因是保单号: "+mCardNo+"所对应的被保人存在理赔");
		System.out.println("WSCertifyToWorkApprovalBL+checkData++--");
		return false;
		}
		return true;
	}


	/**
	 * 提交数据到数据库
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		VData data = new VData();
		data.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		WSCertifyToWorkApprovalBL bl =new WSCertifyToWorkApprovalBL();
//		bl.mCardNo="21000002134";
//		bl.mEdorNo="20100902000002";
//		bl.mGrpContNo="00003372000001";
//		bl.mEdorType="JM";
		GlobalInput gi=new GlobalInput();
		gi.ManageCom="86";
		gi.Operator="xp";

		LICertifySchema tLICertifySchema = new LICertifySchema();
		tLICertifySchema.setCardNo("21000000465");
	    VData tVData = new VData();

	    TransferData tTransferData = new TransferData();

	    tVData.add(tLICertifySchema);
	    tVData.add(tTransferData);
	    tVData.add(gi);
	    bl.submitData(tVData, null);
	}
	
}
