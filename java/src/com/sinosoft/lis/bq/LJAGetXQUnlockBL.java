package com.sinosoft.lis.bq;
//程序名称：LJAGetXQUnlockBL.java
//程序功能：续期付费解锁（个单给付）
//创建日期：2009-4-15
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class LJAGetXQUnlockBL extends LJSUnlockBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 操作员 */
	private String mOperater ;
	
	private String mGetNoticeNo = null ;
	
	private String mPayMode = null ;
	
	private String mDrawer = null ;
	
	private String mDrawerID = null ;
	
	private String mBankCode = null ;
	
	private String mBankAccNo = null ;
	
	private String mAccName = null ;
	
	private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
	
	
	public LJAGetXQUnlockBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData)
	{
        //将操作数据拷贝到本类中
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
            tError.moduleName = "LJAGetXQUnlockBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败LJAGetXQUnlockBL-->getInputData!";
            this.mErrors.addOneError(tError);
	        return false;
	    }
	    
        //数据校验
        if (!checkData()) 
        {
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "LJAGetXQUnlockBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LJAGetXQUnlockBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }
	    
	    //提交
	    if(!submitData())
	    {
	    	return false;
	    }
	    
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		StringBuffer sqlLJA = new StringBuffer();
		StringBuffer sqlLJS = new StringBuffer(); 
		StringBuffer sqlDraw = new StringBuffer(); 
		sqlLJA.append("update ljaget set paymode = '").append(mPayMode).append("', ")
		   .append("bankcode = '").append(mBankCode).append("', ")
		   .append("bankaccno = '").append(mBankAccNo).append("', ")
		   .append("accname = '").append(mAccName).append("', ")
		   .append("drawer = '").append(mDrawer).append("', ")
		   .append("drawerid = '").append(mDrawerID).append("', ")
		   .append("cansendbank = null, operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where actugetno = '").append(mGetNoticeNo).append("' ");
		sqlLJS.append("update ljsget set paymode = '").append(mPayMode).append("', ")
		   .append("bankcode = '").append(mBankCode).append("', ")
		   .append("bankaccno = '").append(mBankAccNo).append("', ")
		   .append("accname = '").append(mAccName).append("', ")
		   .append("drawer = '").append(mDrawer).append("', ")
		   .append("drawerid = '").append(mDrawerID).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where GetNoticeNo = '").append(mGetNoticeNo).append("' ");
		sqlDraw.append("update ljsgetdraw set bankcode = '").append(mBankCode).append("', ")
		   .append("bankaccno = '").append(mBankAccNo).append("', ")
		   .append("accname = '").append(mAccName).append("', ")
		   .append("drawer = '").append(mDrawer).append("', ")
		   .append("drawerid = '").append(mDrawerID).append("', ")
		   .append("operator = '").append(mOperater).append("', ")
		   .append("modifydate = '").append(mCurrentDate).append("', ")
		   .append("modifytime = '").append(mCurrentTime).append("' ")
		   .append("where GetNoticeNo = '").append(mGetNoticeNo).append("' ");
		map.put(sqlLJA.toString(), SysConst.UPDATE);
		map.put(sqlLJS.toString(), SysConst.UPDATE);
		map.put(sqlDraw.toString(), SysConst.UPDATE);
		MMap tMMap = createTask();
        if(tMMap == null)
        {
        	CError tError = new CError();
	        tError.moduleName = "LJAGetXQUnlockBL";
	        tError.functionName = "dealDate";
	        tError.errorMessage = "生成工单信息失败LJAGetXQUnlockBL-->dealData!Line138";
	        this.mErrors .addOneError(tError) ;
	        return false;
        }
        map.add(tMMap);
	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    mGetNoticeNo = (String)tTransferData.getValueByName("GetNoticeNo");
		mPayMode = (String)tTransferData.getValueByName("PayMode");
		mDrawer = (String)tTransferData.getValueByName("Drawer");
		mDrawerID = (String)tTransferData.getValueByName("DrawerID");
		mBankCode = (String)tTransferData.getValueByName("BankCode");
		mBankAccNo = (String)tTransferData.getValueByName("BankAccNo");
		mAccName = (String)tTransferData.getValueByName("AccName");
		LJAGetDB tLJAGetDB = new LJAGetDB();
		tLJAGetDB.setActuGetNo(mGetNoticeNo);
		if(!tLJAGetDB.getInfo())
		{
			return false;	
		}
		mLJAGetSchema = tLJAGetDB.getSchema();
		return true;
	}
	 
    private boolean submitData()
    {
    	VData tVData = new VData();
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJAGetXQUnlockBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    return true;
    }
    
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        if (mGlobalInput == null) 
        {
        	CError tError = new CError();
            tError.moduleName = "LJAGetXQUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mGlobalInput为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mGetNoticeNo == null || "".equals(mGetNoticeNo) || "null".equals(mGetNoticeNo)) 
        {
        	CError tError = new CError();
            tError.moduleName = "LJAGetXQUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mGetNoticeNo为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mPayMode == null || "".equals(mPayMode) || "null".equals(mPayMode)) 
        {
        	CError tError = new CError();
            tError.moduleName = "LJAGetXQUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mPayMode为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * createTask
     *
     * @return MMap
     */
    private MMap createTask()
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mLJAGetSchema.getAppntNo());
        tLGWorkSchema.setInnerSource(mLJAGetSchema.getActuGetNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo("03");
        StringBuffer tRemark = new StringBuffer();
        tRemark.append("自动批注：续期付费记录解锁，由[缴费方式:").append(mLJAGetSchema.getPayMode())
               .append(",银行编码").append(mLJAGetSchema.getBankCode())
               .append(", 账户名:").append(mLJAGetSchema.getAccName())
               .append(", 账号").append(mLJAGetSchema.getBankAccNo())
               .append(", 领取人:").append(mLJAGetSchema.getDrawer())
               .append(", 领取人身份证:").append(mLJAGetSchema.getDrawerID())
               .append("] 变更为 [缴费方式:").append(mPayMode)
               .append(",银行编码").append(mBankCode)
               .append(", 账户名:").append(mAccName)
               .append(", 账号").append(mBankAccNo)
               .append(", 领取人:").append(mDrawer)
               .append(", 领取人身份证:").append(mDrawerID).append("]");
        tLGWorkSchema.setRemark(tRemark.toString());
        String workBoxNo = getWorkBox();
        if(workBoxNo == null)
        {
            return null;
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGlobalInput);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("生成工单信息失败:\n"+ tTaskInputBL.mErrors.getFirstError());
            return null;
        }
        return tMMap;
    }
    /**
     * getWorkBox
     *
     * @return String
     */
    private String getWorkBox()
    {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + mOperater + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if(tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null"))
        {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }
        return tWorkBox;
    }
    public CErrors getErrors()
    {
    	return mErrors;
    }
}
