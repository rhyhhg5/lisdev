package com.sinosoft.lis.bq;

import com.sinosoft.lis.f1print.PrintPauseListBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintGracePeriodNoticeUI {
	public CErrors mErrors = new CErrors();
    private XmlExport tXmlExport;
    public PrintGracePeriodNoticeUI(){
    	
    }
    public XmlExport getXmlExport(VData data, String operate)
    {
    	    PrintGracePeriodNoticeBL tPrintGracePeriodNoticeBL = new PrintGracePeriodNoticeBL();
            tXmlExport=tPrintGracePeriodNoticeBL.getXmlExport(data, operate);
            mErrors.copyAllErrors(tPrintGracePeriodNoticeBL.mErrors);
            return tXmlExport;
    }
}
