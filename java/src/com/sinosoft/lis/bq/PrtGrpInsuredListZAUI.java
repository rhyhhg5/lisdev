package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.VData;

/**
* <p>Title: 团体医疗追加保费清单</p>
* <p>Description:团体医疗追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListZAUI
{
    PrtGrpInsuredListZABL mPrtGrpInsuredListZABL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListZAUI(GlobalInput gi, String edorNo)
    {
        mPrtGrpInsuredListZABL = new PrtGrpInsuredListZABL(gi, edorNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPrtGrpInsuredListZABL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public VData getResult()
    {
        return mPrtGrpInsuredListZABL.getResult();
    }
}
