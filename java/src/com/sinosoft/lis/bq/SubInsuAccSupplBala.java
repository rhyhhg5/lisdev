package com.sinosoft.lis.bq;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsureAccBalanceDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMInsuAccRateDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsureAccBalanceSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.utility.*;

import java.lang.String;

import org.apache.poi.hssf.util.HSSFColor.GOLD;

/**
 * 满期金进入附加万能账户后补利息
 * 
 * @author lzy
 * 
 */
public class SubInsuAccSupplBala {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	private GlobalInput tGI = new GlobalInput();

	private LCContSchema mLCContSchema = null;// 保单信息
	private LCPolSchema mLCPolSchema = null;// 满期主险信息
	private LPEdorItemSchema mLPEdorItemSchema = null;//追加保费工单

	/** 数据操作字符串 */
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	
	private String mSubSql = "";
	
	private ExeSQL mExeSQL = new ExeSQL();
	private MMap mMMap = new MMap();

	public SubInsuAccSupplBala() {
	}

	/**
	 * @param cInputData
	 *            VData，包含： 1、 GlobalInput对象，完整的登陆用户信息 2、
	 *            TransferData对象，包含StartDate， EndDate， ManageCom
	 * @param cOperate
	 *            String：操作类型，此可为空字符串“”
	 * @return boolean：操作成功true，否则false
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 接收传入数据
		if (!getInputData(cInputData)) {
			return false;
		}
		// 处理数据
		if (!dealData()) {
			return false;
		}
		
		if(!submit()){
			return false;
		}
		
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param mInputData
	 *            : 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {
		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

		mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		if (tGI == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitBatchBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);

			return false;
		}
		
		if(null != mLPEdorItemSchema){
			mSubSql = " and b.edorno = '"+mLPEdorItemSchema.getEdorNo()+"' ";
		}

		return true;
	}

	private boolean checkData() {
		// 校验保全
		if (!checkBQ()) {
			return false;
		}
		// 校验理赔
		if (!checkLP()) {
			return false;
		}
		return true;
	}

	private boolean checkBQ() {
		// 校验保全项目
		String sql = "select a.edorNo " + "from LPEdorItem a, LPEdorApp b "
				+ "where a.edorAcceptNo = b.edorAcceptNo "
				+ "   and a.ContNo = '" + mLCContSchema.getContNo() + "' "
				+ "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
		String edorNo = mExeSQL.getOneValue(sql);
		if (!edorNo.equals("")) {
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitGetBL";
			tError.functionName = "checkBQ";
			tError.errorMessage = "保单" + mLCContSchema.getContNo() + "正在做保全业务："
					+ edorNo + "";
			System.out.println(tError.errorMessage);
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean checkLP() {
		// 正在做理赔不允许做满期给付
		String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='"
				+ mLCContSchema.getContNo()
				+ "')"
				+ " and endcasedate is null and rgtstate not in('11','12','14') "; // 11-
		String rgtNo = mExeSQL.getOneValue(sql);
		if (rgtNo != null && !rgtNo.equals("")) {
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitGetBL";
			tError.functionName = "checkLP";
			tError.errorMessage = "保单有未结案的理赔。";
			mErrors.addOneError(tError);
			System.out.println("保单" + mLCContSchema.getContNo() + "有未结案的理赔");
			return false;
		}
		return true;
	}

	public boolean dealData() {
		// 获取附加万能追加保费的工单
		String str = "select distinct a.contno,a.polno,b.edorno from lcpol a,lpedoritem b,lpedorapp c "
				+ " where a.contno=b.contno and a.polno=b.polno and b.edorno=c.edoracceptno and b.edortype='ZB' "
				+ " and a.conttype='1' and a.appflag='1' and c.edorstate='0' "
				+ " and (a.riskcode in (select riskcode from lmriskapp where riskcode=a.riskcode and risktype4='4' and subriskflag='S') "
				+ " or a.riskcode in ('340501','340601','340602') )"
				+ " and not exists (select 1 from lpedorespecialdata where edorno=b.edorno and edortype=b.edortype and detailtype='BALASTATE') "
//				+ " and a.contno='014011774000001' "//测试
				+ mSubSql
				+ " with ur";
		SSRS tSSRS = new ExeSQL().execSQL(str);
		if (null == tSSRS || tSSRS.MaxRow==0) {
			System.out.println("没有需要补结的附加万能账户");
			return true;
		}
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			String mContno = tSSRS.GetText(i, 1);
			String mPolno = tSSRS.GetText(i, 2);
			String mEdorno = tSSRS.GetText(i, 3);
			mLCContSchema = queryCont(mContno);
			mLCPolSchema = queryPol(mPolno);
			// 校验
			if (!checkData()) {
				continue;
			}

			LCInsureAccSet tLCInsureAccSet = getAcc(mLCPolSchema.getPolNo());
			if (null == tLCInsureAccSet) {
				continue;
			}
			for (int j = 1; j <= tLCInsureAccSet.size(); j++) {
				LCInsureAccClassSet tLCInsureAccClassSet = 
					getAccClass(tLCInsureAccSet.get(j));
				for (int k = 1; k <= tLCInsureAccClassSet.size(); k++) {
					LCInsureAccClassSchema classSchema = 
						tLCInsureAccClassSet.get(k);
					if (!balaInsuACC(mEdorno, classSchema)) {
						System.out.println("保单" + classSchema.getContNo()
								+ "的满期金补利息时出错："+mErrors.getFirstError());
						dealErrorLog(mLCPolSchema,"附加万能账户的满期金补结利息出错："+mErrors.getFirstError());
						continue;
					}
				}
				MMap tMap = updateAccInfo(tLCInsureAccSet.get(j).getSchema());
				MMap ttMMap = addEspecialdata(mEdorno);
				mMMap.add(tMap);
				mMMap.add(ttMMap);
			}
		}

		return true;
	}

	private boolean balaInsuACC(String mEdorno,LCInsureAccClassSchema tAccClassSchema){
		//避免C表账户发生变动，这里要用LPInsureAccClass的baladate
    	String str="select distinct b.* from LPInsureAccTrace a,LMInsuAccRate b,LPInsureAccClass c "
    				+ " where a.insuaccno=b.insuaccno "
    				+ " and a.paydate<b.BalaDate "
    				+ " and a.contno=c.contno "
    				+ " and c.polno=a.polno "
    				+ " and a.insuaccno=c.insuaccno "
    				+ " and a.money <>0 "
    				+ " and a.paydate<c.baladate "
    				+ " and b.BalaDate<=c.baladate "
    				+ " and b.RateType = 'C' and b.RateIntv = 1 and b.RateIntvUnit = 'Y' "
    				+ " and c.edorno='"+mEdorno+"' "
    				+ " and a.contno='"+tAccClassSchema.getContNo()+"' "
    				+ " and a.polno='"+tAccClassSchema.getPolNo()+"' "
    				+ " and a.otherno='"+mEdorno+"' "
    				+ " order by b.startbaladate with ur";
    	LMInsuAccRateSet tLMInsuAccRateSet = new LMInsuAccRateDB().executeQuery(str);
    	if(null == tLMInsuAccRateSet || tLMInsuAccRateSet.size()==0){
    		System.out.println("没有查询到需追溯结算的区间");
    		return true;
    	}
    	System.out.println("需对满期金补结算"+tLMInsuAccRateSet.size()+"条");
    	
    	 LMCalModeDB tLMCalModeDB = new LMCalModeDB();
         tLMCalModeDB.setRiskCode(tAccClassSchema.getRiskCode());
         tLMCalModeDB.setType("K");
         LMCalModeSet set = tLMCalModeDB.query();
         if(set.size() == 0)
         {
             mErrors.addOneError("没有查询到账户收益算法");
             return false;
         }
    	
    	FDate tFDate = new FDate();
    	double lastMonthLX = 0;//本月之前补结的利息之和
    	double addMoney = 0;//月结记录增加的金额
    	for(int i=1; i<=tLMInsuAccRateSet.size(); i++){
    		LMInsuAccRateSchema tLMInsuAccRateSchema = tLMInsuAccRateSet.get(i);
    		//计算满期金应进账户后的利息
            String strTrace = "select * from lcinsureacctrace where 1=1 "
            				+ " and contno='" + tAccClassSchema.getContNo()
            				+ "' and polno='" + tAccClassSchema.getPolNo()
            				+ "' and otherno='" + mEdorno
            				+ "' and insuaccno='" + tAccClassSchema.getInsuAccNo()
            				+ "' and money<>0 "
            				+ " with ur";
            LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceDB().executeQuery(strTrace);
            double oldMoney = 0;
            double moneyLx = 0; //本月产生的利息
            for(int j = 1; j <= tLCInsureAccTraceSet.size(); j++)
            {
            	Calculator tCalculator=new Calculator();
            	tCalculator.setCalCode(set.get(1).getCalCode());
            	tCalculator.addBasicFactor("EndDate",tLMInsuAccRateSchema.getBalaDate());
            	tCalculator.addBasicFactor("BalaType","1");
            	tCalculator.addBasicFactor("RateType","1");
            	tCalculator.addBasicFactor("ContNo", tAccClassSchema.getContNo());
                tCalculator.addBasicFactor("PolNo", tAccClassSchema.getPolNo());
                tCalculator.addBasicFactor("InsuredNo", tAccClassSchema.getInsuredNo());
                tCalculator.addBasicFactor("InsuAccNo", tAccClassSchema.getInsuAccNo());
                tCalculator.addBasicFactor("PayPlanCode", tAccClassSchema.getPayPlanCode());
                tCalculator.addBasicFactor("RiskCode", tAccClassSchema.getRiskCode());
                tCalculator.addBasicFactor("InsuAccNo", tAccClassSchema.getInsuAccNo());
                tCalculator.addBasicFactor("StartDate", tLMInsuAccRateSchema.getStartBalaDate());
                String payDate =tLCInsureAccTraceSet.get(j).getPayDate();
                oldMoney = tLCInsureAccTraceSet.get(j).getMoney() + lastMonthLX;//补结第二月起需要将之前补的利息和作为本金计算
                
                if(!tFDate.getDate(payDate).before(tFDate.getDate(tLMInsuAccRateSchema.getStartBalaDate()))
                		&& tFDate.getDate(payDate).before(tFDate.getDate(tLMInsuAccRateSchema.getBalaDate()) )){
                	addMoney += oldMoney;
                }else{
                //若补结进账当月之后的记录时startdate就是公布的利率结算起始日
//                if(tFDate.getDate(payDate).before(tFDate.getDate(tLMInsuAccRateSchema.getStartBalaDate())) ){
                	payDate = tLMInsuAccRateSchema.getStartBalaDate();
                }
                tCalculator.addBasicFactor("TraceStartDate",payDate); //轨迹计算区间为产生轨迹日期到EndDate
                
                tCalculator.addBasicFactor("TraceMoney","" + oldMoney);
                String resultMoney;
                resultMoney = tCalculator.calculate();
                if(tCalculator.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tCalculator.mErrors);
                    return false;
                }
                if(null == resultMoney || "".equals(resultMoney)){
                	mErrors.addOneError("获取账户轨迹计息结果错误");
                	return false;
                }
                moneyLx += Double.parseDouble(resultMoney) - oldMoney;
                addMoney += moneyLx;
            }
            moneyLx = PubFun.setPrecision(moneyLx, "0.00");
            addMoney = PubFun.setPrecision(addMoney, "0.00");
            lastMonthLX += moneyLx;
            System.out.println("结算日："+tLMInsuAccRateSchema.getBalaDate()+",补计利息："+moneyLx);
            
            Reflections ref = new Reflections();
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, tAccClassSchema);

            String tLimit = PubFun.getNoLimit(tAccClassSchema.getManageCom());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            traceSchema.setSerialNo(serNo);
            traceSchema.setOtherType("6"); //月结算
            traceSchema.setMoneyType("LX");
            traceSchema.setPayDate(tLMInsuAccRateSchema.getBalaDate());
            traceSchema.setMoney(PubFun.setPrecision(moneyLx, "0.00"));
            PubFun.fillDefaultField(traceSchema);
            
            //更新账户结算记录表
    		LCInsureAccBalanceDB tAccBalanceDB = new LCInsureAccBalanceDB();
    		tAccBalanceDB.setPolNo(tAccClassSchema.getPolNo());
    		tAccBalanceDB.setContNo(tAccClassSchema.getContNo());
    		tAccBalanceDB.setDueBalaDate(tLMInsuAccRateSchema.getBalaDate());
    		LCInsureAccBalanceSet tAccBalanceSet  = tAccBalanceDB.query();
    		if(null == tAccBalanceSet || tAccBalanceSet.size()!=1 ){
    			mErrors.addOneError("获取账户月结记录失败");
    			return false;
    		}
    		LCInsureAccBalanceSchema tAccBalanceSchema = tAccBalanceSet.get(1).getSchema();
    		tAccBalanceSchema.setInsuAccBalaAfter(tAccBalanceSchema.getInsuAccBalaAfter()+addMoney);
    		tAccBalanceSchema.setModifyDate(PubFun.getCurrentDate());
    		tAccBalanceSchema.setModifyTime(PubFun.getCurrentTime());
    		System.out.println(tAccBalanceSchema.getDueBalaDate()+"的月结后账户余额增加"+addMoney+"元");
    		
    		traceSchema.setOtherNo(tAccBalanceSchema.getSequenceNo());
    		
    		mMMap.put(traceSchema, SysConst.INSERT);
    		mMMap.put(tAccBalanceSchema, SysConst.DELETE_AND_INSERT);
    		
    	}
    	
    	return true;
    }
	
	//为避免月结回退时发生问题，所以这里的结算流水号要使用原来的。
//	private String getBalanceSequenceNo(LCInsureAccTraceSchema traceSchema,LMInsuAccRateSchema tLMInsuAccRateSchema){
//		String str="select sequenceno  from LCInsureAccBalance "
//					+ " where contno='' "
//					+ " and polno='' "
//					+ " and insuaccno='' "
//					+ " and duebaladate='' ";
//		String oldSeq = new ExeSQL().getOneValue(str);
//		if(null != oldSeq && !"".equals(oldSeq)){
//			return oldSeq;
//		}else{
//			return new InsuAccBala().getBalanceSequenceNo(); 
//		}
//	}

	/**
	 * 在已经补结过的追加保费工单添加标记
	 * 
	 * @param mEdorNO
	 * @return
	 */
	private MMap addEspecialdata(String mEdorNO) {
		MMap tMMap = new MMap();
		LPEdorEspecialDataSchema schema = new LPEdorEspecialDataSchema();
		schema.setEdorAcceptNo(mEdorNO);
		schema.setEdorNo(mEdorNO);
		schema.setEdorType(BQ.EDORTYPE_ZB);
		schema.setDetailType("BALASTATE");
		schema.setEdorValue("0");
		schema.setPolNo(BQ.FILLDATA);

		tMMap.put(schema, SysConst.INSERT);
		return tMMap;
	}

	private LCInsureAccSet getAcc(String mPolNO) {
		LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
		tLCInsureAccDB.setPolNo(mPolNO);
		LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
		if (tLCInsureAccDB.mErrors.needDealError()) {
			CError.buildErr(this, "帐户信息查询失败");
			return null;
		}
		if (tLCInsureAccSet == null || tLCInsureAccSet.size() < 1) {
			CError.buildErr(this, "没有帐户信息");
			return null;
		}
		return tLCInsureAccSet;
	}

	private LCInsureAccClassSet getAccClass(LCInsureAccSchema tLCInsureAccSchema) {
		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
		LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
		if (tLCInsureAccClassDB.mErrors.needDealError()) {
			CError.buildErr(this, "子帐户信息查询失败");
			return null;
		}
		if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1) {
			CError.buildErr(this, "没有子帐户信息");
			return null;
		}
		return tLCInsureAccClassSet;
	}

	private LCContSchema queryCont(String mContNo) {
		String str = "select * from LCCont where Contno='" + mContNo + "' ";
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = tLCContDB.executeQuery(str);
		if (tLCContSet.size() > 0) {
			return tLCContSet.get(1).getSchema();
		} else {
			return null;
		}
	}

	private LCPolSchema queryPol(String mPolNo) {
		String str = "select * from LCPol where polno='" + mPolNo + "' ";
		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet tLCPolSet = tLCPolDB.executeQuery(str);
		if (tLCPolSet.size() > 0) {
			return tLCPolSet.get(1).getSchema();
		} else {
			return null;
		}
	}
	
	/**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccSchema mLCInsureAccSchema)
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + mLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        //按轨迹更新账户管理费信息
        sql = "update LCInsureAccClassFee a " +
              "set Fee =(select sum(Fee) from LCInsureAccFeeTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo ) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        //更新账户管理费信息
        sql = "update LCInsureAccFee a " +
              "set (Fee, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(Fee), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClassFee where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + mLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + mLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);

        return tMMap;
    }

    /**
	 * 
	 * @return boolean
	 */
	private boolean submit() {
		try {
			VData tData = new VData();
			tData.add(mMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tData, "")) {
				mMMap =new MMap();
				System.out.print("保存数据返回");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "GetTempFeeSeparateBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception ex) {
			mMMap =new MMap();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMMap =new MMap();
		return true;
	}
	
	/**
     * 加入到日志表数据,记录因为各种原因抽档失败的单子
     * @param
     * @return boolean
     */
 private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
        LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
        tLGErrorLogSchema.setSerialNo(CurrentDate+PubFun.getCurrentTime2());
        tLGErrorLogSchema.setErrorType("0003");//附加万能满期
        tLGErrorLogSchema.setErrorTypeSub("03");
        tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
        tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
        tLGErrorLogSchema.setOperator(tGI.Operator);
        tLGErrorLogSchema.setMakeDate(CurrentDate);
        tLGErrorLogSchema.setMakeTime(CurrentTime);
        tLGErrorLogSchema.setModifyDate(CurrentDate);
        tLGErrorLogSchema.setModifyTime(CurrentTime);
        tLGErrorLogSchema.setDescribe(Error);

        MMap tMap = new MMap();
        tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
            return false;
        }
        return true;
 }
	
	public static void main(String[] args) {
		SubInsuAccSupplBala tAccSupplBala =new SubInsuAccSupplBala();
		GlobalInput tGlobalInput=new GlobalInput();
		VData tData=new VData();
		tData.add(tGlobalInput);
		LPEdorItemSchema lpEdorItemSchema = new LPEdorItemSchema();
		lpEdorItemSchema.setEdorNo("20190122000086");
		tData.add(lpEdorItemSchema);
		tAccSupplBala.submitData(tData, "");
		
	}

}
