package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.db.LOLoanDB;
import com.sinosoft.lis.db.LPGetDB;
import com.sinosoft.lis.db.LPInsureAccTraceDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.AccountManage;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.BqCalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsureAccBalanceSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LMInsuAccRateSchema;
import com.sinosoft.lis.schema.LMPolDutyEdorCalSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.schema.LPInsureAccClassSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.schema.LPLoanSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPReturnLoanSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.lis.vschema.LOLoanSet;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.vschema.LPInsureAccTraceSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

//程序名称：PEdorDDAppConfirm.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorHAAppConfirmBL implements EdorAppConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 合同号 */
	private String mContNo = null;

	/** 保单险种号 */
	private String mPolNo = null;

	/** 险种代码 */
	private String mRiskCode = null;

	/** 修改后的领取方式 */
	private String mBonusGetMode = null;

	/** 保单险种 */
	private LCPolSchema mLCPolSchema = null;

	/** 保单险种 */
	private LPPolSchema mLPPolSchema = null;
	
	private LCGetSchema mLCGetSchema=null;
	
	private LPGetSchema mLPGetSchema=null;
	
	/** 保全生效日期 */
	private String mEdorValiDate = null;

	private ExeSQL mExeSQL = new ExeSQL();

	/** 保全交费总额 */
	private double mTotalGetMoney = 0.00;
	
	/** 红利利息金额 */
	private double mGetMoney = 0.00;
	
	/** 满期金额 */
	private double mGetMJMoney = 0.00;
	
	/** 保全还款金额 */
	private double mLoanMoney = 0.00;
	
	/** 是否存在未还贷款标志*/
    private boolean IsLoan = false;
    
    /**是否曾因为当前未还贷款失效标志*/
    private boolean IsLoanAbate = false;
    
    private Reflections ref=new Reflections();
    
	/** 保全项目特殊数据 */
	private LPLoanSchema mLPLoanSchema = new LPLoanSchema();
	private LPReturnLoanSchema mLPReturnLoanSchema = new LPReturnLoanSchema();

	private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;

	private DetailDataQuery mQuery = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private LCInsureAccSchema mLCInsureAccSchema = null;

    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    
	private LCInsureAccClassSchema mLCInsureAccClassSchema = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String YXCalCode = "";// 有效

	private String ZZCalCode = "";// 失效

	private String FXCalCode = "";// 复效

	private String JYLPCalCode = "";// 解约理赔

	private String MQCalCode = "";// 满期

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			System.out.println("PEdorHAAppConfirm.java->getInputData(cInputData)失败");
			return false;
		}

		if (!checkData()) {
			System.out.println("PEdorHAAppConfirm.java->checkData()失败");
			return false;
		}

		if (!dealData()) {
			System.out.println("PEdorHAAppConfirm.java->dealData()失败");
			return false;
		}
		return true;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	public boolean checkData() {	
		
    	if(mLPEdorItemSchema.getEdorValiDate()==null||mLPEdorItemSchema.getEdorValiDate().equals("")){
    		// @@错误处理
			System.out.println("PEdorCTAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorCTAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "获取保全生效日期失败!";
			mErrors.addOneError(tError);
			return false;
    	}
//    	//保全生效日期与理算日的校验只针对分红险
//    	String tcheckFH=mExeSQL.getOneValue("select 1 from lppol a where edorno='"+mLPEdorItemSchema.getEdorNo()+"' and contno='"+mLPEdorItemSchema.getContNo()+"' " +
//    					" and exists (select 1 from lmriskapp where riskcode=a.riskcode and RiskType4= '2') with ur");
//    	if(tcheckFH!=null&&(!tcheckFH.equals(""))){
//    		if(!PubFun.getLaterDate(mLPEdorItemSchema.getEdorValiDate(), mCurrentDate).equals(mLPEdorItemSchema.getEdorValiDate())){
//        		// @@错误处理
//    			System.out.println("PEdorCTAppConfirmBL+checkData++--");
//    			CError tError = new CError();
//    			tError.moduleName = "PEdorCTAppConfirmBL";
//    			tError.functionName = "checkData";
//    			tError.errorMessage = "保全生效日期必须大于或者等于理算当天!";
//    			mErrors.addOneError(tError);
//    			return false;
//        	}
//    	}
    	
    	
//    	 查询即往借款信息,取得上次借款的本金额度和利息
		LOLoanDB tLOLoanDB = new LOLoanDB();
		LOLoanSet tLOLoanSet = tLOLoanDB.executeQuery("select * from loloan where polno in (select polno from lcpol where contno='" + mLPEdorItemSchema.getContNo()
				+ "') and loantype='0' and payoffflag='0' with ur");
		// //loanType 1-自垫 0-借款
		// tLOLoanDB.setLoanType("0");
		// //1-还清
		// tLOLoanDB.setPayOffFlag("0");

		if (tLOLoanSet.size() > 1) {
			// @@错误处理
			System.out.println("PEdorRFAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorRFAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "获取贷款表失败!";
			mErrors.addOneError(tError);
			return false;
		}
		if(tLOLoanSet.size()==1){
			IsLoan=true;
			ref.transFields(mLPLoanSchema, tLOLoanSet.get(1));
			//actugetno为还款工单号
			mLPLoanSchema.setActuGetNo(mLPEdorItemSchema.getEdorNo());
			mLPLoanSchema.setEdorType(mLPEdorItemSchema.getEdorType());
//        	是否存在未还贷款,并且由此贷款曾经导致过保单失效.
            String tcheck=mExeSQL.getOneValue("select min(b.startdate) from loloan a,lccontstate b "
            		+ " where a.payoffflag='0' and a.polno=b.polno and a.actugetno=b.otherno and b.othernotype='10' " 
            		+ " and b.statereason='LN' and  b.contno='"+mLPEdorItemSchema.getContNo()+"'");
            if(tcheck!=null&&(!tcheck.equals(""))){
//            	保全生效日期必须大于或者等于保单当时的失效日期
            	if(PubFun.getLaterDate(mLPEdorItemSchema.getEdorValiDate(), tcheck).equals(mLPEdorItemSchema.getEdorValiDate())){
//            		存储原始生效日期
            		mEdorValiDate=mLPEdorItemSchema.getEdorValiDate();
//            		设置生效日期为失效当天
            		mLPEdorItemSchema.setEdorValiDate(tcheck);
            		IsLoanAbate=true;
            	}else{
    	        	// @@错误处理
    				System.out.println("PEdorCTAppConfirmBL+checkData++--");
    				CError tError = new CError();
    				tError.moduleName = "PEdorCTAppConfirmBL";
    				tError.functionName = "checkData";
    				tError.errorMessage = "保全生效日期必须晚于由于贷款而失效的当天,该保单该日期为:"+tcheck;
    				mErrors.addOneError(tError);
    				return false;
            	}
            }
		}
		return true;
	}

	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		mEdorNo = mLPEdorItemSchema.getEdorNo();
		mEdorType = mLPEdorItemSchema.getEdorType();
		mContNo = mLPEdorItemSchema.getContNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		String sql = "select * from LCPol a where ContNo = '" + mLPEdorItemSchema.getContNo() + "' " + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '"
				+ BQ.RISKTYPE4_BONUS + "') " + "with ur";
		System.out.println("查询分红险种是否存在：" + sql);
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolSet = tLCPolDB.executeQuery(sql);
		if (tLCPolSet.size() != 1) {
			mErrors.addOneError("找不到分红险种信息！");
			return false;
		} else {
			mPolNo = tLCPolSet.get(1).getPolNo();
			mRiskCode = tLCPolSet.get(1).getRiskCode();
			mBonusGetMode = tLCPolSet.get(1).getBonusGetMode();
		}
		mLCPolSchema=tLCPolSet.get(1).getSchema();
//		mBonusGetMode = mLCPolSchema.getBonusGetMode();
		
		
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		
		LJSGetEndorseSet tempLJSGetEndorseSet = new LJSGetEndorseSet();
		//计算红利利率
		if (mBonusGetMode.equals(BQ.BONUSGET_GETINTEREST)){
			if (!DealInterest()){
				return false;
			}
		}
		//计算满期金
		if(!DealMJMoney()){
			return false;
		}
		//计算还款金额
		if(!DealLoan()){
			return false;
		}
		setEdorItem();
		return true;
	}

	/**
	 * 处理累积生息帐户利息
	 * 
	 * @return boolean
	 */
	private boolean DealInterest() {
		mLCInsureAccSchema = CommonBL.getLCInsureAcc(mPolNo, CommonBL.getInsuAccNo("005", mRiskCode));
		if (mLCInsureAccSchema == null) {
			System.out.println(mPolNo + "分红帐户尚未建立，无需领钱!");
			return true;
		}
		if (mLCInsureAccSchema.getInsuAccBala() == 0) {
			System.out.println(mPolNo + "分红帐户余额为0!");
			return true;
		}
		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		tLCInsureAccClassDB.setPolNo(mPolNo);
		tLCInsureAccClassDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
		mLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
		if (mLCInsureAccClassSchema == null) {
			mErrors.addOneError("找不到分红账户分类信息！");
			return false;
		}
		// 将原账户清0
		setLPInsureAcc();
		setLPInsureAccClass();

		// 计算出的利息金额
		double interestmoney = getinterest();

		if (interestmoney == -1) {
			// @@错误处理
			System.out.println("PEdorHAAppConfirmBL+DealInterest++--");
			CError tError = new CError();
			tError.moduleName = "PEdorHAAppConfirmBL";
			tError.functionName = "DealInterest";
			tError.errorMessage = "计算累积生息帐户利息出错!";
			mErrors.addOneError(tError);
			return false;
		}

		interestmoney = CommonBL.carry(interestmoney);
		mGetMoney = CommonBL.carry(mLCInsureAccSchema.getInsuAccBala() + interestmoney);
		mGetMoney=-Math.abs(mGetMoney);
		// 设置轨迹表,一条本次利息,一条清空轨迹
		setLPInsureAccTrace("LX", interestmoney);
		setLPInsureAccTrace("HL", mGetMoney);
		
		// 设置保全收付费表
		setGetEndorse();
		return true;
	}

	private double getinterest() {
		if(!getcalcode()){
			return -1;
		}
/*    	校验是否存在累积生息帐户结算表,如果不存在则生成,并且同时设置baladate为第一笔非0数据进账户的时间.
    	这里会有一个数据库提交,提交失败则返回false
   */
        if (!HaveBalance())
        {
        	// @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "生成帐户结算轨迹表失败!";
			mErrors.addOneError(tError);
			return -1;
        }
//    	仅仅用LMInsuAccRate来储存每一段结算区间
		LMInsuAccRateSchema tLMInsuAccRateSchema =new LMInsuAccRateSchema();
    	tLMInsuAccRateSchema.setStartBalaDate(mLCInsureAccSchema.getBalaDate());
		tLMInsuAccRateSchema.setBalaDate(mLCPolSchema.getEndDate());
//    	若区间经过保单周年日,则进行拆分
    	LMInsuAccRateSet tLMInsuAccRateSet=getRateSet(tLMInsuAccRateSchema);
//    	此处不用4舍5入
    	double tInterest=getinterest(tLMInsuAccRateSet);
    	return tInterest;
	}
	
    /**
     * 处理帐户结算表初步生成
     * !!!!!!这里有分步提交数据库!!!!!!
     * @return boolean
     */
    private boolean HaveBalance()
    {
    	String Haveacc = mExeSQL.getOneValue("select 1 from lcinsureaccbalance where polno='"+mPolNo+"' fetch first 1 row only with ur");
    	
    	if(Haveacc!=null&&Haveacc.equals("1")){
    		System.out.println("已经生成帐户了!");
    		return true;
    	}
    	String tbaladate=mExeSQL.getOneValue("select min(paydate) from lcinsureacctrace where polno='"+mPolNo+"' and money<>0 with ur");
    	if(tbaladate==null||tbaladate.equals("")||tbaladate.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取最早结算日期出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	String tmoney=mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='"+mPolNo+"' and paydate<='"+tbaladate+"' with ur");
    	if(tmoney==null||tmoney.equals("")||tmoney.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取帐户金额出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	
    	VData tVData = new VData();
    	MMap tMMap =new MMap();
    	
    	tMMap.put("update lcinsureacc set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);
    	tMMap.put("update lcinsureaccclass set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);

        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        new Reflections().transFields(schema, mLCInsureAccSchema);
        schema.setSequenceNo(mLCInsureAccSchema.getPolNo()+ mLCInsureAccSchema.getInsuAccNo());
        schema.setBalaCount(0);
        schema.setDueBalaDate(tbaladate);
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(schema.getMakeDate());
        schema.setRunTime(schema.getMakeTime());
        schema.setPolYear(1);
        schema.setPolMonth(1);
        schema.setInsuAccBalaBefore(tmoney);
        schema.setInsuAccBalaAfter(tmoney);
        schema.setInsuAccBalaGurat(tmoney);
        schema.setPrintCount(0);
        tMMap.put(schema,SysConst.INSERT);

        tVData.add(tMMap);
        //计算并填充账户结构
        if (tVData == null)
        {
        	// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "创建账户结构失败!";
			mErrors.addOneError(tError);
			return false;
        }
//      执行业务数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(tVData, ""))
        {
        	// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "帐户结算创建数据提交失败!";
			mErrors.addOneError(tError);
			return false;
        }
//		提交完成后置全局变量结算日期
        mLCInsureAccSchema.setBalaDate(tbaladate);
    	return true;    	
    }

	// 计算利息
	private double getinterest(LMInsuAccRateSet tLMInsuAccRateSet) {
		String rate1 = GetRate(tLMInsuAccRateSet.get(1).getStartBalaDate());
		double interest = 0;
		if (rate1 == null || rate1.equals("") || rate1.equals("null")) {
			return -1;
		}
		String lastriskbonus = mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='" + mPolNo + "' and paydate<='" + tLMInsuAccRateSet.get(1).getStartBalaDate() + "'");
		if (lastriskbonus == null || lastriskbonus.equals("") || lastriskbonus.equals("null")) {
			return -1;
		}
		// 当前失效,失效日期在结算之前
		String tSXDate = mExeSQL.getOneValue("select startdate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and startdate<'"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and StateReason!='02' " + " and (enddate is null or enddate='' ) " + " union all " + " select makedate from lccontstate "
				+ " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and makedate<'" + tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and StateReason='02' "
				+ " and (enddate is null or enddate='' ) ");
		if (tSXDate != null && (!tSXDate.equals("")) && (!tSXDate.equals("null"))) {
			return 0;
		}
		// 当前失效,失效日期在第一个结算区间
		tSXDate = mExeSQL.getOneValue("select makedate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and makedate>='"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and makedate<'" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason='02' " + " and (enddate is null or enddate='' ) "
				+ " union all " + " select startdate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and startdate>='"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and startdate<'" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason!='02' " + " and (enddate is null or enddate='' ) ");
		if (tSXDate != null && (!tSXDate.equals("")) && (!tSXDate.equals("null"))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			return interest;
		}
		if (tLMInsuAccRateSet.size() == 1) {
			try {
				LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
				interest = getAccInterset(rate1, lastriskbonus, ttLMInsuAccRateSet);
				return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return -1;
			}
		} else if (tLMInsuAccRateSet.size() == 2) {
			String rate2 = GetRate(PubFun.calDate(tLMInsuAccRateSet.get(2).getBalaDate(), -1, "D", null));
			try {
				LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
				double interest1 = getAccInterset(rate1, lastriskbonus, ttLMInsuAccRateSet);
				ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(2));
				lastriskbonus = mExeSQL.getOneValue("select sum(money)+" + interest1 + " from lcinsureacctrace where polno='" + mPolNo + "' and paydate<='"
						+ tLMInsuAccRateSet.get(2).getStartBalaDate() + "'");
				if (lastriskbonus == null || lastriskbonus.equals("") || lastriskbonus.equals("null")) {
					return -1;
				}
				double interest2 = getAccInterset(rate2, lastriskbonus, ttLMInsuAccRateSet);
				interest = interest1 + interest2;
				return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return -1;
			}
		} else {
			return -1;
		}
	}

	// 返回该日期对应的利率
	public String GetRate(String tdate) {
		String tPolYear = String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(), tdate, "Y"));
		String trate = mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='" + mPolNo
				+ "' " + " and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+" + tPolYear + "-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+" + tPolYear
				+ ")))) ");
		return trate;
	}

	// 获取各类算法的calcode以备使用
	public boolean getcalcode() {
		LMRiskBonusDB tLMRiskBonusDB = new LMRiskBonusDB();
		tLMRiskBonusDB.setRiskCode(mLCPolSchema.getRiskCode());
		if (!tLMRiskBonusDB.getInfo()) {
			return false;
		}
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "YX");
		YXCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "ZZ");
		ZZCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "FX");
		FXCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "JY");
		JYLPCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "MQ");
		MQCalCode = tCalculator.calculate();
		return true;
	}

	private double getAccInterset(String rate1, String lastriskbonus, LMInsuAccRateSet tLMInsuAccRateSet) {
		double interest = 0;
		String tSXDate1 = mExeSQL.getOneValue("select min(startdate) from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='0' " + " and startdate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason!='02' " + " and enddate is not null and enddate!='' "
				+ " union all " + " select min(makedate) from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='0' " + " and makedate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason='02' " + " and enddate is not null and enddate!='' ");
		String tFXDate1 = mExeSQL.getOneValue("select max(enddate) from lccontstate where state='0' and polno='" + mPolNo + "' and statetype='Available' and enddate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' ");
		if ((tSXDate1 == null || tSXDate1.equals("") || tSXDate1.equals("null")) && (tFXDate1 == null || tFXDate1.equals("") || tFXDate1.equals("null"))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(YXCalCode);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
			System.out.println(tCalculator.calculate());
			System.out.println(lastriskbonus);
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if (tSXDate1.equals("") && (!tFXDate1.equals(""))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(FXCalCode);
			tCalculator.addBasicFactor("reinstatedate", tFXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if (tFXDate1.equals("") && (!tSXDate1.equals(""))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate1);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if ((!tFXDate1.equals("")) && (!tSXDate1.equals(""))) {
			if (PubFun.calInterval(tSXDate1, tFXDate1, "D") < 0) {
				// 复效日期小于失效日期，按正常来计算，重新设置结算区间
				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(YXCalCode);
				tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
				tCalculator.addBasicFactor("rate", rate1);
				tCalculator.addBasicFactor("lastaccountdate", tFXDate1);
				tCalculator.addBasicFactor("accountdate", tSXDate1);
				interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			} else {
				// 复效日期大于失效日期，分两段来计算
				Calculator tCalculatorzz = new Calculator();
				tCalculatorzz.setCalCode(YXCalCode);
				tCalculatorzz.addBasicFactor("lastriskbonus", lastriskbonus);
				tCalculatorzz.addBasicFactor("rate", rate1);
				tCalculatorzz.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
				tCalculatorzz.addBasicFactor("accountdate", tSXDate1);
				// 第二段复效的
				Calculator tCalculatorfx = new Calculator();
				tCalculatorfx.setCalCode(YXCalCode);
				tCalculatorfx.addBasicFactor("lastriskbonus", tCalculatorzz.calculate());
				tCalculatorfx.addBasicFactor("rate", rate1);
				tCalculatorfx.addBasicFactor("lastaccountdate", tFXDate1);
				tCalculatorfx.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
				interest = Double.parseDouble(tCalculatorfx.calculate()) - Double.parseDouble(lastriskbonus);
			}
		}
		return interest;

	}

	// 拆分结算月度经过保单周年日的情况
	private LMInsuAccRateSet getRateSet(LMInsuAccRateSchema tLMInsuAccRateSchema) {
		LMInsuAccRateSet tLMInsuAccRateSet = new LMInsuAccRateSet();
		int i = 1;
		boolean haveDate = false;
		do {
			// 结算开始日期减去该次保单周年日的日期
			int bigthanstart = PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getStartBalaDate(), "D");
			// 结算结束日期减去该次保单周年日的日期
			int bigthanend = PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getBalaDate(), "D");
			if (bigthanstart > 0) {
				i++;
			} else if (bigthanstart == 0) {
				break;
			} else if (bigthanstart < 0 && bigthanend > 0) {
				haveDate = true;
				break;
			} else if (bigthanend <= 0) {
				break;
			}
		} while (PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getBalaDate(), "D") > 0);
		if (haveDate) {
			String midDate = PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null);
			LMInsuAccRateSchema tLM1 = new LMInsuAccRateSchema();
			tLM1.setStartBalaDate(tLMInsuAccRateSchema.getStartBalaDate());
			tLM1.setBalaDate(midDate);
			LMInsuAccRateSchema tLM2 = new LMInsuAccRateSchema();
			tLM2.setStartBalaDate(midDate);
			tLM2.setBalaDate(tLMInsuAccRateSchema.getBalaDate());
			tLMInsuAccRateSet.add(tLM1);
			tLMInsuAccRateSet.add(tLM2);
		} else {
			tLMInsuAccRateSet = new LMInsuAccRateSet();
			tLMInsuAccRateSet.add(tLMInsuAccRateSchema);
		}
		return tLMInsuAccRateSet;
	}

	private void setLPInsureAcc() {
		LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPInsureAccSchema, mLCInsureAccClassSchema);
		tLPInsureAccSchema.setEdorNo(mEdorNo);
		tLPInsureAccSchema.setEdorType(mEdorType);
		tLPInsureAccSchema.setInsuAccBala(0);
		tLPInsureAccSchema.setBalaDate(mLCPolSchema.getEndDate());
		tLPInsureAccSchema.setPrtNo(mLCInsureAccSchema.getPrtNo());
		tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccSchema.setModifyDate(mCurrentDate);
		tLPInsureAccSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
	}

	/**
	 * 设置帐户领取轨迹
	 * 
	 * @param aLCInsureAccClassSchema
	 *            LCInsureAccClassSchema
	 * @param grpMoney
	 *            double
	 */
	private void setLPInsureAccTrace(String tMoneyType, double tMoney) {
		String serialNo;
		LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
		tLPInsureAccTraceDB.setEdorNo(mEdorNo);
		tLPInsureAccTraceDB.setEdorType(mEdorType);
		tLPInsureAccTraceDB.setContNo(mContNo);
		tLPInsureAccTraceDB.setPolNo(mPolNo);
		tLPInsureAccTraceDB.setInsuAccNo(mLCInsureAccClassSchema.getInsuAccNo());
		tLPInsureAccTraceDB.setMoneyType(tMoneyType);
		LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
		if (tLPInsureAccTraceSet.size() > 0) {
			serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
			String sql = "delete from LPInsureAccTrace " + "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '" + mEdorType + "' " + "and SerialNo = '" + serialNo + "' ";
			mMap.put(sql, "DELETE");
		} else {
			serialNo = PubFun1.CreateMaxNo("SERIALNO", mLCInsureAccClassSchema.getManageCom());
		}
		LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
		tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
		tLPInsureAccTraceSchema.setEdorType(mEdorType);
		tLPInsureAccTraceSchema.setGrpContNo(mLCInsureAccClassSchema.getGrpContNo());
		tLPInsureAccTraceSchema.setGrpPolNo(mLCInsureAccClassSchema.getGrpPolNo());
		tLPInsureAccTraceSchema.setContNo(mLCInsureAccClassSchema.getContNo());
		tLPInsureAccTraceSchema.setPolNo(mLCInsureAccClassSchema.getPolNo());
		tLPInsureAccTraceSchema.setSerialNo(serialNo);
		tLPInsureAccTraceSchema.setInsuAccNo(mLCInsureAccClassSchema.getInsuAccNo());
		tLPInsureAccTraceSchema.setRiskCode(mLCInsureAccClassSchema.getRiskCode());
		tLPInsureAccTraceSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
		tLPInsureAccTraceSchema.setManageCom(mLCInsureAccClassSchema.getManageCom());
		tLPInsureAccTraceSchema.setAccAscription("0");
		tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
		tLPInsureAccTraceSchema.setMoney(tMoney); // 退费
		tLPInsureAccTraceSchema.setUnitCount("0");
		tLPInsureAccTraceSchema.setPayDate(mLCPolSchema.getEndDate());
		tLPInsureAccTraceSchema.setState("0");
		tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
		tLPInsureAccTraceSchema.setOtherType("10");
		tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
		tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
		tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
		tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
	}

	private void setLPInsureAccClass() {
		LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPInsureAccClassSchema, mLCInsureAccClassSchema);
		tLPInsureAccClassSchema.setEdorNo(mEdorNo);
		tLPInsureAccClassSchema.setEdorType(mEdorType);
		tLPInsureAccClassSchema.setInsuAccBala(0);
		tLPInsureAccClassSchema.setBalaDate(mLCPolSchema.getEndDate());
		tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
		tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
		tLPInsureAccClassSchema.setAccAscription("0");
		mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
	}

	/**
	 * 设置批改补退费表
	 * 
	 * @return boolean
	 */
	private boolean setGetEndorse() {
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); // 给付通知书号码，没意义
		tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
		tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
		tLJSGetEndorseSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
		tLJSGetEndorseSchema.setContNo(mLCPolSchema.getContNo());
		tLJSGetEndorseSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
		tLJSGetEndorseSchema.setPolNo(mLCPolSchema.getPolNo());
		tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
		tLJSGetEndorseSchema.setGetMoney(mGetMoney);
		tLJSGetEndorseSchema.setRiskCode(mLCPolSchema.getRiskCode());
		tLJSGetEndorseSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
		tLJSGetEndorseSchema.setAgentCom(mLCPolSchema.getAgentCom());
		tLJSGetEndorseSchema.setAgentType(mLCPolSchema.getAgentType());
		tLJSGetEndorseSchema.setAgentCode(mLCPolSchema.getAgentCode());
		tLJSGetEndorseSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
		tLJSGetEndorseSchema.setApproveCode(mLCPolSchema.getApproveCode());
		tLJSGetEndorseSchema.setApproveDate(mLCPolSchema.getApproveDate());
		tLJSGetEndorseSchema.setApproveTime(mLCPolSchema.getApproveTime());
		tLJSGetEndorseSchema.setFeeFinaType("HL");// 红利累积生息红利领出
		tLJSGetEndorseSchema.setAppntNo(mLCPolSchema.getAppntNo());
		tLJSGetEndorseSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
		tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
		tLJSGetEndorseSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
		tLJSGetEndorseSchema.setOtherNo(mEdorNo);
		tLJSGetEndorseSchema.setOtherNoType("10");
		tLJSGetEndorseSchema.setGetFlag("0");
		tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
		tLJSGetEndorseSchema.setManageCom(mLCPolSchema.getManageCom());
		tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
		mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
		return true;
	}
	
	private boolean DealMJMoney(){
		String strFH = " select 1 from lcpol where contno='"+ mLCPolSchema.getContNo() + "'  " 
					  +" and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur";
		String haveFH = mExeSQL.getOneValue(strFH);
		if (null != haveFH && !"".equals(haveFH)) {
			System.out.println("*************开始处理分红险保单满期金*************");
			//备份LPGet
			String transPget = "INSERT INTO LPGET ( SELECT '"
					+ mLPEdorItemSchema.getEdorNo() + "','"
					+ mLPEdorItemSchema.getEdorType() + "',LCGET.* "
					+ " FROM LCGET WHERE CONTNO='"
					+ mLPEdorItemSchema.getContNo() + "' )";
			String deleteLPget = "Delete From Lpget Where Edorno='"
					+ mLPEdorItemSchema.getEdorNo() + "' and ContNo='"
					+ mLPEdorItemSchema.getContNo() + "' ";

			//备份LPPol
			String transPPol = "INSERT INTO LPpol ( SELECT '"
					+ mLPEdorItemSchema.getEdorNo() + "','"
					+ mLPEdorItemSchema.getEdorType() + "',LCpol.* "
					+ " FROM LCPol WHERE CONTNO='"
					+ mLPEdorItemSchema.getContNo() + "' )";
			String deleteLPPol = "Delete From Lppol Where Edorno='"
					+ mLPEdorItemSchema.getEdorNo() + "' and ContNo='"
					+ mLPEdorItemSchema.getContNo() + "' ";
			MMap tMmap = new MMap();
			tMmap.put(deleteLPget, SysConst.DELETE);// 先删除历史数据
			tMmap.put(transPget, SysConst.INSERT);
			tMmap.put(deleteLPPol, SysConst.DELETE);// 先删除历史数据
			tMmap.put(transPPol, SysConst.INSERT);
			try {
				VData mVdata = new VData();
				mVdata.add(tMmap);
				PubSubmit ttPubSubmit = new PubSubmit();// 立即提交
				ttPubSubmit.submitData(mVdata, "");
			} catch (Exception e) {
				System.out.println("金生无忧保单满期金处理失败");
				CError tError = new CError();
				tError.moduleName = "PEdorCTAppConfirmBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "满期金处理失败";
				mErrors.addOneError(tError);
				return false;
			}
			LJSGetEndorseSet ebLJSGetEndorseSet = calBenefitGet();
			mLJSGetEndorseSet.add(ebLJSGetEndorseSet);
			for (int i = 1; i <= ebLJSGetEndorseSet.size(); i++) {
				mGetMJMoney += ebLJSGetEndorseSet.get(i).getGetMoney();
			}
			mMap.put(mLJSGetEndorseSet,SysConst.DELETE_AND_INSERT);
		}

		return true;
	}
	
	private LJSGetEndorseSet calBenefitGet(){
    	LJSGetEndorseSet getLJSGetEndorseSet=new LJSGetEndorseSet();
		String contNo = mLPEdorItemSchema.getContNo();
		String edorNo = mLPEdorItemSchema.getEdorNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		// 通过LPGET表计算保单未领取的满期金及利息
		String sql = "select distinct insuredno from lpget a,lmdutygetalive b "
				+ " where a.getdutycode = b.getdutycode and contno ='"+ contNo+"'"
				+ " and a.gettodate<='"+ mEdorValiDate+ "'"
				+ " and a.edorno='"+ edorNo+ "'"
				+ " and ((a.getdutykind ='0' and (a.gettodate<=a.getenddate or a.getenddate is null)"
				+ " and not exists (select 1 from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)))"
//				+ ") or( a.getdutykind !='0' and"
//				+ " ((a.gettodate<a.getenddate) or ((select max(curgettodate) from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)<=a.getenddate))))"
				+ " with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String insuredNo = tSSRS.GetText(i, 1);
			LPGetSet tLPGetSet = getLCGetNeedGet(edorNo, contNo, insuredNo);
			
			String strPol="select * from lppol where edorno='"+edorNo+"' and contno='"+contNo+"' and insuredno='"+insuredNo+"' and riskcode in (select riskcode from lmriskapp where risktype4='2')";
			LPPolSet getLPPolSet=new LPPolDB().executeQuery(strPol);
			LPPolSchema getLPPolSchema=getLPPolSet.get(1);
			double sumGetMoney = 0; // 总领取金额
//			double sumLX = 0; // 总领取金额
//			while (tLPGetSet.size() > 0) {
				for (int j = 1; j <= tLPGetSet.size(); j++) {
					// 获取合同下的每条给付的明细信息
					LPGetSchema tLPGetSchema = tLPGetSet.get(j).getSchema();

					String birthdaySQL = "select birthday from lcinsured where contno ='"
							+ contNo+ "'"+" and insuredno='"+ insuredNo+ "' ";
					String mBirthday = mExeSQL.getOneValue(birthdaySQL);
					String tAppntAge = String.valueOf(PubFun.calInterval(
							mBirthday, tLPGetSchema.getGettoDate(), "Y"));
					Calculator tCalculator = new Calculator();
					tCalculator.addBasicFactor("AppntAge", tAppntAge);
					tCalculator.addBasicFactor("Age", tAppntAge);
					LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

					tLMDutyGetaLiveDB.setGetDutyCode(tLPGetSchema
							.getGetDutyCode());
					LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB
							.query();
					LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet
							.get(1);
					String pCalCode = "";
					if (tLMDutyGetAliveSchema.getCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCalCode();
					}
					if (tLMDutyGetAliveSchema.getCnterCalCode() != null
							&& !"".equals(tLMDutyGetAliveSchema
									.getCnterCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
					}
					if (tLMDutyGetAliveSchema.getOthCalCode() != null
							&& !""
									.equals(tLMDutyGetAliveSchema
											.getOthCalCode())) {
						pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
					}
					tCalculator.setCalCode(pCalCode);

					// 责任保额
					String strAmnt = "select sum(amnt) from lcduty where polno='"
							+ tLPGetSchema.getPolNo()
							+ "' and dutycode='"
							+ tLPGetSchema.getDutyCode() + "'";
					String amnt = mExeSQL.getOneValue(strAmnt);


					// 判断主险是否出险,满期给付责任在主险。
					if (getPolAttribute(tLPGetSchema.getPolNo())
							&& tLMDutyGetAliveSchema.getDiscntFlag()
									.equals("0")) {
						// 得到该险种的附加险种
						String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"
								+ tLPGetSchema.getPolNo()
								+ "')  and codetype='checkappendrisk' ";
						String annexRisk = mExeSQL.getOneValue(annexPolNoSQL);
						String annexPolNo = "select polno from lcpol where contno='"
								+ tLPGetSchema.getContNo()
								+ "' and riskcode ='" + annexRisk + "'";
						if (annexRisk.equals("") && annexRisk == "") {
							System.out.println("没有得到附加险信息。");
							continue;
						}
						// 1."如果附加的有LP就不做给付";
						String sqlcliam = "select polNo From ljagetclaim where polno ='"
								+ new ExeSQL().getOneValue(annexPolNo) + "'";
						String cliam = mExeSQL.getOneValue(sqlcliam);
						if (!cliam.equals("") && cliam != "" && cliam != "null") {
							System.out.println("附加险已经出险，不能做满期给付。");
							continue;
						}
						// 2."主险出现了并且是死亡责任的不做给付"
						String mainSQL = "select polNo From ljagetclaim where polno ='"
								+ tLPGetSchema.getPolNo()
								+ "'"
								+ " and getdutykind in('501','502','503','504') ";
						String main = mExeSQL.getOneValue(mainSQL);
						if (!main.equals("") && main != "null") {
							System.out.println("主险责任出现理赔，并且是死亡责任，不能满期给付。");
							continue;
						}
						// 3.如果满期金方式的责任出险，不做给付。
						if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
							String SQL = "select polNo From ljagetclaim where polno ='"
									+ tLPGetSchema.getPolNo() + "'";
							String polSLQ = mExeSQL.getOneValue(SQL);
							if (!polSLQ.equals("") && polSLQ != "null") {
								System.out.println("满期责任已经出险，不做给付。");
								continue;
							}
						}
					}
					
					String payIntv=String.valueOf(mLCPolSchema.getPayIntv());
					tCalculator.addBasicFactor("Amnt", amnt);
					tCalculator.addBasicFactor("PolNo", tLPGetSchema.getPolNo());
					tCalculator.addBasicFactor("PayIntv", payIntv);
					String tStr = tCalculator.calculate();
					double tValue = 0;
					if (tStr == null || tStr.trim().equals("")) {
						tValue = 0;
					} else {
						tValue = Double.parseDouble(tStr);
					}
					sumGetMoney += tValue;
				}
//				VData mVData=new VData();
//				mVData.add(tMMap);
//				PubSubmit mPubSubmit=new PubSubmit();
//				mPubSubmit.submitData(mVData, "");
//				if(mPubSubmit.mErrors.needDealError()){
//					System.out.println("更新满期给付责任数据是发生错误");
//					mErrors.addOneError(mPubSubmit.mErrors.getFirstError());
//					return null;
//				}
//				//获取最新的给付责任数据
//				tLPGetSet = getLCGetNeedGet(edorNo, contNo, insuredNo,false);
//			}
			sumGetMoney=PubFun.setPrecision(sumGetMoney, "0.00");
			
//			sumLX=PubFun.setPrecision(sumLX, "0.00");
			BqCalBL getCalBL=new BqCalBL();
			LJSGetEndorseSchema getLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EB",-sumGetMoney,mGlobalInput);
//			LJSGetEndorseSchema getLxLS=getCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),getLPPolSchema,null,"EBLX",-sumLX,mGlobalInput);
			getLJSGetEndorseSet.add(getLS);
//			getLJSGetEndorseSet.add(getLxLS);
		}
		return getLJSGetEndorseSet;
	
		
	}

	
    private LPGetSet getLCGetNeedGet(String edorNo,String contNo, String insuredNo) {
        String tSBql = " select b.* from lppol a,lpget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.edorno = b.edorno "
                        + " and a.edortype = b.edortype "
                        + " and a.edorno = '"+edorNo+"' "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and b.getdutykind ='0' "
                        + " and a.riskcode in (select riskcode from LMRiskApp where  RiskType4 = '2') "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
                        + " and ( (b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null))) "
                        + " and b.gettodate <= '"+mEdorValiDate+"'"
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur ";

        LPGetSet tLPGetSet = new LPGetSet();
        LPGetDB tLPGetDB = new LPGetDB();
        tLPGetSet = tLPGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLPGetSet.size() == 0) {
            System.out.println("保单号为：" + contNo + "，没有查询到可给付的满期金");
            return new LPGetSet();
        }
        return tLPGetSet;
    }
    
    
    
    /**
     * 判断给付责任的险种是主险还是附加险
     * 主险返回: true;
     * 附加返回：false;
     * @param polNo String
     * @return boolean
     */

    private boolean getPolAttribute(String polNo)
    {
        String polSQL = " select subriskflag from lcpol a,lmriskapp b where  a.riskcode = b.riskcode and a.polno ='"+polNo+"'";
        SSRS tSSRS = mExeSQL.execSQL(polSQL);

        if (tSSRS.getMaxRow()==0)
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getPolAttribute";
            tError.errorMessage = "获得主险信息描述失败。";
            mErrors.addOneError(tError);
            return false;
        }
        // M-- 主险
        // S-- 附加险
        if (tSSRS.GetText(1,1).equals("M"))
        {
            return true ;
        }
        else{
            return false ;
        }
    }
    
    private boolean DealLoan(){
        if(IsLoan){
//        	贷款利息
        	double tInterest=0;
            LCPolDB tLCLoanPolDB = new LCPolDB();
            tLCLoanPolDB.setPolNo(mLPLoanSchema.getPolNo());
            tLCLoanPolDB.getInfo();
            LCPolSchema tLCLoanPolSchema = tLCLoanPolDB.getSchema();
            LJSGetEndorseSchema tLoanLJS = new LJSGetEndorseSchema();
            LJSGetEndorseSchema tLoanLXLJS = new LJSGetEndorseSchema();
            BqCalBase tLoanBqCalBase = new BqCalBase();
            BqCalBL tLoanBqCalBL = new BqCalBL(tLoanBqCalBase, "", "");
            tLoanLJS=tLoanBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
            		tLCLoanPolSchema,
                    null,
                    "RFBF",
                    mLPLoanSchema.getSumMoney(),
                    mGlobalInput);
            if(IsLoanAbate){
            	tInterest=Arith.round(-mLPLoanSchema.getSumMoney() ,2);
            	
            }else{
            	//查询保单信息  --wujun
            	LCContDB tLCContDB = new LCContDB();
            	tLCContDB.setContNo(tLCLoanPolSchema.getContNo());
            	tLCContDB.getInfo();
            	LCContSchema tLCContSchema = tLCContDB.getSchema();
//    			获取贷款利息 
	    		AccountManage tAccountManage=new AccountManage();
	    		tInterest=CommonBL.carry(tAccountManage.getLoanInterest(mLPLoanSchema.getLoanDate(), mLPLoanSchema.getSumMoney(),tLCContSchema.getManageCom(),tLCContSchema.getPrem()+"", mLPEdorItemSchema.getEdorValiDate(), tLCLoanPolSchema.getRiskCode()));
	    		if(tInterest==-1){
	    			// @@错误处理
					System.out.println("PEdorHAAppConfirmBL+DealLoan++--");
					CError tError = new CError();
					tError.moduleName = "PEdorHAAppConfirmBL";
					tError.functionName = "prepareData";
					tError.errorMessage = "贷款利息计算失败!";
					mErrors.addOneError(tError);
					return false;
	    		}
//	    		如果解约金额小于贷款本息合计,则阻断保全理算
//	    		if((tInterest+mLPLoanSchema.getSumMoney()+aTotalReturn)>0){
//	    			// @@错误处理
//					System.out.println("PEdorCTAppConfirmBL+prepareData++--");
//					CError tError = new CError();
//					tError.moduleName = "PEdorCTAppConfirmBL";
//					tError.functionName = "prepareData";
//					tError.errorMessage = "贷款本息合计大于解约所退保费!";
//					mErrors.addOneError(tError);
//					return false;
//	    		}
	    		mLoanMoney=Arith.round(tInterest+mLPLoanSchema.getSumMoney() ,2);
            }
            tLoanLXLJS=tLoanBqCalBL.initLJSGetEndorse(mLPEdorItemSchema.getSchema(),
            		tLCLoanPolSchema,
                    null,
                    "RFLX",
                    tInterest,
                    mGlobalInput);
            mLJSGetEndorseSet.add(tLoanLJS);
            mLJSGetEndorseSet.add(tLoanLXLJS);
            
            mMap.put(mLJSGetEndorseSet, "DELETE&INSERT");
            //已经还款
    		mLPLoanSchema.setPayOffFlag("1");
    		mLPLoanSchema.setPayOffDate(mLPEdorItemSchema.getEdorValiDate());
            mLPLoanSchema.setModifyDate(mCurrentDate);
            mLPLoanSchema.setModifyTime(mCurrentTime);

            mMap.put(mLPLoanSchema, "DELETE&INSERT");
            
//          设置还款信息
//   		 生成“保全还款业务表”数据-------------------->
    		// 记录还款轨迹
    		mLPReturnLoanSchema.setPolNo(mLPLoanSchema.getPolNo());
    		mLPReturnLoanSchema.setEdorType(mLPLoanSchema.getEdorType());
    		mLPReturnLoanSchema.setSerialNo(mLPLoanSchema.getSerialNo()); // 是否生成？
    		mLPReturnLoanSchema.setActuGetNo(mLPLoanSchema.getActuGetNo());
    		mLPReturnLoanSchema.setLoanType(mLPLoanSchema.getLoanType());
    		mLPReturnLoanSchema.setOrderNo(mLPLoanSchema.getOrderNo());
    		mLPReturnLoanSchema.setLoanDate(mLPLoanSchema.getLoanDate());
//    		计算利息截止到前一天
    		mLPReturnLoanSchema.setPayOffDate(mLPEdorItemSchema.getEdorValiDate());
    		mLPReturnLoanSchema.setSumMoney(mLPLoanSchema.getSumMoney());
    		mLPReturnLoanSchema.setInputFlag(mLPLoanSchema.getInputFlag());
    		mLPReturnLoanSchema.setInterestType(mLPLoanSchema.getInterestType());
    		mLPReturnLoanSchema.setInterestRate(mLPLoanSchema.getInterestRate());
    		mLPReturnLoanSchema.setInterestMode(mLPLoanSchema.getInterestMode());
    		mLPReturnLoanSchema.setRateCalType(mLPLoanSchema.getRateCalType());
    		mLPReturnLoanSchema.setRateCalCode(mLPLoanSchema.getRateCalCode());
    		mLPReturnLoanSchema.setSpecifyRate(mLPLoanSchema.getSpecifyRate());
    		mLPReturnLoanSchema.setLeaveMoney("0");
    		mLPReturnLoanSchema.setPayOffFlag("1");
    		mLPReturnLoanSchema.setOperator(this.mGlobalInput.Operator);
    		mLPReturnLoanSchema.setMakeDate(mCurrentDate);
    		mLPReturnLoanSchema.setMakeTime(mCurrentTime);
    		mLPReturnLoanSchema.setModifyDate(mCurrentDate);
    		mLPReturnLoanSchema.setModifyTime(mCurrentTime);
    		mLPReturnLoanSchema.setEdorNo(mLPLoanSchema.getActuGetNo());//还款批单号
    		mLPReturnLoanSchema.setLoanNo(mLPLoanSchema.getEdorNo()); // 借款批单号
    		mLPReturnLoanSchema.setReturnMoney(mLPLoanSchema.getSumMoney());
    		mLPReturnLoanSchema.setReturnInterest(tInterest);
    		mMap.put(mLPReturnLoanSchema, "DELETE&INSERT");
        }
    	return true;
    }
    
	/**
	 * 设置item表中的费用和状态
	 */
	private void setEdorItem() {

		mTotalGetMoney=CommonBL.carry(mGetMoney+mGetMJMoney+mLoanMoney);
		System.out.println(mTotalGetMoney);
		String sql = "update LPEdorItem " + "set GetMoney = " + mTotalGetMoney + ", "
				+ // 这里是负数
				"EdorState = '" + BQ.EDORSTATE_CAL + "',polno='"+mPolNo+"', " + "ModifyDate = '" + mCurrentDate + "', " + "ModifyTime = '" + mCurrentTime + "' "
				+ "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '" + mEdorType + "' ";
		mMap.put(sql.toString(), "UPDATE");
	}
}
