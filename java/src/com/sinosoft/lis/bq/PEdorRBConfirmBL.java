package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCInsureAccBalanceDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LPInsuredDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.db.LPContDB;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.db.LPPremDB;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.db.LPGetDB;
import com.sinosoft.lis.vschema.LPGetSet;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LPGetSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LPDutyDB;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LPAppntSet;
import com.sinosoft.lis.db.LPAppntDB;
import com.sinosoft.lis.schema.LPAppntSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LPInsuredSet;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.db.LPBnfDB;
import com.sinosoft.lis.schema.LPBnfSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PEdorRBConfirmBL implements EdorConfirm {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private LPEdorItemSchema mLPEdorItemSchema = null;
    private ExeSQL mExeSQL = null;
    private LCPolSet tLCPolSet = new LCPolSet();
    private LCPremSet tLCPremSet = new LCPremSet();
    private LCGetSet tLCGetSet = new LCGetSet();
    private LCDutySet tLCDutySet = new LCDutySet();
    private LCAppntSet tLCAppntSet = new LCAppntSet();
    private LCInsuredSet tLCInsuredSet = new LCInsuredSet();
    private LCBnfSet tLCBnfSet = new LCBnfSet();
    private LCContSet tLCContSet = new LCContSet();
    StringBuffer sql = new StringBuffer();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private MMap map = new MMap();
    private String EdorNo = "";
    private String oldEdorNo ;

    public PEdorRBConfirmBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!prepareData()) {
            return false;
        }

        return true;
    }

    public VData getResult() {
        VData tVData = new VData();
        tVData.add(map);
        return tVData;

    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void getInputData(VData cInputData) {
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    }

    private boolean prepareData() {
        EdorNo = mLPEdorItemSchema.getEdorAcceptNo();
        System.out.println(EdorNo);

        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPEdorItemSchema.getEdorAcceptNo());
        if (!tLGWorkDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PEdorRBAppConfirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "查询工单记录失败";
            this.mErrors.addOneError(tError);
            return false;
        }

        oldEdorNo = tLGWorkDB.getInnerSource();
        System.out.println("原来的工单号：" + oldEdorNo);


        //把P表的数据导回到C表中
        LPContSet tLPContSet = getLPCcnt();
        for (int i = 1; i <= tLPContSet.size(); i++) {
            LPContSchema tLPContSchema = tLPContSet.get(i);
            LCContSchema tLCContSchema = new LCContSchema();
            ref.transFields(tLCContSchema, tLPContSchema);
            tLCContSchema.setStateFlag(BQ.STATE_FLAG_SIGN);
            tLCContSchema.setAppFlag("1");

            LCContStateDB t = new LCContStateDB();

            String s = "select * from lccontstate "
                       + "where contno ='" + mLPEdorItemSchema.getContNo() +
                       "' "
                       + "   and polno = '000000' "
                       + "   and StateType ='Terminate' and State = '1'";
            LCContStateSet tS = t.executeQuery(s);
            if (tS != null && tS.size() > 0) {
                sql = new StringBuffer();
                sql.append("update LCContState set State = '0',enddate=current date ")
                        .append("where contno ='")
                        .append(this.mLPEdorItemSchema.getContNo())
                        .append("' and polno ='000000' ")
                        .append(" and StateType ='Terminate' and State = '1' ");
                map.put(sql.toString(), SysConst.UPDATE);
            }
            map.put(mLPEdorItemSchema, SysConst.UPDATE);
            tLCContSet.add(tLCContSchema);
        }
        map.put(tLCContSet, "INSERT");
        //LCPpol
        LPPolSet tLPPolSet = getLPPol();
        for (int i = 1; i <= tLPPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            LCPolSchema tLCPolSchema = new LCPolSchema();
            ref.transFields(tLCPolSchema, tLPPolSchema);
            tLCPolSchema.setOperator(mGlobalInput.Operator);
            tLCPolSchema.setStateFlag(BQ.STATE_FLAG_SIGN);
            tLCPolSchema.setAppFlag("1");

            String updateStr = "update LCContState set State = '0',enddate=current date  "
                        + "where contno ='"
                        + this.mLPEdorItemSchema.getContNo()
                        + "' and polno ='"
                        + tLPPolSet.get(i).getPolNo()
                        + "' and StateType ='Terminate' and State = '1' ";
            map.put(updateStr,SysConst.UPDATE);

            tLCPolSet.add(tLCPolSchema);
        }
        map.put(tLCPolSet, "INSERT");
        //lcprem
        LPPremSet tLPPremSet = getLPPrem();
        for (int i = 1; i <= tLPPremSet.size(); i++) {
            LPPremSchema tLPPremSchema = tLPPremSet.get(i);
            LCPremSchema tLCPremSchema = new LCPremSchema();
            ref.transFields(tLCPremSchema, tLPPremSchema);
            tLCPremSchema.setOperator(mGlobalInput.Operator);
            tLCPremSet.add(tLCPremSchema);
        }
        map.put(tLCPremSet, "INSERT");
        // lcget
        LPGetSet tLPGetSet = getLPGet();
        for (int i = 1; i <= tLPGetSet.size(); i++) {
            LPGetSchema tLPGetSchema = tLPGetSet.get(i);
            LCGetSchema tLCGetSchema = new LCGetSchema();
            ref.transFields(tLCGetSchema, tLPGetSchema);
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            tLCGetSet.add(tLCGetSchema);
        }
        map.put(tLCGetSet, "INSERT");
        // lcduty
        LPDutySet tLPDutySet = getLPDuty();
        for ( int i=1 ; i<= tLPDutySet.size(); i++)
        {
            LPDutySchema tLPDutySchema = tLPDutySet.get(i);
            LCDutySchema tLCDutySchema = new LCDutySchema();
            ref.transFields(tLCDutySchema,tLPDutySchema);
            tLCDutySchema.setOperator(mGlobalInput.Operator);
            tLCDutySet.add(tLCDutySchema);
        }
        map.put(tLCDutySet,"INSERT");
        // lcappnt
        LPAppntSet tLPAppntSet = getLPAppntSet();
        for ( int i = 1; i<=tLPAppntSet.size() ; i++ )
        {
            LPAppntSchema tLPAppntSchema = tLPAppntSet.get(i);
            LCAppntSchema tLCAppntSchema = new LCAppntSchema();
            ref.transFields(tLCAppntSchema,tLPAppntSchema);
            tLCAppntSchema.setOperator(mGlobalInput.Operator);
            tLCAppntSet.add(tLCAppntSchema);
        }
        map.put(tLCAppntSet,"INSERT");
        //lcinsuerd
        LPInsuredSet tLPInsuredSet = getLPInsured();
        for ( int i=1 ; i <=tLPInsuredSet.size() ; i++)
        {
            LPInsuredSchema tLPInsuredSchema = tLPInsuredSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            ref.transFields(tLCInsuredSchema,tLPInsuredSchema);
            tLCInsuredSet.add(tLCInsuredSchema);
        }
        map.put(tLCInsuredSet,"INSERT");
        // lcbnf
        LPBnfSet tLPBnfSet = getLPBnf();
        for ( int i = 1 ; i <= tLPBnfSet.size() ; i++)
        {
            LPBnfSchema tLPBnfSchema = tLPBnfSet.get(i);
            LCBnfSchema tLCBnfSchema = new LCBnfSchema();
            ref.transFields(tLCBnfSchema,tLPBnfSchema);
            tLCBnfSet.add(tLCBnfSchema);
        }
        map.put(tLCBnfSet,"INSERT");
        
        //处理万能险或分红险的退保回退信息
        if(!dealULIRiskDate(tLPContSet,tLPPolSet,oldEdorNo)){
        	System.out.println("++++该保单没有万能险或者分红险++++++");
        }
        
        /**
         * 删除B表的数据。
         */
        String sql1 = " edorno='" + oldEdorNo + "'";
        map.put("delete from lbcont where" + sql1, "DELETE");
        map.put("delete from lbpol where" + sql1, "DELETE");
        map.put("delete from lbprem where" + sql1, "DELETE");
        map.put("delete from lbduty where" + sql1, "DELETE");
        map.put("delete from lbget where" + sql1, "DELETE");
        map.put("delete from lbappnt where" + sql1, "DELETE");
        map.put("delete from lbinsured where" + sql1, "DELETE");
        map.put("delete from LBBnf where" + sql1, "DELETE");
      //  map.put("delete from lccontstate where otherno ='"+oldEdorNo+"'","DELETE");

        return true;
    }

    /**
     * 处理万能险信息
     * WUJUN
     * @param tLPContSet
     * @param tLPPolSet
     * @return
     */
    public boolean dealULIRiskDate(LPContSet tLPContSet,LPPolSet tLPPolSet,String oldEdorNo){
    	if(tLPContSet.size()<=0||tLPPolSet.size()<=0){
    		return false;
    	}
    	mExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	for(int i=1;i<=tLPContSet.size();i++){
    		//判断保单下是否有万能险或者分红险
    		if(this.hasULIRisk(tLPContSet.get(i).getContNo())||this.hasBonusRisk(tLPContSet.get(i).getContNo())){
    			//先将p表数据添加到C表并且删除B表
    			String[] lPTable =
    			{
    					 "LPInsureAcc", "LPInsureAccTrace", "LPInsuredRelated", "LPPremToAcc", "LPGetToAcc", "LPInsureAccFee", "LPInsureAccClassFee", "LPInsureAccClass", "LPInsureAccFeeTrace", "LPCustomerImpart", "LPCustomerImpartParams"
    			};
    			String[] lCTable =
    			{
    					 "LCInsureAcc", "LCInsureAccTrace", "LCInsuredRelated", "LCPremToAcc", "LCGetToAcc", "LCInsureAccFee", "LCInsureAccClassFee", "LCInsureAccClass", "LCInsureAccFeeTrace", "LCCustomerImpart", "LCCustomerImpartParams"
    			};
    			String[] lbTable =
    			{
    					 "LBInsureAcc", "LBInsureAccTrace", "LBInsuredRelated", "LBPremToAcc", "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee", "LBInsureAccClass", "LBInsureAccFeeTrace", "LBCustomerImpart", "LBCustomerImpartParams"
    			};	    			
    			for(int m=1;m<=tLPPolSet.size();m++){
    				for(int j = 0;j < lCTable.length; j++){
    					String tSqlIns ="";
    					String tSqlIns2 ="";
    					String tSqlDel ="";
    					if (lCTable[j].toUpperCase().equals("LCCUSTOMERIMPART") || lCTable[j].toUpperCase().equals("LCCUSTOMERIMPARTPARAMS"))
    					{
    						String tSqlCol = GetColName(lCTable[j]);
    						if(tSqlCol == null || "".equals(tSqlCol))
    						{
    							return false;
    						}
    						tSqlIns = "insert into " + lCTable[j] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and ContNo in (select ContNO from LCPol where PolNo='" + tLPPolSet.get(m).getPolNo() + "') and CustomerNo in (select CustomerNo from LCBnf where PolNo='" + tLPPolSet.get(m).getPolNo() + "' ) and CustomerNoType='3')";
    						map.put(tSqlIns, "INSERT");
    					}
    					else if(lCTable[j].toUpperCase().equals("LCINSUREACCTRACE") || lCTable[j].toUpperCase().equals("LCINSUREACCFEETRACE")){
    						String tSqlCol1 = GetColName1(lCTable[j]);
    						if(tSqlCol1 == null || "".equals(tSqlCol1))
    						{
    							return false;
    						}
    						//判断是否为分红险或者万能险种
    						String riskSQL = "select 1 from lmriskapp where riskcode = '" + tLPPolSet.get(m).getRiskCode() + "' and risktype4 in ('4','2') with ur ";
    						String riskflag = mExeSQL.getOneValue(riskSQL);
    						if(riskflag!=null&&!"".equals(riskflag)){
    							tSSRS = mExeSQL.execSQL("select SERIALNO from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and polno='" + tLPPolSet.get(m).getPolNo() + "'  ");
    							for(int n=1;n<=tSSRS.getMaxRow();n++){
    								String serialNo = PubFun1.CreateMaxNo("SERIALNO",tLPContSet.get(i).getManageCom());
    								tSqlIns = "insert into " + lCTable[j] + "(SERIALNO," + tSqlCol1 + ")" + " (select '"+serialNo+"'," + tSqlCol1 + " from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and polno='" + tLPPolSet.get(m).getPolNo() + "' and serialNo = '"+tSSRS.GetText(n, 1)+"')";
    								map.put(tSqlIns, "INSERT");
    							}
    							
    							String tSqlCol = GetColName(lCTable[j]);
    							if(tSqlCol == null || "".equals(tSqlCol))
    							{
    								return false;
    							}
    							
    							tSqlIns2 = "insert into " + lCTable[j] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "'  and polno='" + tLPPolSet.get(m).getPolNo() + "' )";
    							map.put(tSqlIns2, "INSERT");
    						}
    						
    					}else
    					{
    						//判断是否为分红险或者万能险种
    						String riskSQL = "select 1 from lmriskapp where riskcode = '" + tLPPolSet.get(m).getRiskCode() + "' and risktype4 in ('4','2') with ur ";
    						String riskflag = mExeSQL.getOneValue(riskSQL);
    						if(riskflag!=null&&!"".equals(riskflag)){
    						String tSqlCol = GetColName(lCTable[j]);
    						if(tSqlCol == null || "".equals(tSqlCol))
    						{
    							return false;
    						}
    						tSqlIns = "insert into " + lCTable[j] + "(" + tSqlCol + ")" + " (select " + tSqlCol + " from " + lPTable[j] + " where EdorNo= '" + mLPEdorItemSchema.getEdorAcceptNo() + "' and polno='" + tLPPolSet.get(m).getPolNo() + "')";
    						map.put(tSqlIns, "INSERT");
    						}
    					}   
    					
    					
    					if (lPTable[j].toUpperCase().equals("LPCUSTOMERIMPART") || lPTable[j].toUpperCase().equals("LPCUSTOMERIMPARTPARAMS"))
    					{

    						tSqlDel = "delete from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "' and ContNo ='" + tLPPolSet.get(m).getPolNo() + "' and CustomerNo ='" + tLPPolSet.get(m).getPolNo() + "'  and CustomerNoType='I'";
    						map.put(tSqlDel, "DELETE");

    					}
    					else
    					{
    						//判断是否为分红险或者万能险种
    						String riskSQL = "select 1 from lmriskapp where riskcode = '" + tLPPolSet.get(m).getRiskCode() + "' and risktype4 in ('4','2') with ur ";
    						String riskflag = mExeSQL.getOneValue(riskSQL);
    						if(riskflag!=null&&!"".equals(riskflag)){
    						tSqlDel = "delete from " + lbTable[j] + " where EdorNo= '" + oldEdorNo + "' and polno='" + tLPPolSet.get(m).getPolNo() + "'";
    						map.put(tSqlDel, "DELETE");
    						}
    					}
    				}
    				
    			}
    			
    			
    			//删除月结回退到解约之前
    			String balanceSQL = "delete from lcinsureaccbalance where contno='"+tLPContSet.get(i).getContNo()+"' and sequenceno ='"+oldEdorNo+"' ";
    			map.put(balanceSQL, SysConst.DELETE);
    			
    		}else{
    			return false;
    		}
    	}
    		return true;
    	}
    
    
    /**
     * 判断保单下是否含有万能险种
     * @return boolean
     */
    public boolean hasULIRisk(String ContNo)
    {
        LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setContNo(ContNo);
        LBPolSet tLBPolSet = tLBPolDB.query();
        for (int i = 1; i <= tLBPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE1_ULI)))
            {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * 判断保单下是否含有分红险种
     * @return boolean
     */
    public boolean hasBonusRisk(String ContNo)
    {
    	LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setContNo(ContNo);
        LBPolSet tLBPolSet = tLBPolDB.query();
        for (int i = 1; i <= tLBPolSet.size(); i++)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                return false;
            }
            String riskType4 = tLMRiskAppDB.getRiskType4();
            if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE4_BONUS)))
            {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * 获取表的列
     * WUJUN
     * @param pTableName
     * @return
     */
	private String GetColName(String pTableName)
	{
		String tSqlCol = "select colname from SYSCAT.COLUMNS where tabname='" + pTableName.toUpperCase() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSqlCol);
		if (tSSRS == null || tSSRS.getMaxRow() == 0)
		{
			System.out.println("取得表[" + pTableName + "]的列名失败！");
			return null;
		}
		String tSql="";
		for (int k = 1; k <= tSSRS.getMaxRow(); k++)
		{
			if (k == 1)
			{
				tSql = tSql + tSSRS.GetText(k, 1);
			}
			else
			{
				tSql = tSql + "," + tSSRS.GetText(k, 1);
			}
		}
		return tSql;
	}

	private String GetColName1(String pTableName)
	{
		String tSqlCol = "select colname from SYSCAT.COLUMNS where tabname='" + pTableName.toUpperCase() + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(tSqlCol);
		if (tSSRS == null || tSSRS.getMaxRow() == 0)
		{
			System.out.println("取得表[" + pTableName + "]的列名失败！");
			return null;
		}
		String tSql="";
		for (int k = 1; k <= tSSRS.getMaxRow(); k++)
		{
			if (k == 1)
			{
				if(tSSRS.GetText(k, 1).toUpperCase().equals("SERIALNO")){
					continue;
				}else{
					tSql = tSql + tSSRS.GetText(k, 1);
				}
			}
			else
			{
				if(tSSRS.GetText(k, 1).toUpperCase().equals("SERIALNO")){
					continue;
				}else if(tSql !=null&&!"".equals(tSql)){
				tSql = tSql + "," + tSSRS.GetText(k, 1);
				}else{
					tSql = tSql + tSSRS.GetText(k, 1);
				}
			}
		}
		return tSql;
	}
	
    /**
     * 得到保单信息
     */
    private LPContSet getLPCcnt() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPCont ")
                .append("where EdorNo = '")
                .append(EdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LPContDB tLPContDB = new LPContDB();
        LPContSet tLPContSet = tLPContDB.executeQuery(sql.toString());
        return tLPContSet;
    }

    /**
     * 得到险种信息
     */
    private LPPolSet getLPPol() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPPol ")
                .append("where EdorNo = '")
                .append(EdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LPPolDB tLPPolDB = new LPPolDB();
        LPPolSet tLPPolSet = tLPPolDB.executeQuery(sql.toString());
        return tLPPolSet;
    }

    private LPPremSet getLPPrem() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPPrem ")
                .append("where EdorNo = '")
                .append(EdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LPPremDB tLPPremDB = new LPPremDB();
        LPPremSet tLPPremSet = tLPPremDB.executeQuery(sql.toString());
        return tLPPremSet;
    }

    private LPGetSet getLPGet() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPGet ")
                .append("where EdorNo = '")
                .append(EdorNo)
                .append("' ");
        System.out.println(sql.toString());
        LPGetDB tLPGetDB = new LPGetDB();
        LPGetSet tLPGetSet = tLPGetDB.executeQuery(sql.toString());
        return tLPGetSet;
    }

    private LPDutySet getLPDuty() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ").append("from LPDuty ").append("where EdorNo = '").
                append(EdorNo).append("' ");
        System.out.println(sql.toString());
        LPDutyDB tLPDutyDB = new LPDutyDB();
        LPDutySet tLPDutySet = tLPDutyDB.executeQuery(sql.toString());
        return tLPDutySet ;
    }

    private LPAppntSet getLPAppntSet()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ").append("from LPAppnt ").append(" where EdorNo = '").
                append(EdorNo).append("' ");
        System.out.println(sql.toString());
        LPAppntDB tLPAppntDB = new LPAppntDB();
        LPAppntSet tLPAppntSet = tLPAppntDB.executeQuery(sql.toString());
        return tLPAppntSet ;
    }

    private LPInsuredSet getLPInsured()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ").append("from LPInsured ").append("where EdorNo = '").
                append(EdorNo).append("' ");
        System.out.println(sql.toString());
        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        LPInsuredSet tLPInsuredSet = tLPInsuredDB.executeQuery(sql.toString());
        return tLPInsuredSet ;
    }

    private LPBnfSet getLPBnf()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ").append("from LPBnf ").append(" where EdorNo = '").
                append(EdorNo).append("' ");
        System.out.println(sql.toString());
        LPBnfDB tLPBnfDB = new LPBnfDB();
        LPBnfSet tLPBnfSet = tLPBnfDB.executeQuery(sql.toString());
        return tLPBnfSet ;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args) {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo("20080313000004");

        VData data = new VData();
        data.add(gi);
        data.add(tLPEdorItemDB.query().get(1));

        PEdorRBConfirmBL bl = new PEdorRBConfirmBL();
        if (!bl.submitData(data, "")) {
            System.out.println(bl.mErrors.getErrContent());
        } else {
            System.out.println("All OK");
        }
    }
}
