package com.sinosoft.lis.newpeople;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.newpeople.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保单状态变更功能
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */

public class BqUpdatePolStateUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    public BqUpdatePolStateUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	BqUpdatePolStateBL tBqUpdatePolStateBL = new BqUpdatePolStateBL();
        if (!tBqUpdatePolStateBL.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(tBqUpdatePolStateBL.mErrors);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        /*GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";

        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setContNo("014100865000001");
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet.add(tLCPolSchema);

        //输入参数
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLCPolSet);

        BqUpdatePolStateUI tBqUpdatePolStateUI = new BqUpdatePolStateUI();*/
/*        if (!tBqUpdatePolStateUI.submitData(tVData, "INVALID"))
        {
        }
*/    }
}
