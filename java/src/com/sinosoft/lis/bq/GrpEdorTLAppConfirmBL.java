package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体特需医疗提前领取</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorTLAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    private DetailDataQuery mQuery = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private double uligrpleft = Double.parseDouble(new ExeSQL().getOneValue("select code from ldcode where codetype='uligrpleft'"));
    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mVtsData = new EdorItemSpecialData(edorItem);
        mQuery = new DetailDataQuery(mEdorNo, mEdorType);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setGrpAcc())
        {
            return false;
        }
        if (!setInsuredAcc())
        {
            return false;
        }
        setEdorInfo();
        setGrpEdorItem();
        return true;
    }

    /**
     * 设置团体账户领取保费金额
     */
    private boolean setGrpAcc()
    {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++)
        {
            String grpPolNo = grpPolNos[i];
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(grpPolNo);
            tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
            LCPolSet tLCPolSet = tLCPolDB.query();
            if (tLCPolSet.size() == 0)
            {
            	mErrors.addOneError("找不到公共账户个单险种信息！");
                return false;
            }
            String riskCode = tLCPolSet.get(1).getRiskCode();
            String polNo = tLCPolSet.get(1).getPolNo();
            LCInsureAccSchema tLCInsureAccSchema =
                    CommonBL.getLCInsureAcc(polNo,
                    CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP, riskCode));
            if (tLCInsureAccSchema == null)
            {
                mErrors.addOneError("找不到公共账户信息！");
                return false;
            }
            mSpecialData.setGrpPolNo(polNo);
            double grpMoney = Double.parseDouble(mSpecialData.getEdorValue("GrpLQMoney")); //应领金额
            System.out.println("grpMoney:" + grpMoney);
            double leftMoney = 0.0; //计算余额
            double manageMoney = 0.0;  //计算管理费
            System.out.println("manageMoney" + manageMoney);
//          解约管理费率
        	System.out.println("解约管理费率开始计算");
        	int tpolyear=PubFun.calPolYear(tLCPolSet.get(1).getCValiDate(), mEdorValiDate);
        	String tfee = "";
        	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLCPolSet.get(1).getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
        	tfee = new ExeSQL().getOneValue(polRateSQL);
            if ((tfee == null) || (tfee.equals(""))){
            	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLCPolSet.get(1).getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
            	if ((tfee == null) || (tfee.equals(""))){
            		mErrors.addOneError("查询管理费为空！");
            		return false;
            	}
            }
            double feeRate = Double.parseDouble(tfee); //解约管理费率
            double getMoney = grpMoney;  //计算实领金额
            if(grpMoney<0)
            {
            	mErrors.addOneError("公共账户信息领取金额小于0！");
                return false;
            }
            else if (grpMoney == 0)
            {
                continue;
            }
            else if(tLCInsureAccSchema.getInsuAccBala()*0.15>=grpMoney&&grpMoney>0)
            {
            	leftMoney = CommonBL.carry(tLCInsureAccSchema.getInsuAccBala() - grpMoney); //计算余额
            }
            else
            {
                manageMoney = Math.floor(100*(grpMoney-tLCInsureAccSchema.getInsuAccBala()*0.15) * feeRate)/100;  //计算管理费
                leftMoney = CommonBL.carry(tLCInsureAccSchema.getInsuAccBala() - grpMoney - manageMoney);//计算余额

            }
            if (leftMoney < uligrpleft)
            {
                mErrors.addOneError("公共账户领取后余额少于约定的剩余金额！");
                return false;
            }
            if(leftMoney<0)
            {
            	mErrors.addOneError("公共账户领取后余额为负数！");
                return false;
            }
            System.out.println("line 176 manageMoney:" + manageMoney);
            setLPInsureAcc(tLCInsureAccSchema, grpMoney, leftMoney);
            //处理分类帐号
            LCInsureAccClassSchema tLCInsureAccClassSchema=new LCInsureAccClassSchema();
            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
            tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
            tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
            tLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
            if (tLCInsureAccClassSchema == null)
            {
            	mErrors.addOneError("找不到万能账户分类！");
                return false;
            }
            setLPInsureAccTraceSXF(tLCInsureAccSchema,tLCInsureAccClassSchema, manageMoney);
            setLPInsureAccClass(tLCInsureAccClassSchema, grpMoney, leftMoney);
            setLPInsureAccTraceGet(tLCInsureAccSchema,tLCInsureAccClassSchema, grpMoney); //领取金额
//            setLPInsureAccTrace(tLCInsureAccSchema, manageMoney);  //退保费用
//            setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
//            setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney, feeRate);
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("找不到团体公共账户的险种信息！");
                return false;
            }
            setGetEndorse(tLCPolSchema, getMoney);
            mGetMoney += getMoney;
            //设置批单显示数据
            mVtsData.setGrpPolNo(grpPolNos[i]);
            mVtsData.add("GrpBeforeMoney", CommonBL.bigDoubleToCommonString(tLCInsureAccSchema.getInsuAccBala(), "0.00"));
            mVtsData.add("GrpGetMoney", CommonBL.bigDoubleToCommonString(getMoney, "0.00"));
            mVtsData.add("GrpManageMoney", CommonBL.bigDoubleToCommonString(manageMoney, "0.00"));
            mVtsData.add("GrpLeftMoney", CommonBL.bigDoubleToCommonString(leftMoney, "0.00"));
            
        }
        return true;
    }
    
    /**
     * 设置管理费分类 轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param manageMoney double
     * @param feeRate double
     */
    private void setLPInsureAccClassFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney, double feeRate)
    {
        LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
        tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
        tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccClassFeeSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccClassFeeSchema.setOtherNo(mEdorNo);
        tLPInsureAccClassFeeSchema.setOtherType("3"); //3是保全
        tLPInsureAccClassFeeSchema.setAccAscription("0");
        tLPInsureAccClassFeeSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccClassFeeSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccSchema.getInsuredNo());
        tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccSchema.getAppntNo());
        tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccSchema.getAccType());
        tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccSchema.getAccComputeFlag());
        tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccSchema.getAccFoundDate());
        tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccSchema.getAccFoundTime());
        tLPInsureAccClassFeeSchema.setBalaDate(aLCInsureAccSchema.getBalaDate());
        tLPInsureAccClassFeeSchema.setBalaTime(aLCInsureAccSchema.getBalaTime());
        tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
        tLPInsureAccClassFeeSchema.setFee(manageMoney);
        tLPInsureAccClassFeeSchema.setFeeUnit(0);
        tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
        tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
    }   
    
    

    /**
     * 设置管理费
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param manageMoney double
     */
    private void setLPInsureAccFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney)
    {
        String[] keys = new String[2];
        keys[0] = aLCInsureAccSchema.getPolNo();
        keys[1] = aLCInsureAccSchema.getInsuAccNo();
        LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema)
                mQuery.getDetailData("LCInsureAccFee", keys);
        tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
        tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
    }
    
    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType("TL");
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }    
    
    
    
    private void setLPInsureAccClass(LCInsureAccClassSchema aLCInsureAccClassSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccClassSchema.getSumPaym() + money; //累计领取
        LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccClassSchema, aLCInsureAccClassSchema);
        tLPInsureAccClassSchema.setEdorNo(mEdorNo);
        tLPInsureAccClassSchema.setEdorType(mEdorType);
        tLPInsureAccClassSchema.setInsuAccBala(leftMoney);
        tLPInsureAccClassSchema.setSumPaym(sumPaym);
        tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
        tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
        tLPInsureAccClassSchema.setAccAscription("0");
        mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
    }
    
    /**
     * 设置账户余额
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param getMoney double
     */
    private void setLPInsureAcc(LCInsureAccSchema aLCInsureAccSchema, double money, double leftMoney)
    {
        double sumPaym = aLCInsureAccSchema.getSumPaym() + money; //累计领取
        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccSchema, aLCInsureAccSchema);
        tLPInsureAccSchema.setEdorNo(mEdorNo);
        tLPInsureAccSchema.setEdorType(mEdorType);
        tLPInsureAccSchema.setInsuAccBala(leftMoney);
        tLPInsureAccSchema.setSumPaym(sumPaym);
        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccSchema.setModifyDate(mCurrentDate);
        tLPInsureAccSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取金额轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTraceGet(LCInsureAccSchema aLCInsureAccSchema,LCInsureAccClassSchema tLCInsureAccClassSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(mEdorType);
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    /**
     * 设置帐户领取金额轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTraceSXF(LCInsureAccSchema aLCInsureAccSchema,LCInsureAccClassSchema tLCInsureAccClassSchema, double grpMoney)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType("MF");
        tLPInsureAccTraceSchema.setMoney(-Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    /**
     * 设置个人账户个人缴费领取金额
     * @return boolean
     */
    private boolean setInsuredAcc()
    {
        LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String riskcode = tLCGrpPolSet.get(i).getRiskCode();
            if (!CommonBL.isULIRisk(riskcode))
            {
                continue;
            }
            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            String riskSeqNo = tLCGrpPolSchema.getRiskSeqNo();
            LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet
                    (mEdorNo, mEdorType, mGrpContNo, null,
                    BQ.IMPORTSTATE_SUCC);
            //被保人领取费用合计
            double insuredGetMoney = 0.0;
            //被保人管理费合计
            double insuredManageMoney = 0.0;
            for (int j = 1; j <= tLPDiskImportSet.size(); j++)
            {
            	LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(j);
                String insuredNo = tLPDiskImportSchema.getInsuredNo();
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(grpPolNo,
                        insuredNo);
                if (tLCPolSchema == null)
                {
                    mErrors.addOneError("找不到客户" + insuredNo + "的险种信息！");
                    return false;
                }
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setGrpPolNo(grpPolNo);
                tLCInsureAccDB.setInsuredNo(insuredNo);
                String insuaccno=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLCPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
                tLCInsureAccDB.setInsuAccNo(insuaccno);//个人账户个人缴费部分的账户代码
                LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
                if (tLCInsureAccSet.size() == 0)
                {
                	mErrors.addOneError("找不到客户" + insuredNo + "的个人账户总信息！");
                    return false;
                }
                LCInsureAccSchema tLCInsureAccSchema =tLCInsureAccSet.get(1);
                if (tLCInsureAccSchema == null)
                {
                    mErrors.addOneError("找不到客户" + insuredNo + "的个人账户信息！");
                    return false;
                }
                double insuredMoney = tLPDiskImportSchema.getMoney();
                double getMoney = insuredMoney;  //计算实领金额
                double leftMoney = 0.0;
                double manageMoney = 0.0;//管理费
                if (insuredMoney < 0)
                {
                    mErrors.addOneError("客户" + insuredNo + "录入的领取金额为负数！");
                    return false;
                }
                else if(insuredMoney==0)
                {
                	continue;
                }
                else if(tLCInsureAccSchema.getInsuAccBala()*0.15>=insuredMoney&&insuredMoney>0)
                {
                    leftMoney = CommonBL.carry(tLCInsureAccSchema.getInsuAccBala() - insuredMoney); //计算余额
                }
                else
                {
//                  解约管理费率
                	System.out.println("解约管理费率开始计算");
                	int tpolyear=PubFun.calPolYear(tLCPolSchema.getCValiDate(), mEdorValiDate);
                	String tfee = "";
                	String polRateSQL = "SELECT extractrate FROM LCRISKZTFEE WHERE grpcontno = '"+tLCPolSchema.getGrpContNo()+"'  and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear;
                	tfee = new ExeSQL().getOneValue(polRateSQL);
                    if ((tfee == null) || (tfee.equals(""))){
                    	tfee=new ExeSQL().getOneValue("select extractrate from lmriskztfee where riskcode='"+tLCPolSchema.getRiskCode()+"' and beginpolyear<"+tpolyear+" and endpolyear>="+tpolyear);
                    	if ((tfee == null) || (tfee.equals(""))){
                    		mErrors.addOneError("查询管理费为空！");
                    		return false;
                    	}
                    }
                	double feeRate = Double.parseDouble(tfee); //解约管理费率
                    manageMoney = Math.floor(100*(insuredMoney-tLCInsureAccSchema.getInsuAccBala()*0.15) * feeRate)/100;  //计算管理费
                    leftMoney = CommonBL.carry(tLCInsureAccSchema.getInsuAccBala() - insuredMoney - manageMoney);//计算余额
                }
//                if (leftMoney < uligrpleft)
//                {
//                    mErrors.addOneError("被保人" + tLCInsureAccSchema.getInsuredNo() + "的账户领取后余额少于约定的剩余金额！");
//                    return false;
//                }
                if (leftMoney < 0)
                {
                    mErrors.addOneError("被保人" + tLCInsureAccSchema.getInsuredNo() + "的账户余额不足！");
                    return false;
                }
                setLPInsureAcc(tLCInsureAccSchema, insuredMoney, leftMoney);
                //处理分类帐号
                LCInsureAccClassSchema tLCInsureAccClassSchema=new LCInsureAccClassSchema();
                LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
                tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
                tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
                tLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
                if (tLCInsureAccClassSchema == null)
                {
                	mErrors.addOneError("找不到万能账户分类！");
                    return false;
                }
                setLPInsureAccTraceSXF(tLCInsureAccSchema,tLCInsureAccClassSchema, manageMoney);//设置管理费的trace
                setLPInsureAccClass(tLCInsureAccClassSchema, insuredMoney, leftMoney);
                setLPInsureAccTraceGet(tLCInsureAccSchema,tLCInsureAccClassSchema, insuredMoney);
                setGetEndorse(tLCPolSchema, getMoney);
                mGetMoney += getMoney;
                insuredGetMoney += insuredMoney;
                insuredManageMoney += manageMoney;
                setDiskImport(tLPDiskImportSchema, leftMoney, manageMoney,tLCInsureAccSchema.getInsuAccBala());
            }
            
            //获取公共账户险种的险种号
            LCPolDB mmLCPolDB= new LCPolDB();
            mmLCPolDB.setGrpPolNo(grpPolNo);
            mmLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
            LCPolSet mmLCPolSet = mmLCPolDB.query();
            if (mmLCPolSet.size() == 0)
            {
            	mErrors.addOneError("找不到公共账户个单险种信息！");
                return false;
            }
            //设置批单显示数据
            mVtsData.setGrpPolNo(mmLCPolSet.get(1).getPolNo());
            mVtsData.add("InsuredGetMoney", CommonBL.bigDoubleToCommonString(insuredGetMoney, "0.00"));
            mVtsData.add("InsuredManageMoney", CommonBL.bigDoubleToCommonString(insuredManageMoney, "0.00"));
        }
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LCPolSchema aLCPolSchema, double edorPrem)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(-Math.abs(edorPrem));
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_TB);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem()
    {
        System.out.println(mGetMoney);
        String sql = "update LPGrpEdorItem " +
                "set GetMoney = -" + mGetMoney + ", " + //这里是负数
                "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                "ModifyDate = '" + mCurrentDate + "', " +
                "ModifyTime = '" + mCurrentTime + "' " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema, double leftMoney, double manageMoney, double insuaccbala)
    {
        tLPDiskImportSchema.setInsuAccInterest(String.valueOf(manageMoney));
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setInsuAccBala(String.valueOf(insuaccbala));
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }

    /**
     * 保存要在批单中显示的数据
     */
    private void setEdorInfo()
    {
        mMap.put(mVtsData.getSpecialDataSet(), "DELETE&INSERT");
    }
 }
