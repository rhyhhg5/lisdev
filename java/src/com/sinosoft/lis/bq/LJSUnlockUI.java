package com.sinosoft.lis.bq;
//程序名称：LJSUnlockUI.java
//程序功能：保全续期收付费信息修改
//创建日期：2009-4-15
//创建人  ：zhanggm 
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class LJSUnlockUI 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	public LJSUnlockUI()
	{
	}

    public boolean submitData(VData cInputData, String ywType) 
    {
    	LJSUnlockBL tLJSUnlockBL = creatBL(ywType);
        if (!tLJSUnlockBL.submitData(cInputData)) 
        {
            this.mErrors.copyAllErrors(tLJSUnlockBL.getErrors());
            return false;
        }
        return true;
    }
//  业务类型，BQPAY保全收费，BQGET保全付费，XQPAY续期收费，XQGET续期付费(满期给付)
    public LJSUnlockBL creatBL(String ywType)
    {
    	if("BQPAY".equals(ywType))
    	{
    		return new LJSPayBQUnlockBL();
    	}
    	else if("BQGET".equals(ywType))
    	{
    		return new LJAGetBQUnlockBL();
    	}
    	else if("XQPAY".equals(ywType))
    	{
    		return new LJSPayXQUnlockBL();
    	}
    	else
    	{
    		return new LJAGetXQUnlockBL();
    	}
    }

}
