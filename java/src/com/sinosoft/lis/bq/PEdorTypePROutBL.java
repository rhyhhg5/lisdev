package com.sinosoft.lis.bq;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 个单迁移</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class PEdorTypePROutBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mMap = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    private String mManageCom;

    /** 全局数据 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private EdorItemSpecialData mEdorItemSpecialData;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public PEdorTypePROutBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (getSubmitData(cInputData, cOperate) == null) {
            return false;
        }
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorTypePROutBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    public MMap getSubmitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return null;
        }

        //数据准备操作
        if (!dealData()) {
            return null;
        }
        return mMap;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPEdorItemSchema.setSchema((LPEdorItemSchema) mInputData.
                                        getObjectByObjectName(
                                                "LPEdorItemSchema", 0));

            mGlobalInput = (GlobalInput) mInputData.
                           getObjectByObjectName("GlobalInput", 0);

            if (mLPEdorItemSchema == null) {
                CError.buildErr(this, "接收数据失败");
                return false;
            }
            if (mLPEdorItemSchema.getEdorNo() == null ||
                mLPEdorItemSchema.getContNo() == null) {
                CError.buildErr(this, "接收数据失败,没有传入保全号或者保单号");
                return false;
            }
        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean dealData() {
        //数据转移对象
        mEdorItemSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
        String contNo = mLPEdorItemSchema.getContNo();
        String edorNo = mLPEdorItemSchema.getEdorNo();
        String edorType = mLPEdorItemSchema.getEdorType();
        LPContDB tLPContDB = new LPContDB();
        tLPContDB.setContNo(contNo);
        tLPContDB.setEdorNo(edorNo);
        tLPContDB.setEdorType(edorType);
        if (!tLPContDB.getInfo()) {
            CError.buildErr(this, "查询保单失败");
            return false;
        }

        //查询工单转移规则
        //先找8位，再找4位，没有则报错
        String workBox = null;
        String manageCom8 = tLPContDB.getManageCom();
        String manageCom4 = manageCom8.substring(0, 4);
        ExeSQL tExeSQL = new ExeSQL();

        //查8位规则
        String sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom8 + "' "
                     + "   and goalType = '9' "
                     + "   and defaultFlag = '1' "
                     ;
        String target = tExeSQL.getOneValue(sql);
        if (!target.equals("") && !target.equals("null")) {
            workBox = target;
            mManageCom=manageCom8;
        }
        if (workBox == null) {
            //查4位规则
            sql = "select target "
                  + "from LGAutoDeliverRule "
                  + "where sourceComCode = '" + manageCom4 + "' "
                  + "   and goalType = '9' "
                  + "   and defaultFlag = '1' "
                  ;
            target = tExeSQL.getOneValue(sql);
            if (!target.equals("") && !target.equals("null")) {
                workBox = target;
                mManageCom=manageCom4;
            }
        }
             
        if (workBox == null) {
            mErrors.addOneError("没有查询到机构" + manageCom8 + "的个单迁移转交接受信箱，请进行系统配置");
            return false;
        }
        //------------------------YtZ,20110830修改保单迁移的接受邮件，此处先添加限制，之后会制作运维功能-----------
        String strSQLuserstate="select userstate from lduser where usercode in ( select ownerno" +
   		" from lgworkbox where workboxno=;'"+workBox+"' )";
        String userstate = tExeSQL.getOneValue(strSQLuserstate);
        if(userstate.equals("1"))
       {
	     mErrors.addOneError("查询到机构" + mManageCom + "的个单迁移转交接受信箱"+workBox+"已经失效，请您联系总公司及时进行系统配置");
         return false;
       }
      // -----------------------the end-------------------------------------------
        
        taskDeliver(edorNo,workBox);
        //生成函件
        int serialNumber = getSerialNumber(edorNo);
        LGLetterSchema tLGLetterSchema = new LGLetterSchema();
        //迁入机构销售函件
        tLGLetterSchema.setEdorAcceptNo(edorNo);
        tLGLetterSchema.setSerialNumber(String.valueOf(serialNumber++));
        tLGLetterSchema.setLetterType("1"); //非核保函件
        tLGLetterSchema.setSendObj("3"); //下发对象为销售部门
        tLGLetterSchema.setBackFlag("1"); //需要回销
        tLGLetterSchema.setBackDate(PubFun.calDate(mCurrentDate,3,"D","2000-1-1"));
        tLGLetterSchema.setLetterSubType("8"); //内部流转
        tLGLetterSchema.setState("0"); //待下发
        tLGLetterSchema.setRemark("迁入销售");

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("lcsalechnl");
        tLDCodeDB.setCode(tLPContDB.getSaleChnl());
        if(!tLDCodeDB.getInfo())
        {
            CError.buildErr(this, "销售渠道查询错误");
            return false;
        }

        String title = "保单" + contNo + " 申请迁移到本管理机构，保单销售渠道为" +
                       tLDCodeDB.getCodeName() + "(" + tLPContDB.getSaleChnl() +
                       "),请指定新的业务员"
                       ;
        tLGLetterSchema.setLetterInfo(title);
        tLGLetterSchema.setOperator(mGlobalInput.Operator);
        tLGLetterSchema.setMakeDate(PubFun.getCurrentDate());
        tLGLetterSchema.setMakeTime(PubFun.getCurrentTime());
        tLGLetterSchema.setModifyDate(PubFun.getCurrentDate());
        tLGLetterSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLGLetterSchema, "DELETE&INSERT");
        //如果有健管险种还要生成健管函件
        String tsql = "select count(0) from lmriskapp where risktype ='M' "
                      +"and riskcode in(select riskcode from lcpol where contno ='"+contNo+"')"
                      ;
        String re = tExeSQL.getOneValue(tsql);
        if(!re.equals("0"))
        {
            tLGLetterSchema = new LGLetterSchema();
            tLGLetterSchema.setEdorAcceptNo(edorNo);
            tLGLetterSchema.setSerialNumber(String.valueOf(serialNumber++));
            tLGLetterSchema.setLetterType("1"); //非核保函件
            tLGLetterSchema.setSendObj("2"); //下发对象为销售部门
            tLGLetterSchema.setBackFlag("0"); //不需要回销
            tLGLetterSchema.setLetterSubType("8"); //内部流转
            tLGLetterSchema.setState("0"); //待下发
            tLGLetterSchema.setRemark("迁入健管");
            title = "保单" + contNo + "包含有健管险种申请迁移到本管理机构"
                    ;
            tLGLetterSchema.setLetterInfo(title);
            tLGLetterSchema.setOperator(mGlobalInput.Operator);
            tLGLetterSchema.setMakeDate(PubFun.getCurrentDate());
            tLGLetterSchema.setMakeTime(PubFun.getCurrentTime());
            tLGLetterSchema.setModifyDate(PubFun.getCurrentDate());
            tLGLetterSchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLGLetterSchema, "DELETE&INSERT");
        }
        mEdorItemSpecialData.add("dealstate", "1");
        mMap.put(mEdorItemSpecialData.getSpecialDataSet(), "DELETE&INSERT");
        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 获得已有函件的序号
     * @return String
     */
    private int getSerialNumber(String aEdorNo) {
        //得到序号
        String sql = "select * from LGLetter " +
                     "where EdorAcceptNo = '" + aEdorNo + "' " +
                     "order by int(SerialNumber) desc ";
        LGLetterDB db = new LGLetterDB();
        LGLetterSet set = db.executeQuery(sql);
        if (set.size() == 0) {
            return 1;
        }
        return Integer.parseInt(set.get(1).getSerialNumber()) + 1;

    }

    private boolean taskDeliver(String workno, String workBoxNo) {
        String tCopyFlag = "N"; //非复制转交
        String tDeliverType = "0"; //转交

        //输入参数
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
     
        tLGTraceNodeOpSchema.setWorkNo(workno);
        tLGTraceNodeOpSchema.setOperatorNo("0");
     
        //080220 zhanggm 
        String sql = "select CodeName('station','" + mGlobalInput.ManageCom + "') from dual";
        ExeSQL tExeSQL = new ExeSQL();
        String outCom = tExeSQL.getOneValue(sql);
        String autoRemark = "工单" + workno + "从机构-" + outCom + "-迁入到" ;
        sql = "select OwnerNo from LGWorkBox where workboxno = '"+workBoxNo+"'";
        String inCom = tExeSQL.getOneValue(sql);
        autoRemark += inCom + "的信箱中";

        tLGTraceNodeOpSchema.setRemark(autoRemark);
        tLGTraceNodeOpSchema.setOperatorType("1");
        tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);
        //得到新的作业结点
        sql = "Select Case When max(to_number(NodeNo)) Is Null " +
                     " Then 0 Else max(to_number(NodeNo))+1 End " +
                     "From   LGWorkTrace Where  WorkNo = '" + workno + "' ";
        SSRS tSSRS = tExeSQL.execSQL(sql);
        String tNodeNo = tSSRS.GetText(1, 1);
        tLGTraceNodeOpSchema.setNodeNo(tNodeNo);
        mMap.put(tLGTraceNodeOpSchema, "INSERT"); //插入
        //生成作业历史轨迹
        tLGWorkTraceSchema.setWorkNo(workno);
        tLGWorkTraceSchema.setNodeNo(tNodeNo);
        tLGWorkTraceSchema.setWorkBoxNo(workBoxNo);
        tLGWorkTraceSchema.setInMethodNo("2"); //接收
        tLGWorkTraceSchema.setInDate(mCurrentDate);
        tLGWorkTraceSchema.setInTime(mCurrentTime);
        tLGWorkTraceSchema.setSendComNo(mGlobalInput.ManageCom);
        tLGWorkTraceSchema.setSendPersonNo(mGlobalInput.Operator);
        tLGWorkTraceSchema.setOperator(mGlobalInput.Operator);
        tLGWorkTraceSchema.setMakeDate(mCurrentDate);
        tLGWorkTraceSchema.setMakeTime(mCurrentTime);
        tLGWorkTraceSchema.setModifyDate(mCurrentDate);
        tLGWorkTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLGWorkTraceSchema, "INSERT"); //插入
        String operator = getOperator(workBoxNo);
        if (operator.equals("")) {
            operator = mGlobalInput.Operator;
        }

        sql = "Update LGWork set NodeNo = '" + tNodeNo +
              "', currDoing='0',Operator = '"
              + operator + "', ModifyDate = '" + mCurrentDate +
              "',ModifyTime = '"
              + mCurrentTime + "' Where  WorkNo = '" + workno + "' "
              ;
        mMap.put(sql, "UPDATE"); //修改
        return true;

    }
    private String getOperator(String workBoxNo)
    {
        LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
        tLGWorkBoxDB.setWorkBoxNo(workBoxNo);
        tLGWorkBoxDB.getInfo();
        //个单
        if(tLGWorkBoxDB.getOwnerTypeNo().equals("2"))
        {
            return tLGWorkBoxDB.getOwnerNo();
        }
        else
        {
            return "";
        }
    }
}
