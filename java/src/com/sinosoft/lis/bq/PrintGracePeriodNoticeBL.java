package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintGracePeriodNoticeBL {
	public CErrors mErrors = new CErrors();
	private GlobalInput mGlobalInput; 
	private TransferData mTransferData =new TransferData();
	private String mContNo;
	private double mPrem;
	private XmlExport mXmlExport = new XmlExport();
	private TextTag textTag = new TextTag();
	private ExeSQL mExeSQL = new ExeSQL();
	public XmlExport getXmlExport(VData data, String operate){
		if (!getInputData(data)) {
			return null;
		}

		if (!dealData()) {
			return null;
		}

		return mXmlExport;
		 
	}
	private boolean getInputData(VData data){
		mGlobalInput =(GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		mTransferData=(TransferData)data.getObjectByObjectName("TransferData", 0);

		mContNo =(String) mTransferData.getValueByName("ContNo");
	
		mPrem = Double.parseDouble((String)mTransferData.getValueByName("Prem"));
		
		return true;
	}
	private boolean dealData() {
		setEleValues();
       
		if(!setAppntInfor())
        {
            return false;
        }
		mXmlExport.createDocument("PGracePeriodContNotice.vts", "printer");
		mXmlExport.addTextTag(textTag);
		return true;
	}
	private void setEleValues(){
		
		String tManageCom=getManageCom();
		
		//获取并设置机构信息
		LDComDB tLDComDB = new LDComDB();
		tLDComDB.setComCode(tManageCom);
		tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Operator", mGlobalInput.Operator);
		
        String agentCode = getAgentCode();
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(agentCode);
        tLaAgentDB.getInfo();
        textTag.add("AgentName", StrTool.cTrim(tLaAgentDB.getName()));
        textTag.add("AgentCode", StrTool.cTrim(tLaAgentDB.getAgentCode()));
        
        String agentPhone = "";
        String temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null"))
        {
            agentPhone = tLaAgentDB.getPhone();
        } else
        {
            agentPhone = temPhone;
        }
        textTag.add("Phone", StrTool.cTrim(agentPhone));
        
      //业务员机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLaAgentDB.getAgentGroup());
        tLABranchGroupDB.getInfo();
       
        textTag.add("AgentGroup", StrTool.cTrim(tLABranchGroupDB.getName()));
     
        //获取宽限期始末日期
        String tStartDate ="";
        String tEndDate ="";
        LCContStateSchema tLCContStateSchema= new LCContStateSchema();
        LCContStateDB tLCContStateDB =new LCContStateDB();
        LCContStateSet tLCContStateSet=new LCContStateSet();
        String sql="select * from LCContState where contNo='"+mContNo+"'";
        tLCContStateSet =tLCContStateDB.executeQuery(sql);
        tLCContStateSchema = tLCContStateSet.get(1);
        tStartDate = tLCContStateSchema.getStartDate();
        tEndDate = tLCContStateSchema.getEndDate();
        textTag.add("StartDate",tStartDate);
        textTag.add("EndDate", tEndDate);
        
        textTag.add("Prem", mPrem);
       
        double tAccBala=0.0;
        String tAccBalaSql="select insuAccBala from LCInsureAcc  where contno='"+mContNo+"' with ur";
     
        tAccBala =Double.parseDouble( mExeSQL.getOneValue(tAccBalaSql));
       
        textTag.add("AccBala", tAccBala);
        
	}
	private String getManageCom(){
		String tManageCom="";
		LCContDB tLCContDB =new LCContDB() ;
		LCContSchema tLCContSchema=new  LCContSchema();
		LCContSet tLCContSet =new LCContSet();
		String sql = "select * from LCCont where contno ='"+mContNo+"' ";
		tLCContSet = tLCContDB.executeQuery(sql);
		tLCContSchema = tLCContSet.get(1);
		tManageCom =tLCContSchema.getManageCom();
		return tManageCom;
	}
	
	//设置投保人信息
	private boolean setAppntInfor(){
		LCContDB tLCContDB =new LCContDB() ;
		LCContSchema tLCContSchema=new  LCContSchema();
		LCContSet tLCContSet =new LCContSet();
		String sql = "select * from LCCont where contno ='"+mContNo+"' ";
		tLCContSet = tLCContDB.executeQuery(sql);
		tLCContSchema = tLCContSet.get(1);
		//设置投保人名字
		textTag.add("AppntName", tLCContSchema.getAppntName());
		
		String tAppntNo =tLCContSchema.getAppntNo();
		
		textTag.add("AppntNo", tAppntNo);
		LCAppntSchema tLCAppntSchema=new LCAppntSchema();
		LCAppntDB  tLCAppntDB =new LCAppntDB();
		LCAppntSet tLCAppntSet=new LCAppntSet();
		String tAppntSql="select * from LCAppnt where contno='"+mContNo+"' and appntno='"+tAppntNo+"'  ";
		tLCAppntSet =tLCAppntDB.executeQuery(tAppntSql);
		tLCAppntSchema = tLCAppntSet.get(1);
		String tAddressNo= tLCAppntSchema.getAddressNo();
		
		LCAddressSchema tLCAddressSchema =new LCAddressSchema();
		LCAddressDB tLCAddressDB=new LCAddressDB();
		LCAddressSet tLCAddressSet=new LCAddressSet();
		String tAddressSql = "select * from LCAddress where customerNo='"+tAppntNo+"' and addressno ='"+tAddressNo+"'";
		tLCAddressSet =tLCAddressDB.executeQuery(tAddressSql);
		tLCAddressSchema =tLCAddressSet.get(1);
		
		String tAddress =tLCAddressSchema.getHomeAddress();
		String tZipCode =tLCAddressSchema.getHomeZipCode();
	
		textTag.add("Address", tAddress);
		textTag.add("ZipCode", tZipCode);
		textTag.add("Mobile", tLCAddressSchema.getMobile());
		
		return true;
	}
	private String getAgentCode(){
		String tAgentCode="";
		String tContNo = mContNo;
		String sql ="select AgentCode "
            + "from LCCont "
            + "where ContNo = '" + tContNo + "' "
            + "union "
            + "select AgentCode "
            + "from LBCont "
            + "where ContNo = '" + tContNo + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		tAgentCode = tExeSQL.getOneValue(sql);
		
		if(tAgentCode!= "" && !tAgentCode.equals("null"))
        {
            return tAgentCode;
        }
		return tAgentCode;
	}
}
