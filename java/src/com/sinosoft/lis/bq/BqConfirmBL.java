package com.sinosoft.lis.bq;

import java.util.Set;
import java.util.HashSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Blob;
import com.sinosoft.lis.taskservice.InsuAccBalaTask;
import com.sinosoft.lis.brms.databus.RuleTransfer;
import org.jdom.Document;

/**
 * <p>Title: 保全确认</p>
 * <p>Description: 保全确认 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class BqConfirmBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mUWState = null;

    private String mLetterState = null;

    private String mContType = null;

    private String mBalanceMethodValue = null;

    private String mMessage = "";

    private MMap mMap = new MMap();

    private String mFailType = BQ.FALSE;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private Document doc ;

    private String mPolPassFlag = "0"; //险种通过标记

    private String mContPassFlag = "0"; //合同通过标记

    private String mAllPolPassFlag = "9";


    public BqConfirmBL(GlobalInput gi, String edorNo, String contype,
                       String balanceMethodValue)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mContType = contype;
        this.mBalanceMethodValue = balanceMethodValue;
    }

    /**
     * 提交数据
     * @param operator String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
        	dealUrgeLog("1", "DELETE");
            createRemark(false);
            return false;
        }
        //工单结案后，对万能险进行月结，用来处理因保全导致的未结算
        if (mContType.equals(BQ.CONTTYPE_P))
        {
            balaULI();
        }
        createRemark(true);
        return true;
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 自动审批是否成功标记
     * @return String
     */
    public String getFailType()
    {
        return mFailType;
    }

    /**
     * 判断自动核保是否成功
     * 成功返回"1"，失败返回"0"
     * @return String
     */
    public String autoUWFail()
    {
        if (this.mUWState == null)
        {
            return BQ.TRUE;
        }
        if (this.mUWState.equals(BQ.UWFLAG_FAIL))
        {
            return BQ.FALSE;
        }
        return BQ.TRUE;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	 try 
         {
    		 if(!checkScanning())
    			 return false;
    	if (!dealUrgeLog("1", "INSERT"))
		   {
    		     dealUrgeLog("1", "DELETE");
		         return false;
		   }
    	
        if (mContType.equals(BQ.CONTTYPE_P))
        {
            if (!getUWState())
            {
            	dealUrgeLog("1", "DELETE");
                return false;
            }

            if (!autoUW())
            {
            	dealUrgeLog("1", "DELETE");
                return false;
            }

            if (!checkUWState())
            {
            	dealUrgeLog("1", "DELETE");
                return false;
            }
        }

        if (!autoExamination())
        {
        	dealUrgeLog("1", "DELETE");
            return false;
        }

        if (mContType.equals(BQ.CONTTYPE_P))
        {
            if (!updatePrint())
            {
            	dealUrgeLog("1", "DELETE");
                return false;
            }
        }

        if (!dealAppAcc())
        {
        	dealUrgeLog("1", "DELETE");
            return false;
        }

        if (!dealFinanceData())
        {
        	dealUrgeLog("1", "DELETE");
        	delAppAcc();
        	
            return false;
        }

        if (!edorFinish())
        {
        	dealUrgeLog("1", "DELETE");
//        	20111025 【OoO?】删除ljaget冗余数据
        	delLJAGET();
        	//
        	delAppAcc();
            return false;
        }
       
        }catch (Exception e) 
        {
 			e.printStackTrace();
 			dealUrgeLog("1", "DELETE");
 			this.mErrors.addOneError("该工单正在进行保全确认,请不要重复操作!");
 			return false;
 	    }
        return true;
    }

    /**
     * 更新批单内容
     * @return boolean
     */
    private boolean updatePrint()
    {
        InputStream ins = null;
        String sql = "select * from LPEDORAPPPRINT "
                     + "where EdorAcceptNo = '" + mEdorNo + "' ";
        Connection conn = DBConnPool.getConnection();

        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next())
            {
                Blob tBlob = rs.getBlob("EdorInfo");
                ins = tBlob.getBinaryStream();
            } else
            {
                CError tError = new CError();
                tError.moduleName = "BqConfirmBL";
                tError.functionName = "updatePrint";
                tError.errorMessage = "没有查询到批单信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e)
        {
        	e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BqConfirmBL";
            tError.functionName = "updatePrint";
            tError.errorMessage = "查询批单信息出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        XmlUpdate xmlUpdate = new XmlUpdate(ins);
        TextTag textTag = new TextTag();
        textTag.add("Operator", GetMemberInfo.getMemberName(mGlobalInput));
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        xmlUpdate.addTextTag(textTag);
        xmlUpdate.update();

        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(mEdorNo);
        tLPEdorAppPrintDB.getInfo();
        tLPEdorAppPrintDB.setEdorInfo(xmlUpdate.getInputStream());

        sql = "delete from LPEdorAppPrint " +
              "where EdorAcceptNo = '" + mEdorNo + "' ";
        MMap tMMap = new MMap();
        tMMap.put(sql, "DELETE");
        tMMap.put(tLPEdorAppPrintDB.getSchema(), "BLOBINSERT");
        VData d = new VData();
        d.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(d, ""))
        {
            CError tError = new CError();
            tError.moduleName = "BqConfirmBL";
            tError.functionName = "updatePrint";
            tError.errorMessage = "更新批单信息出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 是否需要自动核保
     * @return boolean
     */
    private boolean needAutoUW()
    {
        /*
                 //如果核保状态是自动核保通过和经过人核保工则不能自动核保
                 if ((mUWState != null) &&
                (mUWState.equals(BQ.UWFLAG_MANU) ||
                 mUWState.equals(BQ.UWFLAG_PASS)))
                 {
            return false;
                 }
                 //如果人工核保结论中有数据则不能自动核保
                 LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
                 tLPUWMasterDB.setEdorNo(mEdorNo);
                 if (tLPUWMasterDB.query().size() > 0)
                 {
            return false;
                 }
         */
        if ((mUWState == null) || (mUWState.equals("")))
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * 自动核保
     * @return boolean
     */
    private boolean autoUW()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String isHumanUWSQL = "select 1 from lpedoritem where edortype in (select code from ldcode where codetype = 'BQUW') and edorno = '"+mEdorNo+"'";
        String needHumanUW = tExeSQL . getOneValue(isHumanUWSQL);
        if("1".equals(needHumanUW)){
        	PEdorAutoUWBL tPEdorAutoUWBL = new PEdorAutoUWBL(mGlobalInput, mEdorNo);
            if (!tPEdorAutoUWBL.submitData()) {
                mErrors.addOneError("自动核保失败！原因是：" +
                                    tPEdorAutoUWBL.mErrors.getFirstError());
                return false;
            }
            this.mUWState = tPEdorAutoUWBL.getUWState();
            System.out.println("mUWState:" + mUWState);
            if (tPEdorAutoUWBL.getUWState().equals(BQ.UWFLAG_FAIL)) {
                mMessage = "自动核保未通过，已送人工核保。原因如下：\n" +
                           tPEdorAutoUWBL.getUWErrors();
                return false;
            }
        }
        //获得当前规则引擎状态 modify by fuxin
        //00为规则引擎开启状态，其它状态为关闭状态
        String flag = tExeSQL . getOneValue ("select code From ldcode where codetype ='RuleButton'");
        String uwstate =tExeSQL.getOneValue("select UwState From lpedorapp where edoracceptno ='"+mEdorNo+"'");
        if(flag.equals("00")&& !uwstate.equals(BQ.UWFLAG_MANU))
        {
           //如果是保全 给param 付值时，需要拼批单号和批改类型：Edorno|EdorType
           String EdoeType = tExeSQL.getOneValue("select edorno||'|'||edortype From lpedoritem where edorno='"+mEdorNo+"'");
//           String EdoeType = tExeSQL.getOneValue("select contno From lpedoritem where edorno='"+mEdorNo+"'");
            //调用规则引擎
            RuleTransfer ruleTransfer = new RuleTransfer();

           //获得规则引擎返回的校验信息
           String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "SU", EdoeType, false);
           //将校验信息转换为Xml的document对象
           doc = ruleTransfer.stringToDoc(xmlStr);
           //获得校验结果 1自核通过，-1执行异常，0自核不通过
            String approved = ruleTransfer.getApproved(doc);
            LMUWSet tLMUWSet = ruleTransfer.getAllPolUWMSG(doc);

            if ("1".equals(approved)) {

                this.mUWState = BQ.UWFLAG_PASS;
                setUWState(false);
                return true ;
            } else if ("0".equals(approved) || "2".equals(approved)) {
                //处理自核不通过的业务数据。
                RulePEdorAutoUWBL tRulePEdorAutoUWBL= new RulePEdorAutoUWBL(mGlobalInput, mEdorNo,tLMUWSet);
                tRulePEdorAutoUWBL.submitData();
               tLMUWSet = ruleTransfer.getAllPolUWMSG(doc);

               for(int m=1;m<=tLMUWSet.size();m++){
                  String tempRiskcode = "";
                  String tempInsured = "";
                   if (tLMUWSet.get(m).getRiskName() != null
                       && !tLMUWSet.get(m).getRiskName().equals("")) {
                       tempInsured += "被保人" + tLMUWSet.get(m).getRiskName();
                       if (tLMUWSet.get(m).getRiskCode() != null
                           && !tLMUWSet.get(m).getRiskCode().equals("000000")
                           && !tLMUWSet.get(m).getRiskCode().equals("")) {
                           tempRiskcode += "，险种代码" + tLMUWSet.get(m).getRiskCode() + "，";
                       } else {
                           tempInsured += "，";
                       }
                   } else {
                       if (tLMUWSet.get(m).getRiskCode() != null
                           && !tLMUWSet.get(m).getRiskCode().equals("000000")
                           && !tLMUWSet.get(m).getRiskCode().equals("")) {
                           tempRiskcode += "险种代码" + tLMUWSet.get(m).getRiskCode() + "，";
                       }
                   }
                   mMessage += tLMUWSet.get(m).getUWCode();
                   mMessage += "：";
                   mMessage += tempInsured;
                   mMessage += tempRiskcode;
                   mMessage += tLMUWSet.get(m).getRemark();
                   mMessage += "<br>";
               }
               System.out.println(mMessage);
                return false ;
            } else if ("-1".equals(approved)) {
                return false;
            } else {
                return false;
            }

        }
        else if(uwstate.equals(BQ.UWFLAG_MANU))
        {
            //人工核保通过。
            return true;
        }
        else
        {
            PEdorAutoUWBL tPEdorAutoUWBL = new PEdorAutoUWBL(mGlobalInput, mEdorNo);
            if (!tPEdorAutoUWBL.submitData()) {
                mErrors.addOneError("自动核保失败！原因是：" +
                                    tPEdorAutoUWBL.mErrors.getFirstError());
                return false;
            }
            this.mUWState = tPEdorAutoUWBL.getUWState();
            System.out.println("mUWState:" + mUWState);
            if (tPEdorAutoUWBL.getUWState().equals(BQ.UWFLAG_FAIL)) {
                mMessage = "自动核保未通过，已送人工核保。原因如下：\n" +
                           tPEdorAutoUWBL.getUWErrors();
                return false;
            }
        }
        return true;
    }

    /**
     * 自动审批
     * @return boolean
     */
    private boolean autoExamination()
    {
        VData data = new VData();
        LGWorkSchema workSchema = new LGWorkSchema();
        workSchema.setWorkNo(mEdorNo);
        data.add(mGlobalInput);
        data.add(workSchema);
        TaskAutoExaminationBL exame = new TaskAutoExaminationBL();
        if (!exame.submitData(data, ""))
        {
            //送审批成功，则本类错误信息中保存送审批原因
            if (exame.sendConfirmSuccess())
            {
                mErrors.copyAllErrors(exame.getSendConfirmReason());
                mFailType = BQ.TRUE;
            } else
            {
                mErrors.copyAllErrors(exame.mErrors);
                mFailType = BQ.FALSE;
            }
            return false;
        }
        return true;
    }

    /**
     * 得到函件状态和核保状态
     * @return String
     */
    private boolean getUWState()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("未找到保全受理信息！");
            return false;
        }
        this.mUWState = tLPEdorAppDB.getUWState();
        //if ((mUWState == null) || mUWState.equals(""))
        //{
        //    mErrors.addOneError("未找到核保状态！");
        //    return false;
        //}
        return true;
    }

    /**
     * 得到函件状态
     * @return boolean
     */
    private boolean getLetterState()
    {
        String sql = "select State from LGLetter " +
                     "where EdorAcceptNo = '" + mEdorNo + "' " +
                     "order by SerialNumber desc ";
        this.mLetterState = (new ExeSQL()).getOneValue(sql);
        if ((mLetterState == null) || mLetterState.equals(""))
        {
            mErrors.addOneError("未找到函件状态！");
            return false;
        }
        return true;
    }

    /**
     * 检查核保状态，如果核保状态为未通过则不能结案
     * @return boolean
     */
    private boolean checkUWState()
    {
        if ((mUWState == null) || (mUWState.equals("")))
        {
            mErrors.addOneError("未找到核保标志！");
            return false;
        }
        if (mUWState.equals(BQ.UWFLAG_FAIL))
        { //如果自动核保未通过
            mErrors.addOneError("该保全未通过自动核保！");
            return false;
        }
        if (mUWState.equals(BQ.UWFLAG_MANU))
        { //如果人工核保完毕
            if (!checkPassFlag())
            { //校验人工核保结论
                return false;
            }
        }
        return true;
    }

    /**
     * 检查人工核保，如果未按照人工核保的结论操作则不能结案
     * @return boolean
     */
    private boolean checkPassFlag()
    {
        if (!getLetterState())
        {
            return false;
        }
        boolean flag = true;
        CTTask tCTTask = new CTTask(mGlobalInput, mEdorNo);
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mEdorNo);
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
        for (int i = 1; i <= tLPUWMasterSet.size(); i++)
        {
            LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(i);
            String edorType = tLPUWMasterSchema.getEdorType();
            String contNo = tLPUWMasterSchema.getContNo();
            String appntNo = tLPUWMasterSchema.getAppntNo();
            String polNo = tLPUWMasterSchema.getPolNo();
            String insuredNo = tLPUWMasterSchema.getInsuredNo();
            String insuredName = CommonBL.getInsuredName(insuredNo);
            String passFlag = tLPUWMasterSchema.getPassFlag();
            if ((passFlag == null) || (passFlag.equals("")))
            {
                mErrors.addOneError("未找到" + edorType + "项目的人工核保结论!");
                return false;
            }
            //如果函件未回销
            if (!passFlag.equals(BQ.PASSFLAG_PASS) &&
                (!mLetterState.equals("3") && //3是回销
                 !mLetterState.equals("4")))
            { //4是强制回销
                mErrors.addOneError("核保函件未回销！");
                return false;
            }
            if (passFlag.equals(BQ.PASSFLAG_PASS))
            { //人工核保通过
            } else if (passFlag.equals(BQ.PASSFLAG_STOP))
            { //需要撤销该项目
//                 LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
//                 tLPEdorItemDB.setEdorNo(mEdorNo);
//                 tLPEdorItemDB.setEdorType(edorType);
//                 tLPEdorItemDB.setInsuredNo(insuredNo);
//                 if (tLPEdorItemDB.query().size() != 0)
//                 {
//                     mErrors.addOneError("需要撤销被保人" + insuredName +
//                             "(" + insuredNo + ")申请的" + edorType + "项目！");
//                     return false;
//                 }
//                LPPolDB tLPPolDB = new LPPolDB();
//                tLPPolDB.setEdorNo(mEdorNo);
//                tLPPolDB.setEdorType(edorType);
//                tLPPolDB.setInsuredNo(insuredNo);
//                if (tLPPolDB.query().size() > 0)
//                {
//                    mErrors.addOneError("需要撤销" + edorType + "项目" +
//                            "被保人" + insuredName + "(" + insuredNo +
//                            ")的变更申请！");
//                    return false;
//                }
            } else if (passFlag.equals(BQ.PASSFLAG_CANCEL))
            { //需要险种解约
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(polNo);
                if (tLCPolDB.getInfo())
                {
                    tCTTask.addCTPol(tLCPolDB.getSchema());
                } else
                { //如果已解约掉则删掉应收
//                    String sql = "delete from LJSGetEndorse " +
//                            "where EndorseMentNo = '" + mEdorNo + "' " +
//                            "and PolNo = '" + polNo + "' ";
//                    mMap.put(sql, "DELETE");
//                    System.out.println(sql);
                }
            } else if (passFlag.equals(BQ.PASSFLAG_CONDITION))
            { //条件通过
                String customerReply = tLPUWMasterSchema.getCustomerReply();
                if (customerReply == null)
                {
                    mErrors.addOneError("核保函件回销时未录入客户意见！");
                    return false;
                }

                if ((customerReply != null) && customerReply.equals("1"))
                { //同意
                } else if ((customerReply != null) && customerReply.equals("2"))
                { //不同意
                    String disagreeDeal = tLPUWMasterSchema.getDisagreeDeal();
                    if (disagreeDeal == null)
                    {
                        mErrors.addOneError("核保时未录入客户不同意处理方式！");
                        return false;
                    }
                    if (disagreeDeal.equals("1"))
                    { //终止申请
//                        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
//                        tLPEdorItemDB.setEdorNo(mEdorNo);
//                        tLPEdorItemDB.setEdorType(edorType);
//                        tLPEdorItemDB.setInsuredNo(insuredNo);
//                        if (tLPEdorItemDB.query().size() != 0)
//                        {
//                            mErrors.addOneError("由于客户不同意附加条件，需要撤销被保人" +
//                                    insuredName +
//                                    "(" + insuredNo + ")申请的" +
//                                    edorType + "项目！");
//                            return false;
//                        }
                    } else if (disagreeDeal.equals("2"))
                    { //险种解约
                        LCPolDB tLCPolDB = new LCPolDB();
                        tLCPolDB.setPolNo(polNo);
                        if (tLCPolDB.getInfo())
                        {
                            tCTTask.addCTPol(tLCPolDB.getSchema());
                        } else
                        {
//                            String sql = "delete from LJSGetEndorse " +
//                                    "where EndorseMentNo = '" + mEdorNo + "' " +
//                                    "and PolNo = '" + polNo + "' ";
//                            mMap.put(sql, "DELETE");
//                                  System.out.println(sql);
                        }
                    }
                }
            }
        }

//        if (mMap.size() > 0)
//        {
//            if (!submit())
//            {
//                return false;
//            }
//            updateItemMoney();
//            updateMainMoney();
//            updateAppMoney();
//            if (!submit())
//            {
//                return false;
//            }
//            rebuildVts();
//        }

        if (tCTTask.getPolSize() > 0)
        {
            if (!tCTTask.createCTTask())
            {
                return false;
            }
            String[] tCTWorks = tCTTask.getWorks();
            if (tCTWorks.length > 0)
            {
                String error = tCTTask.getRemark() + "生成工单";
                for (int i = 0; i < tCTWorks.length; i++)
                {
                    error += tCTWorks[i];
                    if (i < tCTWorks.length - 1)
                    {
                        error += "，";
                    }
                }
                error += "！\n";
                mErrors.addOneError(error);
                return false;
            }
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    private boolean dealAppAcc()
    {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setOtherNo(mEdorNo);
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mEdorNo,
                "YE");
        VData tVData = new VData();
        tVData.add(tSpecialData);
        tVData.add(tLCAppAccTraceSchema);
        tVData.add(mGlobalInput);
        PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
        if (!tPEdorAppAccConfirmBL.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(tPEdorAppAccConfirmBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 处理财务数据，把补退费金额放入财务接口
     * @return boolean
     */
    private boolean dealFinanceData()
    {
        String noticeType;
        if (mContType.equals(BQ.CONTTYPE_P))
        {
            noticeType = BQ.NOTICETYPE_P;
        } else
        {
            noticeType = BQ.NOTICETYPE_G;
        }
        FinanceDataBL tFinanceDataBL = new FinanceDataBL(mGlobalInput, mEdorNo,
                noticeType, mBalanceMethodValue);
        if (!tFinanceDataBL.submitData())
        {
            mErrors.addOneError("设置财务数据失败！原因是：" +
                                tFinanceDataBL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * 保全结案
     * @return boolean
     */
    private boolean edorFinish()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mEdorNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        int ret = this.queryLCGrpBalPlan();
        if (tLJSPaySet.size() > 0 && ret != 1)
        {
            LJSPaySchema tLJSPaySchema = tLJSPaySet.get(1);
            double getMoney = tLJSPaySchema.getSumDuePayMoney();
            if (getMoney > 0)
            {
                mErrors.addOneError("需要财务交费！");
                return true;
            }
        }
        EdorFinishBL tEdorFinishBL = new EdorFinishBL(mGlobalInput, mEdorNo);
        if (!tEdorFinishBL.submitData(mContType))
        {
            mErrors.addOneError("保全确认未通过！原因是：" +
                                tEdorFinishBL.mErrors.getFirstError());
            return false;
        }
        if (tEdorFinishBL.getMessage() != null)
        {
            mMessage = tEdorFinishBL.getMessage();
        }
        return true;
    }

    /**
     * 生成作业历史的自动批注
     * @return boolean
     */
    private boolean createRemark(boolean flag)
    {
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(mEdorNo);
        tLGTraceNodeOpSchema.setOperatorType(Task.HISTORY_TYPE_CONFIRM);
        String edorState = CommonBL.getEdorState(mEdorNo);
        if (edorState == null)
        {
            mMessage = "没有生成作业历史信息！原因是：未找到保全状态！";
            return false;
        }
        if (edorState.equals(BQ.EDORSTATE_WAITPAY))
        { //如果是待收费状态则确认成功
            tLGTraceNodeOpSchema.setRemark("自动批注：保全确认成功。");
        } else
        {
            if (flag == true)
            {
                tLGTraceNodeOpSchema.setRemark("自动批注：保全确认成功。");
            } else
            {
                tLGTraceNodeOpSchema.setRemark("自动批注：保全确认失败！");
            }
        }
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLGTraceNodeOpSchema);
        TaskTraceNodeOpBL bl = new TaskTraceNodeOpBL();
        if (!bl.submitData(data, ""))
        {
            mMessage += "没有生成作业历史信息！原因是：" + bl.mErrors.getFirstError();
        }
        return true;
    }

    //查询是否是定期结算的保单
    private int queryLCGrpBalPlan()
    {
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorAcceptNo(mEdorNo);
        tLPGrpEdorMainDB.setEdorNo(mEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet == null)
        {
            if (tLPGrpEdorMainDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLPGrpEdorMainDB.mErrors);
                return -1;
            }
        }
        if (tLPGrpEdorMainSet.size() == 0)
        {
            return 0;
        }
        //获得团体合同号
        String grpContNo = tLPGrpEdorMainSet.get(1).getGrpContNo();

        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(grpContNo);
        LCGrpBalPlanSet tLCGrpBalPlanSet = tLCGrpBalPlanDB.query();
        //查询定期结算计划出错
        if (tLCGrpBalPlanSet == null)
        {
            if (tLCGrpBalPlanDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
                return -1;
            }
        }
        //不存在定期结算计划或者为随时结算
        if (tLCGrpBalPlanSet.size() == 0)
        {
            return 0;
        }
        LCGrpBalPlanSchema tLCGrpBalPlanSchema = tLCGrpBalPlanSet.get(1);
//        if (!tLCGrpBalPlanSchema.getState().equals("0")) {
//            CError tError = new CError();
//            tError.moduleName = "ValidateFinanceData";
//            tError.functionName = "queryLCGrpBalPlan";
//            tError.errorMessage = "该保单正处于结算过程当中，不能进行保全操作!";
//            this.mErrors.addOneError(tError);
//            return 2;
//        }
        //为随时结算
        if (tLCGrpBalPlanSchema.getBalIntv() == 0)
        {
            return 0;
        }
        //如果选择了延期计算
        if (this.queryEspecialDate() == 1)
        {
            return 0;
        }
        return 1;
    }

    /**
     * 返回值1，表示即时计算。
     * @return int
     */
    private int queryEspecialDate()
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(this.mEdorNo);
        tLPEdorEspecialDataDB.setEdorNo(this.mEdorNo);
        tLPEdorEspecialDataDB.setEdorType("DJ");
        tLPEdorEspecialDataDB.setDetailType("BALTYPE");
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.
                query();

        if (tLPEdorEspecialDataSet == null)
        {
            if (tLPEdorEspecialDataSet.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLPEdorEspecialDataSet.mErrors);
                return -1;
            }
        }
        if (tLPEdorEspecialDataSet.size() == 0)
        {
            return 0;
        } else if (tLPEdorEspecialDataSet.get(1).getEdorValue().equals("1"))
        {
            return 1;
        }
        return 0;
    }

    private void balaULI()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(this.mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            if (CommonBL.hasULIRisk(tLPEdorItemSet.get(i).getContNo()))
            {
                InsuAccBalaTask tInsuAccBalaTask = new InsuAccBalaTask();
                tInsuAccBalaTask.runOneCont(tLPEdorItemSet.get(i).getContNo());
            }
        }
    }

    //20090616 zhanggm 针对dealAppAcc()和dealFinanceData(),删除lcappacctrace冗余数据,修改lcappacc,原则:不阻断
    private void delAppAcc()
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	String sql = "select customerno from LCAppAccTrace where otherno = '" +mEdorNo+ "' ";
    	String tCustomerNo = tExeSQL.getOneValue(sql);
    	if(!"".equals(tCustomerNo) && tCustomerNo!=null && !"null".equals(tCustomerNo))
    	{
    		LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        	tLCAppAccDB.setCustomerNo(tCustomerNo);
        	LCAppAccSchema tLCAppAccSchema = null;
        	if(tLCAppAccDB.getInfo())
        	{
        		tLCAppAccSchema = tLCAppAccDB.getSchema();
        		sql = "select accbala from LCAppAccTrace where customerno = '" + tCustomerNo
        	    	+ "' and otherno != '" +mEdorNo+ "' order by int(serialno) desc";
        		String tAccBala = tExeSQL.getOneValue(sql);
        		tLCAppAccSchema.setAccBala(tAccBala);
        		tLCAppAccSchema.setAccGetMoney(tAccBala);
        		tLCAppAccSchema.setOperator(mGlobalInput.Operator);
        		tLCAppAccSchema.setModifyDate(mCurrentDate);
        		tLCAppAccSchema.setModifyTime(mCurrentTime);
        		String delSql = "delete from LCAppAccTrace where otherno = '" +mEdorNo+ "' ";
        		MMap tMMap = new MMap();
        		tMMap.put(tLCAppAccSchema, SysConst.UPDATE);
        		tMMap.put(delSql, SysConst.DELETE);
        		VData tVData = new VData();
        		tVData.add(tMMap);
        		PubSubmit tPubSubmit = new PubSubmit();
        	    tPubSubmit.submitData(tVData, "");
        	}
    	}
    }
    
    /**20111025 【OoO?】杨天政 dealFinanceData().setLJAGET()是分步提交的数据,如果之后
     * 进行业务数据生成时报错，则会产生LJAGET冗余数据。
     * 此方法是用来删除冗余数据的。
     * 来源：error~~~no10~~~
     */
    private void delLJAGET()
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	String sql = "select otherno from ljaget where othernotype='3' and paymode is null and otherno = '" +mEdorNo+ "' ";
    	String otherno = tExeSQL.getOneValue(sql);
    	if(otherno!=null &&!"".equals(otherno)&& !"null".equals(otherno))
    	{
        		String delSql = "delete from ljaget where othernotype='3' " +
        				"and paymode is null and otherno = '" +mEdorNo+ "' "
        				+ "and not exists (select 1 from lpedorapp where edoracceptno='" + mEdorNo + "' and edorstate='0')";
        		MMap tMMap = new MMap();
        		tMMap.put(delSql, SysConst.DELETE);
        		VData tVData = new VData();
        		tVData.add(tMMap);
        		PubSubmit tPubSubmit = new PubSubmit();
        	    tPubSubmit.submitData(tVData, "");
    	}
    }

    /**
     * 设置核保状态
     */
    private void setUWState(boolean changeEdorState) {
        //更新App表
        String sql = "update LPEdorApp " +
                     "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true) {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorAcceptNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
              "set UWState = '" + mUWState + "' ";
        if (changeEdorState == true) {
            sql += ", EdorState = '" + BQ.EDORSTATE_SENDUW + "' ";
        }
        sql += "where EdorNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
    }
    /**
     * 加入到保全结案日志表数据
     * @param
     * @return boolean
     */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {
    	ExeSQL tExeSQLedortype = new ExeSQL();
    	String EdoeTypeforinsert = tExeSQLedortype.getOneValue("select edortype From lpedoritem where edorno='"+mEdorNo+"' fetch first 1 rows only");
    	System.out.println("select edortype From lpedoritem where edorno='"+mEdorNo+"' fetch first 1 rows only");
    	System.out.println(EdoeTypeforinsert);
    	if(EdoeTypeforinsert.equals("WT")||EdoeTypeforinsert.equals("XT")||EdoeTypeforinsert.equals("CT"))
    	{
        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到保全结案日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mEdorNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setEndDate(mCurrentDate);

        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作，3：保全结案操作
        tLCUrgeVerifyLogSchema.setOperateFlag("3"); //1：个案操作,2：批次操作  3：保全团单结案操作
        tLCUrgeVerifyLogSchema.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("");

        } 
        else if(pmOpreat.equals("UPDATE"))
        {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mEdorNo);
            tLCUrgeVerifyLogDB.setOperateType("3");
            tLCUrgeVerifyLogDB.setOperateFlag("3");
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
            }else{
                return false;
            }
        }


        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("该工单正在进行保全确认!");
            return false;
        }
    	}
        return true;
    }
    /**
     * 新增保全结案时校验需不需要扫描件
     * @return
     */
    public boolean checkScanning(){
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS=new SSRS();
    	if(mContType.equals(BQ.CONTTYPE_P)){
    		String scanSql = "select IsNeedScanning,edorname from lmedoritem where edorcode in (select edortype from lpedoritem where edorno = '"+mEdorNo+"') and appobj = 'I'";
    		tSSRS = tExeSQL.execSQL(scanSql);
    		//据说理赔合同处理只走个，不走团，所以校验加在了此处
    		//校验当前工单是否是理赔合同处理
    		String sql_checkLLContdeal="select count(*) from llcontdeal where 1=1 and EdorNo ='"+mEdorNo+"' ";
    		String llcontdealCount=tExeSQL.getOneValue(sql_checkLLContdeal);
    		if("0".equals(llcontdealCount)){//不是理赔合同处理进入
    		for(int i=1;i<=tSSRS.getMaxRow();i++){
    			String IsNeedScanning = tSSRS.GetText(i, 1);
    			if(IsNeedScanning.equals("1")){
    				String sql = "select 1 from es_doc_relation where bussno = '"+mEdorNo+"'";
    				String result = tExeSQL.getOneValue(sql);
    				if(result==null||"".equals(result)){
    		            this.mErrors.addOneError("保全项目:"+tSSRS.GetText(i, 2)+",需要扫描件才能结案");
    					return false;
    				}
    			}
    		}
    		}
    	}else{
    		String scanSql = "select IsNeedScanning,edorname from lmedoritem where edorcode in (select edortype from lpgrpedoritem where edorno = '"+mEdorNo+"') and appobj = 'G'";
    		tSSRS = tExeSQL.execSQL(scanSql);
    		for(int i=1;i<=tSSRS.getMaxRow();i++){
    			String IsNeedScanning = tSSRS.GetText(i, 1);
    			if(IsNeedScanning.equals("1")){
    				String sql = "select 1 from es_doc_relation where bussno = '"+mEdorNo+"'";
    				String result = tExeSQL.getOneValue(sql);
    				if(result==null||"".equals(result)){
    		            this.mErrors.addOneError("保全项目:"+tSSRS.GetText(i, 2)+",需要扫描件才能结案");
    					return false;
    				}
    			}
    		}
    	}
    	return true;
    }
}
