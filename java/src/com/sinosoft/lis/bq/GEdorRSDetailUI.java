package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:客户资料变更
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @ReWrite ZhangRong,QiuYang
 * @version 1.0
 */
public class GEdorRSDetailUI
{
    private GEdorRSDetailBL mGEdorRSDetailBL = null;

     public GEdorRSDetailUI()
     {
         mGEdorRSDetailBL = new GEdorRSDetailBL();
     }

     /**
      * 调用业务逻辑
      * @param data VData
      * @return boolean
      */
     public boolean submitData(VData data)
     {
         if (!mGEdorRSDetailBL.submitData(data))
         {
             return false;
         }
         return true;
     }

     /**
      * 返回错误信息
      * @return String
      */
     public String getError()
     {
         return mGEdorRSDetailBL.mErrors.getFirstError();
     }
}
