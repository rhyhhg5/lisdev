package com.sinosoft.lis.bq;

import java.io.*;
import org.jdom.input.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 重新生成批单</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class RenewVts
{
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

	/** 保全受理号 */
	private String mEdorAcceptNo;

	/** 保存批单数据的Schema */
	private LPEdorAppPrintSchema mPrintSchema = null;

	/**
	 * 构造函数
	 * @param edorAcceptNo String
	 */
	public RenewVts(String edorAcceptNo)
	{
		this.mEdorAcceptNo = edorAcceptNo;
	}

    /**
     * 重新生成批单，增加退费记录号，在保全确认后调用
     * @param payData TransferData
     */
    public boolean renewNoticeNo()
    {
        try
        {
            XmlExport xmlExport = new XmlExport();
            xmlExport.setDocument(getDocument());
            addNoticeNo(xmlExport); //增加退费记录号
            if (!updateData(xmlExport))
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }


	/**
	 * 重新生成批单，增加交/退费方式，在选择交费方式后调用
	 * @param payData TransferData
	 */
	public boolean renewPayInfo(TransferData payInfo)
	{
		try
		{
			XmlExport xmlExport = new XmlExport();
			xmlExport.setDocument(getDocument());
            createNotice();
			addPayMode(xmlExport, payInfo); //增加交/退费方式
			if (!updateData(xmlExport))
			{
				return false;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 把xml数据转换成Document
	 * @return Document
	 */
	private org.jdom.Document getDocument() throws Exception
	{
		InputStream xmlStream = getXmlStream();
		SAXBuilder saxBuilder = new SAXBuilder();
		org.jdom.Document document = saxBuilder.build(xmlStream);
		return document;
	}

	/**
	 * 从数据库中查出生成批单的xml数据
	 * @return InputStream
	 */
	private InputStream getXmlStream()
	{
		LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
		tLPEdorAppPrintDB.setEdorAcceptNo(mEdorAcceptNo);
		tLPEdorAppPrintDB.getInfo();
		//把查出的数据保存到mPrintSchema中
		mPrintSchema = tLPEdorAppPrintDB.getSchema();
		return tLPEdorAppPrintDB.getEdorInfo();
	}

    private boolean createNotice()
    {
        return true;
    }

    private boolean addNoticeNo(XmlExport xmlExport)
    {
        TextTag textTag = new TextTag();

        //得到退费记录号
        String sql = "select ActuGetNo from LJAGet " +
                     "where OtherNo = '" + mEdorAcceptNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String getNoticeNo = tExeSQL.getOneValue(sql);
        if (getNoticeNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "RenewVts";
            tError.functionName = "addNoticeNo";
            tError.errorMessage = "查不到退费记录号！";
            this.mErrors.addOneError(tError);
        }
        getNoticeNo = "退费记录号：" + getNoticeNo;
        textTag.add("GetNoticeNo", getNoticeNo);
        xmlExport.addTextTag(textTag);
        //xmlExport.outputDocumentToFile("c:\\", "xmlexport");
        return true;
    }

	/**
	 * 向批单中添加交费方式等内容
	 * @param xmlExport XmlExport
	 */
	private boolean addPayMode(XmlExport xmlExport, TransferData payInfo)
			throws Exception
	{
		String payMode = (String) payInfo.getValueByName("payMode");
		String payDate = (String) payInfo.getValueByName("payDate"); //转帐日期
		String endDate = (String) payInfo.getValueByName("endDate"); //截止日期
		String bank = (String) payInfo.getValueByName("bank");
		String bankAccno = (String) payInfo.getValueByName("bankAccno");
		String outPayMode = ""; //交费方式
		String outNotice = "";  //注意事项
		TextTag textTag = new TextTag();

		//得到变更后的差额
		LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
		tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
		if (!tLPEdorAppDB.getInfo())
		{
			CError tError = new CError();
			tError.moduleName = "RenewVts";
			tError.functionName = "addPayMode";
			tError.errorMessage = "查不到受理信息！";
			this.mErrors.addOneError(tError);
			return false;
		}

		//缴费方式
		if (payMode == null)
		{
			CError tError = new CError();
			tError.moduleName = "RenewVts";
			tError.functionName = "addPayMode";
			tError.errorMessage = "交费方式填写错误！";
			this.mErrors.addOneError(tError);
			return false;
		}
		double getMoney = tLPEdorAppDB.getGetMoney();
		//交费
		if (getMoney > 0)
		{
			//现金
			if (payMode.equals("1"))
			{
				outPayMode = "您的交费方式为：现金，请凭本通知书在" +
						CommonBL.decodeDate(endDate) +
						"前到我公司交费。";
				outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
			}
            //支票
            if (payMode.equals("2"))
            {
                outPayMode = "您的交费方式为：支票，请凭本通知书在" +
                        CommonBL.decodeDate(endDate) +
                        "前到我公司交费。";
                outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
            }
			//银行转帐
			else if (payMode.equals("4"))
			{
				outPayMode = "您的交费方式为：银行转帐，交费银行为：" +
						ChangeCodeBL.getCodeName("Bank", bank) +
						"，账号为：" + bankAccno +
						"，转帐总金额为：" + Math.abs(tLPEdorAppDB.getGetMoney()) +
						"元。\n我公司将在" + CommonBL.decodeDate(payDate) +
						"进行转账，请在转账日前确认该账号是否有足够金额。";
				outNotice = "本公司未收到应收保费的，本批注内容无效。";
			}
		}
		//退费
		else if (getMoney < 0)
		{
			//现金
			if (payMode.equals("1"))
			{
				outPayMode = "退费方式为：现金， 请凭本通知书到我公司领款。";
			}
			//银行转帐
			else if (payMode.equals("4"))
			{
				outPayMode = "退费方式为：银行转帐，交易银行为：" +
						ChangeCodeBL.getCodeName("Bank", bank) +
						"，交易账号为：" + bankAccno +
						"，转帐总金额为：" + Math.abs(tLPEdorAppDB.getGetMoney()) +
						"元。\n我公司将在" + CommonBL.decodeDate(payDate) +
						"进行转账，请注意查收。";
			}
		}
		textTag.add("PayMode", outPayMode);
		textTag.add("Notice", outNotice);
		xmlExport.addTextTag(textTag);
		//xmlExport.outputDocumentToFile("c:\\", "xmlexport");
		return true;
	}

	/**
	 * 向数据库中更新批单的Blob
	 * @param xmlExport XmlExport
	 */
	private boolean updateData(XmlExport xmlExport) throws Exception
	{
		LPEdorAppPrintSchema printSchema = new LPEdorAppPrintSchema();
		printSchema.setSchema(mPrintSchema);
		printSchema.setEdorInfo(xmlExport.getInputStream());
		printSchema.setModifyDate(PubFun.getCurrentDate());
		printSchema.setModifyTime(PubFun.getCurrentTime());
		MMap map = new MMap();
		map.put(printSchema, "BLOBUPDATE");

		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, ""))
		{
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "RenewVts";
			tError.functionName = "updateData";
			tError.errorMessage = "批单数据提交失败!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 主函数，测试用
	 * @param args String[]
	 */
    public static void main(String[] args)
    {
        TransferData payData = new TransferData();
        payData.setNameAndValue("payMode", "1");
        payData.setNameAndValue("payDate", "");
        payData.setNameAndValue("endDate", "2005-04-10");
        payData.setNameAndValue("bank", "");
        payData.setNameAndValue("bank", "");

        RenewVts renew = new RenewVts("20050713000019");
        renew.renewPayInfo(payData);
        renew.renewNoticeNo();
    }
}
