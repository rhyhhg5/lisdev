package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全个单新收费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * author Lanjun, QiuYang
 * @version 1.0
 */

public class PEdorTBDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = "TB";

    private String mContNo = null;

    private DetailDataQuery mQuery = null;

    private LPTbInfoSchema mLPTbInfoSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPTbInfoSchema = (LPTbInfoSchema)
                    cInputData.getObjectByObjectName("LPTbInfoSchema", 0);
            LPEdorItemSchema tLPEdorItemSchema = (LPEdorItemSchema)
                    cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = tLPEdorItemSchema.getEdorNo();
            mContNo = tLPEdorItemSchema.getContNo();
            mQuery = new DetailDataQuery(mEdorNo, mEdorType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入参数错误！");
            return false;
        }
        if ((mContNo==null)||(mContNo.equals(""))) {
        	 mErrors.addOneError("缺少保单号！");
             return false;
		}
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        setLPTbInfo();
        setLPPol();
        setLPInfo("LCDuty", "LPDuty");
        setLPInfo("LCPrem", "LPPrem");
        setEdorState(BQ.EDORSTATE_INPUT);
        return true;
    }

    /**
     * setLPInfo
     *
     * @param string String
     * @param string1 String
     */
    private void setLPInfo(String tableNameC, String tableNameP)
    {
        String sql = "delete from " + tableNameP + " "
                     + "where EdorNo = '" + mEdorNo + "' "
                     + "   and EdorType = '" + mEdorType + "' "
                     + "   and ContNo = '" + mContNo + "' ";
        mMap.put(sql, "INSERT");

        sql = "insert into " + tableNameP + " (select '" + mEdorNo +
              "','" + mEdorType + "', " + tableNameC + ".* from "
              + tableNameC
              + " where ContNo = '" + mContNo + "')";
        mMap.put(sql, "INSERT");

        sql = "update " + tableNameP + " "
              + "set MakeDate = '" + mCurrentDate + "', "
              + "   MakeTime = '" + mCurrentTime + "', "
              + "   ModifyDate = '" + mCurrentDate + "', "
              + "   ModifyTime = '" + mCurrentTime + "' "
              + "where EdorNo = '" + mEdorNo + "' "
              + "   and EdorType = '" + mEdorType + "' "
              + "   and ContNo = '"  + mContNo + "' ";
        mMap.put(sql, "INSERT");
    }

    private boolean checkData()
    {
        return true;
    }

    /**
     *
     */
    private void setLPTbInfo()
    {
        mLPTbInfoSchema.setOperator(mGlobalInput.Operator);
        mLPTbInfoSchema.setMakeDate(mCurrentDate);
        mLPTbInfoSchema.setMakeTime(mCurrentTime);
        mLPTbInfoSchema.setModifyDate(mCurrentDate);
        mLPTbInfoSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPTbInfoSchema, "DELETE&INSERT");
    }

    /**
     * TB项目是针对保单的，这里设置LPPol是为了人工核保时能对险种进行核保
     */
    private void setLPPol()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LPPolSchema tLPPolSchema = (LPPolSchema)
                    mQuery.getDetailData("LCPol", tLCPolSchema.getPolNo());
            tLPPolSchema.setOperator(mGlobalInput.Operator);
            tLPPolSchema.setMakeDate(mCurrentDate);
            tLPPolSchema.setMakeTime(mCurrentTime);
            tLPPolSchema.setModifyDate(mCurrentDate);
            tLPPolSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPolSchema, "DELETE&INSERT");
        }
    }

   /**
    * 把保全状态设为已录入
    * @param edorState String
    */
   private void setEdorState(String edorState)
   {
       String sql = "update  LPEdorItem " +
               "set EdorState = '" + edorState + "', " +
               "    Operator = '" + mGlobalInput.Operator + "', " +
               "    ModifyDate = '" + mCurrentDate + "', " +
               "    ModifyTime = '" + mCurrentTime + "' " +
               "where  EdorNo = '" + mEdorNo + "' " +
               "and EdorType = '" + mEdorType + "' " +
               "and ContNo = '" + mContNo + "'";
       mMap.put(sql, "UPDATE");
   }


   /**
    * 提交数据到数据库
    * @return boolean
    */
   private boolean submit()
   {
       VData data = new VData();
       data.add(mMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(data, ""))
       {
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           return false;
       }
       return true;
   }
}
