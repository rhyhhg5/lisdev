package com.sinosoft.lis.bq;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;




import java.util.concurrent.Future;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class LCGrpInputListBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	/** 团体合同号 */
	private String mGrpContNo;

	/** 保单印刷号 */
	private String mPrtNo;

	/** 批次号 */
	private String mBatchNo;

	/** 导入文件名 */
	private String mFileName ;

	/** 导入文件路径 */
	private String mFilePath;
	
	private GlobalInput mGlobalInput;
	
	/** 业务锁对象 */
	private LCGrpLock lock;
	
	private TransferData mTransferData = new TransferData();

	/**
	 * 导入入口函数
	 * 
	 * @param tTransferData
	 * @param lockPrtNo 业务锁处理印刷号
	 * @return
	 */
	public boolean dealList(TransferData tTransferData, String lockPrtNo) {
		this.mTransferData = tTransferData;
		System.out.println("---------标准团单被保人导入入口类---------");
		lock = new LCGrpLock(1, lockPrtNo);
		
		if(!lock.lock()){
			mErrors.addOneError("该保单正在进行其它业务处理");
			return false;
		}
		
		try {
			
			if (!getInputData(mTransferData)) {
				lock.unlock();
				return false;
			}

			if (!checkData()) {
				lock.unlock();
				return false;
			}

			if (!(dealData())) {
				lock.unlock();
				return false;
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
			mErrors.addOneError("导入被保人失败：程序异常终止！");
			
			lock.unlock();
			
			return false;
		} 
//		finally {
//			// 线程完毕处理
//			finalThread();
//		}

		return true;
	}

	/**
	 * 获取处理相关信息
	 * 
	 * @param tTransferData
	 * @return
	 */
	private boolean getInputData(TransferData tTransferData) {

		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mPrtNo = (String) tTransferData.getValueByName("PrtNo");
		this.mFileName = (String) tTransferData.getValueByName("FileName");
		this.mFilePath = (String) tTransferData.getValueByName("FilePath");
		this.mGlobalInput  = (GlobalInput) tTransferData.getValueByName("GlobalInput");

		if (isNull(mGrpContNo) || isNull(mPrtNo) || isNull(mFileName) || isNull(mFilePath)) {
			System.out.println("导入被保人失败：传入的保单信息缺失！");
			mErrors.addOneError("导入被保人失败：传入的保单信息缺失！");
			return false;
		}

		return true;
	}

	/**
	 * 处理前校验
	 * 
	 * @return
	 */
	private boolean checkData() {

		// 校验文件是否存在
		File tFile = new File(mFilePath + mFileName);
		if (!tFile.exists() || !tFile.isFile()) {
			System.out.println("导入被保人失败：上传服务器失败，未找到相关上传文件！");
			mErrors.addOneError("导入被保人失败：上传服务器失败，未找到相关上传文件！");
			return false;
		}

		mBatchNo = mFileName.substring(0, mFileName.lastIndexOf("."));
		// 前台已进行校验
		//checkSql = "select 1 from LCGrpSubInsuredImport where BatchNo='" + mBatchNo + "' fetch first 1 rows only with ur";
		//result = tExeSQL.getOneValue(checkSql);
		//if ("1".equals(result)) {
		//	System.out.println("导入被保人失败：该文件名已使用，请确实该文件是否已经上传！");
		//	mErrors.addOneError("导入被保人失败：该文件名已使用，请确实该文件是否已经上传！");
		//	return false;
		//}

		return true;
	}

	/**
	 * 调用导入处理主线程
	 * 
	 * @return
	 */
	private boolean dealData() {

		String seqNo = PubFun1.CreateMaxNo("BIGGRP", 10);
		
		mTransferData.setNameAndValue("SeqNo", seqNo);

		String filePath = mFilePath + mFileName;

		ExecutorService exec = Executors.newFixedThreadPool(1);
		
		LCGrpInputListThread tLGrpMainThread = new LCGrpInputListThread(filePath, mTransferData, lock);
		Future f = exec.submit(tLGrpMainThread);
		try{
			f.get();
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 校验字段是否为空
	 * 
	 * @param chkStr
	 * @return
	 */
	private boolean isNull(String chkStr) {
		if (chkStr == null || "".equals(chkStr) || "null".equals(chkStr)) {
			return true;
		}
		return false;
	}

	public CErrors getError() {
		return mErrors;
	}
}
