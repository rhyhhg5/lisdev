package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
* <p>Title: 追加保费清单</p>
* <p>Description:追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/


public class PrtGrpInsuredListZEBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_ZE;

    private XmlExport xml = new XmlExport();
    
    private VData mResult = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListZEBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!createXML())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public VData getResult(){
    	return mResult;
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PrtGrpInsuredListZE.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName(mEdorType);
        SSRS tSSRS = new SSRS();
        tSSRS = getInsuredList();
        for (int i=1;i<=tSSRS.getMaxRow();i++)
        {
            String[] column = new String[9];
            column[0] = ""+i;
            column[1] = StrTool.cTrim(tSSRS.GetText(i, 2));
            column[2] = StrTool.cTrim(tSSRS.GetText(i, 3));
            column[3] = StrTool.cTrim(tSSRS.GetText(i, 4));
            column[4] = StrTool.cTrim(tSSRS.GetText(i, 5));
            column[5] = StrTool.cTrim(tSSRS.GetText(i, 6));
            column[6] = StrTool.cTrim(tSSRS.GetText(i, 7));
            column[7] = StrTool.cTrim(String.valueOf(tSSRS.GetText(i, 8)));
            column[8] = CommonBL.decodeState(StrTool.cTrim(tSSRS.GetText(i, 9)));
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[9]);
//        xml.outputDocumentToFile("d:\\", "GDInsuredList");
        mResult = new VData();
        mResult.addElement(xml);
        return true;
    }

    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_ZE); 
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private SSRS getInsuredList()
    {
        String sql = "select SerialNo, InsuredName,InsuredNo,db2inst1.codename('sex',Sex),Birthday," +
                "db2inst1.codename('idtype',IDType),IDNo,Money,State" +
                "  from LPDiskImport where EdorNo = '" + mEdorNo + "' " +
                "and edortype='" + mEdorType + "'" +
                "union select SerialNo, InsuredName,InsuredNo,db2inst1.codename('sex',Sex),Birthday," +
                "db2inst1.codename('idtype',IDType),IDNo,Money,State" +
                "  from LBDiskImport where EdorNo = '" + mEdorNo + "' " +
                "and edortype='" + mEdorType + "' order by SerialNo with ur " ;
        return new ExeSQL().execSQL(sql);
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData d = new VData();
        d.add(gi);

        PrtGrpInsuredListZEBL bl = new PrtGrpInsuredListZEBL(gi, "20070213000061");
        if(!bl.submitData())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
