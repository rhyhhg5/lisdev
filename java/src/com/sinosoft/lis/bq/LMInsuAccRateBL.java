package com.sinosoft.lis.bq;
//程序名称：LMInsuAccRateBL.java
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class LMInsuAccRateBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private VData mResult = new VData();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 数据操作字符串 */
	private String mOperate ;
	
	/** 操作员 */
	private String mOperater ;
	
	/** 业务处理相关变量 */
	private LMInsuAccRateSchema mLMInsuAccRateSchema = new LMInsuAccRateSchema();
	
	/** 结算月份 */
	private String mBalaMonth = null;
	
	/** 需要删除的业务处理相关变量 */
	private LMInsuAccRateSchema mDLMInsuAccRateSchema = new LMInsuAccRateSchema();
	
	/** 需要删除的结算月份 */
	private String mDBalaMonth = null;
	
	public LMInsuAccRateBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData,String cOperate)
	{
        //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
            tError.moduleName = "LMInsuAccRateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败LMInsuAccRateBL-->getInputData!";
            this.mErrors.addOneError(tError);
	        return false;
	    }
	    
        //数据校验
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMInsuAccRateBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LMInsuAccRateBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "LMInsuAccRateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LMInsuAccRateBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }

	    if (!prepareOutputData())
	    {
	        return false;
	    }
	    
	    PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMInsuAccRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
	    
	    mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		System.out.println("进行数据处理.....");
	    if (mOperate.equals("INSERT||MAIN"))
	    {
	    	return insertData();
	    }
	    
	    if (mOperate.equals("UPDATE||MAIN"))
	    {
	    	if(!deleteData())
	    	{
	    		return false;
	    	}
	    	if(!insertData())
	    	{
	    		return false;
	    	}
	    	return true;
	    }
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	        return deleteData(); //删除
	    }
	    return true;
	}
	
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    this.mLMInsuAccRateSchema = (LMInsuAccRateSchema)tTransferData.getValueByName("tLMInsuAccRateSchema");
	    this.mBalaMonth = (String)tTransferData.getValueByName("tBalaMonth");
	    String tStartBalaDate = mBalaMonth + "-1";
		String tBalaDate = PubFun.calDate(tStartBalaDate,1,"M",tStartBalaDate);
		String tSRateDate = tBalaDate;
		this.mLMInsuAccRateSchema.setStartBalaDate(tStartBalaDate);
		this.mLMInsuAccRateSchema.setBalaDate(tBalaDate);
		this.mLMInsuAccRateSchema.setSRateDate(tSRateDate);
		
		this.mDLMInsuAccRateSchema = (LMInsuAccRateSchema)tTransferData.getValueByName("dLMInsuAccRateSchema");
	    this.mDBalaMonth = (String)tTransferData.getValueByName("dBalaMonth");
	    String dStartBalaDate = mDBalaMonth + "-1";
		String dBalaDate = PubFun.calDate(dStartBalaDate,1,"M",dStartBalaDate);
		String dSRateDate = dBalaDate;
		this.mDLMInsuAccRateSchema.setStartBalaDate(dStartBalaDate);
		this.mDLMInsuAccRateSchema.setBalaDate(dBalaDate);
		this.mDLMInsuAccRateSchema.setSRateDate(dSRateDate);
		return true;
	}
	 
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLMInsuAccRateSchema);
            this.mInputData.add(this.mDLMInsuAccRateSchema);
            mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLMInsuAccRateSchema);
            mResult.add(this.mDLMInsuAccRateSchema);
        }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError = new CError();
	 		tError.moduleName = "LMInsuAccRateBL";
	 		tError.functionName = "prepareOutputData";
	 		tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
    }
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        if (mLMInsuAccRateSchema == null) {
        	System.out.println("mLMInsuAccRateSchema == null");
            return false;
        }
        if (mGlobalInput == null) {
        	System.out.println("mGlobalInput == null");
            return false;
        }
        if (mBalaMonth == null) {
        	System.out.println("mBalaMonth == null");
            return false;
        }
        return true;
    }
    
    //插入数据
    private boolean insertData()
    {
    	LMInsuAccRateSchema tLMInsuAccRateSchema = new LMInsuAccRateSchema();
    	tLMInsuAccRateSchema.setRiskCode(mLMInsuAccRateSchema.getRiskCode());
    	tLMInsuAccRateSchema.setInsuAccNo(mLMInsuAccRateSchema.getInsuAccNo());
        tLMInsuAccRateSchema.setStartBalaDate(mLMInsuAccRateSchema.getStartBalaDate());
  	    tLMInsuAccRateSchema.setBalaDate(mLMInsuAccRateSchema.getBalaDate());
    	tLMInsuAccRateSchema.setSRateDate(mLMInsuAccRateSchema.getSRateDate());
    	tLMInsuAccRateSchema.setARateDate(this.mCurrentDate);
    	tLMInsuAccRateSchema.setDrawType("M");
    	tLMInsuAccRateSchema.setRateType(mLMInsuAccRateSchema.getRateType());
    	tLMInsuAccRateSchema.setRateIntv(mLMInsuAccRateSchema.getRateIntv());
    	tLMInsuAccRateSchema.setRateIntvUnit(mLMInsuAccRateSchema.getRateIntvUnit());
    	tLMInsuAccRateSchema.setRate(mLMInsuAccRateSchema.getRate());
    	tLMInsuAccRateSchema.setOperator(this.mOperater);
    	PubFun.fillDefaultField(tLMInsuAccRateSchema);
        map.put(tLMInsuAccRateSchema, "INSERT");
    	return true;
    }
    
    //删除数据
    private boolean deleteData()
    {
    	map.put(mDLMInsuAccRateSchema, "DELETE");
    	return true;
    }
    public VData getResult()
    {
        return this.mResult;
	}
}
