package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保全交费方式变更</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author  Lanjun
 * @version 1.0
 */

public class PEdorTBDetailUI
{
    private PEdorTBDetailBL mPEdorTBDetailBL = null;

    public PEdorTBDetailUI()
    {
        mPEdorTBDetailBL = new PEdorTBDetailBL();
    }

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mPEdorTBDetailBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public String getError()
    {
        return mPEdorTBDetailBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     */
    public static void main(String[] args)
    {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("B21000000000019379");
        tLCPolSchema.setMult("2");
        tLCPolSchema.setAmnt("840000");
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet.add(tLCPolSchema);

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorNo("20060523000006");
        tLPEdorItemSchema.setContNo("00002887601");
        GlobalInput gi = new GlobalInput();
        gi.Operator = "qq";
        VData data = new VData();
        data.add(gi);
        data.add(tLCPolSet);
        data.add(tLPEdorItemSchema);
        PEdorBFDetailUI tPEdorBFDetailUI = new PEdorBFDetailUI();
        if (!tPEdorBFDetailUI.submitData(data, "SAVE"))
        {
            System.out.println(tPEdorBFDetailUI.getError());
        }
        else
        {
        }

    }
}
