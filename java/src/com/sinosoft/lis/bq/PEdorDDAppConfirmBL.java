package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.db.LPInsureAccTraceDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsureAccBalanceSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LMInsuAccRateSchema;
import com.sinosoft.lis.schema.LMPolDutyEdorCalSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsureAccClassSchema;
import com.sinosoft.lis.schema.LPInsureAccSchema;
import com.sinosoft.lis.schema.LPInsureAccTraceSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMInsuAccRateSet;
import com.sinosoft.lis.vschema.LPInsureAccTraceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

//程序名称：PEdorDDAppConfirm.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容
public class PEdorDDAppConfirmBL implements EdorAppConfirm {
	private MMap mMap = new MMap();

	private GlobalInput mGlobalInput = null;

	/** 保全号 */
	private String mEdorNo = null;

	/** 保全类型 */
	private String mEdorType = null;

	/** 合同号 */
	private String mContNo = null;

	/** 保单险种号 */
	private String mPolNo = null;

	/** 险种代码 */
	private String mRiskCode = null;

	/** 修改后的领取方式 */
	private String mAfterGetMode = null;

	/** 保单险种 */
	private LCPolSchema mLCPolSchema = null;

	/** 保全生效日期 */
	private String mEdorValiDate = null;

	private ExeSQL mExeSQL = new ExeSQL();

	/** 保全交费总额 */
	private double mGetMoney = 0.00;

	/** 保全项目特殊数据 */
	private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;

	private DetailDataQuery mQuery = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private LCInsureAccSchema mLCInsureAccSchema = null;

	private LCInsureAccClassSchema mLCInsureAccClassSchema = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String YXCalCode = "";// 有效

	private String ZZCalCode = "";// 失效

	private String FXCalCode = "";// 复效

	private String JYLPCalCode = "";// 解约理赔

	private String MQCalCode = "";// 满期

	/**
	 * 提交数据
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			System.out.println("PEdorDDAppConfirm.java->getInputData(cInputData)失败");
			return false;
		}

		if (!checkData()) {
			System.out.println("PEdorDDAppConfirm.java->checkData()失败");
			return false;
		}

		if (!dealData()) {
			System.out.println("PEdorDDAppConfirm.java->dealData()失败");
			return false;
		}
		return true;
	}

	/**
	 * 返回理算结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		VData data = new VData();
		data.add(mMap);
		return data;
	}

	public boolean checkData() {
		if (mAfterGetMode == null || mLCPolSchema.getBonusGetMode() == null || mAfterGetMode.equals(mLCPolSchema.getBonusGetMode())) {
			// @@错误处理
			System.out.println("PEdorDDAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorDDAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "本次未变更任何数据或保单数据有误!";
			mErrors.addOneError(tError);
			return false;
		}
		if ((!mAfterGetMode.equals(BQ.BONUSGET_GETINTEREST)) && (!mAfterGetMode.equals(BQ.BONUSGET_GETMONEY))) {
			// @@错误处理
			System.out.println("PEdorDDAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorDDAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "修改后分红方式错误!";
			mErrors.addOneError(tError);
			return false;
		}
		if ((!mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)) && (!mLCPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETMONEY))) {
			// @@错误处理
			System.out.println("PEdorDDAppConfirmBL+checkData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorDDAppConfirmBL";
			tError.functionName = "checkData";
			tError.errorMessage = "原保单分红方式错误!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 得到传入参数
	 * 
	 * @param cInputData
	 *            VData
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mLPEdorItemSchema = (LPEdorItemSchema) cInputData.getObjectByObjectName("LPEdorItemSchema", 0);
		mEdorNo = mLPEdorItemSchema.getEdorNo();
		mEdorType = mLPEdorItemSchema.getEdorType();
		mContNo = mLPEdorItemSchema.getContNo();
		mEdorValiDate = mLPEdorItemSchema.getEdorValiDate();
		String sql = "select * from LCPol a where ContNo = '" + mLPEdorItemSchema.getContNo() + "' " + "and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '"
				+ BQ.RISKTYPE4_BONUS + "') " + "with ur";
		System.out.println("查询分红险种是否存在：" + sql);
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolSet = tLCPolDB.executeQuery(sql);
		if (tLCPolSet.size() != 1) {
			mErrors.addOneError("找不到分红险种信息！");
			return false;
		} else {
			mPolNo = tLCPolSet.get(1).getPolNo();
			mRiskCode = tLCPolSet.get(1).getRiskCode();
		}
		mLCPolSchema = tLCPolSet.get(1).getSchema();

		LPPolDB tLPPolDB = new LPPolDB();
		tLPPolDB.setEdorNo(mEdorNo);
		tLPPolDB.setEdorType(mEdorType);
		tLPPolDB.setPolNo(mPolNo);
		if (!tLPPolDB.getInfo()) {
			// @@错误处理
			System.out.println("PEdorDDAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorDDAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "查询保全表数据失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mAfterGetMode = tLPPolDB.getBonusGetMode();
		return true;
	}

	/**
	 * 处理业务数据
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		if (mAfterGetMode.equals(BQ.BONUSGET_GETMONEY))
			if (!DealInterest())
				return false;
		setEdorItem();
		return true;
	}

	/**
	 * 处理累积生息帐户利息
	 * 
	 * @return boolean
	 */
	private boolean DealInterest() {
		mLCInsureAccSchema = CommonBL.getLCInsureAcc(mPolNo, CommonBL.getInsuAccNo("005", mRiskCode));
		if (mLCInsureAccSchema == null) {
			System.out.println(mPolNo + "分红帐户尚未建立，无需领钱!");
			return true;
		}
		if (mLCInsureAccSchema.getInsuAccBala() == 0) {
			System.out.println(mPolNo + "分红帐户余额为0!");
			return true;
		}
		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		tLCInsureAccClassDB.setPolNo(mPolNo);
		tLCInsureAccClassDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
		mLCInsureAccClassSchema = tLCInsureAccClassDB.query().get(1);
		if (mLCInsureAccClassSchema == null) {
			mErrors.addOneError("找不到分红账户分类信息！");
			return false;
		}
		// 将原账户清0
		setLPInsureAcc();
		setLPInsureAccClass();

		// 计算出的利息金额
		double interestmoney = getinterest();

		if (interestmoney == -1) {
			// @@错误处理
			System.out.println("PEdorDDAppConfirmBL+DealInterest++--");
			CError tError = new CError();
			tError.moduleName = "PEdorDDAppConfirmBL";
			tError.functionName = "DealInterest";
			tError.errorMessage = "计算累积生息帐户利息出错!";
			mErrors.addOneError(tError);
			return false;
		}

		interestmoney = CommonBL.carry(interestmoney);
		mGetMoney = CommonBL.carry(mLCInsureAccSchema.getInsuAccBala() + interestmoney);

		//设置轨迹表,一条本次利息,一条清空轨迹
		setLPInsureAccTrace("LX", interestmoney);
		setLPInsureAccTrace("HL", -mGetMoney);

		//设置保全收付费表
		setGetEndorse();
		return true;
	}

	private double getinterest() {
		if(!getcalcode()){
			return -1;
		}
/*    	校验是否存在累积生息帐户结算表,如果不存在则生成,并且同时设置baladate为第一笔非0数据进账户的时间.
    	这里会有一个数据库提交,提交失败则返回false
   */
        if (!HaveBalance())
        {
        	// @@错误处理
			System.out.println("BonusGetBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "生成帐户结算轨迹表失败!";
			mErrors.addOneError(tError);
			return -1;
        }
//    	仅仅用LMInsuAccRate来储存每一段结算区间
		LMInsuAccRateSchema tLMInsuAccRateSchema =new LMInsuAccRateSchema();
    	tLMInsuAccRateSchema.setStartBalaDate(mLCInsureAccSchema.getBalaDate());
		tLMInsuAccRateSchema.setBalaDate(mCurrentDate);
//    	若区间经过保单周年日,则进行拆分
    	LMInsuAccRateSet tLMInsuAccRateSet=getRateSet(tLMInsuAccRateSchema);
//    	此处不用4舍5入
    	double tInterest=getinterest(tLMInsuAccRateSet);
    	return tInterest;
	}
	
    /**
     * 处理帐户结算表初步生成
     * !!!!!!这里有分步提交数据库!!!!!!
     * @return boolean
     */
    private boolean HaveBalance()
    {
    	String Haveacc = mExeSQL.getOneValue("select 1 from lcinsureaccbalance where polno='"+mPolNo+"' fetch first 1 row only with ur");
    	if(Haveacc!=null&&Haveacc.equals("1")){
    		System.out.println("已经生成帐户了!");
    		return true;
    	}
    	String tbaladate=mExeSQL.getOneValue("select min(paydate) from lcinsureacctrace where polno='"+mPolNo+"' and money<>0 with ur");
    	if(tbaladate==null||tbaladate.equals("")||tbaladate.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取最早结算日期出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	String tmoney=mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='"+mPolNo+"' and paydate<='"+tbaladate+"' with ur");
    	if(tmoney==null||tmoney.equals("")||tmoney.equals("null")){
    		// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "获取帐户金额出错";
			mErrors.addOneError(tError);
			return false;
    	}
    	
    	VData tVData = new VData();
    	MMap tMMap =new MMap();
    	
    	tMMap.put("update lcinsureacc set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);
    	tMMap.put("update lcinsureaccclass set baladate='"+tbaladate+"' where polno='"+mPolNo+"'", SysConst.UPDATE);

        LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
        new Reflections().transFields(schema, mLCInsureAccSchema);
        schema.setSequenceNo(mLCInsureAccSchema.getPolNo()+ mLCInsureAccSchema.getInsuAccNo());
        schema.setBalaCount(0);
        schema.setDueBalaDate(tbaladate);
        schema.setDueBalaTime("00:00:00");
        schema.setRunDate(schema.getMakeDate());
        schema.setRunTime(schema.getMakeTime());
        schema.setPolYear(1);
        schema.setPolMonth(1);
        schema.setInsuAccBalaBefore(tmoney);
        schema.setInsuAccBalaAfter(tmoney);
        schema.setInsuAccBalaGurat(tmoney);
        schema.setPrintCount(0);
        tMMap.put(schema,SysConst.INSERT);

        tVData.add(tMMap);
        //计算并填充账户结构
        if (tVData == null)
        {
        	// @@错误处理
			System.out.println("BonusGetBL+HaveAcc++--");
			CError tError = new CError();
			tError.moduleName = "BonusGetBL";
			tError.functionName = "HaveAcc";
			tError.errorMessage = "创建账户结构失败!";
			mErrors.addOneError(tError);
			return false;
        }
//      执行业务数据提交
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(tVData, ""))
        {
        	// @@错误处理
			System.out.println("BonusBalaBL+HaveBalance++--");
			CError tError = new CError();
			tError.moduleName = "BonusBalaBL";
			tError.functionName = "HaveBalance";
			tError.errorMessage = "帐户结算创建数据提交失败!";
			mErrors.addOneError(tError);
			return false;
        }
//		提交完成后置全局变量结算日期
        mLCInsureAccSchema.setBalaDate(tbaladate);
    	return true;    	
    }

	// 计算利息
	private double getinterest(LMInsuAccRateSet tLMInsuAccRateSet) {
		String rate1 = GetRate(tLMInsuAccRateSet.get(1).getStartBalaDate());
		double interest = 0;
		if (rate1 == null || rate1.equals("") || rate1.equals("null")) {
//			//本年度利率为空则查询最近一次公布的累积生息利率
//			rate1=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mPolNo+"' " +
//			" order by bonusyear desc fetch first 1 row only with ur  ");			
			return -1;
		}
		String lastriskbonus = mExeSQL.getOneValue("select sum(money) from lcinsureacctrace where polno='" + mPolNo + "' and paydate<='" + tLMInsuAccRateSet.get(1).getStartBalaDate() + "'");
		if (lastriskbonus == null || lastriskbonus.equals("") || lastriskbonus.equals("null")) {
			return -1;
		}
		// 当前失效,失效日期在结算之前
		String tSXDate = mExeSQL.getOneValue("select startdate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and startdate<'"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and StateReason!='02' " + " and (enddate is null or enddate='' ) " + " union all " + " select makedate from lccontstate "
				+ " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and makedate<'" + tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and StateReason='02' "
				+ " and (enddate is null or enddate='' ) ");
		if (tSXDate != null && (!tSXDate.equals("")) && (!tSXDate.equals("null"))) {
			return 0;
		}
		// 当前失效,失效日期在第一个结算区间
		tSXDate = mExeSQL.getOneValue("select makedate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and makedate>='"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and makedate<'" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason='02' " + " and (enddate is null or enddate='' ) "
				+ " union all " + " select startdate from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='1' " + " and startdate>='"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and startdate<'" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason!='02' " + " and (enddate is null or enddate='' ) ");
		if (tSXDate != null && (!tSXDate.equals("")) && (!tSXDate.equals("null"))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			return interest;
		}
		if (tLMInsuAccRateSet.size() == 1) {
			try {
				LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
				interest = getAccInterset(rate1, lastriskbonus, ttLMInsuAccRateSet);
				return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return -1;
			}
		} else if (tLMInsuAccRateSet.size() == 2) {
			String rate2 = GetRate(PubFun.calDate(tLMInsuAccRateSet.get(2).getBalaDate(), -1, "D", null));
			if(rate2==null||"".equals(rate2)){
				//本年度利率为空则查询最近一次公布的累积生息利率
				rate2=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+mPolNo+"' " +
				" order by bonusyear desc fetch first 1 row only with ur  ");
			}
			try {
				LMInsuAccRateSet ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(1));
				double interest1 = getAccInterset(rate1, lastriskbonus, ttLMInsuAccRateSet);
				ttLMInsuAccRateSet = new LMInsuAccRateSet();
				ttLMInsuAccRateSet.add(tLMInsuAccRateSet.get(2));
				lastriskbonus = mExeSQL.getOneValue("select sum(money)+" + interest1 + " from lcinsureacctrace where polno='" + mPolNo + "' and paydate<='"
						+ tLMInsuAccRateSet.get(2).getStartBalaDate() + "'");
				if (lastriskbonus == null || lastriskbonus.equals("") || lastriskbonus.equals("null")) {
					return -1;
				}
				double interest2 = getAccInterset(rate2, lastriskbonus, ttLMInsuAccRateSet);
				interest = interest1 + interest2;
				return interest;
			} catch (Exception e) {
				// TODO: handle exception
				return -1;
			}
		} else {
			return -1;
		}
	}

	// 返回该日期对应的利率
	public String GetRate(String tdate) {
		String tPolYear = String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(), tdate, "Y"));
		String trate = mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='" + mPolNo
				+ "' " + " and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+" + tPolYear + "-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+" + tPolYear
				+ ")))) ");
		return trate;
	}

	// 获取各类算法的calcode以备使用
	public boolean getcalcode() {
		LMRiskBonusDB tLMRiskBonusDB = new LMRiskBonusDB();
		tLMRiskBonusDB.setRiskCode(mLCPolSchema.getRiskCode());
		if (!tLMRiskBonusDB.getInfo()) {
			return false;
		}
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "YX");
		YXCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "ZZ");
		ZZCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "FX");
		FXCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "JY");
		JYLPCalCode = tCalculator.calculate();
		tCalculator = new Calculator();
		tCalculator.setCalCode(tLMRiskBonusDB.getBonusInterestCode());
		tCalculator.addBasicFactor("caltype1", "MQ");
		MQCalCode = tCalculator.calculate();
		return true;
	}

	private double getAccInterset(String rate1, String lastriskbonus, LMInsuAccRateSet tLMInsuAccRateSet) {
		double interest = 0;
		String tSXDate1 = mExeSQL.getOneValue("select min(startdate) from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='0' " + " and startdate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason!='02' " + " and enddate is not null and enddate!='' "
				+ " union all " + " select min(makedate) from lccontstate " + " where  polno='" + mPolNo + "' and statetype='Available' and state='0' " + " and makedate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' and StateReason='02' " + " and enddate is not null and enddate!='' ");
		String tFXDate1 = mExeSQL.getOneValue("select max(enddate) from lccontstate where state='0' and polno='" + mPolNo + "' and statetype='Available' and enddate between '"
				+ tLMInsuAccRateSet.get(1).getStartBalaDate() + "' and '" + tLMInsuAccRateSet.get(1).getBalaDate() + "' ");
		if ((tSXDate1 == null || tSXDate1.equals("") || tSXDate1.equals("null")) && (tFXDate1 == null || tFXDate1.equals("") || tFXDate1.equals("null"))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(YXCalCode);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
			System.out.println(tCalculator.calculate());
			System.out.println(lastriskbonus);
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if (tSXDate1.equals("") && (!tFXDate1.equals(""))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(FXCalCode);
			tCalculator.addBasicFactor("reinstatedate", tFXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if (tFXDate1.equals("") && (!tSXDate1.equals(""))) {
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode(ZZCalCode);
			tCalculator.addBasicFactor("EvaluateStartDate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			tCalculator.addBasicFactor("EvaluateEndDate", tLMInsuAccRateSet.get(1).getBalaDate());
			tCalculator.addBasicFactor("EndDate", tSXDate1);
			tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
			tCalculator.addBasicFactor("enddate", tSXDate1);
			tCalculator.addBasicFactor("rate", rate1);
			tCalculator.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
			interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
		} else if ((!tFXDate1.equals("")) && (!tSXDate1.equals(""))) {
			if (PubFun.calInterval(tSXDate1, tFXDate1, "D") < 0) {
				// 复效日期小于失效日期，按正常来计算，重新设置结算区间
				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(YXCalCode);
				tCalculator.addBasicFactor("lastriskbonus", lastriskbonus);
				tCalculator.addBasicFactor("rate", rate1);
				tCalculator.addBasicFactor("lastaccountdate", tFXDate1);
				tCalculator.addBasicFactor("accountdate", tSXDate1);
				interest = Double.parseDouble(tCalculator.calculate()) - Double.parseDouble(lastriskbonus);
			} else {
				// 复效日期大于失效日期，分两段来计算
				Calculator tCalculatorzz = new Calculator();
				tCalculatorzz.setCalCode(YXCalCode);
				tCalculatorzz.addBasicFactor("lastriskbonus", lastriskbonus);
				tCalculatorzz.addBasicFactor("rate", rate1);
				tCalculatorzz.addBasicFactor("lastaccountdate", tLMInsuAccRateSet.get(1).getStartBalaDate());
				tCalculatorzz.addBasicFactor("accountdate", tSXDate1);
				// 第二段复效的
				Calculator tCalculatorfx = new Calculator();
				tCalculatorfx.setCalCode(YXCalCode);
				tCalculatorfx.addBasicFactor("lastriskbonus", tCalculatorzz.calculate());
				tCalculatorfx.addBasicFactor("rate", rate1);
				tCalculatorfx.addBasicFactor("lastaccountdate", tFXDate1);
				tCalculatorfx.addBasicFactor("accountdate", tLMInsuAccRateSet.get(1).getBalaDate());
				interest = Double.parseDouble(tCalculatorfx.calculate()) - Double.parseDouble(lastriskbonus);
			}
		}
		return interest;

	}

	// 拆分结算月度经过保单周年日的情况
	private LMInsuAccRateSet getRateSet(LMInsuAccRateSchema tLMInsuAccRateSchema) {
		LMInsuAccRateSet tLMInsuAccRateSet = new LMInsuAccRateSet();
		int i = 1;
		boolean haveDate = false;
		do {
			// 结算开始日期减去该次保单周年日的日期
			int bigthanstart = PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getStartBalaDate(), "D");
			// 结算结束日期减去该次保单周年日的日期
			int bigthanend = PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getBalaDate(), "D");
			if (bigthanstart > 0) {
				i++;
			} else if (bigthanstart == 0) {
				break;
			} else if (bigthanstart < 0 && bigthanend > 0) {
				haveDate = true;
				break;
			} else if (bigthanend <= 0) {
				break;
			}
		} while (PubFun.calInterval(PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null), tLMInsuAccRateSchema.getBalaDate(), "D") > 0);
		if (haveDate) {
			String midDate = PubFun.calDate(mLCPolSchema.getCValiDate(), i, "Y", null);
			LMInsuAccRateSchema tLM1 = new LMInsuAccRateSchema();
			tLM1.setStartBalaDate(tLMInsuAccRateSchema.getStartBalaDate());
			tLM1.setBalaDate(midDate);
			LMInsuAccRateSchema tLM2 = new LMInsuAccRateSchema();
			tLM2.setStartBalaDate(midDate);
			tLM2.setBalaDate(tLMInsuAccRateSchema.getBalaDate());
			tLMInsuAccRateSet.add(tLM1);
			tLMInsuAccRateSet.add(tLM2);
		} else {
			tLMInsuAccRateSet = new LMInsuAccRateSet();
			tLMInsuAccRateSet.add(tLMInsuAccRateSchema);
		}
		return tLMInsuAccRateSet;
	}

	private void setLPInsureAcc() {
		LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPInsureAccSchema, mLCInsureAccClassSchema);
		tLPInsureAccSchema.setEdorNo(mEdorNo);
		tLPInsureAccSchema.setEdorType(mEdorType);
		tLPInsureAccSchema.setInsuAccBala(0);
		tLPInsureAccSchema.setBalaDate(mCurrentDate);
		tLPInsureAccSchema.setPrtNo(mLCInsureAccSchema.getPrtNo());
		tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccSchema.setModifyDate(mCurrentDate);
		tLPInsureAccSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
	}

	/**
	 * 设置帐户领取轨迹
	 * 
	 * @param aLCInsureAccClassSchema
	 *            LCInsureAccClassSchema
	 * @param grpMoney
	 *            double
	 */
	private void setLPInsureAccTrace(String tMoneyType, double tMoney) {
		String serialNo;
		LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
		tLPInsureAccTraceDB.setEdorNo(mEdorNo);
		tLPInsureAccTraceDB.setEdorType(mEdorType);
		tLPInsureAccTraceDB.setContNo(mContNo);
		tLPInsureAccTraceDB.setPolNo(mPolNo);
		tLPInsureAccTraceDB.setInsuAccNo(mLCInsureAccClassSchema.getInsuAccNo());
		tLPInsureAccTraceDB.setMoneyType(tMoneyType);
		LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
		if (tLPInsureAccTraceSet.size() > 0) {
			serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
			String sql = "delete from LPInsureAccTrace " + "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '" + mEdorType + "' " + "and SerialNo = '" + serialNo + "' ";
			mMap.put(sql, "DELETE");
		} else {
			serialNo = PubFun1.CreateMaxNo("SERIALNO", mLCInsureAccClassSchema.getManageCom());
		}
		LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
		tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
		tLPInsureAccTraceSchema.setEdorType(mEdorType);
		tLPInsureAccTraceSchema.setGrpContNo(mLCInsureAccClassSchema.getGrpContNo());
		tLPInsureAccTraceSchema.setGrpPolNo(mLCInsureAccClassSchema.getGrpPolNo());
		tLPInsureAccTraceSchema.setContNo(mLCInsureAccClassSchema.getContNo());
		tLPInsureAccTraceSchema.setPolNo(mLCInsureAccClassSchema.getPolNo());
		tLPInsureAccTraceSchema.setSerialNo(serialNo);
		tLPInsureAccTraceSchema.setInsuAccNo(mLCInsureAccClassSchema.getInsuAccNo());
		tLPInsureAccTraceSchema.setRiskCode(mLCInsureAccClassSchema.getRiskCode());
		tLPInsureAccTraceSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
		tLPInsureAccTraceSchema.setManageCom(mLCInsureAccClassSchema.getManageCom());
		tLPInsureAccTraceSchema.setAccAscription("0");
		tLPInsureAccTraceSchema.setMoneyType(tMoneyType);
		tLPInsureAccTraceSchema.setMoney(tMoney); // 退费
		tLPInsureAccTraceSchema.setUnitCount("0");
		tLPInsureAccTraceSchema.setPayDate(mCurrentDate);
		tLPInsureAccTraceSchema.setState("0");
		tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
		tLPInsureAccTraceSchema.setOtherType("10");
		tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
		tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
		tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
		tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
		mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
	}

	private void setLPInsureAccClass() {
		LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPInsureAccClassSchema, mLCInsureAccClassSchema);
		tLPInsureAccClassSchema.setEdorNo(mEdorNo);
		tLPInsureAccClassSchema.setEdorType(mEdorType);
		tLPInsureAccClassSchema.setInsuAccBala(0);
		tLPInsureAccClassSchema.setBalaDate(mCurrentDate);
		tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
		tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
		tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
		tLPInsureAccClassSchema.setAccAscription("0");
		mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
	}

	/**
	 * 设置批改补退费表
	 * 
	 * @return boolean
	 */
	private boolean setGetEndorse() {
		LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
		tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); // 给付通知书号码，没意义
		tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
		tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
		tLJSGetEndorseSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
		tLJSGetEndorseSchema.setContNo(mLCPolSchema.getContNo());
		tLJSGetEndorseSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
		tLJSGetEndorseSchema.setPolNo(mLCPolSchema.getPolNo());
		tLJSGetEndorseSchema.setGetDate(mCurrentDate);
		tLJSGetEndorseSchema.setGetMoney(-Math.abs(mGetMoney));
		tLJSGetEndorseSchema.setRiskCode(mLCPolSchema.getRiskCode());
		tLJSGetEndorseSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
		tLJSGetEndorseSchema.setAgentCom(mLCPolSchema.getAgentCom());
		tLJSGetEndorseSchema.setAgentType(mLCPolSchema.getAgentType());
		tLJSGetEndorseSchema.setAgentCode(mLCPolSchema.getAgentCode());
		tLJSGetEndorseSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
		tLJSGetEndorseSchema.setApproveCode(mLCPolSchema.getApproveCode());
		tLJSGetEndorseSchema.setApproveDate(mLCPolSchema.getApproveDate());
		tLJSGetEndorseSchema.setApproveTime(mLCPolSchema.getApproveTime());
		tLJSGetEndorseSchema.setFeeFinaType("HL");// 红利累积生息红利领出
		tLJSGetEndorseSchema.setAppntNo(mLCPolSchema.getAppntNo());
		tLJSGetEndorseSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
		tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
		tLJSGetEndorseSchema.setPayPlanCode(mLCInsureAccClassSchema.getPayPlanCode());
		tLJSGetEndorseSchema.setOtherNo(mEdorNo);
		tLJSGetEndorseSchema.setOtherNoType("10");
		tLJSGetEndorseSchema.setGetFlag("0");
		tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
		tLJSGetEndorseSchema.setManageCom(mLCPolSchema.getManageCom());
		tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
		mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
		return true;
	}

	/**
	 * 设置item表中的费用和状态
	 */
	private void setEdorItem() {
		System.out.println(mGetMoney);
		String sql = "update LPEdorItem " + "set GetMoney = -" + mGetMoney + ", "
				+ // 这里是负数
				"EdorState = '" + BQ.EDORSTATE_CAL + "', " + "EdorValiDate = '" + mCurrentDate + "', " + "ModifyDate = '" + mCurrentDate + "', " + "ModifyTime = '" + mCurrentTime + "' "
				+ "where EdorNo = '" + mEdorNo + "' " + "and EdorType = '" + mEdorType + "' ";
		mMap.put(sql.toString(), "UPDATE");
	}
}
