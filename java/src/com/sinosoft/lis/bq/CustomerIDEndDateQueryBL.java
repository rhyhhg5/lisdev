package com.sinosoft.lis.bq;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 * @author lihuaiyu
 * @version 1.0
 */

import java.util.ArrayList;
import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CustomerIDEndDateQueryBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";
	
	//private String mSelectType = "";

	private String mSql = "";

	//查询出投保人证件失效的清单
	private String[][] mExcelData = null;

	public CustomerIDEndDateQueryBL() {}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}

		// 准备所有要打印的数据
		if (!getPrintData()) {
			buildError("getPrintData", "下载失败");
			return null;
		}

		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

		mManageCom = (String) mTransferData.getValueByName("ManageCom");
		if (mManageCom.equals("") || mManageCom == null) {
			buildError("getInputData", "没有得到足够的信息:管理机构不能为空");
			return false;
		}
		
		/*mSelectType = (String) mTransferData.getValueByName("SelectType");
		if (mSelectType.equals("") || mSelectType == null) {
			buildError("getInputData", "没有得到足够的信息:查询层次不能为空");
			return false;
		}*/

		mSql = (String) mTransferData.getValueByName("SQL");

		if (mSql.equals("") || mSql == null) {
			buildError("getInputData", "获取清单查询语句失败");
			return false;
		}
		
		System.out.println(mManageCom);
		//System.out.println(mSelectType);
		System.out.println(mSql);

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LCGrpContF1PBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "LoseEfficacyCustomerList" };
		mCreateExcelList.addSheet(sheetName);
		// 设置表头
		//String[][] tTitle1 = {{ "管理机构编码", "管理机构名称", "客户号", "客户姓名", "性别", "证件类型", "证件号码", "手机号码", "联系电话", "联系地址", "证件有效期始期", "证件有效期止期" } };
		String[][] tTitle2 = {{ "管理机构编码", "管理机构名称", "保单号", "投保险种", "投保人号", "投保人姓名", "投保人证件类型", "投保人证件号码", "投保人证件有效期始期", "投保人证件有效期止期", "投保人手机号码", "投保人联系电话", "被保人号", "被保人姓名", "被保人证件类型", "被保人证件号码", "被保人证件有效期始期", "被保人证件有效期止期", "被保人手机号码", "被保人联系电话", "保费", "保单状态", "销售渠道", "销售人员代码", "销售人员姓名" } };
		// 表头的显示属性
		//int[] displayTitle1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		int[] displayTitle2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
		// 数据的显示属性
		//int[] displayData1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};		
		int[] displayData2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
		
		int row = 0;
		/*if("0".equals(mSelectType)) {   //客户层
			row = mCreateExcelList.setData(tTitle1, displayTitle1);
			if (row == -1) {
				buildError("getPrintData", "EXCEL中指定数据失败！");
				return false;
			}
		}else if("1".equals(mSelectType)) {   //保单层
        */
		row = mCreateExcelList.setData(tTitle2, displayTitle2);
			if (row == -1) {
				buildError("getPrintData", "EXCEL中指定数据失败！");
				return false;
			}
		//}
		
		System.out.println("row : "+row);
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(mSql);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}
		/*
		if("0".equals(mSelectType)) {
			if (mCreateExcelList.setData(mExcelData, displayData1) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}
		}else if("1".equals(mSelectType)) {*/
			if (mCreateExcelList.setData(mExcelData, displayData2) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}
		//}
		
		return true;
	}

	public static void main(String[] args) {

		CustomerIDEndDateQueryBL tbl = new CustomerIDEndDateQueryBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ManageCom", "8644");
		tGlobalInput.ManageCom = "8644";
		// tGlobalInput.Operator="xp";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData);
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\cytzpare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}
}
