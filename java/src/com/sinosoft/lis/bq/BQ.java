package com.sinosoft.lis.bq;

/**
 * <p>Title: 保全常量</p>
 * <p>Description: 存放保全状态，核保状态等常量</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */
public class BQ
{
    public BQ() {}

    /** 是 */
    public static final String TRUE = "1";

    /** 否  */
    public static final String FALSE = "0";

    /** 保全状态，保全初始状态 */
    public static final String EDORSTATE_INIT = "3";

    /** 保全状态，保全明细录入后状态 */
    public static final String EDORSTATE_INPUT = "1";

    /** 保全状态，保全理算后状态 */
    public static final String EDORSTATE_CAL = "2";

    /** 保全状态，保全确认后状态 */
    public static final String EDORSTATE_CONFIRM = "0";

    /** 保全状态，待收费状态 */
    public static final String EDORSTATE_WAITPAY = "9";

    /** 保全状态，送核状态 */
    public static final String EDORSTATE_SENDUW = "10";

    /** 保全状态，核保完毕 */
    public static final String EDORSTATE_AFTERUW = "11";

    /** 保全状态，延期结案 */
    public static final String EDORSTATE_DELAY_FINISH = "12";

    /** 保全状态，函件已回销 */
    public static final String EDORSTATE_LETTERBACK = "7";

    /** 核保状态，未经核保 */
    public static final String UWFLAG_INIT = "0";

    /** 核保状态，自动核保通过 */
    public static final String UWFLAG_PASS = "9";

    /** 核保状态，自动核保未通过 */
    public static final String UWFLAG_FAIL = "5";

    /** 核保状态，经过人工核保 */
    public static final String UWFLAG_MANU = "1";

    /** 人工核保通过标志, 批准申请 */
    public static final String PASSFLAG_PASS = "1";

    /** 人工核保通过标志, 终止申请, 即撤销该项目*/
    public static final String PASSFLAG_STOP = "2";

    /** 人工核保通过标志, 险种解约 */
    public static final String PASSFLAG_CANCEL = "3";

    /** 人工核保通过标志, 附加条件 */
    public static final String PASSFLAG_CONDITION = "4";

    /** 人工核保通过标志, 续保终止*/
    public static final String PASSFLAG_STOPXB = "5";

    /** 人工核保通过标志, 核保结论修改*/
    public static final String PASSFLAG_MODIFY = new String("6");

    /** 补退费类型，保全个单 */
    public static final String NOTICETYPE_P = "10";

    /** 补退费类型，保全团单 */
    public static final String NOTICETYPE_G = "3";

    /** 保单状态，未生效 */
    public static final String APPFLAG_INIT = "0";

    /** 保单状态，已签单 */
    public static final String APPFLAG_SIGN = "1";

    /** 保单状态，保全数据 */
    public static final String APPFLAG_EDOR = "2";

    /** 保单类别，个单 */
    public static final String CONTTYPE_P = "0";

    /** 保单类别，团单 */
    public static final String CONTTYPE_G = "1";

    /** 新契约保费计算方向，标准表定费率 */
    public static final String CALRULE_STANDARD = "0";

    /** 新契约保费计算方向，折扣费率 */
    public static final String CALRULE_DISCOUNT = "2";

    /** 新契约保费计算方向，约定费率 */
    public static final String CALRULE_PROMISE = "3";

    /** 新契约保费计算方向，导入保费 */
    public static final String CALRULE_IMPORT = "4";

    /** 保全特殊数据类型子类型，工本费*/
    public static final String DETAILTYPE_GB = "GB";

    /** 保全特殊数据类型子类型， 个单协议退费金额*/
    public static final String DETAILTYPE_XTFEE = "XTFEE";

    /** 保全特殊数据类型子类型， 团但协议退费金额*/
    public static final String DETAILTYPE_XTFEEG = "XTFEEG";

    /**个单协议退费比例*/
    public static final String XTFEERATEP = "XTFEERATEP";

    /**团单协议退费比例*/
    public static final String XTFEERATEG = "XTFEERATEG";

    /** 保全特殊数据类型子类型， 函件*/
    public static final String DETAILTYPE_EDORSTATE = "EDORSTATE";

    /** 保全特殊数据类型子类型， 减人算法类型*/
    public static final String DETAILTYPE_CALTYPE = "CALTYPE";

    /** 保全特殊数据类型子类型， 帐户余额退还方式*/
    public static final String DETAILTYPE_BALAPAYTYPE = "BALAPAYTYP";

    /** 保全特殊数据类型子类型， 帐户余额退还方式：转到团体账户*/
    public static final String DETAILTYPE_BALAPAYTYPE1 = "1";

    /** 保全特殊数据类型子类型， 帐户余额退还方式：退还公司*/
    public static final String DETAILTYPE_BALAPAYTYPE2 = "2";

    /** 保全特殊数据类型子类型， 满期利息利率录入方式*/
    public static final String DETAILTYPE_ENDTIME_RATETYPE = "ENDTIME_RATETYPE";

    /** 保全特殊数据类型子类型， 满期利息利率录入方式，录入利息*/
    public static final String DEALTYPE_INTERESTINPUT = "1";

    /** 保全特殊数据类型子类型， 满期利息利率录入方式，录入利率*/
    public static final String DEALTYPE_RATEINPUT = "2";

    /** 保全特殊数据类型子类型， 满期利息利率录入方式，默认利率*/
    public static final String DEALTYPE_RATEDEFAULT = "3";

    /** 保全特殊数据类型子类型， 满期处理*/
    public static final String DETAILTYPE_ENDTIME_DEAL = "ENDTIME_DEAL";

     /** 保全特殊数据类型子类型， 满期处理状态*/
     public static final String DETAILTYPE_MJSTATE = "MJSTATE";

     /** 保全特殊数据类型子类型， 满期结算单打印次数*/
     public static final String DETAILTYPE_MJPRINTTIMES = "LISTPRINTTIMES";

    /** 保全特殊数据类型子类型， 账户利率*/
    public static final String DETAILTYPE_ACCRATE = "ACCRATE";

    /** 保全特殊数据类型子类型， 团体医疗账户利息*/
    public static final String DETAILTYPE_INTEREST_GROUP = "INTEREST_GROUP";

    /** 保全特殊数据类型子类型， 团体固定账户利息*/
    public static final String DETAILTYPE_INTEREST_FIXED = "INTEREST_FIXED";

    /** 保全特殊数据类型子类型， 个人账户利息*/
    public static final String DETAILTYPE_INTEREST_INSURED = "INTEREST_INSURED";

    /** 保全特殊数据类型子类型， 被保人数*/
    public static final String DETAILTYPE_PEOPLES2 = "PEOPLES2";

    /** 保全特殊数据类型子类型， 生效日期*/
    public static final String DETAILTYPE_CVALIDATETYPE = "CVALIDATETYPE";

    /** 补/退费财务类型，补费*/
    public static final String FEEFINATYPE_BF = "BF";

    /** 补/退费财务类型，管理费*/
    public static final String FEEFINATYPE_MF = "MF";

    /**补/退费财务类型，冲退保金*/
    public static final String FEEFINATYPE_TB = "TB";

    /**补/退费财务类型，冲退保费*/
    public static final String FEEFINATYPE_TF = "TF";

    /** 补/退费财务类型，保单工本费*/
    public static final String FEEFINATYPE_GB = "GB";
    
    /** 补/退费财务类型，红利利息*/
    public static final String FEEFINATYPE_HLLX = "HLLX";
    
    /** 补/退费财务类型，红利补费 */
    public static final String FEEFINATYPE_HLBF = "HLBF";

    /** 补/退费财务类型，体检费*/
    public static final String FEEFINATYPE_TJ = "TJ";

    /** 补/退费财务类型，投保人帐户转入*/
    public static final String FEEFINATYPE_YEI = "YEI";

    /** 补/退费财务类型，投保人帐户抵扣*/
    public static final String FEEFINATYPE_YE0 = "YEO";

    /** 补/退费财务类型，协议退保时公司的盈利：正常退保退费-协议退保退费*/
    public static final String FEEFINATYPE_YL = "YL";

    /** 补/退费财务类型，定期结算 */
    public static final String FEEFINATYPE_JS = "JS";

    /** 表明无效数据，用于向数据库中填充无效字段 */
    public static final String FILLDATA = "000000";

    /** 2008-9-10 按销售模块要求新增字段 */
    public static final String FILLDATA1 = "333333";

    /** 表明无效数据，用于向数据库中填充无效字段，个险团体号专用 */
    public static final String GRPFILLDATA = "00000000000000000000";

    /** 保全项目类型代码，投保单位资料变更 */
    public static final String EDORTYPE_AC = "AC";

    /** 保全项目类型代码，联系方式变更 */
    public static final String EDORTYPE_AD = "AD";
    
    /** 保全项目类型代码，受益人变更 */
    public static final String EDORTYPE_BC = "BC";

    /** 保全项目类型代码，保障保费调整 */
    public static final String EDORTYPE_BF = "BF";

    /** 保全项目类型代码，保单拆分 */
    public static final String EDORTYPE_CF = "CF";

    /** 保全项目类型代码，团体帐户资金分配 */
    public static final String EDORTYPE_GA = "GA";
    
    /** 保全项目类型代码，团体公共保额分配 */
    public static final String EDORTYPE_GD = "GD";
    
    /** 保全项目类型代码，团险万能部分领取 */
    public static final String EDORTYPE_TL = "TL";
    
    /** 保全项目类型代码，团险万能离职领取 */
    public static final String EDORTYPE_TQ = "TQ";
    
    /** 保全项目类型代码，团险万能满期领取 */
    public static final String EDORTYPE_UM = "UM";
    
    /** 保全项目类型代码，团险万能身故处理 */
    public static final String EDORTYPE_SG = "SG";
    
    /** 保全项目类型代码，犹豫期退保 */
    public static final String EDORTYPE_WT = "WT";

    /** 保全项目类型代码，增加被保人 */
    public static final String EDORTYPE_NI = "NI";
    
    /** 保全项目类型代码，增加被保人 */
    public static final String EDORTYPE_LN = "LN";

    /** 保全项目类型代码，减少被保人 */
    public static final String EDORTYPE_ZT = "ZT";

    /** 保全项目类型代码，退保 */
    public static final String EDORTYPE_CT = "CT";
    /** 保全项目类型代码，团险万能解约 */
    public static final String EDORTYPE_JY = "JY";

    /** 保全项目类型代码，保单遗失补发 */
    public static final String EDORTYPE_LR = "LR";
    
    /** 保全项目类型代码，保全转保 */
    public static final String EDORTYPE_TP = "TP";

    /** 保全项目类型代码，保单遗失补发 */
    public static final String EDORTYPE_XT = "XT";

    /** 保全项目类型代码，交费资料变更 */
    public static final String EDORTYPE_CC = "CC";
    
    /** 保全项目类型代码，红利领取方式变更 */
    public static final String EDORTYPE_DD = "DD";

    /** 保全项目类型代码，交费频次变更 */
    public static final String EDORTYPE_PC = "PC";

    /** 保全项目类型代码，客户资料变更 */
    public static final String EDORTYPE_CM = "CM";

    /**保全项目类型代码，特需满期结算*/
    public static final String EDORTYPE_MJ = "MJ";

    /**保全项目类型代码，无名单实名化*/
    public static final String EDORTYPE_WS = "WS";

    /**保全项目类型代码，无名单实名化删除*/
    public static final String EDORTYPE_WD = "WD";

    /**保全项目类型代码，追加保费*/
    public static final String EDORTYPE_ZB = "ZB";
    
    /**保全项目类型代码，补充医疗追加保费*/
    public static final String EDORTYPE_ZA = "ZA";
    
    /**保全项目类型代码，追加管理费*/
    public static final String EDORTYPE_ZG = "ZG";
    
    /**保全项目类型代码，追加保费*/ //BY GZH 
    public static final String EDORTYPE_TY = "TY";
    /** 保全项目类型代码，团体帐户资金分配 */
    public static final String EDORTYPE_TA = "TA";
    /**保全项目类型代码，团险万能增人*/ 
    public static final String EDORTYPE_TZ = "TZ";

    /**保全项目类型代码，余额提前领取*/
    public static final String EDORTYPE_LQ = "LQ";

    /**保全项目类型代码，续保二核*/
    public static final String EDORTYPE_XB = "XB";

    /**保全项目类型代码，无名单增人*/
    public static final String EDORTYPE_WZ = "WZ";

    /**保全项目类型代码，无名单减人*/
    public static final String EDORTYPE_WJ = "WJ";

    /** 核保 */
    public static final String EDORTYPE_UW = "UW";

    /**保全项目类型代码，增加险种 */
    public static final String EDORTYPE_NS = "NS";

    /**保全项目类型代码，保单复效*/
    public static final String EDORTYPE_FX = "FX";

    /**保全项目类型代码，定期结算*/
    public static final String EDORTYPE_DJ = "DJ";

    /**保全项目类型代码，服务频次变更*/
    public static final String EDORTYPE_FP = "FP";

    /**保全项目类型代码，保单迁移*/
    public static final String EDORTYPE_PR = "PR";

    /**保全项目类型代码，投保事项变更*/
    public static final String EDORTYPE_TB = "TB";

    /** 保全项目类型代码，退保 */
    public static final String EDORTYPE_ZF = "ZF";

    /** 保全项目类型代码，特权复效 */
    public static final String EDORTYPE_TF = "TF";
    
    /** 保全项目类型代码，常B给付补费 */
    public static final String EDORTYPE_CB = "CB";
    
    /** 保全项目类型代码，分红险满期领取 */
    public static final String EDORTYPE_HA = "HA";
    
    /** 保全项目类型代码，免息复效 */
    public static final String EDORTYPE_MF = "MF";
    
    /** 保全项目类型代码，管理式医疗减少保额 */
    public static final String EDORTYPE_ZE = "ZE";

    /** 险种payPlanCode，在生成财务接口数据时，以PAYPLANCODE_ADDFEE标识加费，
     * 供销售提取佣金时去除加费提供依据 */
    public static final String PAYPLANCODE_ADDFEE = "111111";
    /*
     * 险种payPlanCode,
     * */
    public static final String PAYPLANCODE_SUPPLEMENTARY="222222";

    /** 机构编码，总公司 */
    public static final String COMCODE_HEAD = "86";

    /** 险种分类8，TPA产品 */
    public static final String RISKTYPE8_TPA = "6";

    /** 险种分类3，特需医疗帐户类型 */
    public static final String RISKTYPE3_SPECIAL = "7";

    /** 险种分类4，万能险帐户类型，这里由于涉及界面过多所以没有改成 RISKTYPE4_ULI = "4" */
    public static final String RISKTYPE1_ULI = "4";
    
    /** 险种分类4，分红险产品 */
    public static final String RISKTYPE4_BONUS = "2";

    /** 险种分类5，极短险 */
    public static final String RISKTYPE5_LESSTHANYEAR = "1";

    /** 险种分类5，一年期险 */
    public static final String RISKTYPE5_YEAR = "2";

    /** 险种分类5，长期险定期 */
    public static final String RISKTYPE5_LONGTIME = "3";

    /** 险种分类5，长期险终身 */
    public static final String RISKTYPE5_FOREVER = "4";

    /** 险种分类5，其他 */
    public static final String RISKTYPE5_OTHER = "5";

    /** 特需医疗帐户类型，个人理赔帐户 */
    public static final String ACCTYPE_INSURED = "002";

    /** 特需医疗帐户类型，团体理赔帐户 */
    public static final String ACCTYPE_GROUP = "001";

    /** 特需医疗帐户类型，团体固定帐户 */
    public static final String ACCTYPE_FIXED = "004";
    
    /** 红利险种个人账户 */
    public static final String ACCTYPE_BONUS = "005";

    /**新添加的类型用来标识账户型的公共理赔账户，lcpol.poltypfalg ='2' and lcpol.acctype='1' 标识团体理赔账户*/
    public static final String ACCTYPE_PUBLIC ="1";


    /** 特需管理费类型，个人帐户管理费 */
    public static final String FEECODE_INSURED = "000001";

    /** 特需管理费类型，团体帐户管理费 */
    public static final String FEECODE_GROUP = "000002";

    /**特需管理费类型：帐户解约管理费*/
    public static final String FEECODE_CT = "000006";


    /** 险种类型，公共账户 */
    public static final String POLTYPEFLAG_PUBLIC = "2";

    /** 险种类型，无名单 */
    public static final String POLTYPEFLAG_NONAME = "1";

    /** 险种类型，普通被保人 */
    public static final String POLTYPEFLAG_INSURED = "0";

    /** 磁盘导入成功 */
    public static final String IMPORTSTATE_SUCC = "1";

    /** 磁盘导入失败 */
    public static final String IMPORTSTATE_FAIL = "0";

    /** 磁盘导入配置文件 */
    public static final String FILENAME_IMPORT = "BqImport.xml";

    /**与主被保人关系：本人*/
    public static final String MAININSURED = "00";

    /**满期处理：续保*/
    public static final String ENDTIME_KEEP = "1";

    /**保单状态：满期终止*/
    public static final String ENDTIME_STOP = "2";

    public static final String CONTSTATE_MJING = "03030001";  //正在满期结算
    public static final String CONTSTATE_MJED = "03030002";  //满期终止
    public static final String CONTSTATE_MJSIGN = "00060001";  //续保有效

    /**保单状态，续保终止*/
    public static final String POLSTATE_XBEND = "03050001";

    /**保单状态，设缴费终止*/
    public static final String POLSTATE_ZFEND = "03050002";
    
    
    /**团险万能满期终止批处理*/
    public static final String POLSTATE_UAENDDEAL = "03060001";
    
    /**团险万能满期领取保全结束*/
    public static final String POLSTATE_UAEND = "03060002";

    /**满期结算状态，未处理*/
    public static final String MJSTATE_INIT = "1";
    /**满期结算状态，满期理算完毕*/
    public static final String MJSTATE_CAL = "2";
    /**满期结算状态，已打印满期结算单*/
    public static final String MJSTATE_PRINTLIST = "3";
    /**满期结算状态，已结案*/
    public static final String MJSTATE_FINISH = "0";

    public static final String ACCBALA_FIEXD = "ACCBALAFIXED";  //团体固定账户余额
    public static final String ACCBALA_GROUP = "ACCBALAGROUP";  //团一医疗账户余额
    public static final String ACCBALA_INSURED = "ACCBALAINSURED"; //个人账户余额

    /** 新契约简易保全，投保人变更 */
    public static final String EASYEDORTYPE_APPNT = "1";

    /** 新契约简易保全，被保人变更 */
    public static final String EASYEDORTYPE_INSURED = "2";

    /** 新契约简易保全，受益人变更 */
    public static final String EASYEDORTYPE_BNF = "3";

    /** 打印标志，未打印 */
    public static final String PRINTFLAG_N = "0";

    /** 打印标志，已打印 */
    public static final String PRINTFLAG_Y = "1";

    /** 体检状态，通知书未录入 */
    public static final String PESTATE_INIT = "00";

    /** 体检状态，通知书已录入 */
    public static final String PESTATE_INPUT = "01";

    /** 体检状态，通知书已打印 */
    public static final String PESTATE_PRINT = "02";

    /** 核保类型，个单保全核保 */
    public static final String AUTOUWTYPE_IP = "IP";

    /** 核保类型，个单保全保单核保 */
    public static final String AUTOUWTYPE_IC = "IC";

    /** 核保类型，团单保全核保 */
    public static final String AUTOUWTYPE_GP = "GP";

    /** 减人默认费率 */
    public static final double ZT_FEERATE = 0.25;

    public static final String EDORNO = "EDORNO";

    /** 保单状态, 投保*/
    public static final String STATE_FLAG_APP = "0";

    /** 保单状态, 承保有效*/
    public static final String STATE_FLAG_SIGN = "1";

    /** 保单状态, 失效中止*/
    public static final String STATE_FLAG_AVAILABLE = "2";

    /** 保单状态, 终止*/
    public static final String STATE_FLAG_TERMINATE = "3";

    /**导入被保人已经理算*/
    public static final String IMPORTSTATE_CAL = "2";

    /**保全确认后所有送人工核保的险种都未通过将删除LPEdorItem,此时设置此REASONCODE ---20090218*/
    public static final String REASONCODE_HBZZ = "HBZZ";

    /**特需保单终止日期修改 关联字段lgwork-typeno , p表中edornotype*/
    public static final String CINVALIDATE_CHANGE = "09";

    /**保单挂起类型:保全挂起*/
    public static final String HANGUPTYPE_BQ ="2";

    /**保单挂起状态:未挂起*/
    public static final String HANGUSTATE_OFF ="0";

    /**保单挂起状态:已挂起*/
    public static final String HANGUSTATE_ON ="1";
    
    /**万能保费缓交:缓交状态 LCPol-StandByFlag1 */
    public static final String ULI_HUAN ="1";
    
    /**万能保费新缓交:StandByFlag1 缓交期间不收取风险保费*/
    public static final String ULI_DEFER ="2";
    
    /**红利领取方式:累积生息 LCPol-BonusGetMode */
    public static final String BONUSGET_GETINTEREST ="2";
    
    /**红利领取方式:现金领取 LCPol-BonusGetMode */
    public static final String BONUSGET_GETMONEY ="1";
    
    /**保单状态类型：万能保单续期缓交状态*/
    public static final String STATETYPE_ULIDEFER ="ULIDEFER";
}
