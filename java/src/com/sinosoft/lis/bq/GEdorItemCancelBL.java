package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.*;

/**
 * <p>Title:团单项目删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class GEdorItemCancelBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 撤销申请原因*/
    private String delReason;
    private String reasonCode;

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema(); //新的表结构
     private LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema(); //新的表结构
    TransferData tTransferData = new TransferData();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public GEdorItemCancelBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将VData数据还原成业务需要的类
        if (!getInputData())
        {
            return false;
        }
        //qulq modify 070119 添加后会删除其它项目的P表，导致项目失败。
/*
        if(!updateData())
        {
          return false;
        }
 */
        if (!dealData(mLPGrpEdorItemSchema))
        {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

//        PubSubmit tPubSubmit = new PubSubmit();
//
//        if (!tPubSubmit.submitData(mResult, cOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//            return false;
//        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例
        ExeSQL tExeSQL = new ExeSQL();


        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLPGrpEdorItemSchema.setSchema((LPGrpEdorItemSchema) mInputData.
                                       getObjectByObjectName(
                                               "LPGrpEdorItemSchema", 0));
        if (mLPGrpEdorItemSchema == null)
        {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));
            return false;
        }


       String  sql = "select * from LPGrpEdorItem where EdorNo='" + mLPGrpEdorItemSchema.getEdorNo() + "'"
           + " and grpContNo='" + mLPGrpEdorItemSchema.getGrpContNo() + "'"
           + " and EdorType='" + mLPGrpEdorItemSchema.getEdorType() + "'";
       LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
       tLPGrpEdorItemSchema = tLPGrpEdorItemDB.executeQuery(sql).get(1);

       String edorState = tLPGrpEdorItemSchema.getEdorState();
       if (edorState != null && "0".equals(edorState))
       {
         CError tError = new CError();
         tError.moduleName = "GEdorItemCancelBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "该项目已经保全确认,不能删除!";
         this.mErrors.addOneError(tError);
         System.out.println(tError);
         return false;
       }


       tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
       delReason = (String)tTransferData.getValueByName("DelReason");
       reasonCode = (String)tTransferData.getValueByName("ReasonCode");
        //判断选中批改项目是否为最近的批改项目
//        String strsql =
//                "select Max(a.MakeDate) from lpGrpedoritem a where a.edorno='" +
//                mLPGrpEdorItemSchema.getEdorNo() + "' ";
//        String LatestDate = tExeSQL.getOneValue(strsql).substring(0, 10);
//        if (mLPGrpEdorItemSchema.getMakeDate().toString().equals(LatestDate)) //日期相等
//        {
//            strsql =
//                    "select Max(a.MakeTime) from lpGrpedoritem a where a.edorno= '" +
//                    mLPGrpEdorItemSchema.getEdorNo() + "'  and a.makeDate='" +
//                    LatestDate + "' ";
//
//            String LatestTime = tExeSQL.getOneValue(strsql).toString();
//            if (!mLPGrpEdorItemSchema.getMakeTime().toString().equals(
//                    LatestTime))
//            {
//                this.mErrors.addOneError(new CError("该项目不是最后一条，无法删除!"));
//                return false;
//            }
//
//        }
//        else
//        {
//            this.mErrors.addOneError(new CError("该项目不是最后一条，无法删除!"));
//            return false;
//        }

        return true;
    }
    /**
     *
     * @return boolean
     */
    private boolean updateData()
    {
      LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
      LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();

      String strSql = "select * from LPGrpEdorItem where EdorState in ('1','2','4')"
          + " and EdorNo='" + tLPGrpEdorItemSchema.getEdorNo() +
          "'  and (MakeDate>'" + tLPGrpEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
          tLPGrpEdorItemSchema.getMakeDate() + "' and MakeTime>'" +
          tLPGrpEdorItemSchema.getMakeTime() +
          "')) order by MakeDate,MakeTime desc";

      System.out.println(strSql);
      tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(strSql);
      if (tLPGrpEdorItemSet.size() != 0 && tLPGrpEdorItemSet != null) {
        for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++) {
          boolean flag = dealData(tLPGrpEdorItemSet.get(i));
          if (!flag) {
            CError tError = new CError();
            tError.moduleName = "ContCancel";
            tError.functionName = "dealData";
            tError.errorMessage = "更新该项目后的数据失败!";
            this.mErrors.addOneError(tError);
            System.out.println(tError);
            return false;
          }
          else {
            tLPGrpEdorItemSet.get(i).setEdorState("3"); //重新置为未录入
            tLPGrpEdorItemSet.get(i).setOperator(mGlobalInput.Operator);
            tLPGrpEdorItemSet.get(i).setModifyDate(this.theCurrentDate);
            tLPGrpEdorItemSet.get(i).setModifyTime(this.theCurrentTime);

            //对团单下面的LPedoritem表进行更新
            LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(tLPGrpEdorItemSet.get(i).getEdorNo());
            tLPEdorItemDB.setEdorType(tLPGrpEdorItemSet.get(i).getEdorType());
            tLPEdorItemDB.setGrpContNo(tLPGrpEdorItemSet.get(i).getGrpContNo());
            tLPEdorItemSet = tLPEdorItemDB.query();
            if(tLPEdorItemSet.size()!=0 && tLPEdorItemSet != null)
            {
              for (int j = 1; j<=tLPEdorItemSet.size();j++)
              {
                tLPEdorItemSet.get(j).setEdorState("3");
                tLPEdorItemSet.get(j).setOperator(mGlobalInput.Operator);
                tLPEdorItemSet.get(j).setModifyDate(this.theCurrentDate);
                tLPEdorItemSet.get(j).setModifyTime(this.theCurrentTime);
              }
              mMap.put(tLPEdorItemSet,"UPDATE");
            }
          }
        }
        mMap.put(tLPGrpEdorItemSet, "UPDATE");

      }

  //
  //     //if(mEdorState.equals(""))

      return true;

    }

    /**
     * 对业务数据进行加工
     * @return boolean
     */
    private boolean dealData(LPGrpEdorItemSchema aLPGrpEdorItemSchema)
    {
        String delSql;
        String tEdorNo = aLPGrpEdorItemSchema.getEdorNo();

        String tEdorType = aLPGrpEdorItemSchema.getEdorType();
        //将将数据备份，并存储备份信息
        LPGrpEdorItemSchema cLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        LOBGrpEdorItemSchema cLOBGrpEdorItemSchema = new LOBGrpEdorItemSchema();
        LPGrpEdorItemDB cLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet cLPGrpEdorItemSet = new LPGrpEdorItemSet();

        //选出当前记录
        String sql = "select * from LpGrpEdorItem where EdorNo='" + tEdorNo +
            "' and EdorType= '" + tEdorType + "'";
        System.out.println(sql);
        cLPGrpEdorItemSet = cLPGrpEdorItemDB.executeQuery(sql);
        if (cLPGrpEdorItemSet.size() > 0 && cLPGrpEdorItemSet != null) {
          for (int k = 1; k <= cLPGrpEdorItemSet.size(); k++) {
            cLPGrpEdorItemSchema = cLPGrpEdorItemSet.get(k);
            System.out.println("cLPGrpEdorItemSchema:" +
                               cLPGrpEdorItemSchema.getFieldCount()); //测试用
            Reflections crf = new Reflections();
            crf.transFields(cLOBGrpEdorItemSchema, cLPGrpEdorItemSchema); //将一条记录整体复制
            cLOBGrpEdorItemSchema.setReason(delReason); //添加撤销原因
            cLOBGrpEdorItemSchema.setReasonCode(reasonCode); //添加撤销原因
            cLOBGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
            cLOBGrpEdorItemSchema.setMakeDate(theCurrentDate); //设置修改时间为当前时间
            cLOBGrpEdorItemSchema.setMakeTime(theCurrentTime);
            cLOBGrpEdorItemSchema.setModifyDate(theCurrentDate);
            cLOBGrpEdorItemSchema.setModifyTime(theCurrentTime);
          }
          mMap.put(cLOBGrpEdorItemSchema, "DELETE&INSERT");
        }

        String[] tableForDel =
                {
                "LPGeneral", "LPGrpCont", "LPCont", "LPGeneralToRisk", "LPPol",
                "LPGrpAppnt", "LPGrpPol", "LPGetToAcc", "LPInsureAcc",
                "LPPremToAcc", "LPPrem", "LPGet", "LPDuty", "LPPrem_1",
                "LPInsureAccClass", "LPInsureAccTrace", "LPContPlanDutyParam",
                "LPContPlanRisk", "LPContPlan", "LPAppnt", "LPCustomerImpart",
                "LPInsuredRelated", "LPBnf", "LPInsured","LPInsureAccFeeTrace",
                "LPCustomerImpartParams", "LPInsureAccFee",
                "LPInsureAccClassFee",
                "LPContPlanFactory", "LPContPlanParam", "LPGrpFee",
                "LPGrpFeeParam", "LPMove", "LPAppntTrace", "LPLoan",
                "LPReturnLoan", "LPEdorPrint3", "LPGUWError", "LPGUWMaster",
                "LPCUWError",
                "LPCUWMaster", "LPCUWSub", "LPUWError", "LPUWMaster", "LPUWSub",
                "LPGCUWError", "LPGCUWMaster", "LPGCUWSub", "LPGUWSub",
                "LPSpec", "LPIssuePol", "LPGrpIssuePol", "LPCGrpSpec",
                "LPCSpec", "LPAccount", "LPPerson", "LPAddress", "LPGrpAddress",
                "LPGrp", "LPPayRuleFactory", "LPPayRuleParams",
                "LPCustomerImpartDetail", "LPAccMove", "LPEdorEspecialData",
                "LPDiskImport", "LPBudgetResult","lpgrpposition","lpAscriptionrulefactory","LpAscriptionRuleParams"};
        for (int i = 0; i < tableForDel.length; i++)
        {
            delSql = "delete from  " + tableForDel[i] + " where EdorNo = '" +
                     tEdorNo + "' and EdorType='" + tEdorType + "'";
            mMap.put(delSql, "DELETE");
        }


        //处理两个特殊的表，没有edorType 2005-03-31
        String strDelAdd = "delete from LPPENoticeItem where EdorNo = '" +tEdorNo + "'";
        mMap.put(strDelAdd, "DELETE");
        strDelAdd = "delete from LPPENotice where EdorNo = '" +tEdorNo + "'";
        mMap.put(strDelAdd, "DELETE");

        mMap.put("delete from lpedorMain where EdorNo= '" + tEdorNo +
                 "' and 0=(select count(*) from LpEdorItem where lpedorItem.EdorNo='" +
                 tEdorNo + "')",
                 "DELETE"); //若是最后一条项目，则删除对应批单
        mMap.put("delete from LCInsuredList where edorNo = '" + tEdorNo
                 + "'  and 0=(select count(*) from LpEdorItem where lpedorItem.EdorNo='" +
                 tEdorNo + "')",
                 "DELETE");

        //更新主表相关费用字段
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(tEdorNo);
//        tLPGrpEdorMainDB.setEdorAcceptNo(aLPGrpEdorItemSchema.getEdorAcceptNo());
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if (tLPGrpEdorMainSet == null || tLPGrpEdorMainSet.size() < 1)
        {
            this.mErrors.addOneError(new CError("批单主表中没有相关记录"));
            return false;
        }
        LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String strsql =
                "select sum(ChgPrem),sum(ChgAmnt),sum(GetMoney),sum(GetInterest) from LPGrpEdorItem a where a.edorno='" +
                aLPGrpEdorItemSchema.getEdorNo() + "' ";
        System.out.println(strsql);
        tSSRS = tExeSQL.execSQL(strsql);
        if (!"null".equals(tSSRS.GetText(1, 1)))
        {
            tLPGrpEdorMainSchema.setChgPrem(tSSRS.GetText(1, 1));
        }
        else
        {
            tLPGrpEdorMainSchema.setChgPrem(0);
        }
        if (!"null".equals(tSSRS.GetText(1, 2)))
        {
            tLPGrpEdorMainSchema.setChgAmnt(tSSRS.GetText(1, 2));
        }
        else
        {
            tLPGrpEdorMainSchema.setChgAmnt(0);
        }
        if (!"null".equals(tSSRS.GetText(1, 3)))
        {
            tLPGrpEdorMainSchema.setGetMoney(tSSRS.GetText(1, 3));
        }
        else
        {
            tLPGrpEdorMainSchema.setGetMoney(0);
        }
        if (!"null".equals(tSSRS.GetText(1, 4)))
        {
            tLPGrpEdorMainSchema.setGetInterest(tSSRS.GetText(1, 4));
        }
        else
        {
            tLPGrpEdorMainSchema.setGetInterest(0);
        }

        tLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
        tLPGrpEdorMainSchema.setModifyDate(this.theCurrentDate);
        tLPGrpEdorMainSchema.setModifyTime(this.theCurrentTime);
        mMap.put(tLPGrpEdorMainSchema, "UPDATE");

        //删除批改补退费表的相关记录
        mMap.put("delete from LJSGetEndorse where EndorsementNo='" +
                 aLPGrpEdorItemSchema.getEdorNo() + "' and FeeOperationType='" +
                 aLPGrpEdorItemSchema.getEdorType() + "'", "DELETE");
        System.out.println("288");
        try
        {
            Class tClass = Class.forName("com.sinosoft.lis.bq.GrpEdor" +
                                         tEdorType + "CancelBL");
            EdorCancel tGrpEdorCancel = (EdorCancel) tClass.newInstance();
            VData tVData = new VData();

            tVData.add(aLPGrpEdorItemSchema);
            tVData.add(mGlobalInput);

            if (!tGrpEdorCancel.submitData(tVData, "EDORCANCEL||" + tEdorType))
            {

                return false;
            }
            else
            {
                VData rVData = tGrpEdorCancel.getResult();
                MMap tMap = new MMap();
                tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                if (tMap == null)
                {
                    mErrors.addOneError(new CError("得到保单号为：" +
                            aLPGrpEdorItemSchema.getGrpContNo() + "保全项目为:" +
                            tEdorType + "的保全申请确认结果时失败！"));
                    return false;

                }
                else
                {
                    mMap.add(tMap);
                }
            }
        }
        catch (ClassNotFoundException ex)
        {

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        if(!AppAccUoDo())
        {
            return false;
        }

        //createTraceNodeOp();

        return true;
    }

    /**
     * 生成作业历史
     */
    private void createTraceNodeOp()
    {
        String sql = "  select * "
              + "from LGTraceNodeOp "
              + "where workNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
              + "order by int(nodeNo) desc, int(operatorNo) desc ";
        LGTraceNodeOpDB tLGTraceNodeOpDB = new LGTraceNodeOpDB();
        LGTraceNodeOpSet set = tLGTraceNodeOpDB.executeQuery(sql);
        if(set.size() > 0)
        {
            int operatorNo = Integer.parseInt(set.get(1).getOperatorNo());
            set.get(1).setOperatorNo(String.valueOf(operatorNo + 1));
            set.get(1).setOperatorType(Task.HISTORY_TYPE_DELETEEDOR);
            set.get(1).setRemark("撤销保全项目"
                                 + mLPGrpEdorItemSchema.getEdorType());
            set.get(1).setOperator(mGlobalInput.Operator);
            set.get(1).setMakeDate(this.theCurrentDate);
            set.get(1).setMakeTime(this.theCurrentTime);
            set.get(1).setModifyDate(this.theCurrentDate);
            set.get(1).setModifyTime(this.theCurrentTime);
            mMap.put(set.get(1), "INSERT");
        }
    }

    private boolean AppAccUoDo()
        {
            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
            tLPEdorAppDB.setEdorAcceptNo(tLPGrpEdorItemSchema.getEdorNo());
            if (!tLPEdorAppDB.getInfo()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GEdorItemCancelBL";
                tError.functionName = "AppAccUoDo";
                tError.errorMessage = "查询保全信息主表出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }

            AppAcc tAppAcc = new AppAcc();
            String noticeType="";
            if(tLPEdorAppDB.getOtherNoType().equals("1"))
            {
                noticeType=BQ.NOTICETYPE_P;
            }
            if (tLPEdorAppDB.getOtherNoType().equals("2")) {
                noticeType=BQ.NOTICETYPE_G;
            }
            MMap map = tAppAcc.accUnDo(tLPEdorAppDB.getOtherNo(), tLPGrpEdorItemSchema.getEdorNo(),
                                   noticeType);

            if (map == null) {
                if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!")) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tAppAcc.mErrors);
                    return false;
                }
            }
            mMap.add(map);

            return true;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {

      //删除并备份
      LOBGrpEdorItemSchema cLOBGrpEdorItemSchema = new LOBGrpEdorItemSchema();
      Reflections crf = new Reflections();
      crf.transFields(cLOBGrpEdorItemSchema, tLPGrpEdorItemSchema); //将一条记录整体复制

      cLOBGrpEdorItemSchema.setOperator(mGlobalInput.Operator);
      cLOBGrpEdorItemSchema.setMakeDate(theCurrentDate); //设置修改时间为当前时间
      cLOBGrpEdorItemSchema.setMakeTime(theCurrentTime);
      cLOBGrpEdorItemSchema.setReason(delReason); //添加撤销原因
      cLOBGrpEdorItemSchema.setReasonCode(reasonCode); //添加撤销原因

      mMap.put(cLOBGrpEdorItemSchema, "DELETE&INSERT");
      mMap.put(tLPGrpEdorItemSchema, "DELETE");

      String sql = "delete from lpedoritem where edorno='"+tLPGrpEdorItemSchema.getEdorNo()+"'"
          + " and edortype='"+tLPGrpEdorItemSchema.getEdorType()+"'"
          + " and grpcontno='"+tLPGrpEdorItemSchema.getGrpContNo()+"'";
      mMap.put(sql,"DELETE");
      
//    若是最后一条保全项目，则删除对应Main表,080515,Zhanggm
      if(!delLPGrpEdorMain())
      {
    	  CError tError = new CError();
	      tError.moduleName = "GrpEdorItemBL";
	      tError.functionName = "checkData";
	      tError.errorMessage = "删除保全项目对应Main表失败！";
	      this.mErrors.addOneError(tError);
      }

      mResult.clear();
      mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
    public MMap getMap()
    {
      return mMap;
    }
   
//  若是最后一条保全项目，则删除对应Main表,080515,Zhanggm
    private boolean delLPGrpEdorMain()
    {
    	System.out.println("若是最后一条保全项目，则删除对应Main表,080515,Zhanggm");
        String sql = "  delete from LPGrpEdorMain a "
      	           + "  where EdorNo = '" + tLPGrpEdorItemSchema.getEdorNo() + "'"
                   + "      and EdorAcceptNo = '"+tLPGrpEdorItemSchema.getEdorAcceptNo() + "'"
                   + "      and 0 = (select count(1) from LPGrpEdorItem "
                   + "               where EdorNo = a.EdorNo "
                   + "                   and EdorAcceptNo = a.EdorAcceptNo)";
        System.out.println(sql);
        mMap.put(sql , "DELETE"); 
        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo("430110000000421");
        tLPGrpEdorItemSchema.setGrpContNo("240110000000207");
        tLPGrpEdorItemSchema.setEdorType("AD");
        tLPGrpEdorItemSchema.setEdorState("2");
        tLPGrpEdorItemSchema.setMakeDate("2005-03-30");
        tLPGrpEdorItemSchema.setMakeTime("15:42:09");
        tVData.addElement(tGlobalInput);
        tVData.addElement(tLPGrpEdorItemSchema);
        tVData.addElement("ddddddddddd");

        GEdorItemCancelBL tGEdorItemCancelBL = new GEdorItemCancelBL();
        tGEdorItemCancelBL.submitData(tVData, "DELETE||EDOR");

    }

}
