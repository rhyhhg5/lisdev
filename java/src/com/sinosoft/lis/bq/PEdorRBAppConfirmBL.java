package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LPContSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LPContDB;
import com.sinosoft.lis.db.LPPolDB;
import com.sinosoft.lis.db.LPPremDB;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.utility.Reflections;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not fuxin
 * @version 1.0
 */
public class PEdorRBAppConfirmBL implements EdorAppConfirm {

    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String oldEdorNo ;
    double getmoney = 0;
    String actugetno ;

    /** 业务对象 */
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LGWorkSchema tLGWorkSchema = new LGWorkSchema();
    private LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
    private Reflections ref = new Reflections();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap map = new MMap();
    TransferData tempTransferData = new TransferData();
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();



    public PEdorRBAppConfirmBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }
        if (!dealDate())
        {
            return false ;
        }
//        //数据准备操作
//        if (!prepareData()) {
//            return false;
//        }

        prepareOutputData();

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData() {
        mResult.add(map);
    }

    /**
     * 从输入数据中得到所有对象
     * @return
     */
    private boolean getInputData() {
        try {
            mLPEdorItemSchema = (LPEdorItemSchema) mInputData.
                                getObjectByObjectName("LPEdorItemSchema", 0);
            mGlobalInput.setSchema((GlobalInput) mInputData.
                                   getObjectByObjectName("GlobalInput", 0));

        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        return true;
    }


     private boolean dealDate()
     {

         LGWorkDB tLGWorkDB = new LGWorkDB();
         tLGWorkDB.setWorkNo(mLPEdorItemSchema.getEdorAcceptNo());
         if (!tLGWorkDB.getInfo()) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "PEdorRBAppConfirmBL";
             tError.functionName = "prepareData";
             tError.errorMessage = "查询工单记录失败";
             this.mErrors.addOneError(tError);
             return false;
         }

         oldEdorNo = tLGWorkDB.getInnerSource();
         System.out.println("原来的工单号：" +oldEdorNo);

         LJAGetDB tLJAGetDB = new LJAGetDB();
         tLJAGetDB.setOtherNo(oldEdorNo);
         mLJAGetSet = tLJAGetDB.query();

         if (mLJAGetSet.size()==0)
         {
             getmoney =getmoney ;
         }else{
             getmoney = tLJAGetDB.query().get(1).getSumGetMoney();
             System.out.println("退费金额：" + getmoney);
         }

         tLJAGetEndorseDB.setEndorsementNo(oldEdorNo);
         LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
         for(int i = 1 ; i <=tLJAGetEndorseSet.size();i++)
         {
             LJAGetEndorseSchema tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
             LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
             ref.synchronizeFields(tLJSGetEndorseSchema,tLJAGetEndorseSchema);
             tLJSGetEndorseSchema.setGetNoticeNo(mLPEdorItemSchema.getEdorAcceptNo());
             tLJSGetEndorseSchema.setEndorsementNo(mLPEdorItemSchema.getEdorAcceptNo());
             tLJSGetEndorseSchema.setFeeOperationType(mLPEdorItemSchema.getEdorType());
             if(BQ.FEEFINATYPE_GB.equals(tLJAGetEndorseSchema.getFeeFinaType()))
             {
                 tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_GB); //工本费
             }else if(BQ.FEEFINATYPE_HLLX.equals(tLJAGetEndorseSchema.getFeeFinaType()))
             {
                 tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_HLLX); //红利利息
             }else if(BQ.FEEFINATYPE_HLBF.equals(tLJAGetEndorseSchema.getFeeFinaType())){
            	 tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_HLBF); //红利补费
             }else{
            	 tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF); //保费收入
             }
             //modify by fuxin 2009-1-16 责任加费，需要把加费的记录单独显示。
             if(BQ.FILLDATA.equals(tLJAGetEndorseSchema.getPayPlanCode()))
             {
                 tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA1);
             }else
             {
                 tLJSGetEndorseSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
             }
             tLJSGetEndorseSchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
             tLJSGetEndorseSchema.setGetMoney(-1*tLJAGetEndorseSchema.getGetMoney()); //WT退保回出现工本费。
             tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
             tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
             tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
             tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
             tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
             mLJSGetEndorseSet.add(tLJSGetEndorseSchema);
             map.put(tLJSGetEndorseSchema,"DELETE&INSERT");
         }
         /**
          * 更新LPEdorItem表的金额，在框架中会根据LPEdorItem的getmoney来更新
          * 保全的其它表。
          */
         String updateSql =" update LPEdorItem set GetMoney = "+getmoney+""
                          +" ,modifydate ='"+PubFun.getCurrentDate()+"',modifytime ='"+PubFun.getCurrentTime()+"'"
                          +" where edoracceptno ='"+mLPEdorItemSchema.getEdorAcceptNo()+"'"
                          ;
        System.out.println(updateSql);
        map.put(updateSql,"UPDATE");
        
        //处理分红险或者万能险信息
        LPContSet tLPContSet = getLPCcnt(oldEdorNo);
        LPPolSet tLPPolSet = getLPPol(oldEdorNo);
        
        //处理万能险或分红险的退保回退信息
        if(!dealULIRiskDate(tLPContSet,tLPPolSet,oldEdorNo)){
        	System.out.println("++++该保单没有万能险或者分红险++++++");
        }
         return true ;
     }

     /**
      * 处理万能险信息
      * WUJUN
      * @param tLPContSet
      * @param tLPPolSet
      * @return
      */
     public boolean dealULIRiskDate(LPContSet tLPContSet,LPPolSet tLPPolSet,String oldEdorNo){
    	 ExeSQL mExeSQL=new ExeSQL();
     	if(tLPContSet.size()<=0||tLPPolSet.size()<=0){
     		return false;
     	}
     	for(int i=1;i<=tLPContSet.size();i++){
     		//判断保单下是否有万能险或者分红险
     		if(this.hasULIRisk(tLPContSet.get(i).getContNo())||this.hasBonusRisk(tLPContSet.get(i).getContNo())){
     			//首先处理万能账户轨迹表
     			map.put("update LpInsureAccTrace set otherno='"+mLPEdorItemSchema.getEdorAcceptNo()+"', money = -money ,operator ='"+mGlobalInput.Operator+"', modifydate=current date,modifytime = current time where contno='"+tLPContSet.get(i).getContNo()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"'", "UPDATE");
     			map.put("update LpInsureAccTrace set MONEYTYPE='RB' ,operator ='"+mGlobalInput.Operator+"', modifydate=current date,modifytime = current time where contno='"+tLPContSet.get(i).getContNo()+"' and edorno='"+mLPEdorItemSchema.getEdorAcceptNo()+"' and moneytype in('CT','WT')", "UPDATE");

     			//处理lpinsureacc
     			String  sql = "select sum(money) from LbInsureAccTrace where contno='"+tLPContSet.get(i).getContNo()+"' and otherno !='"+oldEdorNo+"'" ;
     			String insuaccbala=mExeSQL.getOneValue(sql);
     			String sqlStr = "update LpInsureAcc set InsuAccBala="+insuaccbala+" ,LastAccBala="+insuaccbala+" , BalaDate =("+
     			" select max(duebaladate) from lcinsureaccbalance where contno='"+tLPContSet.get(i).getContNo()+"' and sequenceno !='"+oldEdorNo+"' " +
     			 " )  where contno='"+tLPContSet.get(i).getContNo()+"'" ; 
     			map.put(sqlStr,"UPDATE");
     			//处理lpinsureaccclass
     			String sqlStrClass = "update LpInsureAccClass set InsuAccBala="+insuaccbala+" ,LastAccBala="+insuaccbala+" , BalaDate =("+
     			"  select max(duebaladate) from lcinsureaccbalance where contno='"+tLPContSet.get(i).getContNo()+"' and sequenceno !='"+oldEdorNo+"' " + 
    			 " )  where contno='"+tLPContSet.get(i).getContNo()+"'" ; 
    			map.put(sqlStrClass,"UPDATE");
    			//处理lpinsureaccfeetrace 
    			map.put("update lpinsureaccfeetrace set otherno='"+mLPEdorItemSchema.getEdorAcceptNo()+"', fee = -fee ,operator ='"+mGlobalInput.Operator+"', modifydate=current date,modifytime = current time where contno='"+tLPContSet.get(i).getContNo()+"' and otherno='"+oldEdorNo+"'", "UPDATE");
    			//处理lpinsureaccfee
    			String sql1 = "select sum(fee) from LBInsureAccFeeTrace where contno='"+tLPContSet.get(i).getContNo()+"' and otherno !='"+oldEdorNo+"'" ;
    			String fee=mExeSQL.getOneValue(sql1);
    			if(fee==null||"".equals(fee)){
    				 fee="0";
    			}
     			String SqlFee = "update LpInsureAccFee set Fee="+fee+" , BalaDate =("+
    			 " select max(duebaladate) from lcinsureaccbalance where contno='"+tLPContSet.get(i).getContNo()+"' and sequenceno !='"+oldEdorNo+"')  where contno='"+tLPContSet.get(i).getContNo()+"'" ; 
    			map.put(SqlFee,"UPDATE");
    			//处理lpinsureaccClassfee
     			String SqlFeeClass = "update LpInsureAccClassFee set Fee="+fee+" , BalaDate =("+
     			" select max(duebaladate) from lcinsureaccbalance where contno='"+tLPContSet.get(i).getContNo()+"' and sequenceno !='"+oldEdorNo+"')  where contno='"+tLPContSet.get(i).getContNo()+"'" ; 
     			map.put(SqlFeeClass,"UPDATE");
     		}else{
     			return false;
     		}
     	}
     		return true;
     	}
     
     /**
      * 得到保单信息
      */
     private LPContSet getLPCcnt(String EdorNo) {
         StringBuffer sql = new StringBuffer();
         sql.append("select * ")
                 .append("from LPCont ")
                 .append("where EdorNo = '")
                 .append(EdorNo)
                 .append("' ");
         System.out.println(sql.toString());
         LPContDB tLPContDB = new LPContDB();
         LPContSet tLPContSet = tLPContDB.executeQuery(sql.toString());
         return tLPContSet;
     }
     
     /**
      * 判断保单下是否含有万能险种
      * @return boolean
      */
     public boolean hasULIRisk(String ContNo)
     {
         LBPolDB tLBPolDB = new LBPolDB();
         tLBPolDB.setContNo(ContNo);
         LBPolSet tLBPolSet = tLBPolDB.query();
         for (int i = 1; i <= tLBPolSet.size(); i++)
         {
             LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
             tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
             if (!tLMRiskAppDB.getInfo())
             {
                 return false;
             }
             String riskType4 = tLMRiskAppDB.getRiskType4();
             if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE1_ULI)))
             {
                 return true;
             }
         }
         return false;
     }
     
     
     /**
      * 判断保单下是否含有分红险种
      * @return boolean
      */
     public boolean hasBonusRisk(String ContNo)
     {
     	LBPolDB tLBPolDB = new LBPolDB();
         tLBPolDB.setContNo(ContNo);
         LBPolSet tLBPolSet = tLBPolDB.query();
         for (int i = 1; i <= tLBPolSet.size(); i++)
         {
             LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
             tLMRiskAppDB.setRiskCode(tLBPolSet.get(i).getRiskCode());
             if (!tLMRiskAppDB.getInfo())
             {
                 return false;
             }
             String riskType4 = tLMRiskAppDB.getRiskType4();
             if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE4_BONUS)))
             {
                 return true;
             }
         }
         return false;
     }

     /**
      * 得到险种信息
      */
     private LPPolSet getLPPol(String EdorNo) {
         StringBuffer sql = new StringBuffer();
         sql.append("select * ")
                 .append("from LPPol ")
                 .append("where EdorNo = '")
                 .append(EdorNo)
                 .append("' ");
         System.out.println(sql.toString());
         LPPolDB tLPPolDB = new LPPolDB();
         LPPolSet tLPPolSet = tLPPolDB.executeQuery(sql.toString());
         return tLPPolSet;
     }
}
