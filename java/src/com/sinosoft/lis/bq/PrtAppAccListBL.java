package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要修改理赔金帐户的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtAppAccListBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private LCAppAccSchema mLCAppAccSchema = null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    ListTable mtListTable = new ListTable();
    private String mOperate=null;

    public PrtAppAccListBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTransferData = (TransferData) cInputData.
                            getObjectByObjectName("TransferData", 0);
            mLCAppAccSchema = (LCAppAccSchema) cInputData.
                               getObjectByObjectName("LCAppAccSchema", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtAppAccList.vts", "printer"); //最好紧接着就初始化xml文档

        String Name=getCustomerName();
        if(Name==null)
        {
            return false;
        }
        //得到公司名
        tag.add("CustomerName", Name);
        tag.add("CustomerNo", mLCAppAccSchema.getCustomerNo());
        xmlexport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }
        String[] title = {"序号", "发生日期","金额增减" ,
                         "用途或来源", "相关受理号/保单号/通知号", "帐户余额"};

        xmlexport.addListTable(tListTable, title);
//        xmlexport.outputDocumentToFile("C:\\", "PrtAppAccListBL");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    //查询公司名称
    private String getCustomerName()
    {
        if(mOperate.equals("0"))
        {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLCAppAccSchema.getCustomerNo());
            if (!tLDPersonDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return null;
            }
            return tLDPersonDB.getName();
        }else if(mOperate.equals("1"))
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCAppAccSchema.getCustomerNo());
            if (!tLDGrpDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return null;
            }
            return tLDGrpDB.getGrpName();
        }else
        {
            mErrors.addOneError("传入的参数不正确。");
            return null;
        }
    }

    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getListTable()
    {
        LCAppAccTraceDB tLCAppAccTraceDB=new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(mLCAppAccSchema.getCustomerNo());
        tLCAppAccTraceDB.setState("1");
        LCAppAccTraceSet tLCAppAccTraceSet=tLCAppAccTraceDB.query();
        if(tLCAppAccTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrtAppAccListBL";
            tError.functionName = "getListTable";
            tError.errorMessage = "查询投保人帐户轨迹表出错!";
            this.mErrors.addOneError(tError);
            return null;
        }

        else if(tLCAppAccTraceSet.size()==0)
        {
            // @@错误处理
            mErrors.copyAllErrors(tLCAppAccTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrtAppAccListBL";
            tError.functionName = "getListTable";
            tError.errorMessage = "投保人帐户没有使用记录!";
            this.mErrors.addOneError(tError);
            return null;
        }
        for (int i = 1; i <= tLCAppAccTraceSet.size(); i++) {
            getAppAccInfo(i, tLCAppAccTraceSet.get(i));
        }

        mtListTable.setName("APPACC");
        return mtListTable;
    }

    private void getAppAccInfo(int t,
                                LCAppAccTraceSchema tLCAppAccTraceSchema)
    {
        System.out.println("insured index:" + t);
        String[] info = new String[6];
        info[0] = "" + t;
        info[1] = CommonBL.decodeDate(tLCAppAccTraceSchema.getConfirmDate());
        info[2] = StrTool.cTrim(String.valueOf(tLCAppAccTraceSchema.getMoney()));
        info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName("appaccdestsource",
                tLCAppAccTraceSchema.getDestSource()));
        info[4] = StrTool.cTrim(tLCAppAccTraceSchema.getOtherNo());
        info[5] = StrTool.cTrim(String.valueOf(tLCAppAccTraceSchema.getAccBala()));

        mtListTable.add(info);
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    private boolean checkData()
    {
        AppAcc tAppAcc=new AppAcc();
        mLCAppAccSchema = tAppAcc.getLCAppAcc(mLCAppAccSchema.getCustomerNo());
        if(mLCAppAccSchema == null)
        {
            this.mErrors.copyAllErrors(tAppAcc.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrtAppAccListBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询投保人帐户时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
