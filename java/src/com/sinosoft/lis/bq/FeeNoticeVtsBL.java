package com.sinosoft.lis.bq;

import java.io.*;

import org.jdom.input.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Blob;

/**
 * <p>Title: 生成交退费通知书</p>
 * <p>Description: 新生成的批单中包含了交费方式等内容 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */
public class FeeNoticeVtsBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    /** 保全受理号 */
    private String mEdorAcceptNo = null;

    /** 保存批单数据的Schema */
    private LPEdorAppPrintSchema mPrintSchema = null;

    /** XML操作 */
    private XmlExport xmlExport = new XmlExport();

    private TextTag textTag = new TextTag();

    /** 存放缴退费信息 */
    private TransferData mPayInfo = null;

    /**
     * 构造函数
     * @param edorAcceptNo String
     */
    public FeeNoticeVtsBL(String edorAcceptNo)
    {
        this.mEdorAcceptNo = edorAcceptNo;
    }

    /**
     * 提交数据
     * @param payInfo TransferData
     * @return boolean
     */
    public boolean submitData(TransferData payInfo)
    {
        this.mPayInfo = payInfo;

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        try
        {
            xmlExport.setDocument(getDocument());
            createNotice();
            updateData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 把xml数据转换成Document
     * @return Document
     */
    private org.jdom.Document getDocument() throws Exception
    {
        InputStream xmlStream = getXmlStream();
        SAXBuilder saxBuilder = new SAXBuilder();
        return saxBuilder.build(xmlStream);
    }

    /**
     * 从数据库中查出生成批单的xml数据
     * @return InputStream
     */
    private InputStream getXmlStream()
    {
        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorAppPrintDB.getInfo();
        //把查出的数据保存到mPrintSchema中
        mPrintSchema = tLPEdorAppPrintDB.getSchema();
        return tLPEdorAppPrintDB.getEdorInfo();
    }

    /**
     * 得到变更后的差额
     * @return double
     */
    private double getChangeMoney()
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError("查不到保全受理信息！");
        }
        return tLPEdorAppDB.getGetMoney();
    }

    /**
     * 生成交退费通知书
     * @return boolean
     */
    private boolean createNotice()
    {
        boolean accFlag = false;
        double money = getChangeMoney();
        //帐户处理begin
        AppAcc tAppAcc = new AppAcc();
        double accMoney = 0.0;
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        if (!tLPEdorAppDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "FeeNoticeVtsBL";
            tError.functionName = "createNotice";
            tError.errorMessage = "查询保全信息时出错!";
            System.out.println("------" + tError);
            this.mErrors.addOneError(tError);
            return false;
        }

        String noticeType="";
        if(tLPEdorAppDB.getOtherNoType().equals("1"))
        {
            noticeType = BQ.NOTICETYPE_P;
        }
        if (tLPEdorAppDB.getOtherNoType().equals("2"))
        {
            noticeType = BQ.NOTICETYPE_G;
        }
        LCAppAccTraceSchema tLCAppAccTraceSchema = tAppAcc.
                getLCAppAccTraceConfirm(
                tLPEdorAppDB.getOtherNo(), mEdorAcceptNo, noticeType);
        if (tLCAppAccTraceSchema == null)
        {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!"))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }
        else if(!tLCAppAccTraceSchema.getDestSource().equals("14"))//如果使用了帐户余额
        {
            accFlag = true;
            addContMoneyInfo();
            if (money > 0)
            {
                addPayMoneyInfo(money);
            }
            else
            {
                addGetMoneyInfo(money);
            }

            accMoney = tLCAppAccTraceSchema.getMoney();
            if (tLCAppAccTraceSchema.getAccType().equals("0"))
            { //抵扣或领取
                if (money >= Math.abs(accMoney))
                {
                    money = CommonBL.carry(money + accMoney); //accMoney为负值
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "FeeNoticeVtsBL";
                    tError.functionName = "createNotice";
                    tError.errorMessage = "帐户余额抵扣计算出错!";
                    System.out.println("------" + tError);
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            if (tLCAppAccTraceSchema.getAccType().equals("1"))
            { //转入帐户余额
                money = 0;
            }
            addAccFeeInfo(money);
            xmlExport.addDisplayControl("displayCash");
        }
        //帐户处理end

        if (money > 0)  //交费
        {
            addContMoneyInfo();
            addPayNoticeNo();
            addPayMoneyInfo(money);
            addPayMode(money);
        }
        else if (money < 0)  //退费
        {
            addContMoneyInfo();
            addGetNoticeNo();
            addGetMoneyInfo(money);
            addGetMode(money);
        }
        else
        {
            if (accFlag == false)
            {
                textTag.add("Blank_Info", "（以下信息空白）");
            }
        }

        //删除重复节点
        try
        {
//            XmlExport xml = deleteOldNode(xmlExport, textTag);
//            if(xml != null)
//            {
//                xmlExport = xml;
//            }
        }
        catch(Exception ex)
        {
            ex.notifyAll();
            CError tError = new CError();
            tError.moduleName = "FeeNoticeVtsBL";
            tError.functionName = "createNotice";
            tError.errorMessage = "删除批单旧信息失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        xmlExport.addTextTag(textTag);
        //结束标记
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        xmlExport.addListTable(tEndListTable);
        xmlExport.outputDocumentToFile("c:\\", "PEdorPrint");
        return true;
    }

    /**
     * deleteOldNode
     * 删除本次处理的原批单节点
     * @return boolean
     */
    private XmlExport deleteOldNode(XmlExport cXmlExport, TextTag cTextTag)
        throws Exception
    {
        //得到原批单信息
        InputStream ins = cXmlExport.getInputStream();

        if(ins != null)
        {
            //修改节点信息
            XmlUpdate xmlUpdate = new XmlUpdate(ins);
            xmlUpdate.addTextTag(cTextTag);
            xmlUpdate.deleteNode();
            String xml = xmlUpdate.getXmlString();
            System.out.println(xml);

            ins = xmlUpdate.getInputStream();

            SAXBuilder saxBuilder = new SAXBuilder();

            XmlExport tXmlExport = new XmlExport();
            tXmlExport.setDocument(saxBuilder.build(ins));

            return tXmlExport;
        }

        return null;
    }

    /**
     * 添收费记录号
     * @return boolean
     */
    private boolean addPayNoticeNo()
    {
        //得到收费记录号
        String sql = "select GetNoticeNo from LJSPay " +
                     "where OtherNo = '" + mEdorAcceptNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String getNoticeNo = tExeSQL.getOneValue(sql);
        if ((getNoticeNo == null) || (getNoticeNo.equals("")))
        {
            mErrors.addOneError("查不到收费记录号！");
        }
        textTag.add("GetNoticeNo", "收费记录号：" + getNoticeNo);
        return true;
    }

    /**
     * 添加退费记录号
     * @return boolean
     */
    private void addGetNoticeNo()
    {
        //得到退费记录号
        String sql = "select ActuGetNo from LJAGet " +
                     "where OtherNo = '" + mEdorAcceptNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String getNoticeNo = tExeSQL.getOneValue(sql);
        if ((getNoticeNo == null) || (getNoticeNo.equals("")))
        {
            mErrors.addOneError("查不到退费记录号！");
        }
        textTag.add("GetNoticeNo", "退费记录号：" + getNoticeNo);
    }

    /**
     * 得到LPEdorMain中的信息
     * @return LPEdorMainSet
     */
    private LPEdorMainSet getEdorMain()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorAcceptNo);
        return tLPEdorMainDB.query();
    }

    /**
     * 添加每个保单的补退费信息
     */
    private void addContMoneyInfo()
    {
        ListTable listTable = new ListTable();
        listTable.setName("Fee");
        LPEdorMainSet tLPEdorMainSet = getEdorMain();
        for (int i = 1; i <= tLPEdorMainSet.size(); i++)
        {
            String[] contMoney = new String[1];
            String contNo = tLPEdorMainSet.get(i).getContNo();
            double getMoney = tLPEdorMainSet.get(i).getGetMoney();
            if (getMoney > 0)
            {
                contMoney[0] = "保单号：" + contNo + "，应收" + CommonBL.bigDoubleToCommonString(getMoney,"0.00") + "元。";
            }
            else if (getMoney < 0)
            {
                contMoney[0] = "保单号：" + contNo + "，应退" +
                         CommonBL.bigDoubleToCommonString(Math.abs(getMoney),"0.00") + "元。";
            }
            listTable.add(contMoney);
        }
        xmlExport.addListTable(listTable, new String[1]);
        xmlExport.addDisplayControl("displayCash");
    }

    /**
     * 添加交费信息
     * @param money double
     */
    private void addPayMoneyInfo(double money)
    {
        textTag.add("FeeInfo", "收费通知书");
        textTag.add("SumMoney", "本次变更应收费合计:" + CommonBL.bigDoubleToCommonString(money,"0.00") + "元。");
    }

    /**
     * 添加退费信息
     * @param money double
     */
    private void addGetMoneyInfo(double money)
    {
        textTag.add("FeeInfo", "退费通知书");
        textTag.add("SumMoney", "本次变更应退费合计:" + CommonBL.bigDoubleToCommonString(Math.abs(money),"0.00") + "元。");
    }

    private void addAccFeeInfo(double money)
    {
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setOtherNo(mEdorAcceptNo);
        tLCAppAccTraceDB.setOtherType(BQ.NOTICETYPE_P);
        LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
        if (tLCAppAccTraceSet.size() == 0)
        {
            return;
        }
        String accFee = "";
        LCAppAccTraceSchema tLCAppAccTraceSchema = tLCAppAccTraceSet.get(1);
        String custoemrName = CommonBL.getAppntName(tLCAppAccTraceSchema.getCustomerNo());
        if (tLCAppAccTraceSchema.getAccType().equals("0"))
        {
            accFee = "用超收保费余额抵扣" +
            		CommonBL.bigDoubleToCommonString(Math.abs(tLCAppAccTraceSchema.getMoney()),"0.00") +
                    "元，" +
                    "抵扣后客户" + tLCAppAccTraceSchema.getCustomerNo() +
                    "的超收保费余额为" + CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getAccBala(),"0.00") + "元，" +
                    "客户还应缴费" + CommonBL.bigDoubleToCommonString(Math.abs(money),"0.00") + "元。";
        }
        else if (tLCAppAccTraceSchema.getAccType().equals("1"))
        {
            accFee = "自动转到超收保费余额帐户，转帐后客户" + tLCAppAccTraceSchema.getCustomerNo() +
                    "的超收保费余额为" + CommonBL.bigDoubleToCommonString(tLCAppAccTraceSchema.getAccBala(),"0.00") + "元。";
        }
        textTag.add("AccFee", accFee);
    }

    /**
     * 添加交费方式
     * @param money double
     */
    private void addPayMode(double money)
    {
        String payMode = (String) mPayInfo.getValueByName("payMode");
        String payDate = (String) mPayInfo.getValueByName("payDate"); //转帐日期
        String endDate = (String) mPayInfo.getValueByName("endDate"); //截止日期
        String bank = (String) mPayInfo.getValueByName("bank");
        String bankAccno = (String) mPayInfo.getValueByName("bankAccno");
        String outPayMode = ""; //交费方式
        String outNotice = "";  //注意事项
        //现金
        if (payMode.equals("1"))
        {
            outPayMode = "您的交费方式为：现金，请凭本通知书在" +
                    CommonBL.decodeDate(endDate) +
                    "前到我公司交费。";
            outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
        }
        //现金支票
        else if (payMode.equals("2"))
        {
            outPayMode = "您的交费方式为：现金支票，请凭本通知书在" +
                    CommonBL.decodeDate(endDate) +
                    "前到我公司交费。";
            outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
        }
        //转帐支票
        else if (payMode.equals("3"))
        {
            outPayMode = "您的交费方式为：转帐支票，请凭本通知书在" +
                    CommonBL.decodeDate(endDate) +
                    "前到我公司交费。";
            outNotice = "超过以上日期本公司未收到应收保费的，本批注内容无效。";
        }
        //银行转帐
        else if (payMode.equals("4"))
        {
            outPayMode = "您的交费方式为：银行转帐，交费银行为：" +
                    ChangeCodeBL.getCodeName("Bank", bank,"Bankcode") +
                    "，账号为：" + bankAccno +
                    "，转帐总金额为：" + CommonBL.bigDoubleToCommonString(Math.abs(money),"0.00") +
                    "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                    "进行转账，请在转账日前确认该账号是否有足够金额。";
            outNotice = "本公司未收到应收保费的，本批注内容无效。";
        }
        textTag.add("PayMode", outPayMode);
        textTag.add("Notice", outNotice);
    }

    /**
     * 添加退费方式
     * @param money double
     */
    private void addGetMode(double money)
    {
        String payMode = (String) mPayInfo.getValueByName("payMode");
        String payDate = (String) mPayInfo.getValueByName("payDate"); //转帐日期
        //String endDate = (String) mPayInfo.getValueByName("endDate"); //截止日期
        String bank = (String) mPayInfo.getValueByName("bank");
        String bankAccno = (String) mPayInfo.getValueByName("bankAccno");
        String outPayMode = ""; //退费方式
        String outNotice = "";  //注意事项

        //现金
        if (payMode.equals("1"))
        {
            outPayMode = "退费方式为：现金，请凭本通知书到我公司领款。";
        }
        //现金支票
        if (payMode.equals("2"))
        {
            outPayMode = "退费方式为：现金支票，请凭本通知书到我公司领款。";
        }
        //转帐支票
        if (payMode.equals("3"))
        {
            outPayMode = "退费方式为：转帐支票，请凭本通知书到我公司领款。";
        }
        //银行转帐
        else if (payMode.equals("4"))
        {
            outPayMode = "退费方式为：银行转帐，交易银行为：" +
                    ChangeCodeBL.getCodeName("Bank", bank, "BankCode") +
                    "，交易账号为：" + bankAccno +
                    "，转帐总金额为：" + CommonBL.bigDoubleToCommonString(Math.abs(money),"0.00") +
                    "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                    "进行转账，请注意查收。";
        }
        textTag.add("PayMode", outPayMode);
        textTag.add("Notice", outNotice);
    }

    /**
     * 向数据库中更新批单的Blob
     * @param xmlExport XmlExport
     */
    private boolean updateData()
    {
        LPEdorAppPrintSchema printSchema = new LPEdorAppPrintSchema();
        printSchema.setSchema(mPrintSchema);
        printSchema.setEdorInfo(xmlExport.getInputStream());
        printSchema.setModifyDate(PubFun.getCurrentDate());
        printSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(printSchema, "BLOBUPDATE");
        return true;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
