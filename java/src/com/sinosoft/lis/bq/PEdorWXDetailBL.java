package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


//程序名称：PEdorWXDetailBL.java  
//程序功能：
//创建日期：2011-08-04
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容

public class PEdorWXDetailBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局基础数据 */
	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;

	private MMap map = new MMap();

	private String mContNo = null;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String mEdorNo = null;

	private String mEdorType = null;

	private String mGetNoticeNo = null;
	
	private String mFee = null;
	
	private String mFeePayDate = null;
	
	private String mFeeMode= null;

	public PEdorWXDetailBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) {
			return false;
		}
		PubSubmit tSubmit = new PubSubmit();

		if (!tSubmit.submitData(mResult, "")) { // 数据提交
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("PEdorWXDetailBL End PubSubmit");
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
			mEdorNo = mLPEdorItemSchema.getEdorNo();
			mContNo = mLPEdorItemSchema.getContNo();
			mEdorType = mLPEdorItemSchema.getEdorType();
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mEdorNo == null || mEdorNo.equals("") || mEdorNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全号失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mEdorType = mLPEdorItemSchema.getEdorType();
		if (mEdorType == null || mEdorType.equals("") || mEdorType.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保全类型失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mContNo = mLPEdorItemSchema.getContNo();
		if (mContNo == null || mContNo.equals("") || mContNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号失败!";
			mErrors.addOneError(tError);
			return false;
		}

		if (mGlobalInput == null || mLPEdorItemSchema == null) {
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "输入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		TransferData tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mGetNoticeNo = (String)tTransferData.getValueByName("getnoticeno");
		if (mGetNoticeNo == null || mGetNoticeNo.equals("") || mGetNoticeNo.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取应收号失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mFee = (String)tTransferData.getValueByName("Fee");
		if (mFee == null || mFee.equals("") || mFee.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取录入补费失败!";
			mErrors.addOneError(tError);
			return false;
		}
		mFeeMode= (String)tTransferData.getValueByName("feemode");
		if (mFeeMode == null || mFeeMode.equals("") || mFeeMode.equals("null")) {
			// @@错误处理
			System.out.println("PEdorWXAppConfirmBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXAppConfirmBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取配置信息失败!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		
		String tFeePayDatemax=new ExeSQL().getOneValue(" select max(paydate) from lcinsureacctrace where contno='"+mContNo+"' and otherno='"+mGetNoticeNo+"' and othertype='2' and moneytype='BF' ");
		String tFeePayDatemin=new ExeSQL().getOneValue(" select min(paydate) from lcinsureacctrace where contno='"+mContNo+"' and otherno='"+mGetNoticeNo+"' and othertype='2' and moneytype='BF' ");
		if(tFeePayDatemax.equals("")||tFeePayDatemax.equals(null)||(!tFeePayDatemax.equals(tFeePayDatemin))){
			// @@错误处理
			System.out.println("PEdorWXDetailBL+dealData++--");
			CError tError = new CError();
			tError.moduleName = "PEdorWXDetailBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取的续期核销日期为多条!";
			mErrors.addOneError(tError);
			return false;
		}
		mFeePayDate=tFeePayDatemin;
		
		mLPEdorItemSchema.setEdorState("1");
		mLPEdorItemSchema.setModifyDate(mCurrentDate);
		mLPEdorItemSchema.setModifyTime(mCurrentTime);
		map.put(mLPEdorItemSchema, "UPDATE");
		
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchemamGetNoticeNo = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchemamGetNoticeNo.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchemamGetNoticeNo.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchemamGetNoticeNo.setEdorType(mEdorType);
		tLPEdorEspecialDataSchemamGetNoticeNo.setDetailType("GETNOTICENO");
		tLPEdorEspecialDataSchemamGetNoticeNo.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchemamGetNoticeNo.setEdorValue(mGetNoticeNo);
		map.put(tLPEdorEspecialDataSchemamGetNoticeNo, "DELETE&INSERT");
		
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchemafee = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchemafee.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchemafee.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchemafee.setEdorType(mEdorType);
		tLPEdorEspecialDataSchemafee.setDetailType("FEE");
		tLPEdorEspecialDataSchemafee.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchemafee.setEdorValue(mFee);
		map.put(tLPEdorEspecialDataSchemafee, "DELETE&INSERT");
		
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchemamFeePayDate = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchemamFeePayDate.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchemamFeePayDate.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchemamFeePayDate.setEdorType(mEdorType);
		tLPEdorEspecialDataSchemamFeePayDate.setDetailType("FEEPAYDATE");
		tLPEdorEspecialDataSchemamFeePayDate.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchemamFeePayDate.setEdorValue(mFeePayDate);
		map.put(tLPEdorEspecialDataSchemamFeePayDate, "DELETE&INSERT");
		
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchemamFeeMode = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchemamFeeMode.setEdorAcceptNo(mEdorNo);
		tLPEdorEspecialDataSchemamFeeMode.setEdorNo(mEdorNo);
		tLPEdorEspecialDataSchemamFeeMode.setEdorType(mEdorType);
		tLPEdorEspecialDataSchemamFeeMode.setDetailType("FEEMODE");
		tLPEdorEspecialDataSchemamFeeMode.setPolNo(BQ.FILLDATA);
		tLPEdorEspecialDataSchemamFeeMode.setEdorValue(mFeeMode);
		map.put(tLPEdorEspecialDataSchemamFeeMode, "DELETE&INSERT");
		return true;
	}

	/**
	 * 根据前面的输入数据，进行校验处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean checkData() {
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setEdorNo(mEdorNo);
		tLPEdorItemDB.setEdorAcceptNo(mEdorNo);
		tLPEdorItemDB.setEdorType(mEdorType);
		tLPEdorItemDB.setContNo(mContNo);
		tLPEdorItemDB.setInsuredNo(BQ.FILLDATA);
		tLPEdorItemDB.setPolNo(BQ.FILLDATA);
		if (!tLPEdorItemDB.getInfo()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "checkDate";
			tError.errorMessage = "无保全申请数据!";
			System.out.println("------" + tError);
			this.mErrors.addOneError(tError);
			return false;
		}

		// 将查询出来的保全主表数据保存至模块变量中，省去其它的重复查询
		mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
		if (mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorZBDetailBL";
			tError.functionName = "checkData";
			tError.errorMessage = "该保全已经保全理算，不能修改!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
//		String tBalaDate=new ExeSQL().getOneValue(" select max(baladate) from lcinsureacc where contno='"+mContNo+"'  ");
//		if(tBalaDate.equals("")||tBalaDate.equals("null")||(!mLPEdorItemSchema.getEdorValiDate().equals(tBalaDate))){
//			// @@错误处理
//			System.out.println("PEdorWXDetailBL+checkData++--");
//			CError tError = new CError();
//			tError.moduleName = "PEdorWXDetailBL";
//			tError.functionName = "checkData";
//			tError.errorMessage = "保全生效日期不为该单结算日!";
//			mErrors.addOneError(tError);
//			return false;
//		}
		
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareData() {
		mResult.clear();
		mResult.add(map);

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}
}

