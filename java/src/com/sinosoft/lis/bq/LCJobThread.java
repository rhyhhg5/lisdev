package com.sinosoft.lis.bq;

import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 * 
 */
public class LCJobThread implements Runnable {
	private String mP_ID = null;

	private List<Schema> mLCInsuredList = null;

	private LCDFService mDFService = null;
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 批次号 */
    private String mBatchNo = null;
    
    /** 团体合同号信息 */
    private LCGrpContSchema mLCGrpContSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
	
	/** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    
    private TransferData mTransferData ;

	public LCJobThread(String cPid, List cLCInsuredList, LCDFService cDFService, TransferData cTransferData) {
		mP_ID = cPid;
		mLCInsuredList = cLCInsuredList;
		mDFService = cDFService;
		mTransferData = cTransferData;
		mGlobalInput = (GlobalInput) mTransferData.getValueByName("GlobalInput");
		mEdorNo = (String) mTransferData.getValueByName("EdorNo");
		mBatchNo = (String) mTransferData.getValueByName("BatchNo");
		mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
	}

	public void run() {
		try {
			System.out.println("当前执行的线程名臣："+Thread.currentThread().getName());
			
			System.out.println("--" + mP_ID + "--");
			
			working();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			mDFService.DownCount();
		}
	}

	private void working() {

		
		MMap map = new MMap();
        for (int i = 0; i < mLCInsuredList.size(); i++) {
            //添加一个被保人
            addOneInsured(map, mLCInsuredList.get(i), i);
        }

        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map);
        tPubSubmit.submitData(tVData, "");
        //提交数据到数据库
//        if (!submitData(map)) {
//            return false;
//        }
	}
	
	/**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
	private void addOneInsured(MMap map,
            Schema schema, int i) {
		LCInsuredListSchema tLCInsuredListSchema = (LCInsuredListSchema)schema;
		if (tLCInsuredListSchema.getContNo() == null ||
				tLCInsuredListSchema.getContNo().equals("")) {
			return;
		}
		tLCInsuredListSchema.setGrpContNo(mGrpContNo);
		tLCInsuredListSchema.setState("0"); //0为未生效，1为有效
		tLCInsuredListSchema.setBatchNo(this.mBatchNo);
		//在磁盘投保时合同号码存储合同id
		if (StrTool.cTrim(tLCInsuredListSchema.getContNo()).equals("")) {
			tLCInsuredListSchema.setContNo(String.valueOf(i));
		}
		String edorValiDate = tLCInsuredListSchema.getEdorValiDate();
		if ((edorValiDate == null) || (edorValiDate.equals(""))) {
			if (mLPGrpEdorItemSchema != null) {
				tLCInsuredListSchema.setEdorValiDate(
						mLPGrpEdorItemSchema.getEdorValiDate());
			}
		}
		tLCInsuredListSchema.setEdorNo(mEdorNo);
		tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
		tLCInsuredListSchema.setMakeDate(mCurrentDate);
		tLCInsuredListSchema.setMakeTime(mCurrentTime);
		tLCInsuredListSchema.setModifyDate(mCurrentDate);
		tLCInsuredListSchema.setModifyTime(mCurrentTime);
		String prem = tLCInsuredListSchema.getPrem();
		if ((prem != null) && (!prem.equals("")) &&
				(Double.parseDouble(prem) > 0)) {
			tLCInsuredListSchema.setPublicAcc(Double.parseDouble(prem));
		}
		//设置计算方向  
		if (tLCInsuredListSchema.getPublicAcc() > 0) {
			tLCInsuredListSchema.setCalRule("4");
		}
		map.put(tLCInsuredListSchema, "DELETE&INSERT");
	}
}
