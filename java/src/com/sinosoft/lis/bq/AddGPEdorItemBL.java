package com.sinosoft.lis.bq;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class AddGPEdorItemBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 往前面传输数据的容器 */
  private VData mResult = new VData();
  private MMap map = new MMap();
  TransferData tTransferData = new TransferData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /** 业务处理相关变量 */
  private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
  private LPGrpEdorItemSet mLPGrpEdorItemSet = new LPGrpEdorItemSet();
  private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
  private GlobalInput mGlobalInput = new GlobalInput();
  private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

  // @Constructor
  public AddGPEdorItemBL() {}

  /**
   * 数据提交的公共方法
   * @param: cInputData 传入的数据
   *		  cOperate 数据操作字符串
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false) {
      return false;
    }

   

    // 根据业务逻辑对数据进行处理
    // 根据业务逻辑对数据进行处理
    
      if (this.dealData() == false) {
        return false;
      }
    
    
    // 装配处理好的数据，准备给后台进行保存
    this.prepareOutputData();
    System.out.println("---prepareOutputData---");
    //　数据提交、保存
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start GrpEdorItemBL Submit...");

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);

      CError tError = new CError();
      tError.moduleName = "GrpEdorItemBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";

      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---commitData---");

    return true;
  }
  
  /**
   * 获取提交数据的公共方法
   * @param: cInputData 传入的数据
   *		 cOperate 数据操作字符串
   * @return:MMap
   */
  public MMap getsubmitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false) {
      return null;
    }

    
    // 根据业务逻辑对数据进行处理
    // 根据业务逻辑对数据进行处理
    if (mOperate.equals("INSERT||GRPEDORITEM")) {
      if (this.dealData() == false) {
        return null;
      }
    }
    else if (mOperate.equals("DELETE||GRPEDORITEM")) {
      if (this.deleteData() == false) {
        return null;
      }
    }
    else if (mOperate.equals("UPDATE||GRPEDORITEM")) {
      if (!updateData()) {
        return null;
      }
    }
    return map;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {

    //全局变量
    mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0));
    //团体批改主表
    mLPGrpEdorMainSchema.setSchema( (LPGrpEdorMainSchema) mInputData.
                                   getObjectByObjectName("LPGrpEdorMainSchema",
        0));

    //批改项目表
    mLPGrpEdorItemSet.set( (LPGrpEdorItemSet) mInputData.
                          getObjectByObjectName("LPGrpEdorItemSet", 0));
//        tTransferData = (TransferData) mInputData.
//                        getObjectByObjectName("TransferData", 0);

    if (mLPGrpEdorMainSchema == null || mLPGrpEdorItemSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpEdorItemBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在接受数据时没有得到团体批改主表或者批改项目表!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLPGrpEdorItemSchema = mLPGrpEdorItemSet.get(1);
    return true;
  }




 



  

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean dealData() {
    LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
    tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
    tLPGrpEdorMainDB.setGrpContNo(mLPGrpEdorMainSchema.getGrpContNo());
    LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
    if (tLPGrpEdorMainSet.size() == 0) {
      mLPGrpEdorMainSchema.setEdorAppNo(mLPGrpEdorMainSchema.
                                        getEdorAcceptNo());
      mLPGrpEdorMainSchema.setEdorNo(mLPGrpEdorMainSchema.getEdorAcceptNo());
      //mLPGrpEdorMainSchema.setEdorValiDate(PubFun.getCurrentDate());
      //mLPGrpEdorMainSchema.setEdorAppDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setEdorState("1");
      mLPGrpEdorMainSchema.setUWState("0");
      mLPGrpEdorMainSchema.setManageCom(mLPGrpEdorMainSchema.getManageCom());
      mLPGrpEdorMainSchema.setOperator(mGlobalInput.Operator);
      mLPGrpEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
      mLPGrpEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
      mLPGrpEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLPGrpEdorMainSchema, "DELETE&INSERT");
    }

    //产生集体投保单号码
   
      for (int i = 1; i <= mLPGrpEdorItemSet.size(); i++) {
        String tEdorNo = mLPGrpEdorItemSet.get(i).getEdorNo();
        if (tEdorNo == null || tEdorNo.equals("")) {
          mLPGrpEdorItemSet.get(i).setEdorNo(mLPGrpEdorMainSchema.getEdorAppNo());
        }

        mLPGrpEdorItemSet.get(i).setEdorState("3"); ////未录入
        //mLPGrpEdorItemSet.get(i).setEdorAppDate(PubFun.getCurrentDate());
        //mLPGrpEdorItemSet.get(i).setEdorValiDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
        mLPGrpEdorItemSet.get(i).setOperator(mGlobalInput.Operator);
        mLPGrpEdorItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
      }
      map.put(mLPGrpEdorItemSet, "INSERT");
   

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean updateData() {
    //产生集体投保单号码
    if (mOperate.equals("UPDATE||GRPEDORITEM")) {
      String tEdorAcceptNo = mLPGrpEdorItemSet.get(1).getEdorAcceptNo();
      String tEdorNo = mLPGrpEdorItemSet.get(1).getEdorNo();
      String tEdorType = mLPGrpEdorItemSet.get(1).getEdorType();
      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      tLPEdorItemDB.setEdorAcceptNo(tEdorAcceptNo);
      tLPEdorItemDB.setEdorNo(tEdorNo);
      tLPEdorItemDB.setEdorType(tEdorType);
      tLPEdorItemDB.setEdorState("3");
      LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
      if (tLPEdorItemSet == null || tLPEdorItemSet.size() <= 0) {
        map.put(
            "update LPGrpEdorItem set edorstate='1' where edoracceptno='" +
            tEdorAcceptNo + "' and edorno='" + tEdorNo +
            "' and edortype='" + tEdorType + "'", "UPDATE");
      }
      else {
        CError tError = new CError();
        tError.moduleName = "GrpEdorItemBL";
        tError.functionName = "checkData";
        tError.errorMessage = "该保全项目下存在装态为3（未录入）的个单!";
        this.mErrors.addOneError(tError);
        return false;
      }
    }

    return true;
  }

  /**
   * 删除传入的险种
   * @param: 无
   * @return: void
   */
  private boolean deleteData() {

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: void
   */
  private void prepareOutputData() {
    mResult.clear();
    mResult.add(mLPGrpEdorItemSet);
    mInputData.clear();
    mInputData.add(map);
  }

  /**
   * 得到处理后的结果集
   * @return 结果集
   */

  public VData getResult() {
    return mResult;
  }

  private boolean checkDate(String grpContNo)
  {
      String sql = "select Cinvalidate From lcgrpcont where grpcontno ='"+grpContNo+"'";
      ExeSQL tExeSQL = new ExeSQL();
      String Cinvalidate = tExeSQL.getOneValue(sql);
      String tEdorappDate = mLPGrpEdorMainSchema.getEdorAppDate();
      PubFun tPubFun = new PubFun();
      int i = tPubFun.calInterval(Cinvalidate,tEdorappDate,"D");
      System.out.println("保全申请时间和保单满期日期间隔:" + i);
      if(i>30)
      {
          mErrors.addOneError("保单已经失效超过满期日30天,不能再添加保全项目!");
          return false;
      }
      return true;
  }
  // 获得保单状态的方法.
  private String checkContState(String grpContNo)
  {
      String sql ="select stateflag From lcgrpcont where grpcontno ='"+ grpContNo +"'";
      ExeSQL tExeSQL = new ExeSQL();
      String stateflag = tExeSQL.getOneValue(sql);

      return stateflag ;
  }
  
//校验在其它保单已存在保全项目状态下该保全项目能否添加 071206 zhanggm
  private boolean checkAddItem()
  {
	  LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
	  tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
	  tLPGrpEdorMainDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
	 // LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
	 // tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
	  if(!tLPGrpEdorMainDB.getInfo())
	  {
		  return true;
	  }
	  else
	  {
		  if(!mLPGrpEdorItemSchema.getGrpContNo().equals(tLPGrpEdorMainDB.getGrpContNo()))
		  {
		      CError tError = new CError();
		      tError.moduleName = "GrpEdorItemBL";
		      tError.functionName = "checkAddItem";
		      tError.errorMessage = "一个工单只能对一个保单进行保全操作！";
		      this.mErrors.addOneError(tError);
		      return false;
		  }
	  }
	  return true;
  }
}
