package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 协议退保项目明细</p>
 * <p>Copyright: Copyright (c) 2005.3.31</p>
 * <p>Company: Sinosoft</p>
 * @author LHS
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class GrpEdorTGDetailBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/** 全局基础数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
	private EdorItemSpecialData mEdorItemSpecialData = null;
	private LPGrpContSchema tLPGrpContSchema = null;
	private LPContSet mLPContSet = null;
	private LPEdorMainSet mLPEdorMainSet = null;
	private LPEdorItemSet mLPEdorItemSet = null;
	private String mCrrDate = PubFun.getCurrentDate();
	private String mCrrTime = PubFun.getCurrentTime();

	/** 传出数据的容器 */
	private MMap mMap = new MMap();

	public GrpEdorTGDetailBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		VData data = new VData();
		data.add(mMap);
		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(data, "")) // 数据提交
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			this.mErrors.addOneError("数据提交失败!");
			return false;
		}
		System.out.println("GrpEdorTGDetailBL End PubSubmit");
		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		if(!(mLPGrpEdorItemSchema.getEdorState().equals("3")
		           || mLPGrpEdorItemSchema.getEdorState().equals("1")))
		        {
		            mErrors.addOneError("保全项目，不能录入。");
		            return false;
		        }
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData(VData data) {
		LPGrpEdorItemSchema grpEdorItem = null;
		try {
			mGlobalInput = (GlobalInput) data.getObjectByObjectName(
					"GlobalInput", 0);
			grpEdorItem = (LPGrpEdorItemSchema) data.getObjectByObjectName(
					"LPGrpEdorItemSchema", 0);
			mEdorItemSpecialData = (EdorItemSpecialData) data
					.getObjectByObjectName("EdorItemSpecialData", 0);
		} catch (Exception e) {
			e.printStackTrace();
			this.mErrors.addOneError("接收数据失败!!");
			return false;
		}

		if (mGlobalInput == null || grpEdorItem == null
				|| mEdorItemSpecialData == null) {
			this.mErrors.addOneError("传入的数据不完整。");
			return false;
		}

		System.out.println(mGlobalInput.Operator + " "
				+ PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + " "
				+ "团体万能协议退保明细录入");

		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		tLPGrpEdorItemDB.setEdorNo(grpEdorItem.getEdorNo());
		tLPGrpEdorItemDB.setEdorType(grpEdorItem.getEdorType());
		tLPGrpEdorItemDB.setGrpContNo(grpEdorItem.getGrpContNo());
		LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() < 1) {
			this.mErrors.addOneError("输入数据有误,没有查询到保全项目信息。");
			return false;
		}

		mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
		mLPGrpEdorItemSchema.setReasonCode(grpEdorItem.getReasonCode());
		mLPGrpEdorItemSchema.setEdorState("1");

		LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
		tLPGrpEdorMainDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPGrpEdorMainDB
				.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
		if (!tLPGrpEdorMainDB.getInfo()) {
			this.mErrors.addOneError("输入数据有误,LPGrpEdorMain中没有相关数据!");
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!dealLPGrpCont()) {
			return false;
		}
		if (!dealLPPol()) {
			return false;
		}
		if (!dealLPInsured()) {
			return false;
		}
		if(!createLPEdorInfo()){
			return false;
		}

		mMap.put(mLPGrpEdorItemSchema, "UPDATE");

		String sql = "delete from LPEdorEspecialData " + "where EdorNo = '"
				+ mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and EdorType = '" + mLPGrpEdorItemSchema.getEdorType()
				+ "' ";
		mMap.put(sql, SysConst.DELETE);

		mMap.put(mEdorItemSpecialData.getSpecialDataSet(), "INSERT");

		return true;
	}

	/**
	 *dealLPInsured
	 * 
	 * @return boolean
	 */
	private boolean dealLPInsured() {
		mMap.put("  delete from LPInsured " + "where edorNo = '"
				+ mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '" + mLPGrpEdorItemSchema.getEdorType()
				+ "' " + "  and grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
		mMap
				.put(
						"  delete from LPInsured p "
								+ "where edorNo = '"
								+ mLPGrpEdorItemSchema.getEdorNo()
								+ "' "
								+ "  and edorType = '"
								+ mLPGrpEdorItemSchema.getEdorType()
								+ "' "
								+ "  and grpContNo = '"
								+ mLPGrpEdorItemSchema.getGrpContNo()
								+ "' AND p.InsuredNO IN (SELECT a.InsuredNO FROM lcInsured a ,lcpol b where a.ContNo =b.ContNo "
								+ " and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
								+ mLPGrpEdorItemSchema.getGrpContNo() + "')",
						"DELETE");

		mMap.put("insert into LPInsured (select '"
				+ this.mLPGrpEdorItemSchema.getEdorNo() + "','"
				+ this.mLPGrpEdorItemSchema.getEdorType()
				+ "',a.* from lcInsured a ,lcpol b where a.ContNo =b.ContNo "
				+ " and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo() + "')", "INSERT");
		updateDefaultFields("LPInsured");
		return true;
	}

	/**
	 * dealLPPol
	 * 
	 * @return boolean
	 */
	private boolean dealLPPol() {
		mMap.put("  delete from LPPol " + "where edorNo = '"
				+ mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType()
				+ "' " + "   and grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");

		mMap.put("insert into LPPol "
                + "(select '" + mLPGrpEdorItemSchema.getEdorNo()
                + "', '" + mLPGrpEdorItemSchema.getEdorType() + "', LCPol.* "
                + "from LCPol "
                + "where grpContNo = '"
                +mLPGrpEdorItemSchema.getGrpContNo()
                 + "') ", "INSERT");

		updateDefaultFields("LPPol");

		return true;
	}

	private void updateDefaultFields(String tableName) {
		mMap.put("  update " + tableName + " set operator = '"
				+ mGlobalInput.Operator + "', " + "    makeDate = '"
				+ PubFun.getCurrentDate() + "', " + "    makeTime = '"
				+ PubFun.getCurrentTime() + "', " + "    modifyDate = '"
				+ PubFun.getCurrentDate() + "', " + "    modifyTime = '"
				+ PubFun.getCurrentTime() + "' " + "where edorNo = '"
				+ mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType()
				+ "' " + "   and grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo() + "' ", SysConst.UPDATE);
	}

	/**
	 * getLPGrpCont 存储LPGrpCont
	 * 
	 * @return boolean
	 */
	private boolean dealLPGrpCont() {
		String sqDelete = "delete from LPGrpCont " + "where EdorNo = '"
				+ mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and Edortype = '" + mLPGrpEdorItemSchema.getEdorType()
				+ "' " + "   and GrpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo() + "' ";

		mMap.put(sqDelete, SysConst.DELETE);

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
		tLCGrpContDB.getInfo();

		tLPGrpContSchema = new LPGrpContSchema();
		new Reflections().transFields(tLPGrpContSchema, tLCGrpContDB
				.getSchema());

		tLPGrpContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPGrpContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPGrpContSchema.setOperator(mGlobalInput.Operator);
		PubFun.fillDefaultField(tLPGrpContSchema);
		mMap.put(tLPGrpContSchema, SysConst.DELETE_AND_INSERT);

		return true;
	}

	private boolean createLPEdorInfo() {
		// 生成被保人保全数据
		if (!deleteOldLPEdorInfo()) {
			return false;
		}

		String contNos = getOneFieldValues("LCCont", "contNo", "");
		if (contNos.equals("")) {
			return false;
		}
		StringBuffer sql = new StringBuffer();
		sql.append(" select a.* ").append("from LCCont a ").append(
				"where a.contNo in (").append(contNos).append(")");
		System.out.println(sql);

		mLPEdorItemSet = new LPEdorItemSet();
		mLPEdorMainSet = new LPEdorMainSet();
		LCContDB tLCContDB = new LCContDB();
		mLPContSet = new LPContSet();
		LCContSet set = null;
		int start = 1;
		int count = 100;

		do {
			set = tLCContDB.executeQuery(sql.toString(), start, count);
			for (int i = 1; i <= set.size(); i++) {
				LCContSchema tLCContSchema = set.get(i);
				if (!createOneLPEdorInfo(tLCContSchema)) {
					return false;
				}
			}
			start += count;
		} while (set.size() > 0);
		mMap.put(mLPEdorItemSet, "INSERT");
		mMap.put(mLPEdorMainSet, "INSERT");
		mMap.put(mLPContSet, "INSERT");
		return true;
	}

	private boolean createOneLPEdorInfo(LCContSchema tLCContSchema) {
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
		tLPEdorItemSchema.setContNo(tLCContSchema.getContNo());
		tLPEdorItemSchema.setInsuredNo(tLCContSchema.getInsuredNo());
		tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
		tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
		tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		tLPEdorItemSchema.setUWFlag("0");
		tLPEdorItemSchema.setMakeDate(mCrrDate);
		tLPEdorItemSchema.setMakeTime(mCrrTime);
		tLPEdorItemSchema.setModifyDate(mCrrDate);
		tLPEdorItemSchema.setModifyTime(mCrrTime);
		tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		mLPEdorItemSet.add(tLPEdorItemSchema);

		LPContSchema tLPContSchema = new LPContSchema();
		ref.transFields(tLPContSchema, tLCContSchema);
		tLPContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPContSchema.setMakeDate(mCrrDate);
		tLPContSchema.setMakeTime(mCrrTime);
		tLPContSchema.setModifyDate(mCrrDate);
		tLPContSchema.setModifyTime(mCrrTime);
		mLPContSet.add(tLPContSchema);

		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		ref.transFields(tLPEdorMainSchema, mLPGrpEdorItemSchema);
		tLPEdorMainSchema.setContNo(tLCContSchema.getContNo());
		tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
		tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
		tLPEdorMainSchema.setUWState("0");
		tLPEdorMainSchema.setEdorState("1");
		tLPEdorMainSchema.setMakeDate(mCrrDate);
		tLPEdorMainSchema.setMakeTime(mCrrTime);
		tLPEdorMainSchema.setModifyDate(mCrrDate);
		tLPEdorMainSchema.setModifyTime(mCrrTime);
		mLPEdorMainSet.add(tLPEdorMainSchema);

		return true;
	}

	private boolean deleteOldLPEdorInfo() {
		mMap.put("  delete from LPCont " + "where edorNo = '"
				+ this.mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '"
				+ this.mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "  and grpContNo = '"
				+ this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
		mMap.put("  delete from LPEdorMain " + "where edorNo = '"
				+ this.mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and contNo in (" + "      select distinct contNo "
				+ "      from LCCont " + "      where grpContNo = '"
				+ this.mLPGrpEdorItemSchema.getGrpContNo() + "') ", "DELETE");
		mMap.put("  delete from LPEdorItem " + "where edorNo = '"
				+ this.mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '"
				+ this.mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "  and grpContNo = '"
				+ this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");

		return true;
	}

	private String getOneFieldValues(String table, String field,
			String condition) {
		StringBuffer sql = new StringBuffer();

		sql.append("select ").append(table).append(".").append(field).append(
				" from LCCont, LCInsured , LCPol ").append(
				"where LCCont.contNo = LCInsured.contNo ").append(
				"   and LCInsured.contNo = LCPol.contNo ").append(
				"   and LCInsured.insuredNo = LCPol.insuredNo ").append(
				"  and LCCont.grpContNo = '").append(
				this.mLPGrpEdorItemSchema.getGrpContNo()).append("' ").append(
				condition).append(" union ");
		SSRS tSSRS = null;
		try {
			String fieldValueSql = sql.substring(0,
					sql.lastIndexOf("union") - 1);
			System.out.println(fieldValueSql);

			ExeSQL e = new ExeSQL();
			tSSRS = e.execSQL(fieldValueSql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (tSSRS == null || tSSRS.getMaxRow() == 0) {
			mErrors.addOneError("查询出错。");
			return "";
		}

		StringBuffer fieldValues = new StringBuffer();
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			fieldValues.append("'").append(tSSRS.GetText(i, 1)).append("',");
		}
		if (fieldValues.length() == 0) {
			return "";
		}
		return fieldValues.toString()
				.substring(0, fieldValues.lastIndexOf(","));
	}

	/**
	 * 调试方法
	 */
	public static void main(String[] args) {
		GlobalInput gi = new GlobalInput();
		gi.Operator = "test";
		gi.ComCode = "86";

		String edorNo = "20070313000001";
		String edorType = "XT";
		String grpContNo = "0000222801";

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorNo(edorNo);
		tLPGrpEdorItemSchema.setEdorType(edorType);
		tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
		tLPGrpEdorItemSchema.setReasonCode("010");

		EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
				edorNo, edorType);
		tEdorItemSpecialData.setGrpPolNo("A,1602");
		tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-10");
		tEdorItemSpecialData.add(BQ.XTFEERATEG, "0.1729");

		tEdorItemSpecialData.setGrpPolNo("A,5601");
		tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-10");
		tEdorItemSpecialData.add(BQ.XTFEERATEG, "0.1729");

		VData tVData = new VData();
		tVData.addElement(gi);
		tVData.addElement(tLPGrpEdorItemSchema);
		tVData.add(tEdorItemSpecialData);

		GrpEdorTGDetailBL bl = new GrpEdorTGDetailBL();
		if (!bl.submitData(tVData, "")) {
			System.out.println(bl.mErrors.getErrContent());
		}
	}
}
