package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务
 * 团单退保项目明细
 * 得到GrpEdorCTDetailUI.java传入的数据，进行退保明细录入业务逻辑的处理。
 * </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorJYDetailBL
{
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局基础数据 */
	private GlobalInput mGI = null;
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
	private LPContPlanRiskSet mLPContPlanRiskSet = null;
	private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;//存储工本费
	private LPContSet mLPContSet = null;
	private LPEdorMainSet mLPEdorMainSet = null;
	private LPEdorItemSet mLPEdorItemSet = null;
	private String mCrrDate = PubFun.getCurrentDate();
	private String mCrrTime = PubFun.getCurrentTime();

	private MMap mMap = new MMap();//待提交的数据集



	public GrpEdorJYDetailBL()
	{
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * @param cInputData 传入的数据,VData对象
	 * @param cOperate 数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		if (!getInputData(cInputData))
		{
			return false;
		}
		if(!checkData())
		{
			return false;
		}
		if (!dealData())
		{
			return false;
		}

		VData data = new VData();
		data.add(mMap);

		PubSubmit tSubmit = new PubSubmit();
		if (!tSubmit.submitData(data, "")) //数据提交
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			return false;
		}
		System.out.println("GrpEdorCTDetailBL End PubSubmit");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		mGI = (GlobalInput) cInputData
		.getObjectByObjectName("GlobalInput", 0);
		
		mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
		.getObjectByObjectName(
				"LPGrpEdorItemSchema", 0);
		
		mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) cInputData
		.getObjectByObjectName(
				"LPEdorEspecialDataSchema", 0);
		
		if (mGI == null || mLPGrpEdorItemSchema == null)
		{
			this.mErrors.addOneError("接收数据失败");
			return false;
		}

		return true;
	}

	/**
	 * 校验当前状态是否可进行明细录入，并且为lpgrpedoritem重新封装内容。
	 * @return boolean
	 */
	private boolean checkData()
	{
		String reasonCode = mLPGrpEdorItemSchema.getReasonCode();  //退保原因

		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
		LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();

		if (tLPGrpEdorItemSet.size() < 1)
		{
			mErrors.addOneError("输入数据有误,LPGrpEdorItem中没有相关数据");
			return false;
		}
		else
		{
			mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
		}
		mLPGrpEdorItemSchema.setReasonCode(reasonCode);

		if(!(mLPGrpEdorItemSchema.getEdorState().equals("3")
				|| mLPGrpEdorItemSchema.getEdorState().equals("1")))
		{
			mErrors.addOneError("保全项目，不能录入。");
			return false;
		}
		return true;
	}
	/**
	 * 进行业务逻辑处理，存储页面传入的信息，同时将需要退保的保障计划下险种备份到LPPol表。
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData()
	{
		if(!dealLPPol())
		{
			return false;
		}
		if(!dealLPInsured())     //添加被保人信息
		{
			return false;
		}
		if(!dealLPEdorItem())
		{
			return false;
		}

		changeItemState(); //设置保全项目状态为录入完毕

		return true;
	}
	
	private boolean dealLPEdorItem()
	{
		if(!createLPEdorInfo())
		{
			mErrors.addOneError("处理个单保全失败");
			return false;
		}
		return true;
	}
	private boolean createLPEdorInfo()
	{
		//生成被保人保全数据
		if(!deleteOldLPEdorInfo())
		{
			return false;
		}

		String contNos = getOneFieldValues("LCCont", "contNo", "");
		if(contNos.equals(""))
		{
			return false;
		}
		StringBuffer sql = new StringBuffer();
		sql.append(" select a.* ")
		.append("from LCCont a ")
		.append("where a.contNo in (").append(contNos).append(")");
		System.out.println(sql);

		mLPEdorItemSet = new LPEdorItemSet();
		mLPEdorMainSet = new LPEdorMainSet();
		LCContDB tLCContDB = new LCContDB();
		mLPContSet = new LPContSet();
		LCContSet set = null;
		int start= 1;
		int count = 100;

		do
		{
			set = tLCContDB.executeQuery(sql.toString(), start, count);
			for (int i = 1; i <= set.size(); i++)
			{
				LCContSchema tLCContSchema = set.get(i);
				if(!createOneLPEdorInfo(tLCContSchema))
				{
					return false;
				}
			}
			start += count;
		}
		while(set.size() > 0);
		mMap.put(mLPEdorItemSet,"INSERT");
		mMap.put(mLPEdorMainSet,"INSERT");
		mMap.put(mLPContSet,"INSERT");
		return true;
	}
	private String getOneFieldValues(String table,String field,String condition)
	{
		StringBuffer sql = new StringBuffer();

		sql.append("select ").append(table).append(".").append(field)
		.append(" from LCCont, LCInsured , LCPol ")
		.append("where LCCont.contNo = LCInsured.contNo ")
		.append("   and LCInsured.contNo = LCPol.contNo ")
		.append("   and LCInsured.insuredNo = LCPol.insuredNo ")
		.append("  and LCCont.grpContNo = '")
		.append(this.mLPGrpEdorItemSchema.getGrpContNo())
		.append("' ")
		.append(condition)
		.append(" union ");	

		SSRS tSSRS = null;
		try
		{
			String fieldValueSql = sql.substring(0,
					sql.lastIndexOf("union") - 1);
			System.out.println(fieldValueSql);

			ExeSQL e = new ExeSQL();
			tSSRS = e.execSQL(fieldValueSql);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		if (tSSRS == null || tSSRS.getMaxRow() == 0)
		{
			mErrors.addOneError("查询出错。");
			return "";
		}

		StringBuffer fieldValues = new StringBuffer();
		for (int i = 1; i <= tSSRS.getMaxRow(); i++)
		{
			fieldValues.append("'").append(tSSRS.GetText(i, 1)).append("',");
		}
		if (fieldValues.length() == 0)
		{
			return "";
		}
		return fieldValues.toString().substring(0, fieldValues.lastIndexOf(","));
	}

	private boolean createOneLPEdorInfo(LCContSchema tLCContSchema)
	{
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		Reflections ref = new Reflections();
		ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
		tLPEdorItemSchema.setContNo(tLCContSchema.getContNo());
		tLPEdorItemSchema.setInsuredNo(tLCContSchema.getInsuredNo());
		tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
		tLPEdorItemSchema.setManageCom(mGI.ManageCom);
		tLPEdorItemSchema.setOperator(mGI.Operator);
		tLPEdorItemSchema.setUWFlag("0");
		tLPEdorItemSchema.setMakeDate(mCrrDate);
		tLPEdorItemSchema.setMakeTime(mCrrTime);
		tLPEdorItemSchema.setModifyDate(mCrrDate);
		tLPEdorItemSchema.setModifyTime(mCrrTime);
		tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		mLPEdorItemSet.add(tLPEdorItemSchema);

		LPContSchema tLPContSchema = new LPContSchema();
		ref.transFields(tLPContSchema, tLCContSchema);
		tLPContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
		tLPContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
		tLPContSchema.setMakeDate(mCrrDate);
		tLPContSchema.setMakeTime(mCrrTime);
		tLPContSchema.setModifyDate(mCrrDate);
		tLPContSchema.setModifyTime(mCrrTime);
		mLPContSet.add(tLPContSchema);

		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		ref.transFields(tLPEdorMainSchema, mLPGrpEdorItemSchema);
		tLPEdorMainSchema.setContNo(tLCContSchema.getContNo());
		tLPEdorMainSchema.setManageCom(mGI.ManageCom);
		tLPEdorMainSchema.setOperator(mGI.Operator);
		tLPEdorMainSchema.setUWState("0");
		tLPEdorMainSchema.setEdorState("1");
		tLPEdorMainSchema.setMakeDate(mCrrDate);
		tLPEdorMainSchema.setMakeTime(mCrrTime);
		tLPEdorMainSchema.setModifyDate(mCrrDate);
		tLPEdorMainSchema.setModifyTime(mCrrTime);
		mLPEdorMainSet.add(tLPEdorMainSchema);

		return true;
	}

	private boolean deleteOldLPEdorInfo()
	{
		mMap.put("  delete from LPCont "
				+ "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '" + this.mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "  and grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
		mMap.put("  delete from LPEdorMain "
				+ "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo()  + "' "
				+ "  and contNo in ("
				+ "      select distinct contNo "
				+ "      from LCCont "
				+ "      where grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "') ", "DELETE");
		mMap.put("  delete from LPEdorItem "
				+ "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '" + this.mLPGrpEdorItemSchema.getEdorType()+ "' "
				+ "  and grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");

		return true;
	}


	/**
	 *
	 * @return boolean
	 */
	private boolean dealLPInsured()
	{
		mMap.put("  delete from LPInsured "
				+ "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '" +mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
		mMap.put("  delete from LPInsured p "
				+ "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "  and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() +
				"' AND p.InsuredNO IN (SELECT a.InsuredNO FROM lcInsured a ,lcpol b where a.ContNo =b.ContNo "
				+" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo()
				+"')", "DELETE");

		mMap.put("insert into LPInsured (select '"
				+this.mLPGrpEdorItemSchema.getEdorNo()
				+"','"
				+this.mLPGrpEdorItemSchema.getEdorType()
				+"',a.* from lcInsured a ,lcpol b where a.ContNo =b.ContNo "
				+" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
				+ mLPGrpEdorItemSchema.getGrpContNo()
				+ "')","INSERT");
		updateDefaultFields("LPInsured");
		return true;
	}



	private void updateDefaultFields(String tableName)
	{
		mMap.put("  update " + tableName
				+ " set operator = '" + mGI.Operator + "', "
				+ "    makeDate = '" + mCrrDate + "', "
				+ "    makeTime = '" + mCrrTime + "', "
				+ "    modifyDate = '" + mCrrDate + "', "
				+ "    modifyTime = '" + mCrrTime + "' "
				+ "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "   and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "INSERT");
	}

	/**
	 * 生成保全险种信息
	 * @return boolean
	 */
	private boolean dealLPPol()
	{
		mMap.put("  delete from LPPol "
				+ "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
				+ "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
				+ "   and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
		//通过保单号
		mMap.put("insert into LPPol "
				+ "(select '" + mLPGrpEdorItemSchema.getEdorNo()
				+ "', '" + mLPGrpEdorItemSchema.getEdorType() + "', LCPol.* "
				+ "from LCPol "
				+ "where grpContNo = '"
				+mLPGrpEdorItemSchema.getGrpContNo()
				+ "') ", "INSERT");
		updateDefaultFields("LPPol");
		return true;
	}

	/**
	 * 存储工本费信息
	 */
	private void dealGBInfo()
	{
		mMap.put("delete from LPEdorEspecialData "
				+ "where edorAcceptNo = '"
				+ mLPGrpEdorItemSchema.getEdorAcceptNo()
				+ "'  and edorType ='" + mLPGrpEdorItemSchema.getEdorType()
				+ "'  and detailType = '" + BQ.DETAILTYPE_GB + "' ",
		"DELETE");

		if(mLPEdorEspecialDataSchema == null)
		{
			return;
		}
		EdorItemSpecialData tEdorItemSpecialData
		= new EdorItemSpecialData(mLPGrpEdorItemSchema);
		tEdorItemSpecialData.add(BQ.DETAILTYPE_GB,
				mLPEdorEspecialDataSchema.getEdorValue());
		mMap.put(tEdorItemSpecialData.getSpecialDataSet(), "INSERT");
	}

	/**
	 * 项目状态为录入完毕
	 */
	private void changeItemState()
	{
		mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
		mLPGrpEdorItemSchema.setOperator(mGI.Operator);
		mLPGrpEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		mLPGrpEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
		mMap.put(mLPGrpEdorItemSchema, "UPDATE");
	}

}

