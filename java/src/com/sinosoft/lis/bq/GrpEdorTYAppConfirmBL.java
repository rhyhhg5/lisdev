package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体特需医疗追加保费</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class GrpEdorTYAppConfirmBL implements EdorAppConfirm {
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 保全类型 */
    private String mEdorType = null;

    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全生效日期 */
    private String mEdorValiDate = null;

    /** 保全交费总额 */
    private double mGetMoney = 0.00;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mSpecialData = null;

    /** 保全项目特殊数据 */
    private EdorItemSpecialData mVtsData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();


    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        getInputData(cInputData);

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 返回理算结果
     * @return VData
     */
    public VData getResult() {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param cInputData VData
     */
    private void getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LPGrpEdorItemSchema edorItem = (LPGrpEdorItemSchema) cInputData.
                                       getObjectByObjectName(
                                               "LPGrpEdorItemSchema", 0);
        mEdorNo = edorItem.getEdorNo();
        mEdorType = edorItem.getEdorType();
        mGrpContNo = edorItem.getGrpContNo();
        mEdorValiDate = edorItem.getEdorValiDate();
        mSpecialData = new EdorItemSpecialData(edorItem);
        mSpecialData.query();
        mVtsData = new EdorItemSpecialData(edorItem);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
        if (!setGrpAcc()) {
            return false;
        }
        /**
         * 370301险种没有个人账户，不走此方法。2018-2-11
         */
        String sqlRiskcode = "select riskcode from lcgrppol  where grpcontno = '"+mGrpContNo+"'";
        String riskCode = new ExeSQL().getOneValue(sqlRiskcode);
        if(riskCode!=null && !"".equals(riskCode) && !"370301".equals(riskCode)){
        	if (!setInsuredAcc()) {
        		return false;
        	}
        }
        setEdorInfo();
        setGrpEdorItem();
        return true;
    }

    /**
     * 设置团体账户追加保费金额
     */
    private boolean setGrpAcc() {
        String[] grpPolNos = mSpecialData.getGrpPolNos();
        for (int i = 0; i < grpPolNos.length; i++) {
            String grpPolNo = grpPolNos[i];
            //考虑团体万能和普通险种混合，只处理团体万能险种。added by gzh 20110131
            String sqlEspecialRiskcode =
                    "select b.risktype4,a.riskcode,b.RiskProp from lcgrppol a,lmriskapp b where a.grppolno = '"
                    + grpPolNo + "' and a.riskcode = b.riskcode  ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
            if (tSSRS.getMaxRow() != 0 && (tSSRS.GetText(1, 1).equals("4") || tSSRS.GetText(1,3).equals("G"))) {
            	String sql = "select a.payplancode,a.accpayclass,c.dutycode,d.feecode from lmdutypay a "+
            				 "left join lmdutypayrela b on a.payplancode = b.payplancode "+
            				 "left join  lmriskduty c on b.dutycode = c.dutycode "+
            				 "left join lcgrpfee d on a.payplancode = d.payplancode "+
            				 "where d.riskcode='"+tSSRS.GetText(1, 2)+"' "+
            				 "and d.grpcontno='"+mGrpContNo+"' " +
            				 "and a.accpayclass = '3'";//此条件判定为公共账户
            	SSRS tSSRS1 = tExeSQL.execSQL(sql);//获取payplancode，accpayclass，dutycode，feecode
            	if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
            		mErrors.addOneError("没有获取到该团单公共账户信息！");
                    return false;
            	}
//                String polNo = CommonBL.getPubliAccPolNo(grpPolNo);
//            	此前公共账户acctype为"1"，团险万能为"001"
            	String polNo = this.getPubliAccPolNo(grpPolNo);
                //多个账户险的出现，需要关联险种到账户类型
                LCInsureAccSchema tLCInsureAccSchema =
                        CommonBL.getLCInsureAcc(polNo,
                                                CommonBL.getInsuAccNo(BQ.
                        ACCTYPE_GROUP,tSSRS.GetText(1, 2)));
                if (tLCInsureAccSchema == null) {
                    mErrors.addOneError("找不到团体账户信息！");
                    return false;
                }
                mSpecialData.setGrpPolNo(grpPolNo);
                double getMoney =
                        Double.parseDouble(mSpecialData.getEdorValue(
                                "GroupMoney"));
                System.out.println("getMoney" + getMoney);
                if (getMoney == 0) {
                	//add by lzy 20160128删除公共账户之前录入的信息，避免批单显示错误。
                    String delStr="delete from lpedorespecialdata where edorno='"+mEdorNo+"' " +
                    		" and polno='"+grpPolNos[i]+"' and detailtype in ('GROUPMONEY','GRPBEFOREMONEY','GRPGETMONEY','GRPLEFTMONEY','GRPMANAGEMONEY') ";
                    mMap.put(delStr, SysConst.DELETE);
                    continue;
                }
//                double manageMoney = CommonBL.carry(getMoney *
//                        CommonBL.getManageFeeRate(grpPolNo, BQ.FEECODE_GROUP));
                double manageFeeRate =this.getManageFeeRate(grpPolNo);
                double manageMoney = Arith.mul(getMoney, manageFeeRate);
                System.out.println("manageMoney：" + manageMoney);
//                double chgMoney = CommonBL.carry(getMoney - manageMoney);
                double chgMoney =Arith.sub(getMoney, manageMoney);
//                double leftMoney = CommonBL.carry(tLCInsureAccSchema.
//                                                  getInsuAccBala() + chgMoney);
                double leftMoney = Arith.add(tLCInsureAccSchema.getInsuAccBala() , chgMoney);
//                double sumMoney = CommonBL.carry(tLCInsureAccSchema.getSumPay() + chgMoney);
                double sumMoney = Arith.add(tLCInsureAccSchema.getSumPay(), chgMoney);
                LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
                Reflections ref = new Reflections();
                ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                tLPInsureAccSchema.setEdorNo(mEdorNo);
                tLPInsureAccSchema.setEdorType(mEdorType);
                tLPInsureAccSchema.setInsuAccBala(leftMoney);
                tLPInsureAccSchema.setSumPay(sumMoney);
                tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                tLPInsureAccSchema.setModifyDate(mCurrentDate);
                tLPInsureAccSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                LCPolSchema tLCPolSchema = CommonBL.getLCPol(polNo);
                if (tLCPolSchema == null) {
                    mErrors.addOneError("找不到团体公共账户的险种信息！");
                    return false;
                }
                setGetEndorse(chgMoney, tLCPolSchema,tSSRS1.GetText(1, 3));//671003
                setGetEndorseMF(manageMoney,tLCPolSchema,tSSRS1.GetText(1, 3)); //add by fuxin 2008-6-30 管理费
                mGetMoney += getMoney;
                //设置批单显示数据
                mVtsData.setGrpPolNo(grpPolNos[i]);
                mVtsData.add("GrpGetMoney", CommonBL.bigDoubleToCommonString(getMoney,"0.00"));
                mVtsData.add("GrpManageMoney", CommonBL.bigDoubleToCommonString(manageMoney,"0.00"));
                mVtsData.add("GrpLeftMoney", CommonBL.bigDoubleToCommonString(leftMoney,"0.00"));
                mVtsData.add("GrpBeforeMoney", CommonBL.bigDoubleToCommonString(tLCInsureAccSchema.getInsuAccBala(),"0.00"));
                setLPInsureAccTrace(tLCInsureAccSchema, chgMoney,"G");
                setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
                //****2012-06-01 新添代码  增加 LPInsureAccClass 的处理***//
                setLPInsureAccClass(tLCInsureAccSchema, leftMoney,sumMoney);
                //****************************************************//
                setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney,
                                       CommonBL.getManageFeeRate(grpPolNo, tSSRS1.GetText(1, 4)));//671401
                setLPInsureAccFeeTrace(tLCInsureAccSchema,manageMoney,
                                       CommonBL.getManageFeeRate(grpPolNo, tSSRS1.GetText(1, 4)),tSSRS1.GetText(1, 4),"G");

            }
        }
        return true;
    }
    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccSchema aLCInsureAccSchema, double grpMoney,String flag)
    {
        String serialNo;
        LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
        tLPInsureAccTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccTraceDB.setEdorType(mEdorType);
        tLPInsureAccTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.query();
        if (tLPInsureAccTraceSet.size() > 0)
        {
            serialNo = tLPInsureAccTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccTrace " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        }
        else
        {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
//        tLPInsureAccTraceSchema.setPayPlanCode(BQ.FILLDATA);
        String insuredNo = aLCInsureAccSchema.getInsuredNo();
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLCInsureAccClassDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLCInsureAccClassDB.setInsuredNo(insuredNo);
//       当传入的是G，代表是处理公共账户，如果传入是I，代表处理个人账户
        if("G".equals(flag)){
        	tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_GROUP);
        }else if ("I".equals(flag)){
        	tLCInsureAccClassDB.setAccType(BQ.ACCTYPE_INSURED);
        }else {
        	System.out.println("flag 是空的");
        }
        
        LCInsureAccClassSet tLCInsureAccClassSet =tLCInsureAccClassDB.query();
        if(tLCInsureAccClassSet.size()>1){
        	return ;
        }        
        tLPInsureAccTraceSchema.setPayPlanCode(tLCInsureAccClassSet.get(1).getPayPlanCode());
        
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(BQ.FEEFINATYPE_BF);
        tLPInsureAccTraceSchema.setMoney(Math.abs(grpMoney));  //退费
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    /**
  * 设置管理费
  * @param aLCInsureAccSchema LCInsureAccSchema
  * @param manageMoney double
  */
 private void setLPInsureAccFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney)
 {
     String[] keys = new String[2];
     DetailDataQuery mQuery = new DetailDataQuery(mEdorNo, mEdorType);
     keys[0] = aLCInsureAccSchema.getPolNo();
     keys[1] = aLCInsureAccSchema.getInsuAccNo();
     LPInsureAccFeeSchema tLPInsureAccFeeSchema = (LPInsureAccFeeSchema)
             mQuery.getDetailData("LCInsureAccFee", keys);

    //若没有管理费，则生成一条
    if(tLPInsureAccFeeSchema.getGrpContNo() == null)
    {
        Reflections ref = new Reflections();
        ref.transFields(tLPInsureAccFeeSchema, aLCInsureAccSchema);
        tLPInsureAccFeeSchema.setFeeRate(0);
        tLPInsureAccFeeSchema.setFee(0);
        tLPInsureAccFeeSchema.setFeeUnit(0);
        tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLPInsureAccFeeSchema);

        LCInsureAccFeeSchema schemaC = new LCInsureAccFeeSchema();
        ref.transFields(schemaC, tLPInsureAccFeeSchema);
        mMap.put(schemaC, SysConst.INSERT);
    }

     tLPInsureAccFeeSchema.setFee(tLPInsureAccFeeSchema.getFee() + manageMoney);
     tLPInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
     tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
     tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
     mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
 }

 /**
  * 设置管理费分类 轨迹
  * @param aLCInsureAccSchema LCInsureAccSchema
  * @param manageMoney double
  * @param feeRate double
  */
 private void setLPInsureAccClassFee(LCInsureAccSchema aLCInsureAccSchema, double manageMoney, double feeRate)
 {
	 LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
	 LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
     LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
     LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
     tLCInsureAccClassFeeDB.setPolNo(aLCInsureAccSchema.getPolNo());
     tLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
     tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
     for(int i=1; i<= tLCInsureAccClassFeeSet.size(); i++){
    	 if("000000".equals(tLCInsureAccClassFeeSet.get(i).getPayPlanCode())){
    		 continue;
    	 }else{
    		 tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(i);
    	 }
     }
     
     tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
     tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
     tLPInsureAccClassFeeSchema.setPolNo(aLCInsureAccSchema.getPolNo());
     tLPInsureAccClassFeeSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
////     tLPInsureAccClassFeeSchema.setPayPlanCode(BQ.FILLDATA);
//     String sql_payplancode = "select payplancode from lcinsureaccclassfee where polno='"+aLCInsureAccSchema.getPolNo()+"' and "
//    		 + " insuaccno='"+aLCInsureAccSchema.getInsuAccNo()+"' and contno='"+aLCInsureAccSchema.getContNo()+"' and "
//    		 + " grppolno='"+aLCInsureAccSchema.getGrpPolNo()+"' and payplancode<>'000000' fetch first 1 rows only ";
//     tLPInsureAccClassFeeSchema.setPayPlanCode(new ExeSQL().getOneValue(sql_payplancode));
     tLPInsureAccClassFeeSchema.setPayPlanCode(tLCInsureAccClassFeeSchema.getPayPlanCode());
     tLPInsureAccClassFeeSchema.setOtherNo(tLCInsureAccClassFeeSchema.getOtherNo());
     tLPInsureAccClassFeeSchema.setOtherType(tLCInsureAccClassFeeSchema.getOtherType()); //3是保全
     tLPInsureAccClassFeeSchema.setAccAscription("0");
     tLPInsureAccClassFeeSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
     tLPInsureAccClassFeeSchema.setContNo(aLCInsureAccSchema.getContNo());
     tLPInsureAccClassFeeSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
     tLPInsureAccClassFeeSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
     tLPInsureAccClassFeeSchema.setManageCom(aLCInsureAccSchema.getManageCom());
     tLPInsureAccClassFeeSchema.setInsuredNo(aLCInsureAccSchema.getInsuredNo());
     tLPInsureAccClassFeeSchema.setAppntNo(aLCInsureAccSchema.getAppntNo());
     tLPInsureAccClassFeeSchema.setAccType(aLCInsureAccSchema.getAccType());
     tLPInsureAccClassFeeSchema.setAccComputeFlag(aLCInsureAccSchema.getAccComputeFlag());
     tLPInsureAccClassFeeSchema.setAccFoundDate(aLCInsureAccSchema.getAccFoundDate());
     tLPInsureAccClassFeeSchema.setAccFoundTime(aLCInsureAccSchema.getAccFoundTime());
     tLPInsureAccClassFeeSchema.setBalaDate(aLCInsureAccSchema.getBalaDate());
     tLPInsureAccClassFeeSchema.setBalaTime(aLCInsureAccSchema.getBalaTime());
     tLPInsureAccClassFeeSchema.setFeeRate(feeRate);
     tLPInsureAccClassFeeSchema.setFee(tLCInsureAccClassFeeSchema.getFee()+manageMoney);
     tLPInsureAccClassFeeSchema.setFeeUnit(0);
     tLPInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
     tLPInsureAccClassFeeSchema.setMakeDate(mCurrentDate);
     tLPInsureAccClassFeeSchema.setMakeTime(mCurrentTime);
     tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
     tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);

     mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
    }


    /**
     * 设置个人账户追加保费金额
     * @return boolean
     */
    private boolean setInsuredAcc() {
        LCGrpPolSet tLCGrpPolSet = CommonBL.getLCGrpPolSet(mGrpContNo);
        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            String grpPolNo = tLCGrpPolSchema.getGrpPolNo();
            //考虑特需和普通险种混合，只处理特需险种。
            String sqlEspecialRiskcode =
                    "select b.risktype4,a.riskcode,b.RiskProp from lcgrppol a,lmriskapp b where a.grppolno = '"
                    + grpPolNo + "' and a.riskcode = b.riskcode  ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sqlEspecialRiskcode);
            if (tSSRS.getMaxRow() != 0 && (tSSRS.GetText(1, 1).equals("4")||tSSRS.GetText(1,3).equals("G"))) {
            	String sql = "select a.payplancode,a.accpayclass,c.dutycode,d.feecode from lmdutypay a "+
				 "left join lmdutypayrela b on a.payplancode = b.payplancode "+
				 "left join  lmriskduty c on b.dutycode = c.dutycode "+
				 "left join lcgrpfee d on a.payplancode = d.payplancode "+
				 "where d.riskcode='"+tSSRS.GetText(1, 2)+"' "+
				 "and d.grpcontno='"+mGrpContNo+"' " +
				 "and a.accpayclass <> '3' " +
				 "order by a.accpayclass ";//此条件判定为公共账户
            	SSRS tSSRS1 = tExeSQL.execSQL(sql);//获取payplancode，accpayclass，dutycode，feecode
            	if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
            		mErrors.addOneError("没有获取到该团单公共账户信息！");
            		return false;
            	}
                //String riskSeqNo = tLCGrpPolSchema.getRiskSeqNo();
                LPDiskImportSet tLPDiskImportSet = CommonBL.getLPDiskImportSet
                        (mEdorNo, mEdorType, mGrpContNo, null,
                         BQ.IMPORTSTATE_SUCC);
                double insuredGetMoney = 0.0;
                double insuredManageMoney = 0.0;
                double insuredGrpMoney = 0.0;
                double insuredPersonMoney = 0.0;
                for (int j = 1; j <= tLPDiskImportSet.size(); j++) {
                    LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(j);
                    String insuredNo = tLPDiskImportSchema.getInsuredNo();
                    String check=new ExeSQL().getOneValue("select 1 from lcpol where grppolno='"+grpPolNo+"' and insuredno='"+insuredNo+"' and polstate in ('03060001','03060002')");
                    if(check!=null&&(!check.equals(""))){
                    	mErrors.addOneError("被保人" + insuredNo + "已经满期给付！");
                    	return false;
                    }
                    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                    tLCInsureAccDB.setGrpPolNo(grpPolNo);
                    tLCInsureAccDB.setInsuredNo(insuredNo);
                    tLCInsureAccDB.setAccType(BQ.ACCTYPE_INSURED);
                    LCInsureAccSet tLCInsureAccSet =tLCInsureAccDB.query();
                    if (tLCInsureAccSet == null || tLCInsureAccSet.size()<=0) {
                    	mErrors.addOneError("找不到客户" + insuredNo + "的个人账户信息！");
                    	return false;
                    }
                    double manageMoneyRate = this.getManageFeeRate(grpPolNo);
                    double getBeforeMoney = 0;
                    double getGetMoney = 0;
                    double SumManageMoney = 0;
                    for(int m=1;m<=tLCInsureAccSet.size();m++){
                    	LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(m);
                    	double getMoney = 0;
                    	String feecode = "";
                    	String dutycode = "";
                    	//20160125加上团体万能B款的账户处理
                    	if("671804".equals(tLCInsureAccSchema.getInsuAccNo())
                    			||"672804".equals(tLCInsureAccSchema.getInsuAccNo()) ){//单位交费
                    		getMoney = tLPDiskImportSchema.getAppntPrem();
                    		insuredGrpMoney+=getMoney;
                    		feecode = tSSRS1.GetText(1, 4);//671402
                    		dutycode = tSSRS1.GetText(1, 3);//671004
                    	}else if("671805".equals(tLCInsureAccSchema.getInsuAccNo())
                    			|| "672805".equals(tLCInsureAccSchema.getInsuAccNo()) ){//单位代扣
                    		getMoney = tLPDiskImportSchema.getPersonPrem();
                    		insuredPersonMoney+=getMoney;
                    		feecode = tSSRS1.GetText(2, 4);//671403
                    		dutycode = tSSRS1.GetText(2, 3);//671005
                    	}else if("671806".equals(tLCInsureAccSchema.getInsuAccNo())
                    			||"672806".equals(tLCInsureAccSchema.getInsuAccNo()) ){//个人缴费
                    		getMoney = tLPDiskImportSchema.getPersonOwnPrem();
                    		feecode = tSSRS1.GetText(3, 4);//671404
                    		dutycode = tSSRS1.GetText(3, 3);//671006
                    	}
                        double manageMoney = Arith.round(Arith.mul(getMoney, manageMoneyRate), 2);
                        SumManageMoney = Arith.add(SumManageMoney, manageMoney);
                        double chgMoney = Arith.sub(getMoney, manageMoney) ;
                        double leftMoney = Arith.add(tLCInsureAccSchema.getInsuAccBala(), chgMoney);
                        getGetMoney = Arith.add(getGetMoney, leftMoney);
                        getBeforeMoney = Arith.add(getBeforeMoney, tLCInsureAccSchema.getInsuAccBala());
                        double sumMoney = Arith.add(tLCInsureAccSchema.getSumPay(),chgMoney);
                        LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
                        Reflections ref = new Reflections();
                        ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
                        tLPInsureAccSchema.setEdorNo(mEdorNo);
                        tLPInsureAccSchema.setEdorType(mEdorType);
                        tLPInsureAccSchema.setInsuAccBala(leftMoney);
                        tLPInsureAccSchema.setSumPay(sumMoney);
                        tLPInsureAccSchema.setOperator(mGlobalInput.Operator);
                        tLPInsureAccSchema.setModifyDate(mCurrentDate);
                        tLPInsureAccSchema.setModifyTime(mCurrentTime);
                        mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
                        LCPolSchema tLCPolSchema = CommonBL.getLCPol(grpPolNo,insuredNo);
                        if (tLCPolSchema == null) {
                            mErrors.addOneError("找不到客户" + insuredNo + "的险种信息！");
                            return false;
                        }
                        setGetEndorse(chgMoney, tLCPolSchema,dutycode);
                        setGetEndorseMF(manageMoney,tLCPolSchema,dutycode);  //add by fuxin 2008-6-30 管理费
                        mGetMoney += getMoney;
                        insuredGetMoney += getMoney;
                        insuredManageMoney += manageMoney;
                        setLPInsureAccTrace(tLCInsureAccSchema, chgMoney,"I");
                        //****2012-06-01 新添代码  增加 LPInsureAccClass 的处理***//
                        setLPInsureAccClass(tLCInsureAccSchema, leftMoney,sumMoney);
                        //****************************************************//
                        setLPInsureAccFee(tLCInsureAccSchema, manageMoney);
                        setLPInsureAccClassFee(tLCInsureAccSchema, manageMoney,manageMoneyRate);
                        setLPInsureAccFeeTrace(tLCInsureAccSchema,manageMoney,manageMoneyRate, feecode,"I");
                    }
                    setDiskImport(tLPDiskImportSchema,getBeforeMoney, getGetMoney,SumManageMoney);
                }
                //设置批单显示数据
                mVtsData.setGrpPolNo(grpPolNo);
                mVtsData.add("InsuredGetMoney", CommonBL.bigDoubleToCommonString(insuredGetMoney,"0.00"));
                mVtsData.add("InsuredManageMoney",CommonBL.bigDoubleToCommonString(insuredManageMoney,"0.00"));
                mVtsData.add("insuredGrpMoney", CommonBL.bigDoubleToCommonString(insuredGrpMoney,"0.00"));
                mVtsData.add("insuredPersonMoney",CommonBL.bigDoubleToCommonString(insuredPersonMoney,"0.00"));
                mVtsData.add("AllGetMoney",CommonBL.bigDoubleToCommonString(mGetMoney,"0.00"));
            }
        }
        return true;
    }

    /**
     * 设置批改补退费表
     * @return boolean
     */
    private void setGetEndorse(double edorPrem, LCPolSchema aLCPolSchema,String aDutyCode) {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
//        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setDutyCode(aDutyCode);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }
    
    private void setLPInsureAccClass(LCInsureAccSchema aLCInsureAccSchema, double InsuAccBala,double SumPay)
    {
   	 LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
   	 LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
   	 LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
        LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
        
   	 tLCInsureAccClassDB.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
   	 tLCInsureAccClassDB.setPolNo(aLCInsureAccSchema.getPolNo());
   	 tLCInsureAccClassDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
   	 tLCInsureAccClassSet  = tLCInsureAccClassDB.query();
   	 tLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);

   	        Reflections ref = new Reflections();
   	        ref.transFields(tLPInsureAccClassSchema, tLCInsureAccClassSchema);
   	        tLPInsureAccClassSchema.setInsuAccBala(InsuAccBala);
   	        tLPInsureAccClassSchema.setSumPay(SumPay);
   	        tLPInsureAccClassSchema.setOperator(mGlobalInput.Operator);
   	        PubFun.fillDefaultField(tLPInsureAccClassSchema);
   	        tLPInsureAccClassSchema.setEdorNo(mEdorNo);
   	        tLPInsureAccClassSchema.setEdorType(mEdorType);
   	        
   	     mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
       }


    /**
     * 设置批改补退费表
     * add by fuxin 2008-6-30 管理费
     * @return boolean
     */
    private void setGetEndorseMF(double edorPrem, LCPolSchema aLCPolSchema,String aDutyCode) {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(mEdorNo); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(mEdorNo);
        tLJSGetEndorseSchema.setFeeOperationType(mEdorType);
        tLJSGetEndorseSchema.setGrpContNo(mGrpContNo);
        tLJSGetEndorseSchema.setContNo(aLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(aLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mEdorValiDate);
        tLJSGetEndorseSchema.setGetMoney(edorPrem);
        tLJSGetEndorseSchema.setRiskCode(aLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(aLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(aLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(aLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(aLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(aLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(aLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_MF);
        tLJSGetEndorseSchema.setAppntNo(aLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
//        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setDutyCode(aDutyCode);
        tLJSGetEndorseSchema.setPayPlanCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setOtherNo(mEdorNo);
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_G);
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(aLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
    }


    /**
     * 设置item表中的费用和状态
     */
    private void setGrpEdorItem() {
        String sql = "update LPGrpEdorItem " +
                     "set GetMoney = " + mGetMoney + ", " +
                     "EdorState = '" + BQ.EDORSTATE_CAL + "', " +
                     "UWFlag = '" + BQ.UWFLAG_PASS + "', " +
                     "ModifyDate = '" + mCurrentDate + "', " +
                     "ModifyTime = '" + mCurrentTime + "' " +
                     "where EdorNo = '" + mEdorNo + "' " +
                     "and EdorType = '" + mEdorType + "' ";
        mMap.put(sql.toString(), "UPDATE");
    }

    /**
     * 设置LPDiskImport中显示的信息
     */
    private void setDiskImport(LPDiskImportSchema tLPDiskImportSchema,double beforeMoney,
                               double leftMoney,double aMoney2) {
    	tLPDiskImportSchema.setMoney(beforeMoney);
        tLPDiskImportSchema.setGetMoney(leftMoney);
        tLPDiskImportSchema.setMoney2(String.valueOf(aMoney2));
        tLPDiskImportSchema.setOperator(mGlobalInput.Operator);
        tLPDiskImportSchema.setModifyDate(mCurrentDate);
        tLPDiskImportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPDiskImportSchema, "DELETE&INSERT");
    }

    /**
     * 保存要在批单中显示的数据
     */
    private void setEdorInfo() {
        mMap.put(mVtsData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     *
     * 管理费轨迹
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @param grpFee double
     * @param feeRate double
     * @param tFeeCode String
     */
    private void setLPInsureAccFeeTrace(LCInsureAccSchema aLCInsureAccSchema, double grpFee, double feeRate,String tFeeCode,String flag){
        String serialNo;
        LPInsureAccFeeTraceDB tLPInsureAccFeeTraceDB = new LPInsureAccFeeTraceDB();
        tLPInsureAccFeeTraceDB.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceDB.setEdorType(mEdorType);
        tLPInsureAccFeeTraceDB.setGrpContNo(mGrpContNo);
        tLPInsureAccFeeTraceDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccFeeTraceDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        LPInsureAccFeeTraceSet tLPInsureAccFeeTraceSet = tLPInsureAccFeeTraceDB.query();
        if (tLPInsureAccFeeTraceSet.size() > 0){
            serialNo = tLPInsureAccFeeTraceSet.get(1).getSerialNo();
            String sql = "delete from LPInsureAccFeeTrace " +
                         "where EdorNo = '" + mEdorNo + "' " +
                         "and EdorType = '" + mEdorType + "' " +
                         "and SerialNo = '" + serialNo + "' ";
            mMap.put(sql, "DELETE");
        } else {
            serialNo = PubFun1.CreateMaxNo("BQACCTRACE", 9);
        }
        LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new LPInsureAccFeeTraceSchema();
        tLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
        tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccSchema.getGrpContNo());
        tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccSchema.getContNo());
        tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccSchema.getPolNo());
        tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
        tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccSchema.getRiskCode());
//        tLPInsureAccFeeTraceSchema.setPayPlanCode(BQ.FILLDATA);
        String insuredNo = aLCInsureAccSchema.getInsuredNo();
        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
        tLCInsureAccClassFeeDB.setGrpPolNo(aLCInsureAccSchema.getGrpPolNo());
        tLCInsureAccClassFeeDB.setPolNo(aLCInsureAccSchema.getPolNo());
        tLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccSchema.getInsuAccNo());
        tLCInsureAccClassFeeDB.setInsuredNo(insuredNo);
//      当传入的是G，代表是处理公共账户，如果传入是I，代表处理个人账户
       if("G".equals(flag)){
    	   tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_GROUP);
       }else if ("I".equals(flag)){
    	   tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
       }else {
       	System.out.println("flag 是空的");
       }
        LCInsureAccClassFeeSet tLCInsureAccClassFeeSet =tLCInsureAccClassFeeDB.query();
        if(tLCInsureAccClassFeeSet.size()>1){
        	return ;
        }        
        tLPInsureAccFeeTraceSchema.setPayPlanCode(tLCInsureAccClassFeeSet.get(1).getPayPlanCode());
        
        tLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setOtherType("3"); //3是保全
        tLPInsureAccFeeTraceSchema.setAccAscription("0");
        tLPInsureAccFeeTraceSchema.setMoneyType(BQ.FEEFINATYPE_MF);
        tLPInsureAccFeeTraceSchema.setFeeRate(feeRate);
        tLPInsureAccFeeTraceSchema.setFee(Math.abs(grpFee));  //退费
        tLPInsureAccFeeTraceSchema.setFeeUnit("0");
        tLPInsureAccFeeTraceSchema.setPayDate(mEdorValiDate);
        tLPInsureAccFeeTraceSchema.setState("0");
        tLPInsureAccFeeTraceSchema.setFeeCode(tFeeCode);
        tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccSchema.getManageCom());
        tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
        tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
}


    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo("20070213000061");

        VData d = new VData();
        d.add(gi);
        d.add(tLPGrpEdorItemDB.query().get(1));

        GrpEdorTYAppConfirmBL bl = new GrpEdorTYAppConfirmBL();
        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
    /**
     * 得到团体公共帐户
     * @param grpPolNo String
     * @return String
     */
    public String getPubliAccPolNo(String grpPolNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpPolNo(grpPolNo);
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
//        tLCPolDB.setAccType("001");
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() == 0)
        {
            throw new RuntimeException();
        }
        return tLCPolSet.get(1).getPolNo();
    }
    /**
     * 得到团体万能初始管理费费率
     * @param grpPolNo String
     * @param accType String
     * @return double
     */
    public double getManageFeeRate(String grpPolNo)
    {
        LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
        tLCGrpFeeDB.setGrpPolNo(grpPolNo);
        String sql = "select * from lcgrpfee where grpPolNo = '"+grpPolNo+"' and FeeType = '0' fetch first 1 rows only";
        LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.executeQuery(sql);
        if (tLCGrpFeeSet.size() == 0)
        {
            return 0;
        }
        return tLCGrpFeeSet.get(1).getFeeValue();
    }
    
    /**
     * 处理个人账户
     * @author gzh
     * @param
     * @return
     * */
}
