package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.LPDutyBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.BuildError;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 被保人重要资料变更保全申请确认</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @ReWrite ZhangRong,Alex,Luomin
 * @author <p>refactor by QiuYang 2005</p>
 * @version 1.0
 */
public class PEdorICAppConfirmBL implements EdorAppConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private LCInsureAccClassSchema mLCInsureAccClassSchema = new LCInsureAccClassSchema();
//  万能重疾的accclass,实际上并不存在,只和主险的accclass差在payplancode上
    private LCInsureAccClassSchema mLCInsureAccClassSchemaZJ = new LCInsureAccClassSchema();

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private String mInsuredNo = null;

    private double mGetMoney = 0.00;

    private double mChgPrem = 0.00;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private ReCalBL mReCalBL = null;
    
    private LPPolSet mLPPolSet=new LPPolSet();

    private EdorItemSpecialData mSpecialData = null;

    //每一保单的万能风险保费补退费
    private double riskPrem = 0.0;
    //万能附加重疾的风险保费补退费
    private double riskPremZJ = 0.0;
    
    private String mProjectType = null;


    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到计算结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                                getObjectByObjectName("LPEdorItemSchema", 0);
            mEdorNo = mLPEdorItemSchema.getEdorNo();
            mEdorType = mLPEdorItemSchema.getEdorType();
            mContNo = mLPEdorItemSchema.getContNo();
            mInsuredNo = mLPEdorItemSchema.getInsuredNo();
            mSpecialData = new EdorItemSpecialData(mLPEdorItemSchema);
            mSpecialData.query();
            mProjectType = getProjectType();
        } catch (Exception ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("传入数据错误！");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (needChangePrem())
        {
            if (!dealPols())
            {
                return false;
            }
            setEdorItem();
            setLPCont();
        }
        return true;
    }

    /**
     * 按险种处理保全数据
     * @return boolean
     */
    /**
     * @return
     */
    private boolean dealPols()
    {
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        tLPPolDB.setEdorType(mEdorType);
        tLPPolDB.setInsuredNo(mInsuredNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
//        	add by xp 090525 由于之前的程序添加了对于同一客户其他保单下资料的修改,现添加如果其他保单中存在无名单则不进行重算费
        	if(tLPPolSet.get(i).getContType().equals("2") && CommonBL.isNoNameGrpCont(tLPPolSet.get(i).getGrpContNo()))
        	{
        		System.out.println(tLPPolSet.get(i).getPolNo()+"险种为无名单下险种,无需重算费");
        		continue;
        	}
 
            if (!CommonBL.hasULIRisk(tLPPolSet.get(i).getContNo()))
            { //如果保单不是万能
                if (!dealOnePol(tLPPolSet.get(i)))
                {
                    return false;
                }
            } else if (CommonBL.isTPHIPol(tLPPolSet.get(i).getRiskCode()))
            {	//税优计算风险保费
            	if (!dealTPHICont(tLPPolSet.get(i)))
                {
                    return false;
                }
                if (riskPrem != 0)
                { //如果有补退费，才生成帐户表和批改补退费表
                    if (!setInsuAcc(tLPPolSet.get(i))) 
                    {
                        return false;
                    }
                }  
                setLJSGetEndorseSY(tLPPolSet.get(i));
            } else if (CommonBL.hasULIRisk(tLPPolSet.get(i).getContNo()))
            { //如果是万能险主险,并且险种为万能险的个险
                if (tLPPolSet.get(i).getPolNo().equals(tLPPolSet.get(i).
                        getMainPolNo())&&CommonBL.isULIRisk(tLPPolSet.get(i).getRiskCode())&&tLPPolSet.get(i).getContType().equals("1") && !CommonBL.isTPHIPol(tLPPolSet.get(i).getRiskCode()))
                {
                	//附加重疾也在此处一并处理
                    	if (!dealOneCont(tLPPolSet.get(i)))
                        {
                            return false;
                        }
                        if (riskPrem != 0||riskPremZJ != 0)
                        { //如果有补退费，才生成帐户表和批改补退费表
                            if (!setInsuAcc(tLPPolSet.get(i))) 
                            {
                                return false;
                            }
                            setLJSGetEndorseWN(tLPPolSet.get(i));
                        }                    
                }else {
                	//如果是附加万能的保单，并且险种不是万能险
                	String isULIRiskSQL = " SELECT 1 FROM LCPOL a WHERE a.POLNO='"+tLPPolSet.get(i).getPolNo()+"' " +
                			" AND EXISTS (SELECT 1 FROM LCPOL b WHERE b.CONTNO=a.contno and riskcode in ( " +
                			" select code from LDCODE1 where CODETYPE='mainsubriskrela' " +
                			" ) and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='4')) " +
                			" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') with ur  ";
                   ExeSQL tExeSQL = new ExeSQL();
                   String isULIRisk = tExeSQL.getOneValue(isULIRiskSQL);
                   if(isULIRisk!=null&&"1".equals(isULIRisk)){
                	   if (!dealOnePol(tLPPolSet.get(i)))
                	   {
                		   return false;
                	   }
                   }else {
                	   //add by fengzhihang 如果健康一生系列与美满系列产品同时承报的保单，传统险种在此处理
                	   String isJKYSSQL = " SELECT 1 FROM LCPOL a WHERE a.POLNO='"+tLPPolSet.get(i).getPolNo()+"' " +
                          		" and exists (select 1 from lcpol where contno=a.contno and riskcode in (select code from ldcode where codetype = 'jkys')) " +
                          		" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') with ur  ";
                        String isJKYSResult = tExeSQL.getOneValue(isJKYSSQL);
                        if(isJKYSResult!=null&&"1".equals(isJKYSResult)){
                     	   if (!dealOnePol(tLPPolSet.get(i)))
                     	   {
                     		   return false;
                     	   }
                        }
                   }
                }
            }
        }
        LPPolSchema tLPPolSchema=new LPPolSchema();
        for(int i=1;i<=mLPPolSet.size();i++)
        {
        	if(mLPPolSet.get(i).getRiskCode().equals("730101"))
        	{
        		tLPPolSchema=mLPPolSet.get(i);
        		
        		for(int j=1;j<=mLPPolSet.size();j++)
                {
                	if((!mLPPolSet.get(j).getRiskCode().equals("730101"))&&(mLPPolSet.get(j).getContNo().equals(tLPPolSchema.getContNo())))
                	{
                		if(tLPPolSchema.getAmnt()<mLPPolSet.get(j).getAmnt())
                		{
                			mErrors.addOneError("分红险的主险重算后的保额不能大于附加险的保额!");
                            return false;
                		}
                	}
                }
        	}
        }
        
        return true;
    }

    /**
     * 处理一个险种
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOnePol(LPPolSchema aLPPolSchema)
    {
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("未找到序号" + tLCPolSchema.getRiskSeqNo()
                                    + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }
            double oldPrem = tLCPolSchema.getPrem();
            double newPrem = calNewPrem(aLPPolSchema);
            double getMoney = calGetMoney(oldPrem, newPrem, aLPPolSchema);
            setLJSGetEndorse(aLPPolSchema, getMoney);
            setLPTables(newPrem - oldPrem);
            mGetMoney += getMoney;
            mChgPrem += newPrem - oldPrem;
        } catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 万能客户资料变更处理一个保单
     * @param aLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealOneCont(LPPolSchema aLPPolSchema)
    {
//    	每循环一次万能保单就初始化一次全局变量
    	//万能客户资料变更费用变化
        riskPrem = 0.0;
        //万能附加重疾客户资料变更费用变化
        riskPremZJ = 0.0;
        //共用同一个accclass
        mLCInsureAccClassSchema = null;
        
        boolean HaveZJ=false;
        String havezj=new ExeSQL().getOneValue("select 1 from lcpol where contno='"+aLPPolSchema.getContNo()+"' and riskcode in ('231001') ");
        if(havezj!=null&&havezj.equals("1")){
        	HaveZJ=true;
        }
        
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(aLPPolSchema.getPolNo());
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("未找到保单号" + tLCPolSchema.getContNo()
                                    + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }

            /*
             * ------------------万能客户资料扣费计算开始部分--------------------------
             */
            //累计已扣风险保费
            double riskPrem1 = 0.0;
            //IC变更后应扣风险保费
            double riskPrem2 = 0.0;
            riskPrem1 = getRiskPrem1(aLPPolSchema.getContNo());
            //返回结果都是负数，如果为1说明查询失败
            if (riskPrem1 == 1)
            {
                mErrors.addOneError("查询累计已扣风险保费失败!");
                return false;
            }
            riskPrem2 = getRiskPrem2(aLPPolSchema.getContNo());
            if (riskPrem2 == 1)
            {
                mErrors.addOneError("计算应扣风险保费失败!");
                return false;
            }
            //相对于原来的帐户价值的补退费，riskPrem更新到acc表
            riskPrem = CommonBL.carry(riskPrem2 - riskPrem1);
            System.out.println("riskPrem2 :   " + riskPrem2);
            System.out.println("riskPrem1 :   " + riskPrem1);
            System.out.println("riskprem  :   " + riskPrem);
            /*
             * ------------------万能客户资料扣费计算结束部分--------------------------
             */
            
            if(HaveZJ){
            /*
             * ------------------万能附加重疾客户资料扣费计算开始部分--------------------------
             */
            //累计已扣风险保费
            riskPrem1 = 0.0;
            //IC变更后应扣风险保费
            riskPrem2 = 0.0;
            riskPrem1 = getRiskPrem1ZJ(aLPPolSchema.getContNo());
            //返回结果都是负数，如果为1说明查询失败
            if (riskPrem1 == 1)
            {
                mErrors.addOneError("查询附加重疾累计已扣风险保费失败!");
                return false;
            }
            riskPrem2 = getRiskPrem2ZJ(aLPPolSchema.getContNo());
            if (riskPrem2 == 1)
            {
                mErrors.addOneError("计算附加重疾应扣风险保费失败!");
                return false;
            }
            //相对于原来的帐户价值的补退费，riskPrem更新到acc表
            riskPremZJ = CommonBL.carry(riskPrem2 - riskPrem1);
            System.out.println("riskPrem2ZJ :   " + riskPrem2);
            System.out.println("riskPrem1ZJ :   " + riskPrem1);
            System.out.println("riskpremZJ  :   " + riskPremZJ);
            /*
             * ------------------万能附加重疾客户资料扣费计算结束部分--------------------------
             */
            }

        } catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 判断是否需要重算保费
     * @return boolean 需要重算保费返回true
     */
    private boolean needChangePrem()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mContNo);
        tLCInsuredDB.setInsuredNo(mInsuredNo);
        if (!tLCInsuredDB.getInfo())
        {
            mErrors.addOneError("找不到被保人信息！");
            return false;
        }

        LPInsuredDB tLPInsuredDB = new LPInsuredDB();
        tLPInsuredDB.setEdorNo(mEdorNo);
        tLPInsuredDB.setEdorType(mEdorType);
        tLPInsuredDB.setContNo(mContNo);
        tLPInsuredDB.setInsuredNo(mInsuredNo);
        if (!tLPInsuredDB.getInfo())
        {
            mErrors.addOneError("找不到被保人保全变更信息！");
            return false;
        }
        //20090112 zhanggm 个单不进行无名单的校验
        if(mProjectType.equals("1") && CommonBL.isNoNameGrpCont(mLPEdorItemSchema.getGrpContNo()))
        {
            //无名单下的CM不用算费
            return false;
        }
        
        //如果性别和年龄改变则重算保额保费
        if (!StrTool.compareString(tLCInsuredDB.getSex(),
                                   tLPInsuredDB.getSex()) ||
            !StrTool.compareString(tLCInsuredDB.getBirthday(),
                                   tLPInsuredDB.getBirthday()) ||
            !StrTool.compareString(tLCInsuredDB.getOccupationType(),
                                   tLPInsuredDB.getOccupationType()))
        {
            return true;
        }
        return false;
    }

    /**
     * 得到保全算费的计算代码
     * @param riskCode String
     * @return String
     */
    private LMPolDutyEdorCalSchema getEdorCal(String riskCode)
    {
        LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
        tLMPolDutyEdorCalDB.setRiskCode(riskCode);
        tLMPolDutyEdorCalDB.setEdorType(mEdorType);
        LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB.query();
        if (tLMPolDutyEdorCalSet.size() == 0)
        {
            return null;
        }
        return tLMPolDutyEdorCalSet.get(1);
    }

//    /**
//     * 得到交费起始日期
//     * @param aLPPolSchema LPPolSchema
//     * @return String
//     */
//    private String getLastPayToDate(LPPolSchema aLPPolSchema)
//    {
//        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
//        tLJAPayPersonDB.setPolNo(aLPPolSchema.getPolNo());
//        tLJAPayPersonDB.setCurPayToDate(aLPPolSchema.getPaytoDate());
//        tLJAPayPersonDB.setPayType("ZC");
//        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
//        if (tLJAPayPersonSet.size() == 0)
//        {
//            return null;
//        }
//        return tLJAPayPersonSet.get(1).getLastPayToDate();
//    }

    /**
     * 得到交费期数，即当前是第几期交费
     * @return int
     */
    private int getPayNum(LPPolSchema tLPPolSchema)
    {
        if (tLPPolSchema.getPayIntv() == 0)
        {
            return 1;
        }
        else
        {
            return PubFun.calInterval(tLPPolSchema.getCValiDate(),
                    tLPPolSchema.getPaytoDate(), "M") / tLPPolSchema.getPayIntv();
        }
    }

    /**
     * 重算保费
     * @param aLPPolSchema LPPolSchema
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private double calNewPrem(LPPolSchema aLPPolSchema) throws Exception
    {
        //重算保费
        mReCalBL = new ReCalBL(aLPPolSchema, mLPEdorItemSchema);
        if (!mReCalBL.recal())
        {
            mErrors.copyAllErrors(mReCalBL.mErrors);
            throw new Exception();
        }
        for(int i=1;i<=mReCalBL.aftLPPolSet.size();i++)
        {
        	mLPPolSet.add(mReCalBL.aftLPPolSet.get(i));
        }
        
        return mReCalBL.aftLPPolSet.get(1).getPrem();
    }

    /**
     * 计算做过保全之后的保费差额
     * @param oldPrem double
     * @param newPrem double
     * @param aLPPolSchema LPPolSchema
     * @return double
     */
    private double calGetMoney(double oldPrem, double newPrem,
            LPPolSchema aLPPolSchema) throws Exception
    {
        double getMoney = 0.0;
        mSpecialData.setPolNo(aLPPolSchema.getPolNo());
        String edorValiDateStr = mSpecialData.getEdorValue("EdorValiDate");
        if ((edorValiDateStr == null) || (edorValiDateStr.equals("")))
        {
            edorValiDateStr = mLPEdorItemSchema.getEdorValiDate();
        }
        Date edorValiDate = CommonBL.stringToDate(edorValiDateStr);
        Date curPayToDate = CommonBL.stringToDate(aLPPolSchema.getPaytoDate());
        String lastPayToDateStr = CommonBL.getLastPayToDate(
                aLPPolSchema.getPolNo(), aLPPolSchema.getPaytoDate());
        Date lastPayToDate = CommonBL.stringToDate(lastPayToDateStr);
        if (lastPayToDate == null)
        {
            mErrors.addOneError("未找到本期交费起始日期！");
            throw new Exception();
        }
        if (curPayToDate == null)
        {
            mErrors.addOneError("未找到本期交至日期！");
            throw new Exception();
        }

        if ((oldPrem == newPrem) || (edorValiDate.after(curPayToDate))) //如果加费起始日期在下期，则本期交费为0
        {
            return 0;
        }
        else if (edorValiDate.before(lastPayToDate)) //如果加费起始日期在上期，则追溯前几期保费
        {
//          getMoney = CommonBL.carry(aLPPolSchema.getPrem() *
//          		(PubFun.calInterval(edorValiDate, curPayToDate, "M") / aLPPolSchema.getPayIntv()));
	
			//20140630--应补交保费的计算公式:所在的交费期的期数×年龄变更后每期应交保费-实际累计已交保费
			 double sumActPay=0.0;	//实际累计已交保费
			 double sumDuePay=0.0;
			 LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
			 tLJAPayPersonDB.setPolNo(aLPPolSchema.getPolNo());
			 tLJAPayPersonDB.setPayType("ZC");
			 LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
			 //累计实际已交保费(计算到月份)
			 for(int i=1;i<=tLJAPayPersonSet.size();i++){
				 Date tempCurPaytoDate=CommonBL.stringToDate(tLJAPayPersonSet.get(i).getCurPayToDate());
				 Date tempLastPaytoDate=CommonBL.stringToDate(tLJAPayPersonSet.get(i).getLastPayToDate());
				 if(tempCurPaytoDate.before(edorValiDate)){
					 continue;
				 }else if(tempCurPaytoDate.after(edorValiDate)&&!tempLastPaytoDate.before(edorValiDate)){
					 sumActPay+=tLJAPayPersonSet.get(i).getSumActuPayMoney();
				 }else if(tempCurPaytoDate.after(edorValiDate)&&tempLastPaytoDate.before(edorValiDate)){
					 //计算保全生效日当期的保费从保全生效日开始计算
					 sumActPay+=CommonBL.carry(tLJAPayPersonSet.get(i).getSumActuPayMoney()*PubFun.calInterval(edorValiDate, tempCurPaytoDate, "M")/aLPPolSchema.getPayIntv());
				 }
			 }
			  //用变更后的保费计算应交
			  sumDuePay = CommonBL.carry(newPrem * PubFun.calInterval(edorValiDate, curPayToDate, "M") / aLPPolSchema.getPayIntv());
			  getMoney=sumDuePay-sumActPay;
  
        }
        else
        {
            //按剩余日期计算保费差额
            BqCalBase tBqCalBase = new BqCalBase();
            tBqCalBase.setPrem(oldPrem);
            tBqCalBase.setNewPrem(newPrem);
            tBqCalBase.setEdorValiDate(edorValiDateStr);
            tBqCalBase.setLastPayToDate(lastPayToDateStr);
            tBqCalBase.setPayToDate(aLPPolSchema.getPaytoDate());
            LMPolDutyEdorCalSchema edorCalSchema =
                    getEdorCal(aLPPolSchema.getRiskCode());
            BqCalBL tBqCalBL = new BqCalBL();
            getMoney += tBqCalBL.calChgMoney(edorCalSchema.getChgPremCalCode(),
                    tBqCalBase);
            if (tBqCalBL.mErrors.needDealError())
            {
                mErrors.addOneError("计算补退费金额失败！");
                throw new Exception();
            }

            //计算利息
            getMoney += tBqCalBL.calChgMoney(edorCalSchema.getInterestCalCode(),
                    tBqCalBase);
            if (tBqCalBL.mErrors.needDealError())
            {
                mErrors.addOneError("计算补退费利息金额失败！");
                throw new Exception();
            }
        }
        return CommonBL.carry(getMoney);
    }

    /**
     * 得到财务类型
     * @param edorType String
     * @param code String
     * @return String
     */
    private String getFeeType(String edorType, String code)
    {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType(edorType);
        tLDCode1DB.setCode(code);
        tLDCode1DB.setCode1("000000");
        if (!tLDCode1DB.getInfo())
        {
            mErrors.addOneError("财务类型表LDCode1中没有该项目的配置信息！");
            return null;
        }
        return tLDCode1DB.getCodeName();
    }

    /**
     *  往批改补退费表（应收/应付）新增数据
     * @param aLPPolSchema LPPolSchema
     * @param changePrem double
     * @return boolean
     */
    private boolean setLJSGetEndorse(LPPolSchema aLPPolSchema, double changePrem)
    {
        String feeType = null;
        if (changePrem > 0) //补费
        {
            feeType = getFeeType(mEdorType, "BF");
        }
        else if (changePrem <= 0) //退费
        {
            feeType = getFeeType(mEdorType, "TF");
        }
        if (feeType == null)
        {
            return false;
        }

        BqCalBL bqCalBL = new BqCalBL();
        LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType, changePrem,
                mGlobalInput);
        tLJSGetEndorseSchema.setManageCom(aLPPolSchema.getManageCom());
        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }

    private void setLJSGetEndorseWN(LPPolSchema aLPPolSchema)
    {
    	//一开始来初始化相关变量
        BqCalBL bqCalBL = new BqCalBL();
        String feeType1 = null;
        String feeType2 = null;
        
//      处理万能主险部分
        if(riskPrem!=0){
        if (riskPrem < 0) //补费
        {
            feeType1 = "BF";
            feeType2 = "TB";
        } else if (riskPrem > 0) //退费
        {
            feeType1 = "BF";
            feeType2 = "TF";
        }
        LJSGetEndorseSchema tLJSGetEndorseSchema1 = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType2,
                -Math.abs(riskPrem),
                mGlobalInput);
        LJSGetEndorseSchema tLJSGetEndorseSchema2 = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType1,
                Math.abs(riskPrem),
                mGlobalInput);
        tLJSGetEndorseSchema1.setManageCom(aLPPolSchema.getManageCom());
        tLJSGetEndorseSchema2.setManageCom(aLPPolSchema.getManageCom());
        mMap.put(tLJSGetEndorseSchema1, "DELETE&INSERT");
        mMap.put(tLJSGetEndorseSchema2, "DELETE&INSERT");
        }
        
//      处理万能附加重疾部分
        if(riskPremZJ!=0){
            if (riskPremZJ < 0) //补费
            {
                feeType1 = "BF";
                feeType2 = "TB";
            } else if (riskPremZJ > 0) //退费
            {
                feeType1 = "BF";
                feeType2 = "TF";
            }
            
//            LCPolDB subLCPolDB = new LCPolDB();
//			subLCPolDB.setContNo(mLCInsureAccClassSchemaZJ.getContNo());
//			subLCPolDB.setMainPolNo(mLCInsureAccClassSchemaZJ.getPolNo());
//			subLCPolDB.setRiskCode(mLCInsureAccClassSchemaZJ.getPayPlanCode());
//			LCPolSet subLCPolSet = subLCPolDB.query();
//			if(subLCPolSet==null || subLCPolSet.size()!=1){
//				// @@错误处理
//				System.out.println("PEdorICAppConfirmBL+getRiskPrem2ZJ++--");
//				CError tError = new CError();
//				tError.moduleName = "PEdorICAppConfirmBL";
//				tError.functionName = "getRiskPrem2ZJ";
//				tError.errorMessage = "获取附加重疾险种信息失败!";
//				this.mErrors.addOneError(tError);
//			}
//			LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
//			Reflections ref =new Reflections();
//			ref.transFields(aLPPolSchema, subLCPolSchema);
//			aLPPolSchema.setRiskCode(mLCInsureAccClassSchemaZJ.getRiskCode());
            LJSGetEndorseSchema tLJSGetEndorseSchema1ZJ = bqCalBL.initLJSGetEndorse(
                    mLPEdorItemSchema, aLPPolSchema, null, feeType2,
                    -Math.abs(riskPremZJ),
                    mGlobalInput);
            LJSGetEndorseSchema tLJSGetEndorseSchema2ZJ = bqCalBL.initLJSGetEndorse(
                    mLPEdorItemSchema, aLPPolSchema, null, feeType1,
                    Math.abs(riskPremZJ),
                    mGlobalInput);
            tLJSGetEndorseSchema1ZJ.setPayPlanCode(mLCInsureAccClassSchemaZJ.getPayPlanCode());
            tLJSGetEndorseSchema1ZJ.setManageCom(aLPPolSchema.getManageCom());
            tLJSGetEndorseSchema2ZJ.setPayPlanCode(mLCInsureAccClassSchemaZJ.getPayPlanCode());
            tLJSGetEndorseSchema2ZJ.setManageCom(aLPPolSchema.getManageCom());
            mMap.put(tLJSGetEndorseSchema1ZJ, "DELETE&INSERT");
            mMap.put(tLJSGetEndorseSchema2ZJ, "DELETE&INSERT");
            }
    }
    private void setLJSGetEndorseSY(LPPolSchema aLPPolSchema)
    {
    	//一开始来初始化相关变量
        BqCalBL bqCalBL = new BqCalBL();
        String feeType = null;

        
//      处理万能主险部分
//        riskPrem=-riskPrem;
        if (riskPrem <= 0) //补费
        {
            feeType = "BF";
        } else if (riskPrem > 0) //退费
        {
//            feeType1 = "TB";
            feeType = "TF";
        }

        LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.initLJSGetEndorse(
                mLPEdorItemSchema, aLPPolSchema, null, feeType,
                -riskPrem,
                mGlobalInput);

        mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");

        

    }
    /**
     * 设置P表的保费
     */
    private void setLPTables(double chgPrem)
    {
        for (int i = 1; i <= mReCalBL.aftLPPolSet.size(); i++)
        {
            double sumPrem = mReCalBL.aftLPPolSet.get(i).getSumPrem() + chgPrem;
            mReCalBL.aftLPPolSet.get(i).setSumPrem(sumPrem);
        }
        mMap.put(mReCalBL.aftLPPolSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPDutySet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPPremSet, "DELETE&INSERT");
        mMap.put(mReCalBL.aftLPGetSet, "DELETE&INSERT");
    }

    /**
     * 更新item表的保费
     */
    private void setEdorItem()
    {
        mLPEdorItemSchema.setGetMoney(mGetMoney);
        mLPEdorItemSchema.setChgPrem(mChgPrem);
        mLPEdorItemSchema.setChgAmnt(0); //只变保费，保额不变
        mLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        mLPEdorItemSchema.setModifyDate(mCurrentDate);
        mLPEdorItemSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPEdorItemSchema, "DELETE&INSERT");
    }

    /**
     * 更新LPCont表的保费
     */
    private void setLPCont()
    {
//        String sql = "update LPCont " +
//                "set Prem = Prem + (" + String.valueOf(mChgPrem) + "), " +
//                "    SumPrem = SumPrem + (" + String.valueOf(mChgPrem) + ") " +
//                "where EdorNo = '" + mEdorNo + "' " +
//                "and EdorType = '" + mEdorType + "' ";
        String sql ="update LPCont a " +
			        "set Prem = (select sum(prem) from lppol where contno=a.contno and edorno=a.edorno and edortype=a.edortype),  " +
			        "SumPrem = (select sum(sumprem) from lppol where contno=a.contno and edorno=a.edorno and edortype=a.edortype)  " +
			        "where EdorNo = '" + mEdorNo + "'  " +
			        "and EdorType = '" + mEdorType + "'  ";
        mMap.put(sql, "UPDATE");
    }

    private boolean setInsuAcc(LPPolSchema cLPPolSchema)
    {    	
    	System.out.println("即将进入变更的地方+++++++++++++++++++");
        String sqlDeleteTrace="delete from lpinsureacctrace where edorno = '"+mLPEdorItemSchema.getEdorNo()+"' and polno ='"+mLPEdorItemSchema.getPolNo()+"'";
     	String sqlDeleteFeeTrace="delete from lpinsureaccfeetrace where edorno = '"+mLPEdorItemSchema.getEdorNo()+"' and polno ='"+mLPEdorItemSchema.getPolNo()+"'";
     	mMap.put(sqlDeleteTrace, "DELETE");
     	mMap.put(sqlDeleteFeeTrace, "DELETE");
     	System.out.println("即将进入变更的地方+++++++++++++++++++");
     	System.out.println("***"+sqlDeleteTrace);
     	System.out.println("***"+sqlDeleteFeeTrace);

        if (CommonBL.isTPHIPol(cLPPolSchema.getRiskCode())){
            Reflections ref = new Reflections();
            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            tLCInsureAccDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccDB.setInsuAccNo(CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
                    cLPPolSchema.getRiskCode()));
            if (!tLCInsureAccDB.getInfo())
            {
                mErrors.addOneError("查询帐户表失败!");
                return false;
            }
            LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
            LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
            ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
            tLPInsureAccSchema.setEdorNo(mEdorNo);
            tLPInsureAccSchema.setEdorType(mEdorType);
            tLPInsureAccSchema.setModifyDate(this.mCurrentDate);
            tLPInsureAccSchema.setModifyTime(this.mCurrentTime);
            
            mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
            LPInsureAccClassSchema tLPInsureAccClassSchema = new
                    LPInsureAccClassSchema();
            ref.transFields(tLPInsureAccClassSchema, mLCInsureAccClassSchema);
            tLPInsureAccClassSchema.setEdorNo(mEdorNo);
            tLPInsureAccClassSchema.setEdorType(mEdorType);
            tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
            tLPInsureAccClassSchema.setModifyTime(mCurrentTime);

            mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
            

            LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
            LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
            tLCInsureAccFeeDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
            tLCInsureAccFeeDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccFeeDB.setRiskCode(tLCInsureAccSchema.getRiskCode());
            tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
            if (tLCInsureAccFeeSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "PEdorZBAppConfirmBL";
                tError.functionName = "setInsuredAcc";
                tError.errorMessage = "查询帐户管理费分类表数据失败!";
                mErrors.addOneError(tError);
                return false;
            }
            LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
            LCInsureAccFeeSchema tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1);
            ref.transFields(tLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
            tLPInsureAccFeeSchema.setEdorNo(mEdorNo);
            tLPInsureAccFeeSchema.setEdorType(mEdorType);
            tLPInsureAccFeeSchema.setFee(CommonBL.carry(tLPInsureAccFeeSchema.getFee() - riskPrem- riskPremZJ));
            tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
            tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
            LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new
                    LCInsureAccClassFeeSet();
            LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new
                    LCInsureAccClassFeeDB();
            tLCInsureAccClassFeeDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
            tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
            if (tLCInsureAccClassFeeSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "PEdorZBAppConfirmBL";
                tError.functionName = "setInsuredAcc";
                tError.errorMessage = "查询帐户分类表数据失败!";
                mErrors.addOneError(tError);
                return false;
            }
            LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new
                    LCInsureAccClassFeeSchema();
            LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new
                    LPInsureAccClassFeeSchema();
            tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(1);
            ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
            tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
            tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
            tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
            tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
            tLPInsureAccClassFeeSchema.setFee(CommonBL.carry(tLPInsureAccClassFeeSchema.getFee() -
                                              riskPrem- riskPremZJ));
            mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
            System.out.println("即将进入变更的地方");
//          万能重疾的轨迹
            System.out.println(mLCInsureAccClassSchema.getPolNo()); 
            this.setLPInsureAccTrace(mLCInsureAccClassSchema, riskPrem, "RP");
            this.setLPInsureAccTrace(mLCInsureAccClassSchema, -riskPrem, "BF"); //moneytype:CM 代表万能客户资料变更风险保费补退费金额
            this.setLPInsureAccFeeTrace(mLCInsureAccClassSchema, -riskPrem, 0,
                                        "000003", "RP");
        }else {
            Reflections ref = new Reflections();
            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            tLCInsureAccDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccDB.setInsuAccNo(CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,
                    cLPPolSchema.getRiskCode()));
            if (!tLCInsureAccDB.getInfo())
            {
                mErrors.addOneError("查询帐户表失败!");
                return false;
            }
            LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
            LPInsureAccSchema tLPInsureAccSchema = new LPInsureAccSchema();
            ref.transFields(tLPInsureAccSchema, tLCInsureAccSchema);
            tLPInsureAccSchema.setEdorNo(mEdorNo);
            tLPInsureAccSchema.setEdorType(mEdorType);
            tLPInsureAccSchema.setModifyDate(this.mCurrentDate);
            tLPInsureAccSchema.setModifyTime(this.mCurrentTime);
            tLPInsureAccSchema.setInsuAccBala(CommonBL.carry(tLPInsureAccSchema.getInsuAccBala() +
                                              riskPrem+riskPremZJ));
            mMap.put(tLPInsureAccSchema, "DELETE&INSERT");
            LPInsureAccClassSchema tLPInsureAccClassSchema = new
                    LPInsureAccClassSchema();
            ref.transFields(tLPInsureAccClassSchema, mLCInsureAccClassSchema);
            tLPInsureAccClassSchema.setEdorNo(mEdorNo);
            tLPInsureAccClassSchema.setEdorType(mEdorType);
            tLPInsureAccClassSchema.setModifyDate(mCurrentDate);
            tLPInsureAccClassSchema.setModifyTime(mCurrentTime);
            tLPInsureAccClassSchema.setInsuAccBala(CommonBL.carry(tLPInsureAccClassSchema.
                                                   getInsuAccBala() + riskPrem+riskPremZJ));
            mMap.put(tLPInsureAccClassSchema, "DELETE&INSERT");
            

            LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
            LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
            tLCInsureAccFeeDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
            tLCInsureAccFeeDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccFeeDB.setRiskCode(tLCInsureAccSchema.getRiskCode());
            tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
            if (tLCInsureAccFeeSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "PEdorZBAppConfirmBL";
                tError.functionName = "setInsuredAcc";
                tError.errorMessage = "查询帐户管理费分类表数据失败!";
                mErrors.addOneError(tError);
                return false;
            }
            LPInsureAccFeeSchema tLPInsureAccFeeSchema = new LPInsureAccFeeSchema();
            LCInsureAccFeeSchema tLCInsureAccFeeSchema = tLCInsureAccFeeSet.get(1);
            ref.transFields(tLPInsureAccFeeSchema, tLCInsureAccFeeSchema);
            tLPInsureAccFeeSchema.setEdorNo(mEdorNo);
            tLPInsureAccFeeSchema.setEdorType(mEdorType);
            tLPInsureAccFeeSchema.setFee(CommonBL.carry(tLPInsureAccFeeSchema.getFee() - riskPrem- riskPremZJ));
            tLPInsureAccFeeSchema.setModifyDate(mCurrentDate);
            tLPInsureAccFeeSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPInsureAccFeeSchema, "DELETE&INSERT");
            LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new
                    LCInsureAccClassFeeSet();
            LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new
                    LCInsureAccClassFeeDB();
            tLCInsureAccClassFeeDB.setPolNo(cLPPolSchema.getPolNo());
            tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
            tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
            if (tLCInsureAccClassFeeSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "PEdorZBAppConfirmBL";
                tError.functionName = "setInsuredAcc";
                tError.errorMessage = "查询帐户分类表数据失败!";
                mErrors.addOneError(tError);
                return false;
            }
            LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new
                    LCInsureAccClassFeeSchema();
            LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new
                    LPInsureAccClassFeeSchema();
            tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(1);
            ref.transFields(tLPInsureAccClassFeeSchema, tLCInsureAccClassFeeSchema);
            tLPInsureAccClassFeeSchema.setEdorNo(mEdorNo);
            tLPInsureAccClassFeeSchema.setEdorType(mEdorType);
            tLPInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
            tLPInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
            tLPInsureAccClassFeeSchema.setFee(CommonBL.carry(tLPInsureAccClassFeeSchema.getFee() -
                                              riskPrem- riskPremZJ));
            mMap.put(tLPInsureAccClassFeeSchema, "DELETE&INSERT");
            System.out.println("即将进入变更的地方");
//          万能重疾的轨迹
            System.out.println(mLCInsureAccClassSchema.getPolNo()); 
            if(riskPrem!=0){
                this.setLPInsureAccTrace(mLCInsureAccClassSchema, riskPrem, "CM"); //moneytype:CM 代表万能客户资料变更风险保费补退费金额
                this.setLPInsureAccFeeTrace(mLCInsureAccClassSchema, -riskPrem, 0,
                                            "000003", "CM");
            }
//              万能重疾的轨迹
            if(riskPremZJ!=0){
                this.setLPInsureAccTrace(mLCInsureAccClassSchemaZJ, riskPremZJ, "CM"); //moneytype:CM 代表万能客户资料变更风险保费补退费金额
                this.setLPInsureAccFeeTrace(mLCInsureAccClassSchemaZJ, -riskPremZJ, 0,
                        "000012", "CM");
            }

        }

        return true;

    }    
    
    /**
     * 个险万能累计已扣风险保费合计
     * */
    private double getRiskPrem1(String cContNo)
    {
        double prem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        //累计已扣风险保额
        String tSql =
                "select value(sum(money),0) from lcinsureacctrace where contno='" +
                cContNo + "' and moneytype in('RP','CM')" +
                //过滤对附加重疾的相关统计 20110623 by xp
                		" and payplancode not in ('231001') ";
        try
        {
            prem = Double.parseDouble(tExeSQL.getOneValue(tSql));
        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
            prem = 1;
        }

        return prem;

    }
    //税优保单处理
    private boolean dealTPHICont(LPPolSchema cLPPolSchema){
        riskPrem = 0.0;
        //累计已扣风险保费
        double riskPrem1 = 0.0;
        //IC变更后应扣风险保费
        double riskPrem2 = 0.0;
        String edorvalidate=mLPEdorItemSchema.getEdorValiDate();
        
        try
        {
            LCPolSchema tLCPolSchema = CommonBL.getLCPol(cLPPolSchema.getPolNo());
            if (tLCPolSchema == null)
            {
                mErrors.addOneError("未找到保单号" + tLCPolSchema.getContNo()
                                    + "险种" + tLCPolSchema.getRiskCode() + "信息!");
                return false;
            }
            
            int tInsuredAge = PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),
					 tLCPolSchema.getPolApplyDate(), "Y");
            String  tSql ="select distinct insuredsex,insuredbirthday from lppol where edorno='" +
            		mEdorNo + "' and edortype='CM' and polno='"+cLPPolSchema.getPolNo()+"'";
            SSRS tSSRS = new ExeSQL().execSQL(tSql);
            String tInsuSexP = tSSRS.GetText(1, 1);
            String tInsuBirthDayP = tSSRS.GetText(1, 2);
            int tInsuredAgeP = PubFun.calInterval(tInsuBirthDayP,
  					 tLCPolSchema.getPolApplyDate(), "Y");

            //根据保全生效日期计算
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            tLJAPayPersonDB.setPolNo(tLCPolSchema.getPolNo());
            tLJAPayPersonDB.setPayType("ZC");
            LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
            //累计实际已交保费(计算到)
            for(int j=1;j<=tLJAPayPersonSet.size();j++){
            	riskPrem1 = 0.0;
            	riskPrem2 = 0.0;
            
            	Date tempCurPaytoDate=CommonBL.stringToDate(tLJAPayPersonSet.get(j).getCurPayToDate());
            	Date tempLastPaytoDate=CommonBL.stringToDate(tLJAPayPersonSet.get(j).getLastPayToDate());
			 	Date tPaytoDate=CommonBL.stringToDate(tLCPolSchema.getPaytoDate());
			 	Date tCValiDate=CommonBL.stringToDate(tLCPolSchema.getCValiDate());
			 	Date tEdorvalidate=CommonBL.stringToDate(edorvalidate);

			 	if(!tCValiDate.after(tempLastPaytoDate)){//生效日期在上次交至日期之前，在当前保单年度
			 		riskPrem1 =riskPrem1 + getRiskPremSY(cLPPolSchema.getPolNo(),tLCPolSchema.getPayIntv(),tInsuredAge,tLCPolSchema.getInsuredSex());
			 		riskPrem2 =riskPrem2 + getRiskPremSY(cLPPolSchema.getPolNo(),tLCPolSchema.getPayIntv(),tInsuredAgeP,tInsuSexP);
                   if (riskPrem1 == 1 )
                   {
                       mErrors.addOneError("查询累计已扣风险保费失败!");
                       return false;
                   }
                   if (riskPrem2 == 1)
                   {
                 	    mErrors.addOneError("计算应扣风险保费失败!");
                       return false;
                   }
			 		if(tempCurPaytoDate.before(tEdorvalidate)){
				 		continue;
				 	}else if(tempCurPaytoDate.after(tEdorvalidate)&&tempLastPaytoDate.before(tEdorvalidate)){
				 		riskPrem = riskPrem + CommonBL.carry(CommonBL.carry(riskPrem2 - riskPrem1) * PubFun.calInterval(tEdorvalidate, tempCurPaytoDate, "D") / PubFun.calInterval(tempLastPaytoDate, tempCurPaytoDate, "D"));
                }else if(tempLastPaytoDate.after(tEdorvalidate)){
                	riskPrem = riskPrem + CommonBL.carry(riskPrem2 - riskPrem1);
                }
			 }else{//生效日期在上次交至日期之后，或者相等，保单年度之外风险保费算整个缴费期间的
//                tSql ="select distinct polapplydate from lbpol where  edorno like 'xb%' and polno='"+cLPPolSchema.getPolNo()+"'" +
//                	  " and prtno='"+tLCPolSchema.getPrtNo()+"' and paytodate='"+tempCurPaytoDate+"'   ";
//                String  polapplydate = new ExeSQL().getOneValue(tSql);
//		 		riskPrem1=riskPrem1 + getRiskPremSY(cLPPolSchema.getPolNo(),tLCPolSchema.getPayIntv(),tInsuredAge,tLCPolSchema.getInsuredSex());
//			 	riskPrem2=riskPrem2 + getRiskPremSY(cLPPolSchema.getPolNo(),tLCPolSchema.getPayIntv(),tInsuredAgeP,tInsuSexP);
//                if (riskPrem1 == 1 )
//                {
//                    mErrors.addOneError("查询累计已扣风险保费失败!");
//                    return false;
//                 }
//                if (riskPrem2 == 1)
//                {
//                	mErrors.addOneError("计算应扣风险保费失败!");
//                    return false;
//                 }
//                if(polapplydate!=null&&!"".equals(polapplydate)){
//                    tInsuredAge = PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),polapplydate, "Y");
//                    tInsuredAgeP = PubFun.calInterval(tInsuBirthDayP,polapplydate, "Y");
//   				 	if(tempCurPaytoDate.before(tEdorvalidate)){
//   				 		continue;
//   				 	}else if(tempCurPaytoDate.after(tEdorvalidate)&&tempLastPaytoDate.before(tEdorvalidate)){
//   				 		riskPrem = riskPrem + CommonBL.carry(CommonBL.carry(riskPrem2 - riskPrem1) * PubFun.calInterval(tempCurPaytoDate, tEdorvalidate, "D") /PubFun.calInterval(tempCurPaytoDate, tempLastPaytoDate, "D"));
//   				 		System.out.println( PubFun.calInterval(tempCurPaytoDate, tEdorvalidate, "D") /PubFun.calInterval(tempCurPaytoDate, tempLastPaytoDate, "D"));
//                    }else if(tempLastPaytoDate.after(tEdorvalidate)){
//                    	riskPrem = riskPrem + CommonBL.carry(riskPrem2 - riskPrem1);
//                    }
//                }else{
//                 	    mErrors.addOneError("计算应扣风险保费失败!");
//                       return false;
//                }     
				 
        	}
		 }

            System.out.println("riskprem  :   " + riskPrem);
            /*
             * ------------------万能客户资料扣费计算结束部分--------------------------
             */
            //税优保单风险保费变更后，保费不能小于风险保费
            riskPrem2=getRiskPremSY(cLPPolSchema.getPolNo(),tLCPolSchema.getPayIntv(),tInsuredAgeP,tInsuSexP);

            double oldPrem = tLCPolSchema.getPrem();
            double newPrem = -riskPrem2;
            double getMoney = -riskPrem;
            System.out.println(oldPrem);
            System.out.println(newPrem);
            System.out.println(newPrem>oldPrem);
            if(newPrem>oldPrem){
                mChgPrem += newPrem - oldPrem;
                setLPTablesTPHI(newPrem,oldPrem);
            }
            mGetMoney += getMoney;



        } catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        
        return true;
    }
    /**
     * 个险万能累计已扣风险保费合计
     * 附加重疾部分
     * */
    private double getRiskPrem1ZJ(String cContNo)
    {
        double prem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        //累计已扣风险保额
        String tSql =
                "select value(sum(money),0) from lcinsureacctrace where contno='" +
                cContNo + "' and moneytype in('RP','CM')" +
                //添加对附加重疾的相关统计 20110623 by xp
                		" and payplancode in ('231001') ";
        try
        {
            prem = Double.parseDouble(tExeSQL.getOneValue(tSql));
        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
            prem = 1;
        }

        return prem;

    }

    /**
     * 个险万能累计应扣风险保费合计
     * */
    private double getRiskPrem2(String cContNo)
    {
        double prem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        String tSql = "";
        //保单生效日
        String tCvalidate;
        //最后一次结算日
        String endBalaDate;
        //上次结算日
        String lastBalaDate;
        //险种编码
        String tRiskCode;
        //帐户号码
        String tInsuAccNo;
        //变更后生日
        String tInsuBirthDay;
        //变更后性别
        String tInsuSex;
        //每次结算时被保险年龄
        int tInsuredAge;
        //保额
        double tAmnt = 0.0;
        String payPlanCode;

        LMInsuAccRateSet tLMInsuAccRateSet = new LMInsuAccRateSet();
        LMInsuAccRateDB tLMInsuAccRateDB = new LMInsuAccRateDB();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        SSRS tSSRS = new SSRS();

        try
        {
            tSql = "select * from lcpol a, lmriskapp b where a.contno='" + cContNo +
                   "' and a.polno=a.mainPolno and a.riskcode = b.riskcode " 
                 + "and b.risktype4 = '4' ";
            tLCPolSchema = tLCPolDB.executeQuery(tSql).get(1);
            tCvalidate = tLCPolSchema.getCValiDate();
            tRiskCode = tLCPolSchema.getRiskCode();
            tAmnt = tLCPolSchema.getAmnt();
            
            
            tInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED, tRiskCode);
            tSql =
                    "select distinct insuredsex,insuredbirthday from lppol where edorno='" +
                    mEdorNo + "' and edortype='CM'";
            tSSRS = tExeSQL.execSQL(tSql);
            tInsuSex = tSSRS.GetText(1, 1);
            tInsuBirthDay = tSSRS.GetText(1, 2);
            tSql = "select baladate from lcinsureacc where contno='" + cContNo +
                   "' and insuaccno='" + tInsuAccNo + "'";
            endBalaDate = tExeSQL.getOneValue(tSql);
//    		tSql="select (date('"+tCvalidate+"') + 1 months)-(day(date('"+tCvalidate+"')) -1) days from dual";
            //首次的上次结算日是保单生效日
            lastBalaDate = tCvalidate;
//    		tBalaCount=PubFun.calInterval(tFirstBalaDate, tBalaDate, "M")+1;

            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
            tLCInsureAccClassDB.setPolNo(tLCPolSchema.getPolNo());
            tLCInsureAccClassDB.setInsuAccNo(tInsuAccNo);
            LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.
                    query();
            if(tLCInsureAccClassSet.size()!=1){
            	// @@错误处理
				System.out.println("PEdorICAppConfirmBL+getRiskPrem2++--");
				CError tError = new CError();
				tError.moduleName = "PEdorICAppConfirmBL";
				tError.functionName = "getRiskPrem2";
				tError.errorMessage = "获取万能账户信息失败!";
				this.mErrors.addOneError(tError);
				return 1;
            }
            mLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);
            payPlanCode = tLCInsureAccClassSet.get(1).getPayPlanCode();
            tSql = " select a.* from LMInsuAccRate a "
                   + "where 1 = 1 "
                   + "   and RateType = 'C' "
                   + "   and RateIntv = 1 "
                   + "   and RateIntvUnit = 'Y' "
                   + "   and a.BalaDate > '" + tCvalidate +
                   "' and a.BalaDate<='" + endBalaDate + "' "
                   + "   and RiskCode = '" + tRiskCode + "' "
                   + "   and InsuAccNo = '"
                   + tInsuAccNo + "' "
                   + "order by a.BalaDate ";
            tLMInsuAccRateSet = tLMInsuAccRateDB.executeQuery(tSql);
            for (int i = 1; i <= tLMInsuAccRateSet.size(); i++)
            {
                tInsuredAge = PubFun.getInsuredAppAge(lastBalaDate,
                        tInsuBirthDay);
                LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
                tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
                tLMRiskFeeDB.setPayPlanCode(payPlanCode);
                tLMRiskFeeDB.setFeeCode("000003"); //feecode=000003为风险保费扣费
                tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
                tLMRiskFeeDB.setFeeKind("03"); //个单管理费
                LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
                
                Calculator tCalculator = new Calculator();
                //add by fengzhihang 
    	        //健康一生系列计算风险保费，需传入风险保额riskAmnt,
                String isJKYSSql = "select 1 from ldcode where codetype = 'jkys' and code = '" +tRiskCode + "'";
    	        String jsysResult = new ExeSQL().getOneValue(isJKYSSql);
    			if(jsysResult != null && jsysResult.equals("1")){
    					String sql1 = " select a.birthday,b.cvalidate from lcinsured a,lcpol b where a.contno=b.contno and  a.contno='" + mLCInsureAccClassSchema.getContNo()
    					+ "' and a.insuredno='"+mLCInsureAccClassSchema.getInsuredNo()+ "' and b.riskcode ='"+mLCInsureAccClassSchema.getRiskCode()+ "' ";
//    				String tBirthday = new ExeSQL().getOneValue(sql1);
    				SSRS mSSRS1 = new ExeSQL().execSQL(sql1);
    				if(mSSRS1.MaxRow == 0){
    					mErrors.addOneError("获取被保人生日信息失败！");
    				}
    				else 
    				{
    					FDate tFDate= new FDate();
    					String getMidDate=PubFun.calDate(mSSRS1.GetText(1, 1), 61, "Y", mSSRS1.GetText(1, 2));

    				    tSql="select a.duebaladate,a.insuaccbalaafter from LCInsureAccBalance a,lcinsureaccclass b where a.polno=b.polno and  a.polno='"+mLCInsureAccClassSchema.getPolNo()+"'" 
    						    	+" and a.duebaladate=b.baladate ";
    					SSRS accSSRS = new ExeSQL().execSQL(tSql);
    					System.out.println(tSql);
    					if(accSSRS.MaxRow == 0){
    						mErrors.addOneError("获取账户月结信息失败！");
    		                return 1;
    					}
    					
    					String mAccSql="select sum(money) from lcinsureacctrace where contno='"+mLCInsureAccClassSchema.getContNo()+"'   and paydate<'"+tLMInsuAccRateSet.get(i).getBalaDate()+"'  ";
    					String mBFSql="select sum(money) from lcinsureacctrace where contno='"+mLCInsureAccClassSchema.getContNo()+"' and moneytype in ('ZF','BF','LQ')  and paydate<'"+tLMInsuAccRateSet.get(i).getBalaDate()+"'  ";
    					String mGLSql="select sum(fee) from lcinsureaccfeetrace where contno='"+mLCInsureAccClassSchema.getContNo()+"' and moneytype='GL' and othertype='1'";
    					//缴纳的保费和追加保费-部分领取的金额 (实收的保费，所以要算上管理费)
    					double BFMoney = PubFun.setPrecision(
    							Double.valueOf(new ExeSQL().getOneValue(mBFSql)) 
    									+ Double.valueOf(new ExeSQL().getOneValue(mGLSql)),"0.00");
    					double accMoney = PubFun.setPrecision(Double.valueOf(new ExeSQL().getOneValue(mAccSql)),"0.00");//个人账户余额
    					//保险金额：     61岁的保单周年日（不含）之前  
    					//1)	160%×（交纳的保险费-累计已领取的“部分领取”金额）；
    					//2)	个人账户价值。
    					// 61岁的保单周年日（含 ）之后
    					//1)	120%×（交纳的保险费-累计已领取的“部分领取”金额）；
    					//2)	个人账户价值。
    					//风险保额是指本合同的保险金额与个人账户价值的差额，差额为零时不收取风险保险费
    					double minMoney=0;//差
    					double riskAmnt=0;//风险保额
    					if(tFDate.getDate(lastBalaDate).compareTo(tFDate.getDate(getMidDate))==-1){
    						BFMoney=BFMoney*1.6;
    						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
    					}
    					else 
    					{
    						BFMoney=BFMoney*1.2;
    						minMoney=PubFun.setPrecision(BFMoney - accMoney, "0.00");
    					}
    					if(minMoney < 0){
    						riskAmnt = 0;
    					}else{
        					riskAmnt=Math.abs(minMoney);
    					}
    					tCalculator.addBasicFactor("RiskAmnt", String.valueOf(riskAmnt));
    					
    				}
    	        	
    	        }

                tCalculator.setCalCode(tLMRiskFeeSet.get(1).getFeeCalCode());
                tCalculator.addBasicFactor("Amnt", "" + tAmnt);
                tCalculator.addBasicFactor("InsuredAppAge", "" + tInsuredAge);
                tCalculator.addBasicFactor("Sex", tInsuSex);
                tCalculator.addBasicFactor("StartDate", lastBalaDate);
                tCalculator.addBasicFactor("EndDate",
                                           tLMInsuAccRateSet.get(i).getBalaDate());
                tCalculator.addBasicFactor("LastBala",new ExeSQL().getOneValue("select sum(money) From lcinsureacctrace where contno ='"+cContNo+"'and insuaccno='"+tInsuAccNo+"' and paydate < '"+ tLMInsuAccRateSet.get(i).getBalaDate() + "' "));
                tCalculator.addBasicFactor("PolNo",tLCPolSchema.getPolNo());
                double fee = Math.abs(PubFun.setPrecision(
                        Double.parseDouble(tCalculator.calculate()), "0.00"));
                if (tCalculator.mErrors.needDealError())
                {
                    System.out.println(tCalculator.mErrors.getErrContent());
                    mErrors.addOneError("计算管理费出错");
                    return 1;
                }
                prem += fee;
                lastBalaDate = tLMInsuAccRateSet.get(i).getBalaDate();
            }
        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
            return 1;
        }
        return -prem;
    }
    /**
     * 税优计算风险保费
     * */
    private double getRiskPremSY(String aPolno,int payIntv,int aInsuredAge,String aInsuSex)
    {  
    	double prem=0.0;
        String tInsuSex=aInsuSex;
        int tInsuredAge=aInsuredAge;
        int tpayIntv=payIntv;
        SSRS tSSRS = new SSRS();
        try
        {   

            
			String strSQL="select insuaccno from lcinsureacc  where  polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取账户信息失败！");
                return 1;
			}
			
            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
            tLCInsureAccClassDB.setPolNo(aPolno);
            tLCInsureAccClassDB.setInsuAccNo(tSSRS.GetText(1, 1));
            LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.
                    query();
            if(tLCInsureAccClassSet.size()!=1){
            	// @@错误处理
				System.out.println("PEdorICAppConfirmBL+getRiskPrem2++--");
				CError tError = new CError();
				tError.moduleName = "PEdorICAppConfirmBL";
				tError.functionName = "getRiskPrem2";
				tError.errorMessage = "获取万能账户信息失败!";
				this.mErrors.addOneError(tError);
				return 1;
            }
            mLCInsureAccClassSchema = tLCInsureAccClassSet.get(1);
 
        	//税优风险保费计算
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(tSSRS.GetText(1, 1));
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("02");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			if (tLMRiskFeeDB.mErrors.needDealError()) 
			{
				CError.buildErr(this, "账户管理费查询失败!");
				return 1;
			}
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(1).getFeeCalCode());
            tCalculator.addBasicFactor("Sex", tInsuSex);
            tCalculator.addBasicFactor("PolNo",aPolno);
			tCalculator.addBasicFactor("PayIntv",""+ tpayIntv);
			tCalculator.addBasicFactor("AppAge",""+ tInsuredAge);
            
			strSQL="select a.payintv,a.insuredappage,b.totaldiscountfactor from lcpol a,lccontsub b where a.prtno=b.prtno and a.polno='"+ aPolno +"' ";
		    tSSRS = new ExeSQL().execSQL(strSQL);
			if(tSSRS.MaxRow == 0){
				mErrors.addOneError("获取保单详细信息失败！");
                return 1;
			}
			//增加折扣因子
			if(null != tSSRS.GetText(1, 3) && !"".equals(tSSRS.GetText(1, 3))){
				tCalculator.addBasicFactor("Totaldiscountfactor", tSSRS.GetText(1, 3));
			}else
			{
				tCalculator.addBasicFactor("Totaldiscountfactor", "1");
			}
			
            double fee = Math.abs(PubFun.setPrecision(
                        Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError())
            {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return 1;
            }
            prem += fee;

        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
            return 1;
        }
        return -prem;
    }

    /**
     * 个险万能累计应扣风险保费合计
     * */
    private double getRiskPrem2ZJ(String cContNo)
    {
        double prem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        String tSql = "";
        //保单生效日
        String tCvalidate;
        //最后一次结算日
        String endBalaDate;
        //上次结算日
        String lastBalaDate;
        //险种编码
        String tRiskCode;
        //帐户号码
        String tInsuAccNo;
        //变更后生日
        String tInsuBirthDay;
        //变更后性别
        String tInsuSex;
        //每次结算时被保险年龄
        int tInsuredAge;
        //保额
        double tAmnt = 0.0;
        String payPlanCode;

        LMInsuAccRateSet tLMInsuAccRateSet = new LMInsuAccRateSet();
        LMInsuAccRateDB tLMInsuAccRateDB = new LMInsuAccRateDB();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        SSRS tSSRS = new SSRS();

        try
        {
            tSql = "select * from lcpol a, lmriskapp b where a.contno='" + cContNo +
                   "' and a.polno=a.mainPolno and a.riskcode = b.riskcode " 
                 + "and b.risktype4 = '4' ";
            tLCPolSchema = tLCPolDB.executeQuery(tSql).get(1);
            tCvalidate = tLCPolSchema.getCValiDate();
            tRiskCode = tLCPolSchema.getRiskCode();
            tAmnt = tLCPolSchema.getAmnt();
            tInsuAccNo = CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED, tRiskCode);
            tSql =
                    "select distinct insuredsex,insuredbirthday from lppol where edorno='" +
                    mEdorNo + "' and edortype='CM'";
            tSSRS = tExeSQL.execSQL(tSql);
            tInsuSex = tSSRS.GetText(1, 1);
            tInsuBirthDay = tSSRS.GetText(1, 2);
            tSql = "select baladate from lcinsureacc where contno='" + cContNo +
                   "' and insuaccno='" + tInsuAccNo + "'";
            endBalaDate = tExeSQL.getOneValue(tSql);
            //首次的上次结算日是保单生效日
            lastBalaDate = tCvalidate;

//          设置万能重疾的Schema
            mLCInsureAccClassSchemaZJ.setSchema(mLCInsureAccClassSchema);
            //这块先写死 20110627 by xp
            mLCInsureAccClassSchemaZJ.setPayPlanCode("231001");
            payPlanCode = mLCInsureAccClassSchema.getPayPlanCode();
            tSql = " select a.* from LMInsuAccRate a "
                   + "where 1 = 1 "
                   + "   and RateType = 'C' "
                   + "   and RateIntv = 1 "
                   + "   and RateIntvUnit = 'Y' "
                   + "   and a.BalaDate > '" + tCvalidate +
                   "' and a.BalaDate<='" + endBalaDate + "' "
                   + "   and RiskCode = '" + tRiskCode + "' "
                   + "   and InsuAccNo = '"
                   + tInsuAccNo + "' "
                   + "order by a.BalaDate ";
            tLMInsuAccRateSet = tLMInsuAccRateDB.executeQuery(tSql);
            for (int i = 1; i <= tLMInsuAccRateSet.size(); i++)
            {
                tInsuredAge = PubFun.getInsuredAppAge(lastBalaDate,
                        tInsuBirthDay);
                LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
                tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
                tLMRiskFeeDB.setPayPlanCode(payPlanCode);
                tLMRiskFeeDB.setFeeCode("000012"); //feecode=000012为附加重疾风险保费扣费
                tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
                tLMRiskFeeDB.setFeeKind("03"); //个单管理费
                LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();

                if(tLMRiskFeeSet.size()!=1){
                	// @@错误处理
					System.out.println("PEdorICAppConfirmBL+getRiskPrem2ZJ++--");
					CError tError = new CError();
					tError.moduleName = "PEdorICAppConfirmBL";
					tError.functionName = "getRiskPrem2ZJ";
					tError.errorMessage = "获取费用计算信息表失败!";
					this.mErrors.addOneError(tError);
					return 1;
                }
                
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(mLCInsureAccClassSchemaZJ.getContNo());
				subLCPolDB.setMainPolNo(mLCInsureAccClassSchemaZJ.getPolNo());
				subLCPolDB.setRiskCode(tLMRiskFeeSet.get(1).getFeeNoti());
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()!=1){
					// @@错误处理
					System.out.println("PEdorICAppConfirmBL+getRiskPrem2ZJ++--");
					CError tError = new CError();
					tError.moduleName = "PEdorICAppConfirmBL";
					tError.functionName = "getRiskPrem2ZJ";
					tError.errorMessage = "获取附加重疾险种信息失败!";
					this.mErrors.addOneError(tError);
					return 1;
				}
				LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
				String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
				String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
				+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
				String tRiskRate = new ExeSQL().getOneValue(sql);
                
                Calculator tCalculator = new Calculator();
                tCalculator.setCalCode(tLMRiskFeeSet.get(1).getFeeCalCode());
				if(tRiskRate==null || tRiskRate.equals(""))
				{
					tCalculator.addBasicFactor("RiskRate", "0");
				}
				else
				{
					tCalculator.addBasicFactor("RiskRate", tRiskRate);
				}
                tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
                tCalculator.addBasicFactor("Amnt", "" + tAmnt);
                tCalculator.addBasicFactor("InsuredAppAge", "" + tInsuredAge);
                tCalculator.addBasicFactor("Sex", tInsuSex);
                tCalculator.addBasicFactor("StartDate", lastBalaDate);
                tCalculator.addBasicFactor("EndDate",
                                           tLMInsuAccRateSet.get(i).getBalaDate());
                tCalculator.addBasicFactor("LastBala",new ExeSQL().getOneValue("select sum(money) From lcinsureacctrace where contno ='"+cContNo+"'and insuaccno='"+tInsuAccNo+"' and paydate < '"+ tLMInsuAccRateSet.get(i).getBalaDate() + "' "));
                tCalculator.addBasicFactor("PolNo",tLCPolSchema.getPolNo());
                double fee = Math.abs(PubFun.setPrecision(
                        Double.parseDouble(tCalculator.calculate()), "0.00"));
                if (tCalculator.mErrors.needDealError())
                {
                    System.out.println(tCalculator.mErrors.getErrContent());
                    mErrors.addOneError("计算管理费出错");
                    return 1;
                }
                prem += fee;
                lastBalaDate = tLMInsuAccRateSet.get(i).getBalaDate();
            }
        } catch (Exception e)
        {
            //累计风险保费扣费一定小于0，如果返回1证明查询累计风险保费扣费失败
        	e.printStackTrace();
            return 1;
        }
        return -prem;
    }

    /**
     * 设置帐户领取轨迹
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param grpMoney double
     */
    private void setLPInsureAccTrace(LCInsureAccClassSchema
                                     aLCInsureAccClassSchema, double grpMoney,
                                     String MoneyType)
    {
        String serialNo = PubFun1.CreateMaxNo("SERIALNO",
                                       aLCInsureAccClassSchema.getManageCom());

        LPInsureAccTraceSchema tLPInsureAccTraceSchema = new
                LPInsureAccTraceSchema();
        tLPInsureAccTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccTraceSchema.setEdorType(mEdorType);
        tLPInsureAccTraceSchema.setGrpContNo(aLCInsureAccClassSchema.
                                             getGrpContNo());
        tLPInsureAccTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.getGrpPolNo());
        tLPInsureAccTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccTraceSchema.setSerialNo(serialNo);
        tLPInsureAccTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.
                                             getInsuAccNo());
        tLPInsureAccTraceSchema.setRiskCode(aLCInsureAccClassSchema.getRiskCode());
        tLPInsureAccTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.
                                               getPayPlanCode());
        tLPInsureAccTraceSchema.setManageCom(aLCInsureAccClassSchema.
                                             getManageCom());
        tLPInsureAccTraceSchema.setAccAscription("0");
        tLPInsureAccTraceSchema.setMoneyType(MoneyType);
        tLPInsureAccTraceSchema.setMoney(grpMoney);
        tLPInsureAccTraceSchema.setUnitCount("0");
        tLPInsureAccTraceSchema.setPayDate(mLPEdorItemSchema.getEdorValiDate());
        tLPInsureAccTraceSchema.setState("0");
        tLPInsureAccTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccTraceSchema.setOtherType("10");
        tLPInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccTraceSchema.setMakeDate(this.mCurrentDate);
        tLPInsureAccTraceSchema.setMakeTime(this.mCurrentTime);
        tLPInsureAccTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccTraceSchema, "DELETE&INSERT");
    }

    private void setLPInsureAccFeeTrace(LCInsureAccClassSchema
                                        aLCInsureAccClassSchema,
                                        double manageMoney, double feeRate,
                                        String tFeeCode, String tMoneyType)
    {
        String serialNo = PubFun1.CreateMaxNo("SERIALNO",
                                       aLCInsureAccClassSchema.getManageCom());

        LPInsureAccFeeTraceSchema tLPInsureAccFeeTraceSchema = new
                LPInsureAccFeeTraceSchema();
        tLPInsureAccFeeTraceSchema.setEdorNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setEdorType(mEdorType);
        tLPInsureAccFeeTraceSchema.setGrpContNo(aLCInsureAccClassSchema.
                                                getGrpContNo());
        tLPInsureAccFeeTraceSchema.setGrpPolNo(aLCInsureAccClassSchema.
                                               getGrpPolNo());
        tLPInsureAccFeeTraceSchema.setContNo(aLCInsureAccClassSchema.getContNo());
        tLPInsureAccFeeTraceSchema.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLPInsureAccFeeTraceSchema.setSerialNo(serialNo);
        tLPInsureAccFeeTraceSchema.setInsuAccNo(aLCInsureAccClassSchema.
                                                getInsuAccNo());
        tLPInsureAccFeeTraceSchema.setRiskCode(aLCInsureAccClassSchema.
                                               getRiskCode());
        tLPInsureAccFeeTraceSchema.setOtherNo(mEdorNo);
        tLPInsureAccFeeTraceSchema.setOtherType("10");
        tLPInsureAccFeeTraceSchema.setPayPlanCode(aLCInsureAccClassSchema.
                                                  getPayPlanCode());
        tLPInsureAccFeeTraceSchema.setAccAscription("0");
        tLPInsureAccFeeTraceSchema.setMoneyType(tMoneyType);
        tLPInsureAccFeeTraceSchema.setPayDate(this.mLPEdorItemSchema.
                                              getEdorValiDate());
        tLPInsureAccFeeTraceSchema.setState("0");
        tLPInsureAccFeeTraceSchema.setManageCom(aLCInsureAccClassSchema.
                                                getManageCom());
        tLPInsureAccFeeTraceSchema.setFeeCode(tFeeCode);
//        tLPInsureAccFeeTraceSchema.setFeeRate(feeRate);
        tLPInsureAccFeeTraceSchema.setFee(manageMoney);
        tLPInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
        tLPInsureAccFeeTraceSchema.setMakeDate(this.mCurrentDate);
        tLPInsureAccFeeTraceSchema.setMakeTime(this.mCurrentTime);
        tLPInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
        tLPInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPInsureAccFeeTraceSchema, "DELETE&INSERT");
    }

    private boolean isMainPol(String polNo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tSql = "select count(*) from lcpol where contno='" + mContNo +
                      "' and mainpolno='" + polNo + "'";
        if (tExeSQL.getOneValue(tSql).equals("0"))
        {
            return false;
        }
        return true;
    }
    
    /**
     * 查询项目类型
     * @param customerNo String，客户号
     * @return String， 1：团险，0个险
     */
    private String getProjectType()
    {
        //查询客户号
        String sql = "select customerNo from LGWork where workNo='" + mLPEdorItemSchema.getEdorAcceptNo() + "' ";
        String customerNo = new ExeSQL().getOneValue(sql);
        if (customerNo.length() == 9)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }
    //税优保费变更
    private void setLPTablesTPHI(double aNewPrem,double aOldPrem){
    	
    	double chMoney=aNewPrem-aOldPrem;
    	double sumPrem=0;
        //税优风险保费变更后，保费也进行变化,税优按照 一个险种一个责任，
        LPDutySet tLPDutySet = null;
        LPPremSet tLPPremSet = null;
        String sql = "select '" + mLPEdorItemSchema.getEdorNo()
                     + "' EdorNo, '" + mLPEdorItemSchema.getEdorType()
                     + "' EdorType, a.* "
                     + "from LCDuty a "
                     + "where polNo = '"
                     + mLPEdorItemSchema.getPolNo() + "' ";
        LPDutyDB tLPDutyDB = new LPDutyDB();
        tLPDutySet = tLPDutyDB.executeQuery(sql);
        LPDutySchema tLPDutySchema = tLPDutySet.get(1);
        sumPrem=tLPDutySchema.getSumPrem() + chMoney;
        tLPDutySchema.setPrem(aNewPrem);
        tLPDutySchema.setStandPrem(aNewPrem);
        tLPDutySchema.setSumPrem(sumPrem);
        mMap.put(tLPDutySchema, "DELETE&INSERT");
        
        String sqlprem = "select '" + mLPEdorItemSchema.getEdorNo()
        			+ "' EdorNo, '" + mLPEdorItemSchema.getEdorType()
        			+ "' EdorType, a.* "
        			+ "from LCPrem a "
        			+ "where polNo = '"
        			+ mLPEdorItemSchema.getPolNo() + "' ";
        LPPremDB tLPPremDB = new LPPremDB();
        tLPPremSet = tLPPremDB.executeQuery(sqlprem);
        LPPremSchema tLPPremSchema = tLPPremSet.get(1);
        sumPrem=tLPPremSchema.getSumPrem() + chMoney;
        tLPPremSchema.setPrem(aNewPrem);
        tLPPremSchema.setStandPrem(aNewPrem);
        tLPPremSchema.setSumPrem(sumPrem);
        mMap.put(tLPPremSchema, "DELETE&INSERT");
        
        
//        String tUpdateLCprem = " update lpprem a set prem='"+aNewPrem+"',sumprem =sumprem +"+chMoney+",standprem= '"+aNewPrem+"' "
//                     		   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
//                               ;
        String tUpdateLCCont = " update lpcont a set   prem='"+aNewPrem+"',sumprem =sumprem +"+chMoney+" "
                               + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
                               ;
        String tUpdateLCpol = " update lppol a set   prem='"+aNewPrem+"',sumprem =sumprem +"+chMoney+",standprem= '"+aNewPrem+"'"
        					   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
        					   ;
//        String tUpdateLCduty = " update lpduty a set  prem='"+aNewPrem+"',sumprem =sumprem +"+chMoney+",standprem= '"+aNewPrem+"'"
//        					   + " WHERE a.EdorType = 'CM' and a.EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"'"
//        					   ;
        mMap.put(tUpdateLCCont,"UPDATE");
        mMap.put(tUpdateLCpol,"UPDATE");
//        mMap.put(tUpdateLCprem,"UPDATE");
//        mMap.put(tUpdateLCduty,"UPDATE");
    }
    
    
    public static void main(String[] args) {
		System.out.println(CommonBL.isULIRisk("531001"));
	}
}
