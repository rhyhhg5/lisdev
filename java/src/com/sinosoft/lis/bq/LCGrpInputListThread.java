package com.sinosoft.lis.bq;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

/**
 * 大团单被保人导入处理主线程控制类
 * 
 * @author
 */
public class LCGrpInputListThread implements Runnable {

	private String filePath;

	private TransferData mTransferData;

	private LCGrpLock lock;

	/**
	 * 构造函数
	 * 
	 * @param filePath 导入文件路径
	 * @param tTransferData
	 * @param lock 业务锁对象
	 */
	public LCGrpInputListThread(String filePath, TransferData tTransferData, LCGrpLock lock) {
		this.filePath = filePath;
		this.mTransferData = tTransferData;
		this.lock = lock;
	}

	public void run() {
		
		// 线程起始校验处理
		if (beginTread()) {
			try {
				long tStart2 = System.nanoTime();

				// 这段以后去掉 重置线程配置
				// LGrpThreadConfig.reload();

				LCDFService service = LCDFService.getService(filePath, mTransferData);
				service.startService();

				long tEnd2 = System.nanoTime();
				System.out.println("大团单导入被保人，耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
				
				
			} catch (RuntimeException e) {
				e.printStackTrace();
			} finally {
				System.out.println("");
				// 线程完毕处理
				finalThread();
			}
		}
	}
	
	/**
	 * 线程起始校验处理
	 * 
	 * @return
	 */
	public boolean beginTread() {
		System.out.println("----校验锁定-----");
		return lock.checklock();
	}
	
	/**
	 * 线程完毕处理
	 */
	public void finalThread() {
		System.out.println("----解除锁定-----");
		lock.unlock();
	}
}
