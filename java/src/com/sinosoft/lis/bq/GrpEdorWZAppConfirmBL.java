package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import java.util.HashMap;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理无名单增人保全理算
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GrpEdorWZAppConfirmBL implements EdorAppConfirm
{
    private VData mResult = null;

    private GlobalInput mGlobalInput = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    private double mContPremAdd = 0;  //个单总交退费
    private double mGrpContPremAdd = 0;  //团单总交退费
    private HashMap mPolAddPremMap = new HashMap();

    private MMap map = new MMap();

    public GrpEdorWZAppConfirmBL()
    {
    }

    /**
     * getResult
     * 得到处理后的结果集
     * @return VData
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public VData getResult()
    {
        mResult = new VData();
        mResult.add(map);

        return mResult;
    }

    /**
     * submitData
     * 操作的提交方法，外部接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 获取页面传入的数据
     * @param data VData：包括：
     * LPGrpEdorItemSchema: 团单项目信息
     * GlobalInput：操作员信息
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) data
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGlobalInput = (GlobalInput) data
                               .getObjectByObjectName("GlobalInput", 0);
        if(mLPGrpEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError("请传入保全项目信息");
            return false;
        }

        return true;
    }

    /**
     * 校验操作的合法性
     * 保全项目必须已录入明细
     * @return boolean: 合法true,否则false
     */
    private boolean checkData()
    {
//        if(!mLPGrpEdorItemSchema.getEdorState().equals(BQ.EDORSTATE_INPUT))
//        {
//            mErrors.addOneError("无名单" + mLPGrpEdorItemSchema.getEdorType()
//                                + "项目状态不为录入完毕，不能进行保全理算");
//            System.out.println(mLPGrpEdorItemSchema.getEdorNo() + ", "
//                               + mLPGrpEdorItemSchema.getEdorType() + ", "
//                               + mLPGrpEdorItemSchema.getEdorState());
//            return false;
//        }

        return true;
    }

    /**
     * 处理业务数据：
     * 得到所有增人信息-〉选择一个增人信息-〉处理该信息下保单临时数据
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        if(tLPEdorItemSet.size() == 0)
        {
            mErrors.addOneError("没有查询到无名单个单项目信息");
            return true;
        }

        boolean passFlag = true;
        for(int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            mContPremAdd = 0;
            if(!dealOneItem(tLPEdorItemSet.get(i)))
            {
                passFlag = false;
            }

            changeLPEdorInfo(tLPEdorItemSet.get(i));

            mGrpContPremAdd += mContPremAdd;
        }

        setfinance(mPolAddPremMap);

        mLPGrpEdorItemSchema.setGetMoney(mGrpContPremAdd);
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
        map.put(mLPGrpEdorItemSchema, "UPDATE");

        return passFlag;
    }

    private void changeLPEdorInfo(LPEdorItemSchema tLPEdorItemSchema)
    {
        tLPEdorItemSchema.setGetMoney(mContPremAdd);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_CAL);
        tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
        tLPEdorItemSchema.setModifyDate(this.mCurDate);
        tLPEdorItemSchema.setModifyTime(this.mCurTime);
        map.put(tLPEdorItemSchema, "UPDATE");

        map.put("update LPEdorMain "
                + "set getMoney = " + mContPremAdd
                + ",   edorState = '" + BQ.EDORSTATE_CAL
                + "',  operator = '" + this.mGlobalInput.Operator
                + "',  modifyDate = '" + this.mCurDate
                + "',  modifyTime = '" + this.mCurTime + "' "
                + "where edorNo = '" + tLPEdorItemSchema.getEdorNo()
                + "'  and contNo = '" + tLPEdorItemSchema.getContNo() + "' ",
                "UPDATE");
    }

    /**
     * 处理一个保障计划的增人信息
     * @param tLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean dealOneItem(LPEdorItemSchema tLPEdorItemSchema)
    {
        mLPEdorItemSchema = tLPEdorItemSchema.getSchema();

        //查询增人信息
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
        tLCInsuredListDB.setGrpContNo(tLPEdorItemSchema.getGrpContNo());
        tLCInsuredListDB.setContNo(tLPEdorItemSchema.getContNo());
        LCInsuredListSet tLCInsuredListSet = tLCInsuredListDB.query();
        if(tLCInsuredListSet.size() == 0)
        {
            mErrors.addOneError("没有查询到个单号为"
                                + tLPEdorItemSchema.getContNo() + "增人信息");
            return false;
        }

        for(int i = 1; i <= tLCInsuredListSet.size(); i++)
        {
            if(!dealContInfo(tLCInsuredListSet.get(i)))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 处理保单临时信息，按原保障计划下保费项保费比例分配费用
     * 1、处理交费项的加减费用：
     * 交费项分配的保费 = 本次录入保费 * （交费项交保费 / 无名单该保障保费）
     * 为消除误差，最后一个交费项的本次分批保费 = 本次录入保费 - 其他交费项分配保费
     * 2、维护责任和险种的prem、sumPrem字段，并生成财务信息LJSGetEndorse
     * 3、维护无名单期交保费Prem、人数Peoples和总保费SumPrem
     * @return boolean：成功true，否则false
     */
    private boolean dealContInfo(LCInsuredListSchema tLCInsuredListSchema)
    {
        //该保障计划录入的总保费
        double sumPremInput = Double.parseDouble(tLCInsuredListSchema.getEdorPrem());
        mContPremAdd += sumPremInput;

        if(!dealPrem(tLCInsuredListSchema, sumPremInput))
        {
            return false;
        }

        //以下变更应该在dealPrem之后
        //维护责任保费
        StringBuffer sql = new StringBuffer();
        sql.append("update LPDuty a ")
            .append("set (prem, sumPrem ) = ")
            .append("     (select sum(prem), sum(sumPrem) from LPPrem where edorNo = a.edorNo and edorType = a.edorType and polNo = a.polNo and dutyCode = a.dutyCode) ")
            .append(",  operator = '").append(this.mGlobalInput.Operator)
            .append("',  modifyDate = '").append(this.mCurDate)
            .append("',  modifyTime = '").append(this.mCurTime)
            .append("' where a.polNo in")
            .append("     (select polNo from LPPol ")
            .append("     where edorNo = '"
                    + mLPGrpEdorItemSchema.getEdorNo() + "' ")
            .append("        and edorType = '"
                    + mLPGrpEdorItemSchema.getEdorType() + "' ")
            .append("        and contNo = '"
                    + tLCInsuredListSchema.getContNo() + "' ")
            .append("        and contPlanCode = '"
                    + tLCInsuredListSchema.getContPlanCode() + "' ")
            .append("     )");
        map.put(sql.toString(), "UPDATE");

        //维护险种保费
        sql.delete(0, sql.length());
        sql.append("update LPPol a ")
            .append("set (prem, sumPrem) = ")
            .append("     (select sum(prem), sum(sumPrem) from LPDuty where edorNo = a.edorNo and edorType = a.edorType and polNo = a.polNo) ")
            .append(",  insuredPeoples = insuredPeoples + ")
            .append(tLCInsuredListSchema.getNoNamePeoples())
            .append(",  operator = '").append(this.mGlobalInput.Operator)
            .append("',  modifyDate = '").append(this.mCurDate)
            .append("',  modifyTime = '").append(this.mCurTime)
            .append("' where edorNo = '"
                    + mLPGrpEdorItemSchema.getEdorNo() + "' ")
            .append("   and edorType = '"
                    + mLPGrpEdorItemSchema.getEdorType() + "' ")
            .append("   and contNo = '"
                    + tLCInsuredListSchema.getContNo() + "' ")
            .append("   and contPlanCode = '"
                    + tLCInsuredListSchema.getContPlanCode() + "' ");
        map.put(sql.toString(), "UPDATE");

        //维护保单信息
        sql.delete(0, sql.length());
        sql.append("update LPCont a ")
            .append("set (prem, sumPrem) = ")
            .append("     (select sum(prem), sum(sumPrem) from LPPol where edorNo = a.edorNo and edorType = a.edorType and contNo = a.contNo) ")
            .append(",   peoples = peoples + ")
            .append(tLCInsuredListSchema.getNoNamePeoples())
            .append(",  operator = '").append(this.mGlobalInput.Operator)
            .append("',  modifyDate = '").append(this.mCurDate)
            .append("',  modifyTime = '").append(this.mCurTime)
            .append("' where edorNo = '"
                    + mLPGrpEdorItemSchema.getEdorNo() + "' ")
            .append("   and edorType = '"
                    + mLPGrpEdorItemSchema.getEdorType() + "' ")
            .append("   and contNo = '"
                    + tLCInsuredListSchema.getContNo() + "' ");
        map.put(sql.toString(), "UPDATE");

        return true;
    }

    /**
     * 查询tLCInsuredListSchema保障计划下的保费向信息
     * @param tLCInsuredListSchema LCInsuredListSchema：增人信息
     * @return LPPremSet
     */
    private LPPremSet getLPPrem(LCInsuredListSchema tLCInsuredListSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* from LPPrem a, LPPol b ")
            .append("where a.edorNo = b.edorNo and a.edorType = b.edorType ")
            .append("   and a.polNo = b.polNo ")
            .append("   and a.edorNo = '")
            .append(this.mLPGrpEdorItemSchema.getEdorNo())
            .append("'  and a.edorType = '")
            .append(this.mLPGrpEdorItemSchema.getEdorType())
            .append("'  and a.contNo = '")
            .append(tLCInsuredListSchema.getContNo())
            .append("'  and b.contPlanCode = '")
            .append(tLCInsuredListSchema.getContPlanCode())
            .append("'  and a.payPlanCode not like '000000%' ");
        System.out.println(sql.toString());
        LPPremSet tLPPremSet = new LPPremDB().executeQuery(sql.toString());
        if(tLPPremSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保障计划为"
                                + tLCInsuredListSchema.getContPlanCode()
                                + "的保全临时数据");
            return null;
        }

        return tLPPremSet;
    }

    /**
     * 处理保费向保费变更情况
     * @param tLPPremSet LPPremSet：保费项稽核
     * @param suPremInput double：录入的保费
     * @return boolean：成功true；否则false
     */
    private boolean dealPrem(LCInsuredListSchema tLCInsuredListSchema,
                             double sumPremInput)
    {
        LPPremSet tLPPremSet = getLPPrem(tLCInsuredListSchema);
        if(tLPPremSet == null)
        {
            return false;
        }

        //计算该保障计划的原总保费
        double sumPrem = 0;
        for(int i = 1; i <= tLPPremSet.size(); i++)
        {
            sumPrem += tLPPremSet.get(i).getPrem();
        }

        double sumPremAdd = 0; //已分配了的保费
        for(int i = 1; i < tLPPremSet.size(); i++)
        {
        	double premAdd = 0;
        	//交费项分配的保费 = 本次录入保费 * （交费项交保费 / 无名单该保障保费）
        	if(sumPrem==0){
        		premAdd = 0;
        	}else{
        		premAdd = sumPremInput * tLPPremSet.get(i).getPrem() / sumPrem;
        	}
            premAdd = PubFun.setPrecision(premAdd, "0.00");
            sumPremAdd += premAdd;

            tLPPremSet.get(i).setPrem(tLPPremSet.get(i).getPrem() + premAdd);
            tLPPremSet.get(i).setSumPrem(tLPPremSet.get(i).getSumPrem() + premAdd);
            tLPPremSet.get(i).setOperator(mGlobalInput.Operator);
            tLPPremSet.get(i).setModifyDate(this.mCurDate);
            tLPPremSet.get(i).setModifyTime(this.mCurTime);

            setPolPremAdd(tLPPremSet.get(i).getPolNo(), premAdd);
        }

        //处理最后一个交费项费用变化情况
        double lastPremAdd = PubFun.setPrecision(sumPremInput - sumPremAdd, "0.00");
        tLPPremSet.get(tLPPremSet.size()).setPrem(
            tLPPremSet.get(tLPPremSet.size()).getPrem() + lastPremAdd);

        //考虑到无名单减人的调用，sumPrem只对增人变更
        if(mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_WZ))
        {
            tLPPremSet.get(tLPPremSet.size()).setSumPrem(
                tLPPremSet.get(tLPPremSet.size()).getSumPrem() + lastPremAdd);
        }
        tLPPremSet.get(tLPPremSet.size()).setOperator(mGlobalInput.Operator);
        tLPPremSet.get(tLPPremSet.size()).setModifyDate(this.mCurDate);
        tLPPremSet.get(tLPPremSet.size()).setModifyTime(this.mCurTime);

        setPolPremAdd(tLPPremSet.get(tLPPremSet.size()).getPolNo(), lastPremAdd);

        map.put(tLPPremSet, "UPDATE");

        return true;
    }


    private void setPolPremAdd(String polNo, double premAdd)
    {
        //存储pol-polPremAdd
        if(mPolAddPremMap.containsKey(polNo))
        {
            double money = Double.parseDouble((String) mPolAddPremMap.get(polNo));
            mPolAddPremMap.put(polNo, String.valueOf(money + premAdd));
        }
        else
        {
            mPolAddPremMap.put(polNo, String.valueOf(premAdd));
        }

    }

    /**
     * 生成财务数据
     * @param LPPremSchema tLPPremSchema:责任信息
     * @return booean：成功true，否则false
     */
    private boolean setfinance(HashMap polAddPrem)
    {
        Object[] polNos = polAddPrem.keySet().toArray();
        for(int i = 0; i < polNos.length; i++)
        {
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
            tLPPolDB.setPolNo((String) polNos[i]);
            tLPPolDB.getInfo();

            String finType = getFinType(mLPGrpEdorItemSchema.getEdorType()
                             , tLPPolDB.getPolNo());

            LPDutyDB tLPDutyDB = new LPDutyDB();
            tLPDutyDB.setEdorNo(tLPPolDB.getEdorNo());
            tLPDutyDB.setEdorType(tLPPolDB.getEdorType());
            tLPDutyDB.setPolNo(tLPPolDB.getPolNo());
            LPDutySet set = tLPDutyDB.query();
            if(set.size() == 0)
            {
                mErrors.addOneError("没有查询到保全责任临时数据");
                return false;
            }

            BqCalBL tBqCalBL = new BqCalBL();
            LJSGetEndorseSchema tLJSGetEndorseSchema
                = tBqCalBL.initLJSGetEndorse(
                    mLPEdorItemSchema, tLPPolDB.getSchema(), set.get(1),
                    finType,
                    Double.parseDouble((String) polAddPrem.get(polNos[i])),
                    mGlobalInput);
            tLJSGetEndorseSchema.setOtherNoType("3");
            tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
            tLJSGetEndorseSchema.setManageCom(tLPPolDB.getManageCom());
            tLJSGetEndorseSchema.setAgentCode(tLPPolDB.getAgentCode());
            tLJSGetEndorseSchema.setAgentCom(tLPPolDB.getAgentCom());
            tLJSGetEndorseSchema.setAgentGroup(tLPPolDB.getAgentGroup());
            tLJSGetEndorseSchema.setAgentType(tLPPolDB.getAgentType());
            map.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        }

        return true;
    }

    /**
     * 得到edorType项目的polNo险种的财务类型
     * @param edorType String
     * @return String
     */
    private String getFinType(String edorType, String polNo)
    {
        String sysType = "";
        if(edorType.equals(BQ.EDORTYPE_WZ))
        {
            sysType = "BF";
        }
        else if(edorType.equals(BQ.EDORTYPE_WJ))
        {
            sysType = "TF";
        }

        //财务类型
        String finType = BqCalBL.getFinType(edorType,sysType, polNo);
        if(finType.equals(""))
        {
            CError.buildErr(this, "在LDCode1中缺少保全财务接口转换类型编码!");
            return "";
        }
        return finType;
    }

    public static void main(String[] args)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorAcceptNo("20060720000012");
        tLPGrpEdorItemDB.setEdorNo(tLPGrpEdorItemDB.getEdorAcceptNo());
        tLPGrpEdorItemDB.setEdorType("WZ");
        tLPGrpEdorItemDB.setGrpContNo("0000000704");
        tLPGrpEdorItemDB.getInfo();

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        VData d = new VData();
        d.add(g);
        d.add(tLPGrpEdorItemDB.getSchema());

        GrpEdorWZAppConfirmBL bl = new GrpEdorWZAppConfirmBL();
        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            PubSubmit p = new PubSubmit();
            if(!p.submitData(bl.getResult(), ""))
            {
                System.out.println(p.mErrors.getErrContent());
            }
        }
    }
}
