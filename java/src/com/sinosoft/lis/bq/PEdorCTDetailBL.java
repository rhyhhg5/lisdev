package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单遗失补发项目明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Alex
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorCTDetailBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = null;
  private LPEdorItemSchema mLPEdorItemSchema = null;
  private LPInsuredSet mLPInsuredSet = new LPInsuredSet();
  private LPContSchema mLPContSchema = null;
  private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema=null;
  private LPPolSet mLPPolSet = null;
  private MMap map = new MMap();
  private Reflections ref = new Reflections();
  
  

  private String currDate = PubFun.getCurrentDate();
  private String currTime = PubFun.getCurrentTime();

  private DisabledManageBL tDisabledManageBL = new DisabledManageBL();

  public PEdorCTDetailBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {

    //将操作数据拷贝到本类中
    this.mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) {
      return false;
    }   
    
    if (!checkData()) {
      return false;
    }

    //进行业务处理
    if (!dealData()) {
      return false;
    }

    //准备往后台的数据
    if (!prepareData()) {
      return false;
    }
    PubSubmit tSubmit = new PubSubmit();

    if (!tSubmit.submitData(mResult, "")) { //数据提交
      // @@错误处理
      this.mErrors.copyAllErrors(tSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "PEdorCTDetailBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("PEdorCTDetailBL End PubSubmit");
    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    try {
      mGlobalInput = (GlobalInput) mInputData
          .getObjectByObjectName("GlobalInput", 0);
      mLPEdorItemSchema = (LPEdorItemSchema) mInputData
          .getObjectByObjectName("LPEdorItemSchema", 0);
      mLPPolSet = (LPPolSet) mInputData.
          getObjectByObjectName("LPPolSet", 0);
      
      mLPContSchema = (LPContSchema) mInputData.getObjectByObjectName(
              "LPContSchema", 0);
      mLPEdorEspecialDataSchema= (LPEdorEspecialDataSchema) mInputData
      	  .getObjectByObjectName("LPEdorEspecialDataSchema", 0);
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "PEdorCTDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGlobalInput == null || mLPEdorItemSchema == null
        || mLPPolSet == null || mLPPolSet.size() == 0) {
      CError tError = new CError();
      tError.moduleName = "PEdorCTDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "输入数据有误!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    //首先更新其后的保全项目的状态
    map.put(UpdateEdorState.getUpdateEdorStateSql(mLPEdorItemSchema), "UPDATE");
    String edorno = mLPEdorItemSchema.getEdorNo();
    String edortype = mLPEdorItemSchema.getEdorType();
    String sql = " edorno='" + edorno + "' and edortype='" + edortype + "'";
    map.put("delete from lppol where" + sql, "DELETE");
    map.put("delete from lpcont where" + sql, "DELETE");
    map.put("delete from lpinsured where" + sql, "DELETE");

    //备份保单信息
    LPContBL tLPContBL = new LPContBL();
    tLPContBL.queryLPCont(mLPEdorItemSchema);
    if (tLPContBL.mErrors.needDealError()) {
      CError.buildErr(this, "查询个人保单出错！");
      return false;
    }
    
    //添加的部分,加paymode
    String strSQL = "  select paymode from lccont where contno = '"+mLPEdorItemSchema.getContNo()+"' ";
    ExeSQL tExeSQL = new ExeSQL();
	String arrResult = tExeSQL.getOneValue(strSQL);
  
    if("4".equals(arrResult)){
    	tLPContBL.setBankCode(mLPContSchema.getBankCode());
    	tLPContBL.setBankAccNo(mLPContSchema.getBankAccNo());
    	tLPContBL.setAccName(mLPContSchema.getAccName());
    }
   
    
    map.put(tLPContBL.getSchema(), "DELETE&INSERT");

    //备份被保人信息
    LCInsuredSet tLCInsuredSet = getLCInsured();
    for (int i = 1; i <= tLCInsuredSet.size(); i++) {
      LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(i);

      LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
      tLPInsuredSchema.setEdorNo(edorno);
      tLPInsuredSchema.setEdorType(edortype);
      ref.transFields(tLPInsuredSchema, tLCInsuredSchema);
      tLPInsuredSchema.setOperator(mGlobalInput.Operator);
      PubFun.fillDefaultField(tLPInsuredSchema);
      mLPInsuredSet.add(tLPInsuredSchema);

      map.put(mLPInsuredSet, "DELETE&INSERT");
      map.put(mLPPolSet, "DELETE&INSERT");
    }
    mLPEdorItemSchema.setEdorState("1");
    mLPEdorItemSchema.setModifyDate(currDate);
    mLPEdorItemSchema.setModifyTime(currTime);
    map.put(mLPEdorItemSchema, "UPDATE");
    
    String taxRisk = "  select riskcode from lcpol  where contno = '"+mLPEdorItemSchema.getContNo()+"'" +
    		" and riskcode in ( select riskcode from lmriskapp where risktype4='4' and  taxoptimal='Y') ";
    
    String result = tExeSQL.getOneValue(taxRisk);
    if(!"".equals(result)){
    	mLPEdorEspecialDataSchema.setEdorNo(edorno);
    	mLPEdorEspecialDataSchema.setEdorType(edortype);
    	map.put(mLPEdorEspecialDataSchema, "DELETE&INSERT");	
    }
   

    return true;
  }
  

  /**
   * 根据前面的输入数据，进行校验处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean checkData()
  {
      LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      String reasonCode = mLPEdorItemSchema.getReasonCode();
      tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
      tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
      tLPEdorItemDB.setContNo(mLPEdorItemSchema.getContNo());
      LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();

      if(mLPPolSet.size() == 0)
      {
          mErrors.addOneError("请选择险种。");
          return false;
      }

      if(tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
      {
          CError tError = new CError();
          tError.moduleName = "PEdorCTDetailBL";
          tError.functionName = "checkData";
          tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
          this.mErrors.addOneError(tError);
          return false;
      }

      mLPEdorItemSchema = tLPEdorItemSet.get(1);
      mLPEdorItemSchema.setEdorState("1");
      mLPEdorItemSchema.setReasonCode(reasonCode);

      if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "PEdorCTDetailBL";
          tError.functionName = "checkData";
          tError.errorMessage = "该保全已经保全理算，不能修改!";
          this.mErrors.addOneError(tError);
          return false;
      }
      
      DetailDataQuery tQuery = new DetailDataQuery(mLPEdorItemSchema.getEdorNo(), mLPEdorItemSchema.getEdorType());
      for(int i = 1; i <= mLPPolSet.size(); i++)
      {
          LPPolSchema tLPPolSchema =  (LPPolSchema) tQuery.getDetailData("LCPol", mLPPolSet.get(i).getPolNo());
          tLPPolSchema.setOperator(mGlobalInput.Operator);
          tLPPolSchema.setMakeDate(currDate);
          tLPPolSchema.setMakeTime(currTime);
          tLPPolSchema.setModifyDate(currDate);
          tLPPolSchema.setModifyTime(currTime);
          mLPPolSet.get(i).setSchema(tLPPolSchema);
      }
      
//    （某些）附加险或者主险任一险种出险，不可以单独退保或者一起退保，添加相关校验
      //如果是理赔人员进行的，可以不用此校验 zhangjl add
      String sOperator=mGlobalInput.Operator;
      if(!sOperator.substring(0, 2).equals("cm")){
      	ExeSQL tLPExeSQL = new ExeSQL();
          String sql_LP="select 1 from llcontdeal where EdorNo = '"+mLPEdorItemSchema.getEdorNo()+"' with ur";
          String tLP =tLPExeSQL.getOneValue(sql_LP);
      	if(!tLP.equals("1")){
      		if(!checkClaimPartOf())
              {
              	return false;
              }
      	}
      }
      if(!checkAppendPol())
      {
          return false;
      }
      //add by luomin
      if(!tDisabledManageBL.dealDisabledpol(mLPPolSet, "CT"))
      {
          CError tError = new CError();
          tError.moduleName = "PEdorCTDetailBL";
          tError.functionName = "checkData";
          if(tDisabledManageBL.getState().equals(BQ.STATE_FLAG_AVAILABLE))
          {
              tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                    "号被保人投保险种'"
                                    + tDisabledManageBL.getRiskcode() +
                                    "'为短险,其CT项目在险种失效状态下不能添加!";
          }
          if(tDisabledManageBL.getState().equals(BQ.STATE_FLAG_TERMINATE))
          {
              tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                    "号被保人投保险种'"
                                    + tDisabledManageBL.getRiskcode() +
                                    "'下CT项目在终止状态下不能添加!";
          }

          this.mErrors.addOneError(tError);
          return false;

      }
      
//    20101220 校验万能险
      if(!checkULICT())
      {
      	return false;
      }
      

  	
  	
      return true;
  }

  /**
   * 得到被保人信息
   * @return LCInsuredSet
   */
  private LCInsuredSet getLCInsured() {
    String insuredNos = "";
    for (int i = 1; i <= mLPPolSet.size(); i++) {
      insuredNos += "'" + mLPPolSet.get(i).getInsuredNo() + "',";
    }
    insuredNos = insuredNos.substring(0, insuredNos.lastIndexOf(","));
    StringBuffer sql = new StringBuffer();

    sql.append("select * ")
        .append("from LCInsured ")
        .append("where contNo = '")
        .append(this.mLPEdorItemSchema.getContNo())
        .append("'  and insuredNo in (").append(insuredNos).append(") ");
    System.out.println(sql.toString());

    LCInsuredDB tLCInsuredDB = new LCInsuredDB();
    LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(sql.toString());

    return tLCInsuredSet;
  }

  /**
   * 校验当前险种是否有附加险，附加险不能单独存在，但可以单独退保
   * @return boolean
   */
  private boolean checkAppendPol() {
    LDCode1DB tLDCode1DB = new LDCode1DB();
    tLDCode1DB.setCodeType("checkappendrisk");

    //未选择的险种
    LCPolSet tLCPolSet = CommonBL.getLeavingPolNoInfo(mLPPolSet);
    if (tLCPolSet == null) {
      return true;
    }

    //若未选择的险种是附加险且其主险已选择，则提示
    for (int i = 1; i <= tLCPolSet.size(); i++) {
      LCPolSchema tLCPolSchema = tLCPolSet.get(i);

      if (tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo())) {
        continue; //不是附加险
      }

      for (int j = 1; j <= mLPPolSet.size(); j++) {
        LPPolSchema tLPPolSchema = mLPPolSet.get(j);
        if (tLPPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())
            && tLPPolSchema.getInsuredNo()
            .equals(tLCPolSchema.getInsuredNo())) {
          mErrors.addOneError("险种 " + tLPPolSchema.getRiskCode()
                              + " 有附加险 " + tLCPolSchema.getRiskCode()
                              + "，请选择其附加险一起退保。");
          return false;
        }
      }
    }
    return true;
  }


  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareData() {
    //mMap.put(mLPEdorMainSchema, "UPDATE");
    //mMap.put(mLPEdorItemSchema, "UPDATE");
    mResult.clear();
    mResult.add(map);

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  
//万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错
//附加重疾可以单独退保
//附加意外不可以单独退保，只能随主险一同退保。
//主险退保时，两个附加险必须随主险一同退保。--已有
//6201不准单独退保
  private boolean checkULICT() 
  {
	  String tContNo = mLPEdorItemSchema.getContNo();
	  if (tContNo == null || tContNo.equals(""))
      {
          CError tError = new CError();
          tError.moduleName = "PEdorCTDetailBL";
          tError.functionName = "checkULICT";
          tError.errorMessage = "保单号查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
	  //	保单不包含万能险，不判断
      LCPolSet tLCPolSet = CommonBL.getLeavingPolNoInfo(mLPPolSet);
      if(tLCPolSet == null || tLCPolSet.size()==0)
      {
          return true;
      }
      else //非整单退保
      {
    	  for(int i=1;i<=mLPPolSet.size();i++)
          {   	
        	  //分红险主险和附加险必须同时退保
        	  if(mLPPolSet.get(i).getRiskCode().equals("730101"))
        	  {
        		  CError tError = new CError();
                  tError.moduleName = "PEdorCTDetailBL";
                  tError.functionName = "checkULICT";
                  tError.errorMessage = "保单号查询失败!";
                  this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同分红险的附加险一同退保!");
                  return false;
        	  }
          }
      }
      
      
	  if(!CommonBL.hasULIRisk(tContNo))
	  {
		  return true;
	  }

      //未选择的险种数量为0，整单退保

      if(tLCPolSet == null || tLCPolSet.size()==0)
      {
          return true;
      }
      else //非整单退保
      {
    	  for(int i=1;i<=mLPPolSet.size();i++)
          {
    		  //万能险套餐，所谓的主险不能单独退保=退保险种中包含主险，且不是整单退保，返回报错
        	  if(CommonBL.isULIRisk(mLPPolSet.get(i).getRiskCode()))
        	  {
        		  CError tError = new CError();
                  tError.moduleName = "PEdorCTDetailBL";
                  tError.functionName = "checkULICT";
                  tError.errorMessage = "保单号查询失败!";
                  this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                  return false;
        	  }
        	  //6201不准单独退保        	  
        	  if(CommonBL.is62Risk(mLPPolSet.get(i).getRiskCode()))
        	  {
        		  CError tError = new CError();
                  tError.moduleName = "PEdorCTDetailBL";
                  tError.functionName = "checkULICT";
                  tError.errorMessage = "保单号查询失败!";
                  this.mErrors.addOneError("险种"+mLPPolSet.get(i).getRiskCode()+"只能同万能主险一同退保");
                  return false;
        	  }
          }
      }
      
      //若未选择的险种包含万能,判断附加险能否退保
      for (int i = 1; i <= tLCPolSet.size(); i++)
      {
          LCPolSchema tLCPolSchema = tLCPolSet.get(i);
          if(CommonBL.isULIRisk(tLCPolSchema.getRiskCode()))
          {
        	  for (int j = 1; j <= mLPPolSet.size(); j++)
              {
        		  LPPolSchema tLPPolSchema = mLPPolSet.get(j);
        		  LMRiskAppDB subLMRiskAppDB = new LMRiskAppDB();
        		  subLMRiskAppDB.setRiskCode(tLPPolSchema.getRiskCode());
        		  if(subLMRiskAppDB.getInfo())
        		  {
        			  String tRiskType3 = subLMRiskAppDB.getRiskType3();
            			if(tRiskType3.equals("3")) //重疾
            			{
            				continue;
            			}
            			else if(tRiskType3.equals("4")) //意外
            			{
            				CError tError = new CError();
            		        tError.errorMessage = "附加险"+subLMRiskAppDB.getRiskCode()+"只能和主险同时退保!";
            		        this.mErrors.addOneError(tError);
            		        return false;
            			}
        		  }
              }
          }
      }
      return true;
	}

  private boolean checkClaimPartOf()
  {
  	   String tcheckohterPolContNo = mLPEdorItemSchema.getContNo();
  	  	
  	  	if (tcheckohterPolContNo == null || tcheckohterPolContNo.equals(""))
  	      {
  	          CError tError = new CError();
  	          tError.moduleName = "PEdorXTDetailBL";
  	          tError.functionName = "checkohterPol";
  	          tError.errorMessage = "保单号查询失败!";
  	          this.mErrors.addOneError(tError);
  	          return false;
  	      }
  	  	
  	     	  for(int i=1;i<=mLPPolSet.size();i++)
  	           {   	
  	     		  //先判断某险种是否存在理赔

  	     		  String strPolno = null;
  	     		  String SQL_checkPolno="select caseno from llcase a where caseno in " +
  	     		  		" (select caseno from llclaimpolicy" +
  	     		  		" where polno ='"+strPolno+"')" +
  	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
  	     		  		" with ur";

  	     		  SSRS tSSRS = null;
  	     		  String tMRiskcode = null;
  	     		  String tcheckpolSQL = null;
  	     		  String mriskcode = null;
  	     		  String friskcode = null;
  	     		  LPPolSchema tLPPolSchema=new LPPolSchema();
  	     		  ExeSQL tcheckotherpolExeSQL = new ExeSQL();
  	     		  String checkPolon = null;
  	     		  String tcheck_F_polSQL = null;
  	     		  String check_F_polno = null;
  	     		  tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
  			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
  	     		  
  	     		  tLPPolSchema=mLPPolSet.get(i);
  	     		System.out.println(tLPPolSchema.getPolNo());
  	     		System.out.println(tLPPolSchema.getMainPolNo());
  	     		  if(!tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
  	     		  {
  	     			 strPolno = tLPPolSchema.getPolNo();//校验险种号为附加险校验险种号
  	     			SQL_checkPolno="select caseno from llcase a where caseno in " +
  	 		  		" (select caseno from llclaimpolicy" +
  	 		  		" where polno ='"+strPolno+"')" +
  	 		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
  	 		  		" with ur";
  	     			 tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
  	     			if (tSSRS.getMaxRow()== 0) //附加险没有出险
  	                {
  	     				strPolno = tLPPolSchema.getMainPolNo();//校验险种号为主险校验险种号
  	     				SQL_checkPolno="select caseno from llcase a where caseno in " +
  	     		  		" (select caseno from llclaimpolicy" +
  	     		  		" where polno ='"+strPolno+"')" +
  	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
  	     		  		" with ur";
  	     				tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
  	     				 if (tSSRS.getMaxRow()== 0) //主险也没有出险
  	     	             {
  	     	            	 continue; 
  	     	             }
  	     				 else//主险出险
  	     				 {
  	     	     			 checkPolon=tLPPolSchema.getMainPolNo();//主险出险得到主险的种号
  	     	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
  	     			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
  	     	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);
  	     	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
  	     	     	          {
  	     	     	          	CError tError = new CError();
  	     	     	          	tError.moduleName = "PEdorXTDetailBL";
  	     	     	              tError.functionName = "checkohterPol";
  	     	     	              tError.errorMessage = "保单号查询失败!";
  	     	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
  	     	     	              return false;
  	     	     	          }
  	     	     	          mriskcode=tMRiskcode;
  	     	     	          friskcode=tLPPolSchema.getRiskCode();
  	     	     		   
  	     				 }
  	                }
  	     			else //附加险出险
  	     			{
  	     			 checkPolon=tLPPolSchema.getMainPolNo();//附加险出险得到主险的险种号
  	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
  			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
  	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);

  	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
  	     	          {
  	     	          	CError tError = new CError();
  	     	          	tError.moduleName = "PEdorXTDetailBL";
  	     	              tError.functionName = "checkohterPol";
  	     	              tError.errorMessage = "保单号查询失败!";
  	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
  	     	              return false;
  	     	          }
  	     	         mriskcode=tMRiskcode;
  	     	         friskcode=tLPPolSchema.getRiskCode();
  	     		   }
  	     		  }
  	     		  //------------选中的只是主险------------
  	     		 if(tLPPolSchema.getMainPolNo().equals(tLPPolSchema.getPolNo()))
  	    		  {
  	    			 strPolno = tLPPolSchema.getMainPolNo();//校验险种号为主险种号
  	    			 SQL_checkPolno="select caseno from llcase a where caseno in " +
  	  		  		" (select caseno from llclaimpolicy" +
  	  		  		" where polno ='"+strPolno+"')" +
  	  		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
  	  		  		" with ur";
  	    			 tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
  	    			if (tSSRS.getMaxRow()== 0) //主险没有出险
  	               {
  	    				//得到附加险的险种号
  	    				tcheck_F_polSQL="select polno from lcpol where MainPolNo='"+tLPPolSchema.getMainPolNo()+"' " +
  		  		  		" and polno<>mainpolno " +
  		  		  		" and  contno='"+mLPEdorItemSchema.getContNo()+"'";
  	    				check_F_polno =tcheckotherpolExeSQL.getOneValue(tcheck_F_polSQL);
  	    				//--------------------------------------------
  	    				strPolno = check_F_polno;//校验险种号为附加险校验险种号
  	    				SQL_checkPolno="select caseno from llcase a where caseno in " +
  	     		  		" (select caseno from llclaimpolicy" +
  	     		  		" where polno ='"+strPolno+"')" +
  	     		  		" and not exists (select 1 from llclaim where caseno=a.caseno and givetype='3')" +
  	     		  		" with ur";
  	    				tSSRS = new ExeSQL().execSQL(SQL_checkPolno);
  	    				 if (tSSRS.getMaxRow()== 0) 
  	    	             {
  	    	            	 continue; 
  	    	             }
  	    				 else//附加险出险
  	    				 {
  	    	     			 checkPolon=check_F_polno;//主险出险得到附加险的种号
  	    	     			tcheckpolSQL="select riskcode from lcpol where polno='"+checkPolon+"' " +
  	    			  		"and contno='"+mLPEdorItemSchema.getContNo()+"'";
  	    	     		     tMRiskcode =tcheckotherpolExeSQL.getOneValue(tcheckpolSQL);
  	    	     	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
  	    	     	          {
  	    	     	          	CError tError = new CError();
  	    	     	          	tError.moduleName = "PEdorXTDetailBL";
  	    	     	              tError.functionName = "checkohterPol";
  	    	     	              tError.errorMessage = "保单号查询失败!";
  	    	     	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
  	    	     	              return false;
  	    	     	          }
  	    	     	          mriskcode=tLPPolSchema.getRiskCode();
  	    	     	          friskcode=tMRiskcode;
  	    				 }
  	               }
  	    			else//主险出险
  	    			{
//  	    				得到附加险的险种号
  	    				tcheck_F_polSQL="select riskcode from lcpol where MainPolNo='"+tLPPolSchema.getMainPolNo()+"' " +
  		  		  		" and polno<>mainpolno " +
  		  		  		" and  contno='"+mLPEdorItemSchema.getContNo()+"'";
  	    				check_F_polno =tcheckotherpolExeSQL.getOneValue(tcheck_F_polSQL);
  	    				//--------------------------------------------
  	    		     tMRiskcode =check_F_polno;
  	    	          if(tMRiskcode==null&&tMRiskcode.equals(null)&&tMRiskcode.equals(""))
  	    	          {
  	    	          	CError tError = new CError();
  	    	          	tError.moduleName = "PEdorXTDetailBL";
  	    	              tError.functionName = "checkohterPol";
  	    	              tError.errorMessage = "保单号查询失败!";
  	    	              this.mErrors.addOneError("险种信息缺失---XTDEtail--628");
  	    	              return false;
  	    	          }
  	    	          mriskcode=tLPPolSchema.getRiskCode();
  	     	          friskcode=tMRiskcode;
  	    		   }
  	    		  }
  	 		        
//  	          LDCode1Schema tLDCode1Schema=new LDCode1Schema();
  	          LDCode1DB tLDCode1DB =new LDCode1DB();
  	  	  //add wdl 原校验为少儿险保单发生理赔则不允许解约，改为当前保单年度内未发生理赔，则允许解约。
  	          if("120706".equals(friskcode) && "320106".equals(mriskcode)){
  	        	  String sql_casedate = "select accdate from LLSubReport where subrptno in(select subrptno from llcaserela where caseno in "
  	        			  + "(select caseno from llclaimdetail where contno = '"+tLPPolSchema.getContNo()+"' and polno='"+tLPPolSchema.getPolNo()+"'))";
  	        	  String sql_contyear = "select cvalidate,payenddate from lcpol where contno = '"+tLPPolSchema.getContNo()+"' and polno='"+tLPPolSchema.getPolNo()+"'"; 
  	        	  String casedate = tcheckotherpolExeSQL.getOneValue(sql_casedate);
  	        	  tSSRS = new ExeSQL().execSQL(sql_contyear);
  	        	  String cvalidate = tSSRS.GetText(1, 1);
  	        	  String payenddate = tSSRS.GetText(1, 2);
  	        	  String sql_test = "select 1 from dual where '"+casedate+"' between '"+cvalidate+"' and '"+payenddate+"' ";
  	        	  String exesql = tcheckotherpolExeSQL.getOneValue(sql_test);
  	        	if(!"1".equals(exesql)){
	        		  continue;
	        	  }  	        	  
  	          }
  	          tLDCode1DB.setCode(friskcode);
  	          tLDCode1DB.setCode1(mriskcode);
  	          tLDCode1DB.setCodeType("checkclaimrisk");
  	          if(!tLDCode1DB.getInfo())
  	          {
  	        	  
  	          	continue;
  	          }
  	          else
  	          {
  	          	CError tError = new CError();
  	              tError.moduleName = "PEdorXTDetailBL";
  	              tError.functionName = "checkohterPol";
  	              tError.errorMessage = "保单号查询失败!";
  	              this.mErrors.addOneError("险种已经出险"+tSSRS.GetText(1, 1)+"，" +
  	              		"和其存在主副险关系的"+tMRiskcode+"都不可以退保!");
  	              return false;
  	          }           	  
  	         }	
  	  	return true;
  	  }
}


