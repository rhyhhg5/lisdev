package com.sinosoft.lis.bq;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.utility.*;

public interface EdorConfirm
{
    public CErrors mErrors = new CErrors();
    public boolean submitData(VData cInputData, String cOperate);

    public VData getResult();
//  public TransferData getReturnTransferData();
//  public CErrors getErrors();
}
