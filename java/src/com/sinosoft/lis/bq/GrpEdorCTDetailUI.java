package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单犹豫期退保明细</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorCTDetailUI{
	
		public CErrors mErrors = new CErrors();
		
		public GrpEdorCTDetailUI()
    {
    }
    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEdorCTDetailBL tGrpEdorCTDetailBL = new GrpEdorCTDetailBL();
        if(!tGrpEdorCTDetailBL.submitData(cInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpEdorCTDetailBL.mErrors);
            return false;
        }

        return true;
    }

}