package com.sinosoft.lis.bq;

import sqlj.runtime.error.Errors;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LMCanloanComSetUI {


    private LMCanloanComSetBL mLMCanloanComSetBL = null;

    public CErrors mErrors = new CErrors();
    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public LMCanloanComSetUI()
    {
    	mLMCanloanComSetBL = new LMCanloanComSetBL();
    }

    /**
     * 调用业务逻辑类
     * @param operator String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!mLMCanloanComSetBL.submitData(cInputData, cOperate))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "设置错误！";
            this.mErrors.addOneError(tError);        	
            return false;
        }
        return true;
    }

    /**
     * 得到错误
     * @return String
     */
    public CErrors getError()
    {
        return mLMCanloanComSetBL.mErrors;
    }


}
