package com.sinosoft.lis.bq;

import java.io.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
* <p>Title: 特需医疗账户资金分配</p>
* <p>Description: 个人账户资金分配清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PrtGrpInsuredListWSBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private String mGrpContNo = null;

    private String mEdorNo = null;

    private String mEdorType = BQ.EDORTYPE_WS;

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PrtGrpInsuredListWSBL(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mGrpContNo = getGrpContNo(edorNo);
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!createXML())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PrtGrpInsuredListWS.vts", "printer");

        //设置批单中TextTag要显示的信息
        TextTag tag = new TextTag();
        tag.add("GrpName", getGrpName(mGrpContNo));
        tag.add("EdorNo", mEdorNo);
        xml.addTextTag(tag);

        //设置被保人清单
        ListTable listTable = new ListTable();
        listTable.setName("WS");
        LPDiskImportSet tLPDiskImportSet = getInsuredList();
        for (int i = 1; i <= tLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = tLPDiskImportSet.get(i);
            String[] column = new String[15];
            column[0] = String.valueOf(i);
            column[1] = StrTool.cTrim(tLPDiskImportSchema.getInsuredName());
            column[2] = StrTool.cTrim(tLPDiskImportSchema.getInsuredNo());
            String sex = tLPDiskImportSchema.getSex();
            if ((sex == null) || sex.equals("") || sex.equals("2"))
            {
                sex = "";
            }
            else
            {
                sex = ChangeCodeBL.getCodeName("Sex",
                        tLPDiskImportSchema.getSex());
            }
            column[3] = sex;
            String birthday = tLPDiskImportSchema.getBirthday();
            if (birthday == null || birthday.equals("") ||
                    birthday.equals("1900-01-01"))
            {
                birthday = "";
            }
            else
            {
                birthday = CommonBL.decodeDate(tLPDiskImportSchema.getBirthday());
            }
            column[4] = birthday;
            column[5] = ChangeCodeBL.getCodeName("IDType",
                    tLPDiskImportSchema.getIDType());
            column[6] = StrTool.cTrim(tLPDiskImportSchema.getIDNo());
            column[7] = StrTool.cTrim(String.valueOf(tLPDiskImportSchema.getContPlanCode()));
            column[8] = "0";
            column[9] = CommonBL.decodeDate(tLPDiskImportSchema.getEdorValiDate());
            column[10] = ChangeCodeBL.getCodeName("Bank",
                    tLPDiskImportSchema.getBankCode(), "BankCode");;
            column[11] = StrTool.cTrim(tLPDiskImportSchema.getBankAccNo());
            column[12] = StrTool.cTrim(tLPDiskImportSchema.getAccName());
            column[13] = StrTool.cTrim(tLPDiskImportSchema.getOthIDNo());
            column[14] = CommonBL.decodeState(tLPDiskImportSchema.getState());
            listTable.add(column);
        }
        xml.addListTable(listTable, new String[15]);
        //xml.outputDocumentToFile("c:\\", "GAInsuredList");
        return true;
    }

    /**
     * 得到团体合同号
     * @param edorNo String
     * @return String
     */
    private String getGrpContNo(String edorNo)
    {
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(edorNo);
        tLPGrpEdorItemDB.setEdorType(mEdorType);
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet.size() == 0)
        {
            return null;
        }
        return tLPGrpEdorItemSet.get(1).getGrpContNo();
    }

    /**
     * 得到团体单位名称
     * @param grpContNo String
     * @return String
     */
    private String getGrpName(String grpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size() == 0)
        {
            return null;
        }
        return tLCGrpContSet.get(1).getGrpName();
    }

    /**
     * 得到被保人列表
     * @return LPDiskImportSet
     */
    private LPDiskImportSet getInsuredList()
    {
        String sql = "select * from LPDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and GrpContNo = '" + mGrpContNo + "' " +
                "union " +
                "select * from LBDiskImport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and GrpContNo = '" + mGrpContNo + "' " +
                "order by SerialNo ";
        System.out.println(sql);
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        return tLPDiskImportDB.executeQuery(sql);
    }
}
