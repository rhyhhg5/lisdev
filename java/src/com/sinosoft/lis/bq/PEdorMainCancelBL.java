package com.sinosoft.lis.bq;

import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除BL层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong modify by Alex
 * @version 1.0
 */
public class PEdorMainCancelBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 撤销申请原因 */
   private String delReason;
   private String reasonCode;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    //private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema(); //
    private TransferData tTransferData = new TransferData();
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();


  public PEdorMainCancelBL()
    {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //撤销原因

        //将VData数据还原成业务需要的类
        if (this.getInputData() == false)
        {
            return false;
        }

        System.out.println("---getInputData successful---");

        if (this.dealData() == false)
        {
            return false;
        }

        System.out.println("---dealdata successful---");

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

//        PubSubmit tPubSubmit = new PubSubmit();
//
//        if (!tPubSubmit.submitData(mResult, cOperate))
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
//
//            return false;
//        }

        return true;
    }

    /**
     * 将UI层传输来得数据根据业务还原成具体的类
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
        this.delReason = (String)tTransferData.getValueByName("DelReason");
        this.reasonCode = (String)tTransferData.getValueByName("ReasonCode");
        if (mGlobalInput == null)
        {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLPEdorMainSchema.setSchema((LPEdorMainSchema) mInputData.
                                    getObjectByObjectName(
                                            "LPEdorMainSchema", 0));

        if (mLPEdorMainSchema == null)
        {
            this.mErrors.addOneError(new CError("传入的信息不完全！"));

            return false;
        }

        //根据已有数据取得数据库中数据
        LPEdorMainDB tLPEdorMainDB=new LPEdorMainDB();
        tLPEdorMainDB.setEdorNo(mLPEdorMainSchema.getEdorNo());
        tLPEdorMainDB.setContNo(mLPEdorMainSchema.getContNo());
        tLPEdorMainDB.setEdorAcceptNo(mLPEdorMainSchema.getEdorAcceptNo());
        if(!tLPEdorMainDB.getInfo())
        {
          CError tError = new CError();
          tError.moduleName = "PEdorMainCancelBL";
          tError.functionName = "dealData";
          tError.errorMessage = "没有相应的批单信息,请确认传递的数据的完整性!";
          this.mErrors.addOneError(tError);
          System.out.println(tError);
          return false;

        }
        mLPEdorMainSchema=tLPEdorMainDB.getSchema();


        //如果该保全项目处于人工核保状态,不能删除
        if("5".equals(mLPEdorMainSchema.getUWState()))
        {
          CError tError = new CError();
          tError.moduleName = "PEdorMainCancelBL";
          tError.functionName = "dealData";
          tError.errorMessage = "该保单处于人工核保状态,不能删除!";
          this.mErrors.addOneError(tError);
          System.out.println(tError);
          return false;
        }

//        String strSql =
//                "select Max(a.edorno) from LPEdormain a where a.EdorState<>'0' and a.ContNo='" +
//                mLPEdorMainSchema.getContNo() + "' ";
//
//        String MaxEdorNo = tExeSQL.getOneValue(strSql);
//
//        System.out.println(MaxEdorNo);
//
//        if ((MaxEdorNo == null) || (MaxEdorNo.trim() == ""))
//        {
//            this.mErrors.addOneError(new CError("不存在此批单号的批改记录!"));
//            return false;
//        }
//
//        if (!MaxEdorNo.equals(mLPEdorMainSchema.getEdorNo()))
//        {
//            this.mErrors.addOneError(new CError("该批单不是最近一条，无法删除!!"));
//            return false;
//        }
//        String strSql =
//                "select Max(a.MakeDate) from LPEdormain a where a.EdorState<>'0' and a.ContNo='" +
//                mLPEdorMainSchema.getContNo() + "' ";
//       String LatestDate = tExeSQL.getOneValue(strSql).substring(0, 10);
//       System.out.println(LatestDate);
//       System.out.println(mLPEdorMainSchema.getMakeDate().toString());
//        if (mLPEdorMainSchema.getMakeDate().toString().equals(LatestDate))
//        {
//            //System.out.println("日期相等");
//            strSql =
//                    "select Max(a.MakeTime) from lpedormain a where a.ContNo= '" +
//                    mLPEdorMainSchema.getContNo() + "'  and a.makeDate='" +
//                    LatestDate + "' ";
//
//            String LatestTime = tExeSQL.getOneValue(strSql).toString();
//            System.out.println(LatestTime + "         " +LatestTime);
//            if (!mLPEdorMainSchema.getMakeTime().toString().equals(LatestTime))
//            {
//                this.mErrors.addOneError(new CError("此保单有多次保全申请未确认，此条申请记录不是最近的一条，撤销时请从此保单最近的未确认保全申请开始!!"));
//                return false;
//            }
//
//        }
//        else
//        {
//            this.mErrors.addOneError(new CError("此保单有多次保全申请未确认，此条申请记录不是最近的一条，撤销时请从此保单最近的未确认保全申请开始!!"));
//            return false;
//        }

        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private boolean dealData()
    {
        String delSql;
        String tEdorNo = mLPEdorMainSchema.getEdorNo();


        //将将数据备份，并存储备份信息
        LPEdorMainSchema cLPEdorMainSchema = new LPEdorMainSchema();
        LOBEdorMainSchema cLOBEdorMainSchema = new LOBEdorMainSchema();
        LPEdorMainDB cLPEdorMainDB = new LPEdorMainDB();
        LPEdorMainSet cLPEdorMainSet = new LPEdorMainSet();

    //选出当前记录
        String sql = "select * from LpedorMain where edorNo='" + tEdorNo + "'";
        cLPEdorMainSet = cLPEdorMainDB.executeQuery(sql);
        if (cLPEdorMainSet.size() > 0 && cLPEdorMainSet != null) {
            LOBEdorMainSet tLOBEdorMainSet = new LOBEdorMainSet();
          for (int k = 1; k <= cLPEdorMainSet.size(); k++) {
            cLPEdorMainSchema = cLPEdorMainSet.get(k);
            System.out.println("cLPEdorMainSchema:" + cLPEdorMainSchema.getContNo()); //测试用
            Reflections crf = new Reflections();
            crf.transFields(cLOBEdorMainSchema, cLPEdorMainSchema); //将一条记录整体复制
            cLOBEdorMainSchema.setReason(delReason); //添加撤销原因
            cLOBEdorMainSchema.setReasonCode(reasonCode); //待添加
            cLOBEdorMainSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(cLOBEdorMainSchema);
            tLOBEdorMainSet.add(cLOBEdorMainSchema);
          }
          mMap.put(cLOBEdorMainSchema, "DELETE&INSERT");
          mMap.put(mLPEdorMainSchema,"DELETE");
        }

        //删除批单下的保全项目
        LPEdorItemSet aLPEdorItemSet = new LPEdorItemSet();
        LPEdorItemDB aLPEdorItemDB = new LPEdorItemDB();
        aLPEdorItemDB.setEdorAcceptNo(mLPEdorMainSchema.getEdorAcceptNo());
        aLPEdorItemDB.setEdorNo(mLPEdorMainSchema.getEdorNo());
        aLPEdorItemDB.setContNo(mLPEdorMainSchema.getContNo());
        aLPEdorItemSet = aLPEdorItemDB.query();
        if(aLPEdorItemSet.size()!=0 &&aLPEdorItemSet != null)
        {
          for(int i= 1; i<=aLPEdorItemSet.size();i++)
          {
            VData tVData = new VData();
            tVData.add(aLPEdorItemSet.get(i));
            GlobalInput tGI = new GlobalInput();
            tGI.ManageCom = mManageCom;
            tGI.Operator = mOperator;
            tVData.add(tGI);
            tVData.addElement(tTransferData);
            PEdorItemCancelBL tPEdorItemCancelBL = new PEdorItemCancelBL();
            if(!tPEdorItemCancelBL.submitData(tVData,"EDORMAIN"))
            {
              this.mErrors.copyAllErrors(tPEdorItemCancelBL.mErrors);
             // System.out.println(tError);
              return false;
            }
            else
            {
                mMap.add(tPEdorItemCancelBL.getMap());
            }

          }
        }
		String[] tableForDel =
				{
				"LPGeneral", "LPCont", "LPGeneralToRisk",
				"LPPol",
				"LPGetToAcc", "LPInsureAcc",
				"LPPremToAcc", "LPPrem", "LPGet",
				"LPDuty", "LPPrem_1", "LPInsureAccClass",
				"LPInsureAccTrace",
				"LPContPlanDutyParam", "LPContPlanRisk",
				"LPContPlan",
				"LPAppnt", "LPCustomerImpart",
				"LPInsuredRelated",
				"LPBnf", "LPInsured", "LPCustomerImpartParams",
				"LPInsureAccFee", "LPInsureAccClassFee",
				"LPContPlanFactory",
				"LPContPlanParam", "LPMove",
				"LPEdorPrint", "LPAppntTrace",
				"LPLoan", "LPReturnLoan", "LPEdorPrint2",
				"LPEdorPrint3",
				"LPGUWError", "LPGUWMaster", "LPCUWError",
				"LPCUWMaster",
				"LPCUWSub",
				"LPUWError", "LPUWMaster", "LPUWSub",
				"LPPENoticeItem", "LPPENotice", "LPGCUWError",
				"LPGCUWMaster",
				"LPGCUWSub", "LPGUWSub", "LPSpec", "LPIssuePol",
				"LPCSpec", "LPAccount",
				"LPPerson", "LPAddress",
				"LPPayRuleFactory", "LPPayRuleParams",
				"LPRReportResult",
				"LPRReport", "LPRReportItem",
				"LPCustomerImpartDetail",
				"LPAccMove", "LPEdorItem",
				"LPEdorMain"};

		for (int i = 0; i < tableForDel.length; i++)
		{
			delSql = "delete from  " + tableForDel[i] + " where EdorNo = '" +
					tEdorNo + "'";
			mMap.put(delSql, "DELETE");
		}

        //删除批改补退费表中相关记录
        delSql = "delete from  LJSGetEndorse where EndorsementNo = '" + tEdorNo + "'";
        mMap.put(delSql, "DELETE");

        //对于一些特殊的保全项目调用特殊的保全项目取消类
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(tEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 0; i < tLPEdorItemSet.size(); i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i + 1);
            try
            {
                Class tClass = Class.forName("com.sinosoft.lis.bq.PEdor" +
                                             tLPEdorItemSchema.getEdorType() +
                                             "CancelBL");
                EdorCancel tGrpEdorCancel = (EdorCancel) tClass.newInstance();
                VData tVData = new VData();

                tVData.add(tLPEdorItemSchema);
                tVData.add(mGlobalInput);

                if (!tGrpEdorCancel.submitData(tVData,
                                               "EDORMAINCANCEL||" +
                                               tLPEdorItemSchema.getEdorType()))
                {
                    return false;
                }
                else
                {
                    VData rVData = tGrpEdorCancel.getResult();
                    MMap tMap = new MMap();
                    tMap = (MMap) rVData.getObjectByObjectName("MMap", 0);
                    if (tMap == null)
                    {
                        mErrors.addOneError(new CError("得到保单号为：" +
                                tLPEdorItemSchema.getContNo() + "保全项目为:" +
                                tLPEdorItemSchema.getEdorType() +
                                "的保全申请确认结果时失败！"));
                        return false;

                    }
                    else
                    {
                        mMap.add(tMap);
                    }
                }

            }
            catch (ClassNotFoundException ex)
            {
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }

        }


        return true;
    }

    /**
     * 准备数据，重新填充数据容器中的内容
     */
    private void prepareOutputData()
    {
        //记录当前操作员
        mResult.clear();
        mResult.add(mMap);
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public MMap getMap()
    {
      return mMap;
    }

}
