package com.sinosoft.lis.bq;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团体保全集体下个人功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class GEdorDetailBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
  private LPEdorItemSet saveLPEdorItemSet = new LPEdorItemSet();
  private LPEdorMainSet saveLPEdorMainSet = new LPEdorMainSet();
  private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
  private LPInsuredSet mLPInsuredSet = null;
  private LPPolSet mLPPolSet = null;

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private Reflections ref= new Reflections();
  private String currDate=PubFun.getCurrentDate();
  private String currTime=PubFun.getCurrentTime();
  private MMap map=new MMap();
  public GEdorDetailBL() {
  }

  public boolean submitData(VData cInputData,String cOperate) {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    mOperate = cOperate;

    //得到外部传入的数据
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //数据校验操作
    if (!checkData()) return false;
    System.out.println("---End checkData---");

    //数据准备操作
    if (mOperate.equals("INSERT||EDOR"))
    {
        if (!prepareData())return false;
        System.out.println("---End prepareData---");
    }
    //数据准备操作
    if (mOperate.equals("DELETE||EDOR"))
    {
        if(mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_ZT))
        {
            if(!deleteDataZT())
            {
                return false;
            }
        }
        else if (!deleteData())
        {
            return false;
        }
        System.out.println("---End prepareData---");
    }

    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mResult, mOperate))
    {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        System.out.println("GedorDetailBL数据提交失败");
        this.mErrors.addOneError("数据提交失败!");
        return false;
    }

    return true;
  }

  public MMap getsubmitData(VData cInputData,String cOperate) {
	    //将操作数据拷贝到本类中
	    mInputData = (VData)cInputData.clone() ;
	    mOperate = cOperate;

	    //得到外部传入的数据
	    if (!getInputData()) return null;
	    System.out.println("---End getInputData---");

	    //JM此时还没有lpgrpedoritem记录,加上此IF
	    if(!mLPGrpEdorItemSchema.getEdorType().equals("JM"))
	    {
	    //数据校验操作
	    if (!checkData()) return null;
	    System.out.println("---End checkData---");
	    }
	    
	    
	    //数据准备操作
	    if (mOperate.equals("INSERT||EDOR"))
	    {
	        if (!prepareData())return null;
	        System.out.println("---End prepareData---");
	    }
	    //数据准备操作
	    if (mOperate.equals("DELETE||EDOR"))
	    {
	        if(mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_ZT))
	        {
	            if(!deleteDataZT())
	            {
	                return null;
	            }
	        }
	        else if (!deleteData())
	        {
	            return null;
	        }
	        System.out.println("---End prepareData---");
	    }

	    return map;
	  }
  
  /**
   * 减人撤销个人申请
   * @return boolean
   */
  private boolean deleteDataZT()
  {
      if(!getLPInsuredInfo())
      {
          return false;
      }

      for(int i = 1; i <= mLPEdorItemSet.size(); i++)
      {
          LPInsuredDB tLPInsuredDB = new LPInsuredDB();
          tLPInsuredDB.setContNo(mLPEdorItemSet.get(i).getContNo());
          LPInsuredSet tLPInsuredSet = tLPInsuredDB.query();

          if(needDeleteLPEdorItem(tLPInsuredSet))
          {
              String sql = "  delete from LPEdorItem "
                           + "where edorNo = '"
                           + mLPEdorItemSet.get(i).getEdorNo() + "' "
                           + "    and edorType = '"
                           + mLPGrpEdorItemSchema.getEdorType() + "' "
                           + "    and contNo = '"
                           + mLPEdorItemSet.get(i).getContNo() + "' ";
              map.put(sql, "DELETE");
          }
          else
          {
              //将insuredNo赋值为非000000
          }

          //删除险种信息
          LPPolDB tLPPolDB = new LPPolDB();
          tLPPolDB.setEdorNo(mLPEdorItemSet.get(i).getEdorNo());
          tLPPolDB.setEdorType(mLPEdorItemSet.get(i).getEdorType());
          tLPPolDB.setContNo(mLPEdorItemSet.get(i).getContNo());
          LPPolSet tLPPolSet = tLPPolDB.query();
          map.put(tLPPolSet, "DELETE");
      }

      String sql = "  delete from LPEdorMain "
                   + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                   + "    and contNo in ("
                   + "        select distinct contNo "
                   + "        from LPInsured "
                   + "        where edorNo = '"
                   + mLPGrpEdorItemSchema.getEdorNo() + "' "
                   + "        and edorType = '"
                   + mLPGrpEdorItemSchema.getEdorType() + "') "
                   + "    and 0 = (select count(edorNo) from LPEdorItem "
                   + "            where edorNo = '"
                   + mLPGrpEdorItemSchema.getEdorNo() + "') ";
      map.put(sql, "DELETE");
      map.put(mLPInsuredSet, "DELETE");

      mResult.clear();
      mResult.add(map);

      return true;
  }

  private boolean needDeleteLPEdorItem(LPInsuredSet tLPInsuredSet)
  {
      //若删除了原来选择的所有被保人，则需要删除个人申请
      int insuredCountInLPInsured = tLPInsuredSet.size();
      int insuredCountSelect = 0;

      for(int i = 1; i <= mLPInsuredSet.size(); i++)
      {
          //若选择的被保人不是主被保人，则需要校验其主被保人是否仍需要减去
          //若主被保人仍需要减去，则其连带被保人不能撤销申请
          if(!mLPInsuredSet.get(i).getRelationToMainInsured()
             .equals(BQ.MAININSURED))
          {
              //处理连带被保人的删除
              System.out.println("需要增加处理连带被保人的删除");
          }

          for(int t = 1; t <= tLPInsuredSet.size(); t++)
          {
              System.out.println(tLPInsuredSet.get(t).getContNo() + " "
                  + mLPInsuredSet.get(i).getContNo() + " "
                  + tLPInsuredSet.get(t).getInsuredNo() + " "
                  + mLPInsuredSet.get(i).getInsuredNo());
              //保单号与被保人号都相等即为同一被保人
              if(tLPInsuredSet.get(t).getContNo()
                 .equals(mLPInsuredSet.get(i).getContNo())
                 && tLPInsuredSet.get(t).getInsuredNo()
                 .equals(mLPInsuredSet.get(i).getInsuredNo()))
              {
                  insuredCountSelect++;
              }
          }
      }
      if(insuredCountInLPInsured == insuredCountSelect)
      {
          return true;
      }

      return false;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()
  {
      try
      {
          mLPEdorItemSet = (LPEdorItemSet) mInputData
                           .getObjectByObjectName("LPEdorItemSet", 0);
          mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) mInputData
                                 .getObjectByObjectName("LPGrpEdorItemSchema", 0);
          mLPInsuredSet = (LPInsuredSet) mInputData
                          .getObjectByObjectName("LPInsuredSet", 0);
          mGlobalInput = (GlobalInput) mInputData
                         .getObjectByObjectName("GlobalInput", 0);
      }
      catch (Exception e)
      {
          this.mErrors.addOneError("接收数据失败");
          return false;
      }

      return true;
  }

  /**
   * 校验传入的数据的合法性
   * @return
   */
  private boolean checkData() {
    LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
    tLPGrpEdorItemDB.setSchema(mLPGrpEdorItemSchema);
    if (!tLPGrpEdorItemDB.getInfo())
    {
      System.out.println("GEdorDetailBL无保全申请数据");
      this.mErrors.addOneError("无保全申请数据");
      return false;
    }

    mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemDB.getSchema());
    if (tLPGrpEdorItemDB.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
    {
        System.out.println("GEdorDetailBL该保全已经保全理算不能修改!");
        this.mErrors.addOneError("该保全已经保全理算不能修改!");
        return false;
    }

    return true;
  }

  /**
   * 减人：若选择了主被保人，则连带被保人也一起选择
   * @return boolean
   */
  private boolean dealMainInsured()
  {
      if(mLPInsuredSet == null)
      {
          mErrors.addOneError("请选择被保人");
          return false;
      }

      //得到完整的被保人信息
      if(!getLPInsuredInfo())
      {
          return false;
      }

      int insuredCount = mLPInsuredSet.size();
      for(int i = 1; i <= insuredCount; i++)
      {
          if(!mLPInsuredSet.get(i).getRelationToMainInsured()
             .equals(BQ.MAININSURED))
          {
             continue;
          }

          //若选择了主被保人
          LCInsuredDB tLCInsuredDB = new LCInsuredDB();
          tLCInsuredDB.setContNo(mLPInsuredSet.get(i).getContNo());
          LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
          for (int temp = 1; temp <= tLCInsuredSet.size(); temp++)
          {
              //同时减去连带被保人
              if ((tLCInsuredSet.get(temp).getInsuredNo())
                  .equals(mLPInsuredSet.get(i).getInsuredNo()))
              {
                  continue;
              }

              LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
              ref.transFields(tLPInsuredSchema, tLCInsuredSet.get(temp));
              tLPInsuredSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
              tLPInsuredSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
              tLPInsuredSchema.setOperator(mGlobalInput.Operator);
              tLPInsuredSchema.setModifyDate(currDate);
              tLPInsuredSchema.setModifyTime(currTime);
              tLPInsuredSchema.setMakeDate(currDate);
              tLPInsuredSchema.setMakeTime(currTime);
              mLPInsuredSet.add(tLPInsuredSchema);
          }
      }

      return true;
  }

  /**
   * 得到完整的被保人信息
   * @return boolean
   */
  private boolean getLPInsuredInfo()
  {
      for(int i = 1; i <= mLPInsuredSet.size(); i++)
      {
          LCInsuredDB aLCInsuredDB = new LCInsuredDB();
          aLCInsuredDB.setContNo(mLPInsuredSet.get(i).getContNo());
          aLCInsuredDB.setInsuredNo(mLPInsuredSet.get(i).getInsuredNo());
          if (!aLCInsuredDB.getInfo())
          {
              return false;
          }

          ref.transFields(mLPInsuredSet.get(i), aLCInsuredDB.getSchema());
          mLPInsuredSet.get(i).setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
          mLPInsuredSet.get(i).setEdorType(mLPGrpEdorItemSchema.getEdorType());
          mLPInsuredSet.get(i).setOperator(mGlobalInput.Operator);
          mLPInsuredSet.get(i).setModifyDate(currDate);
          mLPInsuredSet.get(i).setModifyTime(currTime);
          mLPInsuredSet.get(i).setMakeDate(currDate);
          mLPInsuredSet.get(i).setMakeTime(currTime);
      }

      return true;
  }

  /**
   * 处理整单退保
   */
  private LPEdorItemSchema dealContZT(LPEdorItemSchema tLPEdorItemSchema)
  {
      int selectInsuredCount = 0;
      LPEdorItemSchema pLPEdorItemSchema = tLPEdorItemSchema.getSchema();

      //该团单下的所有被保人
      LCInsuredDB tLCInsuredDB = new LCInsuredDB();
      tLCInsuredDB.setContNo(pLPEdorItemSchema.getContNo());
      LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
      int insuredCount = tLCInsuredSet.size();

      for(int i = 1; i <= tLCInsuredSet.size(); i++)
      {
          for(int t = 1; t <= mLPInsuredSet.size(); t++)
          {
              if(tLCInsuredSet.get(i).getInsuredNo()
                 .equals(mLPInsuredSet.get(t).getInsuredNo()))
              {
                  selectInsuredCount++;
              }
          }
      }

      //所有被保人都选择即为整单退保
      if(insuredCount == selectInsuredCount)
      {
          pLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
      }

      return pLPEdorItemSchema;
  }

  /**
   * 准备需要保存的数据
   * @return
   */
  private boolean prepareData()
  {
      //减人：若选择了主被保人，则连带被保人也一起选择
      if(mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_ZT))
      {
          if(!dealMainInsured())
          {
              return false;
          }
          if(!dealLPPol())
          {
              return false;
          }
      }
      //按个人保全主表进行处理
      for (int i = 1; i <= mLPEdorItemSet.size(); i++)
      {
          String edorState;  //个人保全项目状态
          LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSet.get(i);
          String edorValiDate = tLPEdorItemSchema.getEdorValiDate();

          if (mLPGrpEdorItemSchema.getEdorType().equals(BQ.EDORTYPE_ZT))
          {
              tLPEdorItemSchema = dealContZT(tLPEdorItemSchema);
              edorState = BQ.EDORSTATE_INPUT;
          }
          else
          {
              edorState = BQ.EDORSTATE_INIT;
          }

          ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
          tLPEdorItemSchema.setPolNo("000000");
          tLPEdorItemSchema.setEdorState(edorState); //未录入
          if(edorValiDate != null)
          {
              tLPEdorItemSchema.setEdorValiDate(edorValiDate);
          }
          tLPEdorItemSchema.setManageCom(mGlobalInput.ManageCom);
          tLPEdorItemSchema.setOperator(mGlobalInput.Operator);
          tLPEdorItemSchema.setUWFlag("0");
          tLPEdorItemSchema.setMakeDate(currDate);
          tLPEdorItemSchema.setMakeTime(currTime);
          tLPEdorItemSchema.setModifyDate(currDate);
          tLPEdorItemSchema.setModifyTime(currTime);

          saveLPEdorItemSet.add(tLPEdorItemSchema);

          LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
          tLPEdorMainDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
          tLPEdorMainDB.setContNo(tLPEdorItemSchema.getContNo());
          LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
          tLPEdorMainSet = tLPEdorMainDB.query();
          if (tLPEdorMainDB.mErrors.needDealError())
          {
              CError.buildErr(this, "查询个人保全主表失败!");
              return false;
          }
          if (tLPEdorMainSet.size() == 0)
          {
              LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
              ref.transFields(tLPEdorMainSchema, tLPEdorItemSchema);
              tLPEdorMainSchema.setManageCom(mGlobalInput.ManageCom);
              tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
              tLPEdorMainSchema.setUWState("0");
              tLPEdorMainSchema.setEdorState(edorState);
              tLPEdorMainSchema.setMakeDate(currDate);
              tLPEdorMainSchema.setMakeTime(currTime);
              tLPEdorMainSchema.setModifyDate(currDate);
              tLPEdorMainSchema.setModifyTime(currTime);
              saveLPEdorMainSet.add(tLPEdorMainSchema);
          }

      }
      map.put(saveLPEdorItemSet, "DELETE&INSERT");
      map.put(saveLPEdorMainSet, "DELETE&INSERT");
      map.put(mLPInsuredSet, "DELETE&INSERT");
      for (int i = 1; i <= mLPEdorItemSet.size(); i++) {
          map.put("delete from LPPol where edorNo = '"
              + mLPGrpEdorItemSchema.getEdorNo()
              + "'  and edorType = '"
              + mLPGrpEdorItemSchema.getEdorType() + "' and contno ='"
              +mLPEdorItemSet.get(i).getContNo()+"'",
              "DELETE");
      }
      map.put(mLPPolSet, "INSERT");

      mResult.clear();
      mResult.add(map);

      return true;
  }

  /**
   * 生成退保险种的信息
   * @return boolean
   */
  private boolean dealLPPol()
  {
      mLPPolSet = new LPPolSet();
      for(int i = 1; i <= mLPInsuredSet.size(); i++)
      {
          LCPolDB tLCPolDB = new LCPolDB();
          tLCPolDB.setContNo(mLPInsuredSet.get(i).getContNo());
          tLCPolDB.setInsuredNo(mLPInsuredSet.get(i).getInsuredNo());
          LCPolSet tLCPolSet = tLCPolDB.query();
          for(int temp = 1; temp <= tLCPolSet.size(); temp++)
          {
              LPPolSchema tLPPolSchema = new LPPolSchema();;
              ref.transFields(tLPPolSchema, tLCPolSet.get(temp));
              tLPPolSchema.setEdorNo(mLPInsuredSet.get(i).getEdorNo());
              tLPPolSchema.setEdorType(mLPInsuredSet.get(i).getEdorType());
              tLPPolSchema.setOperator(mGlobalInput.Operator);
              tLPPolSchema.setModifyDate(currDate);
              tLPPolSchema.setModifyTime(currTime);
              tLPPolSchema.setMakeDate(currDate);
              tLPPolSchema.setMakeTime(currTime);

              mLPPolSet.add(tLPPolSchema);
          }
      }

      return true;
  }

  /**
   * 准备需要保存的数据
   * @return
   */
  private boolean deleteData()
  {
      String cotnNoStr = "";
      //按个人保全主表进行处理
      for (int i = 1; i <= mLPEdorItemSet.size(); i++)
      {
          if (i != mLPEdorItemSet.size())
          {
              cotnNoStr = cotnNoStr + "'" + mLPEdorItemSet.get(i).getContNo()
                          + "',";
          }
          else
              cotnNoStr = cotnNoStr + "'" + mLPEdorItemSet.get(i).getContNo()
                          + "'";
      }
      //删除个人批改项目
      String sql = "delete from LPEdorItem where edorno='"
                   + mLPGrpEdorItemSchema.getEdorNo()
                   + "' and edortype='" + mLPGrpEdorItemSchema.getEdorType()
                   + "' and ContNo in ("
                   + cotnNoStr + ")";
      map.put(sql, "UPDATE");
      //当个人批单主表没有批改项目时需要删掉个人批改主表
      String sql1 = "delete from LPEdorMain a where a.edorno='"
                    + mLPGrpEdorItemSchema.getEdorNo()
                    + "' and a.contno in ("
                    + cotnNoStr
          + ") and 0=(select count(1) from lpedoritem b where b.contno=a.contno "
                    + "and b.edorNo='" + mLPGrpEdorItemSchema.getEdorNo()
                    + "') ";
      map.put(sql1, "UPDATE");

      mResult.clear();
      mResult.add(map);

      return true;
  }

}
