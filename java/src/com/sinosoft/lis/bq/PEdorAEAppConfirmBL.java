package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 保全理算</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorAEAppConfirmBL implements EdorAppConfirm
{
    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getInputData(cInputData);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到输入数据
     * @return boolean
     */
    private void getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.
                getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) cInputData.
                getObjectByObjectName("LPEdorItemSchema", 0);
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }
}
