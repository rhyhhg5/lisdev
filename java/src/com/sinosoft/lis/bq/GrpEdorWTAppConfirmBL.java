package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 对保全明细录入的数据进行预算，得到客户交退费信息</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Yang Yalin
 * @version 1.3
 */
public class GrpEdorWTAppConfirmBL implements EdorAppConfirm
{
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;  //保全项目信息
    private LJSGetEndorseSchema mLJSGetEndorseSchema = null;
    private LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();  //保全部退费信息
    private GlobalInput mGI = null;  //完整的操作员信息
    private LPPENoticeSet mLPPENoticeSet = null;
    private BqCalBL mBqCalBL = new BqCalBL();  //生成财务数据的类
    private String mFeeFinaType = null;  //退费类型
    private String mGBFee = null;  //工本费

    private MMap map = new MMap();

    public GrpEdorWTAppConfirmBL()
    {
    }

    /**
     * 操作的提交方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象，包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LPGrpEdorItem对象，保全项目信息。
     * @param cOperate 数据操作字符串，主要包括""和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到处理后的数据集
     * @return VData：处理后的数据集，失败为null
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);

        return data;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                               .getObjectByObjectName("LPGrpEdorItemSchema", 0);
        mGI = ((GlobalInput) cInputData.
                               getObjectByObjectName("GlobalInput", 0));
        if (mLPGrpEdorItemSchema == null || mGI == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
        if (tLPGrpEdorItemSet == null || tLPGrpEdorItemSet.size() != 1)
        {
            mErrors.addOneError(new CError("查询保全项目信息失败！"));
            System.out.println(tLPGrpEdorItemDB.mErrors);
            return false;
        }
        mLPGrpEdorItemSchema.setSchema(tLPGrpEdorItemSet.get(1));

        return true;
    }

    /**
     * 处理增加险种保全理算业务逻辑，对保全明细录入的数据进行预算，得到客户交退费信息：
     i.	按险种生成险种缴费信息LJSGetEndorse，若险种有加费，则同时生成加费的退费信息LJSGetEndore，为销售提数作准备。
     ii.生成工本费财务信息。
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if(!setPolFinFee())
        {
            return false;
        }

        //工本费
        if(!setGBFee())
        {
            return false;
        }

        map.put(mLJSGetEndorseSet, SysConst.DELETE_AND_INSERT);

        if(!dealGrpItemInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 更新保全项目信息
     * @return boolean：成功true，否则false
     */
    private boolean dealGrpItemInfo()
    {
        //主表信息更新
        String getMoney = getGetMoney();
        if(getMoney == null)
        {
            return false;
        }

        mLPGrpEdorItemSchema.setGetMoney(Double.parseDouble(getMoney)
                                         + Double.parseDouble(mGBFee == null ? "0" : mGBFee));
        mLPGrpEdorItemSchema.setOperator(mGI.Operator);
        mLPGrpEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLPGrpEdorItemSchema, SysConst.UPDATE);

        return true;
    }

    /**
     * 生成险种退费财务数据
     * @return boolean：成功true，否则false
     */
    private boolean setPolFinFee()
    {
        //现的到一个LPEdorItemSchema对象，为生成财务数据做准备
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);

        //得到财务类型
        mFeeFinaType = mBqCalBL.getFinType(
            mLPGrpEdorItemSchema.getEdorType(),
            "TF",
            mLPGrpEdorItemSchema.getGrpContNo());
        if(mFeeFinaType.equals(""))
        {
            this.mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码!");
            return false;
        }

        String sql = "  select * "
                     + "from LPPol "
                     + "where grpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo()
                     + "'  and edorType = '" + mLPGrpEdorItemSchema.getEdorType()
                     + "'  and edorNo = '" + mLPGrpEdorItemSchema.getEdorNo()
                     + "' order by polNo ";
        LPPolDB tLPPolDB = new LPPolDB();
        LPPolSet tLPPolSet = null;
        VData data = new VData();   //入库数据的容器

        int start = 1;
        int count = 10000; //每次取count条个险数据进行处理

        do
        {
            tLPPolSet = tLPPolDB.executeQuery(sql, start, count);
            for (int i = 1; i <= tLPPolSet.size(); i++)
            {
                LPPolSchema tLPPolSchema = tLPPolSet.get(i);
                setOnePolFinanceData(tLPEdorItemSchema, tLPPolSchema);
            }

            //若本次处理险种数 <= count，则所有数据一起提交，否则直接提交
//            if(tLPPolSet.size() <= count && start < count)
//            {
//                break;
//            }

            MMap tMMap = new MMap();
            tMMap.put(mLJSGetEndorseSet, SysConst.DELETE_AND_INSERT);
            data.clear();
            data.add(tMMap);
            PubSubmit p = new PubSubmit();
            if(!p.submitData(data, ""))
            {
                System.out.println(p.mErrors.getErrContent());
                CError tError = new CError();
                tError.moduleName = "GrpEdorWTAppConfirmBL";
                tError.functionName = "setLJSGetEndorse";
                tError.errorMessage = "生成财务数据失败";
                mErrors.addOneError(tError);
                return false;
            }

            mLJSGetEndorseSet = new LJSGetEndorseSet();  //用这种方式清空对象值
            start += count;
        }
        while(tLPPolSet.size() > 0);

        return true;
    }

    /**
     * 得到本次犹豫期退保总退保金
     * @return String：总退保金，失败null
     */
    private String getGetMoney()
    {
        String sql = "  select -sum(prem) "
                     + "from LPPol "
                     + "where grpContNo = '"
                     + mLPGrpEdorItemSchema.getGrpContNo() + "' "
                     + "   and edorType = '"
                     + mLPGrpEdorItemSchema.getEdorType() + "' "
                     + "   and edorNo = '"
                     + mLPGrpEdorItemSchema.getEdorNo() + "' ";
        String rs = new ExeSQL().getOneValue(sql);
        if(rs.equals("") || rs.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "GrpEdorWTAppConfirmBL";
            tError.functionName = "getGetMoney";
            tError.errorMessage = "查询险种退费失败";
            mErrors.addOneError(tError);
            return null;
        }
        return rs;
    }

    /**
     * 生成一条个险财务数据
     * @param basicFinanceData LJSGetEndorseSchema
     * @param tLPPolSchema LPPolSchema
     */
    private void setOnePolFinanceData(LPEdorItemSchema tLPEdorItemSchema,
                                      LPPolSchema tLPPolSchema)
    {
        double sumAddFee = getSumAddFee(tLPPolSchema.getPolNo());

        LJSGetEndorseSchema schema
            = mBqCalBL.initLJSGetEndorse(tLPEdorItemSchema, tLPPolSchema, null,
                                         mFeeFinaType,
                                         -(tLPPolSchema.getPrem() - sumAddFee),
                                         mGI);
        //若保费为0，schema.getMoney = -Double.MIN_VALUE，存库会报错，故进行如下处理
        if(Math.abs(schema.getGetMoney() - 0) < 0.00001)
        {
            schema.setGetMoney(0);
        }
        mLJSGetEndorseSet.add(schema);

        //销售提取佣金时，加费额不提佣金，故将退费拆分
        if (sumAddFee != Double.MIN_VALUE)
        {
            LJSGetEndorseSchema addFeeSchema = schema.getSchema();
            addFeeSchema.setPayPlanCode(BQ.PAYPLANCODE_ADDFEE);
            addFeeSchema.setGetMoney( -sumAddFee);
            mLJSGetEndorseSet.add(addFeeSchema);
        }

        mLJSGetEndorseSchema = schema;  //工本费财务数据结构使用险种退费结构
    }

    /**
     * 若有工本费信息，则增加工本费财务接口
     * @return boolean：成功true，否则false
     */
    private boolean setGBFee()
    {
        //查询工本费信息
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPGrpEdorItemSchema);
        tEdorItemSpecialData.query();
        LPEdorEspecialDataSet tLPEdorEspecialDataSet
            = tEdorItemSpecialData.getSpecialDataSet();
        for (int i = 1; i <= tLPEdorEspecialDataSet.size(); i++)
        {
            if (tLPEdorEspecialDataSet.get(i).getDetailType()
                .equals(BQ.DETAILTYPE_GB))
            {
                mGBFee = tLPEdorEspecialDataSet.get(i).getEdorValue();
                break;
            }
        }
        if (mGBFee != null)
        {
            LJSGetEndorseSchema schemaGB = mLJSGetEndorseSchema.getSchema();
            schemaGB.setGrpPolNo(BQ.FILLDATA);
            schemaGB.setContNo(BQ.FILLDATA);
            schemaGB.setInsuredNo(BQ.FILLDATA);
            schemaGB.setPolNo(BQ.FILLDATA);
            schemaGB.setRiskCode(BQ.FILLDATA);
            schemaGB.setDutyCode(BQ.FILLDATA);
            schemaGB.setPayPlanCode(BQ.FILLDATA);
            BqCalBL tBqCalBl = new BqCalBL();
            String feeFinaType = tBqCalBl.getFinType(
                    mLPGrpEdorItemSchema.getEdorType(),
                    "GB",
                    mLPGrpEdorItemSchema.getGrpContNo());
            if (feeFinaType.equals(""))
            {
                this.mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码!");
                return false;
            }
            schemaGB.setFeeFinaType(feeFinaType);
            schemaGB.setGetMoney(mGBFee);
            mLJSGetEndorseSet.add(schemaGB);
        }

        //更新项目表退费记录
        mLPGrpEdorItemSchema.setGetMoney(mLPGrpEdorItemSchema.getGetMoney()
                                         + mLJSGetEndorseSchema.getGetMoney());

        return true;
    }

    /**
     * 生成体检费信息
     * @return boolean
     */
    private boolean setTestFee()
    {
        LPPENoticeDB db = new LPPENoticeDB();
        db.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
        db.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        db.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        mLPPENoticeSet = db.query();
        //若没有体检信息，则不处理
        if(mLPPENoticeSet.size() == 0)
        {
            return true;
        }

        setLPPENoticeItemInfo();

        return true;
    }

    /**
     * 处理体检项信息
     */
    private void setLPPENoticeItemInfo()
    {
        map.put("  delete from LPPENoticeItem "
                + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo()
                + "'  and feeOperationType = '"
                + mLPGrpEdorItemSchema.getEdorType()
                + "'  and feeFinaType = '" + BQ.FEEFINATYPE_TJ
                + "'  and grpContNo = '"
                + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");

        LPPENoticeItemSet tLPPENoticeItemSet = new LPPENoticeItemSet();
        double tjMoney = 0; //体检费
        Reflections tReflections = new Reflections();

        for(int i = 1; i <= mLPPENoticeSet.size(); i++)
        {
            //体检通知下的所有体检项
            LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
            tLCPENoticeItemDB.setProposalContNo(
                    mLPPENoticeSet.get(i).getProposalContNo());
            tLCPENoticeItemDB.setPrtSeq(mLPPENoticeSet.get(i).getPrtSeq());
            LCPENoticeItemSet tLCPENoticeItemSet = tLCPENoticeItemDB.query();

            for(int j = 1; j <= tLCPENoticeItemSet.size(); j++)
            {
                LPPENoticeItemSchema schema = new LPPENoticeItemSchema();
                tReflections.transFields(schema, tLCPENoticeItemSet.get(j));

                schema.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
                schema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
                schema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
                schema.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
                tLPPENoticeItemSet.add(schema);

                //若是险种退保，需要校验体检信息与人是否为选中客户的
                //此处为整单退保
                LDTestPriceMgtDB db = new LDTestPriceMgtDB();
                db.setHospitCode(mLPPENoticeSet.get(i).getPEAddress());
                db.setMedicaItemCode(schema.getPEItemCode());

                LDTestPriceMgtSet set = db.query();
                tjMoney += Double.parseDouble(
                        set.get(1).getMedicaItemPrice());
            }
        }
        setTestMoney(tjMoney);
        map.put(tLPPENoticeItemSet, "INSERT");
    }

    /**
     * 生成体检费财务数据
     * @param tjMoney double
     * @return LJSGetEndorseSchema
     */
    private void setTestMoney(double tjMoney)
    {
        LJSGetEndorseSchema tjLJSGetEndorseSchema =
            mLJSGetEndorseSchema.getSchema();
        tjLJSGetEndorseSchema.setGetMoney(tjMoney);

        BqCalBL tBqCalBl = new BqCalBL();
        String feeFinaType = tBqCalBl.getFinType(
                "WT", "TJ", mLPGrpEdorItemSchema.getGrpContNo());
        if (feeFinaType.equals(""))
        {
            this.mErrors.addOneError("在LDCode1中缺少保全财务接口转换类型编码!");
            return;
        }
        tjLJSGetEndorseSchema.setFeeFinaType(feeFinaType);
        mLJSGetEndorseSet.add(tjLJSGetEndorseSchema);

        mLPGrpEdorItemSchema.setGetMoney(
                mLPGrpEdorItemSchema.getGetMoney()
                + tjLJSGetEndorseSchema.getGetMoney());
    }

    /**
     * 得到该险种的加费额，加费项的payPlanCode 为000000 + 两位流水号
     * @param polNo String
     * @return double
     */
    private double getSumAddFee(String polNo)
    {
        String sql = "  select sum(sumActuPayMoney) "
                     + "from LJAPayPerson a, LPPol b "
                     + "where a.polNo = b.polNo "
                     + "   and payPlanCode like '000000%' "
                     + "   and a.polNo = '" + polNo + "' ";
        String sumAddFee = new ExeSQL().getOneValue(sql);
        return(sumAddFee.equals("") || sumAddFee.equals("null"))
            ? Double.MIN_VALUE : Double.parseDouble(sumAddFee);
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "endor0";
        tGlobalInput.ManageCom = "86";

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo("20061025000004");
        tLPGrpEdorItemDB.setEdorType("WT");

        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLPGrpEdorItemDB.query().get(1));

        GrpEdorWTAppConfirmBL tGrpEdorWTAppConfirmBL = new
                GrpEdorWTAppConfirmBL();
        if (!tGrpEdorWTAppConfirmBL.submitData(tVData, "INSERT"))
        {
            System.out.println(tGrpEdorWTAppConfirmBL.mErrors.getErrContent());
        }

    }
}
