package com.sinosoft.lis.bq;

/**
 * <p>Title: Web业务
 * 团单退保项目明细
 * 得到GrpEdorCTDetailUI.java传入的数据，进行退保明细录入业务逻辑的处理。
 * </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpEdorCTDetailBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局基础数据 */
    private GlobalInput mGI = null;
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;
    private LPContPlanRiskSet mLPContPlanRiskSet = null;
    private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema = null;//存储工本费
    private LPContSet mLPContSet = null;
    private LPEdorMainSet mLPEdorMainSet = null;
    private LPEdorItemSet mLPEdorItemSet = null;
    private String mCrrDate = PubFun.getCurrentDate();
    private String mCrrTime = PubFun.getCurrentTime();
    
    private  boolean isULI= false;
    private MMap mMap = new MMap();//待提交的数据集
    //2006-11-20 qulq add 利息录入
    private LPEdorEspecialDataSet mLPEdorEspecialDataSet =null;



    public GrpEdorCTDetailBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"INSERT"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("qulq ctdetail");
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(mMap);

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(data, "")) //数据提交
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);
            return false;
        }
        System.out.println("GrpEdorCTDetailBL End PubSubmit");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) cInputData
                                  .getObjectByObjectName(
                                      "LPGrpEdorItemSchema", 0);
        mLPContPlanRiskSet = (LPContPlanRiskSet) cInputData
                             .getObjectByObjectName("LPContPlanRiskSet", 0);
        mLPEdorEspecialDataSchema = (LPEdorEspecialDataSchema) cInputData
                                    .getObjectByObjectName(
                                        "LPEdorEspecialDataSchema", 0);
        //set中为输入的利息信息 2006-11-20 qulq
        mLPEdorEspecialDataSet = (LPEdorEspecialDataSet)cInputData
                                 .getObjectByObjectName("LPEdorEspecialDataSet",0);
        if (mGI == null || mLPGrpEdorItemSchema == null)
        {
            this.mErrors.addOneError("接收数据失败");
            return false;
        }
        
        jugdeUliByGrpContNo();//这边判断该团单是团体万能保单还是其他保单 如果isULI为true则为团体万能保单
        if(!isULI){ //判断如果是团体万能，则不走这个流程
        if(mLPContPlanRiskSet == null || mLPContPlanRiskSet.size() == 0)
        {
            mErrors.addOneError( "请选择保障计划下险种进行退保。");
            return false;
        }
        }

        return true;
    }

    /**
     * 校验当前状态是否可进行明细录入
     * @return boolean
     */
    private boolean checkData()
    {
        String reasonCode = mLPGrpEdorItemSchema.getReasonCode();  //退保原因

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();

        if (tLPGrpEdorItemSet.size() < 1)
        {
            mErrors.addOneError("输入数据有误,LPGrpEdorItem中没有相关数据");
            return false;
        }
        else
        {
            mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
        }
        mLPGrpEdorItemSchema.setReasonCode(reasonCode);

        if(!(mLPGrpEdorItemSchema.getEdorState().equals("3")
           || mLPGrpEdorItemSchema.getEdorState().equals("1")))
        {
            mErrors.addOneError("保全项目，不能录入。");
            return false;
        }

        //检验主险下附加险是否选择
        //对于团体万能不对他进行更新，没有附加险的情况
        if(!isULI){
        if (!checkMainPol()) {
            return false;
        }

        //若选择了特需险的公共账户保障计划，则需选择特需被保人的所有保障计划
        //若现在了特需险的所有被保人保障计划，则需选择公共账户的保障计划
        for(int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            if(!CommonBL.isEspecialPol(mLPContPlanRiskSet.get(i).getRiskCode())
            		&& !CommonBL.isTDBCPol(mLPContPlanRiskSet.get(i).getRiskCode()))
            {
                continue;
            }
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(mLPContPlanRiskSet.get(i)
                                           .getGrpContNo());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i)
                                          .getRiskCode());
            LCContPlanRiskSet set = tLCContPlanRiskDB.query(); //特需险保障计划数
            int selectedPlanCount = 0; //选择了的当前保障计划险种下的保障计划数
            //本保障是否公共帐户保障
            boolean isPublicAccPlan = mLPContPlanRiskSet.get(i).getContPlanCode()
                                      .equals("11");
            boolean hasPublicAccPlan = false;  //当前选择的保障计划是否包含公共帐户保障计划
            //得到险种下所有被选择的保障计划数，并判断当前保障计划是否公共帐户保障计划
            for(int t = 1; t <= mLPContPlanRiskSet.size(); t++)
            {
                if(!mLPContPlanRiskSet.get(i).getRiskCode()
                   .equals(mLPContPlanRiskSet.get(t).getRiskCode())) //只处理当前险种
                {
                    continue;
                }
                if(mLPContPlanRiskSet.get(t).getContPlanCode().equals("11"))
                {
                    hasPublicAccPlan = true;
                }
                selectedPlanCount++;
            }
            if (isPublicAccPlan && selectedPlanCount != set.size())
            {
                mErrors.addOneError("您选择了险种公共账户保障计划，"
                                    + "请同时选择所有被保人保障计划。");
                return false;
            }
            //特需险种保障计划数 = 被保人保障计划数 + 1(公共帐户保障计划数)
            if(!isPublicAccPlan && !hasPublicAccPlan
               && set.size() == selectedPlanCount + 1)
            {
                mErrors.addOneError("您选择了所有险种的被保人保障计划，"
                                    + "请同时选择公共账户保障计划。");
                return false;
            }
        }
        }
        return true;
    }
     //qulq 061227 添加 不能单独对主险解约，需要将其附加险一起解约。
    private boolean checkMainPol()
    {
        StringBuffer error = new StringBuffer();
        for(int i =1;i<=mLPContPlanRiskSet.size();i++)
        {
            LPContPlanRiskSchema tLPContPlanRiskSchema = mLPContPlanRiskSet.get(i);
            //如果是主险就校验其下的附加险是否全部选择
            if(tLPContPlanRiskSchema.getMainRiskCode().equals(tLPContPlanRiskSchema.getRiskCode()))
            {
                //取得该主险下所有的附加险。
                LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
                tLCContPlanRiskDB.setGrpContNo(tLPContPlanRiskSchema.getGrpContNo());
                tLCContPlanRiskDB.setContPlanCode(tLPContPlanRiskSchema.getContPlanCode());
                tLCContPlanRiskDB.setMainRiskCode(tLPContPlanRiskSchema.getMainRiskCode());
                LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
                if(tLCContPlanRiskSet.size()==0)
                {
                    mErrors.addOneError("查询保障计划错误");
                    return false;
                }
                for(int j =1;j<=tLCContPlanRiskSet.size();j++)
                {
                    int k =1;
                    for(;k<=mLPContPlanRiskSet.size();k++)
                    {
                        if(mLPContPlanRiskSet.get(k).getRiskCode().equals(tLCContPlanRiskSet.get(j).getRiskCode())
                                &&mLPContPlanRiskSet.get(k).getContPlanCode().equals(tLCContPlanRiskSet.get(j).getContPlanCode()))
                        {
                            break;
                        }
                    }
                    if(k>mLPContPlanRiskSet.size())
                    {
                        error.append("保障计划").append(tLCContPlanRiskSet.get(j).getContPlanCode())
                                .append("下的主险"+tLCContPlanRiskSet.get(j).getMainRiskCode()+"的附加险:")
                                .append(tLCContPlanRiskSet.get(j).getRiskCode()).append("没有选择");
                    }
                }
                if(!(error.toString()==null)&&!error.toString().equals(""))
                {
                    mErrors.addOneError(error.toString());
                    return false;
                }

            }

        }

        return true;
    }
    /**
     * 进行业务逻辑处理，存储页面传入的信息，同时将需要退保的保障计划下险种备份到LPPol表。
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
 //       dealGBInfo();//处理工本费
        //处理保障计划
    	if (!isULI) {
	   if(!dealContPlan())
        {
            return false;
        }	
		}
        if(!dealLPPol())
        {
            return false;
        }
        if(!dealLPInsured())     //添加被保人信息
        {
            return false;
        }
        if(!dealLPEdorItem())
        {
            return false;
        }
        //将利息信息保存 qulq 061120
        if(!rateInfo())
        {
           return false;
       }

        changeItemState(); //设置保全项目状态为录入完毕

        return true;
    }
    private boolean rateInfo()
    {
        //061128 qulq
        String sql ="delete from LPEdorEspecialData where edoracceptno ='"
                    +this.mLPGrpEdorItemSchema.getEdorNo()
                    +"' and edorno ='"
                    +this.mLPGrpEdorItemSchema.getEdorNo()
                    +"' and edortype ='"
                    +BQ.EDORTYPE_CT
                    +"'";
        MMap newmMap = new MMap();
        newmMap.put(sql,"DELETE");
        VData temp = new VData();
        temp.add(newmMap);
        new PubSubmit().submitData(temp,"DELETE");

        EdorItemSpecialData tEdorItemSpecialData =
                new EdorItemSpecialData(this.mLPGrpEdorItemSchema.getEdorNo(),
                                        this.mLPGrpEdorItemSchema.getEdorNo(),BQ.EDORTYPE_CT);
        if(mLPEdorEspecialDataSet==null||mLPEdorEspecialDataSet.size()<1)
        {
            return true;
        }
        for(int i=1;i<=mLPEdorEspecialDataSet.size();i++)
        {
            tEdorItemSpecialData.setGrpPolNo(mLPEdorEspecialDataSet.get(i).getPolNo());
            tEdorItemSpecialData.add(mLPEdorEspecialDataSet.get(i).getDetailType(),
                                    mLPEdorEspecialDataSet.get(i).getEdorValue());
        }
        if(!tEdorItemSpecialData.insert())
        {
            this.mErrors.addOneError("录入mLPEdorEspecialData错误");
            return false;
        }

      return true;
    }
    private boolean dealLPEdorItem()
    {
     if(!createLPEdorInfo())
     {
         mErrors.addOneError("处理个单保全失败");
            return false;
     }
     return true;
    }
    private boolean createLPEdorInfo()
    {
        //生成被保人保全数据
        if(!deleteOldLPEdorInfo())
        {
            return false;
        }

        String contNos = getOneFieldValues("LCCont", "contNo", "");
        if(contNos.equals(""))
        {
            return false;
        }
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.* ")
            .append("from LCCont a ")
            .append("where a.contNo in (").append(contNos).append(")");
        System.out.println(sql);

        mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorMainSet = new LPEdorMainSet();
        LCContDB tLCContDB = new LCContDB();
        mLPContSet = new LPContSet();
        LCContSet set = null;
        int start= 1;
        int count = 100;

        do
        {
            set = tLCContDB.executeQuery(sql.toString(), start, count);
            for (int i = 1; i <= set.size(); i++)
            {
                LCContSchema tLCContSchema = set.get(i);
                if(!createOneLPEdorInfo(tLCContSchema))
                {
                    return false;
                }
            }
            start += count;
        }
        while(set.size() > 0);
        mMap.put(mLPEdorItemSet,"INSERT");
        mMap.put(mLPEdorMainSet,"INSERT");
        mMap.put(mLPContSet,"INSERT");
        return true;
    }
    private String getOneFieldValues(String table,
                                     String field,
                                     String condition)
    {
        StringBuffer sql = new StringBuffer();
        
        if(!isULI){
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            sql.append("select ").append(table).append(".").append(field)
                .append(" from LCCont, LCInsured , LCPol ")
                .append("where LCCont.contNo = LCInsured.contNo ")
                .append("   and LCInsured.contNo = LCPol.contNo ")
                .append("   and LCInsured.insuredNo = LCPol.insuredNo ")
                .append("   and LCInsured.contPlanCode = '")
                .append(mLPContPlanRiskSet.get(i).getContPlanCode())
                .append("'   and LCPol.riskCode = '")
                .append(mLPContPlanRiskSet.get(i).getRiskCode())
                .append("'   and LCCont.grpContNo = '")
                .append(this.mLPGrpEdorItemSchema.getGrpContNo())
                .append("' ")
                .append(condition)
                .append(" union ");
        }
        }else {
        	sql.append("select ").append(table).append(".").append(field)
            .append(" from LCCont, LCInsured , LCPol ")
            .append("where LCCont.contNo = LCInsured.contNo ")
            .append("   and LCInsured.contNo = LCPol.contNo ")
            .append("   and LCInsured.insuredNo = LCPol.insuredNo ")
            .append("  and LCCont.grpContNo = '")
            .append(this.mLPGrpEdorItemSchema.getGrpContNo())
            .append("' ")
            .append(condition)
            .append(" union ");	
		}
        SSRS tSSRS = null;
        try
        {
            String fieldValueSql = sql.substring(0,
                                                 sql.lastIndexOf("union") - 1);
            System.out.println(fieldValueSql);

            ExeSQL e = new ExeSQL();
            tSSRS = e.execSQL(fieldValueSql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (tSSRS == null || tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("查询出错。");
            return "";
        }

        StringBuffer fieldValues = new StringBuffer();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            fieldValues.append("'").append(tSSRS.GetText(i, 1)).append("',");
        }
        if (fieldValues.length() == 0)
        {
            return "";
        }
        return fieldValues.toString().substring(0, fieldValues.lastIndexOf(","));
    }
    private boolean createOneLPEdorInfo(LCContSchema tLCContSchema)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPEdorItemSchema, mLPGrpEdorItemSchema);
        tLPEdorItemSchema.setContNo(tLCContSchema.getContNo());
        tLPEdorItemSchema.setInsuredNo(tLCContSchema.getInsuredNo());
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setManageCom(mGI.ManageCom);
        tLPEdorItemSchema.setOperator(mGI.Operator);
        tLPEdorItemSchema.setUWFlag("0");
        tLPEdorItemSchema.setMakeDate(mCrrDate);
        tLPEdorItemSchema.setMakeTime(mCrrTime);
        tLPEdorItemSchema.setModifyDate(mCrrDate);
        tLPEdorItemSchema.setModifyTime(mCrrTime);
        tLPEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPEdorItemSet.add(tLPEdorItemSchema);

        LPContSchema tLPContSchema = new LPContSchema();
        ref.transFields(tLPContSchema, tLCContSchema);
        tLPContSchema.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
        tLPContSchema.setEdorType(mLPGrpEdorItemSchema.getEdorType());
        tLPContSchema.setMakeDate(mCrrDate);
        tLPContSchema.setMakeTime(mCrrTime);
        tLPContSchema.setModifyDate(mCrrDate);
        tLPContSchema.setModifyTime(mCrrTime);
        mLPContSet.add(tLPContSchema);

        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        ref.transFields(tLPEdorMainSchema, mLPGrpEdorItemSchema);
        tLPEdorMainSchema.setContNo(tLCContSchema.getContNo());
        tLPEdorMainSchema.setManageCom(mGI.ManageCom);
        tLPEdorMainSchema.setOperator(mGI.Operator);
        tLPEdorMainSchema.setUWState("0");
        tLPEdorMainSchema.setEdorState("1");
        tLPEdorMainSchema.setMakeDate(mCrrDate);
        tLPEdorMainSchema.setMakeTime(mCrrTime);
        tLPEdorMainSchema.setModifyDate(mCrrDate);
        tLPEdorMainSchema.setModifyTime(mCrrTime);
        mLPEdorMainSet.add(tLPEdorMainSchema);

        return true;
    }

    private boolean deleteOldLPEdorInfo()
    {
        mMap.put("  delete from LPCont "
                + "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                + "  and edorType = '" + this.mLPGrpEdorItemSchema.getEdorType() + "' "
                + "  and grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
        mMap.put("  delete from LPEdorMain "
                + "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo()  + "' "
                + "  and contNo in ("
                + "      select distinct contNo "
                + "      from LCCont "
                + "      where grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "') ", "DELETE");
        mMap.put("  delete from LPEdorItem "
                + "where edorNo = '" + this.mLPGrpEdorItemSchema.getEdorNo() + "' "
                + "  and edorType = '" + this.mLPGrpEdorItemSchema.getEdorType()+ "' "
                + "  and grpContNo = '" + this.mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");

        return true;
    }


    /**
     *
     * @return boolean
     */
    private boolean dealLPInsured()
    {
        mMap.put("  delete from LPInsured "
         + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
         + "  and edorType = '" +mLPGrpEdorItemSchema.getEdorType() + "' "
         + "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
        if(isULI){
        	mMap.put("  delete from LPInsured p "
                    + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                    + "  and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
                    + "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() +
                    "' AND p.InsuredNO IN (SELECT a.InsuredNO FROM lcInsured a ,lcpol b where a.ContNo =b.ContNo "
                   +" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
                   + mLPGrpEdorItemSchema.getGrpContNo()
                   +"')", "DELETE");

           mMap.put("insert into LPInsured (select '"
                       +this.mLPGrpEdorItemSchema.getEdorNo()
                       +"','"
                       +this.mLPGrpEdorItemSchema.getEdorType()
                       +"',a.* from lcInsured a ,lcpol b where a.ContNo =b.ContNo "
                       +" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
                       + mLPGrpEdorItemSchema.getGrpContNo()
                        + "')","INSERT");
        }else{
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
        //qulq 061122 修改一个被保人可在两险下时的BUG，生成记录错误
        mMap.put("  delete from LPInsured p "
                 + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                 + "  and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
                 + "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() +
                 "' AND p.InsuredNO IN (SELECT a.InsuredNO FROM lcInsured a ,lcpol b where a.ContNo =b.ContNo "
                +" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
                + mLPContPlanRiskSet.get(i).getGrpContNo()
                + "'  and b.contPlanCode = '"
                + mLPContPlanRiskSet.get(i).getContPlanCode()
                + "'  and b.riskCode = '"
                + mLPContPlanRiskSet.get(i).getRiskCode()
                +"')", "DELETE");

        mMap.put("insert into LPInsured (select '"
                    +this.mLPGrpEdorItemSchema.getEdorNo()
                    +"','"
                    +this.mLPGrpEdorItemSchema.getEdorType()
                    +"',a.* from lcInsured a ,lcpol b where a.ContNo =b.ContNo "
                    +" and a.InsuredNo =b.InsuredNo and b.grpContNo = '"
                     + mLPContPlanRiskSet.get(i).getGrpContNo()
                     + "'  and b.contPlanCode = '"
                     + mLPContPlanRiskSet.get(i).getContPlanCode()
                     + "'  and b.riskCode = '"
                     + mLPContPlanRiskSet.get(i).getRiskCode()
                     + "')","INSERT");
       }
        }
       updateDefaultFields("LPInsured");
        return true;
    }


    /**
     *
     * @return boolean
     */
    private boolean dealContPlan()
    {
        mMap.put("  delete from LPContPlanRisk "
                 + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                 + "  and edorType = '" +mLPGrpEdorItemSchema.getEdorType() + "' "
                 + "  and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
        String sql = "";
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        for (int i = 1; i <= this.mLPContPlanRiskSet.size(); i++)
        {
            tLCContPlanRiskDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo());
            tLCContPlanRiskDB.setContPlanCode(
                mLPContPlanRiskSet.get(i).getContPlanCode());
            tLCContPlanRiskDB.setRiskCode(mLPContPlanRiskSet.get(i).getRiskCode());

            LCContPlanRiskSet set = tLCContPlanRiskDB.query();
            if(set.size() == 0)
            {
                mErrors.addOneError("没有查询到相应的保障计划。"
                    + tLCContPlanRiskDB.getContPlanCode() + " "
                    + tLCContPlanRiskDB.getRiskCode());
                return false;
            }
            sql = "  insert into LPContPlanRisk "
                  + "   ( select '" + mLPGrpEdorItemSchema.getEdorNo() + "', "
                  + "   '" + mLPGrpEdorItemSchema.getEdorType() + "', "
                  + "   LCContPlanRisk.* "
                  + "   from LCContPlanRisk "
                  + "   where grpContNo = '" + set.get(1).getGrpContNo()
                  + "'     and contPlanCode = '"
                  + set.get(1).getContPlanCode()
                  + "'     and riskCode = '" + set.get(1).getRiskCode()
                  + "') ";
            mMap.put(sql, "INSERT");
        }
        updateDefaultFields("LPContPlanRisk");
        return true;
    }

    private void updateDefaultFields(String tableName)
    {
        mMap.put("  update " + tableName
              + " set operator = '" + mGI.Operator + "', "
              + "    makeDate = '" + mCrrDate + "', "
              + "    makeTime = '" + mCrrTime + "', "
              + "    modifyDate = '" + mCrrDate + "', "
              + "    modifyTime = '" + mCrrTime + "' "
              + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
              + "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
              + "   and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "INSERT");
    }

    /**
     * 生成保全险种信息
     * @return boolean
     */
    private boolean dealLPPol()
    {
        mMap.put("  delete from LPPol "
                 + "where edorNo = '" + mLPGrpEdorItemSchema.getEdorNo() + "' "
                 + "   and edorType = '" + mLPGrpEdorItemSchema.getEdorType() + "' "
                 + "   and grpContNo = '" + mLPGrpEdorItemSchema.getGrpContNo() + "' ", "DELETE");
        //通过保单号
        if(isULI){
        	 mMap.put("insert into LPPol "
                     + "(select '" + mLPGrpEdorItemSchema.getEdorNo()
                     + "', '" + mLPGrpEdorItemSchema.getEdorType() + "', LCPol.* "
                     + "from LCPol "
                     + "where grpContNo = '"
                     +mLPGrpEdorItemSchema.getGrpContNo()
                      + "') ", "INSERT");
        }else{
        for (int i = 1; i <= mLPContPlanRiskSet.size(); i++)
        {
            mMap.put("insert into LPPol "
                     + "(select '" + mLPGrpEdorItemSchema.getEdorNo()
                     + "', '" + mLPGrpEdorItemSchema.getEdorType() + "', LCPol.* "
                     + "from LCPol "
                     + "where grpContNo = '"
                     + mLPContPlanRiskSet.get(i).getGrpContNo()
                     + "'  and contPlanCode = '"
                     + mLPContPlanRiskSet.get(i).getContPlanCode()
                     + "'  and riskCode = '"
                     + mLPContPlanRiskSet.get(i).getRiskCode() + "') ", "INSERT");
        }
        }
        updateDefaultFields("LPPol");
        return true;
    }

    /**
     * 存储工本费信息
     */
    private void dealGBInfo()
    {
        mMap.put("delete from LPEdorEspecialData "
                 + "where edorAcceptNo = '"
                 + mLPGrpEdorItemSchema.getEdorAcceptNo()
                 + "'  and edorType ='" + mLPGrpEdorItemSchema.getEdorType()
                 + "'  and detailType = '" + BQ.DETAILTYPE_GB + "' ",
                 "DELETE");

        if(mLPEdorEspecialDataSchema == null)
        {
            return;
        }
        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(mLPGrpEdorItemSchema);
        tEdorItemSpecialData.add(BQ.DETAILTYPE_GB,
                                 mLPEdorEspecialDataSchema.getEdorValue());
        mMap.put(tEdorItemSpecialData.getSpecialDataSet(), "INSERT");
    }

    /**
     * 项目状态为录入完毕
     */
    private void changeItemState()
    {
        mLPGrpEdorItemSchema.setEdorState(BQ.EDORSTATE_INPUT);
        mLPGrpEdorItemSchema.setOperator(mGI.Operator);
        mLPGrpEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
        mLPGrpEdorItemSchema.setMakeTime(PubFun.getCurrentTime());
        mMap.put(mLPGrpEdorItemSchema, "UPDATE");
    }
    
    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(this.mLPGrpEdorItemSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
        	System.out.println("此团单不是团体万能");
            return false;
        }
        System.out.println("此团单是团体万能");
        isULI=true;
        return true;
    }
    

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        String edorNo = "20061111000002";
        String edorType = "CT";
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorAcceptNo(edorNo);
        tLPGrpEdorItemSchema.setEdorNo(edorNo);
        tLPGrpEdorItemSchema.setGrpContNo(args[0]);
        tLPGrpEdorItemSchema.setEdorType(edorType);
        tLPGrpEdorItemSchema.setReasonCode("qulq");
        LPContPlanRiskSchema tLPContPlanRiskSchema = new LPContPlanRiskSchema();
        tLPContPlanRiskSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
        tLPContPlanRiskSchema.setEdorType(tLPGrpEdorItemSchema.getEdorType());
        tLPContPlanRiskSchema.setGrpContNo(tLPGrpEdorItemSchema.getGrpContNo());
        tLPContPlanRiskSchema.setContPlanCode("B");
        tLPContPlanRiskSchema.setRiskCode("1603");
        LPContPlanRiskSet tLPContPlanRiskSet = new LPContPlanRiskSet();
        tLPContPlanRiskSet.add(tLPContPlanRiskSchema);

        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        VData data = new VData();
        data.add(tLPGrpEdorItemSchema);
        data.add(gi);
        data.add(tLPContPlanRiskSet);


        GrpEdorCTDetailBL bl = new GrpEdorCTDetailBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("haha qulq is good");
        }
    }
    
    
    
}
