package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class AddPEdorItemBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();
    TransferData tTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 业务处理相关变量 */
    private LPEdorMainSet mLPEdorMainSet = new LPEdorMainSet();
    private LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    private LLContDealSchema mLLContDealSchema = new LLContDealSchema();

    private DisabledManageBL tDisabledManageBL = new DisabledManageBL();
    private GlobalInput mGlobalInput = new GlobalInput();
    String displayType = "";
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;
    private String mManageCom = null;

    // @Constructor
    public AddPEdorItemBL()
    {}

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitData(cInputData, cOperate) == null)
        {
            return false;
        }

        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "PEdorAppItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 处理数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitData(VData cInputData, String cOperate)
    {
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData())
        {
            return null;
        }

       

        if (!dealData())
        {
            return null;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        return map;

    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.
                       getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSet = (LPEdorItemSet) mInputData.
                         getObjectByObjectName("LPEdorItemSet", 0);
        mLLContDealSchema = (LLContDealSchema)mInputData.
                         getObjectByObjectName("LLContDealSchema", 0);

        if (mLPEdorItemSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PEdorAppItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到个人批改项目表!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mEdorNo = mLPEdorItemSet.get(1).getEdorAcceptNo();
        mEdorType = mLPEdorItemSet.get(1).getEdorType();
        mContNo = mLPEdorItemSet.get(1).getContNo();
        mManageCom = mLPEdorItemSet.get(1).getManageCom();
        return true;
    }



    /**
     * 判断是否是续期催收过程之中
     * @return boolean true 正在催收， false 已核销
     */
    private boolean checkDueFee()
    {
        String sql = "select * from LJSPay " +
                     "where OtherNoType = '2' " + //2是个单续期
                     "and OtherNo = '" + mContNo + "' ";
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0)
        {
            mErrors.addOneError("该保单处于续期待收费状态，续期核销之后才能处理保全业务！");
            return false;
        }
        return true;
    }

    /**
     * 新契约保单打印之后才能做保全
     * @return boolean
     */
    private boolean checkPrintCount()
    {
        if (!mEdorType.equals("RB"))
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo())
            {
                mErrors.addOneError("未找到保单信息！");
                return false;
            }
            if (tLCContDB.getPrintCount() == 0 && !mEdorType.equals("WT"))
            {
                mErrors.addOneError("此保单未完成保单打印，保单打印之后才能处理保全业务！");
                return false;
            }
        } else
        {
            //保单可能做正单退保，所以不做新契约保单打印之后才能做保全的校验
            return true;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean checkEdorItem()
    {
        if (mEdorType.equals("TB"))
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            if (tLPEdorItemDB.query().size() > 0)
            {
                mErrors.addOneError("TB项目不能和其它项目一起做！");
                return false;
            }
        }
        if (mEdorType.equals("RB"))
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setContNo(mContNo);
        } else
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            tLPEdorItemDB.setEdorType("TB");
            if (tLPEdorItemDB.query().size() > 0)
            {
                mErrors.addOneError("TB项目不能和其它项目一起做！");
                return false;
            }
        }
        if (CommonBL.hasULIRisk(mContNo))
        {
        	//判断万能险非附加万能且附加万能险种的生效日期小于当前时间时
        	String uLISQL = "SELECT cvalidate FROM lcpol where contno='"+mContNo+"' and " +
        			" exists (select 1 from lmriskapp where riskcode =lcpol.riskcode and risktype4='4' ) " +
        			" and exists (select 1 from ldcode1 where code=lcpol.riskcode and codetype='mainsubriskrela') ";
        	String attachULIRisk = new ExeSQL().getOneValue(uLISQL);
        	if(attachULIRisk!=null&&!"".equals(attachULIRisk)){
        		if(CommonBL.stringToDate(attachULIRisk).after(CommonBL.stringToDate(PubFun.getCurrentDate()))){
        			return true;
        		}
        	}
            if(!checkULI())
            {
            	return false;
            }
        }
        return true;
    }

  
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData()
    {

            for (int i = 1; i <= mLPEdorItemSet.size(); i++)
            {
                if (mLPEdorItemSet.get(i).getEdorNo() == null ||
                    mLPEdorItemSet.get(i).getEdorNo().equals(""))
                {
                    //从本次生成的lpedormain集合中查找是否有该合同的批改主表，如果有，置edorno
                    for (int j = 1; j <= mLPEdorMainSet.size(); j++)
                    {
                        if (mLPEdorItemSet.get(i).getContNo().equals(
                                mLPEdorMainSet.get(j).getContNo()))
                        {
                            mLPEdorItemSet.get(i).setEdorNo(mLPEdorMainSet.get(
                                    j).getEdorNo());
                            mLPEdorItemSet.get(i).setEdorAppNo(mLPEdorMainSet.
                                    get(j).getEdorAppNo());
                            break;
                        }
                    }
                    //如果没有找到，从数据里查
                    if (mLPEdorItemSet.get(i).getEdorNo() == null ||
                        mLPEdorItemSet.get(i).getEdorNo().equals(""))
                    {
                        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
                        tLPEdorMainDB.setEdorAcceptNo(mLPEdorItemSet.get(i).
                                getEdorAcceptNo());
                        tLPEdorMainDB.setContNo(mLPEdorItemSet.get(i).getContNo());
                        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
                        if (tLPEdorMainDB.mErrors.needDealError())
                        {
                            CError.buildErr(this, "查询批改主表失败！");
                            return false;
                        } else
                        {
                            if (tLPEdorMainSet.size() > 0)
                            { //如果从数据库里查到，用数据库里的赋值
                                mLPEdorItemSet.get(i).setEdorNo(tLPEdorMainSet.
                                        get(1).getEdorNo());
                                mLPEdorItemSet.get(i).setEdorAppNo(
                                        tLPEdorMainSet.get(1).getEdorAppNo());
                            } else
                            { //如果没有，产生一条
                                LPEdorMainSchema tLPEdorMainSchema =
                                        new LPEdorMainSchema();
                                tLPEdorMainSchema.setEdorAcceptNo(
                                        mLPEdorItemSet.get(i).getEdorAcceptNo());
                                if (tLPEdorMainSchema.getEdorAppNo() == null ||
                                    tLPEdorMainSchema.getEdorAppNo().equals(
                                            ""))
                                {
                                    String strLimit = PubFun.getNoLimit(
                                    		mManageCom);
                                    String strEdorAppNo = PubFun1.CreateMaxNo(
                                            "EdorAppNo",
                                            strLimit);
                                    if (StrTool.compareString(strEdorAppNo, ""))
                                    {
                                        CError.buildErr(this, "生成保全申请号错误！");
                                        return false;
                                    } else
                                    {
                                        tLPEdorMainSchema.setEdorAppNo(
                                                strEdorAppNo);
                                        tLPEdorMainSchema.setEdorNo(
                                                strEdorAppNo);
                                        mLPEdorItemSet.get(i).setEdorNo(
                                                tLPEdorMainSchema.
                                                getEdorAcceptNo());
                                        mLPEdorItemSet.get(i).setEdorAppNo(
                                                tLPEdorMainSchema.
                                                getEdorAcceptNo());
                                    }
                                }
                                //受理号就是批单号
                                tLPEdorMainSchema.setEdorNo(tLPEdorMainSchema.
                                        getEdorAcceptNo());
                                tLPEdorMainSchema.setEdorAppNo(
                                        tLPEdorMainSchema.getEdorAcceptNo());
                                tLPEdorMainSchema.setContNo(mLPEdorItemSet.get(
                                        i).getContNo());
                                tLPEdorMainSchema.setEdorAppDate(mLPEdorItemSet.
                                        get(i).getEdorAppDate());
                                tLPEdorMainSchema.setEdorValiDate(
                                        mLPEdorItemSet.get(i).getEdorValiDate());
                                tLPEdorMainSchema.setEdorState("1");
                                tLPEdorMainSchema.setUWState("0");
                                tLPEdorMainSchema.setOperator(mGlobalInput.
                                        Operator);
                                tLPEdorMainSchema.setManageCom(mManageCom);
                                tLPEdorMainSchema.setMakeDate(theCurrentDate);
                                tLPEdorMainSchema.setMakeTime(theCurrentTime);
                                tLPEdorMainSchema.setModifyDate(theCurrentDate);
                                tLPEdorMainSchema.setModifyTime(theCurrentTime);
                                mLPEdorMainSet.add(tLPEdorMainSchema);
                            }
                        }
                    }
                }
                LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
                tLMEdorItemDB.setEdorCode(mLPEdorItemSet.get(i).getEdorType());
                tLMEdorItemDB.setAppObj("I");
                if (!tLMEdorItemDB.getInfo())
                {
                    CError.buildErr(this, "查询保全定义表失败！");
                    return false;
                }
                if ("0".equals(tLMEdorItemDB.getNeedDetail()))
                {
                    mLPEdorItemSet.get(i).setEdorState("1"); //不需要录入明细
                } else
                {
                    mLPEdorItemSet.get(i).setEdorState("3"); //申请待录入状态
                }
                //mLPEdorItemSet.get(i).setEdorValiDate(PubFun.getCurrentDate());
                //mLPEdorItemSet.get(i).setEdorAppDate(PubFun.getCurrentDate());
                mLPEdorItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLPEdorItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
                mLPEdorItemSet.get(i).setManageCom(mManageCom);
                mLPEdorItemSet.get(i).setOperator(mGlobalInput.Operator);
                mLPEdorItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLPEdorItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
            }

            if (mLLContDealSchema!=null) {
                if (mLLContDealSchema.getEdorNo()!=null&&mLLContDealSchema.getEdorNo()!="") {
                    LLContDealDB tLLContDealDB = new LLContDealDB();
                    tLLContDealDB.setSchema(mLLContDealSchema);
                    if (tLLContDealDB.getInfo()) {
                        LLContDealSchema tLLContDealSchema = tLLContDealDB.
                                getSchema();
                        tLLContDealSchema.setEdorType(mLLContDealSchema.
                                getEdorType());
                        tLLContDealSchema.setOperator(mGlobalInput.Operator);
                        tLLContDealSchema.setModifyDate(PubFun.getCurrentDate());
                        tLLContDealSchema.setModifyTime(PubFun.getCurrentTime());
                        map.put(tLLContDealSchema, "UPDATE");
                    }
                }
            }

            map.put(mLPEdorItemSet, "INSERT");
            map.put(mLPEdorMainSet, "INSERT");
        

        return true;
    }

    /**
     * 删除传入的险种
     * @param: 无
     * @return: void
     */
    private boolean deleteData()
    {

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData()
    {
        mResult.clear();
        mResult.add(mLPEdorItemSet);
        mResult.add(mLPEdorMainSet);

        mInputData.clear();
        mInputData.add(map);

    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult()
    {
        return mResult;
    }

//  校验在其它保单已存在保全项目状态下该保全项目能否添加 071206 zhanggm
    private boolean checkAddItem()
    {
        if (mLPEdorItemSet.size() == 0)
        {
            return true;
        } else
        {
            LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
            tLPEdorMainDB.setEdorNo(mEdorNo);
            tLPEdorMainDB.setEdorAcceptNo(mEdorNo);
            LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
            if (tLPEdorMainSet.size() == 0)
            {
                return true;
            } else
            {
                if (!mContNo.equals(tLPEdorMainSet.get(1).getContNo()))
                {
                    CError tError = new CError();
                    tError.moduleName = "PEdorItemBL";
                    tError.functionName = "checkAddItem";
                    tError.errorMessage = "一个工单只能对一个保单进行保全操作！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    //校验万能险 20090714 zhanggm
    //万能险保全生效日期不能早于已结算日期
    //一个万能保单只能同时做一个保全项目
    private boolean checkULI()
    {
    	String tsql = "";
    	if(!"BP".equals(mEdorType))
    	{
    		tsql = "select BalaDate from LCInsureAcc where Contno = '" + mContNo + "' ";
        	String tBalaDate = new ExeSQL().getOneValue(tsql);
        	if (PubFun.calInterval(tBalaDate,mLPEdorItemSet.get(1).getEdorValiDate(), "D") <0)
        	{
        		mErrors.addOneError("万能险保全生效日期不能早于已结算日期！本保单上次结算日期是" + tBalaDate);
            	return false;
        	}
    	}
    	tsql = "select edorno,(select min(edorname) from lmedoritem where edorcode = a.edortype) from lpedoritem a "
    		 + "where Contno = '" + mContNo + "' and exists (select 1 from lpedorapp "
    		 + "where edoracceptno = a.edoracceptno and edorstate != '0') ";
    	SSRS tSSRS = new ExeSQL().execSQL(tsql);   	
    	
    	if(tSSRS.MaxRow>0)
    	{
    		tsql="select edortype from lpedoritem where edoracceptno=" + "(select innersource from lgwork where workno='"+mEdorNo+"')";
    		String tEdorType = new ExeSQL().getOneValue(tsql);
    		if(tEdorType.equals("TB"))
        	{
    			return true;	
        	}
    		else
    		{
    			String error = "该保单存在保全项目-" + tSSRS.GetText(1, 2) + "，工单号：" + tSSRS.GetText(1, 1)
	             			+ "，请将该工单结案或撤销后再申请新的保全项目。";
    			mErrors.addOneError(error);
    			return false;
    		}
    	}

    	return true;
    }
}
