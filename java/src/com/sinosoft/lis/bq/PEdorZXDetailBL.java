package com.sinosoft.lis.bq;

//程序名称：PEdorLQDetailBL.java
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PEdorZXDetailBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = null; 
  
  private LPEdorItemSchema mLPEdorItemSchema = null;
  
  private MMap map = new MMap();

  private String currDate = PubFun.getCurrentDate();
  
  private String currTime = PubFun.getCurrentTime();
  
  private LPEdorEspecialDataSet mLPEdorEspecialDataSet=new LPEdorEspecialDataSet();

  public PEdorZXDetailBL() 
  {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) 
  {

    //将操作数据拷贝到本类中
    this.mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) 
    {
      return false;
    }

    if (!checkData()) 
    {
      return false;
    }

    //进行业务处理
    if (!dealData()) 
    {
      return false;
    }

    //准备往后台的数据
    if (!prepareData()) 
    {
      return false;
    }
    PubSubmit tSubmit = new PubSubmit();

    if (!tSubmit.submitData(mResult, "")) 
    { //数据提交
      // @@错误处理
      this.mErrors.copyAllErrors(tSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "PEdorZXDetailBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("PEdorZXDetailBL End PubSubmit");
    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    try {
      mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
      mLPEdorItemSchema = (LPEdorItemSchema) mInputData.getObjectByObjectName("LPEdorItemSchema", 0);
      mLPEdorEspecialDataSet = (LPEdorEspecialDataSet) mInputData.getObjectByObjectName("LPEdorEspecialDataSet", 0);
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "PEdorZXDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (mGlobalInput == null) 
    {
      CError tError = new CError();
      tError.moduleName = "PEdorZXDetailBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "输入数据有误!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() 
  {
	    mLPEdorItemSchema.setEdorState("1");
	    mLPEdorItemSchema.setModifyDate(currDate);
	    mLPEdorItemSchema.setModifyTime(currTime);
	    map.put(mLPEdorItemSchema, "UPDATE");
    map.put(mLPEdorEspecialDataSet, "DELETE&INSERT");
    return true;
  }

  /**
   * 根据前面的输入数据，进行校验处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean checkData()
  {
	  LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
      tLPEdorItemDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
      tLPEdorItemDB.setEdorType(mLPEdorItemSchema.getEdorType());
      LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
       if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1)
       {
           CError tError = new CError();
           tError.moduleName = "PEdorWTDetailBL";
           tError.functionName = "checkData";
           tError.errorMessage = "输入数据有误,LPEdorItem中没有相关数据!";
           this.mErrors.addOneError(tError);
           return false;
       }

       mLPEdorItemSchema = tLPEdorItemSet.get(1);
	  
      if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "PEdorLQDetailBL";
          tError.functionName = "checkData";
          tError.errorMessage = "该保全已经保全理算，不能修改!";
          this.mErrors.addOneError(tError);
          return false;
      }
	  if(mLPEdorEspecialDataSet==null)
	  {
		  return false; 
	  }
      return true;
  }
  
  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareData() 
  {
    mResult.clear();
    mResult.add(map);

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() 
  {
    return mResult;
  }
}
