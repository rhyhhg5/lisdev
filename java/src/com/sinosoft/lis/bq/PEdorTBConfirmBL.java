package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全投保事项变更项目</p>
 * <p>Description: 保全确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorTBConfirmBL implements EdorConfirm
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSchema mLPEdorItemSchema = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mContNo = null;

    private ValidateEdorData2 mValidateEdorData = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        getInputData(data);

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(mMap);
        return data;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private void getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data.getObjectByObjectName(
                "LPEdorItemSchema", 0);
        mEdorNo = mLPEdorItemSchema.getEdorNo();
        mContNo = mLPEdorItemSchema.getContNo();
        mEdorType = mLPEdorItemSchema.getEdorType();
        mValidateEdorData = new ValidateEdorData2(mGlobalInput, mEdorNo,
                mEdorType, mContNo, "ContNo");
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!validateRemark())
        {
            return false;
        }
        if (!validateUWData())
        {
            return false;
        }
        return true;
    }

    private boolean validateRemark()
    {
        LPTbInfoDB tLPTbInfoDB = new LPTbInfoDB();
        tLPTbInfoDB.setEdorNo(mEdorNo);
        tLPTbInfoDB.setEdorType(mEdorType);
        tLPTbInfoDB.setContNo(mContNo);
        if (!tLPTbInfoDB.getInfo())
        {
            mErrors.addOneError("未找到保全变更信息！");
            return false;
        }
        LPTbInfoSchema tLPTbInfoSchema = tLPTbInfoDB.getSchema();
        String newRemark = tLPTbInfoSchema.getRemark();
        String remarkFlag = tLPTbInfoDB.getRemarkFlag();
        if ((remarkFlag != null) && (remarkFlag.equals("1")))
        {
            LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
            if (tLCContSchema == null)
            {
            	LBContDB tLBContDB = new LBContDB();
            	tLBContDB.setContNo(mContNo);
            	if(tLBContDB.getInfo())
            	{
            		LBContSchema tLBContSchema = tLBContDB.getSchema();
            		tLPTbInfoSchema.setRemark(tLBContSchema.getRemark());
                    tLPTbInfoSchema.setOperator(mGlobalInput.Operator);
                    tLPTbInfoSchema.setModifyDate(mCurrentDate);
                    tLPTbInfoSchema.setModifyTime(mCurrentTime);
                    mMap.put(tLPTbInfoSchema, "DELETE&INSERT");
                    String sqlB = "update LBCont " +
                    "set Remark = '" + newRemark + "', " +
                    "  Operator = '" + mGlobalInput.Operator + "', " +
                    "  ModifyDate = '" + mCurrentDate + "', " +
                    "  ModifyTime = '" + mCurrentTime + "' " +
                    "where ContNo = '" + mContNo + "' ";
                    mMap.put(sqlB, "UPDATE");
            	}
            	
            }
            else
            {
            	tLPTbInfoSchema.setRemark(tLCContSchema.getRemark());
                tLPTbInfoSchema.setOperator(mGlobalInput.Operator);
                tLPTbInfoSchema.setModifyDate(mCurrentDate);
                tLPTbInfoSchema.setModifyTime(mCurrentTime);
                mMap.put(tLPTbInfoSchema, "DELETE&INSERT");
                String sql = "update LCCont " +
                        "set Remark = '" + newRemark + "', " +
                        "  Operator = '" + mGlobalInput.Operator + "', " +
                        "  ModifyDate = '" + mCurrentDate + "', " +
                        "  ModifyTime = '" + mCurrentTime + "' " +
                        "where ContNo = '" + mContNo + "' ";
                mMap.put(sql, "UPDATE");
            }
        }
        return true;
    }

    /**
     * 核保之后可能降档和减额，把相关信息放入C表
     * @return boolean
     */
    private boolean validateUWData()
    {
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(mContNo);
    	if(tLCContDB.getInfo())
    	{
        String[] chgTables =
                {"LCCont", "LCPol", "LCDuty", "LCGet"};
        mValidateEdorData.changeData(chgTables);
        mMap.add(mValidateEdorData.getMap());
        String[] addTables =
                {"LCPrem"};
        mValidateEdorData.addData(addTables);
        mMap.add(mValidateEdorData.getMap());
    	}

        return true;
    }
}
