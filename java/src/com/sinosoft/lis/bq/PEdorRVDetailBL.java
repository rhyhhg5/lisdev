package com.sinosoft.lis.bq;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LPContSchema;
import com.sinosoft.lis.schema.LPDutySchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPPremSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LPDutySet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.lis.vschema.LPPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PEdorRVDetailBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 传出数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/** 全局基础数据 */
	private GlobalInput mGlobalInput = null;

	private LPEdorItemSchema mLPEdorItemSchema = null;
	private LPEdorEspecialDataSet mLPEdorEspecialDataSet= new LPEdorEspecialDataSet();
	
	private LJSPaySet mLJSPaySet = new LJSPaySet();
	private MMap map = new MMap();
	
	/** 主险险种号 */
	private String mPolNo = null;
	 //复效日期
    private String edorAppDate;
    //利率
    private String rates;
    private String mAppMoney;
    private String mLoanMoney;
	private String mContNo = null;

	private Reflections ref = new Reflections();

	private String currDate = PubFun.getCurrentDate();

	private String currTime = PubFun.getCurrentTime();

	private String mEdorNo = null;

	private String mLimit = null;

	private String mGetNoticeNo = null;

	private String mSerialNo = null;

	private double mSumPayMoney = 0;
	private String mEdorType = "";
	
	private String mAcceptNo = "";
	private LJSPayPersonSet mLJSPayPersonSet=new LJSPayPersonSet();
	private String aMakeDate = PubFun.getCurrentDate();
	private String aMakeTime = PubFun.getCurrentTime();
	private LPEdorItemSchema mLPEdorItemSchema_in = null;
	private String aOperator;
	private LPContSchema mLPContSchema=new LPContSchema();
	private LPPolSet mLPPolSet=new LPPolSet();
	private LPDutySet mLPDutySet=new LPDutySet();
	private LPPremSet mLPPremSet =new LPPremSet();
	

	public PEdorRVDetailBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"INSERT"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate.trim();
		
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		// 准备往后台的数据
		if (!prepareData()) {
			return false;
		}
		PubSubmit tSubmit = new PubSubmit();

		if (!tSubmit.submitData(mResult, "")) { // 数据提交
			// @@错误处理
			this.mErrors.copyAllErrors(tSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "PEdorRVDetailBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("PEdorRVDetailBL End PubSubmit");
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		System.out.println("#####################开始获得输入数据");
		try {
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
//			mLPPolSet = (LPPolSet) mInputData.getObjectByObjectName("LPPolSet",
//					0);//TODO
			mLPEdorItemSchema_in = (LPEdorItemSchema) mInputData
					.getObjectByObjectName("LPEdorItemSchema", 0);
			mEdorNo = mLPEdorItemSchema_in.getEdorNo();
			mContNo = mLPEdorItemSchema_in.getContNo();
			mEdorType = mLPEdorItemSchema_in.getEdorType();
			mAcceptNo = mLPEdorItemSchema_in.getEdorAcceptNo();
			aOperator = mGlobalInput.Operator;
			
			if("deal".equals(mOperate)){
				TransferData tTransferData = (TransferData) mInputData
				.getObjectByObjectName("TransferData", 0);
			//准备LPEdorEspecialData数据，共三个，分别记录
			LPEdorEspecialDataSchema[] tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema[4];
			
			tLPEdorEspecialDataSchema[0] = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema[0].setEdorNo(mEdorNo);
			tLPEdorEspecialDataSchema[0].setEdorAcceptNo(mAcceptNo);
			tLPEdorEspecialDataSchema[0].setEdorType(mEdorType);
			tLPEdorEspecialDataSchema[0].setPolNo(BQ.FILLDATA);
			tLPEdorEspecialDataSchema[0].setDetailType("RV_R");		
			tLPEdorEspecialDataSchema[0].setEdorValue("");			
			tLPEdorEspecialDataSchema[1] = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema[1].setEdorAcceptNo(mAcceptNo);
			tLPEdorEspecialDataSchema[1].setEdorNo(mEdorNo);
			tLPEdorEspecialDataSchema[1].setEdorType(mEdorType);
			tLPEdorEspecialDataSchema[1].setPolNo(BQ.FILLDATA);
			tLPEdorEspecialDataSchema[1].setDetailType("RV_D");
			tLPEdorEspecialDataSchema[1].setEdorValue("");
			
			tLPEdorEspecialDataSchema[2] = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema[2].setEdorAcceptNo(mAcceptNo);
			tLPEdorEspecialDataSchema[2].setEdorNo(mEdorNo);
			tLPEdorEspecialDataSchema[2].setEdorType(mEdorType);
			tLPEdorEspecialDataSchema[2].setDetailType("RV_APPMONEY");
			tLPEdorEspecialDataSchema[2].setPolNo(BQ.FILLDATA);
			tLPEdorEspecialDataSchema[2].setEdorValue("");
			
			tLPEdorEspecialDataSchema[3] = new LPEdorEspecialDataSchema();
			tLPEdorEspecialDataSchema[3].setEdorAcceptNo(mAcceptNo);
			tLPEdorEspecialDataSchema[3].setEdorNo(mEdorNo);
			tLPEdorEspecialDataSchema[3].setEdorType(mEdorType);
			tLPEdorEspecialDataSchema[3].setDetailType("RV_LOANMONEY");
			tLPEdorEspecialDataSchema[3].setPolNo(BQ.FILLDATA);
			tLPEdorEspecialDataSchema[3].setEdorValue("");
			
			if(tTransferData.getValueByName("rates")!=null){
				tLPEdorEspecialDataSchema[0].setEdorValue((String) tTransferData
						.getValueByName("rates"));
				rates =(String)tTransferData.getValueByName("rates");
			}
			if(tTransferData.getValueByName("edorAppDate")!=null){
				tLPEdorEspecialDataSchema[1].setEdorValue((String) tTransferData
						.getValueByName("edorAppDate"));
				edorAppDate=(String) tTransferData
				.getValueByName("edorAppDate");
			}	
			if(tTransferData.getValueByName("appMoney")!=null){
				tLPEdorEspecialDataSchema[2].setEdorValue((String) tTransferData
						.getValueByName("appMoney"));
				mAppMoney =(String) tTransferData
				.getValueByName("appMoney");
				
			}
			if( tTransferData.getValueByName("LoanMoney")!=null){				

				tLPEdorEspecialDataSchema[3].setEdorValue((String) tTransferData
						.getValueByName("LoanMoney"));				
				mLoanMoney = (String) tTransferData
				.getValueByName("LoanMoney");
				
			}			
			mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema[0]);
			mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema[1]);
			mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema[2]);
			mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema[3]);
			map.put(this.mLPEdorEspecialDataSet ,"DELETE&INSERT");				
			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "PEdorRVDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mGlobalInput == null || mLPEdorItemSchema_in == null) {
			CError tError = new CError();
			tError.moduleName = "PEdorRVDetailBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "输入数据有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	} 
	/**
	 * 根据前面的输入数据，进行逻辑处理 因为退保项目必须在其他项目都结案之后才能进行，所以相应的保全数据可以从C表中得到
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {	
		
		LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
		tLPEdorItemDB.setEdorAcceptNo(mAcceptNo);
		tLPEdorItemDB.setEdorType("RV");
		tLPEdorItemDB.setContNo(mContNo);
		tLPEdorItemDB.setPolNo("000000");
		tLPEdorItemDB.setEdorNo(mEdorNo);
		LPEdorItemSet tLPEdorItemSet=tLPEdorItemDB.query();
		mLPEdorItemSchema =tLPEdorItemSet.get(1);
           
        
		// 说明保单有欠交保费而且,处于缓交或者自动缓交状态,对保单进行临时补催收,生成LJSPay,LJSPayPercon
        if ("init".equals(mOperate))
        {      	  
      	  LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
      	  mLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
            mGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", mLimit);
            mSerialNo = PubFun1.CreateMaxNo("SERIALNO", mLimit);  
      	  String tPayToDate=tLCContSchema.getPaytoDate();
      	  if(!ReCountPayData(tLCContSchema,aMakeDate,tPayToDate))
      	  {
      		    CError tError = new CError();
      		    tError.moduleName = "PEdorRVDetailBL";
      		    tError.functionName = "dealData";
      		    tError.errorMessage = "获取主险保单号失败!";
      		    this.mErrors.addOneError(tError);
      		    return false;
      	  }	
      	 
        }
        else{ 	
    	
      	  mLPEdorItemSchema.setEdorState("1");
      	  mLPEdorItemSchema.setModifyDate(currDate);
      	  mLPEdorItemSchema.setModifyTime(currTime);
      	  
//      	  if(Double.parseDouble(mAppMoney)>=0){
//      		  
//      	  }
      	
        }
       
      //  mLPEdorEspecialDataSchema.setPolNo(mPolNo);
        if(mLJSPayPersonSet.size()>0)
        {
      	  map.put(mLJSPayPersonSet,"DELETE&INSERT");
      	  LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      	  tLJAPaySchema=CommonBL.getLJAPay(this.mContNo, "2");
      	  LJSPaySchema tLJSPaySchema = new LJSPaySchema();
      	  ref.transFields(tLJSPaySchema, tLJAPaySchema);
      	  tLJSPaySchema.setGetNoticeNo(mGetNoticeNo);
      	  tLJSPaySchema.setSerialNo(mSerialNo);
      	  tLJSPaySchema.setOtherNo(mLPEdorItemSchema.getEdorAcceptNo());
      	  tLJSPaySchema.setOtherNoType("10");
      	  //存放保单本次发生交费的日期
      	  tLJSPaySchema.setPayDate(aMakeDate);
      	  tLJSPaySchema.setSumDuePayMoney(mSumPayMoney);
      	  tLJSPaySchema.setBankOnTheWayFlag("0");
      	  tLJSPaySchema.setBankSuccFlag("0");
      	  tLJSPaySchema.setPayDate(aMakeDate);	  
      	  tLJSPaySchema.setRiskCode("000000");
      	  tLJSPaySchema.setMakeDate(aMakeDate);
      	  tLJSPaySchema.setMakeTime(aMakeTime);
      	  tLJSPaySchema.setModifyDate(aMakeDate);
      	  tLJSPaySchema.setModifyTime(aMakeTime);
      	  tLJSPaySchema.setOperator(aOperator);	
      	  mLJSPaySet.add(tLJSPaySchema);
        }
        if(mLJSPaySet.size()>0)
        {
      	  map.put(mLJSPaySet,"DELETE&INSERT");  
        }
        map.put(mLPEdorItemSchema, "UPDATE");
      
		System.out.println("***********deal data over");
		return true;
	}

	/**
	 * 根据前面的输入数据，进行校验处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean checkData() {
		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
		tLPEdorItemDB.setSchema(mLPEdorItemSchema_in);
		
//	    if (!tLPEdorItemDB.getInfo()) {
//	      // @@错误处理
//	      CError tError = new CError();
//	      tError.moduleName = "PEdorZBDetailBL";
//	      tError.functionName = "checkDate";
//	      tError.errorMessage = "无保全申请数据!";
//	      System.out.println("------" + tError);
//	      this.mErrors.addOneError(tError);
//	      return false;
//	    }
//
//	    //将查询出来的保全主表数据保存至模块变量中，省去其它的重复查询
//	    mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
//	    if(mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL))
//	    {
//	        // @@错误处理
//	        CError tError = new CError();
//	        tError.moduleName = "PEdorZBDetailBL";
//	        tError.functionName = "checkData";
//	        tError.errorMessage = "该保全已经保全理算，不能修改!";
//	        this.mErrors.addOneError(tError);
//	        return false;
//	    }
	 
	  
		/*
		if (!tLPEdorItemDB.getInfo()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorrRVDetailBL";
			tError.functionName = "checkDate";
			tError.errorMessage = "无保全申请数据!";			
			this.mErrors.addOneError(tError);
			return false;
		}*/
		//TODO 下面被注释的代码验证保全项目能否添加
		/*
		DisabledManageBL tDisabledManageBL = new DisabledManageBL();
        if(!tDisabledManageBL.dealDisabledpol(mLPPolSet,
                                              mLPEdorItemSchema_in.getEdorType()))
        {
            CError tError = new CError();
            tError.moduleName = "PEdorFXDetailBL";
            tError.functionName = "checkData";
            tError.errorMessage = tDisabledManageBL.getInsuredno() +
                                  "号被保人投报险种'" + tDisabledManageBL.getRiskcode() +
                                  "'下FX项目在"
                                  + CommonBL.getCodeName("stateflag",
                tDisabledManageBL.getState()) + "状态下不能添加!";
            this.mErrors.addOneError(tError);
            return false;

        }
        */
		
		// 将查询出来的保全主表数据保存至模块变量中，省去其它的重复查询
		/*
		mLPEdorItemSchema.setSchema(tLPEdorItemDB.getSchema());
		if (mLPEdorItemSchema.getEdorState().trim().equals(BQ.EDORSTATE_CAL)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PEdorRVDetailBL";
			tError.functionName = "checkData";
			tError.errorMessage = "该保全已经保全理算，不能修改!";
			this.mErrors.addOneError(tError);
			return false;
		}*/
		/*
		if(mLPPolSet!=null){
		 for(int i = 1; i <= mLPPolSet.size(); i++)
	        {
	            LPEdorItemSchema tmLPEdorItemSchema = new LPEdorItemSchema();
	            tmLPEdorItemSchema.setSchema(mLPEdorItemSchema);
	            tmLPEdorItemSchema.setPolNo(mLPPolSet.get(i).getPolNo());
	            LPPolSchema schema = getLPPol(mLPPolSet.get(i));
	            if(schema == null)
	            {
	                return false;
	            }

	            mLPPolSet.get(i).setSchema(schema);
	        }
		}*/
		
//		 if(rates == null || rates.equals(""))
//	        {
//	            CError tError = new CError();
//	            tError.moduleName = "PEdorRVDetailBL";
//	            tError.functionName = "checkData";
//	            tError.errorMessage = "输入数据有误,没有利率信息!";
//	            this.mErrors.addOneError(tError);
//	            return false;
//	        }
		 /*
		    for(int i = 1; i <= this.mLPPolSet.size(); i++)
	        {
	            LPPolSchema schema = mLPPolSet.get(i);

	            String sql = "select PayToDate from LCPol where PayToDate >'"
	                         + edorAppDate
	                         + "' and polno = '" + schema.getPolNo() + "'";
	            String temp = new ExeSQL().getOneValue(sql);
	            if(temp != null && !temp.equals(""))
	            {
	                CError tError = new CError();
	                tError.moduleName = "PEdorFXDetailBL";
	                tError.functionName = "checkData";
	                tError.errorMessage = "复效生效日不能小于险种交至日："
	                                      + schema.getInsuredName() + "，"
	                                      + schema.getRiskCode();
	                this.mErrors.addOneError(tError);
	                return false;
	            }

	            sql = "select 1 from LCPol "
	                  + "where PolNo = '" + schema.getPolNo() + "' "
	                  + "   and EndDate < '" + edorAppDate + "' ";
	            temp = new ExeSQL().getOneValue(sql);
	            if(temp != null && !temp.equals(""))
	            {
	                CError tError = new CError();
	                tError.moduleName = "PEdorFXDetailBL";
	                tError.functionName = "checkData";
	                tError.errorMessage = "复效生效日不能大于险种满期日："
	                                      + schema.getInsuredName() + "，"
	                                      + schema.getRiskCode();
	                this.mErrors.addOneError(tError);
	                return false;
	            }

	        }*/
		 /*
		  ExeSQL tExeSQL = new ExeSQL();
	        for(int i = 1; i <= mLPPolSet.size(); i++)
	        {
	            StringBuffer sql = new StringBuffer(128);
	            sql.append(
	                "select 1 from LPCustomerImpart where EdorType ='RV' and EdorNo ='")
	                .append(mLPEdorItemSchema_in.getEdorAcceptNo())
	                .append("' and CustomerNoType = '1' ")
	                .append("and CustomerNo ='")
	                .append(mLPPolSet.get(i).getInsuredNo())
	                .append("'")
	                ;
	            String resu = tExeSQL.getOneValue(sql.toString());
	            if(resu == null || resu.equals(""))
	            {
	                CError tError = new CError();
	                tError.moduleName = "PEdorFXDetailBL";
	                tError.functionName = "checkData";
	                tError.errorMessage = "复效险种有被保人健康告知未录入!";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
	        }*/
        System.out.println("***********check data over");
		return true;
	}
	/**
     * 查询相应的险种信息
     * @param tLPPolSchema LPPolSchema
     * @return LPPolSchema
     */
    private LPPolSchema getLPPol(LPPolSchema tLPPolSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
        if(!tLCPolDB.getInfo())
        {
           
            return null;
        }

        //交换LPPolSchema 和 LCPolSchema的数据
        LPPolSchema aLPPolSchema = new LPPolSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(aLPPolSchema, tLCPolDB.getSchema());

        aLPPolSchema.setEdorNo(tLPPolSchema.getEdorNo());
        aLPPolSchema.setEdorType(tLPPolSchema.getEdorType());

        return aLPPolSchema;
    }
	/**
	 * 递归进行补催收
	 * 
	 * @param LCContSchema
	 *            LCContSchema 保单数据
	 * @param String
	 *            tCurDate 日期
	 * @return
	 */
	private boolean ReCountPayData(LCContSchema tLCContSchema, String tCurDate,
			String tPayToDate) {

		// 趸交直接返回
		ExeSQL tExeSQL = new ExeSQL();
		if ("0".equals(String.valueOf(tLCContSchema.getPayIntv()))) {
			return true;
		}
		// 时间递归线
		String rPaytoDate = PubFun.calDate(tPayToDate, tLCContSchema//TODO 尚未判断其是否计算正确
				.getPayIntv(), "M", "");
        System.out.println("********rPaytoDate = "+rPaytoDate);
		int tDayIntv = PubFun.calInterval(tPayToDate, tCurDate, "D"); //计算交至日期到现在共多少天
		System.out.println("********交至日期到现在还有tDayIntv = "+tDayIntv+"");
		if (tDayIntv < 0) {
			return true;
		} else {
			// 查询续期是否已经催出应收数据
			String tSQL = "select count(*) from ljspayperson where polno=(select polno from lcpol where contno='"
					+ tLCContSchema.getContNo()
					+ "' and polno=mainPolno) and curpaytodate='"
					+ rPaytoDate
					+ "'";
			String tFlag = tExeSQL.getOneValue(tSQL);
			if (tFlag.equals("0")) {// 如果没有应收，生成应收
				if (!dealOneIntv(tLCContSchema, rPaytoDate)) {
					CError tError = new CError();
					tError.moduleName = "PEdorRVDetailBL";
					tError.functionName = "dealOneIntv";
					tError.errorMessage = "处理一期补收数据失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}

		}
		if (!ReCountPayData(tLCContSchema, tCurDate, rPaytoDate)) {
			CError tError = new CError();
			tError.moduleName = "PEdorRVDetailBL";
			tError.functionName = "ReCountPayData";
			tError.errorMessage = "递归补催收失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/** 处理一期保单应收数据 */
	private boolean dealOneIntv(LCContSchema tLCContSchema, String tPayToDate) {
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(tLCContSchema.getContNo());
		tLCPolSet = tLCPolDB.query();
		// 产生通知书号

		// 产生统一流水号
		String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
		int payCount = PubFun.calInterval(tLCContSchema.getCValiDate(),
				tPayToDate, "M")
				/ tLCContSchema.getPayIntv();

		for (int i = 1; i <= tLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = new LCPolSchema();
			tLCPolSchema = tLCPolSet.get(i);
			LCPremSet tLCPremSet = new LCPremSet();
			LCPremDB tLCPremDB = new LCPremDB();
			tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
			tLCPremSet = tLCPremDB.query();
			for (int j = 1; j <= tLCPremSet.size(); j++) {

				LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
				LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
				tLJAPayPersonSchema = CommonBL.getLJAPayPerson(tLCPremSet
						.get(j).getPolNo());
				ref.transFields(tLJSPayPersonSchema, tLJAPayPersonSchema);
				// 存放保单本次发生交费的日期
				tLJSPayPersonSchema.setPayDate(aMakeDate);
				tLJSPayPersonSchema.setPayCount(payCount);
				tLJSPayPersonSchema.setLastPayToDate(tLCPremSet.get(j)
						.getPaytoDate());
				if ("0".equals(String.valueOf(tLCPremSet.get(j).getPayIntv()))) {
					tLJSPayPersonSchema.setCurPayToDate(tLCPremSet.get(j)
							.getPayEndDate());
				} else {
					tLJSPayPersonSchema.setCurPayToDate(tPayToDate);
				}
				mSumPayMoney += tLCPremSet.get(j).getPrem();
				tLJSPayPersonSchema.setBankOnTheWayFlag("0");
				tLJSPayPersonSchema.setBankSuccFlag("0");
				tLJSPayPersonSchema
						.setDutyCode(tLCPremSet.get(j).getDutyCode());
				tLJSPayPersonSchema.setPayPlanCode(tLCPremSet.get(j)
						.getPayPlanCode());
				tLJSPayPersonSchema.setPayIntv(tLCPremSet.get(j).getPayIntv());
				tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSet.get(j)
						.getPrem());
				tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSet.get(j)
						.getPrem());
				tLJSPayPersonSchema.setSerialNo(aSerialNo);
				tLJSPayPersonSchema.setGetNoticeNo(mGetNoticeNo);
				tLJSPayPersonSchema.setMakeDate(aMakeDate);
				tLJSPayPersonSchema.setMakeTime(aMakeTime);
				tLJSPayPersonSchema.setModifyDate(aMakeDate);
				tLJSPayPersonSchema.setModifyTime(aMakeTime);
				tLJSPayPersonSchema.setOperator(aOperator);
				mLJSPayPersonSet.add(tLJSPayPersonSchema);
			}

		}

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareData() {
        mResult.clear();
        mResult.add(map);
		
		return true;
	}

	public VData getResult() 
	{
	  return mResult;
	}
}
