package com.sinosoft.lis.certifymanage;

import java.util.Date;

import com.sinosoft.lis.db.LZCertifyBatchInfoDB;
import com.sinosoft.lis.db.LZCertifyBatchMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAPresenceSchema;
import com.sinosoft.lis.schema.LZCertifyBatchInfoSchema;
import com.sinosoft.lis.schema.LZCertifyBatchMainSchema;
import com.sinosoft.lis.vschema.LZCertifyBatchInfoSet;
import com.sinosoft.lis.vschema.LZCertifyBatchMainSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyBatchConfirmBL {

	private CErrors mErrors = new CErrors();

	private String mOperatType;

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZCertifyBatchInfoSet tLZCertifyBatchInfoSet = new LZCertifyBatchInfoSet();
	
	private String mBatchNo;

	private String mImportBatchNo;
	
	private String mSerialNo;

	private MMap mMap = new MMap();

	/**
	 * 入口函数
	 * 
	 * @param tVData
	 * @param tType 处理类型 ONLY--选择处理 BATCH--全部处理
	 * @return
	 */
	public boolean submitData(VData tVData, String tType) {

		this.mOperatType = tType;

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}

		return true;
	}

	/**
	 * @param tInputData
	 * @return
	 */
	private boolean getInputData(VData tInputData) {

		TransferData tTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);

		mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);

		mBatchNo = (String) tTransferData.getValueByName("BatchNo");

		mImportBatchNo = (String) tTransferData.getValueByName("ImportBatchNo");
		
		
		
		this.tLZCertifyBatchInfoSet.set((LZCertifyBatchInfoSet) tInputData.
                getObjectByObjectName(
                        "LZCertifyBatchInfoSet", 0));
		//mSerialNo = (String) tTransferData.getValueByName("SerialNo");

		if (mGlobalInput == null || mOperatType == null) {
			setErrors("没有得到足够的信息！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.equals("")) {
			setErrors("请选择处理征订批次！", "getInputData");
			return false;
		}

		if (!mOperatType.equals("BATCH") && (mImportBatchNo == null || mImportBatchNo.trim().equals(""))) {
			setErrors("请选择处理待处理导入批次！", "getInputData");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {

		boolean succFlag = false;

		if (mOperatType.equals("BATCH")) {

			succFlag = confrimAll();

		} else {

			LZCertifyBatchMainDB tLZCertifyBatchMainDB = new LZCertifyBatchMainDB();
			tLZCertifyBatchMainDB.setImportBatchNo(mImportBatchNo);
			
			LZCertifyBatchInfoDB tLZCertifyBatchInfoDB = new LZCertifyBatchInfoDB();
			tLZCertifyBatchInfoDB.setImportBatchNo(mImportBatchNo);
			
			
			
			
			if (!tLZCertifyBatchMainDB.getInfo()) {
				setErrors("获取导入批次异常！", "dealData");
				return false;
			}
			
			succFlag = confrim(tLZCertifyBatchMainDB.getSchema());
			
			for(int i = 1; i <= tLZCertifyBatchInfoSet.size() ; i++){
				
				updateConfirm(tLZCertifyBatchInfoSet.get(i).getDB().getSchema());
			}
		}

		return succFlag;
	}

	/**
	 * 全部审核处理
	 * 
	 * @return
	 */
	private boolean confrimAll() {

		String batchSql = "select * from LZCertifyBatchMain where batchno='" + mBatchNo + "' and comcode like '"
				+ mGlobalInput.ManageCom + "%' and StateFlag='00'";

		System.out.println(batchSql);
		
		LZCertifyBatchMainSet tLZCertifyBatchMainSet = new LZCertifyBatchMainDB().executeQuery(batchSql);
		
		if(tLZCertifyBatchMainSet.size() == 0){
			setErrors("未找到待审核批次！", "dealData");
			return false;
		}

		for (int index = 1; index <= tLZCertifyBatchMainSet.size(); index++) {

			if (!confrim(tLZCertifyBatchMainSet.get(index))) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 勾选审核处理
	 * 
	 * @param tLZPrintListSet
	 * @return
	 */
	private boolean confrim(LZCertifyBatchMainSchema tLZCertifyBatchMainSchema) {

		if (!tLZCertifyBatchMainSchema.getStateFlag().equals("00")) {
			setErrors("导入批次状态为非待审核状态，请重新查询后进行审核！", "confrim");
			return false;
		}
		
		tLZCertifyBatchMainSchema.setStateFlag("01");
		tLZCertifyBatchMainSchema.setModifyDate(new Date());
		tLZCertifyBatchMainSchema.setModifyTime(PubFun.getCurrentTime());
		tLZCertifyBatchMainSchema.setOperator(mGlobalInput.Operator);

		mMap.put(tLZCertifyBatchMainSchema, "UPDATE");

		return true;
	}

	private boolean updateConfirm(LZCertifyBatchInfoSchema tLZCertifyBatchInfoSchema){
		
		mSerialNo = tLZCertifyBatchInfoSchema.getSerialNo();
		
		String strSQL = "select * from LZCertifyBatchInfo where serialno ='" + mSerialNo + "' and comcode like '"
		+ mGlobalInput.ManageCom + "%' ";

		System.out.println(strSQL);

		LZCertifyBatchInfoSet tLZCertifyBatchInfoSet = new LZCertifyBatchInfoDB().executeQuery(strSQL);
		if(tLZCertifyBatchInfoSet.size() == 0){
			setErrors("未找到待审核批次！", "dealData");
			return false;
		}
		
		tLZCertifyBatchInfoSchema.setBatchNo(mBatchNo);
		tLZCertifyBatchInfoSchema.setMakeDate(new Date());
		tLZCertifyBatchInfoSchema.setMakeTime(PubFun.getCurrentTime());
		tLZCertifyBatchInfoSchema.setModifyDate(new Date());
		tLZCertifyBatchInfoSchema.setModifyTime(PubFun.getCurrentTime());
		tLZCertifyBatchInfoSchema.setOperator(mGlobalInput.Operator);
		
		mMap.put(tLZCertifyBatchInfoSchema, "UPDATE");
		
		return true;
	}
	
	/**
	 * @return
	 */
	private boolean submit() {
		// 提交数据库
		VData mInputData = new VData();
		mInputData.add(mMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("--提交数据库失败！--" + tPubSubmit.mErrors.getLastError());

			setErrors(tPubSubmit.mErrors.getLastError(), "submit");

			return false;
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}
}
