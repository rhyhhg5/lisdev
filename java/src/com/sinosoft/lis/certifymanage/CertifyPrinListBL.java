package com.sinosoft.lis.certifymanage;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class CertifyPrinListBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData;

	private VData mResult = new VData();

	private String mOperate;

	private MMap map = new MMap();

	private Map lengthMap = new HashMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZPrintListSet mLZPrintListSet = new LZPrintListSet();

	ExeSQL tExeSQL = new ExeSQL();

	public CertifyPrinListBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			this.mOperate = cOperate;
			if (!mOperate.equals("INSERT") && !mOperate.equals("UPDATE")
					&& !mOperate.equals("DELETE")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!dealData()) {
				return false;
			}
			if (!prepareOutputData())
				return false;

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "CertifyPrinListBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}

		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PrintingShopBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {

		LZPrintListSet tLZPrintListSet = new LZPrintListSet();

		if (mOperate.equals("INSERT")) {
			for (int i = 1; i <= mLZPrintListSet.size(); i++) {

				LZPrintListSchema tLZPrintListSchema = new LZPrintListSchema();
				tLZPrintListSchema = mLZPrintListSet.get(i);

				String tCertifyCode = tLZPrintListSchema.getCertifyCode();
				String tStartNo = tLZPrintListSchema.getStartNo();
				String tEndNo = tLZPrintListSchema.getEndNo();
				int length = getLength(tCertifyCode);
				
				
				//String strSQL =  "select distinct havenumber from LZPrintList lz, lmcarddescription lc  where lc.certifycode = lz.certifycode "
				//	+ " and lz.certifycode = '"+ tCertifyCode +"' "
				//	+ " with ur ";
				String strSQL = "select distinct havenumber from lmcarddescription where certifycode = '"+ tCertifyCode +"'" 
					+ " with ur ";
				
				String havenum = tExeSQL.getOneValue(strSQL);
				System.out.println("这是个无号单证" + havenum ) ;
				
				if(havenum.equals("N")){

					String tSql = "select 1 from LZPrintList "  +" where batchno = '"+ tLZPrintListSchema.getBatchNo() +"'"   
							+ " and certifycode = '"+ tCertifyCode +"' "
							+ "  and printcode= '"+ tLZPrintListSchema.getPrintCode() +"'"
							+ " with ur";
					System.out.println(tSql);
					if ("1".equals(tExeSQL.getOneValue(tSql))) {
						CError tError = new CError();
						tError.moduleName = "CertifyPrinListBL";
						tError.functionName = "dealData";
						tError.errorMessage = "第'" + i + "'款单证，单证编码'"
								+ tCertifyCode + "'已添加印刷通知单！！！ ";
						this.mErrors.addOneError(tError);
						return false;

					}

					
					double tPrice = tLZPrintListSchema.getPrice();
					double tTotalPrice = tPrice * tLZPrintListSchema.getSumCount();

					System.out.println("总价为》》》》》》》》》》》》" + tTotalPrice);

					String SerialNo = PubFun1.CreateMaxNo("SERNO", "");
					tLZPrintListSchema.setSerialNo(SerialNo);
					tLZPrintListSchema.setSumCount(tLZPrintListSchema.getSumCount());
					tLZPrintListSchema.setTotalPrice(tTotalPrice);
					// 00-制定印刷通知书 01-印刷通知单制定完毕 02-未入库 03-已入库
					tLZPrintListSchema.setDealState("00");
					tLZPrintListSchema.setMakeDate(mCurrentDate);
					tLZPrintListSchema.setMakeTime(mCurrentTime);
					tLZPrintListSchema.setModifyDate(mCurrentDate);
					tLZPrintListSchema.setModifyTime(mCurrentTime);
					tLZPrintListSchema.setOperator(mGlobalInput.Operator);
				}
				if(havenum.equals("Y")){
					
				if ("".equals(tStartNo)) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'起始号为空！！！ ";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tEndNo)) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'终止号为空！！！ ";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				
				String startNo = getInterStr(Integer.parseInt(tStartNo), length);
				String endNo = getInterStr(Integer.parseInt(tEndNo), length);

				String strSql = "select 1 from LZPrintList where  startno <= '"
						+ endNo + "' and endno>='" + startNo
						+ "'  and certifycode ='" + tCertifyCode + "'with ur";
				if ("1".equals(tExeSQL.getOneValue(strSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'号段'" + tStartNo + "'-'" + tEndNo
							+ " 已使用，请重新输入号段！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}
				

				int count = Integer.parseInt(endNo) - Integer.parseInt(startNo)
						+ 1;
				
				if (count != tLZPrintListSchema.getSumCount()) {

					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证,印刷数量和号段不符！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}
				
				String tSql = "select 1 from LZPrintList where batchno='"
						+ tLZPrintListSchema.getBatchNo()
						+ "' and certifycode = '" + tCertifyCode
						+ "'  and printcode= '"
						+ tLZPrintListSchema.getPrintCode() + "' with ur";

				if ("1".equals(tExeSQL.getOneValue(tSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "dealData";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'已添加印刷通知单！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}

				
				double tPrice = tLZPrintListSchema.getPrice();
				double tTotalPrice = tPrice * count;

				System.out.println("总价为》》》》》》》》》》》》" + tTotalPrice);

				String SerialNo = PubFun1.CreateMaxNo("SERNO", "");
				tLZPrintListSchema.setSerialNo(SerialNo);
				tLZPrintListSchema.setStartNo(startNo);
				tLZPrintListSchema.setEndNo(endNo);
				tLZPrintListSchema.setSumCount(count);
				tLZPrintListSchema.setTotalPrice(tTotalPrice);
				// 00-制定印刷通知书 01-印刷通知单制定完毕 02-未入库 03-已入库
				tLZPrintListSchema.setDealState("00");
				tLZPrintListSchema.setMakeDate(mCurrentDate);
				tLZPrintListSchema.setMakeTime(mCurrentTime);
				tLZPrintListSchema.setModifyDate(mCurrentDate);
				tLZPrintListSchema.setModifyTime(mCurrentTime);
				tLZPrintListSchema.setOperator(mGlobalInput.Operator);
			}
				tLZPrintListSet.add(tLZPrintListSchema);

			}

			map.put(tLZPrintListSet, "INSERT");
		} else if (mOperate.equals("DELETE")) {

			map.put(mLZPrintListSet, "DELETE");

		} else if (mOperate.equals("UPDATE")) {

			for (int i = 1; i <= mLZPrintListSet.size(); i++) {

				String sql = "Update LZPrintList set DealState='01', "
						+ "ModifyDate = '" + mCurrentDate + "', "
						+ "ModifyTime = '" + mCurrentTime + "' "
						+ "Where  batchno = '"
						+ mLZPrintListSet.get(i).getBatchNo() + "' ";
				map.put(sql, "UPDATE"); // 修改

			}

		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mLZPrintListSet = ((LZPrintListSet) cInputData.getObjectByObjectName(
				"LZPrintListSet", 0));
		System.out.println("getinput" + mLZPrintListSet.size());
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	/**
	 * 获取单证号码长度
	 * 
	 * @param length
	 * @return
	 */
	private int getLength(String certify) {

		String length = (String) lengthMap.get(certify);

		if (length == null) {
			length = new ExeSQL()
					.getOneValue("select certifylength from LMCardDescription where certifycode='"
							+ certify + "'");
			if (length == null || length.trim().equals("")) {
				// 没长度
				length = "10";
			}
			lengthMap.put(certify, length);
		}
		return Integer.parseInt(length);
	}

	/**
	 * 获取数字对应的字符串
	 * 
	 * @param integer
	 * @param length
	 * @return
	 */
	private String getInterStr(int integer, int length) {

		String str = integer + "";

		for (int index = str.length(); index < length; index++) {
			str = "0" + str;
		}
		return str;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinBankAddBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.map);
			mResult = mInputData;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareData", ex.getMessage());
			return false;
		}
		return true;
	}
}
