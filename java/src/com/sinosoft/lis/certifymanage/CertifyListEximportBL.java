package com.sinosoft.lis.certifymanage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class CertifyListEximportBL {
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private String mFilePath = "";

	private BookModelImpl book = new BookModelImpl();

	//业务数据
	private TransferData inTransferData = new TransferData();

	private String fileName = "";

	private LZCertifyBatchMainSet mLZCertifyBatchMainSet = new LZCertifyBatchMainSet();

	private LZCertifyBatchInfoSet mLZCertifyBatchInfoSet = new LZCertifyBatchInfoSet();

	private GlobalInput tG = new GlobalInput();

	private List exList = new ArrayList();

	private MMap map = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String mBatchNo = "";

	private String mImportBatchNo = "";
	
	ExeSQL tExeSQL = new ExeSQL();


	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * @param cInputData 传入的数据,VData对象
	 * @param cOperate 数据操作字符串，主要包括"READ"和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		//进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		if (!prepareOutputData())
			return false;

		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "OLDDiseaseBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * @param: 无
	 * 
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			inTransferData = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
					0);

			if (tG == null || inTransferData == null) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "接收数据失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			mBatchNo = (String) inTransferData.getValueByName("BatchNo");
			System.out.println("mBatchNo====================" + mBatchNo);
			if (mBatchNo == null || mBatchNo.equals("")) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "获取批次信息失败。";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
				// 获取返回文件名称
				fileName = (String) inTransferData.getValueByName("fileName");
				fileName = fileName.replace('\\', '/');
				// 读取银行返回文件，公共部分
				System.out.println("---开始读取文件---");
				if (!readBatchPayExcel(fileName)) {
					return false;
				}
				if (!checkList()) {
					return false;
				}
				if (!setLZCertifyBatchMainSchema()) {
					return false;
				}
				if (!setLZCertifyBatchInfoSchema()) {
					return false;
				}

			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误:" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 读取付费导入文件
	 * @param fileName
	 * @return
	 */
	private boolean readBatchPayExcel(String fileName) {
		try {
			mFilePath = fileName;
			book.initWorkbook();//初始
			book.read(mFilePath, new ReadParams());
			int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
			System.out.println("tSheetNums===" + tSheetNums);

			for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++) {
				book.setSheet(sheetNum);
				int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
				int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
				System.out.println("tLastCol===" + tLastCol);
				System.out.println("tLastRow===" + tLastRow);
				String colValue = "";
				//按列循环
				for (int j = 1; j <= tLastRow; j++) {//循环总行数
					Map tMap = new HashMap();
					for (int i = 0; i <= tLastCol; i++) {//循环总列数
						if (book.getText(j, i) != null) {
							colValue = (String) book.getText(j, i).trim();
						} else {
							colValue = "";
						}
						switch (i) {
						case 1:
							tMap.put("CertifyCode", colValue);
							break;
						case 9:
							tMap.put("SumCount", colValue);
							break;
						}

					}
					tMap.put("ComCode", tG.ManageCom);
					exList.add(tMap);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "readExcelFile";
			tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean checkList() {
		if (exList.size() == 0) {
			System.out.println("-----exList-size-0-----");
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "checkList";
			tError.errorMessage = "未发现导入文件中存在单证信息!";
			this.mErrors.addOneError(tError);
			return false;
		}

		for (int k = 0; k < exList.size(); k++) {
			Map tMap = (Map) exList.get(k);
			// 判断单证号、征订数量是否为空 
			if ("".equals(tMap.get("CertifyCode"))) {
				CError tError = new CError();
				tError.moduleName = "CertifyListEximportBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，单证编码不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else if ("".equals(tMap.get("SumCount"))) {
				CError tError = new CError();
				tError.moduleName = "CertifyListEximportBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，征订数量不能为空!";
				this.mErrors.addOneError(tError);
				return false;

			}
			String tSql =" select 1 from LZCertifyTemplate where batchno='"+mBatchNo+"'"
				       + " and certifycode = '" + tMap.get("CertifyCode")+ "' with ur";
			
			if (!"1".equals(tExeSQL.getOneValue(tSql))) {
				CError tError = new CError();
				tError.moduleName = "CertifyListEximportBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，单证编码"+tMap.get("CertifyCode")+"在征订模板中不存在!";
				this.mErrors.addOneError(tError);
				return false;

			}
		}
		return true;
	}

	/**
	 * 给LJAGetSchema和LJFiGetSchema赋值
	 * @return
	 */
	private boolean setLZCertifyBatchMainSchema() {
		LZCertifyBatchMainSchema tLZCertifyBatchMainSchema = new LZCertifyBatchMainSchema();
		int length = 8;
		String tComCode = tG.ComCode;
		if (tComCode.length() < length) {
			tComCode = PubFun.RCh(tComCode, "0", length);
		}
		try {
			mImportBatchNo = PubFun1.CreateMaxNo("IMPORTBATCHNO", tComCode,
					null);
			System.out.println("BatchNo:" + mImportBatchNo);
		} catch (Exception e) {
			mImportBatchNo = null;
			e.printStackTrace();
		}

		tLZCertifyBatchMainSchema.setImportBatchNo(mImportBatchNo);
		tLZCertifyBatchMainSchema.setBatchNo(mBatchNo);
		tLZCertifyBatchMainSchema.setComCode(tG.ComCode);
		//00-未审核  01-已审核
		tLZCertifyBatchMainSchema.setStateFlag("00");
		tLZCertifyBatchMainSchema.setOperator(tG.Operator);
		tLZCertifyBatchMainSchema.setMakeDate(mCurrentDate);
		tLZCertifyBatchMainSchema.setMakeTime(mCurrentTime);
		tLZCertifyBatchMainSchema.setModifyDate(mCurrentDate);
		tLZCertifyBatchMainSchema.setModifyTime(mCurrentTime);

		mLZCertifyBatchMainSet.add(tLZCertifyBatchMainSchema);
		map.put(mLZCertifyBatchMainSet, "INSERT");

		return true;

	}

	/**
	 * 给LJAGetSchema和LJFiGetSchema赋值
	 * @return
	 */
	private boolean setLZCertifyBatchInfoSchema() {

		for (int j = 0; j < exList.size(); j++) {

			LZCertifyBatchInfoSchema tLZCertifyBatchInfoSchema = new LZCertifyBatchInfoSchema();
			Map xMap = (Map) exList.get(j);

			String SerialNo = PubFun1.CreateMaxNo("SeqNum", "");
			tLZCertifyBatchInfoSchema.setSerialNo(SerialNo);
			tLZCertifyBatchInfoSchema.setBatchNo(mBatchNo);
			tLZCertifyBatchInfoSchema.setImportBatchNo(mImportBatchNo);
			

			tLZCertifyBatchInfoSchema.setCertifyCode((String) xMap
					.get("CertifyCode"));
			tLZCertifyBatchInfoSchema
					.setSumCount((String) xMap.get("SumCount"));
			tLZCertifyBatchInfoSchema.setSumCountYW((String) xMap.get("SumCount"));
			tLZCertifyBatchInfoSchema.setComCode(tG.ComCode);
			tLZCertifyBatchInfoSchema.setOperator(tG.Operator);
			tLZCertifyBatchInfoSchema.setMakeDate(mCurrentDate);
			tLZCertifyBatchInfoSchema.setMakeTime(mCurrentTime);
			tLZCertifyBatchInfoSchema.setModifyDate(mCurrentDate);
			tLZCertifyBatchInfoSchema.setModifyTime(mCurrentTime);

			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>"
					+ tLZCertifyBatchInfoSchema.getCertifyCode());
			mLZCertifyBatchInfoSet.add(tLZCertifyBatchInfoSchema);

		}
		map.put(mLZCertifyBatchInfoSet, "INSERT");
		return true;

	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.tG);
			mInputData.add(this.map);

		} catch (Exception ex) {
			ex.printStackTrace();

			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = ex.getMessage();
			this.mErrors.addOneError(tError);
			return false;

		}
		return true;
	}

	/**
	 *  返回查询条件，用于前台界面展示
	 */
	public String getImportBatchNo() {

		return mImportBatchNo;
	}

}
