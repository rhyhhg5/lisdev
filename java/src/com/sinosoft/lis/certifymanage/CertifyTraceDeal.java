package com.sinosoft.lis.certifymanage;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.db.LZCertifyInventoryDB;
import com.sinosoft.lis.db.LZCertifyInventoryTraceDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LZCertifyInventorySchema;
import com.sinosoft.lis.schema.LZCertifyInventoryTraceSchema;
import com.sinosoft.lis.schema.LZcertifyAssignSchema;
import com.sinosoft.lis.vschema.LZCertifyInventorySet;
import com.sinosoft.lis.vschema.LZCertifyInventoryTraceSet;
import com.sinosoft.lis.vschema.LZcertifyAssignSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;

/**
 * 单证归集处理类
 * 
 * @author 张成轩
 */
public class CertifyTraceDeal {

	private GlobalInput mGlobalInput;

	private CErrors mErrors = new CErrors();

	private Map lengthMap = new HashMap();

	private Reflections mReflections = new Reflections();

	/**
	 * 初始化
	 */
	public CertifyTraceDeal(GlobalInput tGlobalInput) {
		this.mGlobalInput = tGlobalInput;
	}

	/**
	 * 初次进入轨迹表，针对入库操作
	 * 
	 * @param certify 单证编码
	 * @param start 起始号
	 * @param end 终止号
	 * @return
	 */
	public MMap firstTrack(String certify, int start, int end) {

		int length = getLength(certify);

		String startNo = getInterStr(start, length);
		String endNo = getInterStr(end, length);

		LZCertifyInventorySchema inv = getInvSchema(certify, mGlobalInput.ComCode, startNo, endNo);

		LZCertifyInventoryTraceSchema invTrace = getInvTraceSchema(inv, null, "00");

		MMap tMMap = new MMap();
		tMMap.put(inv, "INSERT");
		tMMap.put(invTrace, "INSERT");

		return tMMap;
	}

	/**
	 * 轨迹处理
	 * 
	 * @param certify 单证编码
	 * @param sendOutCom 发放机构
	 * @param incomeCom 接收机构
	 * @param start 起始号
	 * @param end 终止号
	 * @param type 操作类型
	 * @return
	 */
	public MMap dealTrack(String certify, String sendOutCom, String incomeCom, int start, int end, String type) {

		// 多次使用时 从缓存取长度 避免重复调数据库
		int length = getLength(certify);

		// 获取号段的全部库存信息
		String queryInventory = "select * from LZCertifyInventory "
				+ " where int(startno) < " + start
				+ " and int(endno) > " + start
				+ " and certifycode = '" + certify  + "'"
				+ " and comcode='" + sendOutCom + "'"
				+ " union all "
				+ " select * from LZCertifyInventory "
				+ " where int(startno) between " + start
				+ " and " + end
				+ " and certifycode = '" + certify + "'"
				+ " and comcode='" + sendOutCom + "'";

		System.out.println(queryInventory);

		LZCertifyInventoryDB tLZCertifyInventoryDB = new LZCertifyInventoryDB();
		LZCertifyInventorySet invSet = tLZCertifyInventoryDB.executeQuery(queryInventory);

		if (invSet.size() == 0) {
			setErrors("不存在该单证号段的相关库存！", "dealTrack");
			return null;
		} else if (!checkInvent(invSet, start, end)) {
			// 这个是前提 必须现校验库存情况的完整性 已避免出现给的号段中存在库存没有的情况
			setErrors("该单证的库存与待处理的号段不符！", "dealTrack");
			return null;
		}

		// 待更新的数据集合
		LZCertifyInventorySet newInvSet = new LZCertifyInventorySet();
		LZcertifyAssignSet newAssignSet = new LZcertifyAssignSet();
		LZCertifyInventoryTraceSet newTraceSet = new LZCertifyInventoryTraceSet();
		LZCertifyInventorySet deleteInvSet = new LZCertifyInventorySet();
		LZCertifyInventoryTraceSet updateTraceSet = new LZCertifyInventoryTraceSet();

		for (int row = 1; row <= invSet.size(); row++) {
			LZCertifyInventorySchema invSchema = invSet.get(row);

			// 获取轨迹信息
			LZCertifyInventoryTraceDB tLZCertifyInventoryTraceDB = new LZCertifyInventoryTraceDB();
			tLZCertifyInventoryTraceDB.setInventorySerialNo(invSchema.getSerialNo());
			tLZCertifyInventoryTraceDB.setArchiveFlag("00");
			LZCertifyInventoryTraceSet tLZcertifyInventoryTraceSet = tLZCertifyInventoryTraceDB.query();

			if (tLZcertifyInventoryTraceSet.size() != 1) {
				setErrors("库存轨迹异常！", "dealTrack");
				return null;
			}

			// 原始轨迹信息
			LZCertifyInventoryTraceSchema oldTrace = tLZcertifyInventoryTraceSet.get(1);

			int invStartNo = Integer.parseInt(invSchema.getStartNo());
			int invEndNo = Integer.parseInt(invSchema.getEndNo());

			if (start == invStartNo) {

				// 例 现库存为1-100 处理的号段为1-100的情况
				if (end == invEndNo) {
					// 待更新的Schema
					LZCertifyInventorySchema inv = getInvSchema(certify, incomeCom, invSchema.getStartNo(), invSchema
							.getEndNo());
					LZCertifyInventoryTraceSchema trace = getInvTraceSchema(inv, oldTrace, type);

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, inv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(inv);
					}
					newTraceSet.add(trace);
				}

				// 例 现库存为 1-100 处理的号段为1-50的情况 拆分为两部分 1-50 51-100
				if (end < invEndNo) {
					// 拆分的第一部分
					LZCertifyInventorySchema fInv = getInvSchema(certify, incomeCom, invSchema.getStartNo(),
							getInterStr(end, length));
					LZCertifyInventoryTraceSchema fTrace = getInvTraceSchema(fInv, oldTrace, type);

					// 拆分的第二部分
					LZCertifyInventorySchema sInv = getInvSchema(certify, invSchema.getComCode(), getInterStr(end + 1,
							length), invSchema.getEndNo());
					LZCertifyInventoryTraceSchema sTrace = getRestTraceSchema(sInv, oldTrace, type);

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, fInv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(fInv);
					}

					newInvSet.add(sInv);
					newTraceSet.add(fTrace);
					newTraceSet.add(sTrace);
				}

				// 例 现库存为 1-100 处理的号段为1-150的情况 只需要一部分1-100 另一部分不用管101-150
				if (end > invEndNo) {
					LZCertifyInventorySchema inv = getInvSchema(certify, incomeCom, invSchema.getStartNo(), invSchema
							.getEndNo());
					LZCertifyInventoryTraceSchema trace = getInvTraceSchema(inv, oldTrace, "00");

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, inv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(inv);
					}
					newTraceSet.add(trace);
				}
			}

			// 其实可以和 == 的情况合并 为便于理解 暂不合并
			if (start < invStartNo) {

				// 例 现库存为 100-200 处理的号段为1-200的情况 只需要一部分100-200 另一部分不用管1-99
				if (end == invEndNo) {
					LZCertifyInventorySchema inv = getInvSchema(certify, incomeCom, invSchema.getStartNo(), invSchema
							.getEndNo());
					LZCertifyInventoryTraceSchema trace = getInvTraceSchema(inv, oldTrace, "00");

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, inv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(inv);
					}
					newTraceSet.add(trace);
				}

				// 例 现库存为 100-200 处理的号段为1-150的情况 需要两部分100-150 151-200 另一部分不用管
				// 1-99
				if (end < invEndNo) {
					// 拆分的第一部分
					LZCertifyInventorySchema fInv = getInvSchema(certify, incomeCom, invSchema.getStartNo(),
							getInterStr(end, length));
					LZCertifyInventoryTraceSchema fTrace = getInvTraceSchema(fInv, oldTrace, type);

					// 拆分的第二部分
					LZCertifyInventorySchema sInv = getInvSchema(certify, invSchema.getComCode(), getInterStr(end + 1,
							length), invSchema.getEndNo());
					LZCertifyInventoryTraceSchema sTrace = getRestTraceSchema(sInv, oldTrace, type);

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, fInv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(fInv);
					}
					newInvSet.add(sInv);
					newTraceSet.add(fTrace);
					newTraceSet.add(sTrace);
				}

				// 例 现库存为 100-200 处理的号段为1-250的情况 拆分为一部分100-200 其他不要
				if (end > invEndNo) {
					LZCertifyInventorySchema inv = getInvSchema(certify, incomeCom, invSchema.getStartNo(), invSchema
							.getEndNo());
					LZCertifyInventoryTraceSchema trace = getInvTraceSchema(inv, oldTrace, "00");

					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, inv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(inv);
					}
					newTraceSet.add(trace);
				}
			}

			if (start > invStartNo) {

				// 例 现库存为1-100 处理的号段为50-100的情况 拆分为两部分 1-49 50-100
				if (end == invEndNo) {
					// 拆分的第一部分
					LZCertifyInventorySchema fInv = getInvSchema(certify, invSchema.getComCode(), invSchema
							.getStartNo(), getInterStr(start - 1, length));
					LZCertifyInventoryTraceSchema fTrace = getRestTraceSchema(fInv, oldTrace, type);

					// 拆分的第二部分
					LZCertifyInventorySchema sInv = getInvSchema(certify, incomeCom, getInterStr(start, length),
							invSchema.getEndNo());
					LZCertifyInventoryTraceSchema sTrace = getInvTraceSchema(sInv, oldTrace, type);

					newInvSet.add(fInv);
					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, sInv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(sInv);
					}
					newTraceSet.add(fTrace);
					newTraceSet.add(sTrace);
				}

				// 例 现库存为 1-150 处理的号段为50-100的情况 拆分为三部分 1-49 50-100 101-150
				if (end < invEndNo) {
					// 拆分的第一部分
					LZCertifyInventorySchema fInv = getInvSchema(certify, invSchema.getComCode(), invSchema
							.getStartNo(), getInterStr(start - 1, length));
					LZCertifyInventoryTraceSchema fTrace = getRestTraceSchema(fInv, oldTrace, type);

					// 拆分的第二部分
					LZCertifyInventorySchema sInv = getInvSchema(certify, incomeCom, getInterStr(start, length),
							getInterStr(end, length));
					LZCertifyInventoryTraceSchema sTrace = getInvTraceSchema(sInv, oldTrace, type);

					// 拆分的第三部分
					LZCertifyInventorySchema tInv = getInvSchema(certify, invSchema.getComCode(), getInterStr(end + 1,
							length), invSchema.getEndNo());
					LZCertifyInventoryTraceSchema tTrace = getRestTraceSchema(tInv, oldTrace, type);

					newInvSet.add(fInv);
					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, sInv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(sInv);
					}
					newInvSet.add(tInv);
					newTraceSet.add(fTrace);
					newTraceSet.add(sTrace);
					newTraceSet.add(tTrace);
				}

				// 例 现库存为 1-100 处理的号段为50-150的情况 拆分为两部分1-49 50-100 其他不要
				if (end > invEndNo) {
					// 拆分的第一部分
					LZCertifyInventorySchema fInv = getInvSchema(certify, invSchema.getComCode(), invSchema
							.getStartNo(), getInterStr(end - 1, length));
					LZCertifyInventoryTraceSchema fTrace = getRestTraceSchema(fInv, oldTrace, type);

					// 拆分的第二部分
					LZCertifyInventorySchema sInv = getInvSchema(certify, incomeCom, getInterStr(start, length),
							invSchema.getEndNo());
					LZCertifyInventoryTraceSchema sTrace = getInvTraceSchema(sInv, oldTrace, type);

					newInvSet.add(fInv);
					if ("03".equals(type)) {
						LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
						mReflections.transFields(ass, sInv);
						ass.setSendOutCom(sendOutCom);
						ass.setReceiveCom(incomeCom);
						newAssignSet.add(ass);
					} else {
						newInvSet.add(sInv);
					}
					newTraceSet.add(fTrace);
					newTraceSet.add(sTrace);
				}
			}

			// 处理原始数据
			oldTrace.setArchiveFlag("01");

			deleteInvSet.add(invSchema);
			updateTraceSet.add(oldTrace);
		}

		MMap tMMap = new MMap();
		tMMap.put(newInvSet, "INSERT");
		tMMap.put(newTraceSet, "INSERT");
		tMMap.put(newAssignSet, "INSERT");
		tMMap.put(deleteInvSet, "DELETE");
		tMMap.put(updateTraceSet, "UPDATE");

		return tMMap;
	}

	/**
	 * 库存情况校验
	 * 
	 * @param invSet
	 * @return
	 */
	private boolean checkInvent(LZCertifyInventorySet invSet, int start, int end) {

		// 总数量
		int sumCount = 0;
		// 最大号最小号差额
		int subCount = 0;

		LZCertifyInventorySchema invSchema = invSet.get(1);
		int minNo = Integer.parseInt(invSchema.getStartNo());
		int maxNo = Integer.parseInt(invSchema.getEndNo());

		sumCount = (maxNo - minNo + 1);

		for (int row = 2; row <= invSet.size(); row++) {
			invSchema = invSet.get(row);

			int startNo = Integer.parseInt(invSchema.getStartNo());
			int endNo = Integer.parseInt(invSchema.getEndNo());

			if (minNo > startNo) {
				minNo = startNo;
			}
			if (maxNo < endNo) {
				maxNo = endNo;
			}

			// 累积总单证量
			sumCount = (endNo - startNo + 1);
		}

		// 判断差额
		subCount = (maxNo - minNo + 1);

		if (minNo > start) {
			return false;
		}
		if (maxNo < end) {
			return false;
		}

		// 如果不等中间肯定有间断
		return sumCount == subCount;
	}

	/**
	 * 获取单证号码长度
	 * 
	 * @param length
	 * @return
	 */
	private int getLength(String certify) {

		String length = (String) lengthMap.get(certify);

		if (length == null) {
			length = new ExeSQL().getOneValue("select certifylength from LMCardDescription where certifycode='"
					+ certify + "'");
			if (length == null || length.trim().equals("")) {
				// 没长度
				length = "10";
			}
			lengthMap.put(certify, length);
		}
		return Integer.parseInt(length);
	}

	/**
	 * 轨迹Schema处理
	 * 
	 * @param inv 新库存信息
	 * @param trace 原始轨迹
	 * @param opType 操作类型
	 * @return
	 */
	private LZCertifyInventoryTraceSchema getInvTraceSchema(LZCertifyInventorySchema inv,
			LZCertifyInventoryTraceSchema trace, String opType , String prtno) {

		LZCertifyInventoryTraceSchema invTrace = new LZCertifyInventoryTraceSchema();

		invTrace.setSerialNo(PubFun1.CreateMaxNo("LZINVTRACK", 20));
		invTrace.setCertifyCode(inv.getCertifyCode());
		invTrace.setOperatingType(opType);
		invTrace.setStartNo(inv.getStartNo());
		invTrace.setEndNo(inv.getEndNo());
		invTrace.setSumCount(inv.getSumCount());
		invTrace.setSendOutCom(trace == null ? "SYS" : trace.getReceiveCom());
		invTrace.setReceiveCom(inv.getComCode());
		invTrace.setParentSerialNo(trace == null ? null : trace.getSerialNo());
		invTrace.setInventorySerialNo(inv.getSerialNo());
		invTrace.setArchiveFlag("00");
		invTrace.setOperator("cwad");
		invTrace.setMakeDate(new Date());
		invTrace.setMakeTime(PubFun.getCurrentTime());
		invTrace.setModifyDate(new Date());
		invTrace.setModifyTime(PubFun.getCurrentTime());
		invTrace.setPrtNo(prtno);

		return invTrace;
	}

	/**
	 * 轨迹Schema处理,针对拆分的没有实际使用的部分
	 * 
	 * @param inv 新库存信息
	 * @param trace 原始轨迹
	 * @param opType 操作类型
	 * @return
	 */
	private LZCertifyInventoryTraceSchema getRestTraceSchema(LZCertifyInventorySchema inv,
			LZCertifyInventoryTraceSchema trace, String opType) {

		LZCertifyInventoryTraceSchema invTrace = new LZCertifyInventoryTraceSchema();

		invTrace.setSerialNo(PubFun1.CreateMaxNo("LZINVTRACK", 20));
		invTrace.setCertifyCode(inv.getCertifyCode());
		invTrace.setOperatingType("99");
		invTrace.setStartNo(inv.getStartNo());
		invTrace.setEndNo(inv.getEndNo());
		invTrace.setSumCount(inv.getSumCount());
		invTrace.setSendOutCom(trace == null ? "SYS" : trace.getSendOutCom());
		invTrace.setReceiveCom(inv.getComCode());
		invTrace.setParentSerialNo(trace == null ? null : trace.getSerialNo());
		invTrace.setInventorySerialNo(inv.getSerialNo());
		invTrace.setArchiveFlag("00");
		invTrace.setOperator("cwad");
		invTrace.setMakeDate(new Date());
		invTrace.setMakeTime(PubFun.getCurrentTime());
		invTrace.setModifyDate(new Date());
		invTrace.setModifyTime(PubFun.getCurrentTime());

		return invTrace;
	}

	/**
	 * 库存Schema处理
	 * 
	 * @param certify 单证编码
	 * @param com
	 * @param start
	 * @param end
	 * @return
	 */
	private LZCertifyInventorySchema getInvSchema(String certify, String com , int count , String prtno) {

		LZCertifyInventorySchema inv = new LZCertifyInventorySchema();

		inv.setSerialNo(PubFun1.CreateMaxNo("LZINV", 20));
		inv.setCertifyCode(certify);
		inv.setSumCount(count);
		inv.setComCode(com);
		inv.setLevel("02");
		inv.setOperator("cwad");
		inv.setMakeDate(new Date());
		inv.setMakeTime(PubFun.getCurrentTime());
		inv.setModifyDate(new Date());
		inv.setModifyTime(PubFun.getCurrentTime());
		inv.setPrtNo(prtno);

		return inv;
	}
	
	
	private LZCertifyInventorySchema getInvSchema(String certify, String com, String start, String end) {

		LZCertifyInventorySchema inv = new LZCertifyInventorySchema();

		inv.setSerialNo(PubFun1.CreateMaxNo("LZINV", 20));
		inv.setCertifyCode(certify);
		inv.setStartNo(start);
		inv.setEndNo(end);
		inv.setSumCount(Integer.parseInt(end) - Integer.parseInt(start) + 1);
		inv.setComCode(com);
		inv.setLevel("02");
		inv.setOperator("cwad");
		inv.setMakeDate(new Date());
		inv.setMakeTime(PubFun.getCurrentTime());
		inv.setModifyDate(new Date());
		inv.setModifyTime(PubFun.getCurrentTime());

		return inv;
	}
	

	/**
	 * 获取数字对应的字符串
	 * 
	 * @param integer
	 * @param length
	 * @return
	 */
	private String getInterStr(int integer, int length) {

		String str = integer + "";

		for (int index = str.length(); index < length; index++) {
			str = "0" + str;
		}
		return str;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		System.out.println(error);
		CError tError = new CError();
		tError.moduleName = "CertifyTraceDeal";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getCErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {

	}
	
	
	public MMap firstTrack(String certify , int count , String prtno) {

		int length = getLength(certify);


		LZCertifyInventorySchema inv = getInvSchema(certify, mGlobalInput.ComCode, count , prtno);

		LZCertifyInventoryTraceSchema invTrace = getInvTraceSchema(inv, null, "00" , prtno);

		MMap tMMap = new MMap();
		tMMap.put(inv, "INSERT");
		tMMap.put(invTrace, "INSERT");

		return tMMap;
	}
	
	public MMap dealTrack(String certify, String sendOutCom, String incomeCom, String type, int sumCount, String parentno , String prtno) {

		// 多次使用时 从缓存取长度 避免重复调数据库
		int length = getLength(certify);

		// 获取号段的全部库存信息
		String queryInventory = "select * from LZCertifyInventory "
				+ " where certifycode = '" + certify  + "'"
				+ " and comcode='" + sendOutCom + "'"
				+ " and prtno = '" + prtno + "'";
//				+ " select * from LZCertifyInventory "
//				+ " where  certifycode = '" + certify + "'"
//				+ " and comcode='" + sendOutCom + "'";

		System.out.println(queryInventory);

		LZCertifyInventoryDB tLZCertifyInventoryDB = new LZCertifyInventoryDB();
		LZCertifyInventorySet invSet = tLZCertifyInventoryDB.executeQuery(queryInventory);

		if (invSet.size() == 0) {
			setErrors("不存在该单证号段的相关库存！", "dealTrack");
			return null;
		} else if (!checkInvent(invSet)) {
			// 这个是前提 必须现校验库存情况的完整性 已避免出现给的号段中存在库存没有的情况
			setErrors("该单证的库存与待处理的号段不符！", "dealTrack");
			return null;
		}

		// 待更新的数据集合
		LZCertifyInventorySet newInvSet = new LZCertifyInventorySet();
		LZcertifyAssignSet newAssignSet = new LZcertifyAssignSet();
		LZCertifyInventoryTraceSet newTraceSet = new LZCertifyInventoryTraceSet();
		LZCertifyInventorySet deleteInvSet = new LZCertifyInventorySet();
		LZCertifyInventoryTraceSet updateTraceSet = new LZCertifyInventoryTraceSet();
		
		
		
		
		
		
		

		for (int row = 1; row <= invSet.size(); row++) {
			LZCertifyInventorySchema invSchema = invSet.get(row);

			// 获取轨迹信息
			LZCertifyInventoryTraceDB tLZCertifyInventoryTraceDB = new LZCertifyInventoryTraceDB();
			tLZCertifyInventoryTraceDB.setInventorySerialNo(invSchema.getSerialNo());
			tLZCertifyInventoryTraceDB.setArchiveFlag("00");
			LZCertifyInventoryTraceSet tLZcertifyInventoryTraceSet = tLZCertifyInventoryTraceDB.query();

			if (tLZcertifyInventoryTraceSet.size() != 1) {
				setErrors("库存轨迹异常！", "dealTrack");
				return null;
			}

			// 原始轨迹信息
			LZCertifyInventoryTraceSchema oldTrace = tLZcertifyInventoryTraceSet.get(1);


					LZCertifyInventorySchema inv = getInvSchema(certify, incomeCom, sumCount,prtno );
					LZCertifyInventoryTraceSchema trace = getInvTraceSchema(inv, oldTrace, type , prtno);
					if(type.equals("02")&& sumCount == invSchema.getSumCount()){
//						 待更新的Schema
						LZCertifyInventorySchema invs = getInvSchema(certify, incomeCom, sumCount , prtno);
						LZCertifyInventoryTraceSchema traces = getInvTraceSchema(invs, oldTrace, type ,prtno);

						if ("03".equals(type)) {
							LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
							mReflections.transFields(ass, invs);
							ass.setSendOutCom(sendOutCom);
							ass.setReceiveCom(incomeCom);
							newAssignSet.add(ass);
						} else {
							newInvSet.add(invs);
						}
						newTraceSet.add(traces);
					}
					
					if(type.equals("04") && sumCount == invSchema.getSumCount()){

						// 待更新的Schema
						LZCertifyInventorySchema invs = getInvSchema(certify, incomeCom, sumCount , prtno);
						LZCertifyInventoryTraceSchema traces = getInvTraceSchema(invs, oldTrace, type ,prtno);

						if ("03".equals(type)) {
							LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
							mReflections.transFields(ass, invs);
							ass.setSendOutCom(sendOutCom);
							ass.setReceiveCom(incomeCom);
							newAssignSet.add(ass);
						} else {
							newInvSet.add(invs);
						}
						newTraceSet.add(traces);
					
					}

					if (sumCount < invSchema.getSumCount()) {
						// 拆分的第一部分
						LZCertifyInventorySchema fInv = getInvSchema(certify, incomeCom, sumCount , prtno);
						LZCertifyInventoryTraceSchema fTrace = getInvTraceSchema(fInv, oldTrace, type ,prtno);

						// 拆分的第二部分
						LZCertifyInventorySchema sInv = getInvSchema(certify, invSchema.getComCode(),invSchema.getSumCount() - sumCount , prtno );
						LZCertifyInventoryTraceSchema sTrace = getRestTraceSchema(sInv, oldTrace, type , prtno);

						if ("03".equals(type)) {
							LZcertifyAssignSchema ass = new LZcertifyAssignSchema();
							mReflections.transFields(ass, fInv);
							ass.setSendOutCom(sendOutCom);
							ass.setReceiveCom(incomeCom);
							newAssignSet.add(ass);
						} else {
							newInvSet.add(fInv);
						}

						newInvSet.add(sInv);
						newTraceSet.add(fTrace);
						newTraceSet.add(sTrace);
					} else {
						newInvSet.add(inv);
						newTraceSet.add(trace);
					}
					
			
			
			// 处理原始数据
			oldTrace.setArchiveFlag("01");
			oldTrace.setSumCount(oldTrace.getSumCount());
			oldTrace.setParentSerialNo(parentno);
			oldTrace.setPrtNo(prtno);
			deleteInvSet.add(invSchema);
			updateTraceSet.add(oldTrace);
		}

		MMap tMMap = new MMap();
		tMMap.put(newInvSet, "INSERT");
		tMMap.put(newTraceSet, "INSERT");
		tMMap.put(newAssignSet, "INSERT");
		tMMap.put(deleteInvSet, "DELETE");
		tMMap.put(updateTraceSet, "UPDATE");

		return tMMap;
	}
	
	private boolean checkInvent(LZCertifyInventorySet invSet) {

		// 总数量
		int sumCount = 0;
		// 最大号最小号差额

		LZCertifyInventorySchema invSchema = invSet.get(1);


		for (int row = 2; row <= invSet.size(); row++) {
			invSchema = invSet.get(row);


		// 判断差额
			sumCount = invSchema.getSumCount();
		}
		if(sumCount < 0 ){
			return false;
		}

		// 如果不等中间肯定有间断
		return true ;
	}
	
	private LZCertifyInventoryTraceSchema getInvTraceSchema(LZCertifyInventorySchema inv,
			LZCertifyInventoryTraceSchema trace, String opType ) {

		LZCertifyInventoryTraceSchema invTrace = new LZCertifyInventoryTraceSchema();

		invTrace.setSerialNo(PubFun1.CreateMaxNo("LZINVTRACK", 20));
		invTrace.setCertifyCode(inv.getCertifyCode());
		invTrace.setOperatingType(opType);
		invTrace.setStartNo(inv.getStartNo());
		invTrace.setEndNo(inv.getEndNo());
		invTrace.setSumCount(inv.getSumCount());
		invTrace.setSendOutCom(trace == null ? "SYS" : trace.getReceiveCom());
		invTrace.setReceiveCom(inv.getComCode());
		invTrace.setParentSerialNo(trace == null ? null : trace.getSerialNo());
		invTrace.setInventorySerialNo(inv.getSerialNo());
		invTrace.setArchiveFlag("00");
		invTrace.setOperator("cwad");
		invTrace.setMakeDate(new Date());
		invTrace.setMakeTime(PubFun.getCurrentTime());
		invTrace.setModifyDate(new Date());
		invTrace.setModifyTime(PubFun.getCurrentTime());
		return invTrace;
	}
	
	
	private LZCertifyInventoryTraceSchema getRestTraceSchema(LZCertifyInventorySchema inv,
			LZCertifyInventoryTraceSchema trace, String opType ,String prtno) {

		LZCertifyInventoryTraceSchema invTrace = new LZCertifyInventoryTraceSchema();

		invTrace.setSerialNo(PubFun1.CreateMaxNo("LZINVTRACK", 20));
		invTrace.setCertifyCode(inv.getCertifyCode());
		invTrace.setOperatingType("99");
		
		invTrace.setSumCount(inv.getSumCount());
		invTrace.setSendOutCom(trace == null ? "SYS" : trace.getSendOutCom());
		invTrace.setReceiveCom(inv.getComCode());
		invTrace.setParentSerialNo(trace == null ? null : trace.getSerialNo());
		invTrace.setInventorySerialNo(inv.getSerialNo());
		invTrace.setArchiveFlag("00");
		invTrace.setOperator("cwad");
		invTrace.setMakeDate(new Date());
		invTrace.setMakeTime(PubFun.getCurrentTime());
		invTrace.setModifyDate(new Date());
		invTrace.setModifyTime(PubFun.getCurrentTime());
		invTrace.setPrtNo(prtno);

		return invTrace;
	}
	
	
}
