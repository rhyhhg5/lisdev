package com.sinosoft.lis.certifymanage;

import java.util.Date;

import com.sinosoft.lis.db.LZcertifyBatchStateDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyBatchFinishBL {

	private CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mBatchNo;

	private MMap mMap = new MMap();

	/**
	 * 入口函数
	 * 
	 * @param tVData
	 * @param tType 处理类型 ONLY--选择处理 BATCH--全部处理
	 * @return
	 */
	public boolean submitData(VData tVData, String tType) {

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}

		return true;
	}

	/**
	 * @param tInputData
	 * @return
	 */
	private boolean getInputData(VData tInputData) {

		TransferData tTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);

		mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);

		mBatchNo = (String) tTransferData.getValueByName("BatchNo");

		if (mGlobalInput == null) {
			setErrors("没有得到足够的信息！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.equals("")) {
			setErrors("请选择处理征订批次！", "getInputData");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {

		LZcertifyBatchStateDB batchDB = new LZcertifyBatchStateDB();
		batchDB.setBatchNo(mBatchNo);

		if (!batchDB.getInfo()) {
			setErrors("未找到待结束的征订批次", "dealData");
			return false;
		}
		
		if(!mGlobalInput.ManageCom.equals(batchDB.getComCode())){
			setErrors("您没有权限结束本次批次征订", "dealData");
			return false;
		}
		
		batchDB.setBatchState("02");
		batchDB.setEndDate(new Date());
		batchDB.setModifyDate(new Date());
		batchDB.setModifyTime(PubFun.getCurrentTime());
		
		mMap.put(batchDB.getSchema(), "UPDATE");

		return true;
	}

	/**
	 * @return
	 */
	private boolean submit() {
		// 提交数据库
		VData mInputData = new VData();
		mInputData.add(mMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("--提交数据库失败！--" + tPubSubmit.mErrors.getLastError());

			setErrors(tPubSubmit.mErrors.getLastError(), "submit");

			return false;
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}
}
