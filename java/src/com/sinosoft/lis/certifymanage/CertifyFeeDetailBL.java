package com.sinosoft.lis.certifymanage;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LZCertifyInventoryTraceSchema;
import com.sinosoft.lis.vschema.LZCertifyInventoryTraceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 单证分摊费用处理类
 * 
 * @author 张成轩
 */
public class CertifyFeeDetailBL {

	private String mOutXmlPath = null;

	private String zipPath = null;

	private String mBatchNo = "";

	private Map comName = new HashMap();

	private GlobalInput mGI = null;

	private CErrors mErrors = new CErrors();

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {

		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

		TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

		if (mGI == null || tf == null) {
			setErrors("传入的信息不完整！", "getInputData");
			return false;
		}

		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mBatchNo = (String) tf.getValueByName("BatchNo");

		if (mOutXmlPath == null) {
			setErrors("下载文件路径异常！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.trim().equals("")) {
			setErrors("请选择待分摊的征订批次！", "getInputData");
			return false;
		}

		zipPath = mOutXmlPath + mBatchNo + ".zip";
		mOutXmlPath += (mBatchNo + "_" + PubFun.getCurrentDate2() + PubFun.getCurrentTime2() + "/");

		return true;
	}

	private boolean dealData() {

		if (!creatDir(mOutXmlPath)) {
			setErrors("创建文件夹异常", "dealData");
			return false;
		}

		ExeSQL tEx = new ExeSQL();

		SSRS printList = tEx
				.execSQL("select printcode,(select PrintName from LMPrintCom where PrintCode=sl.PrintCode) from lzsendoutlist sl where batchno='"
						+ mBatchNo + "' group by printcode");
		for (int index = 1; index <= printList.MaxRow; index++) {

			List sendoutList = new ArrayList();
			List allotList = new ArrayList();

			// 记录有多少机构
			Set comSet = new HashSet();

			String traceSql = "Select Cit.Serialno, Sol.Price, cit.certifycode,sol.comcode,sol.startno,sol.endno,(select CertifyName from LMCardDescription where certifycode=cit.certifycode) From Lzcertifyinventorytrace Cit Inner Join Lzsendoutlist Sol On Sol.Certifycode = Cit.Certifycode And Sol.Startno = Cit.Startno And Sol.Endno = Cit.Endno Where Sol.batchno='"
					+ mBatchNo
					+ "' and Cit.Operatingtype = '01' and sol.printcode = '"
					+ printList.GetText(index, 1)
					+ "'";
			SSRS traceSSRS = tEx.execSQL(traceSql);

			// 对每个寄发轨迹进行处理
			for (int row = 1; row <= traceSSRS.getMaxRow(); row++) {

				String[] sendout = new String[6];

				sendout[0] = traceSSRS.GetText(row, 4);
				sendout[1] = traceSSRS.GetText(row, 3);
				sendout[2] = traceSSRS.GetText(row, 5);
				sendout[3] = traceSSRS.GetText(row, 6);
				sendout[4] = traceSSRS.GetText(row, 2);
				sendout[5] = traceSSRS.GetText(row, 7);

				sendoutList.add(sendout);
				comSet.add(traceSSRS.GetText(row, 4));

				String serialNo = traceSSRS.GetText(row, 1);

				CertifyTraceQuery tQuery = new CertifyTraceQuery();
				tQuery.getChildTrace(serialNo, "04");
				LZCertifyInventoryTraceSet traceSet = tQuery.getSet();

				for (int tIndex = 1; tIndex <= traceSet.size(); tIndex++) {
					LZCertifyInventoryTraceSchema schema = traceSet.get(tIndex);

					String send = schema.getSendOutCom();
					String receice = schema.getReceiveCom();

					if (!send.substring(0, 4).equals(receice.substring(0, 4))) {
						String[] allot = new String[7];

						allot[0] = schema.getSendOutCom();
						allot[1] = schema.getReceiveCom();
						allot[2] = schema.getCertifyCode();
						allot[3] = schema.getStartNo();
						allot[4] = schema.getEndNo();
						// 单价
						allot[5] = traceSSRS.GetText(row, 2);
						// 单证名称
						allot[6] = traceSSRS.GetText(row, 7);

						allotList.add(allot);

						comSet.add(schema.getSendOutCom());
						comSet.add(schema.getReceiveCom());
					}
				}

			}

			// 开始处理了
			Iterator i = comSet.iterator();
			while (i.hasNext()) {

				String com = (String) i.next();
				String comName = getComName(com);

				creatDir(mOutXmlPath + com + "/");

				String fileDir = mOutXmlPath + com + "/";

				String[][] tToExcel = new String[sendoutList.size() + allotList.size() + 15][11];

				tToExcel[0][0] = "单证费用分摊清单";

				tToExcel[2][0] = "征订批次：" + mBatchNo;
				tToExcel[2][7] = "印刷厂：" + printList.GetText(index, 2);

				tToExcel[3][0] = "单证编码";
				tToExcel[3][1] = "单证名称";
				tToExcel[3][2] = "单位";
				tToExcel[3][3] = "单价";
				tToExcel[3][4] = "寄发数量";
				tToExcel[3][5] = "总价";
				tToExcel[3][6] = "寄发起始号";
				tToExcel[3][7] = "寄发终止号";
				tToExcel[3][8] = "印刷厂";
				tToExcel[3][9] = "寄发机构";

				int xlsRow = 4;

				System.out.println("------------------");

				double sumSend = 0;

				for (int row = 0; row < sendoutList.size(); row++) {
					String[] arr = (String[]) sendoutList.get(row);
					if (arr[0].equals(com)) {

						int count = Integer.parseInt(arr[3]) - Integer.parseInt(arr[2]) + 1;
						double price = Double.parseDouble(arr[4]);

						tToExcel[xlsRow][0] = arr[1];
						tToExcel[xlsRow][1] = arr[5];
						tToExcel[xlsRow][2] = "份";
						tToExcel[xlsRow][3] = arr[4];
						tToExcel[xlsRow][4] = count + "";
						tToExcel[xlsRow][5] = count * price + "";
						tToExcel[xlsRow][6] = arr[2];
						tToExcel[xlsRow][7] = arr[3];
						tToExcel[xlsRow][8] = printList.GetText(index, 2);
						tToExcel[xlsRow][9] = comName;

						sumSend += count * price;

						xlsRow++;
					}
				}
				tToExcel[xlsRow][1] = "合计：";
				tToExcel[xlsRow][5] = sumSend + "";

				xlsRow++;

				tToExcel[xlsRow + 1][0] = "调拨出库情况";

				tToExcel[xlsRow + 2][0] = "单证编码";
				tToExcel[xlsRow + 2][1] = "单证名称";
				tToExcel[xlsRow + 2][2] = "单位";
				tToExcel[xlsRow + 2][3] = "单价";
				tToExcel[xlsRow + 2][4] = "寄发数量";
				tToExcel[xlsRow + 2][5] = "总价";
				tToExcel[xlsRow + 2][6] = "寄发起始号";
				tToExcel[xlsRow + 2][7] = "寄发终止号";
				tToExcel[xlsRow + 2][8] = "印刷厂";
				tToExcel[xlsRow + 2][9] = "调拨出库机构";
				tToExcel[xlsRow + 2][10] = "调拨入库机构";

				xlsRow += 3;

				double sumOut = 0;

				for (int row = 0; row < allotList.size(); row++) {
					String[] arr = (String[]) allotList.get(row);
					if (arr[0].equals(com)) {

						int count = Integer.parseInt(arr[4]) - Integer.parseInt(arr[3]) + 1;
						double price = Double.parseDouble(arr[5]);

						tToExcel[xlsRow][0] = arr[2];
						tToExcel[xlsRow][1] = arr[6];
						tToExcel[xlsRow][2] = "份";
						tToExcel[xlsRow][3] = arr[5];
						tToExcel[xlsRow][4] = count + "";
						tToExcel[xlsRow][5] = count * price + "";
						tToExcel[xlsRow][6] = arr[3];
						tToExcel[xlsRow][7] = arr[4];
						tToExcel[xlsRow][8] = printList.GetText(index, 2);
						tToExcel[xlsRow][9] = comName;
						tToExcel[xlsRow][10] = getComName(arr[1]);

						sumOut += count * price;

						xlsRow++;
					}
				}

				tToExcel[xlsRow][1] = "合计：";
				tToExcel[xlsRow][5] = sumOut + "";

				xlsRow++;

				tToExcel[xlsRow + 1][0] = "调拨入库情况";

				tToExcel[xlsRow + 2][0] = "单证编码";
				tToExcel[xlsRow + 2][1] = "单证名称";
				tToExcel[xlsRow + 2][2] = "单位";
				tToExcel[xlsRow + 2][3] = "单价";
				tToExcel[xlsRow + 2][4] = "寄发数量";
				tToExcel[xlsRow + 2][5] = "总价";
				tToExcel[xlsRow + 2][6] = "寄发起始号";
				tToExcel[xlsRow + 2][7] = "寄发终止号";
				tToExcel[xlsRow + 2][8] = "印刷厂";
				tToExcel[xlsRow + 2][9] = "调拨出库机构";
				tToExcel[xlsRow + 2][10] = "调拨入库机构";

				xlsRow += 3;

				double sumIn = 0;

				for (int row = 0; row < allotList.size(); row++) {
					String[] arr = (String[]) allotList.get(row);
					if (arr[1].equals(com)) {
						int count = Integer.parseInt(arr[4]) - Integer.parseInt(arr[3]) + 1;
						double price = Double.parseDouble(arr[5]);

						tToExcel[xlsRow][0] = arr[2];
						tToExcel[xlsRow][1] = arr[6];
						tToExcel[xlsRow][2] = "份";
						tToExcel[xlsRow][3] = arr[5];
						tToExcel[xlsRow][4] = count + "";
						tToExcel[xlsRow][5] = count * price + "";
						tToExcel[xlsRow][6] = arr[3];
						tToExcel[xlsRow][7] = arr[4];
						tToExcel[xlsRow][8] = printList.GetText(index, 2);
						tToExcel[xlsRow][9] = getComName(arr[0]);
						tToExcel[xlsRow][10] = comName;

						sumIn += count * price;

						xlsRow++;
					}
				}

				tToExcel[xlsRow][1] = "合计：";
				tToExcel[xlsRow][5] = sumIn + "";

				tToExcel[xlsRow + 2][0] = "总合计：";
				tToExcel[xlsRow + 2][5] = sumSend - sumOut + sumIn + "";

				creatExcel(tToExcel, fileDir + mBatchNo + "_" + printList.GetText(index, 1) + "_" + com + ".xls");
			}
		}

		creatZip();

		return true;
	}

	private boolean creatExcel(String[][] tToExcel, String filePath) {

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, tToExcel);
			t.write(filePath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		return true;
	}

	private final String getComName(String comCode) {

		String com = (String) comName.get(comCode);

		if (com == null) {
			ExeSQL tEx = new ExeSQL();
			com = tEx.getOneValue("select name from ldcom where comcode='" + comCode + "'");
			comName.put(comCode, com);
		}

		return com;
	}

	/**
	 * 创建文件夹
	 * 
	 * @param dirPath
	 * @return
	 */
	private boolean creatDir(String dirPath) {

		boolean succFlag = true;

		try {
			File myFilePath = new File(dirPath);

			if (!myFilePath.exists()) {
				succFlag = myFilePath.mkdir();
			}
		} catch (Exception e) {
			e.printStackTrace();
			succFlag = false;
		}
		return succFlag;
	}

	/**
	 * 创建zip
	 * 
	 * @return
	 */
	private boolean creatZip() {

		System.out.println("zip:" + zipPath);
		System.out.println("temp:" + mOutXmlPath);
		
		ZipCompressor zip = new ZipCompressor(zipPath);
		zip.compress(mOutXmlPath);

		deletefile(mOutXmlPath);

		return true;
	}

	/**
	 * 删除文件夹
	 * 
	 * @param delpath
	 * @return
	 */
	public static boolean deletefile(String delpath) {
		try {

			File file = new File(delpath);
			// 当且仅当此抽象路径名表示的文件存在且 是一个目录时，返回 true
			if (!file.isDirectory()) {
				file.delete();
			} else if (file.isDirectory()) {
				String[] filelist = file.list();
				for (int i = 0; i < filelist.length; i++) {
					File delfile = new File(delpath + "/" + filelist[i]);
					if (!delfile.isDirectory()) {
						delfile.delete();
						System.out.println("delete:" + delfile.getAbsolutePath());
					} else if (delfile.isDirectory()) {
						deletefile(delpath + "/" + filelist[i]);
					}
				}
				file.delete();
				System.out.println("delete:" + file.getAbsolutePath());
			}

		} catch (Exception e) {
			System.out.println("deletefile() Exception:" + e.getMessage());
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}
	
	public String getZipPath(){
		return zipPath;
	}

	/**
	 * 测试使用
	 * 
	 * @param arr
	 */
	public void showArr(String[][] arr) {
		System.out.println("---开始---");
		for (int row = 0; row < arr.length; row++) {
			String[] t = arr[row];
			for (int col = 0; col < t.length; col++) {
				System.out.print((t[col] == null ? "" : t[col]) + "|");
			}
			System.out.println();
		}
		System.out.println("---结束---");
	}

	public static void main(String[] arr) {

		GlobalInput tG = new GlobalInput();

		tG.Operator = "cwad";
		tG.ManageCom = "86110000";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "C:\\Documents and Settings\\Administrator\\桌面\\new\\");
		tTransferData.setNameAndValue("BatchNo", "ZD201307170015");

		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);

		CertifyFeeDetailBL tCertifyFeeDetailBL = new CertifyFeeDetailBL();

		tCertifyFeeDetailBL.submitData(vData, "");
	}
}
