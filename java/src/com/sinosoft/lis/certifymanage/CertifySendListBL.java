package com.sinosoft.lis.certifymanage;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class CertifySendListBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData;

	private VData mResult = new VData();

	private String mOperate;

	private MMap map = new MMap();

	private Map lengthMap = new HashMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZSendOutListSet mLZSendOutListSet = new LZSendOutListSet();

	ExeSQL tExeSQL = new ExeSQL();

	public CertifySendListBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {

			this.mOperate = cOperate;
			if (!mOperate.equals("INSERT") && !mOperate.equals("UPDATE")
					&& !mOperate.equals("DELETE")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!dealData()) {
				return false;
			}
			if (!prepareOutputData())
				return false;

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "OLDDiseaseBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}

		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PrintingShopBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {

		LZSendOutListSet tLZSendOutListSett = new LZSendOutListSet();

		if (mOperate.equals("INSERT")) {
			for (int i = 1; i <= mLZSendOutListSet.size(); i++) {

				LZSendOutListSchema tLZSendOutListSchema = new LZSendOutListSchema();
				tLZSendOutListSchema = mLZSendOutListSet.get(i);

				String tCertifyCode = tLZSendOutListSchema.getCertifyCode();
				String tStartNo = tLZSendOutListSchema.getStartNo();
				String tEndNo = tLZSendOutListSchema.getEndNo();
				int length = getLength(tCertifyCode);
				
				String strSQL = "select distinct havenumber from lmcarddescription where certifycode = '"+ tCertifyCode +"'" 
				+ " with ur ";
			
			String havenum = tExeSQL.getOneValue(strSQL);
			System.out.println("这是个无号单证" + havenum ) ;
			
			if(havenum.equals("N")){

				
				//判断号段是否印刷过 
				String sSql = "select 1 from LZPrintList where  certifycode ='" + tCertifyCode + "'with ur";
				if (!"1".equals(tExeSQL.getOneValue(sSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'号段'" + tStartNo + "'-'" + tEndNo
							+ " 尚未印刷，请重新输入！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}

				String tSql = "select 1 from LZSendOutList where batchno='"
						+ tLZSendOutListSchema.getBatchNo()
						+ "' and certifycode = '" + tCertifyCode
						+ "'  and comcode= '"
						+ tLZSendOutListSchema.getComCode() + "' with ur";

				if ("1".equals(tExeSQL.getOneValue(tSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "dealData";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'已添加寄发通知单！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}
				
				String mSql = "select serialno from lzprintlist where batchno='"
						+ tLZSendOutListSchema.getBatchNo()
						+ "' and certifycode = '" + tCertifyCode
						+ "' with ur";
					if(!tExeSQL.getOneValue(mSql).equals("") && !tExeSQL.getOneValue(mSql).equals(null) ){
						tLZSendOutListSchema.setPrtNo(tExeSQL.getOneValue(mSql));
					}

				double tPrice = tLZSendOutListSchema.getPrice();
				double tTotalPrice = tPrice * tLZSendOutListSchema.getSumCount();

				System.out.println("总价为》》》》》》》》》》》》" + tTotalPrice);

				String SerialNo = PubFun1.CreateMaxNo("SERNUM", "");
				tLZSendOutListSchema.setSerialNo(SerialNo);
				tLZSendOutListSchema.setTotalPrice(tTotalPrice);
				tLZSendOutListSchema.setSumCount(tLZSendOutListSchema.getSumCount());
				// 00-制定寄发通知书 01-寄发通知单制定完毕 02-未出库 03-已出库
				tLZSendOutListSchema.setDealState("00");
				tLZSendOutListSchema.setMakeDate(mCurrentDate);
				tLZSendOutListSchema.setMakeTime(mCurrentTime);
				tLZSendOutListSchema.setModifyDate(mCurrentDate);
				tLZSendOutListSchema.setModifyTime(mCurrentTime);
				tLZSendOutListSchema.setOperator(mGlobalInput.Operator);
			}
				
			if(havenum.equals("Y")){	
				
				if ("".equals(tStartNo)) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'起始号为空！！！ ";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tEndNo)) {
					CError tError = new CError();
					tError.moduleName = "CertifyPrinListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'终止号为空！！！ ";
					this.mErrors.addOneError(tError);
					return false;
				}

				String startNo = getInterStr(Integer.parseInt(tStartNo), length);
				String endNo = getInterStr(Integer.parseInt(tEndNo), length);
				
				//判断号段是否印刷过 
				String sSql = "select 1 from LZPrintList where  startno <= '"
						+ endNo + "' and endno>='" + startNo
						+ "'  and certifycode ='" + tCertifyCode + "'with ur";
				if (!"1".equals(tExeSQL.getOneValue(sSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'号段'" + tStartNo + "'-'" + tEndNo
							+ " 尚未印刷，请重新输入！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}
				// 判断号段是否使用
				String strSql = "select 1 from LZSendOutList where  startno <= '"
						+ endNo
						+ "' and endno>='"
						+ startNo
						+ "'  and certifycode ='" + tCertifyCode + "'with ur";
				if ("1".equals(tExeSQL.getOneValue(strSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'号段'" + tStartNo + "'-'" + tEndNo
							+ " 已使用，请重新输入号段！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}

				int count = Integer.parseInt(endNo) - Integer.parseInt(startNo)
						+ 1;

				if (count != tLZSendOutListSchema.getSumCount()) {

					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "cheeckDate";
					tError.errorMessage = "第'" + i + "'款单证,印刷数量和号段不符！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}
				String tSql = "select 1 from LZSendOutList where batchno='"
						+ tLZSendOutListSchema.getBatchNo()
						+ "' and certifycode = '" + tCertifyCode
						+ "'  and comcode= '"
						+ tLZSendOutListSchema.getComCode() + "' with ur";

				if ("1".equals(tExeSQL.getOneValue(tSql))) {
					CError tError = new CError();
					tError.moduleName = "CertifySendListBL";
					tError.functionName = "dealData";
					tError.errorMessage = "第'" + i + "'款单证，单证编码'"
							+ tCertifyCode + "'已添加寄发通知单！！！ ";
					this.mErrors.addOneError(tError);
					return false;

				}

				double tPrice = tLZSendOutListSchema.getPrice();
				double tTotalPrice = tPrice * count;

				System.out.println("总价为》》》》》》》》》》》》" + tTotalPrice);

				String SerialNo = PubFun1.CreateMaxNo("SERNUM", "");
				tLZSendOutListSchema.setSerialNo(SerialNo);
				tLZSendOutListSchema.setTotalPrice(tTotalPrice);
				tLZSendOutListSchema.setStartNo(startNo);
				tLZSendOutListSchema.setEndNo(endNo);
				tLZSendOutListSchema.setSumCount(count);
				// 00-制定寄发通知书 01-寄发通知单制定完毕 02-未出库 03-已出库
				tLZSendOutListSchema.setDealState("00");
				tLZSendOutListSchema.setMakeDate(mCurrentDate);
				tLZSendOutListSchema.setMakeTime(mCurrentTime);
				tLZSendOutListSchema.setModifyDate(mCurrentDate);
				tLZSendOutListSchema.setModifyTime(mCurrentTime);
				tLZSendOutListSchema.setOperator(mGlobalInput.Operator);

				
			}
				tLZSendOutListSett.add(tLZSendOutListSchema);

			}

			map.put(tLZSendOutListSett, "INSERT");
		} else if (mOperate.equals("DELETE")) {

			map.put(mLZSendOutListSet, "DELETE");

		} else if (mOperate.equals("UPDATE")) {

			for (int i = 1; i <= mLZSendOutListSet.size(); i++) {

				String sql = "Update LZSendOutList set DealState='01', "
						+ "ModifyDate = '" + mCurrentDate + "', "
						+ "ModifyTime = '" + mCurrentTime + "' "
						+ "Where  batchno = '"
						+ mLZSendOutListSet.get(i).getBatchNo() + "' ";
				map.put(sql, "UPDATE"); // 修改

			}

		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mLZSendOutListSet = ((LZSendOutListSet) cInputData
				.getObjectByObjectName("LZSendOutListSet", 0));
		System.out.println("getinput" + mLZSendOutListSet.size());
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	/**
	 * 获取单证号码长度
	 * 
	 * @param length
	 * @return
	 */
	private int getLength(String certify) {

		String length = (String) lengthMap.get(certify);

		if (length == null) {
			length = new ExeSQL()
					.getOneValue("select certifylength from LMCardDescription where certifycode='"
							+ certify + "'");
			if (length == null || length.trim().equals("")) {
				// 没长度
				length = "10";
			}
			lengthMap.put(certify, length);
		}
		return Integer.parseInt(length);
	}

	/**
	 * 获取数字对应的字符串
	 * 
	 * @param integer
	 * @param length
	 * @return
	 */
	private String getInterStr(int integer, int length) {

		String str = integer + "";

		for (int index = str.length(); index < length; index++) {
			str = "0" + str;
		}
		return str;
	}

	/**
	 * 获取返回结果
	 * 
	 * @param integer
	 * @param length
	 * @return
	 */
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinBankAddBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.map);
			mResult = mInputData;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareData", ex.getMessage());
			return false;
		}
		return true;
	}
}
