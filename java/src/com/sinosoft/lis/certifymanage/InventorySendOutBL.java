package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.db.LZSendOutListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZSendOutListSchema;
import com.sinosoft.lis.vschema.LZSendOutListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 单证征订出库处理类
 * 
 * @author 张成轩
 */
public class InventorySendOutBL {

	private CErrors mErrors = new CErrors();

	private String mOperatType;

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mBatchNo;

	private String mCertifyCom;

	private LZSendOutListSet mLZSendOutListSet;
	
	ExeSQL tExeSQL = new ExeSQL();

	/**
	 * 入口函数
	 * 
	 * @param tVData
	 * @param tType 处理类型 ONLY--选择处理 COMBATCH--机构全部处理 BATCH--全部处理
	 * @return
	 */
	public boolean submitData(VData tVData, String tType) {

		this.mOperatType = tType;

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * @param tInputData
	 * @return
	 */
	private boolean getInputData(VData tInputData) {

		TransferData tTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);

		mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);

		mBatchNo = (String) tTransferData.getValueByName("BatchNo");

		mCertifyCom = (String) tTransferData.getValueByName("CertifyCom");

		mLZSendOutListSet = (LZSendOutListSet) tInputData.getObjectByObjectName("LZSendOutListSet", 0);

		if (mGlobalInput == null || mOperatType == null) {
			setErrors("没有得到足够的信息！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.equals("")) {
			setErrors("请选择处理征订批次！", "getInputData");
			return false;
		}

		if (mOperatType.equals("COMBATCH") && (mCertifyCom == null || mCertifyCom.trim().equals(""))) {
			setErrors("请选择待处理的征订机构！", "getInputData");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {

		boolean succFlag = false;

		if (mOperatType.equals("BATCH")) {

			succFlag = sendOutAll(null);

		} else if (mOperatType.equals("COMBATCH")) {

			succFlag = sendOutAll(mCertifyCom);

		} else {

			succFlag = sendOut(mLZSendOutListSet);
		}

		return succFlag;
	}

	/**
	 * 全部出库、机构出库处理
	 * 
	 * @param tCertifyCom
	 * @return
	 */
	private boolean sendOutAll(String tCertifyCom) {

		LZSendOutListDB tLZSendOutList = new LZSendOutListDB();
		tLZSendOutList.setBatchNo(mBatchNo);
		tLZSendOutList.setDealState("01");
		tLZSendOutList.setComCode(tCertifyCom);

		LZSendOutListSet tLZSendOutListSet = tLZSendOutList.query();

		if (tLZSendOutListSet.size() == 0) {
			setErrors("该批次已全部出库，未发现需要出库的单证！", "sendOutAll");
			return false;
		}

		return sendOutDeal(tLZSendOutListSet);
	}

	/**
	 * 勾选出库处理
	 * 
	 * @param tLZSendOutListSet
	 * @return
	 */
	private boolean sendOut(LZSendOutListSet tLZSendOutListSet) {

		if (tLZSendOutListSet.size() == 0) {
			setErrors("请勾选待出库单证！", "sendOut");
			return false;
		}

		CertifyLimitCheck tCertifyLimitCheck = new CertifyLimitCheck(mGlobalInput);
		
		LZSendOutListSet dealList = new LZSendOutListSet();

		for (int index = 1; index <= tLZSendOutListSet.size(); index++) {

			LZSendOutListSchema tLZSendOutListSchema = tLZSendOutListSet.get(index);
			LZSendOutListDB tLZListDB = tLZSendOutListSchema.getDB();

			if (!tLZListDB.getInfo()) {
				setErrors("查询单证寄发信息失败！", "sendOut");
				return false;
			}

			if (!mBatchNo.equals(tLZListDB.getBatchNo())) {
				setErrors("单证所在批次与所选批次不符！", "sendOut");
				return false;
			}

			if (!"01".equals(tLZListDB.getDealState())) {
				setErrors("单证" + tLZListDB.getCertifyCode() + "已进行出库，请重新点击查询后再进行出库操作", "sendOut");
				return false;
			}
			
			if (!tCertifyLimitCheck.checkLimit(mGlobalInput.ComCode, tLZListDB.getComCode(), "01")) {
				setErrors(tCertifyLimitCheck.getErrors().getFirstError(), "sendOut");
				return false;
			}

			dealList.add(tLZListDB.getSchema());
		}

		return sendOutDeal(dealList);
	}

	/**
	 * 单证出库处理
	 * 
	 * @param tLZSendOutListSet
	 * @return
	 */
	private boolean sendOutDeal(LZSendOutListSet tLZSendOutListSet) {

		int errorIndex = 0;
		String errorInfo = "";

		CertifyTraceDeal tCertifyTraceDeal = new CertifyTraceDeal(mGlobalInput);

		for (int index = 1; index <= tLZSendOutListSet.size(); index++) {
			LZSendOutListSchema tSendSchema = tLZSendOutListSet.get(index);
			tSendSchema.setDealState("02");
			MMap tMap = new MMap();
			
			String strSQL = "select distinct havenumber from lmcarddescription where certifycode = '"+ tSendSchema.getCertifyCode() +"'" 
			+ " with ur ";
			System.out.println(strSQL);
			String havenum = tExeSQL.getOneValue(strSQL);
			
			if(havenum.equals("N")){
				 tMap = tCertifyTraceDeal.dealTrack(tSendSchema.getCertifyCode(), mGlobalInput.ManageCom, tSendSchema
							.getComCode(), "01" , tSendSchema.getSumCount() , tSendSchema.getSerialNo() ,tSendSchema.getPrtNo());

					if (tMap == null) {
						errorInfo += (++errorIndex + ".单证" + tSendSchema.getCertifyCode() + "出库处理失败：" + tCertifyTraceDeal
								.getCErrors().getLastError());
						continue;
					}
			}
			
			
			if(!havenum.equals("N")){
				int start = Integer.parseInt(tSendSchema.getStartNo());
				int end = Integer.parseInt(tSendSchema.getEndNo());

				 tMap = tCertifyTraceDeal.dealTrack(tSendSchema.getCertifyCode(), mGlobalInput.ManageCom, tSendSchema
						.getComCode(), start, end, "01");

				if (tMap == null) {
					errorInfo += (++errorIndex + ".单证" + tSendSchema.getCertifyCode() + "出库处理失败：" + tCertifyTraceDeal
							.getCErrors().getLastError());
					continue;
				}
			}
			

			tMap.put(tSendSchema, "UPDATE");

			// 按单证进行提交
			if (!submit(tMap)) {
				errorInfo += (++errorIndex + ".单证" + tSendSchema.getCertifyCode() + "出库数据处理失败！");
			}
		}

		if (errorIndex > 0) {
			setErrors(errorInfo, "sendOutDeal");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean submit(MMap tMap) {
		// 提交数据库
		VData mInputData = new VData();
		mInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("--提交数据库失败！--" + tPubSubmit.mErrors.getLastError());

			setErrors(tPubSubmit.mErrors.getLastError(), "submit");

			return false;
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventorySendOutBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		String sNoLimit = PubFun.getNoLimit("86420000");
		System.out.println(sNoLimit);
	}
}
