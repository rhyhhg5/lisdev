package com.sinosoft.lis.certifymanage;

import java.util.HashMap;
import java.util.Map;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CertifyLimitCheck {

	private CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 机构层级权限
	private Map mComMap = new HashMap();

	// 入库权限
	private Map mInMap = new HashMap();

	public CertifyLimitCheck(GlobalInput tGlobalInput) {
		
		this.mGlobalInput = tGlobalInput;
		
		SSRS tSSRS = new ExeSQL().execSQL("select code,codealias,othersign from ldcode where codetype='certifycom'");
		for (int row = 1; row <= tSSRS.MaxRow; row++) {
			String code = tSSRS.GetText(row, 1);
			String codealias = tSSRS.GetText(row, 2);
			String othersign = tSSRS.GetText(row, 3);

			mComMap.put(code, codealias);
			mInMap.put(code, othersign);
		}
	}

	/**
	 * 校验操作类型及层级关系
	 * 
	 * @param sendOutCom
	 * @param incomeCom
	 * @param type
	 * @return
	 */
	public boolean checkLimit(String sendOutCom, String incomeCom, String type) {

		String operatCom = mGlobalInput.ComCode;

		String operatLimit = mGlobalInput.ComCode.length() + "";
		String sendOutLimit = sendOutCom.length() + "";
		String incomeLimit = incomeCom.length() + "";

		if (!"00".equals(type) && sendOutCom.equals(incomeCom)) {
			setErrors("发放机构与接收机构不能相同！", "checkType");
			return false;
		}

		if ("00".equals(type) || "01".equals(type)) {

			String inLimit = (String) mInMap.get(operatLimit);
			if (!"1".equals(inLimit)) {
				setErrors("本机构没有出库入库权限！", "checkType");
				return false;
			}

		} else if ("02".equals(type)) {

			if (!operatCom.equals(sendOutCom)) {
				setErrors("下发单证必须为本机构库存单证！", "checkType");
				return false;
			}

			String sLimit = (String) mComMap.get(sendOutLimit);

			if (!sLimit.equals(incomeLimit)) {
				setErrors("发放机构无法下发到该接收机构！", "checkType");
				return false;
			}

		} else if ("03".equals(type)) {

			if (!operatCom.equals(sendOutCom)) {
				setErrors("下发单证必须为本机构库存单证！", "checkType");
				return false;
			}

		} else if ("04".equals(type)) {

			String oLimit = (String) mComMap.get(operatLimit);

			if (!oLimit.equals(sendOutLimit)) {
				setErrors("本机构无法操作发放机构的单证调拨！", "checkType");
				return false;
			}

			if (!sendOutLimit.equals(incomeLimit)) {
				setErrors("单证调拨无法跨机构层级！", "checkType");
				return false;
			}

		} else {
			setErrors("操作类型异常！", "checkType");
			return false;
		}

		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ComCode = "86";
		
		CertifyLimitCheck tCertifyLimitCheck = new CertifyLimitCheck(tGlobalInput);
		if(tCertifyLimitCheck.checkLimit("86", "8612", "02")){
			System.out.println(tCertifyLimitCheck.getErrors().getFirstError());
		}
	}
}
