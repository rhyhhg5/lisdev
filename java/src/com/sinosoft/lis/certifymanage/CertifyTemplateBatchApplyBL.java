package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;

public class CertifyTemplateBatchApplyBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData;

	private VData mResult = new VData();

	private String mOperate;

	private MMap map = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZcertifyBatchStateSet mLZcertifyBatchStateSet = new LZcertifyBatchStateSet();

	ExeSQL tExeSQL = new ExeSQL();

	public CertifyTemplateBatchApplyBL() {
	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {

			this.mOperate = cOperate;
			if (!mOperate.equals("APPLY")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!dealData()) {
				return false;
			}
			if (!prepareOutputData())
				return false;

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "OLDDiseaseBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}

		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PrintingShopBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {

		if (mOperate.equals("APPLY")) {
			for (int i = 1; i <= mLZcertifyBatchStateSet.size(); i++) {

				String strSql = "select 1 from LZcertifyBatchState where batchno='"
						+ mLZcertifyBatchStateSet.get(i).getBatchNo()
						+ "' with ur";

				String strCount = tExeSQL.getOneValue(strSql);

				if ("1".equals(strCount)) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CertifyListBL";
					tError.functionName = "CertifyListBL";
					tError.errorMessage = "批次号为："
							+ mLZcertifyBatchStateSet.get(i).getBatchNo()
							+ " 的纪录已经存在！";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLZcertifyBatchStateSet.get(i).setMakeDate(mCurrentDate);
				mLZcertifyBatchStateSet.get(i).setMakeTime(mCurrentTime);
				mLZcertifyBatchStateSet.get(i).setModifyDate(mCurrentDate);
				mLZcertifyBatchStateSet.get(i).setModifyTime(mCurrentTime);
				mLZcertifyBatchStateSet.get(i).setOperator(
						mGlobalInput.Operator);
			}
			map.put(this.mLZcertifyBatchStateSet, "INSERT");
		}
		return true;

	}

	/**
	 * 从输入数据中得到所有对象
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { //打印付费
		//全局变量
		mLZcertifyBatchStateSet = ((LZcertifyBatchStateSet) cInputData
				.getObjectByObjectName("LZcertifyBatchStateSet", 0));
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinBankAddBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.map);
			mResult = mInputData;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareData", ex.getMessage());
			return false;
		}
		return true;
	}
}
