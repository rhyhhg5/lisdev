package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.db.LZPrintListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZPrintListSchema;
import com.sinosoft.lis.vschema.LZPrintListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 单证征订入库处理类
 * 
 * @author 张成轩
 */
public class InventoryIncomeBL {

	private CErrors mErrors = new CErrors();

	private String mOperatType;

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZPrintListSet mLZPrintListSet = new LZPrintListSet();

	private String mBatchNo;

	private MMap mMap = new MMap();
	
	ExeSQL tExeSQL = new ExeSQL();

	/**
	 * 入口函数
	 * 
	 * @param tVData
	 * @param tType 处理类型 ONLY--选择处理 BATCH--全部处理
	 * @return
	 */
	public boolean submitData(VData tVData, String tType) {

		this.mOperatType = tType;

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!submit()) {
			return false;
		}

		return true;
	}

	/**
	 * @param tInputData
	 * @return
	 */
	private boolean getInputData(VData tInputData) {

		TransferData tTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);

		mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);

		mLZPrintListSet = (LZPrintListSet) tInputData.getObjectByObjectName("LZPrintListSet", 0);

		mBatchNo = (String) tTransferData.getValueByName("BatchNo");

		if (mGlobalInput == null || mOperatType == null) {
			setErrors("没有得到足够的信息！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.equals("")) {
			setErrors("请选择处理征订批次！", "getInputData");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {

		boolean succFlag = false;

		if (mOperatType.equals("BATCH")) {

			succFlag = incomeAll();

		} else {

			succFlag = income(mLZPrintListSet);
		}

		return succFlag;
	}

	/**
	 * 全部入库处理
	 * 
	 * @return
	 */
	private boolean incomeAll() {

		LZPrintListDB tLZPrintListDB = new LZPrintListDB();
		tLZPrintListDB.setBatchNo(mBatchNo);
		tLZPrintListDB.setDealState("01");

		LZPrintListSet tLZPrintListSet = tLZPrintListDB.query();

		if (tLZPrintListSet.size() == 0) {
			setErrors("该批次已全部入库，未发现需要入库的单证！", "incomeAll");
			return false;
		}

		return incomeDeal(tLZPrintListSet);
	}

	/**
	 * 勾选入库处理
	 * 
	 * @param tLZPrintListSet
	 * @return
	 */
	private boolean income(LZPrintListSet tLZPrintListSet) {

		if (tLZPrintListSet.size() == 0) {
			setErrors("请勾选待入库单证！", "income");
			return false;
		}

		CertifyLimitCheck tCertifyLimitCheck = new CertifyLimitCheck(mGlobalInput);
		if (!tCertifyLimitCheck.checkLimit(mGlobalInput.ComCode, mGlobalInput.ComCode, "00")) {
			setErrors(tCertifyLimitCheck.getErrors().getFirstError(), "income");
			return false;
		}
		
		LZPrintListSet dealPrintList = new LZPrintListSet();

		for (int index = 1; index <= tLZPrintListSet.size(); index++) {

			LZPrintListSchema tLZPrintListSchema = tLZPrintListSet.get(index);
			LZPrintListDB tLZPrintList = tLZPrintListSchema.getDB();

			if (!tLZPrintList.getInfo()) {
				setErrors("查询单证印刷信息失败！", "income");
				return false;
			}

			if (!mBatchNo.equals(tLZPrintList.getBatchNo())) {
				setErrors("单证所在批次与所选批次不符！", "income");
				return false;
			}

			if (!"01".equals(tLZPrintList.getDealState())) {
				setErrors("单证" + tLZPrintList.getCertifyCode() + "已进行入库，请重新点击查询后再进行入库操作", "sendOut");
				return false;
			}

			dealPrintList.add(tLZPrintList.getSchema());
		}

		return incomeDeal(dealPrintList);
	}

	/**
	 * 入库处理
	 * 
	 * @param tLZPrintListSet
	 * @return
	 */
	private boolean incomeDeal(LZPrintListSet tLZPrintListSet) {

		CertifyTraceDeal tCertifyTraceDeal = new CertifyTraceDeal(mGlobalInput);
		for (int index = 1; index <= tLZPrintListSet.size(); index++) {
			LZPrintListSchema tLZPrintListSchema = tLZPrintListSet.get(index);
			tLZPrintListSchema.setDealState("02");
			String strSQL = "select distinct havenumber from lmcarddescription where certifycode = '"+ tLZPrintListSchema.getCertifyCode() +"'" 
			+ " with ur ";
			System.out.println(strSQL);
			String havenum = tExeSQL.getOneValue(strSQL);
			System.out.println(havenum);
			MMap tMap = new MMap();
			
			if(havenum.equals("N")){
				tMap = tCertifyTraceDeal.firstTrack(tLZPrintListSchema.getCertifyCode(),tLZPrintListSchema.getSumCount(),tLZPrintListSchema.getSerialNo() );
				if (tMap == null) {

					String error = tCertifyTraceDeal.getCErrors().getLastError();
					setErrors(error, "income");
					return false;
				}
			}
			if(!havenum.equals("N")){
				 tMap = tCertifyTraceDeal.firstTrack(tLZPrintListSchema.getCertifyCode(), Integer
						.parseInt(tLZPrintListSchema.getStartNo()), Integer.parseInt(tLZPrintListSchema.getEndNo()));
				if (tMap == null) {

					String error = tCertifyTraceDeal.getCErrors().getLastError();
					setErrors(error, "income");
					return false;
				}
			}
			
			

			mMap.put(tLZPrintListSchema, "UPDATE");
			mMap.add(tMap);
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean submit() {
		// 提交数据库
		VData mInputData = new VData();
		mInputData.add(mMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("--提交数据库失败！--" + tPubSubmit.mErrors.getLastError());

			setErrors(tPubSubmit.mErrors.getLastError(), "submit");

			return false;
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		String sNoLimit = PubFun.getNoLimit("86420000");
		System.out.println(sNoLimit);
	}
}
