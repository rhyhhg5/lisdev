package com.sinosoft.lis.certifymanage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LZCertifyInventoryTraceSchema;
import com.sinosoft.lis.vschema.LZCertifyInventoryTraceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 单证分摊费用处理类
 * 
 * @author 张成轩
 */
public class CertifyFeeApportionBL {

	private String mOutXmlPath = null;

	private String mBatchNo = "";

	private GlobalInput mGI = null;

	private CErrors mErrors = new CErrors();

	// 记录各机构合计金额
	private Map sumFeeMap = new HashMap();

	// 记录各机构调拨出金额
	private Map sumSubMap = new HashMap();

	// 记录各机构调拨进入金额
	private Map sumAddMap = new HashMap();

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {

		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

		TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

		if (mGI == null || tf == null) {
			setErrors("传入的信息不完整！", "getInputData");
			return false;
		}

		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mBatchNo = (String) tf.getValueByName("BatchNo");

		if (mOutXmlPath == null) {
			setErrors("下载文件路径异常！", "getInputData");
			return false;
		}

		if (mBatchNo == null || mBatchNo.trim().equals("")) {
			setErrors("请选择待分摊的征订批次！", "getInputData");
			return false;
		}

		return true;
	}

	private boolean dealData() {

		ExeSQL tEx = new ExeSQL();

		// 机构初使总费用获取，用于校验总金额
		String sumSql = "select ComCode,(select name from ldcom where comcode=sol.comcode),sum(TotalPrice) from LZSendOutList sol where batchno='"
				+ mBatchNo + "' group by comcode order by comcode";
		SSRS sumSSRS = tEx.execSQL(sumSql);

		// 查询库存轨迹

		// 先查入库轨迹
		String traceSql = "Select Cit.Serialno, Sol.Price, cit.certifycode From Lzcertifyinventorytrace Cit Inner Join Lzsendoutlist Sol On Sol.Certifycode = Cit.Certifycode And Sol.Startno = Cit.Startno And Sol.Endno = Cit.Endno Where Sol.batchno='"
				+ mBatchNo + "' and Cit.Operatingtype = '01'";
		SSRS traceSSRS = tEx.execSQL(traceSql);

		// 对每个寄发轨迹进行处理
		for (int row = 1; row <= traceSSRS.getMaxRow(); row++) {

			// 单价用于计算费用
			double price = Double.parseDouble(traceSSRS.GetText(row, 2));

			String serialNo = traceSSRS.GetText(row, 1);

			CertifyTraceQuery tQuery = new CertifyTraceQuery();
			tQuery.getChildTrace(serialNo);

			LZCertifyInventoryTraceSet traceSet = tQuery.getSet();

			System.out.println(traceSSRS.GetText(row, 3));

			// 对于一个寄发轨迹的所有轨迹进行处理
			for (int index = 1; index <= traceSet.size(); index++) {
				LZCertifyInventoryTraceSchema schema = traceSet.get(index);

				String sendOutCom = schema.getSendOutCom();
				String receiveCom = schema.getReceiveCom();

				// 不统计下发业务员的
				if (schema.getOperatingType().equals("03")) {
					continue;
				}

				double money = schema.getSumCount() * price;

				if (receiveCom.length() >= 4) {
					receiveCom = receiveCom.substring(0, 4);

					setSumMoney(receiveCom, money);

				}

				if (sendOutCom.length() >= 4) {
					sendOutCom = sendOutCom.substring(0, 4);

					setSumMoney(sendOutCom, -money);
				}

				// 如果跨机构调拨 处理出入金额
				if (receiveCom.length() == 4 && sendOutCom.length() == 4 && !receiveCom.equals(sendOutCom)) {
					setApportionMoney(sendOutCom, receiveCom, money);
				}
			}

		}

		String count = tEx.getOneValue("select count(1) from ldcom where length(trim(comcode)) = 4 with ur");

		String[][] tToExcel = new String[Integer.parseInt(count) + 10][6];

		tToExcel[0][0] = "单证费用分摊清单";

		tToExcel[2][0] = "征订批次：" + mBatchNo;

		tToExcel[3][0] = "机构编码";
		tToExcel[3][1] = "机构名称";
		tToExcel[3][2] = "征订寄发金额";
		tToExcel[3][3] = "调拨出库金额";
		tToExcel[3][4] = "调拨入库金额";
		tToExcel[3][5] = "分摊金额";

		int excelRow = 3;

		for (int row = 1; row <= sumSSRS.MaxRow; row++) {

			excelRow++;

			String com = sumSSRS.GetText(row, 1);
			Object outMoney = sumSubMap.get(com);
			Object inMoney = sumAddMap.get(com);
			Object sumMoney = sumFeeMap.get(com);

			tToExcel[excelRow][0] = com;
			tToExcel[excelRow][1] = sumSSRS.GetText(row, 2);
			tToExcel[excelRow][2] = sumSSRS.GetText(row, 3);
			tToExcel[excelRow][3] = (outMoney == null ? "0.0" : (String) outMoney);
			tToExcel[excelRow][4] = (inMoney == null ? "0.0" : (String) inMoney);
			tToExcel[excelRow][5] = (sumMoney == null ? "0.0" : (String) sumMoney);
		}

		Iterator iter = sumAddMap.keySet().iterator();

		excelRow++;

		while (iter.hasNext()) {
			String key = (String) iter.next();

			boolean dealFlag = true;

			for (int row = 1; row <= sumSSRS.MaxRow; row++) {
				String com = sumSSRS.GetText(row, 1);
				if (key.equals(com)) {
					dealFlag = false;
					break;
				}
			}

			if (dealFlag) {
				excelRow++;

				Object outMoney = sumSubMap.get(key);
				Object sumMoney = sumFeeMap.get(key);

				tToExcel[excelRow][0] = key;
				tToExcel[excelRow][1] = tEx.getOneValue("select name from ldcom where comcode='" + key + "'");
				tToExcel[excelRow][2] = "0.0";
				tToExcel[excelRow][3] = (outMoney == null ? "0.0" : (String) outMoney );
				tToExcel[excelRow][4] = (String) sumAddMap.get(key);
				tToExcel[excelRow][5] = (sumMoney == null ? "0.0" : (String) sumMoney);
			}

		}

		excelRow += 2;

		tToExcel[excelRow][0] = "制表人:" + mGI.Operator;
		tToExcel[excelRow][4] = "制表日期:" + PubFun.getCurrentDate();

		showArr(tToExcel);

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * 累积金额
	 * 
	 * @param com
	 * @param money
	 */
	private void setSumMoney(String com, double money) {

		// 处理金额集
		if (sumFeeMap.get(com) == null) {

			sumFeeMap.put(com, money + "");

		} else {

			double sumMoney = Double.parseDouble((String) sumFeeMap.get(com));
			sumMoney += money;
			sumFeeMap.put(com, sumMoney + "");

		}
	}

	/**
	 * 累积调拨金额
	 * 
	 * @param com
	 * @param money
	 */
	private void setApportionMoney(String sendOutCom, String receiveCom, double money) {
		// 处理调拨金额集
		if (sumSubMap.get(sendOutCom) == null) {

			sumSubMap.put(sendOutCom, money + "");

		} else {

			double sumMoney = Double.parseDouble((String) sumSubMap.get(sendOutCom));
			sumMoney += money;
			sumSubMap.put(sendOutCom, sumMoney + "");
		}

		// 处理调拨金额集
		if (sumAddMap.get(receiveCom) == null) {

			sumAddMap.put(receiveCom, money + "");

		} else {

			double sumMoney = Double.parseDouble((String) sumAddMap.get(receiveCom));
			sumMoney += money;
			sumAddMap.put(receiveCom, sumMoney + "");
		}
	}

	/**
	 * 测试使用
	 * 
	 * @param arr
	 */
	private void showArr(String[][] arr) {
		System.out.println("---开始---");
		for (int row = 0; row < arr.length; row++) {
			String[] t = arr[row];
			for (int col = 0; col < t.length; col++) {
				System.out.print((t[col] == null ? "" : t[col]) + "|");
			}
			System.out.println();
		}
		System.out.println("---结束---");
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {

		GlobalInput tG = new GlobalInput();

		tG.Operator = "cwad";
		tG.ManageCom = "86110000";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "");
		tTransferData.setNameAndValue("BatchNo", "ZD2013070201");

		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);

		CertifyFeeApportionBL tCertifyFeeApportionBL = new CertifyFeeApportionBL();

		tCertifyFeeApportionBL.submitData(vData, "");

	}
}
