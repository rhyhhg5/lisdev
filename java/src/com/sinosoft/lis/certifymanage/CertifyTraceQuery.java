package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.db.LZCertifyInventoryTraceDB;
import com.sinosoft.lis.schema.LZCertifyInventoryTraceSchema;
import com.sinosoft.lis.vschema.LZCertifyInventoryTraceSet;

public class CertifyTraceQuery {
	
	private LZCertifyInventoryTraceSet mTraceSet = new LZCertifyInventoryTraceSet();
	
	/**
	 * 获取全部子节点轨迹，包含本级轨迹
	 * 
	 * @param serialNo 本级轨迹流水
	 * @return
	 */
	public boolean getChildTrace(String serialNo){
		return getChildTrace(serialNo, null);
	}
	
	/**
	 * 获取指定操作类型的全部子节点轨迹，包含本级轨迹
	 * 
	 * @param serialNo 本级轨迹流水
	 * @param operat 操作方式
	 * @return
	 */
	public boolean getChildTrace(String serialNo, String operat){
		
		// 现获取到第一个轨迹的信息
		LZCertifyInventoryTraceDB fDB = new LZCertifyInventoryTraceDB();
		fDB.setSerialNo(serialNo);
		
		if(!fDB.getInfo()){
			return false;
		}
		
		if(operat == null || operat.equals(fDB.getOperatingType())){
			mTraceSet.add(fDB.getSchema());
		}
		
		getTrace(fDB.getSchema(), operat);
		
		return true;
	}
	
	/**
	 * 获取下级轨迹
	 * 
	 * @param serialNo 父节点归集
	 * @return
	 */
	private void getTrace(LZCertifyInventoryTraceSchema db, String operat){
		
		LZCertifyInventoryTraceDB traceDB = new LZCertifyInventoryTraceDB();
		traceDB.setParentSerialNo(db.getSerialNo());
		LZCertifyInventoryTraceSet tTraceSet = traceDB.query();
		
		for(int row = 1; row <= tTraceSet.size(); row++){
			LZCertifyInventoryTraceSchema schema = tTraceSet.get(row);
			
			getTrace(schema, operat);
			
			if(!schema.getOperatingType().equals("99")){
				if(operat == null || operat.equals(schema.getOperatingType())){
					mTraceSet.add(schema);
				}
			}
		}
	}
	
	/**
	 * 获取结果集
	 * 
	 * @return
	 */
	public LZCertifyInventoryTraceSet getSet(){
		return mTraceSet;
	}
	
}
