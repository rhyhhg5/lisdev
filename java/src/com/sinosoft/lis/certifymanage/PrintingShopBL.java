package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;

public class PrintingShopBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData;

	private VData mResult = new VData();

	private String mOperate;

	private MMap map = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LMPrintComSchema mLMPrintComSchema = new LMPrintComSchema();

	public PrintingShopBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {

			this.mOperate = cOperate;
			if (!mOperate.equals("INSERT") && !mOperate.equals("UPDATE")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!dealData()) {
				return false;
			}
			if (!prepareOutputData())
				return false;

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "OLDDiseaseBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}

		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PrintingShopBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {

		LMPrintComSchema tLMPrintComSchema = new LMPrintComSchema();
		if (mOperate.equals("INSERT")) {
			String SerialNo = PubFun1.CreateMaxNo("SeqNo", "");
			tLMPrintComSchema.setSerialNo(SerialNo);
			tLMPrintComSchema.setPrintCode(mLMPrintComSchema.getPrintCode());
			tLMPrintComSchema.setPrintName(mLMPrintComSchema.getPrintName());
			tLMPrintComSchema.setEffectiveState(mLMPrintComSchema
					.getEffectiveState());
			tLMPrintComSchema.setLinkMan(mLMPrintComSchema.getLinkMan());
			tLMPrintComSchema.setPhone(mLMPrintComSchema.getPhone());
			tLMPrintComSchema.setPostalAddress(mLMPrintComSchema
					.getPostalAddress());
			tLMPrintComSchema.setRemark(mLMPrintComSchema.getRemark());
			tLMPrintComSchema.setMakeDate(mCurrentDate);
			tLMPrintComSchema.setMakeTime(mCurrentTime);
			tLMPrintComSchema.setModifyDate(mCurrentDate);
			tLMPrintComSchema.setModifyTime(mCurrentTime);
			tLMPrintComSchema.setOperator(mGlobalInput.Operator);
			map.put(tLMPrintComSchema, "INSERT"); // 插入
		} else if (mOperate.equals("UPDATE")) {

			String sql = "Update LMPrintCom set " + "EffectiveState = '"
					+ mLMPrintComSchema.getEffectiveState() + "', "
					+ "printname = '"+ mLMPrintComSchema.getPrintName()+"',"
					+ "Operator = '" + mGlobalInput.Operator + "', "
					+ "Remark = '"+ mLMPrintComSchema.getRemark() +"',"
					+ "LinkMan = '"+ mLMPrintComSchema.getLinkMan() +"',"
					+ "Phone = '"+ mLMPrintComSchema.getPhone() +"',"
					+ "PostalAddress = '"+ mLMPrintComSchema.getPostalAddress()+"',"
					+ "ModifyDate = '" + mCurrentDate + "', "
					+ "ModifyTime = '" + mCurrentTime + "' "
					+ "Where  PrintCode = '" + mLMPrintComSchema.getPrintCode()
					+ "' ";
			map.put(sql, "UPDATE"); // 修改
		}
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mLMPrintComSchema = ((LMPrintComSchema) cInputData
				.getObjectByObjectName("LMPrintComSchema", 0));
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinBankAddBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.map);
			mResult = mInputData;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareData", ex.getMessage());
			return false;
		}
		return true;
	}
}
