package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCertifyInventorySchema;
import com.sinosoft.lis.vschema.LZCertifyInventorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 单证下发及调拨处理
 * 
 * @author 张成轩
 */
public class SendOutManageBL {

	private CErrors mErrors = new CErrors();

	private String mOperatType;

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZCertifyInventorySet mLZCertifyInventorySet;

	private String mCSendCom;

	private String mCReceiveCom;

	private String mPSendCom;

	private String mPReceivePerson;
	
	ExeSQL tExeSQL = new ExeSQL();

	/**
	 * 入口函数
	 * 
	 * @param tVData
	 * @param tType 处理类型 ONLY--选择处理 COMBATCH--机构全部处理 BATCH--全部处理
	 * @return
	 */
	public boolean submitData(VData tVData, String tType) {

		this.mOperatType = tType;

		if (!getInputData(tVData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * @param tInputData
	 * @return
	 */
	private boolean getInputData(VData tInputData) {

		TransferData tTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);

		mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);

		mLZCertifyInventorySet = (LZCertifyInventorySet) tInputData.getObjectByObjectName("LZCertifyInventorySet", 0);
		mCSendCom = (String) tTransferData.getValueByName("CSendCom");
		mCReceiveCom = (String) tTransferData.getValueByName("CReceiveCom");
		mPSendCom = (String) tTransferData.getValueByName("PSendCom");
		mPReceivePerson = (String) tTransferData.getValueByName("PReceivePerson");

		if (mGlobalInput == null || mOperatType == null || mOperatType == null) {
			setErrors("没有得到足够的信息！", "getInputData");
			return false;
		}

		if (mLZCertifyInventorySet == null || mLZCertifyInventorySet.size() == 0) {
			setErrors("请添加对应的发放、调拨单证！", "getInputData");
			return false;
		}

		CertifyLimitCheck tCertifyLimitCheck = new CertifyLimitCheck(mGlobalInput);
		
		if ("02".equals(mOperatType) || "04".equals(mOperatType)) {

			if (mCSendCom == null || mCSendCom.trim().equals("")) {
				setErrors("请选择发放机构！", "getInputData");
				return false;
			}
			if (mCReceiveCom == null || mCReceiveCom.trim().equals("")) {
				setErrors("请选择接收机构！", "getInputData");
				return false;
			}
			
			if (!tCertifyLimitCheck.checkLimit(mCSendCom, mCReceiveCom, mOperatType)) {
				setErrors(tCertifyLimitCheck.getErrors().getFirstError(), "getInputData");
				return false;
			}

		} else if ("03".equals(mOperatType)) {
			if (mPSendCom == null || mPSendCom.trim().equals("")) {
				setErrors("请选择发放机构！", "getInputData");
				return false;
			}
			if (mPReceivePerson == null || mPReceivePerson.trim().equals("")) {
				setErrors("请选择接收人！", "getInputData");
				return false;
			}
			
			if (!tCertifyLimitCheck.checkLimit(mPSendCom, mPReceivePerson, mOperatType)) {
				setErrors(tCertifyLimitCheck.getErrors().getFirstError(), "getInputData");
				return false;
			}
		} else {
			setErrors("处理类型错误！", "getInputData");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean dealData() {

		boolean succFlag = false;

		if ("02".equals(mOperatType) || "04".equals(mOperatType)) {

			succFlag = sendOutDeal(mLZCertifyInventorySet, mCSendCom, mCReceiveCom);

		} else if ("03".equals(mOperatType)) {

			succFlag = sendOutDeal(mLZCertifyInventorySet, mPSendCom, mPReceivePerson);

		}

		return succFlag;
	}

	/**
	 * 单证出库处理
	 * 
	 * @param tLZSendOutListSet
	 * @return
	 */
	private boolean sendOutDeal(LZCertifyInventorySet tLZCertifyInventorySet, String sendCom, String receiveCom) {

		int errorIndex = 0;
		String errorInfo = "";

		CertifyTraceDeal tCertifyTraceDeal = new CertifyTraceDeal(mGlobalInput);

		for (int index = 1; index <= tLZCertifyInventorySet.size(); index++) {
			
			LZCertifyInventorySchema tSendSchema = tLZCertifyInventorySet.get(index);

			String strSQL = "select distinct havenumber from lmcarddescription where certifycode = '"+ tSendSchema.getCertifyCode() +"'" 
			+ " with ur ";
			System.out.println(strSQL);
			String havenum = tExeSQL.getOneValue(strSQL);
			
			MMap tMap = new MMap();
			
			if(havenum.equals("N")){
				tMap = tCertifyTraceDeal.dealTrack(
						tSendSchema.getCertifyCode(), sendCom, receiveCom,
						 mOperatType , tSendSchema.getSumCount() , tSendSchema.getSerialNo(),tSendSchema.getPrtNo() );
			}
			
			if(!havenum.equals("N")){
				
				int start = Integer.parseInt(tSendSchema.getStartNo());
				int end = Integer.parseInt(tSendSchema.getEndNo());

				if (end - start + 1 != tSendSchema.getSumCount()) {
					errorInfo += (++errorIndex + ".单证"
							+ tSendSchema.getCertifyCode() + "数量与号段不符！");
					continue;
				}

				tMap = tCertifyTraceDeal.dealTrack(
						tSendSchema.getCertifyCode(), sendCom, receiveCom,
						start, end, mOperatType);
			}

			if (tMap == null) {
				errorInfo += (++errorIndex + ".单证" + tSendSchema.getCertifyCode() + "出库处理失败：" + tCertifyTraceDeal
						.getCErrors().getLastError());
				continue;
			}

			// 按单证进行提交
			if (!submit(tMap)) {
				errorInfo += (++errorIndex + ".单证" + tSendSchema.getCertifyCode() + "出库数据处理失败！");
			}
		}

		if (errorIndex > 0) {
			setErrors(errorInfo, "sendOutDeal");
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	private boolean submit(MMap tMap) {
		// 提交数据库
		VData mInputData = new VData();
		mInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("--提交数据库失败！--" + tPubSubmit.mErrors.getLastError());

			setErrors(tPubSubmit.mErrors.getLastError(), "submit");

			return false;
		}
		return true;
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventorySendOutBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		String sNoLimit = PubFun.getNoLimit("86420000");
		System.out.println(sNoLimit);
	}
}
