package com.sinosoft.lis.certifymanage;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LMCardDescriptionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.vschema.LMCardDescriptionSet;
import com.sinosoft.lis.vschema.LZCertifyInventoryTraceSet;
import com.sinosoft.utility.*;

public class CertifyAccountsBL {

	private String mOutXmlPath = null;

	private List mBatchNo = new ArrayList();

	private String mCertifyCode = "";

	private GlobalInput mGI = null;
	

	private CErrors mErrors = new CErrors();

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {

		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			setErrors("传入的信息不完整！", "getInputData");
			return false;
		}

		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mBatchNo = (List) tf.getValueByName("BatchNo");
		mCertifyCode = (String) tf.getValueByName("CertifyCode");

		if (mOutXmlPath == null) {
			setErrors("下载文件路径异常！", "getInputData");
			return false;
		}

//		if (mBatchNo == null || mBatchNo.trim().equals("")) {
//			setErrors("批次号为空，请录入批次号！", "getInputData");
//			return false;
//		}
		
		return true;
	}

	private boolean dealData() {

		ExeSQL tEx = new ExeSQL();

		String tCertifyCode = "";

		boolean tFlag;
		if (mCertifyCode != null && !mCertifyCode.trim().equals("")) {

			tCertifyCode = " and lci.certifycode ='" + mCertifyCode + "' ";
		}
		
		StringBuffer mBatNo = new StringBuffer();
		String a = "";
		if(mBatchNo.size() != 0){
			for(int i = 0; i < mBatchNo.size(); i++){
				a = (String) mBatchNo.get(i);
				if(mBatchNo.get(i) != null && !mBatchNo.get(i).equals("") && mBatchNo.size() == 1 ){
					
					mBatNo.append("" + a + "");
					
					
				}else if(mBatchNo.get(i) != null && !mBatchNo.get(i).equals("") && mBatchNo.size() > 2 ){
					if(i == 0){
						
						mBatNo.append("" + a + "");
					}else if(i == mBatchNo.size() - 1){
						mBatNo.append("'"+ a + "" );
					}else if(i == 1){
						mBatNo.append("','"+ a + "'," );
					}else {
						mBatNo.append("'" + a +"',");
					}
				}else if(mBatchNo.get(i) != null && !mBatchNo.get(i).equals("") && mBatchNo.size() == 2 ){
					if(i == 0){
						
						mBatNo.append("" + a + "");
					}else if(i == mBatchNo.size() - 1){
						mBatNo.append("','"+ a + "" );
					}
				}
			}
		}

		String strSQL = " select lci.serialno from  LZcertifyInventoryTrace lci ,LZPrintList lzp where ( lci.certifycode =lzp.certifycode"
				+ " and lzp.batchno in ( '"
				+ mBatNo
				+ "' )and lzp.startno= lci.startno  and lzp.endno= lci.endno "
				+ " and lci.OperatingType='00' " + " " + tCertifyCode + ""
				+ " ) or ("
				+ " lci.certifycode =lzp.certifycode"
				+ " and lzp.batchno in ( '"
				+ mBatNo
				+ "' )and lzp.startno is null  and lzp.endno is null and lci.prtno = lzp.serialno "
				+ " and lci.OperatingType in('00') " + " " + tCertifyCode + "" 
				+ ") with ur"
				;
		
		System.out.println("1111111111111111111111111111111" + strSQL);
		
		SSRS sumSSRS  = tEx.execSQL(strSQL);
		
		LZCertifyInventoryTraceSet mLZcertifyInventoryTraceSet = new LZCertifyInventoryTraceSet();

		for (int row = 1; row <= sumSSRS.MaxRow; row++) {

			CertifyTraceQuery tCertifyTraceQuery = new CertifyTraceQuery();
			LZCertifyInventoryTraceSet tLZcertifyInventoryTraceSet = new LZCertifyInventoryTraceSet();
			// 成功标记
			tFlag = tCertifyTraceQuery.getChildTrace(sumSSRS.GetText(row, 1));

			if (tFlag) {

				tLZcertifyInventoryTraceSet.add(tCertifyTraceQuery.getSet());

			} else {
				System.out.println("获取轨迹失败！！");
				return false;
			}
			mLZcertifyInventoryTraceSet.add(tLZcertifyInventoryTraceSet);
		}
		// 获取数量
		int count = mLZcertifyInventoryTraceSet.size();
		System.out.println("行数为" + count);

		String[][] tToExcel = new String[count + 10][18];

		tToExcel[0][0] = "台账下载清单";

		tToExcel[3][0] = "征订批次：" ;

		tToExcel[3][1] = "年";
		tToExcel[3][2] = "月";
		tToExcel[3][3] = "日";
		tToExcel[3][4] = "单证编码";
		tToExcel[3][5] = "单证名称";
		tToExcel[3][6] = "单价";
		tToExcel[3][7] = "操作类型";
		tToExcel[3][8] = "出库机构";
		tToExcel[3][9] = "出库机构名称";
		tToExcel[3][10] = "入库机构";
		tToExcel[3][11] = "入库机构名称";
		tToExcel[3][12] = "总价";
		tToExcel[3][13] = "数量";
		tToExcel[3][14] = "起始号";
		tToExcel[3][15] = "终止号";
		tToExcel[3][16] = "印刷厂名称";
		tToExcel[3][17] = "库存量";

		int excelRow = 3;
		double tPrice = 0;
		double tTotalPrice;
		String makedate = "";
		String year = "";
		String month = "";
		String day = "";
		String tCertifyName = "";
		String tSendOutCom = "";
		String tReceiveCom = "";		
		for (int row = 1; row <= count; row++) {

			excelRow++;
			// 获取日期
			makedate = mLZcertifyInventoryTraceSet.get(row).getMakeDate();
			// 获取年
			year = makedate.split("-")[0];
			// 获取月
			month = makedate.split("-")[1];
			// 获取日
			day = makedate.split("-")[2];

			System.out.println("年" + year);
			System.out.println("月" + month);
			System.out.println("日" + day);

			// 获取单证名称以及单证单价
			LMCardDescriptionDB sLMCardDescriptionDB = new LMCardDescriptionDB();
			LMCardDescriptionSet sLMCardDescriptionSet = new LMCardDescriptionSet();
			sLMCardDescriptionDB.setCertifyCode(mLZcertifyInventoryTraceSet.get(row).getCertifyCode());
			sLMCardDescriptionSet = sLMCardDescriptionDB.query();

			for (int m = 1; m <= sLMCardDescriptionSet.size(); m++) {

				tCertifyName = sLMCardDescriptionSet.get(m).getCertifyName();
				tPrice = sLMCardDescriptionSet.get(m).getPrice();
			}

				tTotalPrice = tPrice
					* mLZcertifyInventoryTraceSet.get(row).getSumCount();

			// 获取发放机构名称
			if ("SYS".equals(mLZcertifyInventoryTraceSet.get(row).getSendOutCom())) 
			{
				tSendOutCom = "";
			} else {
				tSendOutCom = tEx.getOneValue("select name from ldcom where comcode = '"+ mLZcertifyInventoryTraceSet.get(row).getSendOutCom() + "'");
			}

			// 获取接收机构名称
			if ("SYS".equals(mLZcertifyInventoryTraceSet.get(row).getReceiveCom())) 
			{
				tReceiveCom = "";
			} else 
			{
				tReceiveCom = tEx.getOneValue("select name from ldcom where comcode = '"+ mLZcertifyInventoryTraceSet.get(row).getReceiveCom() + "'");
			}
			
			
//		   if(row < mBatchNo.size() + 1){
//			   tToExcel[excelRow][0] = (String) mBatchNo.get(row-1);			
//		   }else {
//				tToExcel[excelRow][0] = (String) mBatchNo.get(0);
//			}
		   
		   String tSQL = "select distinct batchno from LZPrintList where ( certifycode = '"+mLZcertifyInventoryTraceSet.get(row).getCertifyCode()+"' and startno <= '"+mLZcertifyInventoryTraceSet.get(row).getStartNo()+"' and endno >= '"+mLZcertifyInventoryTraceSet.get(row).getEndNo()+"' )"
		   			+ " or ( certifycode = '"+mLZcertifyInventoryTraceSet.get(row).getCertifyCode()+"' and startno is null and endno is null and serialno = '"+ mLZcertifyInventoryTraceSet.get(row).getPrtNo() +"') "
		   			
		   
		   			+ " with ur";
//			tToExcel[excelRow][0] = tEx.getOneValue("select distinct lzp.batchno from LZcertifyInventoryTrace lci, LZPrintList lzp ,LZCertifyBatchMain lbm ,LMCardDescription  lmd "
//			+ "where lci.certifycode = lzp.certifycode  and lbm.batchno = lzp.batchno " 
//		    + "and lci.startno = lzp.startno and lci.endno = lzp.endno "
//            + "and lci.OperatingType='00' and lmd.certifycode = lci.certifycode " 
//            + " and lci.SerialNo = '" + mLZcertifyInventoryTraceSet.get(row).getSerialNo() + "'"
//			);
		   tToExcel[excelRow][0] = tEx.getOneValue(tSQL);
			tToExcel[excelRow][1] = year;
			tToExcel[excelRow][2] = month;
			tToExcel[excelRow][3] = day;
			
			tToExcel[excelRow][4] = mLZcertifyInventoryTraceSet.get(row).getCertifyCode();
			tToExcel[excelRow][5] = tCertifyName;
			tToExcel[excelRow][6] = tPrice + "";
			tToExcel[excelRow][7] = tEx.getOneValue("select codename from ldcode1 where codetype ='inventorytrace' and code1 ='"+ mLZcertifyInventoryTraceSet.get(row).getOperatingType() + "'");
			tToExcel[excelRow][8] = mLZcertifyInventoryTraceSet.get(row).getSendOutCom();
			tToExcel[excelRow][9] = tSendOutCom;
			tToExcel[excelRow][10] = mLZcertifyInventoryTraceSet.get(row).getReceiveCom();
			tToExcel[excelRow][11] = tReceiveCom;
			tToExcel[excelRow][12] = tTotalPrice + "";
			tToExcel[excelRow][13] = mLZcertifyInventoryTraceSet.get(row).getSumCount()+ "";
			tToExcel[excelRow][14] = mLZcertifyInventoryTraceSet.get(row).getStartNo();
			tToExcel[excelRow][15] = mLZcertifyInventoryTraceSet.get(row).getEndNo();
			tToExcel[excelRow][16] = tEx.getOneValue("select printname from LZPrintList a, LMPrintCom  b where a.printcode = b.printcode and certifycode = '"+ mLZcertifyInventoryTraceSet.get(row).getCertifyCode() +"' group by printname ");
			tToExcel[excelRow][17] = tEx.getOneValue("select  sum(sumcount) from LZCertifyInventory ci  where certifycode = '"+ mLZcertifyInventoryTraceSet.get(row).getCertifyCode() +"' group by certifycode ");
		}

		excelRow += 2;

		tToExcel[excelRow][0] = "制表人:" + mGI.Operator;
		tToExcel[excelRow][4] = "制表日期:" + PubFun.getCurrentDate();

		showArr(tToExcel);

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * 测试使用
	 * 
	 * @param arr
	 */
	private void showArr(String[][] arr) {
		System.out.println("---开始---");
		for (int row = 0; row < arr.length; row++) {
			String[] t = arr[row];
			for (int col = 0; col < t.length; col++) {
				System.out.print((t[col] == null ? "" : t[col]) + "|");
			}
			System.out.println();
		}
		System.out.println("---结束---");
	}

	/**
	 * 添加错误信息
	 * 
	 * @param error
	 * @param function
	 */
	private void setErrors(String error, String function) {
		CError tError = new CError();
		tError.moduleName = "InventoryIncomeBL";
		tError.functionName = function;
		tError.errorMessage = error;
		this.mErrors.addOneError(tError);
	}

	/**
	 * 获取错误信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {

		GlobalInput tG = new GlobalInput();

		tG.Operator = "cwad";
		tG.ManageCom = "86";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "");
		tTransferData.setNameAndValue("BatchNo", "ZD201307170018");
		tTransferData.setNameAndValue("CertifyCode", "BB22/11B");

		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);

		CertifyAccountsBL tCertifyAccountsBL = new CertifyAccountsBL();

		tCertifyAccountsBL.submitData(vData, "");

	}
}
