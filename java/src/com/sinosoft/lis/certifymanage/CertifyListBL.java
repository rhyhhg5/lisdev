package com.sinosoft.lis.certifymanage;

import com.sinosoft.lis.db.LZCertifyTemplateDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class CertifyListBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData;

	private VData mResult = new VData();

	private String mOperate;

	private MMap map = new MMap();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private GlobalInput mGlobalInput = new GlobalInput();

	private LZcertifyBatchStateSet mLZcertifyBatchStateSet = new LZcertifyBatchStateSet();

	private LZCertifyTemplateSet mLZCertifyTemplateSet = new LZCertifyTemplateSet();

	ExeSQL tExeSQL = new ExeSQL();

	public CertifyListBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {

			this.mOperate = cOperate;
			if (!mOperate.equals("INSERT") && !mOperate.equals("UPDATE")
					&& !mOperate.equals("DELETE") &&  !mOperate.equals("ALLINSERT")
					&& !mOperate.equals("AllDELETE") ) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}

			if (!dealData()) {
				return false;
			}
			if (!prepareOutputData())
				return false;

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "OLDDiseaseBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}

		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PrintingShopBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {

		LZCertifyTemplateSet tLZCertifyTemplateSet = new LZCertifyTemplateSet();

		if (mOperate.equals("INSERT") || mOperate.equals("ALLINSERT")) {
			System.out.println(mLZCertifyTemplateSet.size());

			for (int i = 1; i <= mLZCertifyTemplateSet.size(); i++) {
				System.out.println(i);
				LZCertifyTemplateSchema tLZCertifyTemplateSchema = new LZCertifyTemplateSchema();
				tLZCertifyTemplateSchema = mLZCertifyTemplateSet.get(i);
				System.out.println("传输前数据"
						+ mLZCertifyTemplateSet.get(i).getCertifyCode());
				System.out.println("传输后数据"
						+ tLZCertifyTemplateSchema.getCertifyCode());
				String tSql = "select 1 from LZCertifyTemplate where batchno='"
						+ tLZCertifyTemplateSchema.getBatchNo()
						+ "' and certifycode = '"
						+ tLZCertifyTemplateSchema.getCertifyCode()
						+ "' with ur";
				String strNum = tExeSQL.getOneValue(tSql);

				System.out.println("strNum");
				if ("1".equals(strNum)) {
					continue;
				} else {
					String SerialNo = PubFun1.CreateMaxNo("SeqNum", "");
					tLZCertifyTemplateSchema.setSerialNo(SerialNo);
					tLZCertifyTemplateSchema.setMakeDate(mCurrentDate);
					tLZCertifyTemplateSchema.setMakeTime(mCurrentTime);
					tLZCertifyTemplateSchema.setModifyDate(mCurrentDate);
					tLZCertifyTemplateSchema.setModifyTime(mCurrentTime);
					tLZCertifyTemplateSchema.setOperator(mGlobalInput.Operator);

					tLZCertifyTemplateSet.add(tLZCertifyTemplateSchema);

				}

			}
			map.put(tLZCertifyTemplateSet, "INSERT");

		} else if (mOperate.equals("UPDATE")) {

			for (int i = 1; i <= mLZcertifyBatchStateSet.size(); i++) {

				String sql = "Update LZcertifyBatchState set BatchState='01', "
						+ "ModifyDate = '" + mCurrentDate + "', "
						+ "ModifyTime = '" + mCurrentTime + "' "
						+ "Where  batchno = '"
						+ mLZcertifyBatchStateSet.get(i).getBatchNo() + "' ";
				map.put(sql, "UPDATE"); // 修改

			}

		} else if (mOperate.equals("DELETE")) {

			map.put(this.mLZCertifyTemplateSet, "DELETE");

		}else if (mOperate.equals("AllDELETE")){
			String BatchNo=mLZCertifyTemplateSet.get(1).getBatchNo();
			LZCertifyTemplateDB tLZCertifyTemplateDB = new  LZCertifyTemplateDB();
			tLZCertifyTemplateDB.setBatchNo(BatchNo);
			LZCertifyTemplateSet adLZCertifyTemplateSet=tLZCertifyTemplateDB.query();
			map.put(adLZCertifyTemplateSet, "DELETE");
		}

		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mLZcertifyBatchStateSet = ((LZcertifyBatchStateSet) cInputData
				.getObjectByObjectName("LZcertifyBatchStateSet", 0));
		mLZCertifyTemplateSet = ((LZCertifyTemplateSet) cInputData
				.getObjectByObjectName("LZCertifyTemplateSet", 0));
		System.out.println("getinput" + mLZCertifyTemplateSet.size());
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinBankAddBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.map);
			mResult = mInputData;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareData", ex.getMessage());
			return false;
		}
		return true;
	}
}
