package com.sinosoft.lis.certifymanage;

import com.sinosoft.utility.*;

public class CertifyDescribeUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public CertifyDescribeUI() {
	}

	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		System.out.println("---PrintingShopUI BEGIN---");
		CertifyDescribeBL tCertifyDescribeBL = new CertifyDescribeBL();
		if (tCertifyDescribeBL.submitData(cInputData, mOperate) == false) {
			// @@错误处理
			this.mErrors.copyAllErrors(tCertifyDescribeBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "CertifyDescribeUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";
			this.mErrors.addOneError(tError);
			mResult.clear();
			return false;
		}
		System.out.println("---PrintingShopUI END---");
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

}
