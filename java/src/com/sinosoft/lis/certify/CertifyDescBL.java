/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LMCardRiskDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.schema.LMCardRiskSchema;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CertifyDescBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();//返回结果
    private LMCertifyDesSet mLMCertifyDesSet = new LMCertifyDesSet();//单证描述表
    private LMCardRiskSet mLMCardRiskSet = new LMCardRiskSet();//描述定额单编码和险种之间的对应关系
    private String mOperate; //操作类型
    private String mOperateType = "";//操作类型
    private String mCertifyClass = ""; //记录单证类型P or D
    private String mCertifyCode = "";//单证编码
    public CertifyDescBL() {}


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Start CertifyDescBL submit date ....");
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData)) {
            return false;
        }
        this.mOperate = mOperateType;
        // 数据操作业务处理
        if (cOperate.equals("INSERT")) {
            if (!checkData()) {
                return false;
            }
            if (!dealData()) {
                return false;
            }
        }
        if (cOperate.equals("QUERY")) {
            if (!queryData()) {
                return false;
            }
        }
        if (cOperate.equals("RETURNDATA")) {
            if (!returnData()) {
                return false;
            }
        }
        if (cOperate.equals("DELETE")) {
            if (!dealData()) {
                return false;
            }
        }
        if (cOperate.equals("UPDATE")) {
            if (!checkData()) {
                return false;
            }
            if (!updateQuery()) {
                return false;
            }
            if (!dealData()) {
                return false;
            }
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("Start CertifyDescBL dealData ....");
        mInputData.clear();
        mInputData.add(mOperateType);
        mInputData.add(mCertifyClass);
        mInputData.add(mLMCertifyDesSet);
        if (mCertifyClass.equals("D")) {
            mInputData.add(mLMCardRiskSet);
        }
        //数据提交
        CertifyDescBLS tCertifyDescBLS = new CertifyDescBLS();
        if (!tCertifyDescBLS.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyDescBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /****************************************************************************
     * 接收从CertifyDescSave.jsp中传递的数据
     */

    private boolean getInputData(VData cInputData) {
        System.out.println("Start CertifyDescBL getInputData ....");

        mOperateType = (String) cInputData.get(0); //接收操作类型
        mCertifyClass = (String) cInputData.get(1); //接收单证类型
        System.out.println("所要执行的操作符号是 " + mOperateType + "  单证类型是 " + mCertifyClass);
        mLMCertifyDesSet = ((LMCertifyDesSet) cInputData.getObjectByObjectName(
                "LMCertifyDesSet", 0));
        if (mCertifyClass.equals("D")) {
            System.out.println("定额单证，接收定额单证的信息!!");
            mLMCardRiskSet = ((LMCardRiskSet) cInputData.getObjectByObjectName(
                    "LMCardRiskSet", 0));
        }
        return true;
    }


    /**
     * 查询符合条件的暂交费信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryData() {
        LMCertifyDesSet tLMCertifyDesSet = new LMCertifyDesSet();

        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setSchema(mLMCertifyDesSet.get(1));

        SQLString sqlObj = new SQLString("LMCertifyDes");

        sqlObj.setSQL(5, tLMCertifyDesDB.getSchema());

        String strSQL = sqlObj.getSQL();
        String strSqlAppend = " 1=1 ";

        if (tLMCertifyDesDB.getCertifyName() != null
            && !tLMCertifyDesDB.getCertifyName().trim().equals("")) {

            strSqlAppend = " CertifyName LIKE '%"
                           + tLMCertifyDesDB.getCertifyName()
                           + "%'";
        }

        strSqlAppend += " AND CertifyClass <> 'S' ORDER BY CertifyCode ";

        if (strSQL.toUpperCase().indexOf("WHERE") != -1) {
            strSQL += " AND " + strSqlAppend;
        } else {
            strSQL += " WHERE " + strSqlAppend;
        }

        System.out.println(strSQL);

        tLMCertifyDesSet.set(tLMCertifyDesDB.executeQuery(strSQL));
        System.out.println("在bl中的查询的个数是" + tLMCertifyDesSet.size());

        if (tLMCertifyDesDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLReportBL";
            tError.functionName = "queryData";
            tError.errorMessage = "单证查询失败！！！";
            this.mErrors.addOneError(tError);
            tLMCertifyDesSet.clear();
            return false;
        }
        if (tLMCertifyDesSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有该单证记录！！！";
            this.mErrors.addOneError(tError);
            tLMCertifyDesSet.clear();
            return false;
        }

        mResult.clear();
        if (mCertifyClass.equals("D")) {
            LMCardRiskDB tLMCardRiskDB = new LMCardRiskDB();
            tLMCardRiskDB.setCertifyCode(tLMCertifyDesDB.getCertifyCode());
            LMCardRiskSet tLMCardRiskSet = new LMCardRiskSet();
            tLMCardRiskSet.set(tLMCardRiskDB.query());
            mResult.add(tLMCardRiskSet);
        }
        mResult.add(tLMCertifyDesSet);
        return true;
    }


    /**
     * 校验传入的暂交费收据号是否合法
     * 输出：如果发生错误则返回false,否则返回true
     */
    //校验新增和修改时的数据，单证的号码不能长于18
    private boolean checkData() {
        System.out.println("Start CertifyDescBL checkData ....");
        if (mOperateType.equals("INSERT") || (mOperateType.equals("UPDATE"))) {
            LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
            tLMCertifyDesSchema.setSchema(mLMCertifyDesSet.get(1));
            mCertifyCode = tLMCertifyDesSchema.getCertifyCode();
            System.out.println("校验规则 : " + tLMCertifyDesSchema.getCheckRule());
            System.out.println("业务类型 : " + tLMCertifyDesSchema.getOperateType());
            System.out.println("代码长度是 : " + tLMCertifyDesSchema.getCertifyLength());
            if (tLMCertifyDesSchema.getCertifyLength() > 18) {
                this.mErrors.copyAllErrors(tLMCertifyDesSchema.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLReportBL";
                tError.functionName = "queryData";
                tError.errorMessage = "单证号码的长度不能大于18！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLMCertifyDesSchema.getCertifyLength() < 0) {
                this.mErrors.copyAllErrors(tLMCertifyDesSchema.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLReportBL";
                tError.functionName = "queryData";
                tError.errorMessage = "单证号码的长度不能小于0！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        if (mOperateType.equals("INSERT")) {
            System.out.println("******INSERT*****");
            LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
            tLMCertifyDesSchema.setSchema(mLMCertifyDesSet.get(1));
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            tLMCertifyDesDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
                    
            String CertifyCode=tLMCertifyDesDB.getCertifyCode();
            //  将半角/替换为全角的／
            String tCertifyCode=CertifyCode.replace('/', '／');
            //  对历史数据进行判断
            String Strsql ="select 1 from LMCertifyDes where certifycode in ('"+ CertifyCode+"', '"+ tCertifyCode+"' ) with ur";          
            System.out.println(Strsql );
            
            ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			tSSRS = tExeSQL.execSQL(Strsql);

			if (tSSRS.MaxRow != 0) {
                this.mErrors.copyAllErrors(tLMCertifyDesSchema.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLReportBL";
                tError.functionName = "queryData";
                tError.errorMessage = "该单证编码已经存在，不能进行新增操作！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        // 对一些特殊的字段进行赋值
        LMCertifyDesSchema tLMCertifyDesSchema = mLMCertifyDesSet.get(1);

        //tLMCertifyDesSchema.setSubCode("0");
        tLMCertifyDesSchema.setRiskCode("0");
        tLMCertifyDesSchema.setRiskVersion("0");

        //测试完成需修改返回为 true
        System.out.println("End check date ... ");
        return true;
    }


    //将查询到的数据返回到初始的界面上
    private boolean returnData() {
        LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
        tLMCertifyDesSchema.setSchema(mLMCertifyDesSet.get(1));
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
        LMCertifyDesSet yLMCertifyDesSet = new LMCertifyDesSet();

        yLMCertifyDesSet.set(tLMCertifyDesDB.query());
        mResult.clear();
        mResult.add(yLMCertifyDesSet);
        System.out.println("5-26判断单证的类型是" + mCertifyClass);

        if (mCertifyClass.equals("D")) {
            LMCardRiskSet tLMCardRiskSet = new LMCardRiskSet();
            LMCardRiskDB tLMCardRiskDB = new LMCardRiskDB();
            tLMCardRiskDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
            tLMCardRiskSet.set(tLMCardRiskDB.query());
            mResult.add(tLMCardRiskSet);
            System.out.println("返回时向jsp传递了定额单证的信息！！！！");
        }
        return true;
    }

    //校验单证号码，修改时单证的号码不能修改。
    private boolean updateQuery() {
        return true;
    }

    public static void main(String[] args) {
        String tOperateType = "QUERY";
        String tCertifyCode = "04";
        String tCertifyCode_1 = "04";
        String tCertifyClass = "D";
        LMCertifyDesSchema yLMCertifyDesSchema = new LMCertifyDesSchema();
        LMCertifyDesSet yLMCertifyDesSet = new LMCertifyDesSet();
        LMCardRiskSet yLMCardRiskSet = new LMCardRiskSet();

        LMCardRiskSchema yLMCardRiskSchema = new LMCardRiskSchema();
        yLMCardRiskSchema.setCertifyCode("1");
        yLMCardRiskSchema.setRiskCode("1");
        yLMCardRiskSchema.setPrem("1");
        yLMCardRiskSchema.setPremProp("1");
        yLMCardRiskSet.add(yLMCardRiskSchema);

        yLMCertifyDesSchema.setCertifyCode(tCertifyCode);
        yLMCertifyDesSchema.setRiskVersion(tCertifyCode_1);
        yLMCertifyDesSet.add(yLMCertifyDesSchema);

        VData tVData = new VData();

        tVData.addElement(tOperateType);
        tVData.add("Y");
        tVData.add(yLMCertifyDesSet);

        CertifyDescUI tCertifyDescUI = new CertifyDescUI();
        tCertifyDescUI.submitData(tVData, "QUERY");
    }
}
