/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理单证发放操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */

public class CertSendOutBL extends CertifyBO {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors(); //错误处理类

    /** 前台传入的操作类型 */
    private String mszOperate = ""; //操作类型
    private String mszTakeBackNo = ""; //回收结算单号

    /* 业务相关的数据 */
    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    /** 单证表对象集 */
    private LZCardSet mLZCardSet = new LZCardSet();
    /** 单证轨迹表对象集 */
    private LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet();
    private boolean m_bLimitFlag = false;
    private Hashtable mParams = null;
    /** 传入数据容器，*/
    private VData mResult = new VData();

    /** 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。*/
    private int m_nOperIndex = 0;

    public CertSendOutBL() {
    }

    public static void main(String[] args) {
    }


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            buildFailSet();
            return false;
        }

        return true;
    }

    /**
     * 返回处理结果
     * @return VData的对象
     * @author zhangbin
     * @version 1.0
     */
    public VData getResult() {
        return mResult;
    }


    /**
     * 处理一条单证发放的信息。有可能是单证入库操作，也有可能是单证发放操作。
     * @param aLZCardSchema,aLZCardPrintSchema,vResult
     * @return 布尔值（如果在处理过程中出错，则返回false,否则返回true）
     */
    private boolean dealOne(LZCardSchema aLZCardSchema,
                            LZCardPrintSchema aLZCardPrintSchema,
                            VData vResult) {
        System.out.println("in dealOne()...");
        // Verify SendOutCom and ReceiveCom
        if (!CertifyFunc.verifyComs(globalInput, aLZCardSchema.getSendOutCom(),
                                    aLZCardSchema.getReceiveCom())) {

            mErrors.copyAllErrors(CertifyFunc.mErrors);
            return false;
        }

        if (aLZCardSchema.getSendOutCom().equals(CertifyFunc.INPUT_COM)) { // 如果是入库操作
            if (!CertifyFunc.inputCertify(globalInput, aLZCardSchema,
                                          aLZCardPrintSchema,
                                          CertifyFunc.CERTIFY_CLASS_CERTIFY,
                                          vResult)) {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
                return false;
            }
        } else { // 如果不是入库操作，则进行单证拆分操作
            if (!CertifyFunc.splitCertifySendOut(globalInput, aLZCardSchema,
                                                 m_bLimitFlag, vResult)) {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
                return false;
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mszOperate.equals("INSERT")) {
            // 产生回收清算单号
            mszTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO",
                                                PubFun.getNoLimit(globalInput.
                    ComCode));

            mResult.clear();
            mResult.add(mszTakeBackNo);
            m_nOperIndex = 0;

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) {
                LZCardPrintSchema tLZCardPrintSchema = null;
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                String tReceiveCom = tLZCardSchema.getReceiveCom();
                String tCertifyCode = tLZCardSchema.getCertifyCode();

                LZCardSet newLZCardSet = new LZCardSet();

                // 对单证印刷列表的特殊处理
                if (nIndex < mLZCardPrintSet.size()) {
                    tLZCardPrintSchema = mLZCardPrintSet.get(nIndex + 1);
                }

                // 如果不是入库操作，对传入的单证信息进行格式化。
                if (!tLZCardSchema.getSendOutCom().equals(CertifyFunc.INPUT_COM)) {

                    // Verify SendOutCom and ReceiveCom
                    if (!CertifyFunc.verifyComs(globalInput,
                                                tLZCardSchema.getSendOutCom(),
                                                tLZCardSchema.getReceiveCom())) {

                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    newLZCardSet = CertifyFunc.formatCardList(tLZCardSchema);

                    if (null == newLZCardSet) {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }
                } else {
                    newLZCardSet.add(tLZCardSchema);
                }
                System.out.println("m_bLimitFlag" + m_bLimitFlag +
                                   "  newLZCardSet.size():" + newLZCardSet.size());

                int nMaxCount = CertifyFunc.getMaxCount(tReceiveCom.substring(1),
                        tCertifyCode);
                String tAuthorizeNo = tLZCardSchema.getAuthorizeNo();
                if (tReceiveCom.charAt(0) == 'E') {
                    if (mLZCardSet.size() > nMaxCount &&
                        (tAuthorizeNo == null || tAuthorizeNo.equals(""))) {
                        buildError("verifyMaxCount",
                                   "单次的发放量不得超过" + nMaxCount + "张");
                        return false;
                    }
                }
                if (nIndex == 0 && m_bLimitFlag == true &&
                    !CertifyFunc.verifyMaxCount(tLZCardSchema, mLZCardSet.size())) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }
                if (m_bLimitFlag == true &&
                    !CertifyFunc.verifyMaxCount(tLZCardSchema, newLZCardSet)) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                VData vResult = new VData();

                // 处理数据
                for (int nCount = 0; nCount < newLZCardSet.size(); nCount++) {
                    LZCardSchema newLZCardSchema = newLZCardSet.get(nCount + 1);

                    newLZCardSchema.setTakeBackNo(mszTakeBackNo);
                    if (nIndex == 0) {
                        newLZCardSchema.setPayFlag("1");
                        newLZCardSchema.setSumCount(mLZCardSet.size());
                    }
                    VData vOneResult = new VData();

                    if (!dealOne(newLZCardSchema, tLZCardPrintSchema,
                                 vOneResult)) {
                        return false;
                    }
                    vResult.add(vOneResult);
                }

                // 保存数据
                CertSendOutBLS tCertSendOutBLS = new CertSendOutBLS();
                if (!tCertSendOutBLS.submitData(vResult, "INSERT")) {
                    if (tCertSendOutBLS.mErrors.needDealError()) {
                        mErrors.copyAllErrors(tCertSendOutBLS.mErrors);
                        return false;
                    } else {
                        buildError("dealOne", "CertSendOutBL出错，但是没有提供详细的信息");
                        return false;
                    }
                }

                m_nOperIndex++; // 记录下当前操作到哪一条记录
            }

        } else if (mszOperate.equals("BATCH")) {
            // 清空日志表
            clearLog();

            // 产生回收清算单号
            mszTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO",
                                                PubFun.getNoLimit(globalInput.
                    ComCode));

            mResult.clear();
            mResult.add(mszTakeBackNo);

            m_nOperIndex = 0;
            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) {
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);
                tLZCardSchema.setTakeBackNo(mszTakeBackNo);
                if (!batchSendOut(tLZCardSchema)) {
//          return false;
                    continue;
                }

                m_nOperIndex++; // 记录下当前操作到哪一条记录
            }
        } else {
            buildError("dealData", "不支持的操作字符串");
            return false;
        }

        return true;
    }


    /**
     * 对于每一个发放者、接收者、单证类型。执行下面的函数。
     * 此时的接收者，应该是一个展业机构的代码。
     * @param aLZCardSchema
     * @return
     */
    private boolean batchSendOut(LZCardSchema aLZCardSchema) {
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        boolean bReturn = true;

//    是否需要判断展业机构的存在性？Kevin 2003-05-28
//    LABranchGroupSet tLABranchGroupSet =
//        new LABranchGroupDB().executeQuery("SELECT * FROM LABranchGroup WHERE BranchAttr ＝ '" + szReceiveCom + "'%");

        LABranchGroupSet tLABranchGroupSet =
                new LABranchGroupDB().executeQuery(
                        "SELECT * FROM LABranchGroup WHERE BranchAttr LIKE '" +
                        szReceiveCom + "%'");

        if (tLABranchGroupSet.size() == 0) {
            logError(aLZCardSchema, "展业机构" + szReceiveCom + "不存在");
            bReturn = false;
        } else {
            for (int nIndex = 0; nIndex < tLABranchGroupSet.size(); nIndex++) {
                bReturn = batchGroupSendOut(globalInput,
                                            aLZCardSchema,
                                            tLABranchGroupSet.get(nIndex + 1).
                                            getAgentGroup());
                if (!bReturn) {
                    break;
                }
            }
        }
        return bReturn;
    }


    /**
     * Kevin 2003-05-28
     * 往一个代理人组中所有代理人发放指定类型的单证，使所有代理人手中的该种单证数量达到所能
     * 执有的最大数量。
     * @param aGlobalInput
     * @param aLZCardSchema
     * @param strAgentBranchCode
     * @param strErr
     * @return
     */
    private boolean batchGroupSendOut(GlobalInput aGlobalInput,
                                      LZCardSchema aLZCardSchema,
                                      String strAgentBranchCode) {
        LAAgentSet tLAAgentSet =
                new LAAgentDB().executeQuery(
                        "SELECT * FROM LAAgent WHERE BranchCode = '" +
                        strAgentBranchCode + "'");

        for (int nIndex = 0; nIndex < tLAAgentSet.size(); nIndex++) {
            LAAgentSchema tLAAgentSchema = tLAAgentSet.get(nIndex + 1);

            // 构造一个LZCardSchema
            LZCardSchema tLZCardSchema = new LZCardSchema();

            tLZCardSchema.setCertifyCode(aLZCardSchema.getCertifyCode());
            tLZCardSchema.setSendOutCom(aLZCardSchema.getSendOutCom());
            tLZCardSchema.setReceiveCom("D" + tLAAgentSchema.getAgentCode());
            tLZCardSchema.setTakeBackNo(aLZCardSchema.getTakeBackNo());

            int nMaxCount = CertifyFunc.getMaxCount(tLZCardSchema.getReceiveCom().
                    substring(1),
                    tLZCardSchema.getCertifyCode());

            if (nMaxCount < 0) {
                logError(tLZCardSchema, "找不到这个代理人的最大领用数配置信息");
                continue; // 缺少某个代理人的最大领用数配置，直接到下一个代理人
            }

            // 得到代理人现有单证数量
            String strSQL =
                    "SELECT SUM(SUMCOUNT) FROM LZCard WHERE StateFlag = '0'"
                    + " AND CertifyCode = '" + tLZCardSchema.getCertifyCode() +
                    "'"
                    + " AND ReceiveCom = '" + tLZCardSchema.getReceiveCom() +
                    "'";

            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(strSQL);

            if (exeSQL.mErrors.needDealError()) {
                logError(tLZCardSchema, exeSQL.mErrors.getFirstError());
                continue;
            }

            int nCurCount = Integer.parseInt(ssrs.GetText(1, 1));

            // 代理人手中的单证已经达到最大数量
            if (nCurCount >= nMaxCount) {
                logError(tLZCardSchema, "代理人执有单证数" + String.valueOf(nCurCount) +
                         "已经达到最大数" + String.valueOf(nMaxCount));
                continue;
            }

            // 本次需要发放的单证数
            tLZCardSchema.setSumCount(nMaxCount - nCurCount);

            CertifyFunc.mErrors.clearErrors();
            LZCardSet tLZCardSet = CertifyFunc.findNo(tLZCardSchema);

            if (tLZCardSet == null) {
                logError(tLZCardSchema, CertifyFunc.mErrors.getFirstError());
                mErrors.copyAllErrors(CertifyFunc.mErrors);
                return false;
            }

            VData vResult = new VData();

            for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                   nInnerIndex++) {
                VData vOneResult = new VData();

                mErrors.clearErrors();
                CertifyFunc.mErrors.clearErrors();
                if (!dealOne(tLZCardSet.get(nInnerIndex + 1), null, vOneResult)) {
                    logError(tLZCardSet.get(nInnerIndex + 1),
                             mErrors.getFirstError());
                    continue;
                }

                vResult.add(vOneResult);
            }

            // 保存数据
            CertSendOutBLS tCertSendOutBLS = new CertSendOutBLS();
            if (!tCertSendOutBLS.submitData(vResult, "INSERT")) {
                if (tCertSendOutBLS.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tCertSendOutBLS.mErrors);
                    return false;
                } else {
                    buildError("dealOne", "CertSendOutBL出错，但是没有提供详细的信息");
                    return false;
                }
            }
        } // end of for

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param vData VData
     * @return boolean 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData vData) {
        if (mszOperate.equals("INSERT")) {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mLZCardPrintSet.set((LZCardPrintSet) vData.getObjectByObjectName(
                    "LZCardPrintSet", 0));
            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);

            String str = (String) vData.getObjectByObjectName("String", 0);
            if (str == null || !str.equals("NO")) {
                m_bLimitFlag = true; // 缺省的行为是要校验
            } else {
                m_bLimitFlag = false;
            }

        } else if (mszOperate.equals("BATCH")) {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));

        } else {
            buildError("getInputData", "不支持的操作字符串");
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @param vData VData
     * @return boolean
     */
    private boolean prepareOutputData(VData vData) {
        try {
            if (mszOperate.equals("INSERT")) {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
                vData.addElement(mLZCardPrintSet);
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertSendOutBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertSendOutBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate) {
        String szReturn = "";
        // 单证发放、批量发放
        String szOperates[] = {"INSERT", "BATCH"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++) {
            if (szOperate.equals(szOperates[nIndex])) {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }

    private void buildFailSet() {
        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = m_nOperIndex; nIndex < mLZCardSet.size(); nIndex++) {
            tLZCardSet.add(mLZCardSet.get(nIndex + 1));
        }

        mResult.add(tLZCardSet);
    }
}
