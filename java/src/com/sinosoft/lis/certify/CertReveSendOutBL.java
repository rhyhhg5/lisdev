/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理普通单证回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class CertReveSendOutBL
{
    /**
     * 错误处理类，每个需要错误处理的类中都放置该类
     */
    public CErrors mErrors = new CErrors();


    /** 私有成员 */
    private String mszOperate = "";


    /*业务相关的数据*/
    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    /** 单证表对象集 */
    private LZCardSet mLZCardSet = new LZCardSet();
    /** 数据容器*/
    private VData mResult = new VData();
    private Hashtable mParams = null;

    private int m_nOperIndex = 0;

    /** 构造方法 */
    public CertReveSendOutBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))return false;

        if (!dealData())
        {
            buildFailSet();
            return false;
        }

        return true;
    }

    /**
     * 返回处理结果
     * @return VData的对象
     * @author zhangbin
     * @version 1.0
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（如果在处理过程中出错，则返回false,否则返回true）
     */
    private boolean dealData()
    {
        if (mszOperate.equals("INSERT"))
        {

            // 产生回收清算单号
            String strNoLimit = PubFun.getNoLimit(globalInput.ComCode);
            String strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);

            mResult.clear();
            mResult.add(strTakeBackNo);

            m_nOperIndex = 0;
            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                // Kevin 2003-08-05
                // 单证发放回退时，可以去掉发放者和接收者的校验。因为，如果数据库中存在发放的记录，
                // 这条记录就一定能被发放回退，而不必考虑发放者和接收者的当前状态。（如代理人的离职）

//        // Verify SendOutCom and ReceiveCom
//        if( !CertifyFunc.verifyComs(globalInput, tLZCardSchema.getReceiveCom( ),
//                      tLZCardSchema.getSendOutCom( )) ) {
//          mErrors.copyAllErrors(CertifyFunc.mErrors);
//          return false;
//        }

                LZCardSet tLZCardSet = CertifyFunc.formatCardList(tLZCardSchema);

                if (tLZCardSet == null)
                {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                VData vResult = new VData();

                for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                       nInnerIndex++)
                {

                    tLZCardSchema = tLZCardSet.get(nInnerIndex + 1);
                    tLZCardSchema.setTakeBackNo(strTakeBackNo);

                    VData vOneResult = new VData();

                    // 单证反发放操作
                    if (!CertifyFunc.splitCertReveSendOut(globalInput,
                            tLZCardSchema, vOneResult))
                    {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    vResult.add(vOneResult);
                }

                CertReveSendOutBLS tCertReveSendOutBLS = new CertReveSendOutBLS();

                if (!tCertReveSendOutBLS.submitData(vResult, "INSERT"))
                {
                    mErrors.copyAllErrors(tCertReveSendOutBLS.mErrors);
                    return false;
                }

                m_nOperIndex++;
            } // end of for(int nIndex = 0; ...
        } // end of if( mszOperate.equals("INSERT") );

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param vData - 页面传入的LZCardSchema对象
     * @return 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData vData)
    {
        if (mszOperate.equals("INSERT"))
        {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);

            if (mParams == null || mParams.get("CertifyClass") == null)
            {
                buildError("getInputData", "缺少必要的参数");
                return false;
            }
        }
        else
        {
            buildError("getInputData", "不支持的操作字符串");
            return false;
        }

        return true;
    }

    /**
    * 准备往后层输出所需要的数据
    * @param vData - 准备传出的数据
    * @return 如果准备数据时发生错误则返回false,否则返回true
    */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            if (mszOperate.equals("INSERT"))
            {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertReveSendOutBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertReveSendOutBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }

    private void buildFailSet()
    {
        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = m_nOperIndex; nIndex < mLZCardSet.size(); nIndex++)
        {
            tLZCardSet.add(mLZCardSet.get(nIndex + 1));
        }
        mResult.add(tLZCardSet);
    }
}
