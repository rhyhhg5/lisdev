/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardPrintSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理普通单证发放操作（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */
public class CertSendOutUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput globalInput = new GlobalInput(); //登陆用户信息
    private LZCardSet mLZCardSet = new LZCardSet(); //单证状态表
    private LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet(); //单证印刷表
    private String m_szLimitFlag = "NO"; // 增领标志字符串。

    private VData mResult = null;

    public CertSendOutUI() {
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();

        globalInput.Operator = "bfgxdz";
        globalInput.ComCode = "86";
        globalInput.ManageCom = "";

        CertSendOutUI tCertSendOutUI = new CertSendOutUI();

        LZCardSet setLZCard = new LZCardSet();
         for( int i = 1; i <= 3; i ++ ) {
             LZCardSchema schemaLZCard = new LZCardSchema();
             String strCurSerNo = String.valueOf(i);
             strCurSerNo = PubFun.LCh(strCurSerNo, "0", 7);
             schemaLZCard.setSendOutCom("Bbfgxdz");
             schemaLZCard.setReceiveCom("D1101000144");
             System.out.println(schemaLZCard.getReceiveCom().substring(1));
             schemaLZCard.setCertifyCode("twuser");
             //schemaLZCard.setStartNo("0000008");
             schemaLZCard.setSubCode("22");
            // schemaLZCard.setEndNo("0000008");
            schemaLZCard.setStartNo(strCurSerNo);
            schemaLZCard.setEndNo(strCurSerNo);
             schemaLZCard.setSumCount(1);

             schemaLZCard.setPrem("");
             schemaLZCard.setAmnt("100");
             schemaLZCard.setHandler("kevin");
             schemaLZCard.setHandleDate("2002-9-01");
             schemaLZCard.setInvaliDate("2002-9-01");

             schemaLZCard.setTakeBackNo("");
             schemaLZCard.setSaleChnl("");
             schemaLZCard.setStateFlag("");
             schemaLZCard.setOperateFlag("");
             schemaLZCard.setPayFlag("");
             schemaLZCard.setEnterAccFlag("");
             schemaLZCard.setReason("");
             schemaLZCard.setState("1");
             schemaLZCard.setOperator("faint");
             schemaLZCard.setMakeDate("");
             schemaLZCard.setMakeTime("");
             schemaLZCard.setModifyDate("");
             schemaLZCard.setModifyTime("");
             schemaLZCard.setSubCode("22");
             //schemaLZCard.setAuthorizeNo("21");

             setLZCard.add(schemaLZCard);
         }
        LZCardPrintSet setLZCardPrint = new LZCardPrintSet();
        LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema();

        schemaLZCardPrint.setCertifyCode("twuser");
        schemaLZCardPrint.setRiskCode("");
        schemaLZCardPrint.setRiskVersion("");
        schemaLZCardPrint.setSubCode("22");

        /* schemaLZCardPrint.setPrtNo("80000000133");
         schemaLZCardPrint.setCertifyCode("AA");
         schemaLZCardPrint.setRiskCode("");
         schemaLZCardPrint.setRiskVersion("");
         schemaLZCardPrint.setSubCode("21");
         schemaLZCardPrint.setMaxMoney("");
                  schemaLZCardPrint.setMaxDate("");
                  schemaLZCardPrint.setComCode("");
                  schemaLZCardPrint.setPhone("");
                  schemaLZCardPrint.setLinkMan("");
                  schemaLZCardPrint.setCertifyPrice("100");
                  schemaLZCardPrint.setManageCom("");
                  schemaLZCardPrint.setOperatorInput("");
                  schemaLZCardPrint.setInputDate("");
                  schemaLZCardPrint.setInputMakeDate("");
                  schemaLZCardPrint.setGetMan("");
                  schemaLZCardPrint.setGetDate("");
                  schemaLZCardPrint.setOperatorGet("");
                  schemaLZCardPrint.setStartNo("1");
                  schemaLZCardPrint.setEndNo("2");
                  schemaLZCardPrint.setGetMakeDate("");
                  schemaLZCardPrint.setSumCount("1");
                  schemaLZCardPrint.setModifyDate("");
                  schemaLZCardPrint.setModifyTime("");
                  schemaLZCardPrint.setState("0");
        schemaLZCardPrint.setState("1");*/

        setLZCardPrint.add(schemaLZCardPrint);

        VData vData = new VData();

        vData.addElement(globalInput);
        vData.addElement(setLZCard);
        vData.addElement(setLZCardPrint);
        vData.addElement("YES");
        vData.addElement("DSend");
        //Hashtable hashParams = new Hashtable();
        //hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
        //vData.addElement(hashParams);
        if (!tCertSendOutUI.submitData(vData, "INSERT")) {
            System.out.println(tCertSendOutUI.mErrors.getFirstError());
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            mOperate = cOperate;

            //得到外部传入的数据,将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            //进行业务处理
            if (!dealData()) {
                return false;
            }

            VData vData = new VData();

            //准备往后台的数据
            if (!prepareOutputData(vData)) {
                return false;
            }

            vData = cInputData; // 不使用经过UI处理过的数据

            CertSendOutBL tCertSendOutBL = new CertSendOutBL();

            boolean bReturn = tCertSendOutBL.submitData(vData, mOperate);

            mResult = tCertSendOutBL.getResult();

            if (!bReturn) {
                if (tCertSendOutBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tCertSendOutBL.mErrors);
                } else {
                    buildError("submitData", "CertSendOutBL发生错误，但是没有提供详细的信息");
                }
            }

            return bReturn;
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submitData", ex.getMessage());
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            if (mOperate.equals("INSERT")) {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
                vData.addElement(mLZCardPrintSet);
                vData.addElement(m_szLimitFlag);
            }

        } catch (Exception ex) {
            // @@错误处理
            buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        if (mOperate.equals("INSERT")) {
            try {
                globalInput.setSchema((GlobalInput) cInputData.
                                      getObjectByObjectName("GlobalInput", 0));
                mLZCardSet.set((LZCardSet) cInputData.getObjectByObjectName(
                        "LZCardSet", 0));
                mLZCardPrintSet.set((LZCardPrintSet) cInputData.
                                    getObjectByObjectName("LZCardPrintSet", 0));

                String str = (String) cInputData.getObjectByObjectName("String",
                        0);
                if (str == null || !str.equals("NO")) {
                    m_szLimitFlag = "YES";
                } else {
                    m_szLimitFlag = "NO";
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }

        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertSendOutUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult() {
        return mResult;
    }
}
