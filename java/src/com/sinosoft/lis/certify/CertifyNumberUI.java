package com.sinosoft.lis.certify;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.operfee.GrpDueFeeUI;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CertifyNumberUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误容器
    private VData mInputData = new VData(); //存储数据
    private VData mResult = new VData(); //返回结果
    private String mOperate; //操作类型

    public CertifyNumberUI() {
    }

//操作的提交方法，作为页面数据的入口，准备完数据后执行入库操作。
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        CertifyNumberBL tCertifyNumberBL = new CertifyNumberBL();
        System.out.println("---CertifyNumberUI  BEGIN---");
        if (tCertifyNumberBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyNumberBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        } else {
            mResult = tCertifyNumberBL.getResult();
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GrpDueFeeUI GrpDueFeeUI1 = new GrpDueFeeUI();
        // GlobalInput tGI = new GlobalInput();
        // tGI.ComCode="86";
        //tGI.Operator="wuser";
        //tGI.ManageCom="86";
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CardType", "61");
        //tTransferData.setNameAndValue("CardSerNo", "1");
        //tTransferData.setNameAndValue("CardNum", "5");
        //tTransferData.setNameAndValue("OperateType", "0");
        tTransferData.setNameAndValue("PrtNo","20061129");
        tTransferData.setNameAndValue("PageType","0");
        tTransferData.setNameAndValue("Operate","confirm");

        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGI);
        CertifyNumberBL tCertifyNumberBL = new CertifyNumberBL();
        tCertifyNumberBL.submitData(tVData, "INSERT");

    }

}
