package com.sinosoft.lis.certify;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ecwebservice.subinterface.BaseService;
import com.ecwebservice.subinterface.ExternalActive;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.WFInsuListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ECCardActiveBL {
	
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap map = new MMap();
    
    /** 往界面传输数据的容器 */
    private VData mResult = new VData(); //返回结果
    private String CurrentDate = PubFun.getCurrentDate(); //当前时间
    private String CurrentTime = PubFun.getCurrentTime(); //当前时间
    private TransferData mTransferData = new TransferData(); //存储接受数据
    private GlobalInput mGI = new GlobalInput(); //存储用户信息
    private Log log = LogFactory.getLog(this.getClass().getName());//日志
    
    private String batchno = null;// 批次号
    
	private String cardNum      = null;
	private String cardPassword = null;
	private String cvalidate = null;
	private String mult = null;
//投保人                            
	private String appName 			= null;
	private String appIdType 		= null;
	private String appSex			  = null;
	private String appIdNo 			= null;
	private String appBirthday  = null;
//被保人                            
	private String[] insuNumber= null;
	private String[] insuName =  null;
	private String[] insuSex =   null;
	private String[] insuIdtype= null;
	private String[] insuIdno=   null;
	private String[] insuBirth=  null;
	private String[] insuAge=  null;
	private String[] insuCopy=  null;
	private String[] insuOccupation=  null;
	
	private String certifycode = null;
	private String certifyname = null;
	private String wrapcode    = null;
	
	private String prem        = null;
	private String insuYear    = null;
	private String insuYearFlag = null;
	private String saleAgent   = null;
	
	// 保险卡信息
	private WFContListSchema contListSchema = new WFContListSchema();
	
	//投保人信息
	private WFAppntListSchema appntListSchema = null;
	
	// 被保人信息
	private WFInsuListSet insuListSet = new WFInsuListSet();
	private WFInsuListSchema insuListSchema = new WFInsuListSchema();
	
	


    public ECCardActiveBL() {}
    
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
	{
    	try
		{
			// 将操作数据拷贝到本类中
			mInputData = (VData) cInputData.clone();
			if (!getInputData(mInputData))
			{
				return false;
			}
			if(!check()){
				return false;
			}
			if(!dealData()){
				return false;
			}

			PubSubmit t = new PubSubmit();
			VData v = new VData();
			v.add(map);
			if (t.submitData(v, "") == false)
			{
				this.mErrors.addOneError("PubSubmit:处理数据库失败!");
				return false;
			}
		}
		catch (Exception ex)
		{
			// @@异常或者错误处理
			ex.printStackTrace();
			//CError.buildErr(this, ex.toString());
			return false;
		}			
    	return true;
	}
    
    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0); //传输页面输入的起止时间
        if (mTransferData == null || mGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCardActiveBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
         cardNum      = (String) mTransferData.getValueByName("cardNum");
		 cardPassword = (String) mTransferData.getValueByName("cardPassword");
		 cvalidate = (String) mTransferData.getValueByName("cvalidate");
		 mult = (String) mTransferData.getValueByName("mult");		 
		
		 appName 	  = (String) mTransferData.getValueByName("appName");
		 appIdType 	  = (String) mTransferData.getValueByName("appIdType");
		 appSex		  = (String) mTransferData.getValueByName("appSex");
		 appIdNo 	  = (String) mTransferData.getValueByName("appIdNo");
		 appBirthday  = (String) mTransferData.getValueByName("appBirthday");
		
		 //toAppRela 	  = (String) mTransferData.getValueByName("toAppRela");
		 insuNumber 	  = (String[]) mTransferData.getValueByName("insuNumber");
		 insuName	  = (String[]) mTransferData.getValueByName("insuName");
		 insuSex 	  = (String[]) mTransferData.getValueByName("insuSex");
		 insuIdtype  = (String[]) mTransferData.getValueByName("insuIdtype");
		 insuIdno  	  = (String[]) mTransferData.getValueByName("insuIdno");
		 insuBirth  	  = (String[]) mTransferData.getValueByName("insuBirth");
		 //insuAge  = (String[]) mTransferData.getValueByName("insuAge");
		 //insuCopy  	  = (String[]) mTransferData.getValueByName("insuCopy");
		 insuOccupation  	  = (String[]) mTransferData.getValueByName("insuOccupation");
		 
		 if(insuName == null || insuSex == null || insuIdtype == null || insuIdno == null || insuBirth == null 
				 || insuOccupation == null){
			 // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "ECCardActiveBL";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "没有得到足够的被保人数据，请您确认!";
	            this.mErrors.addOneError(tError);
	            return false;
		 }
        return true;
    }
    
    private boolean check(){
    	
    	return true;
    }
    
    private boolean dealData(){
    	
    	//检验功能是否开启
    	String tStrSql = "select 1 from ldsysvar where sysvar='CardActive' and sysvarvalue='1' ";
    	String reu = new ExeSQL().getOneValue(tStrSql);
    	if(!"1".equals(reu))
    	{
    		CError tError = new CError();
            tError.moduleName = "ECCardActiveBL";
            tError.functionName = "dealData";
            tError.errorMessage = "卡激活功能没有开启，请您确认!";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	
    	
    	batchno=PubFun1.CreateMaxNo("CA", 10);
    	if(!dealCardInfo()){
    		System.out.println("处理保险卡信息失败");
    		return false;
    	}
    	
    	if(!dealAppntInfo()){
    		System.out.println("处理投保人信息失败");
    		return false;
    	}
    	if(!dealInsuInfo()){
    		System.out.println("处理被保人信息失败");
    		return false;
    	}
    	

		LICardActiveInfoListSet exCardActiveInfoListSet = writeLICardActiveInfoList(contListSchema, insuListSet);
		
		//state In Lzcard 需要修改为14.
		String[] updateArr = getUpdateLzcardSql(this.cardNum);
		
		String stateInLzcard = updateArr[1];
		//判断单证号是否有效
		if("2".equals(stateInLzcard)||"10".equals(stateInLzcard)||"11".equals(stateInLzcard)||"13".equals(stateInLzcard)){
			System.out.println("单证号有效.");
		}else{
			// @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCardActiveBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "单证号无效";
            this.mErrors.addOneError(tError);
			return false;
		}
		if("11".equals(stateInLzcard)||"10".equals(stateInLzcard)){
			System.out.println("state In Lzcard 需要修改为14.");
			String modifyStateSql = updateArr[0];
			map.put(modifyStateSql, "UPDATE");
		}

		map.put(exCardActiveInfoListSet, "INSERT");
    	return true;
    }
    
    private boolean dealCardInfo(){
    	try{
    		String passSQL = "select cardpassword from lzcardnumber where cardno='"+this.cardNum+"'";
        	String pass = new ExeSQL().getOneValue(passSQL);
        	if(pass == null || pass.equals("")){
        		CError tError = new CError();
                tError.moduleName = "ECCardActiveBL";
                tError.functionName = "dealData";
                tError.errorMessage = "卡号没有对应的密码";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	LisIDEA tLisIdea = new LisIDEA();
        	if(!this.cardPassword.equals(tLisIdea.decryptString(pass))){
        		CError tError = new CError();
                tError.moduleName = "ECCardActiveBL";
                tError.functionName = "dealData";
                tError.errorMessage = "卡号与密码不匹配";
                this.mErrors.addOneError(tError);
                return false;
        	}
            System.out.println("卡号："+this.cardNum +" 密码：" + tLisIdea.decryptString(pass));
            
            String riskSQL="select lmcd.certifycode, lmcd.certifyname, lmcr.riskcode "
							  +"from LZCardNumber lzcn "
							 +" inner join lmcertifydes lmcd on lmcd.subcode = lzcn.cardtype"
							 +" inner join LMCardRisk lmcr on lmcr.certifycode = lmcd.certifycode"
							 +" where lzcn.cardno='"+this.cardNum+"'";
            SSRS mSSRS = new ExeSQL().execSQL(riskSQL);
            if(mSSRS == null){
            	CError tError = new CError();
                tError.moduleName = "ECCardActiveBL";
                tError.functionName = "dealData";
                tError.errorMessage = "卡没有对应的产品信息";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.certifycode = mSSRS.GetText(1, 1);
            this.certifyname = mSSRS.GetText(1, 2);
            this.wrapcode    = mSSRS.GetText(1, 3);
            
         // 根据单证编码查询保费
			String premSql = "select prem from lmcardrisk where CertifyCode='"
					+ certifycode + "'";
			String insuYearSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
					+ wrapcode+ "' and calfactor='InsuYear' ";
			String insuYearFlagSql = "select distinct  calfactorvalue from ldriskdutywrap where riskwrapcode = '"
					+ wrapcode+ "' and calfactor='InsuYearFlag' ";
			
			prem = new ExeSQL().getOneValue(premSql);
			System.out.println("查询出的保费" + prem);
			insuYear = new ExeSQL().getOneValue(insuYearSql);
			insuYearFlag = new ExeSQL().getOneValue(insuYearFlagSql);
			
			// 根据单证号 查询单证编码 查询出LZCardNumber 中的ReceiveCom
			// 供查询销售机构使用
			String certifyCodeSql = "select lzc.CertifyCode,lzc.ReceiveCom from LZCardNumber lzcn "
					+ "inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
					+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 and lzcn.CardNo='"
					+ this.cardNum + "' ";
			SSRS sResult = new ExeSQL().execSQL(certifyCodeSql);

			// 管理机构 同时作为电子商务的销售机构
			String manageCom = null;
			if (sResult.MaxRow > 0) {
				// 下发的单证号的 接收机构（完整 其中第一位为标识位）
				String receiveCom = sResult.GetText(1, 2);
				// 获取接收机构的前缀标识
				String receiveComMark = receiveCom
						.substring(0, 1);
				String receiveComCode = receiveCom
						.substring(1);
				System.out.println("获取接收机构的前缀标识"
						+ receiveComMark);
				System.out.println("获取接收机构的后缀代码"
						+ receiveComCode);
				String manageComSql = null;

				// 根据接收机构的前缀 分别进行处理
				// 如果单证号发放至代理机构
				if ("E".equalsIgnoreCase(receiveComMark)) {
					manageComSql = "select lac.name from LACom lac where 1 = 1 and lac.AgentCom in ('"
							+ receiveComCode + "') ";
					manageCom = new ExeSQL().getOneValue(manageComSql);						
				}
				// 如果单证号发放至业务员 代理人
				if ("D".equalsIgnoreCase(receiveComMark)) {
					manageComSql = " select name from ldcom where comcode in (select laa.ManageCom from LAAgent laa where 1 = 1 and laa.AgentCode in ('"
							+ receiveComCode
							+ "') )";
					manageCom = new ExeSQL().getOneValue(manageComSql);
				}
				System.out.println("获取的销售机构" + manageCom);
				saleAgent = manageCom;
			}
    	}catch(Exception e){
    		e.printStackTrace();
    		System.out.println("");
    		return false;
    	}
    	
    	contListSchema.setBatchNo(batchno);//????如何生成
		contListSchema.setSendDate(PubFun.getCurrentDate());
		contListSchema.setSendTime(PubFun.getCurrentTime());
		contListSchema.setMessageType("01");//固定为01
		contListSchema.setCardDealType("01");//待定 保险卡处理类别：01－报文中明确提供卡号密码；02－只说明要承保哪种类型保险卡，由系统获取一张尚未使用的卡进行激活。
		// 不为空
		contListSchema.setBranchCode("860");
		contListSchema.setSendOperator("100");
		contListSchema.setActiveDate(PubFun.getCurrentDate());//??激活日期
		
		//增加生效日期校验
		String realCvaliDate = calCValidate(this.cvalidate, PubFun.getCurrentDate());//??生效日期		
		contListSchema.setCvaliDate(realCvaliDate);
		contListSchema.setRiskCode(wrapcode);

		contListSchema.setAppntNo("1");
		contListSchema.setCardNo(cardNum);
		contListSchema.setPassword(cardPassword);
		contListSchema.setCertifyCode(certifycode);
		contListSchema.setCertifyName(certifyname);
		//contListSchema.setCardArea(cardrea);//承保区域 待定
		// 新增 保费 保险期间 销售机构写入中间表
		contListSchema.setPrem(prem);
		contListSchema.setInsuYear(insuYear);
		contListSchema.setInsuYearFlag(insuYearFlag);
		contListSchema.setBak1(saleAgent); 
		contListSchema.setMult(mult);//无法得到档次??????

		map.put(contListSchema, "INSERT");
    	
    	return true;
    }
    
    private boolean dealAppntInfo(){
    	try{
    		appntListSchema = new WFAppntListSchema();
        	appntListSchema.setBatchNo(batchno);//???????????
    		appntListSchema.setCardNo(this.cardNum);		
    		appntListSchema.setAppntNo("1");
    		appntListSchema.setName(this.appName);
    		appntListSchema.setSex(codeSex(this.appSex));
    		appntListSchema.setBirthday(this.appBirthday);
    		appntListSchema.setIDType(this.appIdType);
    		appntListSchema.setIDNo(this.appIdNo);

    		map.put(appntListSchema, "INSERT");
    	}catch(Exception e){
    		e.printStackTrace();
    		CError tError = new CError();
            tError.moduleName = "ECCardActiveBL";
            tError.functionName = "dealAppntInfo";
            tError.errorMessage = "系统内部异常";
            this.mErrors.addOneError(tError);
    		return false;
    	}  	
    	return true;
    }
    
    private boolean dealInsuInfo(){
    	try{
    		int insuNo = this.insuNumber.length;
    		
        	for (int i=0;i<insuNo;i++){
        		
        		String insureSql = null;               //------------------------------
                String cardNoStr = this.cardNum.toString().substring(0, 1);
                ExeSQL tExeSQL = new ExeSQL();         //------------------------------
                insureSql="select count(1) + 1 from LICardActiveInfoList where cardtype='"//--------fromYZF
                          +this.wrapcode+"' and name='"+insuName[i]+"' and sex='"+insuSex[i]
                          +"' and birthday='"+insuBirth[i]+"' and idtype='"+ insuIdtype[i]
                          +"' and idno='"+insuIdno[i]+"' ";
                System.out.println(insureSql+"-----------------");
                String temp1=tExeSQL.execSQL(insureSql).GetText(1, 1);
                int alreadyNo = 0;
                if(temp1.matches("^\\d+$")){
                	alreadyNo = Integer.parseInt(temp1)+1;
                }  
                
                String maxNoSql=null;
                maxNoSql=" select maxcopys from ldwrap where riskwrapcode='"+this.wrapcode+"' ";
                System.out.println(maxNoSql+"-----------------");
                String temp2=tExeSQL.getOneValue(maxNoSql);
                int maxNo = 0;
               
                	if(temp2.matches("^\\d+$")){
                    	maxNo = Integer.parseInt(temp2);
                    	
                        if(maxNo<alreadyNo){
//                       	 @@错误处理被保人投保分数超过验证
//                           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             				CError tError = new CError();
             				tError.moduleName = "ECCardActiveBL";
             				tError.functionName = "dealInsuInfo";
             				tError.errorMessage = "被保人投保分数已超过可投保的最大分数!";
             				this.mErrors.addOneError(tError);
             				return false;
                         }
                    } 
                
                              
                System.out.println("max:"+maxNo+"alread:"+alreadyNo);
                batchno=PubFun1.CreateMaxNo("CA", 10);    		
        		insuListSchema.setBatchNo(this.batchno);
        		insuListSchema.setCardNo(this.cardNum);
        		insuListSchema.setInsuNo("1");//被保人序号
        		//	insuListSchema.setToAppntRela(toAppntRela);//表中可以为空，是否必录?????
        		insuListSchema.setName(this.insuName[i]);
        		insuListSchema.setSex(codeSex(this.insuSex[i]));
        		insuListSchema.setBirthday(this.insuBirth[i]);
        		if(!this.checkDate(insuBirth[i])){
        			CError tError = new CError();
                    tError.moduleName = "ECCardActiveBL";
                    tError.functionName = "dealInsuInfo";
                    tError.errorMessage = "第"+(i+1)+"个被保人出生日期格式有误，应为YYYY-MM-DD";
                    this.mErrors.addOneError(tError);
                    return false;
        		}
        		int insuAge = PubFun.getInsuredAppAge(this.CurrentDate,insuBirth[i] );
        		String insuAgeSQL = "select mininsuredage,maxinsuredage from ldriskwrap where " +
        				"riskwrapcode ='"+this.wrapcode+"' fetch first  1 row only";
        		SSRS dSSRS = new SSRS();
        		dSSRS = new ExeSQL().execSQL(insuAgeSQL);
        		String minAge = dSSRS.GetText(1, 1);
        		String maxAge = dSSRS.GetText(1, 2);
        		if(insuAge<Integer.parseInt(minAge)){
        			CError tError = new CError();
                    tError.moduleName = "ECCardActiveBL";
                    tError.functionName = "dealInsuInfo";
                    tError.errorMessage = "第"+(i+1)+"个被保人年龄小于产品最小年龄"+minAge;
                    this.mErrors.addOneError(tError);
            		return false;
        		}
        		if(insuAge>Integer.parseInt(maxAge)){
        			CError tError = new CError();
                    tError.moduleName = "ECCardActiveBL";
                    tError.functionName = "dealInsuInfo";
                    tError.errorMessage = "第"+(i+1)+"个被保人年龄大于产品最大年龄"+maxAge;
                    this.mErrors.addOneError(tError);
            		return false;
        		}
        		
        		insuListSchema.setIDType(this.insuIdtype[i]);
        		insuListSchema.setIDNo(this.insuIdno[i]);
        		insuListSchema.setOccupationType(this.insuOccupation[i]);
        		
        		this.insuListSet.add(insuListSchema);
        	}
    		map.put(insuListSet, "INSERT");
    	}catch(Exception e){
    		e.printStackTrace();
    		CError tError = new CError();
            tError.moduleName = "ECCardActiveBL";
            tError.functionName = "dealInsuInfo";
            tError.errorMessage = "系统内部异常";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	
		return true;
    }
    
    /**
     * 根据激活日期计算生效日期。
     * <br />激活日期次日零时，为保单生效日期。
     * @param cActiveDate 激活日期
     * @return
     */
    public String calCValidate(String strCValidate, String strActiveDate)
    {
        String caledCValidateStr;
    	Date tResCValidate = null;

        FDate tFDate = new FDate();
        Date tCValidate = tFDate.getDate(strCValidate);
        Date tActiveDate = tFDate.getDate(strActiveDate);

        if (tActiveDate == null)
        {
            return null;
        }

        if (tCValidate != null && tCValidate.after(tActiveDate))
        {
            tResCValidate = tCValidate;
        }
        else
        {
            int tIntervalDays = 1;
            Calendar tParamDate = Calendar.getInstance();
            try
            {
                tParamDate.setTime(tActiveDate);
                tParamDate.add(Calendar.DATE, tIntervalDays);
                tResCValidate = tParamDate.getTime();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");  
        caledCValidateStr=format1.format(tResCValidate); 


        return caledCValidateStr;
    }
    
    /**
     * 对性别进行编码
     * @param sex
     * @return
     */
	public String codeSex(String sex) {
		if ("男".equals(sex)) {
			return "0";
		} else if ("女".equals(sex)) {
			return "1";
		} else {
			return sex;
		}
	}
	
	public boolean checkDate(String sourceDate){
        if(sourceDate==null){
            return false;
        }
        try {
               SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
               dateFormat.setLenient(false);
               dateFormat.parse(sourceDate);
               return true;
        } catch (Exception e) {
        }
        System.out.println("日期格式有误，应为YYYY-MM-DD");
         return false;
    }

	public LICardActiveInfoListSet writeLICardActiveInfoList(
			WFContListSchema contListSchema, WFInsuListSet tInsuListSet) {

		// 写入卡单外部激活信息表 LICardActiveInfoList
		LICardActiveInfoListSet cardActiveInfoListSet = new LICardActiveInfoListSet();

		if (null != contListSchema && null != tInsuListSet) {
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();

			// 判断激活还是撤单
			// "02"为撤单
			// 更新LICardActiveInfoList表中的CardStatus 02-退保
			if (contListSchema.getMessageType().equals("02")) {
				String tbCardNo = contListSchema.getCardNo();
				String sql = "update LICardActiveInfoList set CardStatus='02',modifydate='"
						+ currentDate
						+ "',modifytime='"
						+ currentTime
						+ "' where CardNo='" + tbCardNo + "'";
				ExeSQL tExeSQL = new ExeSQL();
				boolean updateFlag = tExeSQL.execUpdateSQL(sql);
				// if (updateFlag) {
				// return true;
				// } else {
				// return false;
				// }

			}

			WFInsuListSchema insuListSchema;
			for (int i = 1; i <= tInsuListSet.size(); i++) {
				LICardActiveInfoListSchema cardActiveInfoListSchema = new LICardActiveInfoListSchema();
				insuListSchema = tInsuListSet.get(i);

				cardActiveInfoListSchema.setDealFlag("00");
				cardActiveInfoListSchema.setState("00");
				cardActiveInfoListSchema.setCardStatus("01");

				cardActiveInfoListSchema.setCardNo(contListSchema.getCardNo());
				// 被保人流水号
				cardActiveInfoListSchema.setSequenceNo(insuListSchema
						.getInsuNo());
				// 卡类型
				cardActiveInfoListSchema.setCardType(contListSchema
						.getRiskCode());
				cardActiveInfoListSchema.setActiveDate(contListSchema
						.getActiveDate());
				cardActiveInfoListSchema.setCValidate(contListSchema
						.getCvaliDate());
				//保障期间
				cardActiveInfoListSchema.setInsuYear(contListSchema.getInsuYear());
				cardActiveInfoListSchema.setInsuYearFlag(contListSchema.getInsuYearFlag());
				

				cardActiveInfoListSchema.setName(insuListSchema.getName());
				cardActiveInfoListSchema.setSex(insuListSchema.getSex());
				cardActiveInfoListSchema.setBirthday(insuListSchema
						.getBirthday());
				cardActiveInfoListSchema.setIdType(insuListSchema.getIDType());
				cardActiveInfoListSchema.setIdNo(insuListSchema.getIDNo());
				cardActiveInfoListSchema.setOccupationType(insuListSchema
						.getOccupationType());
				cardActiveInfoListSchema.setOccupationCode(insuListSchema
						.getOccupationCode());
				cardActiveInfoListSchema.setPostalAddress(insuListSchema
						.getPostalAddress());
				cardActiveInfoListSchema
						.setZipCode(insuListSchema.getZipCode());
				cardActiveInfoListSchema.setPhone(insuListSchema.getPhont());
				cardActiveInfoListSchema.setMobile(insuListSchema.getMobile());
				cardActiveInfoListSchema
						.setGrpName(insuListSchema.getGrpName());
				cardActiveInfoListSchema.setCompanyPhone(insuListSchema
						.getCompanyPhone());
				cardActiveInfoListSchema.setEMail(insuListSchema.getEmail());

				cardActiveInfoListSchema.setOperator(contListSchema
						.getSendOperator());// 卡单激活未提供该字段假数据"100"

				cardActiveInfoListSchema.setMakeDate(currentDate);
				cardActiveInfoListSchema.setMakeTime(currentTime);
				cardActiveInfoListSchema.setModifyDate(currentDate);
				cardActiveInfoListSchema.setModifyTime(currentTime);
				cardActiveInfoListSchema.setPrem(String.valueOf(contListSchema
						.getPrem()));
				cardActiveInfoListSchema.setAmnt(String.valueOf(contListSchema
						.getAmnt()));
				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
//				if (contListSchema.getCopys() > 0) {
//					cardActiveInfoListSchema.setCopys("1");
//				}
				cardActiveInfoListSchema.setCopys(String.valueOf(contListSchema.getCopys()));
				cardActiveInfoListSchema.setTicketNo(contListSchema
						.getTicketNo());
				cardActiveInfoListSchema.setTeamNo(contListSchema.getTeamNo());
				cardActiveInfoListSchema.setSeatNo(contListSchema.getSeatNo());
				cardActiveInfoListSchema.setFrom(contListSchema.getFrom());
				cardActiveInfoListSchema.setTo(contListSchema.getTo());

				cardActiveInfoListSchema.setMult(String.valueOf(contListSchema
						.getMult()));
				// 将生效日期及生效时间 电子商务传入的remark
				// 拼串写入cardActiveInfoListSchema的remark字段

				cardActiveInfoListSchema.setRemark(contListSchema
						.getCvaliDate()
						+ " "
						+ contListSchema.getCvaliDateTime()
						+ "     "
						+ contListSchema.getRemark());

				cardActiveInfoListSet.add(cardActiveInfoListSchema);

			}
		}
		return cardActiveInfoListSet;
	}
	
	// 更新lzcard 中的状态 将state 改为14
	public String[] getUpdateLzcardSql(String cardNo) {
		String[] updateArr = new String[2];
		String modifyStateSql = null;
		String lzcardPKSql = " select  lzc.CertifyCode, lzc.SubCode, lzc.riskcode,lzc.RiskVersion ,lzc.StartNo,lzc.endno,lzc.state "
				+ "from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo "
				+ "and lzc.EndNo = lzcn.CardSerNo where 1 = 1 	and lzcn.CardNo like '"
				+ cardNo + "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS lzcardPKSSRS = tExeSQL.execSQL(lzcardPKSql);
		if (lzcardPKSSRS.MaxRow > 0) {
			String CertifyCode = lzcardPKSSRS.GetText(1, 1);
			String SubCode = lzcardPKSSRS.GetText(1, 2);
			String RiskCode = lzcardPKSSRS.GetText(1, 3);
			String RiskVersion = lzcardPKSSRS.GetText(1, 4);
			String StartNo = lzcardPKSSRS.GetText(1, 5);
			String EndNo = lzcardPKSSRS.GetText(1, 6);
			modifyStateSql = "update lzcard set state='14' where CertifyCode='"
					+ CertifyCode + "' " + "and SubCode='" + SubCode
					+ "' and RiskCode='" + RiskCode + "' and RiskVersion='"
					+ RiskVersion + "' " + "and StartNo='" + StartNo
					+ "' and EndNo='" + EndNo + "'";
			updateArr[0] = modifyStateSql;
			updateArr[1] = lzcardPKSSRS.GetText(1, 7);
		}
		return updateArr;
	}
}
