/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: CardPrintUI </p>
 * <p>Description: CardPrintUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: kevin
 * @CreateDate：2003-10-23
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LZTempReceptSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class CertTempFeeAuditBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private MMap map = new MMap();
    //private PubFun pubFun = new PubFun();

    LZTempReceptSchema mLZTempReceptSchema = new LZTempReceptSchema();
    GlobalInput mGlobalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate="";


    //业务处理相关变量
    /** 全局数据 */

    public CertTempFeeAuditBL()
    {
    }

    /**
    * 提交数据处理方法
    * @param cInputData 传入的数据,VData对象
    * @param cOperate 数据操作字符串
    * @return 布尔值（true--提交成功, false--提交失败）
    */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.strOperate=cOperate;
        System.out.println("..BL.."+cOperate+"  aa");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        System.out.println("..BL1..");
        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("..BL2..");
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        System.out.println("..BL3..");

        PubSubmit tPubSubmit = new PubSubmit();

        if(strOperate.equals("RecordInform"))
        {
            if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN")) {
                buildError("AuditTemp", "不能记录团体暂收费收据信息，该单证可能已被核销");
                return false;
            }
        }
        if(strOperate.equals("AuditCansel"))
        {
            if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN")) {
                buildError("AuditTemp", "团体暂收费收据已核销，但修改记录状态时错误");
                return false;
            }
        }

        if(strOperate.equals("DELETERECORD"))
        {
            if (!tPubSubmit.submitData(mInputData, "DELETE||MAIN")) {
                buildError("AuditTemp", "团体暂收费收据已核销，但修改记录状态时错误");
                return false;
            }
        }

        //this.mResult=mInputData;
        mInputData = null;

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode   = "86940000";
        globalInput.ManageCom = "86940000";
        globalInput.Operator  = "001";

        // 准备传输数据 VData
        VData vData = new VData();
        vData.add(globalInput);
        try
        {
            CertTempFeeAuditBL tCertTempFeeAuditBL = new CertTempFeeAuditBL();

            LZTempReceptSchema schemaLZTempRecept = new LZTempReceptSchema();

            schemaLZTempRecept.setReceptHeading("中国人民银行");
            schemaLZTempRecept.setContNo("contno");
            schemaLZTempRecept.setTempFeeNo("82000000129");
            schemaLZTempRecept.setPayMoney("2000");
            schemaLZTempRecept.setAgentCode("1101001");
            schemaLZTempRecept.setOpenTime("2006-06-30");
            schemaLZTempRecept.setManageCom("8611");

            vData.addElement(schemaLZTempRecept);
            vData.add(globalInput);

            if (!tCertTempFeeAuditBL.submitData(vData, "RecordInform"))
            {
                if (tCertTempFeeAuditBL.mErrors.needDealError())
                {
                    System.out.println(tCertTempFeeAuditBL.mErrors.getFirstError());
                }
                else
                {
                    System.out.println("保存失败，但是没有详细的原因");
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println("dealData()......"+this.strOperate+"bb");
        if(this.strOperate.equals("RecordInform"))
        {
            System.out.println("RecordInform......");
            mLZTempReceptSchema.setOperator(mGlobalInput.Operator);
            mLZTempReceptSchema.setMakeDate(PubFun.getCurrentDate());
            mLZTempReceptSchema.setMakeTime(PubFun.getCurrentTime());
            mLZTempReceptSchema.setModifyDate(PubFun.getCurrentDate());
            mLZTempReceptSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLZTempReceptSchema,"INSERT");
        }
        if (strOperate.equals("AuditCansel"))
        {
            LZTempReceptDB dbLZTempRecept = mLZTempReceptSchema.getDB();

            if(!dbLZTempRecept.getInfo())
            {
                return false;
            }
            System.out.println("AuditCansel......");
            mLZTempReceptSchema =  dbLZTempRecept.getSchema();

            mLZTempReceptSchema.setState("1");
            map.put(mLZTempReceptSchema,"UPDATE");
        }
        if (strOperate.equals("DELETERECORD"))
        {
            System.out.println("DELETERECORD......");
            String sql=" delete from LZTempRecept where TempFeeNo='"+mLZTempReceptSchema.getTempFeeNo()
                       +"' and ReceptHeading='"+mLZTempReceptSchema.getReceptHeading()+"'";
            map.put(sql,"DELETE");
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
       try
       {
           this.mInputData.clear();
           this.mInputData.add(this.mLZTempReceptSchema);
           mInputData.add(map);
           mResult.clear();
           mResult.add(this.mLZTempReceptSchema);
       } catch (Exception ex) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "CertTempFeeAuditBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("aaaaaaa.");
        this.mLZTempReceptSchema.setSchema((LZTempReceptSchema) cInputData.
                                   getObjectByObjectName("LZTempReceptSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /**
     * 错误处理类
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPlanUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
