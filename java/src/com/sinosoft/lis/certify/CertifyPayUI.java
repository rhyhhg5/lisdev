/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LZCardPaySchema;
import com.sinosoft.lis.vschema.LZCardPaySet;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.schema.LMCardRiskSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LZCardPayDB;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CertifyPayUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误容器
    private VData mResult = new VData(); //返回结果
    private String mOperate; //操作类型
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    public CertifyPayUI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        CertifyPayBL tCertifyPayBL = new CertifyPayBL();
        System.out.println("---CertifyPayUI UI BEGIN---");
        if (tCertifyPayBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyPayBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        } else {
            mResult = tCertifyPayBL.getResult();
        }
        System.out.println("---CertifyPayUI  END---");
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput(); //repair:
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";

        LZCardPaySet mLZCardPaySet = new LZCardPaySet();
        CertifyPayUI mCertifyPayUI = new CertifyPayUI();
        // for (int i = 0; i <= 1; i++) {
             LZCardPaySchema mLZCardPaySchema = new LZCardPaySchema();
             mLZCardPaySchema.setPayNo("000012");
             mLZCardPaySchema.setCardType("23");
             mLZCardPaySchema.setManageCom(tGI.ManageCom);
             mLZCardPaySchema.setHandlerCode("1102000038");
             mLZCardPaySchema.setHandler("dddd");
             mLZCardPaySchema.setAgentCom("");
             mLZCardPaySchema.setAgentComName("");
             mLZCardPaySchema.setState("0");
             mLZCardPaySchema.setStartNo("65");
             mLZCardPaySchema.setEndNo("66");
             mLZCardPaySchema.setSumCount("2");
             mLZCardPaySchema.setDuePayMoney(352);
             mLZCardPaySchema.setActPayMoney(352);
             mLZCardPaySchema.setHandleDate(PubFun.getCurrentDate());

             mLZCardPaySchema.setOperator(tGI.Operator);
             mLZCardPaySchema.setMakeDate(PubFun.getCurrentDate());
             mLZCardPaySchema.setMakeTime(PubFun.getCurrentTime());
             mLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
             mLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());

            mLZCardPaySet.add(mLZCardPaySchema);
            mLZCardPaySchema = new LZCardPaySchema();
            mLZCardPaySchema.setPayNo("000012");
            mLZCardPaySchema.setCardType("23");
            mLZCardPaySchema.setManageCom(tGI.ManageCom);
            mLZCardPaySchema.setHandlerCode("1102000038");
            mLZCardPaySchema.setHandler("dddd");
            mLZCardPaySchema.setAgentCom("");
            mLZCardPaySchema.setAgentComName("");
            mLZCardPaySchema.setState("0");
            mLZCardPaySchema.setStartNo("67");
            mLZCardPaySchema.setEndNo("68");
            mLZCardPaySchema.setSumCount("2");
            mLZCardPaySchema.setDuePayMoney(352);
            mLZCardPaySchema.setActPayMoney(352);
            mLZCardPaySchema.setHandleDate(PubFun.getCurrentDate());

            mLZCardPaySchema.setOperator(tGI.Operator);
            mLZCardPaySchema.setMakeDate(PubFun.getCurrentDate());
            mLZCardPaySchema.setMakeTime(PubFun.getCurrentTime());
            mLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
            mLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());

            mLZCardPaySet.add(mLZCardPaySchema);

         //}
        //VData tVData = new VData();
        //tVData.addElement("RETURNDATA");
        /* LZCardPaySchema tLZCardPaySchema = new LZCardPaySchema();
         LZCardPaySet tLZCardPaySet = new LZCardPaySet();
         tLZCardPaySchema.setPayNo("JSBJ20060922");
         tLZCardPaySchema.setCardType("21");
         tLZCardPaySchema.setStartNo("00000002");
         tLZCardPaySchema.setEndNo("0000005");*/

        // tLZCardPaySet.add(tLZCardPaySchema);
        LZCardPayDB mLZCardPayDB = new LZCardPayDB();
       // LZCardPaySet mLZCardPaySet = new LZCardPaySet();
       // mLZCardPayDB.setPayNo("jsbj2006092201");
        //mLZCardPayDB.setState("0");
        //mLZCardPaySet = mLZCardPayDB.query();

        VData tVData = new VData();
        tVData.addElement("UPDATE");
        tVData.addElement(mLZCardPaySet);
        tVData.addElement(tGI);
        mCertifyPayUI.submitData(tVData, "UPDATE");
    }

    private void jbInit() throws Exception {
    }
}
