/**
 * 2011-8-9
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;



public class LZBriGrpBatchImportUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public LZBriGrpBatchImportUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            LZBriGrpBatchImportBL tBusLogic = new LZBriGrpBatchImportBL();
            if (!tBusLogic.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBusLogic.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
