/**
 * 
 */
package com.sinosoft.lis.certify;

import java.math.BigInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author Administrator
 *
 */
public class CertifyReTakeBackBL
{
    public CErrors mErrors = new CErrors();//错误处理类
    private VData result = new VData();//数据流转类
    private MMap map = new MMap();//存放待处理数据
    private GlobalInput globalInput = new GlobalInput();//公共方法
    private LZCardSchema tLZCardSchema;
    private String currentDate = PubFun.getCurrentDate();//当前日期
    private String currendTime = PubFun.getCurrentTime();//当前时间
    private String operator;//当前操作者
    private String comCode;//当前操作者管理机构
    private String startNo;//待处理单证起始号
    private String endNo;//待处理单证终止号
    private String oldStateFlag;//单证当前状态
    private String newStateFlag;//单证新状态
    private int sumCount;//处理的单证数据
    private Log log = LogFactory.getLog(this.getClass().getName());//日志
    
    /**
     * 公共方法
     * 业务入口
     * 
     * @param inputData
     * @param operate
     * @return
     */
    public boolean submitData(VData inputData, String operate)
    {
        if (!getInputData(inputData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(result, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     * 公共方法
     * 用于接收前台传来的数据
     * 
     * @param inputData
     * @return
     */
    private boolean getInputData(VData inputData)
    {
        globalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
        tLZCardSchema = (LZCardSchema) inputData.getObjectByObjectName("LZCardSchema", 0);
        TransferData tTransferData = (TransferData) inputData.getObjectByObjectName("TransferData", 0);
        oldStateFlag = (String) tTransferData.getValueByName("oldStateFlag");
        newStateFlag = (String) tTransferData.getValueByName("newStateFlag");
        
        operator = globalInput.Operator;
        comCode = globalInput.ComCode;
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于数据校验
     * 
     * @return
     */
    private boolean checkData()
    {
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于处理业务
     * 
     * @return
     */
    private boolean dealData()
    {
        String certifyCode = tLZCardSchema.getCertifyCode();
        startNo = tLZCardSchema.getStartNo();
        endNo = tLZCardSchema.getEndNo();
        LZCardDB tLZCardDB = new LZCardDB();
        
        //查询待处理的单证起始号所在的单证号段
        String SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
            + "and StartNo <= '" + startNo + "' and EndNo >= '" + startNo + "' "
            + "and StateFlag = '" + oldStateFlag + "' ";
        log.info(SQL);
        LZCardSet mLZCardSet = tLZCardDB.executeQuery(SQL);
        String minStartNo = "";
        if(mLZCardSet.size() == 1)
            minStartNo = mLZCardSet.get(1).getStartNo();
        else
        {
            buildError("dealData", "单证起始号查询错误！");
            return false;
        }

        //查询待处理的单证终止号所在的单证号段
        SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
            + "and StartNo <= '" + endNo + "' and EndNo >= '" + endNo + "' "
            + "and StateFlag = '" + oldStateFlag + "' ";
        log.info(SQL);
        mLZCardSet = tLZCardDB.executeQuery(SQL);
        String maxEndNo = "";
        if(mLZCardSet.size() == 1)
            maxEndNo = mLZCardSet.get(1).getEndNo();
        else
        {
            buildError("dealData", "单证终止号查询错误！");
            return false;
        }
        
        String sendSQL = "";
        if(comCode.length() == 2)
        {
            sendSQL = "";
        }
        else if(comCode.length() == 4)
        {
            sendSQL = "and (SendOutCom in ("
                + "select ReceiveCom from LZAccess where SendOutCom = 'A" + comCode + "' "
                + "union select 'A" + comCode + "' from dual) "
                + "or SendOutCom like 'D%' or SendOutCom like 'E%') ";
        }
        else
        {
            buildError("dealData", "只有总公司及分公司操作员可以做单证状态修改！");
            return false;
        }
        
        SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
            + "and StartNo >= '" + minStartNo + "' and EndNo <= '" + maxEndNo + "' "
            + "and StateFlag = '" + oldStateFlag + "' "
            + sendSQL
            + "order by StartNo with ur";
        log.info(SQL);
        mLZCardSet = tLZCardDB.executeQuery(SQL);
        if(mLZCardSet.size() < 0)
        {
            buildError("dealData", "单证不在可修改范围内！");
            return false;
        }

        for(int j = 0; j < mLZCardSet.size(); j++)
        {
            LZCardSchema mLZCardSchema = mLZCardSet.get(j + 1);
            if(!dealOneCertify(mLZCardSchema))
            {
                return false;
            }
        }
        
        if(sumCount != tLZCardSchema.getSumCount())
        {
            buildError("dealOneCertify", "请检查单证号段内是否都是可修改！");
            return false;
        }
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于存放待处理的数据
     * 
     * @return
     */
    private boolean prepareOutputData() {

        try {
            result.add(map);
        }
        catch (Exception e)
        {
            buildError("prepareOutputData", "准备传输数据失败！");
            return false;
        }
        return true;
    }
    
    /**
     * 处理一个号段的单证
     * 所处理号段单证信息是从数据库中查询出来的实际值
     * 
     * @param tLZCardSchema
     * @return
     */
    private boolean dealOneCertify(LZCardSchema tLZCardSchema)
    {
        try
        {
            map.put(tLZCardSchema.clone(), SysConst.DELETE);
            //填充公共字段
            dealGeneralField(tLZCardSchema);
            //如果实际的起始号比当前起始号小，则拆分成两条记录
            if(tLZCardSchema.getStartNo().compareTo(startNo) < 0)
            {
                //新的单证号段
                LZCardSchema newLZCardSchema = (LZCardSchema) tLZCardSchema.clone();
                tLZCardSchema.setStartNo(startNo);
                
                newLZCardSchema.setEndNo(CertifyFunc.bigIntegerPlus(startNo, "-1", startNo.length()));
                newLZCardSchema.setSumCount((int) new BigInteger(newLZCardSchema.getEndNo())
                    .subtract(new BigInteger(newLZCardSchema.getStartNo())).longValue() + 1);
                //生成轨迹
                LZCardTrackSchema newLZCardTrackSchema = new LZCardTrackSchema();
                new Reflections().transFields(newLZCardTrackSchema, newLZCardSchema);

                map.put(newLZCardSchema, SysConst.DELETE_AND_INSERT);
                map.put(newLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
            }
            //如果实际的起始号比当前起始号大，则拆分成两条记录
            if(tLZCardSchema.getEndNo().compareTo(endNo) > 0)
            {
                //新的单证号段
                LZCardSchema newLZCardSchema = (LZCardSchema) tLZCardSchema.clone();
                tLZCardSchema.setEndNo(endNo);
                newLZCardSchema.setStartNo(CertifyFunc.bigIntegerPlus(endNo, "1", endNo.length()));
                newLZCardSchema.setSumCount((int) new BigInteger(newLZCardSchema.getEndNo())
                    .subtract(new BigInteger(newLZCardSchema.getStartNo())).longValue() + 1);
                //生成轨迹
                LZCardTrackSchema newLZCardTrackSchema = new LZCardTrackSchema();
                new Reflections().transFields(newLZCardTrackSchema, newLZCardSchema);

                map.put(newLZCardSchema, SysConst.DELETE_AND_INSERT);
                map.put(newLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
            }

            tLZCardSchema.setSumCount((int) new BigInteger(tLZCardSchema.getEndNo())
                .subtract(new BigInteger(tLZCardSchema.getStartNo())).longValue() + 1);
            sumCount += tLZCardSchema.getSumCount();//记录处理的单证数量
            tLZCardSchema.setStateFlag(newStateFlag);
            tLZCardSchema.setOperateFlag("0");
            //生成轨迹
            LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();
            new Reflections().transFields(tLZCardTrackSchema, tLZCardSchema);
            
            map.put(tLZCardSchema, SysConst.DELETE_AND_INSERT);
            map.put(tLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
        }
        catch(Exception e)
        {
            buildError("dealOneCertify", "处理单证错误！");
            return false;
        }
        return true;
    }
    
    /**
     * 处理公共字段
     * 
     * @param tLZCardSchema
     */
    private void dealGeneralField(LZCardSchema tLZCardSchema)
    {
        tLZCardSchema.setHandler(operator);
        tLZCardSchema.setHandleDate(currentDate);
        tLZCardSchema.setOperator(operator);
        tLZCardSchema.setMakeDate(currentDate);
        tLZCardSchema.setMakeTime(currendTime);
        tLZCardSchema.setModifyDate(currentDate);
        tLZCardSchema.setModifyTime(currendTime);
    }

    /**
     * 公共错误处理方法
     * 
     * @param functionName
     * @param errorMessage
     */
    private void buildError(String functionName, String errorMessage) {
        CError cError = new CError();
        cError.moduleName = "CertifyReTakeBackBL";
        cError.functionName = functionName;
        cError.errorMessage = errorMessage;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

    }

}
