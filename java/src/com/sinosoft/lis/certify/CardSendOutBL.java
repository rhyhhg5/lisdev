/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardPrintSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理普通单证发放操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */

public class CardSendOutBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /* 私有成员 */
    private String mszOperate = "";
    private String mszTakeBackNo = "";


    /* 业务相关的数据 */
    private GlobalInput globalInput = new GlobalInput();
    private LZCardSet mLZCardSet = new LZCardSet();
    private LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet();
    private boolean m_bLimitFlag = false;

    public CardSendOutBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))return false;
        if (!dealData())return false;
        return true;
    }


    /**
     * 处理一条单证发放的信息。有可能是单证入库操作，也有可能是单证发放操作。
     * @param aLZCardSchema
     * @return
     */
    private boolean dealOne(LZCardSchema aLZCardSchema,
                            LZCardPrintSchema aLZCardPrintSchema)
    {

        // Verify SendOutCom and ReceiveCom
        if (!CertifyFunc.verifyComs(globalInput, aLZCardSchema.getSendOutCom(),
                                    aLZCardSchema.getReceiveCom()))
        {

            mErrors.copyAllErrors(CertifyFunc.mErrors);
            return false;
        }

        VData vResult = new VData();

        if (aLZCardSchema.getSendOutCom().equals(CertifyFunc.INPUT_COM))
        { // 如果是入库操作
            if (!CertifyFunc.inputCertify(globalInput, aLZCardSchema,
                                          aLZCardPrintSchema,
                                          CertifyFunc.CERTIFY_CLASS_CARD,
                                          vResult))
            {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
                return false;
            }
        }
        else
        { // 如果不是入库操作，则进行单证拆分操作
            if (!CertifyFunc.splitCertifySendOut(globalInput, aLZCardSchema,
                                                 m_bLimitFlag, vResult))
            {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
                return false;
            }
        }

        CardSendOutBLS tCardSendOutBLS = new CardSendOutBLS();
        if (!tCardSendOutBLS.submitData(vResult, "INSERT"))
        {
            if (tCardSendOutBLS.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCardSendOutBLS.mErrors);
                return false;
            }
            else
            {
                buildError("dealOne", "CertifySendOutBLS出错，但是没有提供详细的信息");
                return false;
            }
        }

        return true;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        if (mszOperate.equals("INSERT"))
        {
            if (!mLZCardSet.get(1).getSendOutCom().equals("00"))
            {
                for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
                {

                    LZCardSchema schemaLZCard = mLZCardSet.get(nIndex + 1); // faint to death

                    if (CertifyFunc.bigIntegerDiff(schemaLZCard.getStartNo(),
                            schemaLZCard.getEndNo()) > 0)
                    {
                        buildError("dealData", "输入的起始单证号不能大于终止单证号，请重新输入!");
                        return false;
                    }
                }
            }

            // 产生回收清算单号
            mszTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO",
                                                PubFun.getNoLimit(globalInput.
                    ComCode.substring(1)));

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                LZCardPrintSchema tLZCardPrintSchema = mLZCardPrintSet.get(
                        nIndex + 1);
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                tLZCardSchema.setTakeBackNo(mszTakeBackNo);

                if (!dealOne(tLZCardSchema, tLZCardPrintSchema))
                {
                    return false;
                }
            }
        } // end of if( mszOperate.equals("INSERT") );

        System.out.println(" End of deel date ... ");
        return true;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
        if (mszOperate.equals("INSERT"))
        {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mLZCardPrintSet.set((LZCardPrintSet) vData.getObjectByObjectName(
                    "LZCardPrintSet", 0));

            String str = (String) vData.getObjectByObjectName("String", 0);
            if (str == null || !str.equals("NO"))
            {
                m_bLimitFlag = true; // 缺省的行为是要校验
            }
            else
            {
                m_bLimitFlag = false;
            }
        }

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            if (mszOperate.equals("INSERT"))
            {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
                vData.addElement(mLZCardPrintSet);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CardSendOutBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardSendOutUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }
}
