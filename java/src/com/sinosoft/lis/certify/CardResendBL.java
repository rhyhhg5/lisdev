/**
 * 
 */
package com.sinosoft.lis.certify;

import java.math.BigInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * 定额单证的空白回销
 * 
 * @author Administrator
 *
 */
public class CardResendBL
{
    public CErrors mErrors = new CErrors();//错误处理类
    private VData result = new VData();//数据流转类
    private MMap map = new MMap();//存放待处理数据
    private GlobalInput globalInput = new GlobalInput();//公共方法
    private LZCardSet tLZCardSet;
    private String currentDate = PubFun.getCurrentDate();//当前日期
    private String currendTime = PubFun.getCurrentTime();//当前时间
    private String operator;//当前操作者
    private String comCode;//当前操作者管理机构
    private String startNo;//待处理单证起始号
    private String endNo;//待处理单证终止号
    private int sumCount;//处理的单证数据
    private Log log = LogFactory.getLog(this.getClass().getName());//日志
    
    /**
     * 公共方法
     * 业务入口
     * 
     * @param inputData
     * @param operate
     * @return
     */
    public boolean submitData(VData inputData, String operate)
    {
        if (!getInputData(inputData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(result, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     * 公共方法
     * 用于接收前台传来的数据
     * 
     * @param inputData
     * @return
     */
    private boolean getInputData(VData inputData)
    {
        globalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
        tLZCardSet = (LZCardSet) inputData.getObjectByObjectName("LZCardSet", 0);
        
        operator = globalInput.Operator;
        comCode = globalInput.ComCode;
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于数据校验
     * 
     * @return
     */
    private boolean checkData()
    {
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于处理业务
     * 
     * @return
     */
    private boolean dealData()
    {
        for(int i = 0; i < tLZCardSet.size() ; i++)
        {
            LZCardSchema tLZCardSchema = tLZCardSet.get(i + 1);
            String certifyCode = tLZCardSchema.getCertifyCode();
            startNo = tLZCardSchema.getStartNo();
            endNo = tLZCardSchema.getEndNo();
            LZCardDB tLZCardDB = new LZCardDB();
            
            //查询待处理的单证起始号所在的单证号段
            String SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
                + "and StartNo <= '" + startNo + "' and EndNo >= '" + startNo + "' ";
            log.info(SQL);
            LZCardSet mLZCardSet = tLZCardDB.executeQuery(SQL);
            String minStartNo = "";
            if(mLZCardSet.size() == 1)
                minStartNo = mLZCardSet.get(1).getStartNo();
            else
            {
                buildError("dealOneCertify", "回销单证起始号错误！");
                return false;
            }

            //查询待处理的单证终止号所在的单证号段
            SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
                + "and StartNo <= '" + endNo + "' and EndNo >= '" + endNo + "' ";
            log.info(SQL);
            mLZCardSet = tLZCardDB.executeQuery(SQL);
            String maxEndNo = "";
            if(mLZCardSet.size() == 1)
                maxEndNo = mLZCardSet.get(1).getEndNo();
            else
            {
                buildError("dealData", "回销单证终止号错误！");
                return false;
            }
            
            SQL = "select * from LZCard where CertifyCode = '" + certifyCode + "' "
                + "and StartNo >= '" + minStartNo + "' and EndNo <= '" + maxEndNo + "' "
                + "and State in ('3', '8', '9', '10', '11') ";
            if(comCode.length() <= 4)
                SQL += "and ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A"
                    + comCode + "') ";
            else
                SQL += "and SendOutCom = 'B" + globalInput.Operator + "' ";
            SQL += "order by StartNo with ur";
            log.info(SQL);
            mLZCardSet = tLZCardDB.executeQuery(SQL);
            if(mLZCardSet.size() < 0)
            {
                buildError("dealData", "单证不在可回销范围内！");
                return false;
            }

            for(int j = 0; j < mLZCardSet.size(); j++)
            {
                LZCardSchema mLZCardSchema = mLZCardSet.get(j + 1);
                if(!dealOneCertify(mLZCardSchema))
                {
                    return false;
                }
            }
            
            if(sumCount != tLZCardSchema.getSumCount())
            {
                buildError("dealOneCertify", "请检查单证号段内是否都是可空白回销单证！");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * 公共方法
     * 用于存放待处理的数据
     * 
     * @return
     */
    private boolean prepareOutputData() {

        try {
            result.add(map);
        }
        catch (Exception e)
        {
            buildError("prepareOutputData", "准备传输数据失败！");
            return false;
        }
        return true;
    }
    
    /**
     * 处理一个号段的单证
     * 所处理号段单证信息是从数据库中查询出来的实际值
     * 
     * @param tLZCardSchema
     * @return
     */
    private boolean dealOneCertify(LZCardSchema tLZCardSchema)
    {
        try
        {
            map.put(tLZCardSchema.clone(), SysConst.DELETE);
            //填充公共字段
            dealGeneralField(tLZCardSchema);
            //如果实际的起始号比当前起始号小，则拆分成两条记录
            if(tLZCardSchema.getStartNo().compareTo(startNo) < 0)
            {
                //新的单证号段
                LZCardSchema newLZCardSchema = (LZCardSchema) tLZCardSchema.clone();
                tLZCardSchema.setStartNo(startNo);
                
                newLZCardSchema.setEndNo(CertifyFunc.bigIntegerPlus(startNo, "-1", startNo.length()));
                newLZCardSchema.setSumCount((int) new BigInteger(newLZCardSchema.getEndNo())
                    .subtract(new BigInteger(newLZCardSchema.getStartNo())).longValue() + 1);
                //生成轨迹
                LZCardTrackSchema newLZCardTrackSchema = new LZCardTrackSchema();
                new Reflections().transFields(newLZCardTrackSchema, newLZCardSchema);

                map.put(newLZCardSchema, SysConst.DELETE_AND_INSERT);
                map.put(newLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
            }
            //如果实际的起始号比当前起始号大，则拆分成两条记录
            if(tLZCardSchema.getEndNo().compareTo(endNo) > 0)
            {
                //新的单证号段
                LZCardSchema newLZCardSchema = (LZCardSchema) tLZCardSchema.clone();
                tLZCardSchema.setEndNo(endNo);
                newLZCardSchema.setStartNo(CertifyFunc.bigIntegerPlus(endNo, "1", endNo.length()));
                newLZCardSchema.setSumCount((int) new BigInteger(newLZCardSchema.getEndNo())
                    .subtract(new BigInteger(newLZCardSchema.getStartNo())).longValue() + 1);
                //生成轨迹
                LZCardTrackSchema newLZCardTrackSchema = new LZCardTrackSchema();
                new Reflections().transFields(newLZCardTrackSchema, newLZCardSchema);

                map.put(newLZCardSchema, SysConst.DELETE_AND_INSERT);
                map.put(newLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
            }

            tLZCardSchema.setSumCount((int) new BigInteger(tLZCardSchema.getEndNo())
                .subtract(new BigInteger(tLZCardSchema.getStartNo())).longValue() + 1);
            sumCount += tLZCardSchema.getSumCount();//记录处理的单证数量
            tLZCardSchema.setSendOutCom(tLZCardSchema.getReceiveCom());
            //由于之前的回销程序在连续回销时会出错,ReceiveCom改为按登陆人的管理机构来判断 add by xp  090326
            if(comCode.length() <= 4){
            	tLZCardSchema.setReceiveCom("A"+comCode);
            }
            else{
            	tLZCardSchema.setReceiveCom("B"+operator);
            }
            tLZCardSchema.setState("3");
            
            //生成轨迹
            LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();
            new Reflections().transFields(tLZCardTrackSchema, tLZCardSchema);
            
            map.put(tLZCardSchema, SysConst.DELETE_AND_INSERT);
            map.put(tLZCardTrackSchema, SysConst.DELETE_AND_INSERT);
        }
        catch(Exception e)
        {
            buildError("dealOneCertify", "处理单证错误！");
            return false;
        }
        return true;
    }
    
    /**
     * 处理公共字段
     * 
     * @param tLZCardSchema
     */
    private void dealGeneralField(LZCardSchema tLZCardSchema)
    {
        tLZCardSchema.setHandler(operator);
        tLZCardSchema.setHandleDate(currentDate);
        tLZCardSchema.setOperator(operator);
        tLZCardSchema.setMakeDate(currentDate);
        tLZCardSchema.setMakeTime(currendTime);
        tLZCardSchema.setModifyDate(currentDate);
        tLZCardSchema.setModifyTime(currendTime);
    }

    /**
     * 公共错误处理方法
     * 
     * @param functionName
     * @param errorMessage
     */
    private void buildError(String functionName, String errorMessage) {
        CError cError = new CError();
        cError.moduleName = "CardResendBL";
        cError.functionName = functionName;
        cError.errorMessage = errorMessage;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

    }

}
