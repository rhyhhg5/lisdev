/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zhouping
 * @version 1.0
 */

public class CertifyInputBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;

    public CertifyInputBLS()
    {
    }

    public static void main(String[] args)
    {
        CertifyInputBLS CertifyInputBLS1 = new CertifyInputBLS();
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        System.out.println("Start CertifyInputBLS Submit");

        if (cOperate == "INSERT")
        {
            tReturn = save();
        }
        else
        {
            tReturn = false;
        }

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }

        System.out.println("End CertifyInputBLS Submit");

        //如果有需要处理的错误，则返回
//    if (tCertifyInputBLS.mErrors .needDealError() )
//        this.mErrors .copyAllErrors(tCertifyInputBLS.mErrors ) ;

        mInputData = null;
        return tReturn;
    }

    private boolean save()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        try
        {
            JdbcUrl tJdbcUrl = new JdbcUrl();
            // 特别约定
            System.out.println("Start 特约...");
            // LCSpecSet tLCSpecSet   = (LCSpecSet)mInputData.getObjectByObjectName("com.sinosoft.lis.vschema.LCSpecSet",0);
            // LCSpecDBSet tLCSpecDBSet=new LCSpecDBSet(tJdbcUrl);
            // tLCSpecDBSet.save(tLCSpecSet) ;
        }
        catch (Exception ex)
        {
            System.out.println("Exception in BLS");
            System.out.println("Exception:" + ex.toString());
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyInputBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
        }
        return tReturn;
    }
}
