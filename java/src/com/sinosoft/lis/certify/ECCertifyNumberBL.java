package com.sinosoft.lis.certify;

import java.sql.Connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sinosoft.lis.db.LMCardRiskDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LZCardPrintDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.schema.LZCardNumberSchema;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.LZCardTrackSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 单证号码生成</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */
public class ECCertifyNumberBL {
    

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 数据库连接  **/
    private Connection mConn = null; //Added For LargeData Disposal At 2008.08.12

    /** 往界面传输数据的容器 */
    private VData mResult = new VData(); //返回结果
    private LZCardNumberSet mLZCardNumberSet = new LZCardNumberSet();
    private LZCardNumberSet mOldLZCardNumberSet = new LZCardNumberSet();
    private LMCertifyDesSchema schemaLMCertifyDes = null;
    private LZCardSchema schemaLZCard = null;
    private LZCardSet mLZCardSet = new LZCardSet( );
    private LZCardTrackSchema mLZCardTrackSchema = null; 
    private LZCardTrackSet mLZCardTrackSet = new LZCardTrackSet();
    private LZCardPrintSchema mLZCardPrint = new LZCardPrintSchema();
    private String CurrentDate = PubFun.getCurrentDate(); //当前时间
    private String CurrentTime = PubFun.getCurrentTime(); //当前时间
    private TransferData mTransferData = new TransferData(); //存储接受数据
    private GlobalInput mGI = new GlobalInput(); //存储用户信息
    /** 印刷号*/
	private String mPrtNo = null;  
	/** 单证编码*/
	private String mCertifyCode = null;
	/** 单证类型*/
	private String mCardType =  null;
	/** 卡起始号*/
	private String mCardSerNo  = 	null;
	/** 卡终止号*/
	private String strEndNo = null;
	/** 生成数量*/
	private String mCardAmount = 	null;
	/** 卡业务类型名称*/
	private String mCardTypeName = null;
	/** 卡业务类型*/
	private String mOperateType = null;
	/** 校验规则名称*/
	private String mChkRuleName = null;
	/** 单证号码长度*/
	private String mCertifyLengthV = null;
	/** 单证价格*/
	private String mCertifyPrice =  null;
	/** 是否生成密码*/
	private String mCreatePass = 	null;
	/** 发放者*/
	private String mSendOutComEx =  null;
	/** 接收者*/
	private String mAgentCode = null;
	/** */
	private String mPageType = null;
	/** */
	private String operate = 	null;
	/** */
	private String mCheckRule = null;
    private Log log = LogFactory.getLog(this.getClass().getName());//日志

    public ECCertifyNumberBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
	{
    	if(!getInputData(cInputData)){
    		System.out.println("接收页面数据异常");
    		return false;
    	}
    	if(!deal()){
    		System.out.println("数据处理异常");
    		return false;
    	}
    	if(!save()){
    		System.out.println("数据提交异常");
    		return false;
    	}
		return true;
	}

    public VData getResult() {
        return mResult;
    }

    /**
     * 接收从CertifyDescSave.jsp中传递的数据
     */
    private boolean getInputData(VData cInputData) {
        log.info("CertifyNumberBL  getInputData  start ...");
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0); //传输页面输入的起止时间
        mGI = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0); //传输页面输入的起止时间
        if (mTransferData == null || mGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }       
        //
        this.mPrtNo=(String) mTransferData.getValueByName("PrtNo");	
        this.mPrtNo =PubFun1.CreateMaxNo("PrtNo",PubFun.getNoLimit(mGI.ComCode.substring(1)));
        this.mCertifyCode=(String) mTransferData.getValueByName("CertifyCode");
        this.mCardType=(String) mTransferData.getValueByName("CardType");		
        this.mCardSerNo=(String) mTransferData.getValueByName("CardSerNo");	
        this.mCardAmount=(String) mTransferData.getValueByName("CardAmount");	
        this.mCardTypeName=(String) mTransferData.getValueByName("CardTypeName");
        this.mOperateType=(String) mTransferData.getValueByName("OperateType");
        this.mChkRuleName=(String) mTransferData.getValueByName("ChkRuleName");
        this.mCertifyLengthV=(String) mTransferData.getValueByName("CertifyLengthV");
        this.mCertifyPrice=(String) mTransferData.getValueByName("CertifyPrice");
        this.mCreatePass=(String) mTransferData.getValueByName("CreatePass");	
        this.mSendOutComEx=(String) mTransferData.getValueByName("SendOutComEx");
        this.mAgentCode=(String) mTransferData.getValueByName("AgentCode");	
                                               
        this.mPageType=(String) mTransferData.getValueByName("PageType");				
        this.mCheckRule=(String) mTransferData.getValueByName("CheckRule");	


        return true;
    }
    
    /**
     * 业务处理
     * @return
     */
    private boolean deal(){
    	if(!dealLZCardNumberInfo()){
    		System.out.println("生成卡号异常");
    		return false;
    	}
    	
    	if(!dealLZCardPrintInfo()){
    		System.out.println("定单入库异常");
    		return false;
    	}
    	if(!dealLZCardInfo()){
    		System.out.println("定单入库lzcard异常");
    		return false;
    	}
    	return true;
    }
    
    /**
     * 生成卡号
     * @return
     */
    private boolean dealLZCardNumberInfo(){
    	try{

            log.info("ECCertifyNumberBL  dealData  start ...");

            String strContr = "";
            mLZCardNumberSet.clear();
            mInputData.clear();
            
            //查询单证描述
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            tLMCertifyDesDB.setSubCode(mCardType);
            LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.query();
            if(tLMCertifyDesSet.size() != 1)
            {
                this.mErrors.addOneError("查询单证描述失败！");
                return false;
            }
            LMCertifyDesSchema tLMCertifyDesSchema = tLMCertifyDesSet.get(1);
            String checkRule = tLMCertifyDesSchema.getCheckRule();//校验规则：1-有校验，2无校验
            System.out.println("校验规则："+checkRule+"====219");
            String operateType = tLMCertifyDesSchema.getOperateType();//单证业务类型：0-卡单业务，1-撕单业务，4-网销
            if(!operateType.equals(this.mOperateType)){
            	 // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ECCertifyNumberBL";
                tError.functionName = "dealLZCardNumberInfo";
                tError.errorMessage = "此单证没有被描述为网销类型!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
//            if (mPageType.equals("0")) {//单证号码生成
                int intSerNo = Integer.parseInt(mCardSerNo);
                int intNum = Integer.parseInt(this.mCardAmount);
                LisIDEA tLisIDEA = new LisIDEA();
                for (int i = 0; i < intNum; i++) {
                    String strCurSerNo = String.valueOf(intSerNo + i);
                    
                    //根据单证长度填充单证码
                    int certifyLength = (int) tLMCertifyDesSchema.getCertifyLength();
                    strCurSerNo = PubFun.LCh(strCurSerNo, "0", certifyLength);
                    
                    String strCardSer = mCardType + strCurSerNo;
                    String checkNo = "00";
                    String cardNo = "";
                    //有校验规则的单证生成单证校验码
                    if("1".equals(checkRule))
                    {
                        strContr = strCardSer + strCardSer.substring(0, 8);
                        checkNo = GetVerifyBit(strContr);
                        cardNo = strCardSer + checkNo;
                    }
                    else
                    {
                        cardNo = strCardSer;
                    }
                    
                    //单证密码生成
                    String strPass = "";
                    String strPassword = "";
                    if (this.mCreatePass.equals("Y")) {
                        strPass = genRandomNum(6);
                        StringBuffer sqlSB = new StringBuffer();
                        sqlSB.append(String.valueOf(strPass.charAt(3)))
                                .append(String.valueOf(strPass.charAt(0)))
                                .append(String.valueOf(strPass.charAt(4)))
                                .append(String.valueOf(strPass.charAt(1)))
                                .append(String.valueOf(strPass.charAt(5)))
                                .append(String.valueOf(strPass.charAt(2)))
                                ;
                        strPassword = sqlSB.toString();
                    }
                    strPass = tLisIDEA.encryptString(strPass);

                    LZCardNumberSchema tLZCardNumberSchema = new LZCardNumberSchema();
                    tLZCardNumberSchema.setCardChkNo(checkNo);
                    tLZCardNumberSchema.setPrtNo(this.mPrtNo);
                    tLZCardNumberSchema.setCardNo(cardNo);
                    tLZCardNumberSchema.setOperateType(operateType);
                    tLZCardNumberSchema.setOperator(mGI.Operator);
                    tLZCardNumberSchema.setCardPassword(strPass);
                    tLZCardNumberSchema.setCardPassword2(strPassword);
                    tLZCardNumberSchema.setCardSerNo(strCurSerNo);
                    tLZCardNumberSchema.setCardType(mCardType);
                    tLZCardNumberSchema.setMakeDate(CurrentDate);
                    tLZCardNumberSchema.setMakeTime(CurrentTime);
                    tLZCardNumberSchema.setModifyDate(CurrentDate);
                    tLZCardNumberSchema.setModifyTime(CurrentTime);
                    tLZCardNumberSchema.setPrintFlag("0");
                    mLZCardNumberSet.add(tLZCardNumberSchema);

                    //超过5000条记录的set则每5000条记录写入数据库但不提交  Added For LargeData Disposal At 2008.08.12  Begin....
                    if(mLZCardNumberSet.size()> SysConst.FETCHCOUNT)
                    {
                    	mLZCardNumberSet.clear();
                    	// @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ECCertifyNumberBL";
                        tError.functionName = "dealLZCardNumberInfo";
                        tError.errorMessage = "生成卡号数量大于5000条!";
                        this.mErrors.addOneError(tError);
                        return false;                   	
                    }
                    //超过5000条记录的set则每5000条记录写入数据库但不提交  Added For LargeData Disposal At 2008.08.12  End....
                }
                map.put(mLZCardNumberSet, "INSERT");
//            } 
//            else if (mPageType.equals("1")) {//重复下载类
//                if (!getOldData()) {
//                    return false;
//                }
//                if (!deleteData()) {
//                    return false;
//                }
//                LisIDEA tLisIDEA = new LisIDEA();
//                for (int i = 1; i <= mOldLZCardNumberSet.size(); i++) {
//                    LZCardNumberSchema tLZCardNumberSchema = new LZCardNumberSchema();
//                    tLZCardNumberSchema = mOldLZCardNumberSet.get(i);
//                    String strPass = "";
//                    String strPassword = "";
//                    if (operateType.equals("0")||operateType.equals("2")) {
//                        strPass = genRandomNum(6);
//                        StringBuffer sqlSB = new StringBuffer();
//                        sqlSB.append(String.valueOf(strPass.charAt(3)))
//                                .append(String.valueOf(strPass.charAt(0)))
//                                .append(String.valueOf(strPass.charAt(4)))
//                                .append(String.valueOf(strPass.charAt(1)))
//                                .append(String.valueOf(strPass.charAt(5)))
//                                .append(String.valueOf(strPass.charAt(2)))
//                                ;
//                        strPassword = sqlSB.toString();
//                    }
//                    strPass = tLisIDEA.encryptString(strPass);
//                    tLZCardNumberSchema.setCardPassword(strPass);
//                    tLZCardNumberSchema.setCardPassword2(strPassword);
//                    tLZCardNumberSchema.setOperator(mGI.Operator);
//                    tLZCardNumberSchema.setModifyDate(CurrentDate);
//                    tLZCardNumberSchema.setModifyTime(CurrentTime);
//                    mLZCardNumberSet.add(tLZCardNumberSchema);
//                }
//            }
        
    	}catch(Exception e){
    		// @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberBL";
            tError.functionName = "dealLZCardNumberInfo";
            tError.errorMessage = "生成卡号异常!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	return true;
    }
    
    private boolean dealLZCardPrintInfo(){
    	try{
    		String strSql = "SELECT * FROM LMCertifyDes  WHERE CertifyCode = '" + this.mCertifyCode + "'";
    		System.out.println(strSql);
    		LMCertifyDesSet setLMCertifyDes = new LMCertifyDesDB().executeQuery(strSql);
    		if (setLMCertifyDes.size() != 1)
            {
    			// @@错误处理
                CError tError = new CError();
                tError.moduleName = "ECCertifyNumberBL";
                tError.functionName = "dealLZCardPrintInfo";
                tError.errorMessage = "无效的单证编码!";
                this.mErrors.addOneError(tError);
                return false;
            }
//    		if(!setLMCertifyDes.get(1).getManageCom().equals(mGI.ComCode))
//            {
//             // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "ECCertifyNumberBL";
//                tError.functionName = "dealLZCardPrintInfo";
//                tError.errorMessage = "当前操作员不能操作此种单证!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
    		
    		schemaLMCertifyDes = setLMCertifyDes.get(1);
    		if ("D".equals(schemaLMCertifyDes.getCertifyClass())) {
                LMCardRiskDB tLMCardRiskDB = new LMCardRiskDB();
                tLMCardRiskDB.setCertifyCode(schemaLMCertifyDes.
                                             getCertifyCode());
                LMCardRiskSet tLMCardRiskSet = new LMCardRiskSet();
                tLMCardRiskSet = tLMCardRiskDB.query();
                if (tLMCardRiskSet.size() == 0) {
                 // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ECCertifyNumberBL";
                    tError.functionName = "dealLZCardPrintInfo";
                    tError.errorMessage = "单证未绑定险种!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            int nLen = (int) schemaLMCertifyDes.getCertifyLength();

            if (schemaLMCertifyDes.getHaveNumber().equals("N"))
            { // 如果是无号单证
                if (Integer.parseInt(this.mCardAmount) < 1)
                {
                 // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ECCertifyNumberBL";
                    tError.functionName = "dealLZCardPrintInfo";
                    tError.errorMessage = "对于无号单证，请输入正确的单证数量!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                // 自动产生起始号和终止号
                String strSQL = "select case when to_number(MAX(EndNo))+1 is null then 1 else to_number(MAX(EndNo)) + 1 end from LZCardPrint WHERE CertifyCode = '" +
                                this.mCertifyCode + "'";

                String strStartNo = new ExeSQL().getOneValue(strSQL);
                strStartNo = CertifyFunc.bigIntegerPlus(strStartNo, "0", nLen);

                strEndNo = CertifyFunc.bigIntegerPlus(strStartNo,
                        String.valueOf(Integer.parseInt(this.mCardAmount) - 1),
                        nLen);
            }
            else
            { // 如果是有号单证
                if (this.mCardSerNo.length() != nLen)
                {
                 // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ECCertifyNumberBL";
                    tError.functionName = "dealLZCardPrintInfo";
                    tError.errorMessage = "单证号的长度应该为" + String.valueOf(nLen);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                strEndNo = CertifyFunc.bigIntegerPlus(mCardSerNo,
                        String.valueOf(Integer.parseInt(this.mCardAmount) - 1),
                        nLen);
                String strSql2 = "SELECT * FROM LZCardPrint" +
                                " WHERE (( StartNo <= '" +
                                this.mCardSerNo +
                                "' AND EndNo >= '" +
                                strEndNo + "' ) " +
                                " OR ( StartNo <= '" +
                                strEndNo +
                                "' AND EndNo >= '" +
                                strEndNo + "' )) " +
                                " AND CertifyCode = '" +
                                this.mCertifyCode + "'";

                System.out.println(strSql2);

                if (new LZCardPrintDB().executeQuery(strSql2).size() > 0)
                {
                 // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ECCertifyNumberBL";
                    tError.functionName = "dealLZCardPrintInfo";
                    tError.errorMessage = "输入的单证与已印刷的单证重复了";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
    	}catch(Exception e){
    		// @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberBL";
            tError.functionName = "dealLZCardPrintInfo";
            tError.errorMessage = "定单入库异常!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	
    	this.mLZCardPrint.setPrtNo(mPrtNo);
    	mLZCardPrint.setCertifyCode(this.mCertifyCode);
    	mLZCardPrint.setRiskCode(schemaLMCertifyDes.getRiskCode());
    	mLZCardPrint.setRiskVersion(schemaLMCertifyDes.getRiskVersion());
    	mLZCardPrint.setSubCode(this.mCardType);
		System.out.println(mLZCardPrint.getSubCode());
		mLZCardPrint.setMaxMoney(this.mCertifyPrice);
		//mLZCardPrint.setMaxDate(request.getParameter("MaxDate"));
		//mLZCardPrint.setComCode(request.getParameter("ComCode"));
		mLZCardPrint.setPhone("");
		mLZCardPrint.setLinkMan("");
		mLZCardPrint.setCertifyPrice(mCertifyPrice);
		mLZCardPrint.setManageCom(mGI.ManageCom);
		mLZCardPrint.setOperatorInput(mGI.Operator);
		String inputDateSQL="select date('"+CurrentDate+"')-7 day from dual";
		String inputDate = new ExeSQL().getOneValue(inputDateSQL);
		mLZCardPrint.setInputDate(inputDate);
		mLZCardPrint.setInputMakeDate(CurrentDate);
		mLZCardPrint.setGetMan(mGI.Operator);
		mLZCardPrint.setGetDate(CurrentDate);
		mLZCardPrint.setOperatorGet(mGI.Operator);
		mLZCardPrint.setStartNo(this.mCardSerNo);
		mLZCardPrint.setEndNo(this.strEndNo);
		mLZCardPrint.setGetMakeDate(CurrentDate);
		mLZCardPrint.setSumCount(this.mCardAmount);
		mLZCardPrint.setState("1");
		mLZCardPrint.setModifyDate(CurrentDate);
		mLZCardPrint.setModifyTime(this.CurrentTime);
		map.put(mLZCardPrint, "INSERT");
    	
    	return true;
    }
    
    private boolean dealLZCardInfo(){
//    	int cardLength=7;
    	LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setSubCode(mCardType);
        LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.query();
        if(tLMCertifyDesSet.size() != 1)
        {
            this.mErrors.addOneError("查询单证描述失败！");
            return false;
        }
        LMCertifyDesSchema tLMCertifyDesSchema = tLMCertifyDesSet.get(1);
    	int cardLength = (int) tLMCertifyDesSchema.getCertifyLength();
    	int intStartNo = Integer.parseInt(this.mCardSerNo);
    	 // 产生回收清算单号
        String mszTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO",PubFun.getNoLimit(this.mGI.ComCode));
    	try{
    		
    		
    		for(int i=intStartNo;i<=Integer.parseInt(this.strEndNo);i++){
    			schemaLZCard=new LZCardSchema( );
    			String strCurSerNo = String.valueOf(i);
    	        strCurSerNo = PubFun.LCh(strCurSerNo, "0", cardLength); 
    	        
    			schemaLZCard.setCertifyCode(this.mCertifyCode);
        		schemaLZCard.setSubCode(this.mCardType);
        		schemaLZCard.setRiskCode("0");
        		schemaLZCard.setRiskVersion("0");
        		schemaLZCard.setStartNo(strCurSerNo);
        		schemaLZCard.setEndNo(strCurSerNo);
        		if (this.mGI.ComCode.substring(0,2).equals("86"))
				{
        			schemaLZCard.setSendOutCom("A"+mGI.ComCode);
				}else
			  	{
			  		schemaLZCard.setSendOutCom("B"+mGI.ComCode);
			  	}
        	    schemaLZCard.setReceiveCom("D"+mAgentCode);
        	    schemaLZCard.setState("10");
        		schemaLZCard.setSumCount(1);
        		schemaLZCard.setPrem("");
        	    schemaLZCard.setAmnt("");
        	    schemaLZCard.setHandler(mGI.Operator);
        	    schemaLZCard.setHandleDate(CurrentDate);
        	    schemaLZCard.setInvaliDate(CurrentDate);
        		schemaLZCard.setTakeBackNo(mszTakeBackNo);
        		schemaLZCard.setSaleChnl("");        		
        		schemaLZCard.setStateFlag("0");//??       		
        		schemaLZCard.setOperateFlag("0");
        		schemaLZCard.setPayFlag("");
        		schemaLZCard.setEnterAccFlag("");
        		schemaLZCard.setReason("");  
        		schemaLZCard.setOperator(mGI.Operator);
        		schemaLZCard.setMakeDate(CurrentDate);
        		schemaLZCard.setMakeTime(this.CurrentTime);
        		schemaLZCard.setModifyDate(CurrentDate);
        		schemaLZCard.setModifyTime(CurrentTime);
        		this.mLZCardSet.add(schemaLZCard);
        		
        		//lzcardtrack
        		mLZCardTrackSchema=new LZCardTrackSchema();
        		this.mLZCardTrackSchema.setCertifyCode(this.mCertifyCode);
        		this.mLZCardTrackSchema.setSubCode(this.mCardType);
        		this.mLZCardTrackSchema.setRiskCode("0");
        		this.mLZCardTrackSchema.setRiskVersion("0");
        		mLZCardTrackSchema.setStartNo(strCurSerNo);
        		mLZCardTrackSchema.setEndNo(strCurSerNo);
        		if (this.mGI.ComCode.substring(0,2).equals("86"))
				{
        			mLZCardTrackSchema.setSendOutCom("A"+mGI.ComCode);
				}else
			  	{
					mLZCardTrackSchema.setSendOutCom("B"+mGI.ComCode);
			  	}
        		mLZCardTrackSchema.setReceiveCom("D"+mAgentCode);
        		mLZCardTrackSchema.setPrem("");
        		mLZCardTrackSchema.setAmnt("");
        		mLZCardTrackSchema.setHandler(mGI.Operator);
        		mLZCardTrackSchema.setHandleDate(CurrentDate);
        		mLZCardTrackSchema.setInvaliDate(CurrentDate);
        		mLZCardTrackSchema.setTakeBackNo(mszTakeBackNo);
        		mLZCardTrackSchema.setSaleChnl("");        		
        		mLZCardTrackSchema.setStateFlag("0");//??       		
        		mLZCardTrackSchema.setOperateFlag("0");
        		mLZCardTrackSchema.setPayFlag("");
        		mLZCardTrackSchema.setEnterAccFlag("");
        		mLZCardTrackSchema.setReason("");  
        		mLZCardTrackSchema.setOperator(mGI.Operator);
        		mLZCardTrackSchema.setMakeDate(CurrentDate);
        		mLZCardTrackSchema.setMakeTime(this.CurrentTime);
        		mLZCardTrackSchema.setModifyDate(CurrentDate);
        		mLZCardTrackSchema.setModifyTime(CurrentTime);
        		this.mLZCardTrackSet.add(mLZCardTrackSchema);
    		}
    		map.put(mLZCardSet, "INSERT");  
    		map.put(mLZCardTrackSet, "INSERT");  
    	}catch(Exception e)
    	{
    		// @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberBL";
            tError.functionName = "dealLZCardInfo";
            tError.errorMessage = "定单入库lzcard异常!";
            this.mErrors.addOneError(tError);
    		e.printStackTrace();
    		return false;
    	}	    
		return true;
    }
    
    private boolean save(){
    	
    	try{
    		PubSubmit p = new PubSubmit();
    		VData v= new VData();
    		v.add(map);
    		if(!p.submitData(v, null)){
    			// @@错误处理
                CError tError = new CError();
                tError.moduleName = "ECCertifyNumberBL";
                tError.functionName = "save";
                tError.errorMessage = "数据提交异常!";
                this.mErrors.addOneError(tError);
        		return false;
    		}
    	}catch(Exception e){
    		// @@错误处理
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberBL";
            tError.functionName = "save";
            tError.errorMessage = "单证下发异常!";
            this.mErrors.addOneError(tError);
    		e.printStackTrace();
    		return false;
    	}
    	return true;
    }
    //原理:
  //∑(a[i]*W[i]) mod 11 ( i = 2, 3, ..., 18 )(1)
  //"*" 表示乘号
  //i--------表示身份证号码每一位的序号，从右至左，最左侧为18，最右侧为1。
  //a[i]-----表示身份证号码第 i 位上的号码
  //W[i]-----表示第 i 位上的权值 W[i] = 2^(i-1) mod 11
  //计算公式 (1) 令结果为 R
  //根据下表找出 R 对应的校验码即为要求身份证号码的校验码C。
  //R 0 1 2 3 4 5 6 7 8 9 10
  //C 1 0 X 9 8 7 6 5 4 3 2
  // X 就是 10，罗马数字中的 10 就是 X
  //15位转18位中,计算校验位即最后一位

      private String GetVerifyBit(String id) {
          String result = "";

          int multiply[] = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
          int nNum = 0;
          for(int i = 0; i < multiply.length; i++)
          {
              int num = 0;
              if("0123456789".indexOf(id.substring(i, i + 1)) != -1)
              {
                  num = Integer.parseInt(id.substring(i, i + 1));
              }
              else
              {
                  num = 0;
              }
              nNum += num * multiply[i];
          }
          nNum = nNum % 11;
          switch (nNum) {
          case 0:
              result = "20"; //20
              break;
          case 1:
              result = "51"; //51
              break;
          case 2:
              result = "42"; //42
              break;
          case 3:
              result = "83"; //83
              break;
          case 4:
              result = "34"; //34
              break;
          case 5:
              result = "65"; //65
              break;
          case 6:
              result = "76"; //76
              break;
          case 7:
              result = "97"; //97
              break;
          case 8:
              result = "08"; //08
              break;
          case 9:
              result = "59"; //59
              break;
          case 10:
              result = "10"; //10
              break;

          }
          return result;
      }

      /**
       * 生成随即密码
       * @param pwd_len 生成的密码的总长度
       * @return  密码的字符串
       */
      public static String genRandomNum(int pwd_len) {
      	
      	System.out.println("进入新算法");
          //35是因为数组是从0开始的，26个字母+10个数字
//          final int maxNum = 100;
          int i; //生成的随机数
          int count = 0; //生成的密码的长度

          char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

          StringBuffer pwd = new StringBuffer("");
//          Random r = new Random();
          while (count < pwd_len) {
              //生成随机数，取绝对值，防止生成负数，

//              i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
  //由于随机数产生重复太多,重写算法并注释掉line 481 488 492
          	i = (int)(Math.random()*10);

          	
              if (i >= 0 && i < str.length) {
                  pwd.append(str[i]);
                  count++;
              }
          }

          return pwd.toString();
      }
}
