/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
/*
 * <p>ClassName: CardPlanBLS </p>
 * <p>Description: CardPlanBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2003-10-23
 */
package com.sinosoft.lis.certify;

import java.sql.Connection;

import com.sinosoft.lis.db.LZCardPlanDB;
import com.sinosoft.lis.schema.LZCardPlanSchema;
import com.sinosoft.lis.vdb.LZCardPlanDBSet;
import com.sinosoft.lis.vschema.LZCardPlanSet;
import com.sinosoft.utility.*;

public class CardPlanBLS
{

    public CErrors mErrors = new CErrors();
    private String strOperate;
    private LZCardPlanSchema mLZCardPlanSchema = new LZCardPlanSchema();
    private LZCardPlanSet mLZCardPlanSet = new LZCardPlanSet();

    public CardPlanBLS()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean bReturn = false;
        //将操作数据拷贝到本类中
        strOperate = verifyOperate(cOperate);

        if (strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }

        //
        // begin to save data
        //
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            buildError("submitData", "连接数据库失败");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);

            if (cOperate.equals("INSERT||MAIN"))
            {
                bReturn = insert(mLZCardPlanSchema, mLZCardPlanSet, conn);
            }
            else if (cOperate.equals("UPDATE||MAIN"))
            {
                bReturn = update(mLZCardPlanSchema, mLZCardPlanSet, conn);
            }
            else if (cOperate.equals("UPDATE||RET"))
            {
                bReturn = savePlanList(mLZCardPlanSet, conn);
            }
            else if (cOperate.equals("DELETE||MAIN"))
            {
                bReturn = delete(mLZCardPlanSchema, mLZCardPlanSet, conn);
            }
            else if (cOperate.equals("PACK||MAIN"))
            {
                bReturn = update(mLZCardPlanSchema, mLZCardPlanSet, conn);
            }
            else
            {
                buildError("submitData", "不支持的操作字符串");
                bReturn = false;
            }

            if (bReturn)
            {
                conn.commit();
            }
            else
            {
                conn.rollback();
            }

            conn.close();
            conn = null;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return bReturn;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPlanBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT||MAIN", "UPDATE||MAIN",
                              "UPDATE||RET", "DELETE||MAIN", "PACK||MAIN"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mLZCardPlanSchema.setSchema((LZCardPlanSchema) cInputData.
                                             getObjectByObjectName(
                    "LZCardPlanSchema", 0));
//      this.mOldLZCardPlanSet.set((LZCardPlanSet)cInputData.getObjectByObjectName("LZCardPlanSet",0));
            this.mLZCardPlanSet.set((LZCardPlanSet) cInputData.
                                    getObjectByObjectName("LZCardPlanSet", 0));
//      mLZCardPlanSet.set((LZCardPlanSet)cInputData.get(3));

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getInputData", "发生异常");
            return false;
        }
        return true;
    }

    private boolean insert(LZCardPlanSchema aLZCardPlanSchema,
                           LZCardPlanSet aLZCardPlanSet,
                           Connection conn)
    {
        LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB(conn);
        tLZCardPlanDB.setSchema(aLZCardPlanSchema);
        if (!tLZCardPlanDB.insert())
        {
            mErrors.copyAllErrors(tLZCardPlanDB.mErrors);
            return false;
        }

        if (!savePlanList(aLZCardPlanSet, conn))
        {
            return false;
        }

        return true;
    }

    private boolean update(LZCardPlanSchema aLZCardPlanSchema,
                           LZCardPlanSet aLZCardPlanSet,
                           Connection conn)
    {
        LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB(conn);
        tLZCardPlanDB.setSchema(aLZCardPlanSchema);
        if (!tLZCardPlanDB.update())
        {
            mErrors.copyAllErrors(tLZCardPlanDB.mErrors);
            return false;
        }

        if (!savePlanList(aLZCardPlanSet,
                          conn))
        {
            return false;
        }

        return true;
    }

    private boolean delete(LZCardPlanSchema aLZCardPlanSchema,
                           LZCardPlanSet aLZCardPlanSet,
                           Connection conn)
    {
        LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB(conn);
        tLZCardPlanDB.setSchema(aLZCardPlanSchema);
        if (!tLZCardPlanDB.delete())
        {
            mErrors.copyAllErrors(tLZCardPlanDB.mErrors);
            return false;
        }

        if (!savePlanList(aLZCardPlanSet, conn))
        {
            return false;
        }

        return true;
    }

    private boolean savePlanList(LZCardPlanSet aLZCardPlanSet,
                                 Connection conn)
    {
        LZCardPlanDBSet tLZCardPlanDBSet = new LZCardPlanDBSet(conn);
        tLZCardPlanDBSet.set(aLZCardPlanSet);
        if (!tLZCardPlanDBSet.update())
        {
            mErrors.copyAllErrors(tLZCardPlanDBSet.mErrors);
            return false;
        }

        return true;
    }
}
