/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZCardPayDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LZCardPaySchema;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.vschema.LZCardPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.vschema.LZCardTrackSet;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.schema.LZCardSchema;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CertifyPayBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误容器
    private GlobalInput mGI = new GlobalInput(); //对象，完整的登陆用户信息

    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData(); //返回结果
    private LZCardPaySet mLZCardPaySet = new LZCardPaySet(); //单证描述表
    private LZCardPaySet mNewLZCardPaySet = new LZCardPaySet(); //单证描述表
    private LZCardSet mLZCardSet = new LZCardSet(); //单证状态表
    private LZCardTrackSet mLZCardTrackSet = new LZCardTrackSet(); //单证轨迹表
    private LZCardSet mNewLZCardSet = new LZCardSet(); //单证状态表
    private LZCardTrackSet mNewLZCardTrackSet = new LZCardTrackSet(); //单证轨迹表
    private String mOperate; //操作类型
    private String mOperateType = ""; //操作类型
    private String mCertifyCode = ""; //单证编码
    public CertifyPayBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:ZCardTr
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        System.out.println("---BL BEGIN---");
        mInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData)) {
            return false;
        }
        this.mOperate = mOperateType;
        // 数据操作业务处理
        if (cOperate.equals("INSERT")) {
            if (!checkData()) {
                return false;
            }
            if (!dealData()) {
                return false;
            }

            System.out.println("---dealData---");
        }
        if (cOperate.equals("QUERY")) {
            if (!queryData()) {
                return false;
            }
        }

        if (cOperate.equals("DELETE")) {
            if (!getDelData()) {
                return false;
            }
            if (!dealData()) {
                return false;
            }
        }
        if (cOperate.equals("UPDATE")) {
            if (!checkData()) {
                return false;
            }
            if (!updateQuery()) {
                return false;
            }
            if (!dealData()) {
                return false;
            }
        }
        if (cOperate.equals("AFFIRM")) {

            if (!dealData()) {
                return false;
            }
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        mInputData.clear();
        mInputData.add(mOperateType);
        LZCardPaySet setLZCardPay = new LZCardPaySet();
        if (mOperateType.equals("INSERT") || mOperateType.equals("UPDATE") ||
            mOperateType.equals("DELETE")) {
            for (int i = 1; i <= mLZCardPaySet.size(); i++) {

                LZCardPaySchema aLZCardPaySchema = new LZCardPaySchema();
                aLZCardPaySchema = mLZCardPaySet.get(i);
                //int intStartNo = Integer.parseInt(aLZCardPaySchema.getStartNo().trim());
                //int intEndNo = Integer.parseInt(aLZCardPaySchema.getEndNo());
                LZCardPaySchema bLZCardPaySchema = new LZCardPaySchema();
                //String strStartNo = String.valueOf(intStartNo);
                //strStartNo = PubFun.LCh(strStartNo, "0", 7);
                String strStartNo = aLZCardPaySchema.getStartNo();
                bLZCardPaySchema.setStartNo(strStartNo);
                //String strEndNo = String.valueOf(intEndNo);
                //strEndNo = PubFun.LCh(strEndNo, "0", 7);
                String strEndNo = aLZCardPaySchema.getEndNo();
                System.out.println("StartNo : " + strStartNo + "EndNo : " + strEndNo);
                bLZCardPaySchema.setEndNo(strEndNo);
                bLZCardPaySchema.setPayNo(aLZCardPaySchema.getPayNo());
                bLZCardPaySchema.setCardType(aLZCardPaySchema.getCardType());
                bLZCardPaySchema.setManageCom(aLZCardPaySchema.getManageCom());
                bLZCardPaySchema.setActPayMoney(aLZCardPaySchema.
                                                getActPayMoney());
                bLZCardPaySchema.setDuePayMoney(aLZCardPaySchema.
                                                getDuePayMoney());
                bLZCardPaySchema.setHandlerCode(aLZCardPaySchema.
                                                getHandlerCode());
                bLZCardPaySchema.setHandler(aLZCardPaySchema.getHandler());
                bLZCardPaySchema.setAgentCom(aLZCardPaySchema.getAgentCom());
                bLZCardPaySchema.setAgentComName(aLZCardPaySchema.
                                                 getAgentComName());
                bLZCardPaySchema.setState("0");
                bLZCardPaySchema.setSumCount(aLZCardPaySchema.getSumCount());
                bLZCardPaySchema.setOperator(aLZCardPaySchema.getOperator());
                bLZCardPaySchema.setHandleDate(PubFun.getCurrentDate());
                bLZCardPaySchema.setMakeDate(PubFun.getCurrentDate());
                bLZCardPaySchema.setMakeTime(PubFun.getCurrentTime());
                bLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
                bLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());
                setLZCardPay.add(bLZCardPaySchema);

                StringBuffer sb = new StringBuffer();
                /* sb.append(" select * from LZCardPay where CardType ='")
                         .append(aLZCardPaySchema.getCardType())
                         .append("' and  StartNo='")
                         .append(strStartNo)
                         .append("' and  EndNo='")
                         .append(strEndNo)
                         .append("' with ur")
                         ;
                 LZCardPayDB dbLZCardPay = new LZCardPayDB();
                 LZCardPaySet setNewLZCardPay = new LZCardPaySet();
                 setNewLZCardPay = dbLZCardPay.executeQuery(sb.toString());
                 mNewLZCardPaySet.add(setNewLZCardPay);*/
                LZCardDB tLZCardDB = new LZCardDB();
                sb = new StringBuffer();
                String strDelState = "";
                if (mOperateType.equals("DELETE")) {
                    strDelState = "'12'";
                } else if (mOperateType.equals("INSERT")) {
                    strDelState = "'10','11'";
                } else if (mOperateType.equals("UPDATE")) {
                    strDelState = "'10','11','12'";
                }
                sb.append(" select * from LZCard where subcode ='")
                        .append(aLZCardPaySchema.getCardType())
                        .append("' and  StartNo>='")
                        .append(strStartNo)
                        .append("' and  StartNo<='")
                        .append(strEndNo)
                        .append("' and state in (")
                        .append(strDelState)
                        .append(")")
                        ;
                String strSql = sb.toString();
                System.out.println(strSql);
                LZCardSet tLZCardSet = tLZCardDB.executeQuery(strSql);
                if (tLZCardSet == null || tLZCardSet.size() <= 0) {
                    String str = "查询卡单信息失败!";
                    buildError("dealData", str);
                    System.out.println(
                            "在程序CertifyPayBL.dealData() - 167 : " +
                            str);
                    return false;
                }
                if (tLZCardSet.size() != aLZCardPaySchema.getSumCount()) {
                    String str = "卡单已领用数和录入数不同!";
                    buildError("dealData", str);
                    System.out.println(
                            "在程序CertifyPayBL.dealData() - 175 : " +
                            str);
                    return false;
                }

                for (int m = 1; m <= tLZCardSet.size(); m++) {

                    LZCardSchema tLZCardSchema = new LZCardSchema();
                    tLZCardSchema = tLZCardSet.get(m).getSchema();
                    String strReceive = "";
                    if (mOperateType.equals("INSERT") ||
                        mOperateType.equals("UPDATE")) {
                        tLZCardSet.get(m).setState("12"); //已录入
                    } else {
                        strReceive = tLZCardSchema.getReceiveCom().
                                     substring(0, 1);
                        if (strReceive.equals("D")) {
                            tLZCardSet.get(m).setState("10"); //已发放给代理人
                        } else if (strReceive.equals("E")) {
                            tLZCardSet.get(m).setState("11"); //已发放给代理机构
                        }
                    }

                    LZCardTrackSchema tLZCardTrackSchema = new
                            LZCardTrackSchema();
                    tLZCardTrackSchema.setCertifyCode(tLZCardSchema.
                            getCertifyCode());

                    tLZCardTrackSchema.setSubCode(tLZCardSchema.
                                                  getSubCode());

                    tLZCardTrackSchema.setRiskCode("0");
                    tLZCardTrackSchema.setRiskVersion("0");

                    tLZCardTrackSchema.setStartNo(tLZCardSchema.
                                                  getStartNo());
                    tLZCardTrackSchema.setEndNo(tLZCardSchema.getEndNo());
                    tLZCardTrackSchema.setSendOutCom(tLZCardSchema.
                            getSendOutCom());
                    tLZCardTrackSchema.setReceiveCom(tLZCardSchema.
                            getReceiveCom());
                    tLZCardTrackSchema.setSumCount(tLZCardSchema.
                            getSumCount());
                    tLZCardTrackSchema.setPrem("");
                    tLZCardTrackSchema.setAmnt("");
                    tLZCardTrackSchema.setHandler(tLZCardSchema.getHandler());
                    tLZCardTrackSchema.setHandleDate(tLZCardSchema.
                            getHandleDate());
                    tLZCardTrackSchema.setInvaliDate(tLZCardSchema.
                            getInvaliDate());
                    tLZCardTrackSchema.setTakeBackNo(tLZCardSchema.
                            getTakeBackNo());
                    tLZCardTrackSchema.setSaleChnl(tLZCardSchema.
                            getSaleChnl());
                    tLZCardTrackSchema.setStateFlag(tLZCardSchema.
                            getStateFlag());
                    tLZCardTrackSchema.setOperateFlag("0");
                    tLZCardTrackSchema.setPayFlag(tLZCardSchema.getPayFlag());
                    tLZCardTrackSchema.setEnterAccFlag("");
                    tLZCardTrackSchema.setReason("");
                    tLZCardTrackSchema.setState(tLZCardSet.get(m).getState());
                    tLZCardTrackSchema.setOperator(mGI.Operator);
                    tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
                    tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
                    tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
                    tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());

                    mLZCardTrackSet.add(tLZCardTrackSchema);
                }
                mLZCardSet.add(tLZCardSet);
            }

            System.out.println("setLZCardPay.get(1).getActPayMoney() : " +
                               setLZCardPay.get(1).getActPayMoney());
            if (!CertifyFunc.splitDivMoney(setLZCardPay,
                                           setLZCardPay.get(1).getActPayMoney())) {

                mErrors.addOneError("结算单录入时分费用错误,原因是总费用与拆分后的费用不符!");
                return false;
            }
            //处理原结算单中存在的但修改后不存在的卡单状态
            if( mOperateType.equals("UPDATE") && !UpdateModify()){
                mErrors.addOneError("查询卡单信息失败!");
                return false;
           }

            /* if (mOperateType.equals("DELETE") && mLZCardPaySet != null &&
                 mLZCardPaySet.size() > 0) {
                 LZCardPayDB dbLZCardPay = new LZCardPayDB();
                 dbLZCardPay.setPayNo(mLZCardPaySet.get(1).getPayNo());
                 setLZCardPay = dbLZCardPay.query();
             }*/
        } else if (mOperateType.equals("AFFIRM")) {

            double sumPay = 0;
            for (int i = 1; i <= mLZCardPaySet.size(); i++) {
                LZCardPaySchema aLZCardPaySchema = new LZCardPaySchema();
                aLZCardPaySchema = mLZCardPaySet.get(i);
                int intStartNo = Integer.parseInt(aLZCardPaySchema.getStartNo().
                                                  trim());
                int intEndNo = Integer.parseInt(aLZCardPaySchema.getEndNo());
                int length = aLZCardPaySchema.getStartNo().length();
                for (int j = intStartNo; j <= intEndNo; j++) {
                    String strCurSerNo = String.valueOf(j);
                    //strCurSerNo = PubFun.LCh(strCurSerNo, "0", 7);
                    strCurSerNo = PubFun.LCh(strCurSerNo, "0", length);
                    LZCardPaySchema bLZCardPaySchema = new LZCardPaySchema();
                    bLZCardPaySchema.setStartNo(strCurSerNo);
                    bLZCardPaySchema.setEndNo(strCurSerNo);
                    bLZCardPaySchema.setPayNo(aLZCardPaySchema.getPayNo());
                    bLZCardPaySchema.setCardType(aLZCardPaySchema.getCardType());
                    bLZCardPaySchema.setManageCom(aLZCardPaySchema.getManageCom());
                    bLZCardPaySchema.setActPayMoney(aLZCardPaySchema.
                            getActPayMoney());
                    bLZCardPaySchema.setDuePayMoney(aLZCardPaySchema.
                            getDuePayMoney());
                    bLZCardPaySchema.setHandlerCode(aLZCardPaySchema.
                            getHandlerCode());
                    bLZCardPaySchema.setHandler(aLZCardPaySchema.getHandler());
                    bLZCardPaySchema.setAgentCom(aLZCardPaySchema.getAgentCom());
                    bLZCardPaySchema.setAgentComName(aLZCardPaySchema.
                            getAgentComName());
                    bLZCardPaySchema.setState("1");
                    bLZCardPaySchema.setSumCount(1);
                    bLZCardPaySchema.setOperator(aLZCardPaySchema.getOperator());
                    bLZCardPaySchema.setHandleDate(PubFun.getCurrentDate());
                    bLZCardPaySchema.setMakeDate(aLZCardPaySchema.getMakeDate());
                    bLZCardPaySchema.setMakeTime(aLZCardPaySchema.getMakeTime());
                    bLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
                    bLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());
                    setLZCardPay.add(bLZCardPaySchema);
                }
                sumPay += aLZCardPaySchema.getActPayMoney();
            }
            if (!CertifyFunc.dealDivMoney(setLZCardPay, setLZCardPay.size(),
                                          sumPay)) {

                mErrors.addOneError("分费用错误,原因是总费用与拆分后的费用不符!");
                return false;
            }

        }
        mInputData.add(setLZCardPay);
        mInputData.add(mLZCardPaySet);
        mInputData.add(mLZCardSet);
        mInputData.add(mLZCardTrackSet);
        mInputData.add(mNewLZCardPaySet);
        mInputData.add(mNewLZCardSet);
        mInputData.add(mNewLZCardTrackSet);

        //数据提交
        CertifyPayBLS tCertifyPayBLS = new CertifyPayBLS();
        System.out.println("Start tCertifyPayBLS Submit...");
        if (!tCertifyPayBLS.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyPayBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "CertifyPayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /****************************************************************************
     * 接收从CertifyDescSave.jsp中传递的数据
     */

    private boolean getInputData(VData cInputData) {

        mOperateType = (String) cInputData.get(0); //接收操作类型
        System.out.println("所要执行的操作符号是" + mOperateType);
        mLZCardPaySet = ((LZCardPaySet) cInputData.getObjectByObjectName(
                "LZCardPaySet", 0));
        mGI = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGI == null || mLZCardPaySet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeMultiBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 取得要删除的数据，结算单号对应所有数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean getDelData() {

        LZCardPayDB tLZCardPayDB = new LZCardPayDB();
        tLZCardPayDB.setPayNo(mLZCardPaySet.get(1).getPayNo());
        mLZCardPaySet.clear();
        mLZCardPaySet = tLZCardPayDB.query();
        if (mLZCardPaySet == null) {
            this.mErrors.copyAllErrors(tLZCardPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "getDelData";
            tError.errorMessage = "单证查询失败！！！";
            this.mErrors.addOneError(tError);
            mLZCardPaySet.clear();
            return false;
        }

        System.out.println("在bl中的查询的个数是" + mLZCardPaySet.size());

        if (tLZCardPayDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLZCardPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "getDelData";
            tError.errorMessage = "单证查询失败！！！";
            this.mErrors.addOneError(tError);
            mLZCardPaySet.clear();
            return false;
        }
        if (mLZCardPaySet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "getDelData";
            tError.errorMessage = "没有该单证记录！！！";
            this.mErrors.addOneError(tError);
            mLZCardPaySet.clear();
            return false;
        }
        return true;
    }


    /**
     * 查询符合条件的暂交费信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryData() {
        LZCardPaySet tLZCardPaySet = new LZCardPaySet();

        LZCardPayDB tLZCardPayDB = new LZCardPayDB();

        tLZCardPayDB.setPayNo(mLZCardPaySet.get(1).getPayNo());
        //tLZCardPayDB.setCardType(mLZCardPaySet.get(1).getCardType());
        //tLZCardPayDB.setStartNo(mLZCardPaySet.get(1).getStartNo());
        //tLZCardPayDB.setEndNo(mLZCardPaySet.get(1).getEndNo());
        tLZCardPaySet = tLZCardPayDB.query();
        if (tLZCardPaySet == null) {
            this.mErrors.copyAllErrors(tLZCardPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "queryData";
            tError.errorMessage = "单证查询失败！！！";
            this.mErrors.addOneError(tError);
            tLZCardPaySet.clear();
            return false;
        }

        System.out.println("在bl中的查询的个数是" + tLZCardPaySet.size());

        if (tLZCardPayDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLZCardPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "queryData";
            tError.errorMessage = "单证查询失败！！！";
            this.mErrors.addOneError(tError);
            tLZCardPaySet.clear();
            return false;
        }
        if (tLZCardPaySet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyPayBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有该单证记录！！！";
            this.mErrors.addOneError(tError);
            tLZCardPaySet.clear();
            return false;
        }

        mResult.clear();
        mResult.add(tLZCardPaySet);
        return true;
    }

    /**
     * 校验传入的暂交费收据号是否合法
     * 输出：如果发生错误则返回false,否则返回true
     */
    //校验新增和修改时的数据，单证的号码不能长于18
    private boolean checkData() {
        if (mOperateType.equals("INSERT")) {
            LZCardPaySchema tLZCardPaySchema = new LZCardPaySchema();
            for (int i = 1; i <= mLZCardPaySet.size(); i++) {
                tLZCardPaySchema.setSchema(mLZCardPaySet.get(i));
                LZCardPayDB tLZCardPayDB = new LZCardPayDB();
                tLZCardPayDB.setCardType(tLZCardPaySchema.getCardType());
                String startNo = tLZCardPaySchema.getStartNo();
                String EndNo = tLZCardPaySchema.getEndNo();
                //startNo = PubFun.LCh(startNo, "0", 7);
                //EndNo = PubFun.LCh(EndNo, "0", 7);
                startNo = tLZCardPaySchema.getStartNo();
                EndNo = tLZCardPaySchema.getEndNo();

                tLZCardPayDB.setStartNo(startNo);
                tLZCardPayDB.setEndNo(EndNo);
                LZCardPaySet tLZCardPaySet = new LZCardPaySet();
                tLZCardPaySet.set(tLZCardPayDB.query());
                if (tLZCardPaySet.size() > 0) {
                    this.mErrors.copyAllErrors(tLZCardPaySchema.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CertifyPayBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = tLZCardPaySchema.getStartNo() + "--" +
                                          tLZCardPaySchema.getEndNo() +
                                          "号码已经存在，不能该进行操作！！！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!CertifyFunc.chkAgentCom(tLZCardPaySchema)) {
                    mErrors.addOneError("录入的和下发的代理机构不相等！");
                    return false;
                }
                if (!CertifyFunc.chkAgentComExist(tLZCardPaySchema)) {
                    mErrors.addOneError("未发放到代理机构！");
                    return false;
                }
            }

        }
        if (mOperateType.equals("UPDATE")) {
            for (int i = 1; i <= mLZCardPaySet.size(); i++) {
                LZCardPaySchema tLZCardPaySchema = new LZCardPaySchema();
                tLZCardPaySchema.setSchema(mLZCardPaySet.get(i));
                if (!CertifyFunc.chkAgentCom(tLZCardPaySchema)) {
                    mErrors.addOneError("在第" + i + "行，录入的和下发的代理机构不相等！");
                    return false;
                }
            }
        }
        double dMoney = mLZCardPaySet.get(1).getDuePayMoney();
        if (!CertifyFunc.chkRiskMoney(mLZCardPaySet, 1, dMoney)) {
            mErrors.addOneError("结算总金额与险种保费不同！");
            return false;
        }

        // 对一些特殊的字段进行赋值

        return true;
    }

    //将查询到的数据返回到初始的界面上
    private boolean returnData() {
        LZCardPaySchema tLZCardPaySchema = new LZCardPaySchema();
        tLZCardPaySchema.setSchema(mLZCardPaySet.get(1));
        LZCardPayDB tLZCardPayDB = new LZCardPayDB();
        tLZCardPayDB.setCertifyCode(tLZCardPaySchema.getCertifyCode());
        LZCardPaySet yLZCardPaySet = new LZCardPaySet();

        yLZCardPaySet.set(tLZCardPayDB.query());
        mResult.clear();
        mResult.add(yLZCardPaySet);

        return true;
    }

    //校验单证号码，修改时单证的号码不能修改。
    private boolean updateQuery() {
        return true;
    }

    public static void main(String[] args) {
        String tOperateType = "QUERY";
        String tCertifyCode = "04";
        String tCertifyCode_1 = "04";
        String tCertifyClass = "D";
        LZCardPaySchema yLZCardPaySchema = new LZCardPaySchema();
        LZCardPaySet yLZCardPaySet = new LZCardPaySet();
        LMCardRiskSet yLMCardRiskSet = new LMCardRiskSet();

        VData tVData = new VData();

        tVData.addElement(tOperateType);
        tVData.add("Y");
        tVData.add(yLZCardPaySet);

        CertifyDescUI tCertifyDescUI = new CertifyDescUI();
        tCertifyDescUI.submitData(tVData, "QUERY");
    }

    /**
     * Kevin 2003-03-24
     * 拆分单证（结算单录入）
     * aLZCardPaySchema : 单证记录。表示要发放的单证信息
     * vResult : 返回的结果集。其中第一个元素是要拆分的setLZCardPay的信息
     */
    protected static boolean splitPayInput(LZCardPaySchema aLZCardPaySchema,
                                           VData vResult) {

        System.out.println("正式拆分:========   单证类型: " +
                           aLZCardPaySchema.getCardType() + "  单证持有机构: " +
                           aLZCardPaySchema.getManageCom())
                ;
        System.out.println("起始号：" + aLZCardPaySchema.getStartNo() + "  终止号：" +
                           aLZCardPaySchema.getEndNo());

        vResult.clear();
        vResult.add(0, null);

        LZCardPaySet setLZCardPay = new LZCardPaySet();
        int intStartNo = Integer.parseInt(aLZCardPaySchema.getStartNo().trim());
        int intEndNo = Integer.parseInt(aLZCardPaySchema.getEndNo().trim());
        for (int i = intStartNo; i <= intEndNo; i++) {

            String strCurSerNo = String.valueOf(i);
            strCurSerNo = PubFun.LCh(strCurSerNo, "0", 7);
            aLZCardPaySchema.setStartNo(strCurSerNo);
            aLZCardPaySchema.setEndNo(strCurSerNo);
            aLZCardPaySchema.setMakeDate(PubFun.getCurrentDate());
            aLZCardPaySchema.setMakeTime(PubFun.getCurrentTime());
            aLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
            aLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());
            setLZCardPay.add(aLZCardPaySchema);
            vResult.set(0, setLZCardPay);
        }
        return true;
    }

    /**
     * Kevin 2003-03-24
     * 拆分单证（结算单录入）
     * aLZCardPaySchema : 单证记录。表示要发放的单证信息
     * vResult : 返回的结果集。其中第一个元素是要拆分的setLZCardPay的信息
     */
    protected boolean UpdateModify() {
        LZCardPaySet tLZCardPaySet = new LZCardPaySet();
        LZCardPayDB tLZCardPayDB = new LZCardPayDB();
        tLZCardPayDB.setPayNo(mLZCardPaySet.get(1).getPayNo());
        tLZCardPaySet = tLZCardPayDB.query();
        for (int i = 1; i <= tLZCardPaySet.size(); i++) {
            StringBuffer sb = new StringBuffer();

            LZCardPaySchema aLZCardPaySchema = new LZCardPaySchema();
            aLZCardPaySchema = tLZCardPaySet.get(i);
            String strStartNo = aLZCardPaySchema.getStartNo();
            String strEndNo = aLZCardPaySchema.getEndNo();
            String strDelState = "'12'";
            sb.append(" select * from LZCard where subcode ='")
                    .append(aLZCardPaySchema.getCardType())
                    .append("' and  StartNo>='")
                    .append(strStartNo)
                    .append("' and  StartNo<='")
                    .append(strEndNo)
                    .append("' and state in (")
                    .append(strDelState)
                    .append(")")
                    ;
            String strSql = sb.toString();
            System.out.println(strSql);
            LZCardDB tLZCardDB = new LZCardDB();
            LZCardSet tLZCardSet = tLZCardDB.executeQuery(strSql);
            if (tLZCardSet == null || tLZCardSet.size() <= 0) {
                String str = "查询卡单信息失败!";
                buildError("dealData", str);
                System.out.println(
                        "在程序CertifyPayBL.dealData() - 686 : " +
                        str);
                return false;
            }

            for (int m = 1; m <= tLZCardSet.size(); m++) {

                LZCardSchema tLZCardSchema = new LZCardSchema();
                tLZCardSchema = tLZCardSet.get(m).getSchema();
                String strReceive = "";

                strReceive = tLZCardSchema.getReceiveCom().
                             substring(0, 1);
                if (strReceive.equals("D")) {
                    tLZCardSet.get(m).setState("10"); //已发放给代理人
                } else if (strReceive.equals("E")) {
                    tLZCardSet.get(m).setState("11"); //已发放给代理机构
                }

                LZCardTrackSchema tLZCardTrackSchema = new
                        LZCardTrackSchema();
                tLZCardTrackSchema.setCertifyCode(tLZCardSchema.
                                                  getCertifyCode());

                tLZCardTrackSchema.setSubCode(tLZCardSchema.
                                              getSubCode());

                tLZCardTrackSchema.setRiskCode("0");
                tLZCardTrackSchema.setRiskVersion("0");

                tLZCardTrackSchema.setStartNo(tLZCardSchema.
                                              getStartNo());
                tLZCardTrackSchema.setEndNo(tLZCardSchema.getEndNo());
                tLZCardTrackSchema.setSendOutCom(tLZCardSchema.
                                                 getSendOutCom());
                tLZCardTrackSchema.setReceiveCom(tLZCardSchema.
                                                 getReceiveCom());
                tLZCardTrackSchema.setSumCount(tLZCardSchema.
                                               getSumCount());
                tLZCardTrackSchema.setPrem("");
                tLZCardTrackSchema.setAmnt("");
                tLZCardTrackSchema.setHandler(tLZCardSchema.getHandler());
                tLZCardTrackSchema.setHandleDate(tLZCardSchema.
                                                 getHandleDate());
                tLZCardTrackSchema.setInvaliDate(tLZCardSchema.
                                                 getInvaliDate());
                tLZCardTrackSchema.setTakeBackNo(tLZCardSchema.
                                                 getTakeBackNo());
                tLZCardTrackSchema.setSaleChnl(tLZCardSchema.
                                               getSaleChnl());
                tLZCardTrackSchema.setStateFlag(tLZCardSchema.
                                                getStateFlag());
                tLZCardTrackSchema.setOperateFlag("0");
                tLZCardTrackSchema.setPayFlag(tLZCardSchema.getPayFlag());
                tLZCardTrackSchema.setEnterAccFlag("");
                tLZCardTrackSchema.setReason("");
                tLZCardTrackSchema.setState(tLZCardSet.get(m).getState());
                tLZCardTrackSchema.setOperator(mGI.Operator);
                tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
                tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
                tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
                tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());
                mNewLZCardTrackSet.add(tLZCardTrackSchema);
            }
            mNewLZCardSet.add(tLZCardSet);
        }

        return true;
    }


}
