package com.sinosoft.lis.certify;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LZAccessSet;
import com.sinosoft.lis.schema.LZAccessSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006-04-14</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CertifySendConUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;


    //业务处理相关变量
    /** 全局数据 */

    public CertifySendConUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        CertifySendConBL tCertifySendConBL = new CertifySendConBL();

        if (!tCertifySendConBL.submitData(cInputData, cOperate))
        {
            if (tCertifySendConBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tCertifySendConBL.mErrors);
            }
            else
            {
                buildError("submitData", "tCardPlanBL发生错误，但是没有提供详细信息！");
            }
            return false;
        }
        mResult = tCertifySendConBL.getResult();
        return true;

    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ComCode = "8611";
        globalInput.Operator = "001";

        String sendOutCom[]= new String[2];
        sendOutCom[0]="86";
        sendOutCom[1]="8611";

        String receiveCom[]= new String[2];
        receiveCom[0]="aa";
        receiveCom[1]="bb";

        LZAccessSet setLZAccess = new LZAccessSet();
        int nIndex;
        for( nIndex = 0; nIndex < sendOutCom.length; nIndex ++ )
        {
          LZAccessSchema schemaLZAccess = new LZAccessSchema( );

          schemaLZAccess.setSendOutCom(sendOutCom[nIndex].trim());
          schemaLZAccess.setReceiveCom(receiveCom[nIndex].trim());
          setLZAccess.add(schemaLZAccess);
        }

        VData vData = new VData();
        String operateFlag = "INSERT||CONFIG";
        vData.addElement(globalInput);
        vData.addElement(setLZAccess);
        vData.addElement(operateFlag);


        CertifySendConUI tCert = new CertifySendConUI();
        tCert.submitData(vData,"INSERT||CONFIG");
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPlanUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
