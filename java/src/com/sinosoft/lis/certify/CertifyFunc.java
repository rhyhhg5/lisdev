/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.math.BigInteger;
import java.sql.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理公用模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */

public class CertifyFunc {
    //错误处理类，每个需要错误处理的类中都放置该类
    public static CErrors mErrors = new CErrors();
    public static VData mResult = new VData();
    public static final String INPUT_COM = "00"; // 该机构编码表示入库操作的机构
    public static final int FIRST_COM = 2; // 入库操作只能发放到一级机构，一级机构的长度是2
    public static final int LAST_COM = 9;

    public static final String CERTIFY_CLASS_CERTIFY = "P"; // 普通单证
    public static final String CERTIFY_CLASS_CARD = "D"; // 定额单证
    public static final String CERTIFY_CLASS_SYS = "S"; // 系统单证

    private static final String VALID_AGENT_STATE = " < '06' "; // 可用的代理人的状态

    public CertifyFunc() {
    }

    public static void main(String[] args) {
    }


    /**
     * Kevin 2003-03-24
     * 从单证印刷表中产生新的单证号
     * globalInput : 全局空间
     * aLZCardSchema : 单证记录。
     * aLZCardPrintSchema : 单证印刷记录，其中记录着要入库的单证印刷号。
     * strCertifyClass : 要入库的单证类型，常量值在CertifyFunc的成员变量中定义
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     * 第六个元素是要更新的LZCardPrint的信息（对单证入库操作的支持）。
     */
    protected static boolean inputCertify(GlobalInput globalInput,
                                          LZCardSchema aLZCardSchema,
                                          LZCardPrintSchema aLZCardPrintSchema,
                                          String strCertifyClass, VData vResult) {
        System.out.println("In CertifyFunc->inputCertify()");
        String szSql = "";

        vResult.clear();

        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);
        vResult.add(5, null);

        mErrors.clearErrors();

        szSql = "SELECT * FROM LZCardPrint WHERE PrtNo = '";
        szSql += aLZCardPrintSchema.getPrtNo();
        szSql += "' AND CertifyCode = '";
        szSql += aLZCardSchema.getCertifyCode();
        szSql += "' AND State = '1'";
        //szSql += " AND ManageCom = '" + globalInput.ComCode + "'"; //

        System.out.println(szSql);

        LZCardPrintDB dbLZCardPrint = new LZCardPrintDB();
        LZCardPrintSet setLZCP = dbLZCardPrint.executeQuery(szSql);
        LZCardPrintSchema schemaLZCardPrint = null;

        if (setLZCP.size() != 1) {
            buildError("inputCertify", "没有该印刷号的单证或者还未提单或者当前操作员不能操作此种单证");
            return false;
        } else {
            schemaLZCardPrint = setLZCP.get(1);
        }

        // 更新单证印刷表中的数据
        dbLZCardPrint.setSchema(schemaLZCardPrint);
        dbLZCardPrint.setState("2");

        vResult.set(5, dbLZCardPrint.getSchema());

        // 检查单证状态表，看是否已经存在这样的数据
        szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
        szSql += schemaLZCardPrint.getCertifyCode();
        szSql += "' AND StartNo <= '" + schemaLZCardPrint.getStartNo() +
                "' AND EndNo >= '" + schemaLZCardPrint.getStartNo();
        szSql += "' AND StartNo <= '" + schemaLZCardPrint.getEndNo() +
                "' AND EndNo >= '" + schemaLZCardPrint.getEndNo() + "'";

        System.out.println(szSql);

        LZCardDB dbLZCard = new LZCardDB();
        if (dbLZCard.executeQuery(szSql).size() >= 1) {
            buildError("inputCertify", "在单证状态表中已经存在要入库的单证");
            return false;
        }

        // 得到单证的信息
        szSql = "SELECT * FROM LMCertifyDes WHERE CertifyCode = '";
        szSql += schemaLZCardPrint.getCertifyCode() + "'";

        System.out.println(szSql);

        LMCertifyDesDB dbLMCertifyDes = new LMCertifyDesDB();
        LMCertifyDesSet setLMCertifyDes = dbLMCertifyDes.executeQuery(szSql);

        if (setLMCertifyDes.size() == 0) {
            buildError("inputCertify", "输入的单证编码有误");
            return false;
        }

        LMCertifyDesSchema schemaLMCertifyDes = setLMCertifyDes.get(1);

        // 新增单证状态记录
        LZCardSchema tLZCardSchema = new LZCardSchema();

        tLZCardSchema.setCertifyCode(schemaLZCardPrint.getCertifyCode());
        if (schemaLZCardPrint.getSubCode() == null ||
            schemaLZCardPrint.getSubCode().equals("")) {
            tLZCardSchema.setSubCode("0");
        } else {
            tLZCardSchema.setSubCode(schemaLZCardPrint.getSubCode());
        }
        tLZCardSchema.setRiskCode(schemaLMCertifyDes.getRiskCode());
        tLZCardSchema.setRiskVersion(schemaLMCertifyDes.getRiskVersion());

        tLZCardSchema.setStartNo(schemaLZCardPrint.getStartNo());
        tLZCardSchema.setEndNo(schemaLZCardPrint.getEndNo());
        tLZCardSchema.setSendOutCom(INPUT_COM);
        tLZCardSchema.setReceiveCom(aLZCardSchema.getReceiveCom());
        tLZCardSchema.setSumCount(
                (int) CertifyFunc.bigIntegerDiff(schemaLZCardPrint.getEndNo(),
                                                 schemaLZCardPrint.getStartNo()) +
                1);
        tLZCardSchema.setHandler(aLZCardSchema.getHandler());
        tLZCardSchema.setHandleDate(aLZCardSchema.getHandleDate());
        tLZCardSchema.setInvaliDate(aLZCardSchema.getInvaliDate());
        tLZCardSchema.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        tLZCardSchema.setSaleChnl(aLZCardSchema.getSaleChnl());
        tLZCardSchema.setStateFlag(aLZCardSchema.getStateFlag());
        tLZCardSchema.setOperateFlag(aLZCardSchema.getOperateFlag());
        tLZCardSchema.setPayFlag(aLZCardSchema.getPayFlag());
        tLZCardSchema.setEnterAccFlag(aLZCardSchema.getEnterAccFlag());
        tLZCardSchema.setReason(aLZCardSchema.getReason());
        tLZCardSchema.setState(aLZCardSchema.getState());
        tLZCardSchema.setOperator(globalInput.Operator);
        tLZCardSchema.setMakeDate(PubFun.getCurrentDate());
        tLZCardSchema.setMakeTime(PubFun.getCurrentTime());
        tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
        tLZCardSchema.setModifyTime(PubFun.getCurrentTime());

        vResult.set(3, tLZCardSchema);

        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();
        tLZCardTrackSchema.setCertifyCode(schemaLZCardPrint.getCertifyCode());
        if (schemaLZCardPrint.getSubCode() == null ||
            schemaLZCardPrint.getSubCode().equals("")) {
            tLZCardTrackSchema.setSubCode("0");
        } else {
            tLZCardTrackSchema.setSubCode(schemaLZCardPrint.getSubCode());
        }
        tLZCardTrackSchema.setRiskCode(schemaLMCertifyDes.getRiskCode());
        tLZCardTrackSchema.setRiskVersion(schemaLMCertifyDes.getRiskVersion());

        tLZCardTrackSchema.setStartNo(schemaLZCardPrint.getStartNo());
        tLZCardTrackSchema.setEndNo(schemaLZCardPrint.getEndNo());
        tLZCardTrackSchema.setSendOutCom(INPUT_COM);
        tLZCardTrackSchema.setReceiveCom(aLZCardSchema.getReceiveCom());
        tLZCardTrackSchema.setSumCount((int) CertifyFunc.bigIntegerDiff(
                tLZCardTrackSchema.getEndNo(), tLZCardTrackSchema.getStartNo()) +
                                       1);
        tLZCardTrackSchema.setPrem("");
        tLZCardTrackSchema.setAmnt("");
        tLZCardTrackSchema.setHandler(aLZCardSchema.getHandler());
        tLZCardTrackSchema.setHandleDate(aLZCardSchema.getHandleDate());
        tLZCardTrackSchema.setInvaliDate(aLZCardSchema.getInvaliDate());
        tLZCardTrackSchema.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        tLZCardTrackSchema.setSaleChnl(aLZCardSchema.getSaleChnl());
        tLZCardTrackSchema.setStateFlag(aLZCardSchema.getStateFlag());
        tLZCardTrackSchema.setOperateFlag("0");
        tLZCardTrackSchema.setPayFlag(aLZCardSchema.getPayFlag());
        tLZCardTrackSchema.setEnterAccFlag("");
        tLZCardTrackSchema.setReason("");
        tLZCardTrackSchema.setState(aLZCardSchema.getState());
        tLZCardTrackSchema.setOperator(globalInput.Operator);
        tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
        tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
        tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
        tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());

        vResult.set(4, tLZCardTrackSchema);

        return true;
    }

    /**
     * Kevin 2003-03-24
     * 拆分单证（发放）
     * golbalInput : 全局空间
     * aLZCardSchema : 单证记录。表示要发放的单证信息
     * bLimitFlag : 增领标志。true表示要增领，则不校验业务员手中的最大单证数量；否则，需要校验。
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     * 第六个元素是要更新的LZCardPrint的信息（对单证入库操作的支持）。
     */
    protected static boolean splitCertifySendOut(GlobalInput globalInput,
                                                 LZCardSchema aLZCardSchema,
                                                 boolean bLimitFlag,
                                                 VData vResult) {
        System.out.println("正式拆分:========   单证类型: " +
                           aLZCardSchema.getCertifyCode() + "  单证持有机构: " +
                           aLZCardSchema.getReceiveCom() + "  单证发放机构: " +
                           aLZCardSchema.getSendOutCom());
        System.out.println("起始号：" + aLZCardSchema.getStartNo() + "  终止号：" +
                           aLZCardSchema.getEndNo());
        String sFlag = aLZCardSchema.getPayFlag();
        int sumCnt = aLZCardSchema.getSumCount();
        String szSql = "";
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        String szSendOutCom = aLZCardSchema.getSendOutCom();

        vResult.clear();

        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);
        vResult.add(5, null);

        mErrors.clearErrors();

        if (bLimitFlag == true && !verifyMaxCount(aLZCardSchema)) {
            return false;
        }

        String strStartNo = aLZCardSchema.getStartNo();
        String strEndNo = aLZCardSchema.getEndNo();
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return false;
        }
        String conttr = " and StateFlag in ('0','7','8','9')";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            conttr = " and StateFlag in ( '0','3','7','8')";
        }
        szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
        szSql += szCertifyCode;
        szSql += "' " + conttr + " AND ReceiveCom = '";
        szSql += szSendOutCom;
        szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                strStartNo;
        szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet set = dbLZCard.executeQuery(szSql);
        if (set.size() == 1) { // 输入的单证号在可用的单证号内
            dbLZCard.setSchema(set.get(1)); // 直接使用查询出来的那条数据
            String strOldStartNo = dbLZCard.getStartNo();
            String strOldEndNo = dbLZCard.getEndNo();
            int nNoLen = getCertifyNoLen(dbLZCard.getCertifyCode());

            // 从LZCard表中删除原有单证
            vResult.set(0, dbLZCard.getSchema());

            // 插入第一个新单证到LZCard表中
            if (CertifyFunc.bigIntegerDiff(strStartNo, strOldStartNo) > 0) {
                dbLZCard.setStartNo(strOldStartNo);
                dbLZCard.setEndNo(bigIntegerPlus(strStartNo, "-1", nNoLen));
                dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(
                        strStartNo, strOldStartNo));
                dbLZCard.setModifyDate(PubFun.getCurrentDate());
                dbLZCard.setModifyTime(PubFun.getCurrentTime());

                vResult.set(1, dbLZCard.getSchema());
            }

            // 插入第二个新单证到LZCard表中
            if (CertifyFunc.bigIntegerDiff(strOldEndNo, strEndNo) > 0) {
                dbLZCard.setStartNo(bigIntegerPlus(strEndNo, "1", nNoLen));
                dbLZCard.setEndNo(strOldEndNo);
                dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(
                        strOldEndNo, strEndNo));

                vResult.set(2, dbLZCard.getSchema());
            }
        } else {
            buildError("splitCertify", "校验单证号失败，输入的单证号不在可用单证号的范围内!");
            return false;
        }

        // 保存新的单证
        dbLZCard.setCertifyCode(aLZCardSchema.getCertifyCode());
        if (aLZCardSchema.getSubCode() == null ||
            aLZCardSchema.getSubCode().equals("")) {
            dbLZCard.setSubCode("0");
        } else {
            dbLZCard.setSubCode(aLZCardSchema.getSubCode());
        }
        dbLZCard.setStartNo(aLZCardSchema.getStartNo());
        dbLZCard.setEndNo(aLZCardSchema.getEndNo());
        dbLZCard.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCard.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(dbLZCard.getEndNo(),
                dbLZCard.getStartNo()) + 1);
        dbLZCard.setHandler(aLZCardSchema.getHandler());
        dbLZCard.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCard.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCard.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        dbLZCard.setSaleChnl(aLZCardSchema.getSaleChnl());
        dbLZCard.setStateFlag(getCardState(aLZCardSchema.getReceiveCom()));
        dbLZCard.setOperateFlag("0");
        dbLZCard.setPayFlag("");
        dbLZCard.setEnterAccFlag("");
        dbLZCard.setReason("");
        dbLZCard.setState(aLZCardSchema.getState());
        dbLZCard.setOperator(globalInput.Operator);
        dbLZCard.setMakeDate(PubFun.getCurrentDate());
        dbLZCard.setMakeTime(PubFun.getCurrentTime());
        dbLZCard.setModifyDate(PubFun.getCurrentDate());
        dbLZCard.setModifyTime(PubFun.getCurrentTime());
        dbLZCard.setAuthorizeNo(aLZCardSchema.getAuthorizeNo());

        vResult.set(3, dbLZCard.getSchema());

        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();

        tLZCardTrackSchema.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (dbLZCard.getSubCode() == null || dbLZCard.equals("")) {
            tLZCardTrackSchema.setSubCode("0");
        } else {
            tLZCardTrackSchema.setSubCode(dbLZCard.getSubCode());
        }
        if (dbLZCard.getRiskCode() == null || dbLZCard.getRiskCode().equals("")) {
            tLZCardTrackSchema.setRiskCode("0");
        } else {
            tLZCardTrackSchema.setRiskCode(dbLZCard.getRiskCode());
        }
        tLZCardTrackSchema.setRiskVersion(dbLZCard.getRiskVersion());
        tLZCardTrackSchema.setStartNo(aLZCardSchema.getStartNo());
        tLZCardTrackSchema.setEndNo(aLZCardSchema.getEndNo());
        tLZCardTrackSchema.setSendOutCom(aLZCardSchema.getSendOutCom());
        tLZCardTrackSchema.setReceiveCom(aLZCardSchema.getReceiveCom());
        tLZCardTrackSchema.setSumCount((int) CertifyFunc.bigIntegerDiff(
                tLZCardTrackSchema.getEndNo(), tLZCardTrackSchema.getStartNo()) +
                                       1);
        tLZCardTrackSchema.setPrem("");
        tLZCardTrackSchema.setAmnt(aLZCardSchema.getAmnt());
        tLZCardTrackSchema.setHandler(aLZCardSchema.getHandler());
        tLZCardTrackSchema.setHandleDate(aLZCardSchema.getHandleDate());
        tLZCardTrackSchema.setInvaliDate(aLZCardSchema.getInvaliDate());
        tLZCardTrackSchema.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        tLZCardTrackSchema.setSaleChnl(aLZCardSchema.getSaleChnl());
        tLZCardTrackSchema.setStateFlag(dbLZCard.getStateFlag());
        tLZCardTrackSchema.setOperateFlag("0");
        tLZCardTrackSchema.setPayFlag("");
        tLZCardTrackSchema.setEnterAccFlag("");
        tLZCardTrackSchema.setReason("");
        tLZCardTrackSchema.setState(aLZCardSchema.getState());
        tLZCardTrackSchema.setOperator(globalInput.Operator);
        tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
        tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
        tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
        tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());

        int nMaxCount = getMaxCount(szReceiveCom.substring(1),
                                    szCertifyCode);
        System.out.println(" MaxCount(): " + nMaxCount);


        if (sFlag.equals("1") && aLZCardSchema.getAuthorizeNo() != null &&
            !aLZCardSchema.getAuthorizeNo().equals("") && nMaxCount != -1) {

            String contt = "  StateFlag in ('0','7','8','9')";
            if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
                contt = " State in ('10','11')";
            }

            // 得到代理人现有单证数量
            String strSQL =
                    "SELECT SUM(SUMCOUNT) FROM LZCard WHERE " + contt
                    + " AND CertifyCode = '" + szCertifyCode + "'"
                    + " AND ReceiveCom = '" + szReceiveCom + "'";
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(strSQL);
            //查询代理人现有单证数量，如果出错，则跳出
            if (exeSQL.mErrors.needDealError()) {
                mErrors.copyAllErrors(exeSQL.mErrors);
                return false;
            }
            //代理人现有单证数量
            int nCurCount = Integer.parseInt(ssrs.GetText(1, 1));
            int tOverCnt = nCurCount + sumCnt - nMaxCount;
            tLZCardTrackSchema.setMaxCount(nMaxCount);
            tLZCardTrackSchema.setOverCount(tOverCnt);
        } else {
            tLZCardTrackSchema.setMaxCount(0);
            tLZCardTrackSchema.setOverCount(0);
        }
        tLZCardTrackSchema.setAuthorizeNo(aLZCardSchema.getAuthorizeNo());
        vResult.set(4, tLZCardTrackSchema);

        return true;
    }

    /**
     * Kevin 2002-09-23
     * 校验输入的发放机构和接收机构
     */
    protected static boolean verifyComs(GlobalInput globalInput,
                                        String szSendOutCom,
                                        String szReceiveCom) {
        mErrors.clearErrors();
        System.out.println("in verifyComs()");
        if (!isComsExist(szSendOutCom, szReceiveCom)) { //校验机构，管理员，业务员是否存在
            return false;
        }
        System.out.println("in verifyComs(): szSendOutCom: " + szSendOutCom +
                           " szReceiveCom:" + szReceiveCom);
        if (szSendOutCom.charAt(0) == 'A') { // 发放机构是区站
            if (!szSendOutCom.equals('A' + globalInput.ComCode)) {
                buildError("verifyComs", "发放机构与管理员的登录机构不符");
                return false;
            }

            if (szReceiveCom.charAt(0) == 'A' || szReceiveCom.charAt(0) == 'B') { // 接收机构是区站或者是管理员
                if (!verifyAccess(szSendOutCom, szReceiveCom)) {
                    return false;
                }

            } else if (szReceiveCom.charAt(0) == 'C') {
                ; // do nothing

            } else if (szReceiveCom.charAt(0) == 'D') { // 接收机构是代理人

                if (!verifyAgent(szSendOutCom, szReceiveCom)) {
                    return false;
                }
            } else {
                buildError("verifyComs", "接收机构的格式非法!");
                return false;
            }

        } else if (szSendOutCom.charAt(0) == 'B') { // 发放机构是管理员
            if (!szSendOutCom.equals('B' + globalInput.Operator)) {
                buildError("verifyComs", "发放机构与当前操作员不符");
                return false;
            }

            if (szReceiveCom.charAt(0) == 'A' || szReceiveCom.charAt(0) == 'B') { // 接收机构是区站或者是管理员
                if (!verifyAccess(szSendOutCom, szReceiveCom)) {
                    return false;
                }

            } else if (szReceiveCom.charAt(0) == 'C') {
                ; // do nothing

            }

            else if (szReceiveCom.charAt(0) == 'D') { // 接收机构是代理人
                //if (!verifyAgent(globalInput.ComCode, szReceiveCom))return false; // 取当前的登录机构做为发放机构

            } else if (szReceiveCom.charAt(0) == 'E') { // 接收机构是代理机构
                //if (!verifyAgent(globalInput.ComCode, szReceiveCom))return false; // 取当前的登录机构做为发放机构

            } else {
                buildError("verifyComs", szSendOutCom + "接收机构的格式非法!");
                return false;
            }
        } else if (szSendOutCom.equals(INPUT_COM)) { // 表示是入库操作
            if (!szReceiveCom.equals("A" + globalInput.ComCode)) {
                buildError("verifyComs", "接收机构与管理员的登录机构不符");
                return false;
            }

            // 2003-08-27
            // 现在，分公司也可以直接印刷单证，所以去掉这个控制。
            //if( szReceiveCom.length( ) != FIRST_COM + 1 ) {  // 接收机构不是一级机构
            //    buildError("verifyComs", "接收机构不是一级机构");
            //    return false;
            //}
        } else {
            buildError("verifyComs", "校验失败!发放机构格式非法!");
            return false;
        }

        return true;
    }


    /**
     * Kevin 2003-03-18
     * 拆分单证（回收）
     * 对于从业务员处的回收操作，允许单证的拆分操作；否则，必须整批回收。
     * golbalInput : 全局空间
     * aLZCardSchema : 单证记录。表示要回收的单证信息。
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     */
    protected static boolean splitCertifyTakeBack(GlobalInput globalInput,
                                                  LZCardSchema aLZCardSchema,
                                                  VData vResult) {
        // 清空返回结果集
        vResult.clear();

        // 初始化返回结果集
        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);

        String szSql = "";
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        String szSendOutCom = aLZCardSchema.getSendOutCom();

        String strStartNo = aLZCardSchema.getStartNo();
        String strEndNo = aLZCardSchema.getEndNo();

        // 单证轨迹校验(单证回收必须按照单证发放的轨迹原路返回)
        szSql = "SELECT a.* FROM LZCardTrack a" +
                " WHERE a.ModifyDate = " +
                "   ( SELECT MAX(b.ModifyDate) FROM LZCardTrack b" +
                "     WHERE b.CertifyCode = '" + szCertifyCode + "'" +
                "     AND b.OperateFlag IN ('0', '3')" +
                "     AND b.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND b.SendOutCom = '" + szReceiveCom + "'" +
                "     AND b.StartNo <= '" + strStartNo + "' AND b.EndNo >= '" +
                strStartNo + "'" +
                "     AND b.StartNo <= '" + strEndNo + "' AND b.EndNo >= '" +
                strEndNo + "')" +
                " AND a.ModifyTime = " +
                "   ( SELECT MAX(c.ModifyTime) FROM LZCardTrack c" +
                "     WHERE c.ModifyDate = a.ModifyDate" +
                "     AND c.OperateFlag IN ('0', '3')" +
                "     AND c.CertifyCode = '" + szCertifyCode + "'" +
                "     AND c.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND c.SendOutCom = '" + szReceiveCom + "'" +
                "     AND c.StartNo <= '" + strStartNo + "' AND c.EndNo >= '" +
                strStartNo + "'" +
                "     AND c.StartNo <= '" + strEndNo + "' AND c.EndNo >= '" +
                strEndNo + "')" +
                " AND a.CertifyCode = '" + szCertifyCode + "'" +
                " AND a.OperateFlag IN ('0', '3')" +
                " AND a.ReceiveCom = '" + szSendOutCom + "'" +
                " AND a.SendOutCom = '" + szReceiveCom + "'" +
                " AND a.StartNo <= '" + strStartNo + "' AND a.EndNo >= '" +
                strStartNo + "'" +
                " AND a.StartNo <= '" + strEndNo + "' AND a.EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardTrackDB dbLZCardTrack = new LZCardTrackDB();
        if (dbLZCardTrack.executeQuery(szSql).size() != 1) {
            buildError("splitCertify",
                       "校验单证轨迹失败，单证回收必须按单证发放的轨迹返回!或者是输入的单证号不在可用单证的范围内!");
            return false;
        }

        // 单证状态校验
        if (szSendOutCom.charAt(0) == 'D') { // 如果是从业务员处回收单证
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag in ('0','7','8') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                    strStartNo;
            szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                    strEndNo + "'";
        } else if (szSendOutCom.equals(szReceiveCom)) { // 如果是从业务员处回收单证
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag in ('0','7','8') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                    strStartNo;
            szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                    strEndNo + "'";
        }

        else {
            // 如果不是从业务员处回收单证，要求整批回收
            // 注意状态的判断
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag not in ('0','7','8') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo = '" + strStartNo + "' AND EndNo = '" +
                    strEndNo + "'";
        }

        System.out.println(szSql);

        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet set = dbLZCard.executeQuery(szSql);
        if (set.size() != 1) { // 输入的单证号在可用的单证号内
            buildError("splitCertify", "校验单证状态失败，输入的单证号不在可用单证号的范围内!");
            return false;
        }

        /* * * * * * * * * * * * * * * *
         * 开始拆分单证
         * * * * * * * * * * * * * * * */
        dbLZCard.setSchema(set.get(1)); // 直接使用查询出来的那条数据
        String strOldStartNo = dbLZCard.getStartNo();
        String strOldEndNo = dbLZCard.getEndNo();
        int nNoLen = getCertifyNoLen(dbLZCard.getCertifyCode());

        // 记录下原来单证的状态，如果是“0”，表示从业务员手中回收；否则，表示机构间回收。
        String strOldStateFlag = dbLZCard.getStateFlag();

        // 从LZCard表中删除原有单证，将要删除的数据放入返回结果集中
        vResult.set(0, dbLZCard.getSchema());

        // 插入第一个新单证到LZCard表中
        if (strStartNo.compareTo(strOldStartNo) > 0) {
            dbLZCard.setStartNo(strOldStartNo);
            dbLZCard.setEndNo(bigIntegerPlus(strStartNo, "-1", nNoLen));
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strStartNo,
                    strOldStartNo));
            dbLZCard.setModifyDate(PubFun.getCurrentDate());
            dbLZCard.setModifyTime(PubFun.getCurrentTime());

            vResult.set(1, dbLZCard.getSchema());
        }

        // 插入第二个新单证到LZCard表中
        if (strOldEndNo.compareTo(strEndNo) > 0) {
            dbLZCard.setStartNo(bigIntegerPlus(strEndNo, "1", nNoLen));
            dbLZCard.setEndNo(strOldEndNo);
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strOldEndNo,
                    strEndNo));

            vResult.set(2, dbLZCard.getSchema());
        }

        /* * * * * * * * * * * * * * * *
         * 拆分单证结束
         * * * * * * * * * * * * * * * */

        // 保存新的单证
        dbLZCard.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCard.setStartNo(aLZCardSchema.getStartNo());
        dbLZCard.setEndNo(aLZCardSchema.getEndNo());
        dbLZCard.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCard.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(dbLZCard.getEndNo(),
                dbLZCard.getStartNo()) + 1);
        dbLZCard.setHandler(aLZCardSchema.getHandler());
        dbLZCard.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCard.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCard.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        dbLZCard.setSaleChnl("");

        if (strOldStateFlag.equals("0")) { // 如果是从业务员手中回收，可以置回收状态
            dbLZCard.setStateFlag(aLZCardSchema.getStateFlag());
        } else { // 如果是机构间的回收，直接使用从业务员手中回收时设置的状态
            dbLZCard.setStateFlag(strOldStateFlag);
        }

        dbLZCard.setOperateFlag("1");
        dbLZCard.setPayFlag("");
        dbLZCard.setEnterAccFlag("");
        dbLZCard.setReason("");
        dbLZCard.setState("");
        dbLZCard.setOperator(globalInput.Operator);
        dbLZCard.setMakeDate(PubFun.getCurrentDate());
        dbLZCard.setMakeTime(PubFun.getCurrentTime());
        dbLZCard.setModifyDate(PubFun.getCurrentDate());
        dbLZCard.setModifyTime(PubFun.getCurrentTime());
        if (aLZCardSchema.getSubCode() == null ||
            aLZCardSchema.getSubCode().equals("")) {
            dbLZCard.setSubCode("0");
        } else {
            dbLZCard.setSubCode(aLZCardSchema.getSubCode());
        }
        vResult.set(3, dbLZCard.getSchema());

        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();

        dbLZCardTrack.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCardTrack.setSubCode(dbLZCard.getSubCode());
        dbLZCardTrack.setRiskCode(dbLZCard.getRiskCode());
        dbLZCardTrack.setRiskVersion(dbLZCard.getRiskVersion());
        dbLZCardTrack.setStartNo(aLZCardSchema.getStartNo());
        dbLZCardTrack.setEndNo(aLZCardSchema.getEndNo());
        dbLZCardTrack.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCardTrack.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCardTrack.setSumCount((int) CertifyFunc.bigIntegerDiff(
                dbLZCardTrack.getEndNo(), dbLZCardTrack.getStartNo()) + 1);
        dbLZCardTrack.setPrem("");
        dbLZCardTrack.setAmnt(aLZCardSchema.getAmnt());
        dbLZCardTrack.setHandler(aLZCardSchema.getHandler());
        dbLZCardTrack.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCardTrack.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCardTrack.setTakeBackNo(dbLZCard.getTakeBackNo());
        dbLZCardTrack.setSaleChnl(aLZCardSchema.getSaleChnl());
        dbLZCardTrack.setStateFlag(dbLZCard.getStateFlag());
        dbLZCardTrack.setOperateFlag("1");
        dbLZCardTrack.setPayFlag("");
        dbLZCardTrack.setEnterAccFlag("");
        dbLZCardTrack.setReason("");
        dbLZCardTrack.setState("1");
        dbLZCardTrack.setOperator(globalInput.Operator);
        dbLZCardTrack.setMakeDate(PubFun.getCurrentDate());
        dbLZCardTrack.setMakeTime(PubFun.getCurrentTime());
        dbLZCardTrack.setModifyDate(PubFun.getCurrentDate());
        dbLZCardTrack.setModifyTime(PubFun.getCurrentTime());

        vResult.set(4, dbLZCardTrack.getSchema());
        return true;
    }

    /**
     * Kevin 2003-03-18
     * 拆分单证（核销）
     * 对于从业务员处的回收操作，允许单证的拆分操作；否则，必须整批回收。
     * golbalInput : 全局空间
     * aLZCardSchema : 单证记录。表示要回收的单证信息。
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     */
    protected static boolean splitCertifyAuditCancel(GlobalInput globalInput,
            LZCardSchema aLZCardSchema,
            VData vResult) {
        // 清空返回结果集
        vResult.clear();

        // 初始化返回结果集
        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);

        String szSql = "";
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        String szSendOutCom = aLZCardSchema.getSendOutCom();
        String szState = aLZCardSchema.getState();

        String strStartNo = aLZCardSchema.getStartNo();
        String strEndNo = aLZCardSchema.getEndNo();
        System.out.println("正式拆分:========   单证类型: " +
                           aLZCardSchema.getCertifyCode() + "  单证持有机构: " +
                           aLZCardSchema.getReceiveCom() + "  单证发放机构: " +
                           aLZCardSchema.getSendOutCom());
        System.out.println("起始号：" + aLZCardSchema.getStartNo() + "  终止号：" +
                           aLZCardSchema.getEndNo());

        // 保存新的单证
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return false;
        }

        // 单证状态校验
        if (szSendOutCom.charAt(0) == 'D' || szSendOutCom.charAt(0) == 'E') { // 如果是从业务员处回收单证
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag in ('0','7','8','9') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                    strStartNo;
            szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                    strEndNo + "'";
        } else if (szSendOutCom.equals(szReceiveCom)) { // 如果是从业务员处回收单证
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag in ('0','7','8','9') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                    strStartNo;
            szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                    strEndNo + "'";
        } else {
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
            szSql += szCertifyCode;
            szSql += "' AND StateFlag in ('0','7','8','9') AND ReceiveCom = '";
            szSql += szSendOutCom;
            szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                    strStartNo;
            szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                    strEndNo + "'";

        }

        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            String stateSql;//状态校验脚本。当前状态不为遗失、损毁、作废状态
            if(szState.equals("3"))
            {
                //空白回销：下级机构
                if(globalInput.ComCode.length() <= 4)
                    stateSql = "and ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = '" + szReceiveCom + "') ";
                else
                    stateSql = "and SendOutCom = '" + szReceiveCom + "' and State in ('10','11') ";
            } else if (szState.equals("5")) {
                //销毁
                if(globalInput.ComCode.length() <= 4)
                    stateSql = "and (ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = '"
                               + szReceiveCom + "') or ReceiveCom='" + szReceiveCom + "') ";
                else
                    stateSql = "and SendOutCom = '" + szReceiveCom + "' and State in ('10','11') ";
            } else {
                //其它回销：本级机构
                if(globalInput.ComCode.length() <= 4)
                    stateSql = "and ReceiveCom = '" + szReceiveCom + "' ";
                else
                    stateSql = "and SendOutCom = '" + szReceiveCom + "' and State in ('10','11') ";
            }

            //修改定额单证回销的校验：空白回销只能回销下级机构的；其它回销只能回销本级机构的
            szSql = "SELECT * FROM LZCard WHERE CertifyCode = '" + szCertifyCode + "' "
                + stateSql
                + "and State in ('0', '3', '8', '9', '10', '11') "
                + "and StartNo <= '" + strStartNo + "' and EndNo >= '" + strStartNo + "' "
                + "and StartNo <= '" + strEndNo + "' and EndNo >= '" + strEndNo + "'";
        }

        System.out.println("***********" + szSql + "**********");
        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet set = dbLZCard.executeQuery(szSql);
        if (set.size() != 1) { // 输入的单证号在可用的单证号内
            AuditCancelBL.ErrorState = "6";
            buildError("splitCertify", "校验单证状态失败，输入的单证号不在可用单证号的范围内!");
            return false;
        }

        /* * * * * * * * * * * * * * * *
         * 开始拆分单证
         * * * * * * * * * * * * * * * */
        dbLZCard.setSchema(set.get(1)); // 直接使用查询出来的那条数据
        String strOldStartNo = dbLZCard.getStartNo();
        String strOldEndNo = dbLZCard.getEndNo();
        int nNoLen = getCertifyNoLen(dbLZCard.getCertifyCode());//长度

        // 从LZCard表中删除原有单证，将要删除的数据放入返回结果集中
        vResult.set(0, dbLZCard.getSchema());

        // 插入第一个新单证到LZCard表中
        if (strStartNo.compareTo(strOldStartNo) > 0) {
            dbLZCard.setStartNo(strOldStartNo);
            dbLZCard.setEndNo(bigIntegerPlus(strStartNo, "-1", nNoLen));
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strStartNo,
                    strOldStartNo));
            dbLZCard.setModifyDate(PubFun.getCurrentDate());
            dbLZCard.setModifyTime(PubFun.getCurrentTime());

            vResult.set(1, dbLZCard.getSchema());
        }

        // 插入第二个新单证到LZCard表中
        if (strOldEndNo.compareTo(strEndNo) > 0) {
            dbLZCard.setStartNo(bigIntegerPlus(strEndNo, "1", nNoLen));
            dbLZCard.setEndNo(strOldEndNo);
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strOldEndNo,
                    strEndNo));

            vResult.set(2, dbLZCard.getSchema());
        }

        dbLZCard.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCard.setStartNo(aLZCardSchema.getStartNo());
        dbLZCard.setEndNo(aLZCardSchema.getEndNo());

        dbLZCard.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCard.setReceiveCom("SYS");
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            if (globalInput.ComCode.length() == 2) {
                dbLZCard.setSendOutCom("00");
                dbLZCard.setReceiveCom("A" + globalInput.ComCode);
            } else if (globalInput.ComCode.length() == 4) {
                dbLZCard.setSendOutCom("A" + globalInput.ComCode.substring(0, 2));
                dbLZCard.setReceiveCom("A" + globalInput.ComCode);
            } else {
                dbLZCard.setSendOutCom("A" + globalInput.ComCode.substring(0, 4));
                dbLZCard.setReceiveCom("B" + globalInput.Operator);
            }

        }
        dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(dbLZCard.getEndNo(),
                dbLZCard.getStartNo()) + 1);
        dbLZCard.setHandler(aLZCardSchema.getHandler());
        dbLZCard.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCard.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCard.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        dbLZCard.setSaleChnl("");

        dbLZCard.setStateFlag(aLZCardSchema.getStateFlag()); //置为核销状态

        dbLZCard.setOperateFlag("5");
        dbLZCard.setPayFlag("");
        dbLZCard.setEnterAccFlag("");
        dbLZCard.setReason("");
        dbLZCard.setState(aLZCardSchema.getState());
        dbLZCard.setOperator(globalInput.Operator);
        dbLZCard.setMakeDate(PubFun.getCurrentDate());
        dbLZCard.setMakeTime(PubFun.getCurrentTime());
        dbLZCard.setModifyDate(PubFun.getCurrentDate());
        dbLZCard.setModifyTime(PubFun.getCurrentTime());
        if (aLZCardSchema.getSubCode() == null ||
            aLZCardSchema.getSubCode().equals("")) {
            dbLZCard.setSubCode("0");
        } else {
            dbLZCard.setSubCode(aLZCardSchema.getSubCode());
        }

        vResult.set(3, dbLZCard.getSchema());

        LZCardTrackDB dbLZCardTrack = new LZCardTrackDB();
        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();

        dbLZCardTrack.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCardTrack.setSubCode(dbLZCard.getSubCode());
        dbLZCardTrack.setRiskCode(dbLZCard.getRiskCode());
        dbLZCardTrack.setRiskVersion(dbLZCard.getRiskVersion());
        dbLZCardTrack.setStartNo(aLZCardSchema.getStartNo());
        dbLZCardTrack.setEndNo(aLZCardSchema.getEndNo());
        dbLZCardTrack.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCardTrack.setReceiveCom(aLZCardSchema.getReceiveCom());
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            if (globalInput.ComCode.length() == 2) {
                dbLZCardTrack.setSendOutCom("00");
                dbLZCardTrack.setReceiveCom("A" + globalInput.ComCode);
            } else if (globalInput.ComCode.length() == 4) {
                dbLZCardTrack.setSendOutCom("A" + globalInput.ComCode.substring(0, 2));
                dbLZCardTrack.setReceiveCom("A" + globalInput.ComCode);
            } else {
                dbLZCardTrack.setSendOutCom("A" + globalInput.ComCode.substring(0, 4));
                dbLZCardTrack.setReceiveCom("B" + globalInput.Operator);
            }
        }

        dbLZCardTrack.setSumCount((int) CertifyFunc.bigIntegerDiff(
                dbLZCardTrack.getEndNo(), dbLZCardTrack.getStartNo()) + 1);
        dbLZCardTrack.setPrem("");
        dbLZCardTrack.setAmnt(aLZCardSchema.getAmnt());
        dbLZCardTrack.setHandler(aLZCardSchema.getHandler());
        dbLZCardTrack.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCardTrack.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCardTrack.setTakeBackNo(dbLZCard.getTakeBackNo());
        dbLZCardTrack.setSaleChnl(aLZCardSchema.getSaleChnl());
        dbLZCardTrack.setStateFlag(dbLZCard.getStateFlag());
        dbLZCardTrack.setOperateFlag("1");
        dbLZCardTrack.setPayFlag("");
        dbLZCardTrack.setEnterAccFlag("");
        dbLZCardTrack.setReason("");
        dbLZCardTrack.setState(dbLZCard.getState());
        dbLZCardTrack.setOperator(globalInput.Operator);
        dbLZCardTrack.setMakeDate(PubFun.getCurrentDate());
        dbLZCardTrack.setMakeTime(PubFun.getCurrentTime());
        dbLZCardTrack.setModifyDate(PubFun.getCurrentDate());
        dbLZCardTrack.setModifyTime(PubFun.getCurrentTime());

        vResult.set(4, dbLZCardTrack.getSchema());
        return true;
    }


    /**
     * Kevin 2003-04-14
     * 拆分单证（反发放）
     * 程序逻辑和单证回收的类似
     * golbalInput : 全局空间
     * aLZCardSchema : 单证记录。表示要回收的单证信息。
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     */
    protected static boolean splitCertReveSendOut(GlobalInput globalInput,
                                                  LZCardSchema aLZCardSchema,
                                                  VData vResult) {
        // 清空返回结果集
        vResult.clear();
        //清空静态错误
        mErrors.clearErrors();

        // 初始化返回结果集
        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);

        String szSql = "";
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        String szSendOutCom = aLZCardSchema.getSendOutCom();

        String strStartNo = aLZCardSchema.getStartNo();
        String strEndNo = aLZCardSchema.getEndNo();

        // 单证轨迹校验(单证反发放必须按照单证发放的轨迹原路返回)
        szSql = "SELECT a.* FROM LZCardTrack a" +
                " WHERE a.ModifyDate = " +
                "   ( SELECT MAX(b.ModifyDate) FROM LZCardTrack b" +
                "     WHERE b.CertifyCode = '" + szCertifyCode + "'" +
                "     AND b.OperateFlag = '0'" +
                "     AND b.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND b.SendOutCom = '" + szReceiveCom + "'" +
                "     AND b.StartNo <= '" + strStartNo + "' AND b.EndNo >= '" +
                strStartNo + "'" +
                "     AND b.StartNo <= '" + strEndNo + "' AND b.EndNo >= '" +
                strEndNo + "')" +
                " AND a.ModifyTime = " +
                "   ( SELECT MAX(c.ModifyTime) FROM LZCardTrack c" +
                "     WHERE c.ModifyDate = a.ModifyDate" +
                "     AND c.OperateFlag = '0'" +
                "     AND c.CertifyCode = '" + szCertifyCode + "'" +
                "     AND c.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND c.SendOutCom = '" + szReceiveCom + "'" +
                "     AND c.StartNo <= '" + strStartNo + "' AND c.EndNo >= '" +
                strStartNo + "'" +
                "     AND c.StartNo <= '" + strEndNo + "' AND c.EndNo >= '" +
                strEndNo + "')" +
                " AND a.CertifyCode = '" + szCertifyCode + "'" +
                " AND a.OperateFlag = '0'" +
                " AND a.ReceiveCom = '" + szSendOutCom + "'" +
                " AND a.SendOutCom = '" + szReceiveCom + "'" +
                " AND a.StartNo <= '" + strStartNo + "' AND a.EndNo >= '" +
                strStartNo + "'" +
                " AND a.StartNo <= '" + strEndNo + "' AND a.EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardTrackDB dbLZCardTrack = new LZCardTrackDB();
        if (dbLZCardTrack.executeQuery(szSql).size() != 1) {
            buildError("splitCertReveSendOut",
                       "校验单证轨迹失败，单证反发放必须按单证发放的轨迹返回!或者是输入的单证号不在可用单证的范围内!");
            return false;
        }

        // 单证状态校验
        // 单证处于发放状态，并且号码处于可用的范围。
        szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
        szSql += szCertifyCode;
        szSql += "' AND StateFlag in ('0','7','8') AND ReceiveCom = '";
        szSql += szSendOutCom;
        szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                strStartNo;
        szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet set = dbLZCard.executeQuery(szSql);
        if (set.size() != 1) { // 输入的单证号在可用的单证号内
            buildError("splitCertReveSendOut", "校验单证状态失败，输入的单证号不在可用单证号的范围内!");
            return false;
        }

        /* * * * * * * * * * * * * * * *
         * 开始拆分单证
         * * * * * * * * * * * * * * * */
        dbLZCard.setSchema(set.get(1)); // 直接使用查询出来的那条数据
        String strOldStartNo = dbLZCard.getStartNo();
        String strOldEndNo = dbLZCard.getEndNo();
        int nNoLen = getCertifyNoLen(dbLZCard.getCertifyCode());

        // 从LZCard表中删除原有单证，将要删除的数据放入返回结果集中
        vResult.set(0, dbLZCard.getSchema());

        // 插入第一个新单证到LZCard表中
        if (strStartNo.compareTo(strOldStartNo) > 0) {
            dbLZCard.setStartNo(strOldStartNo);
            dbLZCard.setEndNo(bigIntegerPlus(strStartNo, "-1", nNoLen));
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strStartNo,
                    strOldStartNo));
            dbLZCard.setModifyDate(PubFun.getCurrentDate());
            dbLZCard.setModifyTime(PubFun.getCurrentTime());

            vResult.set(1, dbLZCard.getSchema());
        }

        // 插入第二个新单证到LZCard表中
        if (strOldEndNo.compareTo(strEndNo) > 0) {
            dbLZCard.setStartNo(bigIntegerPlus(strEndNo, "1", nNoLen));
            dbLZCard.setEndNo(strOldEndNo);
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strOldEndNo,
                    strEndNo));

            vResult.set(2, dbLZCard.getSchema());
        }

        /* * * * * * * * * * * * * * * *
         * 拆分单证结束
         * * * * * * * * * * * * * * * */

        // 保存新的单证
        dbLZCard.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCard.setStartNo(aLZCardSchema.getStartNo());
        dbLZCard.setEndNo(aLZCardSchema.getEndNo());
        dbLZCard.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCard.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(dbLZCard.getEndNo(),
                dbLZCard.getStartNo()) + 1);
        dbLZCard.setHandler(aLZCardSchema.getHandler());
        dbLZCard.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCard.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCard.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        dbLZCard.setSaleChnl("");

        // 置状态标志和操作标志
        dbLZCard.setStateFlag(getCardState(aLZCardSchema.getReceiveCom()));
        dbLZCard.setOperateFlag("2");

        dbLZCard.setPayFlag("");
        dbLZCard.setEnterAccFlag("");
        dbLZCard.setReason("");
        dbLZCard.setState(aLZCardSchema.getState());
        dbLZCard.setOperator(globalInput.Operator);
        dbLZCard.setMakeDate(PubFun.getCurrentDate());
        dbLZCard.setMakeTime(PubFun.getCurrentTime());
        dbLZCard.setModifyDate(PubFun.getCurrentDate());
        dbLZCard.setModifyTime(PubFun.getCurrentTime());
        if (aLZCardSchema.getSubCode() == null ||
            aLZCardSchema.getSubCode().equals("")) {
            dbLZCard.setSubCode("0");
        } else {
            dbLZCard.setSubCode(aLZCardSchema.getSubCode());
        }
        vResult.set(3, dbLZCard.getSchema());

        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();

        dbLZCardTrack.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCardTrack.setSubCode(dbLZCard.getSubCode());
        dbLZCardTrack.setRiskCode(dbLZCard.getRiskCode());
        dbLZCardTrack.setRiskVersion(dbLZCard.getRiskVersion());
        dbLZCardTrack.setStartNo(aLZCardSchema.getStartNo());
        dbLZCardTrack.setEndNo(aLZCardSchema.getEndNo());
        dbLZCardTrack.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCardTrack.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCardTrack.setSumCount((int) CertifyFunc.bigIntegerDiff(
                dbLZCardTrack.getEndNo(), dbLZCardTrack.getStartNo()) + 1);
        dbLZCardTrack.setPrem("");
        dbLZCardTrack.setAmnt(aLZCardSchema.getAmnt());
        dbLZCardTrack.setHandler(aLZCardSchema.getHandler());
        dbLZCardTrack.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCardTrack.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCardTrack.setTakeBackNo(dbLZCard.getTakeBackNo());
        dbLZCardTrack.setSaleChnl(aLZCardSchema.getSaleChnl());

        // 置状态标志和操作标志
        dbLZCardTrack.setStateFlag(dbLZCard.getStateFlag());
        dbLZCardTrack.setOperateFlag("2");

        dbLZCardTrack.setPayFlag("");
        dbLZCardTrack.setEnterAccFlag("");
        dbLZCardTrack.setReason("");
        dbLZCardTrack.setState(aLZCardSchema.getState());
        dbLZCardTrack.setOperator(globalInput.Operator);
        dbLZCardTrack.setMakeDate(PubFun.getCurrentDate());
        dbLZCardTrack.setMakeTime(PubFun.getCurrentTime());
        dbLZCardTrack.setModifyDate(PubFun.getCurrentDate());
        dbLZCardTrack.setModifyTime(PubFun.getCurrentTime());

        vResult.set(4, dbLZCardTrack.getSchema());
        return true;
    }


    /**
     * Kevin 2003-05-16
     * 拆分单证（回收回退）
     * 回收回退单证
     * golbalInput : 全局空间
     * aLZCardSchema : 单证记录。表示要回收的单证信息。
     * vResult : 返回的结果集。其中第一个元素是要删除的LZCard的信息，
     * 第二和第三个元素是要插入的LZCard的信息，
     * 第四个元素是要插入的LZCard的信息，
     * 第五个元素是要插入的LZCardTrack的信息。
     */
    protected static boolean splitCertReveTakeBack(GlobalInput globalInput,
            LZCardSchema aLZCardSchema, VData vResult) {
        // 清空返回结果集
        vResult.clear();
        //清空静态错误
        mErrors.clearErrors();

        // 初始化返回结果集
        vResult.add(0, null);
        vResult.add(1, null);
        vResult.add(2, null);
        vResult.add(3, null);
        vResult.add(4, null);

        String szSql = "";
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        String szSendOutCom = aLZCardSchema.getSendOutCom();

        String strStartNo = aLZCardSchema.getStartNo();
        String strEndNo = aLZCardSchema.getEndNo();

        // 单证轨迹校验(单证回收回退必须按照单证回收的轨迹原路返回)
        szSql = "SELECT a.* FROM LZCardTrack a" +
                " WHERE a.ModifyDate = " +
                "   ( SELECT MAX(b.ModifyDate) FROM LZCardTrack b" +
                "     WHERE b.CertifyCode = '" + szCertifyCode + "'" +
                "     AND b.OperateFlag = '1'" +
                "     AND b.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND b.SendOutCom = '" + szReceiveCom + "'" +
                "     AND b.StartNo <= '" + strStartNo + "' AND b.EndNo >= '" +
                strStartNo + "'" +
                "     AND b.StartNo <= '" + strEndNo + "' AND b.EndNo >= '" +
                strEndNo + "')" +
                " AND a.ModifyTime = " +
                "   ( SELECT MAX(c.ModifyTime) FROM LZCardTrack c" +
                "     WHERE c.ModifyDate = a.ModifyDate" +
                "     AND c.OperateFlag = '1'" +
                "     AND c.CertifyCode = '" + szCertifyCode + "'" +
                "     AND c.ReceiveCom = '" + szSendOutCom + "'" +
                "     AND c.SendOutCom = '" + szReceiveCom + "'" +
                "     AND c.StartNo <= '" + strStartNo + "' AND c.EndNo >= '" +
                strStartNo + "'" +
                "     AND c.StartNo <= '" + strEndNo + "' AND c.EndNo >= '" +
                strEndNo + "')" +
                " AND a.CertifyCode = '" + szCertifyCode + "'" +
                " AND a.OperateFlag = '1'" +
                " AND a.ReceiveCom = '" + szSendOutCom + "'" +
                " AND a.SendOutCom = '" + szReceiveCom + "'" +
                " AND a.StartNo <= '" + strStartNo + "' AND a.EndNo >= '" +
                strStartNo + "'" +
                " AND a.StartNo <= '" + strEndNo + "' AND a.EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardTrackDB dbLZCardTrack = new LZCardTrackDB();
        if (dbLZCardTrack.executeQuery(szSql).size() != 1) {
            buildError("splitCertify", "校验单证轨迹失败，单证回收回退必须按单证回收的轨迹返回!");
            return false;
        }

        // 单证状态校验
        szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
        szSql += szCertifyCode;
        szSql += "' AND StateFlag <> '0' AND ReceiveCom = '";
        szSql += szSendOutCom;
        szSql += "' AND StartNo <= '" + strStartNo + "' AND EndNo >= '" +
                strStartNo;
        szSql += "' AND StartNo <= '" + strEndNo + "' AND EndNo >= '" +
                strEndNo + "'";

        System.out.println(szSql);

        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet set = dbLZCard.executeQuery(szSql);
        if (set.size() != 1) { // 输入的单证号在可用的单证号内
            buildError("splitCertify", "校验单证状态失败，输入的单证号不在可用单证号的范围内!");
            return false;
        }

        /* * * * * * * * * * * * * * * *
         * 开始拆分单证
         * * * * * * * * * * * * * * * */
        dbLZCard.setSchema(set.get(1)); // 直接使用查询出来的那条数据
        String strOldStartNo = dbLZCard.getStartNo();
        String strOldEndNo = dbLZCard.getEndNo();
        int nNoLen = getCertifyNoLen(dbLZCard.getCertifyCode());

        // 从LZCard表中删除原有单证，将要删除的数据放入返回结果集中
        vResult.set(0, dbLZCard.getSchema());

        // 插入第一个新单证到LZCard表中
        if (strStartNo.compareTo(strOldStartNo) > 0) {
            dbLZCard.setStartNo(strOldStartNo);
            dbLZCard.setEndNo(bigIntegerPlus(strStartNo, "-1", nNoLen));
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strStartNo,
                    strOldStartNo));
            dbLZCard.setModifyDate(PubFun.getCurrentDate());
            dbLZCard.setModifyTime(PubFun.getCurrentTime());

            vResult.set(1, dbLZCard.getSchema());
        }

        // 插入第二个新单证到LZCard表中
        if (strOldEndNo.compareTo(strEndNo) > 0) {
            dbLZCard.setStartNo(bigIntegerPlus(strEndNo, "1", nNoLen));
            dbLZCard.setEndNo(strOldEndNo);
            dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(strOldEndNo,
                    strEndNo));

            vResult.set(2, dbLZCard.getSchema());
        }

        /* * * * * * * * * * * * * * * *
         * 拆分单证结束
         * * * * * * * * * * * * * * * */

        // 保存新的单证
        dbLZCard.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCard.setStartNo(aLZCardSchema.getStartNo());
        dbLZCard.setEndNo(aLZCardSchema.getEndNo());
        dbLZCard.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCard.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCard.setSumCount((int) CertifyFunc.bigIntegerDiff(dbLZCard.getEndNo(),
                dbLZCard.getStartNo()) + 1);
        dbLZCard.setHandler(aLZCardSchema.getHandler());
        dbLZCard.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCard.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCard.setTakeBackNo(aLZCardSchema.getTakeBackNo());
        dbLZCard.setSaleChnl("");

        // 置状态标志和操作标志
        if (szReceiveCom.charAt(0) == 'D') { // 如果回收回退到业务员
            dbLZCard.setStateFlag("0");
        } else {
            dbLZCard.setStateFlag("1");
        }

        dbLZCard.setOperateFlag("3");

        dbLZCard.setPayFlag("");
        dbLZCard.setEnterAccFlag("");
        dbLZCard.setReason("");
        dbLZCard.setState(aLZCardSchema.getState());
        dbLZCard.setOperator(globalInput.Operator);
        dbLZCard.setMakeDate(PubFun.getCurrentDate());
        dbLZCard.setMakeTime(PubFun.getCurrentTime());
        dbLZCard.setModifyDate(PubFun.getCurrentDate());
        dbLZCard.setModifyTime(PubFun.getCurrentTime());
        if (aLZCardSchema.getSubCode() == null ||
            aLZCardSchema.getSubCode().equals("")) {
            dbLZCard.setSubCode("0");
        } else {
            dbLZCard.setSubCode(aLZCardSchema.getSubCode());
        }
        vResult.set(3, dbLZCard.getSchema());

        // 记录单证操作轨迹
        LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();

        dbLZCardTrack.setCertifyCode(aLZCardSchema.getCertifyCode());
        dbLZCardTrack.setSubCode(dbLZCard.getSubCode());
        dbLZCardTrack.setRiskCode(dbLZCard.getRiskCode());
        dbLZCardTrack.setRiskVersion(dbLZCard.getRiskVersion());
        dbLZCardTrack.setStartNo(aLZCardSchema.getStartNo());
        dbLZCardTrack.setEndNo(aLZCardSchema.getEndNo());
        dbLZCardTrack.setSendOutCom(aLZCardSchema.getSendOutCom());
        dbLZCardTrack.setReceiveCom(aLZCardSchema.getReceiveCom());
        dbLZCardTrack.setSumCount((int) CertifyFunc.bigIntegerDiff(
                dbLZCardTrack.getEndNo(), dbLZCardTrack.getStartNo()) + 1);
        dbLZCardTrack.setPrem("");
        dbLZCardTrack.setAmnt(aLZCardSchema.getAmnt());
        dbLZCardTrack.setHandler(aLZCardSchema.getHandler());
        dbLZCardTrack.setHandleDate(aLZCardSchema.getHandleDate());
        dbLZCardTrack.setInvaliDate(aLZCardSchema.getInvaliDate());
        dbLZCardTrack.setTakeBackNo(dbLZCard.getTakeBackNo());
        dbLZCardTrack.setSaleChnl(aLZCardSchema.getSaleChnl());
        dbLZCardTrack.setStateFlag(dbLZCard.getStateFlag());
        dbLZCardTrack.setOperateFlag("3");
        dbLZCardTrack.setPayFlag("");
        dbLZCardTrack.setEnterAccFlag("");
        dbLZCardTrack.setReason("");
        dbLZCardTrack.setState("1");
        dbLZCardTrack.setOperator(globalInput.Operator);
        dbLZCardTrack.setMakeDate(PubFun.getCurrentDate());
        dbLZCardTrack.setMakeTime(PubFun.getCurrentTime());
        dbLZCardTrack.setModifyDate(PubFun.getCurrentDate());
        dbLZCardTrack.setModifyTime(PubFun.getCurrentTime());

        vResult.set(4, dbLZCardTrack.getSchema());
        return true;
    }


    /**
     * 用来处理外部的单证回收的请求
     * @return
     */
    protected static boolean handleTakeBack(LZCardSchema aLZCardSchema,
                                            VData vResult) {

        mErrors.clearErrors();

        // 清除返回结果集
        vResult.clear();

        // 根据传入的单证号码和单证类型从数据库中取出发放者和接收者
        String strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '"
                        + aLZCardSchema.getCertifyCode() + "' AND StartNo <= '"
                        + aLZCardSchema.getStartNo() + "' AND EndNo >= '"
                        + aLZCardSchema.getStartNo() + "' AND StateFlag = '0'";
        System.out.println("strSQL is : " + strSQL);
        LZCardSet tLZCardSet = new LZCardDB().executeQuery(strSQL);

        if (tLZCardSet.size() < 1) {
            buildError("handleTakeBack", "找不到要回收的单证");
            return false;
        }

        LZCardSchema tLZCardSchema = tLZCardSet.get(1);

        if (tLZCardSchema.getReceiveCom().charAt(0) != 'D') {
            if (tLZCardSchema.getReceiveCom().equals(tLZCardSchema.
                    getSendOutCom())) {

            } else {
                buildError("handleTakeBack", "该单证不在代理人手中，不能正常回收！");
                return false;
            }
        }

        // 对传入的数据做一些初始化的操作
        aLZCardSchema.setReceiveCom(tLZCardSchema.getSendOutCom());
        aLZCardSchema.setSendOutCom(tLZCardSchema.getReceiveCom());
        aLZCardSchema.setHandler(tLZCardSchema.getOperator());
        aLZCardSchema.setHandleDate(PubFun.getCurrentDate());
        aLZCardSchema.setStateFlag("1"); // 外部的回收请求，作为正常回收。

        // 有发放权限也就有回收权限。在此处不校验权限

        // 取得回收操作的发放者和接收者
        String strReceiveCom = aLZCardSchema.getReceiveCom();
        String strSendOutCom = aLZCardSchema.getSendOutCom();

        // 产生回收清算单号
        String strNoLimit = PubFun.getNoLimit(strReceiveCom.substring(1));
        String strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);

        aLZCardSchema.setTakeBackNo(strTakeBackNo);

        // 构造需要的GlobalInput对象
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.ComCode = strReceiveCom.substring(1);
        tGlobalInput.Operator = aLZCardSchema.getOperator();

        VData vOneResult = new VData();

        // 回收时拆分单证
        if (!splitCertifyTakeBack(tGlobalInput, aLZCardSchema, vOneResult)) {
            return false;
        }

        vResult.add(vOneResult);

        return true;
    }

    protected static boolean AutoTakeBack(LZCardSchema aLZCardSchema,
                                          VData vResult) {

        mErrors.clearErrors();

        // 清除返回结果集
        vResult.clear();

        // 根据传入的单证号码和单证类型从数据库中取出发放者和接收者
        String strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '"
                        + aLZCardSchema.getCertifyCode() + "' AND StartNo <= '"
                        + aLZCardSchema.getStartNo() + "' AND EndNo >= '"
                        + aLZCardSchema.getStartNo() + "' AND StateFlag = '0'";
        System.out.println("strSQL is : " + strSQL);
        LZCardSet tLZCardSet = new LZCardDB().executeQuery(strSQL);

        if (tLZCardSet.size() < 1) {
            buildError("handleTakeBack", "找不到要回收的单证");
            return false;
        }

        LZCardSchema tLZCardSchema = tLZCardSet.get(1);

        if (tLZCardSchema.getReceiveCom().charAt(0) != 'D') {
            buildError("handleTakeBack", "该单证不在代理人手中，不能正常回收！");
            return false;
        }

        // 对传入的数据做一些初始化的操作
        aLZCardSchema.setReceiveCom(tLZCardSchema.getSendOutCom());
        aLZCardSchema.setSendOutCom(tLZCardSchema.getReceiveCom());
        aLZCardSchema.setHandler(tLZCardSchema.getOperator());
        aLZCardSchema.setHandleDate(PubFun.getCurrentDate());
        aLZCardSchema.setStateFlag("1"); // 外部的回收请求，作为正常回收。

        // 有发放权限也就有回收权限。在此处不校验权限

        // 取得回收操作的发放者和接收者
        String strReceiveCom = aLZCardSchema.getReceiveCom();
        String strSendOutCom = aLZCardSchema.getSendOutCom();

        // 产生回收清算单号
        String strNoLimit = PubFun.getNoLimit(strReceiveCom.substring(1));
        String strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);

        aLZCardSchema.setTakeBackNo(strTakeBackNo);

        // 构造需要的GlobalInput对象
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.ComCode = strReceiveCom.substring(1);
        tGlobalInput.Operator = aLZCardSchema.getOperator();

        VData vOneResult = new VData();

        // 回收时拆分单证
        if (!splitCertifyTakeBack(tGlobalInput, aLZCardSchema, vOneResult)) {
            return false;
        }

        vResult.add(vOneResult);

        return true;
    }

    /**
     * 用来处理外部的核销单证的请求
     * @return
     */
    protected static boolean AuditCancelBack(LZCardSchema aLZCardSchema,
                                             VData vResult) {

        mErrors.clearErrors();

        // 清除返回结果集
        vResult.clear();

        // 根据传入的单证号码和单证类型从数据库中取出发放者和接收者
        String strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '"
                        + aLZCardSchema.getCertifyCode() + "' AND StartNo <= '"
                        + aLZCardSchema.getStartNo() + "' AND EndNo >= '"
                        + aLZCardSchema.getStartNo() + "' AND StateFlag = '0'";
        System.out.println("strSQL is : " + strSQL);
        LZCardSet tLZCardSet = new LZCardDB().executeQuery(strSQL);

        if (tLZCardSet.size() < 1) {
            buildError("handleTakeBack", "找不到要回收的单证");
            return false;
        }

        LZCardSchema tLZCardSchema = tLZCardSet.get(1);

        // 对传入的数据做一些初始化的操作
        aLZCardSchema.setReceiveCom(tLZCardSchema.getSendOutCom());
        aLZCardSchema.setSendOutCom(tLZCardSchema.getReceiveCom());
        aLZCardSchema.setHandler(tLZCardSchema.getOperator());
        aLZCardSchema.setHandleDate(PubFun.getCurrentDate());

        if (tLZCardSchema.getReceiveCom().charAt(0) != 'D') {
            if (tLZCardSchema.getReceiveCom().equals(tLZCardSchema.
                    getSendOutCom())) {
                aLZCardSchema.setStateFlag("5"); // 从业务员手中核销的单证，作为正常核销。
            } else {
                aLZCardSchema.setStateFlag("6"); // 从部门核销的单证，作为正常核销。
            }
        }
        // 有发放权限也就有回收权限。在此处不校验权限

        // 取得回收操作的发放者和接收者
        String strReceiveCom = aLZCardSchema.getReceiveCom();
        String strSendOutCom = aLZCardSchema.getSendOutCom();

        // 产生回收清算单号
        String strNoLimit = PubFun.getNoLimit(strReceiveCom.substring(1));
        String strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);

        aLZCardSchema.setTakeBackNo(strTakeBackNo);

        // 构造需要的GlobalInput对象
        GlobalInput tGlobalInput = new GlobalInput();

        //tGlobalInput.ComCode = strReceiveCom.substring(1);
        tGlobalInput.Operator = aLZCardSchema.getOperator();

        VData vOneResult = new VData();

        // 回收时拆分单证
        if (!splitCertifyTakeBack(tGlobalInput, aLZCardSchema, vOneResult)) {
            return false;
        }

        vResult.add(vOneResult);

        return true;
    }

    /*
     * add by kevin, 2002-09-23
     */
    private static void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertifyFunc";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;

        System.out.println("In CertifyFunc buildError() : " + szErrMsg);
        mErrors.addOneError(cError);
    }


    /**
     * kevin, 2002-09-25
     * 输入：
     *     SendOutCom  发放机构
     *     AgentCode   代理人
     * 功能：
     *     查询LAAgent表和LZAccess表，看发放机构是否有对该代理人进行单证发放的权限
     */
    private static boolean verifyAgentAccess(String szSendOutCom,
                                             String szAgentCode) {
        System.out.println("CertifyFunc -> verifyAgentAccess()" + szSendOutCom);
        String szCom = "";
        String szSql = "SELECT * FROM LZAccess WHERE SendOutCom = '";
        szSql += szSendOutCom;
        szSql += "' AND ReceiveCom LIKE 'D%'";

        System.out.println(szSql);

        LZAccessSet setLZAccess = new LZAccessDB().executeQuery(szSql);

        /*
         * if no records are found, return false;
         * nIndex begin with 1;
         */
        for (int nIndex = 1; nIndex <= setLZAccess.size(); nIndex++) {
            szCom = szSendOutCom.substring(1);
            if (setLZAccess.get(nIndex).getReceiveCom().trim().length() > 2) {
                szCom += setLZAccess.get(nIndex).getReceiveCom().trim().
                        substring(1);
            }

            szSql = "SELECT * FROM LAAgent WHERE AgentCode = '";
            szSql += szAgentCode;
            szSql += "' AND AgentState" + VALID_AGENT_STATE +
                    " AND ManageCom LIKE '";
            szSql += szCom;
            szSql += "%'";

            System.out.println(szSql);

            LAAgentSet setLAAgent = new LAAgentDB().executeQuery(szSql);

            if (setLAAgent.size() == 1) {
                return true; // pass
            }
        }

        buildError("verifyAgentAccess", "该发放机构没有往该代理人发放单证的权限");
        return false;
    }


    /**
     * kevin, 2002-09-25
     * 输入：
     *     SendOutCom  发放机构
     *     ReceiveCom  接收机构
     * 功能：
     *     查询LZAccess表，看发放机构是否有对该接收机构进行单证发放的权限
     */
    private static boolean verifyAccess(String szSendOutCom,
                                        String szReceiveCom) {
        String szCom = "";
        String szSql = "SELECT * FROM LZAccess WHERE SendOutCom = '";
        szSql += szSendOutCom;
        szSql += "' AND ReceiveCom = '";
        szSql += szReceiveCom;
        szSql += "'";
        LZAccessSet setLZAccess = new LZAccessDB().executeQuery(szSql);

        if (setLZAccess.size() == 1) {
            return true; // pass
        }

        buildError("verifyAccess", "该发放机构没有往该接收机构发放单证的权限");
        return false;
    }


    /**
     * kevin, 2002-09-25
     * 输入：
     *     SendOutCom  发放机构
     *     ReceiveCom  代理人
     * 功能：
     *     发放机构是否有对该代理进行单证发放的权限，
     *     在此函数中将判断发放机构是不是最末一级的机构
     */
    private static boolean verifyAgent(String szSendOutCom, String szReceiveCom) {
        System.out.println("in verifyAgent()");
        if (szSendOutCom.length() == LAST_COM) { // 是最末级的机构
            // 看这个代理人是不是属于这个末级机构的
            String szSql = "SELECT * FROM LAAgent WHERE AgentCode = '";
            szSql += szReceiveCom.substring(1);
            szSql += "' AND AgentState " + VALID_AGENT_STATE; // 查询可用的代理人

            LAAgentSet setLAAgent = new LAAgentDB().executeQuery(szSql);

            if (setLAAgent.size() == 1) {
                //if( setLAAgent.get(1).getManageCom().trim().equals(szSendOutCom.substring(1)) ) {
                //    return true;
                //}
                return true;
            }
            buildError("verifyAgent", "代理人不可用或者代理人不属于该机构");
            return false;
        } // if( szSendOutCom.length( ) == LAST_COM )

        // 如果不是最末级的机构，则需要查询权限表。
        if (szReceiveCom.length() > FIRST_COM) {
            if (!verifyAgentAccess(szSendOutCom, szReceiveCom.substring(1))) {
                return false;
            }
        } else {
            buildError("verifyAgent", "接收机构的格式非法!");
            return false;
        }
        return true;
    }


    /**
     * wentao, 2004-10-26
     * 输入：
     *     PolNo       保单号
     *     SendOutCom  'D' + 代理人编码
     *     ReceiveCom  'A' + 管理机构代码
     * 功能：
     *     该保单号对应的是否是这个代理人
     */
    public static boolean verifyAgentCode(String szPolNo, String szSendOutCom,
                                          String szReceiveCom) {
        mErrors.clearErrors();

        //查询保单对应的代理人编码和管理机构
        String sql = "SELECT * FROM lcpol WHERE polno = '" + szPolNo
                     + "' ";

        //查询该保单是否存在
        LCPolSet setLCPol = new LCPolDB().executeQuery(sql);
        if (setLCPol.size() != 1) {
            buildError("verifyAgentCode", "该保单不存在！");
            return false;
        }

        String agentcode = setLCPol.get(1).getAgentCode();
        String comcode = setLCPol.get(1).getManageCom();

        System.out.println("Query result : " + agentcode + " -- " + comcode);
        //判断该保单对应的代理人和输入的代理人是否一致
        if (szSendOutCom.substring(1).trim().equals(agentcode) == false) {
            buildError("verifyAgentCode", "该保单的代理人和录入的代理人不一致！");
            return false;
        }
        if (szReceiveCom.substring(1).trim().equals(comcode) == false) {
            buildError("verifyAgentCode", "该保单的机构代码和录入的机构代码不一致！");
            return false;
        }
        return true;
    }


    /*
     * Kevin 2002-10-30
     * 校验发放机构和接收机构的存在
     */
    protected static boolean isComsExist(String szSendOutCom,
                                         String szReceiveCom) {
        String szSql;
        mErrors.clearErrors();
        if (szSendOutCom == null || szReceiveCom == null ||
            szSendOutCom.length() < 2 || szReceiveCom.length() < 2) {
            buildError("isComsExist", "没有指定发放/接收机构，或者是机构长度小于2位。");
            return false;
        }

        // verify SendOutCom
        if (szSendOutCom.charAt(0) == 'A') {
            szSql = "SELECT * FROM LDCom WHERE ComCode = '" +
                    szSendOutCom.substring(1) + "'";
            if (new LDComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist", szSendOutCom.substring(1) + "该机构不存在!");
                return false;
            }

        } else if (szSendOutCom.charAt(0) == 'B') {
            szSql = "SELECT * FROM LZCertifyUser WHERE UserCode = '" +
                    szSendOutCom.substring(1) + "' AND StateFlag = '0'";
            System.out.println("zb szSql: " + szSql);
            if (new LZCertifyUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist",
                           szSendOutCom.substring(1) + "该用户不是单证管理员!");
                return false;
            }

        } else if (szSendOutCom.charAt(0) == 'D') {
            szSql = "SELECT * FROM LAAgent" +
                    " WHERE AgentCode = '" + szSendOutCom.substring(1) + "'" +
                    " AND AgentState " + VALID_AGENT_STATE;
            if (new LAAgentDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist",
                           szSendOutCom.substring(1) + "该业务员不存在或者已经失效!");
                return false;
            }

        } else if (szSendOutCom.equals("00")) {
            // do nothing

        } else {
            buildError("isComsExist",
                       szSendOutCom.substring(1) + "格式错误，必须以A、B或D开头。");
            return false;
        }

        // verify ReceiveCom
        if (szReceiveCom.charAt(0) == 'A') {
            szSql = "SELECT * FROM LDCom WHERE ComCode = '" +
                    szReceiveCom.substring(1) + "'";
            if (new LDComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist", szReceiveCom.substring(1) + "该机构不存在!");
                return false;
            }
        } else if (szReceiveCom.charAt(0) == 'B') {
            szSql = "SELECT * FROM LZCertifyUser WHERE UserCode = '" +
                    szReceiveCom.substring(1) + "' AND StateFlag = '0'";
            if (new LDUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist",
                           szReceiveCom.substring(1) + "该用户不是单证管理员!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'C') {
            szSql = "SELECT * FROM LDUser WHERE UserCode = '" +
                    szReceiveCom.substring(1) + "'";
            if (new LDUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist", szReceiveCom.substring(1) + "该用户不存在!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'D') {
            szSql = "SELECT * FROM LAAgent" +
                    " WHERE AgentCode = '" + szReceiveCom.substring(1) + "'" +
                    " AND AgentState " + VALID_AGENT_STATE;
            if (new LAAgentDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist",
                           szReceiveCom.substring(1) + "该业务员不存在或者已经失效!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'E') {
            szSql = "SELECT * FROM lACom" +
                    " WHERE AgentCom = '" + szReceiveCom.substring(1) + "'";
            if (new LAComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该代理机构不存在!");
                return false;
            }

        } else {
            buildError("isComsExist2", szReceiveCom + "格式错误，必须以A、B、C、D或E开头。");
            return false;
        }

        return true;
    }


    /*
     * wentao 2004-10-12
     * 校验发放机构和接收机构的存在
     * (个人保单的回执不进行校验业务员的状态)
     */
    protected static boolean isComsExist2(String szSendOutCom,
                                          String szReceiveCom) {
        String szSql;
        //清空静态错误
        mErrors.clearErrors();

        if (szSendOutCom == null || szReceiveCom == null ||
            szSendOutCom.length() < 2 || szReceiveCom.length() < 2) {
            buildError("isComsExist2", "没有指定发放/接收机构，或者是机构长度小于2位。");
            return false;
        }

        // verify SendOutCom
        if (szSendOutCom.charAt(0) == 'A') {
            szSql = "SELECT * FROM LDCom WHERE ComCode = '" +
                    szSendOutCom.substring(1) + "'";
            if (new LDComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szSendOutCom.substring(1) + "该机构不存在!");
                return false;
            }

        } else if (szSendOutCom.charAt(0) == 'B') {
            szSql = "SELECT * FROM LDUser WHERE UserCode = '" +
                    szSendOutCom.substring(1) + "' AND CertifyFlag = '1'";
            if (new LDUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szSendOutCom.substring(1) + "该用户不存在!");
                return false;
            }

        } else if (szSendOutCom.charAt(0) == 'D') {
            szSql = "SELECT * FROM LAAgent" +
                    " WHERE AgentCode = '" + szSendOutCom.substring(1) + "'";
            if (new LAAgentDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szSendOutCom.substring(1) + "该业务员不存在!");
                return false;
            }

        } else if (szSendOutCom.equals("00")) {
            // do nothing

        } else {
            buildError("isComsExist2",
                       szSendOutCom.substring(1) + "格式错误，必须以A、B或D开头。");
            return false;
        }

        // verify ReceiveCom
        if (szReceiveCom.charAt(0) == 'A') {
            szSql = "SELECT * FROM LDCom WHERE ComCode = '" +
                    szReceiveCom.substring(1) + "'";
            if (new LDComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该机构不存在!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'B') {
            szSql = "SELECT * FROM LDUser WHERE UserCode = '" +
                    szReceiveCom.substring(1) + "' AND CertifyFlag = '1'";
            if (new LDUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该用户不存在!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'C') {
            szSql = "SELECT * FROM LDUser WHERE UserCode = '" +
                    szReceiveCom.substring(1) + "'";
            if (new LDUserDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该用户不存在!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'D') {
            szSql = "SELECT * FROM LAAgent" +
                    " WHERE AgentCode = '" + szReceiveCom.substring(1) + "'";
            if (new LAAgentDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该业务员不存在!");
                return false;
            }

        } else if (szReceiveCom.charAt(0) == 'E') {
            szSql = "SELECT * FROM lACom" +
                    " WHERE AgentCom = '" + szReceiveCom.substring(1) + "'";
            if (new LAComDB().executeQuery(szSql).size() != 1) {
                buildError("isComsExist2",
                           szReceiveCom.substring(1) + "该代理机构不存在!");
                return false;
            }

        }

        else {
            buildError("isComsExist2", szReceiveCom + "格式错误，必须以A、B、C、D或E开头。");
            return false;
        }

        return true;
    }


    /**
     * 返回两个大数字的差值
     * @param strMinuend　被减数
     * @param strSubtrahend　减数
     * @return　strMinuend - strSubtrahend
     */
    protected static long bigIntegerDiff(String strMinuend,
                                         String strSubtrahend) {
        BigInteger bigIntMinuend = new BigInteger(strMinuend);
        BigInteger bigIntSubtrahend = new BigInteger(strSubtrahend);
        return bigIntMinuend.subtract(bigIntSubtrahend).longValue();
    }


    /**
     * 返回两个大数相加的结果
     * @param strX 加数一
     * @param strY 加数二
     * @param nMinLen 返回结果的最小长度
     * @return strX + strY
     */
    protected static String bigIntegerPlus(String strX, String strY,
                                           int nMinLen) {
        BigInteger bigX = new BigInteger(strX);
        BigInteger bigY = new BigInteger(strY);

        String str = (bigX.add(bigY)).toString();
        String strTemp = "00000000000000000000";
        strTemp = strTemp.substring(1, (nMinLen - str.length()) + 1);

        return strTemp + str;
    }


    /**
     * 处理系统单证的描述
     * @param aLZSysCertifySchema 系统单证数据
     * @param conn 数据库连接对象
     * @param nFlag 标识：0表示执行BeforeSql；1表示执行AfterSql
     * @param sb 在描述表中描述的错误信息
     * @return true执行成功；false执行失败
     * @throws Exception
     */
    protected static boolean handleSysCertifyDesc(LZSysCertifySchema
                                                  aLZSysCertifySchema,
                                                  Connection conn,
                                                  int nFlag,
                                                  StringBuffer sb) throws
            Exception {
        //清空静态错误
        mErrors.clearErrors();

        if (conn == null || (nFlag != 0 && nFlag != 1)) {
            throw new Exception("数据库连接为空或者是非法的标识");
        }

        // First, we get calcode associated with this kind of SysCertify
        LMCertifySubDB tLMCertifySubDB = new LMCertifySubDB(conn);

        tLMCertifySubDB.setCertifyCode(aLZSysCertifySchema.getCertifyCode());

        if (tLMCertifySubDB.getInfo() == false) {
            buildError("handleSysCertifyDesc", "系统单证的描述有错");
            return false;
        }

        sb.append(tLMCertifySubDB.getErrorMessage());

        // After getting the calcode, use
        String strCalCode = "";

        if (nFlag == 0) {
            strCalCode = tLMCertifySubDB.getBeforeSQL();
        } else {
            strCalCode = tLMCertifySubDB.getAfterSQL();
        }

        Calculator calculator = new Calculator();

        calculator.addBasicFactor("CertifyNo", aLZSysCertifySchema.getCertifyNo());
        calculator.addBasicFactor("StateFlag", aLZSysCertifySchema.getStateFlag());
        calculator.addBasicFactor("AgentCode",
                                  aLZSysCertifySchema.getReceiveCom());
        calculator.setCalCode(strCalCode);

        String strSQL = calculator.calculate();

        System.out.println("SQL : " + strSQL);
        String CurDate = PubFun.getCurrentDate();
//        strSQL = strSQL.toLowerCase();
        if (strSQL.indexOf("sysdate") != -1) {
            String SQLx = strSQL.substring(0, strSQL.indexOf("sysdate"))
                          + "'" + CurDate + "'"
                          + strSQL.substring(strSQL.indexOf("sysdate") + 7);
            strSQL = SQLx;
            //strSQL.replaceAll("sysdate", "'" + CurDate + "'");
            //strSQL.replaceFirst("sysdate",CurDate);
        }
        System.out.println("SQL : " + strSQL);

        Statement stmt = conn.createStatement();
        ResultSet rs = null;
        boolean bFlag = false;

        try {
            if (0 == nFlag) {
                rs = stmt.executeQuery(strSQL);
                if (rs.next()) {
                    bFlag = rs.getString(1).equals("1");
                }
                rs.close();
            } else if (1 == nFlag) {
                bFlag = true;

                String strTemp = "";
                char cTemp = ' ';

                for (int nIndex = 0; nIndex < strSQL.length(); nIndex++) {
                    cTemp = strSQL.charAt(nIndex);
                    if (cTemp == ';') {
                        stmt.executeUpdate(strTemp);
                        System.out.println(strTemp);
                        strTemp = "";
                    } else {
                        strTemp += String.valueOf(cTemp);
                    }
                }

                if (!strTemp.equals("")) {
                    System.out.println(strTemp);
                    stmt.executeUpdate(strTemp);
                }
            }
            stmt.close();
        } catch (Exception ex) {
            if (rs != null) {
                rs.close();
            }
            stmt.close();
            return false;
        }

        return bFlag;
    }


    /**
     * Kevin 2003-03-25
     * 校验代理人可执有的单证数量的最大值
     * @param aLZCardSchema
     * @return
     */
    private static boolean verifyMaxCount(LZCardSchema aLZCardSchema) {
        //清空静态错误
        mErrors.clearErrors();

        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        System.out.println("in verifyMaxCount()..." + szReceiveCom);
        int nExsitCount = 0;
        String szAuthorNo = aLZCardSchema.getAuthorizeNo();

        // 如果不是发到代理人，代理机构手中，则不用校验了。
        if (szReceiveCom.charAt(0) != 'D' && szReceiveCom.charAt(0) != 'E') {
            return true;
        }

        int nMaxCount = getMaxCount(szReceiveCom.substring(1), szCertifyCode);
        System.out.println(" MaxCount(): " + nMaxCount);
        //如果代理人手中的最大单证数量为-1表示数据有误，跳出
        if (nMaxCount == -1) {
            return false;
        }
        if (szReceiveCom.charAt(0) == 'E') {
            nMaxCount = nMaxCount * 130 / 100;
        }
        System.out.println(" MaxCount(): " + nMaxCount);

        //修改状态校验
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());
        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return false;
        }
        String conttr = " StateFlag = '0'";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            conttr = " State in ('10','11')";
        }

        // 得到代理人现有单证数量
        String strSQL =
                "SELECT SUM(SUMCOUNT) FROM LZCard WHERE " + conttr
                + " AND CertifyCode = '" + szCertifyCode + "'"
                + " AND ReceiveCom = '" + szReceiveCom + "'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(strSQL);
        //查询代理人现有单证数量，如果出错，则跳出
        if (exeSQL.mErrors.needDealError()) {
            mErrors.copyAllErrors(exeSQL.mErrors);
            return false;
        }
        //代理人现有单证数量
        int nCurCount = Integer.parseInt(ssrs.GetText(1, 1));
        nExsitCount = nCurCount;
        //打算发给代理人的单证数量
        int nCount = (int) bigIntegerDiff(aLZCardSchema.getEndNo(),
                                          aLZCardSchema.getStartNo()) + 1;
        int a = nCurCount + nCount;
        System.out.println("代理人现有单证数量: " + nCurCount + " 打算发给代理人的单证数量:" +
                           nCount + " 发放后用户总数量：" + a);

        // 得到单证类型
        /* strSQL =
                 "SELECT CertifyClass FROM LMCertifyDes WHERE"
                 + " CertifyCode = '" + szCertifyCode + "'"
                 ;

         exeSQL = new ExeSQL();
         SSRS ssrD = exeSQL.execSQL(strSQL);
         //查询代理人现有单证数量，如果出错，则跳出
         if (exeSQL.mErrors.needDealError()) {
             mErrors.copyAllErrors(exeSQL.mErrors);
             return false;
         }
         //王珑添加 如果是定额保单，暂不限制
         String sCertifyClass = ssrD.GetText(1, 1);*/
        //if (sCertifyClass.equals("P")) {
        //校验现有数量＋打算发放数量是否超出代理人所能拥有的最大数量
        if (nCurCount + nCount > nMaxCount &&
            (szAuthorNo == null || szAuthorNo.equals(""))) {
            int nError = Math.abs(nMaxCount - nCurCount);
            if (nMaxCount == nExsitCount) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else if (nError == 0) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else {
                nExsitCount = Math.abs(nMaxCount - nExsitCount);
                buildError("verifyMaxCount",
                           "发放后，业务员手中执有的单证数将超过最大数量，本次的发放量不得超过" + nExsitCount +
                           "张");
            }
            return false;
        }
        // }

        return true;
    }


    /**
     * Kevin 2003-03-25
     * 校验代理人可执有的单证数量的最大值
     * @param aLZCardSchema
     * @return
     */
    protected static boolean verifyMaxCount(LZCardSchema tLZCardSchema,
                                            LZCardSet aLZCardSet) {
        //清空静态错误
        mErrors.clearErrors();

        int nCurCount = 0;
        int nExsitCount = 0;
        // 如果不是发到代理人手中，则不用校验了。
        if (tLZCardSchema.getReceiveCom().charAt(0) != 'D' &&
            tLZCardSchema.getReceiveCom().charAt(0) != 'E') {
            return true;
        }

        String szAuthorNo = tLZCardSchema.getAuthorizeNo();
        int nMaxCount = getMaxCount(tLZCardSchema.getReceiveCom().substring(1),
                                    tLZCardSchema.getCertifyCode());

        //如果代理人手中的最大单证数量为-1表示数据有误，跳出
        if (nMaxCount == -1) {
            return false;
        }
        if (tLZCardSchema.getReceiveCom().charAt(0) == 'E') {
            nMaxCount = nMaxCount * 130 / 100;
        }
        System.out.println(" MaxCount(): " + nMaxCount);

        //修改状态校验
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setCertifyCode(tLZCardSchema.getCertifyCode());
        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return false;
        }
        String conttr = " StateFlag = '0'";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            conttr = " State in ('10','11')";
        }

        // 得到代理人现有单证数量
        String strSQL =
                "SELECT SUM(SUMCOUNT) FROM LZCard WHERE " + conttr
                + " AND CertifyCode = '" + tLZCardSchema.getCertifyCode() + "'"
                + " AND ReceiveCom = '" + tLZCardSchema.getReceiveCom() + "'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(strSQL);
        //查询代理人现有单证数量，如果出错，则跳出
        if (exeSQL.mErrors.needDealError()) {
            mErrors.copyAllErrors(exeSQL.mErrors);
            return false;
        }
        //代理人现有单证数量
        nCurCount = Integer.parseInt(ssrs.GetText(1, 1));
        nExsitCount = nCurCount;
        System.out.println("in verifyMaxCount()-> nCurCount: " + nCurCount);

        //得到打算发给代理人的单证数量
        int nCount = tLZCardSchema.getSumCount();
        System.out.println("in verifyMaxCount()-> nCount: " + nCount);

        nCurCount = nCurCount + nCount;

        int setN = aLZCardSet.size();

        LZCardSchema aLZCardSchema = aLZCardSet.get(1);
        String szCertifyCode = aLZCardSchema.getCertifyCode();
        String szReceiveCom = aLZCardSchema.getReceiveCom();
        System.out.println("in verifyMaxCount()..." + szReceiveCom);

        //打算发给代理人的单证数量
        //int nCount = (int) bigIntegerDiff(aLZCardSchema.getEndNo(),
        //                                  aLZCardSchema.getStartNo()) +1;

        System.out.println(" MaxCount: " + nMaxCount + "  startno:" +
                           aLZCardSchema.getStartNo() + " EndNo: " +
                           aLZCardSchema.getEndNo() + "   代理人现有单证数量: " +
                           nCurCount + " 打算发给代理人的单证数量:" +
                           nCount + " 发放后用户总数量：" + nCurCount);

        //校验现有数量＋打算发放数量是否超出代理人所能拥有的最大数量
        if (nCurCount > nMaxCount &&
            (szAuthorNo == null || szAuthorNo.equals(""))) {
            int nError = Math.abs(nMaxCount - nCurCount);
            if (nMaxCount == nExsitCount) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else if (nError == 0) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else {
                nExsitCount = Math.abs(nMaxCount - nExsitCount);
                buildError("verifyMaxCount",
                           "发放后，业务员手中执有的单证数将超过最大数量，本次的发放量不得超过" + nExsitCount +
                           "张");
            }
            return false;
        }

        return true;
    }

    /**
     * Kevin 2003-03-25
     * 校验代理人可执有的单证数量的最大值
     * @param aLZCardSchema
     * @return
     */
    protected static boolean verifyMaxCount(LZCardSchema tLZCardSchema,
                                            int mCount) {
        //清空静态错误
        mErrors.clearErrors();

        int nCurCount = 0;
        int nExsitCount = 0;
        // 如果不是发到代理人手中，则不用校验了。
        if (tLZCardSchema.getReceiveCom().charAt(0) != 'D' &&
            tLZCardSchema.getReceiveCom().charAt(0) != 'E') {
            return true;
        }
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(tLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return false;
        }
        if (tLMCertifyDesDB.getCertifyClass().equals("P")) {
            return true;
        }
        String szAuthorNo = tLZCardSchema.getAuthorizeNo();
        int nMaxCount = getMaxCount(tLZCardSchema.getReceiveCom().substring(1),
                                    tLZCardSchema.getCertifyCode());
        System.out.println(" MaxCount(): " + nMaxCount);
        //如果代理人手中的最大单证数量为-1表示数据有误，跳出
        if (nMaxCount == -1) {
            return false;
        }
        if (tLZCardSchema.getReceiveCom().charAt(0) == 'E') {
            nMaxCount = nMaxCount * 130 / 100;
        }
        System.out.println(" MaxCount(): " + nMaxCount);

        //修改状态校验
        String conttr = " StateFlag = '0'";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            conttr = " State in ('10','11')";
        }

        // 得到代理人现有单证数量
        String strSQL =
                "SELECT SUM(SUMCOUNT) FROM LZCard WHERE " + conttr
                + " AND CertifyCode = '" + tLZCardSchema.getCertifyCode() + "'"
                + " AND ReceiveCom = '" + tLZCardSchema.getReceiveCom() + "'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(strSQL);
        //查询代理人现有单证数量，如果出错，则跳出
        if (exeSQL.mErrors.needDealError()) {
            mErrors.copyAllErrors(exeSQL.mErrors);
            return false;
        }
        //代理人现有单证数量
        nCurCount = Integer.parseInt(ssrs.GetText(1, 1));
        nExsitCount = nCurCount;
        System.out.println("in verifyMaxCount()-> nCurCount: " + nCurCount);

        //得到打算发给代理人的单证数量
        int nCount = mCount;
        System.out.println("in verifyMaxCount()-> nCount: " + nCount);

        nCurCount = nCurCount + nCount;

        //int setN = aLZCardSet.size();

        // LZCardSchema aLZCardSchema = aLZCardSet.get(1);
        String szCertifyCode = tLZCardSchema.getCertifyCode();
        String szReceiveCom = tLZCardSchema.getReceiveCom();
        System.out.println("in verifyMaxCount()..." + szReceiveCom);

        //打算发给代理人的单证数量
        //int nCount = (int) bigIntegerDiff(aLZCardSchema.getEndNo(),
        //                                  aLZCardSchema.getStartNo()) +1;

        System.out.println(" MaxCount: " + nMaxCount + "  startno:" +
                           tLZCardSchema.getStartNo() + " EndNo: " +
                           tLZCardSchema.getEndNo() + "   代理人现有单证数量: " +
                           nCurCount + " 打算发给代理人的单证数量:" +
                           nCount + " 发放后用户总数量：" + nCurCount);

        //校验现有数量＋打算发放数量是否超出代理人所能拥有的最大数量
        if (nCurCount > nMaxCount &&
            (szAuthorNo == null || szAuthorNo.equals(""))) {
            int nError = Math.abs(nMaxCount - nCurCount);
            if (nMaxCount == nExsitCount) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else if (nError == 0) {
                buildError("verifyMaxCount", "业务员手中执有的单证数已经达到最大数量");
            } else {
                nExsitCount = Math.abs(nMaxCount - nExsitCount);
                buildError("verifyMaxCount",
                           "发放后，领用人手中执有的单证数将超过最大数量，本次的发放量不得超过" + nExsitCount +
                           "张");
            }
            return false;
        }

        return true;
    }

    /**
     * 从系统变量表中取得单证号码长度的定义
     * @return
     */
    public static int getCertifyNoLen(String szCertifyCode) {
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(szCertifyCode);

        int nLen = 14;

        if (tLMCertifyDesDB.getInfo()) {
            nLen = (int) tLMCertifyDesDB.getCertifyLength();
        } else {
            System.out.println(
                    "Can't get 'CertifyNoLen' information in LDSysVar, use default len 14!");
        }
        return nLen;
    }


    /**
     * 调用单证管理服务
     * @param aLMCertifySubSchema
     * @return
     */
    protected static boolean callCertifyService(LZSysCertifySchema
                                                aLZSysCertifySchema,
                                                GlobalInput aGlobalInput) {
        //清空静态错误
        mErrors.clearErrors();

        LMCertifySubDB tLMCertifySubDB = new LMCertifySubDB();
        tLMCertifySubDB.setCertifyCode(aLZSysCertifySchema.getCertifyCode());
        if (!tLMCertifySubDB.getInfo()) {
            buildError("callCertifyService", "系统单证的描述有错");
            return false;
        }

        if (tLMCertifySubDB.getInterfaceClass() == null ||
            StrTool.cTrim(tLMCertifySubDB.getInterfaceClass()).equals("")) {
            return true;
        }

        // 调用单证管理服务
        try {
            Class cls = Class.forName(tLMCertifySubDB.getInterfaceClass());
            CertifyService ps = (CertifyService) cls.newInstance();

            // 准备数据
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setPrtSeq(aLZSysCertifySchema.getCertifyNo());
            if (!tLOPRTManagerDB.getInfo()) {
                buildError("callCertifyService", "打印管理队列有错");
                return false;
            }
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setSchema(tLOPRTManagerDB);

            String strOperate = "TakeBack";

            VData vData = new VData();

            vData.add(aGlobalInput);
            vData.add(tLOPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }

            mResult = ps.getResult();

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callCertifyService", ex.toString());
            return false;
        }

        return true;
    }


    /**
     * Kevin 2003-05-08
     * 将传入的单证列表信息转化成单证操作所需要的格式。主要是针对于无号单证的处理。
     * 对于无号单证，将根据输入的单证数量，从数据库中凑足所需数量。
     * @param aLZCardSet
     * @return
     */
    public static LZCardSet formatCardList(LZCardSchema aLZCardSchema) {
        System.out.println("formatCardList(): SendOutCom: " +
                           aLZCardSchema.getSendOutCom() + " ReceiveCom:" +
                           aLZCardSchema.getReceiveCom());
        mErrors.clearErrors();

        LZCardSet tLZCardSet = new LZCardSet();

        // 查询单证描述表，确认某一个单证是否是有号单证
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return null;
        }

        // 有号单证
        if (tLMCertifyDesDB.getHaveNumber().equals("Y")) {
            String strErrMsg = verifyNo(aLZCardSchema);//验证起始编号和结束编号

            if (!strErrMsg.equals("")) {
                buildError("formatCardSet", strErrMsg);
                return null;
            }
            strErrMsg = getRealNo(aLZCardSchema, tLZCardSet);

            if (!strErrMsg.equals("")) {
                buildError("formatCardSet", strErrMsg);
                return null;
            }

            // 无号单证
        } else if (tLMCertifyDesDB.getHaveNumber().equals("N")) {
            LZCardSet newLZCardSet = findNo(aLZCardSchema);

            if (newLZCardSet == null) {
                return null;
            }

            tLZCardSet.add(newLZCardSet);
        } else {
            buildError("formatCardSet", "错误的单证是否有号标志");
            return null;
        }
        return tLZCardSet;
    }


    /**
     * Javabean 2006-03-11
     * 将传入的单证列表信息转化成单证操作所需要的格式。主要是针对于无号单证的处理。
     * 对于无号单证，将根据输入的单证数量，从数据库中凑足所需数量。
     * @param aLZCardSet
     * @return
     */
    public static LZCardSet formatAuditCardList(LZCardSchema aLZCardSchema) {
        System.out.println("CertifyFunc->formatAuditCardList()");
        mErrors.clearErrors();
        LZCardSet tLZCardSet = new LZCardSet();

        // 查询单证描述表，确认某一个单证是否是有号单证
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return null;
        }

        // 有号单证
        if (tLMCertifyDesDB.getHaveNumber().equals("Y")) {
            System.out.println("有号单证。");
            String strErrMsg = "";

            strErrMsg = verifyNo(aLZCardSchema);
            if (!strErrMsg.equals("")) {
                buildError("formatCardSet", strErrMsg);
                return null;
            } ///在dealData()中已经校验过

            /*if(!getCardOwner(aLZCardSchema))
                         {
                return null;
                         }*/

            strErrMsg = getAuditCancelRealNo(aLZCardSchema, tLZCardSet);
            if (!strErrMsg.equals("")) {
                AuditCancelBL.ErrorState = "1";
                buildError("formatCardSet", strErrMsg);
                return null;
            }

            // 无号单证
        } else if (tLMCertifyDesDB.getHaveNumber().equals("N")) {
            System.out.println("无号单证。");
            LZCardSet newLZCardSet = findNo(aLZCardSchema);

            if (newLZCardSet == null) {
                return null;
            }

            tLZCardSet.add(newLZCardSet);
        } else {
            AuditCancelBL.ErrorState = "5";
            buildError("formatCardSet", "错误的单证是否有号标志");
            return null;
        }
        return tLZCardSet;
    }


    /**
     * Kevin 2003-04-25
     * 根据数量，从业务系统中查询出所需数量的单证。
     * @param aLZCardSchema
     * @return 返回空值，表示出错。
     */
    protected static LZCardSet findNo(LZCardSchema aLZCardSchema) {
        System.out.println("in findNo().");
        int nSumCount = aLZCardSchema.getSumCount();
        System.out.println("nSumCount: " + nSumCount);

        LZCardDB tLZCardDB = new LZCardDB();

//        tLZCardDB.setCertifyCode(aLZCardSchema.getCertifyCode());
//        tLZCardDB.setReceiveCom(aLZCardSchema.getSendOutCom());
//        tLZCardDB.setStateFlag("0");
        String tSQL = "select * from lzcard where certifycode='"+aLZCardSchema.getCertifyCode()
                    + "' and ReceiveCom='"+aLZCardSchema.getSendOutCom()
                    + "' and stateflag in ('0','7','8','9') ";
        LZCardSet tempLZCardSet = tLZCardDB.executeQuery(tSQL);
        if (tLZCardDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLZCardDB.mErrors);
            return null;
        }
        LZCardSet tLZCardSet = new LZCardSet();
        int nIndex = 0;
        while (nSumCount > 0 && nIndex < tempLZCardSet.size()) {
            LZCardSchema tLZCardSchema = tempLZCardSet.get(nIndex + 1);

            if (nSumCount <= tLZCardSchema.getSumCount()) {
                tLZCardSchema.setSumCount(nSumCount);
                tLZCardSchema.setEndNo(
                        bigIntegerPlus(tLZCardSchema.getStartNo(),
                                       String.valueOf(nSumCount - 1),
                                       getCertifyNoLen(tLZCardSchema.
                        getCertifyCode())));
            }
            nSumCount -= tLZCardSchema.getSumCount();

            LZCardSchema newLZCardSchema = new LZCardSchema();
            newLZCardSchema.setSchema(aLZCardSchema);
            newLZCardSchema.setStartNo(tLZCardSchema.getStartNo());
            newLZCardSchema.setEndNo(tLZCardSchema.getEndNo());
            newLZCardSchema.setSumCount(tLZCardSchema.getSumCount());

            tLZCardSet.add(newLZCardSchema);
            nIndex++;
        }
        if (nSumCount > 0) {
            buildError("findNo", "没有足够的单证");
            return null;
        }
        return tLZCardSet;
    }


    /**
     * 校验单证数据
     * @param aLZCardSchema
     * @return
     */
    public static String verifyNo(LZCardSchema aLZCardSchema) {
        System.out.println("verifyNo()..");
        int nNoLen = getCertifyNoLen(aLZCardSchema.getCertifyCode());
        if (aLZCardSchema.getStartNo() == null
            || aLZCardSchema.getStartNo().length() != nNoLen) {
            AuditCancelBL.ErrorState = "1";
            return "起始号长度不等于" + String.valueOf(nNoLen);
        }

        if (aLZCardSchema.getEndNo() == null
            || aLZCardSchema.getEndNo().length() != nNoLen) {
            AuditCancelBL.ErrorState = "1";
            return "终止号长度不等于" + String.valueOf(nNoLen);
        }

        try {
            new BigInteger(aLZCardSchema.getStartNo());
        } catch (Exception ex) {
            AuditCancelBL.ErrorState = "1";
            return aLZCardSchema.getStartNo() + "不是数字";
        }

        try {
            new BigInteger(aLZCardSchema.getEndNo());
        } catch (Exception ex) {
            AuditCancelBL.ErrorState = "1";
            return aLZCardSchema.getStartNo() + "不是数字";
        }

        if (bigIntegerDiff(aLZCardSchema.getStartNo(),
                           aLZCardSchema.getEndNo()) > 0) {
            AuditCancelBL.ErrorState = "2";
            return "输入的起始单证号不能大于终止单证号，请重新输入!";
        }

        return "";
    }

    /**
     * Zhangbin 2004-03-25
     * 得到单证是否需要校验发放到业务员
     * @param aLZCardSchema
     * @return 校验发放到业务员标志
     */
    protected static String verifyOperate(LZCardSchema aLZCardSchema) {
        // 检查单证状态表，看是否已经存在这样的数据
        String szSql = "";
        szSql = "SELECT * FROM LMCertifyDes WHERE CertifyCode = '";
        szSql += aLZCardSchema.getCertifyCode();
        szSql += "'";

        System.out.println(" zb SQL: " + szSql);

        LMCertifyDesDB dbLMCertifyDes = new LMCertifyDesDB();
        LMCertifyDesSet certifyDesSet = dbLMCertifyDes.executeQuery(szSql);
        if (certifyDesSet.size() == 0) {
            buildError("inputCertify", "没有此种的单证类型!");
            return null;
        }

        String verifyFlag = certifyDesSet.get(1).getVerifyFlag();
        if (verifyFlag == null) {
            return "";
        } else if (verifyFlag.equals("Y")) {
            return "Y"; //该类型单证需要业务员校验
        } else if (verifyFlag.equals("N")) {
            return "N"; //不需要业务员校验
        } else {
            return "";
        }
    }

    /**
     * 查询某一个代理人对于某一个单证的最大领用数
     * @param szAgentCode
     * @param szCertifyCode
     * @return
     */
    protected static int getMaxCount(String szAgentCode, String szCertifyCode) {
        //清空静态错误
        mErrors.clearErrors();

        LAAgentDB tLAAgentSDB = new LAAgentDB(); //个险代理人，即：代理人
        LAAgentSet tLAAgentSSet = new LAAgentSet(); //个险代理人
        String strSQL = "SELECT * FROM LAAgent WHERE AgentCode = '" + szAgentCode + "' AND BranchType='1'";
        tLAAgentSSet = tLAAgentSDB.executeQuery(strSQL);

        //按代理人编码查询最大数配置
        LDAgentCardCountDB tLDAgentCardCountDB = new LDAgentCardCountDB();
        tLDAgentCardCountDB.setAgentCode("Y");
        tLDAgentCardCountDB.setCertifyCode(szCertifyCode);
        tLDAgentCardCountDB.setAgentGrade("N");
        tLDAgentCardCountDB.setManageCom("N");

        if (tLDAgentCardCountDB.getInfo() && tLAAgentSSet != null && tLAAgentSSet.size() > 0) {
            return (int) tLDAgentCardCountDB.getMaxCount();
        }

        LAAgentDB tLAAgentGDB = new LAAgentDB(); //团险业务员，即：业务员
        LAAgentSet tLAAgentGSet = new LAAgentSet();
        strSQL = "SELECT * FROM LAAgent WHERE AgentCode = '" + szAgentCode + "' AND BranchType='2'";
        tLAAgentGSet = tLAAgentGDB.executeQuery(strSQL);

        //按业务员查询最大数配置
        tLDAgentCardCountDB = new LDAgentCardCountDB();
        tLDAgentCardCountDB.setAgentCode("N");
        tLDAgentCardCountDB.setCertifyCode(szCertifyCode);
        tLDAgentCardCountDB.setAgentGrade("Y");
        tLDAgentCardCountDB.setManageCom("N");

        if (tLDAgentCardCountDB.getInfo() && tLAAgentGSet != null && tLAAgentGSet.size() > 0) {
            return (int) tLDAgentCardCountDB.getMaxCount();
        }

        LAComDB tLAComDB = new LAComDB();//代理机构
        LAComSet tLAComSet = new LAComSet();
        strSQL = "SELECT * FROM LACom WHERE AgentCom = '" + szAgentCode + "'";
        tLAComSet = tLAComDB.executeQuery(strSQL);

        //按代理机构查询最大数配置
        tLDAgentCardCountDB = new LDAgentCardCountDB();
        tLDAgentCardCountDB.setAgentCode("N");
        tLDAgentCardCountDB.setCertifyCode(szCertifyCode);
        tLDAgentCardCountDB.setAgentGrade("N");
        tLDAgentCardCountDB.setManageCom("N");
        tLDAgentCardCountDB.setAgentCom("Y");

        if (tLDAgentCardCountDB.getInfo() && tLAComSet != null && tLAComSet.size() > 0) {
            return (int) tLDAgentCardCountDB.getMaxCount();
        }

        //如没有配置，则查询整个系统的默认配置
        tLDAgentCardCountDB.setAgentCode("*");
        tLDAgentCardCountDB.setCertifyCode("*");
        tLDAgentCardCountDB.setAgentGrade("*");
        tLDAgentCardCountDB.setManageCom("*");
        tLDAgentCardCountDB.setAgentCom("*");

        if (tLDAgentCardCountDB.getInfo()) {
            return (int) tLDAgentCardCountDB.getMaxCount();
        }

        buildError("getMaxCount", "缺少最大领用数的配置");
        return -1;
    }


    /**
     * Kevin 2003-06-03
     *
     * 如果数据库中有1到100号和101到200号单证。用户可以直接输入51到150号。
     * 在这个函数中，将用户输入的51到150号拆分成51到100号和101到150号单证。
     * @param aLZCardSchema
     * @return 返回空值，表示出错。
     */
    protected static String getRealNo(LZCardSchema aLZCardSchema,
                                      LZCardSet aLZCardSet) {
        System.out.println("getRealNo()!");
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return "查询LMCertifyDes数据错误";
        }
        String conttr = " AND StateFlag in ('0','7','8','9')";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            conttr = " AND StateFlag in ( '0','3','7','8')";
        }

        String strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '" +
                        aLZCardSchema.getCertifyCode() + "' AND ReceiveCom = '" +
                        aLZCardSchema.getSendOutCom() + "' AND StartNo <= '" +
                        aLZCardSchema.getEndNo() + "' AND EndNo >= '" +
                        aLZCardSchema.getStartNo() + "'" +
                        conttr +
                        " ORDER BY StartNo";
        System.out.println("zb strSQL: " + strSQL);
        LZCardDB tLZCardDB = new LZCardDB();
        LZCardSet tempLZCardSet = tLZCardDB.executeQuery(strSQL);

        if (tLZCardDB.mErrors.needDealError()) {
            return tLZCardDB.mErrors.getFirstError();
        }

        LZCardSchema tLZCardSchema = null;

        // 对第一个元素进行特殊处理
        if (tempLZCardSet.size() == 0) {
            return "在库存中，找不到单证号为" + aLZCardSchema.getStartNo() + "的单证";
        }

        tLZCardSchema = new LZCardSchema();

        tLZCardSchema.setSchema(aLZCardSchema);
        tLZCardSchema.setStartNo(tempLZCardSet.get(1).getStartNo());
        tLZCardSchema.setEndNo(tempLZCardSet.get(1).getEndNo());

        // 判断起始号
        if (bigIntegerDiff(tLZCardSchema.getStartNo(), aLZCardSchema.getStartNo()) >
            0) {
            return "在库存中，找不到单证号为" + aLZCardSchema.getStartNo() + "的单证";
        }

        tLZCardSchema.setStartNo(aLZCardSchema.getStartNo());

        // 判断终止号
        if (bigIntegerDiff(tLZCardSchema.getEndNo(),
                           aLZCardSchema.getEndNo()) > 0) {
            tLZCardSchema.setEndNo(aLZCardSchema.getEndNo());
            aLZCardSet.add(tLZCardSchema);
            return "";
        }

        // 将符合条件的记录放到返回的Set中
        aLZCardSet.add(tLZCardSchema);

        String strEndNo = tLZCardSchema.getEndNo(); // 记录下单证终止号
        int nNoLen = getCertifyNoLen(aLZCardSchema.getCertifyCode());

        for (int nIndex = 1; nIndex < tempLZCardSet.size(); nIndex++) {
            tLZCardSchema = new LZCardSchema();

            tLZCardSchema.setSchema(aLZCardSchema);
            tLZCardSchema.setStartNo(tempLZCardSet.get(nIndex + 1).getStartNo());
            tLZCardSchema.setEndNo(tempLZCardSet.get(nIndex + 1).getEndNo());

            if (bigIntegerDiff(tLZCardSchema.getStartNo(),
                               strEndNo) != 1) {
                break;
            }

            if (bigIntegerDiff(tLZCardSchema.getEndNo(),
                               aLZCardSchema.getEndNo()) > 0) {
                tLZCardSchema.setEndNo(aLZCardSchema.getEndNo());
            }

            strEndNo = tLZCardSchema.getEndNo();

            // 将符合条件的记录放到返回的Set中
            aLZCardSet.add(tLZCardSchema);
        }

        if (!strEndNo.equals(aLZCardSchema.getEndNo())) {
            AuditCancelBL.ErrorState = "1";
            return "在库存中，找不到单证号为" + bigIntegerPlus(strEndNo, "1", nNoLen) +
                    "的单证";
        } else {
            return "";
        }
    }

    /**
     * Kevin 2003-06-03
     *
     * 如果数据库中有1到100号和101到200号单证。用户可以直接输入51到150号。
     * 在这个函数中，将用户输入的51到150号拆分成51到100号和101到150号单证。
     * @param aLZCardSchema
     * @return 返回空值，表示出错。
     */
    protected static String getAuditCancelRealNo(LZCardSchema aLZCardSchema,
                                                 LZCardSet aLZCardSet) {

        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            return "单证描述表中不存在该单证";
        }

        System.out.println("getAuditCancelRealNo()");
        String strSQL = "";
        strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '" +
                 aLZCardSchema.getCertifyCode() + "' AND ReceiveCom = '" +
                 aLZCardSchema.getSendOutCom() + "' AND StartNo <= '" +
                 aLZCardSchema.getEndNo() + "' AND EndNo >= '" +
//                    aLZCardSchema.getStartNo() + "' AND StateFlag = '0'" +
                 aLZCardSchema.getStartNo() + "'" +
                 " ORDER BY StartNo";
        if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
            strSQL = "SELECT * FROM LZCard WHERE CertifyCode = '" +
                     aLZCardSchema.getCertifyCode() + "' AND (ReceiveCom = '" +
                     aLZCardSchema.getReceiveCom() + "' OR SendOutCom = '" +
                     aLZCardSchema.getReceiveCom() + "') AND StartNo <= '" +
                     aLZCardSchema.getEndNo() + "' AND EndNo >= '" +
//                    aLZCardSchema.getStartNo() + "' AND StateFlag = '0'" +
                     aLZCardSchema.getStartNo() + "'" +
                     " ORDER BY StartNo";
        }
        System.out.println("zb sql: " + strSQL);
        LZCardDB tLZCardDB = new LZCardDB();
        LZCardSet tempLZCardSet = tLZCardDB.executeQuery(strSQL);

        if (tLZCardDB.mErrors.needDealError()) {
            AuditCancelBL.ErrorState = "1";
            return tLZCardDB.mErrors.getFirstError();
        }

        LZCardSchema tLZCardSchema = null;

        // 对第一个元素进行特殊处理
        if (tempLZCardSet.size() == 0) {
            return "在库存中，找不到单证号为" + aLZCardSchema.getStartNo() + "的单证";
        }

        tLZCardSchema = new LZCardSchema();

        tLZCardSchema.setSchema(aLZCardSchema);
        tLZCardSchema.setStartNo(tempLZCardSet.get(1).getStartNo()); //得到数据库中符合条件的记录的起始号
        tLZCardSchema.setEndNo(tempLZCardSet.get(1).getEndNo());

        // 判断起始号
        if (bigIntegerDiff(tLZCardSchema.getStartNo(), aLZCardSchema.getStartNo()) >
            0) { //记录中起始号必须在录入起始号之前
            return "在库存中，找不到单证号为" + aLZCardSchema.getStartNo() + "的单证";
        }

        tLZCardSchema.setStartNo(aLZCardSchema.getStartNo());

        // 判断终止号
        if (bigIntegerDiff(tLZCardSchema.getEndNo(),
                           aLZCardSchema.getEndNo()) > 0) { //记录中终止号必须在录入终止号之后
            tLZCardSchema.setEndNo(aLZCardSchema.getEndNo());
            aLZCardSet.add(tLZCardSchema);
            return "";
        }

        // 将符合条件的记录放到返回的Set中
        aLZCardSet.add(tLZCardSchema);

        String strEndNo = tLZCardSchema.getEndNo(); // 记录下单证终止号
        int nNoLen = getCertifyNoLen(aLZCardSchema.getCertifyCode());

        for (int nIndex = 1; nIndex < tempLZCardSet.size(); nIndex++) {
            tLZCardSchema = new LZCardSchema();

            tLZCardSchema.setSchema(aLZCardSchema);
            tLZCardSchema.setStartNo(tempLZCardSet.get(nIndex + 1).getStartNo());
            tLZCardSchema.setEndNo(tempLZCardSet.get(nIndex + 1).getEndNo());

            if (bigIntegerDiff(tLZCardSchema.getStartNo(),
                               strEndNo) != 1) {
                break;
            }

            if (bigIntegerDiff(tLZCardSchema.getEndNo(),
                               aLZCardSchema.getEndNo()) > 0) {
                tLZCardSchema.setEndNo(aLZCardSchema.getEndNo());
            }

            strEndNo = tLZCardSchema.getEndNo();

            // 将符合条件的记录放到返回的Set中
            aLZCardSet.add(tLZCardSchema);
        }

        if (!strEndNo.equals(aLZCardSchema.getEndNo())) {
            return "在库存中，找不到单证号为" + bigIntegerPlus(strEndNo, "1", nNoLen) +
                    "的单证";
        } else {
            return "";
        }
    }

    /**
     * Kevin 2003-03-24
     * 拆分单证（结算单录入）
     * aLZCardPaySchema : 单证记录。表示要发放的单证信息
     * vResult : 返回的结果集。其中第一个元素是要拆分的setLZCardPay的信息
     */
    protected static boolean splitPayInput(LZCardPaySchema aLZCardPaySchema,
                                           VData vResult) {

        System.out.println("正式拆分:========   单证类型: " +
                           aLZCardPaySchema.getCardType() + "  单证持有机构: " +
                           aLZCardPaySchema.getManageCom())
                ;
        System.out.println("起始号：" + aLZCardPaySchema.getStartNo() + "  终止号：" +
                           aLZCardPaySchema.getEndNo());

        vResult.clear();
        vResult.add(0, null);

        LZCardPaySet setLZCardPay = new LZCardPaySet();
        int intStartNo = Integer.parseInt(aLZCardPaySchema.getStartNo().trim());
        int intEndNo = Integer.parseInt(aLZCardPaySchema.getEndNo().trim());
        for (int i = intStartNo; i <= intEndNo; i++) {

            String strCurSerNo = String.valueOf(i);
            strCurSerNo = PubFun.LCh(strCurSerNo, "0", 7);
            aLZCardPaySchema.setStartNo(strCurSerNo);
            aLZCardPaySchema.setEndNo(strCurSerNo);
            aLZCardPaySchema.setMakeDate(PubFun.getCurrentDate());
            aLZCardPaySchema.setMakeTime(PubFun.getCurrentTime());
            aLZCardPaySchema.setModifyDate(PubFun.getCurrentDate());
            aLZCardPaySchema.setModifyTime(PubFun.getCurrentTime());
            setLZCardPay.add(aLZCardPaySchema);
            vResult.set(0, setLZCardPay);
        }
        return true;
    }

    /**
     * dealDivMoney
     * 结算单录入确认时拆分保费
     * @param setLZCardPay LZCardPaySet
     * @param i int
     * @param d double
     * @return boolean
     */
    protected static boolean dealDivMoney(LZCardPaySet setLZCardPay, int i,
                                          double tMoney) {
        /** 进行费用拆分 */
        double sumPay = tMoney;
        System.out.println("总金额 : " + sumPay);
        System.out.println("总件数 : " + i);

        double perPay = Arith.div(sumPay, i, 5);
        System.out.println("每人费用为 : " + perPay);

        if (Math.abs(Arith.round(Arith.round(perPay * i, 5) - sumPay, 5)) > 0) {
            String str = "分费用错误,原因是总费用与拆分后的费用不符!";
            buildError("dealDivMoney", str);
            System.out.println("在程序CertifyPayBL.dealDivMoney() - 177 : " + str);
            return false;
        }
        for (int m = 1; m <= setLZCardPay.size(); m++) {
            setLZCardPay.get(m).setActPayMoney(perPay);
        }
        return true;
    }

    /**
     * splitDivMoney
     * 结算单录入时拆分保费
     * @param setLZCardPay LZCardPaySet
     * @param i int
     * @param d double
     * @return boolean
     */
    protected static boolean splitDivMoney(LZCardPaySet setLZCardPay,
                                           double tMoney) {
        /** 进行费用拆分 */
        double sumPay = tMoney;
        System.out.println("总金额 : " + sumPay);
        int sumCont = 0;
        for (int j = 1; j <= setLZCardPay.size(); j++) {
            sumCont += setLZCardPay.get(j).getSumCount();
        }
        System.out.println("总件数 : " + sumCont);

        double perPay = Arith.div(sumPay, sumCont, 5);
        System.out.println("每人费用为 : " + perPay);

        if (Math.abs(Arith.round(Arith.round(perPay * sumCont, 5) - sumPay, 5)) >
            0) {
            String str = "结算单录入时分费用错误,原因是总费用与拆分后的费用不符!";
            buildError("dealDivMoney", str);
            System.out.println("在程序CertifyPayBL.dealDivMoney() : " + str);
            return false;
        }
        for (int m = 1; m <= setLZCardPay.size(); m++) {

            int curCnt = setLZCardPay.get(m).getSumCount();
            double curPay = Arith.round(perPay * curCnt, 5);
            setLZCardPay.get(m).setActPayMoney(curPay);
        }
        return true;
    }

    /**
     * chkRiskMoney
     * 校验录入的总结算金额是否符合险种的保费
     * @param setLZCardPay LZCardPaySet
     * @param i int
     * @param d double
     * @return boolean
     */
    protected static boolean chkRiskMoney(LZCardPaySet setLZCardPay, int i,
                                          double tMoney) {

        /** 进行费用拆分 */
        double sumPay = tMoney;
        int sumCnt = getCardCount(setLZCardPay);
        System.out.println("总金额 : " + sumPay);
        System.out.println("总件数 : " + sumCnt);
        LZCardPaySchema schemaLZCardPay = new LZCardPaySchema();
        schemaLZCardPay = setLZCardPay.get(1);
        String strSql = "select sum(a.Prem) from LMCardRisk a,LMCertifyDes b"
                        + " where a.CertifyCode =b.CertifyCode and b.SubCode='"
                        + schemaLZCardPay.getCardType()
                        + "'"
                        ;
        ExeSQL tExeSQL = new ExeSQL();

        String strSumPay = tExeSQL.getOneValue(strSql);

        if (tExeSQL.mErrors.needDealError()) {

            buildError("dealDivMoney", "查询险种保费错误");
            System.out.println("在程序CertifyFunc.chkRiskMoney() : 查询险种保费错误");
            return false;
        }
        double sumRiskPay = 0;
        if (strSumPay == null || strSumPay.equals("")) {
            sumRiskPay = 0;
        } else {
            sumRiskPay = Double.parseDouble(strSumPay);
        }
        System.out.println(sumRiskPay + ":" + sumCnt);
        sumRiskPay = Arith.round(sumRiskPay * sumCnt, 2);
        sumPay = Arith.round(sumPay, 2);

        if (Math.abs(Arith.round(sumRiskPay - sumPay, 2)) > 0) {
            String str = "录入结算金额错误,与险种保费不同!";
            System.out.println("在程序CertifyFunc.chkRiskMoney() : " + str);
            return false;
        }

        return true;
    }

    /**
     * chkRiskMoney
     * 校验是否录入的金额是否符合险种的保费
     * @param setLZCardPay LZCardPaySet
     * @param i int
     * @param d double
     * @return boolean
     */
    protected static int getCardCount(LZCardPaySet setLZCardPay) {
        /** 进行费用拆分 */
        int sumCount = 0;

        for (int i = 1; i <= setLZCardPay.size(); i++) {
            LZCardPaySchema schemaLZCardPay = new LZCardPaySchema();
            schemaLZCardPay = setLZCardPay.get(i);
            sumCount += schemaLZCardPay.getSumCount();
        }
        return sumCount;
    }

    /**
     * chkSendOut
     * 校验录入的卡单是否下发
     * @param schemaLZCardPay LZCardPaySchema
     * @return boolean
     */
    protected static boolean chkSendOut(LZCardPaySchema schemaLZCardPay) {

        /** 进行费用拆分 */
        String strStartNo = String.valueOf(schemaLZCardPay.getStartNo());
        String strEndNo = String.valueOf(schemaLZCardPay.getEndNo());
        strStartNo = PubFun.LCh(strStartNo, "0", 7);
        strEndNo = PubFun.LCh(strEndNo, "0", 7);

        String strSql = "select count(*) from LZCard a"
                        + " where a.SubCode='"
                        + schemaLZCardPay.getCardType()
                        + "' and State in ('10','11') and (StartNo <='"
                        + strEndNo
                        + "' and StartNo >='" + strStartNo
                        + "')"
                        ;
        ExeSQL tExeSQL = new ExeSQL();
        String strCnt = tExeSQL.getOneValue(strSql);
        if (tExeSQL.mErrors.needDealError()) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 查询已发放数量错误");
            return false;
        }
        if (strCnt == null || strCnt.equals("0")) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 不存在对应的已下发的单证");
            return false;
        }
        String strSumCnt = schemaLZCardPay.getSumCount() + "";
        if (!strCnt.equals(strSumCnt)) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 和下发的单证数量不相等");
            return false;
        }

        return true;
    }

    /**
     * chkAgentCom
     * 校验录入的卡单与下发的
     * @param schemaLZCardPay LZCardPaySchema
     * @return boolean
     */
    protected static boolean chkAgentCom(LZCardPaySchema schemaLZCardPay) {

        /** 进行费用拆分 */
        String strStartNo = String.valueOf(schemaLZCardPay.getStartNo());
        String strEndNo = String.valueOf(schemaLZCardPay.getEndNo());
        strStartNo = PubFun.LCh(strStartNo, "0", 7);
        strEndNo = PubFun.LCh(strEndNo, "0", 7);

        String strSql = "select distinct  ReceiveCom from LZCard a"
                        + " where a.SubCode='"
                        + schemaLZCardPay.getCardType()
                        + "' and State in ('10','11','12') and (StartNo <='"
                        + strEndNo
                        + "' and StartNo >='" + strStartNo
                        + "') order by ReceiveCom desc"
                        ;
        ExeSQL tExeSQL = new ExeSQL();
        String strCom = tExeSQL.getOneValue(strSql);
        if (tExeSQL.mErrors.needDealError()) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 查询已发放代理机构错误");
            return false;
        }
        if (strCom == null || strCom.equals("")) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 不存在对应的已下发的单证");
            return false;
        }
        if (strCom.substring(0, 1).equals("D")) {
            return true;
        }
        String strSumCom = "E" + schemaLZCardPay.getAgentCom();
        if (!strCom.equals(strSumCom)) {
            System.out.println("在程序CertifyFunc.chkSendOut() : " + strCom +
                               "和下发的单证的代理机构不相等");
            return false;
        }

        return true;
    }

    /**
     * chkAgentCom
     * 校验录入的卡单与下发的
     * @param schemaLZCardPay LZCardPaySchema
     * @return boolean
     */
    protected static boolean chkAgentComExist(LZCardPaySchema schemaLZCardPay) {

        /** 进行费用拆分 */
        String strStartNo = String.valueOf(schemaLZCardPay.getStartNo());
        String strEndNo = String.valueOf(schemaLZCardPay.getEndNo());
        strStartNo = PubFun.LCh(strStartNo, "0", 7);
        strEndNo = PubFun.LCh(strEndNo, "0", 7);

        String strSql = "select distinct  ReceiveCom from LZCard a"
                        + " where a.SubCode='"
                        + schemaLZCardPay.getCardType()
                        + "' and State in ('10','11','12') and (StartNo <='"
                        + strEndNo
                        + "' and StartNo >='" + strStartNo
                        + "') order by ReceiveCom desc"
                        ;
        ExeSQL tExeSQL = new ExeSQL();
        String strCom = tExeSQL.getOneValue(strSql);
        if (tExeSQL.mErrors.needDealError()) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 查询已发放代理机构错误");
            return false;
        }
        if (strCom == null || strCom.equals("")) {
            System.out.println("在程序CertifyFunc.chkSendOut() : 不存在对应的已下发的单证");
            return false;
        }
        if (strCom.substring(0, 1).equals("D") &&
            !schemaLZCardPay.getAgentCom().equals("") &&
            schemaLZCardPay.getAgentCom() != null) {
            System.out.println("在程序CertifyFunc.chkSendOut() : " + strCom +
                               "未发到代理机构");
            return false;

        }

        return true;
    }

    /**
     * 普通单证根据下发的机构修改单证状态（三级机构/部门单证管理员-7、业务员、代理人、代理机构-8）
     * @param aReceiveCom String
     * @return String
     */
    private static String getCardState(String aReceiveCom) {
        if (aReceiveCom.substring(0,1).equals("A")&&aReceiveCom.length()==9
            || aReceiveCom.substring(0,1).equals("B")) {
            return "7";
        } else if (aReceiveCom.substring(0,1).equals("D")||aReceiveCom.substring(0,1).equals("E")) {
            return "8";
        } else {
            return "0";
        }
    }

    /**
     * 传入单证类型、起始号、增加量，返回终止号
     * @param aLZCardSchema LZCardSchema
     * @param nNoLen int
     * @return String
     */
    public String getCertifyCode(LZCardSchema aLZCardSchema,int nNoLen){
    int tNoLen = getCertifyNoLen(aLZCardSchema.getCertifyCode());
        String tEndNo = bigIntegerPlus(aLZCardSchema.getStartNo(), ""+nNoLen, tNoLen);
        return tEndNo;
    }
}
