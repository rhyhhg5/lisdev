package com.sinosoft.lis.certify;

import com.sinosoft.lis.operfee.GrpDueFeeUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ECCardActiveUI {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误容器
    private VData mInputData = new VData(); //存储数据
    private VData mResult = new VData(); //返回结果
    private String mOperate; //操作类型

    public ECCardActiveUI() {
    }

//操作的提交方法，作为页面数据的入口，准备完数据后执行入库操作。
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        ECCardActiveBL tECCardActiveBL = new ECCardActiveBL();
        System.out.println("---CertifyNumberUI  BEGIN---");
        if (tECCardActiveBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tECCardActiveBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportUI";
            tError.functionName = "submitData";
            this.mErrors.addOneError(tECCardActiveBL.mErrors.getErrContent());
            mResult.clear();
            return false;
        } 
        return true;
    }


}
