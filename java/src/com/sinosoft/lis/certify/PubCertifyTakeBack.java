/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * hzm 用来回收单证的公用类(已用于暂加费核销和投保单核销)
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hzm
 * @version 1.0
 */

public class PubCertifyTakeBack
{
    //定已单证类型的常量
    public final String CERTIFY_Proposal = "1101"; //个人投保单
    public final String CERTIFY_ProposalBank = "1501"; //银行险
    public final String CERTIFY_GrpProposal = "1201"; //集体投保单
    public final String CERTIFY_ProSpec = "1401"; //定额投保单
    public final String CERTIFY_ProSimple = "1601"; //简易投保单
    public final String CERTIFY_TempFee = "10004"; //暂交费收据

    public final String CERTIFY_CheckNo1 = "1"; //验证暂交费收据号
    public final String CERTIFY_CheckNo2 = "2"; //验证投保单印刷号码


    //错误类：发生错误可直接返回该变量
    public CErrors mErrors = new CErrors();

    public PubCertifyTakeBack()
    {
    }

    public static void main(String[] args)
    {
        PubCertifyTakeBack tPubCertifyTakeBack = new PubCertifyTakeBack();
//    tPubCertifyTakeBack.TakeBackLCPol();
//    tPubCertifyTakeBack.TakeBackLCGrpPol();
//    tPubCertifyTakeBack.TakeBackTempfee();
        tPubCertifyTakeBack.CertifyTakeBack_A("0000000004", "0000000004",
                                              "10004", "ac");

    }


    /**
     * 单证回收：适用于只需要以下参数即可的情况
     * @param StartCertifyNo 单证起始号 (如果只有一个号，那么起始和终止号相同)
     * @param EndCertifyNo   单证终止号
     * @param CertifyType    单证类型
     * @param Operate        操作员
     * @return
     */
    public boolean CertifyTakeBack_A(String StartCertifyNo, String EndCertifyNo,
                                     String CertifyType, String Operate)
    {
        LZCardSet setLZCard = new LZCardSet();
        LZCardSchema schemaLZCard = new LZCardSchema();
        schemaLZCard.setStartNo(StartCertifyNo);
        schemaLZCard.setEndNo(EndCertifyNo);
        schemaLZCard.setCertifyCode(CertifyType);
        schemaLZCard.setOperator(Operate);
        setLZCard.add(schemaLZCard);

        VData vData = new VData();
        vData.addElement(setLZCard);
        CertTakeBackUI tCertTakeBackUI = new CertTakeBackUI();
        if (!tCertTakeBackUI.submitData(vData, "TAKEBACK"))
        {
            mErrors.copyAllErrors(tCertTakeBackUI.mErrors);
            System.out.println(tCertTakeBackUI.mErrors.getFirstError());
            return false;
        }
        return true;
    }


    /**
     * 是否检验单证
     * @return
     */
    public boolean CheckNewType()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("CheckNewType");
        tLDSysVarDB.setSysVarValue("1");
        LDSysVarSet tLDSysVarSet = new LDSysVarSet();
        tLDSysVarSet = tLDSysVarDB.query();
        if (tLDSysVarDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLDSysVarDB.mErrors);
            return false;
        }
        if (tLDSysVarSet.size() == 0)
            return false;

        return true;
    }


    /**
     * 是否检验单证
     * @return
     */
    public boolean CheckNewType(String CerType)
    {
        //查询凡是同一个收据号的纪录
        String sqlStr =
                "select * from LDSysVar where SysVar='CheckNewType' and (SysVarValue='" +
                CerType + "' or SysVarValue='3')";
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        LDSysVarSet tLDSysVarSet = new LDSysVarSet();

        tLDSysVarSet = tLDSysVarDB.executeQuery(sqlStr);
        if (tLDSysVarDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLDSysVarDB.mErrors);
            return false;
        }
        if (tLDSysVarSet.size() == 0)
            return false;

        return true;
    }


    /**
     * 回收所有没有回收的暂交费单证
     * @return
     */
    public boolean TakeBackTempfee()
    {
        //1 查询所有未回收的暂交费单证
        String strSQL = "select distinct ljtempfee.tempfeeno,ljtempfee.TempFeeType,ljtempfee.Operator from ljtempfee, lzcard where 1=1 ";
        strSQL = strSQL +
                 " and tempfeeno between lzcard.startno and lzcard.endno ";
        strSQL = strSQL + " and lzcard.receivecom like 'D%'";
        strSQL = strSQL + " and lzcard.certifycode in ('3201', '1401')";

        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(strSQL);

        //2 循环，判断暂交费类型，调用相应的回收程序
        String tempfeeno = "";
        String operator = "";
        String tempfeetype = "";
        String takebackType = "";
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            tempfeeno = tSSRS.GetText(i, 1);
            tempfeetype = tSSRS.GetText(i, 2);
            operator = tSSRS.GetText(i, 3);

            if (tempfeetype.equals("6"))
            { //定额单
                takebackType = CERTIFY_ProSpec;
            }
            else
            {
                //普通单证
                        takebackType = CERTIFY_TempFee;
            }
            System.out.println("开始回收");
            if (!CertifyTakeBack_A(tempfeeno, tempfeeno, takebackType, operator))
            {
                System.out.println("单证回收错误（" + i + "）:" + tempfeeno);
                System.out.println("错误原因：" + mErrors.getFirstError().toString());
            }
        }
        return true;
    }


    /**
     * 回收所有没有回收的个人投保单单证
     * @return
     */
    public boolean TakeBackLCPol()
    {
        //1 查询所有未回收的投保单单证(包括已经签单的)
        String strSQL =
                "select distinct lcpol.prtno, lcpol.Operator from lcpol, lzcard where 1=1 ";
        strSQL = strSQL + " and lcpol.prtno like '861101%' ";
        strSQL = strSQL +
                 " and lcpol.prtno between lzcard.startno and lzcard.endno";
        strSQL = strSQL + " and lzcard.receivecom like 'D%'";
        strSQL = strSQL + " and lzcard.certifycode = '1101'";

        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(strSQL);
        String prtno = "";
        String operator = "";
        String takebackType = "";
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            prtno = tSSRS.GetText(i, 1);
            operator = tSSRS.GetText(i, 2);

            if (prtno.substring(2, 4).equals("15"))
            { //银行险的编码15，个险11，团险12,定额单
                takebackType = CERTIFY_ProposalBank;
            }
            else
            {
                if (prtno.substring(2, 4).equals("14"))
                {
                    takebackType = CERTIFY_ProSpec;
                }
                else if (prtno.substring(2, 4).equals("16"))
                {
                    takebackType = CERTIFY_ProSimple;
                }
                else
                {
                    takebackType = CERTIFY_Proposal;
                }
            }
            if (!CertifyTakeBack_A(prtno, prtno, takebackType, operator))
            {
                System.out.println("单证回收错误（个人投保单）:" + prtno);
                System.out.println("错误原因：" + mErrors.getFirstError().toString());
            }
        }

        return true;
    }


    /**
     * 回收所有没有回收的集体投保单单证
     * @return
     */
    public boolean TakeBackLCGrpPol()
    {
        //1 查询所有未回收的投保单单证(包括已经签单的)
        String strSQL = "select distinct lcgrppol.prtno, lcgrppol.Operator from lcgrppol, lzcard where 1=1 ";
        strSQL = strSQL + " and lcgrppol.prtno like '861201%' ";
        strSQL = strSQL +
                 " and lcgrppol.prtno between lzcard.startno and lzcard.endno";
        strSQL = strSQL + " and lzcard.receivecom like 'D%'";
        strSQL = strSQL + " and lzcard.certifycode = '1201'";

        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(strSQL);
        String prtno = "";
        String operator = "";
        String takebackType = "";
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            prtno = tSSRS.GetText(i, 1);
            operator = tSSRS.GetText(i, 2);
            takebackType = CERTIFY_GrpProposal;
            if (!CertifyTakeBack_A(prtno, prtno, takebackType, operator))
            {
                System.out.println("单证回收错误（集体投保单）:" + prtno);
                System.out.println("错误原因：" + mErrors.getFirstError().toString());
            }
        }
        return true;
    }

}
