package com.sinosoft.lis.certify;

import java.sql.Connection;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LZCardNumberDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.schema.LZCardNumberSchema;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 单证号码生成</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */
public class CertifyNumberBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap map = new MMap();
    /** 数据库连接  **/
    private Connection mConn = null; //Added For LargeData Disposal At 2008.08.12

    /** 往界面传输数据的容器 */
    private VData mResult = new VData(); //返回结果
    private LZCardNumberSet mLZCardNumberSet = new LZCardNumberSet();
    private LZCardNumberSet mOldLZCardNumberSet = new LZCardNumberSet();
    private String CurrentDate = PubFun.getCurrentDate(); //当前时间
    private String CurrentTime = PubFun.getCurrentTime(); //当前时间
    private TransferData mTransferData = new TransferData(); //存储接受数据
    private GlobalInput mGI = new GlobalInput(); //存储用户信息
    private String mCardType = ""; //单证类型
    private String mCardSerNo = ""; //单证起始流水号
    private String mCardNum = ""; //单证数量
    private String mPrtNo = ""; //批次号
    private String mPageType = ""; //0:编码定义页；1：重复下载页
    private String mOperate = ""; //单证数量
    private Log log = LogFactory.getLog(this.getClass().getName());//日志

    public CertifyNumberBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
	{
		log.info("CertifyNumberBL  submitData  start ...");

		// 创建数据库连接 Added For LargeData Disposal At 2008.08.12 Begin....
		if (mConn == null)
		{
			mConn = DBConnPool.getConnection();
		}
		if (mConn == null)
		{
			// @@错误处理
			CError.buildErr(this, "数据库连接失败");
			return false;
		}
		// 创建数据库连接 Added For LargeData Disposal At 2008.08.12 End....

		try
		{
			// 将操作数据拷贝到本类中
			mInputData = (VData) cInputData.clone();
			if (!getInputData(cInputData))
			{
				return false;
			}
			//修改印刷号在最大号表中的值,以免再下次生成时重复
			if(!addMaxPrtNo(mPrtNo)){
				return false;
			}
			
			// 数据操作业务处理
			log.info("---dealData---");
			if (!getOldData())
			{
				return false;
			}
			// 单证编码定义（包括重复下载）
			if (mOperate.equals("new"))
			{
				if (!dealData())
				{
					return false;
				}
				if (!prepareData())
				{
					return false;
				}
			}
			if (mOperate.equals("delete"))
			{
				if (!deleteData())
				{
					return false;
				}
			}
			if (mOperate.equals("confirm"))
			{
				if (!confirmData())
				{
					return false;
				}
			}

			PubSubmit tPubSubmit = new PubSubmit();

			// 统一提交数据库 Added For LargeData Disposal At 2008.08.12 begin
			if (mConn == null)
			{// 校验连接是否还在
				// @@错误处理
				CError.buildErr(this, "数据库连接失败");
				return false;
			}
			tPubSubmit.setConnection(mConn);// 传入连接
			tPubSubmit.setCommitFlag(true);// 传入提交方式
			// 统一提交数据库 Added For LargeData Disposal At 2008.08.12 end

			if (tPubSubmit.submitData(mInputData, "") == false)
			{

				this.mErrors.addOneError("PubSubmit:处理数据库失败!");
				return false;
			}
		}
		catch (Exception ex)
		{
			// @@异常或者错误处理
			ex.printStackTrace();
			CError.buildErr(this, ex.toString());
			try
			{
				mConn.rollback();
				mConn.close();
			}
			catch (Exception x)
			{}
			return false;
		}
		finally
		{// 释放没有释放的数据库连接
			try
			{
				if (mConn != null && !mConn.isClosed())
				{
					mConn.close();
				}
			}
			catch (Exception x)
			{}
		}
		return true;
	}

    public VData getResult() {
        return mResult;
    }

    private boolean addMaxPrtNo( String sPrtNo){
    	try{
            PubSubmit tPubsubmit= new PubSubmit();
            MMap tmap = new MMap();
            VData cInputData = new VData();
            String upSql =" update ldmaxno set Maxno=Maxno+1 where Notype='CERTIFYPRTNO' ";
            tmap.put(upSql, "UPDATE");
            cInputData.add(tmap);
            tPubsubmit.submitData(cInputData,"");
    	}catch(Exception e){
    		return false;
    	}
    	return true;
    }
    /**
	 * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
    private boolean dealData() {
        log.info("CertifyNumberBL  dealData  start ...");

        String strContr = "";
        mLZCardNumberSet.clear();
        mInputData.clear();
        
        //查询单证描述
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setSubCode(mCardType);
        LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.query();
        if(tLMCertifyDesSet.size() != 1)
        {
            this.mErrors.addOneError("查询单证描述失败！");
            return false;
        }
        LMCertifyDesSchema tLMCertifyDesSchema = tLMCertifyDesSet.get(1);
        String checkRule = tLMCertifyDesSchema.getCheckRule();//校验规则：1-有校验，2无校验
        String operateType = tLMCertifyDesSchema.getOperateType();//单证类型：0-卡单业务，1-撕单业务
        
        if (mPageType.equals("0")) {//单证号码生成
            int intSerNo = Integer.parseInt(mCardSerNo);
            int intNum = Integer.parseInt(mCardNum);
            LisIDEA tLisIDEA = new LisIDEA();
            for (int i = 0; i < intNum; i++) {
                String strCurSerNo = String.valueOf(intSerNo + i);
                
                //根据单证长度填充单证码
                int certifyLength = (int) tLMCertifyDesSchema.getCertifyLength();
                strCurSerNo = PubFun.LCh(strCurSerNo, "0", certifyLength);
                
                String strCardSer = mCardType + strCurSerNo;
                String checkNo = "00";
                String cardNo = "";
                //有校验规则的单证生成单证校验码
                if("1".equals(checkRule))
                {
                    strContr = strCardSer + strCardSer.substring(0, 8);
                    checkNo = GetVerifyBit(strContr);
                    cardNo = strCardSer + checkNo;
                }
                else
                {
                    cardNo = strCardSer;
                }
                
                //单证密码生成
                String strPass = "";
                String strPassword = "";
                if (operateType.equals("0")||operateType.equals("2")||operateType.equals("4")) {
                    strPass = genRandomNum(6);
                    StringBuffer sqlSB = new StringBuffer();
                    sqlSB.append(String.valueOf(strPass.charAt(3)))
                            .append(String.valueOf(strPass.charAt(0)))
                            .append(String.valueOf(strPass.charAt(4)))
                            .append(String.valueOf(strPass.charAt(1)))
                            .append(String.valueOf(strPass.charAt(5)))
                            .append(String.valueOf(strPass.charAt(2)))
                            ;
                    strPassword = sqlSB.toString();
                }
                strPass = tLisIDEA.encryptString(strPass);

                LZCardNumberSchema tLZCardNumberSchema = new LZCardNumberSchema();
                tLZCardNumberSchema.setCardChkNo(checkNo);
                tLZCardNumberSchema.setPrtNo(mPrtNo);
                tLZCardNumberSchema.setCardNo(cardNo);
                tLZCardNumberSchema.setOperateType(operateType);
                tLZCardNumberSchema.setOperator(mGI.Operator);
                tLZCardNumberSchema.setCardPassword(strPass);
                tLZCardNumberSchema.setCardPassword2(strPassword);
                tLZCardNumberSchema.setCardSerNo(strCurSerNo);
                tLZCardNumberSchema.setCardType(mCardType);
                tLZCardNumberSchema.setMakeDate(CurrentDate);
                tLZCardNumberSchema.setMakeTime(CurrentTime);
                tLZCardNumberSchema.setModifyDate(CurrentDate);
                tLZCardNumberSchema.setModifyTime(CurrentTime);
                tLZCardNumberSchema.setPrintFlag("0");
                mLZCardNumberSet.add(tLZCardNumberSchema);

                //超过5000条记录的set则每5000条记录写入数据库但不提交  Added For LargeData Disposal At 2008.08.12  Begin....
                if(mLZCardNumberSet.size()== SysConst.FETCHCOUNT)
                {
                	MMap tMap = new MMap();
                	VData tVData = new VData();
                	tMap.put(mLZCardNumberSet, "INSERT");
                    tVData.add(tMap);

                    PubSubmit tPubSubmit = new PubSubmit();
                    tPubSubmit.setConnection(mConn);//传入连接
                    tPubSubmit.setCommitFlag(false);//传入提交方式
                    if (tPubSubmit.submitData(tVData, "") == false) {
                        this.mErrors.addOneError("PubSubmit:处理数据库失败!");
                        return false;
                    }

                	mLZCardNumberSet.clear();
                }
                //超过5000条记录的set则每5000条记录写入数据库但不提交  Added For LargeData Disposal At 2008.08.12  End....
            }

        } else if (mPageType.equals("1")) {//重复下载类
            if (!getOldData()) {
                return false;
            }
            if (!deleteData()) {
                return false;
            }
            LisIDEA tLisIDEA = new LisIDEA();
            for (int i = 1; i <= mOldLZCardNumberSet.size(); i++) {
                LZCardNumberSchema tLZCardNumberSchema = new LZCardNumberSchema();
                tLZCardNumberSchema = mOldLZCardNumberSet.get(i);
                String strPass = "";
                String strPassword = "";
                if (operateType.equals("0")||operateType.equals("2")) {
                    strPass = genRandomNum(6);
                    StringBuffer sqlSB = new StringBuffer();
                    sqlSB.append(String.valueOf(strPass.charAt(3)))
                            .append(String.valueOf(strPass.charAt(0)))
                            .append(String.valueOf(strPass.charAt(4)))
                            .append(String.valueOf(strPass.charAt(1)))
                            .append(String.valueOf(strPass.charAt(5)))
                            .append(String.valueOf(strPass.charAt(2)))
                            ;
                    strPassword = sqlSB.toString();
                }
                strPass = tLisIDEA.encryptString(strPass);
                tLZCardNumberSchema.setCardPassword(strPass);
                tLZCardNumberSchema.setCardPassword2(strPassword);
                tLZCardNumberSchema.setOperator(mGI.Operator);
                tLZCardNumberSchema.setModifyDate(CurrentDate);
                tLZCardNumberSchema.setModifyTime(CurrentTime);
                mLZCardNumberSet.add(tLZCardNumberSchema);
            }
        }

        log.info("CertifyNumberBL End deel date ... ");
        return true;
    }

    /**
     * 接收从CertifyDescSave.jsp中传递的数据
     */
    private boolean getInputData(VData cInputData) {
        log.info("CertifyNumberBL  getInputData  start ...");
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0); //传输页面输入的起止时间
        mGI = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0); //传输页面输入的起止时间
        if (mTransferData == null || mGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        mCardType = (String) mTransferData.getValueByName("CardType");
        mCardSerNo = (String) mTransferData.getValueByName("CardSerNo");
        mCardNum = (String) mTransferData.getValueByName("CardNum");
        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        mPageType = (String) mTransferData.getValueByName("PageType");
        mOperate = (String) mTransferData.getValueByName("Operate");

        return true;
    }

    // 得到重复下载已存在的数据
    public boolean getOldData() {
        log.info("CertifyNumberBL  getOldData  start ...");

        LZCardNumberDB tLZCardnumberDB = new LZCardNumberDB();
        String sqlCard = "select * from lzcardnumber where cardtype='"
                         + mCardType
                         + "' and prtno ='"
                         + mPrtNo
                         + "'";
        mOldLZCardNumberSet = tLZCardnumberDB.executeQuery(sqlCard);
        if (!(mPageType.equals("0") && mOperate.equals("new")) &&
            mOldLZCardNumberSet == null && mOldLZCardNumberSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    // 为更新数据库准备数据
    public boolean deleteData() {
        map.put(mOldLZCardNumberSet, "DELETE");
        mInputData.add(map);
        return true;
    }

    // 为更新数据库准备数据
    public boolean confirmData() {
        mInputData.clear();
        for (int i = 1; i <= mOldLZCardNumberSet.size(); i++) {
            mOldLZCardNumberSet.get(i).setPrintFlag("1");
            mOldLZCardNumberSet.get(i).setCardPassword2(null);
        }
        map.put(mOldLZCardNumberSet, "UPDATE");
        mInputData.add(map);
        return true;
    }

    // 为更新数据库准备数据
    public boolean prepareData() {
        map.put(mLZCardNumberSet, "INSERT");
        mInputData.add(map);
        return true;
    }

    //原理:
//∑(a[i]*W[i]) mod 11 ( i = 2, 3, ..., 18 )(1)
//"*" 表示乘号
//i--------表示身份证号码每一位的序号，从右至左，最左侧为18，最右侧为1。
//a[i]-----表示身份证号码第 i 位上的号码
//W[i]-----表示第 i 位上的权值 W[i] = 2^(i-1) mod 11
//计算公式 (1) 令结果为 R
//根据下表找出 R 对应的校验码即为要求身份证号码的校验码C。
//R 0 1 2 3 4 5 6 7 8 9 10
//C 1 0 X 9 8 7 6 5 4 3 2
// X 就是 10，罗马数字中的 10 就是 X
//15位转18位中,计算校验位即最后一位

    private String GetVerifyBit(String id) {
        String result = "";

        int multiply[] = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        int nNum = 0;
        for(int i = 0; i < multiply.length; i++)
        {
            int num = 0;
            if("0123456789".indexOf(id.substring(i, i + 1)) != -1)
            {
                num = Integer.parseInt(id.substring(i, i + 1));
            }
            else
            {
                num = 0;
            }
            nNum += num * multiply[i];
        }
        nNum = nNum % 11;
        switch (nNum) {
        case 0:
            result = "20"; //20
            break;
        case 1:
            result = "51"; //51
            break;
        case 2:
            result = "42"; //42
            break;
        case 3:
            result = "83"; //83
            break;
        case 4:
            result = "34"; //34
            break;
        case 5:
            result = "65"; //65
            break;
        case 6:
            result = "76"; //76
            break;
        case 7:
            result = "97"; //97
            break;
        case 8:
            result = "08"; //08
            break;
        case 9:
            result = "59"; //59
            break;
        case 10:
            result = "10"; //10
            break;

        }
        return result;
    }

    /**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @return  密码的字符串
     */
    public static String genRandomNum(int pwd_len) {
    	
    	System.out.println("进入新算法");
        //35是因为数组是从0开始的，26个字母+10个数字
//        final int maxNum = 100;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度

        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        StringBuffer pwd = new StringBuffer("");
//        Random r = new Random();
        while (count < pwd_len) {
            //生成随机数，取绝对值，防止生成负数，

//            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1
//由于随机数产生重复太多,重写算法并注释掉line 481 488 492
        	i = (int)(Math.random()*10);

        	
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }

        return pwd.toString();
    }

    public static void main(String[] args) {
//        String tOperateType = "QUERY";
//        String tCertifyCode = "04";
//        String tCertifyCode_1 = "04";
//        String tCertifyClass = "D";
//
//        VData tVData = new VData();
//
//        tVData.addElement(tOperateType);
//        tVData.add("Y");
//
//        CertifyDescUI tCertifyDescUI = new CertifyDescUI();
//        tCertifyDescUI.submitData(tVData, "QUERY");
    	CertifyNumberBL n = new CertifyNumberBL();
    	VerifyCertifyNo v = new VerifyCertifyNo();
    	System.out.println(n.GetVerifyBit("17000016017000016"));
    	System.out.println(v.getCRCNo("170000160170000160"));
    	System.out.println(n.GetVerifyBit("aa0000001aa000000"));
    	System.out.println(v.getCRCNo("aa0000001aa0000001"));
    }
}
