/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.schema.LMCardRiskSchema;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CertifyDescUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();//错误容器
    private VData mResult = new VData(); //返回结果
    private String mOperate; //操作类型
    public CertifyDescUI()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        CertifyDescBL tCertifyDescBL = new CertifyDescBL();
        System.out.println("---UI BEGIN---");
        if (tCertifyDescBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyDescBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        }
        else
            mResult = tCertifyDescBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        LMCertifyDesSchema mLMCertifyDesSchema = new LMCertifyDesSchema();
        LMCertifyDesSet mLMCertifyDesSet = new LMCertifyDesSet();
        LMCardRiskSet mLMCardRiskSet = new LMCardRiskSet();
        CertifyDescUI mCertifyDescUI = new CertifyDescUI();
        mLMCertifyDesSchema.setCertifyCode("aa");
        mLMCertifyDesSchema.setVerifyFlag("Y");
        mLMCertifyDesSchema.setSubCode("0");
        mLMCertifyDesSchema.setRiskCode("0");
        mLMCertifyDesSchema.setCertifyName("FDDD");
        mLMCertifyDesSchema.setPrem(0);
        mLMCertifyDesSchema.setAmnt(1);
        mLMCertifyDesSchema.setCertifyClass("D");
        mLMCertifyDesSchema.setNote("Note");
        mLMCertifyDesSchema.setImportantLevel("0");
        mLMCertifyDesSchema.setState("1");
        mLMCertifyDesSchema.setManageCom("86");
        mLMCertifyDesSchema.setInnerCertifyCode("0");
        mLMCertifyDesSchema.setHaveNumber("Y");
        mLMCertifyDesSchema.setCertifyLength("1");
        mLMCertifyDesSchema.setUnit("ff");
        mLMCertifyDesSchema.setCertifyClass2("1");
        mLMCertifyDesSchema.setPolPeriod("1");
        mLMCertifyDesSchema.setPolPeriodFlag("Y");


        LMCardRiskSchema tLMCardRiskSchema = new LMCardRiskSchema();
        tLMCardRiskSchema.setCertifyCode("aa");
        tLMCardRiskSchema.setRiskCode("1101");
        //tLMCardRiskSchema.setPrem();
        tLMCardRiskSchema.setPremProp("1");
        tLMCardRiskSchema.setMult(String.valueOf("1000"));
        mLMCardRiskSet.add(tLMCardRiskSchema);

        mLMCertifyDesSet.add(mLMCertifyDesSchema);
        VData tVData = new VData();
       tVData.addElement("INSERT");
       tVData.addElement("D");
       tVData.addElement(mLMCardRiskSet);
       tVData.addElement(mLMCertifyDesSet);
       mCertifyDescUI.submitData(tVData,"INSERT");
    }
}
