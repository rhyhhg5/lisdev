/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardPrintSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理普通单证发放操作（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */
public class CardSendOutUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 数据操作字符串 */
    private String mOperate;


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput globalInput = new GlobalInput();
    private LZCardSet mLZCardSet = new LZCardSet();
    private LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet();
    private String m_szLimitFlag = "NO";

    public CardSendOutUI()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();

        globalInput.Operator = "Admin";
        globalInput.ComCode = "0101";
        globalInput.ManageCom = "";

        CardSendOutUI tCardSendOutUI = new CardSendOutUI();

        LZCardSet setLZCard = new LZCardSet();
        LZCardSchema schemaLZCard = new LZCardSchema();

        schemaLZCard.setCertifyCode("02");
        schemaLZCard.setSubCode("");
        schemaLZCard.setRiskCode("");
        schemaLZCard.setRiskVersion("");

        schemaLZCard.setStartNo("800000000000000021");
        schemaLZCard.setEndNo("800000000000000021");

        schemaLZCard.setSendOutCom("A8611");
        schemaLZCard.setReceiveCom("D0000000001");

        schemaLZCard.setSumCount(0);
        schemaLZCard.setPrem("");
        schemaLZCard.setAmnt("100");
        schemaLZCard.setHandler("kevin");
        schemaLZCard.setHandleDate("2002-9-01");
        schemaLZCard.setInvaliDate("2002-9-01");

        schemaLZCard.setTakeBackNo("");
        schemaLZCard.setSaleChnl("");
        schemaLZCard.setStateFlag("");
        schemaLZCard.setOperateFlag("");
        schemaLZCard.setPayFlag("");
        schemaLZCard.setEnterAccFlag("");
        schemaLZCard.setReason("");
        schemaLZCard.setState("");
        schemaLZCard.setOperator("faint");
        schemaLZCard.setMakeDate("");
        schemaLZCard.setMakeTime("");
        schemaLZCard.setModifyDate("");
        schemaLZCard.setModifyTime("");

        setLZCard.add(schemaLZCard);

        LZCardPrintSet setLZCardPrint = new LZCardPrintSet();
        LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema();

        schemaLZCardPrint.setPrtNo("01");

        schemaLZCardPrint.setCertifyCode("01");
        schemaLZCardPrint.setRiskCode("");
        schemaLZCardPrint.setRiskVersion("");
        schemaLZCardPrint.setSubCode("");
        schemaLZCardPrint.setMaxMoney("");
        schemaLZCardPrint.setMaxDate("");
        schemaLZCardPrint.setComCode("");
        schemaLZCardPrint.setPhone("");
        schemaLZCardPrint.setLinkMan("");
        schemaLZCardPrint.setCertifyPrice("");
        schemaLZCardPrint.setManageCom("");
        schemaLZCardPrint.setOperatorInput("");
        schemaLZCardPrint.setInputDate("");
        schemaLZCardPrint.setInputMakeDate("");
        schemaLZCardPrint.setGetMan("");
        schemaLZCardPrint.setGetDate("");
        schemaLZCardPrint.setOperatorGet("");
        schemaLZCardPrint.setStartNo("");
        schemaLZCardPrint.setEndNo("");
        schemaLZCardPrint.setGetMakeDate("");
        schemaLZCardPrint.setSumCount("");
        schemaLZCardPrint.setModifyDate("");
        schemaLZCardPrint.setModifyTime("");
        schemaLZCardPrint.setState("");

        setLZCardPrint.add(schemaLZCardPrint);

        VData vData = new VData();

        vData.addElement(globalInput);
        vData.addElement(setLZCard);
        vData.addElement(setLZCardPrint);
        vData.addElement("NO");

        if (!tCardSendOutUI.submitData(vData, "INSERT"))
        {
            System.out.println(tCardSendOutUI.mErrors.getFirstError());
        }
    }


    /*
     * public function used to send data
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            this.mOperate = verifyOperate(cOperate);
            if (mOperate.equals(""))
            {
                buildError("verifyOperate", "不支持的操作字符串");
                return false;
            }

            //得到外部传入的数据,将数据备份到本类中
            if (!getInputData(cInputData))
                return false;

            //进行业务处理
            if (!dealData())
                return false;

            VData vData = new VData();

            //准备往后台的数据
            if (!prepareOutputData(vData))
                return false;

            CardSendOutBL cardSendOutBL = new CardSendOutBL();

            System.out.println("cardSendOutBL.submitData(vData,mOperate)");

            cardSendOutBL.submitData(vData, mOperate);

            //如果有需要处理的错误，则返回
            if (cardSendOutBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(cardSendOutBL.mErrors);
                buildError("submitData", "数据提交失败!");
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.getMessage());
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            if (mOperate.equals("INSERT"))
            {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
                vData.addElement(mLZCardPrintSet);
                vData.addElement(m_szLimitFlag);
            }

        }
        catch (Exception ex)
        {
            // @@错误处理
            buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            if (mOperate.equals("INSERT"))
            {
                globalInput.setSchema((GlobalInput) cInputData.
                                      getObjectByObjectName("GlobalInput", 0));
                mLZCardSet.set((LZCardSet) cInputData.getObjectByObjectName(
                        "LZCardSet", 0));
                mLZCardPrintSet.set((LZCardPrintSet) cInputData.
                                    getObjectByObjectName("LZCardPrintSet", 0));

                String str = (String) cInputData.getObjectByObjectName("String",
                        0);
                if (str == null || !str.equals("NO"))
                {
                    m_szLimitFlag = "YES";
                }
                else
                {
                    m_szLimitFlag = "NO";
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardSendOutUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }
}
