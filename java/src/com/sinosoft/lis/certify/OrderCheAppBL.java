/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-08-03
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZOrderDetailDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZOrderDetailSchema;
import com.sinosoft.lis.schema.LZOrderSchema;
import com.sinosoft.lis.vschema.LZOrderCardSet;
import com.sinosoft.lis.vschema.LZOrderDetailSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZOrderDB;

public class OrderCheAppBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate = "";
    private LZOrderSchema mLZOrderSchema = new LZOrderSchema();
    private LZOrderCardSet mLZOrderCardSet = new LZOrderCardSet();
    private LZOrderDetailSet mLZOrderDetailSet = new LZOrderDetailSet();

    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();
    private String strOpt;


    //业务处理相关变量
    /** 全局数据 */

    public OrderCheAppBL()
    {
    }

    /**
    * 提交数据处理方法
    * @param cInputData 传入的数据,VData对象
    * @param cOperate 数据操作字符串
    * @return 布尔值（true--提交成功, false--提交失败）
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       System.out.println("Come in BL.Submit..............");
       this.strOperate = cOperate;
       if (strOperate.equals(""))
       {
           buildError("verifyOperate", "不支持的操作字符串");
           return false;
       }
       if (!getInputData(cInputData))return false;

       if (!dealData())
       {
           return false;
       }





       return true;
   }

   public static void main(String[] args)
      {
          GlobalInput globalInput = new GlobalInput();
          globalInput.ComCode = "8611";
          globalInput.Operator = "001";

          OrderCheAppBL tOrderCheAppBL = new OrderCheAppBL();

          LZOrderSchema tLZOrderSchema = new LZOrderSchema();
          tLZOrderSchema.setSerialNo("200608042");
          tLZOrderSchema.setDescCom("8611");

          LZOrderDetailSet tLZOrderDetailSet = new LZOrderDetailSet();
          LZOrderDetailSchema tLZOrderDetailSchema = new LZOrderDetailSchema();

          tLZOrderDetailSchema.setSerialNo("200608046");
          tLZOrderDetailSchema.setCertifyCode("7774");
          tLZOrderDetailSchema.setOrderCom("8611");
          tLZOrderDetailSchema.setAppendTime(0);
//          tLZOrderDetailSchema.setOrderState("3");
//          tLZOrderDetailSchema.setAmnt("1000");
//          tLZOrderDetailSchema.setOrderPerson("zb");
          tLZOrderDetailSet.add(tLZOrderDetailSchema);

          // prepare main plan
          // 准备传输数据 VData
          VData vData = new VData();

          try
          {
              vData.add(tLZOrderSchema);
              vData.add(tLZOrderDetailSet);
              vData.add("ALLRETIFY");
              vData.add(globalInput);
              tOrderCheAppBL.submitData(vData,"UPDATE");
          }
          catch (Exception ex)
          {
              ex.printStackTrace();
          }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
            //进行插入数据
           if (this.strOperate.equals("INSERT")) {
               if (!insertData()) {
                   // @@错误处理
                   buildError("insertData", "增加单证管理员时出现错误!");
                   return false;
               }
           }
       //对数据进行修改操作
           if (this.strOperate.equals("UPDATE")) {
               String ope = this.strOpt;
               if (ope.equals("ALLRETIFY"))
               {
                   if(!retifyData()) //修改批次状态
                   {
                       buildError("insertData", "修改批次状态时出错!");
                       return false;
                   }
               }
               else if (!updateData(ope)) {
                   // @@错误处理
                 buildError("insertData", "修改订单状态时出错!");
                 return false;
               }
           }
           //对数据进行删除操作
           if (this.strOperate.equals("DELETE")) {
               if (!deleteData()) {
                   // @@错误处理
                   buildError("insertData", "!");
                   return false;
               }
           }
           return true;
    }

    /**
     * retifyData
     *
     * @return boolean
     */
    private boolean retifyData() //修改批次状态
    {
        LZOrderDB tLZOrderDB = new LZOrderDB();
        tLZOrderDB.setSchema(mLZOrderSchema);
        if (!tLZOrderDB.getInfo())
        {
            buildError("insertData", "修改批次状态失败!");
            return false;
        }
        tLZOrderDB.setState("1");
        mMap.put(tLZOrderDB.getSchema(),"UPDATE");
        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "添加批次信息时出现错误!");
                return false;
            }
        }
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData(String ope) {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        LZOrderDetailSet aLZOrderDetailSet = new LZOrderDetailSet();
        for (int i=0;i<mLZOrderDetailSet.size();i++)
        {
            LZOrderDetailDB tLZOrderDetailDB = new LZOrderDetailDB();

            tLZOrderDetailDB.setSchema(mLZOrderDetailSet.get(i + 1));

            if (!tLZOrderDetailDB.getInfo())
            {
                buildError("insertData", "修改订单状态时出错!");
            }
            LZOrderDetailSchema tLZOrderDetailSchema = new LZOrderDetailSchema();
            tLZOrderDetailSchema = tLZOrderDetailDB.getSchema();
            if(ope.equals("RETIFY"))
            {
                tLZOrderDetailSchema.setOrderState("3");
            }
            if(ope.equals("BACK"))
            {
                tLZOrderDetailSchema.setOrderState("2");
            }

            aLZOrderDetailSet.add(tLZOrderDetailSchema);
        }
        mMap.put(aLZOrderDetailSet,"UPDATE");
        if (!prepareOutputData())
       {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData,""))
       {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "添加批次信息时出现错误!");
               return false;
           }
       }
       mMap = null;
       tPubSubmit = null;

       return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData()
    {

        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mLZOrderSchema.setSchema((LZOrderSchema) cInputData.getObjectByObjectName("LZOrderSchema",0));
        mLZOrderDetailSet.set((LZOrderDetailSet) cInputData.getObjectByObjectName("LZOrderDetailSet",0));
        strOpt = (String)cInputData.getObjectByObjectName("String",0);
        return true;
    }

    public String getResult()
    {
        return mLZOrderSchema.getSerialNo();
    }

    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "OrderDescBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
