/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.sql.Connection;
import com.sinosoft.lis.schema.LZCardPaySchema;
import com.sinosoft.lis.vschema.LZCardPaySet;
import com.sinosoft.lis.db.LZCardPayDB;
import com.sinosoft.lis.schema.LZCardPaySchema;
import com.sinosoft.lis.vdb.LZCardPayDBSet;
import com.sinosoft.lis.vschema.LZCardPaySet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vdb.LZCardDBSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vdb.LZCardTrackDBSet;
import com.sinosoft.lis.vschema.LZCardTrackSet;


/**
 * <p>Title: Web业务系统案件－报案保存功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 */

public class CertifyPayBLS {
    //传输数据类
    private VData mInputData;
    private String mOperateType = ""; //操作类型
    private LZCardPaySet mLZCardPaySet = new LZCardPaySet(); //单证描述表
    private LZCardSet mLZCardSet = new LZCardSet(); //单证描述表
    private LZCardTrackSet mLZCardTrackSet = new LZCardTrackSet(); //单证描述表
    private LZCardPaySet mOldLZCardPaySet = new LZCardPaySet(); //结算单录入
    private LZCardSet mNewLZCardSet = new LZCardSet(); //单证状态表
    private LZCardTrackSet mNewLZCardTrackSet = new LZCardTrackSet(); //单证轨迹表
    private LZCardPaySet mNewLZCardPaySet = new LZCardPaySet(); //结算单录入

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    public CertifyPayBLS() {}

    public static void main(String[] args) {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        boolean tReturn = false;
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("Start CertifyPayBLS Submit...");
        mOperateType = (String) cInputData.get(0);
        System.out.println("所要执行的操作符号是" + mOperateType);
        mLZCardPaySet = ((LZCardPaySet) cInputData.getObjectByObjectName(
                "LZCardPaySet", 0));
        mOldLZCardPaySet = ((LZCardPaySet) cInputData.getObjectByObjectName(
                "LZCardPaySet", 2));
        mLZCardSet = ((LZCardSet) cInputData.getObjectByObjectName(
                "LZCardSet", 0));
        mLZCardTrackSet = ((LZCardTrackSet) cInputData.getObjectByObjectName(
                "LZCardTrackSet", 0));
        mNewLZCardPaySet = ((LZCardPaySet) cInputData.getObjectByObjectName(
                "LZCardPaySet", 4));
        mNewLZCardSet = ((LZCardSet) cInputData.getObjectByObjectName(
               "LZCardSet", 5));
       mNewLZCardTrackSet = ((LZCardTrackSet) cInputData.getObjectByObjectName(
               "LZCardTrackSet", 6));

        if (mOperateType.equals("INSERT")) {
            if (!this.saveData()) {
                return false;
            }
        }
        if (mOperateType.equals("UPDATE")) {
            if (!updateData()) {
                return false;
            }
        }
        if (mOperateType.equals("DELETE")) {
            if (!deleteData()) {
                return false;
            }
        }
        if (mOperateType.equals("AFFIRM")) {
            if (!deleteOldData()) {
                return false;
            }
            if (!updateData()) {
                return false;
            }
        }

        System.out.println("End Report BLS Submit...");
        mInputData = null;
        return true;
    }

    private boolean saveData() {
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyPayBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);

            LZCardPayDBSet tLZCardPayDBSet = new LZCardPayDBSet();
            tLZCardPayDBSet.set(mLZCardPaySet);
            if (!tLZCardPayDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LZCardDBSet tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mLZCardSet);
            if (!tLZCardDBSet.update()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LZCardTrackDBSet tLZCardTrackDBSet = new LZCardTrackDBSet();
            tLZCardTrackDBSet.set(mLZCardTrackSet);
            if (!tLZCardTrackDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) { // end of try
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyPayBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {
            }
            return false;
        }
        return true;
    }

    private boolean deleteData() {
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyPayBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        try {
            conn.setAutoCommit(false);
            LZCardPayDBSet tLZCardPayDBSet = new LZCardPayDBSet();
            tLZCardPayDBSet.set(mLZCardPaySet);
            if (!tLZCardPayDBSet.delete()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败！！！";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LZCardDBSet tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mLZCardSet);
            if (!tLZCardDBSet.delete()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mLZCardSet);
            if (!tLZCardDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LZCardTrackDBSet tLZCardTrackDBSet = new LZCardTrackDBSet();
            tLZCardTrackDBSet.set(mLZCardTrackSet);
            if (!tLZCardTrackDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
        } catch (Exception ex) { // end of try
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {
            }
            return false;
        }
        return true;
    }

    //在结算单确认时，删除老数据
    private boolean deleteOldData() {
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        try {
            conn.setAutoCommit(false);
            LZCardPayDBSet tLZCardPayDBSet = new LZCardPayDBSet();
            tLZCardPayDBSet.set(mOldLZCardPaySet);
            if (!tLZCardPayDBSet.delete()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyDescBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败！！！";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) { // end of try
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {
            }
            return false;
        }
        return true;
    }


    //先进行删除，在进行新增操作
    private boolean updateData() {
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            CError tError = new CError();
            tError.moduleName = "CertifyPayBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);

            LZCardSet tLZCardSet = new LZCardSet();
            LZCardTrackSet tLZCardTrackSet = new LZCardTrackSet();
           // delete exist payno in lzpaycard
            LZCardPayDBSet tLZCardPayDBSet = new LZCardPayDBSet();
            LZCardPaySet tLZCardPaySet = new LZCardPaySet();
            LZCardPayDB tLZCardPayDB = new LZCardPayDB();
            String sql = "select * from LZCardPay WHERE PayNo ='"+mLZCardPaySet.get(1).getPayNo()+"'";

            tLZCardPaySet = tLZCardPayDB.executeQuery(sql);
            tLZCardPayDBSet.set(tLZCardPaySet);
            if (!tLZCardPayDBSet.delete()) {
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "DeleteData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            // delete exist lzcard(delete exist payno in lzpaycard)
            LZCardDBSet tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mNewLZCardSet);
            if (!tLZCardDBSet.delete()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mNewLZCardSet);
            if (!tLZCardDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //delete exist LZCardTrack(delete exist payno in lzpaycard)
            LZCardTrackDBSet tLZCardTrackDBSet = new LZCardTrackDBSet();
            tLZCardTrackDBSet.set(mNewLZCardTrackSet);
            if (!tLZCardTrackDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            /*tLZCardPayDBSet = new LZCardPayDBSet();
            tLZCardPayDBSet.set(mNewLZCardPaySet);
            if (!tLZCardPayDBSet.delete()) {
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "DeleteData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }*/
            tLZCardPayDBSet = new LZCardPayDBSet();
            tLZCardPayDBSet.set(mLZCardPaySet);

            if (!tLZCardPayDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardPayDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ReportBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mLZCardSet);
            if (!tLZCardDBSet.delete()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            tLZCardDBSet = new LZCardDBSet();
            tLZCardDBSet.set(mLZCardSet);
            if (!tLZCardDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            tLZCardTrackDBSet = new LZCardTrackDBSet();
            tLZCardTrackDBSet.set(mLZCardTrackSet);
            if (!tLZCardTrackDBSet.delete()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            tLZCardTrackDBSet.set(mLZCardTrackSet);
            if (!tLZCardTrackDBSet.insert()) {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLZCardDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyPayBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }


            conn.commit();
            conn.close();
            System.out.println("save7");
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "CaseCureBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {
            }
            return false;
        }
        return true;
    }
}
