/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
/*
 * <p>ClassName: CertBalanceBL </p>
 * <p>Description: 单证日结 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: lis
 * @CreateDate：2003-08-11
 */
package com.sinosoft.lis.certify;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;

public class CertBalanceBL
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 全局数据 */
    private GlobalInput m_GlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String m_strOperate;


    /** 业务处理相关变量 */
    private LZCardSet m_LZCardSet = new LZCardSet();
    private Hashtable m_hashParams = null;

    public CertBalanceBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        m_strOperate = verifyOperate(cOperate);
        if (m_strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
            return false;

        if (!dealData())
            return false;

//    VData tVData = new VData();
//    if( !prepareOutputData(tVData) )
//      return false;
//
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            if (m_strOperate.equals("QUERY||MAIN"))
            {
                return submitQueryMain();
            }
            else
            {
                buildError("dealData", "不支持的操作字符串");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.m_GlobalInput.setSchema((GlobalInput) cInputData.
                                         getObjectByObjectName("GlobalInput", 0));
            m_LZCardSet = (LZCardSet) cInputData.getObjectByObjectName(
                    "LZCardSet", 0);
            m_hashParams = (Hashtable) cInputData.getObjectByObjectName(
                    "Hashtable", 0);

            if (m_LZCardSet == null)
            {
                buildError("getInputData", "没有传入所需要的查询条件");
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getInputData", ex.getMessage());
            return false;
        }
        return true;
    }

    private boolean prepareOutputData(VData aVData)
    {
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertBalanceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"QUERY||MAIN"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }


    /**
     * 初始化。对于所有的单证，将四个日结统计的数据都设为0。
     * @return
     */
    private boolean initResult(Hashtable hashResult)
    {
        if (hashResult == null)
        {
            return false;
        }

        hashResult.clear();

        String strSQL = "SELECT * FROM LMCertifyDes"
                        + " WHERE CertifyClass='P' OR CertifyClass='D'"
                        + " ORDER BY CertifyCode";

        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.executeQuery(strSQL);

        String[] strArr = null;

        for (int nIndex = 0; nIndex < tLMCertifyDesSet.size(); nIndex++)
        {
            strArr = new String[6];
            strArr[0] = tLMCertifyDesSet.get(nIndex + 1).getCertifyCode();
            strArr[1] = tLMCertifyDesSet.get(nIndex + 1).getCertifyName();
            strArr[2] = tLMCertifyDesSet.get(nIndex + 1).getUnit();
            strArr[3] = "0"; // 单价
            strArr[4] = "0"; // 领取数量
            strArr[5] = "0"; // 印刷费用

            hashResult.put(tLMCertifyDesSet.get(nIndex + 1).getCertifyCode(),
                           strArr);
        }

        // 取印刷费用
        // 取单证印刷表中最后一次的单证印刷费用
        strSQL = "SELECT CertifyCode, CertifyPrice FROM LZCardPrint A"
                 + " WHERE PrtNo = (SELECT MAX(PrtNo) FROM LZCardPrint B"
                 + "    WHERE B.CertifyCode = A.CertifyCode)"
                 + " ORDER BY CertifyCode";

        ExeSQL es = new ExeSQL();
        SSRS ssrs = es.execSQL(strSQL);

        if (ssrs.mErrors.needDealError())
        {
            mErrors.copyAllErrors(ssrs.mErrors);
            return false;
        }

        for (int nIndex = 0; nIndex < ssrs.getMaxRow(); nIndex++)
        {
            String strCertifyCode = ssrs.GetText(nIndex + 1, 1);
            strArr = (String[]) hashResult.get(strCertifyCode);
            if (strArr == null)
            {
                continue;
            }

            strArr[3] = ssrs.GetText(nIndex + 1, 2);
        }

        return true;
    }


    /**
     * 调用CertStatBL功能
     * @param hashResult
     * @param aLZCardSet
     * @return
     */
    private boolean callStatBL(Hashtable hashResult, LZCardSet aLZCardSet)
    {
        CertStatBL tCertStatBL = new CertStatBL();

        VData tVData = new VData();

        tVData.add(m_GlobalInput);
        tVData.add(aLZCardSet);
        tVData.add(new Hashtable());

        if (!tCertStatBL.submitData(tVData, "REPORT||QUERY"))
        {
            buildError("callStatBL", tCertStatBL.mErrors.getFirstError());
            return false;
        }

        tVData = tCertStatBL.getResult();

        // 处理得到的数据
        LZCardSet tLZCardSet = (LZCardSet) tVData.getObjectByObjectName(
                "LZCardSet", 0);
        double dSumMoney = 0; // 费用合计

        for (int nIndex = 0; nIndex < tLZCardSet.size(); nIndex++)
        {
            LZCardSchema tLZCardSchema = tLZCardSet.get(nIndex + 1);
            String[] strArr = (String[]) hashResult.get(tLZCardSchema.
                    getCertifyCode());

            if (strArr == null)
            {
                continue;
            }

            double dPrice = Double.parseDouble(strArr[3]);
            strArr[4] = String.valueOf(tLZCardSchema.getSumCount());

            double dMoney = dPrice * tLZCardSchema.getSumCount();
            strArr[5] = new DecimalFormat("0.00").format(dMoney);

            dSumMoney += dMoney;
        }

        hashResult.put("SumMoney", new DecimalFormat("0.00").format(dSumMoney));

        return true;
    }

    private boolean submitQueryMain()
    {
        Hashtable hashResult = new Hashtable();

        if (!initResult(hashResult))
        {
            buildError("submitQueryMain", "初始化查询结果集失败。");
            return false;
        }

        CertStatBL tCertStatBL = null;

        // 构造调用CertStat所需要的参数
        LZCardSet tLZCardSet = new LZCardSet();

        LZCardSchema tLZCardSchema1 = new LZCardSchema();
        LZCardSchema tLZCardSchema2 = new LZCardSchema();

        tLZCardSchema1.setSchema(m_LZCardSet.get(1));
        tLZCardSchema2.setSchema(m_LZCardSet.get(2));

        tLZCardSet.add(tLZCardSchema1);
        tLZCardSet.add(tLZCardSchema2);

        // 构造调用CertStatBL所必要的参数
        tLZCardSchema1.setSendOutCom("A" + tLZCardSchema1.getSendOutCom());
        tLZCardSchema1.setReceiveCom("A" + tLZCardSchema1.getReceiveCom());
//    tLZCardSchema1.setMakeDate("");
        tLZCardSchema1.setState(CertStatBL.STAT_SENDED);

        tLZCardSchema2.setSendOutCom("0");
        tLZCardSchema2.setReceiveCom("0");
//    tLZCardSchema2.setMakeDate("");

        if (!callStatBL(hashResult, tLZCardSet))
        {
            buildError("submitQueryMain", "查询单证结算信息失败");
            return false;
        }

        // 生成打印所需的数据
        XmlExport xmlExport = new XmlExport();
        xmlExport.createDocument("CertBalance.vts", "");

        TextTag textTag = new TextTag();

        try
        {
            textTag.add("SendOutCom",
                        getComName(m_LZCardSet.get(1).getSendOutCom()));
            textTag.add("ReceiveCom",
                        getComName(m_LZCardSet.get(1).getReceiveCom()));
        }
        catch (Exception ex)
        {
            buildError("submitQueryMain", ex.getMessage());
            return false;
        }

        FDate fdate = new FDate();
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");

        if (m_LZCardSet.get(1).getMakeDate() != null)
        {
            textTag.add("MakeDateB",
                        df.format(fdate.getDate(m_LZCardSet.get(1).getMakeDate())));
        }

        if (m_LZCardSet.get(2).getMakeDate() != null)
        {
            textTag.add("MakeDateE",
                        df.format(fdate.getDate(m_LZCardSet.get(2).getMakeDate())));
        }

        textTag.add("SumMoney", (String) hashResult.get("SumMoney"));

        xmlExport.addTextTag(textTag);

        // 按照单证编码排序
        String strSQL = "SELECT * FROM LMCertifyDes"
                        + " WHERE CertifyClass='P' OR CertifyClass='D'"
                        + " ORDER BY CertifyCode";

        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.executeQuery(strSQL);

        ListTable listTable = new ListTable();
        String[] strArr = null;

        for (int nIndex = 0; nIndex < tLMCertifyDesSet.size(); nIndex++)
        {
            strArr = (String[]) hashResult.get(tLMCertifyDesSet.get(nIndex + 1).
                                               getCertifyCode());
            listTable.add(strArr);
        }

        strArr = new String[6];
        strArr[0] = "单证编码";
        strArr[1] = "单证名称";
        strArr[2] = "单证单位";
        strArr[3] = "单价";
        strArr[4] = "领取数量";
        strArr[5] = "印刷费用";

        listTable.setName("CertInfo");

        xmlExport.addListTable(listTable, strArr);

        mResult.clear();
        mResult.add(xmlExport);

        return true;
    }

    public String getComName(String strComCode) throws Exception
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            throw new Exception("在取得LDCode的数据时发生错误");
        }
        return tLDCodeDB.getCodeName();
    }
}
