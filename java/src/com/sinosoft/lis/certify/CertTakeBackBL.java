/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class CertTakeBackBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /* 私有成员 */
    private String mszOperate = "";


    /* 业务相关的数据 */
    private GlobalInput globalInput = new GlobalInput();
    private LZCardSet mLZCardSet = new LZCardSet();
    private VData mResult = new VData();
    private Hashtable mParams = null;


    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。
    private int m_nOperIndex = 0;

    public CertTakeBackBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))return false;
        if (!dealData())
        {
            buildFailSet();
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        // 产生回收清算单号
        String strNoLimit = "";
        String strTakeBackNo = "";
        if (mszOperate.equals("INSERT"))
        {

            strNoLimit = PubFun.getNoLimit(globalInput.ComCode);
            strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);

            mResult.clear();
            mResult.add(strTakeBackNo);

            m_nOperIndex = 0;
            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                // Kevin 2003-07-17
                // 单证回收时，可以去掉发放者和接收者的校验。因为，如果数据库中存在发放的记录，
                // 这条记录就一定能被回收，而不必考虑发放者和接收者的当前状态。（如代理人的离职）

                // Verify SendOutCom and ReceiveCom
//        if( !CertifyFunc.verifyComs(globalInput, tLZCardSchema.getReceiveCom( ),
//                      tLZCardSchema.getSendOutCom( )) ) {
//          mErrors.copyAllErrors(CertifyFunc.mErrors);
//          return false;
//        }

                // 格式化数据
                LZCardSet tLZCardSet = CertifyFunc.formatCardList(tLZCardSchema);

                if (tLZCardSet == null)
                {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                VData vResult = new VData();

                for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                       nInnerIndex++)
                {

                    tLZCardSchema = tLZCardSet.get(nInnerIndex + 1);
                    // 设置回收清算单号
                    tLZCardSchema.setTakeBackNo(strTakeBackNo);

                    VData vOneResult = new VData();

                    // 单证回收操作
                    if (!CertifyFunc.splitCertifyTakeBack(globalInput,
                            tLZCardSchema, vOneResult))
                    {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    vResult.add(vOneResult);
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vResult, "INSERT"))
                {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }

                m_nOperIndex++;
            } // end of for(int nIndex = 0; ...
        } // end of if( mszOperate.equals("INSERT") );

        // 回收接口的实现过程
        if (mszOperate.equals("TAKEBACK"))
        {

            VData vData = new VData();

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                if (!CertifyFunc.handleTakeBack(mLZCardSet.get(nIndex + 1),
                                                vData))
                {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vData, "TAKEBACK"))
                {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }
            }
        }
        if (mszOperate.equals("AUTOTAKEBACK"))
        {

            VData vData = new VData();

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) {
                if (!CertifyFunc.AutoTakeBack(mLZCardSet.get(nIndex + 1),
                                                vData)) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vData, "AUTOTAKEBACK")) {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }
            }
        } 
        if (mszOperate.equals("HEXIAO"))
        {
            System.out.println("============= "+globalInput.ComCode+" ====================");
            strNoLimit = PubFun.getNoLimit(globalInput.ComCode);
            strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);
            
            mResult.clear();
            mResult.add(strTakeBackNo);

            m_nOperIndex = 0;
            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                // Kevin 2003-07-17
                // 单证回收时，可以去掉发放者和接收者的校验。因为，如果数据库中存在发放的记录，
                // 这条记录就一定能被回收，而不必考虑发放者和接收者的当前状态。（如代理人的离职）

                // Verify SendOutCom and ReceiveCom
//        if( !CertifyFunc.verifyComs(globalInput, tLZCardSchema.getReceiveCom( ),
//                      tLZCardSchema.getSendOutCom( )) ) {
//          mErrors.copyAllErrors(CertifyFunc.mErrors);
//          return false;
//        }

                // 格式化数据
                LZCardSet tLZCardSet = CertifyFunc.formatCardList(tLZCardSchema);

                if (tLZCardSet == null)
                {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                VData vResult = new VData();

                for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                       nInnerIndex++)
                {

                    tLZCardSchema = tLZCardSet.get(nInnerIndex + 1);
                    // 设置回收清算单号
                    tLZCardSchema.setTakeBackNo(strTakeBackNo);

                    VData vOneResult = new VData();

                    // 单证回收操作
                    if (!CertifyFunc.splitCertifyAuditCancel(globalInput,
                            tLZCardSchema, vOneResult))
                    {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    vResult.add(vOneResult);
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vResult, "INSERT"))
                {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }

                m_nOperIndex++;
            } // end of for(int nIndex = 0; ...
        } // end of if( mszOperate.equals("INSERT") );
        return true;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
        if (mszOperate.equals("INSERT"))
        {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);

        }
        else if (mszOperate.equals("TAKEBACK"))
        { // 外部调用回收操作

            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));

        }
        else if (mszOperate.equals("AUTOTAKEBACK"))
        { // 外部调用改变状态操作

            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));

        }
        else if (mszOperate.equals("HEXIAO"))
        { // 外部调用改变状态操作

            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);

        }

        else
        {
            buildError("getInputData", "不支持的操作字符串");
            return false;
        }

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            if (mszOperate.equals("INSERT"))
            {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyTakeBackBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                {"INSERT", "TAKEBACK","HEXIAO"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }

    private void buildFailSet()
    {
        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = m_nOperIndex; nIndex < mLZCardSet.size(); nIndex++)
        {
            tLZCardSet.add(mLZCardSet.get(nIndex + 1));
        }

        mResult.add(tLZCardSet);
    }
}
