/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LZCertifyUserDB;
import com.sinosoft.lis.schema.LZCertifyUserSchema;
import com.sinosoft.lis.db.LMCertifyDesDB;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理核销操作</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author zhangbin
 * @version 1.0
 */

public class AuditCancelBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    public static String ErrorState = "";

    /* 私有成员 */
    /** 前台传入的操作类型 */
    private String mszOperate = "";
    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    /** 单证表对象集 */
    private LZCardSet mLZCardSet = new LZCardSet();
    /** 数据容器*/
    private VData mResult = new VData();
    private Hashtable mParams = null;
    private LZCardSet setLZCard = new LZCardSet();
    /** 临时数据容器 */
    private VData tData = new VData();
    /** 自动核销标志 */
    private String autoFlag = "";
    //private static LZCardSchema tLZCardSchema=new LZCardSchema();

    /** 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。 */
    private int m_nOperIndex = 0;
    /** 核销单证的类型 */
    private String certifyCode = "";
    /** 核销单证的号码 */
    private String certifyNo = "";
    /** 对单证进行下发业务员校验标志 */
    private String verifyFlag = "";
    /** 核销类型：核销或人工处理 */
    private String auditType = "";
    //private String possessCom="";

    /**
     * 构造方法
     */
    public AuditCancelBL() {
    }

    public static void main(String[] args) {
        PubFun1.CreateMaxNo("TAKEBACKNO", "8611002007");
        /*AuditCancelBL tAuditCancelBL = new AuditCancelBL();
        VData tData = new VData();
        GlobalInput gInput = new GlobalInput();
        gInput.Operator = "bjdz";
        gInput.ManageCom = "86110000";
        gInput.ComCode = "86110000";
        LZCardSet setLZCard = new LZCardSet();
        for (int nIndex = 6; nIndex <= 7; nIndex++) {
            LZCardSchema schemaLZCard = new LZCardSchema();

            schemaLZCard.setCertifyCode("h_wang");
            //schemaLZCard.setSubCode("");
            //schemaLZCard.setRiskCode("");
            //schemaLZCard.setRiskVersion("");

            schemaLZCard.setStartNo("0000006");
            schemaLZCard.setEndNo("0000007");
            schemaLZCard.setStateFlag("");
            schemaLZCard.setState("5");

            schemaLZCard.setSendOutCom("A" + gInput.ComCode);
            if (gInput.ComCode.length() == 2) {
                schemaLZCard.setSendOutCom("00");
                schemaLZCard.setReceiveCom("A" + gInput.ComCode);
            } else if (gInput.ComCode.length() == 4) {
                schemaLZCard.setSendOutCom("A" + gInput.ComCode.substring(0, 2));
                schemaLZCard.setReceiveCom("A" + gInput.ComCode);
            } else {
                schemaLZCard.setSendOutCom("A" + gInput.ComCode.substring(0, 4));
                schemaLZCard.setReceiveCom("B" + gInput.Operator);
            }

            //schemaLZCard.setReceiveCom("SYS");

            schemaLZCard.setSumCount(1);
            schemaLZCard.setHandler(gInput.Operator);
            schemaLZCard.setHandleDate(PubFun.getCurrentDate());

            setLZCard.add(schemaLZCard);
        }
        tData.clear();
        tData.addElement(gInput);
        tData.addElement(setLZCard);
        tData.addElement("SINGLE");
        Hashtable hashParams = new Hashtable();
        hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
        tData.addElement(hashParams);
        if (!tAuditCancelBL.submitData(tData, "INSERT")) {
            System.out.println("error!");
        }*/

    }

    /**
     * 自动核销提交
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean autoSubmitData(VData cInputData, String cOperate) {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!readyData()) {
            return false;
        }

        if (!submitData(tData, "INSERT")) {
            return false;
        }

        return true;
    }

    /**
     * 准备数据
     * @param 无
     * @return 布尔值（true--准备数据成功, false--准备数据失败）
     */
    private boolean readyData() {
        LZCardSchema schemaLZCard = new LZCardSchema();
        schemaLZCard.setCertifyCode(certifyCode);
        schemaLZCard.setStartNo(certifyNo);
        schemaLZCard.setEndNo(certifyNo);
        schemaLZCard.setStateFlag("5");

        setLZCard.add(schemaLZCard);
        tData.addElement(setLZCard);
        tData.addElement("BATCH");

        Hashtable hashParams = new Hashtable();
        hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
        tData.addElement(hashParams);
        System.out.println(
                "AuditCancelBL.java --> readData() --> globalInput.ComCode..................: " +
                globalInput.ComCode);
        tData.addElement(globalInput);

        return true;
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            buildFailSet();
            return false;
        }

        return true;
    }

    /**
     * 返回处理结果
     * @return VData的对象
     * @author zhangbin
     * @version 1.0
     */
    public VData getResult() {
        return mResult;
    }
    
    /**
     * 返回处理错误
     */
    public CErrors getError() {
        return mErrors;
    }


    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（如果在处理过程中出错，则返回false,否则返回true）
     */
    private boolean dealData() {
        // 产生回收清算单号
        String strNoLimit = "";
        String strTakeBackNo = "";

        ErrorState = "";

        if (mszOperate.equals("INSERT")) {
            strNoLimit = PubFun.getNoLimit(globalInput.ComCode);
            strTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", strNoLimit);
            mResult.clear();
            mResult.add(strTakeBackNo);
            m_nOperIndex = 0;

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) { //
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);
                System.out.println("aaaa   单证类型: " +
                                   tLZCardSchema.getCertifyCode() +
                                   "  单证持有机构: " + tLZCardSchema.getReceiveCom() +
                                   "  单证发放机构: " + tLZCardSchema.getSendOutCom());
                System.out.println("起始号：" + tLZCardSchema.getStartNo() +
                                   "  终止号：" + tLZCardSchema.getEndNo());

                if (tLZCardSchema.getEndNo() == null ||
                    tLZCardSchema.getEndNo().equals("")) {
                    tLZCardSchema.setEndNo(tLZCardSchema.getStartNo());
                }

                LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

                tLMCertifyDesDB.setCertifyCode(tLZCardSchema.getCertifyCode());

                if (!tLMCertifyDesDB.getInfo()) {
                    mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
                    return false;
                }
                if (tLMCertifyDesDB.getCertifyClass().equals("D")) {
                    //修改为按单证长度自动填充长度    zhangjianbao    2007-10-29
                    int certifyLength = (int) tLMCertifyDesDB.getCertifyLength();
                    tLZCardSchema.setEndNo(PubFun.LCh(tLZCardSchema.getEndNo(),
                            "0", certifyLength));

                    tLZCardSchema.setStartNo(PubFun.LCh(tLZCardSchema.
                            getStartNo(), "0", certifyLength));

                }

                tLZCardSchema.setSubCode(tLMCertifyDesDB.getSubCode());
                // 有号单证
                if (tLMCertifyDesDB.getCertifyClass().equals("P")) {
                    if (tLMCertifyDesDB.getHaveNumber().equals("Y")) {
                        tLZCardSchema = getCardOwner(tLZCardSchema);
                    }
                }
                System.out.println("******" + tLMCertifyDesDB.getCertifyClass() + "******" + tLZCardSchema + "*****");

                if (tLZCardSchema == null) {
                    return false;
                }
                System.out.println(" finished getCardOwner()...");

                // 格式化数据：验证单证的格式是否正确
                LZCardSet tLZCardSet = CertifyFunc.formatAuditCardList(
                        tLZCardSchema); //!!!

                if (tLZCardSet == null) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }
                System.out.println(
                        " finished CertifyFunc.formatAuditCardList().........");

                VData vResult = new VData();

                for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                       nInnerIndex++) {
                    tLZCardSchema = tLZCardSet.get(nInnerIndex + 1);
                    // 设置回收清算单号
                    tLZCardSchema.setTakeBackNo(strTakeBackNo);

                    VData vOneResult = new VData();

                    // 单证核销操作
                    if (!CertifyFunc.splitCertifyAuditCancel(globalInput,
                            tLZCardSchema, vOneResult)) {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    vResult.add(vOneResult);
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vResult, "INSERT")) {
                    AuditCancelBL.ErrorState = "7";
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }

                m_nOperIndex++;
            } // end of for(int nIndex = 0; ...
        } // end of if( mszOperate.equals("INSERT") );

        // 回收接口的实现过程
        if (mszOperate.equals("TAKEBACK")) {
            VData vData = new VData();

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) {
                if (!CertifyFunc.handleTakeBack(mLZCardSet.get(nIndex + 1),
                                                vData)) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vData, "TAKEBACK")) {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }
            }
        }
        if (mszOperate.equals("AUTOTAKEBACK")) {

            VData vData = new VData();

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) {
                if (!CertifyFunc.AutoTakeBack(mLZCardSet.get(nIndex + 1),
                                              vData)) {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                CertTakeBackBLS tCertTakeBackBLS = new CertTakeBackBLS();

                if (!tCertTakeBackBLS.submitData(vData, "AUTOTAKEBACK")) {
                    mErrors.copyAllErrors(tCertTakeBackBLS.mErrors);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 对页面传入的需要核销的数据进行校验，并返回处理的数据
     * @param 页面传入的LZCardSchema对象
     * @return LZCardSchema对象（通过校验后返回处理过的LZCardSchema对象）
     */
    private LZCardSchema getCardOwner(LZCardSchema aLZCardSchema) {
        // 检查单证状态表，看是否已经存在这样的数据
        String szSql = "";
        szSql = "SELECT * FROM LZCard WHERE CertifyCode = '";
        szSql += aLZCardSchema.getCertifyCode();
        szSql += "' AND StartNo <= '" + aLZCardSchema.getStartNo() +
                "' AND EndNo >= '" + aLZCardSchema.getStartNo() + "'";
        //szSql += "' AND StartNo <= '" + aLZCardSchema.getEndNo() +
        //        "' AND EndNo >= '" + aLZCardSchema.getEndNo() + "'";
        System.out.println(" get card owner SQL: " + szSql);
        LZCardDB dbLZCard = new LZCardDB();
        LZCardSet tLZCardSet = dbLZCard.executeQuery(szSql);
        if (tLZCardSet.size() == 0) {
            AuditCancelBL.ErrorState = "1";
            buildError("inputCertify", "没有要处理的单证");
            return null;
        }
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setCertifyCode(aLZCardSchema.getCertifyCode());

        if (!tLMCertifyDesDB.getInfo()) {
            buildError("inputCertify", "没有要处理的单证");
            return null;
        }
        String vFlag = CertifyFunc.verifyOperate(aLZCardSchema); //返回"Y"校验业务员,"N"不校验
        if (!auditType.equals("HANDAUDIT")) { //如果不是人工核销进行下面校验，如果是人工核销不进行下面的校验
            if (tLMCertifyDesDB.getCertifyClass().equals("D") ||
                (!aLZCardSchema.getStateFlag().equals("5") &&
                 tLMCertifyDesDB.getCertifyClass().equals("P"))) { //如果是作废或挂失操作
                int comLen = globalInput.ComCode.length();
                System.out.println("登陆机构: " + globalInput.ComCode +
                                   "   持有单证的机构: " +
                                   tLZCardSet.get(1).getReceiveCom());
                if (!tLZCardSet.get(1).getStateFlag().equals("0")
                    &&!tLZCardSet.get(1).getStateFlag().equals("7")
                    &&!tLZCardSet.get(1).getStateFlag().equals("8")
                    &&!tLZCardSet.get(1).getStateFlag().equals("9")) {
                    buildError("inputCertify",
                               "该单证(" + tLZCardSet.get(1).getStartNo() +
                               ")，可能已被核销或处理!");
                    return null;
                }
                if (tLZCardSet.get(1).getReceiveCom().charAt(0) != 'D' &&
                    tLZCardSet.get(1).getReceiveCom().charAt(0) != 'E') {
                    if (comLen <= 4) {
                        if (comLen > tLZCardSet.get(1).getReceiveCom().length()) {
                            buildError("inputCertify",
                                       "您没有权限对该单证(" +
                                       tLZCardSet.get(1).getStartNo() +
                                       ")进行作废、挂失或销毁操作!");
                            return null;
                        }
                        if (!globalInput.ComCode.equals(tLZCardSet.get(1).
                                getReceiveCom().substring(1, comLen + 1))) {
                            buildError("inputCertify",
                                       "您没有权限对该单证(" +
                                       tLZCardSet.get(1).getStartNo() +
                                       ")进行作废、挂失或销毁操作!");
                            return null;
                        }
                    } else {
                        if (!tLZCardSet.get(1).getReceiveCom().equals("B" +
                                globalInput.Operator)) {
                            buildError("inputCertify",
                                       "您没有权限对该单证(" +
                                       tLZCardSet.get(1).getStartNo() +
                                       ")进行作废、挂失或销毁操作!");
                            return null;
                        }
                    }
                }
            } else { //如果是核销操作
                System.out.println("单证处理类型=========：" +
                                   aLZCardSchema.getStateFlag());

                if (vFlag == null || vFlag.equals("")) {
                    AuditCancelBL.ErrorState = "8";
                    buildError("inputCertify", "单证描述错误");
                    return null;
                } else if (vFlag.equals("Y")) { //如果需要校验业务员
                    System.out.println("需校验业务员................");
                    if (!tLZCardSet.get(1).getStateFlag().equals("0")
                        &&!tLZCardSet.get(1).getStateFlag().equals("8")) {
                        AuditCancelBL.ErrorState = "6";
                        buildError("inputCertify", "单证不在可用范围内,可能已被核销或作废");
                        return null;
                    }
                    if (tLZCardSet.get(1).getReceiveCom().charAt(0) != 'D') {
                        AuditCancelBL.ErrorState = "3";
                        buildError("inputCertify", "该单证没有下发给业务员");
                        return null;
                    }
                } else { //如果不需要校验业务员
                    System.out.println("业务处理机构3: ---　" + globalInput.ComCode +
                                       "  单证持有机构3: ---  " +
                                       tLZCardSet.get(1).getReceiveCom());
                    if (!tLZCardSet.get(1).getStateFlag().equals("0")
                        &&!tLZCardSet.get(1).getStateFlag().equals("7")
                        &&!tLZCardSet.get(1).getStateFlag().equals("8")
                        &&!tLZCardSet.get(1).getStateFlag().equals("9")) {
                        AuditCancelBL.ErrorState = "6";
                        buildError("inputCertify", "单证不在可用范围内,可能已被核销或作废");
                        return null;
                    }
                    /*=================================单证发放到单证管理员校验()===========================*/
                    if (tLZCardSet.get(1).getReceiveCom().charAt(0) != 'B') { //如果发放到机构
                        if (!tLZCardSet.get(1).getReceiveCom().equals("A" +
                                globalInput.ComCode)) { //globalInput.ComCode是单证的业务处理机构
                            AuditCancelBL.ErrorState = "4";
                            buildError("inputCertify", "单证所属机构与业务处理机构不符");
                            return null;
                        }
                        /*else
                                                {
                            AuditCancelBL.ErrorState = "9";
                            buildError("inputCertify", "单证未下发到单证管理员");
                            return null;
                                                }*/
                    } else {
                        LZCertifyUserDB tLZCertifyUserDB = new LZCertifyUserDB();
                        tLZCertifyUserDB.setUserCode(tLZCardSet.get(1).
                                getReceiveCom().substring(1));
                        if (!tLZCertifyUserDB.getInfo()) {
                            AuditCancelBL.ErrorState = "10";
                            buildError("inputCertify", "单证管理员无效");
                            return null;
                        }
                        LZCertifyUserSchema tLZCertifyUserSchema =
                                tLZCertifyUserDB.getSchema();
                        if(globalInput.ComCode.length()< tLZCertifyUserSchema.getComCode().length()){
                        	  if (!tLZCertifyUserSchema.getComCode().substring(0,
                        			  globalInput.ComCode.length()).
                                  equals(globalInput.ComCode)) { //判断业务处理机构与单证管理员所属机构是否相符，（可以是单证管理员所属机构的下级机构）
                                  AuditCancelBL.ErrorState = "4";
                                  buildError("inputCertify", "单证所属机构与业务处理机构不符");
                                  return null;
                              }
                        }else{
                        	  if (!globalInput.ComCode.substring(0,
                                      tLZCertifyUserSchema.getComCode().length()).
                                  equals(tLZCertifyUserSchema.getComCode())) { //判断业务处理机构与单证管理员所属机构是否相符，（可以是单证管理员所属机构的下级机构）
                                  AuditCancelBL.ErrorState = "4";
                                  buildError("inputCertify", "单证所属机构与业务处理机构不符");
                                  return null;
                              }
                        	
                        }
                      
                    }
                    /*===================================================*/
                }
            }
        }

        /*
               if (tLZCardSet.get(1).getReceiveCom().charAt(0) != 'D') //如果该单证不在业务员手中进行verifyflag校验
               {

         System.out.println(" finished verifyOperate()...................................................");
                       if (vFlag.equals("Y")) {
                           AuditCancelBL.ErrorState="3";
                           buildError("inputCertify", "该单证没有下发给业务员");
                           return null;
                       }

                       //if (!globalInput.Operator.equals("AUTO")) //如果手动核销不用校验机构
                       //{
         if (!tLZCardSet.get(1).getReceiveCom().equals(globalInput.ComCode)) {
         System.out.println("业务处理机构3: ----------------------------------------------------------　"+globalInput.ComCode);
                               AuditCancelBL.ErrorState="4";
                               buildError("inputCertify", "单证所属机构与业务处理机构不符");
                               return null;
                           }
                       //}
                   }
         */

        aLZCardSchema.setReceiveCom("SYS");
        aLZCardSchema.setSendOutCom(tLZCardSet.get(1).getReceiveCom());
        System.out.println("getReceiveCom().........: " +
                           aLZCardSchema.getReceiveCom());
        System.out.println("getSendOutCom().........: " +
                           aLZCardSchema.getSendOutCom());
        aLZCardSchema.setOperator(globalInput.Operator);
        return aLZCardSchema;
    }


    /**
     * 向Errors对象添加错误信息
     * @param szFunc - 错误功能名
     * @param szErrMsg - 错误信息
     * @return 无
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertifyFunc";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;

        System.out.println("In CertifyFunc buildError() : " + szErrMsg);
        mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * @param vData - 页面传入的LZCardSchema对象
     * @return 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData vData) {
        if (mszOperate.equals("INSERT")) { //人工处理时mszOperate为INSERT

            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            System.out.println("业务处理机构: " + globalInput.ComCode);
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++) { //
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);
                System.out.println("aaaa1   单证类型: " +
                                   tLZCardSchema.getCertifyCode() +
                                   "  单证发放机构: " + tLZCardSchema.getSendOutCom() +
                                   "  单证持有机构: " + tLZCardSchema.getReceiveCom()
                        );
                System.out.println("起始号：" + tLZCardSchema.getStartNo() +
                                   "  终止号：" +
                                   tLZCardSchema.getEndNo());
            }

            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);
            auditType = (String) vData.getObjectByObjectName("String", 0);
            System.out.println("处理类型: " + auditType);
        } else if (mszOperate.equals("TAKEBACK")) { // 外部调用回收操作
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
        } else if (mszOperate.equals("AUTOTAKEBACK")) { // 外部调用改变状态操作
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
        } else if (mszOperate.equals("AUTOAUDIT")) {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            certifyCode = (String) vData.getObjectByObjectName("String", 0);
            certifyNo = (String) vData.getObjectByObjectName("String", 1);
            System.out.println("getInput() --> certifyCode: " + certifyCode +
                               "       certifyNo:  " + certifyNo);
        } else {
            buildError("getInputData", "不支持的操作字符串");
            return false;
        }

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true

    /**
     * 准备往后层输出所需要的数据
     * @param vData - 准备传出的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            if (mszOperate.equals("INSERT")) {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyTakeBackBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 校验操作类型
     * @param szOperator - "INSERT", "TAKEBACK","AUTOAUDIT" 三种操作参数
     * @return 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private String verifyOperate(String szOperate) {
        System.out.println(" zb verifyOperate()...");
        String szReturn = "";
        String szOperates[] = {"INSERT", "TAKEBACK", "AUTOAUDIT"};
        for (int nIndex = 0; nIndex < szOperates.length; nIndex++) {
            if (szOperate.equals(szOperates[nIndex])) {
                szReturn = szOperate;
            }
        }
        return szReturn;
    }

    private void buildFailSet() {
        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = m_nOperIndex; nIndex < mLZCardSet.size(); nIndex++) {
            tLZCardSet.add(mLZCardSet.get(nIndex + 1));
        }

        mResult.add(tLZCardSet);
    }

}
