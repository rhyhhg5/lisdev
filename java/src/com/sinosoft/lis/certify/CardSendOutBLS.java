/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理普通单证发放操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */

public class CardSendOutBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    public CardSendOutBLS()
    {
    }

    public static void main(String[] args)
    {
        CardSendOutBLS cardSendOutBLS = new CardSendOutBLS();

        cardSendOutBLS.submitData(null, "INSERT");
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean bReturn = true;

        try
        {
            if (cOperate.equals("INSERT"))
            {
                if (!saveData(cInputData))
                {
                    bReturn = false;
                }
            }
            else
            {
                buildError("submitData", "不支持的操作字符串");
                bReturn = false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.getMessage());
            bReturn = false;
        }

        if (!bReturn)
        {
            if (CertifyFunc.mErrors.needDealError())
            {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
            }
            else
            {
                buildError("submitData", "发生错误，但是CertifyFunc没有提供详细信息");
            }
            System.out.println(mErrors.getFirstError());
        }

        return bReturn;
    }


    /**
     * Kevin, 2003-03-24
     * 保存数据。在传入的VData中。
     * 第一到第四个元素都是LZCardSchema，第一个元素是要删除的数据，其它的数据是要插入的数据。
     * 第五个元素是要插入到LZCardTrack表中的数据。
     * 第六个元素是要更新LZCardPrint表的数据。
     * @param vData
     * @return
     */
    private boolean saveData(VData vData)
    {
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            buildError("saveData", "连接数据库失败");
            return false;
        }

        LZCardDB tLZCardDB = new LZCardDB(conn);
        LZCardTrackDB tLZCardTrackDB = new LZCardTrackDB(conn);
        LZCardPrintDB tLZCardPrintDB = new LZCardPrintDB(conn);

        LZCardSchema tLZCardSchema = null;
        LZCardTrackSchema tLZCardTrackSchema = null;
        LZCardPrintSchema tLZCardPrintSchema = null;

        try
        {
            conn.setAutoCommit(false);

            tLZCardSchema = (LZCardSchema) vData.get(0);

            if (tLZCardSchema != null)
            {
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.delete())
                {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("删除旧的LZCard数据时出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(1);

            if (tLZCardSchema != null)
            {
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert())
                {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入拆分后的第一部分LZCard出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(2);

            if (tLZCardSchema != null)
            {
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert())
                {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入拆分后的第二部分LZCard出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(3);

            if (tLZCardSchema != null)
            {
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert())
                {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入新的LZCard出错");
                }
            }

            tLZCardTrackSchema = (LZCardTrackSchema) vData.get(4);

            if (tLZCardTrackSchema != null)
            {
                tLZCardTrackDB.setSchema(tLZCardTrackSchema);
                if (!tLZCardTrackDB.insert())
                {
                    mErrors.copyAllErrors(tLZCardTrackDB.mErrors);
                    throw new Exception("插入单证轨迹时出错");
                }
            }

            tLZCardPrintSchema = (LZCardPrintSchema) vData.get(5);

            if (tLZCardPrintSchema != null)
            {
                tLZCardPrintDB.setSchema(tLZCardPrintSchema);
                if (!tLZCardPrintDB.update())
                {
                    mErrors.copyAllErrors(tLZCardPrintDB.mErrors);
                    throw new Exception("更新单证印刷表失败");
                }
            }

            conn.commit();
            conn.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}

            return false;
        }

        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardSendOutBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
