/**
 * 2011-8-10
 */
package com.sinosoft.lis.certify;



import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LCBriGrpImportBatchInfoDB;
import com.sinosoft.lis.db.LCBriGrpImportDetailDB;
import com.sinosoft.lis.db.LCCertifyTakeBackDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBriGrpImportDetailSchema;
import com.sinosoft.lis.schema.LCCertifyTakeBackSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LCBriGrpImportDetailSet;
import com.sinosoft.lis.vschema.LCCertifyTakeBackSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.wiitb.MsgXmlParse;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



public class LZPubBriGrpBatchImportAPI
{
    private final static Logger mLogger = Logger.getLogger(LZPubBriGrpBatchImportAPI.class);

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mHandOprator = "";
    
    private String mBatchNo = null;
    
    private String mMsgType = null;

    private Document mInXmlDoc = null;

	private String mSendDate;

	private String mSendTime;
    
    public LZPubBriGrpBatchImportAPI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（BatchNo）]不存在。");
            return false;
        }

        mInXmlDoc = (Document) mTransferData.getValueByName("InXmlDoc");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[报文数据（InXmlDoc）]不存在。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if ("Import".equals(mOperate))
        {
            if (!importDatas())
            {
                return false;
            }
        }

        return true;
    }

    private boolean importDatas()
    {
        IXmlMsgColls tMsgCols = null;

        // 报文归档
        //        MsgXmlArchive.xmlArchive(cInXmlDoc);
        // --------------------

        // 解析报文
        MsgXmlParse tMsgXmlParse = new MsgXmlParse();
        tMsgCols = tMsgXmlParse.deal(mInXmlDoc);
        if (tMsgCols == null)
        {
            buildError("importDatas", tMsgXmlParse.getErrInfo());
            return false;
        }
        // --------------------

        // 校验报文基本信息完整性
        if (!chkXmlBaseInfo(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 创建批次任务
//        if (!createBatchMission(tMsgCols))
//        {
//            return false;
//        }
        // --------------------

        // 对应业务处理调度
        if (!dealTask(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 处理结果封装
        //        Document tOutDoc = null;
        //        MsgXmlPack tMsgXmlPack = new MsgXmlPack();
        //        tOutDoc = tMsgXmlPack.deal(tMsgCols);
        // --------------------

        return true;
    }

//    private boolean createBatchMission(IXmlMsgColls cMsgCols)
//    {
//        // 判断该批次是否已经产生任务清单，如已经产生，则不进行重新创建。
//        LCBriGrpImportBatchInfoDB tImportBatchInfo = new LCBriGrpImportBatchInfoDB();
//        tImportBatchInfo.setBatchNo(mBatchNo);
//        if (tImportBatchInfo.getInfo())
//        {
//            mLogger.info("[" + mBatchNo + "]报文已经产生对应任务清单。");
//            return true;
//        }
//        // --------------------
//
//        VData tVData = new VData();
//
//        TransferData tTransferData = new TransferData();
//        tVData.add(tTransferData);
//
//        tVData.add(cMsgCols);
//        tVData.add(mGlobalInput);
//
//        BriGrpCreatTaskBL tBriGrpCreatTaskBL = new BriGrpCreatTaskBL();
//        if (!tBriGrpCreatTaskBL.submitData(tVData, null))
//        {
//            buildError("createBatchMission", tBriGrpCreatTaskBL.mErrors.getFirstError());
//            return false;
//        }
//
//        return true;
//    }

    private boolean chkXmlBaseInfo(IXmlMsgColls cMsgCols)
    {
        // 校验报文头
        IXmlMsgColls[] tLMsgHeadInfo = cMsgCols.getBodyByFlag("MsgHead");
        if (tLMsgHeadInfo == null || tLMsgHeadInfo.length != 1)
        {
            buildError("chkXmlBaseInfo", "报文头信息格式不符合约定，请核实。");
            return false;
        }

        IXmlMsgColls tMsgHeadInfo = tLMsgHeadInfo[0];

        String tMsgHeadBatchNo = tMsgHeadInfo.getTextByFlag("BatchNo");
        if (tMsgHeadBatchNo == null || !tMsgHeadBatchNo.equals(mBatchNo))
        {
            buildError("chkXmlBaseInfo", "批次号与文件中批次号不符，请核对批次文件名称与报文内容批次信息是否一致。");
            return false;
        }
        
        mMsgType = tMsgHeadInfo.getTextByFlag("MsgType");
        if (mMsgType == null || "".equals(mMsgType))
        {
            buildError("chkXmlBaseInfo", "报文类型为空，请核查！");
            return false;
        }

        LZBriGrpCommonCheckBL tChkCommTool = new LZBriGrpCommonCheckBL();
        if (!tChkCommTool.checkMsgHead(tMsgHeadInfo))
        {
            buildError("chkXmlBaseInfo", tChkCommTool.getErrInfo());
            return false;
        }
        mHandOprator=tMsgHeadInfo.getTextByFlag("SendOperator");
        mSendDate=tMsgHeadInfo.getTextByFlag("SendDate");
        mSendTime=tMsgHeadInfo.getTextByFlag("SendTime");
        // --------------------

        // 校验正文数据内容
        IXmlMsgColls[] tLMsgBody = cMsgCols.getBodyByFlag("CertifyInfo");
        if (tLMsgBody == null || tLMsgBody.length <= 0)
        {
            buildError("chkXmlBaseInfo", "报文中不存在业务数据信息或业务信息节点命名不符合约定规范，请核实。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealTask(IXmlMsgColls cMsgCols)
    {
        String tCurBatchNo = mBatchNo;

        // --------------------

        // 创建日志记录器
        CertifyDiskImportLog tImportLog = null;

        try
        {
            tImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }
        // --------------------
        
        if (!dealSubTask(cMsgCols, tImportLog))
            {
        	
            return false;
            }
        
      

        return true;
    }

    private boolean dealSubTask(IXmlMsgColls cMsgCols,
            CertifyDiskImportLog cImportLog)
    {
        MMap tMMap = null;
        MMap tTmpMap = null;

       
        IXmlMsgColls[] tMsgBodyDatas = cMsgCols.getBodyByFlag("CertifyInfo");

        for (int i = 0; i < tMsgBodyDatas.length; i++)
        {
            IXmlMsgColls tSubMsgBodyData = tMsgBodyDatas[i];
            String tCertifyCode = tSubMsgBodyData.getTextByFlag("CertifyCode");
            String tStartNo = tSubMsgBodyData.getTextByFlag("StartNo");
            String tManageCom = tSubMsgBodyData.getTextByFlag("ManageCom");
            String tContNo  = tSubMsgBodyData.getTextByFlag("ContNo");
            String auditType="SINGLE";
            String tOperator = "";
            if("FPLNDZ01".equals(mMsgType)){
            	tOperator = "INSERT";
            }
            VData tVData = new VData();
            LZCardSet tLZCard = new LZCardSet( );
            if(!checkCardClass(tCertifyCode,tStartNo)){
            	buildError("checkCardClass", "报文中存在非普通单证"+tCertifyCode+tStartNo+"，请核实。");
                return false;
            }
            tLZCard=LZCardInsert(tSubMsgBodyData);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("BatchNo", mBatchNo);
            tTransferData.setNameAndValue("CertifyCode", tCertifyCode);
            tTransferData.setNameAndValue("XmlMsgDatas", tSubMsgBodyData);
            tTransferData.setNameAndValue("ImportLog", cImportLog);
            tTransferData.setNameAndValue("MsgType", mMsgType);

            tVData.add(mGlobalInput);
            tVData.add(tLZCard);
            tVData.addElement(auditType);
            tVData.add(tTransferData);
            AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
            if (!tCertTakeBackUI.submitData(tVData, tOperator)) { 
            	System.out.println(tCertifyCode+tStartNo+"单证未导入成功");
            	continue;
            } 
            else{
            	ExeSQL tExeSQL = new ExeSQL();
        		
        		tExeSQL.execUpdateSQL("update lzcardtrack a set ReceiveCom='"+"A"+tManageCom+"' where " +
        				" certifycode='"+tCertifyCode+"' and startno='"+tStartNo+"' " +
        						"and exists (select 1 from lzcard where certifycode=a.certifycode" +
        								"  and startno=a.startno" +
        								" and sendoutcom=a.sendoutcom" +
        								" and ReceiveCom=a.ReceiveCom " +
        								" and stateflag=a.stateflag) with ur "
            		);
        		tExeSQL.execUpdateSQL("update lzcard a set ReceiveCom='"+"A"+tManageCom+"' where " +
        				" certifycode='"+tCertifyCode+"' and startno='"+tStartNo+"' " +
        						"  with ur "
            		);
        		if(tContNo!=null&&!tContNo.equals("")){
        			if(!cancelCont(tStartNo,tContNo)){
        				buildError("cancelCont", "做保单回销时出错："+tCertifyCode+tStartNo+tContNo+"，请核实。");
                        return false;
        			}
        		}
            }
            
//            tMMap = new MMap();
//            tTmpMap = null;
//
//          
//
//            if (!submit(tMMap))
//            {
//            	
//                return false;
//            }
//            tMMap = null;

           
        }

        return true;
    }
    
    private boolean cancelCont(String tStartNo,String tContNo){
    	LCCertifyTakeBackDB tLCCertifyTakeBackDB=new LCCertifyTakeBackDB();
    	LCCertifyTakeBackSet tLCCertifyTakeBackSet=new LCCertifyTakeBackSet();
    	LCCertifyTakeBackSchema tLCCertifyTakeBackSchema=new LCCertifyTakeBackSchema();
    	MMap tMMap = new MMap();
    	tLCCertifyTakeBackDB.setCardNo(tStartNo);
    	tLCCertifyTakeBackDB.setContNo(tContNo);
    	tLCCertifyTakeBackDB.setState("1");
    	tLCCertifyTakeBackSet=tLCCertifyTakeBackDB.query();
    	if(tLCCertifyTakeBackSet.size()>0){
    		ExeSQL tExeSQL = new ExeSQL();
    		
    		tExeSQL.execUpdateSQL("update LCCertifyTakeBack a set state='0' where cardno='"+tStartNo+"'" +
    								" and contno='"+tContNo+"' with ur "
        		);
    	}
    	
    		String tBatchNo = (PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+PubFun1.CreateMaxNo("CERTCONT", 5)).replaceAll(":", "");
		    tLCCertifyTakeBackSchema.setBatchNo(tBatchNo);
		    tLCCertifyTakeBackSchema.setState("1");
		    tLCCertifyTakeBackSchema.setCardNo(tStartNo);
		    tLCCertifyTakeBackSchema.setContNo(tContNo);
		    tLCCertifyTakeBackSchema.setOperator(mGlobalInput.Operator);
        	tLCCertifyTakeBackSchema.setMakeDate(PubFun.getCurrentDate());
        	tLCCertifyTakeBackSchema.setMakeTime(PubFun.getCurrentTime());
        	tLCCertifyTakeBackSchema.setModifyDate(PubFun.getCurrentDate());
        	tLCCertifyTakeBackSchema.setModifyTime(PubFun.getCurrentTime());
        	tMMap.put(tLCCertifyTakeBackSchema, SysConst.INSERT);
        	
    	
    	if(!submit(tMMap))
        {
        	return false;
        }
    	tMMap = null;
    	return true;
    }
    private boolean checkCardClass(String tCertifyCode,String tStartNo){
    	LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(tCertifyCode);

        if (!tLMCertifyDesDB.getInfo()) {
        	buildError("checkCardClass", tCertifyCode+"单证没有定义信息，请核实。");
            return false;
           
        }
        //发票只针对普通单证
        if (!tLMCertifyDesDB.getCertifyClass().equals("P")) {
        	buildError("checkCardClass", "报文中存在非普通单证的数据"+tCertifyCode+tStartNo+"，请核实。");
            return false;
        }
        return true;
    }
    
    private LZCardSet LZCardInsert(IXmlMsgColls tSubMsgBodyData){
    	LZCardSet setLZCard = new LZCardSet( );
    	LZCardSchema schemaLZCard = new LZCardSchema( );
    	String tCertifyCode = tSubMsgBodyData.getTextByFlag("CertifyCode");
        String tStartNo = tSubMsgBodyData.getTextByFlag("StartNo");
        String tManageCom = tSubMsgBodyData.getTextByFlag("ManageCom");
        String tOperator=mGlobalInput.Operator;
        String tSendOperator=mHandOprator;
        
        
        
        schemaLZCard.setCertifyCode(tCertifyCode);
	    schemaLZCard.setStartNo(tStartNo);
	    schemaLZCard.setEndNo(tStartNo);
		schemaLZCard.setStateFlag("5");
		//schemaLZCard.setState("10");
		schemaLZCard.setReceiveCom(tSendOperator);
	   	schemaLZCard.setSumCount("1");
	    schemaLZCard.setHandler(tOperator);
	    schemaLZCard.setHandleDate(PubFun.getCurrentDate());
	    schemaLZCard.setInvaliDate(PubFun.getCurrentDate());
	    schemaLZCard.setOperator(tOperator);
	    if (mGlobalInput.ComCode.length()<=4)
	    {
	    	schemaLZCard.setSendOutCom("A"+tManageCom);
	    }
	  	else
	  	{
	  		schemaLZCard.setSendOutCom("B"+mGlobalInput.Operator);
	  	}
				    
	    setLZCard.add(schemaLZCard);
	    return setLZCard;
    	
    }
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
       


    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        mLogger.error(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "LZBriGrpBatchImportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
