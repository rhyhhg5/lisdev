/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.sql.Connection;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.db.LZCardTrackDB;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMCertifyDesDB;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class CertTakeBackBLS {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    public CertTakeBackBLS() {
    }

    public static void main(String[] args) {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        boolean bReturn = true;

        if (cOperate.equals("INSERT") || cOperate.equals("TAKEBACK") ||
            cOperate.equals("AUTOTAKEBACK")) {

            Connection conn = null;

//      if( conn == null ) {
//        buildError("submitData", "连接数据库失败");
//        return false;
//      }

            try {

                for (int nIndex = 0; nIndex < cInputData.size(); nIndex++) {
                    // 开始事务
                    if (conn != null) {
                        conn.close();
                    }
                    conn = DBConnPool.getConnection();
                    conn.setAutoCommit(false);
                    if (conn == null) {
                        buildError("submitData", "连接数据库失败");
                        return false;
                    }

                    if (!saveData((VData) cInputData.get(nIndex), conn)) {
                        bReturn = false;
                        break;
                    }
                }

                if (bReturn) {
                    conn.commit();
                } else {
                    conn.rollback();
                }

                conn.close();

            } catch (Exception ex) {
                try {
                    if (conn != null) {
                        conn.rollback();
                        conn.close();
                    }
                } catch (Exception e) {
                    // do nothing
                }
                bReturn = false;
            }
        } else {
            buildError("submitData", "不支持的操作字符串");
            bReturn = false;
        }

        if (!bReturn) {
            if (CertifyFunc.mErrors.needDealError()) {
                mErrors.copyAllErrors(CertifyFunc.mErrors);
            } else if (!mErrors.needDealError()) {
                buildError("submitData", "发生错误，但是CertifyFunc没有提供详细的信息");
            }
            //System.out.println("return ErrorMsg in CertTakeBackBLS : " + mErrors.getFirstError( ));
        }

        return bReturn;
    }


    /**
     * Kevin, 2003-03-19
     * 保存数据。在传入的VData中。第一到第四个元素都是LZCardSchema，第一个元素是要删除的数据，
     * 其它的数据是要插入的数据。第五个元素是要插入到LZCardTrack表中的数据。
     * @param vData
     * @return
     */
    private boolean saveData(VData vData, Connection conn) {
        LZCardDB tLZCardDB = new LZCardDB(conn);
        LZCardTrackDB tLZCardTrackDB = new LZCardTrackDB(conn);

        LZCardSchema tLZCardSchema = null;
        LZCardTrackSchema tLZCardTrackSchema = null;

        try {
            tLZCardSchema = (LZCardSchema) vData.get(0);
            String sql = "Select certifycode from lzcard where"
                         + " startno = '" + tLZCardSchema.getStartNo()
                         + "' and endno = '" + tLZCardSchema.getEndNo() +
                         "' for update ";
            System.out.println(sql);

            ExeSQL tExeSql = new ExeSQL(conn);
//      SSRS tSSRS = tExeSql.getOneValue(sql);
            String rs = tExeSql.getOneValue(sql);
            if (tExeSql.mErrors.needDealError()) {
                mErrors.copyAllErrors(tExeSql.mErrors);
                throw new Exception("资源忙，请稍候！");
            }

            if (rs.equals("")) {
                mErrors.copyAllErrors(tLZCardDB.mErrors);
                throw new Exception("并发操作数据，请重新操作！");
            }

            if (tLZCardSchema != null) {
                tLZCardDB.setSchema(tLZCardSchema);
//        System.out.println("Delete Operator --- ( " + tLZCardSchema.getStartNo() + " , " + tLZCardSchema.getEndNo() + ")");
                if (!tLZCardDB.delete()) {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    System.out.println("delete error : " +
                                       tLZCardDB.mErrors.getLastError());
                    throw new Exception("删除旧的LZCard数据时出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(1);
            if (tLZCardSchema != null) {
//        System.out.println("Cert Part 1 : " + tLZCardSchema.getStartNo() + " - " + tLZCardSchema.getEndNo());
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert()) {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入拆分后的第一部分LZCard出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(2);
            if (tLZCardSchema != null) {
//        System.out.println("Cert Part 2 : " + tLZCardSchema.getStartNo() + " - " + tLZCardSchema.getEndNo());
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert()) {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入拆分后的第二部分LZCard出错");
                }
            }

            tLZCardSchema = (LZCardSchema) vData.get(3);
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

            if (tLZCardSchema != null) {
//        System.out.println("Cert Part 3 : " + tLZCardSchema.getStartNo() + " - " + tLZCardSchema.getEndNo());

                tLMCertifyDesDB.setCertifyCode(tLZCardSchema.getCertifyCode());

                if (!tLMCertifyDesDB.getInfo()) {
                    mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
                    return false;
                }
                if (!tLZCardSchema.getStateFlag().equals("3") &&
                    tLMCertifyDesDB.getCertifyClass().equals("D")) {
                    tLZCardSchema.setReceiveCom(tLZCardDB.getSendOutCom());
                    tLZCardSchema.setSendOutCom(tLZCardDB.getReceiveCom());
                }
                tLZCardDB.setSchema(tLZCardSchema);
                if (!tLZCardDB.insert()) {
                    mErrors.copyAllErrors(tLZCardDB.mErrors);
                    throw new Exception("插入新的LZCard出错");
                }
            }

            tLZCardTrackSchema = (LZCardTrackSchema) vData.get(4);
            if (tLZCardTrackSchema != null) {
                //System.out.println("Cert Track : " + tLZCardSchema.getStartNo() + " - " + tLZCardSchema.getEndNo());
                tLZCardTrackDB.setSchema(tLZCardTrackSchema);
                if (!tLZCardSchema.getStateFlag().equals("3") &&
                    tLMCertifyDesDB.getCertifyClass().equals("D")) {
                    tLZCardTrackDB.setReceiveCom(tLZCardDB.getSendOutCom());
                    tLZCardTrackDB.setSendOutCom(tLZCardDB.getReceiveCom());
                }

                if (!tLZCardTrackDB.insert()) {
                    mErrors.copyAllErrors(tLZCardTrackDB.mErrors);
                    throw new Exception("插入单证轨迹时出错");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("saveData()", ex.toString());
            System.out.println("Exception in CertTakeBackBLS saveData() : " +
                               ex.toString());
            return false;
        }

        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
