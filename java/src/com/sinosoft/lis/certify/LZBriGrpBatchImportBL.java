/**
 * 2011-8-9
 */
package com.sinosoft.lis.certify;

import java.io.File;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.wiitb.XmlFileUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class LZBriGrpBatchImportBL 
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mFilePath = null;

    private String mFileName = null;

    private String mBatchNo = null;

    public LZBriGrpBatchImportBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（BatchNo）]不存在。");
            return false;
        }

        mFileName = (String) mTransferData.getValueByName("FileName");
        if (mFileName == null || mFileName.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（FileName）]不存在。");
            return false;
        }

        mFilePath = (String) mTransferData.getValueByName("FilePath");
        if (mFilePath == null || mFilePath.equals(""))
        {
            buildError("getInputData", "参数[导入文件路径（FilePath）]不存在。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if ("Import".equals(mOperate))
        {
            if (!importBriGrpPolicy())
            {
                return false;
            }
        }

        return true;
    }

    private boolean importBriGrpPolicy()
    {
        // 读取文件，转换成报文数据Docment
        Document tInXmlDoc = loadFile4Xml();
        if (tInXmlDoc == null)
        {
            return false;
        }
        // --------------------

        // 调用承保导入接口，处理报文
        VData tVData = new VData();
        String auditType="BATCH";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("InXmlDoc", tInXmlDoc);
        tVData.addElement(auditType);
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        LZPubBriGrpBatchImportAPI tBusLogicBL = new LZPubBriGrpBatchImportAPI();
        if (!tBusLogicBL.submitData(tVData, mOperate))
        {
            buildError("importBriGrpPolicy", tBusLogicBL.mErrors.getFirstError());
            return false;
        }
        // --------------------

        return true;
    }

    private Document loadFile4Xml()
    {
        String tFileName = mFilePath + File.separator + mFileName;

        Document tXmlDoc = null;
        tXmlDoc = XmlFileUtil.xmlFile2Doc(tFileName);
        if (tXmlDoc == null)
        {
            buildError("loadFile4Xml", "解析Xml文件失败。");
            return null;
        }

        return tXmlDoc;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpBatchImportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
