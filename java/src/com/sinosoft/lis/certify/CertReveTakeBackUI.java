/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收回退操作（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */
public class CertReveTakeBackUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 数据操作字符串 */
    private String mOperate;


    //业务处理相关变量
    /** 全局数据 */
//  private GlobalInput globalInput = new GlobalInput();
//  private LZCardSet mLZCardSet = new LZCardSet();

    private VData mResult = null;

    public CertReveTakeBackUI()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput globalInput = new GlobalInput();

        globalInput.Operator = "001";
        globalInput.ComCode = "8611";
        globalInput.ManageCom = "";

        CertReveTakeBackUI tCertReveTakeBackUI = new CertReveTakeBackUI();

        LZCardSet setLZCard = new LZCardSet();
        LZCardSchema schemaLZCard = new LZCardSchema();

        schemaLZCard.setSendOutCom("A8611");
        schemaLZCard.setReceiveCom("D8611000211");

        schemaLZCard.setCertifyCode("1101");
        schemaLZCard.setStartNo("86110100000011");
        schemaLZCard.setEndNo("86110100000011");
        schemaLZCard.setSumCount(5);

        schemaLZCard.setPrem("");
        schemaLZCard.setAmnt("100");
        schemaLZCard.setHandler("kevin");
        schemaLZCard.setHandleDate("2002-9-01");
        schemaLZCard.setInvaliDate("2002-9-01");

        schemaLZCard.setTakeBackNo("");
        schemaLZCard.setSaleChnl("");
        schemaLZCard.setStateFlag("");
        schemaLZCard.setOperateFlag("");
        schemaLZCard.setPayFlag("");
        schemaLZCard.setEnterAccFlag("");
        schemaLZCard.setReason("");
        schemaLZCard.setState("");
        schemaLZCard.setOperator("faint");
        schemaLZCard.setMakeDate("");
        schemaLZCard.setMakeTime("");
        schemaLZCard.setModifyDate("");
        schemaLZCard.setModifyTime("");

        setLZCard.add(schemaLZCard);

        VData vData = new VData();

        vData.addElement(globalInput);
        vData.addElement(setLZCard);

        Hashtable hashParams = new Hashtable();
        hashParams.put("CertifyClass", "P");
        vData.addElement(hashParams);

        if (!tCertReveTakeBackUI.submitData(vData, "INSERT"))
        {
            System.out.println(tCertReveTakeBackUI.mErrors.getFirstError());
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            //得到外部传入的数据,将数据备份到本类中
            if (!getInputData(cInputData))
                return false;

            //进行业务处理
            if (!dealData())
                return false;

            VData vData = new VData();

            //准备往后台的数据
            if (!prepareOutputData(vData))
                return false;

            vData = cInputData;

            CertReveTakeBackBL tCertReveTakeBackBL = new CertReveTakeBackBL();
            boolean bReturn = tCertReveTakeBackBL.submitData(vData, mOperate);
            mResult = tCertReveTakeBackBL.getResult();

            if (!bReturn)
            {
                if (tCertReveTakeBackBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tCertReveTakeBackBL.mErrors);
                }
                else
                {
                    buildError("submitData",
                               "CertReveTakeBackBL发生错误，但是没有提供详细的信息");
                }
            }

            return bReturn;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.getMessage());
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }


    /*
     * add by kevin, 2002-09-23
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertReveTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }

    public VData getResult()
    {
        return mResult;
    }
}
