/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CertifyMaxSaveUI
{
    public CErrors mErrors = new CErrors();//错误处理类
    private VData mInputData = new VData();//存放数据
    private VData mResult = new VData();//返回结果
    private String mOperate;//操作类型
    public CertifyMaxSaveUI()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        CertifyMaxSaveBL tCertifyMaxSaveBL = new CertifyMaxSaveBL();
        System.out.println("开始执行到CertifyMaxSaveUI");
        if (tCertifyMaxSaveBL.submitData(cInputData, mOperate) == false)
        {
            this.mErrors.copyAllErrors(tCertifyMaxSaveBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tCertifyMaxSaveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据操作失败！！！";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        }
        else
            mResult = tCertifyMaxSaveBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
