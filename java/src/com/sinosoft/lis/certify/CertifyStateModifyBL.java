package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyStateModifyBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();


	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate(); //获得当前日期

	private String CurrentTime = PubFun.getCurrentTime();//获得当前时间

	//录入单证信息
	private String  yCertifyCode ="";//表单中取出  条件值
	private String yStartNo="";//表单中取出  条件值
	private String yEndNo="";//表单中取出  条件值
	private String kStateflag=""; // 修改值
	private String kState="";// 修改值

	private MMap mMap = new MMap();

	public CertifyStateModifyBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CertifyStateModifyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start CertifyStateModifyBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();  
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		
		yCertifyCode = (String)tTransferData.getValueByName("CertifyCode");
	          if(yCertifyCode==null || "".equals(yCertifyCode)){//添加错误信息
	        	   CError tError = new CError();
	        	   tError.moduleName = "CertifyStateModifyBL";
	        	   tError.functionName="getInputData";
	        	   tError.errorMessage="单证编码为空！";
	        	   this.mErrors.addOneError(tError);
	        	   return false;
	          }
		yStartNo = (String)tTransferData.getValueByName("StartNo");
	              if(yStartNo ==null ||"".equals(yStartNo)){
	            	  CError tError =new CError();
	            	  tError.moduleName="CertifyStateModifyBL";
	            	  tError.functionName="getInputData";
	            	  tError.errorMessage="起始号码为空";
	            	  this.mErrors.addOneError(tError);
	            	  return false;
	              }
		yEndNo = (String)tTransferData.getValueByName("EndNo");
				if(yEndNo ==null ||"".equals(yEndNo)){
		      	  CError tError =new CError();
		      	  tError.moduleName="CertifyStateModifyBL";
		      	  tError.functionName="getInputData";
		      	  tError.errorMessage="终止号码为空";
		      	  this.mErrors.addOneError(tError);
		      	  return false;
		        }		
		kStateflag = (String)tTransferData.getValueByName("upStateModify");
		kState = (String)tTransferData.getValueByName("upStateflagModify");
		  if((kStateflag==null || "".equals(kStateflag)) && (kState==null || "".equals(kState)) ){
			  CError tError =new CError();
	      	  tError.moduleName="CertifyStateModifyBL";
	      	  tError.functionName="getInputData";
	      	  tError.errorMessage="修改的单证状态不能同时为空";
	      	  this.mErrors.addOneError(tError);
	      	  return false;		  
		  }
			
		return true;

	}

	private boolean dealData(){
		
		//String newDate ="modifydate = '"+CurrentDate+"',modifytime = '"+CurrentTime+"' ";
		//拼接sql，进行逻辑判断，
		String upSql = "";
		if(kState != null && !"".equals(kState)){
			upSql = "state = '"+kState+"' ";
		}
		if(kStateflag != null && !"".equals(kStateflag)){
			if("".equals(upSql)){
				upSql += "stateflag = '"+kStateflag+"' ";
			}else{
				upSql += ",stateflag = '"+kStateflag+"' ";//“，”逗号，会报sql异常
			}
			
		}
		//修改日期：Modifydate Modifytime;CurrentDate CurrentTime
    	String tSQL = "update lzcard set "+upSql+" , Modifydate='"+CurrentDate+"' ,Modifytime='"+CurrentTime+"'  where certifycode = '"+yCertifyCode+"' and startno = '"+yStartNo+"' and endno = '"+yEndNo+"' ";		
		mMap.put(tSQL, SysConst.UPDATE);
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
