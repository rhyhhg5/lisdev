/**
 * Copyright (c) 2010 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICardActModifyDB;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.schema.LICardActModifySchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LICardActModifySet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;

/**
 * <p>
 * Title: 激活卡信息修改
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Pangxy
 * @version 1.0
 */

public class ActivationCardModify
{
    /* 共有成员 */

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public static String ErrorState = "";

    /** 校验标记
     * (校验成功 = true;校验失败 = false (默认值))
     * */
    public boolean mCheckFlg = false;

    /** Debug信息标记
     * (Debug信息打开 = true;Debug信息关闭 = false (默认值))
     * */
    public boolean mDebugFlg = false;

    /* 私有成员 */

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LICardActModifySchema mLICardActModifySchema;

    private LICertifySchema mLICertifySchema;

    private LCContSchema mLCContSchema;

    private LICertifyInsuredSchema mLICertifyInsuredSchema;

    /** 处理标记 */
    private String mDealFlag = "";

    /** LICardActiveInfoList表存在标记
     * (LICertify表中不存在,LICardActiveInfoList表存在 = true;其他情况 = false (默认值))
     * */
    boolean mTableExistFlg = false;

    /** 生效日期修改标记
     * (生效日期有修改 = true;生效日期无修改 = false (默认值))
     * */
    private boolean mCvaliDateFlg = false;

    /** 基本信息修改标记 
     * (基本信息有修改 = true;基本信息无修改 = false (默认值))
     * */
    private boolean mBasicInfoFlg = false;

    /** 一般信息修改标记 
     * (一般信息有修改 = true;一般信息无修改 = false (默认值))
     * */
    private boolean mGeneralInfoFlg = false;

    /** 实名化标记 
     *  已经实名化 = true (默认值);未实名化 = false
     * */
    private boolean mWsStateFlg = true;

    /** 当前日期 */
    private String mCurrentDate = "";

    /** 当前时间 */
    private String mCurrentTime = "";

    /** 地址号码 */
    private String mAddressNo = "";

    /**
     * 构造方法
     */
    public ActivationCardModify()
    {
    }

    /*	public static void main(String[] args) {
     VData test = new VData();
     String apCardNo = "77000000097";
     GlobalInput mGlobalInput = new GlobalInput();
     mGlobalInput.Operator = "Test";
     test.add(mGlobalInput);
     test.add(apCardNo);
     test.add("1");
     test.add("2010-02-08");
     ActivationCardModify tActivationCardModify = new ActivationCardModify();
     tActivationCardModify.submitData(test);
     System.out.println(tActivationCardModify.Name + " * "
     + tActivationCardModify.Sex + " * "
     + tActivationCardModify.Birthday + " * "
     + tActivationCardModify.IdNo + " * "
     + tActivationCardModify.CvaliDate + " * "
     + tActivationCardModify.Mobile + " * "
     + tActivationCardModify.Phone + " * "
     + tActivationCardModify.PostalAddress + " * "
     + tActivationCardModify.ZipCode + " * "
     + tActivationCardModify.Email + " * ");
     System.out.println(tActivationCardModify.CurrentDate + " * "
     + tActivationCardModify.CurrentTime);
     if(tActivationCardModify.mErrors.getErrorCount()>0){
     System.out.println(tActivationCardModify.mErrors.getLastError());
     }
     System.out.println("处理标示:"+tActivationCardModify.DealFlag);
     }
     */
    /**
     * 提交数据更新
     * 
     * @param VData
     * @param String
     * @return boolean(更新成功返回true,更新失败返回false)
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!parameterInit())
        {
            return false;
        }
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        if (dealData())
        {
            mCheckFlg = true;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN"))
        {
            String tStrErr = "数据提交失败";
            buildError("submitData", tStrErr);
            return false;
        }
        return true;
    }

    /**
     * 参数的初始化
     * 
     * @return 处理成功返回true,处理失败返回false
     */
    private boolean parameterInit()
    {
        debugLog("+++++参数初始化处理开始+++++"); //Debug
        try
        {
            mTransferData = null;
            mWsStateFlg = false;
            mTableExistFlg = false;
            mCvaliDateFlg = false;
            mBasicInfoFlg = false;
            mGeneralInfoFlg = false;
            mCheckFlg = false;
            mLCContSchema = null;
            mLICardActModifySchema = null;
            mLICertifyInsuredSchema = null;
            mLICertifySchema = null;
            mDealFlag = "";
            mAddressNo = "";
            mCurrentDate = PubFun.getCurrentDate();
            mCurrentTime = PubFun.getCurrentTime();
            mInputData.clear();
        }
        catch (Exception ex)
        {
            String tStrErr = "参数初始化错误";
            buildError("getInputData", tStrErr);
            debugLog(ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 
     * @param VData
     * @param String
     * @return boolean(如果出错，返回false,否则返回true)
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        debugLog("+++++数据取得处理开始+++++"); //Debug
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        String tCardNo = (String) mTransferData.getValueByName("CardNo");
        String tSequenceNo = (String) mTransferData
                .getValueByName("SequenceNo");
        String tExcSQL = "";
        tExcSQL = "SELECT * from licardactmodify where cardno='" + tCardNo
                + "' and sequenceno = '" + tSequenceNo + "'";
        debugLog("licardactmodify:" + tExcSQL); //Debug
        LICardActModifyDB dbLICardActModifyDB = new LICardActModifyDB();
        LICardActModifySet tLICardActModifySet = null;
        tLICardActModifySet = dbLICardActModifyDB.executeQuery(tExcSQL);
        if (tLICardActModifySet == null || tLICardActModifySet.size() != 1)
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 对应的记录数为空";
            buildError("getInputData", tStrErr);
            return false;
        }
        mLICardActModifySchema = tLICardActModifySet.get(1);
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean dealData()
    {
        String tChkBusinessResult = checkBusiness();
        if (tChkBusinessResult.equals("09")) // 实名化校验错误,返回false
        {
            return false;
        }
        if (tChkBusinessResult.equals("00")) // 未实名化
        {
            if (mCvaliDateFlg && !checkCvaliDate()) // 未实名化时新生效日期不在提交日期15天以内,返回false
            {
                return false;
            }
        }
        if(mTableExistFlg && tChkBusinessResult.equals("01")) // 只在LICardActiveInfoList表中存在的数据,只校验生效日期，然后更新LICardActiveInfoList表
        {
            if (mCvaliDateFlg && !checkCvaliDate())
            {
                return false;
            }
            return true;
        }
        if (!checkOtherInfo()) // 其他信息校验
        {
            return false;
        }
        if (tChkBusinessResult.equals("01") && mGeneralInfoFlg) // 已实名化并且一般信息有修改
        {
            if (!getInsuredNo()) // 客户号取得
            {
                return false;
            }
            if (!checkAddress()) // 地址信息校验
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 校验是否已经实名化
     * 
     * @return String(未实名化返回:00,已经实名化返回:01,其他校验错误返回:09)
     */
    private String checkBusiness()
    {
        debugLog("+++++实名化校验处理开始+++++"); //Debug
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tCValidate = mLICardActModifySchema.getCValidate();
        String tWebModifyDate = mLICardActModifySchema.getWebModifyDate();
        String pCValiDate = "";
        String tWsState = "";
        String tContNo = "";
        String tActiveFlag = "";
        String tExcSQL = "select * from licertify where cardno = '" + tCardNo
                + "'";
        debugLog("licertify:" + tExcSQL); //Debug
        LICertifyDB dbLICertifyDB = new LICertifyDB();
        LICertifySet tLICertifySet = null;
        tLICertifySet = dbLICertifyDB.executeQuery(tExcSQL);
        if (tLICertifySet == null || tLICertifySet.size() != 1)
        {
            debugLog("LICertify表中无对应记录,从LICardActiveInfoList表中进行查找"); //Debug
            tExcSQL = "select * from licardactiveinfolist where SequenceNo='1' and cardno = '" + tCardNo
                    + "'";
            LICardActiveInfoListDB dbLICardActiveInfoListDB = new LICardActiveInfoListDB();
            LICardActiveInfoListSet tLICardActiveInfoListSet = null;
            tLICardActiveInfoListSet = dbLICardActiveInfoListDB.executeQuery(tExcSQL);
            if(tLICardActiveInfoListSet == null || tLICardActiveInfoListSet.size() != 1)
            {
                String tStrErr = "数据异常:卡号(" + tCardNo + ") 在LICertify表及LICardActiveInfoList表里没有对应数据";
                buildError("checkBusiness", tStrErr);
                mDealFlag = "10"; // 10:LICertify表及LICardActiveInfoList表里没有对应数据
                return "09";
            }
            mTableExistFlg = true;
            LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
            pCValiDate = tLICardActiveInfoListSchema.getCValidate();
        }
        else
        {
            mLICertifySchema = tLICertifySet.get(1);
            pCValiDate = mLICertifySchema.getCValidate();
            tWsState = mLICertifySchema.getWSState();
            tContNo = mLICertifySchema.getContNo();
            tActiveFlag = mLICertifySchema.getActiveFlag();
            if ( tActiveFlag == null || tActiveFlag.equals("00") )
            {
                debugLog("卡未激活,从LICardActiveInfoList表中进行查找"); //Debug
                tExcSQL = "select * from licardactiveinfolist where SequenceNo='1' and cardno = '" + tCardNo
                        + "'";
                LICardActiveInfoListDB dbLICardActiveInfoListDB = new LICardActiveInfoListDB();
                LICardActiveInfoListSet tLICardActiveInfoListSet = null;
                tLICardActiveInfoListSet = dbLICardActiveInfoListDB.executeQuery(tExcSQL);
                if(tLICardActiveInfoListSet == null || tLICardActiveInfoListSet.size() != 1)
                {
                    String tStrErr = "数据异常:卡号(" + tCardNo + ") 未激活,不能进行修改";
                    buildError("checkBusiness", tStrErr);
                    mDealFlag = "02"; // 02:卡未激活,不能进行修改
                    return "09";
                }
                mTableExistFlg = true;
                LICardActiveInfoListSchema tLICardActiveInfoListSchema = tLICardActiveInfoListSet.get(1);
                pCValiDate = tLICardActiveInfoListSchema.getCValidate();
            }
        }
        if (PubFun.calInterval(tCValidate, pCValiDate, "D") != 0)
        { //生效日期变动
            mCvaliDateFlg = true;
        }
        int cCheckPoint = 0;
        cCheckPoint = PubFun.calInterval(tWebModifyDate, pCValiDate, "D");
        if (mCvaliDateFlg && cCheckPoint <= 0)
        { //生效日期变动 并且 旧生效日期 - 网站提交修改日期 < 0(卡已经生效)
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 已生效,不能进行修改";
            buildError("checkBusiness", tStrErr);
            mDealFlag = "03"; // 03:激活卡已生效,不能进行修改
            return "09";
        }
        if(mTableExistFlg) // 只在LICardActiveInfoList表中存在的数据,只校验生效日期的修改,这里直接返回
        {
            return "01";
        }
        if(mLICertifySchema.getWSState().equals("01"))
        {
        	if (tContNo == null || "".equals(tContNo))
            {
                String tStrErr = "数据异常:卡号(" + tCardNo + ") 对应合同号为空";
                buildError("checkBusiness", tStrErr);
                mDealFlag = "11"; // 11:LICertify表里对应合同号为空
                return "09";
            }
        	else
            {
                if (tWsState != null && tWsState.equals("00"))
                {
                    mWsStateFlg = false;
                    return "00";
                }
                if (tWsState != null && tWsState.equals("01"))
                {
                    mWsStateFlg = true;
                    if (mCvaliDateFlg)
                    { //已经实名化,生效日期有变动
                        String tStrErr = "数据异常:卡号(" + tCardNo + ") 已实名化,生效日期不能进行修改";
                        buildError("checkBusiness", tStrErr);
                        mDealFlag = "04"; // 04:已经实名化,不能修改基本信息
                        return "09";
                    }
                    return "01";
                }
                String tStrErr = "数据异常:卡号(" + tCardNo + ") 的实名化状态错误";
                buildError("checkBusiness", tStrErr);
                mDealFlag = "12"; // 12:LICertify表里实名化状态错误
                return "09";
            }
        }
        return "00";
    }

    /**
     * 校验生效日期
     * 
     * @return boolean(已生效返回true,未生效返回false)
     */
    private boolean checkCvaliDate()
    {
        debugLog("+++++生效日期校验处理开始+++++"); //Debug
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tCValidate = mLICardActModifySchema.getCValidate();
        String tWebModifyDate = mLICardActModifySchema.getWebModifyDate();
        int cCheckPoint = 0;
        cCheckPoint = PubFun.calInterval(tWebModifyDate, tCValidate, "D");
        debugLog("网站提交修改日期:" + tWebModifyDate); //Debug
        debugLog("新生效日期:" + tCValidate); //Debug
        debugLog("卡号(" + tCardNo + ") 距离生效还有" + cCheckPoint + "天"); //Debug
        if (cCheckPoint < 1 || cCheckPoint > 15)
        { //新生效日期 在 网站提交修改日期次日 的15天外
            String tStrErr = "查询LCCont表时出错!数据异常:卡号(" + tCardNo
                    + ") 生效日期在保险责任未生效前的15天之外或者当天,不能进行修改";
            buildError("dealData", tStrErr);
            mDealFlag = "05"; // 05:新生效日期在激活卡已激活但保险责任未生效前的15天之外
            return false;
        }
        return true;
    }

    /**
     * 校验其他修改信息(生效日期以外的基本信息及一般信息)
     * 
     * @return boolean(校验成功返回true,校验失败返回false)
     */
    private boolean checkOtherInfo()
    {
        debugLog("+++++其他修改信息校验处理开始+++++"); //Debug
        int cIdx1 = 0;
        int cIdx2 = 0;
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tName = mLICardActModifySchema.getName();
        String tSex = mLICardActModifySchema.getSex();
        String tBirthday = mLICardActModifySchema.getBirthday();
        String tIdType = mLICardActModifySchema.getIdType();
        String tIdNo = mLICardActModifySchema.getIdNo();
        String tOccupationType = mLICardActModifySchema.getOccupationType();
        String tMobile = mLICardActModifySchema.getMobile();
        String tPhone = mLICardActModifySchema.getPhone();
        String tPostalAddress = mLICardActModifySchema.getPostalAddress();
        String tZipCode = mLICardActModifySchema.getZipCode();
        String tEmail = mLICardActModifySchema.getEMail();
        String tExcSQL = "select * from licertifyinsured where cardno = '"
                + tCardNo + "'";
        debugLog("licertifyinsured:" + tExcSQL); //Debug
        LICertifyInsuredDB dbLICertifyInsuredDB = new LICertifyInsuredDB();
        LICertifyInsuredSet tLICertifyInsuredSet = null;
        tLICertifyInsuredSet = dbLICertifyInsuredDB.executeQuery(tExcSQL);
        if (tLICertifyInsuredSet == null || tLICertifyInsuredSet.size() != 1)
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 的旧基本信息取得失败";
            buildError("checkOtherInfo", tStrErr);
            mDealFlag = "30"; // 30:LICertifyInsured表信息取得失败
            return false;
        }
        mLICertifyInsuredSchema = tLICertifyInsuredSet.get(1);
        String pName = mLICertifyInsuredSchema.getName();
        String pSex = mLICertifyInsuredSchema.getSex();
        String pBirthday = mLICertifyInsuredSchema.getBirthday();
        String pIdType = mLICertifyInsuredSchema.getIdType();
        String pIdNo = mLICertifyInsuredSchema.getIdNo();
        String pOccupationType = mLICertifyInsuredSchema.getOccupationType();
        String pMobile = mLICertifyInsuredSchema.getMobile();
        String pPhone = mLICertifyInsuredSchema.getPhone();
        String pPostalAddress = mLICertifyInsuredSchema.getPostalAddress();
        String pZipCode = mLICertifyInsuredSchema.getZipCode();
        String pEmail = mLICertifyInsuredSchema.getEMail();
        if (!StrTool.cTrim(pIdType).equals(StrTool.cTrim(tIdType)))
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 的证件类型不允许修改";
            buildError("checkOtherInfo", tStrErr);
            mDealFlag = "06"; // 06:证件类型不允许修改
            return false;
        }
        if (!StrTool.cTrim(pOccupationType).equals(
                StrTool.cTrim(tOccupationType)))
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 的职业类别不允许修改";
            buildError("checkOtherInfo", tStrErr);
            mDealFlag = "07"; // 07:职业类别不允许修改
            return false;
        }
        if (!StrTool.cTrim(pName).equals(StrTool.cTrim(tName)))
        {
            cIdx1++;
            cIdx2++;
        }
        if (!StrTool.cTrim(pIdNo).equals(StrTool.cTrim(tIdNo)))
        {
            cIdx1++;
        }
        if (cIdx1 > 1)
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 的证件号与姓名不允许同时修改";
            buildError("checkOtherInfo", tStrErr);
            mDealFlag = "08"; // 08:不允许同时修改证件号与姓名
            return false;
        }
        if (!StrTool.cTrim(pSex).equals(StrTool.cTrim(tSex)))
        {
            cIdx2++;
        }
        if (!StrTool.cTrim(pBirthday).equals(StrTool.cTrim(tBirthday)))
        {
            cIdx2++;
        }
        if (cIdx2 > 1)
        {
            String tStrErr = "数据异常:卡号(" + tCardNo
                    + ") 的姓名,性别与出生日期的其中两项信息不允许同时修改";
            buildError("checkOtherInfo", tStrErr);
            mDealFlag = "09"; // 09:不允许同时修改姓名,性别与出生日期的其中两项信息
            return false;

        }
        if (cIdx1 > 0 || cIdx2 > 0)
        {
            mBasicInfoFlg = true; // 标记基本信息修改为true
            if (mWsStateFlg)
            {
                String tStrErr = "数据异常:卡号(" + tCardNo + ") 已经实名化，不能修改基本信息";
                buildError("checkOtherInfo", tStrErr);
                mDealFlag = "04"; // 04:已经实名化，不能修改基本信息
                return false;
            }
        }
        if (!StrTool.cTrim(pMobile).equals(StrTool.cTrim(tMobile))
                || !StrTool.cTrim(pPhone).equals(StrTool.cTrim(tPhone))
                || !StrTool.cTrim(pPostalAddress).equals(
                        StrTool.cTrim(tPostalAddress))
                || !StrTool.cTrim(pZipCode).equals(StrTool.cTrim(tZipCode))
                || !StrTool.cTrim(pEmail).equals(StrTool.cTrim(tEmail)))
        {
            mGeneralInfoFlg = true; // 标记一般信息修改为true
        }
        return true;
    }

    /**
     * 客户号取得
     * 
     * @return boolean(取得成功返回true,取得失败返回false)
     */
    private boolean getInsuredNo()
    {
        debugLog("+++++客户号取得处理开始+++++"); //Debug
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tContNo = mLICertifySchema.getContNo();
        String tExcSQL = "select * from lccont where contno = '" + tContNo
                + "'";
        debugLog("lccont:" + tExcSQL); //Debug
        LCContDB dbLCContDB = new LCContDB();
        LCContSet tLCContSet = null;
        tLCContSet = dbLCContDB.executeQuery(tExcSQL);
        mLCContSchema = tLCContSet.get(1);
        String tInsuredNo = mLCContSchema.getInsuredNo();
        if (tInsuredNo == null || "".equals(tInsuredNo))
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 的客户号取得失败";
            buildError("getInsuredNo", tStrErr);
            mDealFlag = "20"; // 20:LCCont表里没有对应的客户号
            return false;
        }
        return true;
    }

    /**
     * 校验地址相关信息
     * 
     * @return boolean(校验成功返回true,校验失败返回false)
     */
    private boolean checkAddress()
    {
        debugLog("+++++地址相关信息校验处理开始+++++"); //Debug
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tInsuredNo = mLCContSchema.getInsuredNo();
        ExeSQL tExeSQL = new ExeSQL();
        String pSQL = "select max(cast(addressno as integer))+1 from lcaddress where customerno = '"
                + tInsuredNo + "'";
        try
        {
            mAddressNo = tExeSQL.getOneValue(pSQL);
        }
        catch (Exception ex)
        {
            String tStrErr = "数据异常:卡号(" + tCardNo + ") 对应的地址号码的值可能不是数字";
            buildError("checkAddress", tStrErr);
            mDealFlag = "40"; // 40:地址表查询失败,地址号码的值可能不是数字
            return false;
        }
        debugLog("+++++地址相关信息校验处理结束+++++"); //Debug
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        debugLog("+++++传输数据准备处理开始+++++"); //Debug
        MMap tMap = new MMap();
        MMap pMap = new MMap();
        VData tData = new VData();
        debugLog("生效日期是否有修改:" + mCvaliDateFlg); //Debug
        debugLog("是否实名化:" + mWsStateFlg); //Debug
        debugLog("一般信息是否有修改:" + mGeneralInfoFlg); //Debug
        debugLog("关键信息是否有修改:" + mBasicInfoFlg); //Debug
        debugLog("是否查询了LICardActiveInfoList表:" + mTableExistFlg); //Debug
        if (mCheckFlg && !mTableExistFlg)
        { //逻辑校验成功,并且此卡不只是在LICardActiveInfoList表中存在，对各表执行数据准备作业
            if (!updateLICertify(tMap))
            { //准备LICertify表的更新数据
                mCheckFlg = false;
            }
            if (mCheckFlg && !updateLICertifyInsured(tMap))
            { //准备LICertifyInsured表的更新数据
                mCheckFlg = false;
            }
            if (mCheckFlg && mWsStateFlg)
            {
                if (mCheckFlg && !insertLCAddress(tMap))
                { //准备LCAddress表的数据
                    mCheckFlg = false;
                }
                if (mCheckFlg && !updateLCInsured(tMap))
                { //准备LCInsured表的数据
                    mCheckFlg = false;
                }
            }
        }
        if (mCheckFlg && !updateLICardActiveInfoList(tMap))
        { //准备LICardActiveInfoList表的更新数据
            mCheckFlg = false;
        }
        if (mCheckFlg)
        { //逻辑校验成功并且数据准备无错的情况下,提交准备数据
            pMap = tMap;
            mDealFlag = "01";
        }
        if (!updateLICardActModify(pMap))
        {
            return false;
        }
        tData.add(pMap);
        mInputData = tData;
        debugLog("+++++传输数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * LICertify更新
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean updateLICertify(MMap fpMap)
    {
        debugLog("+++++LICertify表数据准备处理开始+++++"); //Debug
        if (!mCvaliDateFlg)
        {
            debugLog("生效日期未变化，不需要修改LICertify表"); //Debug
            return true;
        }
        String tCValidate = mLICardActModifySchema.getCValidate();
        String tOperator = mGlobalInput.Operator;
        try
        {
            mLICertifySchema.setCValidate(tCValidate);
            mLICertifySchema.setModifyDate(mCurrentDate);
            mLICertifySchema.setModifyTime(mCurrentTime);
            mLICertifySchema.setOperator(tOperator);
            fpMap.put(mLICertifySchema, "UPDATE");
        }
        catch (Exception ex)
        {
            String tStrErr = "在准备往后层处理所需要的数据时LICertify表数据错误";
            buildError("UpdateLICertify", tStrErr);
            mDealFlag = "13"; // 13:在准备往后层处理所需要的数据时LICertify表数据错误
            return false;
        }
        debugLog("+++++LICertify表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * LICertifyInsured更新
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean updateLICertifyInsured(MMap fpMap)
    {
        debugLog("+++++LICertifyInsured表数据准备处理开始+++++"); //Debug
        if (!mBasicInfoFlg && !mGeneralInfoFlg)
        {
            debugLog("关键及一般信息未变化，不需要修改LICertifyInsured表"); //Debug
            return true;
        }
        String tName = mLICardActModifySchema.getName();
        String tSex = mLICardActModifySchema.getSex();
        String tBirthday = mLICardActModifySchema.getBirthday();
        String tIdNo = mLICardActModifySchema.getIdNo();
        String tMobile = mLICardActModifySchema.getMobile();
        String tPhone = mLICardActModifySchema.getPhone();
        String tPostalAddress = mLICardActModifySchema.getPostalAddress();
        String tZipCode = mLICardActModifySchema.getZipCode();
        String tEmail = mLICardActModifySchema.getEMail();
        String tOperator = mGlobalInput.Operator;
        try
        {
            if (mBasicInfoFlg)
            {
                mLICertifyInsuredSchema.setName(tName);
                mLICertifyInsuredSchema.setSex(tSex);
                mLICertifyInsuredSchema.setBirthday(tBirthday);
                mLICertifyInsuredSchema.setIdNo(tIdNo);
            }
            if (mGeneralInfoFlg)
            {
                mLICertifyInsuredSchema.setMobile(tMobile);
                mLICertifyInsuredSchema.setPhone(tPhone);
                mLICertifyInsuredSchema.setZipCode(tZipCode);
                mLICertifyInsuredSchema.setEMail(tEmail);
                mLICertifyInsuredSchema.setPostalAddress(tPostalAddress);
            }
            mLICertifyInsuredSchema.setModifyDate(mCurrentDate);
            mLICertifyInsuredSchema.setModifyTime(mCurrentTime);
            mLICertifyInsuredSchema.setOperator(tOperator);
            fpMap.put(mLICertifyInsuredSchema, "UPDATE");
        }
        catch (Exception ex)
        {
            String tStrErr = "在准备往后层处理所需要的数据时LICertifyInsured表数据错误";
            buildError("updateLICertifyInsured", tStrErr);
            mDealFlag = "31"; // 31:在准备往后层处理所需要的数据时LICertifyInsured表数据错误
            return false;
        }
        debugLog("+++++LICertifyInsured表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * 增加LCAddress数据
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean insertLCAddress(MMap fpMap)
    {
        debugLog("+++++LCAddress表数据准备处理开始+++++"); //Debug
        if (!mGeneralInfoFlg)
        {
            debugLog("一般信息未变化，不需要修改LCAddress表"); //Debug
            return true;
        }
        String tMobile = mLICardActModifySchema.getMobile();
        String tPhone = mLICardActModifySchema.getPhone();
        String tPostalAddress = mLICardActModifySchema.getPostalAddress();
        String tZipCode = mLICardActModifySchema.getZipCode();
        String tEmail = mLICardActModifySchema.getEMail();
        String tInsuredNo = mLCContSchema.getInsuredNo();
        String tOperator = mGlobalInput.Operator;
        LCAddressSchema mLCAddressSchema = new LCAddressSchema();
        try
        {
            mLCAddressSchema.setCustomerNo(tInsuredNo);
            mLCAddressSchema.setAddressNo(mAddressNo);
            mLCAddressSchema.setMobile(tMobile);
            mLCAddressSchema.setPhone(tPhone);
            mLCAddressSchema.setZipCode(tZipCode);
            mLCAddressSchema.setEMail(tEmail);
            mLCAddressSchema.setPostalAddress(tPostalAddress);
            mLCAddressSchema.setMakeDate(mCurrentDate);
            mLCAddressSchema.setMakeTime(mCurrentTime);
            mLCAddressSchema.setModifyDate(mCurrentDate);
            mLCAddressSchema.setModifyTime(mCurrentTime);
            mLCAddressSchema.setOperator(tOperator);
            fpMap.put(mLCAddressSchema, "INSERT");
        }
        catch (Exception ex)
        {
            String tStrErr = "在准备往后层处理所需要的数据时LCAddress表数据错误";
            buildError("insertLCAddress", tStrErr);
            mDealFlag = "41"; // 41:在准备往后层处理所需要的数据时LCAddress表数据错误
            return false;
        }
        debugLog("+++++LCAddress表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * LCInsured更新
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean updateLCInsured(MMap fpMap)
    {
        debugLog("+++++LCInsure表数据准备处理开始+++++"); //Debug
        if (!mGeneralInfoFlg)
        {
            debugLog("一般信息未变化，不需要修改LCInsure表"); //Debug
            return true;
        }
        String tContNo = mLICertifySchema.getContNo();
        String tInsuredNo = mLCContSchema.getInsuredNo();
        String tOperator = mGlobalInput.Operator;
        LCInsuredDB mLCInsuredDB = new LCInsuredDB();
        LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
        try
        {
            mLCInsuredDB.setContNo(tContNo);
            mLCInsuredDB.setInsuredNo(tInsuredNo);
            mLCInsuredDB.getInfo();
            mLCInsuredSchema = mLCInsuredDB.getSchema();
            mLCInsuredSchema.setAddressNo(mAddressNo);
            mLCInsuredSchema.setModifyDate(mCurrentDate);
            mLCInsuredSchema.setModifyTime(mCurrentTime);
            mLCInsuredSchema.setOperator(tOperator);
            fpMap.put(mLCInsuredSchema, "UPDATE");
        }
        catch (Exception ex)
        {
            this.mErrors.copyAllErrors(mLCInsuredDB.mErrors);
            String tStrErr = "在准备往后层处理所需要的数据时LCInsure表数据错误";
            buildError("updateLCInsured", tStrErr);
            mDealFlag = "51"; // 51:在准备往后层处理所需要的数据时LCInsured表数据错误
            return false;
        }
        debugLog("+++++LCInsure表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * LICardActModify更新
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean updateLICardActModify(MMap fpMap)
    {
        debugLog("+++++LICardActModify表数据准备处理开始+++++"); //Debug
        String tOperator = mGlobalInput.Operator;
        try
        {
            mLICardActModifySchema.setDealFlag(mDealFlag);
            mLICardActModifySchema.setDealDate(mCurrentDate);
            mLICardActModifySchema.setModifyDate(mCurrentDate);
            mLICardActModifySchema.setModifyTime(mCurrentTime);
            mLICardActModifySchema.setOperator(tOperator);
            fpMap.put(mLICardActModifySchema, "UPDATE");
        }
        catch (Exception ex)
        {
            String tStrErr = "在准备往后层处理所需要的数据时LICardActModify表数据错误";
            buildError("updateLICardActModify", tStrErr);
            return false;
        }
        debugLog("+++++LICardActModify表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * LICardActiveInfoList更新
     * 
     * @param MMap
     * @return boolean(如果在处理过程中出错，则返回false,否则返回true)
     */
    private boolean updateLICardActiveInfoList(MMap fpMap)
    {
        debugLog("+++++LICardActiveInfoList表数据准备处理开始+++++"); //Debug
        String tCardNo = mLICardActModifySchema.getCardNo();
        String tCValidate = mLICardActModifySchema.getCValidate();
        String tName = mLICardActModifySchema.getName();
        String tSex = mLICardActModifySchema.getSex();
        String tBirthday = mLICardActModifySchema.getBirthday();
        String tIdNo = mLICardActModifySchema.getIdNo();
        String tPostalAddress = mLICardActModifySchema.getPostalAddress();
        String tZipCode = mLICardActModifySchema.getZipCode();
        String tPhone = mLICardActModifySchema.getPhone();
        String tMobile = mLICardActModifySchema.getMobile();
        String tEmail = mLICardActModifySchema.getEMail();
        String tOperator = mGlobalInput.Operator;
        LICardActiveInfoListDB mLICardActiveInfoListDB = new LICardActiveInfoListDB();
        LICardActiveInfoListSchema mLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
        try
        {
            mLICardActiveInfoListDB.setCardNo(tCardNo);
            mLICardActiveInfoListDB.setSequenceNo("1"); // 目前只能处理单被保人情况
            mLICardActiveInfoListDB.getInfo();
            mLICardActiveInfoListSchema = mLICardActiveInfoListDB.getSchema();
            mLICardActiveInfoListSchema.setCValidate(tCValidate);
            mLICardActiveInfoListSchema.setName(tName);
            mLICardActiveInfoListSchema.setSex(tSex);
            mLICardActiveInfoListSchema.setBirthday(tBirthday);
            mLICardActiveInfoListSchema.setIdNo(tIdNo);
            mLICardActiveInfoListSchema.setPostalAddress(tPostalAddress);
            mLICardActiveInfoListSchema.setZipCode(tZipCode);
            mLICardActiveInfoListSchema.setPhone(tPhone);
            mLICardActiveInfoListSchema.setMobile(tMobile);
            mLICardActiveInfoListSchema.setEMail(tEmail);
            mLICardActiveInfoListSchema.setOperator(tOperator);
            mLICardActiveInfoListSchema.setModifyDate(mCurrentDate);
            mLICardActiveInfoListSchema.setModifyTime(mCurrentTime);
            fpMap.put(mLICardActiveInfoListSchema, "UPDATE");
        }
        catch (Exception ex)
        {
            this.mErrors.copyAllErrors(mLICardActiveInfoListDB.mErrors);
            String tStrErr = "在准备往后层处理所需要的数据时LICardActiveInfoList表数据错误";
            buildError("updateLICardActiveInfoList", tStrErr);
            mDealFlag = "61"; // 61:在准备往后层处理所需要的数据时LICardActiveInfoList表数据错误
            return false;
        }
        debugLog("+++++LICardActiveInfoList表数据准备处理结束+++++"); //Debug
        return true;
    }

    /**
     * Debug信息显示
     * 
     * @param String(Debug显示的信息)
     */
    public void debugLog(String str)
    {
        if (mDebugFlg)
        {
            System.out.println(str);
        }
    }

    /**
     * 创建错误日志。
     * 
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ActivationCardModify";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
