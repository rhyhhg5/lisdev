package com.sinosoft.lis.certify;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class CertifyStateModifyUI {

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;

	  public CertifyStateModifyUI() {}

	  /**
	  传输数据的公共方法
	  */
	  public boolean submitData(VData cInputData,String cOperate)
	  {
	    //将操作数据拷贝到本类中
	    this.mOperate = cOperate;

	    CertifyStateModifyBL tCertifyStateModifyBL=new CertifyStateModifyBL();

	    if (tCertifyStateModifyBL.submitData(cInputData,mOperate) == false)
		{
	  		// @@错误处理
	      this.mErrors.copyAllErrors(tCertifyStateModifyBL.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "ProposalQueryUI";
	      tError.functionName = "submitData";
	      tError.errorMessage = "数据查询失败!";
	      this.mErrors .addOneError(tError) ;
	      mInputData.clear();
	      return false;
		}
	    return true;
	  }

	  public VData getResult()
	  {
	  	return mInputData;
	  }
}
