package com.sinosoft.lis.certify;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.io.InputStream;

public class CardSendPrintBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 全局数据 */
    private VData mResult = new VData(); //返回结果
    private String mSQL = "";
    private String mReportType = ""; //报表类型
    private String mStartDate = "";
    private String mEndDate = "";
    private String mReceiveCom = "";
    private String mCardType = "";
    private String mStateType = "";
    private String mMagCom = "";
    private String mStartNo = "";
    private String mEndNo = "";
    private SSRS mSSRS; //存放数据
    TextTag textTag = new TextTag(); //新建一个TextTag的实例
    private GlobalInput mGI = new GlobalInput(); //用户登录信息
    private XmlExport mXmlExport;
    public CardSendPrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("CardSendPrintBL begin");
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        System.out.println("CardSendReportPrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "CardSendPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mGI = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (tTransferData == null || mGI == null) {
            buildError("getInputData", "从页面传入数据缺失！！！");
            return false;
        }
        mSQL = (String) tTransferData.getValueByName("PubSQL");
        mReportType = (String) tTransferData.getValueByName("ReportType");
        mStartDate = (String) tTransferData.getValueByName("StartDate");
        mEndDate = (String) tTransferData.getValueByName("EndDate");
        mReceiveCom = (String) tTransferData.getValueByName("ReceiveCom");
        mCardType = (String) tTransferData.getValueByName("CardType");
        mStateType = (String) tTransferData.getValueByName("StateType");
        mMagCom = (String) tTransferData.getValueByName("ManCom");
        mStartNo = (String) tTransferData.getValueByName("StartNo");
        mEndNo = (String) tTransferData.getValueByName("EndNo");
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {

        mXmlExport = new XmlExport(); //新建一个XmlExport的实例
        textTag.add("PrintDate", PubFun.getCurrentDate());
        textTag.add("PrintOperator", mGI.Operator);
        if (mReportType.equals("1")) {

            String[] title = {"单证类型", "单证名称", "发放数", "正常回销数", "遗失数",
                             "作废数", "毁损数"};
            mXmlExport.createDocument("CardSendList.vts", "printer"); //最好紧接着就初始化xml文档
            mXmlExport.addListTable(getListTable(), title);
        } else if (mReportType.equals("2")) {

            String[] title = {"领用人", "领用时间", "单证号码", "状态", "回销时间"};

            mXmlExport.createDocument("CardReceiveList.vts", "printer"); //最好紧接着就初始化xml文档
            mXmlExport.addListTable(getListTable(), title);

        }
        if (textTag.size() > 0) {
            mXmlExport.addTextTag(textTag);
        }
        // xmlexport.outputDocumentToFile("c:\\", "TaskPrint"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(mXmlExport);
        return true;
    }

    /**
     * 查询列表显示数据
     * @param
     * @return String
     */

    private boolean getSqlStr() {

        String strCon = "";
        String strName = "";
        String strSQL1 = "";
        String strSQL2 = "";
        String likName = "";
        String strNoBetween = "";

        if ("2".equals(mReportType)) {
            return true;
        }

        mSQL = "";
        if (mCardType != null && !mCardType.equals("")) {
            strCon = strCon + " and b.SubCode='" + mCardType + "'";
        }
        if (mStartNo != null && !mStartNo.equals("") && mEndNo != null &&
            !mEndNo.equals("")) {
            strNoBetween = " and (b.StartNo >='" + mStartNo +
                           "' and b.StartNo <='" + mEndNo
                           + "' or b.StartNo <='" + mStartNo +
                           "' and b.EndNo >='" + mStartNo + "')"
                           ;
        }
        strSQL1 = "select distinct  '','',char(c.CardNo) ,'未领用',''"
                  + " from LZCard b,LZCardNumber c"
                  + " where "
                  +
                  " c.CardType=b.SubCode and c.CardSerNo >=b.StartNo and c.CardSerNo <=b.EndNo "
                  + " and b.state in ('8','9')"
                  + strCon
                  + strNoBetween
                  + " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like'" +
                  mMagCom + "%%'))>0"
                  ;

        if (mReceiveCom != null && !mReceiveCom.equals("")) {
            likName = " and Name like '" + mReceiveCom + "%%'";
        }
        String strRec = "";
        if (mReceiveCom != null && !mReceiveCom.equals("")) {
            strRec = " and b.receivecom ='" +
                     mReceiveCom + "' ";
        }

        strName = "case when b.state in('3','4','5','6') " + strRec + "then substr(b.receivecom,2) else (select name from laagent where agentcode=substr(b.receivecom,2) "
                  + likName +
                  " union all select name from lacom where agentcom=substr(b.receivecom,2) "
                  + likName +
                  " union all select name from laagent where agentcode=substr(b.SendOutCom,2) "
                  + likName +
                  " union all select name from lacom where agentcom=substr(b.SendOutCom,2) "
                  + likName
                  + " ) end"
                  ;
        if (mStateType.equals("1")) {
            strCon = strCon + " and b.state in ('10','11')";
        } else if (mStateType == null || mStateType.equals("")) {
            strCon = strCon +
                     " and b.state in ('2','3','4','5','6','8','9','10','11','12')";
        } else {
            strCon = strCon + " and b.state ='" + mStateType + "'";
        }

        if (mStartDate != null && !mStartDate.equals("") && mEndDate != null &&
            !mEndDate.equals("")) {
            strCon = strCon +
                     "  and (select count(*) from LZCardtrack e where "
                     + " e.HandleDate>='" + mStartDate +
                     "' and  e.HandleDate<='" + mEndDate + "'"
                     + " and e.SubCode=b.SubCode  and e.CertifyCode=b.CertifyCode and e.StartNo =b.StartNo and e.EndNo =b.EndNo and e.RiskCode=b.RiskCode and e.RiskVersion=b.RiskVersion"
                     + " and e.state in ('10','11'))>0";
        }

        if (mStartDate != null && !mStartDate.equals("") &&
            (mEndDate == null || mEndDate.equals(""))) {
            strCon = strCon + " and (select count(*) from LZCardtrack e where "
                     + " e.HandleDate>='" + mStartDate + "'"
                     + " and e.SubCode=b.SubCode  and e.CertifyCode=b.CertifyCode and e.StartNo =b.StartNo and e.EndNo =b.EndNo and e.RiskCode=b.RiskCode and e.RiskVersion=b.RiskVersion"
                     + " and e.state in ('10','11'))>0";
        }
        if ((mStartDate == null || mStartDate.equals("")) &&
            mEndDate != null && !mEndDate.equals("")) {
            strCon = strCon + " and (select count(*) from LZCardtrack e where "
                     + " e.HandleDate<='" + mEndDate + "'"
                     + " and e.SubCode=b.SubCode  and e.CertifyCode=b.CertifyCode and e.StartNo =b.StartNo and e.EndNo =b.EndNo and e.RiskCode=b.RiskCode and e.RiskVersion=b.RiskVersion"
                     + " and e.state in ('10','11'))>0";

        }

        strSQL2 = "select " + strName
                  + " ,char((select HandleDate from lzcardtrack a where a.SubCode=b.SubCode and a.CertifyCode=b.CertifyCode and a.StartNo =b.StartNo and a.EndNo =b.EndNo and a.RiskCode=b.RiskCode and a.RiskVersion=b.RiskVersion and a.state in ('10','11') fetch first 1 rows only )) "
                  + ",c.CardNo ,"
                  +
                  "(select codename from ldcode where codetype='certifystate' and code=b.state),"
                  + " case when b.state in ('2','3','4','5','6') then char(b.HandleDate) else '' end "
                  + " from LZCard b,LZCardNumber c"
                  + " where 1=1"
                  + " and (" + strName
                  + " ) is not null"
                  + " and c.CardType=b.SubCode and c.CardSerNo >=b.StartNo and c.CardSerNo <=b.EndNo "
                  + strCon
                  + strNoBetween
                  + " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like'" +
                  mMagCom + "%%'))>0"
                  ;
        if (mStateType.equals("0")) {
            mSQL = strSQL1 + " with ur";
        } else {
            mSQL = strSQL2 + " with ur";
        }
        if (mStateType == null || mStateType.equals("")) {
            mSQL = strSQL2 + " with ur";
            if (mReceiveCom == null || mReceiveCom.equals("")) {
                mSQL = strSQL1 + " union all " + strSQL2 + " with ur";

            }

        }
        System.out.println(mSQL);
        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
//        if (mReportType.equals("2")) {
//
//            if (!getSqlStr()) {
//                return false;
//            }
//        }
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL(mSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[mSSRS.getMaxCol()];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }


    public static void main(String[] args) {
        CardSendPrintBL p = new CardSendPrintBL();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";
        String tsql = " select b.SubCode,a.CertifyName,(select sum(SumCount) from LZCard c where '1160634295000'='1160634295000' and  c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode   and c.state in ('2','8','9','10','11','12') group by c.HandleDate),(select sum(SumCount) from LZCard c where c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode   and c.state in ('2') group by c.HandleDate),(select sum(SumCount) from LZCard c where c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode   and c.state in ('4') ),(select sum(SumCount) from LZCard c where c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode   and c.state in ('6') ),(select sum(SumCount) from LZCard c where c.SubCode=b.SubCode and c.CertifyCode=b.CertifyCode   and c.state in ('3') ) from LMCertifyDes a,LZCard b where a.SubCode=b.SubCode and a.CertifyCode=b.CertifyCode and b.state in ('2','4','5','6','8','9','10','11','12') and a.CertifyClass='D' group by b.SubCode,a.CertifyName,b.CertifyCode fetch first 3000 rows only";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PubSQL", tsql);
        tTransferData.setNameAndValue("ReportType", "2");
        tTransferData.setNameAndValue("StateType", "1");
        tTransferData.setNameAndValue("ManCom", "86");
        VData tVData = new VData();
        tVData.addElement(tGI);

        tVData.addElement(tTransferData);
        p.submitData(tVData, "PRINT");

    }

}
