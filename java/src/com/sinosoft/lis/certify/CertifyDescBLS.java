/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.sql.Connection;

import com.sinosoft.lis.db.LMCardRiskDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.vdb.LMCardRiskDBSet;
import com.sinosoft.lis.vdb.LMCertifyDesDBSet;
import com.sinosoft.lis.vschema.LMCardRiskSet;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统案件－报案保存功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 */

public class CertifyDescBLS
{
    //传输数据类
    private VData mInputData;
    private String mOperateType = "";//操作类型
    private String mCertifyClass = ""; //记录单证类型 P or D
    private LMCertifyDesSet mLMCertifyDesSet = new LMCertifyDesSet();//单证描述表
    private LMCardRiskSet mLMCardRiskSet = new LMCardRiskSet();//描述定额单编码和险种之间的对应关系

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    public CertifyDescBLS()
    {}

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start CertifyDescBLS Submit...");
        boolean tReturn = false;
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("Start Report BLS Submit...");
        mOperateType = (String) cInputData.get(0);
        mCertifyClass = (String) cInputData.get(1);
        System.out.println("所要执行的操作符号是" + mOperateType);
        System.out.println("BLS中获取的单证类型是" + mCertifyClass);
        mLMCertifyDesSet = ((LMCertifyDesSet) cInputData.getObjectByObjectName(
                "LMCertifyDesSet", 0));
        if (mCertifyClass.equals("D"))
        {
            mLMCardRiskSet = ((LMCardRiskSet) cInputData.getObjectByObjectName(
                    "LMCardRiskSet", 0));
            System.out.println("BLS中接收的数据记录是" + mLMCardRiskSet.size());
        }
        System.out.println("BLS 中接收的数据的记录是" + mLMCertifyDesSet.size());

        if (mOperateType.equals("INSERT"))
        {
            if (!this.saveData())
                return false;
        }
        if (mOperateType.equals("UPDATE"))
        {
            if (!updateData())
                return false;
        }
        if (mOperateType.equals("DELETE"))
        {
            if (!deleteData())
                return false;
        }
        System.out.println("End Report BLS Submit...");
        mInputData = null;
        return true;
    }

    private boolean saveData()
    {
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LMCertifyDesDBSet tLMCertifyDesDBSet = new LMCertifyDesDBSet(conn);
            tLMCertifyDesDBSet.set(mLMCertifyDesSet);
            if (tLMCertifyDesDBSet.insert() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCertifyDesDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyDescBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
//            if (mCertifyClass.equals("D"))
//            {
//                LMCardRiskDBSet tLMCardRiskDBSet = new LMCardRiskDBSet(conn);
//                tLMCardRiskDBSet.set(mLMCardRiskSet);
//                if (!tLMCardRiskDBSet.insert())
//                {
//                    CError tError = new CError();
//                    tError.moduleName = "CertifyDescBLS";
//                    tError.functionName = "saveData";
//                    tError.errorMessage = "请您检查险种号码是否出现重复！！！";
//                    this.mErrors.addOneError(tError);
//                    conn.rollback();
//                    conn.close();
//                    return false;
//                }
//            }
            conn.commit();
            conn.close();
        } // end of try
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        return true;
    }

    private boolean deleteData()
    {
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB(conn);
            LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
            tLMCertifyDesSchema.setSchema(mLMCertifyDesSet.get(1));
            tLMCertifyDesDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
            if (!tLMCertifyDesDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyDescBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除失败！！！";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } // end of try
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        return true;
    }

    //先进行删除，在进行新增操作
    private boolean updateData()
    {
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "CertifyDescBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
            tLMCertifyDesSchema.setSchema(mLMCertifyDesSet.get(1));
            tLMCertifyDesDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
            if (!tLMCertifyDesDB.delete())
            {
                this.mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CertifyDesBLS";
                tError.functionName = "DeleteData";
                tError.errorMessage = "删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LMCertifyDesDBSet tLMCertifyDesDBSet = new LMCertifyDesDBSet();
            mLMCertifyDesSet.clear();
            mLMCertifyDesSet.add(tLMCertifyDesSchema);
            System.out.println("在BLS中所得到的单证代码是" +
                               tLMCertifyDesSchema.getCertifyCode());
            System.out.println("在BLS中所得到的险种版本是" +
                               tLMCertifyDesSchema.getRiskVersion());
            tLMCertifyDesDBSet.set(mLMCertifyDesSet);
            if (!tLMCertifyDesDBSet.insert())
            {
                System.out.println("开始执行save操作4");
                this.mErrors.copyAllErrors(tLMCertifyDesDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ReportBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //添加对定额单险种信息描述表的操作；
            if (mCertifyClass.equals("D"))
            {
                System.out.println("5-26 BLS中的单证编码是" +
                                   tLMCertifyDesSchema.getCertifyCode());
                LMCardRiskDB tLMCardRiskDB = new LMCardRiskDB(conn);
                tLMCardRiskDB.setCertifyCode(tLMCertifyDesSchema.getCertifyCode());
                if (!tLMCardRiskDB.deleteSQL())
                {
                    CError tError = new CError();
                    tError.moduleName = "CertifyDesBLS";
                    tError.functionName = "DeleteData";
                    tError.errorMessage = "定额单险种信息删除失败！！！";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                LMCardRiskDBSet tLMCardRiskDBSet = new LMCardRiskDBSet(conn);
                tLMCardRiskDBSet.set(mLMCardRiskSet);
                if (mLMCardRiskSet!=null) {
                    if (!tLMCardRiskDBSet.insert()) {
                        CError tError = new CError();
                        tError.moduleName = "CertifyDescBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "请您检查险种号码是否出现重复！！！";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
            }
            conn.commit();
            conn.close();
            System.out.println("save7");
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "CaseCureBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        return true;
    }
}
