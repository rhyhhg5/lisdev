package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

//程序名称：CertifyActiveModifyBL.java
//程序功能：
//创建日期：2011-6-14 
//创建人  ：XiePan
//更新记录：  更新人    更新日期     更新原因/内容

public class CertifyActiveModifyBL {
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = new GlobalInput();

	// 相关参数
	private String mCertifyCode = new String();

	private String mStartNo = new String();

	private String mEndNo = new String();

	private String mCardType = new String();

	private String mAgentCode = new String();

	private String mAgentType = new String();

	private int mcardlength = 0;

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private TransferData tTransferData = new TransferData();

	private MMap map = new MMap();

	public CertifyActiveModifyBL() {
	}

	public boolean submitData(VData data) {
		if (!getInputData(data)) {
			return false;
		}

		if (!CheckData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		VData tVData = new VData();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "")) {
			System.out.println(tPubSubmit.mErrors.getErrContent());
			mErrors.addOneError("提交数库失败");
			return false;
		}
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		int intStartNo = Integer.parseInt(mStartNo.trim());
		int intEndNo = Integer.parseInt(mEndNo.trim());
		Reflections tref=new Reflections();
		for (int i = intStartNo; i <= intEndNo; i++) {
			String strSerNo = PubFun.LCh(String.valueOf(i), "0", mcardlength);
			LZCardSchema tLZCardSchema = new LZCardSchema();
			LZCardDB tLZCardDB= new LZCardDB();
			tLZCardDB.setCertifyCode(mCertifyCode);
			tLZCardDB.setSubCode(mCardType);
			tLZCardDB.setRiskCode("0");
			tLZCardDB.setRiskVersion("0");
			tLZCardDB.setStartNo(strSerNo);
			tLZCardDB.setEndNo(strSerNo);
			tLZCardDB.setState("14");
			if(!tLZCardDB.getInfo()){
				// @@错误处理
				System.out.println("CertifyActiveModifyBL+dealData++--");
				CError tError = new CError();
				tError.moduleName = "CertifyActiveModifyBL";
				tError.functionName = "dealData";
				tError.errorMessage = "单证编码"+mCertifyCode+"下号码为"+strSerNo+"的单证不是已激活状态,无法进行变更！";
				this.mErrors.addOneError(tError);
				return false;
			}
			tLZCardSchema=tLZCardDB.getSchema();
			tLZCardSchema.setReceiveCom(mAgentType+mAgentCode);
			tLZCardSchema.setOperator(mGI.Operator);
			tLZCardSchema.setModifyDate(mCurrentDate);
			tLZCardSchema.setModifyTime(mCurrentTime);
			map.put(tLZCardSchema, "DELETE&INSERT");
			LZCardTrackSchema tLZCardTrackSchema = new LZCardTrackSchema();
			tref.transFields(tLZCardTrackSchema,tLZCardSchema);
			//state为14OperateFlag为1时表示该卡在已激活状态下进行了变更下发机构的操作.
			tLZCardTrackSchema.setOperateFlag("1");
			tLZCardTrackSchema.setMakeDate(mCurrentDate);
			tLZCardTrackSchema.setMakeTime(mCurrentTime);
			map.put(tLZCardTrackSchema, "INSERT");
		}
		return true;
	}

	/**
	 * CheckData
	 * 
	 * @return boolean
	 */
	private boolean CheckData() {
		int intStartNo = Integer.parseInt(mStartNo.trim());
		int intEndNo = Integer.parseInt(mEndNo.trim());
		ExeSQL tExeSQL = new ExeSQL();
		for (int i = intStartNo; i <= intEndNo; i++) {
			String strSerNo = PubFun.LCh(String.valueOf(i), "0", mcardlength);
			String tCardNo = tExeSQL.getOneValue("select cardno from lzcardnumber where cardtype='" + mCardType + "' and cardserno='" + strSerNo + "'");
			String tCheck = tExeSQL.getOneValue("select 1 from licertify where cardno='" + tCardNo + "' union select 1 from libcertify where cardno='" + tCardNo + "' ");
			if (tCheck == null || !tCheck.equals("1")) {
				continue;
			} else {
				// @@错误处理
				System.out.println("CertifyActiveModifyBL+CheckData++--");
				CError tError = new CError();
				tError.moduleName = "CertifyActiveModifyBL";
				tError.functionName = "CheckData";
				tError.errorMessage = "卡号为：" + tCardNo + "的单证已经操作了卡折导入,请操作卡折删除后再在此页面变更即可。";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		return true;
	}

	private boolean getInputData(VData data) {
		mGI.setSchema((GlobalInput) data.getObjectByObjectName("GlobalInput", 0));
		if (mGI == null || mGI.Operator == null) {
			mErrors.addOneError("没有获取到操作员信息");
			return false;
		}
		tTransferData = (TransferData) data.getObjectByObjectName("TransferData", 0);
		mCertifyCode = (String) tTransferData.getValueByName("CertifyCodeModify");
		if (mCertifyCode == null || mCertifyCode.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mCertifyCode获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		mStartNo = (String) tTransferData.getValueByName("StartNoModify");
		if (mStartNo == null || mStartNo.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mStartNo获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		mEndNo = (String) tTransferData.getValueByName("EndNoModify");
		if (mEndNo == null || mEndNo.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mEndNo获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		mCardType = (String) tTransferData.getValueByName("CardTypeModify");
		if (mCardType == null || mCardType.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mCardType获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAgentCode = (String) tTransferData.getValueByName("AgentCode");
		if (mAgentCode == null || mAgentCode.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mAgentCode获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAgentType = (String) tTransferData.getValueByName("AgentType");
		if (mAgentType == null || mAgentType.equals("")) {
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mAgentType获取失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!(mAgentType.equals("E")||mAgentType.equals("D"))){
			// @@错误处理
			System.out.println("CertifyActiveModifyBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "CertifyActiveModifyBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收的代理人类型有误!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mcardlength = mStartNo.length();
		return true;
	}

	public static void main(String[] args) {
		CertifyActiveModifyBL bl = new CertifyActiveModifyBL();
	}
}

