package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZAccessDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZAccessSchema;
import com.sinosoft.lis.vschema.LZAccessSet;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006-04-14</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CertifySendConBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LZAccessSchema mLZAccessSchema = new LZAccessSchema();
    private LZAccessSet mLZAccessSet = new LZAccessSet();
    private LZAccessSet saveLZAccessSet = new LZAccessSet();

    MMap mMap = new MMap();
    PubSubmit tPubSubmit = new PubSubmit();

    //private LDComSet mLDComSet=new LDComSet();
    public CertifySendConBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
System.out.println("come to BL..........................");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
            return false;

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //进行插入数据
        if (mOperate.equals("INSERT||CONFIG")) {
            if (!insertData()) {
                // @@错误处理
                return false;
            }
        }
    //对数据进行修改操作
        if (mOperate.equals("DELETE||CONFIG")) {
            if (!deleteData()) {
                return false;
            }
            System.out.println("---updateData---");
        }
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        LZAccessSchema tLZAccessSchema ;
        for (int i=1; i<=mLZAccessSet.size();i++)
        {
            tLZAccessSchema = new LZAccessSchema();
            tLZAccessSchema=mLZAccessSet.get(i);
            if (!tLZAccessSchema.getReceiveCom().substring(0,2).equals("86"))
            {
                tLZAccessSchema.setReceiveCom("B"+tLZAccessSchema.getReceiveCom());
            }
            else //暂不区分机构和单证管理员，以后是否区分再议
            {
                tLZAccessSchema.setReceiveCom("A"+tLZAccessSchema.getReceiveCom());
            }
            LZAccessDB dbLZAccess = new LZAccessDB();
            dbLZAccess.setSendOutCom(tLZAccessSchema.getSendOutCom());
            dbLZAccess.setReceiveCom(tLZAccessSchema.getReceiveCom());
            if (dbLZAccess.getInfo())
            {
                buildError("insertData","已经存在要添加的发放关系!");
                return false;
            }
            saveLZAccessSet.add(tLZAccessSchema);
        }
        mMap.put(saveLZAccessSet,"INSERT");

        //准备往后台的数据
       if (!prepareOutputData())
           return false;


        if (!tPubSubmit.submitData(this.mInputData, ""))
        {
           if (tPubSubmit.mErrors.needDealError())
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               CError tError = new CError();
               tError.moduleName = "ComBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "数据提交失败!";
               this.mErrors.addOneError(tError);
               return false;
           }
           tPubSubmit=null;
           mMap=null;
           return false;
       }
System.out.println("after PubSubmit ............");

       return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        LZAccessSchema tLZAccessSchema ;
        for (int i=1; i<=mLZAccessSet.size();i++)
        {
            tLZAccessSchema = new LZAccessSchema();
            tLZAccessSchema=mLZAccessSet.get(i);

            LZAccessDB dbLZAccess = new LZAccessDB();
            dbLZAccess.setSendOutCom(tLZAccessSchema.getSendOutCom());
            dbLZAccess.setReceiveCom(tLZAccessSchema.getReceiveCom());
            if (!dbLZAccess.getInfo())
            {
                buildError("insertData","没有要删除的记录!");
                return false;
            }
            saveLZAccessSet.add(tLZAccessSchema);
        }

        mMap.put(saveLZAccessSet,"DELETE");

      //准备往后台的数据
      if (!prepareOutputData())
        return false;
      if (!tPubSubmit.submitData(this.mInputData, ""))
       {
          buildError("deleteData","删除失败");
          return false;
      }
      tPubSubmit=null;
      mMap=null;
      return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLZAccessSet.set((LZAccessSet) cInputData.
                                    getObjectByObjectName("LZAccessSet", 0));
        System.out.println("in getInput......."+this.mLZAccessSet.size());
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LZAccessDB tLZAccessDB = new LZAccessDB();
        tLZAccessDB.setSchema(this.mLZAccessSchema);
        ///this.mLDComSet=tLDComDB.query();
        //this.mResult.add(this.mLDComSet);
        //如果有需要处理的错误，则返回
        if (tLZAccessDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLZAccessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();

      cError.moduleName = "CertifyFunc";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;

      System.out.println("In CertifyFunc buildError() : " + szErrMsg);
      mErrors.addOneError(cError);
  }

}


