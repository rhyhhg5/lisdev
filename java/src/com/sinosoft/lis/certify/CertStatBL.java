/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTempSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.LZCardTempSet;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: CertStatBL </p>
 * <p>Description: 单证统计查询的实现文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: lis
 * @CreateDate：2002-04-10
 */
public class CertStatBL
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 全局数据 */
    private GlobalInput m_GlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String m_strOperate;


    /** 业务处理相关变量 */
    private LZCardSet m_LZCardSet = new LZCardSet();
    private String m_strWhere = "";
    private Hashtable m_hashParams = null;

    public static String STAT_STOCK = "1"; // 库存
    public static String STAT_SENDED = "2"; // 已发放
    public static String STAT_TAKEBACKED = "3"; // 已回收
    public static String STAT_S_T = "4"; // 发放未回收
    public static String STAT_GROSS = "5"; // 入库总量

    public static String PRT_STATE = "0"; // 查询打印状态表中的数据
    public static String PRT_TRACK = "1"; // 查询打印轨迹表中的数据

    public CertStatBL()
    {
    }

    public static void main(String[] args)
    {

        CertStatBL test = new CertStatBL();
        LZCardTempSchema t = new LZCardTempSchema();
        LZCardTempSet setT = new LZCardTempSet();
        VData v = new VData();
        t.setAmnt(2000);
        t.setCertifyCode("8611240102001615");
        t.setEndNo("8611240102001700");
        t.setInvaliDate("2004-09-25");
        t.setMakeDate("2004-09-25");
        t.setMakeTime("2004-09-25");
        t.setModifyDate("2004-09-25");
        t.setModifyTime("15:20");
        t.setOperateFlag("1");
        t.setOperator("0");
        t.setPrem(200);
        t.setRiskCode("2401");
        t.setState("1");
        setT.add(t);
        v.add(setT);
        try
        {
            test.processData("1", "", v);
        }
        catch (Exception e)
        {}

    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        m_strOperate = verifyOperate(cOperate);
        if (m_strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
            return false;

        if (!dealData())
            return false;

//    VData tVData = new VData();
//    if( !prepareOutputData(tVData) )
//      return false;
//
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            if (m_strOperate.equals("QUERY||MAIN"))
            {
                return submitQueryMain();
            }
            else if (m_strOperate.equals("QUERY||PRINT"))
            {
                return submitQueryPrint();
            }
            else if (m_strOperate.equals("REPORT||QUERY"))
            {
                return submitReportQuery();
            }
            else if (m_strOperate.equals("REPORT||PRINT"))
            {
                return submitReportPrint();
            }
            else
            {
                buildError("dealData", "不支持的操作字符串");
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.m_GlobalInput.setSchema((GlobalInput) cInputData.
                                         getObjectByObjectName("GlobalInput", 0));
            m_LZCardSet = (LZCardSet) cInputData.getObjectByObjectName(
                    "LZCardSet", 0);
            m_hashParams = (Hashtable) cInputData.getObjectByObjectName(
                    "Hashtable", 0);

            if (m_LZCardSet == null)
            {
                buildError("getInputData", "没有传入所需要的查询条件");
                return false;
            }

            LZCardSchema tLZCardSchema = m_LZCardSet.get(1);

            if (tLZCardSchema.getState() == null ||
                tLZCardSchema.getState().equals(""))
            {
                buildError("getInputData", "请输入统计条件");
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getInputData", ex.getMessage());
            return false;
        }
        return true;
    }

    private boolean prepareOutputData(VData aVData)
    {
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertStatBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                {"QUERY||MAIN", "QUERY||PRINT", "REPORT||QUERY",
                "REPORT||PRINT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }


    /**
     * 解析传入的条件，生成查询需要的Where条件。
     * @param aLZCardSet
     * @return
     * @throws Exception
     */
    private VData parseParams(LZCardSet aLZCardSet) throws Exception
    {
        LZCardSchema tLZCardSchema = null;

        String strSQL = "";
        String strWhere = "";

        tLZCardSchema = aLZCardSet.get(1);

        String strState = tLZCardSchema.getState();

        String strSendOutCom = tLZCardSchema.getSendOutCom();
        String strReceiveCom = tLZCardSchema.getReceiveCom();

        strWhere += getWherePart("CertifyCode", tLZCardSchema.getCertifyCode());
        strWhere += getWherePart("Handler", tLZCardSchema.getHandler());
        strWhere += getWherePart("StateFlag", tLZCardSchema.getStateFlag());
        //strWhere += getWherePart("Operator", tLZCardSchema.getOperator());
        strWhere +=
                getWherePart("StartNo", "EndNo", tLZCardSchema.getStartNo(), 0);
        strWhere +=
                getWherePart("StartNo", "EndNo", tLZCardSchema.getEndNo(), 0);
        strWhere += getWherePart("HandleDate", aLZCardSet.get(1).getHandleDate(),
                                 aLZCardSet.get(2).getHandleDate(), 1);

        if (strState.equals(CertStatBL.STAT_STOCK))
        {
            strSQL = "SELECT * FROM LZCard WHERE 1=1" + strWhere;

            // 库存：只查出处于发放状态的单证
            strSQL += " AND StateFlag in ('0','7')" +
                    getWherePart("ReceiveCom", strReceiveCom);

        }
        else if (strState.equals(CertStatBL.STAT_SENDED))
        {
            strSQL = "SELECT * FROM LZCardTrack WHERE 1=1" + strWhere;

            // 已发放：从轨迹表中查出处于发放和反发放状态的单证
            strSQL += " AND (( OperateFlag = '0'" +
                    getWherePart("SendOutCom", strSendOutCom) +
                    getWherePart("ReceiveCom", strReceiveCom) +
                    " ) OR (OperateFlag = '2'" +
                    getWherePart("ReceiveCom", strSendOutCom) +
                    getWherePart("SendOutCom", strReceiveCom) +
                    " ))";

        }
        else if (strState.equals(CertStatBL.STAT_TAKEBACKED))
        {
            strSQL = "SELECT * FROM LZCardTrack WHERE 1=1" + strWhere;

            // 已回收：从轨迹表中查出处于回收和反回收状态的单证
            strSQL += " AND (( OperateFlag = '1'" +
                    getWherePart("SendOutCom", strSendOutCom) +
                    getWherePart("ReceiveCom", strReceiveCom) +
                    " ) OR (OperateFlag = '3'" +
                    getWherePart("ReceiveCom", strSendOutCom) +
                    getWherePart("SendOutCom", strReceiveCom) +
                    " ))";

        }
        else if (strState.equals(CertStatBL.STAT_S_T))
        {
            strSQL = "SELECT * FROM LZCardTrack WHERE 1=1" + strWhere;

            // 发放未回收：查询出轨迹表中已发放和已回收单证的差值
            strSQL += " AND (( (OperateFlag = '0' OR OperateFlag = '3')" +
                    getWherePart("SendOutCom", strSendOutCom) +
                    getWherePart("ReceiveCom", strReceiveCom) +
                    " ) OR ( (OperateFlag = '1' OR OperateFlag = '2')" +
                    getWherePart("ReceiveCom", strSendOutCom) +
                    getWherePart("SendOutCom", strReceiveCom) +
                    " ))";

        }
        else if (strState.equals(CertStatBL.STAT_GROSS))
        {
            strSQL = "SELECT * FROM LZCardTrack WHERE 1=1" + strWhere;

            // 入库总量：从轨迹表中查询出发放状态的单证
            strSQL += " AND OperateFlag = '0'" +
                    getWherePart("ReceiveCom", strReceiveCom);

        }

        strSQL +=
                " ORDER BY CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo";

        System.out.println("SQL");
        System.out.println(strSQL);

        VData tVData = new VData();

        tVData.add(0, strSQL);

        return tVData;
    }

    private String getWherePart(String strColName, String strColValue)
    {
        if (strColValue == null || strColValue.equals(""))
        {
            return "";
        }
        return " AND " + strColName + " = '" + strColValue + "'";
    }

    private String getWherePart(String strCol1, String strCol2, String strCol3,
                                int nFlag)
    {
        if (nFlag == 0)
        {
            if (strCol3 == null || strCol3.equals(""))
            {
                return "";
            }

            return " AND " + strCol1 + " <= '" + strCol3 + "' AND " +
                    strCol2 + " >= '" + strCol3 + "'";
        }
        else
        {
            String str = "";

            if (strCol2 == null || strCol2.equals(""))
            {
                str += "";
            }
            else
            {
                str += " AND " + strCol1 + " >= '" + strCol2 + "'";
            }

            if (strCol3 == null || strCol3.equals(""))
            {
                str += "";
            }
            else
            {
                str += " AND " + strCol1 + " <= '" + strCol3 + "'";
            }

            return str;
        }
    }


    /**
     * 查询函数
     * @throws Exception
     */
    private boolean submitQueryMain() throws Exception
    {
        VData tVData = parseParams(m_LZCardSet); // 解析查询条件

        mResult = genQueryResult(m_LZCardSet.get(1).getState(), // 取得要统计的标志
                                 tVData); // 利用临时表对查询结果进行筛选
        return true;
    }


    /**
     * 查询打印所对应的参数解析函数
     * @param aLZCardSet
     * @return
     * @throws Exception
     */
    private VData parseParamsPrint(LZCardSet aLZCardSet) throws Exception
    {
        LZCardSchema tLZCardSchema = null;

        String strSQL = "";
        String strWhere = "";

        String strManageCom = "A" + m_GlobalInput.ComCode;

        tLZCardSchema = aLZCardSet.get(1);

        String strState = tLZCardSchema.getState();

        strWhere += getWherePart("CertifyCode", tLZCardSchema.getCertifyCode());
        strWhere += getWherePart("SendOutCom", tLZCardSchema.getSendOutCom());
        strWhere += getWherePart("ReceiveCom", tLZCardSchema.getReceiveCom());
        strWhere += getWherePart("Handler", tLZCardSchema.getHandler());
        strWhere += getWherePart("Operator", tLZCardSchema.getOperator());
        strWhere += getWherePart("TakeBackNo", tLZCardSchema.getTakeBackNo());
        strWhere += getWherePart("HandleDate", aLZCardSet.get(1).getHandleDate(),
                                 aLZCardSet.get(2).getHandleDate(), 1);

        if (strState.equals(CertStatBL.PRT_STATE))
        {
            strSQL = "SELECT * FROM LZCard WHERE 1=1";

            // strWhere += " AND ReceiveCom LIKE '"  + strManageCom + "%'";

        }
        else if (strState.equals(CertStatBL.PRT_TRACK))
        {
            strSQL = "SELECT * FROM LZCardTrack WHERE 1=1";

            // strWhere += " AND ReceiveCom LIKE '"  + strManageCom + "%'";

        }

        strSQL += strWhere;
        strSQL +=
                " ORDER BY CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo";

        System.out.println("SQL : " + strSQL);

        VData tVData = new VData();

        tVData.add(0, strSQL);

        return tVData;
    }


    /**
     * 查询打印所对应的功能函数
     * @return
     * @throws Exception
     */
    private boolean submitQueryPrint() throws Exception
    {
        mResult.clear();

        VData vData = parseParamsPrint(m_LZCardSet);

        String strSQL = (String) vData.getObjectByObjectName("String", 0);

        LZCardDB tLZCardDB = new LZCardDB();
        LZCardSet tLZCardSet = tLZCardDB.executeQuery(strSQL);

        if (tLZCardDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLZCardDB.mErrors);
            return false;
        }

        boolean bFlag = false; // 表示是从机构到业务员的单证发放操作

        // 外部的参数，指示是否需要使用不同的模板。
        if (getParams("NoUseAgentTemplate").equals(""))
        {
            if (tLZCardSet.size() > 0)
            {
                LZCardSchema tLZCardSchema = tLZCardSet.get(1);

                if (tLZCardSchema.getReceiveCom().charAt(0) == 'D'
                    && tLZCardSchema.getOperateFlag().equals("0"))
                {
                    bFlag = true;
                }
            }
        }

        XmlExport xe = new XmlExport();

        if (bFlag)
        {
            printAgent(xe, tLZCardSet);
        }
        else
        {
            printOther(xe, tLZCardSet);
        }

        mResult.add(xe);
        return true;
    }


    // 一些辅助函数，用来将编码转换成名字
    private String getCertifyName(String strCertifyCode) throws Exception
    {
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();

        tLMCertifyDesDB.setCertifyCode(strCertifyCode);

        if (!tLMCertifyDesDB.getInfo())
        {
            mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
            throw new Exception("在取得LMCertifyDes的数据时发生错误");
        }

        return tLMCertifyDesDB.getCertifyName();
    }

    private String getComName(String strComCode) throws Exception
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            throw new Exception("在取得LDCode的数据时发生错误");
        }
        return tLDCodeDB.getCodeName();
    }

    private String getAgentName(String strAgentCode) throws Exception
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(strAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            throw new Exception("在取得LAAgent的数据时发生错误");
        }
        return tLAAgentDB.getName();
    }

    private String transComCode(String strComCode) throws Exception
    {
        if (strComCode.trim().equals(""))
        {
            return strComCode;
        }

        if (strComCode.charAt(0) == 'A')
        {
            return getComName(strComCode.substring(1));
        }
        else if (strComCode.charAt(0) == 'B')
        {
            throw new Exception("目前还不支持");
        }
        else if (strComCode.charAt(0) == 'D')
        {
            return getAgentName(strComCode.substring(1));
        }
        else
        {
            return strComCode;
        }
    }


    /**
     * Kevin 2003-05-08
     * 针对于发放单证到业务员的情况的打印逻辑
     * @param xe
     * @param aLZCardSet
     * @throws Exception
     */
    private void printAgent(XmlExport xe, LZCardSet aLZCardSet) throws
            Exception
    {
        xe.createDocument("CertifyList.vts", "");

        int nLen = 7; // 列表数据的列数
        String[] values = new String[nLen];

        ListTable lt = new ListTable();
        lt.setName("INFO");

        if (aLZCardSet.size() >= 1)
        {
            LZCardSchema yLZCardSchema = new LZCardSchema();
            yLZCardSchema.setSchema(aLZCardSet.get(1));
            int tCountFee = 0;
            String tDate = yLZCardSchema.getMakeDate(); // 领取时间
            String tNameReceiveCom = transComCode(yLZCardSchema.getReceiveCom());
            String tCodeReceiveCom = "";

            if (yLZCardSchema.getReceiveCom().length() >= 1)
            {
                tCodeReceiveCom = yLZCardSchema.getReceiveCom().substring(1); // 获取工号
            }

            TextTag texttag = new TextTag();

            texttag.add("NameReceiveCom", tNameReceiveCom);
            texttag.add("CodeReceiveCom", tCodeReceiveCom);
            texttag.add("MakeDate", tDate);
            texttag.add("CountFee", tCountFee);
            xe.addTextTag(texttag);
            if (aLZCardSet.size() <= 13)
            {
                xe.addDisplayControl("displaycompart");
                for (int nIndex = 0; nIndex < aLZCardSet.size(); nIndex++)
                {
                    LZCardSchema tLZCardSchema = aLZCardSet.get(nIndex + 1);
                    values = new String[nLen];
                    values[0] = tLZCardSchema.getCertifyCode();
                    values[1] = getCertifyName(tLZCardSchema.getCertifyCode());
                    values[2] = tLZCardSchema.getStartNo();
                    values[3] = tLZCardSchema.getEndNo();
                    values[4] = String.valueOf(tLZCardSchema.getSumCount());
                    values[5] = "";
                    values[6] = "";
                    lt.add(values);
                }
                for (int nIndex = aLZCardSet.size(); nIndex < 13; nIndex++)
                {
                    values = new String[nLen];
                    values[0] = "";
                    values[1] = "";
                    values[2] = "";
                    values[3] = "";
                    values[4] = "";
                    values[5] = "";
                    values[6] = "";
                    lt.add(values);
                }
            }
            else
            {
                xe.addDisplayControl("displaydouble");
                for (int nIndex = 0; nIndex < aLZCardSet.size(); nIndex++)
                {
                    LZCardSchema tLZCardSchema = aLZCardSet.get(nIndex + 1);
                    values = new String[nLen];
                    values[0] = tLZCardSchema.getCertifyCode();
                    values[1] = getCertifyName(tLZCardSchema.getCertifyCode());
                    values[2] = tLZCardSchema.getStartNo();
                    values[3] = tLZCardSchema.getEndNo();
                    values[4] = String.valueOf(tLZCardSchema.getSumCount());
                    values[5] = "";
                    values[6] = "";
                    lt.add(values);
                }
            }
            values = new String[nLen];
            values[0] = "CertifyCode";
            values[1] = "CertifyName";
            values[2] = "StartNo";
            values[3] = "EndNo";
            values[4] = "SumCount";
            values[5] = "Fee"; // 单价
            values[6] = "FeeCount"; // 单证费用
            xe.addListTable(lt, values);
        }
    }


    /**
     * Kevin 2003-05-08
     * 针对于其它情况的单证清单打印
     * @param xe
     * @param aLZCardSet
     * @throws Exception
     */
    private void printOther(XmlExport xe, LZCardSet aLZCardSet) throws
            Exception
    {
        xe.createDocument("CertifyListEx.vts", "");

        int nLen = 6; // 列表数据的列数
        String[] values = new String[nLen];

        ListTable lt = new ListTable();
        lt.setName("Info");

        for (int nIndex = 0; nIndex < aLZCardSet.size(); nIndex++)
        {
            LZCardSchema tLZCardSchema = aLZCardSet.get(nIndex + 1);

            values = new String[nLen];

            values[0] = transComCode(tLZCardSchema.getSendOutCom());
            values[1] = transComCode(tLZCardSchema.getReceiveCom());
            values[2] = getCertifyName(tLZCardSchema.getCertifyCode());
            values[3] = tLZCardSchema.getStartNo();
            values[4] = tLZCardSchema.getEndNo();
            values[5] = String.valueOf(tLZCardSchema.getSumCount());

            lt.add(values);
        }

        values = new String[nLen];

        values[0] = "SendOutCom";
        values[1] = "ReceiveCom";
        values[2] = "CertifyCode";
        values[3] = "StartNo";
        values[4] = "EndNo";
        values[5] = "SumCount";

        xe.addListTable(lt, values);

        String CurrentDate = PubFun.getCurrentDate();

        TextTag texttag = new TextTag();
        texttag.add("ManageCom", getComName(m_GlobalInput.ManageCom));
        texttag.add("Operator", m_GlobalInput.Operator);
        texttag.add("time", CurrentDate);
        xe.addTextTag(texttag);
    }


    /**
     * Kevin 2003-06-18
     * 针对于单证统计报表打印
     * @param xe
     * @param aLZCardSet
     * @throws Exception
     */
    private void printReport(XmlExport xe, LZCardSet aLZCardSet) throws
            Exception
    {
        xe.createDocument("CertifyReport.vts", "");

        // get latest print price of each kind of certify
        Hashtable hashPrice = new Hashtable();

        String strSQL = "SELECT CertifyCode, CertifyPrice FROM LZCardPrint A"
                        +
                        " WHERE PrtNo = (SELECT MAX(PrtNo) FROM LZCardPrint B"
                        + "    WHERE B.CertifyCode = A.CertifyCode)"
                        + " ORDER BY CertifyCode";

        ExeSQL es = new ExeSQL();
        SSRS ssrs = es.execSQL(strSQL);

        if (ssrs.mErrors.needDealError())
        {
            mErrors.copyAllErrors(ssrs.mErrors);
            throw new Exception("取单证印刷费用失败！");
        }

        String strCertifyCode = "";

        for (int nIndex = 0; nIndex < ssrs.getMaxRow(); nIndex++)
        {
            strCertifyCode = ssrs.GetText(nIndex + 1, 1);
            hashPrice.put(strCertifyCode, ssrs.GetText(nIndex + 1, 2));
        }

        // put data to xmlexport
        int nLen = 7; // 列表数据的列数
        String[] values = new String[nLen];
        double dMoney = 0.0; // print cost
        double dSumMoney = 0.0; // sum print cost
        double dSumCount = 0.0; // sum count

        ListTable lt = new ListTable();
        lt.setName("Info");

        for (int nIndex = 0; nIndex < aLZCardSet.size(); nIndex++)
        {
            LZCardSchema tLZCardSchema = aLZCardSet.get(nIndex + 1);

            values = new String[nLen];

            strCertifyCode = tLZCardSchema.getCertifyCode();

            values[0] = strCertifyCode;
            values[1] = getCertifyName(tLZCardSchema.getCertifyCode());
            values[2] = transComCode(tLZCardSchema.getSendOutCom());
            values[3] = transComCode(tLZCardSchema.getReceiveCom());
            values[4] = String.valueOf(tLZCardSchema.getSumCount());
            values[5] = (String) hashPrice.get(strCertifyCode);

            dSumCount += tLZCardSchema.getSumCount();
            dMoney = tLZCardSchema.getSumCount() * Double.parseDouble(values[5]);
            dSumMoney += dMoney;

            values[6] = new DecimalFormat("0.00").format(dMoney);

            lt.add(values);
        }

        values = new String[nLen];

        values[0] = "CertifyCode";
        values[1] = "CertifyName";
        values[2] = "SendOutCom";
        values[3] = "ReceiveCom";
        values[4] = "SumCount";
        values[5] = "Price";
        values[6] = "PrintMoney";

        xe.addListTable(lt, values);

        TextTag tag = new TextTag();
        tag.add("SumMoney", new DecimalFormat(".00").format(dSumMoney));
        tag.add("SumCount", new DecimalFormat("0").format(dSumCount));
        xe.addTextTag(tag);
    }


    /**
     * 2003-05-09 Kevin
     * 利用临时表对第一次得到的结果进行筛选，从而得到实际的查询结果
     * @param strState 统计状态（库存、已发放、已回收、发放未回收、发放总量）
     * @param aQueryData
     * @return
     */
    private VData genQueryResult(String strState, VData aQueryData) throws
            Exception
    {
        VData tResult = new VData();
        String strTemp = "";
        String strSQL = (String) aQueryData.get(0); // 取得详细信息

        if (strState.equals(CertStatBL.STAT_STOCK)
            || strState.equals(CertStatBL.STAT_GROSS))
        {
            // 库存和入库总量：不需要使用临时表，直接使用解析得到的SQL语句

            // tResult.add( tLZCardSet );
            // 原来返回查询后的结果集，现在返回查询前的SQL语句。
            // 在processData函数中也将做同样的改动。
            tResult.add(strSQL);

        }
        else if (strState.equals(CertStatBL.STAT_SENDED)
                 || strState.equals(CertStatBL.STAT_TAKEBACKED)
                 || strState.equals(CertStatBL.STAT_S_T))
        {

            processData(strState, strSQL, tResult);
            // 得到查询语句
            strSQL = (String) tResult.get(0);
        }

        // 格式化要输出的列
        // 将原来SQL语句中的“SELECT *”改成所要的列。
        strTemp =
                "SELECT CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo, SumCount, Operator"
                + strSQL.substring(strSQL.indexOf('*') + 1);

        tResult.set(0, strTemp);

        // 取总数：将原来SQL语句中的“SELECT *”改成“SELECT NVL(SumCount), 0)”
        strTemp =
                "select case when SUM(SumCount) is null then 0 else sum(SumCount) end "
                + strSQL.substring(strSQL.indexOf('*') + 1);
        strTemp = strTemp.substring(0, strTemp.indexOf("ORDER"));

        ExeSQL es = new ExeSQL();
        SSRS ssrs = es.execSQL(strTemp);

        if (es.mErrors.needDealError())
        {
            throw new Exception(es.mErrors.getFirstError());
        }

        tResult.add(ssrs.GetText(1, 1));

        return tResult;
    }


    /**
     * 2003-05-09 Kevin
     * 利用临时表对第一次得到的结果进行筛选，从而得到实际的查询结果
     * @param strState
     * @param strSQL
     * @param aResult
     * @throws Exception
     */
    private void processData(String strState, String strSrcSQL, VData aResult) throws
            Exception
    {
        String strOper = m_GlobalInput.Operator;

        // 将数据存放到临时表中
        String strSQL =
                "INSERT INTO LZCardTemp SELECT lzct.*,'" + strOper +
                "' FROM LZCardTrack lzct "
                + strSrcSQL.substring(strSrcSQL.indexOf("WHERE"));

        System.out.println(strSQL);

        new ExeSQL().execUpdateSQL("DELETE FROM LZCardTemp WHERE QueryOper = '"
                                   + strOper + "'");
        new ExeSQL().execUpdateSQL(strSQL);

        String[] strFlagDel = null; // 要删除数据的标志
        String[] strFlagLeft = null; // 要保留数据的标志

        if (strState.equals(CertStatBL.STAT_SENDED))
        {
            strFlagDel = new String[1];
            strFlagDel[0] = "2";

            strFlagLeft = new String[1];
            strFlagLeft[0] = "0";

        }
        else if (strState.equals(CertStatBL.STAT_TAKEBACKED))
        {
            strFlagDel = new String[1];
            strFlagDel[0] = "3";

            strFlagLeft = new String[1];
            strFlagLeft[0] = "1";

        }
        else if (strState.equals(CertStatBL.STAT_S_T))
        {
            strFlagDel = new String[3];
            strFlagDel[0] = "2";
            strFlagDel[1] = "3";
            strFlagDel[2] = "1";

            strFlagLeft = new String[3];
            strFlagLeft[0] = "0";
            strFlagLeft[1] = "1";
            strFlagLeft[2] = "0";
        }

        Connection conn = DBConnPool.getConnection();
        Statement stmt = null;
        ResultSet rs = null;

        LZCardTempDB tLZCardTempDB = null;
        LZCardTempSet tLZCardTempSet = null;
        LZCardTempSchema tOldLZCardTempSchema = null;
        LZCardTempSchema tNewLZCardTempSchema = new LZCardTempSchema();
        int nNoLen = 0;

        if (conn == null)
        {
            throw new Exception("连接数据库失败！");
        }

        try
        {
            stmt = conn.createStatement();

            for (int nIndex = 0; nIndex < strFlagLeft.length; nIndex++)
            {
                strSQL = "SELECT CertifyCode,SubCode,RiskCode,RiskVersion,StartNo,EndNo,SendOutCom,ReceiveCom,MakeDate,MakeTime,QueryOper FROM LZCardTemp WHERE QueryOper = '"
                         + strOper + "' AND OperateFlag = '"
                         + strFlagDel[nIndex] +
                         "' ORDER BY ModifyDate DESC, ModifyTime DESC with ur";

                rs = stmt.executeQuery(strSQL);

                while (rs.next())
                {
                    strSQL = "SELECT * FROM LZCardTemp WHERE CertifyCode = '" +
                             rs.getString("CertifyCode") + "' and SubCode = '" +
                             rs.getString("SubCode") + "' and RiskCode = '" +
                             rs.getString("RiskCode") + "' and RiskVersion = '" +
                             rs.getString("RiskVersion") + "' and StartNo = '" +
                             rs.getString("StartNo") + "' and EndNo = '" +
                             rs.getString("EndNo") + "' and SendOutCom = '" +
                             rs.getString("SendOutCom") +
                             "' and ReceiveCom = '" +
                             rs.getString("ReceiveCom") + "' and MakeDate = '" +
                             rs.getString("MakeDate") + "' and MakeTime = '" +
                             rs.getString("MakeTime") + "' and QueryOper = '" +
                             rs.getString("QueryOper") + "' with ur";
                    System.out.println(strSQL);

                    tOldLZCardTempSchema = new LZCardTempDB().executeQuery(
                            strSQL).get(1);
                    strSQL = "SELECT * FROM LZCardTemp WHERE QueryOper = '" +
                             strOper +
                             "' AND OperateFlag = '" + strFlagLeft[nIndex] +
                             "' AND CertifyCode = '" +
                             tOldLZCardTempSchema.getCertifyCode() +
                             "' AND SendOutCom = '" +
                             tOldLZCardTempSchema.getReceiveCom() +
                             "' AND ReceiveCom = '" +
                             tOldLZCardTempSchema.getSendOutCom() +
                             "' AND StartNo <= '" +
                             tOldLZCardTempSchema.getStartNo() +
                             "' AND EndNo >= '" +
                             tOldLZCardTempSchema.getStartNo() +
                             "' AND StartNo <= '" +
                             tOldLZCardTempSchema.getEndNo() +
                             "' AND EndNo >= '" + tOldLZCardTempSchema.getEndNo() +
                             "' AND ( ModifyDate < '" +
                             tOldLZCardTempSchema.getModifyDate() +
                             "' OR ( ModifyDate = '" +
                             tOldLZCardTempSchema.getModifyDate() +
                             "' AND ModifyTime <= '" +
                             tOldLZCardTempSchema.getModifyTime() +
                             "')) ORDER BY ModifyDate DESC, ModifyTime DESC with ur";
                    System.out.println(strSQL);

                    // 删除需要删除的数据
                    tLZCardTempDB = new LZCardTempDB();
                    tLZCardTempDB.setSchema(tOldLZCardTempSchema);
                    tLZCardTempDB.delete();

                    // 查询出要拆分的数据
                    tLZCardTempSet = new LZCardTempDB().executeQuery(strSQL);

                    if (tLZCardTempSet.size() < 1)
                    {
                        continue;
                    }

                    nNoLen = CertifyFunc.getCertifyNoLen(tOldLZCardTempSchema.
                            getCertifyCode());

                    tNewLZCardTempSchema.setSchema(tLZCardTempSet.get(1));

                    // 删除旧的数据
                    tLZCardTempDB = new LZCardTempDB();
                    tLZCardTempDB.setSchema(tNewLZCardTempSchema);
                    tLZCardTempDB.delete();

                    // 拆分第一部分单证
                    tNewLZCardTempSchema.setEndNo(
                            CertifyFunc.bigIntegerPlus(
                            tOldLZCardTempSchema.getStartNo(), "-1", nNoLen));

                    tNewLZCardTempSchema.setSumCount(
                            (int) CertifyFunc.bigIntegerDiff(
                            tNewLZCardTempSchema.getEndNo(),
                            tNewLZCardTempSchema.getStartNo()) + 1);

                    if (tNewLZCardTempSchema.getSumCount() > 0)
                    {
                        tLZCardTempDB = new LZCardTempDB();
                        tLZCardTempDB.setSchema(tNewLZCardTempSchema);
                        tLZCardTempDB.insert();
                    }

                    // 拆分第二部分的单证
                    tNewLZCardTempSchema.setSchema(tLZCardTempSet.get(1));

                    tNewLZCardTempSchema.setStartNo(
                            CertifyFunc.bigIntegerPlus(
                            tOldLZCardTempSchema.getEndNo(), "1", nNoLen));

                    tNewLZCardTempSchema.setSumCount(
                            (int) CertifyFunc.bigIntegerDiff(
                            tNewLZCardTempSchema.getEndNo(),
                            tNewLZCardTempSchema.getStartNo()) + 1);

                    if (tNewLZCardTempSchema.getSumCount() > 0)
                    {
                        tLZCardTempDB = new LZCardTempDB();
                        tLZCardTempDB.setSchema(tNewLZCardTempSchema);
                        tLZCardTempDB.insert();
                    }

                }

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (stmt != null)
        {
            stmt.close();
            stmt = null;
        }

        if (rs != null)
        {
            rs.close();
            rs = null;
        }

        if (conn != null)
        {
            conn.close();
            conn = null;
        }

        strSQL = "SELECT * FROM LZCardTemp WHERE QueryOper = '" + strOper + "'";
        strSQL +=
                " ORDER BY CertifyCode, SendOutCom, ReceiveCom, StartNo, EndNo ";

        // aResult.add(new LZCardDB().executeQuery(strSQL));
        // 原来返回查询后的结果集，现在返回查询前的SQL语句。
        aResult.add(strSQL);
    }

    private String getParams(String strKey)
    {
        if (m_hashParams == null)
        {
            return "";
        }

        String str = (String) m_hashParams.get(strKey);

        if (str == null)
        {
            return "";
        }
        else
        {
            return str;
        }
    }


    // 下面是单证统计报表的相关功能模块

    /**
     * 解析传入的条件，生成查询需要的Where条件。
     * @param aLZCardSet
     * @return
     * @throws Exception
     */
    private VData parseParamsReport(LZCardSet aLZCardSet) throws Exception
    {
        LZCardSchema tLZCardSchema = null;

        String strSQL = "";
        String strWhere = "";
        String strGroupBy = "";

        // 设置分组条件
        tLZCardSchema = aLZCardSet.get(2);

        strSQL = "SELECT CertifyCode";
        strGroupBy = " GROUP BY CertifyCode";

        if (tLZCardSchema.getSendOutCom() == null
            || tLZCardSchema.getSendOutCom().equals("")
            || tLZCardSchema.getReceiveCom() == null
            || tLZCardSchema.getReceiveCom().equals(""))
        {
            throw new Exception("缺少必要的参数");
        }

        if (tLZCardSchema.getSendOutCom().equals("0"))
        {
            strSQL += ", ' '";
        }
        else if (tLZCardSchema.getSendOutCom().equals("1"))
        {
            strSQL += ", SendOutCom";
            strGroupBy += ", SendOutCom";
        }

        if (tLZCardSchema.getReceiveCom().equals("0"))
        {
            strSQL += ", ' '";
        }
        else if (tLZCardSchema.getReceiveCom().equals("1"))
        {
            strSQL += ", ReceiveCom";
            strGroupBy += ", ReceiveCom";
        }

        strSQL += ", SUM(SumCount) FROM";

        // 设置其它条件
        tLZCardSchema = aLZCardSet.get(1);

        String strState = tLZCardSchema.getState();

        String strSendOutCom = tLZCardSchema.getSendOutCom();
        String strReceiveCom = tLZCardSchema.getReceiveCom();

        strWhere += getWherePart("CertifyCode", tLZCardSchema.getCertifyCode());
        strWhere += getWherePart("Handler", tLZCardSchema.getHandler());
        //strWhere += getWherePart("Operator", tLZCardSchema.getOperator());
        strWhere +=
                getWherePart("StartNo", "EndNo", tLZCardSchema.getStartNo(), 0);
        strWhere +=
                getWherePart("StartNo", "EndNo", tLZCardSchema.getEndNo(), 0);
        strWhere += getWherePart("StateFlag", tLZCardSchema.getStateFlag());
        strWhere += getWherePart("HandleDate", aLZCardSet.get(1).getHandleDate(),
                                 aLZCardSet.get(2).getHandleDate(), 1);

        if (strState.equals(CertStatBL.STAT_STOCK))
        {
            // 库存：只查出处于发放状态的单证
            strSQL += " LZCard WHERE 1=1" + strWhere
                    + " AND StateFlag in  ('0','7')"
                    + getWherePart("ReceiveCom", strReceiveCom);

        }
        else if (strState.equals(CertStatBL.STAT_SENDED))
        {
            // 已发放（轨迹表）：处于发放状态的单证 - 处于反发放状态的单证
            strSQL += "(( SELECT CertifyCode, SendOutCom, ReceiveCom, SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND OperateFlag = '0'"
                    + getWherePart("SendOutCom", strSendOutCom)
                    + getWherePart("ReceiveCom", strReceiveCom)
                    + ") UNION ALL "
                    +
                    "( SELECT CertifyCode, ReceiveCom SendOutCom, SendOutCom ReceiveCom, -1*SumCount SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND OperateFlag = '2'"
                    + getWherePart("ReceiveCom", strSendOutCom)
                    + getWherePart("SendOutCom", strReceiveCom)
                    + ")) t";
            System.out.println("已发放轨迹查询" + strSQL);

        }
        else if (strState.equals(CertStatBL.STAT_TAKEBACKED))
        {
            // 已回收（轨迹表）：处于回收状态的单证 - 处于反回收状态的单证
            strSQL += "(( SELECT CertifyCode, SendOutCom, ReceiveCom, SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND OperateFlag = '1'"
                    + getWherePart("SendOutCom", strSendOutCom)
                    + getWherePart("ReceiveCom", strReceiveCom)
                    + ") UNION ALL "
                    +
                    "( SELECT CertifyCode, ReceiveCom SendOutCom, SendOutCom ReceiveCom, -1*SumCount SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND OperateFlag = '3'"
                    + getWherePart("ReceiveCom", strSendOutCom)
                    + getWherePart("SendOutCom", strReceiveCom)
                    + ")) t";
            System.out.println("已回收轨迹查询" + strSQL);
        }
        else if (strState.equals(CertStatBL.STAT_S_T))
        {
            // 发放未回收（轨迹表）：已发放 - 已回收
            //  ( 状态为'0' - 状态为'2' ) - ( 状态为'1' - 状态为'3' )
            // -> ( 状态为'0' + 状态为'3' ) - ( 状态为'1' + 状态为'2' )
            strSQL += "(( SELECT CertifyCode, SendOutCom, ReceiveCom, SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND (OperateFlag = '0' OR OperateFlag = '3')"
                    + getWherePart("SendOutCom", strSendOutCom)
                    + getWherePart("ReceiveCom", strReceiveCom)
                    + ") UNION ALL "
                    +
                    "( SELECT CertifyCode, ReceiveCom SendOutCom, SendOutCom ReceiveCom, -1*SumCount SumCount"
                    + " FROM LZCardTrack WHERE 1=1" + strWhere
                    + " AND (OperateFlag = '1' OR OperateFlag = '2')"
                    + getWherePart("ReceiveCom", strSendOutCom)
                    + getWherePart("SendOutCom", strReceiveCom)
                    + ")) t";
            System.out.println("发放未回收放轨迹查询" + strSQL);
        }
        else if (strState.equals(CertStatBL.STAT_GROSS))
        {
            // 入库总量：从轨迹表中查询出发放状态的单证
            strSQL += " LZCardTrack WHERE 1=1" + strWhere
                    + " AND OperateFlag = '0'"
                    + getWherePart("ReceiveCom", strReceiveCom);

        }

        strSQL += strGroupBy;
        strSQL += " ORDER BY CertifyCode";

        System.out.println("SQL");
        System.out.println(strSQL);

        VData tVData = new VData();

        tVData.add(0, strSQL);

        return tVData;
    }


    /**
     * 统计报表的查询功能
     * @return
     */
    private boolean submitReportQuery() throws Exception
    {
        VData tVData = parseParamsReport(m_LZCardSet); // 解析查询条件

        String strSQL = (String) tVData.getObject(0);

        ExeSQL es = new ExeSQL();
        SSRS rs = es.execSQL(strSQL);

        if (es.mErrors.needDealError())
        {
            mErrors.copyAllErrors(es.mErrors);
            return false;
        }

        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = 0; nIndex < rs.MaxRow; nIndex++)
        {
            LZCardSchema tLZCardSchema = new LZCardSchema();

            tLZCardSchema.setCertifyCode(rs.GetText(nIndex + 1, 1));
            tLZCardSchema.setSendOutCom(rs.GetText(nIndex + 1, 2));
            tLZCardSchema.setReceiveCom(rs.GetText(nIndex + 1, 3));
            tLZCardSchema.setSumCount(rs.GetText(nIndex + 1, 4));

            tLZCardSet.add(tLZCardSchema);
        }

        mResult.clear();
        mResult.add(strSQL);
        mResult.add(tLZCardSet);

        return true;
    }


    /**
     * 统计报表的打印功能
     * @return
     */
    private boolean submitReportPrint() throws Exception
    {
        mResult.clear();

        XmlExport xe = new XmlExport();
        printReport(xe, m_LZCardSet);
        mResult.add(xe);
        return true;
    }
}
