/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
/*
 * <p>ClassName: CardPrintBLS </p>
 * <p>Description: CardPrintBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-12
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZCardPrintDB;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.utility.*;

public class CardPrintBLS
{

    public CErrors mErrors = new CErrors();
    private String strOperate;

    public CardPrintBLS()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean bReturn = false;
        //将操作数据拷贝到本类中
        strOperate = verifyOperate(cOperate);

        if (strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        LZCardPrintDB tLZCardPrintDB = new LZCardPrintDB();

        tLZCardPrintDB.setSchema((LZCardPrintSchema) cInputData.
                                 getObjectByObjectName("LZCardPrintSchema", 0));

        if (strOperate.equals("INSERT"))
        {
            bReturn = tLZCardPrintDB.insert();
        }
        else
        {
            bReturn = tLZCardPrintDB.update();
        }

        mErrors.copyAllErrors(tLZCardPrintDB.mErrors);

        return bReturn;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPrintBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT", "UPDATE"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }
}
