package com.sinosoft.lis.certify;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description: 用于校验所有单证号码的CRC校验相关</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 1.0
 */

public class VerifyCertifyNo {

    /**
     * 使用说明：
     * 1、所有的单证号码校验都是用静态方法verify
     * 2、verify有两个参数，第一个是要校验的单证号码，第二个是对应单证号码的类型
     * 3、号码类型对应VerifyCertifyNo中的一个方法
     * 4、此方法用于校验当前类型的号码是否符合相关规则，此方法必须为boolean类型
     * 5、每添加一个校验号码类型都要添加一个公共静态的最终变量，标示号码类型，变量
     * 值为必须为校验方法名，外部调用时直接调用最终变量，不允许直接填写方法
     */
    public VerifyCertifyNo() {
    }

    /** 个人卡单业务卡单号码 */
    public static final String PERSONAL_CARD = "PersonalCard";


    /**
     * 校验对应类型的卡号是否有效
     * @param tCardNo String
     * @param tCardType String
     * @return boolean
     */
    public static boolean verify(String tCardNo, String tCardType) throws
            Exception {
        VerifyCertifyNo o = new VerifyCertifyNo();
        /** 首先校验传入校验类型是否存在 */
        Field[] f = o.getClass().getFields();
        boolean b = false;
        for (int i = 0; i < f.length; i++) {
            if (f[i].get(null).equals(tCardType)) {
                b = true;
                break;
            }
        }
        if (!b) {
            throw new Exception("不存在“" + tCardType + "”校验类型");
        }

        Method m = null;
        m = o.getClass().getMethod(tCardType, (new Class[] {java.lang.String.class}));
        return ((Boolean) m.invoke(o, (new String[] {tCardNo}))).booleanValue();
    }

    /**
     * 个人卡单校验
     * @param tCardNo String
     * @return boolean
     */
    public boolean PersonalCard(String tCardNo) {
        /**
         * 个单卡单号码规则，11位号码，9位流水号+2位校验码，
         * 交验规则为九位流水号拼接两倍18位CRC校验生成2位校验码
         */
        //*** 由于单证长度变为不定长，校验规则也相应改变  zhangjianbao  2007-10-29 ***
        /*if (tCardNo == null || tCardNo.length() != 11) {
            return false;
        }*/
        if(tCardNo == null) return false;
        String cardType = tCardNo.substring(0, 2);
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL("select operatetype from Lmcertifydes where subcode =  '" + cardType + "'");
        if(ssrs.GetText(1, 1).equals("0")) return true;
        //**********************************************************************

        String tCertifyNo = tCardNo.substring(0, 9);
        String tCRC = tCardNo.substring(9);
        tCertifyNo += tCertifyNo;
        return checkPersonalCardCRCError(tCertifyNo, tCRC);
    }

    /**
     * checkPersonalCardCRCError
     *
     * @param tCertifyNo String
     * @param tCRC String
     * @return boolean
     */
    private boolean checkPersonalCardCRCError(String tCertifyNo, String tCRC) {
        return getCRCNo(tCertifyNo).equals(tCRC);
    }

    /**
     * getCRCN
     *
     * @return Object
     */
    public String getCRCNo(String tCertifyNo) {
        /**
         * 原理:
         * ∑(a[i]*W[i]) mod 11 ( i = 2, 3, ..., 18 )(1)
         * "*" 表示乘号
         * i--------表示身份证号码每一位的序号，从右至左，最左侧为18，最右侧为1。
         * a[i]-----表示身份证号码第 i 位上的号码
         * W[i]-----表示第 i 位上的权值 W[i] = 2^(i-1) mod 11
         * 计算公式 (1) 令结果为 R
         * 根据下表找出 R 对应的校验码即为要求身份证号码的校验码C。
         * R 0 1 2 3 4 5 6 7 8 9 10
         * C 1 0 X 9 8 7 6 5 4 3 2
         *  X 就是 10，罗马数字中的 10 就是 X
         * 15位转18位中,计算校验位即最后一位
         */
        int R = 0;
        String C = "";
        String tempCertify = "";
        for (int i = 0; i < tCertifyNo.length() - 1; i++) {
            tempCertify = String.valueOf(tCertifyNo.charAt(i)) + tempCertify;
        }
        tCertifyNo = tempCertify;
        for (int i = 1; i <= tCertifyNo.length(); i++) {
            int a = 0;
            try
            {
            	a = Integer.parseInt(String.valueOf(tCertifyNo.charAt(i - 1)));
            }
            catch(NumberFormatException e)
            {
            	a = 0;
            }
            int w = ((int) (Math.pow(2, ((i + 1) - 1)))) % 11;
            System.out.println("w " + w);
            R += a * w;
        }
        R = R % 11;
        switch (R) {
        case 0:
            C = "20"; //20
            break;
        case 1:
            C = "51"; //51
            break;
        case 2:
            C = "42"; //42
            break;
        case 3:
            C = "83"; //83
            break;
        case 4:
            C = "34"; //34
            break;
        case 5:
            C = "65"; //65
            break;
        case 6:
            C = "76"; //76
            break;
        case 7:
            C = "97"; //97
            break;
        case 8:
            C = "08"; //08
            break;
        case 9:
            C = "59"; //59
            break;
        case 10:
            C = "10"; //10
            break;
        }
        return C;
    }

    public static void main(String[] args) {
        try {
            System.out.println(VerifyCertifyNo.verify("21000000083",
                    PERSONAL_CARD));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
