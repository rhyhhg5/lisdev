/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
/*
 * <p>ClassName: CardPrintBL </p>
 * <p>Description: CardPrintBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-12
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LZCardPrintDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LMCertifyDesSchema;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardPrintSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMCardRiskDB;
import com.sinosoft.lis.vschema.LMCardRiskSet;

public class CardPrintBL
{

    public CErrors mErrors = new CErrors();//错误容器
    private VData mResult = new VData();//返回结果


    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String strOperate;


    /** 业务处理相关变量 */
    private LZCardPrintSchema mLZCardPrintSchema = new LZCardPrintSchema();//单证印刷信息
    private LZCardPrintSet mLZCardPrintSet = new LZCardPrintSet();//存放一条单证印刷信息
    private String m_strWhere = "";//SQL连接串
    private Hashtable m_hashParams = null;

    public CardPrintBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        strOperate = verifyOperate(cOperate);
        if (strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
            return false;

        if (!dealData())
            return false;

        if (!prepareOutputData())
            return false;

        if (strOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }

        if (strOperate.equals("INSERT||REQUEST") ||
            strOperate.equals("INSERT||CONFIRM"))
        {

            String str = "";

            if (strOperate.equals("INSERT||REQUEST"))
            {
                str = "INSERT";
            }
            else if (strOperate.equals("INSERT||CONFIRM"))
            {
                str = "UPDATE";
            }

            CardPrintBLS tCardPrintBLS = new CardPrintBLS();

            if (!tCardPrintBLS.submitData(mInputData, str))
            {
                if (tCardPrintBLS.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tCardPrintBLS.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "CardPrintBLS发生错误，但是没有详细信息。");
                    return false;
                }
            }
        }

        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (strOperate.equals("INSERT||REQUEST"))
        {
            return saveCardPrint(mLZCardPrintSchema, mGlobalInput);
        }

        if (strOperate.equals("INSERT||CONFIRM"))
        {
            return confirmCardPrint(mLZCardPrintSchema, mGlobalInput);
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mLZCardPrintSchema.setSchema((LZCardPrintSchema) cInputData.
                                              getObjectByObjectName(
                    "LZCardPrintSchema", 0));
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            m_strWhere = (String) cInputData.getObjectByObjectName("String", 0);
            m_hashParams = (Hashtable) cInputData.getObjectByObjectName(
                    "Hashtable", 0);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getInputData", "发生异常");
            return false;
        }
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        mResult.clear();
        System.out.println("Start CardPrintBL Submit...");

        LZCardPrintDB dbLZCardPrint = new LZCardPrintDB();

        SQLString sqlObj = new SQLString("LZCardPrint");

        sqlObj.setSQL(5, mLZCardPrintSchema);

        String strSql = sqlObj.getSQL();
        String strSqlAppend = " ManageCom = '" + mGlobalInput.ComCode + "' ";

        if (m_strWhere != null && !m_strWhere.equals(""))
        {
            strSqlAppend += " AND " + m_strWhere;
        }

        if (strSql.toUpperCase().indexOf("WHERE") != -1)
        {
            strSql += " AND " + strSqlAppend;
        }
        else
        {
            strSql += " WHERE " + strSqlAppend;
        }

        System.out.println(strSql);

        //mResult.add(dbLZCardPrint.executeQuery(strSql));

        if (dbLZCardPrint.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(dbLZCardPrint.mErrors);
            buildError("submitData", "数据提交失败");
            return false;
        }

        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData = new VData();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLZCardPrintSchema);
            mResult=mInputData;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareData", ex.getMessage());
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CardPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                {"INSERT||REQUEST", "INSERT||CONFIRM",
                "QUERY||MAIN"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }


    /**
     * 定单操作函数
     */
    private boolean saveCardPrint(LZCardPrintSchema schemaLZCardPrint,
                                  GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            if (!schemaLZCardPrint.getOperatorInput().equals(globalInput.
                    Operator))
            {
                buildError("saveCardPrint", "定单操作员不是当前登录的操作员");
                return false;
            }

            String strSql;

System.out.println("aaaaaaaa: "+schemaLZCardPrint.getCertifyCode());



            strSql = "SELECT * FROM LMCertifyDes";
            strSql += " WHERE CertifyCode = '" +
                    schemaLZCardPrint.getCertifyCode() + "'";
            //strSql += " AND ManageCom = '" + globalInput.ComCode + "'";

            System.out.println(strSql);

            LMCertifyDesSet setLMCertifyDes = new LMCertifyDesDB().executeQuery(
                    strSql);
            LMCertifyDesSchema schemaLMCertifyDes = null;

            if (setLMCertifyDes.size() != 1)
            {
                buildError("saveCardPrint", "无效的单证编码");
                return false;
            }
            else if (!setLMCertifyDes.get(1).getManageCom().equals("A"))
            {
                if(!setLMCertifyDes.get(1).getManageCom().equals(globalInput.ComCode))
                {
                    buildError("saveCardPrint", "当前操作员不能操作此种单证");
                    return false;
                }
                schemaLMCertifyDes = setLMCertifyDes.get(1);
            }
            else
            {
                schemaLMCertifyDes = setLMCertifyDes.get(1);
            }

            if ("D".equals(schemaLMCertifyDes.getCertifyClass())) {
                LMCardRiskDB tLMCardRiskDB = new LMCardRiskDB();
                tLMCardRiskDB.setCertifyCode(schemaLMCertifyDes.
                                             getCertifyCode());
                LMCardRiskSet tLMCardRiskSet = new LMCardRiskSet();
                tLMCardRiskSet = tLMCardRiskDB.query();
                if (tLMCardRiskSet.size() == 0) {
                    buildError("saveCardPrint", "单证未绑定险种！");
                    return false;
                }
            }

            if (!checkNo(schemaLZCardPrint))
            {
                return false;
            }

            String strStartNo = schemaLZCardPrint.getStartNo();
            String strEndNo = schemaLZCardPrint.getEndNo();
//
//     strSql = "SELECT * FROM LZCardPrint";
//     strSql += " WHERE (( StartNo <= '" + strStartNo;
//     strSql += "' AND EndNo >= '" + strStartNo + "' ) ";
//     strSql += " OR ( StartNo <= '" + strEndNo;
//     strSql += "' AND EndNo >= '" + strEndNo + "' )) ";
//     strSql += " AND CertifyCode = '" + schemaLZCardPrint.getCertifyCode() + "'";
//
//     System.out.println(strSql);
//
//     if(new LZCardPrintDB().executeQuery(strSql).size() > 0 ) {
//       buildError("saveCardPrint", "输入的单证与已印刷的单证重复了");
//       return false;
//     }

            LZCardPrintSchema tLZCardPrintSchema = new LZCardPrintSchema();

            tLZCardPrintSchema.setSchema(schemaLZCardPrint);

            tLZCardPrintSchema.setPrtNo(PubFun1.CreateMaxNo("PrtNo",
                    PubFun.getNoLimit(globalInput.ComCode.substring(1))));

            //tLZCardPrintSchema.setSubCode(schemaLMCertifyDes.getSubCode());
            tLZCardPrintSchema.setRiskCode(schemaLMCertifyDes.getRiskCode());
            tLZCardPrintSchema.setRiskVersion(schemaLMCertifyDes.getRiskVersion());

            tLZCardPrintSchema.setManageCom(schemaLMCertifyDes.getManageCom());

            tLZCardPrintSchema.setSumCount((int) CertifyFunc.bigIntegerDiff(
                    strEndNo, strStartNo) + 1);
            tLZCardPrintSchema.setOperatorInput(globalInput.Operator);
            tLZCardPrintSchema.setOperatorGet("");
            tLZCardPrintSchema.setGetMan("");
            tLZCardPrintSchema.setGetDate("");
            tLZCardPrintSchema.setGetMakeDate("");
            tLZCardPrintSchema.setInputMakeDate(PubFun.getCurrentDate());
            tLZCardPrintSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardPrintSchema.setModifyTime(PubFun.getCurrentTime());

            tLZCardPrintSchema.setState("0");

            mLZCardPrintSchema.setSchema(tLZCardPrintSchema);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("saveCardPrint", ex.getMessage());
            bReturn = false;
        }
        return bReturn;
    }


    /**
     * 提单操作函数
     */
    private boolean confirmCardPrint(LZCardPrintSchema schemaLZCardPrint,
                                     GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            if (!schemaLZCardPrint.getOperatorGet().equals(globalInput.Operator))
            {
                buildError("confirmCardPrint", "提单操作员不是当前登录的操作员");
                return false;
            }

            String strSql;

            LZCardPrintDB tLZCardPrintDB = new LZCardPrintDB();
            tLZCardPrintDB.setPrtNo(schemaLZCardPrint.getPrtNo());
            if (!tLZCardPrintDB.getInfo())
            {
                buildError("confirmCardPrint",
                           "找不到印刷号为" + schemaLZCardPrint.getPrtNo() + "的单证");
                return false;
            }

            /*
            if (!tLZCardPrintDB.getManageCom().equals(globalInput.ComCode))
            {
                buildError("confirmCardPrint",
                           "操作员当前登录机构与要操作单证的管理机构不符");
                return false;
            }
            */
            if (!tLZCardPrintDB.getState().equals("0"))
            {
                buildError("confirmCardPrint",
                           "印刷号为" + schemaLZCardPrint.getPrtNo() + "的单证不能提单");
                return false;
            }

            tLZCardPrintDB.setOperatorGet(globalInput.Operator);
            tLZCardPrintDB.setGetMakeDate(PubFun.getCurrentDate());
            tLZCardPrintDB.setModifyDate(PubFun.getCurrentDate());
            tLZCardPrintDB.setModifyTime(PubFun.getCurrentTime());

            tLZCardPrintDB.setGetDate(schemaLZCardPrint.getGetDate());
            tLZCardPrintDB.setGetMan(schemaLZCardPrint.getGetMan());

            tLZCardPrintDB.setState("1");

            mLZCardPrintSchema = tLZCardPrintDB.getSchema();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("confirmCardPrint", ex.getMessage());
            bReturn = false;
        }
        return bReturn;
    }


    /**
     * Kevin 2003-04-24
     * 校验单证号码
     * 根据单证描述表中的信息。
     * 如果是无号单证，则根据输入的数量，自动产生起始号和终止号。
     * 如果是有号单证，校验单证号码的正确性。
     * @param aLZCardSchema
     * @return
     */
    private boolean checkNo(LZCardPrintSchema aLZCardPrintSchema)
    {

        try
        {
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            tLMCertifyDesDB.setCertifyCode(aLZCardPrintSchema.getCertifyCode());
            if (!tLMCertifyDesDB.getInfo())
            {
                mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);
                return false;
            }

            int nLen = (int) tLMCertifyDesDB.getCertifyLength();

            if (tLMCertifyDesDB.getHaveNumber().equals("N"))
            { // 如果是无号单证
                if (aLZCardPrintSchema.getSumCount() < 1)
                {
                    buildError("autoGenNo", "对于无号单证，请输入正确的单证数量");
                    return false;
                }

                // 自动产生起始号和终止号
                String strSQL = "select case when to_number(MAX(EndNo))+1 is null then 1 else to_number(MAX(EndNo)) + 1 end from LZCardPrint WHERE CertifyCode = '" +
                                aLZCardPrintSchema.getCertifyCode() + "'";

                String strStartNo = new ExeSQL().getOneValue(strSQL);
                strStartNo = CertifyFunc.bigIntegerPlus(strStartNo, "0", nLen);

                String strEndNo = CertifyFunc.bigIntegerPlus(strStartNo,
                        String.valueOf(aLZCardPrintSchema.getSumCount() - 1),
                        nLen);

                aLZCardPrintSchema.setStartNo(strStartNo);
                aLZCardPrintSchema.setEndNo(strEndNo);

            }
            else
            { // 如果是有号单证
                if (aLZCardPrintSchema.getStartNo().length() != nLen
                    || aLZCardPrintSchema.getEndNo().length() != nLen)
                {

                    buildError("checkNo", "单证号的长度应该为" + String.valueOf(nLen));
                    return false;
                }

                if (CertifyFunc.bigIntegerDiff(aLZCardPrintSchema.getStartNo(),
                                               aLZCardPrintSchema.getEndNo()) >
                    0)
                {

                    buildError("checkNo", "起始单证号大于终止单证号，保存失败！");
                    return false;
                }

//                String strSql = "SELECT * FROM LZCardPrint" +
//                                " WHERE (( StartNo <= '" +
//                                aLZCardPrintSchema.getStartNo() +
//                                "' AND EndNo >= '" +
//                                aLZCardPrintSchema.getStartNo() + "' ) " +
//                                " OR ( StartNo <= '" +
//                                aLZCardPrintSchema.getEndNo() +
//                                "' AND EndNo >= '" +
//                                aLZCardPrintSchema.getEndNo() + "' )) " +
//                                " AND CertifyCode = '" +
//                                aLZCardPrintSchema.getCertifyCode() + "'";
                
                String strSql = "select * from lzcardprint "
                	   + "where ((startno<='"+aLZCardPrintSchema.getStartNo()+"' "
                	   + "and endno>='"+aLZCardPrintSchema.getStartNo()+"') "
                	   + "or (startno<='"+aLZCardPrintSchema.getEndNo()+"' "
                	   + "and endno>='"+aLZCardPrintSchema.getEndNo()+"') "
                	   + "or (startno>='"+aLZCardPrintSchema.getStartNo()+"' "
                	   + "and endno<='"+aLZCardPrintSchema.getEndNo()+"')) "
                	   + "and certifycode='"+aLZCardPrintSchema.getCertifyCode()+"'";

                System.out.println(strSql);

                if (new LZCardPrintDB().executeQuery(strSql).size() > 0)
                {
                    buildError("saveCardPrint", "输入的单证与已印刷的单证重复了");
                    return false;
                }
            }
        }
        catch (NumberFormatException nfe)
        {
            nfe.printStackTrace();
            buildError("checkNo", "不是数值型数据");
            return false;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("checkNo", ex.getMessage());
            return false;
        }

        return true;
    }
}
