/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;


/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public interface CertifyService
{
    public boolean submitData(VData cInputData, String cOperate);

    public VData getResult();

    public CErrors getErrors();
}
