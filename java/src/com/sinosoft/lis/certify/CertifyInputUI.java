/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理入库操作（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */
public class CertifyInputUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /* CertifyCode */
    private String mszCertifyCode = "";

    public CertifyInputUI()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.Operator = "Admin";
        tGlobalInput.ComCode = "asd";
        tGlobalInput.ManageCom = "sdd";

        // 准备传输数据 VData
        VData tVData = new VData();

        tVData.addElement("00000");
        tVData.addElement(tGlobalInput);

        CertifyInputUI tCertifyInputUI = new CertifyInputUI();

        if (!tCertifyInputUI.submitData(tVData, "INSERT"))
        {
            System.out.println(tCertifyInputUI.mErrors.getFirstError());
        }
    }


    /*
     * public function used to send data
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        CertifyInputBL tCertifyInputBL = new CertifyInputBL();
        System.out.println("Start Proposal UI Submit...");

        tCertifyInputBL.submitData(mInputData, mOperate);

        System.out.println("End Proposal UI Submit...");

        //如果有需要处理的错误，则返回
        if (tCertifyInputBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tCertifyInputBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CertifyInputUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();

            mInputData.addElement(mGlobalInput);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyInputUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GI", 0));

        return true;
    }
}
