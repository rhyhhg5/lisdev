/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zhouping
 * @version 1.0
 */

public class CertifyInputBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private String mszCertifyNo = "";

    public CertifyInputBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("OperateData:" + cOperate);
        //进行业务处理
        if (!dealData())
            return false;
        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        System.out.println("Start Proposal BL Submit...");

        // CertifyInputBLS tCertifyInputBLS=new CertifyInputBLS();
        // tCertifyInputBLS.submitData(mInputData,cOperate);

        System.out.println("End Proposal BL Submit...");

        //如果有需要处理的错误，则返回
        /*
             if (tCertifyInputBLS.mErrors .needDealError()) {
          this.mErrors .copyAllErrors(tCertifyInputBLS.mErrors ) ;
             } */

        mInputData = null;
        return true;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        boolean tReturn = false;

        System.out.println("In Deal Data first");
        tReturn = getInputData();
        System.out.println("After getinputdata");

        //
        System.out.println("Validity of certify number should be test here ...");

        return tReturn;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
        mszCertifyNo = (String) mInputData.getObjectByObjectName("String", 0);
        System.out.println(mszCertifyNo);

        if (mszCertifyNo == null || mszCertifyNo.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyInputBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在单证入库操作中没有得到有效的单证号!";
            System.out.println(tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData()
    {
        try
        {
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyInputBL";
            tError.functionName = "checkData";
            tError.errorMessage = "在校验输入的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(mszCertifyNo);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyInputBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
