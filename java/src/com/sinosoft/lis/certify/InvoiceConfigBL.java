package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BPOMissionStateSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class InvoiceConfigBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 数据操作字符串 */
	private String operate;

	/** 保单印刷号 */
	private String manageCom;

	/** 外包批次号 */
	private String certifyCode;

	/** 外包批次号 */
	private String certifyName;

	/**
	 * submitData
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		operate = cOperate;
        mInputData = (VData) cInputData.clone();
        
		if (!getInputData()) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			
			TransferData mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
			manageCom = (String) mTransferData.getValueByName("ManageCom");
			certifyCode = (String) mTransferData.getValueByName("CertifyCode");
			certifyName = (String) mTransferData.getValueByName("CertifyName");
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		if (operate.equals(SysConst.INSERT)) {
			map.put("insert into LDCode values ('invoicetype','" + manageCom + "','" + certifyCode 
					+ "','" + certifyName + "',null,null)", SysConst.INSERT);
			return true;
		}
		else if (operate.equals(SysConst.UPDATE)) {
			map.put("update LDCode set CodeName = '" + certifyCode + "', CodeAlias = '" + certifyName 
					+ "' where CodeType = 'invoicetype' and Code = '" + manageCom + "'", SysConst.UPDATE);
			return true;
		} 
		else if (operate.equals(SysConst.DELETE)) {
			map.put("delete from LDCode where CodeType = 'invoicetype' and Code = '" + manageCom + "'", SysConst.DELETE);
			return true;
		}
		else
		{
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "dealData";
			tError.errorMessage = "不支持的操作类型！";
			this.mErrors.addOneError(tError);
			return false;
		}
	}
	
	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

}
