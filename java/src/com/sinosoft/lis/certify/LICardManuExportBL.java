package com.sinosoft.lis.certify;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.certifybusiness.DentisServiceClient;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeLogSchema;
import com.sinosoft.lis.schema.LICardUploadLogSchema;
import com.sinosoft.lis.vschema.LAChargeBSet;
import com.sinosoft.lis.vschema.LAChargeLogSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LICardUploadLogSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LICardManuExportBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCalType = "99";
    //获得存储号码
    private String mNewEdorNo = "";
    private String mStartDate = "";
    private String mEndDate = "";

    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String ManageCom;
    private String mBranchType;
    private String mBranchType2;
    private boolean mReturnState = false;
    private Element  root=new  Element("PACKET");
	private Document doc = new  Document(root);
	private Document doc1 = null;
	private SSRS tSSRS=null;
	private String mTranscationNum=null;
	private String mFilePath=null;
	private LICardUploadLogSet mLICardUploadLogSet=new LICardUploadLogSet();
    /** 业务处理相关变量 */
    private LAChargeLogSchema mLAChargeLogSchema = new LAChargeLogSchema();
    private LAChargeLogSet mLastLAChargeLogSet = new LAChargeLogSet();
    private LAChargeBSet mLAChargeBSet = new LAChargeBSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();

    private MMap mMap = new MMap();

    public LICardManuExportBL() {
    }

    private boolean getInputData(VData cInputData) {
        this.mStartDate = (String) cInputData.get(1);
        this.mEndDate = (String) cInputData.get(2);
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    private boolean check() {

        return true;
    }

    private boolean prepareOutputData() {

    	this.mInputData = new VData();
        this.mMap.put(this.mLICardUploadLogSet,"INSERT");
        mInputData.add(mMap);
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            if (mErrors.needDealError()) {
                System.out.println("程序异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LICardManuExportBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "INSERT||MAIN")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LICardManuExportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean dealData() {
    	if(!getInitData()){
        	System.out.println("牙科激活卡初始化数据时失败");
        	CError tError = new CError();
            tError.moduleName = "LICardManuExportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "牙科激活卡初始化数据时失败!";
            this.mErrors.addOneError(tError);
        	return false;
        }
        if(!createJDomData()){
        	System.out.println("牙科激活卡取核心数据时失败");
        	return false ;
        }
        if(!save(doc)){
        	return false;
        }
        if(!uploadJDomData()){
        	System.out.println("牙科激活卡上传数据时失败");
        	return false;
        }
        if(!createLogData()){
        	System.out.println("牙科激活卡上传数据准备日志信息1时失败");
        	return false ;
        }
    	return true;
    }
    public boolean save(Document pXmlDoc) {
    	String pName=this.mTranscationNum+".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
			
		}
		return true;
	}
    private boolean createJDomData(){
    	try{
    		root.addAttribute("type", "REQUEST");
    		root.addAttribute("version", "1.0");
    		Element head=new  Element("HEAD");
    		Element REQUEST_TYPE=new  Element("REQUEST_TYPE");
    		Element TRANSACTION_NUM=new  Element("TRANSACTION_NUM");
    		REQUEST_TYPE.addContent("PICC");
    		TRANSACTION_NUM.addContent(this.mTranscationNum);
    		head.addContent(REQUEST_TYPE);
    		head.addContent(TRANSACTION_NUM);
    		root.addContent(head);
    		Element body=new  Element("BODY");
    		Element INSURED_LIST=new  Element("INSURED_LIST");
    		root.addContent(body);
    		body.addContent(INSURED_LIST);
    		Element INSURED_DATA=null;
    		Element CARDNO=null;
    		Element RISKCODE=null;
    		Element PAYCOUNT=null;
    		Element CVALIDATE=null;
    		Element ENDDATE=null;
    		Element PERSON_NAME=null;
    		Element SEX=null;
    		Element ID_TYPE=null;
    		Element ID_NUMBER=null;
    		Element BIRTH_DATE=null;
    		SSRS tempSSRS=new SSRS();
    		tSSRS=new SSRS();
        	ExeSQL tExeSQL=new ExeSQL();
        	String tSQL	="select a.cardno,c.riskcode,"
        		+"(select comcode from ldcode where codetype='TeethRiskCode' and code=c.riskcode),b.cvalidate," 
        		+"b.cvalidate,"
        		+"a.name,a.sex,a.idtype,a.idno,a.birthday "
        		+" from licertifyinsured a,licertify b,lmcardrisk c "
        		+" where a.cardno=b.cardno and b.certifycode=c.certifycode and "
        		+" c.riskcode in (select code from ldcode where codetype='TeethRiskCode') "
        		+" and a.modifydate between '"+this.mStartDate+"' and '"+this.mEndDate+"' "
        		+"and b.grpcontno is not null and b.grpcontno<>'' ";
        	tSSRS=tExeSQL.execSQL(tSQL);
        	if(tSSRS!=null&&tSSRS.MaxRow>0){
        		for(int i=1;i<=tSSRS.MaxRow;i++){
        			INSURED_DATA=new  Element("INSURED_DATA");
        			CARDNO=new  Element("CARDNO");
        			CARDNO.addContent(tSSRS.GetText(i,1));
        			System.out.println("CARDNO"+tSSRS.GetText(i,1));
        			INSURED_DATA.addContent(CARDNO);
        			RISKCODE=new  Element("RISKCODE");
        			tSQL="select distinct riskcode,calfactorvalue,calfactor from ldriskdutywrap where riskwrapcode='"
        				+tSSRS.GetText(i,2)+"' and calfactor in ('InsuYear','InsuYearFlag') order by calfactor asc";
        			tempSSRS=tExeSQL.execSQL(tSQL);
        			RISKCODE.addContent(tempSSRS.GetText(1,1));
        			System.out.println("RISKCODE"+tempSSRS.GetText(1,1));
        			INSURED_DATA.addContent(RISKCODE);
        			PAYCOUNT=new  Element("PAYCOUNT");
        			PAYCOUNT.addContent(tSSRS.GetText(i,3));
        			System.out.println("PAYCOUNT"+tSSRS.GetText(i,3));
        			INSURED_DATA.addContent(PAYCOUNT);
        			CVALIDATE=new  Element("CVALIDATE");
        			CVALIDATE.addContent(tSSRS.GetText(i,4));
        			System.out.println("CVALIDATE"+tSSRS.GetText(i,4));
        			INSURED_DATA.addContent(CVALIDATE);
        			ENDDATE=new  Element("ENDDATE");
        			ENDDATE.addContent(PubFun.calDate(tSSRS.GetText(i,5), Integer.parseInt(tempSSRS.GetText(1, 2)), tempSSRS.GetText(2, 2), null));
        			System.out.println("ENDDATE"+ENDDATE.getText());
        			INSURED_DATA.addContent(ENDDATE);
        			PERSON_NAME=new  Element("PERSON_NAME");
        			PERSON_NAME.addContent(tSSRS.GetText(i,6));
        			System.out.println("PERSON_NAME"+tSSRS.GetText(i,6));
        			INSURED_DATA.addContent(PERSON_NAME);
        			SEX=new  Element("SEX");
        			SEX.addContent(tSSRS.GetText(i,7));
        			System.out.println("SEX"+tSSRS.GetText(i,7));
        			INSURED_DATA.addContent(SEX);
        			ID_TYPE=new  Element("ID_TYPE");
        			ID_TYPE.addContent(tSSRS.GetText(i,8));
        			System.out.println("ID_TYPE"+tSSRS.GetText(i,8));
        			INSURED_DATA.addContent(ID_TYPE);
        			ID_NUMBER=new  Element("ID_NUMBER");
        			ID_NUMBER.addContent(tSSRS.GetText(i,9));
        			System.out.println("ID_NUMBER"+tSSRS.GetText(i,9));
        			INSURED_DATA.addContent(ID_NUMBER);
        			BIRTH_DATE=new  Element("BIRTH_DATE");
        			BIRTH_DATE.addContent(tSSRS.GetText(i,10));
        			System.out.println("BIRTH_DATE"+tSSRS.GetText(i,10));
        			INSURED_DATA.addContent(BIRTH_DATE);
        			INSURED_LIST.addContent(INSURED_DATA);
        		}
        	}else{
        		CError tError = new CError();
                tError.moduleName = "LICardManuExportBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有查询到需要上传的数据!";
                this.mErrors.addOneError(tError);
        		return false;
        	}
        	return true;
    	}catch(Exception ex){
    		CError tError = new CError();
            tError.moduleName = "LICardManuExportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "在生成上传数据jdom时发生异常!";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	
    }
    
    private boolean getInitData(){
    	ExeSQL tExeSQL=new ExeSQL();
    	String tResult=null;
    	String tSQL="select max(distinct transcationnum) from LICardUploadLog "
    		+" where substr(transcationnum,1,8)=date_format(current date,'yyyymmdd')";
    	tResult=tExeSQL.getOneValue(tSQL);
    	if(tResult!=null&&!tResult.equals("")){
    		String tempStr=tResult.substring(8,11);
    		int tempInt=Integer.parseInt(tempStr);
    		if(tempInt+1>99){
    			tResult=tResult.substring(0,8)+String.valueOf(tempInt+1);
    		}else if(tempInt+1>9){
    			tResult=tResult.substring(0,9)+String.valueOf(tempInt+1);
    		}else{
    			tResult=tResult.substring(0,10)+String.valueOf(tempInt+1);
    		}
    	}else{
    		tSQL="select date_format(current date,'yyyymmdd')||'001' from dual";
    		tResult=tExeSQL.getOneValue(tSQL);
    	}
    	mTranscationNum=tResult;
    	this.mFilePath=this.getClass().getResource("/").getPath();
    	this.mFilePath=this.mFilePath.replaceAll("WEB-INF/classes/", "vtsfile/cardupload/");
//    	this.mFilePath+=mTranscationNum+".xml";
    	System.out.println("...........this.mFilePath"+this.mFilePath);
    	return true;
    }
    
    private boolean uploadJDomData(){
    	DentisServiceClient tDentisServiceClient=new DentisServiceClient();
    	String tServerAddress="http://121.52.210.101/Service.asmx";
    	try{
    		doc1=tDentisServiceClient.callUploadXmlService(tServerAddress, doc);
    		JdomUtil.print(doc1);
    	}catch(Exception ex){
    		CError tError = new CError();
            tError.moduleName = "LICardManuExportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "在上传时发生异常!";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	return true;
    }
    
    private boolean createLogData()
    {
    	String tSQL="select max(int(idx)) from licarduploadlog ";
		int idx=0;
		ExeSQL tExeSQL=new ExeSQL();
		String tRs=tExeSQL.getOneValue(tSQL);
		SSRS tempSSRS=new SSRS();
		if(tRs!=null&&!tRs.equals("")){
			idx=Integer.parseInt(tRs);
		}
		LICardUploadLogSchema tLICardUploadLogSchema=null;
    	if(doc1!=null){
    		Element root1=doc1.getRootElement();
    		Element HEAD = root1.getChild("HEAD");
    		Element REQUEST_TYPE = HEAD.getChild("REQUEST_TYPE");
    		System.out.println("........REQUEST_TYPE"+REQUEST_TYPE.getTextTrim());
    		Element TRANSACTION_NUM = HEAD.getChild("TRANSACTION_NUM");
    		System.out.println("........TRANSACTION_NUM"+TRANSACTION_NUM.getTextTrim());
    		Element RESPONSE_CODE = HEAD.getChild("RESPONSE_CODE");
    		System.out.println("........RESPONSE_CODE"+RESPONSE_CODE.getTextTrim());
    		Element ERROR_MESSAGE = HEAD.getChild("ERROR_MESSAGE");
    		System.out.println("........ERROR_MESSAGE"+ERROR_MESSAGE.getTextTrim());
    		for(int i=1;i<=tSSRS.MaxRow;i++){
    			idx++;
    			tLICardUploadLogSchema=new LICardUploadLogSchema();
    			tLICardUploadLogSchema.setIdx(String.valueOf(idx));
    			tLICardUploadLogSchema.setRequestType("PICC");
    			tLICardUploadLogSchema.setTranscationNum(this.mTranscationNum);
    			tLICardUploadLogSchema.setStartDate(this.mStartDate);
    			tLICardUploadLogSchema.setEndDate(this.mEndDate);
    			tLICardUploadLogSchema.setCardNo(tSSRS.GetText(i,1));
    			tSQL="select distinct riskcode,calfactorvalue,calfactor from ldriskdutywrap where riskwrapcode='"
    				+tSSRS.GetText(i,2)+"' and calfactor in ('InsuYear','InsuYearFlag') order by calfactor asc";
    			tempSSRS=tExeSQL.execSQL(tSQL);
    			tLICardUploadLogSchema.setRiskCode(tempSSRS.GetText(1,1));
    			tLICardUploadLogSchema.setUploadFlag(RESPONSE_CODE.getText());
    			if(ERROR_MESSAGE.getText()!=null&&!ERROR_MESSAGE.getText().equals("")
    					&&RESPONSE_CODE.getText()!=null&&RESPONSE_CODE.getText().equals("0")){
    				tLICardUploadLogSchema.setUploadInfo(ERROR_MESSAGE.getText());
    			}else{
    				tLICardUploadLogSchema.setUploadInfo("上传成功！");
    			}
    			tLICardUploadLogSchema.setOperator(this.mGlobalInput.Operator);
    			tLICardUploadLogSchema.setMakeDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setMakeTime(PubFun.getCurrentTime());
    			tLICardUploadLogSchema.setModifyDate(PubFun.getCurrentDate());
    			tLICardUploadLogSchema.setModifyTime(PubFun.getCurrentTime());
    			this.mLICardUploadLogSet.add(tLICardUploadLogSchema);
    		}
    	}else{
    		CError tError = new CError();
            tError.moduleName = "LICardManuExportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "没有收到上传返回的信息，请检查数据是否上传完成!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	return true;
    }


}
