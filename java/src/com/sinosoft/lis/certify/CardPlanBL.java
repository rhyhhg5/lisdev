/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: CardPlanBL </p>
 * <p>Description: CardPlanBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2003-10-23
 */
package com.sinosoft.lis.certify;

import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardBugetSchema;
import com.sinosoft.lis.schema.LZCardPlanSchema;
import com.sinosoft.lis.vschema.LZCardBugetSet;
import com.sinosoft.lis.vschema.LZCardPlanSet;
import com.sinosoft.utility.*;

public class CardPlanBL
{
    // 一些常量定义
    public static String STATE_APP = "A";
    public static String STATE_CON = "C";
    public static String STATE_RET = "R";
    public static String STATE_PACK = "P";
    public static String RET_STATE_PASS = "Y";
    public static String RET_STATE_REFUSE = "N";
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String strOperate;


    /** 业务处理相关变量 */
    private LZCardPlanSchema mLZCardPlanSchema = new LZCardPlanSchema();
    private LZCardPlanSet mLZCardPlanSet = new LZCardPlanSet();
    private String m_strWhere = "";
    private Hashtable m_hashParams = null;

    public CardPlanBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        strOperate = verifyOperate(cOperate);
        if (strOperate.equals(""))
        {
            buildError("submitData", "不支持的操作字符串");

            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        VData tVData = new VData();

        if (!prepareOutputData(tVData))
        {
            return false;
        }
        if (strOperate.equals("QUERY||BUGET"))
        {
            this.queryPlanBuget(mLZCardPlanSchema, mGlobalInput);
        }
        else if (strOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            CardPlanBLS tCardPlanBLS = new CardPlanBLS();

            if (!tCardPlanBLS.submitData(tVData, strOperate))
            {
                if (tCardPlanBLS.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tCardPlanBLS.mErrors);

                    return false;
                }
                else
                {
                    buildError("submitData", "CardPlanBLS发生错误，但是没有详细信息。");

                    return false;
                }
            }
        }

        mInputData = null;

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (strOperate.equals("INSERT||MAIN"))
        {
            return saveCardPlan(mLZCardPlanSchema, mLZCardPlanSet, mGlobalInput);
        }

        if (strOperate.equals("UPDATE||MAIN"))
        {
            return updateCardPlan(mLZCardPlanSchema, mLZCardPlanSet,
                                  mGlobalInput);
        }

        if (strOperate.equals("UPDATE||RET"))
        {
            return retCardPlan(mLZCardPlanSchema, mLZCardPlanSet, mGlobalInput);
        }

        if (strOperate.equals("DELETE||MAIN"))
        {
            return deleteCardPlan(mLZCardPlanSchema, mLZCardPlanSet,
                                  mGlobalInput);
        }

        if (strOperate.equals("PACK||MAIN"))
        {
            return packCardPlan(mLZCardPlanSchema, mLZCardPlanSet, mGlobalInput);
        }

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            this.mLZCardPlanSchema.setSchema((LZCardPlanSchema) cInputData
                                             .getObjectByObjectName(
                    "LZCardPlanSchema",
                    0));
            this.mLZCardPlanSet.set((LZCardPlanSet) cInputData
                                    .getObjectByObjectName("LZCardPlanSet", 0));
            this.mGlobalInput.setSchema((GlobalInput) cInputData
                                        .getObjectByObjectName("GlobalInput", 0));
            m_strWhere = (String) cInputData.getObjectByObjectName("String", 0);
            m_hashParams = (Hashtable) cInputData.getObjectByObjectName(
                    "Hashtable",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getInputData", "发生异常");

            return false;
        }

        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        mResult.clear();

        SQLString sqlObj = new SQLString("LZCardPlan");

        sqlObj.setSQL(5, mLZCardPlanSchema);

        String strSql = sqlObj.getSQL();
        String strSqlAppend = "1=1";

        // process two extra parameters
        String strTemp = "";

        strTemp = (String) m_hashParams.get("MakeDateB");
        if ((strTemp != null) && !strTemp.equals(""))
        {
            strSqlAppend += (" AND MakeDate >= '" + strTemp + "'");
        }

        strTemp = (String) m_hashParams.get("MakeDateE");
        if ((strTemp != null) && !strTemp.equals(""))
        {
            strSqlAppend += (" AND MakeDate <= '" + strTemp + "'");
        }

        // process m_strwhere
        if ((m_strWhere != null) && !m_strWhere.equals(""))
        {
            strSqlAppend += (" AND " + m_strWhere);
        }

        if (strSql.toUpperCase().indexOf("WHERE") != -1)
        {
            strSql += (" AND " + strSqlAppend);
        }
        else
        {
            strSql += (" WHERE " + strSqlAppend);
        }

        // 如果没有传入起始页和每页的记录数，则查询出所有满足条件的记录。
        int nCurrentPage = Integer.parseInt((String) m_hashParams.get(
                "CurrentPage"));
        int nRecordNum = Integer.parseInt((String) m_hashParams.get("RecordNum"));

        try
        {
            nCurrentPage = Integer.parseInt((String) m_hashParams.get(
                    "CurrentPage"));
            nRecordNum = Integer.parseInt((String) m_hashParams.get("RecordNum"));
        }
        catch (Exception ex)
        {
            nCurrentPage = -1;
        }

        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = null;

        if ((nCurrentPage >= 0) && (nRecordNum > 0))
        {
            ssrs = exeSQL.execSQL(strSql, (nCurrentPage * nRecordNum) + 1,
                                  nRecordNum);
        }
        else
        {
            ssrs = exeSQL.execSQL(strSql);
        }

        if (exeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(exeSQL.mErrors);
            buildError("submitData", "数据提交失败");

            return false;
        }

        mResult.add(String.valueOf(getPageCount(strSql, nRecordNum)));
        mResult.add(ssrs);

        mInputData = null;

        return true;
    }

    private boolean prepareOutputData(VData aVData)
    {
        try
        {
            aVData.clear();

            aVData.add(mGlobalInput);
            aVData.add(mLZCardPlanSchema);
            aVData.add(mLZCardPlanSet);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareData", ex.getMessage());

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CardPlanBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String[] szOperates =
                {
                "INSERT||MAIN", "UPDATE||MAIN", "UPDATE||RET",
                "DELETE||MAIN", "QUERY||MAIN", "PACK||MAIN",
                "QUERY||BUGET"
        };

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }


    /**
     * query   CardPlan    Buget
     * refactor  for method  by guoxiang at 2004-5-12
     * @param aLZCardPlanSchema
     * @param aLZCardPlanSet
     * @param globalInput
     * @return
     */
    protected boolean queryPlanBuget(LZCardPlanSchema aLZCardPlanSchema,
                                     GlobalInput globalInput)
    {
        mResult.clear();

        boolean OverBuget = false;

        //check whether over card buget . add by guoxiang at 2004-04-03
        //first set switch var=1 or 0 for function control
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("CheckCardBuget");
        if (!tLDSysVarDB.getInfo())
        {
            mErrors.copyAllErrors(tLDSysVarDB.mErrors);

            return false;
        }

        String strCheckCardBuget = tLDSysVarDB.getSysVarValue();
        if (strCheckCardBuget.equals("1"))
        { //Switch on

            //get price from lzcardprint put into hashtalbe
            Hashtable hashPrice = new Hashtable();
            String strSQL =
                    "SELECT CertifyCode, CertifyPrice FROM LZCardPrint A"
                    + " WHERE PrtNo = (SELECT MAX(PrtNo) FROM LZCardPrint B"
                    + " WHERE B.CertifyCode = A.CertifyCode)"
                    + " ORDER BY CertifyCode";
            ExeSQL es = new ExeSQL();
            SSRS ssrs = es.execSQL(strSQL);
            if (ssrs.mErrors.needDealError())
            {
                buildError("queryPlanBuget", "取单证印刷费用失败！");
                mErrors.copyAllErrors(ssrs.mErrors);
            }

            String pCertifyCode = "";
            for (int nIndex = 0; nIndex < ssrs.getMaxRow(); nIndex++)
            {
                pCertifyCode = ssrs.GetText(nIndex + 1, 1);
                hashPrice.put(pCertifyCode, ssrs.GetText(nIndex + 1, 2));
            }

            String strCertifyCode = aLZCardPlanSchema.getCertifyCode();

            //新增或查询该计划的对应的最终日期，时间,机构
            //以及批复数量和印刷费用
            String newPlanDate = aLZCardPlanSchema.getUpdateDate();
            String newPlanTime = aLZCardPlanSchema.getUpdateTime();
            String newAppCom = aLZCardPlanSchema.getAppCom();
            String newReState = aLZCardPlanSchema.getRetState();
            double newPlanCount = aLZCardPlanSchema.getAppCount();
            String sCertifyPrice = (String) hashPrice.get(strCertifyCode);
            double newPlanMoney = newPlanCount *
                                  ReportPubFun.functionDouble(sCertifyPrice);

            //查询预算表,使得该计划的时间和申请单位进入预算表，取得的费用
            LZCardBugetDB tLZCardBugetDB = new LZCardBugetDB();
            String SSQL = "select * from LZCardBuget where 1=1 "
                          + ReportPubFun.getWherePart("SDate", "", newPlanDate,
                    1)
                          + ReportPubFun.getWherePart("EDate", newPlanDate, "",
                    1)
                          + ReportPubFun.getWherePart("ManageCom", newAppCom);
            LZCardBugetSet tLZCardBugetSet = tLZCardBugetDB.executeQuery(SSQL);
            String ttSDate = "";
            String ttEDate = "";
            double BugetMoney = 0;
            for (int nIndex = 0; nIndex < tLZCardBugetSet.size(); nIndex++)
            {
                LZCardBugetSchema tLZCardBugetSchema = tLZCardBugetSet.get(
                        nIndex
                        + 1);
                BugetMoney = tLZCardBugetSchema.getBuget(); //预算表的费用
                ttSDate = tLZCardBugetSchema.getSDate(); //预算开始时间
                ttEDate = tLZCardBugetSchema.getEDate(); //预算结束时间

                break;
            }

            //新增或查询计划的时间段内和单位对应的计划表中的所有在的这个计划
            //最终系统时间之前的已批付的计划的费用总和
            String mCertifyCodePrice = "";
            double mAppCount = 0;
            double mMoney = 0;
            double sMoney = 0;
            Date tttDate = null;

            LZCardPlanDB mLZCardPlanDB = new LZCardPlanDB();
            String StrSQL = "select * from lzcardplan where retstate='Y'" //已批付的
                            + " AND (updatedate<'" + newPlanDate + "'"
                            + " OR (updatedate='" + newPlanDate +
                            "' AND updatetime<'" + newPlanTime + "'))"
                            + ReportPubFun.getWherePart("updatedate", ttSDate,
                    ttEDate, 1) //新增或查询计划的时间段内
                            + ReportPubFun.getWherePart("AppCom", newAppCom); //单位对应
            System.out.println("print sql" + StrSQL);
            LZCardPlanSet mLZCardPlanSet = mLZCardPlanDB.executeQuery(StrSQL);

            for (int nIndex = 0; nIndex < mLZCardPlanSet.size(); nIndex++)
            {
                LZCardPlanSchema tLZCardPlanSchema = mLZCardPlanSet.get(nIndex
                        + 1);
                mCertifyCodePrice = (String) hashPrice.get(tLZCardPlanSchema
                        .getCertifyCode());
                mAppCount = tLZCardPlanSchema.getAppCount();
                mMoney = mAppCount *
                         ReportPubFun.functionDouble(mCertifyCodePrice);
                System.out.println("第" + (nIndex + 1) + "已批付的计划的印刷费用" + mMoney);
                sMoney += mMoney;
            }

            String message = "";

            if (((sMoney + newPlanMoney) - BugetMoney) > 0)
            {
                message = "超过预算!\n";
                OverBuget = true;
            }
            else
            {
                message = "没有超过预算!\n";
                OverBuget = false;
            }

            message += ("费用名细如下：\n");
            message +=
                    (" 1.计划批复状态为" + newReState + "的该计划印刷费用：" + newPlanMoney + "元；\n");
            message +=
                    (" 2.在" + ttSDate + "至" + ttEDate + "时间段内并且时间在" + newPlanDate +
                     "-" + newPlanTime + "之前，\n");
            message += ("申请机构为" + newAppCom + "的已批付计划的总印刷费用：" + sMoney + "元；\n");
            message += (" 3.在" + ttSDate + "至" + ttEDate + "时间段内\n");
            message += ("申请机构为" + newAppCom + "的单证预算：" + BugetMoney + "元；\n");
            System.out.println(message);
            mResult.add(message);
            mInputData = null;
        }

        return OverBuget;
    }


    /**
     * create a new card plan
     * @param aLZCardPlanSchema
     * @param aLZCardPlanSet
     * @param globalInput
     * @return
     */
    private boolean saveCardPlan(LZCardPlanSchema aLZCardPlanSchema,
                                 LZCardPlanSet aLZCardPlanSet,
                                 GlobalInput globalInput)
    {
        boolean bReturn = true;
        try
        {
            // check whether current com can print specified certify
            String strCertifyCode = aLZCardPlanSchema.getCertifyCode();
            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            tLMCertifyDesDB.setCertifyCode(strCertifyCode);
            if (!tLMCertifyDesDB.getInfo())
            {
                mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);

                return false;
            }

            String strManageCom = tLMCertifyDesDB.getManageCom();
            if ((strManageCom != null) && !strManageCom.equals(""))
            {
                if (strManageCom.equals(globalInput.ComCode))
                {
                    throw new Exception("所选单证不需要上报上级公司");
                }
            }

            // set value of mail plan info
            aLZCardPlanSchema.setPlanID(PubFun1.CreateMaxNo("SERIALNO",
                    "CARDPLAN"));
            aLZCardPlanSchema.setAppCom(globalInput.ComCode);
            aLZCardPlanSchema.setAppOperator(globalInput.Operator);
            aLZCardPlanSchema.setRetCom(getSuperior(globalInput.ComCode));
            aLZCardPlanSchema.setRetState(CardPlanBL.RET_STATE_REFUSE);
            aLZCardPlanSchema.setRelaPlan(SysConst.ZERONO);
            aLZCardPlanSchema.setRelaPrint(SysConst.ZERONO);
            aLZCardPlanSchema.setMakeDate(PubFun.getCurrentDate());
            aLZCardPlanSchema.setMakeTime(PubFun.getCurrentTime());
            aLZCardPlanSchema.setUpdateDate(PubFun.getCurrentDate());
            aLZCardPlanSchema.setUpdateTime(PubFun.getCurrentTime());
            aLZCardPlanSchema.setPlanState(CardPlanBL.STATE_APP);

            LZCardPlanDB tLZCardPlanDB = null;

            // check info of plan list
            for (int nIndex = 0; nIndex < aLZCardPlanSet.size(); nIndex++)
            {
                LZCardPlanSchema tLZCardPlanSchema = aLZCardPlanSet.get(nIndex
                        + 1);
                if (!strCertifyCode.equals(tLZCardPlanSchema.getCertifyCode()))
                {
                    throw new Exception("关联列表中的单证编码与单证计划中的不一致");
                }

                tLZCardPlanDB = new LZCardPlanDB();
                tLZCardPlanDB.setPlanID(tLZCardPlanSchema.getPlanID());
                tLZCardPlanDB.getInfo();
                tLZCardPlanSchema.setSchema(tLZCardPlanDB);

                tLZCardPlanSchema.setRelaPlan(aLZCardPlanSchema.getPlanID());
                tLZCardPlanSchema.setPlanState(CardPlanBL.STATE_CON);
            }
            if (queryPlanBuget(aLZCardPlanSchema, globalInput))
            { // 超出单证预算
                bReturn = true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("saveCardPlan", ex.getMessage());
            bReturn = false;
        }

        return bReturn;
    }


    /**
     * update a card plan
     * @param aLZCardPlanSchema
     * @param aOldLZCardPlanSet
     * @param aNewLZCardPlanSet
     * @param globalInput
     * @return
     */
    private boolean updateCardPlan(LZCardPlanSchema aLZCardPlanSchema,
                                   LZCardPlanSet aLZCardPlanSet,
                                   GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            LZCardPlanSchema tLZCardPlanSchema = new LZCardPlanSchema();

            // get the original card plan
            LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB();
            tLZCardPlanDB.setPlanID(aLZCardPlanSchema.getPlanID());
            if (!tLZCardPlanDB.getInfo())
            {
                mErrors.copyAllErrors(tLZCardPlanDB.mErrors);

                return false;
            }
            tLZCardPlanSchema.setSchema(tLZCardPlanDB);

            if (!tLZCardPlanSchema.getPlanState().equals(CardPlanBL.STATE_APP))
            {
                throw new Exception("该单证计划不是处于申请状态，不能修改。");
            }

            // check whether current com can print specified certify
            String strCertifyCode = aLZCardPlanSchema.getCertifyCode();

            LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
            tLMCertifyDesDB.setCertifyCode(strCertifyCode);
            if (!tLMCertifyDesDB.getInfo())
            {
                mErrors.copyAllErrors(tLMCertifyDesDB.mErrors);

                return false;
            }

            String strManageCom = tLMCertifyDesDB.getManageCom();
            if ((strManageCom != null) && !strManageCom.equals(""))
            {
                if (strManageCom.equals(globalInput.ComCode))
                {
                    throw new Exception("所选单证不需要上报上级公司");
                }
            }

            // set value of main plan info
            tLZCardPlanSchema.setCertifyCode(aLZCardPlanSchema.getCertifyCode());
            tLZCardPlanSchema.setAppCount(aLZCardPlanSchema.getAppCount());
            tLZCardPlanSchema.setUpdateDate(PubFun.getCurrentDate());
            tLZCardPlanSchema.setUpdateTime(PubFun.getCurrentTime());

            // copy info to aLZCardPlanSchema
            aLZCardPlanSchema.setSchema(tLZCardPlanSchema);

            LZCardPlanSet tLZCardPlanSet = new LZCardPlanSet();

            // get the original card plan list
            tLZCardPlanDB = new LZCardPlanDB();
            tLZCardPlanDB.setRelaPlan(aLZCardPlanSchema.getPlanID());
            tLZCardPlanSet.set(tLZCardPlanDB.query());

            // format original plan list of this plan
            for (int nIndex = 0; nIndex < tLZCardPlanSet.size(); nIndex++)
            {
                tLZCardPlanSchema = tLZCardPlanSet.get(nIndex + 1);

                tLZCardPlanSchema.setRelaPlan(SysConst.ZERONO);
                tLZCardPlanSchema.setPlanState(CardPlanBL.STATE_APP);
            }

            // format new plan lis of this plan
            for (int nIndex = 0; nIndex < aLZCardPlanSet.size(); nIndex++)
            {
                tLZCardPlanSchema = aLZCardPlanSet.get(nIndex + 1);

                if (!strCertifyCode.equals(tLZCardPlanSchema.getCertifyCode()))
                {
                    throw new Exception("关联列表中的单证编码与单证计划中的不一致");
                }

                tLZCardPlanDB = new LZCardPlanDB();
                tLZCardPlanDB.setPlanID(tLZCardPlanSchema.getPlanID());
                tLZCardPlanDB.getInfo();
                tLZCardPlanSchema.setSchema(tLZCardPlanDB);

                tLZCardPlanSchema.setRelaPlan(aLZCardPlanSchema.getPlanID());
                tLZCardPlanSchema.setPlanState(CardPlanBL.STATE_CON);
            }

            aLZCardPlanSet.add(tLZCardPlanSet);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("updateCardPlan", ex.getMessage());
            bReturn = false;
        }

        return bReturn;
    }


    /**
     * ret a card plan
     * @param aLZCardPlanSchema
     * @param aOldLZCardPlanSet
     * @param aNewLZCardPlanSet
     * @param globalInput
     * @return
     */
    private boolean retCardPlan(LZCardPlanSchema aLZCardPlanSchema,
                                LZCardPlanSet aLZCardPlanSet,
                                GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            LZCardPlanSchema tLZCardPlanSchema = null;
            LZCardPlanDB tLZCardPlanDB = null;

            // check info of plan list
            for (int nIndex = 0; nIndex < aLZCardPlanSet.size(); nIndex++)
            {
                tLZCardPlanSchema = aLZCardPlanSet.get(nIndex + 1);

                tLZCardPlanDB = new LZCardPlanDB();
                tLZCardPlanDB.setPlanID(tLZCardPlanSchema.getPlanID());
                tLZCardPlanDB.getInfo();

                //        if( tLZCardPlanDB.getPlanState().equals(CardPlanBL.STATE_PACK) ) {
                //          throw new Exception(tLZCardPlanDB.getPlanID() + "号计划已经归档，不能批复。");
                //        }
                // set values which user input
                tLZCardPlanDB.setRetCount(tLZCardPlanSchema.getRetCount());
                tLZCardPlanDB.setRetState(tLZCardPlanSchema.getRetState());
                tLZCardPlanDB.setPlanState(CardPlanBL.STATE_RET);

                tLZCardPlanSchema.setSchema(tLZCardPlanDB);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("retCardPlan", ex.getMessage());
            bReturn = false;
        }

        return bReturn;
    }


    /**
     * deleteCardPlan
     * @param aLZCardPlanSchema
     * @param aOldLZCardPlanSet
     * @param aNewLZCardPlanSet
     * @param globalInput
     * @return
     */
    private boolean deleteCardPlan(LZCardPlanSchema aLZCardPlanSchema,
                                   LZCardPlanSet aLZCardPlanSet,
                                   GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            LZCardPlanSchema tLZCardPlanSchema = new LZCardPlanSchema();

            // get the original card plan
            LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB();
            tLZCardPlanDB.setPlanID(aLZCardPlanSchema.getPlanID());
            if (!tLZCardPlanDB.getInfo())
            {
                mErrors.copyAllErrors(tLZCardPlanDB.mErrors);

                return false;
            }
            tLZCardPlanSchema.setSchema(tLZCardPlanDB);

            if (!tLZCardPlanSchema.getPlanState().equals(CardPlanBL.STATE_APP))
            {
                throw new Exception("该单证计划不是处于申请状态，不能删除。");
            }

            // copy info to aLZCardPlanSchema
            aLZCardPlanSchema.setSchema(tLZCardPlanSchema);

            // get the original card plan list
            tLZCardPlanDB = new LZCardPlanDB();
            tLZCardPlanDB.setRelaPlan(aLZCardPlanSchema.getPlanID());
            aLZCardPlanSet.set(tLZCardPlanDB.query());

            for (int nIndex = 0; nIndex < aLZCardPlanSet.size(); nIndex++)
            {
                tLZCardPlanSchema = aLZCardPlanSet.get(nIndex + 1);

                tLZCardPlanSchema.setRelaPlan(SysConst.ZERONO);
                tLZCardPlanSchema.setPlanState(CardPlanBL.STATE_APP);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("deleteCardPlan", ex.getMessage());
            bReturn = false;
        }

        return bReturn;
    }


    /**
     * package a card plan
     * @param aLZCardPlanSchema
     * @param aLZCardPlanSet
     * @param globalInput
     * @return
     */
    private boolean packCardPlan(LZCardPlanSchema aLZCardPlanSchema,
                                 LZCardPlanSet aLZCardPlanSet,
                                 GlobalInput globalInput)
    {
        boolean bReturn = true;

        try
        {
            LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB();
            tLZCardPlanDB.setPlanID(aLZCardPlanSchema.getPlanID());
            if (!tLZCardPlanDB.getInfo())
            {
                mErrors.copyAllErrors(tLZCardPlanDB.mErrors);

                return false;
            }
            aLZCardPlanSchema.setSchema(tLZCardPlanDB);

            if (!aLZCardPlanSchema.getPlanState().equals(CardPlanBL.STATE_RET))
            {
                throw new Exception("该单证计划不是处于批复状态，不能归档。");
            }

            // set package info
            aLZCardPlanSchema.setUpdateDate(PubFun.getCurrentDate());
            aLZCardPlanSchema.setUpdateTime(PubFun.getCurrentTime());
            aLZCardPlanSchema.setPlanState(CardPlanBL.STATE_PACK);

            // clear card plan list
            aLZCardPlanSet.clear();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("packCardPlan", ex.getMessage());
            bReturn = false;
        }

        return bReturn;
    }


    /**
     * query ldcom, get superior com of specified com
     * @param strCom
     * @return
     */
    private String getSuperior(String strCom)
    {
        String strSQL = "select code from ldcode"
                        + " where codetype = 'station'" + " and '" + strCom
                        + "' like '%' || trim(code) || '%'" + " and '" + strCom
                        + "' <> code" + " order by code desc";

        ExeSQL exeSQL = new ExeSQL();
        SSRS rs = exeSQL.execSQL(strSQL);

        if (rs.getMaxRow() != 0)
        {
            return rs.GetText(1, 1);
        }

        return "";
    }

    private int getPageCount(String strSQL, int nRecordNum)
    {
        String sSQL = strSQL.toLowerCase();

        sSQL = "select count(*) " + strSQL.substring(strSQL.indexOf("from"));

        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(sSQL);

        int nRecordCount = Integer.parseInt(ssrs.GetText(1, 1));

        return ((nRecordCount + nRecordNum) - 1) / nRecordNum;
    }
}
