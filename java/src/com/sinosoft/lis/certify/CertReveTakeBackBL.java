/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import java.util.Hashtable;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理单证发放操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 周平
 * @version 1.0
 */

public class CertReveTakeBackBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /* 私有成员 */
    private String mszOperate = "";
    private String mszTakeBackNo = "";


    /* 业务相关的数据 */
    private GlobalInput globalInput = new GlobalInput();
    private LZCardSet mLZCardSet = new LZCardSet();
    private Hashtable mParams = null;

    private VData mResult = new VData();

    private int m_nOperIndex = 0;

    public CertReveTakeBackBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = verifyOperate(cOperate);
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))return false;

        if (!dealData())
        {
            buildFailSet();
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        if (mszOperate.equals("INSERT"))
        {
            // 产生回收清算单号
            mszTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO",
                                                PubFun.getNoLimit(globalInput.
                    ComCode));

            mResult.clear();
            mResult.add(mszTakeBackNo);

            m_nOperIndex = 0;

            for (int nIndex = 0; nIndex < mLZCardSet.size(); nIndex++)
            {
                LZCardSchema tLZCardSchema = mLZCardSet.get(nIndex + 1);

                // Verify SendOutCom and ReceiveCom
                if (!CertifyFunc.verifyComs(globalInput,
                                            tLZCardSchema.getSendOutCom(),
                                            tLZCardSchema.getReceiveCom()))
                {

                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                LZCardSet tLZCardSet = CertifyFunc.formatCardList(tLZCardSchema);

                if (tLZCardSet == null)
                {
                    mErrors.copyAllErrors(CertifyFunc.mErrors);
                    return false;
                }

                VData vResult = new VData();

                for (int nInnerIndex = 0; nInnerIndex < tLZCardSet.size();
                                       nInnerIndex++)
                {
                    tLZCardSchema = tLZCardSet.get(nInnerIndex + 1);

                    tLZCardSchema.setTakeBackNo(mszTakeBackNo);

                    VData vOneResult = new VData();

                    if (!CertifyFunc.splitCertReveTakeBack(globalInput,
                            tLZCardSchema, vOneResult))
                    {
                        mErrors.copyAllErrors(CertifyFunc.mErrors);
                        return false;
                    }

                    vResult.add(vOneResult);
                }

                CertReveTakeBackBLS tCertReveTakeBackBLS = new
                        CertReveTakeBackBLS();
                if (!tCertReveTakeBackBLS.submitData(vResult, "INSERT"))
                {
                    if (tCertReveTakeBackBLS.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tCertReveTakeBackBLS.mErrors);
                        return false;
                    }
                    else
                    {
                        buildError("dealOne",
                                   "CertReveTakeBackBLS出错，但是没有提供详细的信息");
                        return false;
                    }
                }

                m_nOperIndex++;
            }
        } // end of if( mszOperate.equals("INSERT") );

        return true;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
        if (mszOperate.equals("INSERT"))
        {
            globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLZCardSet.set((LZCardSet) vData.getObjectByObjectName("LZCardSet",
                    0));
            mParams = (Hashtable) vData.getObjectByObjectName("Hashtable", 0);
        }
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            if (mszOperate.equals("INSERT"))
            {
                vData.clear();
                vData.addElement(globalInput);
                vData.addElement(mLZCardSet);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            buildError("prepareData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertReveTakeBackBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String verifyOperate(String szOperate)
    {
        String szReturn = "";
        String szOperates[] =
                              {"INSERT"};

        for (int nIndex = 0; nIndex < szOperates.length; nIndex++)
        {
            if (szOperate.equals(szOperates[nIndex]))
            {
                szReturn = szOperate;
            }
        }

        return szReturn;
    }

    private void buildFailSet()
    {
        LZCardSet tLZCardSet = new LZCardSet();

        for (int nIndex = m_nOperIndex; nIndex < mLZCardSet.size(); nIndex++)
        {
            tLZCardSet.add(mLZCardSet.get(nIndex + 1));
        }

        mResult.add(tLZCardSet);
    }
}
