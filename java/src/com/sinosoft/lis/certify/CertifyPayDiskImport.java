package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LZCardPaySet;


/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class CertifyPayDiskImport {
    /** 团单被保人清单Schema */
    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LZCardPaySchema";
    /** 团单被保人清单Schema */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LZCardPaySet";
    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;
    /** 错误处理 */
    public CErrors mErrrors = new CErrors();

    public CertifyPayDiskImport(String fileName, String configFileName,
                         String sheetName) {
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport()) {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet() {
        return importer.getSchemaSet();
    }

    public static void main(String[] args) {
        String path = "D:\\workspace\\UI\\temp\\18000013201_01.xls";
        String config = "D:\\workspace\\UI\\temp\\GrpDiskImport.xml";
        CertifyPayDiskImport tCertifyPayDiskImport = new CertifyPayDiskImport(path, config, "Sheet1");
        tCertifyPayDiskImport.doImport();
        LZCardPaySet tLZCardPaySet =
                (LZCardPaySet) tCertifyPayDiskImport.getSchemaSet();
        for (int i = 1; i <= tLZCardPaySet.size(); i++) {
            System.out.println("处理数据 " + i +
                               tLZCardPaySet.get(i).encode().replace('|',
                    '\t'));
        }
    }
}
