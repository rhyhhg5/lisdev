package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.certify.CertifyPayDiskImport;
import com.sinosoft.lis.certify.CertifyFunc;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class AddCardPayList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    /** 保全号 */
    private String mEdorNo = null;
    /** 批次号 */
    private String mBatchNo = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "CertifyPayDiskImport.xml";
    private LZCardSet mLZCardSet = new LZCardSet(); //单证状态表
    private LZCardPaySet mLZCardPaySet = new LZCardPaySet(); //单证结算单录入
    private LZCardTrackSet mLZCardTrackSet = new LZCardTrackSet(); //单证轨迹表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public AddCardPayList(GlobalInput gi) {
        this.mGlobalInput = gi;
    }


    /**
     * 添加被保人清单
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        CertifyPayDiskImport importFile = new CertifyPayDiskImport(path +
                fileName,
                path + configName,
                sheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrrors);
            return false;
        }

        LZCardPaySet tLZCardPaySet = (LZCardPaySet) importFile.
                                     getSchemaSet();

        //校验导入的数据
        if (!checkData(tLZCardPaySet)) {
            return false;
        }

        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLZCardPaySet.size(); i++) {
            //添加一个被保人
            addOneCardPay(map, tLZCardPaySet.get(i), i);
        }
        if (!CertifyFunc.splitDivMoney(tLZCardPaySet,
                                       tLZCardPaySet.get(1).getActPayMoney())) {

            mErrors.addOneError("结算单录入时分费用错误,原因是总费用与拆分后的费用不符!");
            return false;
        }

        this.mResult.add(tLZCardPaySet);
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }

        return true;
    }

    private void deletePayList(MMap map) {

    }

    /**
     * 校验导入数据
     * @param cLZCardPaySet LZCardPaySet
     * @return boolean
     */
    private boolean checkData(LZCardPaySet cLZCardPaySet) {
        for (int i = 1; i <= cLZCardPaySet.size(); i++) {
            LZCardPaySchema schema = cLZCardPaySet.get(i);
            String strPayNo = schema.getPayNo();
            if ((strPayNo == null) || (strPayNo.equals(""))) {
                mErrors.addOneError("缺少结算单号！");
                return false;
            }

            String strAgentCode = schema.getHandlerCode();
            if ((strAgentCode == null) || (strAgentCode.equals(""))) {
                mErrors.addOneError("缺少回销人！");
                return false;
            }
            String strCardType = schema.getCardType();
            if ((strCardType == null) || (strCardType.equals(""))) {
                mErrors.addOneError("缺少单证编码类型！");
                return false;
            }

            String strStartNo = schema.getStartNo();
            if ((strStartNo == null) || (strStartNo.equals(""))) {
                mErrors.addOneError("缺少起始号！");
                return false;
            }

            String strEndNo = schema.getEndNo();
            if ((strEndNo == null) || (strEndNo.equals(""))) {
                mErrors.addOneError("缺少终止号！");
                return false;
            }
            if (strStartNo.compareTo(strEndNo) > 0) {
                mErrors.addOneError("起始号大于终止号！");
                return false;
            }
            int intSumCount = schema.getSumCount();
            if ((intSumCount == 0 || intSumCount <= 0)) {
                mErrors.addOneError("卡数量不正确！");
                return false;
            }
            double dSumCount = schema.getDuePayMoney();
            if ((dSumCount == 0 || dSumCount <= 0)) {
                mErrors.addOneError("结算总金额错误！");
                return false;
            }
            if (!CertifyFunc.chkSendOut(schema)) {
                mErrors.addOneError("在第" + i + "行，录入的和下发的单证数量不相等！");
                return false;
            }
            if (!CertifyFunc.chkAgentCom(schema)) {
                mErrors.addOneError("在第" + i + "行，录入的和下发的代理机构不相等！");
                return false;
            }

        }
        double dMoney = cLZCardPaySet.get(1).getDuePayMoney();
        if (!CertifyFunc.chkRiskMoney(cLZCardPaySet, 1, dMoney)) {
            mErrors.addOneError("结算总金额与险种保费不同！");
            return false;
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private void addOneCardPay(MMap map,
                               LZCardPaySchema cLZCardPaySchema, int i) {

        LZCardPaySchema tLZCardPaySchema = cLZCardPaySchema;
        String strStartNo = String.valueOf(cLZCardPaySchema.getStartNo());
        String strEndNo = String.valueOf(cLZCardPaySchema.getEndNo());
        strStartNo = PubFun.LCh(strStartNo, "0", 7);
        strEndNo = PubFun.LCh(strEndNo, "0", 7);
        tLZCardPaySchema.setStartNo(strStartNo);
        tLZCardPaySchema.setEndNo(strEndNo);
        tLZCardPaySchema.setManageCom(mGlobalInput.ManageCom);
        tLZCardPaySchema.setHandleDate(mCurrentDate);
        tLZCardPaySchema.setState("0");
        tLZCardPaySchema.setActPayMoney(tLZCardPaySchema.getDuePayMoney());
        tLZCardPaySchema.setOperator(mGlobalInput.Operator);
        tLZCardPaySchema.setMakeDate(mCurrentDate);
        tLZCardPaySchema.setMakeTime(mCurrentTime);
        tLZCardPaySchema.setModifyDate(mCurrentDate);
        tLZCardPaySchema.setModifyTime(mCurrentTime);
        LZCardDB tLZCardDB = new LZCardDB();
        StringBuffer sb = new StringBuffer();
        sb.append(" select * from LZCard where subcode ='")
                .append(tLZCardPaySchema.getCardType())
                .append("' and  StartNo>='")
                .append(strStartNo)
                .append("' and  StartNo<='")
                .append(strEndNo)
                .append("' and state in ('10','11')")
                ;
        String strSql = sb.toString();
        System.out.println(strSql);
        LZCardSet tLZCardSet = tLZCardDB.executeQuery(strSql);
        if (tLZCardSet == null || tLZCardSet.size() <= 0) {
            String str = "查询卡单信息失败!";
            // buildError("dealData", str);
            System.out.println(
                    "在程序CertifyPayBL.dealData() - 167 : " +
                    str);
            // return false;
        }
        if (tLZCardSet.size() != tLZCardPaySchema.getSumCount()) {
            String str = "卡单已领用数和录入数不同!";
            //buildError("dealData", str);
            System.out.println(
                    "在程序CertifyPayBL.dealData() - 175 : " +
                    str);
            // return false;
        }

        for (int m = 1; m <= tLZCardSet.size(); m++) {

            LZCardSchema tLZCardSchema = new LZCardSchema();
            tLZCardSchema = tLZCardSet.get(m).getSchema();
            tLZCardSet.get(m).setState("12"); //已录入
            LZCardTrackSchema tLZCardTrackSchema = new
                    LZCardTrackSchema();
            tLZCardTrackSchema.setCertifyCode(tLZCardSchema.
                                              getCertifyCode());

            tLZCardTrackSchema.setSubCode(tLZCardSchema.
                                          getSubCode());

            tLZCardTrackSchema.setRiskCode("0");
            tLZCardTrackSchema.setRiskVersion("0");

            tLZCardTrackSchema.setStartNo(tLZCardSchema.
                                          getStartNo());
            tLZCardTrackSchema.setEndNo(tLZCardSchema.getEndNo());
            tLZCardTrackSchema.setSendOutCom(tLZCardSchema.
                                             getSendOutCom());
            tLZCardTrackSchema.setReceiveCom(tLZCardSchema.
                                             getReceiveCom());
            tLZCardTrackSchema.setSumCount(tLZCardSchema.
                                           getSumCount());
            tLZCardTrackSchema.setPrem("");
            tLZCardTrackSchema.setAmnt("");
            tLZCardTrackSchema.setHandler(tLZCardSchema.getHandler());
            tLZCardTrackSchema.setHandleDate(tLZCardSchema.
                                             getHandleDate());
            tLZCardTrackSchema.setInvaliDate(tLZCardSchema.
                                             getInvaliDate());
            tLZCardTrackSchema.setTakeBackNo(tLZCardSchema.
                                             getTakeBackNo());
            tLZCardTrackSchema.setSaleChnl(tLZCardSchema.
                                           getSaleChnl());
            tLZCardTrackSchema.setStateFlag(tLZCardSchema.
                                            getStateFlag());
            tLZCardTrackSchema.setOperateFlag("0");
            tLZCardTrackSchema.setPayFlag(tLZCardSchema.getPayFlag());
            tLZCardTrackSchema.setEnterAccFlag("");
            tLZCardTrackSchema.setReason("");
            tLZCardTrackSchema.setState("12");
            tLZCardTrackSchema.setOperator(mGlobalInput.Operator);
            tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
            tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
            tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());

            mLZCardTrackSet.add(tLZCardTrackSchema);
        }
        mLZCardSet.add(tLZCardSet);
        mLZCardPaySet.add(tLZCardPaySchema);
        map.put(mLZCardPaySet, "DELETE&INSERT");
        map.put(mLZCardTrackSet, "DELETE&INSERT");
        map.put(mLZCardSet, "DELETE&INSERT");
    }


    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";
        String grpContNo = "1400000915";
        String path = "d:\\";
        String fileName = "JSBJ2006070601.xls";
        path = "D:\\work\\UI\\temp\\";
        String config = "D:\\work\\UI\\temp\\CertifyPayDiskImport.xml";
        AddCardPayList tAddCardPayList = new AddCardPayList(tGI);
        if (!tAddCardPayList.doAdd(path, fileName)) {
            System.out.println(tAddCardPayList.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
}
