package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;

public class InvoiceNoModifyBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LOPRTManager2Set mLOPRTManager2Set = new LOPRTManager2Set();
    private String mCertifyCode = "";

    public InvoiceNoModifyBL() {
    }

    public static void main(String[] args) {
        String tLimit = PubFun.getNoLimit("86210000");
        String RGTNO = PubFun1.CreateMaxNo("CASENO", tLimit);
        VData tno = new VData();
        tno.add(0, 12 + "");
        tno.add(1, RGTNO);
        tno.add(2, "MAX");
        PubFun1.CreateMaxNo("CaseNo", tLimit, tno);
        System.out.println("getInputData start......");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        } else {
            System.out.println("getinputdata success!");
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDDrugBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLDDrugBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        } else {
            System.out.println("Start InvoiceNoMODIFYBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mResult, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "MODIFYBL";
                tError.functionName = "MODIFYBL";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End InvoiceNoModifyBL Submit...");
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
        for (int i = 1; i <= mLOPRTManager2Set.size(); i++) {
            String tOtherno = mLOPRTManager2Set.get(i).getOtherNo();
            String tStandByFlag4 = mLOPRTManager2Set.get(i).getStandbyFlag4();
            String tCertifyNo = mLOPRTManager2Set.get(i).getStandbyFlag1();

            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            LOPRTManager2Set aLOPRTManager2Set = new LOPRTManager2Set();
            tLOPRTManager2DB.setOtherNo(tOtherno);
            tLOPRTManager2DB.setStandbyFlag4(tStandByFlag4);
            aLOPRTManager2Set = tLOPRTManager2DB.query();
            if (aLOPRTManager2Set.size() != 1) {
                CError tError = new CError();
                tError.moduleName = "MODIFYBL";
                tError.functionName = "dealData";
                tError.errorMessage = "打印信息查询失败";
                this.mErrors.addOneError(tError);
                return false;
            }
            LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
            tLOPRTManager2Schema = aLOPRTManager2Set.get(1);

            LZCardDB tLZCardDB = new LZCardDB();
            String tCertifySQL = "select * from lzcard where certifycode = '" +
                                 mCertifyCode + "' and startno <= '" +
                                 tCertifyNo +
                                 "' and endno >= '" + tCertifyNo +
                                 "' and stateflag in ('2','3','4','5') and sendoutcom = 'B" +
                                 mGlobalInput.Operator +
                                 "'";
            LZCardSet tLZCardSet = new LZCardSet();
            tLZCardSet = tLZCardDB.executeQuery(tCertifySQL);
            if (tLZCardSet.size() != 1) {
                CError tError = new CError();
                tError.moduleName = "MODIFYBL";
                tError.functionName = "dealData";
                tError.errorMessage = "修改的发票号错误";
                this.mErrors.addOneError(tError);
                return false;
            }

            tLOPRTManager2Schema.setStandbyFlag4(tCertifyNo);
            tLOPRTManager2Schema.setExeOperator(mGlobalInput.Operator);
            tLOPRTManager2Set.add(tLOPRTManager2Schema);
        }
        map.put(tLOPRTManager2Set,"UPDATE");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData start......");
        this.mLOPRTManager2Set.add((LOPRTManager2Set) cInputData.
                                   getObjectByObjectName("LOPRTManager2Set", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mCertifyCode = ((LZCardSchema) cInputData.getObjectByObjectName(
                "LZCardSchema", 0)).getCertifyCode();
        return true;
    }


    private boolean prepareOutputData() {
        try {
//            mInputData.add(map);
            mResult.clear();
            mResult.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
