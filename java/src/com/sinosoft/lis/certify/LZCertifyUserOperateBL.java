package com.sinosoft.lis.certify;

import com.sinosoft.lis.db.LZAccessDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZAccessSchema;
import com.sinosoft.lis.vschema.LZAccessSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LZCertifyUserSchema;
import com.sinosoft.lis.db.LZCertifyUserDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006-04-14</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class LZCertifyUserOperateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LZCertifyUserSchema mLZCertifyUserSchema = new LZCertifyUserSchema();
    private LZAccessSet mLZAccessSet = new LZAccessSet();
    private LZAccessSet saveLZAccessSet = new LZAccessSet();

    MMap mMap = new MMap();
    PubSubmit tPubSubmit = new PubSubmit();

    //private LDComSet mLDComSet=new LDComSet();
    public LZCertifyUserOperateBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
System.out.println("come to BL..........................");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
System.out.println("cOperate: "+cOperate);
        //得到外部传入的数据,将数据备份到本类中

        if (!getInputData(cInputData))
            return false;
        if (!dealData())
            return false;
        //进行业务处理
        //准备往后台的数据

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //进行插入数据
        if (mOperate.equals("INSERT")) {
            if (!insertData()) {
                // @@错误处理
                //buildError("insertData", "增加单证管理员时出现错误!");
                return false;
            }
        }
    //对数据进行修改操作
        if (mOperate.equals("UPDATE")) {
            if (!updateData()) {
                // @@错误处理
              buildError("insertData", "单证管理员信息有误!");
              return false;
            }
        }
        //对数据进行删除操作
        if (mOperate.equals("DELETE")) {
            if (!deleteData()) {
                // @@错误处理
                buildError("insertData", "删除单证管理员时出现错误!");
                return false;
            }
        }

        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        LZCertifyUserDB tLZCertifyUserDB = new LZCertifyUserDB();
        tLZCertifyUserDB.setUserCode(mLZCertifyUserSchema.getUserCode());
        if (tLZCertifyUserDB.getInfo())
        {
            buildError("insertData", "该单证管理员已经存在!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();
System.out.println("currentTime: "+currentTime);
        mLZCertifyUserSchema.setMakeDate(currentDate);
        mLZCertifyUserSchema.setMakeTime(currentTime);
        mLZCertifyUserSchema.setModifyDate(currentDate);
        mLZCertifyUserSchema.setModiftTime(currentTime);

        String operator = mGlobalInput.Operator;
        mLZCertifyUserSchema.setOperator(operator);
        mLZCertifyUserSchema.setManageCom(mGlobalInput.ComCode);

        mMap.put(mLZCertifyUserSchema,"INSERT");
        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, ""))
        {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("insertData", "添加单证管理员时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        LZCertifyUserDB tLZCertifyUserDB = new LZCertifyUserDB();
        tLZCertifyUserDB.setUserCode(mLZCertifyUserSchema.getUserCode());
        if (!tLZCertifyUserDB.getInfo())
        {
            buildError("updateData", "单证管理员信息有误!");
            return false;
        }
        LZCertifyUserSchema tLZCertifyUserSchema = tLZCertifyUserDB.getSchema();


        tLZCertifyUserSchema.setUserCode(mLZCertifyUserSchema.getUserCode());
        tLZCertifyUserSchema.setUserName(mLZCertifyUserSchema.getUserName());
        tLZCertifyUserSchema.setCertifyDeal(mLZCertifyUserSchema.getCertifyDeal());
        tLZCertifyUserSchema.setComCode(mLZCertifyUserSchema.getComCode());
        tLZCertifyUserSchema.setDescription(mLZCertifyUserSchema.getDescription());
System.out.println("99999999999999999: "+mLZCertifyUserSchema.getDescription());
        tLZCertifyUserSchema.setStateFlag(mLZCertifyUserSchema.getStateFlag());
        tLZCertifyUserSchema.setUpUserCode(mLZCertifyUserSchema.getUpUserCode());

        tLZCertifyUserSchema.setModiftTime(currentTime);
        tLZCertifyUserSchema.setModifyDate(currentDate);
        String operator = mGlobalInput.Operator;
        tLZCertifyUserSchema.setOperator(operator);
        tLZCertifyUserSchema.setManageCom(mGlobalInput.ComCode);
        System.out.println(operator +"    *****    "+mGlobalInput.ComCode);
        mMap.put(tLZCertifyUserSchema,"UPDATE");

        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, ""))
       {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "单证管理员信息更新失败!");
               return false;
           }
           mMap = null;
       }
       System.out.println("after PubSubmit ............");
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        LZCertifyUserDB tLZCertifyUserDB = new LZCertifyUserDB();
        tLZCertifyUserDB.setUserCode(mLZCertifyUserSchema.getUserCode());
        if (!tLZCertifyUserDB.getInfo())
        {
            buildError("deleteData", "没有此单证管理员的信息");
            return false;
        }
        LZCertifyUserSchema tLZCertifyUserSchema = tLZCertifyUserDB.getSchema();
        mMap.put(tLZCertifyUserSchema,"DELETE");

        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, ""))
       {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "该单证管理员已经存在!");
               return false;
           }
           mMap = null;
       }
       System.out.println("after PubSubmit ............");
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLZCertifyUserSchema.setSchema((LZCertifyUserSchema) cInputData.
                                    getObjectByObjectName("LZCertifyUserSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {/*
        this.mResult.clear();
        System.out.println("Start LDComBLQuery Submit...");
        LZAccessDB tLZAccessDB = new LZAccessDB();
        tLZAccessDB.setSchema(this.mLZAccessSchema);
        ///this.mLDComSet=tLDComDB.query();
        //this.mResult.add(this.mLDComSet);
        System.out.println("End LDComBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLZAccessDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLZAccessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
    */
        return true;
    }

    private boolean prepareOutputData()
    {
System.out.println("Come to prepareOutputData()...........");
        try
        {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

   private void buildError(String szFunc, String szErrMsg)
   {
       CError cError = new CError();

       cError.moduleName = "CertifyFunc";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;

       System.out.println("In CertifyFunc buildError() : " + szErrMsg);
       mErrors.addOneError(cError);
   }

}


