package com.sinosoft.lis.certify;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author JavaBean
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class OrderCollectBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public OrderCollectBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("Come to RiskPayRateBL submitData()..........");

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("Come to RiskPayRateBL getInputData()..........");
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLRiskPayRateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //统计的起讫时间
        String serialNo   = (String) mTransferData.getValueByName("SerialNo");
        ListTable multTable = new ListTable();
        multTable.setName("MULT");
        String[] Title = {"col1", "col2", "col3","col4", "col5"};
        try {
            SSRS tSSRS = new SSRS();
            String sql =  "select a.certifycode ,b.certifyname,a.OrderCom,c.Name,a.Amnt  from LZOrderdetail a,LMCertifyDes b ,LDCom c "
            + " where a.certifycode=b.certifycode and c.ComCode=a.OrderCom "
            + " and a.SerialNo='"+serialNo+"' "
            + " order by a.certifyCode "
            ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            String temp[][] = tSSRS.getAllData();
            String certCode=temp[0][0];
            System.out.println("0 0 : "+certCode);
            String[] strCol;
            String[] tempCol ;
            int sumAmnt = 0;
            for (int i = 0; i < count; i++) {
                strCol = new String[5];
                if(temp[i][0].equals(certCode))
                {
                    strCol[0] = temp[i][0];
                    strCol[1] = temp[i][1];
                    strCol[2] = temp[i][2];
                    strCol[3] = temp[i][3];
                    strCol[4] = temp[i][4];
                    sumAmnt = sumAmnt + Integer.parseInt(temp[i][4]);
                    System.out.println("aaaa: "+strCol[0]+"  "+strCol[1]+"  "+strCol[2]+"  "+strCol[3]+"  "+strCol[4]);
                }
                else
                {
                    tempCol = new String[5];
                    tempCol[0] = temp[i-1][0];
                    tempCol[1] = temp[i-1][1];
                    tempCol[2] = "汇  总";
                    tempCol[3] = "";
                    tempCol[4] = sumAmnt+"";
                    sumAmnt = 0;
                    multTable.add(tempCol);

                    strCol[0] = temp[i][0];
                    strCol[1] = temp[i][1];
                    strCol[2] = temp[i][2];
                    strCol[3] = temp[i][3];
                    strCol[4] = temp[i][4];

                    sumAmnt = sumAmnt + Integer.parseInt(temp[i][4]);
                    certCode=temp[i][0];
                }
                multTable.add(strCol);
            }
            tempCol = new String[5];
            tempCol[0] = temp[count-1][0];
            tempCol[1] = temp[count-1][1];
            tempCol[2] = "汇  总";
            tempCol[3] = "";
            tempCol[4] = sumAmnt+"";

            multTable.add(tempCol);

        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "OrderCollectBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LZOrderCollect.vts", "printer"); //最好紧接着就初始化xml文档

        //OrderCollectBL模版元素
        texttag.add("SerialNo", serialNo);
        texttag.add("ManageComName", mGlobalInput.ComCode);
        texttag.add("CurrentDate", PubFun.getCurrentDate());
        texttag.add("Operator",mGlobalInput.Operator);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        //保存信息

        xmlexport.addListTable(multTable, Title);

//        xmlexport.addListTable(MonMult1Table, Title_1);



//        xmlexport.addListTable(MonMult2Table, Title_2);
//        xmlexport.addListTable(MonMult3Table, Title_3);
//        xmlexport.addListTable(MonMult4Table, Title_4);
//        xmlexport.addListTable(MonMult5Table, Title_5);
//        xmlexport.addListTable(MonMult6Table, Title_6);
//        xmlexport.addListTable(MonMult7Table, Title_7);
//        xmlexport.addListTable(MonMult8Table, Title_8);
//        xmlexport.addListTable(MonMult9Table, Title_9);
//        xmlexport.addListTable(MonMult10Table, Title_10);
//        xmlexport.addListTable(MonMult11Table, Title_11);
//        xmlexport.addListTable(MonMult12Table, Title_12);

//        xmlexport.addListTable(SumMultTable, Title_s);
//
//        xmlexport.addListTable(SumMonMult1Table, Title_s1);
//        xmlexport.addListTable(SumMonMult2Table, Title_s2);
//        xmlexport.addListTable(SumMonMult3Table, Title_s3);
//        xmlexport.addListTable(SumMonMult4Table, Title_s4);
//        xmlexport.addListTable(SumMonMult5Table, Title_s5);
//        xmlexport.addListTable(SumMonMult6Table, Title_s6);
//        xmlexport.addListTable(SumMonMult7Table, Title_s7);
//        xmlexport.addListTable(SumMonMult8Table, Title_s8);
//        xmlexport.addListTable(SumMonMult9Table, Title_s9);
//        xmlexport.addListTable(SumMonMult10Table, Title_s10);
//        xmlexport.addListTable(SumMonMult11Table, Title_s11);
//        xmlexport.addListTable(SumMonMult12Table, Title_s12);

        /*xmlexport.addListTable(RefuseTable, Title_3);
        xmlexport.addListTable(XYTable, Title_4);
        xmlexport.addListTable(TRTable, Title_5);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        */
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }



    public static void main(String[] args) {


//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

    private void jbInit() throws Exception {
    }

}
