/**
 * Copyright (c) 2010 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICardActModifyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LICardActModifySchema;
import com.sinosoft.lis.vschema.LICardActModifySet;

/**
 * <p>
 * Title: 激活卡信息批量修改
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Pangxy
 * @version 1.0
 */

public class ActivationCardModifyBL
{

    // 错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    public static String ErrorState = "";

    /* 私有成员 */

    /** 公共容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mOperate = "";

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 激活卡信息修改类 */
    private ActivationCardModify mActivationCardModify = new ActivationCardModify();

    /**
     * 构造方法
     */
    public ActivationCardModifyBL()
    {
    }

    /**
     * 提交单条数据更新
     * 
     * @param String(网站提交修改日期)
     * @return 布尔值(更新成功返回true,更新失败返回false)
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 提交单条数据更新
     * 
     * @return 布尔值(更新成功返回true,更新失败返回false)
     */
    private boolean submitSingleData(VData cInputData, String cOperate)
    {
        if (!mActivationCardModify.submitData(cInputData, cOperate))
        {
            return false;
        }
        return true;
    }

    /**
     * 得到需要处理的对象
     * 
     * @return 如果出错，返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
//        mActivationCardModify.mDebugFlg = true;  //Debug
        mActivationCardModify.debugLog("----------数据取得处理开始----------");
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mOperate = cOperate;
        mActivationCardModify.mDebugFlg=true;
        return true;
    }

    private boolean checkData()
    {
        mActivationCardModify.debugLog("----------传入参数校验处理开始----------");
        String tWebModifyDate = (String) mTransferData.getValueByName("WebModifyDate");
        if(tWebModifyDate == null || "".equals(tWebModifyDate))
        {
            String tStrErr = "提交日期为空";
            buildError("dealData", tStrErr);
            System.out.println(tStrErr);
            return false;
        }
        if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator))
        {
            System.out.println("操作员名称为空,设置为默认值[NoName]");
            mGlobalInput.Operator = "NoName";
        }
        else
        {
            if (mGlobalInput.Operator.length() > 10)
            {
                String tStrErr = "操作员名称过长";
                buildError("dealData", tStrErr);
                System.out.println(tStrErr);
                return false;
            }
        }
        return true;
    }

    private boolean dealData()
    {
        mActivationCardModify.debugLog("----------逻辑处理开始----------");
        String tWebModifyDate = (String) mTransferData
                .getValueByName("WebModifyDate");
        System.out.println("\n\n" + "***************信息提交日期为" + tWebModifyDate
                + "的激活卡信息批量修改处理开始***************");
        String tExcSQL = "select * from licardactmodify where webmodifydate = '"
                + tWebModifyDate + "' and dealflag <> '01'";
        LICardActModifyDB dbLICardActModifyDB = new LICardActModifyDB();
        LICardActModifySet tLICardActModifySet = null;
        tLICardActModifySet = dbLICardActModifyDB.executeQuery(tExcSQL);
        if (tLICardActModifySet == null || tLICardActModifySet.size() == 0)
        {
            String tStrErr = "没有需要处理的数据";
            buildError("dealData", tStrErr);
            System.out.println("没有日期为" + tWebModifyDate + "的待处理数据");
            return false;
        }
        int cSuccessCount = 0;
        int cErrorCount = 0;
        int cTotalCount = tLICardActModifySet.size();
        for (int cIdx = 1; cIdx <= cTotalCount; cIdx++)
        {
            mActivationCardModify.debugLog("\n\n" + "----------开始处理第" + cIdx
                    + "条记录----------"); //Debug
            TransferData tTransferData = new TransferData();
            LICardActModifySchema tLICardActModifySchema = tLICardActModifySet
                    .get(cIdx);
            String tSequenceNo = tLICardActModifySchema.getSequenceNo();
            String tCardNo = tLICardActModifySchema.getCardNo();
            mActivationCardModify.debugLog("流水号:" + tSequenceNo); //Debug
            mActivationCardModify.debugLog("卡号:" + tCardNo); //Debug
            tTransferData.setNameAndValue("SequenceNo", tSequenceNo);
            tTransferData.setNameAndValue("CardNo", tCardNo);
            tTransferData.setNameAndValue("WebModifyDate", tWebModifyDate);
            mInputData = new VData();
            mInputData.add(mGlobalInput);
            mInputData.add(tTransferData);
            if (!submitSingleData(mInputData, mOperate))
            {
                System.out
                        .println("***************处理失败,批量信息修改处理终止***************"); //Debug
                System.out.println("已处理记录" + --cIdx + "条(正常:" + cSuccessCount
                        + "条;异常:" + cErrorCount + "条)");
                return false;
            }
            if (!mActivationCardModify.mCheckFlg)
            { //Debug
                mActivationCardModify.debugLog("卡号:" + tCardNo
                        + " 的信息处理完毕,但是有错误产生"); //Debug
                cErrorCount++;
            }
            else
            { //Debug
                mActivationCardModify.debugLog("卡号:" + tCardNo + " 的信息成功处理完毕"); //Debug
                cSuccessCount++;
            } //Debug
        }
        System.out.println("***************信息提交日期为" + tWebModifyDate
                + "的全部数据处理完毕***************");
        System.out.println("共处理记录" + cTotalCount + "条(正常:" + cSuccessCount
                + "条;异常:" + cErrorCount + "条)");
        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ActivationCardModifyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        TransferData tTransferData = new TransferData();
        ActivationCardModifyBL tActivationCardModifyBL = new ActivationCardModifyBL();
        VData tVData = new VData();
        String tWebModifyDate="";
        String tOperator="";
        if(args.length>0){
            tWebModifyDate = args[0];
        }
        if(args.length>1)
        {
            tOperator = args[1];
        }
        tTransferData.setNameAndValue("WebModifyDate", tWebModifyDate);
        tGlobalInput.Operator = tOperator;
        tVData.add(tGlobalInput);
        tVData.add(tTransferData);
        tActivationCardModifyBL.submitData(tVData, "Start");
    }
}