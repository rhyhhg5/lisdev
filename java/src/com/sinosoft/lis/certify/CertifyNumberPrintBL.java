package com.sinosoft.lis.certify;

import com.sinosoft.lis.operfee.GrpDueFeeUI;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.CErrors;
import java.sql.ResultSet;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.lis.schema.LZCardNumberSchema;
import com.sinosoft.lis.db.LZCardNumberDB;
import java.util.Random;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CertifyNumberPrintBL {
    /**错误的容器*/
    public CErrors mErrors = new CErrors(); //错误容器
    private TransferData mTransferData = new TransferData(); //接收数据
    private LZCardNumberSet mLZCardNumberSet = new LZCardNumberSet(); //存卡号密码
    private XmlExport xmlexport = null; //生成xml
    private VData mResult = null; //返回结果
    private SSRS mSSRS; //存储数据
    private GlobalInput mGI = new GlobalInput(); //存储业务信息
    private String CurrentDate = PubFun.getCurrentDate(); //当前时间
    private String mOperateType = ""; //单证业务类型：0卡单业务，1撕单业务,2激活卡业务,3pos机出单
    private String mCardNo = ""; //单证卡号
    private String mCardType = ""; //单证类型
    private String mCardSerNo = ""; //单证起始流水号
    private String mCardNum = ""; //单证数量
    private String mPrtNo = ""; //单证数量
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();


    public CertifyNumberPrintBL() {
    }

    public static void main(String[] args) {
        GrpDueFeeUI GrpDueFeeUI1 = new GrpDueFeeUI();
        // GlobalInput tGI = new GlobalInput();
        // tGI.ComCode="86";
        //tGI.Operator="wuser";
        //tGI.ManageCom="86";
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CardType", "61");
        tTransferData.setNameAndValue("PrtNo", "20061129");

        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGI);
        CertifyNumberPrintBL tCertifyNumberPrintBL = new CertifyNumberPrintBL();
        tCertifyNumberPrintBL.submitData(tVData, "INSERT");

    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        //获取打印所需数据
        /*if (!prepareData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {

            this.mErrors.addOneError("PubSubmit:处理数据库失败!");
            return false;
        }*/
        return true;

    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGI = (GlobalInput) cInputData.
                  getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mTransferData == null || mGI == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CertifyNumberPrintBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的数据，请您确认!";
                this.mErrors.addOneError(tError);

                return false;
            }
            mCardType = (String) mTransferData.getValueByName("CardType");
            mPrtNo = (String) mTransferData.getValueByName("PrtNo");

        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    /**
     * 查询列表显示数据
     * @param 无
     * @return String
     */

    private boolean getListData() {

        StringBuffer sqlSB = new StringBuffer();
        String strSql  = "select  OperateType from lzcardnumber where CardType='"
                      + mCardType + "' and PrtNo='"
                      +mPrtNo  + "' fetch first 1 row only";

        ExeSQL tExeSQL = new ExeSQL();
        mOperateType = tExeSQL.getOneValue(strSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tError = new CError();
           tError.moduleName = "MakeXMLBL";
           tError.functionName = "creatFile";
           tError.errorMessage = "查询lzcardnumber数据出错！";
           this.mErrors.addOneError(tError);
           return false;
       }


        //卡单业务
        if (mOperateType.equals("0")||mOperateType.equals("2")) {
            sqlSB.append("select CardNo,CardPassword2 from lzcardnumber")
                    .append(" where CardType='")
                    .append(mCardType)
                    .append("'")
                    .append(" and OperateType='")
                    .append(mOperateType)
                    .append("' and  PrtNo='")
                    .append(mPrtNo)
                    .append("' order by CardNo ")
                    ;
        }
        //撕单业务
        if (mOperateType.equals("1")||mOperateType.equals("3")) {
            sqlSB.append("select CardNo from lzcardnumber")
                    .append(" where CardType='")
                    .append(mCardType)
                    .append("'")
                    .append(" and OperateType='")
                    .append(mOperateType)
                    .append("' and  PrtNo='")
                    .append(mPrtNo)
                    .append("' order by CardNo ")
                    ;
        }

        String tSQL = sqlSB.toString();
        System.out.println("msql:" + tSQL);

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询lzcardnumber列表数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

//        ResultSet rs = null;
//        rs = execQuery(this.msql);
//
//        mRS = rs;*/

       return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        //卡单业务
        if (mOperateType.equals("0")||mOperateType.equals("2")) {
            xmlexport.createDocument("CertifyNumberPrint.vts", "printer"); //最好紧接着就初始化xml文档
        } else {
            xmlexport.createDocument("CertifyNumberSPrint.vts", "printer"); //最好紧接着就初始化xml文档
        }
        String[] title = {"单证号码", "单证密码"};
        xmlexport.addListTable(getListTable(), title);
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {

        int intNum =  mSSRS.getMaxRow()  ;
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= intNum; i++) {
            String[] info = new String[mSSRS.getMaxCol()];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {

                //撕单业务
                info[j - 1] = mSSRS.GetText(i, j);
                System.out.println(j + mSSRS.GetText(i, j));
                //卡单业务
                if (j == 2) {
                    String strP = mSSRS.GetText(i, j);
                    if (strP!=null && strP.length() == 6) {
                        StringBuffer sqlSB = new StringBuffer();
                        sqlSB.append(String.valueOf(strP.charAt(1)))
                                .append(String.valueOf(strP.charAt(3)))
                                .append(String.valueOf(strP.charAt(5)))
                                .append(String.valueOf(strP.charAt(0)))
                                .append(String.valueOf(strP.charAt(2)))
                                .append(String.valueOf(strP.charAt(4)))
                                ;
                        strP = sqlSB.toString();
                    }
                    info[j - 1] = strP;
                }
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;

    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

    // 为更新数据库准备数据
    public boolean prepareData() {
        mInputData.clear();
        mInputData.add(map);
        return true;
    }

    /**
     * 生成随即密码
     * @param pwd_len 生成的密码的总长度
     * @return  密码的字符串
     */
    public static String genRandomNum(int pwd_len) {
        //35是因为数组是从0开始的，26个字母+10个数字
        Thread tThread = new Thread();
        final int maxNum = 100;
        int i; //生成的随机数
        int count = 0; //生成的密码的长度

        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        try {
            tThread.sleep(15);
        } catch (Exception e) {
            // todo auto-generated catch block
            e.getStackTrace();
        }

        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < pwd_len) {
            //生成随机数，取绝对值，防止生成负数，

            i = Math.abs(r.nextInt(maxNum)); //生成的数最大为36-1

            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }

        return pwd.toString();
    }


}
