package com.sinosoft.lis.certify;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.operfee.GrpDueFeeUI;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ECCertifyNumberUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误容器
    private VData mInputData = new VData(); //存储数据
    private VData mResult = new VData(); //返回结果
    private String mOperate; //操作类型

    public ECCertifyNumberUI() {
    }

//操作的提交方法，作为页面数据的入口，准备完数据后执行入库操作。
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        ECCertifyNumberBL tECCertifyNumberBL = new ECCertifyNumberBL();
        System.out.println("---CertifyNumberUI  BEGIN---");
        if (!tECCertifyNumberBL.submitData(cInputData, null)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tECCertifyNumberBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ECCertifyNumberUI";
            tError.functionName = "submitData";
            tError.errorMessage = tECCertifyNumberBL.mErrors.getErrContent();
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        } else {
            mResult = tECCertifyNumberBL.getResult();
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
