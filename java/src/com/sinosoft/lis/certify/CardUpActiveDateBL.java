package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardPrintSchema;
import com.sinosoft.lis.vschema.LZCardPrintSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

//程序名称：CertifyActiveModifyBL.java
//程序功能：
//创建日期：2011-6-14 
//创建人  ：XiePan
//更新记录：  更新人    更新日期     更新原因/内容

public class CardUpActiveDateBL {
	public CErrors mErrors = new CErrors();

	private LZCardPrintSet mLZCardPrintSet = null;

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	public CardUpActiveDateBL() {
	}

	public boolean submitData(VData data) {
		if (!getInputData(data)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		VData tVData = new VData();
		tVData.add(map);
		if (!tPubSubmit.submitData(tVData, "")) {
			System.out.println(tPubSubmit.mErrors.getErrContent());
			mErrors.addOneError("提交数库失败");
			return false;
		}
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		for (int i = 1; i <= mLZCardPrintSet.size(); i++) {
			LZCardPrintSchema mLZCardPrintSchema = new LZCardPrintSchema();
			mLZCardPrintSchema = mLZCardPrintSet.get(i);

			String strSOL = " update lzcardprint set  activedate ='"
					+ mLZCardPrintSchema.getActiveDate() + "',modifydate ='"
					+ CurrentDate + "',modifytime='" + CurrentTime + "' "
					+ " where 1=1 and  prtno='" + mLZCardPrintSchema.getPrtNo()
					+ "'";

			System.out.println("批次号为" + mLZCardPrintSchema.getPrtNo());
			System.out.println("最晚激活日期为" + mLZCardPrintSchema.getActiveDate());

			map.put(strSOL, "UPDATE");
		}
		return true;
	}

	private boolean getInputData(VData data) {
		mLZCardPrintSet = (LZCardPrintSet) data.getObjectByObjectName(
				"LZCardPrintSet", 0);

		if (mLZCardPrintSet == null || mLZCardPrintSet == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BatchPayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		CardUpActiveDateBL bl = new CardUpActiveDateBL();
	}
}
