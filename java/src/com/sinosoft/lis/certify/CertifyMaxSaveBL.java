/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDAgentCardCountSchema;
import com.sinosoft.lis.vschema.LDAgentCardCountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 单证数量配置</p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: Sinosoft</p>
 * @author 张建宝
 * @version 1.0
 */
public class CertifyMaxSaveBL {

	private GlobalInput globalInput = new GlobalInput();

	public CErrors mErrors = new CErrors();//错误处理类

	private VData mInputData;//接收传入数据

	private MMap map = new MMap();

	private String operate;//操作类型

	private LDAgentCardCountSchema mLDAgentCardCountSchema = new LDAgentCardCountSchema(); //代理人最大领取表 

	public CertifyMaxSaveBL() {}

	public boolean submitData(VData cInputData, String cOperate) {
		operate = cOperate;
		mInputData = (VData) cInputData.clone();

		if (!getInputData()) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try
		{
			globalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			mLDAgentCardCountSchema = (LDAgentCardCountSchema) mInputData.getObjectByObjectName("LDAgentCardCountSchema", 0);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		if (operate.equals(SysConst.INSERT)) {
			map.put(mLDAgentCardCountSchema, SysConst.INSERT);
			return true;
		}
		else if (operate.equals(SysConst.UPDATE)) {
			map.put(mLDAgentCardCountSchema, SysConst.UPDATE);
			return true;
		} 
		else if (operate.equals(SysConst.DELETE)) {
			map.put(mLDAgentCardCountSchema, SysConst.DELETE);
			return true;
		}
		else
		{
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "dealData";
			tError.errorMessage = "不支持的操作类型！";
			this.mErrors.addOneError(tError);
			return false;
		}
	}

	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		LDAgentCardCountSchema aLDAgentCardCountSchema = new
		LDAgentCardCountSchema();
		LDAgentCardCountSet aLDAgentCardCountSet = new LDAgentCardCountSet();
		String tOperateType = "QUERY";
		String tUpdateType = "AGE";
		// aLDAgentCardCountSchema.setAgentCode("111000");
		aLDAgentCardCountSchema.setCertifyCode("01");
		aLDAgentCardCountSchema.setMaxCount("23");
		aLDAgentCardCountSet.add(aLDAgentCardCountSchema);
		VData tVData = new VData();
		tVData.addElement("N");
		tVData.addElement("N");
		tVData.addElement("N");
		LDAgentCardCountSet mLDAgentCardCountSet = new LDAgentCardCountSet();//代理人最大发放表
		LDAgentCardCountSchema tLDAgentCardCountSchema = new
		LDAgentCardCountSchema();
		//代理人最大领取表
		tLDAgentCardCountSchema.setAgentCode("Y");
		tLDAgentCardCountSchema.setManageCom("N");
		tLDAgentCardCountSchema.setAgentCom("N");
		tLDAgentCardCountSchema.setAgentGrade("N");
		tLDAgentCardCountSchema.setCertifyCode("ymym");
		tLDAgentCardCountSchema.setMaxCount("10");
		tLDAgentCardCountSchema.setVerPeriod("10");
		mLDAgentCardCountSet.add(tLDAgentCardCountSchema);
		tVData.clear();
		tVData.addElement(tOperateType);
		tVData.addElement(tUpdateType);
		tVData.addElement(mLDAgentCardCountSet);
		tVData.addElement(tOperateType);
		tVData.addElement(tUpdateType);
		tVData.addElement("UPDATE");
		//tVData.addElement(aLDAgentCardCountSet);
		CertifyMaxSaveUI tCertifyMaxSaveUI = new CertifyMaxSaveUI();
		tCertifyMaxSaveUI.submitData(tVData, "UPDATE");
	}
}
