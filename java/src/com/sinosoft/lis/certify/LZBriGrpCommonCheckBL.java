package com.sinosoft.lis.certify;

import java.util.List;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.collections.XmlMsgColls;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LZBriGrpCommonCheckBL
{
    private StringBuffer mErrInfo = null;

    /**
     * 报文头结点数据质检。
     * @param mRoot
     */
    public boolean checkMsgHead(IXmlMsgColls mRoot)
    {
        mErrInfo = null;
        boolean flag = true;

        if (mRoot == null)
        {
            String tStrErr = "获取报文MsgHead节点数据为空，请检查上传数据！";
            errLog(tStrErr);
            flag = false;
        }

        //    	校验报文类型
        String tMsgType = mRoot.getTextByFlag("MsgType");
        if (!isNULL(tMsgType))
        {
            String tStrErr = "上传数据中报文类型为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!checkMsgType(tMsgType))
        {
            String tStrErr = "上传报文类型与约定不符，请核实！";
            errLog(tStrErr);
            flag = false;
        }
        //    	校验批次号
        String tBatchNo = mRoot.getTextByFlag("BatchNo");
        if (!isNULL(tBatchNo))
        {
            String tStrErr = "上传数据中批次号为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!checkBatchNo(tBatchNo))
        {
            String tStrErr = "上传数据中批次号不符合约定格式，请核查！";
            errLog(tStrErr);
            flag = false;
        }
       
        //    	报文发送日期
        String tSendDate = mRoot.getTextByFlag("SendDate");
        if (!isNULL(tSendDate))
        {
            String tStrErr = "上传数据中报文发送日期为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.checkDateForm(tSendDate))
        {
            String tStrErr = "上传数据中报文发送日期格式与约定格式不符，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //    	报文发送时间
        String tSendTime = mRoot.getTextByFlag("SendTime");
        if (!isNULL(tSendTime))
        {
            String tStrErr = "上传数据中报文发送时间为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	报送单位
        String tComCode = mRoot.getTextByFlag("ComCode");
        if (!isNULL(tComCode))
        {
            String tStrErr = "上传数据中报送单位为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	操作员
        String tSendOperator = mRoot.getTextByFlag("SendOperator");
        if (!isNULL(tSendOperator))
        {
            String tStrErr = "上传数据中操作员为空！";
            errLog(tStrErr);
            flag = false;
        }

        return flag;
    }

 

    //	获取节点数量
//    private static int arrLnegth(IXmlMsgColls[] aIXmlMsgColls)
//    {
//        return aIXmlMsgColls.length;
//    }

    //	校验节点值是否为空
    private static boolean isNULL(String aFlagValue)
    {
        if (aFlagValue == null || "".equals(aFlagValue))
        {
            return false;
        }
        return true;
    }

    //	校验报文类型
    private static boolean checkMsgType(String aMsgType)
    {
        if (!"FPLNDZ01".equals(aMsgType))
        {
            return false;
        }
        return true;
    }

    //	校验批次号格式
    private static boolean checkBatchNo(String aBatchNo) {
    	
//		String tPatMsgHeadFormat = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)";
    	String tPatMsgHeadFormat ="FPLNDZ01([1-3])\\d{3}(0?[1-9]{1}|1[0-2])(0?[1-9]{1}|[1-2][0-9]|3[0-1])(([0-1]?[0-9]|2[0-3])[0-5]?[0-9][0-5]?[0-9])\\d{5}";
//    	String tPatMsgHeadFormat ="LISWIIDLGSSys(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)(0?[1-9]{1}|1[0-2])(0?[1-9]{1}|[1-2][0-9]|3[0-1])(([0-1]?[0-9]|2[0-3])[0-5]?[0-9][0-5]?[0-9])\\d{5}";
    	StringBuffer tPatMsgFormat = new StringBuffer();
		tPatMsgFormat.append("^");
		tPatMsgFormat.append(tPatMsgHeadFormat);
		tPatMsgFormat.append("$");

		Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

		Matcher tMatcher = null;

		// 校验消息
		tMatcher = pattern.matcher(aBatchNo);
		if (!tMatcher.matches()) {
			return false;
		}
		return true;
	}

  
    // 校验保单是否处理完成
    private static String getBatchNo(String aPrtNo,String aBatchNo){
    	String tSql = "select batchno from lcbrigrpimportdetail where bussno = '"+aPrtNo+"' and enableflag  != '02' and confstate != '02' and batchno !='"+aBatchNo+"'";//有效未确认的保单
    	ExeSQL tExeSQL = new ExeSQL();
    	String tBatchNo = tExeSQL.getOneValue(tSql);
    	return tBatchNo;
    }
    /**
     * 生成错误信息。
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        System.out.println("数据质检错误：" + cErrInfo);
        if (mErrInfo == null)
        {
            mErrInfo = new StringBuffer();
            mErrInfo.append(cErrInfo);
        }
        else
        {
            mErrInfo.append("&");
            mErrInfo.append(cErrInfo);
        }
    }
    //是否已有签单数据
    private boolean checkSign(String aPrtNo){
    	String tSignSql = "select appflag,signdate from lcgrpcont where prtno = '"+aPrtNo+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
    	if(tSSRS != null && tSSRS.MaxRow == 1){
    		if("1".equals(tSSRS.GetText(1, 1)) && tSSRS.GetText(1, 2) != null && !"".equals(tSSRS.GetText(1, 2))){//已签单
    			return true;
    		}
    	}
    	return false;
    }
    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo){
    	String tSql = "select 1 from ljtempfee where otherno = '"+aPrtNo+"' and enteraccdate is not null and confdate is null ";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }
    /**
     * 校验代理机构是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentCom(String aAgentCom){
    	String tSql = "select 1 from lacom where agentcom = '"+aAgentCom+"' and endflag = 'N' ";
    	String tAgentComFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tAgentComFlag)){
			return false;
    	}
    	return true;
    }
    /**
     * 校验代理人编码是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentCode(String aAgentCode){
    	String tSql = "select 1 from laagent where agentcode = '"+aAgentCode+"' and agentstate <= '02' ";
    	String tAgentCodeFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tAgentCodeFlag)){
			return false;
    	}
    	return true;
    }
    
    /**
     * 校验代理人名称是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentName(String aAgentCode,String aAgentName){
    	String tSql = "select 1 from laagent where agentcode = '"+aAgentCode+"' and name = '"+aAgentName+"' ";
    	String tAgentCodeFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tAgentCodeFlag)){
			return false;
    	}
    	return true;
    }

    public String getErrInfo()
    {
        return mErrInfo.toString();
    }

    public static void main(String[] args)
    {
//        XmlMsgColls xml = new XmlMsgColls();
//        xml.setBodyByFlag("BatchNo", "LISWIIDLGSSys2011072609150000001");
//        xml.setBodyByFlag("SendDate", "2011-7-13");
//        xml.setBodyByFlag("SendTime", "15:45:36");
//
//        XmlMsgColls xml1 = new XmlMsgColls();
//        xml1.setBodyByFlag("BatchNo", "LISWIIDLGSSys2011072609150000002");
//        xml1.setBodyByFlag("SendDate", "2011-7-13");
//        xml1.setBodyByFlag("SendTime", "15:45:36");
//
//        XmlMsgColls root = new XmlMsgColls();
//        root.setBodyByFlag("MsgHead", xml);
//        root.setBodyByFlag("MsgHead", xml1);
//
//        IXmlMsgColls[] ires = root.getBodyByFlag("MsgHead");
//        System.out.println(ires.length);
//        for (int i = 0; i < ires.length; i++)
//        {
//            List list = ires[i].getBodyIdxList();
//            for (int j = 0; j < list.size(); j++)
//            {
//                String tmpTitle = (String) list.get(j);
//                System.out.println(tmpTitle + ":" + ires[i].getTextByFlag(tmpTitle));
//            }
//        }
    	String tBatchNo = "LISWIIDLGSSys2011021609155900001" ;
    	//String tBatchNo = "20110816" ;
    	if(!checkBatchNo(tBatchNo)){
    		System.out.print("失败");
    	}else{
    		System.out.print("成功");
    	}
    }
}
