/**
 * Copyright (c) 2006 sinosoft  Co. Ltd.
 * All right reserved.
 */

/*
 * <p>ClassName: OrderDescUI </p>
 * <p>Description: OrderDescUI类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: Zhang Bin
 * @CreateDate：2006-07-30
 */
package com.sinosoft.lis.certify;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LZOrderCardSchema;
import com.sinosoft.lis.schema.LZOrderSchema;
import com.sinosoft.lis.vschema.LZOrderCardSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZOrderDB;
import com.sinosoft.lis.db.LZOrderCardDB;
import com.sinosoft.lis.db.LZOrderDetailDB;
import com.sinosoft.lis.vschema.LZOrderDetailSet;

public class OrderDescBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String strOperate;
    private LZOrderSchema mLZOrderSchema = new LZOrderSchema();

    private LZOrderCardSet mLZOrderCardSet = new LZOrderCardSet();

    private MMap mMap = new MMap();

    private PubSubmit tPubSubmit = new PubSubmit();


    //业务处理相关变量
    /** 全局数据 */

    public OrderDescBL()
    {
    }

    /**
    * 提交数据处理方法
    * @param cInputData 传入的数据,VData对象
    * @param cOperate 数据操作字符串
    * @return 布尔值（true--提交成功, false--提交失败）
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       System.out.println("Come in BL.Submit..............");
       this.strOperate = cOperate;
       if (strOperate.equals(""))
       {
           buildError("verifyOperate", "不支持的操作字符串");
           return false;
       }
       System.out.println("before getInputData()........");
       if (!getInputData(cInputData))return false;
       System.out.println("after getInputData()........");

       if (!dealData())
       {
           return false;
       }

       return true;
   }

   public static void main(String[] args)
      {
          GlobalInput globalInput = new GlobalInput();
          globalInput.ComCode = "8611";
          globalInput.Operator = "001";

          OrderDescBL tOrderDesBL = new OrderDescBL();

          LZOrderCardSet tLZOrderCardset = new LZOrderCardSet();
          LZOrderCardSchema tLZOrderCardSchema = new LZOrderCardSchema();
          tLZOrderCardSchema.setCertifyCode("HT01/05B");
          tLZOrderCardSchema.setOrderMoney("0.26");

          tLZOrderCardset.add(tLZOrderCardSchema);

          LZOrderSchema tLZOrderSchema = new LZOrderSchema();

          tLZOrderSchema.setSerialNo("23454");
          tLZOrderSchema.setAttachDate("2006-07-01");
          tLZOrderSchema.setDescPerson("zb");
          tLZOrderSchema.setNote("adfad");

          // prepare main plan
          // 准备传输数据 VData
          VData vData = new VData();

          try
          {
              vData.add(tLZOrderSchema);
              vData.add(tLZOrderCardset);
              vData.add(globalInput);
              tOrderDesBL.submitData(vData,"INSERT");
          }
          catch (Exception ex)
          {
              ex.printStackTrace();
          }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        System.out.println("Come to prepareOutputData()...........");
        try
        {
            this.mInputData.clear();
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
            //进行插入数据
           if (this.strOperate.equals("INSERT")) {
               if (!insertData()) {
                   // @@错误处理
                   //buildError("insertData", "增加单证管理员时出现错误!");
                   return false;
               }
           }
       //对数据进行修改操作
           if (this.strOperate.equals("UPDATE")) {
               if (!updateData()) {
                   // @@错误处理
                 buildError("insertData", "单证管理员信息有误!");
                 return false;
               }
           }
           //对数据进行删除操作
           if (this.strOperate.equals("DELETE")) {
               if (!deleteData()) {
                   // @@错误处理
                   buildError("insertData", "删除单证管理员时出现错误!");
                   return false;
               }
           }
           return true;
    }

    /**
     * deleteData
     *
     * @return boolean
     */
    private boolean deleteData() {
        LZOrderDB tLZOrderDB = new LZOrderDB();
        tLZOrderDB.setSerialNo(mLZOrderSchema.getSerialNo());
        if (!tLZOrderDB.getInfo())
        {
            buildError("insertData", "该批次不存在!");
            return false;
        }
        LZOrderSchema tLZOrderSchema = tLZOrderDB.getSchema();
        tLZOrderSchema.setSerialNo(mLZOrderSchema.getSerialNo());
        mMap.put(tLZOrderSchema,"DELETE");

        LZOrderCardDB tLZOrderCardDB = new LZOrderCardDB();
        tLZOrderCardDB.setSerialNo(mLZOrderSchema.getSerialNo());
        LZOrderCardSet tLZOrderCardSet = new LZOrderCardSet();
        tLZOrderCardSet = tLZOrderCardDB.query();
        mMap.put(tLZOrderCardSet,"DELETE");

        LZOrderDetailDB tLZOrderDetailDB = new LZOrderDetailDB();
        tLZOrderDetailDB.setSerialNo(mLZOrderSchema.getSerialNo());
        LZOrderDetailSet tLZOrderDetailSet = new LZOrderDetailSet();
        tLZOrderDetailSet = tLZOrderDetailDB.query();
        mMap.put(tLZOrderDetailSet,"DELETE");

        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改注释时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        LZOrderDB tLZOrderDB = new LZOrderDB();
        tLZOrderDB.setSerialNo(mLZOrderSchema.getSerialNo());
        if (!tLZOrderDB.getInfo())
        {
            buildError("insertData", "该批次不存在!");
            return false;
        }

        LZOrderSchema tLZOrderSchema = tLZOrderDB.getSchema();
        tLZOrderSchema.setNote(mLZOrderSchema.getNote());

        mMap.put(tLZOrderSchema,"UPDATE");
        if (!prepareOutputData())
        {
            return false;
        }
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                buildError("updateData", "修改注释时出现错误!");
                return false;
            }
        }
        System.out.println("after PubSubmit ............");
        mMap = null;
        tPubSubmit = null;
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        LZOrderDB tLZOrderDB = new LZOrderDB();
        tLZOrderDB.setSerialNo(mLZOrderSchema.getSerialNo());
        System.out.println("come in insert........");
        if (tLZOrderDB.getInfo())
        {
            buildError("insertData", "该批次已经存在!");
            return false;
        }

        PubFun tPubFun = new PubFun();
        String currentDate = tPubFun.getCurrentDate();
        String currentTime = tPubFun.getCurrentTime();

        mLZOrderSchema.setSerialNo(createWorkNo(mLZOrderSchema.getAttachDate()));

        mLZOrderSchema.setState("0");
        mLZOrderSchema.setDescCom(globalInput.ComCode);

        mLZOrderSchema.setOperator(globalInput.Operator);
        mLZOrderSchema.setMakeDate(currentDate);
        mLZOrderSchema.setMakeTime(currentTime);
        mLZOrderSchema.setModifyDate(currentDate);
        mLZOrderSchema.setModifyTime(currentTime);

        for (int i=0;i<mLZOrderCardSet.size();i++)
        {
            LZOrderCardSchema tLZOrderCardSchema = new LZOrderCardSchema();
            tLZOrderCardSchema = mLZOrderCardSet.get(i + 1);

            tLZOrderCardSchema.setSerialNo(mLZOrderSchema.getSerialNo());
            tLZOrderCardSchema.setDescCom(globalInput.ComCode);
            tLZOrderCardSchema.setDescPerson(mLZOrderSchema.getDescPerson());

            tLZOrderCardSchema.setOperator(globalInput.Operator);
            tLZOrderCardSchema.setMakeDate(currentDate);
            tLZOrderCardSchema.setMakeTime(currentTime);
            tLZOrderCardSchema.setModifyDate(currentDate);
            tLZOrderCardSchema.setModifyTime(currentTime);
        }

        mMap.put(mLZOrderSchema,"INSERT");
        mMap.put(mLZOrderCardSet,"INSERT");

        if (!prepareOutputData())
       {
           return false;
       }
       if (!tPubSubmit.submitData(this.mInputData, ""))
       {
           if (tPubSubmit.mErrors.needDealError()) {
               // @@错误处理
               buildError("insertData", "添加批次信息时出现错误!");
               return false;
           }
       }
       System.out.println("after PubSubmit ............");
       mMap = null;
       tPubSubmit = null;

        return true;
    }

    public String createWorkNo(String tDate)
    {
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7);
        String tWorkNo = tDate + PubFun1.CreateMaxNo(tDate, 3);
        return tWorkNo;
    }



    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        this.mLZOrderSchema.setSchema((LZOrderSchema) cInputData.
                                    getObjectByObjectName("LZOrderSchema", 0));
        mLZOrderCardSet.set((LZOrderCardSet) cInputData.getObjectByObjectName("LZOrderCardSet",0));
        return true;
    }

    public String getResult()
    {
        return mLZOrderSchema.getSerialNo();
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "OrderDescBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
