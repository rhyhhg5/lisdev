package com.sinosoft.lis.get;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author TJJ
 * @version 1.0
 */
public class ApplyGetAccumulateBL  {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 数据操作字符串 */
  private Reflections mReflections = new Reflections();
  private String mOperate;
  private String mOperater;
  private String mManageCom;
  private String mPolNo;
  private String mOtherNo;
  private String mInsuAccNo;
  private double mCurrentMoney = 0;
  private double mValiCurrentMoney=0;
  private double mApplyMoney=0;
  private String mBalanceDate;
  private String mRateType="D";
  private String mIntervalType="D";
  private String CurrentDate=PubFun.getCurrentDate();
  private String CurrentTime=PubFun.getCurrentTime();
  private String mLimit ;
  private String mActuGetNo ;//产生实付号码
  private String mGetNoticeNo;//产生即付通知书号

  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  /** 业务处理相关变量 */
  private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
  private LCPolSchema mLCPolSchema  = new LCPolSchema ();
  private LCInsureAccSchema mLCInsureAccSchema = new LCInsureAccSchema();
  private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
  private LJAGetSet mLJAGetSet = new LJAGetSet();
  private LCInsureAccTraceSchema mLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
  private LCInsureAccTraceSchema mLXLCInsureAccTraceSchema = new LCInsureAccTraceSchema();

  private LJABonusGetSchema mLJABonusGetSchema = new LJABonusGetSchema();
  private String mDrawer = "";
  private String mDrawerID = "";
  private VData mResult = new VData();
  public ApplyGetAccumulateBL() {
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate =cOperate;
	//得到外部传入的数据,将数据备份到本类中
	if (!getInputData(cInputData))
	  return false;
    if (!checkData())
	   return false;

	//进行业务处理
	if (!dealData())
	{
	  return false;
	}
    if(mOperate.equals("INSERT") )
	{
	if (!prepareOutputData())
		return false;

     //公共提交
	 PubSubmit tPubSubmit =new PubSubmit();
	 if(tPubSubmit.submitData(mResult,"INSERT")==false)
		 return false;

     mResult.clear() ;
     mResult.add(mActuGetNo);
	}


	 return true;
  }



  public String getActuGetNo()
  {
	return this.mActuGetNo;
  }




  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
	if(mOperate.equals("INSERT") )
	{
	  if(!PrepareCurrentMoney())
	      return false;
      if(!PrepareValiCurrentMoney())
	       return false;
      if(mValiCurrentMoney >= mApplyMoney)
      {
	      if(!prepareGet())
	       return false;
	      return true;
      }
      else
      {
		CError tError = new CError();
		tError.moduleName = "PRnewManualDunBL";
		tError.functionName = "getInputData";
		tError.errorMessage = "申请金额大于可领取金额，请确认申请金额!";
		this.mErrors .addOneError(tError) ;
		return false;
      }
	}
	if(mOperate.equals("CalCurrentMoney") )
	{
	  if(!getCurrentMoney())
	      return false;
	   return true ;
	}
	if(mOperate.equals("CalValiCurrentMoney") )
	{
	  if(!getValiCurrentMoney())
	      return false;
	   return true ;
	}
	else
	{
	  CError tError = new CError();
	  tError.moduleName = "PRnewManualDunBL";
	  tError.functionName = "getInputData";
	  tError.errorMessage = "前台传输操作类型出错!";
	  this.mErrors .addOneError(tError) ;
	  return false;
	}
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	//从输入数据中得到所有对象
	//获得全局公共数据
	 mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
	 mLCInsureAccSchema= (LCInsureAccSchema)cInputData.getObjectByObjectName("LCInsureAccSchema",0);

	 mInputData = cInputData ;
	 if ( mGlobalInput == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 //获得操作员编码
	 mOperater = mGlobalInput.Operator;
	 if ( mOperater == null || mOperater.trim().equals("") )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据Operater失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 //获得登陆机构编码
	 mManageCom = mGlobalInput.ManageCom;
	 if ( mManageCom == null || mManageCom.trim().equals("") )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 //获得业务数据
	 if ( mLCInsureAccSchema == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输业务数据失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 mPolNo = mLCInsureAccSchema.getPolNo() ;
	 if ( mPolNo == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输业务数据PrtNo失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 mInsuAccNo = mLCInsureAccSchema.getInsuAccNo() ;
	 if ( mInsuAccNo == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输业务数据InsuAccNo失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }


//	 mOtherNo = mLCInsureAccSchema.getOtherNo() ;
	 if ( mOtherNo == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "PRnewManualDunBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输业务数据OtherNo失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }
	 TransferData mTransferData= new TransferData();
	 mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
	 //获得业务数据
	  if ( mTransferData == null  )
	  {
		// @@错误处理
		//this.mErrors.copyAllErrors( tLCPolDB.mErrors );
		CError tError = new CError();
		tError.moduleName = "PRnewPrintAutoHealthAfterInitService";
		tError.functionName = "getInputData";
		tError.errorMessage = "前台传输业务数据失败!";
		this.mErrors .addOneError(tError) ;
		return false;
	  }

	  if(mOperate.equals("INSERT") )
	  {
		String ApplyMoney = (String)mTransferData.getValueByName("ApplyMoney");
		if ( ApplyMoney == null || ApplyMoney.trim().equals("") )
		{
		  // @@错误处理
		  //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
		  CError tError = new CError();
		  tError.moduleName = "PRnewPrintAutoHealthAfterInitService";
		  tError.functionName = "getInputData";
		  tError.errorMessage = "前台传输业务数据中ApplyMoney失败!";
		  this.mErrors .addOneError(tError) ;
		  return false;
		}
		mApplyMoney = Double.parseDouble(ApplyMoney) ;//借用该字段存储了前台的申请领取的金额
	  }
	 return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
	mResult.clear();
	MMap map = new MMap();


   //添加应付总表数据
   if(mLJAGetSchema != null && mLJAGetSchema.getGetNoticeNo()!= null && !mLJAGetSchema.getGetNoticeNo().trim().equals("") )
      map.put(mLJAGetSchema, "INSERT");

   //添加应付分类表数据
   if(mLJABonusGetSchema != null && mLJABonusGetSchema.getGetNoticeNo()!= null)
   {
	 map.put(mLJABonusGetSchema, "INSERT");
   }

   //添加账户表数据
   if(mLCInsureAccSchema != null && mLCInsureAccSchema.getPolNo()!= null && !mLCInsureAccSchema.getPolNo().trim().equals("") )
		map.put(mLCInsureAccSchema, "UPDATE");


   //添加账户轨迹表数据
   if(mLCInsureAccTraceSchema != null && mLCInsureAccTraceSchema.getPolNo()!= null && !mLCInsureAccTraceSchema.getPolNo().trim().equals("") )
		map.put(mLCInsureAccTraceSchema, "INSERT");

   //添加账户轨迹表利息数据
   if(mLXLCInsureAccTraceSchema != null && mLXLCInsureAccTraceSchema.getPolNo()!= null && !mLXLCInsureAccTraceSchema.getPolNo().trim().equals("") )
		map.put(mLXLCInsureAccTraceSchema, "INSERT");

   mResult.add(map) ;
   return true;
  }


  private boolean checkData()
  {
	//校验保单信息
	LCPolDB tLCPolDB = new LCPolDB();
	tLCPolDB.setPolNo(mPolNo) ;
	if(!tLCPolDB.getInfo())
	{
	  CError tError = new CError();
	  tError.moduleName = "PEdorUWAddAfterInitService";
	  tError.functionName = "checkData";
	  tError.errorMessage = "保单"+mPolNo+"信息查询失败!";
	  this.mErrors .addOneError(tError) ;
	  return false;
	}
	mLCPolSchema.setSchema(tLCPolDB) ;

	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
	tLCInsureAccDB.setPolNo(mPolNo) ;
	tLCInsureAccDB.setInsuAccNo(mInsuAccNo);
//	tLCInsureAccDB.setOtherNo(mOtherNo);
	if(!tLCInsureAccDB.getInfo())
	{
	  CError tError = new CError();
	  tError.moduleName = "PEdorUWAddAfterInitService";
	  tError.functionName = "checkData";
	  tError.errorMessage = "保单"+mPolNo+"信息查询失败!";
	  this.mErrors .addOneError(tError) ;
	  return false;
	}
	mLCInsureAccSchema.setSchema(tLCInsureAccDB) ;

	return true;
  }

  public String getLJAGetSet()
  {
	   if (mLJAGetSet == null || mLJAGetSet.size() == 0)
		   return "";
	   String tReturn = "";
	   tReturn = "0|" + mLJAGetSet.size() + "^" + mLJAGetSet.encode();
	   return tReturn;
 }

 public VData getResult()
{
	 return mResult;
 }
  private boolean prepareGet()
  {
	mLimit = PubFun.getNoLimit( mLCPolSchema.getManageCom() );
	mActuGetNo = PubFun1.CreateMaxNo( "ACTUGETNO", mLimit );//产生实付号码
	mGetNoticeNo=PubFun1.CreateMaxNo("GETNOTICENO",mLimit);//产生即付通知书号
	String tSerialNo = PubFun1.CreateMaxNo( "SERIALNO", mLimit );//产生流水号码
	String tLXSerialNo = PubFun1.CreateMaxNo( "SERIALNO", mLimit );//产生流水号码

	String tDrawerID="";
	LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
	tLCAppntIndDB.setPolNo(mLCPolSchema.getPolNo());
	tLCAppntIndDB.setCustomerNo(mLCPolSchema.getAppntNo());
	if(tLCAppntIndDB.getInfo()==false)
	{
	  tDrawerID="";
	}
	else
	{
	  tDrawerID = tLCAppntIndDB.getIDNo();
	}

	//实付数据-总表
	mLJAGetSchema.setActuGetNo(mActuGetNo);
	mLJAGetSchema.setOtherNo(mLCPolSchema.getPolNo());
	mLJAGetSchema.setOtherNoType("7");
	mLJAGetSchema.setPayMode("1");
	mLJAGetSchema.setAppntNo(mLCPolSchema.getAppntNo());
	mLJAGetSchema.setSumGetMoney(mApplyMoney);
	mLJAGetSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
	mLJAGetSchema.setShouldDate(CurrentDate);
	mLJAGetSchema.setApproveCode(mLCPolSchema.getApproveCode());
	mLJAGetSchema.setApproveDate(mLCPolSchema.getApproveDate());
	mLJAGetSchema.setGetNoticeNo(mGetNoticeNo);
	mLJAGetSchema.setDrawer(mLCPolSchema.getAppntName());
	mLJAGetSchema.setDrawerID(tDrawerID);
	mLJAGetSchema.setSerialNo(tSerialNo);
	mLJAGetSchema.setOperator(mOperater);
	mLJAGetSchema.setMakeDate(CurrentDate);
	mLJAGetSchema.setMakeTime(CurrentTime);
	mLJAGetSchema.setModifyDate(CurrentDate);
	mLJAGetSchema.setModifyTime(CurrentTime);
	mLJAGetSchema.setManageCom(mLCPolSchema.getManageCom());
	mLJAGetSchema.setAgentCom(mLCPolSchema.getAgentCom());
	mLJAGetSchema.setAgentType(mLCPolSchema.getAgentType());
	mLJAGetSchema.setAgentCode(mLCPolSchema.getAgentCode());
	mLJAGetSchema.setAgentGroup(mLCPolSchema.getAgentGroup());

    //mLJAGetSet.add(mLJAGetSchema);
	//实付子表-红利给付实付表
	mLJABonusGetSchema.setActuGetNo(mActuGetNo);
	mLJABonusGetSchema.setOtherNo(mLCPolSchema.getPolNo());
	mLJABonusGetSchema.setOtherNoType("6");
	mLJABonusGetSchema.setPayMode("1");
	mLJABonusGetSchema.setGetMoney(mApplyMoney);
	mLJABonusGetSchema.setGetDate(CurrentDate);
	mLJABonusGetSchema.setManageCom(mLCPolSchema.getManageCom());
	mLJABonusGetSchema.setAgentCom(mLCPolSchema.getAgentCom());
	mLJABonusGetSchema.setAgentType(mLCPolSchema.getAgentType());
	mLJABonusGetSchema.setAPPntName(mLCPolSchema.getAppntName());
	mLJABonusGetSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
	mLJABonusGetSchema.setAgentCode(mLCPolSchema.getAgentCode());
	mLJABonusGetSchema.setFeeFinaType("LJTF"); //红利累计退费
	mLJABonusGetSchema.setFeeOperationType("LJTF");
	mLJABonusGetSchema.setBonusYear(CurrentDate.substring(0,4)) ;
	mLJABonusGetSchema.setState("0");
	mLJABonusGetSchema.setSerialNo(tSerialNo);
	mLJABonusGetSchema.setOperator(mOperater);
	mLJABonusGetSchema.setMakeDate(CurrentDate);
	mLJABonusGetSchema.setMakeTime(CurrentTime);
	mLJABonusGetSchema.setModifyDate(CurrentDate);
	mLJABonusGetSchema.setModifyTime(CurrentTime);
	mLJABonusGetSchema.setGetNoticeNo(mGetNoticeNo);

	//准备账户轨迹表信息
	//填充保险帐户表记价履历表
	mLCInsureAccTraceSchema.setSerialNo(tSerialNo);
	//2005.1.20杨明注释掉问题代码mLCInsureAccTraceSchema.setInsuredNo(mLCInsureAccSchema.getInsuredNo());
	mLCInsureAccTraceSchema.setPolNo(mLCInsureAccSchema.getPolNo());
	mLCInsureAccTraceSchema.setMoneyType("GF");
	mLCInsureAccTraceSchema.setRiskCode(mLCInsureAccSchema.getRiskCode());
	mLCInsureAccTraceSchema.setOtherNo(mLCInsureAccSchema.getPolNo());
	mLCInsureAccTraceSchema.setOtherType("1");
	mLCInsureAccTraceSchema.setMoney(-mApplyMoney);
	mLCInsureAccTraceSchema.setContNo(mLCInsureAccSchema.getContNo());
	mLCInsureAccTraceSchema.setGrpPolNo(mLCInsureAccSchema.getGrpPolNo());
	mLCInsureAccTraceSchema.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
/*Lis5.3 upgrade get
	mLCInsureAccTraceSchema.setAppntName(mLCInsureAccSchema.getAppntName());
*/
  mLCInsureAccTraceSchema.setState(mLCInsureAccSchema.getState());
	mLCInsureAccTraceSchema.setManageCom(mLCInsureAccSchema.getManageCom());
	mLCInsureAccTraceSchema.setOperator(mLCInsureAccSchema.getOperator());
	mLCInsureAccTraceSchema.setMakeDate(CurrentDate);
	mLCInsureAccTraceSchema.setMakeTime(CurrentTime);
	mLCInsureAccTraceSchema.setModifyDate(CurrentDate);
	mLCInsureAccTraceSchema.setModifyTime(CurrentTime);


	//准备账户轨迹表信息
	//填充保险帐户表记价履历表一条利息记录
	mLXLCInsureAccTraceSchema.setSerialNo(tLXSerialNo);
	//2005.1.20杨明注释掉问题代码mLXLCInsureAccTraceSchema.setInsuredNo(mLCInsureAccSchema.getInsuredNo());
	mLXLCInsureAccTraceSchema.setPolNo(mLCInsureAccSchema.getPolNo());
	mLXLCInsureAccTraceSchema.setMoneyType("LX");
	mLXLCInsureAccTraceSchema.setRiskCode(mLCInsureAccSchema.getRiskCode());
	mLXLCInsureAccTraceSchema.setOtherNo(mLCInsureAccSchema.getPolNo());
	mLXLCInsureAccTraceSchema.setOtherType("1");
	mLXLCInsureAccTraceSchema.setMoney(mCurrentMoney-mLCInsureAccSchema.getSumPay());
	mLXLCInsureAccTraceSchema.setContNo(mLCInsureAccSchema.getContNo());
	mLXLCInsureAccTraceSchema.setGrpPolNo(mLCInsureAccSchema.getGrpPolNo());
	mLXLCInsureAccTraceSchema.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
/*Lis5.3 upgrade get
	mLXLCInsureAccTraceSchema.setAppntName(mLCInsureAccSchema.getAppntName());
*/
  mLXLCInsureAccTraceSchema.setState(mLCInsureAccSchema.getState());
	mLXLCInsureAccTraceSchema.setManageCom(mLCInsureAccSchema.getManageCom());
	mLXLCInsureAccTraceSchema.setOperator(mLCInsureAccSchema.getOperator());
	mLXLCInsureAccTraceSchema.setMakeDate(CurrentDate);
	mLXLCInsureAccTraceSchema.setMakeTime(CurrentTime);
	mLXLCInsureAccTraceSchema.setModifyDate(CurrentDate);
	mLXLCInsureAccTraceSchema.setModifyTime(CurrentTime);


	//准备账户信息
	mLCInsureAccSchema.setBalaDate(CurrentDate);
	mLCInsureAccSchema.setBalaTime(CurrentTime);
	mLCInsureAccSchema.setInsuAccBala(mCurrentMoney-mApplyMoney) ;
	mLCInsureAccSchema.setInsuAccGetMoney(mValiCurrentMoney-mApplyMoney) ;
	mLCInsureAccSchema.setSumPaym(mApplyMoney+mLCInsureAccSchema.getSumPaym());
	mLCInsureAccSchema.setModifyDate(CurrentDate) ;
	mLCInsureAccSchema.setModifyTime(CurrentTime);
	mLCInsureAccSchema.setOperator(mOperater) ;

	return true ;
  }


  private boolean PrepareCurrentMoney()
  {
    AccountManage mAccountManage = new AccountManage();
	mBalanceDate = PubFun.getCurrentDate();
	mRateType="Y";
    mIntervalType="D";
	mCurrentMoney = mAccountManage.getAccBalance(mInsuAccNo,mPolNo,mBalanceDate,mRateType,mIntervalType) ;
	mCurrentMoney= PubFun.setPrecision(mCurrentMoney,"0.00") ;
	return true;
  }

  private boolean getCurrentMoney()
  {
	AccountManage mAccountManage = new AccountManage();
	mBalanceDate = PubFun.getCurrentDate();
	mRateType="Y";
	mIntervalType="D";
	mCurrentMoney = mAccountManage.getAccBalance(mInsuAccNo,mPolNo,mBalanceDate,mRateType,mIntervalType) ;
	mCurrentMoney= PubFun.setPrecision(mCurrentMoney,"0.00") ;
	mResult.clear() ;
	mResult.add(String.valueOf(mCurrentMoney));
	return true;
  }


  private boolean PrepareValiCurrentMoney()
  {
	if( mCurrentMoney>0 && mLCInsureAccSchema.getFrozenMoney()>0  )
		mValiCurrentMoney= mCurrentMoney-mLCInsureAccSchema.getFrozenMoney() ;
	else
	   mValiCurrentMoney= mCurrentMoney;
     mValiCurrentMoney= PubFun.setPrecision(mValiCurrentMoney,"0.00") ;
   return true;
  }

  private boolean getValiCurrentMoney()
  {
	if(!PrepareCurrentMoney())
	     return false;
	if( mCurrentMoney>0 && mLCInsureAccSchema.getFrozenMoney()>0  )
       mValiCurrentMoney= mCurrentMoney-mLCInsureAccSchema.getFrozenMoney() ;
    else
      mValiCurrentMoney= mCurrentMoney;
    mValiCurrentMoney= PubFun.setPrecision(mValiCurrentMoney,"0.00") ;
	mResult.clear() ;
	mResult.add(String.valueOf(mValiCurrentMoney));
    return true;
  }


  public static void main(String[] args) {

  }
}
