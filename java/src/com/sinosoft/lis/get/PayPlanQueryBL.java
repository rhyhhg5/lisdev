package com.sinosoft.lis.get;

import com.sinosoft.lis.get.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author GYJ
 * @version 1.0
 */
public class PayPlanQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 保单 */
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LJSGetSet  mLJSGetSet = new LJSGetSet();
  private LCPolSet mLCPolSet=new LCPolSet();
  public PayPlanQueryBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
        System.out.println("---getInputData-gyj--");

    //进行业务处理
	if (cOperate.equals("QUERY||MAIN"))
	{
	    if (!queryData())
	      return false;
		System.out.println("---queryData---");
	}

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 保单查询条件
    mLCPolSchema.setSchema((LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0));
    if(mLCPolSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayPlanQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 查询符合条件的保单
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {
  	LCPolDB tLCPolDB=new LCPolDB();
        tLCPolDB.setSchema(this.mLCPolSchema);
        LCPolSet tLCPolSet=new LCPolSet();
        tLCPolSet= tLCPolDB.query();
        this.mLCPolSet=tLCPolSet;
        if (tLCPolDB.mErrors.needDealError() == true)
	{
      // @@错误处理
	  this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ProposalQueryBL";
          tError.functionName = "queryData";
          tError.errorMessage = "保单查询失败!";
          this.mErrors.addOneError(tError);
          mLCPolSet.clear();
          return false;
        }
        if (mLCPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PayPlanQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();
            return false;
	}
        for (int i=1;i<=this.mLCPolSet.size();i++)
        {

          LJSGetDB tLJSGetDB=new LJSGetDB();
          tLJSGetDB.setOtherNo(this.mLCPolSet.get(i).getPolNo());
          tLJSGetDB.setOtherNoType("2");
          System.out.println("=============111===");
          LJSGetSet tLJSGetSet=new LJSGetSet();
          tLJSGetSet=tLJSGetDB.query();
          for (int j=1;j<=tLJSGetSet.size();j++)
          {
             tLJSGetSet.get(j).setAppntNo(this.mLCPolSet.get(i).getInsuredNo());
             tLJSGetSet.get(j).setApproveCode(this.mLCPolSet.get(i).getRiskCode());
             this.mLJSGetSet.add(tLJSGetSet.get(j));

          }
       }
        if (mLJSGetSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PayPlanQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLCPolSet.clear();
            return false;
	}
	mResult.clear();
	mResult.add(this.mLCPolSet);
        mResult.add(this.mLJSGetSet);
      	return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
//    mResult.clear();
//    try
//    {
//      mResult.add();
//      mResult.add( mLCAppntIndSchema );
//     }
//    catch(Exception ex)
//    {
//      // @@错误处理
//      CError tError =new CError();
//      tError.moduleName="ProposalQueryBL";
//      tError.functionName="prepareOutputData";
//      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
    return true;
  }
}
