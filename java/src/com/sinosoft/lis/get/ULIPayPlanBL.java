package com.sinosoft.lis.get;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCGetToAccBL;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.Date;
import com.sinosoft.lis.get.PayPlanBLS;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author TJJ
 * @version 1.0
 */
public class ULIPayPlanBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();   
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往后面传输数据的容器 */
    private VData mResult;
    /** 数据操作字符串 */
    //    private String mOperate;
    /**控制信息传输类*/
    private TransferData mTransferData = new TransferData();
    /**参数描述*/
    private String mTimeEnd, mSerialNo;
    /**给付至日期*/
    private Date mGetToDate;

    /** 业务处理相关变量 */
    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    private LCGetSchema mLCGetSchema = new LCGetSchema();
    private LCGetSet mLCGetSet=new LCGetSet();
    private LJSGetSet mLJSGetSet=new LJSGetSet();
    private  LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
    private LJAGetDrawSet mLJAGetDrawSet=new LJAGetDrawSet();
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LCPolSchema mLCPolSchema=new LCPolSchema();
    private LCInsuredSchema mLCInsuredSchema=new LCInsuredSchema();
    private LMDutyGetAliveSchema mLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    private LMDutyGetSchema mLMDutyGetSchema = new LMDutyGetSchema();
    private String mGrpContno =null;
    private String mInsuredNo = null;
    private String minsuaccno = null;
    private String mPolno = null;
    public MMap tPerMMap = new MMap();
    private String mStartDate = "";
    private double sumMoney = 0;
    public ULIPayPlanBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据    
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        //        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        //进行业务处理
        if (!dealData(cInputData)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PayPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败PayPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareData())
            return false;
        System.out.println("Start PayPlan BLS Submit...");

        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mInputData, ""))
        { 
           System.out.println("被保险人"+mInsuredNo+"要生成的的记录催付失败");
        }
        System.out.println("End PayPlan BLS Submit...");
        //如果有需要处理的错误，则返回
        
        mInputData = null;
        return true;
    }

    /**
     * 初始化查询出给付单子，通过gettodate小于等于当前日期.
     * @param Schema
     * @return
     */
    private String initCondGet() {
        String aSql = "select * from LCGet a where 1 = 1";
        if (mLCGetSchema.getManageCom()==null||mLCGetSchema.getManageCom().trim().equals(""))
          aSql = aSql + " and a.ManageCom like '"+mGlobalInput.ManageCom+"%'";
        else
          aSql = aSql + " and a.ManageCom like '"+mLCGetSchema.getManageCom()+"%'";

        if (mLCGetSchema.getGrpContNo()!=null&&!mLCGetSchema.getGrpContNo().trim().equals(""))
          aSql = aSql + " and GrpContNo= '"+mLCGetSchema.getGrpContNo()+"'";


        if (mLCGetSchema.getPolNo()!=null&&!mLCGetSchema.getPolNo().trim().equals(""))
          aSql = aSql + " and PolNo= '"+mLCGetSchema.getPolNo()+"'";
        /*Lis5.3 upgrade get
        if (mLCGetSchema.getAppntNo()!=null&&!mLCGetSchema.getAppntNo().trim().equals(""))
          aSql = aSql + " and AppntNo= '"+mLCGetSchema.getAppntNo()+"'";
        */
        if (mLCGetSchema.getInsuredNo()!=null&&!mLCGetSchema.getInsuredNo().trim().equals(""))
          aSql = aSql + " and InsuredNo= '"+mLCGetSchema.getInsuredNo()+"'";

        //if (!(mTimeStart== null&&mTimeStart.trim().equals("")))
        // aSql = aSql + " and gettodate>='"+mTransferData.getValueByName("timeStart")+"'";

        if (!(mTimeEnd== null&&mTimeEnd.trim().equals("")))
          aSql = aSql + " and a.grpcontno='00003443000005' and a.gettodate<= date('"+mTransferData.getValueByName("timeEnd")+"')";
          aSql = aSql + " and a.getenddate>=date('"+mTransferData.getValueByName("timeEnd")+"')";
         aSql = aSql + " and a.CanGet = '1' and a.LiveGetType='0' and  a.UrgeGetFlag = 'Y' and exists (select 1 from lcpol c where c.polno=a.polno and  exists (select 1 from  lmriskapp m where m.risktype4='4'and m.Riskprop='G' and m.riskcode=c.riskcode))order by PolNo";
        System.out.println(aSql);
        return aSql;
      }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    public boolean dealData(VData cInputData) {
        boolean tReturn = true;
        String tGetNoticeNo;
        String condSql;
        ExeSQL tExeSQL = new ExeSQL();
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

         // 第一步 初始化催付查询条件 把该次查询出来的给付信息查询出来
        condSql = initCondGet();
        System.out.println(condSql);
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setSchema(mLCGetSchema);

        mLCGetSet = tLCGetDB.executeQuery(condSql);

        int tCount = mLCGetSet.size();

        if (tCount > 0) {
            String aPolNo = null;
            String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", PubFun
                .getNoLimit(mGlobalInput.ManageCom));
            mSerialNo = tSerialNo;
            System.out.println("--------------serialNo:" + mSerialNo);
            System.out.println("---tCount :" + tCount);
            FDate tFDate = new FDate();
            for (int j = 1; j <= tCount; j++) {
                //首先给出三个变量的值，方便后面的调用.
                LCGetSchema mLCGetSchema = mLCGetSet.get(j);
                mGrpContno = mLCGetSchema.getGrpContNo();
                mInsuredNo = mLCGetSchema.getInsuredNo();
                mPolno = mLCGetSchema.getPolNo();
                if (j == 1
						|| !StrTool.cTrim(mLCGetSchema.getPolNo()).equals(
								aPolNo)) {
					aPolNo = mLCGetSchema.getPolNo();
					LCPolDB tLCPolDB = new LCPolDB();
					tLCPolDB.setPolNo(aPolNo);
					if (!tLCPolDB.getInfo()) {
						CError.buildErr(this, "查询保单信息失败！polno："
								+ mLCGetSchema.getPolNo());
						continue;
					}
					tLCPolSchema.setSchema(tLCPolDB.getSchema());
					if (!StrTool.cTrim(tLCPolSchema.getAppFlag()).equals("1")) {
						CError.buildErr(this, "查询保单未生效！polno："
								+ mLCGetSchema.getPolNo());
						continue;
					}
				      minsuaccno=new ExeSQL().getOneValue(" select insuaccno from lmriskinsuacc a where insuaccno in (select insuaccno from lmrisktoacc where riskcode='"+tLCPolSchema.getRiskCode()+"') and insuaccno in  (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')) ");
				      
					this.setPolInfo(tLCPolSchema);//置上lcpol的值
					LCInsuredDB tLCInsuredDB = new LCInsuredDB();
					/* Lis5.3 upgrade set */
					tLCInsuredDB.setContNo(tLCPolSchema.getContNo());
					tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());

					if (!tLCInsuredDB.getInfo()) {
						CError.buildErr(this, "被保人信息不足无法计算!");
						continue;
					}
					tLCInsuredSchema.setSchema(tLCInsuredDB.getSchema());
					this.setInsuredInfo(tLCInsuredSchema);
				}
                //判断上各月必须月结完才能做这个生成年金领取金额以及后续的工作
                String ISBeforeLastAccBala =CommonBL.ISBeforeLastAccBala(mLCPolSchema.getPolNo(),mTimeEnd);
				System.out.println(ISBeforeLastAccBala.substring(0, 1));
		    	if(ISBeforeLastAccBala.substring(0, 1).equals("1"))
		    	{
		    		  System.out.println("批处理啊时间"+mTimeEnd+"不应早于该被保人"+mLCPolSchema.getInsuredName()+"的最后一次账户资金变化日"+ISBeforeLastAccBala.substring(1,ISBeforeLastAccBala.length()));
		    		continue;
		    	}
		    	
		    	if(!CommonBL.ISLastMonthAccBala(mLCPolSchema.getPolNo(), mTimeEnd))
		    	{
		    		System.out.println("被保人" +mLCPolSchema.getInsuredName()  + "保全生效日 "
		    				 +mTimeEnd
								+ "的上个月还未月结，请先进行月结再申请该保全");
		    			continue;
		    	}
               // 计算应领至日期
                if (mLCGetSchema.getGetStartDate() == null
                        || mLCGetSchema.getGetStartDate().equals("")) {
                    mLCGetSchema.setGetStartDate(mLCGetSchema.getGettoDate());
                }
                if (mLCGetSchema.getGetEndDate() == null
                        || mLCGetSchema.getGetEndDate().equals("")) {
                    mLCGetSchema.setGetEndDate(mLCGetSchema.getGettoDate());
                }
                if (mLCGetSchema.getGettoDate() != null
                        && !mLCGetSchema.getGettoDate().equals("")) {
                    //可能不是第一次催付或者不是年金转换后的保单，需要判断保单生效日期
                    String CValiDate = tExeSQL.getOneValue("select cvalidate "
                            + " from lcgrpcont where grpcontno='"
                            + mLCGetSchema.getGrpContNo() + "'");
                    if (mLCGetSchema.getGettoDate().compareTo(CValiDate) < 0) {
                        //起领时间在保单生效时间之前，按照保单生效时间催付
                        mLCGetSchema.setGettoDate(CValiDate);
                    }
                }

                if (mLCGetSchema.getGettoDate() == null
                        || mLCGetSchema.getGettoDate().equals("")) {
                    //做过年金转换后的第一次催付，需要判断保单生效日期
                    String CValiDate = tExeSQL.getOneValue("select cvalidate "
                            + " from lcgrpcont where grpcontno='"
                            + mLCGetSchema.getGrpContNo() + "'");
                    if (mLCGetSchema.getGetStartDate().compareTo(CValiDate) < 0) {
                        //起领时间在保单生效时间之前，按照保单生效时间催付
                        mLCGetSchema.setGettoDate(CValiDate);
                    } else {
                        mLCGetSchema.setGettoDate(mLCGetSchema
                            .getGetStartDate());
                    }
                }

                Date GetToDate = this.calGetToDate(mLCGetSchema);
                //                if (GetToDate.compareTo(tFDate.getDate((String) mTransferData
                //                    .getValueByName("timeEnd"))) > 0) {
                //                    //该人的催至日期已经超过录入日期，不处理；
                //                    continue;
                //                }
                if (GetToDate == null) {
                    //该人已经超过起领日期
                    continue;
                }
                    this.setGetToDate(GetToDate);
                    String tActuGetNo = PubFun1.CreateMaxNo("GETNO", PubFun
                        .getNoLimit(mLCGetSet.get(j).getManageCom())); //参数为机构代码
                    LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
                    LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();

                    tLJAGetDrawDB.setPolNo(mLCGetSet.get(j).getPolNo());
                    tLJAGetDrawDB.setDutyCode(mLCGetSet.get(j).getDutyCode());
                    tLJAGetDrawDB.setGetDutyCode(mLCGetSet.get(j)
                        .getGetDutyCode());
                    tLJAGetDrawDB.setGetDutyKind(mLCGetSet.get(j)
                        .getGetDutyKind());
                    tLJAGetDrawSet.clear();
                    tLJAGetDrawSet = tLJAGetDrawDB.query();
                    //校验各种限制条件同时准备数据
                    if (!this.checkDataToGet(mLCGetSet.get(j)))
                        continue;

                    tLJAGetDrawSchema = new LJAGetDrawSchema();
                    //生成给付子表的信息
                    //计算给付至日期
                    //计算应给付金额

                    Date aGetDate = this.getGetToDate();
                    System.out.println("-----GetDate:" + aGetDate);
                    //生成通知
                    tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", PubFun
                        .getNoLimit(tLCPolSchema.getManageCom()));
                    System.out.println("========crt max NO======"
                            + tGetNoticeNo);
                    tLJAGetDrawSchema.setActuGetNo(tActuGetNo);
                    tLJAGetDrawSchema.setGetNoticeNo(tGetNoticeNo);
                    tLJAGetDrawSchema.setSerialNo(tSerialNo);
                    tLJAGetDrawSchema.setGetDate(mLCGetSet.get(j)
                        .getGettoDate());
                    tLJAGetDrawSchema.setDutyCode(mLCGetSet.get(j)
                        .getDutyCode());
                    tLJAGetDrawSchema.setGetDutyCode(mLCGetSet.get(j)
                        .getGetDutyCode());
                    tLJAGetDrawSchema.setGetDutyKind(mLCGetSet.get(j)
                        .getGetDutyKind());
                    tLJAGetDrawSchema.setLastGettoDate(mLCGetSet.get(j)
                        .getGettoDate());

                    tLJAGetDrawSchema.setFeeFinaType("LIVGET");
                    tLJAGetDrawSchema.setFeeOperationType("TF");

                    tLJAGetDrawSchema.setCurGetToDate(aGetDate);

                    //给付渠道
                    tLJAGetDrawSchema.setGetChannelFlag("1");

                    tLJAGetDrawSchema.setAppntNo(tLCPolSchema.getAppntNo());
                    tLJAGetDrawSchema.setPolNo(tLCPolSchema.getPolNo());
                    tLJAGetDrawSchema.setGrpName(tLCPolSchema.getAppntName());
                    tLJAGetDrawSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                    tLJAGetDrawSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                    tLJAGetDrawSchema.setContNo(tLCPolSchema.getContNo());
                    tLJAGetDrawSchema.setPolNo(tLCPolSchema.getPolNo());
                    tLJAGetDrawSchema.setPolType("1");
                    tLJAGetDrawSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                    tLJAGetDrawSchema.setAgentCode(tLCPolSchema.getAgentCode());
                    tLJAGetDrawSchema.setAgentCom(tLCPolSchema.getAgentCom());
                    tLJAGetDrawSchema.setAgentGroup(tLCPolSchema
                        .getAgentGroup());
                    tLJAGetDrawSchema.setAgentType(tLCPolSchema.getAgentType());
                    tLJAGetDrawSchema.setRiskCode(tLCPolSchema.getRiskCode());
                    tLJAGetDrawSchema.setKindCode(tLCPolSchema.getKindCode());
                    tLJAGetDrawSchema.setRiskVersion(tLCPolSchema
                        .getRiskVersion());
                    tLJAGetDrawSchema.setApproveTime(PubFun.getCurrentTime());
                    tLJAGetDrawSchema.setApproveDate(PubFun.getCurrentDate());
                    tLJAGetDrawSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetDrawSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());
                    tLJAGetDrawSchema.setOperator(mGlobalInput.Operator);
                    tLJAGetDrawSchema.setManageCom(tLCPolSchema.getManageCom());
                    tLJAGetSchema = new LJAGetSchema();
                    tLJAGetSchema.setActuGetNo(tActuGetNo);
                    tLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
                    tLJAGetSchema.setSerialNo(tSerialNo);
                    tLJAGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
                    tLJAGetSchema.setOtherNo(tLCPolSchema.getGrpContNo());
                    tLJAGetSchema.setOtherNoType("2");
                    tLJAGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
                    tLJAGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
                    tLJAGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                    tLJAGetSchema.setAgentType(tLCPolSchema.getAgentType());
                    tLJAGetSchema.setBankAccNo(tLCInsuredSchema.getBankAccNo());
                    tLJAGetSchema.setBankCode(tLCInsuredSchema.getBankCode());
                    tLJAGetSchema.setPayMode(mLCGetSchema.getGetMode());
                    tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
                    tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
                    tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                    tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
                    tLJAGetSchema.setOperator(mGlobalInput.Operator);
                    tLJAGetSchema.setManageCom(tLCPolSchema.getManageCom());
                    
                     //保存金额时，判断上一月结息时的金额是否够每次领取的金额，若不够，则该月用保证利率进行结息，把结息后的钱置为最后一次领取的
                     //的金额，且把getenddate不变，那么下次就不会进行年金领取（根据查询条件），若够得话，则按正常的钱取出来
                    double totalMoney =getMoney(tLCPolSchema,mLCGetSchema);
                    LCInsureAccDB tLCInsureaccDB = new LCInsureAccDB();
                    tLCInsureaccDB.setGrpContNo(mGrpContno);
                    tLCInsureaccDB.setInsuAccNo(minsuaccno);//取出个人账户的总金额
                    tLCInsureaccDB.setPolNo(mPolno);
                    tLCInsureAccSet = tLCInsureaccDB.query();
                    if(tLCInsureAccSet.size()==0 ||tLCInsureAccSet.size()>1 ){
                   	 System.out.println("查询出来的账户信息为空 或者大于一条，错误"); 
                    }
                    mStartDate =tLCInsureAccSet.get(1).getBalaDate();
                    System.out.println("上次结息的时间是"+mStartDate);
                    System.out.println("结息之后的金额为"+totalMoney);
                    if(totalMoney>=mLCGetSet.get(j).getStandMoney()){
                    	sumMoney = mLCGetSet.get(j)
                        .getStandMoney();
                    	 tLJAGetSchema.setSumGetMoney(sumMoney);
                         tLJAGetDrawSchema.setGetMoney(sumMoney);
                         mLCGetSchema.setGettoDate(GetToDate);
                    }else{
                    	System.out.println("测试用的");
            	        //结息对这几天进行正常结息，用到保证利率
                    	VData tVData = new VData();
                    	TransferData tf = new TransferData();
                    	System.out.println("传过去的日期为"+mLCGetSchema.getGettoDate());
                    	tf.setNameAndValue("EndDate", mLCGetSchema.getGettoDate());
                    	tVData.add(mLCPolSchema);  	
                    	tVData.add(mGlobalInput);  
                    	tVData.add(tf);
                    	 ULIPromiseInsuAccBala tUliPromiseInsuAccBala = new ULIPromiseInsuAccBala();
                    	 //批处理这边不生成错误，不可能一条错误，导致很多条数据都不执行，这边给它友情提示，如果结算失败，那就看日志吧.
                         if(!tUliPromiseInsuAccBala.submitData(tVData, "",mLCPolSchema.getGrpContNo()))
                         {
                             String errInfo ="个人:" + mLCPolSchema.getInsuredName() + " 结算失败: ";
                             System.out.println(errInfo);
                             continue;
                         }
                        //算出用 保证利率算出之后的金额，并且lcget的payEndDate不变，使下次催付时不在进行出来. 
                         tLCInsureaccDB = new LCInsureAccDB();
                         tLCInsureaccDB.setGrpContNo(mGrpContno);
                         tLCInsureaccDB.setInsuAccNo(minsuaccno);//取出个人账户的总金额
                         tLCInsureaccDB.setPolNo(mPolno);
                         tLCInsureAccSet = tLCInsureaccDB.query();
                         if(tLCInsureAccSet.size()==0 ||tLCInsureAccSet.size()>1 ){
                        	 System.out.println("查询出来的账户信息为空 或者大于一条，错误"); 
                         }
                         double tempMoney  = tLCInsureAccSet.get(1).getInsuAccBala();
                         sumMoney = tempMoney;
                         System.out.println("被保险人"+mLCGetSchema.getInsuredNo()+"结息后的金额为"+sumMoney);
                         tLJAGetSchema.setSumGetMoney(sumMoney);
                         tLJAGetDrawSchema.setGetMoney(sumMoney);
                         //对lcget的standmoney 和actulMoney 进行更新，把最后的金额赋值给这两个字段。
                         mLCGetSchema.setActuGet(sumMoney);
                         mLCGetSchema.setStandMoney(sumMoney);
                         mLCGetSchema.setGetEndDate(mLCGetSchema.getGettoDate());//注意如果是最后一次的话GetEndDate的值不变.
                         mLCGetSchema.setGettoDate(GetToDate);
                         //对于最后一次用保证利率算出来的金额和之前取过的金额总和为lcget的summoney();
                         System.out.println("账户之前还未领取之前的总金额"+mLCGetSchema.getSumMoney()+"本次结息后的金额为"+tempMoney+"所以本次总金额为"+mLCGetSchema.getSumMoney()+tempMoney);
                      //针对最后一次的金额，则要置保单的状态为满期状态3                         
                         updatecontstate(tLCPolSchema); 
                    }//保存领取的总金额
                    mLCGetSchema.setSumMoney(mLCGetSchema.getSumMoney()+sumMoney);
                    mLCGetSchema.setMakeDate(PubFun.getCurrentDate());
                    mLCGetSchema.setMakeTime(PubFun.getCurrentTime());
                    mLCGetSchema.setModifyDate(PubFun.getCurrentDate());
                    mLCGetSchema.setModifyTime(PubFun.getCurrentTime());
                    mLCGetSchema.setOperator(mGlobalInput.Operator);
                   /* if (GetToDate.compareTo(tEndDate) <= 0) {
                    	//这边应该只能执行一次，不能多次循环
                        while (GetToDate.compareTo(tEndDate) <= 0) {
                            //领至日期小于起领日期，按照日期进行循环处理
                            tLJAGetSchema.setSumGetMoney(tLJAGetDrawSchema
                                .getGetMoney()
                                    + mLCGetSet.get(j).getStandMoney());
                            tLJAGetDrawSchema.setGetMoney(tLJAGetDrawSchema
                                .getGetMoney()
                                    + mLCGetSet.get(j).getStandMoney());
                            mLCGetSchema.setGettoDate(GetToDate);
                            GetToDate = this.calGetToDate(mLCGetSchema);
                        }
                    } else {
                        tLJAGetSchema.setSumGetMoney(mLCGetSet.get(j)
                            .getStandMoney());
                        tLJAGetDrawSchema.setGetMoney(mLCGetSet.get(j)
                            .getStandMoney());
                    }*/
                    tLJAGetDrawSchema.setCurGetToDate(GetToDate);
                    if (tLJAGetSchema.getPayMode() == null
                            || tLJAGetSchema.getPayMode().equals("")
                            || !tLJAGetSchema.getPayMode().equals("6")
                            || !tLJAGetSchema.getPayMode().equals("5")) {
                        tLJAGetSchema.setPayMode("4");
                        tLJAGetSchema.setBankCode(tLJAGetSchema.getManageCom());
                        tLJAGetSchema.setBankAccNo("0000000000");
                    }
                    //生成账户领取信息
                    //更新几个账户的信息.
                    creatInsuretrace(tLCInsureAccSet.get(1));
                    tPerMMap.add(updateAccInfo(tLCInsureAccSet.get(1)));
                    //更新LCget信息，主要更新金额，paytodate payEndDate,
                    mLJAGetDrawSet.add(tLJAGetDrawSchema);
                   
                    mLJAGetSet.add(tLJAGetSchema);
                    tPerMMap.put(mLJAGetDrawSet, SysConst.INSERT);
                    tPerMMap.put(mLJAGetSet,SysConst.INSERT);
                    mLCGetSchema.setSumMoney(mLCGetSchema.getSumMoney()
                            + tLJAGetSchema.getSumGetMoney());
                    mLCGetSchema.setGettoDate(tLJAGetDrawSchema
                        .getCurGetToDate());
                    mLCGetSet.add(mLCGetSchema);
                    tPerMMap.put(mLCGetSet,SysConst.UPDATE);
                    
            }
        } else {
            CError.buildErr(this,"没有符合条件的催付数据!") ;
            return false;
        }
        return tReturn;
    }
    //针对最后一次领取，则要置保单状态为3
    private void updatecontstate(LCPolSchema lcpol){
    	tPerMMap.put("  update lccont "
                + "set stateflag = '3'"
                + " where  grpcontno = '" + lcpol.getGrpContNo()
                + "'   and insuredno = '" + lcpol.getInsuredNo()
                + "'   and contNo = '" + lcpol.getContNo() + "' ",
                "UPDATE");
    	tPerMMap.put("  update lcpol "
                + "set stateflag = '3'"
                + " where  grpcontno = '" + lcpol.getGrpContNo()
                + "'   and insuredno = '" + lcpol.getInsuredNo()
                + "'   and contNo = '" + lcpol.getContNo() + "' ",
                "UPDATE");
 	   
 }
    /**
     * updateAccInfo
     * 更新总账户信息
     * @return MMap
     */
    private MMap updateAccInfo(LCInsureAccSchema aLCInsureAccSchema)
    {
        MMap tMMap = new MMap();

        String sql = "update LCInsureAccClass a "
                     + "set (InsuAccBala, LastAccBala) "
                     + "   =(select sum(Money), sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo) "
                     + "where PolNo = '"
                     + aLCInsureAccSchema.getPolNo() + "' "
                     + "   and InsuAccNo = '"
                     + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        //更新账户信息
        sql = "update LCInsureAcc a "
              + "set (InsuAccBala, LastAccBala, BalaDate, BalaTime, Operator, ModifyDate, ModifyTime) "
              + " =(select sum(InsuAccBala), sum(LastAccBala), min(BalaDate), min(BalaTime), Operator, ModifyDate, ModifyTime " +
              " from LCInsureAccClass where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo "
              + " group by Operator, ModifyDate, ModifyTime) "
              + "where PolNo = '"
              + aLCInsureAccSchema.getPolNo() + "' "
              + "   and InsuAccNo = '"
              + aLCInsureAccSchema.getInsuAccNo() + "' ";
        tMMap.put(sql, SysConst.UPDATE);
        
        return tMMap;
    }  
    private void creatInsuretrace(LCInsureAccSchema tLCInsureAccSchema){
    	//通过账户查询出子账户的信息
    	 LCInsureAccClassDB tInsureAccClassDB = new LCInsureAccClassDB();
    	   LCInsureAccClassSchema tlcAccClassSchema = new LCInsureAccClassSchema();
    	 LCInsureAccClassSet tlcinsureAccClassSet = new LCInsureAccClassSet();
    	 tInsureAccClassDB.setGrpContNo(tLCInsureAccSchema.getGrpContNo());
    	 tInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
    	 tInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
    	 tInsureAccClassDB.setInsuredNo(tLCInsureAccSchema.getInsuredNo());
    	 tlcinsureAccClassSet = tInsureAccClassDB.query();
    	 if(tlcinsureAccClassSet.size()==0){
    		 System.out.println("查询出来的子账户为空");
    	 }
    	 //这里针对团体万能只有一个子账户，所以直接获取
    	 tlcAccClassSchema =tlcinsureAccClassSet.get(1);
         Reflections ref = new Reflections();
         LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
         ref.transFields(traceSchema, tlcAccClassSchema);
         String tLimit = PubFun.getNoLimit(tlcAccClassSchema.getManageCom());
         String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
         traceSchema.setSerialNo(serNo);
         traceSchema.setMoneyType("HL");
         traceSchema.setMoney(-Math.abs(sumMoney));
         traceSchema.setPayDate(mTimeEnd);
         PubFun.fillDefaultField(traceSchema);
         MMap tMMap = new MMap();
         tMMap.put(traceSchema, SysConst.INSERT);
         tPerMMap.add(tMMap);
    	
    }
    /**
     * 取出待领取的金额
     * @return boolean
     */
    private double  getMoney(LCPolSchema tLCPolSchema,LCGetSchema mLCGetSchema){
//    	查找出所有的保单
        String accsql ="select nvl(sum(insuaccbala),0) from lcinsureacc where grpcontno='"+tLCPolSchema.getGrpContNo()+"' and polno='"+tLCPolSchema.getPolNo()+"' and  insuaccno='"+minsuaccno+"'";
    	return Double.parseDouble(new ExeSQL().getOneValue(accsql));
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
        mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
            "LCPolSchema", 0));
        mLCGetSchema.setSchema((LCGetSchema) cInputData.getObjectByObjectName(
            "LCGetSchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
            "TransferData", 0);
        //mTimeStart = (String)mTransferData.getValueByName("timeStart");
        mTimeEnd = (String) mTransferData.getValueByName("timeEnd");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareData() {
        mInputData = new VData();
        try {
            if (mLJAGetSet.size() == 0 && mLJAGetSet.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "ULIPayPlanBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "没有符合条件的催付记录";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData.add(this.mLJAGetDrawSet);
            mInputData.add(this.mLJAGetSet);
            mInputData.add(this.mGlobalInput);
            mInputData.add(mLCGetSet);
            mInputData.add(tPerMMap);

            mResult = new VData();
            mResult.addElement(mSerialNo);
           if(mLJAGetSet.size() > 0) {
                mResult.addElement(String.valueOf(mLJAGetSet.size()));
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ULIPayPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //领取时给付校验
    private boolean checkDataToGet(LCGetSchema aLCGetSchema) {
        //        LCPolSchema tLCPolSchema = new LCPolSchema();
        //        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        //        tLCPolSchema = this.getPolInfo();

        //判断是否为生存给付责任,并准备给付责任数据
        LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
        tLMDutyGetDB.setGetDutyCode(aLCGetSchema.getGetDutyCode());
        if (!tLMDutyGetDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFGetPayBL";
            tError.functionName = "checkDataToGet";
            tError.errorMessage = "无给付责任描述数据。";
            this.mErrors.addOneError(tError);
            return false;
        }

        this.setDutyGetInfo(tLMDutyGetDB.getSchema());

        return true;
    }

    //计算新的领至日期
    public Date calGetToDate(LCGetSchema aLCGetSchema) {
        LMDutyGetAliveDB tLMDutyGetAliveDB = new LMDutyGetAliveDB();
        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        tLMDutyGetAliveDB.setGetDutyCode(aLCGetSchema.getGetDutyCode());
        tLMDutyGetAliveDB.setGetDutyKind(aLCGetSchema.getGetDutyKind());
        if (!tLMDutyGetAliveDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PayPlanBL";
            tError.functionName = "checkDataToGet";
            tError.errorMessage = "该责任存在描述表LMDutyGetAlive中!";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLMDutyGetAliveSchema = tLMDutyGetAliveDB.getSchema();
        String MAXGETCOUNTTYPE = tLMDutyGetAliveSchema.getMaxGetCountType();
        int MAXGETCOUNT = tLMDutyGetAliveSchema.getMaxGetCount();
        //给付最大次数类型:
        //0--无条件给付最大次数
        //1--无条件给付最大年龄数 
        //2--被保人死亡给付最大次数
        //3--被保人死亡标志给付最大年龄
        //4--投保人死亡标志给付最大次数
        //5--投保人死亡标志给付最大年龄
        //如果MAXGETCOUNTTYPE为空，则判断lcget的领至日GettoDate和终领日期GetEndDate
        //如果MAXGETCOUNTTYPE为空不为空，则按MAXGETCOUNTTYPE和MAXGETCOUNT字段进行判断，
        //死亡的判断标准：lcpol的deathdate不为空
        //次数计算：aCurGetTimes = PubFun.calInterval(aGetStartDate, tGetToDate, aGetIntv) + 1;
        Date aGetToDate;
        FDate tFDate = new FDate();
        String aLastToDate = aLCGetSchema.getGetEndDate();
        String aGetStartDate = aLCGetSchema.getGettoDate();
        String StartDate = aLCGetSchema.getGetStartDate();
        Date a = tFDate.getDate(aLastToDate);
        Date b = tFDate.getDate(aGetStartDate);
        //趸领记成原领至日期即起领日期；
        aGetToDate = PubFun.calDate(b, aLCGetSchema.getGetIntv(), "M", b);
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aLCGetSchema.getPolNo());
        LCPolSchema tLCPolSchema = new LCPolSchema();
        if (!tLCPolDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "PayPlanBL";
            tError.functionName = "checkDataToGet";
            tError.errorMessage = "被保人表中没有数据!";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLCPolSchema = tLCPolDB.getSchema();

        //已经领取的次数计算
        int aCurGetTimes = PubFun.calInterval(StartDate, aGetStartDate, String
            .valueOf(aLCGetSchema.getGetIntv())) + 1;
        if (MAXGETCOUNTTYPE == null || MAXGETCOUNTTYPE.equals("")) {
            if (aGetToDate.compareTo(a) <= 0) {
                return aGetToDate;
            } else {
                return null;
            }
        } else {
            if (MAXGETCOUNTTYPE.equals("0")) {
                if (MAXGETCOUNT >= aCurGetTimes) {
                    return aGetToDate;
                } else {
                    return null;
                }
            } else if (MAXGETCOUNTTYPE.equals("1")) {
                int age = PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),
                    PubFun.getCurrentDate(), "Y");
                if (MAXGETCOUNT >= age) {
                    return aGetToDate;
                } else {
                    return null;
                }
            }else {
                return null;
            }
        }
    }
    //计算给付金对应的给付金额
    public double calGetMoney(LCGetSchema aLCGetSchema) {
        double aGetMoney = 0;
        BqCalBase aBqCalBase = new BqCalBase();
        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        FDate tFDate = new FDate();
        String aCalCode;

        tLMDutyGetAliveSchema = getGetAliveInfo();
        tLCPolSchema.setSchema(this.getPolInfo());

        //趸领生存给付计算
        if (aLCGetSchema.getGetIntv() == 0) {
            if (aLCGetSchema.getSumMoney() > 0
                    && aLCGetSchema.getSumMoney() > 0) {
                this.setGetToDate(tFDate.getDate(aLCGetSchema.getGetEndDate()));
            } else {
                if (tLMDutyGetAliveSchema.getOthCalCode() == null
                        || tLMDutyGetAliveSchema.getOthCalCode().trim().equals(
                            "")) {
                    aGetMoney = aLCGetSchema.getActuGet();
                    this.setGetToDate(tFDate.getDate(aLCGetSchema
                        .getGetEndDate()));
                } else {
                    aBqCalBase = new BqCalBase();
                    aBqCalBase = initCalBase(aLCGetSchema);
                    aCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                    System.out.println("aCalCode :" + aCalCode);
                    BqCalBL tBqCalBL = new BqCalBL(aBqCalBase, aCalCode, "");
                    aGetMoney = tBqCalBL.calGetDraw();
                    this.setGetToDate(tFDate.getDate(aLCGetSchema
                        .getGetEndDate()));
                }
            }
        } else {
            //期领责任给付计算
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tLCGetSchema.setSchema(aLCGetSchema);
            while (tLCGetSchema.getGettoDate().compareTo(mTimeEnd) <= 0) {
                System.out.println("---oridate:" + tLCGetSchema.getGettoDate());
                System.out.println("---Todate:" + mTimeEnd);
                aBqCalBase = new BqCalBase();
                aBqCalBase = initCalBase(tLCGetSchema);

                if (tLMDutyGetAliveSchema.getMaxGetCount() == 0) {
                    if (tLCGetSchema.getGettoDate().compareTo(
                        tLCGetSchema.getGetEndDate()) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                } else {
                    //无条件给付最大次数
                    if (tLMDutyGetAliveSchema.getMaxGetCountType().equals("0")
                            && aBqCalBase.getGetTimes().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                    //无条件给付最大年龄
                    else if (tLMDutyGetAliveSchema.getMaxGetCountType().equals(
                        "1")
                            && aBqCalBase.getGetAge().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                    //被保人死亡标志给付最大次数
                    else if (tLCPolSchema.getDeadFlag().equals("1")
                            && tLMDutyGetAliveSchema.getMaxGetCountType()
                                .equals("2")
                            && aBqCalBase.getGetTimes().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                    //被保人死亡标志给付最大年龄
                    else if (tLCPolSchema.getDeadFlag().equals("1")
                            && tLMDutyGetAliveSchema.getMaxGetCountType()
                                .equals("3")
                            && aBqCalBase.getGetAge().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                    //投保人死亡标志给付最大次数
                    else if (tLCPolSchema.getDeadFlag().equals("2")
                            && tLMDutyGetAliveSchema.getMaxGetCountType()
                                .equals("4")
                            && aBqCalBase.getGetTimes().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    }
                    //投保人死亡标志给付最大年龄
                    else if (tLCPolSchema.getDeadFlag().equals("2")
                            && tLMDutyGetAliveSchema.getMaxGetCountType()
                                .equals("5")
                            && aBqCalBase.getGetAge().compareTo(
                                String.valueOf(tLMDutyGetAliveSchema
                                    .getMaxGetCount())) > 0) {
                        this.setCancelFlag("1");
                        break;
                    } else {
                        System.out
                            .println("------------------no descriptions----------");
                        ;
                    }
                }

                //计算应领金额
                if (tLMDutyGetAliveSchema.getOthCalCode() == null
                        || tLMDutyGetAliveSchema.getOthCalCode().equals(
                            "XXXXXX")
                        || tLMDutyGetAliveSchema.getOthCalCode().trim().equals(
                            "")) {
                    //非递增
                    if (tLMDutyGetAliveSchema.getAddFlag().equals("N")) {
                        aGetMoney = aGetMoney + tLCGetSchema.getActuGet();
                    } else {
                        aGetMoney = aGetMoney
                                + calAddGetMoney(aBqCalBase, tLCGetSchema);
                    }
                } else {
                    aCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                    BqCalBL tBqCalBL = new BqCalBL(aBqCalBase, aCalCode, "");
                    aGetMoney = aGetMoney + tBqCalBL.calGetDraw();
                }
                //  计算应领至日期
                tLCGetSchema.setGettoDate(this.calGetToDate(tLCGetSchema));
                this.setGetToDate(tFDate.getDate(tLCGetSchema.getGettoDate()));
                System.out.println("---oridate:" + tLCGetSchema.getGettoDate());
            }
            System.out.println("------end while-----");
        }
        System.out.println("-------getMoney:" + aGetMoney);
        return aGetMoney;
    }

    private BqCalBase initCalBase(LCGetSchema aLCGetSchema) {
        System.out.println("......initCalBase ");
        BqCalBase aBqCalBase = new BqCalBase();
        FDate tFDate = new FDate();
        int aGetYears;
        //        int aLastGetTimes;
        int aCurGetTimes, aGetAge, aAppYears;
        String aGetIntv;
        Date tGetToDate, aGetStartDate;
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

        if (aLCGetSchema.getGetIntv() == 0) {
            tGetToDate = tFDate.getDate(mTimeEnd);
        } else {
            tGetToDate = this.calGetToDate(aLCGetSchema);
        }
        aGetStartDate = tFDate.getDate(aLCGetSchema.getGetStartDate());
        aGetIntv = String.valueOf(aLCGetSchema.getGetIntv());

        //本次是第几年给付（领取时的领取年期)
        aGetYears = PubFun.calInterval(aGetStartDate, tGetToDate, "Y");
        // 上次是第几次给付
        //        aLastGetTimes = PubFun.calInterval(aLCGetSchema.getGetStartDate(),
        //            aLCGetSchema.getGettoDate(), aGetIntv) + 1;
        //本次是第几次给付
        aCurGetTimes = PubFun.calInterval(aGetStartDate, tGetToDate, aGetIntv) + 1;
        //领取时被保人年龄,投保年期
        tLCPolSchema = this.getPolInfo();
        tLCInsuredSchema = this.getInsuredInfo();
        //投保年龄，已交费年期，始领年龄，始领年期
        int aAppAge, aPayYear, aGetStartAge, aGetStartYear;
        /*Lis5.3 upgrade get
         aGetAge = PubFun.calInterval(tFDate.getDate(tLCInsuredSchema.getBirthday()),tGetToDate,"Y");
         aAppAge = PubFun.calInterval(tLCInsuredSchema.getBirthday(),tLCPolSchema.getCValiDate(),"Y");
         aGetStartAge = PubFun.calInterval(tLCInsuredSchema.getBirthday(),aLCGetSchema.getGetStartDate(),"Y");
         */
        aGetAge = 0;
        aAppAge = 0;
        aGetStartAge = 0;
        aAppYears = PubFun.calInterval(tFDate.getDate(tLCPolSchema
            .getCValiDate()), tGetToDate, "Y");
        aPayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(), tLCPolSchema
            .getPaytoDate(), "Y");
        aGetStartYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
            aLCGetSchema.getGetStartDate(), "Y");

        //得到getblance值
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(aLCGetSchema.getPolNo());
        tLCInsureAccDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLCInsureAccDB.setAccType("003");
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
        System.out.println("tLCInsureAccSet :" + tLCInsureAccSet.size());
        double aGetBalance = 0.0;

        for (int i = 1; i <= tLCInsureAccSet.size(); i++)
            aGetBalance += tLCInsureAccSet.get(i).getInsuAccGetMoney();

        aBqCalBase.setAppAge(aAppAge);
        /*Lis5.3 upgrade get
         aBqCalBase.setGrpPolNo(aLCGetSchema.getGrpPolNo());
         */
        aBqCalBase.setAddRate(aLCGetSchema.getAddRate());
        aBqCalBase.setGet(aLCGetSchema.getActuGet());
        aBqCalBase.setGetIntv(aLCGetSchema.getGetIntv());
        aBqCalBase.setGDuty(aLCGetSchema.getGetDutyCode());
        aBqCalBase.setGetStartDate(aLCGetSchema.getGetStartDate());

        aBqCalBase.setGetStartYear(aGetStartYear);
        aBqCalBase.setGetStartAge(aGetStartAge);
        aBqCalBase.setGetAge(aGetAge);
        aBqCalBase.setGetTimes(aCurGetTimes);
        aBqCalBase.setPayEndYear(aPayYear);
        aBqCalBase.setGetYear(aGetYears);
        aBqCalBase.setGetAppYear(aAppYears);
        aBqCalBase.setInterval(1);
        aBqCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());

        aBqCalBase.setMult(tLCPolSchema.getMult());
        aBqCalBase.setPayIntv(tLCPolSchema.getPayIntv());
        aBqCalBase.setGetIntv(aLCGetSchema.getGetIntv());
        aBqCalBase.setPolNo(tLCPolSchema.getPolNo());
        aBqCalBase.setCValiDate(tLCPolSchema.getCValiDate());
        aBqCalBase.setYears(tLCPolSchema.getYears());
        aBqCalBase.setPrem(tLCPolSchema.getPrem());
        /*Lis5.3 upgrade get
         aBqCalBase.setSex(tLCInsuredSchema.getSex());
         */
        aBqCalBase.setJob(tLCInsuredSchema.getOccupationCode());

        aBqCalBase.setGetBalance(aGetBalance);

        this.displayObject(aBqCalBase);
        return aBqCalBase;
    }

    //递增给付领取处理
    private double calAddGetMoney(BqCalBase aBqCalBase, LCGetSchema aLCGetSchema) {
        double aGetMoney = 0;
        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        //        BqCalBase tBqCalBase = new BqCalBase();
        tLMDutyGetAliveSchema.setSchema(this.getGetAliveInfo());
        int aGetAge, aGetAppYear, aGetYear, aTimes;

        aGetAge = Integer.parseInt(aBqCalBase.getGetAge());
        aGetAppYear = Integer.parseInt(aBqCalBase.getGetAppYear());
        aGetYear = Integer.parseInt(aBqCalBase.getGetYear());
        aTimes = 0;

        if (aLCGetSchema.getAddRate() > 0) {
            tLMDutyGetAliveSchema.setAddValue(aLCGetSchema.getAddRate());
        }
        //递增开始年龄
        if (tLMDutyGetAliveSchema.getAddStartUnit().equals("0")) {
            if (aGetAge < tLMDutyGetAliveSchema.getAddStartPeriod()) {
                aGetMoney = aLCGetSchema.getActuGet();
            } else {
                if (aGetAge > tLMDutyGetAliveSchema.getAddEndPeriod()) {
                    aGetAge = tLMDutyGetAliveSchema.getAddEndPeriod();
                }

                aGetAge = aGetAge - tLMDutyGetAliveSchema.getAddStartPeriod()
                        + 1;
                //递增间隔单位默认为年
                if (aGetAge % tLMDutyGetAliveSchema.getAddIntv() != 0) {
                    aTimes = aGetAge / tLMDutyGetAliveSchema.getAddIntv() + 1;
                } else {
                    aTimes = aGetAge / tLMDutyGetAliveSchema.getAddIntv();
                }
                aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),
                    tLMDutyGetAliveSchema.getAddValue(), aTimes,
                    tLMDutyGetAliveSchema.getAddType());
            }
        }
        //领取递增开始年期,领取后多少年开始递增
        else if (tLMDutyGetAliveSchema.getAddStartUnit().equals("1")) {
            if (aGetYear < tLMDutyGetAliveSchema.getAddStartPeriod()) {
                aGetMoney = aLCGetSchema.getActuGet();
            } else {
                if (aGetYear > tLMDutyGetAliveSchema.getAddEndPeriod()) {
                    aGetYear = tLMDutyGetAliveSchema.getAddEndPeriod();
                }

                aGetYear = aGetYear - tLMDutyGetAliveSchema.getAddStartPeriod()
                        + 1;
                //递增间隔单位默认为年
                if (aGetYear % tLMDutyGetAliveSchema.getAddIntv() != 0) {
                    aTimes = aGetYear / tLMDutyGetAliveSchema.getAddIntv() + 1;
                } else {
                    aTimes = aGetYear / tLMDutyGetAliveSchema.getAddIntv();
                }
                aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),
                    tLMDutyGetAliveSchema.getAddValue(), aTimes,
                    tLMDutyGetAliveSchema.getAddType());
            }
        }
        //投保递增开始年期,投保后多少年递增
        else if (tLMDutyGetAliveSchema.getAddStartUnit().equals("2")) {
            if (aGetAppYear < tLMDutyGetAliveSchema.getAddStartPeriod()) {
                aGetMoney = aLCGetSchema.getActuGet();
            } else {
                if (aGetAppYear > tLMDutyGetAliveSchema.getAddEndPeriod()) {
                    aGetAppYear = tLMDutyGetAliveSchema.getAddEndPeriod();
                }

                aGetAppYear = aGetAppYear
                        - tLMDutyGetAliveSchema.getAddStartPeriod() + 1;
                //递增间隔单位默认为年
                if (aGetAppYear % tLMDutyGetAliveSchema.getAddIntv() != 0) {
                    aTimes = aGetAppYear / tLMDutyGetAliveSchema.getAddIntv()
                            + 1;
                } else {
                    aTimes = aGetAppYear / tLMDutyGetAliveSchema.getAddIntv();
                }
                aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),
                    tLMDutyGetAliveSchema.getAddValue(), aTimes,
                    tLMDutyGetAliveSchema.getAddType());
            }
        } else {
            System.out.println("-----------no this add descriptions--------");
        }

        return aGetMoney;
    }

    //计算递增值
    private double calAddData(double aOriginData, double aAddRate,
            double aTimes, String aAddRateType) {
        double aResultData;
        //按照比例递增
        if (aAddRateType.equals("R")) {
            aResultData = aOriginData + aOriginData * aAddRate * aTimes;
        } else {
            aResultData = aOriginData + aAddRate * aTimes;
        }

        return aResultData;
    }

    //得到保单是否销户标志
    //    private String getContCancelFlag(String aCancelFlag) {
    //        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    //        tLMDutyGetAliveSchema = this.getGetAliveInfo();
    //
    //        //默认为000，为防止描述造成的数据问题
    //        try {
    //            if (tLMDutyGetAliveSchema.getAfterGet() == null
    //                    || tLMDutyGetAliveSchema.getAfterGet().trim().equals("")) {
    //                tLMDutyGetAliveSchema.setAfterGet("000");
    //            }
    //        } catch (Exception ex) {
    //        }
    //        //"000" 无动作
    //        if (tLMDutyGetAliveSchema.getAfterGet().equals("000"))
    //            aCancelFlag = "0";
    //        // "001"   保额递减（暂不用）
    //        else if (tLMDutyGetAliveSchema.getAfterGet().equals("001"))
    //            aCancelFlag = "0";
    //        //"002"    保额递增（暂不用）
    //        else if (tLMDutyGetAliveSchema.getAfterGet().equals("002"))
    //            aCancelFlag = "0";
    //        // "003" 无条件销户
    //        else if (tLMDutyGetAliveSchema.getAfterGet().equals("003"))
    //            aCancelFlag = "1";
    //        //"004" 最后一次给付销户
    //        else if (tLMDutyGetAliveSchema.getAfterGet().equals("004")) {
    //            if (aCancelFlag.equals("1"))
    //                aCancelFlag = "1";
    //            else
    //                aCancelFlag = "0";
    //        } else
    //            System.out.println("----no des getAliv---");
    //
    //        return aCancelFlag;
    //    }

    //是否销户标志
    private void setCancelFlag(String aCancelFlag) {
        //        mCancelFlag = aCancelFlag;
    }

    //    private String getCancelFlag() {
    //        if (mCancelFlag == null)
    //            mCancelFlag = "0";
    //
    //        return mCancelFlag;
    //    }

    //给付金额
    //    private void setGetMoney(double aGetMoney) {
    //        mGetMoney = aGetMoney;
    //    }
    //
    //    private double getGetMoney() {
    //        return mGetMoney;
    //    }

    //给付至日期
    private void setGetToDate(Date aGetToDate) {
        mGetToDate = aGetToDate;
    }

    private Date getGetToDate() {
        return mGetToDate;
    }

    //保单信息
    private LCPolSchema getPolInfo() {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(mLCPolSchema);
        return tLCPolSchema;
    }

    private void setPolInfo(LCPolSchema tLCPolSchema) {
        mLCPolSchema.setSchema(tLCPolSchema);
    }

    //保单客户信息
    private LCInsuredSchema getInsuredInfo() {
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        tLCInsuredSchema.setSchema(mLCInsuredSchema);
        return tLCInsuredSchema;
    }

    private void setInsuredInfo(LCInsuredSchema tLCInsuredSchema) {
        mLCInsuredSchema.setSchema(tLCInsuredSchema);
    }

    //生存给付项信息
    private LMDutyGetAliveSchema getGetAliveInfo() {
        LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
        tLMDutyGetAliveSchema.setSchema(mLMDutyGetAliveSchema);
        return tLMDutyGetAliveSchema;
    }

    //    private void setGetAliveInfo(LMDutyGetAliveSchema tLMDutyGetAliveSchema) {
    //        mLMDutyGetAliveSchema.setSchema(tLMDutyGetAliveSchema);
    //    }
    //
    //    //给付项信息
    //    private LMDutyGetSchema getDutyGetInfo() {
    //        LMDutyGetSchema tLMDutyGetSchema = new LMDutyGetSchema();
    //        tLMDutyGetSchema.setSchema(mLMDutyGetSchema);
    //        return tLMDutyGetSchema;
    //    }

    private void setDutyGetInfo(LMDutyGetSchema tLMDutyGetSchema) {
        mLMDutyGetSchema.setSchema(tLMDutyGetSchema);
    }

    public VData getResult() {
        return mResult;
    }

    private void displayObject(Object a) {
        Reflections aReflections = new Reflections();
        aReflections.printFields(a);
    }

    public static void main(String[] args) {
   /*     VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCGetSchema tLCGetSchema = new LCGetSchema();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("timeEnd", "2011-3-26");
        tG.Operator = "CJG";
        tG.ManageCom = "86";
        //tLCGetSchema.setPolNo("86110020020210000443");-
        tVData.addElement(tG);
        tVData.addElement(tLCPolSchema);
        tVData.addElement(tLCGetSchema);
        tVData.addElement(tTransferData);
        ULIPayPlanBL tULIPayPlanBL = new ULIPayPlanBL();

        tULIPayPlanBL.submitData(tVData, "");*/
    	String test ="";
    	double te =0.0;
    System.out.println(-Math.abs(0.00));	
    	
    }
}
