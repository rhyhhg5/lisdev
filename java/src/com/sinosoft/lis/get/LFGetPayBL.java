package com.sinosoft.lis.get;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author TJJ
 * @version 1.0
 */
public class LFGetPayBL  {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 数据操作字符串 */
  private String mOperate;

  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  /** 业务处理相关变量 */

  private LJSGetDrawSet mLJSGetDrawSet=new LJSGetDrawSet();

  private LJSGetSet aLJSGetSet = new LJSGetSet();
  private LJAGetDrawSet aLJAGetDrawSet=new LJAGetDrawSet();
  private LJAGetSet aLJAGetSet=new LJAGetSet();
  private LJSGetDrawSet aLJSGetDrawSet=new LJSGetDrawSet();
  private LCGetSet aLCGetSet = new LCGetSet();
  private LJFIGetSet aLJFIGetSet = new LJFIGetSet();
  private LJTempFeeClassSet aLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeSet aLJTempFeeSet = new LJTempFeeSet();
  private LJAPaySet aLJAPaySet = new LJAPaySet();
  private LJAPayPersonSet aLJAPayPersonSet = new LJAPayPersonSet();
  private LCInsureAccSet aLCInsureAccSet = new LCInsureAccSet();
  private LCInsureAccTraceSet aLCInsureAccTraceSet = new LCInsureAccTraceSet();

  private String mActuGetNo="";
  private String mDrawer = "";
  private String mDrawerID = "";

  public LFGetPayBL() {
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //进行业务处理
    if (!dealData())
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LFGetPayBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败LFGetPayBL-->dealData!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    System.out.println("after dealData....");


    if (mLJSGetDrawSet.size()>0)
    {
      aLJSGetDrawSet.clear();
      aLJAGetDrawSet.clear();
      aLJSGetSet.clear();
      aLJAGetSet.clear();
      aLCGetSet.clear();
      aLJFIGetSet.clear();
      aLJTempFeeClassSet.clear();
      aLJTempFeeSet.clear();
      aLJAPaySet.clear();
      aLJAPayPersonSet.clear();
      aLCInsureAccSet.clear();
      aLCInsureAccTraceSet.clear();

      for (int i=1;i<=this.mLJSGetDrawSet.size();i++)
      {
        LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
        tLJSGetDrawSchema = mLJSGetDrawSet.get(i);

        //得到所有的必需的存储和修改数据
        if (!processOneLJSGetDraw(tLJSGetDrawSchema))
            return false;
        System.out.println("afterOneLJSGetDraw....");

        //准备往后台的数据
        if (!prepareOutputData())
        return false;
        //催付核销数据库操作
        System.out.println("Start LFGetPay BL Submit...");
        LFGetPayBLS tLFGetPayBLS=new LFGetPayBLS();
        tLFGetPayBLS.submitData(mInputData,cOperate);
        System.out.println("End LFGetPay BL Submit...");
        System.out.println("-------------------");

        //如果有需要处理的错误，则返回
        if (tLFGetPayBLS.mErrors.needDealError()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLFGetPayBLS.mErrors);
          CError tError = new CError();
          tError.moduleName = "LFGetPayBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        System.out.println("Start 销户操作....");
        System.out.println(aLJAGetDrawSet.size());
        //销户操作
        if (aLJAGetDrawSet.size()>0) {
            String aPolNo;
            aPolNo="";
            for (int j=1;j<=aLJAGetDrawSet.size();j++) {
                if (j==1||aLJAGetDrawSet.get(j).getPolNo().equals(aPolNo)) {
                    aPolNo = aLJAGetDrawSet.get(j).getPolNo();
                    String destroyFlag = aLJAGetDrawSet.get(j).getDestrayFlag();
                    System.out.println("---" + destroyFlag);
                    if (destroyFlag != null && destroyFlag.equals("1")) {

					  //sxy-2003-09-10修改该保单状态为终止.
					  String tsql = "select * from lcpol where polno = '"+aPolNo+"' ";
					  LCPolDB tLCPolDB = new LCPolDB();
					  LCPolSet tLCPolSet = new LCPolSet();
					  tLCPolSet = tLCPolDB.executeQuery(tsql);
					  if(tLCPolSet.size()==1)
					  {
						 LCPolSchema tLCPolSchema = new LCPolSchema();
						 VData tVData = new VData();
						 tLCPolSchema.setPolNo(tLCPolSet.get(1).getPolNo());
						 tLCPolSchema.setPolState( "0303" + tLCPolSet.get(1).getPolState().substring(0,3));

						 tVData.add(tLCPolSchema) ;
						 ChangPolStateBLS tChangPolStateBLS = new ChangPolStateBLS();
						 if(!tChangPolStateBLS.submitData(tVData,"UPDATA"))
						 {
						   // @@错误处理
						   this.mErrors.copyAllErrors(tLFGetPayBLS.mErrors);
						   CError tError = new CError();
						   tError.moduleName = "LFGetPayBL";
						   tError.functionName = "submitData";
						   tError.errorMessage = "保单状态数据修改失败!";
						   this.mErrors .addOneError(tError) ;
						   return false;
						 }

					  }
					  else
					  {
						// @@错误处理
						   this.mErrors.copyAllErrors(tLFGetPayBLS.mErrors);
						   CError tError = new CError();
						   tError.moduleName = "LFGetPayBL";
						   tError.functionName = "submitData";
						   tError.errorMessage = "保单数据查询出错!";
						   this.mErrors .addOneError(tError) ;
						   return false;
					  } //sxy-2003-09-10

                        ContCancel aContCancel = new ContCancel(aPolNo,aLJAGetDrawSet.get(j).getActuGetNo());
                        if (!aContCancel.submitData())
                              return false;
                    }
                 }
             }
        }
      } //end of first cycle : mLJSGetDrawSet
     }

     return true;
  }



  public String getActuGetNo()
  {
    return this.mActuGetNo;
  }

  private boolean processOneLJSGetDraw(LJSGetDrawSchema tLJSGetDrawSchema)
  {
      LJSGetDrawDB tLJSGetDrawDB=new LJSGetDrawDB();
      tLJSGetDrawDB.setSchema(tLJSGetDrawSchema);
      if (!tLJSGetDrawDB.getInfo()) {
         this.mErrors.copyAllErrors(tLJSGetDrawDB.mErrors);
         return false;
      }

      LJSGetDB tLJSGetDB = new LJSGetDB();
      tLJSGetDB.setGetNoticeNo(tLJSGetDrawSchema.getGetNoticeNo());
      if (!tLJSGetDB.getInfo()) {
          this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
          return false;
      }

      tLJSGetDrawSchema = tLJSGetDrawDB.getSchema();
      LJSGetSchema tLJSGetSchema = new LJSGetSchema();
      tLJSGetSchema.setSchema(tLJSGetDB.getSchema());

      LCGetSchema tLCGetSchema = new LCGetSchema();
      LCGetDB tLCGetDB = new LCGetDB();
      tLCGetDB.setPolNo(tLJSGetDrawSchema.getPolNo());
      tLCGetDB.setDutyCode(tLJSGetDrawSchema.getDutyCode());
      tLCGetDB.setGetDutyCode(tLJSGetDrawSchema.getGetDutyCode());
      if (!tLCGetDB.getInfo()) {
           this.mErrors.copyAllErrors(tLCGetDB.mErrors);
           return false;
      }
      tLCGetSchema.setSchema(tLCGetDB.getSchema());
      tLCGetSchema.setGettoDate(tLJSGetDrawSchema.getCurGetToDate());
      //查询保单号，得到相应的付费方式
      LCPolSchema tLCPolSchema = new LCPolSchema();
      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setPolNo(tLJSGetDrawSchema.getPolNo());
      if (!tLCPolDB.getInfo()) {
           this.mErrors.copyAllErrors(tLCPolDB.mErrors);
           return false;
      }
      tLCPolSchema.setSchema(tLCPolDB.getSchema());
      System.out.println("after LCPolSchema get...");
      String flag = tLCPolSchema.getLiveGetMode();

      //默认的领取方式为现金方式
      if (flag == null || flag.trim().equals("")) {
      	flag = "1";
      }

      if (flag.equals("1")) { //现金方式
          if (!CashProcess(tLCPolSchema,tLJSGetDrawSchema,tLJSGetSchema,tLCGetSchema))
              return false;
          else
              return true;
      }

     else if (flag.equals("2")) {  //抵交保费
          if (!PremProcess(tLCPolSchema,tLJSGetDrawSchema,tLJSGetSchema,tLCGetSchema))
              return false;
          else
              return true;
      }

      else if (flag.equals("4")) { //累计生息
          if (!AccProcess(tLCPolSchema,tLJSGetDrawSchema,tLJSGetSchema,tLCGetSchema))
              return false;
          else
              return true;
      }
     else
     {
         tLCGetSchema.setSumMoney(tLCGetSchema.getSumMoney() +tLJSGetDrawSchema.getGetMoney());
     }
      return true;

  }


  private boolean AccProcess(
      LCPolSchema tLCPolSchema,
      LJSGetDrawSchema tLJSGetDrawSchema,
      LJSGetSchema tLJSGetSchema,
      LCGetSchema tLCGetSchema)
  {

      System.out.println("start AccProcess....");
      VData tVData = new VData();
      DealAccount tDealAccount = new DealAccount();
      LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
      LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
      LMRiskInsuAccSet tLMRiskInsuAccSet = new LMRiskInsuAccSet();

      //取累计生息帐户
      String tsql = "";
      tsql = "select * from LMRiskInsuAcc where InsuAccNo in (select InsuAccNo from LMRiskToAcc where riskcode = '"+tLCPolSchema.getRiskCode().trim()+"') and acctype = '003'";
      tLMRiskInsuAccSet = tLMRiskInsuAccDB.executeQuery(tsql);
      if (tLMRiskInsuAccSet.size() > 0) {
          tLMRiskInsuAccSchema = tLMRiskInsuAccSet.get(1);
      }

      System.out.println("tLMRiskInsuAccSchema :" + tLMRiskInsuAccSchema.getInsuAccNo());

      LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
      LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
      tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
      tLCInsureAccDB.setAccType("003");
      tLCInsureAccSet = tLCInsureAccDB.query();
      System.out.println("tLCInsureAccSet size :" + tLCInsureAccSet.size());
      //生成帐户数据
      if( tLCInsureAccSet.size() > 0) {
          tVData = tDealAccount.addPrem(tLCPolSchema.getPolNo(),tLMRiskInsuAccSchema.getInsuAccNo(),tLCPolSchema.getPolNo(),"1","LX",tLCPolSchema.getManageCom(),tLJSGetDrawSchema.getGetMoney());
      } else {
          tVData = tDealAccount.getLCInsureAcc(tLCPolSchema.getPolNo(),tLMRiskInsuAccSchema.getAccCreatePos(),tLCPolSchema.getPolNo(),"1",tLCPolSchema.getManageCom(),"003","LX",tLJSGetDrawSchema.getGetMoney());
      }

      if (tVData == null)
          {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFGetPayBL";
            tError.functionName = "AccProcess";
            tError.errorMessage = tLCPolSchema.getPolNo()+"帐户数据生成失败!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          //帐户表
          tLCInsureAccSet = new LCInsureAccSet();
          tLCInsureAccSet = (LCInsureAccSet)tVData.getObjectByObjectName("LCInsureAccSet",0);
          if (tLCInsureAccSet.size()> 0)
          {
            for (int i = 1;i <= tLCInsureAccSet.size();i++)
            {
              LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
              tLCInsureAccSchema = tLCInsureAccSet.get(i);
              aLCInsureAccSet.add(tLCInsureAccSchema);
            }
          }
          //帐户轨迹表
          LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
          tLCInsureAccTraceSet = (LCInsureAccTraceSet)tVData.getObjectByObjectName("LCInsureAccTraceSet",0);
          if (tLCInsureAccTraceSet.size()> 0)
          {
            for (int i = 1;i <= tLCInsureAccTraceSet.size();i++)
            {
              LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
              tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(i);
              aLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
            }
          }

          return true;
  }



  private boolean PremProcess(
      LCPolSchema tLCPolSchema,
      LJSGetDrawSchema tLJSGetDrawSchema,
      LJSGetSchema tLJSGetSchema,
      LCGetSchema tLCGetSchema)
  {
      System.out.println("start PremProcess....");
      String tActuGetNo=PubFun1.CreateMaxNo("GETNO",PubFun.getNoLimit(tLJSGetDrawSchema.getManageCom())); //参数为机构代码
      mActuGetNo=tActuGetNo;

      String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());

      LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
      tLJAGetDrawSchema = initLJAGetDrawSchema(tLJSGetDrawSchema);

      LJAGetSchema tLJAGetSchema = new LJAGetSchema();
      tLJAGetSchema = initLJAGetSchema(tLJSGetSchema);

      tLCGetSchema.setGettoDate(tLJSGetDrawSchema.getCurGetToDate());
      tLCGetSchema.setSumMoney(tLCGetSchema.getSumMoney()+tLJSGetDrawSchema.getGetMoney());

      //财务实付总表
      LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();

      //--取投保人姓名
      String tappntname = "";
      LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
      tLCAppntIndDB.setPolNo(tLJAGetSchema.getOtherNo());
      tLCAppntIndDB.setCustomerNo(tLJAGetSchema.getAppntNo());
      if(!tLCAppntIndDB.getInfo()) {

      } else {
          tappntname = tLCAppntIndDB.getName();
      }

      tLJFIGetSchema.setActuGetNo(tLJAGetSchema.getActuGetNo());

      //----------------is here right?----------------------
      //tLJFIGetSchema.setPayMode(tLJAGetSchema.getPayMode());
      tLJFIGetSchema.setPayMode("5");
      //-------------------------------------------------------

      tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
      tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
      tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
      tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());

      //------------------------------------------------------------
      tLJFIGetSchema.setEnterAccDate(tLJAGetSchema.getEnterAccDate());
      tLJFIGetSchema.setEnterAccDate(PubFun.getCurrentDate());
      //-------------------------------------------------------------

      tLJFIGetSchema.setConfDate(tLJAGetSchema.getConfDate());
      tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
      tLJFIGetSchema.setManageCom(tLJAGetSchema.getManageCom());
      tLJFIGetSchema.setAPPntName(tappntname);
      tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
      tLJFIGetSchema.setAgentType(tLJAGetSchema.getAgentType());
      tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
      tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
      tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
      tLJFIGetSchema.setDrawer(tLJAGetSchema.getDrawer());
      tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());

      System.out.println("tLJAGetSchema.getOperator() :====" + tLJAGetSchema.getOperator());
      tLJFIGetSchema.setOperator(tLJAGetSchema.getOperator());

      tLJFIGetSchema.setMakeTime(tLJAGetSchema.getMakeTime());
      tLJFIGetSchema.setMakeDate(tLJAGetSchema.getMakeDate());
      tLJFIGetSchema.setState("0");
      tLJFIGetSchema.setModifyDate(tLJAGetSchema.getModifyDate());
      tLJFIGetSchema.setModifyTime(tLJAGetSchema.getModifyTime());

      aLJFIGetSet.add(tLJFIGetSchema);

      //暂交费分类表
      LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      String tPayNo = PubFun1.CreateMaxNo("PayNo",tLimit);
      String serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);

      tLJTempFeeClassSchema.setTempFeeNo(tPayNo);
      tLJTempFeeClassSchema.setPayMode("5");
      tLJTempFeeClassSchema.setChequeNo("");
      tLJTempFeeClassSchema.setPayMoney(tLJSGetDrawSchema.getGetMoney());
//      tLJTempFeeClassSchema.setAPPntName(tappntname);
      tLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setConfDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setApproveDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setConfFlag("1");
      tLJTempFeeClassSchema.setSerialNo(serNo);
      tLJTempFeeClassSchema.setOperator(tLJAGetSchema.getOperator());
      tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
      tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
      tLJTempFeeClassSchema.setManageCom(tLJSGetDrawSchema.getManageCom());

      aLJTempFeeClassSet.add(tLJTempFeeClassSchema);

      //暂交费表
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

      tLJTempFeeSchema.setTempFeeNo(tPayNo);
      tLJTempFeeSchema.setTempFeeType("2");
      tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
      tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
      tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
      tLJTempFeeSchema.setOtherNoType("0");
      tLJTempFeeSchema.setPayMoney(tLJSGetDrawSchema.getGetMoney());
      tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
      tLJTempFeeSchema.setManageCom(tLJSGetDrawSchema.getManageCom());
      tLJTempFeeSchema.setAgentCom(tLCPolSchema.getAgentCom());
      tLJTempFeeSchema.setAgentType(tLCPolSchema.getAgentType());
      tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
      tLJTempFeeSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
      tLJTempFeeSchema.setAgentCode(tLCPolSchema.getAgentCode());
      tLJTempFeeSchema.setConfFlag("1");
      tLJTempFeeSchema.setSerialNo(serNo);
      tLJTempFeeSchema.setOperator(tLJAGetSchema.getOperator());
      tLJTempFeeSchema.setState("0");
      tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
      tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

      aLJTempFeeSet.add(tLJTempFeeSchema);

      //实收总表
      LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      //String tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());

      tLJAPaySchema.setPayNo(tPayNo);         //交费收据号码
      //tLJAPaySchema.setIncomeNo("00000000000000000000");          //应收/实收编号
      tLJAPaySchema.setIncomeNo(tLJSGetSchema.getOtherNo());    //应收/实收编号
      tLJAPaySchema.setIncomeType("2");    //应收/实收编号类型
      tLJAPaySchema.setAppntNo(tLCPolSchema.getAppntNo());           //  投保人客户号码
      tLJAPaySchema.setSumActuPayMoney(tLJSGetDrawSchema.getGetMoney()); // 总实交金额
      tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());   // 到帐日期
      tLJAPaySchema.setPayDate(PubFun.getCurrentDate());           //交费日期
      tLJAPaySchema.setConfDate(PubFun.getCurrentDate());      //确认日期
      tLJAPaySchema.setApproveCode(tLCPolSchema.getApproveCode());   //复核人编码
      tLJAPaySchema.setApproveDate(PubFun.getCurrentDate());   //  复核日期
      tLJAPaySchema.setSerialNo(serNo);                           //流水号
      tLJAPaySchema.setOperator(tLJAGetSchema.getOperator());         // 操作员
      tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());                     //入机时间
      tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());                     //入机时间
      tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());                   //最后一次修改日期
      tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());                   //最后一次修改时间
      tLJAPaySchema.setManageCom(tLCPolSchema.getManageCom());
      tLJAPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
      tLJAPaySchema.setAgentType(tLCPolSchema.getAgentType());
      /*Lis5.3 upgrade get
      tLJAPaySchema.setBankCode(tLCPolSchema.getBankCode());      //银行编码
      tLJAPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());   //银行帐号
      */
      tLJAPaySchema.setRiskCode(tLCPolSchema.getRiskCode());   // 险种编码
      tLJAPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
      tLJAPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());

      aLJAPaySet.add(tLJAPaySchema);

      //个人实收表
      LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();

      tLJAPayPersonSchema.setPolNo(tLCPolSchema.getPolNo());           //保单号码
      tLJAPayPersonSchema.setPayCount("");     //第几次交费
      tLJAPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());     //集体保单号码
      tLJAPayPersonSchema.setContNo(tLCPolSchema.getContNo());         //总单/合同号码
      tLJAPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());       //投保人客户号码
      tLJAPayPersonSchema.setPayNo(tPayNo);          //交费收据号码

      if (tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
      {
          tLJAPayPersonSchema.setPayAimClass("1");//交费目的分类
      }
      if (!tLCPolSchema.getGrpPolNo().equals("00000000000000000000")&&tLCPolSchema.getContNo().equals("00000000000000000000"))
      {
          tLJAPayPersonSchema.setPayAimClass("2");//交费目的分类
      }

      tLJAPayPersonSchema.setDutyCode("0000000000");      //责任编码
      tLJAPayPersonSchema.setPayPlanCode("00000000");//交费计划编码
      tLJAPayPersonSchema.setSumDuePayMoney(0);//总应交金额
      tLJAPayPersonSchema.setSumActuPayMoney(tLJSGetDrawSchema.getGetMoney());//总实交金额
      tLJAPayPersonSchema.setPayIntv(0);        //交费间隔
      tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());        //交费日期
      tLJAPayPersonSchema.setPayType("YET");        //交费类型
      tLJAPayPersonSchema.setEnterAccDate(PubFun.getCurrentDate()); //到帐日期
      tLJAPayPersonSchema.setConfDate(PubFun.getCurrentDate());         //确认日期
      tLJAPayPersonSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());  //原交至日期
      tLJAPayPersonSchema.setCurPayToDate(tLCPolSchema.getPaytoDate());    //现交至日期
      tLJAPayPersonSchema.setInInsuAccState("0");//转入保险帐户状态
      tLJAPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());      //复核人编码
      tLJAPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());      //复核日期
      tLJAPayPersonSchema.setSerialNo(serNo);    //流水号

      //--------is it right?------------------------------------
      tLJAPayPersonSchema.setOperator(tLJAGetSchema.getOperator());      //操作员
      //--------------------------------------------------------

      tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());                        //入机日期
      tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());                        //入机时间
      tLJAPayPersonSchema.setGetNoticeNo("00000000000000000000");//通知书号码
      tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());                      //最后一次修改日期
      tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());                      //最后一次修改时间

      tLJAPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());    //管理机构
      tLJAPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());//代理机构
      tLJAPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());      //代理机构内部分类
      tLJAPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());      //险种编码

      aLJAPayPersonSet.add(tLJAPayPersonSchema);

      aLJSGetDrawSet.add(tLJSGetDrawSchema);
      aLJAGetDrawSet.add(tLJAGetDrawSchema);

      aLJSGetSet.add(tLJSGetSchema);
      aLJAGetSet.add(tLJAGetSchema);
      aLCGetSet.add(tLCGetSchema);

      return true;
  }

  private boolean CashProcess(
      LCPolSchema tLCPolSchema,
      LJSGetDrawSchema tLJSGetDrawSchema,
      LJSGetSchema tLJSGetSchema,
      LCGetSchema tLCGetSchema)
  {
      System.out.println("start CashProcess");

      String tActuGetNo=PubFun1.CreateMaxNo("GETNO",PubFun.getNoLimit(tLJSGetDrawSchema.getManageCom())); //参数为机构代码
      mActuGetNo=tActuGetNo;

      LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
      tLJAGetDrawSchema = initLJAGetDrawSchema(tLJSGetDrawSchema);

      LJAGetSchema tLJAGetSchema = new LJAGetSchema();
      tLJAGetSchema = initLJAGetSchema(tLJSGetSchema);

      tLCGetSchema.setGettoDate(tLJSGetDrawSchema.getCurGetToDate());
      tLCGetSchema.setSumMoney(tLCGetSchema.getSumMoney()+tLJSGetDrawSchema.getGetMoney());
      aLJSGetDrawSet.add(tLJSGetDrawSchema);
      aLJAGetDrawSet.add(tLJAGetDrawSchema);

      aLJSGetSet.add(tLJSGetSchema);
      aLJAGetSet.add(tLJAGetSchema);
      aLCGetSet.add(tLCGetSchema);

      return true;
  }
  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    boolean tReturn =true;
    double aActGetMoney =0;
    String tActuGetNo="";
//自动运行接口
    if (mGlobalInput.Operator.equals("AUTOBATCH")&&(mLJSGetDrawSet==null||mLJSGetDrawSet.size()==0))
    {
      mLJSGetDrawSet.clear();
      String sql = "select * from lJSGetDraw where GetDate<='"+PubFun.getCurrentDate()+"' and GetMoney <> 0 and ManageCom like '"+mGlobalInput.ManageCom+"%'";
      System.out.println("----sql"+sql);
      LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
      mLJSGetDrawSet = tLJSGetDrawDB.executeQuery(sql);
    }
    if (mLJSGetDrawSet.size() > 0)
    {
        return tReturn;
    }
    else
    {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LFGetPayBL";
        tError.functionName = "dealData";
        tError.errorMessage = "没有符合条件的应付记录。";
        this.mErrors.addOneError(tError);
        return false;
    }
/*

    if (mLJSGetDrawSet.size()>0)
    {
      aLJSGetDrawSet.clear();
      aLJAGetDrawSet.clear();
      aLJSGetSet.clear();
      aLJAGetSet.clear();
      aLCGetSet.clear();

      for (int i=1;i<=this.mLJSGetDrawSet.size();i++)
      {
        LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
        tLJSGetDrawSchema = mLJSGetDrawSet.get(i);

        if (!processOneLJSGetDraw(tLJSGetDrawSchema))
            return false;

        LFGetPayBLS tLFGetPayBLS = new LFGetPayBLS();




        LJSGetDrawDB tLJSGetDrawDB=new LJSGetDrawDB();
        tLJSGetDrawDB.setSchema(tLJSGetDrawSchema);
        if (!tLJSGetDrawDB.getInfo())
        {
         this.mErrors.copyAllErrors(tLJSGetDrawDB.mErrors);
         return false;
        }

        tActuGetNo=PubFun1.CreateMaxNo("GETNO",PubFun.getNoLimit(tLJSGetDrawDB.getManageCom())); //参数为机构代码
        mActuGetNo=tActuGetNo;

        tLJSGetDrawSchema = tLJSGetDrawDB.getSchema();
        LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
        tLJAGetDrawSchema = initLJAGetDrawSchema(tLJSGetDrawSchema);

        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(tLJSGetDrawSchema.getGetNoticeNo());
        if (!tLJSGetDB.getInfo())
        {
          this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
          return false;
        }

        LJSGetSchema tLJSGetSchema = new LJSGetSchema();
        tLJSGetSchema.setSchema(tLJSGetDB.getSchema());


        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema = initLJAGetSchema(tLJSGetSchema);

        LCGetSchema tLCGetSchema = new LCGetSchema();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(tLJSGetDrawSchema.getPolNo());
        tLCGetDB.setDutyCode(tLJSGetDrawSchema.getDutyCode());
        tLCGetDB.setGetDutyCode(tLJSGetDrawSchema.getGetDutyCode());
        if (!tLCGetDB.getInfo())
        {
           this.mErrors.copyAllErrors(tLCGetDB.mErrors);
           return false;
        }
        tLCGetSchema.setSchema(tLCGetDB.getSchema());
        tLCGetSchema.setGettoDate(tLJSGetDrawSchema.getCurGetToDate());
        tLCGetSchema.setSumMoney(tLCGetSchema.getSumMoney()+tLJSGetDrawSchema.getGetMoney());

        aLJSGetDrawSet.add(tLJSGetDrawSchema);
        aLJAGetDrawSet.add(tLJAGetDrawSchema);

        aLJSGetSet.add(tLJSGetSchema);
        aLJAGetSet.add(tLJAGetSchema);
        aLCGetSet.add(tLCGetSchema);

      }
    }
    else
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="dealData";
      tError.errorMessage="没有符合条件的应付记录。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
*/

  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    TransferData aTransferData = new TransferData();
    //全局变量
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLJSGetDrawSet.set((LJSGetDrawSet)cInputData.getObjectByObjectName("LJSGetDrawSet",0));
    aTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
   // mDrawer = (String)aTransferData.getValueByName("Drawer");
   // mDrawerID = (String)aTransferData.getValueByName("DrawerID");

    return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    //mInputData=new VData();
    try
    {
      mInputData=new VData();
      mInputData.addElement(mGlobalInput);
      mInputData.addElement(aLJSGetSet);
      mInputData.addElement(aLJAGetSet);
      mInputData.addElement(aLJSGetDrawSet);
      mInputData.addElement(aLJAGetDrawSet);
      mInputData.addElement(aLCGetSet);

      mInputData.addElement(aLJFIGetSet);
      mInputData.addElement(aLJTempFeeClassSet);
      mInputData.addElement(aLJTempFeeSet);
      mInputData.addElement(aLJAPaySet);
      mInputData.addElement(aLJAPayPersonSet);
      mInputData.addElement(aLCInsureAccSet);
      mInputData.addElement(aLCInsureAccTraceSet);

    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  private boolean checkData()
  {
    if (mGlobalInput==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="prepareData";
      tError.errorMessage="请重新登陆页面。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if (mLJSGetDrawSet==null||mLJSGetDrawSet.size()==0)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="prepareData";
      tError.errorMessage="请选择领取责任。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private LJAGetSchema initLJAGetSchema(LJSGetSchema aLJSGetSchema)
  {
    LJAGetSchema aLJAGetSchema = new LJAGetSchema();
    Reflections tReflections = new Reflections();
    //此处增加一些计算代码
    tReflections.transFields(aLJAGetSchema,aLJSGetSchema);
    aLJAGetSchema.setActuGetNo(mActuGetNo);
    //aLJAGetSchema.setDrawer(mDrawer);
    //aLJAGetSchema.setDrawerID(mDrawerID);
    aLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
    aLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
    aLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
    aLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
    aLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
    return aLJAGetSchema;
  }

  private LJAGetDrawSchema initLJAGetDrawSchema(LJSGetDrawSchema aLJSGetDrawSchema)
  {
    LJAGetDrawSchema aLJAGetDrawSchema = new LJAGetDrawSchema();
    Reflections tReflections = new Reflections();
    //此处增加一些计算代码
    tReflections.transFields(aLJAGetDrawSchema,aLJSGetDrawSchema);
    aLJAGetDrawSchema.setActuGetNo(mActuGetNo);
    aLJAGetDrawSchema.setGetDate(PubFun.getCurrentDate());
    aLJAGetDrawSchema.setMakeDate(PubFun.getCurrentDate());
    aLJAGetDrawSchema.setMakeTime(PubFun.getCurrentTime());
    aLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
    aLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());
    System.out.print("---modifyTime:"+aLJAGetDrawSchema.getModifyTime());
    return aLJAGetDrawSchema;
  }


  public static void main(String[] args) {
    VData tVData = new VData();
    GlobalInput tG = new GlobalInput();
    //LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    tG.Operator="AUTOBATCH";
    tG.ManageCom="001";
    tVData.addElement(tG);
    //tVData.addElement(tLJSGetSchema);
    LFGetPayBL tLFGetPayBL = new LFGetPayBL();
    tLFGetPayBL.submitData(tVData,"INSERT||PERSON");

  }
}
