package com.sinosoft.lis.get;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.util.*;
import java.lang.String;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class ApplyGetAccumulateUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mStrLJAGetSet = new String();
  private String mOperate;
  private String mResultQueryString = "";

  public ApplyGetAccumulateUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;
	ApplyGetAccumulateBL tApplyGetAccumulateBL = new ApplyGetAccumulateBL();
	System.out.println("---UI BEGIN---"+mOperate);
	if (tApplyGetAccumulateBL.submitData(cInputData,mOperate) == false)
	{
	  // @@错误处理
	  this.mErrors.copyAllErrors(tApplyGetAccumulateBL.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "PRnewAppCancelUI";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据撤销失败!";
//      this.mErrors .addOneError(tError) ;
//      mResult.clear();
	  return false;
	}
	mResult = tApplyGetAccumulateBL.getResult();
    mStrLJAGetSet= tApplyGetAccumulateBL.getLJAGetSet();


	return true;
  }

  public VData getQueryResult()
  {
	return mResult;
  }

  public String getLJAGetSet()
  {
	 return mStrLJAGetSet;
  }
  public static void main(String[] args)
  {

  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ManageCom = "86";
  tG.ComCode = "86";
  LCInsureAccSchema mLCInsureAccSchema   = new LCInsureAccSchema();

  mLCInsureAccSchema.setPolNo("86110020030210004195");
  mLCInsureAccSchema.setInsuAccNo("000001");
//  mLCInsureAccSchema.setOtherNo("86110020030210004195");
  TransferData tTransferData= new TransferData();
  tTransferData.setNameAndValue("ApplyMoney","1000");
	VData tVData = new VData();

  tVData.add( mLCInsureAccSchema );
  tVData.add(tTransferData) ;
  tVData.add( tG );
  ApplyGetAccumulateUI ui = new ApplyGetAccumulateUI();
  if( ui.submitData( tVData, "CalCurrentMoney" ) == true )
	  System.out.println("---ok---");
  else
	  System.out.println("---NO---");
	System.out.println("-------test...");
  }
}
