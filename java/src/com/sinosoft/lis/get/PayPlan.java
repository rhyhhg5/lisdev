/**
 * <p>Title: PayPlan.java</p>
 * <p>Description: 自动催付程序</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.get;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.get.*;
import com.sinosoft.utility.*;
import java.util.*;
import java.text.*;

public class PayPlan {
    public static void main(String[] args) {
        CErrors tError = null;
        String tBmCert = "";
        String tRela  = "";
        String FlagStr = "";
        String Content = "";
        String Count = "";
        System.out.println("Start PayPlan JSP Submit...");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode  ="001";
        tGlobalInput.ManageCom="86";

        String PolNo="";    //保单号码
        String GrpPolNo=""; //集体保单号码
        String ManageCom="";//业务区站
        String AppntNo="";  //投保人客户号码
        String InsuredNo="";//被保人客户号码

        String timeEnd = "";//催付结束日期
        String pattern="yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date today = new Date();
        Date finday;
        finday=PubFun.calDate(today,2,"M",new Date());
        timeEnd = df.format(finday);
        LCPolSchema tLCPolSchema=new LCPolSchema();
        tLCPolSchema.setPolNo(PolNo);
        tLCPolSchema.setGrpPolNo(GrpPolNo);
        tLCPolSchema.setManageCom(ManageCom);
        tLCPolSchema.setAppntNo(AppntNo);
        tLCPolSchema.setInsuredNo(InsuredNo);

        LCGetSchema tLCGetSchema=new LCGetSchema();
        tLCGetSchema.setPolNo(PolNo);
        /*Lis5.3 upgrade set
        tLCGetSchema.setGrpPolNo(GrpPolNo);
        tLCGetSchema.setAppntNo(AppntNo);
        */
        tLCGetSchema.setManageCom(ManageCom);
        tLCGetSchema.setInsuredNo(InsuredNo);

        TransferData aTransferData = new TransferData();
        aTransferData.setNameAndValue("timeEnd",timeEnd);
        PayPlanUI tPayPlanUI = new PayPlanUI();

        VData tVData = new VData();
        tVData.addElement(tGlobalInput) ;
        tVData.addElement(aTransferData);
        tVData.addElement(tLCPolSchema);
        tVData.addElement(tLCGetSchema);

        System.out.println("Start PayPlan  Submit...");
        try{
             tPayPlanUI.submitData(tVData,"INSERT||PERSON");
        }
        catch(Exception ex){
             Content = "保存失败，原因是:" + ex.toString();
             FlagStr = "Fail";
        }

        //如果在Catch中发现异常，则不从错误类中提取错误信息
        if (FlagStr==""){
            tError = tPayPlanUI.mErrors;
            if (!tError.needDealError()){
                 Content = " 保存成功";
                 if (tPayPlanUI.getResult()!=null&&tPayPlanUI.getResult().size()>0) {
                      System.out.println("------SerialNo:"+tPayPlanUI.getResult().get(0));
                      FlagStr = (String)tPayPlanUI.getResult().get(0);
                      Count = (String)tPayPlanUI.getResult().get(1);
                 }
                 else{
                      Content = "催付失败：已经产生催付记录！";
                      FlagStr = "Fail";
                 }

          }
          else{
                  Content = " 保存失败，原因是:" + tError.getFirstError();
                  FlagStr = "Fail";
          }
       }
    }
}
