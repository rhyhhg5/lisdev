package com.sinosoft.lis.get;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;
import java.lang.*;
import com.sinosoft.lis.get.PayPlanBLS;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author TJJ
 * @version 1.0
 */
public class PayPlanBL  {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 往后面传输数据的容器 */
  private VData mResult;
  /** 数据操作字符串 */
  private String mOperate;
  /**控制信息传输类*/
  private TransferData mTransferData = new TransferData();
  /**参数描述*/
  private String mTimeStart,mTimeEnd,mSerialNo;
  /**给付至日期*/
  private Date mGetToDate;
  /**给付金额*/
  private double mGetMoney;
  /**销户标志*/
  private String mCancelFlag;

  /** 业务处理相关变量 */
  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private LCGetSchema mLCGetSchema = new LCGetSchema();
  private LCGetToAccBL mLCGetToAccBL=new LCGetToAccBL();
  private LCGetSet mLCGetSet=new LCGetSet();
  private LJSGetSet mLJSGetSet=new LJSGetSet();
  private LJSGetDrawSet mLJSGetDrawSet=new LJSGetDrawSet();

  private LCPolSchema mLCPolSchema=new LCPolSchema();
  private LCInsuredSchema mLCInsuredSchema=new LCInsuredSchema();
  private LCPolSchema aLCPolSchema = new LCPolSchema();
  private LMDutyGetAliveSchema mLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
  private LMDutyGetSchema mLMDutyGetSchema = new LMDutyGetSchema();

  public PayPlanBL() {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //进行业务处理
    if (!dealData(cInputData))
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayPlanBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败PayPlanBL-->dealData!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    //准备往后台的数据
    if (!prepareData())
      return false;
    System.out.println("Start PayPlan BLS Submit...");

    PayPlanBLS tPayPlanBLS=new PayPlanBLS();
    tPayPlanBLS.submitData(mInputData,cOperate);

    System.out.println("End PayPlan BLS Submit...");
    //如果有需要处理的错误，则返回
    if (tPayPlanBLS.mErrors.needDealError())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPayPlanBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "PayPlanBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    mInputData=null;
    return true;
  }

  /**
   * 初始化查询条件
   * @param Schema
   * @return
   */
  private String initCondGet()
  {
    String aSql = "select * from LCGet where 1 = 1";
    if (mLCGetSchema.getManageCom()==null||mLCGetSchema.getManageCom().trim().equals(""))
      aSql = aSql + " and ManageCom like '"+mGlobalInput.ManageCom+"%'";
    else
      aSql = aSql + " and ManageCom like '"+mLCGetSchema.getManageCom()+"%'";

    if (mLCGetSchema.getGrpContNo()!=null&&!mLCGetSchema.getGrpContNo().trim().equals(""))
      aSql = aSql + " and GrpContNo= '"+mLCGetSchema.getGrpContNo()+"'";


    if (mLCGetSchema.getPolNo()!=null&&!mLCGetSchema.getPolNo().trim().equals(""))
      aSql = aSql + " and PolNo= '"+mLCGetSchema.getPolNo()+"'";
    /*Lis5.3 upgrade get
    if (mLCGetSchema.getAppntNo()!=null&&!mLCGetSchema.getAppntNo().trim().equals(""))
      aSql = aSql + " and AppntNo= '"+mLCGetSchema.getAppntNo()+"'";
    */
    if (mLCGetSchema.getInsuredNo()!=null&&!mLCGetSchema.getInsuredNo().trim().equals(""))
      aSql = aSql + " and InsuredNo= '"+mLCGetSchema.getInsuredNo()+"'";



    //if (!(mTimeStart== null&&mTimeStart.trim().equals("")))
    // aSql = aSql + " and gettodate>='"+mTransferData.getValueByName("timeStart")+"'";

    if (!(mTimeEnd== null&&mTimeEnd.trim().equals("")))
      aSql = aSql + " and gettodate<='"+mTransferData.getValueByName("timeEnd")+"'";
      aSql = aSql + " and getenddate>='"+mTransferData.getValueByName("timeEnd")+"'";
    aSql = aSql + " and LiveGetType='0' and  UrgeGetFlag = 'Y' order by PolNo";
    System.out.println(aSql);
    return aSql;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  public boolean dealData(VData cInputData)
  {
    boolean tReturn =true;
    String tLimit,tGetNoticeNo;
    String condSql;

    LJSGetSchema tLJSGetSchema=new LJSGetSchema();
    LJSGetDrawSchema tLJSGetDrawSchema=new LJSGetDrawSchema();
    LCPolSet tLCPolSet=new LCPolSet();
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

//初始化催付查询条件
    condSql = initCondGet();
    System.out.println(condSql);
    LCGetDB tLCGetDB=new LCGetDB();
    FDate tFDate = new FDate();
    tLCGetDB.setSchema(mLCGetSchema);

    mLCGetSet=tLCGetDB.executeQuery(condSql);

    int tCount=mLCGetSet.size();
    double tGetMoney=0;

    if (tCount>0)
    {
      boolean tSucc = false;
      String aPolNo = null;

      String tSerialNo = PubFun1.CreateMaxNo("SERIALNO",PubFun.getNoLimit(mGlobalInput.ManageCom));
      mSerialNo = tSerialNo;
      System.out.println("--------------serialNo:"+mSerialNo);
      boolean mAppFlag = true;
      System.out.println("---tCount :" + tCount);
      for (int j=1;j<=tCount;j++)
      {
        //判断该给付责任是否已经催付
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();

        tLJSGetDrawDB.setPolNo(mLCGetSet.get(j).getPolNo());
        tLJSGetDrawDB.setDutyCode(mLCGetSet.get(j).getDutyCode());
        tLJSGetDrawDB.setGetDutyCode(mLCGetSet.get(j).getGetDutyCode());
        tLJSGetDrawDB.setGetDutyKind(mLCGetSet.get(j).getGetDutyKind());
        tLJSGetDrawSet.clear();
        tLJSGetDrawSet = tLJSGetDrawDB.query();
        if (tLJSGetDrawSet.size()==0)
        {
          if (j==1||!mLCGetSet.get(j).getPolNo().equals(aPolNo))
          {
            aPolNo= mLCGetSet.get(j).getPolNo();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(aPolNo);
            if (!tLCPolDB.getInfo())
              return false;
            tLCPolSchema.setSchema(tLCPolDB.getSchema());
            if (!tLCPolSchema.getAppFlag().trim().equals("1"))
            {
              mAppFlag = false;
            }
            else
              mAppFlag = true;

            this.setPolInfo(tLCPolSchema);
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            /*Lis5.3 upgrade set*/
            tLCInsuredDB.setContNo(tLCPolSchema.getContNo());
            tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());

            if (!tLCInsuredDB.getInfo())
            {
              CError tError = new CError();
              tError.moduleName = "PayPlanBL";
              tError.functionName = "submitData";
              tError.errorMessage = "被保人信息不足无法计算!";
              this.mErrors .addOneError(tError) ;
              return false;
            }
            tLCInsuredSchema.setSchema(tLCInsuredDB.getSchema());
            this.setInsuredInfo(tLCInsuredSchema);
          }
          System.out.println("mAppFlag :" + mAppFlag);
          if (mAppFlag==false)
            continue;

          //校验各种限制条件同时准备数据
          if (!this.checkDataToGet(mLCGetSet.get(j)))
            return false;

          tLJSGetDrawSchema=new LJSGetDrawSchema();
          //生成给付子表的信息
          //计算给付至日期
          //计算应给付金额
          tLJSGetDrawSchema.setGetMoney(this.calGetMoney(mLCGetSet.get(j)));
          String tCancelFlag;
          System.out.println(this.getCancelFlag());
          tCancelFlag = this.getContCancelFlag(this.getCancelFlag());
          if (tCancelFlag==null)
          {
            tCancelFlag = "0";
          }
          tLJSGetDrawSchema.setDestrayFlag(this.getContCancelFlag(this.getCancelFlag()));

          Date aGetDate = this.getGetToDate();
          System.out.println("-----GetDate:"+aGetDate);
          //生成通知
          tGetNoticeNo=PubFun1.CreateMaxNo("GetNoticeNo",PubFun.getNoLimit(tLCPolSchema.getManageCom()));
          System.out.println("========crt max NO======"+tGetNoticeNo);
          tLJSGetDrawSchema.setGetNoticeNo(tGetNoticeNo);
          tLJSGetDrawSchema.setSerialNo(tSerialNo);
          tLJSGetDrawSchema.setGetDate(mLCGetSet.get(j).getGettoDate());
          tLJSGetDrawSchema.setDutyCode(mLCGetSet.get(j).getDutyCode());
          tLJSGetDrawSchema.setGetDutyCode(mLCGetSet.get(j).getGetDutyCode());
          tLJSGetDrawSchema.setGetDutyKind(mLCGetSet.get(j).getGetDutyKind());
          tLJSGetDrawSchema.setLastGettoDate(mLCGetSet.get(j).getGettoDate());

          tLJSGetDrawSchema.setFeeFinaType("LIVGET");
          tLJSGetDrawSchema.setFeeOperationType("TF");

          tLJSGetDrawSchema.setCurGetToDate(aGetDate);
          //是否首期给付
          if (mLCGetSet.get(j).getGetStartDate().equals(mLCGetSet.get(j).getGettoDate()))
            tLJSGetDrawSchema.setGetFirstFlag("1");
          else
            tLJSGetDrawSchema.setGetFirstFlag("0");

          //给付渠道
          tLJSGetDrawSchema.setGetChannelFlag("1");

          tLJSGetDrawSchema.setAppntNo(tLCPolSchema.getAppntNo());
          tLJSGetDrawSchema.setPolNo(tLCPolSchema.getPolNo());
          tLJSGetDrawSchema.setGrpName(tLCPolSchema.getAppntName());
          tLJSGetDrawSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
          tLJSGetDrawSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
          tLJSGetDrawSchema.setContNo(tLCPolSchema.getContNo());
          tLJSGetDrawSchema.setPolNo(tLCPolSchema.getPolNo());
          tLJSGetDrawSchema.setPolType("1");
          tLJSGetDrawSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
          tLJSGetDrawSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJSGetDrawSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJSGetDrawSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          tLJSGetDrawSchema.setAgentType(tLCPolSchema.getAgentType());
          tLJSGetDrawSchema.setRiskCode(tLCPolSchema.getRiskCode());
          tLJSGetDrawSchema.setKindCode(tLCPolSchema.getKindCode());
          tLJSGetDrawSchema.setRiskVersion(tLCPolSchema.getRiskVersion());

          tLJSGetDrawSchema.setMakeDate(PubFun.getCurrentDate());
          tLJSGetDrawSchema.setMakeTime(PubFun.getCurrentTime());
          tLJSGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
          tLJSGetDrawSchema.setModifyTime(PubFun.getCurrentTime());
          tLJSGetDrawSchema.setOperator(mGlobalInput.Operator);
          tLJSGetDrawSchema.setManageCom(tLCPolSchema.getManageCom());

          mLJSGetDrawSet.add(tLJSGetDrawSchema);
          //总表与子表信息量一致
          tLJSGetSchema =new LJSGetSchema();

          tLJSGetSchema.setGetNoticeNo(tGetNoticeNo);
          tLJSGetSchema.setSerialNo(tSerialNo);
          tLJSGetSchema.setAppntNo(tLCPolSchema.getAppntNo());
          tLJSGetSchema.setOtherNo(tLCPolSchema.getPolNo());
          tLJSGetSchema.setOtherNoType("2");
          tLJSGetSchema.setAgentCode(tLCPolSchema.getAgentCode());
          tLJSGetSchema.setAgentCom(tLCPolSchema.getAgentCom());
          tLJSGetSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
          tLJSGetSchema.setAgentType(tLCPolSchema.getAgentType());

          tLJSGetSchema.setSumGetMoney(tLJSGetDrawSchema.getGetMoney());
          //待处理
          tLJSGetSchema.setGetDate(tLJSGetDrawSchema.getLastGettoDate());

          tLJSGetSchema.setMakeDate(PubFun.getCurrentDate());
          tLJSGetSchema.setMakeTime(PubFun.getCurrentTime());
          tLJSGetSchema.setModifyDate(PubFun.getCurrentDate());
          tLJSGetSchema.setModifyTime(PubFun.getCurrentTime());
          tLJSGetSchema.setOperator(mGlobalInput.Operator);
          tLJSGetSchema.setManageCom(tLCPolSchema.getManageCom());
          mLJSGetSet.add(tLJSGetSchema);
        }
        else
        {
          System.out.println("--------------already procedure LJSGetDraw Info--------");
        }
      }
    }
    else
    {
      CError tError = new CError();
      tError.moduleName = "PayPlanBL";
      tError.functionName = "dealdata";
      tError.errorMessage = "没有相关条件的记录!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return tReturn ;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    //全局变量
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLCPolSchema.setSchema((LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0));
    mLCGetSchema.setSchema((LCGetSchema)cInputData.getObjectByObjectName("LCGetSchema",0));
    mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
    //mTimeStart = (String)mTransferData.getValueByName("timeStart");
    mTimeEnd = (String)mTransferData.getValueByName("timeEnd");
    return true;
  }
  /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 public boolean getEdorData(VData cInputData)
 {
   mLCGetSchema.setSchema((LCGetSchema)cInputData.getObjectByObjectName("LCGetSchema",0));
   mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
   mTimeEnd = (String)mTransferData.getValueByName("timeEnd");
   return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareData()
  {
    mInputData=new VData();
    try
    {
      // mInputData.add(this.mLCGetSet);
      mInputData.add(this.mLJSGetDrawSet);
      mInputData.add(this.mLJSGetSet);
      mInputData.add(this.mGlobalInput);

      if (mLJSGetSet!=null&&mLJSGetSet.size()>0)
      {
        mResult = new VData();
        mResult.addElement(mSerialNo);
        mResult.addElement(String.valueOf(mLJSGetSet.size()));
      }
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
//领取时给付校验
  private boolean checkDataToGet(LCGetSchema aLCGetSchema)
  {
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    tLCPolSchema = this.getPolInfo();

    //判断是否为生存给付责任,并准备给付责任数据
    LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
    tLMDutyGetDB.setGetDutyCode(aLCGetSchema.getGetDutyCode());
    if(!tLMDutyGetDB.getInfo())
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="checkDataToGet";
      tError.errorMessage="无给付责任描述数据。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    this.setDutyGetInfo(tLMDutyGetDB.getSchema());

    //检查生存给付描述表是否存在,并准备给付责任数据。
    LMDutyGetAliveDB tLMDutyGetAliveDB = new LMDutyGetAliveDB();
    tLMDutyGetAliveDB.setGetDutyCode(aLCGetSchema.getGetDutyCode());
    tLMDutyGetAliveDB.setGetDutyKind(aLCGetSchema.getGetDutyKind());
    if (!tLMDutyGetAliveDB.getInfo())
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LFGetPayBL";
      tError.functionName="checkDataToGet";
      tError.errorMessage="无生存给付描述数据。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    this.setGetAliveInfo(tLMDutyGetAliveDB.getSchema());
    tLMDutyGetAliveSchema.setSchema(tLMDutyGetAliveDB.getSchema());
    //校验责任领取条件
    if (tLMDutyGetAliveSchema.getGetCond().equals("0"))
    {
      if (tLCPolSchema.getPayEndDate().compareTo(aLCGetSchema.getGetStartDate())<0)
      {
        CError tError = new CError();
        tError.moduleName = "PayPlanBL";
        tError.functionName = "checkDataToGet";
        tError.errorMessage = "该责任不满足领取条件（交费不足）!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }
    return true;
  }
//计算新的领至日期
  public Date calGetToDate(LCGetSchema aLCGetSchema)
  {
    Date aGetToDate;
    FDate tFDate = new FDate();

    String aLastToDate = aLCGetSchema.getGettoDate();
    String aGetStartDate = aLCGetSchema.getGetStartDate();
    Date a = tFDate.getDate(aLastToDate);
    Date b = tFDate.getDate(aGetStartDate);
//趸领记成原领至日期即起领日期；
    aGetToDate = PubFun.calDate( a,aLCGetSchema.getGetIntv(),"M",b);
    return aGetToDate;
  }

//计算给付金对应的给付金额
  public double calGetMoney(LCGetSchema aLCGetSchema)
  {
    double aGetMoney=0;
    BqCalBase aBqCalBase = new BqCalBase();
    LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    LCPolSchema tLCPolSchema = new LCPolSchema();
    FDate tFDate = new FDate();
    String aCalCode;

    tLMDutyGetAliveSchema = getGetAliveInfo();
    tLCPolSchema.setSchema(this.getPolInfo());



//趸领生存给付计算
    if (aLCGetSchema.getGetIntv()==0)
    {
      if (aLCGetSchema.getSumMoney()>0&&aLCGetSchema.getSumMoney()>0)
      {
        this.setGetToDate(tFDate.getDate(aLCGetSchema.getGetEndDate()));
      }
      else
      {
        if (tLMDutyGetAliveSchema.getOthCalCode()==null||tLMDutyGetAliveSchema.getOthCalCode().trim().equals(""))
        {
          aGetMoney = aLCGetSchema.getActuGet();
          this.setGetToDate(tFDate.getDate(aLCGetSchema.getGetEndDate()));
        }
        else
        {
          aBqCalBase = new BqCalBase();
          aBqCalBase = initCalBase(aLCGetSchema);
          aCalCode = tLMDutyGetAliveSchema.getOthCalCode();
          System.out.println("aCalCode :" + aCalCode);
          BqCalBL tBqCalBL = new BqCalBL(aBqCalBase,aCalCode,"");
          aGetMoney = tBqCalBL.calGetDraw();
          this.setGetToDate(tFDate.getDate(aLCGetSchema.getGetEndDate()));
        }
      }
    }
    else
    {
      //期领责任给付计算
      LCGetSchema tLCGetSchema = new LCGetSchema();
      tLCGetSchema.setSchema(aLCGetSchema);
      while (tLCGetSchema.getGettoDate().compareTo(mTimeEnd)<=0)
      {
        System.out.println("---oridate:"+tLCGetSchema.getGettoDate());
        System.out.println("---Todate:"+mTimeEnd);
        aBqCalBase = new BqCalBase();
        aBqCalBase = initCalBase(tLCGetSchema);

        if (tLMDutyGetAliveSchema.getMaxGetCount()==0)
        {
          if (tLCGetSchema.getGettoDate().compareTo(tLCGetSchema.getGetEndDate())>0)
          {
            this.setCancelFlag("1");
            break;
          }
        }
        else
        {
          //无条件给付最大次数
          if (tLMDutyGetAliveSchema.getMaxGetCountType().equals("0")&&aBqCalBase.getGetTimes().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0)
          {
            this.setCancelFlag("1");
            break;
          }
          //无条件给付最大年龄
          else  if (tLMDutyGetAliveSchema.getMaxGetCountType().equals("1")&&aBqCalBase.getGetAge().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0)
          {
            this.setCancelFlag("1");
            break;
          }
          //被保人死亡标志给付最大次数
          else  if (tLCPolSchema.getDeadFlag().equals("1")&&tLMDutyGetAliveSchema.getMaxGetCountType().equals("2")&&aBqCalBase.getGetTimes().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0 )
          {
            this.setCancelFlag("1");
            break;
          }
          //被保人死亡标志给付最大年龄
          else  if (tLCPolSchema.getDeadFlag().equals("1")&&tLMDutyGetAliveSchema.getMaxGetCountType().equals("3")&&aBqCalBase.getGetAge().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0 )
          {
            this.setCancelFlag("1");
            break;
          }
          //投保人死亡标志给付最大次数
          else  if (tLCPolSchema.getDeadFlag().equals("2")&&tLMDutyGetAliveSchema.getMaxGetCountType().equals("4")&&aBqCalBase.getGetTimes().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0 )
          {
            this.setCancelFlag("1");
            break;
          }
          //投保人死亡标志给付最大年龄
          else  if (tLCPolSchema.getDeadFlag().equals("2")&&tLMDutyGetAliveSchema.getMaxGetCountType().equals("5")&&aBqCalBase.getGetAge().compareTo(String.valueOf(tLMDutyGetAliveSchema.getMaxGetCount()))>0 )
          {
            this.setCancelFlag("1");
            break;
          }
          else
          {
            System.out.println("------------------no descriptions----------");;
          }
        }

        //计算应领金额
        if (tLMDutyGetAliveSchema.getOthCalCode()==null||tLMDutyGetAliveSchema.getOthCalCode().equals("XXXXXX")||tLMDutyGetAliveSchema.getOthCalCode().trim().equals(""))
        {
          //非递增
          if (tLMDutyGetAliveSchema.getAddFlag().equals("N"))
          {
            aGetMoney = aGetMoney + tLCGetSchema.getActuGet();
          }
          else
          {
            aGetMoney  = aGetMoney + calAddGetMoney(aBqCalBase,tLCGetSchema);
          }
        }
        else
        {
          aCalCode = tLMDutyGetAliveSchema.getOthCalCode();
          BqCalBL tBqCalBL = new BqCalBL(aBqCalBase,aCalCode,"");
          aGetMoney = aGetMoney + tBqCalBL.calGetDraw();
        }
        //  计算应领至日期
        tLCGetSchema.setGettoDate(this.calGetToDate(tLCGetSchema));
        this.setGetToDate(tFDate.getDate(tLCGetSchema.getGettoDate()));
        System.out.println("---oridate:"+tLCGetSchema.getGettoDate());
      }
      System.out.println("------end while-----");
    }
    System.out.println("-------getMoney:"+aGetMoney);
    return aGetMoney;
  }

  private BqCalBase initCalBase(LCGetSchema aLCGetSchema)
  {
  	System.out.println("......initCalBase ");
    BqCalBase aBqCalBase = new BqCalBase();
    FDate tFDate = new FDate();
    int aGetYears,aLastGetTimes,aCurGetTimes,aGetAge,aAppYears;
    String aGetIntv;
    Date tGetToDate,aGetStartDate;
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

    if (aLCGetSchema.getGetIntv()==0)
    {
      tGetToDate = tFDate.getDate(mTimeEnd);
    }
    else
    {
      tGetToDate = this.calGetToDate(aLCGetSchema);
    }
    aGetStartDate = tFDate.getDate(aLCGetSchema.getGetStartDate());
    aGetIntv = String.valueOf(aLCGetSchema.getGetIntv());

    //本次是第几年给付（领取时的领取年期)
    aGetYears = PubFun.calInterval(aGetStartDate,tGetToDate,"Y");
    // 上次是第几次给付
    aLastGetTimes = PubFun.calInterval(aLCGetSchema.getGetStartDate(),aLCGetSchema.getGettoDate(),aGetIntv) + 1;
    //本次是第几次给付
    aCurGetTimes = PubFun.calInterval(aGetStartDate,tGetToDate,aGetIntv) + 1;
    //领取时被保人年龄,投保年期
    tLCPolSchema = this.getPolInfo();
    tLCInsuredSchema = this.getInsuredInfo();
    //投保年龄，已交费年期，始领年龄，始领年期
    int aAppAge,aPayYear,aGetStartAge,aGetStartYear;
    /*Lis5.3 upgrade get
    aGetAge = PubFun.calInterval(tFDate.getDate(tLCInsuredSchema.getBirthday()),tGetToDate,"Y");
    aAppAge = PubFun.calInterval(tLCInsuredSchema.getBirthday(),tLCPolSchema.getCValiDate(),"Y");
    aGetStartAge = PubFun.calInterval(tLCInsuredSchema.getBirthday(),aLCGetSchema.getGetStartDate(),"Y");
    */
    aGetAge=0;
    aAppAge=0;
    aGetStartAge=0;
    aAppYears = PubFun.calInterval(tFDate.getDate(tLCPolSchema.getCValiDate()),tGetToDate,"Y");
    aPayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),tLCPolSchema.getPaytoDate(),"Y");
    aGetStartYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),aLCGetSchema.getGetStartDate(),"Y");

    //得到getblance值
    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
    tLCInsureAccDB.setPolNo(aLCGetSchema.getPolNo());
    tLCInsureAccDB.setRiskCode(tLCPolSchema.getRiskCode());
    tLCInsureAccDB.setAccType("003");
    LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
    System.out.println("tLCInsureAccSet :" + tLCInsureAccSet.size());
    double aGetBalance = 0.0;

    for (int i = 1; i <= tLCInsureAccSet.size(); i++)
        aGetBalance += tLCInsureAccSet.get(i).getInsuAccGetMoney();



    aBqCalBase.setAppAge(aAppAge);
    /*Lis5.3 upgrade get
    aBqCalBase.setGrpPolNo(aLCGetSchema.getGrpPolNo());
    */
    aBqCalBase.setAddRate(aLCGetSchema.getAddRate());
    aBqCalBase.setGet(aLCGetSchema.getActuGet());
    aBqCalBase.setGetIntv(aLCGetSchema.getGetIntv());
    aBqCalBase.setGDuty(aLCGetSchema.getGetDutyCode());
    aBqCalBase.setGetStartDate(aLCGetSchema.getGetStartDate());

    aBqCalBase.setGetStartYear(aGetStartYear);
    aBqCalBase.setGetStartAge(aGetStartAge);
    aBqCalBase.setGetAge(aGetAge);
    aBqCalBase.setGetTimes(aCurGetTimes);
    aBqCalBase.setPayEndYear(aPayYear);
    aBqCalBase.setGetYear(aGetYears);
    aBqCalBase.setGetAppYear(aAppYears);
    aBqCalBase.setInterval(1);
    aBqCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());

    aBqCalBase.setMult(tLCPolSchema.getMult());
    aBqCalBase.setPayIntv(tLCPolSchema.getPayIntv());
    aBqCalBase.setGetIntv(aLCGetSchema.getGetIntv());
    aBqCalBase.setPolNo(tLCPolSchema.getPolNo());
    aBqCalBase.setCValiDate(tLCPolSchema.getCValiDate());
    aBqCalBase.setYears(tLCPolSchema.getYears());
    aBqCalBase.setPrem(tLCPolSchema.getPrem());
    /*Lis5.3 upgrade get
    aBqCalBase.setSex(tLCInsuredSchema.getSex());
    */
    aBqCalBase.setJob(tLCInsuredSchema.getOccupationCode());

    aBqCalBase.setGetBalance(aGetBalance);

    this.displayObject(aBqCalBase);
    return aBqCalBase;
  }

//递增给付领取处理
  private double calAddGetMoney(BqCalBase aBqCalBase,LCGetSchema aLCGetSchema)
  {
    double aGetMoney = 0;
    LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    BqCalBase tBqCalBase = new BqCalBase();
    tLMDutyGetAliveSchema.setSchema(this.getGetAliveInfo());
    int aGetAge,aGetAppYear,aGetYear,aTimes;

    aGetAge = Integer.parseInt(aBqCalBase.getGetAge());
    aGetAppYear = Integer.parseInt(aBqCalBase.getGetAppYear());
    aGetYear = Integer.parseInt(aBqCalBase.getGetYear());
    aTimes = 0;

    if (aLCGetSchema.getAddRate()>0)
    {
      tLMDutyGetAliveSchema.setAddValue(aLCGetSchema.getAddRate());
    }
    //递增开始年龄
    if (tLMDutyGetAliveSchema.getAddStartUnit().equals("0"))
    {
      if (aGetAge<tLMDutyGetAliveSchema.getAddStartPeriod())
      {
        aGetMoney = aLCGetSchema.getActuGet();
      }
      else
      {
        if (aGetAge>tLMDutyGetAliveSchema.getAddEndPeriod())
        {
          aGetAge =tLMDutyGetAliveSchema.getAddEndPeriod();
        }

        aGetAge = aGetAge - tLMDutyGetAliveSchema.getAddStartPeriod() + 1;
        //递增间隔单位默认为年
        if (aGetAge%tLMDutyGetAliveSchema.getAddIntv()!=0)
        {
          aTimes = aGetAge/tLMDutyGetAliveSchema.getAddIntv() + 1;
        }
        else
        {
          aTimes = aGetAge/tLMDutyGetAliveSchema.getAddIntv();
        }
        aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),tLMDutyGetAliveSchema.getAddValue(),aTimes,tLMDutyGetAliveSchema.getAddType());
      }
    }
    //领取递增开始年期,领取后多少年开始递增
    else if (tLMDutyGetAliveSchema.getAddStartUnit().equals("1"))
    {
      if (aGetYear<tLMDutyGetAliveSchema.getAddStartPeriod())
      {
        aGetMoney = aLCGetSchema.getActuGet();
      }
      else
      {
        if (aGetYear>tLMDutyGetAliveSchema.getAddEndPeriod())
        {
          aGetYear =tLMDutyGetAliveSchema.getAddEndPeriod();
        }

        aGetYear = aGetYear - tLMDutyGetAliveSchema.getAddStartPeriod() + 1;
        //递增间隔单位默认为年
        if (aGetYear%tLMDutyGetAliveSchema.getAddIntv()!=0)
        {
          aTimes = aGetYear/tLMDutyGetAliveSchema.getAddIntv() + 1;
        }
        else
        {
          aTimes = aGetYear/tLMDutyGetAliveSchema.getAddIntv();
        }
        aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),tLMDutyGetAliveSchema.getAddValue(),aTimes,tLMDutyGetAliveSchema.getAddType());
      }
    }
    //投保递增开始年期,投保后多少年递增
    else if (tLMDutyGetAliveSchema.getAddStartUnit().equals("2"))
    {
      if (aGetAppYear<tLMDutyGetAliveSchema.getAddStartPeriod())
      {
        aGetMoney = aLCGetSchema.getActuGet();
      }
      else
      {
        if (aGetAppYear>tLMDutyGetAliveSchema.getAddEndPeriod())
        {
          aGetAppYear =tLMDutyGetAliveSchema.getAddEndPeriod();
        }

        aGetAppYear = aGetAppYear - tLMDutyGetAliveSchema.getAddStartPeriod() + 1;
        //递增间隔单位默认为年
        if (aGetAppYear%tLMDutyGetAliveSchema.getAddIntv()!=0)
        {
          aTimes = aGetAppYear/tLMDutyGetAliveSchema.getAddIntv() + 1;
        }
        else
        {
          aTimes = aGetAppYear/tLMDutyGetAliveSchema.getAddIntv();
        }
        aGetMoney = this.calAddData(aLCGetSchema.getActuGet(),tLMDutyGetAliveSchema.getAddValue(),aTimes,tLMDutyGetAliveSchema.getAddType());
      }
    }
    else
    {
      System.out.println("-----------no this add descriptions--------");
    }

    return aGetMoney;
  }

//计算递增值
  private double calAddData(double aOriginData,double aAddRate,double aTimes,String aAddRateType)
  {
    double aResultData;
    //按照比例递增
    if (aAddRateType.equals("R"))
    {
      aResultData = aOriginData + aOriginData * aAddRate * aTimes;
    }
    else
    {
      aResultData = aOriginData + aAddRate * aTimes;
    }

    return aResultData;
  }
  //得到保单是否销户标志
  private String getContCancelFlag(String aCancelFlag)
  {
    LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    tLMDutyGetAliveSchema = this.getGetAliveInfo();

    //默认为000，为防止描述造成的数据问题
    try
    {
      if (tLMDutyGetAliveSchema.getAfterGet()==null||tLMDutyGetAliveSchema.getAfterGet().trim().equals(""))
      {
        tLMDutyGetAliveSchema.setAfterGet("000");
      }
    }
    catch (Exception ex)
    {}
    //"000" 无动作
    if (tLMDutyGetAliveSchema.getAfterGet().equals("000"))
      aCancelFlag = "0";
    // "001"   保额递减（暂不用）
    else if (tLMDutyGetAliveSchema.getAfterGet().equals("001"))
      aCancelFlag = "0";
    //"002"    保额递增（暂不用）
    else if (tLMDutyGetAliveSchema.getAfterGet().equals("002"))
      aCancelFlag = "0";
    // "003" 无条件销户
    else if (tLMDutyGetAliveSchema.getAfterGet().equals("003"))
      aCancelFlag = "1";
    //"004" 最后一次给付销户
    else if (tLMDutyGetAliveSchema.getAfterGet().equals("004"))
    {
      if (aCancelFlag.equals("1"))
        aCancelFlag ="1";
      else
        aCancelFlag = "0";
    }
    else
      System.out.println("----no des getAliv---");

    return aCancelFlag;
  }

//是否销户标志
  private void setCancelFlag(String aCancelFlag)
  {
    mCancelFlag = aCancelFlag;
  }

  private String getCancelFlag()
  {
    if (mCancelFlag==null)
      mCancelFlag = "0";

    return mCancelFlag;
  }

//给付金额
  private void setGetMoney(double aGetMoney)
  {
    mGetMoney = aGetMoney;
  }

  private double getGetMoney()
  {
    return mGetMoney;
  }

//给付至日期
  private void setGetToDate(Date aGetToDate)
  {
    mGetToDate = aGetToDate;
  }

  private Date getGetToDate()
  {
    return mGetToDate;
  }
//保单信息
  private LCPolSchema getPolInfo()
  {
    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setSchema(mLCPolSchema);
    return tLCPolSchema;
  }

  private void setPolInfo(LCPolSchema tLCPolSchema)
  {
    mLCPolSchema.setSchema(tLCPolSchema);
  }
//保单客户信息
  private LCInsuredSchema getInsuredInfo()
  {
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
    tLCInsuredSchema.setSchema(mLCInsuredSchema);
    return tLCInsuredSchema;
  }

  private void setInsuredInfo(LCInsuredSchema tLCInsuredSchema)
  {
    mLCInsuredSchema.setSchema(tLCInsuredSchema);
  }
//生存给付项信息
  private LMDutyGetAliveSchema getGetAliveInfo()
  {
    LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
    tLMDutyGetAliveSchema.setSchema(mLMDutyGetAliveSchema);
    return tLMDutyGetAliveSchema;
  }

  private void setGetAliveInfo(LMDutyGetAliveSchema tLMDutyGetAliveSchema)
  {
    mLMDutyGetAliveSchema.setSchema(tLMDutyGetAliveSchema);
  }
//给付项信息
  private LMDutyGetSchema getDutyGetInfo()
  {
    LMDutyGetSchema tLMDutyGetSchema = new LMDutyGetSchema();
    tLMDutyGetSchema.setSchema(mLMDutyGetSchema);
    return tLMDutyGetSchema;
  }

  private void setDutyGetInfo(LMDutyGetSchema tLMDutyGetSchema)
  {
    mLMDutyGetSchema.setSchema(tLMDutyGetSchema);
  }

  public VData getResult()
  {
    return mResult;
  }

  public LJSGetDrawSet getLJSGetDrawSet()
  {
    return mLJSGetDrawSet;
  }

  private void displayObject(Object a)
  {
    Reflections aReflections = new Reflections();
    aReflections.printFields(a);
  }

  public static void main(String[] args)
  {
    VData tVData = new VData();
    GlobalInput tG = new GlobalInput();
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LCGetSchema tLCGetSchema = new LCGetSchema();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("timeEnd","2007-12-21");
    tG.Operator="tjj";
    tG.ManageCom="86110000";
    tLCGetSchema.setPolNo("86110020020210000443");
    tVData.addElement(tG);
    tVData.addElement(tLCPolSchema);
    tVData.addElement(tLCGetSchema);
    tVData.addElement(tTransferData);
    PayPlanBL tPayPlanBL = new PayPlanBL();

    tPayPlanBL.submitData(tVData,"");
  }
}
