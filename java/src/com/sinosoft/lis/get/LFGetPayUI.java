package com.sinosoft.lis.get;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.get.LFGetPayBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:生存领取确认（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author GYJ
 * @version 1.0
 */
public class LFGetPayUI {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData =new VData();
  /** 数据操作字符串 */
  private String mOperate;
  //业务处理相关变量
   /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private LJSGetSchema mLJSGetSchema=new LJSGetSchema();
  private LJSGetDrawSet mLJSGetDrawSet=new LJSGetDrawSet();
  private String mActuGetNo="";
  public LFGetPayUI ()
  {
  }
   /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    mInputData = (VData)cInputData.clone();
    //得到外部传入的数据,将数据备份到本类中


    //进行业务处理

    //准备往后台的数据

    LFGetPayBL tLFGetPayBL=new LFGetPayBL();
    System.out.println("Start LFGetPay UI Submit...");
    tLFGetPayBL.submitData(cInputData,mOperate);
    System.out.println("End LFGetPay UI Submit...");
    //如果有需要处理的错误，则返回
    if (tLFGetPayBL .mErrors .needDealError() )
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tLFGetPayBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "PayPlanUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    this.mActuGetNo=tLFGetPayBL.getActuGetNo();
    return true;
  }

   public String getActuGetNo()
  {
    return this.mActuGetNo;
  }

  public static void main(String[] args) {
     String tDay="2001-01-01";

     GlobalInput tGlobalInput = new GlobalInput();
     tGlobalInput.Operator ="Admin";
     tGlobalInput.ComCode  ="asd";
     tGlobalInput.ManageCom="sdd";
     VData tVData = new VData();
     tVData.add(tGlobalInput);
     LJSGetSchema tLJSGetSchema=new LJSGetSchema();
     tLJSGetSchema.setGetNoticeNo("000sdd20020990000019");
      tVData.add(tLJSGetSchema);
     LJSGetDrawSet tLJSGetDrawSet=new LJSGetDrawSet();
     LJSGetDrawSchema tLJSGetDrawSchema=new LJSGetDrawSchema();
     tLJSGetDrawSchema.setDutyCode("D-0001");
     tLJSGetDrawSchema.setGetDutyKind("GA0001");

     tLJSGetDrawSet.add(tLJSGetDrawSchema);
     tLJSGetDrawSchema=new LJSGetDrawSchema();
     tLJSGetDrawSchema.setDutyCode("D-0001");
     tLJSGetDrawSchema.setGetDutyKind("GA0002");

     tLJSGetDrawSet.add(tLJSGetDrawSchema);
     tVData.add(tLJSGetDrawSet);
     LFGetPayUI tLFGetPayUI = new LFGetPayUI();
     tLFGetPayUI.submitData(tVData,"INSERT||PERSON");
     System.out.println("==========="+tLFGetPayUI.getActuGetNo());

  }
}