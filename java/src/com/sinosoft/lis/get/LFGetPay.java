/**
 * <p>Title: LFGetPay</p>
 * <p>Description: 批量生存自动领取程序</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.get;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.get.*;
import com.sinosoft.lis.pubfun.*;

public class LFGetPay {
      public static void main(String[] args) {
             LJSGetSchema tLJSGetSchema=new LJSGetSchema();
             LJSGetDrawSet tLJSGetDrawSet  = new LJSGetDrawSet();
             TransferData tTransferData = new TransferData();
             LJSGetDrawSchema tLJSGetDrawSchema;
             CErrors tError = null;
             String Content="";
             String FlagStr="";
             String tLastGettoDate="";
             String tCurGetToDate="";
             String tActuGetNo="";
             String tGetMoney="" ;
             int iMax=0;
             boolean tflagchk=false;

             GlobalInput tGlobalInput = new GlobalInput();
             tGlobalInput.ManageCom = "86";
             tGlobalInput.Operator = "AUTOBATCH";
             tGlobalInput.ComCode="001";
            tTransferData.setNameAndValue("GetType","0");
            VData tVData = new VData();
            tVData.add(tGlobalInput);
            tVData.add(tTransferData);
            tVData.add(tLJSGetDrawSet);
            System.out.println("start before ui.....");
            LFGetPayUI tLFGetPayUI = new LFGetPayUI();
            try{
                 tLFGetPayUI.submitData(tVData,"INSERT||PERSON");
            }
            catch(Exception ex){
                  Content = "保存失败，原因是:" + ex.toString();
                  FlagStr = "Fail";
            }
            //如果在Catch中发现异常，则不从错误类中提取错误信息
            if (FlagStr==""){
                   tError = tLFGetPayUI.mErrors;
                   if (!tError.needDealError()){
                     Content = " 核销成功";
                     FlagStr = "Succ";
                   }
                   else{
                      Content = " 保存失败，原因是:" + tError.getFirstError();
                      FlagStr = "Fail";
                  }
            }
     }
}