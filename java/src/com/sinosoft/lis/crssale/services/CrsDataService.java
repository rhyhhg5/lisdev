/**
 * 2010-8-2
 */
package com.sinosoft.lis.crssale.services;

import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.crssale.response.CrsBaseDataSyncAPI;

/**
 * @author LY
 *
 */
public class CrsDataService
{
    private final static Logger cLogger = Logger.getLogger(CrsChkService.class);

    public CrsDataService()
    {
        cLogger.info("集团公司正常数据下发(CrsDataService)");
    }

    public String service(String cInXmlStr, String cMsgType)
    {
        cLogger.info("Into CrsDataService.service()...");

        long mStartMillis = System.currentTimeMillis();
        cLogger.debug(cInXmlStr);
        String mOutXmlStr = null;

        try
        {
            //获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(cInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            //调用核心后台处理
            CrsBaseDataSyncAPI tBusLogic = new CrsBaseDataSyncAPI();
            Document tOutXmlDoc = tBusLogic.deal(tInXmlDoc, cMsgType);

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
        }
        catch (Throwable ex)
        {
            cLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }

        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        cLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");

        cLogger.debug(mOutXmlStr);
        cLogger.info("Out CrsDataService.service()!");
        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        cLogger.info("Into CrsDataService.getError()...");

        cLogger.info("Out CrsDataService.getError()!");
        return null;
    }
}
