package com.sinosoft.lis.crssale.services;

import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.crssale.response.CrsResDataChkAPI;

public class CrsChkService
{
    private final static Logger cLogger = Logger.getLogger(CrsChkService.class);

    public CrsChkService()
    {
        cLogger.info("集团公司下发校验结果数据(CrsChkService)");
    }

    public String service(String cInXmlStr, String cMsgType)
    {
        cLogger.info("Into CrsChkService.service()...");

        long mStartMillis = System.currentTimeMillis();
        cLogger.debug(cInXmlStr);
        String mOutXmlStr = null;

        try
        {
            //获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(cInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            //保存网站传入的xml到本地文件系统
            //			String tSaveName = "Card_" + DateUtil.getCurrentDate("HHmmss") + ".xml";
            //			SaveMessage.save(tInXmlDoc, tSaveName, "InNoStd");

            //解密及校验(待沟通)

            //调用核心后台处理
            CrsResDataChkAPI tBusLogic = new CrsResDataChkAPI();
            Document tOutXmlDoc = tBusLogic.deal(tInXmlDoc, cMsgType);

            //			保存返回给网站的xml到本地文件系统
            //            String tSaveName = "Card_" + DateUtil.getCurrentDate("HHmmss")
            //                    + ".xml";
            //            SaveMessage.save(tOutXmlDoc, tSaveName, "OutStd");

            //加密及校验(待沟通)

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
        }
        catch (Throwable ex)
        {
            cLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }

        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        cLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");

        cLogger.debug(mOutXmlStr);
        cLogger.info("Out CrsChkService.service()!");
        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        cLogger.info("Into CrsChkService.getError()...");

        cLogger.info("Out CrsChkService.getError()!");
        return null;
    }

}
