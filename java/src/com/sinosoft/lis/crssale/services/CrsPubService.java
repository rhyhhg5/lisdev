/**
 * 2010-8-10
 */
package com.sinosoft.lis.crssale.services;

import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.crssale.response.CrsPubDataAPI;

/**
 * @author LY
 *
 */
public class CrsPubService
{
    private final static Logger cLogger = Logger.getLogger(CrsChkService.class);

    public CrsPubService()
    {
        cLogger.info("(CrsPubService)");
    }

    public String service(String cInXmlStr)
    {
        cLogger.info("Into CrsPubService.service()...");

        long mStartMillis = System.currentTimeMillis();
        cLogger.debug(cInXmlStr);
        String mOutXmlStr = null;

        try
        {
            //获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(cInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            //调用核心后台处理
            CrsPubDataAPI tBusLogic = new CrsPubDataAPI();
            Document tOutXmlDoc = tBusLogic.deal(tInXmlDoc);

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
            mOutXmlStr = resDocFilter(mOutXmlStr);
        }
        catch (Throwable ex)
        {
            cLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }

        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        cLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");

        cLogger.debug(mOutXmlStr);
        cLogger.info("Out CrsChkService.service()!");
        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        cLogger.info("Into CrsPubService.getError()...");

        cLogger.info("Out CrsPubService.getError()!");
        return null;
    }

    private String resDocFilter(String cResXml)
    {
        String tOutXmlStr = cResXml;
        tOutXmlStr = tOutXmlStr.replaceAll("xmlnsSxsi", "xmlns:xsi");
        tOutXmlStr = tOutXmlStr.replaceAll("xmlnsSxsd", "xmlns:xsd");

        return tOutXmlStr;
    }
}
