package com.sinosoft.lis.crssale.services;

import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.crssale.CrsUserQueryDeal;

public class CrsUserQueService
{
    private final static Logger cLogger = Logger
            .getLogger(CrsUserQueService.class);

    public CrsUserQueService()
    {
        cLogger.info("集团交叉销售保单查询(CrsUserQueServive.java)");
    }

    public String service(String pInXmlStr)
    {
        cLogger.info("Into CrsUserQueServive.service()...");

        long mStartMillis = System.currentTimeMillis();
        cLogger.debug(pInXmlStr);
        String mOutXmlStr = null;

        try
        {
            //获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(pInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            //调用核心后台处理
            CrsUserQueryDeal tCrsUserQueryDeal = new CrsUserQueryDeal(tInXmlDoc);
            Document tOutXmlDoc = tCrsUserQueryDeal.deal();

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
        }
        catch (Throwable ex)
        {
            cLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }

        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        cLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");

        cLogger.debug(mOutXmlStr);
        cLogger.info("Out CardService.service()!");
        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        cLogger.info("Into BaiFenService.getError()...");

        cLogger.info("Out BaiFenService.getError()!");
        return null;
    }
}
