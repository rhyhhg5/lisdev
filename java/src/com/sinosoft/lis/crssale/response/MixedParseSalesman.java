package com.sinosoft.lis.crssale.response;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MixedParseSalesman
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private VData mResult = new VData();

    public MixedParseSalesman()
    {
    }

    public boolean run(String cMsgInfo)
    {
        try
        {
            if (!parseMsg(cMsgInfo))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            System.out.println("readTextDate END With Exception...");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean parseMsg(String cMsgInfo)
    {
        LOMixSalesmanSchema tMixSalesman = new LOMixSalesmanSchema();

        String tPatMsgHeadFormat = "[A-Z]-[A-Z]-\\d+-\\d+-[A-Za-z]\\d+-[A-Za-z0-9]+-\\d{14}:";
        String tPatMsgContextFormat = ".*(\\|(.*))+";

        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(cMsgInfo);
        if (!tMatcher.matches())
        {
        	CError tError = new CError();
            tError.moduleName = "MixedParseSalesman";
            tError.functionName = "parseMsg";
            tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return false;
        String tContext = cMsgInfo.substring(idx + 1, cMsgInfo.length());
        String[] tSubContext = tContext.split("\\|", -1);

        tMixSalesman.setSales_Cod(StrTool.cTrim(tSubContext[0])); //销售人员工号
        tMixSalesman.setComp_Cod(StrTool.cTrim(tSubContext[1])); //子公司代码
        tMixSalesman.setComp_Nam(StrTool.cTrim(tSubContext[2])); //子公司名称
        tMixSalesman.setParty_Id(StrTool.cTrim(tSubContext[3])); //当事方编码
        tMixSalesman.setParty_Rol_Cod(StrTool.cTrim(tSubContext[4])); //当事方角色代码
        tMixSalesman.setSales_Nam(StrTool.cTrim(tSubContext[5])); //姓名
        tMixSalesman.setDate_Birthd(StrTool.cTrim(tSubContext[6])); // 出生日期
        tMixSalesman.setSex_Cod(StrTool.cTrim(tSubContext[7])); //性别代码
        tMixSalesman.setMan_Org_Cod(StrTool.cTrim(tSubContext[8])); //管理机构代码
        tMixSalesman.setMan_Org_Nam(StrTool.cTrim(tSubContext[9])); //管理机构名称
        tMixSalesman.setIdtyp_Cod(StrTool.cTrim(tSubContext[10])); //证件类型代码
        tMixSalesman.setIdtyp_Nam(StrTool.cTrim(tSubContext[11])); //证件类型名称
        tMixSalesman.setId_No(StrTool.cTrim(tSubContext[12])); //证件号码
        tMixSalesman.setSales_Tel(StrTool.cTrim(tSubContext[13])); //联系电话
        tMixSalesman.setSales_Mob(StrTool.cTrim(tSubContext[14])); //联系手机
        tMixSalesman.setSales_Mail(StrTool.cTrim(tSubContext[15])); //电子邮件
        tMixSalesman.setStatus_Cod(StrTool.cTrim(tSubContext[16])); //在职状态代码
        tMixSalesman.setStatus(StrTool.cTrim(tSubContext[17])); //在职状态名称
        tMixSalesman.setSales_Typ_Cod(StrTool.cTrim(tSubContext[18])); //销售人员类型代码
        tMixSalesman.setSales_Typ(StrTool.cTrim(tSubContext[19])); //销售人员类型名称
        tMixSalesman.setData_Upd_Typ(StrTool.cTrim(tSubContext[20])); //数据变更类型
        tMixSalesman.setDate_Send(StrTool.cTrim(tSubContext[21])); //子公司报送时间
        tMixSalesman.setDate_Update(StrTool.cTrim(tSubContext[22])); //时间戳
        tMixSalesman.setSalecha_Cod(StrTool.cTrim(tSubContext[23])); //销售渠道代码
        tMixSalesman.setSalecha_Nam(StrTool.cTrim(tSubContext[24])); //销售渠道名称
        tMixSalesman.setEntrant_Date(StrTool.cTrim(tSubContext[25])); //入职时间
        tMixSalesman.setDimission_Date(StrTool.cTrim(tSubContext[26])); //准离司时间/离司时间
        tMixSalesman.setCheck_Status(StrTool.cTrim(tSubContext[27])); //集团校验标志
        tMixSalesman.setCheck_Date(StrTool.cTrim(tSubContext[28])); //校验时间
        tMixSalesman.setColl_Type(StrTool.cTrim(tSubContext[29])); //交叉销售权限

        tMixSalesman.setMakeDate(PubFun.getCurrentDate());
        tMixSalesman.setMakeTime(PubFun.getCurrentTime());
        tMixSalesman.setModifyDate(PubFun.getCurrentDate());
        tMixSalesman.setModifyTime(PubFun.getCurrentTime());

        mMap = new MMap();
        mMap.put(tMixSalesman, "DELETE&INSERT");
        if (!SubmitMap())
        {
            return false;
        }
        return true;
    }

    /*数据处理*/
    private boolean SubmitMap()
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult = new VData();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, ""))
        {
            buildError("SubmitMap", "提交数据失败！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) throws Exception
    {
        MixedParseSalesman mMixedSalesmanTask = new MixedParseSalesman();
        mMixedSalesmanTask
                .run("R-I-000002-11000000-S001-test3123-20100527123430: 3508812019|000002|中国人民财产保险股份有限公司|||吴兰兰|||35080200|中国人民财产保险股份有限公司龙岩新罗区支|||||||1|在职|1|直销|I|2009042912:10:52|2009042912:10:52|1|直销|20091002|20100403||20090328 15:30:33|000085,000100");
        return;

    }
}