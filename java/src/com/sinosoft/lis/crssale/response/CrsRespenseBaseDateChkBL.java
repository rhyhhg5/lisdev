/**
 * 2010-7-15
 */
package com.sinosoft.lis.crssale.response;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.Crs_CheckResultLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CrsRespenseBaseDateChkBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 消息数据流 */
    private String mMsgInfo = null;

    /** 消息类型 */
    private String mMsgType = null;

    public CrsRespenseBaseDateChkBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mMsgInfo = (String) mTransferData.getValueByName("MsgInfo");
        if (mMsgInfo == null || mMsgInfo.equals(""))
        {
            buildError("getInputData", "获取消息失败。");
            return false;
        }

        mMsgType = (String) mTransferData.getValueByName("MsgType");
        if (mMsgType == null || mMsgType.equals(""))
        {
            buildError("getInputData", "获取消息类型失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Receive".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = dealCrsChkResult();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    /**
     * 处理数据校验反馈信息。
     * @return
     */
    private MMap dealCrsChkResult()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 解析消息
        Crs_CheckResultLogSchema tCrsMsg = null;
        tCrsMsg = parseMsg(mMsgInfo);
        if (tCrsMsg == null)
        {
            return null;
        }
        // --------------------

        // 存储轨迹
        tTmpMap = saveTrace(tCrsMsg);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理业务逻辑
        tTmpMap = dealBaseData(tCrsMsg, mMsgType);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private Crs_CheckResultLogSchema parseMsg(String cMsgInfo)
    {
        Crs_CheckResultLogSchema tCrsChkMsg = new Crs_CheckResultLogSchema();

        String tPatMsgHeadFormat = "E-\\d+-\\d+-[A-Za-z]\\d+-\\d+-\\d{14}:";
        String tPatMsgContextFormat = ".*(\\|(.*))+";

        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(cMsgInfo);
        if (!tMatcher.matches())
        {
            buildError("parseMsg", "消息格式不正确。");
            return null;
        }
        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return null;
        String tContext = cMsgInfo.substring(idx + 1, cMsgInfo.length());
        String[] tSubContext = tContext.split("\\|", -1);

        tCrsChkMsg.setMsgBatchNo(tSubContext[0] + "|" + tSubContext[5]);

        tCrsChkMsg.setinfo_id(tSubContext[0]);
        tCrsChkMsg.setcheck_type(tSubContext[1]);
        tCrsChkMsg.setcheck_account(tSubContext[2]);
        tCrsChkMsg.setcheck_date(tSubContext[3]);
        tCrsChkMsg.setState(tSubContext[4]);
        tCrsChkMsg.setdate_send(tSubContext[5]);

        tCrsChkMsg.setOperator("CRS_CHK");
        tCrsChkMsg.setMakeDate(PubFun.getCurrentDate());
        tCrsChkMsg.setMakeTime(PubFun.getCurrentTime());
        tCrsChkMsg.setModifyDate(PubFun.getCurrentDate());
        tCrsChkMsg.setModifyTime(PubFun.getCurrentTime());
        // --------------------

        return tCrsChkMsg;
    }

    private MMap saveTrace(Crs_CheckResultLogSchema cCrsChkLog)
    {
        MMap tMMap = new MMap();
        tMMap.put(cCrsChkLog, SysConst.DELETE_AND_INSERT);
        return tMMap;
    }

    private MMap dealBaseData(Crs_CheckResultLogSchema cCrsChkLog,
            String tMsgType)
    {
        MMap tMMap = new MMap();

        String tStrSql = null;

        // 解析审核结果
        String tCrsChkRes = "99";
        /*
         *  0：未校验
			1：校验未通过
			4：校验通过
         */
        // tCrsChkRes状态：00-未上报；01-已上报，待审核；02-通过审核；03-未通过审核”
//        if ("0".equals(cCrsChkLog.getState()))
//        {
//            tCrsChkRes = "02";
//        }
        if ("1".equals(cCrsChkLog.getState()))
        {
            tCrsChkRes = "03";
        }
        else if ("4".equals(cCrsChkLog.getState()))
        {
        	tCrsChkRes = "02";
        }
        // --------------------

        // 解析数据唯一标识码
        String tInfo_id = cCrsChkLog.getinfo_id();
        int tIdx = tInfo_id.indexOf("-");
        String tKey = null;
        if (tIdx >= 0)
        {
            tKey = tInfo_id.substring(tIdx + 1, tInfo_id.length());
        }
        // --------------------

        // 解析数据类型
        if ("D001".equals(tMsgType))
        {
            tStrSql = "update LACom set crs_chk_status = '" + tCrsChkRes
                    + "' where ComCode = '" + tKey + "'";
        }
        else if ("S001".equals(tMsgType))
        {
            tStrSql = "update LAAgent set crs_chk_status = '" + tCrsChkRes
                    + "' where AgentCode = '" + tKey + "'";
        }
        else if ("S003".equals(tMsgType))
        {
            //            tStrSql = "update LAAgent set crs_chk_status = '" + tCrsChkRes
            //                    + "' where AgentCode = '" + tKey + "'";
        }
        // --------------------

        if (tStrSql != null && tStrSql.equals(""))
        {
            tMMap.put(tStrSql, SysConst.UPDATE);
        }

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println("[" + szFunc + "]: " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CrsRespenseBaseDateChkBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
