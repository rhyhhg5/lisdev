/**
 * 2010-8-10
 */
package com.sinosoft.lis.crssale.response;

import java.io.StringReader;
import java.io.StringWriter;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CrsPubDataAPI
{
    private String mBusType = null;

    private String mMsgType = null;

    public CrsPubDataAPI()
    {
    }

    public Document deal(Document cInXmlDoc)
    {
        System.out.println("CrsPubDataAPI start ...");

        // 逻辑处理
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "CRS_SYS";

        String cMsgInfo = loadData(cInXmlDoc);

        if (!parseMsg(cMsgInfo))
        {
            return getResult("02");
        }

        String tMsgResult = "00";

        if ("E".equals(mBusType))
        {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("MsgInfo", cMsgInfo);
            tTransferData.setNameAndValue("MsgType", mMsgType);

            VData tVData = new VData();
            tVData.add(tTransferData);
            tVData.add(tGI);

            CrsRespenseBaseDateChkBL tBusinessDeal = new CrsRespenseBaseDateChkBL();
            if (!tBusinessDeal.submitData(tVData, "Receive"))
            {
                tMsgResult = "02";
            }
            else
            {
                tMsgResult = "01";
            }
        }
        else if ("R".equals(mBusType))
        {
            if ("D001".equals(mMsgType))
            {
                MixedParseManagecom tBusinessDeal = new MixedParseManagecom();
                if (!tBusinessDeal.run(cMsgInfo))
                {
                    tMsgResult = "02";
                }
                else
                {
                    tMsgResult = "01";
                }
            }
            else if ("S001".equals(mMsgType))
            {
                MixedParseSalesman tBusinessDeal = new MixedParseSalesman();
                if (!tBusinessDeal.run(cMsgInfo))
                {
                    tMsgResult = "02";
                }
                else
                {
                    tMsgResult = "01";
                }
            }
            else if ("S003".equals(mMsgType))
            {
                MixedParseCrsUser tBusinessDeal = new MixedParseCrsUser();
                if (!tBusinessDeal.run(cMsgInfo))
                {
                    tMsgResult = "02";
                }
                else
                {
                    tMsgResult = "01";
                }
            }
        }
        // --------------------

        System.out.println("CrsPubDataAPI end ...");
        return getResult(tMsgResult);
    }

    private String loadData(Document cInXmlDoc)
    {
        Document tTmpDoc = cInXmlDoc;

        if (tTmpDoc == null)
        {
            return null;
        }

        Element tRootData = tTmpDoc.getRootElement();
        if (tRootData == null)
        {
            errLog("无报文数据");
            return null;
        }

        String tMsgInfo = tRootData.getChildTextTrim("MESSAGE");

        return tMsgInfo;
    }

    private boolean parseMsg(String cMsgInfo)
    {
        //        String tPatMsgHeadFormat = "[A-Z]-[A-Z]-\\d+-[A-Za-z]\\d+-\\d+-\\d{14}:";
        //        String tPatMsgContextFormat = ".*(\\|(.*))+";
        //
        //        StringBuffer tPatMsgFormat = new StringBuffer();
        //        tPatMsgFormat.append("^");
        //        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        //        tPatMsgFormat.append("$");
        //
        //        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());
        //
        //        Matcher tMatcher = null;
        //
        //        // 校验消息
        //        tMatcher = pattern.matcher(cMsgInfo);
        //        if (!tMatcher.matches())
        //        {
        //            errLog("地址码超长,生成号码失败,请先删除原来的超长地址码!");
        //            return false;
        //        }
        //        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return false;

        String tContext = cMsgInfo.substring(0, idx);
        String[] tSubContext = tContext.split("-");

        mBusType = tSubContext[0];

        if ("E".equals(mBusType))
            mMsgType = tSubContext[3];
        else if ("R".equals(mBusType))
            mMsgType = tSubContext[4];
        else
        {
            return false;
        }

        return true;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        //        cLogger.error(cErrInfo);
        //        this.mErrInfo.append(cErrInfo);
        System.out.println(cErrInfo);
    }

    private Document getResult(String cMsgResult)
    {
        String tStrRes = "01".equals(cMsgResult) ? "TRUE" : "FALSE";
        String tStrResinfo = "TRUE".equals(tStrRes) ? "发送成功" : "消息处理失败";

        Element tState = new Element("RESULT");
        tState.setText(tStrRes);

        Element tStateMsg = new Element("MESSAGE");
        tStateMsg.setText(tStrResinfo);

        Element mRootData = new Element("sysnet");
        mRootData.addAttribute("xmlnsSxsi",
                "http://www.w3.org/2001/XMLSchema-instance");
        mRootData.addAttribute("xmlnsSxsd", "http://www.w3.org/2001/XMLSchema");

        mRootData.addContent(tState);
        mRootData.addContent(tStateMsg);

        Document tDocDealResult = new Document(mRootData);

        //        Document tDocFilter = resDocFilter(tDocDealResult);
        //        if (tDocFilter != null)
        //            tDocDealResult = tDocFilter;

        // cMsgResult: 01-成功；02-失败。
        return tDocDealResult;
    }

    //    private Document resDocFilter(Document cResDoc)
    //    {
    //        try
    //        {
    //            StringWriter tStringWriter = new StringWriter();
    //            XMLOutputter tXMLOutputter = new XMLOutputter();
    //            tXMLOutputter.output(cResDoc, tStringWriter);
    //
    //            String cOutXmlStr = tStringWriter.toString();
    //            cOutXmlStr = cOutXmlStr.replaceAll("xmlnsSxsi", "xmlns:xsi");
    //            cOutXmlStr = cOutXmlStr.replaceAll("xmlnsSxsd", "xmlns:xsd");
    //
    //            StringReader tInXmlReader = new StringReader(cOutXmlStr);
    //            SAXBuilder tSAXBuilder = new SAXBuilder();
    //            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);
    //
    //            return tInXmlDoc;
    //        }
    //        catch (Exception e)
    //        {
    //            e.printStackTrace();
    //            return null;
    //        }
    //    }
}
