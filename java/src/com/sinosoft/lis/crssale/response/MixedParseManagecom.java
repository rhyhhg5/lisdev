package com.sinosoft.lis.crssale.response;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class MixedParseManagecom
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private VData mResult = new VData();

    public MixedParseManagecom()
    {
    }

    public boolean run(String cMsgInfo)
    {
        try
        {
            if (!parseMsg(cMsgInfo))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("run", "处理中出现异常。");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean parseMsg(String cMsgInfo)
    {
        LOMixComSchema tCrsChkMsg = new LOMixComSchema();

        String tPatMsgHeadFormat = "[A-Z]-[A-Z]-\\d+-\\d+-[A-Za-z]\\d+-[A-Za-z0-9]+-\\d{14}:";
        String tPatMsgContextFormat = ".*(\\|(.*))+";

        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(cMsgInfo);
        if (!tMatcher.matches())
        {
            CError tError = new CError();
            tError.moduleName = "groupContBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return false;
        String tContext = cMsgInfo.substring(idx + 1, cMsgInfo.length());
        String[] tSubContext = tContext.split("\\|", -1);

        tCrsChkMsg.setGrpAgentCom(StrTool.cTrim(tSubContext[0])); //管理机构

        tCrsChkMsg.setComp_Cod(StrTool.cTrim(tSubContext[1])); //子公司代码 
        tCrsChkMsg.setComp_Nam(StrTool.cTrim(tSubContext[2])); //子公司名称

        tCrsChkMsg.setProv_Orgcod(StrTool.cTrim(tSubContext[3])); //省级分公司代码
        tCrsChkMsg.setProv_Orgname(StrTool.cTrim(tSubContext[4])); //省级分公司名称
        tCrsChkMsg.setCity_Orgcod(StrTool.cTrim(tSubContext[5])); //地市级分公司代码
        tCrsChkMsg.setCity_Orgname(StrTool.cTrim(tSubContext[6])); //地市级分公司名称
        tCrsChkMsg.setTown_Orgcod(StrTool.cTrim(tSubContext[7])); //县支级分公司代码
        tCrsChkMsg.setTown_Orgname(StrTool.cTrim(tSubContext[8])); //县支级分公司名称
        tCrsChkMsg.setUnder_Orgcod(StrTool.cTrim(tSubContext[9])); //代理机构代码
        tCrsChkMsg.setUnder_Orgname(StrTool.cTrim(tSubContext[10])); //代理机构名称
        tCrsChkMsg.setOrg_Lvl(StrTool.cTrim(tSubContext[11])); //机构等级
        tCrsChkMsg.setStart_Dat(StrTool.cTrim(tSubContext[12])); //起始日期
        tCrsChkMsg.setEnd_Dat(StrTool.cTrim(tSubContext[13])); //终止日期
        tCrsChkMsg.setStatus_Cod(StrTool.cTrim(tSubContext[14])); //状态
        tCrsChkMsg.setStatus_Nam(StrTool.cTrim(tSubContext[15])); //状态说明
        tCrsChkMsg.setDate_Send(StrTool.cTrim(tSubContext[16])); //子公司报送时间
        tCrsChkMsg.setStandByFlag1(StrTool.cTrim(tSubContext[17])); //第五级机构代码
        tCrsChkMsg.setStandByFlag2(StrTool.cTrim(tSubContext[18])); //第五级机构名称

        tCrsChkMsg.setMakeDate(PubFun.getCurrentDate());
        tCrsChkMsg.setMakeTime(PubFun.getCurrentTime());
        tCrsChkMsg.setModifyDate(PubFun.getCurrentDate());
        tCrsChkMsg.setModifyTime(PubFun.getCurrentTime());

        mMap = new MMap();
        mMap.put(tCrsChkMsg, "DELETE&INSERT");
        if (!SubmitMap())
        {
            return false;
        }
        return true;
    }

    /*数据处理*/
    private boolean SubmitMap()
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult = new VData();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, ""))
        {
            System.out.println("提交数据失败！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) throws Exception
    {
        MixedParseManagecom mMixedComTask = new MixedParseManagecom();
        mMixedComTask
                .run("R-I-000002-11000000-D001-aaa11010000-20100527123430: 1940306|000100|中国人保寿险有限公司|1|东部|1940000|深圳市分公司|1940300|深圳分公司本部|1|深|1|深|3|2||1|正常|440300|深圳市|440300|深圳市|||I|20091208 04:12:42|20091208 04:12:42|11010102|机构名称");
        return;

    }
}