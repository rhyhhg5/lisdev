/**
 * 2010-7-26
 */
package com.sinosoft.lis.crssale.response;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CrsResDataChkAPI
{
    public CrsResDataChkAPI()
    {
    }

    public Document deal(Document cInXmlDoc, String cMsgType)
    {
        System.out.println("CrsResDataChkAPI start ...");

        // 逻辑处理
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "CRS_SYS";

        String cMsgInfo = loadData(cInXmlDoc);

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("MsgInfo", cMsgInfo);
        tTransferData.setNameAndValue("MsgType", cMsgType);

        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGI);

        String tMsgResult = "00";
        CrsRespenseBaseDateChkBL tBusinessDeal = new CrsRespenseBaseDateChkBL();
        if (!tBusinessDeal.submitData(tVData, "Receive"))
        {
            tMsgResult = "02";
        }
        else
        {
            tMsgResult = "01";
        }
        // --------------------

        System.out.println("CrsResDataChkAPI end ...");
        return getResult(tMsgResult);
    }

    private String loadData(Document cInXmlDoc)
    {
        Document tTmpDoc = cInXmlDoc;

        if (tTmpDoc == null)
        {
            return null;
        }

        Element tRootData = tTmpDoc.getRootElement();
        if (tRootData == null)
        {
            errLog("无报文数据");
            return null;
        }

        String tMsgInfo = tRootData.getChildTextTrim("MESSAGE");

        return tMsgInfo;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        //        cLogger.error(cErrInfo);
        //        this.mErrInfo.append(cErrInfo);
        System.out.println(cErrInfo);
    }

    private Document getResult(String cMsgResult)
    {
        String tStrRes = "01".equals(cMsgResult) ? "TRUE" : "FALSE";
        String tStrResinfo = "01".equals(tStrRes) ? "发送成功" : "消息处理失败";

        Element tState = new Element("RESULT");
        tState.setText(tStrRes);

        Element tStateMsg = new Element("MESSAGE");
        tStateMsg.setText(tStrResinfo);

        Element mRootData = new Element("sysnet");
        mRootData.addContent(tState);
        mRootData.addContent(tStateMsg);

        Document tDocDealResult = new Document(mRootData);

        // cMsgResult: 01-成功；02-失败。
        return tDocDealResult;
    }
}
