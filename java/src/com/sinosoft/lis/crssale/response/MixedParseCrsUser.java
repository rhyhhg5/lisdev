package com.sinosoft.lis.crssale.response;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.lis.crssale.up.CrsBaseSendCrsUserXml;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.schema.LOMCommissionSchema;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MixedParseCrsUser
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private VData mResult = new VData();

    LOMCommissionSchema tMixCrsUser = new LOMCommissionSchema();
    
    LDUserDB tLDUserDB= new LDUserDB();
    LDUserSet tLDUserSet = new LDUserSet();
    
    public MixedParseCrsUser()
    {
    }

    public boolean run(String cMsgInfo)
    {
        try
        {
            if (!parseMsg(cMsgInfo))
            {
                return false;
            }
            //add by fuxin 2010-9-7 营销员信息直接上报
            CrsBaseSendCrsUserXml tCrsBaseSendCrsUserXml = new CrsBaseSendCrsUserXml();
            tCrsBaseSendCrsUserXml.sendCrsUserXml(tMixCrsUser.getSerialno());
        }
        catch (Exception ex)
        {
            System.out.println("readTextDate END With Exception...");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean parseMsg(String cMsgInfo)
    {
        

        String tPatMsgHeadFormat = "[A-Z]-[A-Z]-\\d+-\\d+-[A-Za-z]\\d+-[A-Za-z0-9]+-\\d{14}:";
        String tPatMsgContextFormat = ".*(\\|(.*))+";

        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat + tPatMsgContextFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(cMsgInfo);
        if (!tMatcher.matches())
        {
            CError tError = new CError();
            tError.moduleName = "MixedParseSalesman";
            tError.functionName = "parseMsg";
            tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //add by fuxin 提取下发报文的操作符。
        String tOpeType = String.valueOf(cMsgInfo.charAt(2));
        
        if (!"I".equals(tOpeType) && !"U".equals(tOpeType))
        {
            System.out.println("操作符不符合规范。");
            return false;
        }
        // --------------------

        // 拆分消息
        int idx = cMsgInfo.indexOf(":");
        if (idx == -1)
            return false;
        String tContext = cMsgInfo.substring(idx + 1, cMsgInfo.length());
        String[] tSubContext = tContext.split("\\|", -1);

        tMixCrsUser.setoperatorcode(StrTool.cTrim(tSubContext[0])); //代理方出单人员工号
        tMixCrsUser.setcomp_cod(StrTool.cTrim(tSubContext[1])); //子公司代码
        tMixCrsUser.setcomp_nam(StrTool.cTrim(tSubContext[2])); //子公司名称
        tMixCrsUser.setman_org_cod(StrTool.cTrim(tSubContext[3])); //管理机构编号
        tMixCrsUser.setman_org_nam(StrTool.cTrim(tSubContext[4])); //管理机构名称
        tMixCrsUser.setsales_nam(StrTool.cTrim(tSubContext[5])); //姓名
        tMixCrsUser.setdate_birthd(StrTool.cTrim(tSubContext[6])); // 出生日期
        tMixCrsUser.setsex_cod(StrTool.cTrim(tSubContext[7])); //性别代码
        tMixCrsUser.setidtyp_cod(StrTool.cTrim(tSubContext[8])); //证件类型代码
        tMixCrsUser.setpaperworkid(StrTool.cTrim(tSubContext[9])); //证件号
        tMixCrsUser.settelephone(StrTool.cTrim(tSubContext[10])); //联系电话
        tMixCrsUser.setsales_mob(StrTool.cTrim(tSubContext[11])); //联系手机
        tMixCrsUser.setsales_mail(StrTool.cTrim(tSubContext[12])); //电子邮件
        tMixCrsUser.setstatus_cod(StrTool.cTrim(tSubContext[13])); //在职状态代码
        tMixCrsUser.setstatus(StrTool.cTrim(tSubContext[14])); //在职状态名称
        tMixCrsUser.setfunction(StrTool.cTrim(tSubContext[15])); //职能
        tMixCrsUser.setentrant_date(StrTool.cTrim(tSubContext[16])); //入职时间
        tMixCrsUser.setdimission_date(StrTool.cTrim(tSubContext[17])); //离司时间
        tMixCrsUser.setdata_upd_typ(StrTool.cTrim(tSubContext[18])); //数据变更类型
        tMixCrsUser.setprt_entrust_orgid(StrTool.cTrim(tSubContext[19])); //财险委托方机构编码
        tMixCrsUser.setprt_entrust_orgname(StrTool.cTrim(tSubContext[20])); //财险委托方机构名称
        tMixCrsUser.setli_entrust_orgid(StrTool.cTrim(tSubContext[21])); //健康险委托方机构编码
        tMixCrsUser.setli_entrust_orgname(StrTool.cTrim(tSubContext[22])); //健康险委托方机构名称
        tMixCrsUser.sethi_entrust_orgid(StrTool.cTrim(tSubContext[23])); //寿险委托方机构编码
        tMixCrsUser.sethi_entrust_orgname(StrTool.cTrim(tSubContext[24])); //寿险委托方机构名称
        tMixCrsUser.setprt_check_date(StrTool.cTrim(tSubContext[25])); //财险审批时间
        tMixCrsUser.setli_check_date(StrTool.cTrim(tSubContext[26])); //健康险审批时间
        tMixCrsUser.sethi_check_date(StrTool.cTrim(tSubContext[27])); //寿险审批时间
        //根据健康险委托方机构编码从LDUser表中获取对应用户编码和密码
//        tLDUserDB.setComCode(StrTool.cTrim(tSubContext[21]));
//        tLDUserSet=tLDUserDB.query();
        String sql =" select usercode,password from lduser where comcode like '"+StrTool.cTrim(tSubContext[21]).substring(0, 4)+"%' and crs_check_status ='00' and usercode = 'jc"+StrTool.cTrim(tSubContext[21]).substring(2, 4)+"01'"; 
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS == null || tSSRS.getMaxRow()<=0){
        	buildError("parseMsg", "获取可登录集团交叉用户名和密码失败！");
            return false;
        }
        for(int i=1;i<=tSSRS.getMaxRow();i++){
 
        	tMixCrsUser.setusercode(StrTool.cTrim(tSSRS.GetText(i, 1)));//用户编码
        	tMixCrsUser.setpassword(StrTool.cTrim(tSSRS.GetText(i, 2)));//密码        	
        }        
        tMixCrsUser.setSerialno(PubFun1.CreateMaxNo("SCRNO", 10)); //流水号

        tMixCrsUser.setmakedate(PubFun.getCurrentDate());
        tMixCrsUser.setmaketime(PubFun.getCurrentTime());
        tMixCrsUser.setmodifydate(PubFun.getCurrentDate());
        tMixCrsUser.setmodifytime(PubFun.getCurrentTime());

        mMap = new MMap();
        mMap.put(tMixCrsUser, "DELETE&INSERT");
        if (!SubmitMap())
        {
            return false;
        }
        return true;
    }

    /*数据处理*/
    private boolean SubmitMap()
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult = new VData();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, ""))
        {
            buildError("SubmitMap", "提交数据失败！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "MixedParseCrsUser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) throws Exception
    {
        MixedParseCrsUser mMixedCrsUserTask = new MixedParseCrsUser();
        mMixedCrsUserTask
                .run("R-I-000002-11000000-S003-test123123-20100527123430: 3508812019|000002|中国人民财产保险股份有限公司|35080200|中国人民财产保险股份有限公司龙岩新罗区支|吴兰兰||||||||1|在职||||I|||86110000||||||20090328 15:30:33");
        
        return;

    }
}