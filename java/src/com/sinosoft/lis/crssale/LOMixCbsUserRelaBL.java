package com.sinosoft.lis.crssale;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;

/*
 * <p>ClassName: LAComPersonalBL </p>
 * <p>Description: LAComPersonalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2005-11-01
 */

public class LOMixCbsUserRelaBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    private MMap map = new MMap();

    /** 业务处理相关变量 */
    private LOMCommissionSchema mLOMCommissionSchema = new LOMCommissionSchema();
    private LOMixCbsUserRelaSchema mLOMixCbsUserRelaSchema = new LOMixCbsUserRelaSchema();

    public LOMixCbsUserRelaBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOMixCbsUserRelaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LOMixCbsUserRelaBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LOMixCbsUserRelaBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        int tCount = 0;
//        String currentDate = PubFun.getCurrentDate();
//        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {

//            System.out
//                    .println("与机构关系放入完毕111111111111111111111111111111111111!");
            String toperatorcode = mLOMCommissionSchema.getoperatorcode();
            if (toperatorcode.equals("") || toperatorcode == null)
            {
                CError tError = new CError();
                tError.moduleName = "LOMixCbsUserRelaBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "代理方出单人员工号为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLOMixCbsUserRelaSchema.setoperatorcode(toperatorcode);
            
            String tcomp_cod = mLOMCommissionSchema.getcomp_cod();
            if (tcomp_cod.equals("") || tcomp_cod == null)
            {
                CError tError = new CError();
                tError.moduleName = "LOMixCbsUserRelaBL";
                tError.functionName = "dealdata";
                tError.errorMessage = " 子公司代码为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLOMixCbsUserRelaSchema.setcomp_cod(tcomp_cod);
            
            String tman_org_cod = mLOMCommissionSchema.getman_org_cod();
            if (tman_org_cod.equals("") || tman_org_cod == null)
            {
                CError tError = new CError();
                tError.moduleName = "LOMixCbsUserRelaBL";
                tError.functionName = "dealdata";
                tError.errorMessage = " 管理机构编号为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLOMixCbsUserRelaSchema.setman_org_cod(tman_org_cod);
            mLOMixCbsUserRelaSchema.setCrs_Check_Status("00");
            
            map.put(mLOMCommissionSchema, "INSERT");
            map.put(mLOMixCbsUserRelaSchema, "INSERT");
        }
        else if (this.mOperate.equals("UPDATE||MAIN")
                || this.mOperate.equals("DELETE||MAIN"))
        {
            
            if (this.mOperate.equals("UPDATE||MAIN"))
            {
                map.put(mLOMCommissionSchema, "UPDATE");
                map.put(mLOMixCbsUserRelaSchema, "UPDATE");
            }

            if (this.mOperate.equals("DELETE||MAIN"))
            {
                map.put(mLOMCommissionSchema, "DELETE");
                map.put(mLOMixCbsUserRelaSchema, "DELETE");
            }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLOMCommissionSchema.setSchema((LOMCommissionSchema) cInputData
                .getObjectByObjectName("LOMCommissionSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOMixCbsUserRelaBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
