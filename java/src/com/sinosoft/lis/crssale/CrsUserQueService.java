package com.sinosoft.lis.crssale;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

public class CrsUserQueService
{
    private final static Logger cLogger = Logger
            .getLogger(CrsUserQueService.class);

    public CrsUserQueService()
    {
        cLogger.info("集团交叉销售保单查询(CrsUserQueServive.java)");
    }

    public String service(String pInXmlStr)
    {
        cLogger.info("Into CrsUserQueServive.service()...");

        long mStartMillis = System.currentTimeMillis();
        cLogger.debug(pInXmlStr);
        String mOutXmlStr = null;

        try
        {
            //获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(pInXmlStr);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            //保存网站传入的xml到本地文件系统
            //			String tSaveName = "Card_" + DateUtil.getCurrentDate("HHmmss") + ".xml";
            //			SaveMessage.save(tInXmlDoc, tSaveName, "InNoStd");

            //解密及校验(待沟通)

            //调用核心后台处理
            CrsUserQueryDeal tCrsUserQueryDeal = new CrsUserQueryDeal(tInXmlDoc);
            Document tOutXmlDoc = tCrsUserQueryDeal.deal();

            //			保存返回给网站的xml到本地文件系统
            //            String tSaveName = "Card_" + DateUtil.getCurrentDate("HHmmss")
            //                    + ".xml";
            //            SaveMessage.save(tOutXmlDoc, tSaveName, "OutStd");

            //加密及校验(待沟通)

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tOutXmlDoc, tStringWriter);
            mOutXmlStr = tStringWriter.toString();
        }
        catch (Throwable ex)
        {
            cLogger.error("交易出错！", ex);
            mOutXmlStr = getError(ex.toString());
        }

        long mDealMillis = System.currentTimeMillis() - mStartMillis;
        cLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");

        cLogger.debug(mOutXmlStr);
        cLogger.info("Out CardService.service()!");
        return mOutXmlStr;
    }

    //非预期异常处理
    public String getError(String pErrorMsg)
    {
        cLogger.info("Into BaiFenService.getError()...");

        cLogger.info("Out BaiFenService.getError()!");
        return null;
    }

    //    public static void main(String[] args) throws Exception
    //    {
    //        System.out.println("程序开始…");
    //
    //        String mInFilePath = "D:/eclipse_workspace/picch/ui/WEB-INF/ContListInfo_baifen_2.xml";
    //        String mOutFilePath = "D:/eclipse_workspace/picch/ui/WEB-INF/ContListInfo_baifen_out_2.xml";
    //
    //        InputStream mIs = new FileInputStream(mInFilePath);
    //        byte[] mInXmlBytes = IOTrans.InputStreamToBytes(mIs);
    //        String mInXmlStr = new String(mInXmlBytes, "utf-8");
    //        System.out.println(mInXmlStr + "\n");
    //
    //        String mOutXmlDoc = new CrsUserQueService().service(mInXmlStr);
    //        System.out.println(mOutXmlDoc + "\n");
    //
    //        //		JdomUtil.print(mOutXmlDoc);
    //
    //        OutputStream mOs = new FileOutputStream(mOutFilePath);
    //        mOs.write(mOutXmlDoc.getBytes("utf-8"));
    //        //		JdomUtil.output(mOutXmlDoc, mOs);
    //        mOs.flush();
    //        mOs.close();
    //
    //        System.out.println("成功结束！");
    //    }
}
