package com.sinosoft.lis.crssale.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.security.AlgorithmParameters;
import java.security.AlgorithmParametersSpi;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

/**
 * 
 * <p>
 * Title: rsa加密和解密
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Company: sysnet
 * </p>
 * 
 * @author zhang_lc
 * @date Jun 11, 2010
 */
public class SecurityRsa
{

    public static final String KEY_ALGORITHM = "RSA";

    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

    /**
     * 
     * <p>
     * Title: 公钥RSA加密
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param enStr
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static String encryptPubRsa(String enStr)
    {
        ObjectInputStream pub = null;
        String str = "";
        try
        {
            pub = new ObjectInputStream(getResourceAsStream("Skey_RSA_pub.dat"));
            RSAPublicKey publicKey = (RSAPublicKey) pub.readObject();
            byte code[] = SecurityRsa.encryptByPublicKey(enStr.getBytes(), publicKey);
            str = encryptBASE64(code);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return str;
    }

    /**
     * 
     * <p>
     * Title: 私钥RSA加密
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param enStr
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static String encryptPivRsa(String enStr)
    {
        ObjectInputStream pub = null;
        String str = "";
        try
        {
            pub = new ObjectInputStream(getResourceAsStream("Skey_RSA_priv.dat"));
            RSAPrivateKey privateKey = (RSAPrivateKey) pub.readObject();
            byte code[] = SecurityRsa.encryptByPrivateKey(enStr.getBytes(), privateKey);
            str = encryptBASE64(code);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return str;
    }

    private static InputStream getResourceAsStream(String fileName)
    {
        if (fileName == null)
        {
            return null;
        }
        InputStream in = SecurityRsa.class.getResourceAsStream(fileName);
        System.out.println("in0:" + in);
        if (in == null)
        {
            in = SecurityRsa.class.getResourceAsStream("/" + fileName);
            System.out.println("in1:" + in);
        }

        if (in == null)
        {
            try
            {
                in = new FileInputStream(fileName);
                System.out.println("in2:" + in);
            }
            catch (FileNotFoundException ex)
            {
                ex.printStackTrace();
            }
        }
        return in;
    }

    /**
     * 
     * <p>
     * Title: 公钥RSA解密
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param dphStr
     * @return
     */
    public static String decipheringPubRsa(String dphStr)
    {
        ObjectInputStream pub = null;
        String str = null;
        try
        {
            byte st[] = SecurityRsa.decryptBASE64(dphStr);
            pub = new ObjectInputStream(getResourceAsStream("Skey_RSA_pub.dat"));
            RSAPublicKey publicKey = (RSAPublicKey) pub.readObject();
            byte by[] = SecurityRsa.decryptByPublicKey(st, publicKey);
            str = new String(by);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return str;
    }

    /**
     * 
     * <p>
     * Title: 私钥RSA解密
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param dphStr
     * @return
     */
    public static String decipheringPivRsa(String dphStr)
    {
        ObjectInputStream pub = null;
        String str = null;
        try
        {
            byte st[] = SecurityRsa.decryptBASE64(dphStr);
            pub = new ObjectInputStream(getResourceAsStream("Skey_RSA_priv.dat"));
            RSAPrivateKey privateKey = (RSAPrivateKey) pub.readObject();
            byte by[] = SecurityRsa.decryptByPrivateKey(st, privateKey);
            str = new String(by);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return str;
    }

    /**
     * 
     * <p>
     * Title: 私钥RSA解密
     * </p>
     * <p>
     * Description:
     * </p>
     * 
     * @param dphStr
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws ShortBufferException
     */
    private static byte[] decryptByPrivateKey(byte[] data, RSAPrivateKey privateKey) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, ShortBufferException, IllegalBlockSizeException,
            BadPaddingException
    {
        // 对密钥解密
        // byte[] keyBytes = decryptBASE64(key);

        // 取得私钥钥
        // X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] plain2 = new byte[data.length];
        int ixplain2 = 0;
        int ixcode2 = 0;
        while ((data.length - ixcode2) > 128)
        {// 每128字节做一次解密
            ixplain2 += cipher.doFinal(data, ixcode2, 128, plain2, ixplain2);
            ixcode2 += 128;
        }

        ixplain2 += cipher.doFinal(data, ixcode2, data.length - ixcode2, plain2, ixplain2);
        byte[] ru = new byte[ixplain2];
        for (int i = 0; i < ixplain2; i++)
        {
            ru[i] = plain2[i];
        }

        return ru;
    }

    public static byte[] decryptBASE64(String str) throws IOException
    {
        sun.misc.BASE64Decoder ba = new sun.misc.BASE64Decoder();
        byte deccoded[] = ba.decodeBuffer(str);
        return deccoded;

    }

    public static String encryptBASE64(byte[] by) throws IOException
    {
        sun.misc.BASE64Encoder ea = new sun.misc.BASE64Encoder();
        String encoded = ea.encode(by);
        return encoded;

    }

    /** */
    /**
     * 解密<br>
     * 用公钥钥解密
     * 
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPublicKey(byte[] data, PublicKey publicKey) throws Exception
    {
        // 对密钥解密
        // byte[] keyBytes = decryptBASE64(key);

        // 取得公钥
        // X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        //      KeyFactory keyFactory = KeyFactory.getInstance("RSA/ /ZeroPadding");
        // Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据解密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] plain2 = new byte[data.length];
        int ixplain2 = 0;
        int ixcode2 = 0;
        while ((data.length - ixcode2) > 128)
        {// 每128字节做一次解密
            ixplain2 += cipher.doFinal(data, ixcode2, 128, plain2, ixplain2);
            ixcode2 += 128;
        }

        ixplain2 += cipher.doFinal(data, ixcode2, data.length - ixcode2, plain2, ixplain2);
        byte[] ru = new byte[ixplain2];
        for (int i = 0; i < ixplain2; i++)
        {
            ru[i] = plain2[i];
        }

        return ru;
    }

    /** */
    /**
     * 加密<br>
     * 用公钥加密
     * 
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPublicKey(byte[] data, PublicKey publicKey) throws Exception
    {
        // 对公钥解密
        // byte[] keyBytes = decryptBASE64(key);

        // 取得公钥
        // X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] code = new byte[(((data.length - 1) / 117 + 1)) * 128];
        int ixplain = 0;
        int ixcode = 0;
        while ((data.length - ixplain) > 117)
        {// 每117字节做一次加密
            ixcode += cipher.doFinal(data, ixplain, 117, code, ixcode);
            ixplain += 117;
        }
        cipher.doFinal(data, ixplain, data.length - ixplain, code, ixcode);
        return code;
    }

    /**
     * 加密<br>
     * 用公钥加密
     * 
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPrivateKey(byte[] data, PrivateKey privateKey) throws Exception
    {
        // 对公钥解密
        // byte[] keyBytes = decryptBASE64(key);

        // 取得公钥
        // X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // Key publicKey = keyFactory.generatePublic(x509KeySpec);

        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] code = new byte[(((data.length - 1) / 117 + 1)) * 128];
        int ixplain = 0;
        int ixcode = 0;
        while ((data.length - ixplain) > 117)
        {// 每117字节做一次加密
            ixcode += cipher.doFinal(data, ixplain, 117, code, ixcode);
            ixplain += 117;
        }
        cipher.doFinal(data, ixplain, data.length - ixplain, code, ixcode);
        return code;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchProviderException,
            NoSuchPaddingException, Exception
    {
        /**
         * 实验一：公钥加密，私钥解密
         * 结论：成功。
         */
        //        String tIde = "4a765ed1-9c0f-4c55-a8a7-a42bdb4854ab";
        //        String tRes_1 = SecurityRsa.encryptPubRsa(tIde);
        //        System.out.println(tRes_1);
        //        String ss = SecurityRsa.decipheringPivRsa(tRes_1);
        //        System.out.println(ss);
        /**
         * 实验2：私钥加密，公钥解密
         * 结论：失败。
         */
        //        String tIde1 = "4a765ed1-9c0f-4c55-a8a7-a42bdb4854ab";
        //        String tRes_11 = SecurityRsa.encryptPivRsa(tIde1);
        //        System.out.println(tRes_11);
        //        String ss1 = SecurityRsa.decipheringPubRsa(tRes_11);
        //        System.out.println(ss1);
    }

}
