package com.sinosoft.lis.crssale.up;

import java.io.StringWriter;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LDCodeDB;

public class CrsBaseSendCrsUserXml {

	public boolean sendCrsUserXml(String cUserCode) {
		MixedGetAgentCodeCD tBus = new MixedGetAgentCodeCD();

		String tStrRes = tBus.run(cUserCode, "I");
		if (tStrRes == null) {
			return false;
		}

		String tStrXml = packXml(tStrRes);
		System.out.println(tStrXml);

		if (tStrXml == null) {
			return false;
		}

		if (!sendWS(tStrXml)) {
			return false;
		}

		return true;
	}

	private String packXml(String cStrRes) {
		Element tReceiver = new Element("RECEIVER");
		tReceiver.setText("CENTER");

		Element tMessage = new Element("MESSAGE");
		tMessage.setText(cStrRes);

		Element tMsgType = new Element("MSGTYPE");
		tMsgType.setText("mainData");

		Element tMsgName = new Element("MSGNAME");
		String tMsgHead = null;
		int tIdx = cStrRes.indexOf(":");
		if (tIdx == -1) {
			tMsgHead = "";
			errLog("报文样式不符合规范。");
			return null;
		} else {
			tMsgHead = cStrRes.substring(0, tIdx - 1);
		}
		tMsgName.setText(tMsgHead);

		Element mRootData = new Element("sysnet");
		mRootData.addContent(tReceiver);
		mRootData.addContent(tMessage);
		mRootData.addContent(tMsgType);
		mRootData.addContent(tMsgName);

		Document tDocDealResult = new Document(mRootData);

		String tOutXmlStr = null;
		try {
			StringWriter tStringWriter = new StringWriter();
			XMLOutputter tXMLOutputter = new XMLOutputter();
			tXMLOutputter.output(tDocDealResult, tStringWriter);

			tOutXmlStr = tStringWriter.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		// cMsgResult: 01-成功；02-失败。
		return tOutXmlStr;
	}

	private boolean sendWS(String tInXmlStr) {
		try {

			LDCodeDB tCodeInfoDB = new LDCodeDB();
			tCodeInfoDB.setCodeType("crs_ws_url");
			tCodeInfoDB.setCode("crs_ws_up");
			if (!tCodeInfoDB.getInfo()) {
				errLog("获取配置信息失败。");
				return false;
			}

			String targetEendPoint = tCodeInfoDB.getCodeName();
			System.out.println("targetEendPoint:" + targetEendPoint);

			String tFunApi = tCodeInfoDB.getCodeAlias();
			System.out.println("tFunApi:" + tFunApi);

			RPCServiceClient client = new RPCServiceClient();

			EndpointReference erf = new EndpointReference(targetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			QName name = new QName(targetEendPoint, tFunApi);
			Object[] object = new Object[] { tInXmlStr };
			Class[] returnTypes = new Class[] { String.class };
			Object[] response = client
					.invokeBlocking(name, object, returnTypes);
			String p = (String) response[0];

			System.out.println("return:" + p);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * 记录错误日志
	 * 
	 * @param cErrInfo
	 */
	private void errLog(String cErrInfo) {
		// cLogger.error(cErrInfo);
		// this.mErrInfo.append(cErrInfo);
		System.out.println(cErrInfo);
	}
}
