package com.sinosoft.lis.crssale.up;

import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>
 * Title:MixedAgentTask.java
 * </p>
 * 
 * <p>
 * Description:集团交叉销售批处理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:sinosoft
 * </p>
 * 
 * @author huxl
 * @version 1.0
 */
public class MixedGetAgentCodeCD
{
    public String mCurrentDate = PubFun.getCurrentDate();

    public MixedGetAgentCodeCD()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String run(String agentcode, String cOptType)
    {
        System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
                + PubFun.getCurrentTime());

        String tResult = getMixedAgentcode(agentcode, cOptType);
        if (tResult == null)
        {
            return null;
        }

        System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
                + PubFun.getCurrentTime());

        return tResult;
    }

    private String getMixedAgentcode(String agentcode, String cOptType)
    {
        String tResult = getS001(agentcode, cOptType);
        if (tResult == null)
        {
            return null;
        }

        return tResult;
    }

    /**
     * 营销员信息表
     * 
     * @return
     */
    private String getS001(String operatorcode, String cOptType)
    {
        String strCont = "";
        String strContent = "";

        String tOpeType = cOptType;
        if (!"I".equals(tOpeType) && !"U".equals(tOpeType))
        {
            System.out.println("操作符不符合规范。");
            return null;
        }

        try
        {
            String tSQL = "select 'P','" + tOpeType
                    + "','000085',operatorcode,'S003', operatorcode, " + " '"
                    + PubFun.getCurrentDate2() + PubFun.getCurrentTime2()
                    + "' 子公司报送时间,'' 时间戳 " + "from lomcommission "
                    + "where serialno = '" + operatorcode + "' with ur";

            SSRS tSSRS = new ExeSQL().execSQL(tSQL);
            if (tSSRS.getMaxRow() <= 0)
            {
                return null;
            }
            String tempString = tSSRS.encode();
            String[] tempStringArr = tempString.split("\\^");
            System.out.println(tempStringArr);

            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                String t = tempStringArr[i].replaceAll("\\|", "\\-");
                t = t.substring(0, t.length() - 1);
                strCont = t;
            }

            String aSQL = "select Serialno "
                    + "from lomcommission " + "where serialno = '"
                    + operatorcode + "' with ur";

            SSRS aSSRS = new ExeSQL().execSQL(aSQL);
            if (aSSRS.getMaxRow() <= 0)
            {
                return null;
            }
            String tempString1 = aSSRS.encode();
            String strCont1 = "";
            String[] tempStringArr1 = tempString1.split("\\^");
            System.out.println(tempStringArr1);

            for (int t = 1; t <= aSSRS.getMaxRow(); t++)
            {
//                String a = tempStringArr1[t].replaceAll("\\|", "\\-");
//                a = a.substring(0, a.length() - 1);
//                strCont1 = a;
                String a = tempStringArr1[t];
                strCont1 = a;
            }

            String mSQL = "select operatorcode,comp_cod,comp_nam,"
                    + " man_org_cod,man_org_nam,prt_entrust_orgid ,prt_entrust_orgname ,"
                    + " '' ," + " li_entrust_orgid,li_entrust_orgname," + " '"
                    + strCont1 + "',"

                    + " hi_entrust_orgid,hi_entrust_orgname,''"
                    + " from lomcommission " + " where serialno = '"
                    + operatorcode + "' with ur";

            SSRS mSSRS = new ExeSQL().execSQL(mSQL);
            if (mSSRS.getMaxRow() <= 0)
            {
                return null;
            }
            String tempStringCont = mSSRS.encode();
            String[] tempStringContArr = tempStringCont.split("\\^");
            System.out.println(tempStringContArr);

            for (int i = 1; i <= mSSRS.getMaxRow(); i++)
            {
                String m = tempStringContArr[i].replaceAll("\\|", "','");
                String[] mArr = m.split("\\,");
                mArr[3] = mArr[3].substring(1, mArr[3].length() - 1);
                System.out.println(tempStringContArr[i]);
                strContent += tempStringContArr[i];
            }
        }
        catch (Exception ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
        String strManagecomCont = strCont + ":" + strContent;
        System.out.println(strManagecomCont);
        return strManagecomCont;
    }

    //    public static void main(String args[])
    //    {
    //        MixedGetAgentCodeCD tMixedAgentTask = new MixedGetAgentCodeCD();
    //        tMixedAgentTask.run("3508812019");
    //        return;
    //    }

    private void jbInit() throws Exception
    {
    }

}
