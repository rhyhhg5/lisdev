/**
 * 2010-8-15
 */
package com.sinosoft.lis.crssale.logon;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.crssale.util.SecurityRsa;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * @author LY
 *
 */
public class LogonRsaAct
{
    private String mEmployeeCode = null;

    private String mOrgId = null;

    private String mOrgType = null;

    private String mCenterOrgId = null;

    private String mGepCod = null;
    
    //获取用户编码和密码
    public SSRS tSSRS = null;

    public LogonRsaAct()
    {
    }

    public boolean services(String cIdentity)
    {
        String tIdentity = cIdentity;
        if (tIdentity == null || tIdentity.equalsIgnoreCase(""))
        {
            errLog("验证信息获取失败。");
            return false;
        }

        String tUserXml = parseIdentity(tIdentity);
        if (tUserXml == null || "".equals(tUserXml))
        {
            errLog("验证失败");
            return false;
        }

        if (!parseUserInfo(tUserXml))
        {
            return false;
        }

        return true;
    }

    private String parseIdentity(String cIdentity)
    {
        String tUserXml = null;

        // 解密验证信息
        String tSimple = SecurityRsa.decipheringPivRsa(cIdentity);
        System.out.println("tSimple:" + tSimple);
        // --------------------

        // 加密验证信息
        String tCipher = SecurityRsa.encryptPubRsa(tSimple);
        System.out.println("tCipher:" + tCipher);
        // --------------------

        // web验证反馈
        String tInDocXml = packResult(tCipher);
        tUserXml = swapIdentity(tInDocXml);
        System.out.println("tUserXml_Cipher:" + tUserXml);
        //        tUserXml = "Kj8V3viH9jEGAHxvOZhzZVVFfDmcP/p6VgddMUA/eU340ugWzorubkqfLcy1BwM01+AqwUdYvYdWgNyCs5oapxDFUfHgOIxEgf10a1GXUUx26+GOOqhg9D6ktbMWfpV406SS1iif0boDYlF1iFpKOPIkraLcGgCZpj6ZCYyZUoAAiwQXE4vOz11lyC8L8j0eOLyD7FjMWMCrEBx/2t91h8pHIVoJeVOPGRqxE9NqYlMqboqqGNt8v8QxQLHIBe5ajrWnjQNGs16bezDAU2sRoVzmgHUdnXuNr7Q5Olzi0yEOw82xxkDJoMu9VaySri0jy1JT7qpxawQwj6M6qJb/9kgAsh2HV+ffUpLIYIR4Dd0kv7fEJRSSuVGDE+CqmrXl9VrMbeu1+ejlOTVBeq3aZrLIpcqf6+IfVijrdAamI7hShGYYs2NVnpj83nacTFiU7CoekToknb4pL5ZRAorD0T+8PmbafZ2bpZI92HP7ezr994eJtN0HiIkLkBgcTLiQKC+zvu+i/ssoWMEo5UGAvB4Kqu1UE7lJpNTFgUEJsPFo4mnz4XmGTTvI0WPy6U+HkCzt2+KV1xwpVHJAoEu/djcATUOEtTDwTvCn4Ccp6enVOuEdLvWdO0lXshDRJvbkaaveko8lcyBSOkAryrAyd7R0VbqlyEFeyaGEBSMjZ/I=";
        // --------------------

        // 解密出单人员信息
        tUserXml = SecurityRsa.decipheringPivRsa(tUserXml);
        System.out.println("tUserXml_Simple:" + tUserXml);
        // --------------------

        return tUserXml;
    }

    private String swapIdentity(String cStrXml)
    {
        try
        {
            LDCodeDB tCodeInfoDB = new LDCodeDB();
            tCodeInfoDB.setCodeType("crs_ws_url");
            tCodeInfoDB.setCode("crs_ws_logon");
            if (!tCodeInfoDB.getInfo())
            {
                errLog("获取配置信息失败。");
                return null;
            }

            String targetEendPoint = tCodeInfoDB.getCodeName();
            System.out.println("targetEendPoint:" + targetEendPoint);

            String tFunApi = tCodeInfoDB.getCodeAlias();
            System.out.println("tFunApi:" + tFunApi);

            RPCServiceClient client = new RPCServiceClient();

            EndpointReference erf = new EndpointReference(targetEendPoint);
            Options option = client.getOptions();
            option.setTo(erf);
            QName name = new QName(targetEendPoint, tFunApi);
            Object[] object = new Object[] { new Integer(1111), cStrXml };
            Class[] returnTypes = new Class[] { String.class };
            Object[] response = client
                    .invokeBlocking(name, object, returnTypes);
            String p = (String) response[0];

            System.out.println("return:" + p);

            return p;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private boolean parseUserInfo(String cUserXml)
    {
        try
        {
            // 获取传入的xml数据，并解析为org.jdom.Document
            StringReader tInXmlReader = new StringReader(cUserXml);
            SAXBuilder tSAXBuilder = new SAXBuilder();
            Document tInXmlDoc = tSAXBuilder.build(tInXmlReader);

            if (tInXmlDoc == null)
            {
                errLog("获取报文失败。");
                return false;
            }

            Element tRootData = tInXmlDoc.getRootElement();
            if (tRootData == null)
            {
                errLog("无报文数据");
                return false;
            }

            Element tBody = tRootData.getChild("BODY");
            mEmployeeCode = tBody.getChildTextTrim("EMPLOYEECODE");
            mOrgId = tBody.getChildTextTrim("ORGID");
            mOrgType = tBody.getChildTextTrim("ORGTYPE");
            mCenterOrgId = tBody.getChildTextTrim("CENTERORGID");
            mGepCod = tBody.getChildTextTrim("GEPCOD");

//            String tStrSql = "select 1 from lomcommission where Serialno = '"
//                    + mEmployeeCode + "'";
            //获取用户编码usercode，密码password，健康险委托方机构编码   2101022 by gzh
            String tStrSql = "select usercode,password,li_entrust_orgid from lomcommission where Serialno = '"
                + mEmployeeCode + "'";
            tSSRS = new ExeSQL().execSQL(tStrSql);
            if(tSSRS.getMaxRow()!=1){
            	errLog("查询结果不唯一。");
                return false;
            }
            if(tSSRS.GetText(1, 1)==null || tSSRS.GetText(1, 1).equals("")){
            	errLog("获取的用户编码为空。");
                return false;
            }
            if(tSSRS.GetText(1, 2)==null || tSSRS.GetText(1, 2).equals("")){
            	errLog("获取的用户密码为空。");
                return false;
            }
            if(tSSRS.GetText(1, 3)==null || tSSRS.GetText(1, 3).equals("")){
            	errLog("获取的健康险委托方机构编码为空。");
                return false;
            }

//            if (!"1".equals(new ExeSQL().getOneValue(tStrSql)))
//            {
//                errLog("校验身份失败。");
//                return false;
//            }

            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private String packResult(String cMsgResult)
    {
        try
        {
            String tResXml = null;

            //            Element tClassic = new Element("CLASSIC");
            //            tClassic.setText("SSO");
            //
            //            Element tQuenu = new Element("QUENU");
            //            tQuenu.setText("");
            //
            //            Element tStatus = new Element("STATUS");
            //            tStatus.setText("");
            //
            //            Element tHead = new Element("HEAD");
            //            tHead.addContent(tClassic);
            //            tHead.addContent(tQuenu);
            //            tHead.addContent(tStatus);
            //
            //            CDATA tCDIdentity = new CDATA("IDENTITY");
            //            tCDIdentity.setText(cMsgResult);
            //
            //            Element tIdentity = new Element("IDENTITY");
            //            //            tIdentity.setText(cMsgResult);
            //            tIdentity.addContent(tCDIdentity);
            //
            //            Element tBody = new Element("BODY");
            //            tBody.addContent(tIdentity);
            //
            //            Element mRootData = new Element("PACKET");
            //            mRootData.addAttribute("type", "RESPONSE");
            //            mRootData.addAttribute("version", "1.0");
            //
            //            mRootData.addContent(tHead);
            //            mRootData.addContent(tBody);

            Element tIdentity = new Element("IDENTITY");
            tIdentity.setText(cMsgResult);

            Element mRootData = new Element("SYSNET");
            mRootData.addContent(tIdentity);

            Document tDocDealResult = new Document(mRootData);

            StringWriter tStringWriter = new StringWriter();
            XMLOutputter tXMLOutputter = new XMLOutputter();
            tXMLOutputter.output(tDocDealResult, tStringWriter);
            tResXml = tStringWriter.toString();

            return tResXml;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("校验报文失败。");
            return null;
        }

    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        //        cLogger.error(cErrInfo);
        //        this.mErrInfo.append(cErrInfo);
        System.out.println(cErrInfo);
    }

    public String getCenterOrgId()
    {
        return mCenterOrgId;
    }

    public String getEmployeeCode()
    {
        return mEmployeeCode;
    }

    public String getGepCod()
    {
        return mGepCod;
    }

    public String getOrgId()
    {
        return mOrgId;
    }

    public String getOrgType()
    {
        return mOrgType;
    }

	public SSRS getSSRS() {
		return tSSRS;
	}

	public void setSSRS(SSRS tssrs) {
		tSSRS = tssrs;
	}
    
}
