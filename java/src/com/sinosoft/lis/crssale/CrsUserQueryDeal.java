package com.sinosoft.lis.crssale;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CrsUserQueryDeal
{
    private final static Logger cLogger = Logger
            .getLogger(CrsUserQueryDeal.class);

    private final Document cInXmlDoc;

    private String mDealState = null;

    private StringBuffer mErrInfo = new StringBuffer();

    /** 报文处理成功 */
    public final static String DEAL_SUCC = new String("00");

    /** 报文处理失败 */
    public final static String DEAL_FAIL = new String("01");

    // ----- 卡单中所需数据 -----
    private String mOperator = null;

    private String mBak1 = null;

    private String mBak2 = null;

    private String mBak3 = null;

    // -------------------------

    public CrsUserQueryDeal(Document pInXmlDoc)
    {
        cInXmlDoc = pInXmlDoc;
    }

    public Document deal()
    {
        cLogger.info("Into ContInfoQueryDeal.deal()...");

        if (!dealContXml())
        {
            this.mDealState = CrsUserQueryDeal.DEAL_FAIL;
        }
        else
        {
            this.mDealState = CrsUserQueryDeal.DEAL_SUCC;
        }

        // 以报文形式，返回处理结果
        Document tDealResultDoc = createDealResultDoc();
        // --------------------

        cLogger.info("Out BusinessDeal.deal()!");

        return tDealResultDoc;
    }

    /**
     * 处理接收到的报文xml
     * @return
     */
    private boolean dealContXml()
    {
        // 装载报文中业务数据。
        if (!loadData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkLogicData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkBaseData())
        {
            return false;
        }

        return true;
    }

    private boolean loadData()
    {
        Document tTmpDoc = this.cInXmlDoc;

        if (tTmpDoc == null)
        {
            return false;
        }

        Element tRootData = tTmpDoc.getRootElement();

        this.mOperator = tRootData.getChildTextTrim("OPERATORCODE");

        return true;
    }

    /**
     * 校验报文数据是否完整。
     * @return
     */
    private boolean checkBaseData()
    {
        return true;
    }

    /**
     * 校验业务逻辑
     * @return
     */
    private boolean checkLogicData()
    {
        if (this.mOperator == null)
        {
            errLog("传入的身份标识为空,请检查");
            return false;
        }
        return true;
    }

    private Document createDealResultDoc()
    {
        String tOperatorCode = mOperator;
        String tStrSql = null;

        //查询符合条件的记录
        SSRS tSSRS = new SSRS();
        
        tStrSql = " select usercode 出单人员工号 "
        		+" , username 姓名 "
		        +" ,'' 出生日期 "
		        +" ,'' 性别代码 "
		        +" ,'' 证件类型代码 "
		        +" ,'' 证件号 "
		        +" ,'' 联系电话 "
		        +" ,'' 联系手机 "
		        +" ,'' 电子邮件 "
		        +" ,'' 在职状态代码 "
		        +" ,'' 在职状态名称 "
		        +" ,'' 入职时间 "
		        +" ,'' 离司时间 "
		        +" ,'000085' 子公司代码 "
		        +" ,'中国人民健康保险股份有限公司' 子公司名称 "
		        +" ,comcode 管理机构代码 "
		        +" ,(select name from ldcom where comcode = a.comcode) 管理机构名称 "
		        +" From lduser a where usercode ='"+this.mOperator+"'" ;

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tStrSql);

        Element tPacket = new Element("PACKET");
        Element tRootData = new Element("BODY");

        tPacket.addAttribute("type", "RESPONSE");
        tPacket.addAttribute("version", "1.0");

        Element mHeadData = new Element("HEAD");
        Element tOperator = new Element("CLASSIC");
        tOperator.setText(this.mOperator);

        Element tQuenu = new Element("QUENU"); //子公司编号+机构编号+员工工号
//        String mStrSql = " select distinct b.comp_cod,"
//                + " b.man_org_cod,b.operatorcode from LOMCommission b where 1=1 "
//                + " and b.operatorcode = '" + this.mOperator + "'";
        String mStrSql = "select 000085,comcode,usercode From lduser where usercode ='"+this.mOperator+"'";

        ExeSQL mExeSQL = new ExeSQL();
        SSRS mSSRS = new SSRS();
        mSSRS = mExeSQL.execSQL(mStrSql);

        Element tStatus = new Element("STATUS");
        if (tSSRS.MaxRow >= 500)
        {
            tStatus.setText("2");
        }
        else
        {
            if (tSSRS.MaxRow < 0)
            {
                tStatus.setText("0");
            }
            else
            {
                tStatus.setText("1");
            }

        }

        if (tSSRS.MaxRow > 0)
        {
            String comp_cod = mSSRS.GetText(1, 1);
            String man_org_cod = mSSRS.GetText(1, 2);
            String operatorcode = mSSRS.GetText(1, 3);
            String strVar = comp_cod + "|" + man_org_cod + "|" + operatorcode;
            tQuenu.setText(strVar);
        }

        mHeadData.addContent(tOperator);
        mHeadData.addContent(tQuenu);
        mHeadData.addContent(tStatus);
        tPacket.addContent(mHeadData);

        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            Element mRootData = new Element("BODY");
            Element tOperCode = new Element("OPERATORCODE");
            tOperCode.setText(tSSRS.GetText(i, 1));

            Element tSalesName = new Element("SALESNAME");
            tSalesName.setText(tSSRS.GetText(i, 2));

            Element tDateBirthday = new Element("DATEBIRTHD");
            tDateBirthday.setText(tSSRS.GetText(i, 3));

            Element tSex = new Element("SEXCOD");
            tSex.setText(tSSRS.GetText(i, 4));

            Element tIdtype = new Element("IDTYPCOD");
            tIdtype.setText(tSSRS.GetText(i, 5));

            Element tPaperWorkId = new Element("PAPERWORKID");
            tPaperWorkId.setText(tSSRS.GetText(i, 6));

            Element tTelePhone = new Element("TELEPHONE");
            tTelePhone.setText(tSSRS.GetText(i, 7));

            Element tSalesMob = new Element("SALESMOB");
            tSalesMob.setText(tSSRS.GetText(i, 8));

            Element tSalesMail = new Element("SALESMAIL");
            tSalesMail.setText(tSSRS.GetText(i, 9));

            Element tStatuScod = new Element("STATUSCOD");
            tStatuScod.setText(tSSRS.GetText(i, 10));

            Element tStatusFla = new Element("STATUS");
            tStatusFla.setText(tSSRS.GetText(i, 11));

            Element tEnterantDate = new Element("ENTRANTDATE");
            tEnterantDate.setText(tSSRS.GetText(i, 12));

            Element tDimissionDate = new Element("DIMISSIONDATE");
            tDimissionDate.setText(tSSRS.GetText(i, 13));

            Element tCompCod = new Element("COMPCOD");
            tCompCod.setText(tSSRS.GetText(i, 14));

            Element tComPanam = new Element("COMPNAM");
            tComPanam.setText(tSSRS.GetText(i, 15));

            Element tManorgCod = new Element("MANORGCOD");
            tManorgCod.setText(tSSRS.GetText(i, 16));

            Element tManorgnam = new Element("MANORGNAM");
            tManorgnam.setText(tSSRS.GetText(i, 17));

            tRootData.addContent(tOperCode);
            tRootData.addContent(tSalesName);
            tRootData.addContent(tDateBirthday);
            tRootData.addContent(tSex);
            tRootData.addContent(tIdtype);
            tRootData.addContent(tPaperWorkId);
            tRootData.addContent(tTelePhone);
            tRootData.addContent(tSalesMob);
            tRootData.addContent(tSalesMail);
            tRootData.addContent(tStatuScod);
            tRootData.addContent(tStatusFla);
            tRootData.addContent(tEnterantDate);
            tRootData.addContent(tDimissionDate);
            tRootData.addContent(tCompCod);
            tRootData.addContent(tComPanam);
            tRootData.addContent(tManorgCod);
            tRootData.addContent(tManorgnam);

            // tRootData.addContent(mRootData);

        }
        tPacket.addContent(tRootData);
        Document tDocDealResult = new Document(tPacket);

        return tDocDealResult;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        cLogger.error(cErrInfo);
        this.mErrInfo.append(cErrInfo);
    }
}
