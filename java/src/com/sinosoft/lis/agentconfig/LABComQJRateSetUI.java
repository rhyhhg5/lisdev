package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.lis.agentdaily.LAAtoAscriptionBL;
import com.sinosoft.lis.vschema.LARateCommisionSet;
/**
 * <p>Title: LABComQJRateSetUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LABComQJRateSetUI
{
	
	public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LABComQJRateSetUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LABComQJRateSetBL tLABComQJRateSetBL=new LABComQJRateSetBL();
        try
        {
            if (!tLABComQJRateSetBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLABComQJRateSetBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptionUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
 //       mResult=tLAAtoAscriptionBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
	
    
}
