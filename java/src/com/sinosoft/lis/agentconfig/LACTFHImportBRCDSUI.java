package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LACTFHImportBRCDSUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LACTFHImportBRCDSBL mLACTFHImportBRCDSBL = null;

    public LACTFHImportBRCDSUI()
    {
    	mLACTFHImportBRCDSBL = new LACTFHImportBRCDSBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLACTFHImportBRCDSBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLACTFHImportBRCDSBL.mErrors);
            return false;
        }
        //
        if(mLACTFHImportBRCDSBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLACTFHImportBRCDSBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLACTFHImportBRCDSBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLACTFHImportBRCDSBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportBRCDSUI zLADiskImportBRCDSUI = new LADiskImportBRCDSUI();
    }
}
