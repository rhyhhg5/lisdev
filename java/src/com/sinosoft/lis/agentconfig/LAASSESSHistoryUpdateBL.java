package com.sinosoft.lis.agentconfig;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LAAssessHistoryDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;





public class LAASSESSHistoryUpdateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 全局变量 */
    private LAAssessHistorySchema mLAASSESSHistorySchema = new LAAssessHistorySchema();
    private LAAssessHistorySet mLAAssessHistorySet = new LAAssessHistorySet(); 

    public LAASSESSHistoryUpdateBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAH";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAH-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAH";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	LAAssessHistoryDB tLAWageHistoryDB = new LAAssessHistoryDB();
    	LAAssessHistorySet tLAAssessHistorySet = new LAAssessHistorySet();
        // 团队信息修改
    	String sql = "select * from laassesshistory where 1=1 and branchtype ='"+this.mLAASSESSHistorySchema.getBranchType()+"'" +
    			" and branchtype2 = '"+this.mLAASSESSHistorySchema.getBranchType2()+"' and ManageCom='"+this.mLAASSESSHistorySchema.getManageCom()+"' and indexcalno= '"+this.mLAASSESSHistorySchema.getIndexCalNo()+"' ";
    	tLAAssessHistorySet =tLAWageHistoryDB.executeQuery(sql);
    	for(int i = 1;i<=tLAAssessHistorySet.size();i++)
    	{
    		LAAssessHistorySchema tLAAssessHistorySchema = new LAAssessHistorySchema();
    		tLAAssessHistorySchema =tLAAssessHistorySet.get(i).getSchema();
    		tLAAssessHistorySchema.setState(this.mLAASSESSHistorySchema.getState());
    		tLAAssessHistorySchema.setOperator(this.mGlobalInput.Operator);
    		tLAAssessHistorySchema.setModifyDate(this.currentDate);
    		tLAAssessHistorySchema.setModifyTime(this.currentTime);
    		this.mLAAssessHistorySet.add(tLAAssessHistorySchema);
    	}
    	map.put(mLAAssessHistorySet, "UPDATE");
        return true;
    } 
    

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAASSESSHistorySchema.setSchema((LAAssessHistorySchema) cInputData.
                                            getObjectByObjectName(
                "LAAssessHistorySchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        
        
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAASSESSHistoryUpdateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mLAASSESSHistorySchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAASSESSHistoryUpdateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLAASSESSHistorySchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

 

    private boolean prepareOutputData()
    {
        try
        {
        	mInputData.clear();
        	mInputData.add(map);
           
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAASSESSHistoryUpdateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  
    public VData getResult()
    {
        return this.mResult;
    }

 

}
