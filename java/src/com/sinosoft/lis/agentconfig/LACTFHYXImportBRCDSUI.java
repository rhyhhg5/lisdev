package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LACTFHYXImportBRCDSUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LACTFHYXImportBRCDSBL mLACTFHYXImportBRCDSBL = null;

    public LACTFHYXImportBRCDSUI()
    {
    	mLACTFHYXImportBRCDSBL = new LACTFHYXImportBRCDSBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLACTFHYXImportBRCDSBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLACTFHYXImportBRCDSBL.mErrors);
            return false;
        }
        //
        if(mLACTFHYXImportBRCDSBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLACTFHYXImportBRCDSBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLACTFHYXImportBRCDSBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLACTFHYXImportBRCDSBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportBRCDSUI zLADiskImportBRCDSUI = new LADiskImportBRCDSUI();
    }
}
