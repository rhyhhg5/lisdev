/*
 * <p>ClassName: ALARewardPunishBL </p>
 * <p>Description: ALARewardPunishBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
//import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.*;

public class LABankRateConvRangeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LABaseWageSchema mLABaseWageSchema = new LABaseWageSchema();

    private LABaseWageSchema tLABaseWageSchema = new LABaseWageSchema();

    private LABaseWageSet mLABaseWageSet = new LABaseWageSet();

    private LABaseWageSet tLABaseWageSet = new LABaseWageSet();

    private String mName = "";
    private String mBranchAttr = "";
    private String mAgentGroup = "";
    private String mManageCom = "";


    public LABankRateConvRangeBL() {}

    public static void main(String[] args)
    { }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankRateConvRangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LABankRateConvRangeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        PubSubmit t = new PubSubmit();
         t.submitData(mInputData, "");
        // 准备往后台的数据
        if(!prepareOutputData()) {
                return false;
        }


            if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        } else
        {}
        mInputData = null;
        return true;
    }



    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
       // String tRewardPunishType = ""; //奖励类型

       if (this.mOperate.equals("INSERT||MAIN"))
        {
         String sql = "select * from labasewage where managecom='"+this.mLABaseWageSchema.getManageCom()+"' and type='02'";
         SSRS tSSRS = new SSRS();
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         if(tSSRS.getMaxRow()>0) {
             CError tError = new CError();
             tError.moduleName = "LABankRateConvRangeBL";
             tError.functionName = "dealData";
             tError.errorMessage = "该机构的此管理机构的提奖折算比例已经存在！";
             this.mErrors.addOneError(tError);
             return false;

         }

//         if (this.mLABaseWageSchema.getMonthRate()+this.mLABaseWageSchema.getSeasonRate()+this.mLABaseWageSchema.getYearRate()!=1)
//        {
//            CError tError = new CError();
//             tError.moduleName = "LABankRateRangeBL";
//             tError.functionName = "dealData";
//             tError.errorMessage = "月，季，年度折算比例之和必须为1！";
//             this.mErrors.addOneError(tError);
//             return false;
//        }

         // 确定记录顺序号
          String tSQL = "select max(Idx) from LABaseWage ";
           ExeSQL ttExeSQL = new ExeSQL();
           String tCount;
            int i;
            tCount = ttExeSQL.getOneValue(tSQL);
            if (tCount != null && !tCount.equals(""))
             {

              Integer tInteger = new Integer(tCount);
              i = tInteger.intValue();
               i = i + 1;
              }
                else
                {
                i = 1;
                }
              System.out.println(i);
               this.mLABaseWageSchema.setIdx(i);


               this.mLABaseWageSchema.setMakeDate(currentDate);
               this.mLABaseWageSchema.setMakeTime(currentTime);
               this.mLABaseWageSchema.setModifyDate(currentDate);
               this.mLABaseWageSchema.setModifyTime(currentTime);
               this.mLABaseWageSchema.setOperator(this.mGlobalInput.
                            Operator);
               System.out.println("nnnnnnnnnnnnnnnn"+this.mGlobalInput.
                            Operator);



            mMap.put(mLABaseWageSchema, "INSERT");

            mInputData.add(mMap);
        }



        if(this.mOperate.equals("UPDATE||MAIN")) {

            if ((this.mLABaseWageSchema.getMonthRate()>1||this.mLABaseWageSchema.getMonthRate()<0)
                ||(this.mLABaseWageSchema.getSeasonRate()>1||this.mLABaseWageSchema.getSeasonRate()<0)
                ||(this.mLABaseWageSchema.getYearRate()>1||this.mLABaseWageSchema.getYearRate()<0)
                ) {
                CError tError = new CError();
                tError.moduleName = "LABankRateConvRangeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "月,季，年度折算比例必须为0-1(包括0与1)的数！";
                this.mErrors.addOneError(tError);
                return false;
            }
//           else {
//               if (this.mLABaseWageSchema.getMonthRate()+this.mLABaseWageSchema.getSeasonRate()+this.mLABaseWageSchema.getYearRate()!=1)
//               {
//                   CError tError = new CError();
//                   tError.moduleName = "LABankRateRangeBL";
//                   tError.functionName = "dealData";
//                   tError.errorMessage = "月，季，年度折算比例之和必须为1！";
//                   this.mErrors.addOneError(tError);
//                   return false;
//               }
//
//           }


            LABaseWageDB tLABaseWageDB = new LABaseWageDB();
             tLABaseWageDB.setIdx(this.mLABaseWageSchema.getIdx());
             if(!tLABaseWageDB.getInfo()) {
//        	 @@错误处理
                  this.mErrors.copyAllErrors(tLABaseWageDB.mErrors);
                  CError tError = new CError();
                   tError.moduleName = "LABankRateConvRangeBL";
                   tError.functionName = "submitData";
                   tError.errorMessage = "数据提交失败!";
                   this.mErrors.addOneError(tError);
                   return false;
              }
           tLABaseWageSchema = tLABaseWageDB.getSchema();
           if(!tLABaseWageSchema.getManageCom().equals(this.mLABaseWageSchema.getManageCom())) {
               this.mErrors.copyAllErrors(tLABaseWageDB.mErrors);
                  CError tError = new CError();
                   tError.moduleName = "LABankRateConvRangeBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "管理机构不可修改!";
                   this.mErrors.addOneError(tError);
                   return false;

           }
           if (this.mLABaseWageSchema.getMonthRate()+this.mLABaseWageSchema.getSeasonRate()+this.mLABaseWageSchema.getYearRate()!=1)
           {
               CError tError = new CError();
               tError.moduleName = "LABankRateConvRangeBL";
               tError.functionName = "dealData";
               tError.errorMessage = "月，季，年度折算比例之和必须为100！";
               this.mErrors.addOneError(tError);
               return false;
           }


           tLABaseWageSchema.setManageCom(this.mLABaseWageSchema.getManageCom());
           tLABaseWageSchema.setType("02");
           tLABaseWageSchema.setMonthRate(this.mLABaseWageSchema.getMonthRate());
           tLABaseWageSchema.setSeasonRate(this.mLABaseWageSchema.getSeasonRate());
           tLABaseWageSchema.setYearRate(this.mLABaseWageSchema.getYearRate());
           tLABaseWageSchema.setOperator(this.mGlobalInput.
                       Operator);
           tLABaseWageSchema.setModifyDate(currentDate);
           tLABaseWageSchema.setModifyTime(currentTime);

          mMap.put(this.tLABaseWageSchema, "UPDATE");
          System.out.println(mMap);
          mInputData.clear();
          mInputData.add(mMap);
        }


        return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("oooooooooooooooo");
        this.mLABaseWageSchema.setSchema((LABaseWageSchema) cInputData.
                getObjectByObjectName(
                        "LABaseWageSchema",
                        0));


        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput",
                0));



        if (mGlobalInput == null || mManageCom == null) {
            CError tError = new CError();
            tError.moduleName = "LABankRateConvRangeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传入参数失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        System.out.println("Start ALABKRewardPunishBLQuery Submit...");
        LABaseWageDB tLABaseWageDB = new LABaseWageDB();
        tLABaseWageDB.setSchema(this.mLABaseWageSchema);
        this.mLABaseWageSet = tLABaseWageDB.query();
        this.mResult.add(this.mLABaseWageSet);
        System.out.println("End LABankRateConvRangeBLQuery Submit...");
        // 如果有需要处理的错误，则返回
        if (tLABaseWageDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABaseWageDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankRateConvRangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
//            this.mInputData = new VData();
//            this.mInputData.add(mName);
//             //this.mInputData.add(mBranchAttr);
//             this.mInputData.add(mAgentGroup);
//             this.mInputData.add(mManageCom);
            this.mInputData.add(this.mLABaseWageSchema);
            this.mInputData.add(this.mGlobalInput);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankRateConvRangeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LABankRateConvRangeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
