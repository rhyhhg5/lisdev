package com.sinosoft.lis.agentconfig;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: LAAChargeWrapRateUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LASITaxRateUI
{
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();
    
    private VData mResult = new VData();

    public LASITaxRateUI()
    {
        System.out.println("LASITaxRateUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LASITaxRateBL bl = new LASITaxRateBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }else{
        	 this.mResult = bl.getResult();
        }
        return true;
    }

    public static void main(String[] args)
    {
    	LASITaxRateUI tLASITaxRateUI = new   LASITaxRateUI();
         System.out.println("LAXBTQueryUI"); 
    }
}
