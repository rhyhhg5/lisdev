package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LARiskBankImportQuarter1UI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LARiskBankImportQuarter1BL mLARiskBankImportQuarter1BL = null;

    public LARiskBankImportQuarter1UI()
    {
    	mLARiskBankImportQuarter1BL = new LARiskBankImportQuarter1BL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLARiskBankImportQuarter1BL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLARiskBankImportQuarter1BL.mErrors);
            return false;
        }
        //
        if(mLARiskBankImportQuarter1BL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLARiskBankImportQuarter1BL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLARiskBankImportQuarter1BL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLARiskBankImportQuarter1BL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LARiskBankImportQuarter1UI zLARiskImportQuarterUI = new LARiskBankImportQuarter1UI();
    }
}
