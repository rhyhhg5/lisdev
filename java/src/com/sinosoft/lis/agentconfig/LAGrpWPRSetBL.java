package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDCodeSchema;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LAGrpWPRSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz
 * @version 1.0
 */
public class LAGrpWPRSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LDCodeSet mLDCodeSet = new LDCodeSet();
  private Reflections ref = new Reflections();
  //private LDCodeBSet mLDCodeBSet = new LDCodeBSet();

  public LAGrpWPRSetBL() {

  }

//	public static void main(String[] args)
//	{
//		LAGrpWPRSetBL LAGrpWPRSetBL = new LAGrpWPRSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAGrpWPRSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //检查数据
    if (!check()) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAGrpWPRSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAGrpWPRSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  private boolean check()
  {
	  //进行重复数据校验
	  System.out.println("Begin LAGrpWPRSetBL.check.........1"+mLDCodeSet.size());
	  StringBuffer tSB=new StringBuffer();
	  ExeSQL tExeSQL=new ExeSQL();
	  String tResult=null;
	  if(mOperate.equals("INSERT")){
		  LDCodeSchema tLDCodeSchema=null;
		  LDCodeSchema tLDCodeSchema2=null;
		  for(int i=1;i<=this.mLDCodeSet.size();i++){
			  tLDCodeSchema=this.mLDCodeSet.get(i);
			  tSB.append("select 1 from ldcode where codetype='")
			  	.append(tLDCodeSchema.getCodeType())
			  	.append("' and codename='")
			  	.append(tLDCodeSchema.getCodeName())
			  	.append("' and comcode='")
			  	.append(tLDCodeSchema.getComCode())
			  	.append("'");
			  tResult=tExeSQL.getOneValue(tSB.toString());
			  if(tResult!=null&&tResult.equals("1")){
				  CError tError = new CError();
			      tError.moduleName = "LAGRCSSpecialBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+((i-1)/2+1)+"行的险种和管理机构的设置信息在数据库已经存在。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
			  for(int j=i+1;j<=this.mLDCodeSet.size();j++){
				  tLDCodeSchema2=mLDCodeSet.get(j);
				  if(tLDCodeSchema.getCodeType()
						  .equals(tLDCodeSchema2.getCodeType())
						  &&tLDCodeSchema.getCodeName()
						  .equals(tLDCodeSchema2.getCodeName())
						  &&tLDCodeSchema.getComCode()
						  .equals(tLDCodeSchema2.getComCode())){
					  System.out.println("...........erro1");
					  CError tError = new CError();
				      tError.moduleName = "LAGRCSSpecialBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+((i-1)/2+1)+"行与第"+((j-1)/2+1)+"行数据重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
	      }
      }
	  if(mOperate.equals("DELETE")){
		  for(int i=1;i<=this.mLDCodeSet.size();i++){
			  if(this.mLDCodeSet.get(i).getComCode().equals("86")){
				  System.out.println("...........erro1");
				  CError tError = new CError();
			      tError.moduleName = "LAGRCSSpecialBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+((i-1)/2+1)+"行为总公司原始最高设置，不能删除，请进行修改。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
		  }
	  }
	  return true;
  }
  private String getCode(String codeType){
	  String tResult=null;
	  ExeSQL tExeSQL=new ExeSQL();
	  String tSQL="select max(cast(code as decimal)) from ldcode where codetype='"
		  +codeType+"'";
	  tResult=tExeSQL.getOneValue(tSQL);
	  if(tResult==null||tResult.equals("")){
		  tResult="0";
	  }
	  tResult=String.valueOf(Integer.parseInt(tResult)+1);
	  return tResult;  
  }
  
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAGrpWPRSetBL.getInputData.........");
      this.mErrors.clearErrors();
      this.mErrors.removeLastError();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLDCodeSet.set( (LDCodeSet) cInputData.get(1));
     this.mLDCodeSet.set( (LDCodeSet) cInputData.getObjectByObjectName("LDCodeSet",0));
      System.out.println("LDCodeSet get"+mLDCodeSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAGrpWPRSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAGrpWPRSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("INSERT")) {
        System.out.println("Begin LAGrpWPRSetBL.dealData.........1"+mLDCodeSet.size());
        for (int i = 1; i <= mLDCodeSet.size(); i++) {
        	mLDCodeSet.get(i).setCode(getCode(mLDCodeSet.get(i).getCodeType()));
        	mLDCodeSet.get(i).setCodeAlias(this.mGlobalInput.Operator);
//        	if(mLDCodeSet.get(i).getCodeType().equals("GWRISKCODE")){
//        		if(mLDCodeSet.get(i).getOtherSign().equals("0")){
//        			mLDCodeSet.get(i).setCodeAlias("AP0052");
//        		}else{
//        			mLDCodeSet.get(i).setCodeAlias("GW0052");
//        		}
//        	}else{
//        		if(mLDCodeSet.get(i).getOtherSign().equals("0")){
//        			mLDCodeSet.get(i).setCodeAlias("AP0153");
//        		}else{
//        			mLDCodeSet.get(i).setCodeAlias("GSP153");
//        		}
//        	}
        }
        map.put(mLDCodeSet, "INSERT");
      }else if ( mOperate.equals("UPDATE")){
    	  for (int i = 1; i <= mLDCodeSet.size(); i++) {
    		  mLDCodeSet.get(i).setCodeAlias(this.mGlobalInput.Operator);
//          	if(mLDCodeSet.get(i).getCodeType().equals("GWRISKCODE")){
//        		if(mLDCodeSet.get(i).getOtherSign().equals("0")){
//        			mLDCodeSet.get(i).setCodeAlias("AP0052");
//        		}else{
//        			mLDCodeSet.get(i).setCodeAlias("GW0052");
//        		}
//        	}else{
//        		if(mLDCodeSet.get(i).getOtherSign().equals("0")){
//        			mLDCodeSet.get(i).setCodeAlias("AP0153");
//        		}else{
//        			mLDCodeSet.get(i).setCodeAlias("GSP153");
//        		}
//        	}
    	  }
    	  map.put(mLDCodeSet, "UPDATE");
      }else if ( mOperate.equals("DELETE")){
      	  map.put(mLDCodeSet, "DELETE");
      }
    }catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAGrpWPRSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAGrpWPRSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAGrpWPRSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
