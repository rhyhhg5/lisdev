package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportHealthUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportHealthBL mLADiskImportHealthBL = null;

    public LADiskImportHealthUI()
    {
    	mLADiskImportHealthBL = new LADiskImportHealthBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportHealthBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportHealthBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportHealthBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportHealthBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportHealthBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportHealthBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportHealthUI zLADiskImportBRCDSUI = new LADiskImportHealthUI();
    }
}
