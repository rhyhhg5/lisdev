package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LARiskBankImportQuarterUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LARiskBankImportQuarterBL mLARiskBankImportQuarterBL = null;

    public LARiskBankImportQuarterUI()
    {
    	mLARiskBankImportQuarterBL = new LARiskBankImportQuarterBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLARiskBankImportQuarterBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLARiskBankImportQuarterBL.mErrors);
            return false;
        }
        //
        if(mLARiskBankImportQuarterBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLARiskBankImportQuarterBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLARiskBankImportQuarterBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLARiskBankImportQuarterBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LARiskBankImportQuarterUI zLARiskImportQuarterUI = new LARiskBankImportQuarterUI();
    }
}
