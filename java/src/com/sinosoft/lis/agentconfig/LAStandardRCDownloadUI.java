package com.sinosoft.lis.agentconfig;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAStandardRCDownloadUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAStandardRCDownloadUI()
    {
        System.out.println("LAStandardRCDownloadUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAStandardRCDownloadBL tLAStandardRCDownloadBL = new LAStandardRCDownloadBL();
        if(!tLAStandardRCDownloadBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tLAStandardRCDownloadBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
