/*
 * <p>ClassName: ALARewardPunishUI </p>
 * <p>Description: ALARewardPunishUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 奖惩管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABaseWageDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LABaseWageSchema;
import com.sinosoft.lis.vschema.LABaseWageSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LAAgentSet;
import java.util.ArrayList;


public class LABankRateConvRangeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 存放查询结果得容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    // 业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

        private LABaseWageSchema mLABaseWageSchema = new LABaseWageSchema();
        //private LARewardPInfoSet mmLARewardPInfoSet = new LARewardPInfoSet();
        private LABaseWageSet mLABaseWageSet = new LABaseWageSet();
        private String mName = "";
        private String mBranchAttr = "";
        private String mAgentGroup = "";
        private String mManageCom = "";


        public LABankRateConvRangeUI()
        {}


        public static void main(String[] args)
    {

    }

        /**
         * 传输数据的公共方法
         */
        public boolean submitData(VData cInputData, String cOperate)
        {
                // 将操作数据拷贝到本类中
                System.out.println("UI Start...");
                this.mOperate = cOperate;
                // 得到外部传入的数据,将数据备份到本类中
                if (!getInputData(cInputData))
                {
                        return false;
                }
                // 进行业务处理
                if (!dealData())
                {
                        return false;
                }
                // 准备往后台的数据
                if (!prepareOutputData())
                {
                        return false;
                }
                //System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa");

                LABankRateConvRangeBL tLABankRateConvRangeBL = new LABankRateConvRangeBL();
                System.out.println("Start LAActiRateManage UI Submit...");
                tLABankRateConvRangeBL.submitData(mInputData, mOperate);
                System.out.println("End LAActiRateManage UI Submit...");

                PubSubmit tPubSubmit = new PubSubmit();
                tPubSubmit.submitData(mInputData,"");
                // 如果有需要处理的错误，则返回
                if (tLABankRateConvRangeBL.mErrors.needDealError())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLABankRateConvRangeBL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LABankRateConvRangeUI";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                }
                if (mOperate.equals("QUERY||MAIN"))
                {
                        this.mResult.clear();
                        this.mResult = tLABankRateConvRangeBL.getResult();
                }
                mInputData = null;
                return true;
        }


        /**
         * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
         */
        private boolean prepareOutputData()
        {
                try
                {
                        mInputData.clear();

//                        mInputData.add(this.mName);
//                        //mInputData.add(this.mBranchAttr);
//                        mInputData.add(this.mAgentGroup);
//                        mInputData.add(this.mManageCom);

                        mInputData.add(this.mLABaseWageSchema);
                        mInputData.add(this.mGlobalInput);

                }
                catch (Exception ex)
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LABankRateConvRangeUI";
                        tError.functionName = "prepareData";
                        tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                        this.mErrors.addOneError(tError);
                        return false;
                }
                return true;
        }

        /**
         * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
         */
        private boolean dealData()
        {
                boolean tReturn = false;
                // 此处增加一些校验代码
                tReturn = true;
                return tReturn;
        }

        /**
         * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData)
        {
                // 全局变量
                 System.out.println("ertertertretertert");
//               mName = (String) cInputData.get(0);
//               // mBranchAttr = (String) cInputData.get(1);
//                 mAgentGroup = (String) cInputData.get(1);
//                 mManageCom = (String) cInputData.get(2);
                this.mLABaseWageSchema.setSchema((LABaseWageSchema) cInputData.getObjectByObjectName("LABaseWageSchema", 0));
                mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));


          //System.out.println(this.mManageCom);
                if (mGlobalInput == null)
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LABankRateConvRangeUI";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "没有得到足够的信息！";
                        this.mErrors.addOneError(tError);
                        return false;
                }
                System.out.println("UI finish get data...");
                return true;
        }

        public VData getResult()
        {
                return this.mResult;
        }
}
