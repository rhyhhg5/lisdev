package com.sinosoft.lis.agentconfig;

import java.io.File;


import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LABRCImportAMBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LAAwardMoneyImport.xml";
    private String mSQL = "";
    private String mAgentCode="";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	
    private LARewardPunishSet mLARewardPunishSetFinal=new LARewardPunishSet();
    

    private LARewardPunishSet mLARewardPunishSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private int NumJ=0;//
    private StringBuffer merrorInfo=new StringBuffer();
    public LABRCImportAMBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        //System.out.println("begin import");
        //从磁盘导入数据
        LARewardPunishImporter importer = new LARewardPunishImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("DiskImportRateCommisionBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LARewardPunish")) {
            mLARewardPunishSet = (LARewardPunishSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
    	
    	
        //展业证信息校验
        if (diskimporttype.equals("LARewardPunish")) {
            if (mLARewardPunishSet == null) {
                mErrors.addOneError("导入奖励方案奖金失败！没有得到要导入的数据或者导入模版不是最新，请下载最新的模版。");
                return false;
            } else {
            	String tempSQL="select * from ldcom where (length(trim(comcode))=4 or length(trim(comcode))=8)";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
                ExeSQL tExe1 = new ExeSQL();
            	LARewardPunishDB mTempLARewardPunishDB=new LARewardPunishDB();
            	//LARateCommisionSet mLARateCommisionSet=new LARateCommisionSet();
            	LARewardPunishSchema mTempLARewardPunishSchema=new LARewardPunishSchema();
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
                for (int i = 1; i <= mLARewardPunishSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLARewardPunishSchema=mLARewardPunishSet.get(i);
                	mSQL = "select agentcode from laagent where groupagentcode = '"+mTempLARewardPunishSchema.getAgentCode()+"' and managecom = '"+mTempLARewardPunishSchema.getManageCom()+"'";
                	mAgentCode=tExe1.getOneValue(mSQL);
                	String wSQL = "select * from lawage where agentcode = '"+mAgentCode+"' and indexcalno = '"+mTempLARewardPunishSchema.getWageNo()+
                	"' and managecom = '"+mTempLARewardPunishSchema.getManageCom()+"'";
                	SSRS mSSRS=new SSRS();
                    ExeSQL tExeSQL=new ExeSQL();
                    mSSRS=tExeSQL.execSQL(wSQL);
                    if(mSSRS!=null&&mSSRS.getMaxRow()>0){
                    	mErrors.addOneError("本月薪资已计算，不能录入");
                    	return false;
                    }
                    if (mTempLARewardPunishSchema.getAgentCode() == null ||
                    		mTempLARewardPunishSchema.getAgentCode().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("业务员代码为空/");
                    }
      		        if(!mAgentCode.equals("")){
      		    	   mTempLARewardPunishSchema.setAgentCode(mAgentCode); 
      		        }else{
    		    	   this.merrorNum++;
    		    	   this.merrorInfo.append("该管理机构下此业务员不存在/");
    		        }             	
                    if (mTempLARewardPunishSchema.getManageCom() == null ||
                    		mTempLARewardPunishSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构为空/");
                    }
                    if (mTempLARewardPunishSchema.getWageNo() == null ||
                    		mTempLARewardPunishSchema.getWageNo().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("薪资月为空/");
                    }
                    if (mTempLARewardPunishSchema.getWageNo() != null &&
                    		!mTempLARewardPunishSchema.getWageNo().equals("")
                    		
                    ) {
                       String wageno = mTempLARewardPunishSchema.getWageNo();
                       if(wageno.trim().length()==6){
//	                       int mYear =Integer.valueOf(wageno.trim().substring(0, 4)).intValue();
	                       int mMonth = Integer.valueOf(wageno.trim().substring(4, 6)).intValue();
//	                       System.out.println(mYear);
//	                       System.out.println(mMonth);
//	                       if(mYear<2015||mYear>2100){
//	                    	   this.merrorNum++;
//	                           this.merrorInfo.append("薪资月不正确/");
//	                       }
	                       if(mMonth<01||mMonth>12){
	                    	   this.merrorNum++;
	                           this.merrorInfo.append("薪资月不正确/");
                       }
                       }else{
                    	   this.merrorNum++;
                           this.merrorInfo.append("薪资月不正确/");
                       }
                    }  
                    
                    if(mTempLARewardPunishSchema.getManageCom()!=null
                    		&&!mTempLARewardPunishSchema.getManageCom().equals("")
                    		&&!mTempLARewardPunishSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLARewardPunishSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }                                                      

                  
                    
                    if(this.merrorNum==0){
                       checkExist(mTempLARewardPunishSchema,mTempLARewardPunishDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LARewardPunishSchema mLARewardPunishSchema,LARewardPunishDB mLARewardPunishDB){

	LARewardPunishSet tempLARewardPunishSet=new LARewardPunishSet();
	String tempSql="select * from LARewardPunish where managecom = '" 
		+mLARewardPunishSchema.getManageCom()+"' and agentcode = '"
		+mLARewardPunishSchema.getAgentCode()+"' and wageno = '"
		+mLARewardPunishSchema.getWageNo()+"' and aclass = '30'"
		+" and branchtype = '3' and branchtype2='01'"
		;
	System.out.println("checkExist:tempsql"+tempSql);
	tempLARewardPunishSet=mLARewardPunishDB.executeQuery(tempSql);
	if(tempLARewardPunishSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LARewardPunishSchema tempLARewardPunishSchema=new LARewardPunishSchema();
		tempLARewardPunishSchema.setManageCom(mLARewardPunishSchema.getManageCom());
		tempLARewardPunishSchema.setAgentCode(mLARewardPunishSchema.getAgentCode());
		tempLARewardPunishSchema.setWageNo(mLARewardPunishSchema.getWageNo());
		tempLARewardPunishSchema.setMoney(mLARewardPunishSchema.getMoney());
		mLARewardPunishSetFinal.add(tempLARewardPunishSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	 ExeSQL tExe = new ExeSQL();
    	 String tSql = "";
            
        if (diskimporttype.equals("LARewardPunish")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARewardPunishSetFinal.size();
            if(mLARewardPunishSetFinal!=null&&mLARewardPunishSetFinal.size()>0){
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLARewardPunishSetFinal.size(); i++) {
            	tSql =  "select int(max(idx)) from LARewardPunish where agentcode = '"+mLARewardPunishSetFinal.get(i).getAgentCode()+"' order by 1 desc ";
  	            String strIdx = "";
	            int tMaxIdx = 0;
	            strIdx = tExe.getOneValue(tSql);
	            if (strIdx == null || strIdx.trim().equals("")) {
	        	   tMaxIdx = 0;
	            }
	            else {
	        	   tMaxIdx = Integer.parseInt(strIdx);
	               //System.out.println(tMaxIdx);
	            }
	            tMaxIdx += i;
            	
              	System.out.println("preparedata : managecom"+mLARewardPunishSetFinal.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	mLARewardPunishSetFinal.get(i).setIdx(tMaxIdx);
              	mLARewardPunishSetFinal.get(i).setBranchType(branchtype);
              	mLARewardPunishSetFinal.get(i).setBranchType2(branchtype2);
              	mLARewardPunishSetFinal.get(i).setAClass("30");
              	mLARewardPunishSetFinal.get(i).setMakeDate(CurrentDate);
              	mLARewardPunishSetFinal.get(i).setMakeTime(CurrentTime);
              	mLARewardPunishSetFinal.get(i).setModifyDate(CurrentDate);
              	mLARewardPunishSetFinal.get(i).setModifyTime(CurrentTime);
              	mLARewardPunishSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mmap.put(mLARewardPunishSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportRateCommisionBL d = new DiskImportRateCommisionBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
