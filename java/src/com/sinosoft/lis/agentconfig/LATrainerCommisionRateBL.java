package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LATrainerIndexSet;
import com.sinosoft.lis.schema.LATrainerIndexSchema;
import com.sinosoft.lis.db.LATrainerIndexDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LATrainerCommisionRateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2018-6-7</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author WangQingMin 2018-6-7
 * @version 1.0
 */
public class LATrainerCommisionRateBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
//  private MMap newMap = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LATrainerIndexSet mLATrainerIndexSet = new LATrainerIndexSet();
  private Reflections ref = new Reflections();
  public LATrainerCommisionRateBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LATrainerCommisionRateBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionRateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LATrainerCommisionRateBL.check.........1"+mLATrainerIndexSet.size());	 
	  	LATrainerIndexSchema tempLATrainerIndexSchema1; 
	  	LATrainerIndexSchema tempLATrainerIndexSchema2;
	  	LATrainerIndexSchema tLATrainerIndexSchemaNew = new LATrainerIndexSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  LATrainerIndexDB tLATrainerIndexDB = new LATrainerIndexDB();
		  double sumRate=0;
		  double tSumRate=0;
		  String tSql="";
		  String strSumRate = "";
		  StringBuffer tSql1 ;
		  for(int i=1;i<=this.mLATrainerIndexSet.size();i++){
			  tSql1=new StringBuffer();
			  tempLATrainerIndexSchema1=this.mLATrainerIndexSet.get(i);
			  sumRate=tempLATrainerIndexSchema1.getCommisionRate();
	          tSql1.append("select sum(commisionRate) from LATrainerIndex where wagecode = 'GX0002' and wageno='"+tempLATrainerIndexSchema1.getWageNo()+"' and agentgroup= '"+tempLATrainerIndexSchema1.getAgentGroup()+"' and trainercode not in('1' ");
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLATrainerIndexSet.size();j++){
					  tempLATrainerIndexSchema2 = this.mLATrainerIndexSet.get(j);
					  if(tempLATrainerIndexSchema1.getPManageCom().equals(tempLATrainerIndexSchema2.getPManageCom())&&tempLATrainerIndexSchema1.getAgentGroup().equals(tempLATrainerIndexSchema2.getAgentGroup())&&tempLATrainerIndexSchema1.getTrainerCode().equals(tempLATrainerIndexSchema2.getTrainerCode())&&tempLATrainerIndexSchema1.getWageNo().equals(tempLATrainerIndexSchema2.getWageNo())){
							 BuildError("dealData","选中的第"+i+","+j+"行录入条件重复请检查！");
		                     return false; 
					  }
					  
					  //校验同一部门当月总绩效比例不能大于1！
					  if(tempLATrainerIndexSchema1.getAgentGroup().equals(tempLATrainerIndexSchema2.getAgentGroup())&&tempLATrainerIndexSchema1.getWageNo().equals(tempLATrainerIndexSchema2.getWageNo())){
	                          sumRate=sumRate+tempLATrainerIndexSchema2.getCommisionRate();
					  }
				  }
			
				  
				  
				  if(this.mOperate.equals("INSERT")){
		          tSql="select sum(commisionRate) from LATrainerIndex where wagecode = 'GX0002' and wageno='"+tempLATrainerIndexSchema1.getWageNo()+"' and agentgroup= '"+tempLATrainerIndexSchema1.getAgentGroup()+"' ";
		          strSumRate = tExeSQL.getOneValue(tSql);
				  }else{
					  //如果是修改，则进行修改查询sql
				  for(int k =1; k<=this.mLATrainerIndexSet.size();k++){
						  tempLATrainerIndexSchema2 = this.mLATrainerIndexSet.get(k);
						  //校验同一部门当月总绩效比例不能大于1！
						  if(tempLATrainerIndexSchema1.getAgentGroup().equals(tempLATrainerIndexSchema2.getAgentGroup())&&tempLATrainerIndexSchema1.getWageNo().equals(tempLATrainerIndexSchema2.getWageNo())){
		                          tSql1.append(",'"+tempLATrainerIndexSchema2.getTrainerCode()+"'");
						  }
					  }
                  tSql1.append(" ) with ur");
		          strSumRate = tExeSQL.getOneValue(tSql1.toString());
				  }
		     
		          if (strSumRate == null || strSumRate.trim().equals("")) {
		        	  tSumRate=0;
		          }
		          else {
		        	  tSumRate = Double.parseDouble(strSumRate);
		          } 
				  if((tSumRate+sumRate)>1){
					  BuildError("dealData","选中的第"+i+"行录入所在部门当月绩效比例已超出范围，请检查！");
	                     return false; 
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LATrainerIndex where  pmanagecom='")
				  .append(tempLATrainerIndexSchema1.getPManageCom())
				  .append("' and agentgroup = '")
				  .append(tempLATrainerIndexSchema1.getAgentGroup())
				  .append("' and wageno = '")
				  .append(tempLATrainerIndexSchema1.getWageNo())
				  .append("' and trainercode = '")
				  .append(tempLATrainerIndexSchema1.getTrainerCode())
				  .append("' and wagecode = '")
				  .append(tempLATrainerIndexSchema1.getWageCode())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LATrainerCommisionRateBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "选中的第"+i+"行该机构下人员月绩效比例已录入";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
			  if(this.mOperate.equals("UPDATE")){
					 tLATrainerIndexDB.setIdx(tempLATrainerIndexSchema1.getIdx());
					 tLATrainerIndexSchemaNew = tLATrainerIndexDB.query().get(1);
					 if((!tempLATrainerIndexSchema1.getPManageCom().equals(tLATrainerIndexSchemaNew.getPManageCom()))||(!tempLATrainerIndexSchema1.getAgentGroup().equals(tLATrainerIndexSchemaNew.getAgentGroup()))||(!tempLATrainerIndexSchema1.getWageNo().equals(tLATrainerIndexSchemaNew.getWageNo()))||(!tempLATrainerIndexSchema1.getTrainerCode().equals(tLATrainerIndexSchemaNew.getTrainerCode()))){
						 BuildError("dealData","选中的第"+i+"行修改时只能修改月绩效比例！");
	                     return false;
					 }
				 }
	  }
		  
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LATrainerCommisionRateBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLATrainerIndexSet.set( (LATrainerIndexSet) cInputData.getObjectByObjectName("LATrainerIndexSet",0));
      System.out.println("LATrainerIndexSet get"+mLATrainerIndexSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionRateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LATrainerCommisionRateBL.dealData........."+mOperate);
    try {
    	LATrainerIndexSchema  tLATrainerIndexSchemaNew = new  LATrainerIndexSchema();
    	LATrainerIndexSchema  tLATrainerIndexSchemaOld = new  LATrainerIndexSchema();
    	LATrainerIndexDB tLATrainerIndexDB =new  LATrainerIndexDB();
    	
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LATrainerCommisionRateBL.dealData.........INSERT"+mLATrainerIndexSet.size());
	        LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
	        for (int i = 1; i <= mLATrainerIndexSet.size(); i++) {
	        	tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);	
		          ExeSQL tExe = new ExeSQL();
		          String tSql=
		              "select max(int(idx)) from LATrainerIndex order by 1 desc";
		          String strIdx = "";
		          int tMaxIdx = 0;
		          strIdx = tExe.getOneValue(tSql);
		          if (strIdx == null || strIdx.trim().equals("")) {
		            tMaxIdx = 0;
		          }
		          else {
		            tMaxIdx = Integer.parseInt(strIdx);
		          }
		      tMaxIdx += i;
		      String sMaxIdx =tMaxIdx+"";
		      tLATrainerIndexSchemaNew.setIdx(sMaxIdx);
		      tLATrainerIndexSchemaNew.setMakeDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setMakeTime(CurrentTime);
		      tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
		      tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
      }
        	map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("UPDATE")) {
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
    		 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("DELETE")){
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){	
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
		      
    	 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionRateBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionRateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionRateBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
