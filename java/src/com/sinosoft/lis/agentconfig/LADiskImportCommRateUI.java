package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportCommRateUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportCommRateBL mLADiskImportCommRateBL = null;

    public LADiskImportCommRateUI()
    {
    	mLADiskImportCommRateBL = new LADiskImportCommRateBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportCommRateBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportCommRateBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportCommRateBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportCommRateBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportCommRateBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportCommRateBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportCommRateUI zLADiskImportBRCDSUI = new LADiskImportCommRateUI();
    }
}
