package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateChargeSet;
import com.sinosoft.lis.vschema.LARateChargeBSet;
import com.sinosoft.lis.schema.LARateChargeSchema;
import com.sinosoft.lis.schema.LARateChargeBSchema;
import com.sinosoft.lis.db.LARateChargeDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LAAChargeRateSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAAChargeRateSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateChargeSet mLARateChargeSet = new LARateChargeSet();
  private Reflections ref = new Reflections();

  public LAAChargeRateSetBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAAChargeRateSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAAChargeRateSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAAChargeRateSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验
	  System.out.println("Begin LAAChargeRateSetBL.check.........1"+mLARateChargeSet.size());
      if(!this.mOperate.equals("DELETE"))
      {
    	  LARateChargeSchema tempLARateChargeSchema1;
    	  LARateChargeSchema tempLARateChargeSchema2;
    	  ExeSQL tExeSQL=new ExeSQL();
    	  StringBuffer tSB;
    	  String tResult;
	      for(int i=1;i<=this.mLARateChargeSet.size();i++)
          {
	    	  tempLARateChargeSchema1=this.mLARateChargeSet.get(i);
		      for(int j=i+1;j<=this.mLARateChargeSet.size();j++){
		    	  tempLARateChargeSchema2=this.mLARateChargeSet.get(j);
			  if(tempLARateChargeSchema1.getOtherNo().equals(tempLARateChargeSchema2.getOtherNo())
					  &&tempLARateChargeSchema1.getRiskCode().equals(tempLARateChargeSchema2.getRiskCode())
					  &&tempLARateChargeSchema1.getStartDate().equals(tempLARateChargeSchema2.getStartDate())
					  &&tempLARateChargeSchema1.getEndDate().equals(tempLARateChargeSchema2.getEndDate())){
				           CError tError = new CError();
			               tError.moduleName = "LAAChargeRateSetBL";
			               tError.functionName = "check()";
			               tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
			               this.mErrors.clearErrors();
			               this.mErrors.addOneError(tError);
			               return false;
			           }
		     }
		  if(this.mOperate.equals("INSERT")){
			  LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
			  tLARateChargeSchema = mLARateChargeSet.get(i);
		      String sql = "select count(1) from (" 
		    	  +" select '1' from laratecharge "
		    	  +" where otherno='"+tLARateChargeSchema.getOtherNo()+"'" 
		    	  +" and riskcode='"+tLARateChargeSchema.getRiskCode()+"'" 
		    	  +" and startdate='"+tLARateChargeSchema.getStartDate()+"'" 
		    	  +" and enddate='"+tLARateChargeSchema.getEndDate()+"'" 
		    	  +" and othernotype='"+tLARateChargeSchema.getOtherNoType()+"'" 
		    	  +" and caltype = '"+tLARateChargeSchema.getCalType()+"'" 
		    	  +" union "
		    	  +" select '1' from laratecharge "
		    	  +" where otherno='"+tLARateChargeSchema.getOtherNo()+"'" 
		    	  +" and riskcode='"+tLARateChargeSchema.getRiskCode()+"'" 
		    	  +" and startdate<='"+tLARateChargeSchema.getStartDate()+"'" 
		    	  +" and enddate>='"+tLARateChargeSchema.getStartDate()+"'" 
		    	  +" and othernotype='"+tLARateChargeSchema.getOtherNoType()+"'" 
		    	  +" and caltype = '"+tLARateChargeSchema.getCalType()+"'" 
		    	  +" union "
		    	  +" select '1' from laratecharge "
		    	  +" where otherno='"+tLARateChargeSchema.getOtherNo()+"'" 
		    	  +" and riskcode='"+tLARateChargeSchema.getRiskCode()+"'" 
		    	  +" and startdate<='"+tLARateChargeSchema.getEndDate()+"'" 
		    	  +" and enddate>='"+tLARateChargeSchema.getEndDate()+"'" 
		    	  +" and othernotype='"+tLARateChargeSchema.getOtherNoType()+"'" 
		    	  +" and caltype = '"+tLARateChargeSchema.getCalType()+"'"
		    	  +") as hh ";
		  //ExeSQL tExeSQL = new ExeSQL();
		  String count = tExeSQL.getOneValue(sql);
		  System.out.println("..............count"+count);
		  if(count.compareTo("1")>=0)
		  {
			  CError tError = new CError();
		      tError.moduleName = "LAAChargeRateSetBL";
		      tError.functionName = "check";
		      tError.errorMessage = "数据库已经存在同样情况的比例,或者存在于有效起期和有效止期有交叉的记录,请执行修改或者删除!";
		      this.mErrors.addOneError(tError);
		      return false;
		  }
		}
		  if(this.mOperate.equals("UPDATE")){
			  LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
			  tLARateChargeSchema = mLARateChargeSet.get(i);
			  SSRS tSSRS=new SSRS();
			  String sql = " select count(1) from laratecharge "
		    	  +" where otherno='"+tLARateChargeSchema.getOtherNo()+"'" 
		    	  +" and riskcode='"+tLARateChargeSchema.getRiskCode()+"'" 
		    	  +" and startdate='"+tLARateChargeSchema.getStartDate()+"'" 
		    	  +" and othernotype='"+tLARateChargeSchema.getOtherNoType()+"'" 
		    	  +" and caltype = '"+tLARateChargeSchema.getCalType()+"'" 
		    	  +" and f03='"+tLARateChargeSchema.getF03()+"'"
		    	  ;
			  String count = tExeSQL.getOneValue(sql);
			  if(count.compareTo("0")==0)
			  {
				  CError tError = new CError();
			      tError.moduleName = "LAAChargeRateSetBL";
			      tError.functionName = "check";
			      tError.errorMessage = "只能修改有效止期和比例!";
			      this.mErrors.addOneError(tError);
			      return false;
			  }
			 
			  sql =" select count('1') from laratecharge "
		    	  +" where otherno='"+tLARateChargeSchema.getOtherNo()+"'" 
		    	  +" and riskcode='"+tLARateChargeSchema.getRiskCode()+"'" 
		    	  +" and startdate<='"+tLARateChargeSchema.getEndDate()+"'" 
		    	  +" and enddate>='"+tLARateChargeSchema.getEndDate()+"'" 
		    	  +" and othernotype='"+tLARateChargeSchema.getOtherNoType()+"'" 
		    	  +" and caltype = '"+tLARateChargeSchema.getCalType()+"'"
		    	  +" and f03<>'"+tLARateChargeSchema.getF03()+"'"
		    	  ;
			  count = tExeSQL.getOneValue(sql);
			  if(count.compareTo("1")>=0)
			  {
				  CError tError = new CError();
			      tError.moduleName = "LAAChargeRateSetBL";
			      tError.functionName = "check";
			      tError.errorMessage = "有效止期有交叉，请重新调整!";
			      this.mErrors.addOneError(tError);
			      return false;
			  }
		  }
      }
    }
	return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAAChargeRateSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

      this.mLARateChargeSet.set( (LARateChargeSet) cInputData.getObjectByObjectName("LARateChargeSet",0));
      System.out.println("LARateCommisionSet get"+mLARateChargeSet.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAChargeRateSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAAChargeRateSetBL.dealData........."+mOperate);
    try {
    	 if (mOperate.equals("UPDATE") ) {
    	        LARateChargeBSet tLARateChargeBSet = new LARateChargeBSet();
    	        for (int i = 1; i <= mLARateChargeSet.size(); i++) {
    	        	LARateChargeSchema mLARateChargeSchema=new LARateChargeSchema();
    	        	mLARateChargeSchema=mLARateChargeSet.get(i);
    	        	LARateChargeSchema tLARateChargeSchemaold = new   LARateChargeSchema();
    	        	LARateChargeDB tLARateChargeDB = new LARateChargeDB();
    	        	tLARateChargeDB.setOtherNo(mLARateChargeSchema.getOtherNo());
    	        	tLARateChargeDB.setOtherNoType(mLARateChargeSchema.getOtherNoType());
    	        	tLARateChargeDB.setRiskCode(mLARateChargeSchema.getRiskCode());
    	        	tLARateChargeDB.setF03(mLARateChargeSchema.getF03());
    	    	

    	        	tLARateChargeSchemaold = tLARateChargeDB.query().get(1);
    	        	String tsql="update LARATECHARGE set rate="+mLARateChargeSchema.getRate()+"  "
    	        	+",  enddate='"+mLARateChargeSchema.getEndDate()+"'"
    	        	+",modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' "
    	        	+" where OtherNo ='"+mLARateChargeSchema.getOtherNo()+"'"
    	        	+" and OtherNoType='"+mLARateChargeSchema.getOtherNoType()+"' "
    	        	+"and RiskCode='"+mLARateChargeSchema.getRiskCode()+"' "
    	        	+"and f03='"+mLARateChargeSchema.getF03()+"' "
    	        	+"and CalType='51' ";
    	        	
    	        	map.put(tsql, mOperate);
    	        	
    	            LARateChargeBSchema tLARateChargeBSchema = new LARateChargeBSchema();
    	            ref.transFields(tLARateChargeBSchema, tLARateChargeSchemaold);
    	            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    	            tLARateChargeBSchema.setEdorNo(tEdorNo);
    	         
    	        	  tLARateChargeBSchema.setEdorType("02");
    	         
    	          tLARateChargeBSet.add(tLARateChargeBSchema);

    	        }
    	        
    	        map.put(tLARateChargeBSet, "INSERT");
    	      }
    	      
    	      else if( mOperate.equals("DELETE"))
    	      {
    	    	  LARateChargeSet tLARateChargeSet = new LARateChargeSet();
    	          LARateChargeBSet tLARateChargeBSet = new LARateChargeBSet();
    	          for (int i = 1; i <= mLARateChargeSet.size(); i++) {
    	          	LARateChargeSchema mLARateChargeSchema=new LARateChargeSchema();
    	          	mLARateChargeSchema=mLARateChargeSet.get(i);
    	          	LARateChargeSchema tLARateChargeSchemaold = new   LARateChargeSchema();
    	          	LARateChargeDB tLARateChargeDB = new LARateChargeDB();
    	          	tLARateChargeDB.setOtherNo(mLARateChargeSchema.getOtherNo());
    	          	tLARateChargeDB.setOtherNoType(mLARateChargeSchema.getOtherNoType());
    	          	tLARateChargeDB.setRiskCode(mLARateChargeSchema.getRiskCode());
    	          	tLARateChargeDB.setF03(mLARateChargeSchema.getF03());
    	          	          	

    	          	tLARateChargeSchemaold = tLARateChargeDB.query().get(1);          	
    	          	tLARateChargeSet.add(tLARateChargeSchemaold);
    	          	
    	              LARateChargeBSchema tLARateChargeBSchema = new LARateChargeBSchema();
    	              ref.transFields(tLARateChargeBSchema, tLARateChargeSchemaold);
    	              String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    	              tLARateChargeBSchema.setEdorNo(tEdorNo);
    	          	  tLARateChargeBSchema.setEdorType("01");
    	              tLARateChargeBSet.add(tLARateChargeBSchema);

    	          }
    	          map.put(tLARateChargeSet, mOperate);
    	          map.put(tLARateChargeBSet, "INSERT");  
    	      }
    	      
    	      
      else {
    	  LARateChargeSet tLARateChargeSet = new  LARateChargeSet();
        for (int i = 1; i <= mLARateChargeSet.size(); i++) {
        	LARateChargeSchema tLARateChargeSchemanew = new LARateChargeSchema();
        	tLARateChargeSchemanew = mLARateChargeSet.get(i);
        	String tNo = PubFun1.CreateMaxNo("ENo", 10); 
        	tLARateChargeSchemanew.setF03(tNo);
        	tLARateChargeSchemanew.setOperator(mGlobalInput.Operator);
        	tLARateChargeSchemanew.setMakeDate(CurrentDate);
        	tLARateChargeSchemanew.setMakeTime(CurrentTime);
        	tLARateChargeSchemanew.setModifyDate(CurrentDate);
        	tLARateChargeSchemanew.setModifyTime(CurrentTime);
        	tLARateChargeSet.add(tLARateChargeSchemanew);
        }
        map.put(tLARateChargeSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAChargeRateSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAChargeRateSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  public static void main(String[] args)
  {
  	LAAChargeRateSetBL tLAAChargeRateSetBL = new   LAAChargeRateSetBL();
  	String tNo = PubFun1.CreateMaxNo("ENo", 10); 
       System.out.println("LAXBTQueryUI"+tNo); 
  }
}
