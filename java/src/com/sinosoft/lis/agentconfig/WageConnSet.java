package com.sinosoft.lis.agentconfig;

import java.sql.Connection;

import com.sinosoft.lis.db.LALinkWageDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LALinkWageBSchema;
import com.sinosoft.lis.schema.LALinkWageSchema;
import com.sinosoft.lis.vdb.LALinkWageBDBSet;
import com.sinosoft.lis.vdb.LALinkWageDBSet;
import com.sinosoft.lis.vschema.LALinkWageBSet;
import com.sinosoft.lis.vschema.LALinkWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class WageConnSet
{
//错误处理类
    public static CErrors mErrors = new CErrors();
//业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private LALinkWageSet mLALinkWageSet = new LALinkWageSet();
    private String mOldManageCom = "";
    private String mFlag = ""; //保存－S、修改－U


    public WageConnSet()
    {
    }

    public static void main(String[] args)
    {
        WageConnSet wageConnSet1 = new WageConnSet();
    }

    public boolean saveData()
    {
        LALinkWageSet tSet = new LALinkWageSet();
        PubFun tPF = new PubFun();
        String tWageCode = "";
        String tLastMode = "";
        String tLastAgentGrade = "";
        String tPayMode = "";

        int tCount = this.mLALinkWageSet.size();

        System.out.println("-----传入记录条数：" + tCount);
        tSet.clear();
        for (int i = 1; i <= tCount; i++)
        {
            System.out.println("-----for---" + i + "----");
            LALinkWageSchema tSch = new LALinkWageSchema();
            tSch.setSchema(this.mLALinkWageSet.get(i));
            System.out.println("-----tAgentGrade---" + tSch.getAgentGrade());
            tPayMode = tSch.getPayMode().trim();

            //校验同批保存的数据相同职级对应的发放方式是否相同
            if (tSch.getAgentGrade().trim().equals(tLastAgentGrade))
            {
                if (!tLastMode.trim().equals(tPayMode.trim()))
                {
                    dealError("saveData", "发放方式录入不一致！");
                    return false;
                }
            }

            //校验招募基金和创业奖的payMode是否一致
            tWageCode = tSch.getWageCode();
            System.out.println("-----WageCode = ---" + tWageCode + "----");
            if (tWageCode.trim().equals("W00059") ||
                tWageCode.trim().equals("W00060"))
            {

                if (!checkData(tSch))
                {
                    return false;
                }
            }
            System.out.println("--------checkData Pass-------");

            tSch.setOperator(this.mGlobalInput.Operator);
            tSch.setMakeDate(tPF.getCurrentDate());
            tSch.setMakeTime(tPF.getCurrentTime());
            tSch.setModifyDate(tPF.getCurrentDate());
            tSch.setModifyTime(tPF.getCurrentTime());
            tSet.add(tSch);

            tLastMode = tSch.getPayMode().trim();
            tLastAgentGrade = tSch.getAgentGrade().trim();
        }

        System.out.println("---保存条数--tSet.size = " + tSet.size());
        LALinkWageDBSet tDBSet = new LALinkWageDBSet();
        tDBSet.set(tSet);
        if (!tDBSet.insert())
        {
            System.out.println("--Save--Error--" +
                               tDBSet.mErrors.getFirstError().toString().trim());
            dealError("saveData", tDBSet.mErrors.getFirstError().toString());
            return false;
        }

        return true;
    }

    private boolean checkData(LALinkWageSchema tSch)
    {
        String tWageCode = "";
        String tPayMode1 = "";
        String tPayMode = "";
        String tSql = "";
        LALinkWageSet tSet = new LALinkWageSet();
        LALinkWageDB tDB = new LALinkWageDB();
        int tCount = 0;
        String tManageCom = "";
        if (this.mFlag.trim().equals("U"))
        {
            tManageCom = this.mOldManageCom.trim();
        }
        else
        {
            tManageCom = tSch.getManageCom().trim();
        }

        tPayMode1 = tSch.getPayMode();
        if (tSch.getWageCode().trim().equals("W00059"))
        {
            tWageCode = "W00060";
        }
        else
        {
            tWageCode = "W00059";
        }
        tSql = "select * from lalinkwage where (wagecode = '" + tWageCode.trim()
               + "' or wagecode = '" + tSch.getWageCode().trim()
               + "') and agentgrade = '" + tSch.getAgentGrade().trim()
               + "' and managecom = '" + tManageCom.trim() + "'";
        System.out.println("-----checkData---tSql = " + tSql);
        tSet = tDB.executeQuery(tSql);

        tCount = tSet.size();
        System.out.println("---checkData--1--tCount = " + tCount);
        if (tCount == 0)
        {
            return true;
        }

        for (int i = 1; i <= tCount; i++)
        {
            LALinkWageSchema tCheckSch = new LALinkWageSchema();
            tCheckSch.setSchema(tSet.get(i));
            tPayMode = tCheckSch.getPayMode();
            if (tCheckSch.getWageCode().equals(tSch.getWageCode()))
            {
                continue;
            }
            if (tPayMode.trim().equals(tPayMode1))
            {
                continue;
            }
            else
            {
                dealError("checkData",
                          "发放方式与" + tCheckSch.getWageCode().trim() + "不一致！");
                return false;
            }
        }
        return true;
    }

    public boolean updateData(String tOldManageCom)
    {
        LALinkWageSet tSet = new LALinkWageSet();
        LALinkWageSet tDSet = new LALinkWageSet();
        LALinkWageBSet tBSet = new LALinkWageBSet();

        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();
        String tWageCode = "";
        String tLastMode = "";
        String tLastAgentGrade = "";
        String tPayMode = "";
        this.mOldManageCom = tOldManageCom.trim();
        this.mFlag = "U";

        Reflections tRf = new Reflections();
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("updateData", "数据库连接失败！");
            return false;
        }
        LALinkWageBDBSet tBDBSet = new LALinkWageBDBSet(conn);
        LALinkWageDBSet tDBSet = new LALinkWageDBSet(conn);

        int tCount = this.mLALinkWageSet.size();
        System.out.println("-----更新条数：" + tCount);
        tSet.clear();
        tDSet.clear();
        tBSet.clear();
        for (int i = 1; i <= tCount; i++)
        {
            System.out.println("--updateData---for---" + i + "----");
            LALinkWageSchema tSch = new LALinkWageSchema();
            LALinkWageSchema tDSch = new LALinkWageSchema();
            LALinkWageBSchema tBSch = new LALinkWageBSchema();
            tSch.setSchema(this.mLALinkWageSet.get(i));
            tWageCode = tSch.getWageCode();

            tSch.setOperator(this.mGlobalInput.Operator);
            tSch.setMakeDate(tPF.getCurrentDate());
            tSch.setMakeTime(tPF.getCurrentTime());
            tSch.setModifyDate(tPF.getCurrentDate());
            tSch.setModifyTime(tPF.getCurrentTime());
            tSet.add(tSch);

            tDSch.setSchema(tSch);
            tDSch.setManageCom(this.mOldManageCom.trim());
            tDSet.add(tDSch);

            tRf.transFields(tBSch, tSch);
            tBSch.setManageCom(tOldManageCom.trim());
            tBSch.setOperator(this.mGlobalInput.Operator);
            tBSch.setMakeDate(tPF.getCurrentDate());
            tBSch.setMakeTime(tPF.getCurrentTime());
            tBSch.setModifyDate(tPF.getCurrentDate());
            tBSch.setModifyTime(tPF.getCurrentTime());
            tBSch.setEdorNo(tPF1.CreateMaxNo("EdorNo", 20));
            tBSch.setEdorType("02");
            tBSet.add(tBSch);

            tPayMode = tSch.getPayMode().trim();
            //校验同批保存的数据相同职级对应的发放方式是否相同
            if (tSch.getAgentGrade().trim().equals(tLastAgentGrade))
            {
                if (tLastMode.trim().equals(tPayMode.trim()))
                {
                    continue;
                }
                else
                {
                    dealError("saveData", "发放方式录入不一致！");
                    return false;
                }
            }

            //校验招募基金和创业奖的payMode是否一致
            if (tWageCode.trim().equals("W00059") ||
                tWageCode.trim().equals("W00060"))
            {
                if (!checkData(tSch))
                {
                    return false;
                }
            }

            tLastMode = tSch.getPayMode().trim();
            tLastAgentGrade = tSch.getAgentGrade().trim();
        }
        System.out.println("----tBSet.count = " + tBSet.size());
        System.out.println("----tSet.count = " + tSet.size());
        System.out.println("----tDSet.count = " + tDSet.size());

        try
        {
            conn.setAutoCommit(false);

            tBDBSet.set(tBSet);
            if (!tBDBSet.insert())
            {
                dealError("updateData",
                          tBDBSet.mErrors.getFirstError().toString());
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---insert b----");
            tDBSet.set(tDSet);
            if (!tDBSet.delete())
            {
                dealError("updateD0ata",
                          tDBSet.mErrors.getFirstError().toString());
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---delete----");
            tDBSet.set(tSet);
            if (!tDBSet.insert())
            {
                dealError("updateData", tDBSet.mErrors.getFirstError().toString());
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---insert----");

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("updateData", ex.toString());
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception et)
            {
                et.printStackTrace();
                dealError("updateData", et.toString());
                return false;
            }
            return false;
        }

        return true;
    }

    public boolean deleteData(VData tInputData)
    {
        String tManageCom = "";
        String tWageCode = "";
        String tAgentGrade = "";
        String tAreaType = "";
        LALinkWageSet tLALinkWageSet = new LALinkWageSet();
        LALinkWageSet tLALinkWageSetNew = new LALinkWageSet();
        try
        {

            this.mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            tLALinkWageSet = (LALinkWageSet) tInputData.getObjectByObjectName(
                    "LALinkWageSet", 0);
        }
        catch (Exception ex)
        {
            dealError("deleteData", "接收传入数据失败！");
            return false;
        }

        LALinkWageBSet tLALinkWageBSet = new LALinkWageBSet();
        Reflections tRf = new Reflections();
        PubFun tPF = new PubFun();
        PubFun1 tPF1 = new PubFun1();

        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("updateData", "数据库连接失败！");
            return false;
        }

        LALinkWageBDBSet tLALinkWageBDBSet = new LALinkWageBDBSet(conn);
        LALinkWageDBSet tLALinkWageDBSet = new LALinkWageDBSet(conn);
        int tCount = tLALinkWageSet.size();
        if (tCount == 0)
        {
            return true;
        }

        tLALinkWageBSet.clear();
        for (int i = 1; i <= tCount; i++)
        {
            LALinkWageSchema tLALinkWageSchema = new LALinkWageSchema();
            LALinkWageBSchema tLALinkWageBSchema = new LALinkWageBSchema();
            LALinkWageDB tLALinkWageDB = new LALinkWageDB();
            tLALinkWageSchema.setSchema(tLALinkWageSet.get(i));

            tLALinkWageDB.setAgentGrade(tLALinkWageSchema.getAgentGrade());
            tLALinkWageDB.setManageCom(tLALinkWageSchema.getManageCom());
            tLALinkWageDB.setWageCode(tLALinkWageSchema.getWageCode());
            tLALinkWageDB.setAreaType(tLALinkWageSchema.getAreaType());
            tLALinkWageDB.setPayPeriodType(tLALinkWageSchema.getPayPeriodType());
            tLALinkWageDB.setPayMonth(tLALinkWageSchema.getPayMonth());

            if (!tLALinkWageDB.getInfo())
            {
                dealError("deleteData", "查找不到需要被删除的记录");
                return false;
            }
            tLALinkWageSchema.setSchema(tLALinkWageDB.getSchema());
            tLALinkWageSetNew.add(tLALinkWageSchema);

            tRf.transFields(tLALinkWageBSchema, tLALinkWageSchema);
            tLALinkWageBSchema.setOperator(this.mGlobalInput.Operator);
            tLALinkWageBSchema.setMakeDate(tPF.getCurrentDate());
            tLALinkWageBSchema.setMakeTime(tPF.getCurrentTime());
            tLALinkWageBSchema.setModifyDate(tPF.getCurrentDate());
            tLALinkWageBSchema.setModifyTime(tPF.getCurrentTime());
            tLALinkWageBSchema.setEdorNo(tPF1.CreateMaxNo("EdorNo", 20));
            tLALinkWageBSchema.setEdorType("01");
            tLALinkWageBSet.add(tLALinkWageBSchema);

        }
        System.out.println("----tBSet.count = " + tLALinkWageBSet.size());

        try
        {
            conn.setAutoCommit(false);

            tLALinkWageBDBSet.set(tLALinkWageBSet);
            if (!tLALinkWageBDBSet.insert())
            {
                dealError("deleteData",
                          tLALinkWageBDBSet.mErrors.getFirstError().toString());
                conn.rollback();
                conn.close();
                return false;
            }

            tLALinkWageDBSet.add(tLALinkWageSetNew);
            if (!tLALinkWageDBSet.delete())
            {
                dealError("deleteData",
                          tLALinkWageDBSet.mErrors.getFirstError().toString());
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("deleteData", ex.toString());
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception et)
            {
                et.printStackTrace();
                dealError("deleteData", et.toString());
                return false;
            }
            return false;
        }
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mLALinkWageSet = (LALinkWageSet) tInputData.
                                  getObjectByObjectName(
                                          "LALinkWageSet", 0);
            this.mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception ex)
        {
            dealError("getInputData", "传入数据失败！");
            return false;
        }

        int tCount = mLALinkWageSet.size();
        System.out.println("-----InputData----set.count = " + tCount);
        if (tCount == 0)
        {
            dealError("getInputData", "传入LALinkWageSet为空！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "AssessConfirmSave";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

}
