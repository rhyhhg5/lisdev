package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LARiskImportQuarterUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LARiskImportQuarterBL mLARiskImportQuarterBL = null;

    public LARiskImportQuarterUI()
    {
    	mLARiskImportQuarterBL = new LARiskImportQuarterBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLARiskImportQuarterBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLARiskImportQuarterBL.mErrors);
            return false;
        }
        //
        if(mLARiskImportQuarterBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLARiskImportQuarterBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLARiskImportQuarterBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLARiskImportQuarterBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LARiskImportQuarterUI zLARiskImportQuarterUI = new LARiskImportQuarterUI();
    }
}
