package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportBRCDSDLUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportBRCDSDLBL mLADiskImportBRCDSDLBL = null;

    public LADiskImportBRCDSDLUI()
    {
    	mLADiskImportBRCDSDLBL = new LADiskImportBRCDSDLBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportBRCDSDLBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportBRCDSDLBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportBRCDSDLBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportBRCDSDLBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportBRCDSDLBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportBRCDSDLBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportBRCDSDLUI zLADiskImportBRCDSDLUI = new LADiskImportBRCDSDLUI();
    }
}
