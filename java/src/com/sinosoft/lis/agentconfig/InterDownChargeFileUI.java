package com.sinosoft.lis.agentconfig;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>
 * Title:健康险平台数据采集类 Description:
 * 
 * Company: 中科软科技股份有限公司
 * 
 * @author : czl
 * @date:2010-12-17
 * @version 1.0
 */
public class InterDownChargeFileUI {
	public CErrors mErrors = new CErrors();
	public GlobalInput tG = null;
	//先后传递的参数
	public VData mResult = new VData();
	public InterDownChargeFileUI() {
	}
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			System.out.println("*********InterDownChargeFileUI**********");
			tG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0); 
//			TransferData inputTransfer = (TransferData) cInputData.getObjectByObjectName("TransferData",0); 
//			mStartDate = (String) inputTransfer.getValueByName("StartDate");
//			mEndDate = (String) inputTransfer.getValueByName("EndDate");
			mResult.add(tG);
//			mResult.add(inputTransfer);
			JsonTest tInterDownChargeFileBL = new JsonTest();
			if (!tInterDownChargeFileBL.submitData(mResult, "")) {
				if (tInterDownChargeFileBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tInterDownChargeFileBL.mErrors);
					return false;
				} else {
					buildError("submitData","InterDownChargeFileBL.submitData出错");
					return false;
				}
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "tInterDownChargeFileBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "tInterDownChargeFileBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
