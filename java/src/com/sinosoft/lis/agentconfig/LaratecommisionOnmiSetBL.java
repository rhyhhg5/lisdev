package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LaratecommisionOnmiSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author lihai
 * @version 1.0
 */
public class LaratecommisionOnmiSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public LaratecommisionOnmiSetBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LaratecommisionOnmiSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!checkData()){
    	return false;
    }
    
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LaratecommisionOnmiSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LaratecommisionOnmiSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("endblhere:.........................");
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LaratecommisionOnmiSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
     this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));
      System.out.println("LARateCommisionSet get"+mLARateCommisionSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionOnmiSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  
  private boolean checkData()
  {
	  if(!this.mOperate.equals("DELETE"))
	  {
		  LARateCommisionSchema tLARateCommisionSchema1 = new LARateCommisionSchema();
		  LARateCommisionSchema tLARateCommisionSchema2 = new LARateCommisionSchema();
		  String tJudgePremType ="04,05";
		  String tJudgeSql ="select * from lmriskapp where taxoptimal = 'Y' with ur ";
		  LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
		  LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		  tLMRiskAppSet = tLMRiskAppDB.executeQuery(tJudgeSql);
		  
		  for (int i = 1; i <= mLARateCommisionSet.size(); i++) 
		  {
			tLARateCommisionSchema1 = mLARateCommisionSet.get(i);
			for (int j = 1; j <= tLMRiskAppSet.size(); j++) 
			{
				LMRiskAppSchema tLMRiskAppSchema = new LMRiskAppSchema();
				tLMRiskAppSchema = tLMRiskAppSet.get(j);
				if(tLMRiskAppSchema.getRiskCode().equals(tLARateCommisionSchema1.getRiskCode()))
				{   
					//税优产品的提奖比例的缴费方式一律按年缴的提取
					if(!"12".equals(tLARateCommisionSchema1.getPayIntv()))
					{
						System.out.println("...........erro1");
						  CError tError = new CError();
					      tError.moduleName = "LaratecommisionOnmiSetBL";
					      tError.functionName = "check()";
					      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行的险种为税优险种时，缴费方式只能取年缴的提奖比例！";
					      this.mErrors.clearErrors();
					      this.mErrors.addOneError(tError);
					      return false;
						
					}
					//税优产品保费类型只有：帐户保费；风险保费
					if(tJudgePremType.indexOf(tLARateCommisionSchema1.getF01()) == -1)
					{
						System.out.println("...........erro1");
						  CError tError = new CError();
					      tError.moduleName = "LaratecommisionOnmiSetBL";
					      tError.functionName = "check()";
					      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行的险种为税优险种时，保费类型只能为：04-帐户保费，05-风险保费";
					      this.mErrors.clearErrors();
					      this.mErrors.addOneError(tError);
					      return false;
					}
				}
				
			}
			for (int k = i+1; k <= mLARateCommisionSet.size(); k++) 
			{
				tLARateCommisionSchema2 = mLARateCommisionSet.get(k);
				if(tLARateCommisionSchema1.getManageCom()
						.equals(tLARateCommisionSchema2.getManageCom())&&
						tLARateCommisionSchema1.getPayIntv()
						.equals(tLARateCommisionSchema2.getPayIntv())&&
						tLARateCommisionSchema1.getCurYear()
						==tLARateCommisionSchema2.getCurYear()&&
						tLARateCommisionSchema1.getRiskCode()
						.equals(tLARateCommisionSchema2.getRiskCode())&&
						tLARateCommisionSchema1.getF01()
						.equals(tLARateCommisionSchema2.getF01())&&
						tLARateCommisionSchema1.getF03()
						==tLARateCommisionSchema2.getF03()&&
						tLARateCommisionSchema1.getF04()
						==tLARateCommisionSchema2.getF04())
				{
					System.out.println("...........erro1");
					  CError tError = new CError();
				      tError.moduleName = "LaratecommisionOnmiSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+k+"行数据重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				}
			}
		  }
		  
	  }
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LaratecommisionOnmiSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
        System.out.println("Begin LaratecommisionOnmiSetBL.dealData.........1"+mLARateCommisionSet.size());
        LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
        LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          System.out.println("Begin LaratecommisionOnmiSetBL.dealData.........2");
          LARateCommisionSchema tLARateCommisionSchemaold = new
              LARateCommisionSchema();
          LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
          LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
           System.out.println("Begin LaratecommisionOnmiSetBL.dealData.........3");
            System.out.println("++++++++"+mLARateCommisionSet.get(i).getRiskCode());
          System.out.println("++++++++"+mLARateCommisionSet.get(i).getIdx());


          tLARateCommisionDB.setIdx(mLARateCommisionSet.get(i).getIdx());
//          tLARateCommisionDB.setRiskCode(mLARateCommisionSet.get(i).getRiskCode());
          tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
          tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
          //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
          tLARateCommisionSchemanew.setMakeDate(tLARateCommisionSchemaold.
                                                getMakeDate());
          tLARateCommisionSchemanew.setF01(mLARateCommisionSet.get(i).
                  getF01());
          tLARateCommisionSchemanew.setMakeTime(tLARateCommisionSchemaold.
                                                getMakeTime());
          tLARateCommisionSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionSchemanew.setModifyTime(CurrentTime);
          tLARateCommisionSchemanew.setVersionType("11");
          tLARateCommisionSet.add(tLARateCommisionSchemanew);
          LARateCommisionBSchema tLARateCommisionBSchema = new
              LARateCommisionBSchema();
          ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
          //获取最大的ID号
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select int(max(idx)) from laratecommisionb order by 1 desc ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
            //System.out.println(tMaxIdx);
          }
          tMaxIdx++;
          tLARateCommisionBSchema.setIdx(tMaxIdx);
          if(i>1){
            tLARateCommisionBSchema.setIdx(tLARateCommisionBSet.get(i - 1).
                                           getIdx() + 1);
          }
          tLARateCommisionBSchema.setEdorNo(tLARateCommisionSchemaold.getIdx()+"");
          if(mOperate.equals("UPDATE"))
            tLARateCommisionBSchema.setEdorType("02");
          else
            tLARateCommisionBSchema.setEdorType("01");
          tLARateCommisionBSet.add(tLARateCommisionBSchema);

        }
        map.put(tLARateCommisionSet, mOperate);
        map.put(tLARateCommisionBSet, "INSERT");
      }
      else {

    	  LARateCommisionSet tLARateCommisionSet = new
          LARateCommisionSet();
  for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
      LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
      tLARateCommisionSchemanew = mLARateCommisionSet.get(i);

      String agentSQL =
      "select idx from laratecommision where riskcode='"+tLARateCommisionSchemanew.getRiskCode()+"'"
      +" and managecom='"+tLARateCommisionSchemanew.getManageCom()+"'"
      +" and curyear="+tLARateCommisionSchemanew.getCurYear()+""
      +" and payintv='"+tLARateCommisionSchemanew.getPayIntv()+"'"
      +" and f03="+tLARateCommisionSchemanew.getF03()+""
      +" and f04="+tLARateCommisionSchemanew.getF04()+""
      +" and f01='"+tLARateCommisionSchemanew.getF01()+"'"
      +" and branchtype='"+tLARateCommisionSchemanew.getBranchType()+"'"
      +" and branchtype2='"+tLARateCommisionSchemanew.getBranchType2()+"'";

       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(agentSQL);
       if(tSSRS.getMaxRow()>0)
       {
               // @@错误处理
       this.mErrors.copyAllErrors(tSSRS.mErrors);
       CError tError = new CError();
       tError.moduleName = "LaratecommisionOnmiSetBL";
       tError.functionName = "submitDat";
       tError.errorMessage = "已经保存了相同险种的佣金率内容，请重新录入!";
       this.mErrors.addOneError(tError);
       return false;
       }

      tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
tLARateCommisionSchemanew.setMakeDate(CurrentDate);
tLARateCommisionSchemanew.setMakeTime(CurrentTime);
tLARateCommisionSchemanew.setModifyDate(CurrentDate);
tLARateCommisionSchemanew.setModifyTime(CurrentTime);
tLARateCommisionSchemanew.setVersionType("11");
tLARateCommisionSet.add(tLARateCommisionSchemanew);

  }
  map.put(tLARateCommisionSet, mOperate);
}
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionOnmiSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LaratecommisionOnmiSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
      System.out.println(
          "end  LaratecommisionOnmiSetBL.prepareOutputData.........");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionOnmiSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
