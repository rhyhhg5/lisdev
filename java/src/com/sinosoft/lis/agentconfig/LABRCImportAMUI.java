package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LABRCImportAMUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LABRCImportAMBL mLABRCImportAMBL = null;

    public LABRCImportAMUI()
    {
    	mLABRCImportAMBL = new LABRCImportAMBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLABRCImportAMBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLABRCImportAMBL.mErrors);
            return false;
        }
        //
        if(mLABRCImportAMBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLABRCImportAMBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLABRCImportAMBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLABRCImportAMBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportBRCDSUI zLADiskImportBRCDSUI = new LADiskImportBRCDSUI();
    }
}
