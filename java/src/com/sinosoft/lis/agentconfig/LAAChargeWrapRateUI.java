package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.lis.agentquery.LAXBTQueryBL;
import com.sinosoft.lis.agentquery.LAXBTQueryUI;
import com.sinosoft.lis.vschema.LARateCommisionSet;
/**
 * <p>Title: LAAChargeWrapRateUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAAChargeWrapRateUI
{
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAAChargeWrapRateUI()
    {
        System.out.println("LAAChargeWrapRateUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAAChargeWrapRateBL bl = new LAAChargeWrapRateBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    	LAAChargeWrapRateBL tLAAChargeWrapRateBL = new   LAAChargeWrapRateBL();
         System.out.println("LAXBTQueryUI"); 
    }
}
