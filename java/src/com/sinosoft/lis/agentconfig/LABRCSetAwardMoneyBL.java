package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz 2009-1-23
 * @version 1.0
 */
public class LABRCSetAwardMoneyBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
  private Reflections ref = new Reflections();
  private String mSQL = "";
  private String mAgentCode="";
  public LABRCSetAwardMoneyBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LABRCSetAwardMoneyBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABRCSetAwardMoneyBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验
      ExeSQL tExe1 = new ExeSQL();
	  System.out.println("Begin LABRCSetAwardMoneyBL.check.........1"+mLARewardPunishSet.size());
	 
		  LARewardPunishSchema tempLARewardPunishSchema1;
		  LARewardPunishSchema tempLARewardPunishSchema2;
//		  LARewardPunishSchema tLARewardPunishSchemaold = new LARewardPunishSchema();
		  LARewardPunishSchema tLARewardPunishSchemanew = new LARewardPunishSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
		  for(int i=1;i<=this.mLARewardPunishSet.size();i++){
			  tempLARewardPunishSchema1=this.mLARewardPunishSet.get(i);
			  mSQL = "select agentcode from laagent where groupagentcode = '"+tempLARewardPunishSchema1.getAgentCode()+"'";
		      mAgentCode=tExe1.getOneValue(mSQL);
		      String wSQL = "select * from lawage where agentcode = '"+mAgentCode+"' and indexcalno = '"+tempLARewardPunishSchema1.getWageNo()+
          	"' and managecom = '"+tempLARewardPunishSchema1.getManageCom()+"'";
          	SSRS mSSRS=new SSRS();
              ExeSQL aExeSQL=new ExeSQL();
              mSSRS=aExeSQL.execSQL(wSQL);
              if(mSSRS!=null&&mSSRS.getMaxRow()>0){
		    	  CError tError = new CError();
			      tError.moduleName = "LABRCSetAwardMoneyBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "本月薪资已计算不能录入";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
              }
		      if(!mAgentCode.equals("")){
		    	  tempLARewardPunishSchema1.setAgentCode(mAgentCode); 
		      }else{
		    	  CError tError = new CError();
			      tError.moduleName = "LABRCSetAwardMoneyBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "该工号不存在";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
		      }
			  for(int j=i+1;j<=this.mLARewardPunishSet.size();j++){
				  tempLARewardPunishSchema2=this.mLARewardPunishSet.get(j);  
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from larewardpunish where managecom='")
				  .append(tempLARewardPunishSchema1.getManageCom())
				  .append("' and agentcode = '")
				  .append(tempLARewardPunishSchema1.getAgentCode())
				  .append("' and aclass = '")
				  .append(tempLARewardPunishSchema1.getAClass())
				  .append("' and wageno = '")
				  .append(tempLARewardPunishSchema1.getWageNo())
				  .append("' and branchtype='")
				  .append(tempLARewardPunishSchema1.getBranchType())
				  .append("' and branchtype2='")
				  .append(tempLARewardPunishSchema1.getBranchType2())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetAwardMoneyBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "该业务员该薪资月奖金已录入";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
			 if(mOperate.equals("UPDATE")){
				 tLARewardPunishDB.setAgentCode(tempLARewardPunishSchema1.getAgentCode());
				 tLARewardPunishDB.setIdx(tempLARewardPunishSchema1.getIdx());
				 tLARewardPunishSchemanew = tLARewardPunishDB.query().get(1);
				 if(!tempLARewardPunishSchema1.getManageCom().equals(tLARewardPunishSchemanew.getManageCom())||
					!tempLARewardPunishSchema1.getAgentCode().equals(tLARewardPunishSchemanew.getAgentCode())||
					!tempLARewardPunishSchema1.getWageNo().equals(tLARewardPunishSchemanew.getWageNo())
				 ){
					 BuildError("dealData","管理机构,业务员代码,薪资月不能修改！");
                     return false;
				 }
			 }
		  
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABRCSetAwardMoneyBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLARewardPunishSet.set( (LARewardPunishSet) cInputData.getObjectByObjectName("LARewardPunishSet",0));
      System.out.println("LARewardPunishSet get"+mLARewardPunishSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetAwardMoneyBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LABRCSetAwardMoneyBL.dealData........."+mOperate);
    try {
        LARewardPunishSchema  tLARewardPunishSchemaNew = new  LARewardPunishSchema();
        LARewardPunishSchema  tLARewardPunishSchemaOld = new  LARewardPunishSchema();
        LARewardPunishDB tLARewardPunishDB =new  LARewardPunishDB();
        if(mOperate.equals("INSERT")) {
        System.out.println("Begin LABRCSetAwardMoneyBL.dealData.........1"+mLARewardPunishSet.size());
        LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
        for (int i = 1; i <= mLARewardPunishSet.size(); i++) {
          tLARewardPunishSchemaNew = mLARewardPunishSet.get(i);	
	          ExeSQL tExe = new ExeSQL();
	          String tSql =
	              "select int(max(idx)) from LARewardPunish where agentcode = '"+tLARewardPunishSchemaNew.getAgentCode()+"' order by 1 desc ";
	          String strIdx = "";
	          int tMaxIdx = 0;
	          strIdx = tExe.getOneValue(tSql);
	          if (strIdx == null || strIdx.trim().equals("")) {
	            tMaxIdx = 0;
	          }
	          else {
	            tMaxIdx = Integer.parseInt(strIdx);
	            //System.out.println(tMaxIdx);
	          }
	      tMaxIdx += i;
	      tLARewardPunishSchemaNew.setIdx(tMaxIdx);
	      tLARewardPunishSchemaNew.setMakeDate(CurrentDate);
	      tLARewardPunishSchemaNew.setMakeTime(CurrentTime);
          tLARewardPunishSchemaNew.setModifyDate(CurrentDate);
          tLARewardPunishSchemaNew.setModifyTime(CurrentTime);
          tLARewardPunishSchemaNew.setOperator(mGlobalInput.Operator); 
          tLARewardPunishSet.add(tLARewardPunishSchemaNew);
      }
        	map.put(tLARewardPunishSet, "INSERT");
      }
      if(mOperate.equals("UPDATE")||mOperate.equals("DELETE")) {
    	 LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
    	 for(int i = 1;i<=mLARewardPunishSet.size();i++){		 
    		 tLARewardPunishDB.setIdx(mLARewardPunishSet.get(i).getIdx());
    		 tLARewardPunishDB.setAgentCode(mLARewardPunishSet.get(i).getAgentCode());
    		 tLARewardPunishSchemaOld = tLARewardPunishDB.query().get(1); 
    		 tLARewardPunishSchemaNew = mLARewardPunishSet.get(i);
    		 tLARewardPunishSchemaNew.setMakeDate(tLARewardPunishSchemaOld.getMakeDate());
    		 tLARewardPunishSchemaNew.setMakeTime(tLARewardPunishSchemaOld.getMakeTime());
    		 tLARewardPunishSchemaNew.setModifyDate(CurrentDate);
    		 tLARewardPunishSchemaNew.setModifyTime(CurrentTime);
    		 tLARewardPunishSchemaNew.setOperator(mGlobalInput.Operator); 
    		 tLARewardPunishSet.add(tLARewardPunishSchemaNew);
    	 }
    	 map.put(tLARewardPunishSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetAwardMoneyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetAwardMoneyBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LABRCSetAwardMoneyBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
