package com.sinosoft.lis.agentconfig;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: LAECWrapRateSetUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAECWrapRateSetUI
{
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAECWrapRateSetUI()
    {
        System.out.println("LAECWrapRateSetUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAECWrapRateSetBL bl = new LAECWrapRateSetBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    	LAECWrapRateSetBL tLAECWrapRateSetBL = new   LAECWrapRateSetBL();
         System.out.println("LAECWrapRateSetBL"); 
    }
}
