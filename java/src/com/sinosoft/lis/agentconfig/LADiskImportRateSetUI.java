package com.sinosoft.lis.agentconfig;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportRateSetUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportRateSetBL mLADiskImportRateSetBL = null;

    public LADiskImportRateSetUI()
    {
    	mLADiskImportRateSetBL = new LADiskImportRateSetBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportRateSetBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportRateSetBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportRateSetBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportRateSetBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportRateSetBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportRateSetBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportRateSetUI zLADiskImportRateSetUI = new LADiskImportRateSetUI();
    }
}
