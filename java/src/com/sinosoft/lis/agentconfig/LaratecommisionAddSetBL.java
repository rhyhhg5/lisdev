package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateCommisionAddSet;
import com.sinosoft.lis.vschema.LARateCommisionAddBSet;
import com.sinosoft.lis.schema.LARateCommisionAddSchema;
import com.sinosoft.lis.schema.LARateCommisionAddBSchema;
import com.sinosoft.lis.db.LARateCommisionAddDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LaratecommisionSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LaratecommisionAddSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionAddSet mLARateCommisionAddSet = new LARateCommisionAddSet();
  private Reflections ref = new Reflections();
  //private LARateCommisionAddBSet mLARateCommisionAddBSet = new LARateCommisionAddBSet();

  public LaratecommisionAddSetBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionAddSetBL LaratecommisionAddSetbl = new LaratecommisionAddSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LaratecommisionAddSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if (!check()) {
      return false;
    }

    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LaratecommisionAddSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LaratecommisionAddSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LaratecommisionAddSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLARateCommisionAddSet.set( (LARateCommisionAddSet) cInputData.get(1));
     this.mLARateCommisionAddSet.set( (LARateCommisionAddSet) cInputData.getObjectByObjectName("LARateCommisionAddSet",0));
      System.out.println("LARateCommisionAddSet get"+mLARateCommisionAddSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionAddSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 添加校验
   * /
   * @return boolean
   */
  private boolean check()
  {
      //删除不校验
      if( mOperate.equals("DELETE"))
      {
          return true;
      }
      else
      {
         for (int i = 1; i <= mLARateCommisionAddSet.size(); i++)
         {
             LARateCommisionAddSet tLARateCommisionAddSet = new LARateCommisionAddSet();
             LARateCommisionAddSchema tLARateCommisionAddSchema = new LARateCommisionAddSchema();
             tLARateCommisionAddSchema=mLARateCommisionAddSet.get(i);

             String tRiskCode=tLARateCommisionAddSchema.getRiskCode();
             int tIdx=tLARateCommisionAddSchema.getIdx();
             int tCurYear=tLARateCommisionAddSchema.getCurYear();
             int tF03=tLARateCommisionAddSchema.getF03();
             int tF04=tLARateCommisionAddSchema.getF04();
             String tManageCom=tLARateCommisionAddSchema.getManageCom();
             String tStartGiveDate=tLARateCommisionAddSchema.getStartGiveDate();
             String tEndGiveDate=tLARateCommisionAddSchema.getEndGiveDate();
             String tStartSignDate=tLARateCommisionAddSchema.getStartSignDate();
             String tEndSignDate=tLARateCommisionAddSchema.getEndSignDate();
             String tBranchType=tLARateCommisionAddSchema.getBranchType();
             String tBranchType2=tLARateCommisionAddSchema.getBranchType2();
             if("0".equals(tLARateCommisionAddSchema.getPayIntv()))
             {

                 LARateCommisionAddDB tLARateCommisionAddDB = new LARateCommisionAddDB();
                 tLARateCommisionAddDB.setRiskCode(tLARateCommisionAddSchema.getRiskCode());
                 tLARateCommisionAddDB.setPayIntv("0");
                 tLARateCommisionAddDB.setManageCom(tLARateCommisionAddSchema.getManageCom());
                 tLARateCommisionAddDB.setBranchType(tLARateCommisionAddSchema.getBranchType());
                 tLARateCommisionAddDB.setBranchType2(tLARateCommisionAddSchema.getBranchType2());
                 tLARateCommisionAddSet=tLARateCommisionAddDB.query();

                 if(tLARateCommisionAddSet!=null && tLARateCommisionAddSet.size()>=1)
                 {
                     for (int k = 1; k <= tLARateCommisionAddSet.size(); k++)
                     {
                         String sStartGiveDate=tLARateCommisionAddSet.get(k).getStartGiveDate();
                         String sEndGiveDate=tLARateCommisionAddSet.get(k).getEndGiveDate();
                         String sStartSignDate=tLARateCommisionAddSet.get(k).getStartSignDate();
                         String sEndSignDate=tLARateCommisionAddSet.get(k).getEndSignDate();
                         if(tStartGiveDate.compareTo(sStartGiveDate)>=0 && tStartGiveDate.compareTo(sEndGiveDate)<=0)
                         {
                             CError tError = new CError();
                             tError.moduleName = "LaratecommisionAddSetBL";
                             tError.functionName = "prepareOutputData";
                             tError.errorMessage = "险种"+tLARateCommisionAddSchema.getRiskCode()
                                                  +"的趸缴类型已经存在交单日期在"+tStartGiveDate+"的数据!";
                             this.mErrors.addOneError(tError);
                             return false;
                         }
                         if(tEndGiveDate.compareTo(sStartGiveDate)>=0 && tEndGiveDate.compareTo(sEndGiveDate)<=0)
                         {
                             CError tError = new CError();
                             tError.moduleName = "LaratecommisionAddSetBL";
                             tError.functionName = "prepareOutputData";
                             tError.errorMessage = "险种"+tLARateCommisionAddSchema.getRiskCode()
                                                  +"的趸缴类型已经存在交单日期在"+tEndGiveDate+"的数据!";
                             this.mErrors.addOneError(tError);
                             return false;
                         }
                         if(tStartSignDate.compareTo(sStartSignDate)>=0 && tStartSignDate.compareTo(sEndSignDate)<=0)
                         {
                             CError tError = new CError();
                             tError.moduleName = "LaratecommisionAddSetBL";
                             tError.functionName = "prepareOutputData";
                             tError.errorMessage = "险种"+tLARateCommisionAddSchema.getRiskCode()
                                                  +"的趸缴类型已经存在交单日期在"+tStartSignDate+"的数据!";
                             this.mErrors.addOneError(tError);
                             return false;
                         }
                         if(tEndSignDate.compareTo(sStartGiveDate)>=0 && tEndSignDate.compareTo(sEndGiveDate)<=0)
                         {
                             CError tError = new CError();
                             tError.moduleName = "LaratecommisionAddSetBL";
                             tError.functionName = "prepareOutputData";
                             tError.errorMessage = "险种"+tLARateCommisionAddSchema.getRiskCode()
                                                  +"的趸缴类型已经存在交单日期在"+tEndSignDate+"的数据!";
                             this.mErrors.addOneError(tError);
                             return false;
                         }

                     }
                 }
                 //校验录入是否有重复
                 for (int k = 1; k <= mLARateCommisionAddSet.size(); k++)
                 {
                     if("0".equals(mLARateCommisionAddSet.get(k).getPayIntv()) && k != i)
                     {
                        if(!mLARateCommisionAddSet.get(k).getRiskCode().equals(tRiskCode))
                        {
                            continue;
                        }
                        if(!mLARateCommisionAddSet.get(k).getManageCom().equals(tManageCom))
                        {
                            continue;
                        }
                        if(!mLARateCommisionAddSet.get(k).getBranchType().equals(tBranchType))
                        {
                            continue;
                        }
                        if(!mLARateCommisionAddSet.get(k).getBranchType2().equals(tBranchType2))
                        {
                            continue;
                        }
                        String sStartGiveDate=mLARateCommisionAddSet.get(k).getStartGiveDate();
                        String sEndGiveDate=mLARateCommisionAddSet.get(k).getEndGiveDate();
                        String sStartSignDate=mLARateCommisionAddSet.get(k).getStartSignDate();
                        String sEndSignDate=mLARateCommisionAddSet.get(k).getEndSignDate();
                        if(tStartGiveDate.compareTo(sStartGiveDate)>=0 && tStartGiveDate.compareTo(sEndGiveDate)<=0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "LaratecommisionAddSetBL";
                            tError.functionName = "prepareOutputData";
                            tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                                 +"的趸缴类型在交单日期在"+tStartGiveDate+"的数据 !";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        if(tEndGiveDate.compareTo(sStartGiveDate)>=0 && tEndGiveDate.compareTo(sEndGiveDate)<=0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "LaratecommisionAddSetBL";
                            tError.functionName = "prepareOutputData";
                            tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                                 +"的趸缴类型在交单日期在"+tEndGiveDate+"的数据 !";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        if(tStartSignDate.compareTo(sStartSignDate)>=0 && tStartSignDate.compareTo(sEndSignDate)<=0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "LaratecommisionAddSetBL";
                            tError.functionName = "prepareOutputData";
                            tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                                 +"的趸缴类型在交单日期在"+tStartSignDate+"的数据 !";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        if(tEndSignDate.compareTo(sStartGiveDate)>=0 && tEndSignDate.compareTo(sEndGiveDate)<=0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "LaratecommisionAddSetBL";
                            tError.functionName = "prepareOutputData";
                            tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                                 +"的趸缴类型在交单日期在"+tEndSignDate+"的数据 !";
                            this.mErrors.addOneError(tError);
                            return false;
                        }


                     }

                 }

             }
             else
             {//不是趸缴
                 String tSql="select 'N' from laratecommisionadd  where riskcode='"+tRiskCode+"' and Payintv<>'0' "
                            +"  and managecom='"+tManageCom+"' and curyear="+tCurYear+" and "+tF03+">=f03 and "+tF03+"<f04"
                            +" and  BranchType='"+tBranchType+"' and branchtype2='"+tBranchType2+"'  "
                            +" and  ('"+tStartGiveDate+"' between startgivedate and  endgivedate or '"+tEndGiveDate
                            +"'  between  startgivedate and  endgivedate or '"+tStartSignDate+"' between startsigndate and "
                            +" endsigndate  or '"+tEndSignDate+"' between startsigndate and endsigndate )";

                String tSql1="select 'N' from laratecommisionadd  where riskcode='"+tRiskCode+"' and Payintv<>'0' "
                           +"  and managecom='"+tManageCom+"' and curyear="+tCurYear+" and "+tF04+">=f03 and "+tF04+"<f04"
                           +" and  BranchType='"+tBranchType+"' and branchtype2='"+tBranchType2+"'  "
                           +" and  ('"+tStartGiveDate+"' between startgivedate and  endgivedate or '"+tEndGiveDate
                           +"'  between  startgivedate and  endgivedate or '"+tStartSignDate+"' between startsigndate and "
                           +" endsigndate  or '"+tEndSignDate+"' between startsigndate and endsigndate) ";

              if (mOperate.equals("UPDATE"))
              {
                  tSql+=" and   idx<>"+tIdx+" ";
                  tSql1+=" and   idx<>"+tIdx+" ";
              }

                ExeSQL tExeSql = new ExeSQL();
                String tFlag=tExeSql.getOneValue(tSql);
                String tFlag1=tExeSql.getOneValue(tSql1);
                if("N".equals(tFlag) || "N".equals(tFlag1) )
                {
                    System.out.println("11111111111111111111111111111111111111111111111");
                    CError tError = new CError();
                    tError.moduleName = "LaratecommisionAddSetBL";
                    tError.functionName = "prepareOutputData";
                    tError.errorMessage = "险种"+tLARateCommisionAddSchema.getRiskCode()
                                         +"的信息已存在!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
             }
             //校验录入是否有重复
             for (int k = 1; k <= mLARateCommisionAddSet.size(); k++)
             {
                 if(!"0".equals(mLARateCommisionAddSet.get(k).getPayIntv()) && k != i)
                 {
                     if(!mLARateCommisionAddSet.get(k).getRiskCode().equals(tRiskCode))
                     {
                         continue;
                     }
                     if(!mLARateCommisionAddSet.get(k).getManageCom().equals(tManageCom))
                     {
                         continue;
                     }
                     if(mLARateCommisionAddSet.get(k).getCurYear()!= tCurYear)
                     {
                         continue;
                     }
                     if(!mLARateCommisionAddSet.get(k).getBranchType().equals(tBranchType))
                     {
                         continue;
                     }
                     if(!mLARateCommisionAddSet.get(k).getBranchType2().equals(tBranchType2))
                     {
                         continue;
                     }
                    String sStartGiveDate=mLARateCommisionAddSet.get(k).getStartGiveDate();
                    String sEndGiveDate=mLARateCommisionAddSet.get(k).getEndGiveDate();
                    String sStartSignDate=mLARateCommisionAddSet.get(k).getStartSignDate();
                    String sEndSignDate=mLARateCommisionAddSet.get(k).getEndSignDate();
                    if(tStartGiveDate.compareTo(sStartGiveDate)>=0 && tStartGiveDate.compareTo(sEndGiveDate)<=0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "LaratecommisionAddSetBL";
                        tError.functionName = "prepareOutputData";
                        tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                             +"在交单日期在"+tStartGiveDate+"的数据 !";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if(tEndGiveDate.compareTo(sStartGiveDate)>=0 && tEndGiveDate.compareTo(sEndGiveDate)<=0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "LaratecommisionAddSetBL";
                        tError.functionName = "prepareOutputData";
                        tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                             +"在交单日期在"+tEndGiveDate+"的数据 !";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if(tStartSignDate.compareTo(sStartSignDate)>=0 && tStartSignDate.compareTo(sEndSignDate)<=0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "LaratecommisionAddSetBL";
                        tError.functionName = "prepareOutputData";
                        tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                             +"在交单日期在"+tStartSignDate+"的数据 !";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if(tEndSignDate.compareTo(sStartGiveDate)>=0 && tEndSignDate.compareTo(sEndGiveDate)<=0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "LaratecommisionAddSetBL";
                        tError.functionName = "prepareOutputData";
                        tError.errorMessage = "不能录入多条险种"+tLARateCommisionAddSchema.getRiskCode()
                                             +"在交单日期在"+tEndSignDate+"的数据 !";
                        this.mErrors.addOneError(tError);
                        return false;
                    }


                 }
            }

         }
      }
      return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LaratecommisionAddSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        System.out.println("Begin LaratecommisionAddSetBL.dealData.........1"+mLARateCommisionAddSet.size());
        LARateCommisionAddSet tLARateCommisionAddSet = new LARateCommisionAddSet();
        LARateCommisionAddBSet tLARateCommisionAddBSet = new LARateCommisionAddBSet();
        for (int i = 1; i <= mLARateCommisionAddSet.size(); i++) {
          System.out.println("Begin LaratecommisionAddSetBL.dealData.........2");
          LARateCommisionAddSchema tLARateCommisionAddSchemaold = new
              LARateCommisionAddSchema();
          LARateCommisionAddSchema tLARateCommisionAddSchemanew = new
              LARateCommisionAddSchema();
          LARateCommisionAddDB tLARateCommisionAddDB = new LARateCommisionAddDB();
           System.out.println("Begin LaratecommisionAddSetBL.dealData.........3");
            System.out.println("++++++++"+mLARateCommisionAddSet.get(i).getRiskCode());
          System.out.println("++++++++"+mLARateCommisionAddSet.get(i).getIdx());


          tLARateCommisionAddDB.setIdx(mLARateCommisionAddSet.get(i).getIdx());
          tLARateCommisionAddDB.setRiskCode(mLARateCommisionAddSet.get(i).getRiskCode());
          tLARateCommisionAddSchemaold = tLARateCommisionAddDB.query().get(1);
          tLARateCommisionAddSchemanew = mLARateCommisionAddSet.get(i);
          //tLARateCommisionAddSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionAddSchemanew.setOperator(mGlobalInput.Operator);
          tLARateCommisionAddSchemanew.setMakeDate(tLARateCommisionAddSchemaold.
                                                getMakeDate());
          tLARateCommisionAddSchemanew.setMakeTime(tLARateCommisionAddSchemaold.
                                                getMakeTime());
          tLARateCommisionAddSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionAddSchemanew.setModifyTime(CurrentTime);
          tLARateCommisionAddSchemanew.setVersionType("11");
          tLARateCommisionAddSet.add(tLARateCommisionAddSchemanew);
          LARateCommisionAddBSchema tLARateCommisionAddBSchema = new LARateCommisionAddBSchema();
          ref.transFields(tLARateCommisionAddBSchema, tLARateCommisionAddSchemaold);
          tLARateCommisionAddBSchema.setAddEdorNo(tEdorNo);
          if(mOperate.equals("UPDATE"))
          {
              tLARateCommisionAddBSchema.setAddEdorType("02");
          }
          else
          {
              tLARateCommisionAddBSchema.setAddEdorType("01");
          }
          tLARateCommisionAddBSet.add(tLARateCommisionAddBSchema);

        }
        map.put(tLARateCommisionAddSet, mOperate);
        map.put(tLARateCommisionAddBSet, "INSERT");
      }
      else {
        LARateCommisionAddSet tLARateCommisionAddSet = new
            LARateCommisionAddSet();
        for (int i = 1; i <= mLARateCommisionAddSet.size(); i++) {
          LARateCommisionAddSchema tLARateCommisionAddSchemanew = new
              LARateCommisionAddSchema();
          tLARateCommisionAddSchemanew = mLARateCommisionAddSet.get(i);
          tLARateCommisionAddSchemanew.setOperator(mGlobalInput.
                                                Operator);
          //tLARateCommisionAddSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionAddSchemanew.setMakeDate(CurrentDate);
          tLARateCommisionAddSchemanew.setMakeTime(CurrentTime);
          tLARateCommisionAddSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionAddSchemanew.setModifyTime(CurrentTime);
          tLARateCommisionAddSchemanew.setVersionType("11");
          tLARateCommisionAddSet.add(tLARateCommisionAddSchemanew);

        }
        map.put(tLARateCommisionAddSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionAddSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LaratecommisionAddSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LaratecommisionAddSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
