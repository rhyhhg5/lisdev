package com.sinosoft.lis.agentconfig;


import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LDCodeRelaSet;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LDCodeRelaDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LAAreaBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0
 */
public class LAAreaBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
 
  private Reflections ref = new Reflections();
  private LDCodeRelaSchema mLDCodeRelaSchema = new LDCodeRelaSchema();
  private LDCodeRelaSet mLDCodeRelaSet = new LDCodeRelaSet();
  public LAAreaBL() {

  }


  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAAreaBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAAreaBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAAreaBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
     
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
     this.mLDCodeRelaSchema.setSchema((LDCodeRelaSchema)cInputData.
    		 getObjectByObjectName("LDCodeRelaSchema", 0)); 

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAreaBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAAreaBL.dealData........."+mOperate);

      if (mOperate.equals("INSERT")) 
      {
    	  //INSERT包含新插入与修改两个操作，因此采用DELETE&Insert的方式
    	  LDCodeRelaDB tLDCodeRelaDB = new LDCodeRelaDB();
    	  LDCodeRelaSet tLDCodeRelaSet = new LDCodeRelaSet();
    	  String DSQL ="select * from LDCodeRela where relatype = '"+this.mLDCodeRelaSchema.getRelaType()+"'" +
    	  		" and code1 ='"+this.mLDCodeRelaSchema.getCode1()+"' " ;
    	  System.out.println(DSQL);
       try{
    	  tLDCodeRelaSet = tLDCodeRelaDB.executeQuery(DSQL);
    	  System.out.println("tLDCodeRelaSet.size()大小："+tLDCodeRelaSet.size());
    	 }
    	 catch(Exception ex)
    	 {
    		 CError tError = new CError();
    	      tError.moduleName = "LAAreaBL";
    	      tError.functionName = "dealData";
    	      tError.errorMessage = "查询时出错。";
    	      this.mErrors.addOneError(tError);
    	      return false;
    	 }   	 
    	 if(tLDCodeRelaSet.size()>0)
    	 {
    		 map.put(tLDCodeRelaSet, "DELETE"); 
    	 }   	  
    	  map.put(this.mLDCodeRelaSchema, "INSERT");
      }
      else if(mOperate.equals("DELETE"))
      {
    	  map.put(this.mLDCodeRelaSchema, "DELETE");
      }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAAreaBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAreaBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
