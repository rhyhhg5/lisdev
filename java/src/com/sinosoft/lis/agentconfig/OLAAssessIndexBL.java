/*
 * <p>ClassName: OLAAssessIndexBL </p>
 * <p>Description: LAAssessIndexBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessIndexSchema;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class OLAAssessIndexBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAssessIndexSchema mLAAssessIndexSchema = new LAAssessIndexSchema();
    private LAAssessIndexSet mLAAssessIndexSet = new LAAssessIndexSet();
    public OLAAssessIndexBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理

            CError tError = new CError();
            tError.moduleName = "LAAssessIndexBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAssessIndexBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.indexOf("QUERY") != -1)
        {
            String sqlStr = "";
            if (this.mOperate.equals("QUERY||INDEXSET"))
            {
                sqlStr = this.getSqlStr();
            }
            this.submitquery(sqlStr);
        }
        else
        {
            System.out.println("Start OLAAssessIndexBL Submit...");
            OLAAssessIndexBLS tOLAAssessIndexBLS = new OLAAssessIndexBLS();
            tOLAAssessIndexBLS.submitData(mInputData, cOperate);
            System.out.println("End OLAAssessIndexBL Submit...");
            //如果有需要处理的错误，则返回
            if (tOLAAssessIndexBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLAAssessIndexBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAssessIndexBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //   this.mLAAssessIndexSchema.setModifyDate(getDate());
        //  this.mLAAssessIndexSchema.setModifyTime();
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAssessIndexSchema.setSchema((LAAssessIndexSchema) cInputData.
                                            getObjectByObjectName(
                "LAAssessIndexSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery(String sqlStr)
    {
        System.out.println("---sqlStr---" + sqlStr);
        LAAssessIndexSchema tLAAssessIndexSchema = new LAAssessIndexSchema();
        LAAssessIndexSet tLAAssessIndexSet = new LAAssessIndexSet();

        LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
        if (this.mOperate.equals("QUERY||INDEXSET"))
        {
            tLAAssessIndexSet = tLAAssessIndexDB.executeQuery(sqlStr);
        }
        else if (this.mOperate.equals("QUERY||MAIN"))
        {
            tLAAssessIndexDB.setSchema(this.mLAAssessIndexSchema);
            tLAAssessIndexSet = tLAAssessIndexDB.query();
        }
        if (tLAAssessIndexDB.mErrors.needDealError() == true)
        {
            this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLAAssessIndexBL";
            tError.functionName = "SubmitQurey";
            tError.errorMessage = "相关指标信息查询失败!";
            this.mErrors.addOneError(tError);
            tLAAssessIndexSet.clear();
            return false;
        }

        if (tLAAssessIndexSet.size() == 0)
        {
            tLAAssessIndexSet.clear();
        }
        this.mResult.add(tLAAssessIndexSet);
        System.out.println("End LAAssessIndexBLQuery Submit...");

        mInputData = null;
        return true;
    }

    private String getSqlStr()
    {
        String sqlStr = "select * from LAAssessIndex where 1=1";
        if (this.mOperate.equals("QUERY||INDEXSET"))
        {
            String tIndexSet = this.mLAAssessIndexSchema.getIndexSet();
            String tBranchType = this.mLAAssessIndexSchema.getBranchType();
            String tIndexType = this.mLAAssessIndexSchema.getIndexType();

            sqlStr += " and IndexType = '" + tIndexType + "'";
            sqlStr += " and BranchType = '" + tBranchType + "'";
            if (tIndexSet != null && !tIndexSet.equals(""))
            {
                String[] indexArr = PubFun.split(tIndexSet, ",");
                tIndexSet = "'";
                for (int i = 0; i < indexArr.length - 1; i++)
                {
                    tIndexSet += indexArr[i] + "','";
                }
                tIndexSet += indexArr[indexArr.length - 1];
                tIndexSet += "'";
                sqlStr += " and IndexCode in (" + tIndexSet + ")";
            }
        }
        else
        {
            return null;
        }

        return sqlStr;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAssessIndexSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessIndexBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
