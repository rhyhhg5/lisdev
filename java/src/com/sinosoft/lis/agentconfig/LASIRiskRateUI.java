package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.lis.agentquery.LAXBTQueryBL;
import com.sinosoft.lis.agentquery.LAXBTQueryUI;
import com.sinosoft.lis.vschema.LARateCommisionSet;
/**
 * <p>Title: LAAChargeWrapRateUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LASIRiskRateUI
{
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();
    
    private VData mResult = new VData();

    public LASIRiskRateUI()
    {
        System.out.println("LASIRiskRateUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LASIRiskRateBL bl = new LASIRiskRateBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }else{
        	 this.mResult = bl.getResult();
        }
        return true;
    }

    public static void main(String[] args)
    {
    	LASIRiskRateUI tLASIRiskRateUI = new   LASIRiskRateUI();
         System.out.println("LAXBTQueryUI"); 
    }
}
