package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LAWageRadixAllSet;
import com.sinosoft.lis.schema.LAWageRadixAllSchema;
import com.sinosoft.lis.db.LAWageRadixAllDB;
import com.sinosoft.lis.schema.LAWageRadixAllTraceSchema;
import com.sinosoft.lis.vschema.LAWageRadixAllTraceSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author wangrx 2015-9-23
 * @version 1.0
 */
public class LAHealthAgentStaffBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String tFlag = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
//  private MMap newMap = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAWageRadixAllSet mLAWageRadixAllSet = new LAWageRadixAllSet();
  private Reflections ref = new Reflections();
  public LAHealthAgentStaffBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAHealthAgentStaffBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAHealthAgentStaffBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LAHealthAgentStaffBL.check.........1"+mLAWageRadixAllSet.size());	 
	  	LAWageRadixAllSchema tempLAWageRadixAllSchema1; 
	  	LAWageRadixAllSchema tempLAWageRadixAllSchema2;
	  	LAWageRadixAllSchema tLAWageRadixAllSchemaNew = new LAWageRadixAllSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  LAWageRadixAllDB tLAWageRadixAllDB = new LAWageRadixAllDB();
		  for(int i=1;i<=this.mLAWageRadixAllSet.size();i++){
			  tempLAWageRadixAllSchema1=this.mLAWageRadixAllSet.get(i);
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLAWageRadixAllSet.size();j++){
					  tempLAWageRadixAllSchema2 = this.mLAWageRadixAllSet.get(j);
					  if(tempLAWageRadixAllSchema1.getManageCom().equals(tempLAWageRadixAllSchema2.getManageCom())
						&&tempLAWageRadixAllSchema1.getAgentGrade().equals(tempLAWageRadixAllSchema2.getAgentGrade())
					  ){
							 BuildError("dealData","第"+i+","+j+"行管理机构,职级代码相同请检查！");
		                     return false; 
					  }
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LAWageRadixAll where  managecom='")
				  .append(tempLAWageRadixAllSchema1.getManageCom())
				  .append("' and agentgrade = '")
				  .append(tempLAWageRadixAllSchema1.getAgentGrade())
				  .append("' and wagecode = '")
				  .append(tempLAWageRadixAllSchema1.getWageCode())
				  .append("' and branchtype='")
				  .append(tempLAWageRadixAllSchema1.getBranchType())
				  .append("' and branchtype2='")
				  .append(tempLAWageRadixAllSchema1.getBranchType2())
				  .append("' and agenttype = '")
				  .append(tempLAWageRadixAllSchema1.getAgentType())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LAHealthAgentStaffBL";
				      tError.functionName = "check()";
				      if(tempLAWageRadixAllSchema1.getAgentType().equals("1")){
				    	  tError.errorMessage = "第"+i+"行该机构下该职级基本收入已录入";
				      }else {
				    	  tError.errorMessage = "第"+i+"行该机构下该职级固定工资已录入";
				      }
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
			 if(mOperate.equals("UPDATE")){
				 tLAWageRadixAllDB.setIdx(tempLAWageRadixAllSchema1.getIdx());
				 tLAWageRadixAllSchemaNew = tLAWageRadixAllDB.query().get(1);
				 if(!tempLAWageRadixAllSchema1.getManageCom().equals(tLAWageRadixAllSchemaNew.getManageCom())||
					!tempLAWageRadixAllSchema1.getAgentGrade().equals(tLAWageRadixAllSchemaNew.getAgentGrade())
				 ){
					 BuildError("dealData","第"+i+"行管理机构,职级代码,不能修改！");
                     return false;
				 }
			 }
		  
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAHealthAgentStaffBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLAWageRadixAllSet.set( (LAWageRadixAllSet) cInputData.getObjectByObjectName("LAWageRadixAllSet",0));
      System.out.println("LAWageRadixAllSet get"+mLAWageRadixAllSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAHealthAgentStaffBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAHealthAgentStaffBL.dealData........."+mOperate);
    try {
    	LAWageRadixAllSchema  tLAWageRadixAllSchemaNew = new  LAWageRadixAllSchema();
    	LAWageRadixAllSchema  tLAWageRadixAllSchemaOld = new  LAWageRadixAllSchema();
    	LAWageRadixAllDB tLAWageRadixAllDB =new  LAWageRadixAllDB();
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LAHealthAgentStaffBL.dealData.........INSERT"+mLAWageRadixAllSet.size());
	        LAWageRadixAllSet tLAWageRadixAllSet = new LAWageRadixAllSet();
	        LAWageRadixAllTraceSet tLAWageRadixAllTraceSet = new LAWageRadixAllTraceSet();
	        for (int i = 1; i <= mLAWageRadixAllSet.size(); i++) {
	        	tLAWageRadixAllSchemaNew = mLAWageRadixAllSet.get(i);	
		          ExeSQL tExe = new ExeSQL();
		          String tSql =
		              "select max(int(idx)) from LAWageRadixAll order by 1 desc";
		          String strIdx = "";
		          int tMaxIdx = 0;
		          strIdx = tExe.getOneValue(tSql);
		          if (strIdx == null || strIdx.trim().equals("")) {
		            tMaxIdx = 0;
		          }
		          else {
		            tMaxIdx = Integer.parseInt(strIdx);
		          }
		      tMaxIdx += i;
		      String sMaxIdx =tMaxIdx+"";
		      tLAWageRadixAllSchemaNew.setIdx(sMaxIdx);
		      tLAWageRadixAllSchemaNew.setMakeDate(CurrentDate);
		      tLAWageRadixAllSchemaNew.setMakeTime(CurrentTime);
		      tLAWageRadixAllSchemaNew.setModifyDate(CurrentDate);
		      tLAWageRadixAllSchemaNew.setModifyTime(CurrentTime);
		      tLAWageRadixAllSchemaNew.setOperator(mGlobalInput.Operator); 
		      tLAWageRadixAllSet.add(tLAWageRadixAllSchemaNew);
      }
        	map.put(tLAWageRadixAllSet, mOperate);
        	map.put(tLAWageRadixAllTraceSet, "INSERT");
      }
      if(mOperate.equals("UPDATE")) {
    	  LAWageRadixAllSet tLAWageRadixAllSet = new LAWageRadixAllSet();
    	  LAWageRadixAllTraceSet tLAWageRadixAllTraceSet = new LAWageRadixAllTraceSet();
    	 for(int i = 1;i<=mLAWageRadixAllSet.size();i++){
    		 tLAWageRadixAllDB.setIdx(mLAWageRadixAllSet.get(i).getIdx());
    		 tLAWageRadixAllSchemaOld = tLAWageRadixAllDB.query().get(1); 
    		 tLAWageRadixAllSchemaNew = mLAWageRadixAllSet.get(i);
    		 tLAWageRadixAllSchemaNew.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
    		 tLAWageRadixAllSchemaNew.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
    		 tLAWageRadixAllSchemaNew.setModifyDate(CurrentDate);
    		 tLAWageRadixAllSchemaNew.setModifyTime(CurrentTime);
    		 tLAWageRadixAllSchemaNew.setOperator(mGlobalInput.Operator); 
    		 tLAWageRadixAllSet.add(tLAWageRadixAllSchemaNew);
		      //插入备份轨迹表
    		  LAWageRadixAllTraceSchema  tLAWageRadixAllTraceSchema = new  LAWageRadixAllTraceSchema();
		      String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		      tLAWageRadixAllTraceSchema.setEdorNo(tEdorNo);
		      tLAWageRadixAllTraceSchema.setEdorType("01");//”修改“标识
		      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
		      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
		      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
		      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
		      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
		      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
		      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
		      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
		      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
		      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
		      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
		      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
		      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
		      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
		      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
		      tLAWageRadixAllTraceSet.add(tLAWageRadixAllTraceSchema);
    	 }
    	 map.put(tLAWageRadixAllSet, mOperate);
    	 map.put(tLAWageRadixAllTraceSet, "INSERT");
      }
      if(mOperate.equals("DELETE")){
    	  LAWageRadixAllSet tLAWageRadixAllSet = new LAWageRadixAllSet();
    	  LAWageRadixAllTraceSet tLAWageRadixAllTraceSet = new LAWageRadixAllTraceSet();
    	 for(int i = 1;i<=mLAWageRadixAllSet.size();i++){	
    		 tLAWageRadixAllDB.setIdx(mLAWageRadixAllSet.get(i).getIdx());
    		 tLAWageRadixAllSchemaOld = tLAWageRadixAllDB.query().get(1); 
    		 tLAWageRadixAllSchemaNew = mLAWageRadixAllSet.get(i);
    		 tLAWageRadixAllSchemaNew.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
    		 tLAWageRadixAllSchemaNew.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
    		 tLAWageRadixAllSchemaNew.setModifyDate(CurrentDate);
    		 tLAWageRadixAllSchemaNew.setModifyTime(CurrentTime);
    		 tLAWageRadixAllSchemaNew.setOperator(mGlobalInput.Operator);
    		 tLAWageRadixAllSet.add(tLAWageRadixAllSchemaNew);
		      //插入备份轨迹表
    		  LAWageRadixAllTraceSchema  tLAWageRadixAllTraceSchema = new  LAWageRadixAllTraceSchema();
		      String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		      tLAWageRadixAllTraceSchema.setEdorNo(tEdorNo);
		      tLAWageRadixAllTraceSchema.setEdorType("02");//“删除“标识
		      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
		      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
		      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
		      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
		      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
		      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
		      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
		      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
		      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
		      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
		      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
		      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
		      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
		      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
		      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
		      tLAWageRadixAllTraceSet.add(tLAWageRadixAllTraceSchema);
    	 }
    	 map.put(tLAWageRadixAllSet, mOperate);
    	 map.put(tLAWageRadixAllTraceSet, "INSERT");
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAHealthAgentStaffBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAHealthAgentStaffBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LAHealthAgentStaffBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
