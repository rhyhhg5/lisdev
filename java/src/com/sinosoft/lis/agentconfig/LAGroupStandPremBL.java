package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateStandPremSet;
import com.sinosoft.lis.vschema.LARateStandPremBSet;
import com.sinosoft.lis.schema.LARateStandPremSchema;
import com.sinosoft.lis.schema.LARateStandPremBSchema;

import com.sinosoft.lis.db.LARateStandPremDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title: LAGroupStandPremBL
 * </p>
 * 
 * <p>
 * Description: LIS - 销售管理
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * 
 * <p>
 * Company: SinoSoft
 * </p>
 * 
 * @author luomin
 * @version 1.0
 */
public class LAGroupStandPremBL {
	// 错误处理类
	public  CErrors mErrors = new CErrors();
	// 业务处理相关变量
	/** 全局数据 */
	private VData mInputData = new VData();
	private String mOperate = "";
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private MMap map = new MMap();
	public GlobalInput mGlobalInput = new GlobalInput();
	private LARateStandPremSet mLARateStandPremSet = new LARateStandPremSet();
	private Reflections ref = new Reflections();

	// private LARateStandPremBSet mLARateStandPremBSet = new
	// LARateStandPremBSet();

	public LAGroupStandPremBL() {

	}

	// public static void main(String[] args)
	// {
	// LAGroupStandPremBL LAGroupStandPremBL = new LAGroupStandPremBL();
	// }
	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("Begin LAGroupStandPremBL.submitData.........");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 检查数据
		if (!check()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start LAGroupStandPremBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAGroupStandPremBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean check() {
		// 进行重复数据校验
		System.out.println("Begin LAGroupStandPremBL.check.........1"
				+ mLARateStandPremSet.size());
		LARateStandPremSchema tLARateStandPremSchema1 = new LARateStandPremSchema();
		LARateStandPremSchema tLARateStandPremSchema2 = new LARateStandPremSchema();
		if (this.mOperate.equals("INSERT")) {
			for (int i = 1; i <= this.mLARateStandPremSet.size(); i++) {
				tLARateStandPremSchema1 = mLARateStandPremSet.get(i);
				for (int j = i + 1; j <= this.mLARateStandPremSet.size(); j++) {
					tLARateStandPremSchema2 = mLARateStandPremSet.get(j);
					if (tLARateStandPremSchema1.getRiskCode().equals(
							tLARateStandPremSchema2.getRiskCode())
							&& tLARateStandPremSchema1.getManageCom().equals(
									tLARateStandPremSchema2.getManageCom())
							&& tLARateStandPremSchema1.getBranchType().equals(
									tLARateStandPremSchema2.getBranchType())
							&& tLARateStandPremSchema1.getBranchType2().equals(
									tLARateStandPremSchema2.getBranchType2())) {
						if(tLARateStandPremSchema1.getF01().equals(tLARateStandPremSchema2.getF01())
								&& tLARateStandPremSchema1.getF02().equals(tLARateStandPremSchema2.getF02())){
						
							CError tError = new CError();
							tError.moduleName = "LAZTGroupStandPremBL";
							tError.functionName = "check()";
							tError.errorMessage = "在验证操作数据时出错。在选中的数据中第" + i + "行与第"
									+ j + "行数据重复。";
							this.mErrors.clearErrors();
							this.mErrors.addOneError(tError);
						}	
						if ((Integer.parseInt(tLARateStandPremSchema2.getF01()) < Integer
								.parseInt(tLARateStandPremSchema1.getF02()))
								&& (Integer.parseInt(tLARateStandPremSchema1
										.getF01()) < Integer
										.parseInt(tLARateStandPremSchema1.getF02()))) {
							CError tError = new CError();
							tError.moduleName = "LAZTGroupStandPremBL";
							tError.functionName = "check()";
							tError.errorMessage = "在验证操作数据时出错。在选中的数据中第" + i + "行与第"
									+ j + "行保险期区间重复。";
							this.mErrors.clearErrors();
							this.mErrors.addOneError(tError);
							return false;
						}
					}

				}
				
//				LARateStandPremSchema tLARateStandPremSchema = new LARateStandPremSchema();
//				tLARateStandPremSchema = mLARateStandPremSet.get(i);
				String sql = "select count(1) from laratestandprem where "
						+ "riskcode='" + tLARateStandPremSchema1.getRiskCode()
						+ "'" + " and f01='" + tLARateStandPremSchema1.getF01()
						+ "'" + " and f02='" + tLARateStandPremSchema1.getF02()
						+ "'" + " and managecom='"
						+ tLARateStandPremSchema1.getManageCom() + "'"
						+ " and branchtype='"
						+ tLARateStandPremSchema1.getBranchType() + "'"
						+ "and branchtype2= '"
						+ tLARateStandPremSchema1.getBranchType2() + "'";
				ExeSQL tExeSQL = new ExeSQL();
				String count = tExeSQL.getOneValue(sql);
				if (count.compareTo("1") >= 0) {
					CError tError = new CError();
					tError.moduleName = "LAGroupRateCommSetBL";
					tError.functionName = "check";
					tError.errorMessage = "数据库已经存在同样情况的折标系数设置,请执行修改或者删除!";
					this.mErrors.addOneError(tError);
					return false;
				}
				
		          String agentSQL1 =
		              "select f01,f02 from laratestandprem "
		              +" where riskcode='"+tLARateStandPremSchema1.getRiskCode()+"'"
		              +" and managecom='"+tLARateStandPremSchema1.getManageCom()+"'"
		              +" and branchtype='"+tLARateStandPremSchema1.getBranchType()+"'"
		              +" and branchtype2='"+tLARateStandPremSchema1.getBranchType2()+"'";

		              SSRS tSSRS1 = new SSRS();
		              ExeSQL tExeSQL1 = new ExeSQL();
		              tSSRS1 = tExeSQL1.execSQL(agentSQL1);
		            
		              if(tSSRS1.getMaxRow()>0)
		              {
		            	  for(int k=1;k<=tSSRS1.getMaxRow();k++){
		            		  if((Integer.parseInt(tSSRS1.GetText(k,1))<Integer.parseInt(tLARateStandPremSchema1.getF02()))
		            		    &&(Integer.parseInt(tLARateStandPremSchema1.getF01())<Integer.parseInt(tSSRS1.GetText(k,2)))		  
		            		    ){
		    					  CError tError = new CError();
		    				      tError.moduleName = "LASocialRateSetBL";
		    				      tError.functionName = "check()";
		    				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据与数据库中已有数据保险区间重复。";
		    				      this.mErrors.clearErrors();
		    				      this.mErrors.addOneError(tError);
		    				      return false;  
		            		  }
		            	  }

		               }
				
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		try {
			System.out
					.println("Begin LAGroupStandPremBL.getInputData.........");
			this.mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			System.out.println("GlobalInput get");
			// this.mLARateStandPremSet.set( (LARateStandPremSet)
			// cInputData.get(1));
			this.mLARateStandPremSet.set((LARateStandPremSet) cInputData
					.getObjectByObjectName("LARateStandPremSet", 0));
			System.out.println("LARateStandPremSet get"
					+ mLARateStandPremSet.size());

		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGroupStandPremBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在读取处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("getInputData end ");
		return true;
	}

	/**
	 * 业务处理主函数
	 */
	private boolean dealData() {
		System.out.println("Begin LAGroupStandPremBL.dealData........."
				+ mOperate);
		try {
			if (mOperate.equals("UPDATE")) {
				System.out
						.println("Begin LAGroupStandPremBL.dealData.........1"
								+ mLARateStandPremSet.size());

				LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
//				LARateStandPremBSet tLARateStandPremBSet = new LARateStandPremBSet();
				for (int i = 1; i <= mLARateStandPremSet.size(); i++) {
					System.out
							.println("Begin LAGroupStandPremBL.dealData.........2");
					LARateStandPremSchema tLARateStandPremSchemaold = new LARateStandPremSchema();
					LARateStandPremSchema tLARateStandPremSchemanew = new LARateStandPremSchema();
					LARateStandPremDB tLARateStandPremDB = new LARateStandPremDB();
					System.out
							.println("Begin LAGroupStandPremBL.dealData.........3");
					System.out.println("++++++++"
							+ mLARateStandPremSet.get(i).getRiskCode());

					tLARateStandPremDB.setRiskCode(mLARateStandPremSet.get(i)
							.getRiskCode());
					tLARateStandPremSchemaold = tLARateStandPremDB.query().get(
							1);
					// map.put(tLARateStandPremSchemaold,"DELETE");
					tLARateStandPremSchemanew = mLARateStandPremSet.get(i);
					// tLARateStandPremSchemanew.setManageCom(mGlobalInput.ManageCom);
					tLARateStandPremSchemanew
							.setOperator(mGlobalInput.Operator);
					tLARateStandPremSchemanew
							.setMakeDate(tLARateStandPremSchemaold
									.getMakeDate());
					tLARateStandPremSchemanew
							.setMakeTime(tLARateStandPremSchemaold
									.getMakeTime());
					tLARateStandPremSchemanew.setModifyDate(CurrentDate);
					tLARateStandPremSchemanew.setModifyTime(CurrentTime);
					// tLARateStandPremSchemanew.setVersionType("11");
					tLARateStandPremSet.add(tLARateStandPremSchemanew);
					map.put(tLARateStandPremSet, "UPDATE");

//					LARateStandPremBSchema tLARateStandPremBSchema = new LARateStandPremBSchema();
//					ref.transFields(tLARateStandPremBSchema,
//							tLARateStandPremSchemaold);
//					// 获取最大的ID号
//					ExeSQL tExe = new ExeSQL();
//					String tSql = "select int(max(edorno)) from laratestandpremb order by 1 desc ";
//					String strIdx = "";
//					int tMaxIdx = 0;
//
//					strIdx = tExe.getOneValue(tSql);
//					if (strIdx == null || strIdx.trim().equals("")) {
//						tMaxIdx = 0;
//					} else {
//						tMaxIdx = Integer.parseInt(strIdx);
//						// System.out.println(tMaxIdx);
//					}
//					tMaxIdx++;
//					tLARateStandPremBSchema.setEdorNo(String.valueOf(tMaxIdx));
//					if (i > 1) {
//						tLARateStandPremBSchema.setEdorNo(tLARateStandPremBSet
//								.get(i - 1).getEdorNo() + 1);
//					}
//
//					if (mOperate.equals("UPDATE"))
//						tLARateStandPremBSchema.setEdorType("02");
//					else
//						tLARateStandPremBSchema.setEdorType("01");
//					tLARateStandPremBSchema.setStartDate(CurrentDate);
//					tLARateStandPremBSchema.setEndDate(CurrentDate);
//
//					tLARateStandPremBSet.add(tLARateStandPremBSchema);

				}

//				map.put(tLARateStandPremBSet, "INSERT");
			} else if (mOperate.equals("DELETE")) {

				System.out
						.println("Begin LAGroupStandPremBL.dealData.........1"
								+ mLARateStandPremSet.size());

				LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
//				LARateStandPremBSet tLARateStandPremBSet = new LARateStandPremBSet();
				for (int i = 1; i <= mLARateStandPremSet.size(); i++) {
					System.out
							.println("Begin LAGroupStandPremBL.dealData.........2");
					LARateStandPremSchema tLARateStandPremSchemaold = new LARateStandPremSchema();
					LARateStandPremSchema tLARateStandPremSchemanew = new LARateStandPremSchema();
					LARateStandPremDB tLARateStandPremDB = new LARateStandPremDB();
					System.out
							.println("Begin LAGroupStandPremBL.dealData.........3");
					System.out.println("++++++++"
							+ mLARateStandPremSet.get(i).getRiskCode());

					tLARateStandPremDB.setRiskCode(mLARateStandPremSet.get(i)
							.getRiskCode());
					tLARateStandPremSchemaold = tLARateStandPremDB.query().get(
							1);
					tLARateStandPremSchemanew = mLARateStandPremSet.get(i);
					// tLARateStandPremSchemanew.setManageCom(mGlobalInput.ManageCom);
					tLARateStandPremSchemanew
							.setOperator(mGlobalInput.Operator);
					tLARateStandPremSchemanew
							.setMakeDate(tLARateStandPremSchemaold
									.getMakeDate());
					tLARateStandPremSchemanew
							.setMakeTime(tLARateStandPremSchemaold
									.getMakeTime());
					tLARateStandPremSchemanew.setModifyDate(CurrentDate);
					tLARateStandPremSchemanew.setModifyTime(CurrentTime);
					// tLARateStandPremSchemanew.setVersionType("11");
					tLARateStandPremSet.add(tLARateStandPremSchemanew);

//					LARateStandPremBSchema tLARateStandPremBSchema = new LARateStandPremBSchema();
//					ref.transFields(tLARateStandPremBSchema,
//							tLARateStandPremSchemaold);
//					// 获取最大的ID号
//					ExeSQL tExe = new ExeSQL();
//					String tSql = "select int(max(edorno)) from laratestandpremb order by 1 desc ";
//					String strIdx = "";
//					int tMaxIdx = 0;
//
//					strIdx = tExe.getOneValue(tSql);
//					if (strIdx == null || strIdx.trim().equals("")) {
//						tMaxIdx = 0;
//					} else {
//						tMaxIdx = Integer.parseInt(strIdx);
//						// System.out.println(tMaxIdx);
//					}
//					tMaxIdx++;
//					tLARateStandPremBSchema.setEdorNo(String.valueOf(tMaxIdx));
//					if (i > 1) {
//						tLARateStandPremBSchema.setEdorNo(tLARateStandPremBSet
//								.get(i - 1).getEdorNo() + 1);
//					}
//
//					if (mOperate.equals("UPDATE"))
//						tLARateStandPremBSchema.setEdorType("02");
//					else
//						tLARateStandPremBSchema.setEdorType("01");
//					tLARateStandPremBSchema.setStartDate(CurrentDate);
//					tLARateStandPremBSchema.setEndDate(CurrentDate);
//
//					tLARateStandPremBSet.add(tLARateStandPremBSchema);

				}

				map.put(tLARateStandPremSet, "DELETE");
//				map.put(tLARateStandPremBSet, "INSERT");

			} else {
				LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
				for (int i = 1; i <= mLARateStandPremSet.size(); i++) {
					LARateStandPremSchema tLARateStandPremSchemanew = new LARateStandPremSchema();
					tLARateStandPremSchemanew = mLARateStandPremSet.get(i);
					tLARateStandPremSchemanew
							.setOperator(mGlobalInput.Operator);
					// tLARateStandPremSchemanew.setManageCom(mGlobalInput.ManageCom);
					tLARateStandPremSchemanew.setMakeDate(CurrentDate);
					tLARateStandPremSchemanew.setMakeTime(CurrentTime);
					tLARateStandPremSchemanew.setModifyDate(CurrentDate);
					tLARateStandPremSchemanew.setModifyTime(CurrentTime);
					// tLARateStandPremSchemanew.setVersionType("11");
					tLARateStandPremSet.add(tLARateStandPremSchemanew);

				}
				map.put(tLARateStandPremSet, mOperate);
			}
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGroupStandPremBL";
			tError.functionName = "dealData";
			tError.errorMessage = "在处理所数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			System.out
					.println("Begin LAGroupStandPremBL.prepareOutputData.........");
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGroupStandPremBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
