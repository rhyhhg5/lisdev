/*
 * <p>ClassName: OLAIndexVsCommBL </p>
 * <p>Description: LAIndexVsCommBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LAIndexVsCommDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAIndexVsCommSchema;
import com.sinosoft.lis.vschema.LAIndexVsCommSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class OLAIndexVsCommBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAIndexVsCommSchema mLAIndexVsCommSchema = new LAIndexVsCommSchema();
    private LAIndexVsCommSet mLAIndexVsCommSet = new LAIndexVsCommSet();
    public OLAIndexVsCommBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsCommBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAIndexVsCommBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start OLAIndexVsCommBL Submit...");
            OLAIndexVsCommBLS tOLAIndexVsCommBLS = new OLAIndexVsCommBLS();
            tOLAIndexVsCommBLS.submitData(mInputData, cOperate);
            System.out.println("End OLAIndexVsCommBL Submit...");
            //如果有需要处理的错误，则返回
            if (tOLAIndexVsCommBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLAIndexVsCommBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAIndexVsCommBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //查询录入主键是否重复
        LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
        tLAIndexVsCommDB.setAgentGrade(this.mLAIndexVsCommSchema.getAgentGrade());
        tLAIndexVsCommDB.setWageCode(this.mLAIndexVsCommSchema.getWageCode());
        int iCount = tLAIndexVsCommDB.getCount();
        if (tLAIndexVsCommDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLAIndexVsCommBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询所录佣金代码是否存在出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("count:" + iCount);
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (iCount > 0)
            {
                CError tError = new CError();
                tError.moduleName = "OLAIndexVsCommBL";
                tError.functionName = "dealData";
                tError.errorMessage = "佣金编码" + mLAIndexVsCommSchema.getWageCode() +
                                      "已存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else if (this.mOperate.equals("UPDATE||MAIN") ||
                 this.mOperate.equals("DELETE||MAIN"))
        {
            if (iCount != 1)
            {
                CError tError = new CError();
                tError.moduleName = "OLAIndexVsCommBL";
                tError.functionName = "dealData";
                tError.errorMessage = "佣金编码" + mLAIndexVsCommSchema.getWageCode() +
                                      "不存在，无法作修改或删除操作！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAIndexVsCommSchema.setSchema((LAIndexVsCommSchema) cInputData.
                                            getObjectByObjectName(
                "LAIndexVsCommSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAIndexVsCommBLQuery Submit...");
        LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
        tLAIndexVsCommDB.setSchema(this.mLAIndexVsCommSchema);
        this.mLAIndexVsCommSet = tLAIndexVsCommDB.query();
        this.mResult.add(this.mLAIndexVsCommSet);
        System.out.println("End LAIndexVsCommBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAIndexVsCommDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexVsCommBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAIndexVsCommSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsCommBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
