package com.sinosoft.lis.agentconfig;


import java.io.File;

import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionCompareSchema;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.vschema.LARateCommisionCompareSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author lihai
 * @version 1.1
 */
public class OnmiDiskImportRateCommisionBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    private MMap mmapCompare = new MMap();

    private int importPersons = 0; //记录导入成功的记录数
    private StringBuffer unImportRecords = new StringBuffer(); //记录重复的记录数

    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "OnmiRateCommisionDiskImport.xml";
    
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();

    private LARateCommisionCompareSet mLARateCommisionCompareSetB = new LARateCommisionCompareSet();
    private LARateCommisionSet mLARateCommisionSetFinal=new LARateCommisionSet();

    private LARateCommisionSet mLARateCommisionSet = null;


    public OnmiDiskImportRateCommisionBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        
        //从磁盘导入数据
        LARateCommisionDiskImporter importer = new LARateCommisionDiskImporter(fileName,
                configFileName,
                diskimporttype);
       
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LARateCommision")) {
            mLARateCommisionSet = (LARateCommisionSet) importer
                                  .getSchemaSet();

        }


        //若被保人在保单中没有记录，则剔出
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("OnmiDiskImportRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);

        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }

        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");

        return true;
    }

    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LARateCommision")) {
            if (mLARateCommisionSet == null) {
                mErrors.addOneError("导入万能险佣金率信息失败！没有得到要导入的数据。");
                return false;
            } else {

            	
            	String sql="select * from ldcom where char(length(trim(comcode)))in ('4','2')";
            	mLDComSet=mLDComDB.executeQuery(sql);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LARateCommisionDB mTempLARateCommisionDB=new LARateCommisionDB();
            	
            	LARateCommisionSchema mTempLARateCommisionSchema=new LARateCommisionSchema();
                for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
                    if (mLARateCommisionSet.get(i).getManageCom() == null ||
                        mLARateCommisionSet.get(i).getManageCom().equals("")) {
                        mErrors.addOneError("第"+i+"行导入管理机构不能为空！请改正后重新导入");
                        return false;
                    } else if (mLARateCommisionSet.get(i).getRiskCode() == null ||
                               mLARateCommisionSet.get(i).getRiskCode().equals(
                                       "")) {
                        mErrors.addOneError("第"+i+"行导入险种编码不能为空！请改正后重新导入");
                        return false;
                    } else if (mLARateCommisionSet.get(i).getCurYear() <= 0 ) {
                        mErrors.addOneError("第"+i+"行导入保单年度不能为空！请改正后重新导入");
                        return false;
                    } else if (mLARateCommisionSet.get(i).getPayIntv() == null ||
                               mLARateCommisionSet.get(i).getPayIntv().equals("")) {
                        mErrors.addOneError("第"+i+"行导入缴费方式不能为空！请改正后重新导入");
                        return false;
                    } else if(!mLARateCommisionSet.get(i).getPayIntv().equals("0")&&!mLARateCommisionSet.get(i).getPayIntv().equals("1")
                    	&&!mLARateCommisionSet.get(i).getPayIntv().equals("3")&&!mLARateCommisionSet.get(i).getPayIntv().equals("6")
                    	&&!mLARateCommisionSet.get(i).getPayIntv().equals("12")){
                    	mErrors.addOneError("第"+i+"行导入缴费方式错误！缴费方式必须为0（趸交）、1（月交）、3（季交）、6（半年交）、12（年交）中的一个，请改正后重新导入");
                    	return false;
                    } else if (mLARateCommisionSet.get(i).getF01() == null ||
                            mLARateCommisionSet.get(i).getF01().equals("")) {
                     mErrors.addOneError("第"+i+"行导入保费类型不能为空！请改正后重新导入");
                     return false;
                    } else if(!mLARateCommisionSet.get(i).getF01().equals("01")&&!mLARateCommisionSet.get(i).getF01().equals("02")
                        	&&!mLARateCommisionSet.get(i).getF01().equals("03")){
                        	mErrors.addOneError("第"+i+"行导入保费类型错误！保费类型必须为01（基本保费）、02（额外保费）、03（追加保费）的一个，请改正后重新导入");
                        	return false;
                    }else if (mLARateCommisionSet.get(i).getRate() <= 0 ) {
                    	System.out.println("OnmiDiskImportRateCommisionBL.java: getRate:"+mLARateCommisionSet.get(i).getRate());
                        mErrors.addOneError("第"+i+"行导入佣金比率不能为空！请改正后重新导入");
                        return false;
                    } else if (mLARateCommisionSet.get(i).getF03()< 0 ) {
                        mErrors.addOneError("第"+i+"行导入保险期间始期不能为空！请改正后重新导入");
                        return false;
                    }else if (mLARateCommisionSet.get(i).getF04()<= 0 ) {
                        mErrors.addOneError("第"+i+"行导入保险期间止期不能为空或0！请改正后重新导入");
                        return false;
                    }
                    System.out.println("OnmiDiskImportRateCommisionBL.java: getRate:"+mLARateCommisionSet.get(i).getRate());
                    if (mLARateCommisionSet.get(i).getF03() >0&&mLARateCommisionSet.get(i).getF04()>0 ) {
//                        
                    	int intStartDate =mLARateCommisionSet.get(i).getF03();
                    	int intEndDate =mLARateCommisionSet.get(i).getF04();
                        if (intStartDate > intEndDate) {
                            mErrors.addOneError("第"+i+"行导入保险期间始期不能晚于保险期间止期！请改正后重新导入");
                            return false;
                        }
                    }
                 
                    System.out.println(mLARateCommisionSet.get(i).getManageCom());
                    mTempLARateCommisionSchema=mLARateCommisionSet.get(i);
                   
                    if(mTempLARateCommisionSchema.getManageCom().equals("86")){
                    	
                    	checkExist(mTempLARateCommisionSchema,mTempLARateCommisionDB,mLDComSet);
                    }else{
                    	
                    	checkExist(mTempLARateCommisionSchema,mTempLARateCommisionDB);
                    }
                    

                }
            }
        }

        //checkAgent();

        return true;
    }
private boolean checkExist(LARateCommisionSchema mLARateCommisionSchema,LARateCommisionDB mLARateCommisionDB,LDComSet mLDComSet){
	String manageCom=null;
	System.out.println("checkExist:mLDComSet"+mLDComSet.size());
	for(int i=1;i<=mLDComSet.size();i++){
		System.out.println("checkExist:comcode"+mLDComSet.get(i).getComCode());
		manageCom=mLDComSet.get(i).getComCode();
		mLARateCommisionSchema.setManageCom(manageCom);
		checkExist(mLARateCommisionSchema,mLARateCommisionDB);
	}
	return true;
}
private boolean checkExist(LARateCommisionSchema mLARateCommisionSchema,LARateCommisionDB mLARateCommisionDB){

	LARateCommisionSet tempLARateCommisionSet=new LARateCommisionSet();
	String tempSql="select * from laratecommision where managecom='"
		+mLARateCommisionSchema.getManageCom()+"' and riskcode='"
		+mLARateCommisionSchema.getRiskCode()+"' and payintv='"
		+mLARateCommisionSchema.getPayIntv()+"' and f01="
		+mLARateCommisionSchema.getF01()+"' and curyear="
		+mLARateCommisionSchema.getCurYear()+" and f03="
		+mLARateCommisionSchema.getF03()+" and f04="
		+mLARateCommisionSchema.getF04()+" and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")+"'";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLARateCommisionSet=mLARateCommisionDB.executeQuery(tempSql);
	if(tempLARateCommisionSet.size()>0){
		System.out.println("unImportRecords appending one");
		LARateCommisionCompareSchema tempLARateCommisionCompareSchema=new LARateCommisionCompareSchema();
		tempLARateCommisionCompareSchema.setManageCom(mLARateCommisionSchema.getManageCom());
		tempLARateCommisionCompareSchema.setRiskCode(mLARateCommisionSchema.getRiskCode());
		tempLARateCommisionCompareSchema.setRate(mLARateCommisionSchema.getRate());
		tempLARateCommisionCompareSchema.setCurYear(mLARateCommisionSchema.getCurYear());
		tempLARateCommisionCompareSchema.setF03(mLARateCommisionSchema.getF03());
		tempLARateCommisionCompareSchema.setF04(mLARateCommisionSchema.getF04());
		tempLARateCommisionCompareSchema.setPayIntv(mLARateCommisionSchema.getPayIntv());
		//tempLARateCommisionSchema=mLARateCommisionSchema;
		unImportRecords.append(mLARateCommisionSchema.getManageCom()+"*"
				+mLARateCommisionSchema.getRiskCode()+"*"
				+mLARateCommisionSchema.getPayIntv()+"*"
				+mLARateCommisionSchema.getCurYear()+"*"
				+mTransferData.getValueByName("branchtype")+"*"
				+mTransferData.getValueByName("branchtype2")+";");
		mLARateCommisionCompareSetB.add(tempLARateCommisionCompareSchema);
	}else{
		LARateCommisionSchema tempLARateCommisionSchema=new LARateCommisionSchema();
		tempLARateCommisionSchema.setManageCom(mLARateCommisionSchema.getManageCom());
		tempLARateCommisionSchema.setRiskCode(mLARateCommisionSchema.getRiskCode());
		tempLARateCommisionSchema.setRate(mLARateCommisionSchema.getRate());
		tempLARateCommisionSchema.setCurYear(mLARateCommisionSchema.getCurYear());
		tempLARateCommisionSchema.setF01(mLARateCommisionSchema.getF01());
		tempLARateCommisionSchema.setF03(mLARateCommisionSchema.getF03());
		tempLARateCommisionSchema.setF04(mLARateCommisionSchema.getF04());
		tempLARateCommisionSchema.setPayIntv(mLARateCommisionSchema.getPayIntv());
		//tempLARateCommisionSchema=mLARateCommisionSchema;
		mLARateCommisionSetFinal.add(tempLARateCommisionSchema);
	}
	return true;
}
    

    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;
    	String idxTempCompare = (String) mTransferData.getValueByName("idxCompare");
    	int idxCompare=0;
    	if(idxTempCompare!=null&&!idxTempCompare.equals("")){
    		idxCompare=Integer.parseInt(idxTempCompare)+1;
    	}else{
    		idxCompare=1;
    	}
    	System.out.println("prepareData:idxTempCompare"+idxCompare);
        if (diskimporttype.equals("LARateCommision")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARateCommisionSetFinal.size();
            
            if(mLARateCommisionSetFinal!=null&&mLARateCommisionSetFinal.size()>0){
             	
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLARateCommisionSetFinal.size(); i++) {

              	System.out.println("preparedata : managecom"+mLARateCommisionSetFinal.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	
              	mLARateCommisionSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLARateCommisionSetFinal.get(i).setIdx(idx++);
              	mLARateCommisionSetFinal.get(i).setBranchType(branchtype);
              	mLARateCommisionSetFinal.get(i).setBranchType2(branchtype2); 
              	mLARateCommisionSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARateCommisionSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mLARateCommisionSetFinal.get(i).setYear("1");
              	mLARateCommisionSetFinal.get(i).setAppAge(0);
              	//mLARateCommisionSetFinal.get(i).setF01("10");
              	mLARateCommisionSetFinal.get(i).setF02("A");
              	mLARateCommisionSetFinal.get(i).setF05("0");
              	mLARateCommisionSetFinal.get(i).setF06("0");
              	mLARateCommisionSetFinal.get(i).setsex("A");
              	//mLARateCommisionSet.get(i).setRateType("11");
              	mLARateCommisionSetFinal.get(i).setVersionType("11");
              	mmap.put(mLARateCommisionSetFinal.get(i), "INSERT");
              }
            }
            
            if(mLARateCommisionCompareSetB!=null&&mLARateCommisionCompareSetB.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	System.out.println("prepareDatacompare:importPersons"+importPersons);
            	for (int i = 1; i <= mLARateCommisionCompareSetB.size(); i++) {
//                  
              	System.out.println("preparedata : managecom"+mLARateCommisionCompareSetB.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	
              	mLARateCommisionCompareSetB.get(i).setOperator(mGlobalInput.Operator);
              	mLARateCommisionCompareSetB.get(i).setIdx(idxCompare++);
              	mLARateCommisionCompareSetB.get(i).setBranchType(branchtype);
              	mLARateCommisionCompareSetB.get(i).setBranchType2(branchtype2); 
              	mLARateCommisionCompareSetB.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARateCommisionCompareSetB.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARateCommisionCompareSetB.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARateCommisionCompareSetB.get(i).setModifyTime(PubFun.getCurrentTime());
              	mLARateCommisionCompareSetB.get(i).setYear("1");
              	mLARateCommisionCompareSetB.get(i).setAppAge(0);
              	//mLARateCommisionCompareSetB.get(i).setF01("10");
              	mLARateCommisionCompareSetB.get(i).setF02("A");
              	mLARateCommisionCompareSetB.get(i).setF05("0");
              	mLARateCommisionCompareSetB.get(i).setF06("0");
              	mLARateCommisionCompareSetB.get(i).setsex("A");
              	//mLARateCommisionSet.get(i).setRateType("11");
              	mLARateCommisionCompareSetB.get(i).setVersionType("11");
              	mmap.put(mLARateCommisionCompareSetB.get(i), "INSERT");
              }
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }

        return true;

    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public StringBuffer getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        OnmiDiskImportRateCommisionBL d = new OnmiDiskImportRateCommisionBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
