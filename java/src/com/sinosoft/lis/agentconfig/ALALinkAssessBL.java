/*
 * <p>ClassName: ALALinkAssessBL </p>
 * <p>Description: ALALinkAssessBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LALinkAssessDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LALinkAssessSchema;
import com.sinosoft.lis.vschema.LALinkAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALALinkAssessBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LALinkAssessSchema mLALinkAssessSchema = new LALinkAssessSchema();
    private LALinkAssessSet mLALinkAssessSet = new LALinkAssessSet();
    public ALALinkAssessBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALALinkAssessBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALALinkAssessBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALALinkAssessBL Submit...");
            ALALinkAssessBLS tALALinkAssessBLS = new ALALinkAssessBLS();
            tALALinkAssessBLS.submitData(mInputData, cOperate);
            System.out.println("End ALALinkAssessBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALALinkAssessBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALALinkAssessBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALALinkAssessBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println(this.mLALinkAssessSchema.getAreaType());
        LALinkAssessDB tLALinkAssessDB = new LALinkAssessDB();
        tLALinkAssessDB.setManageCom(this.mLALinkAssessSchema.getManageCom());
        tLALinkAssessDB.setAgentGrade(this.mLALinkAssessSchema.getAgentGrade());
        tLALinkAssessDB.setAreaType(this.mLALinkAssessSchema.getAreaType());
        int iCount = tLALinkAssessDB.getCount();
        if (tLALinkAssessDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLALinkAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALALinkAssessBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询所录记录是否已存在失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (iCount > 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALALinkAssessBL";
                tError.functionName = "dealData";
                tError.errorMessage = "所录记录已存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLALinkAssessSchema.setMakeDate(currentDate);
            this.mLALinkAssessSchema.setMakeTime(currentTime);
            this.mLALinkAssessSchema.setModifyDate(currentDate);
            this.mLALinkAssessSchema.setModifyTime(currentTime);
        }
        else if (this.mOperate.equals("UPDATE||MAIN") ||
                 mOperate.equals("DELETE||MAIN"))
        {
            if (iCount < 1)
            {
                CError tError = new CError();
                tError.moduleName = "ALALinkAssessBL";
                tError.functionName = "dealData";
                tError.errorMessage = "所修改记录不存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tLALinkAssessDB = new LALinkAssessDB();
            tLALinkAssessDB.setManageCom(this.mLALinkAssessSchema.getManageCom());
            tLALinkAssessDB.setAgentGrade(this.mLALinkAssessSchema.
                                          getAgentGrade());
            tLALinkAssessDB.setAreaType(this.mLALinkAssessSchema.getAreaType());
            if (!tLALinkAssessDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLALinkAssessDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALALinkAssessBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询所修改记录原信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLALinkAssessSchema.setMakeDate(tLALinkAssessDB.getMakeDate());
            this.mLALinkAssessSchema.setMakeTime(tLALinkAssessDB.getMakeTime());
            this.mLALinkAssessSchema.setModifyDate(currentDate);
            this.mLALinkAssessSchema.setModifyTime(currentTime);
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLALinkAssessSchema.setSchema((LALinkAssessSchema) cInputData.
                                           getObjectByObjectName(
                "LALinkAssessSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALALinkAssessBLQuery Submit...");
        LALinkAssessDB tLALinkAssessDB = new LALinkAssessDB();
        tLALinkAssessDB.setSchema(this.mLALinkAssessSchema);
        this.mLALinkAssessSet = tLALinkAssessDB.query();
        this.mResult.add(this.mLALinkAssessSet);
        System.out.println("End ALALinkAssessBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLALinkAssessDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLALinkAssessDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALALinkAssessBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLALinkAssessSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALALinkAssessBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
