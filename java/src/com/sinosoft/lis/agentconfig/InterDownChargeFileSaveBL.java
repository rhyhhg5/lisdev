package com.sinosoft.lis.agentconfig;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import net.sf.json.JSONObject;


import com.sinosoft.lis.db.LAAccountsDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAInterChargeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.taskservice.LAInterMixedSpecialCharge;
import com.sinosoft.lis.vschema.LAInterChargeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class InterDownChargeFileSaveBL {

	private String CrmPath = "";
	private String interFilePath = "";
	private BufferedWriter mBufferedWriter;
	private String mURL = "";
	private ExeSQL mExeSQL = new ExeSQL();
	private String mCurrentDate = PubFun.getCurrentDate();
	public CErrors mErrors = new CErrors();
	private String mIP = "";
	private String mUserName = "";
	private String mTransMode = "";
	private int mPort;
	private String mPassword = "";

	private LAInterChargeSet mLAInterChargeSet = new LAInterChargeSet();
	private String mCurrentTime = PubFun.getCurrentTime();
	private String mOperator = "jxxs";
	private String mFileDate = PubFun.getCurrentDate();
	private LJAGetSet mLJAGetSet = new LJAGetSet();
	private MMap mSecondMap = new MMap();
	private String Operator = "jxsh";
	private String mFilePosPoth = "";
	private MMap mMap = new MMap();
	private VData mResult = new VData();
	
	public boolean submitData(VData cInpuptData, String cOpearate) {

		// 获取时间、Session等参数
		if (!getInputData()) {
			return false;
		}

		try {
			// 读取数据到核心
			if (!readTextDate()) {
				return false;
			} else {
				// 生成应付数据
				if (!creatLjaGet()) {
					return false;
				}
			}
		} catch (Exception ex) {
			System.out.println("readTextDate END With Exception...");
			ex.printStackTrace();
			return false;
		} finally {

		}

		return true;
	}


	private boolean getInputData() {
		String tSql = "select sysvarvalue from ldsysvar where sysvar = 'MixedComGetUrl'";
		 CrmPath = mExeSQL.getOneValue(tSql);

//		CrmPath = "D:\\CRMDate\\";
		return true;
	}

	private void FtpTransferFiles() {
		FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
				mTransMode);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
			} else {
				String[] tFileNames = new File(CrmPath).list();

				if (tFileNames == null) {
					System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
				} else {
					for (int i = 0; i < tFileNames.length; i++) {
						System.out.println(tFileNames[i]);
						if ("000085".equals(tFileNames[i].substring(0, 6))) {
							// 财险报送文件夹
							tFTPTool.upload("/hTest", CrmPath + tFileNames[i]);
							System.out.println("文件上传成功了！");
						}
					}
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
	}

	public static void main(String[] args) {
		JsonTest tt = new JsonTest();
		VData t = new VData();
		tt.submitData(t, "11");

		// String query02 =
		// "{\"reqData\":{\"insuredFlag\":\"03\",\"idType\":\"0101\",\"customerId\":\"370829198812291023\",\"minAmt\":\"1000\",\"lossDay\":\"7\",\"serialNo\":\"999999201702180302110001\"}}";
		// System.out.println();
		// try {
		// JSONObject jsonObject = new JSONObject();
		// JSONObject
		// jsonObect1=jsonObject.fromObject(query02).getJSONObject("reqData");
		// //
		// System.out.println("1:"+jsonObject.fromObject(query02).getJSONObject("reqData"));
		// System.out.println(jsonObect1.getString("insuredFlag"));

		// String aString = jsonObject.getString("reqData");
		// System.out.println(aString);
		// JSONObject jsonObject1 = new JSONObject();
		// System.out.println("2:"+jsonObject1.toString());
		// String idType = jsonObject1.getString("idType");
		// System.out.println(idType);
		// String serialNo = jsonObject1.getString("serialNo");
		// System.out.println(serialNo);
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	/**
	 * 修改生成的文件夹名称 （添加MD5后的名称）
	 * 
	 * @param cFilePath
	 *            生成的原文件路径
	 * @param cChargeType
	 *            手续费类型
	 * @param cToCompare
	 *            发给财或寿类型
	 * @return
	 */
	private boolean changeFileName(String cFilePath, String cChargeType,
			String cToCompare) {
		File tOldFile = new File(cFilePath);
		if (!tOldFile.exists()) {
			System.out.println("原文件夹不存在！");
			return false;// 重命名文件不存在
		}
		String tMD5 = "";
		try {
			FileInputStream tFileInputStream = new FileInputStream(cFilePath);
			// tMD5 =
			// DigestUtils.md5Hex(tFileInputStream.toString()).substring(8,
			// 24);
			tMD5 = getMD5(tFileInputStream).substring(8, 24);

			tFileInputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		File tNewFile = new File(mURL + "000085" + cChargeType
				+ mCurrentDate.replaceAll("-", "") + tMD5 + cToCompare + ".txt");
		if (tNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
		{
			System.out.println("新文件已经存在！");
			return false;// 重命名文件不存在
		}
		boolean flag = tOldFile.renameTo(tNewFile);
		return flag;
	}

	public static String getMD5(InputStream is)
			throws NoSuchAlgorithmException, IOException {
		StringBuffer md5 = new StringBuffer();
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] dataBytes = new byte[1024];

		int nread = 0;
		while ((nread = is.read(dataBytes)) != -1) {
			md.update(dataBytes, 0, nread);
		}
		;
		byte[] mdbytes = md.digest();

		// convert the byte to hex format
		for (int i = 0; i < mdbytes.length; i++) {
			md5.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
					.substring(1));
		}
		return md5.toString();
	}

	private boolean readTextDate() throws java.io.FileNotFoundException,
			java.io.IOException {

		System.out.println("开始读取FTP数据");
		// 删除核心服务期旧文件
		 RefreshFileList(CrmPath);
		// 从财险FTP服务器下载文件到核心服务器
		 FtpDownFiles();
		System.out.println("文件下载完毕");
		// 读取核心服务器文件并插入到核心数据库
		File dir = new File(CrmPath);
		File[] files = dir.listFiles();
		if (files == null) {
			System.out.println("文件查询错误");
			return false;
		}
		for (int k = 0; k < files.length; k++) {
			String strFileName = files[k].getName();
			System.out.println(strFileName);
			// 判断是否数据库中是否已经存在此文件
			String judgefileSql = "select distinct 1 from laintercharge where F1 ='"
					+ strFileName + "' ";
			ExeSQL tExeSQL = new ExeSQL();
			String FileExist = tExeSQL.getOneValue(judgefileSql);
			if ("1".equals(FileExist)) {
				System.out.println("文件已经在数据库存中存在！");
				continue;
			}
			// file
			// String mLogName = FindLogFile(CrmPath);
			// if(mLogName.equals(""))
			// return false;
			// interFilePath = CrmPath + mLogName;
			// String mFileName="";
			interFilePath = CrmPath + strFileName;
			String toDelete = "I[F001]:";
			System.out.println("文件路径--+++++++----" + interFilePath
					+ "#@@@#@#@@#@@#@#");
			// interFilePath="d:\\crmdata\\" + mFileName;
			// BufferedReader reader = new BufferedReader(new
			// InputStreamReader(new FileInputStream(interFilePath), "GBK"));
			// FileReader fr = new FileReader(interFilePath);
			// String
			// testFilepath="/wasfs/lis_war/printdata/20120905U210102788793885.xml";
			// //String test2FilePath="";
			// FileInputStream tfis1 = new FileInputStream(testFilepath);
			// System.out.println("@test1读取成功："+tfis1.read());
			// InputStreamReader tisr1 = new InputStreamReader(tfis1,"GBK");
			// System.out.println("@test2读取成功："+tisr1.read());
			// BufferedReader reader1 = new BufferedReader(tisr1);
			// System.out.println("@@test3文件成功读取"+reader1.read()+"字符@@");

			FileInputStream tfis = new FileInputStream(interFilePath);
			// System.out.println("@1读取成功："+tfis.read());
			InputStreamReader tisr = new InputStreamReader(tfis, "GBK");
			// System.out.println("@2读取成功："+tisr.read());
			BufferedReader reader = new BufferedReader(tisr);
			// System.out.println("@@文件成功读取"+reader.read()+"字符@@");
			// BufferedReader reader = new BufferedReader(fr);
			String Line = reader.readLine();

			LAAgentDB tLAAgentDB = new LAAgentDB();
			int countNum = 0;
			while (Line != null) {

				if (Line.indexOf("policyList") != -1) {
					JSONObject jn = JSONObject.fromObject(Line);
					LAInterChargeSchema tJsonLAInterChargeSchema = new LAInterChargeSchema();

					String tJsonuserName = "";
					String tJsonUserComId = "";
					String tJsonHandler1Code = "";
					String tJsonHandler1ComCode = "";
					String tJsonHandlerCode = "";
					String tJsonHandlerComCode = "";
					String tJsonPubToPubPaymentNo = "";
					String tJsonCommissionNonExtractionDesc = "";

					String tJsonCommissionAmount = jn
							.getString("commissionAmount");
					String tJsonCommissionExtractionStatus = jn
							.getString("commissionExtractionStatus");

					// String tJsonCrossSellAgentCode = jn
					// .getString("crossSellAgentCode");
					// String tJsonCrossSellComCode = jn
					// .getString("crossSellComCode");
					// String tJsonCrossSellUserCode = jn
					// .getString("crossSellUserCode");
					// String tJsonCrossSellStatus =
					// jn.getString("crossSellStatus");
					String tJsonPolicyComId = jn.getString("policyComId");
					String tJsonPolicyNo = jn.getString("policyNo");
					String tJsonPolicyPremium = jn.getString("policyPremium");
					String tJsonUserCode = jn.getString("userCode");
					if (jn.has("userComId")) {
						tJsonUserComId = jn.getString("userComId");
					}
					if (jn.has("userName")) {
						tJsonuserName = jn.getString("userName");
					}
					if (jn.has("handler1Code")) {
						tJsonHandler1Code = jn.getString("handler1Code").trim();
					}
					if (jn.has("handler1ComCode")) {
						tJsonHandler1ComCode = jn.getString("handler1ComCode");
					}
					if (jn.has("handlerCode")) {
						tJsonHandlerCode = jn.getString("handlerCode").trim();
					}
					if (jn.has("handlerComCode")) {
						tJsonHandlerComCode = jn.getString("handlerComCode")
								.trim();
					}
					if (jn.has("pubToPubPaymentNo")) {
						tJsonPubToPubPaymentNo = jn
								.getString("pubToPubPaymentNo");
					}
					if (jn.has("commissionNonExtractionDesc")) {
						tJsonCommissionNonExtractionDesc = jn
								.getString("commissionNonExtractionDesc");
					}
					String tJsonPolicySystem = jn.getString("policySystem");
					// String tJsonMakeCode = jn.getString("makeCode");
					// String tJsonMakeComCode = jn.getString("makeComCode");
					String tJsonCostFee = jn.getString("costFee");
					String tJsonPaydocFee = jn.getString("payDocFee");
					String tJsonCostRate = jn.getString("costrate");
					// String tJsonReMark = jn.getString("remark");
					String tJsonSignDate = jn.getString("signDate");

					double tPerRate = 0.8;
					double tCharge = 0;
					// LAinterCharge表主键生成方式
					String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					String tInterActNo = PubFun1.CreateMaxNo("ICGETNO", tLimit);
					tCharge = Double.parseDouble(tJsonCommissionAmount);
					// 业务员转换为内部工号
					tLAAgentDB.setGroupAgentCode(tJsonUserCode);
					String tAgentCode = "";
					String tManageCom = "";
					if (tLAAgentDB.query().size() > 0) {
						tAgentCode = tLAAgentDB.query().get(1).getAgentCode();
						tManageCom = tLAAgentDB.query().get(1).getManageCom();

					}

					tJsonLAInterChargeSchema.setInterActNo(tInterActNo);
					tJsonLAInterChargeSchema.setCommissionAmount(tCharge);
					tJsonLAInterChargeSchema
							.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
					tJsonLAInterChargeSchema
							.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
					// tJsonLAInterChargeSchema
					// .setCrossSellAgentCode(tJsonCrossSellAgentCode);
					// tJsonLAInterChargeSchema
					// .setCrossSellComCode(tJsonCrossSellComCode);
					// tJsonLAInterChargeSchema
					// .setCrossSellUserCode(tJsonCrossSellUserCode);
					tJsonLAInterChargeSchema.setPolicyComId(tJsonPolicyComId);
					tJsonLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
					tJsonLAInterChargeSchema.setPolicyPremium(Double
							.parseDouble(tJsonPolicyPremium));
					tJsonLAInterChargeSchema.setUsercode(tJsonUserCode);
					tJsonLAInterChargeSchema.setUserComId(tJsonUserComId);
					tJsonLAInterChargeSchema.setUserName(tJsonuserName);

					tJsonLAInterChargeSchema.setHandlerCode(tJsonHandlerCode);
					tJsonLAInterChargeSchema
							.setHandlerComCode(tJsonHandlerComCode);
					tJsonLAInterChargeSchema.setHandler1Code(tJsonHandler1Code);
					tJsonLAInterChargeSchema
							.setHandler1ComCode(tJsonHandler1ComCode);
					// tJsonLAInterChargeSchema.setMakeCode(tJsonMakeCode);
					// tJsonLAInterChargeSchema.setMakeComCode(tJsonMakeComCode);
					tJsonLAInterChargeSchema.setCostFee(tJsonCostFee);
					tJsonLAInterChargeSchema.setCostRate(tJsonCostRate);
					tJsonLAInterChargeSchema.setPaydocFee(tJsonPaydocFee);
					// tJsonLAInterChargeSchema.setRemark(tJsonReMark);
					tJsonLAInterChargeSchema
							.setPubToPubPaymentNo(tJsonPubToPubPaymentNo);
					tJsonLAInterChargeSchema.setPolicySystem(tJsonPolicySystem);

					tJsonLAInterChargeSchema.setMakeDate(mCurrentDate);
					tJsonLAInterChargeSchema.setMakeTime(mCurrentTime);
					tJsonLAInterChargeSchema.setModifyDate(mCurrentDate);
					tJsonLAInterChargeSchema.setModifyTime(mCurrentTime);
					tJsonLAInterChargeSchema.setAgentCode(tAgentCode);
					tJsonLAInterChargeSchema.setPerCharge(tCharge * tPerRate);
					tJsonLAInterChargeSchema.setReMainCharge(tCharge - tCharge
							* tPerRate);
					tJsonLAInterChargeSchema.setF1(strFileName);
					tJsonLAInterChargeSchema.setF0("C");
					tJsonLAInterChargeSchema.setF6(tJsonSignDate);
					// tJsonLAInterChargeSchema.setF9(tManageCom);
					mLAInterChargeSet.add(tJsonLAInterChargeSchema);

					// JSONObject tJsonPolicyList =(JSONObject)
					// jn.get("policyList");
					// JSONArray tJsonMainInsuranceList = (JSONArray)
					// tJsonPolicyList.get("mainInsuranceList");

					// if(tJsonMainInsuranceList.size()>0)
					// {
					// for (int i = 0; i < tJsonMainInsuranceList.size(); i++)
					// {
					// JSONObject tSubMainJSONObject = new JSONObject();
					// LAInterChargeSchema tMainLAInterChargeSchema = new
					// LAInterChargeSchema();
					// tSubMainJSONObject =
					// tJsonMainInsuranceList.getJSONObject(i);
					//
					// String tJsonSubMainPremium =
					// tSubMainJSONObject.getString("mainPremium");
					// String tJsonSubMainRiskCode =
					// tSubMainJSONObject.getString("mainRiskCode");
					// String tJsonSubMainRiskName =
					// tSubMainJSONObject.getString("mainRiskName");
					//
					// //LAinterCharge表主键生成方式
					// String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					// String tInterActNo = PubFun1.CreateMaxNo("ICGETNO",
					// tLimit);
					// tCharge = Double.parseDouble(tJsonCommissionAmount);
					// //业务员转换为内部工号
					// tLAAgentDB.setGroupAgentCode(tJsonUserCode);
					// String tAgentCode="";
					// if(tLAAgentDB.query().size()>0){
					// tAgentCode = tLAAgentDB.query().get(1).getAgentCode();
					// }
					//
					//
					// tMainLAInterChargeSchema.setInterActNo(tInterActNo);
					// tMainLAInterChargeSchema.setCommissionAmount(tCharge);
					// tMainLAInterChargeSchema.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
					// tMainLAInterChargeSchema.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
					// tMainLAInterChargeSchema.setCrossSellAgentCode(tJsonCrossSellAgentCode);
					// tMainLAInterChargeSchema.setCrossSellComCode(tJsonCrossSellComCode);
					// tMainLAInterChargeSchema.setCrossSellUserCode(tJsonCrossSellUserCode);
					// tMainLAInterChargeSchema.setF3(tJsonPolicyComId);
					// tMainLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
					// tMainLAInterChargeSchema.setPolicyPremium(Double.parseDouble(tJsonPolicyPremium));
					// tMainLAInterChargeSchema.setUsercode(tJsonUserCode);
					// tMainLAInterChargeSchema.setUserComId(tJsonUserComId);
					// tMainLAInterChargeSchema.setUserName(tJsonuserName);
					// tMainLAInterChargeSchema.setMainPremium(Double.parseDouble(tJsonSubMainPremium));
					// tMainLAInterChargeSchema.setMainRiskCode(tJsonSubMainRiskCode);
					// tMainLAInterChargeSchema.setMainRiskName(tJsonSubMainRiskName);
					// tMainLAInterChargeSchema.setMakeDate(mCurrentDate);
					// tMainLAInterChargeSchema.setMakeTime(mCurrentTime);
					// tMainLAInterChargeSchema.setModifyDate(mCurrentDate);
					// tMainLAInterChargeSchema.setModifyTime(mCurrentTime);
					// tMainLAInterChargeSchema.setAgentCode(tAgentCode);
					// tMainLAInterChargeSchema.setPerCharge(tCharge*tPerRate);
					// tMainLAInterChargeSchema.setReMainCharge(tCharge-tCharge*tPerRate);
					// tMainLAInterChargeSchema.set
					// mLAInterChargeSet.add(tMainLAInterChargeSchema);
					// }
					//
					// }
					// JSONArray tJsonSubInsuranceList = (JSONArray)
					// tJsonPolicyList.get("subInsuranceList");
					// if(tJsonSubInsuranceList.size()>0)
					// {
					// for (int i = 0; i < tJsonSubInsuranceList.size(); i++)
					// {
					// JSONObject tSubInsuranceListObject = new JSONObject();
					// LAInterChargeSchema tSubLAInterChargeSchema = new
					// LAInterChargeSchema();
					// tSubInsuranceListObject =
					// tJsonSubInsuranceList.getJSONObject(i);
					// String tJsonSubMainPremium =
					// tSubInsuranceListObject.getString("subPremium");
					// String tJsonSubMainRiskCode =
					// tSubInsuranceListObject.getString("subRiskCode");
					// String tJsonSubMainRiskName =
					// tSubInsuranceListObject.getString("subRiskName");
					//
					// //LAinterCharge表主键生成方式
					// String tLimit = PubFun.getNoLimit(tJsonPolicyNo);
					// String tInterActNo = PubFun1.CreateMaxNo("ICGETNO",
					// tLimit);
					//
					// //业务员转换为内部工号
					// tLAAgentDB.setGroupAgentCode(tJsonUserCode);
					// String tAgentCode="";
					// if(tLAAgentDB.query().size()>0){
					// tAgentCode = tLAAgentDB.query().get(1).getAgentCode();
					// }
					//
					//
					// tSubLAInterChargeSchema.setInterActNo(tInterActNo);
					// tSubLAInterChargeSchema.setCommissionAmount(Double.parseDouble(tJsonCommissionAmount));
					// tSubLAInterChargeSchema.setCommissionExtractionStatus(tJsonCommissionExtractionStatus);
					// tSubLAInterChargeSchema.setCommissionNonPaymentDesc(tJsonCommissionNonExtractionDesc);
					// tSubLAInterChargeSchema.setCrossSellAgentCode(tJsonCrossSellAgentCode);
					// tSubLAInterChargeSchema.setCrossSellComCode(tJsonCrossSellComCode);
					// tSubLAInterChargeSchema.setCrossSellUserCode(tJsonCrossSellUserCode);
					// tSubLAInterChargeSchema.setF3(tJsonPolicyComId);
					// tSubLAInterChargeSchema.setPolicyNo(tJsonPolicyNo);
					// tSubLAInterChargeSchema.setPolicyPremium(Double.parseDouble(tJsonPolicyPremium));
					// tSubLAInterChargeSchema.setUsercode(tJsonUserCode);
					// tSubLAInterChargeSchema.setUserComId(tJsonUserComId);
					// tSubLAInterChargeSchema.setUserName(tJsonuserName);
					// tSubLAInterChargeSchema.setMainPremium(Double.parseDouble(tJsonSubMainPremium));
					// tSubLAInterChargeSchema.setMainRiskCode(tJsonSubMainRiskCode);
					// tSubLAInterChargeSchema.setMainRiskName(tJsonSubMainRiskName);
					// tSubLAInterChargeSchema.setMakeDate(mCurrentDate);
					// tSubLAInterChargeSchema.setMakeTime(mCurrentTime);
					// tSubLAInterChargeSchema.setModifyDate(mCurrentDate);
					// tSubLAInterChargeSchema.setModifyTime(mCurrentTime);
					// tSubLAInterChargeSchema.setAgentCode(tAgentCode);
					// mLAInterChargeSet.add(tSubLAInterChargeSchema);
					// }
					// }

					if (countNum == 5000) {
						mMap.put(mLAInterChargeSet, "INSERT");
						if (!SubmitMap(mMap)) {
							return false;
						}
						mLAInterChargeSet.clear();
						mMap.keySet().clear();
						countNum = 0;
					}

					System.out.println("here1:" + Line);

					countNum++;
				}
				Line = reader.readLine();
			}
			mMap.put(mLAInterChargeSet, "INSERT");
			if (!SubmitMap(mMap)) {
				return false;
			}

			mLAInterChargeSet.clear();
			mMap.keySet().clear();
			RefreshFileList(CrmPath);
			// tfis1.close();
			// tisr1.close();
			// reader1.close();
			tfis.close();
			tisr.close();
			reader.close();
		}
		return true;
	}
    
	private boolean FtpDownFiles() {

		String tFtpSQL = "select code,codename from ldcode where codetype = 'jxchargeftp' ";
		SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
		if (tSSRS == null || tSSRS.MaxRow < 1) {
			System.out.println("没有找到集团交叉ftp配置信息！");
			return false;
		}
		
		String tFileSql ="select codename from ldcode where codetype ='caichuanjian' and code ='ccj' ";
		SSRS tSSRS1 = new ExeSQL().execSQL(tFileSql);
		if (tSSRS1 == null || tSSRS1.MaxRow < 1) {
			System.out.println("没有找到财险传递健康险的文件路径！");
			return false;
		}
		mFilePosPoth = tSSRS1.GetText(1, 1);
		
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			if ("IP".equals(tSSRS.GetText(i, 1))) {
				mIP = tSSRS.GetText(i, 2);
			} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
				mUserName = tSSRS.GetText(i, 2);
			} else if ("Password".equals(tSSRS.GetText(i, 1))) {
				mPassword = tSSRS.GetText(i, 2);
			} else if ("Port".equals(tSSRS.GetText(i, 1))) {
				mPort = Integer.parseInt(tSSRS.GetText(i, 2));
			} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
				mTransMode = tSSRS.GetText(i, 2);
			}
		}

		FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
				mTransMode);
		try {
			if (!tFTPTool.loginFTP()) {
				System.out.println(tFTPTool.getErrContent(1));
			} else {
				// String tFileImportPath =
				// "/gather/gather/mqm/sales/rec/mainData/";//ftp上存放文件的目录
				// String tFileImportPath = "D:\\CRMDate\\";//ftp上存放文件的目录

				String tBeForDate = getSpecifiedDayBefore(mFileDate).replace(
						"-", "");

				tFTPTool.List(mFilePosPoth);
				if (tFTPTool.arFiles.size() > 0) {
					for (int i = 0; i < tFTPTool.arFiles.size(); i++) {
						String tFileName = (String) tFTPTool.arFiles.get(i);
						System.out.println("@@@@@@" + tFileName + "$$$$$$$$");
						if (tFileName.substring(10, 18).equals(tBeForDate)) {
							tFTPTool.downloadFile(
									mFilePosPoth,
									CrmPath, tFileName);
							System.out.println("文件下载成功了！");
						}
					}
				}
				// String tFileImportPathBackUp =
				// "/gather/gather/mqm/sales/"+PubFun.getCurrentDate()+"/";//FTP备份目录
				// if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
				// System.out.println("^&&&&&&&&新建目录已存在&&&&&&&@@@@@@@@@");
				// }
				// System.out.println("OK");

				// String[] tPath =
				// tFTPTool.downloadAllDirectory(tFileImportPath,
				// tFileImportPathBackUp);
				// System.out.println(tPath.length);
				// tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
				// CrmPath);
				// System.out.println("文件下载成功了！");

				// tFTPTool.download("/gather/gather/mqm/sales/rec/mainData/",
				// CrmPath);
				// System.out.println("文件下载成功了！");

			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
		return true;
	}
	
	/**
	 * 获得指定日期的前一天
	 * 
	 * @param specifiedDay
	 * @return
	 * @throws Exception
	 */
	public static String getSpecifiedDayBefore(String specifiedDay) {
		// SimpleDateFormat simpleDateFormat = new
		// SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date = null;

		try {
			date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day - 1);

		String dayBefore = new SimpleDateFormat("yyyy-MM-dd").format(c
				.getTime());
		return dayBefore;
	}
	
	private boolean creatLjaGet() {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		LAInterChargeSet tLAInterChargeSet = new LAInterChargeSet();
		ExeSQL ttExeSQL = new ExeSQL();
		LAAgentDB tLAAgentDB = new LAAgentDB();
		String tsql = " select sum(PerCharge),agentcode,makedate,F9 from laintercharge where PerCharge > 0 and makedate ='"+mCurrentDate+"' and f0='C' group by agentcode,makedate,F9  ";
		tSSRS = ttExeSQL.execSQL(tsql);
		String tPaySign = "C";
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
				LAInterChargeSchema tLAInterChargeSchema = new LAInterChargeSchema();
				LAAgentSchema tLAAgentSchema = new LAAgentSchema();
				tLAInterChargeSchema = tLAInterChargeSet.get(i);
				String tLimit = PubFun.getNoLimit(tSSRS.GetText(i, 2));
				String tDealDate = tSSRS.GetText(i, 3).replace("-", "");
				tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("BCGETNO",
						tLimit));

				tLJAGetSchema.setOtherNo(tSSRS.GetText(i, 2).trim() + tDealDate
						+ tPaySign);
				tLJAGetSchema.setGetNoticeNo(tSSRS.GetText(i, 2));
				tLJAGetSchema.setPayMode("11");
				tLJAGetSchema.setShouldDate(mFileDate);
				// tLJAGetSchema.setAgentGroup(tLAWageSchema.getAgentGroup());
				tLJAGetSchema.setOtherNoType("25");
				tLJAGetSchema.setManageCom(tSSRS.GetText(i, 4));
				tLJAGetSchema.setAgentCode(tSSRS.GetText(i, 2));
				tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i, 1));
				tLAAgentDB.setAgentCode(tSSRS.GetText(i, 2));
				tLAAgentSchema = tLAAgentDB.getSchema();
				String tSql = "select * from laaccounts where agentcode ='"
						+ tLAAgentSchema.getAgentCode()
						+ "' and state ='0'  with ur ";
				LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
				LAAccountsDB tLAAccountsDB = new LAAccountsDB();
				tLAAccountsSchema = tLAAccountsDB.executeQuery(tSql).get(1);
				tLJAGetSchema.setBankAccNo(tLAAccountsSchema.getAccount());

				tLJAGetSchema.setAccName(tLAAccountsSchema.getAccountName());
				tLJAGetSchema.setBankCode(tLAAccountsSchema.getT1());
				// 添加领取人相关信息
				tLJAGetSchema.setDrawer(tLAAgentSchema.getName());
				// tLJAGetSchema.setDrawerID(tLAAgentSchema.getIDNo());
				tLJAGetSchema.setOperator(Operator);
				tLJAGetSchema.setMakeDate(mCurrentDate);
				tLJAGetSchema.setMakeTime(mCurrentTime);
				tLJAGetSchema.setModifyDate(mCurrentDate);
				tLJAGetSchema.setModifyTime(mCurrentTime);
				this.mLJAGetSet.add(tLJAGetSchema);
			}
		}
		mSecondMap.put(this.mLJAGetSet, "INSERT");
		if (!SubmitMap(mSecondMap)) {
			return false;
		}
		return true;
	}

	private boolean RefreshFileList(String StrPath) {
		File dir = new File(StrPath);
		File[] files = dir.listFiles();
		if (files == null) {
			return false;
		}
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				RefreshFileList(files[i].getAbsolutePath());
			} else {
				String strFileName = files[i].getAbsolutePath();
				if (strFileName.endsWith("000085.txt")) {
					System.out.println("正在删除：" + strFileName);
					files[i].delete();
				}
			}
		}
		return true;
	}
	private boolean SubmitMap(MMap tmmap) {
		PubSubmit tPubSubmit = new PubSubmit();
		mResult = new VData();
		mResult.add(tmmap);
		if (!tPubSubmit.submitData(mResult, "")) {
			System.out.println("提交数据失败！");
			return false;
		}
		// System.gc();
		return true;
	}
}
