package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportActiveUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportActiveBL mLADiskImportActiveBL = null;

    public LADiskImportActiveUI()
    {
    	mLADiskImportActiveBL = new LADiskImportActiveBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportActiveBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportActiveBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportActiveBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportActiveBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportActiveBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportActiveBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportActiveUI zLADiskImportBRCDSUI = new LADiskImportActiveUI();
    }
}
