package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportUniCommAwardUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportUniCommAwardBL mLADiskImportUniCommAwardBL = null;

    public LADiskImportUniCommAwardUI()
    {
    	mLADiskImportUniCommAwardBL = new LADiskImportUniCommAwardBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportUniCommAwardBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportUniCommAwardBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportUniCommAwardBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportUniCommAwardBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportUniCommAwardBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportUniCommAwardBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportUniCommAwardUI zLADiskImportBRCDSUI = new LADiskImportUniCommAwardUI();
    }
}
