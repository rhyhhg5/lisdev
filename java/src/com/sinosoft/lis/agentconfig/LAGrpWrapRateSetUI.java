package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.lis.vschema.LARateCommisionSet;
/**
 * <p>Title: LAGrpWrapRateSetUI</p>
 *
 * <p>Description:  LIS - ���۹���</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author  
 * @version 1.0
 */
public class LAGrpWrapRateSetUI
{
       public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAGrpWrapRateSetUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("come here   ");
        LAGrpWrapRateSetBL tLAGrpWrapRateSetBL = new LAGrpWrapRateSetBL();

        if (!tLAGrpWrapRateSetBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAGrpWrapRateSetBL.mErrors);

            return false;
        } else {
            this.mResult = tLAGrpWrapRateSetBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
