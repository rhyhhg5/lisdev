package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LATaxSet;
import com.sinosoft.lis.schema.LATaxSchema;
import com.sinosoft.lis.db.LATaxDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author wangrx 2015-9-23
 * @version 1.0
 */
public class LAActiveTaxBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String tFlag = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
//  private MMap newMap = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LATaxSet mLATaxSet = new LATaxSet();
  private Reflections ref = new Reflections();
  public LAActiveTaxBL() {

  }

	public static void main(String[] args)
	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//		System.out.println(5/4);
	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveTaxBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveTaxBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LAActiveTaxBL.check.........1"+mLATaxSet.size());	 
	  	LATaxSchema tempLATaxSchema1; 
	  	LATaxSchema tempLATaxSchema2;
	  	LATaxSet tLATaxSet = new LATaxSet();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  LATaxDB tLATaxDB = new LATaxDB();
		  for(int i=1;i<=this.mLATaxSet.size();i++){
			  tempLATaxSchema1=this.mLATaxSet.get(i);
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LATax where  managecom='")
				  .append(tempLATaxSchema1.getManageCom())
				  .append("' and taxtype = '")
				  .append(tempLATaxSchema1.getTaxType())
				  .append("' and taxcode = ")
				  .append(tempLATaxSchema1.getTaxCode())
				  .append("");
				  tResult=tExeSQL.getOneValue(tSB.toString());
				if(tResult!=null&&tResult.equals("1")){
					 CError tError = new CError();                   
				     tError.moduleName = "LAActiveAgentTaxBL";
				     tError.functionName = "check()";
				     tError.errorMessage = "第"+(i%4==0?i/4:i/4+1)+"行中录入管理机构的与数据库中管理机构相同！";
				     this.mErrors.clearErrors();
				     this.mErrors.addOneError(tError);
				     return false;				
				}
		  
			  
//			 if(mOperate.equals("UPDATE")){
////				 tLATaxDB.setTaxCode(tempLATaxSchema1.getTaxCode());
////				 tLATaxDB.setTaxType(tempLATaxSchema1.getTaxType());
////				 tLATaxDB.setManageCom(tempLATaxSchema1.getManageCom());
//				String tTaxSQL ="select * from latax where taxcode ='"+tempLATaxSchema1.getTaxCode()+"'" +
//						" and taxtype ='"+tempLATaxSchema1.getTaxType()+"' and managecom ='"+tempLATaxSchema1.getManageCom()+"' ";
//				 tLATaxSet = tLATaxDB.executeQuery(tTaxSQL);
//				 if(tLATaxSet.size()>0){
//					 
//				 }
//				 if(!tempLATaxSchema1.getManageCom().equals(tLATaxSchemaNew.getManageCom())){
//					 BuildError("dealData","第"+(i%4==0?i/4:i/4+1)+"行管理机构,不能修改！");
//                     return false;
//				 }
//			 }
//				}	
			for (int j = i+1; j <= this.mLATaxSet.size(); j++) {
				tempLATaxSchema2 = mLATaxSet.get(j);
				if(tempLATaxSchema2.getManageCom().equals(tempLATaxSchema1.getManageCom())&&
					tempLATaxSchema2.getTaxCode()==tempLATaxSchema1.getTaxCode()){
					System.out.println("...........erro1");
					  CError tError = new CError();
				      tError.moduleName = "LACommisionAwardRateBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。管理机构数据重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				}
			   }
			  }	
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAActiveTaxBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLATaxSet.set( (LATaxSet) cInputData.getObjectByObjectName("LATaxSet",0));
      System.out.println("LATaxSet get"+mLATaxSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveTaxBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAActiveTaxBL.dealData........."+mOperate);
    try {
    	LATaxSchema  tLATaxSchemaNew = new  LATaxSchema();
    	LATaxSchema  tLATaxSchemaOld = new  LATaxSchema();
    	LATaxDB tLATaxDB =new  LATaxDB();
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LAActiveTaxBL.dealData.........INSERT"+mLATaxSet.size());
	        LATaxSet tLATaxSet = new LATaxSet();
	        for (int i = 1; i <= mLATaxSet.size(); i++) {
	        	tLATaxSchemaNew = mLATaxSet.get(i);	
		         
		      tLATaxSet.add(tLATaxSchemaNew);
      }
        	map.put(tLATaxSet, mOperate);
      }
      if(mOperate.equals("UPDATE")) {
    	  LATaxSet tLATaxSet = new LATaxSet();
    	 for(int i = 1;i<=mLATaxSet.size();i++){
    		 tLATaxDB.setTaxCode(mLATaxSet.get(i).getTaxCode());
    		 tLATaxDB.setTaxType(mLATaxSet.get(i).getTaxType());
    		 tLATaxDB.setManageCom(mLATaxSet.get(i).getManageCom());
    		 tLATaxSchemaOld = tLATaxDB.query().get(1); 
    		 tLATaxSchemaNew = mLATaxSet.get(i);
    		 tLATaxSet.add(tLATaxSchemaNew);
    	 }
    	 map.put(tLATaxSet, mOperate);
      }
      if(mOperate.equals("DELETE")){
    	  LATaxSet tLATaxSet = new LATaxSet();
    	 for(int i = 1;i<=mLATaxSet.size();i++){	
    		 tLATaxDB.setTaxType(mLATaxSet.get(i).getTaxType());
    		 tLATaxDB.setManageCom(mLATaxSet.get(i).getManageCom());
    		 tLATaxSchemaOld = tLATaxDB.query().get(1); 
    		 tLATaxSchemaNew = mLATaxSet.get(i);
    		 tLATaxSet.add(tLATaxSchemaNew);
    	 }
    	 map.put(tLATaxSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveTaxBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveTaxBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LAActiveTaxBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
