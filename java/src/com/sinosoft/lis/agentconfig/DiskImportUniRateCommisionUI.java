package com.sinosoft.lis.agentconfig;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportUniRateCommisionUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportUniRateCommisionBL mDiskImportUniRateCommisionBL = null;

    public DiskImportUniRateCommisionUI()
    {
        mDiskImportUniRateCommisionBL = new DiskImportUniRateCommisionBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mDiskImportUniRateCommisionBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportUniRateCommisionBL.mErrors);
            return false;
        }
        //
        if(mDiskImportUniRateCommisionBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportUniRateCommisionBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportUniRateCommisionBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mDiskImportUniRateCommisionBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
        DiskImportUniRateCommisionUI zDiskImportUniRateCommisionUI = new DiskImportUniRateCommisionUI();
    }
}
