
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetWNDSBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz
 * @version 1.0
 */
public class LABRCSetWNZXBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public LABRCSetWNZXBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LABRCSetWNDSBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABRCSetWNDSBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABRCSetWNDSBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
     this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetWNZXBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  if(!this.mOperate.equals("DELETE")){
	  LARateCommisionSchema tempLARateCommisionSchema1;
	  LARateCommisionSchema tempLARateCommisionSchema2;
	  LARateCommisionSchema tempLARateCommisionSchema3;
	  LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
	  ExeSQL tExeSQL=new ExeSQL();
	  StringBuffer tSB;
	  String tResult;
	  for(int i=1;i<=this.mLARateCommisionSet.size();i++){
		  tempLARateCommisionSchema1=this.mLARateCommisionSet.get(i);
		  for(int j=i+1;j<=this.mLARateCommisionSet.size();j++){
			  tempLARateCommisionSchema2=this.mLARateCommisionSet.get(j);
			//add by wrx 2015-7
	          	//添加--交验---要导入的数据中相互间是否存在重复年期
			  if(tempLARateCommisionSchema1.getRiskCode().equals(tempLARateCommisionSchema2.getRiskCode())&&
					  tempLARateCommisionSchema1.getManageCom().equals(tempLARateCommisionSchema2.getManageCom())&&
					  tempLARateCommisionSchema1.getF01().equals(tempLARateCommisionSchema2.getF01())&&
					  tempLARateCommisionSchema1.getPayIntv().equals(tempLARateCommisionSchema2.getPayIntv())&&
					  tempLARateCommisionSchema1.getCurYear()==tempLARateCommisionSchema2.getCurYear()&&
					  tempLARateCommisionSchema1.getYear()==tempLARateCommisionSchema2.getYear()&&
					  tempLARateCommisionSchema1.getF06().equals(tempLARateCommisionSchema2.getF06())){
				  Integer tStartYear1 =Integer.valueOf(tempLARateCommisionSchema2.getF03());
				  Integer tEndYear1 = Integer.valueOf(tempLARateCommisionSchema2.getF03());
				  Integer mStartYear1 = Integer.valueOf(tempLARateCommisionSchema1.getF03());
				  Integer mEndYear1 = Integer.valueOf(tempLARateCommisionSchema1.getF04());
				  if((mStartYear1.intValue()>=tStartYear1.intValue())&&mStartYear1.intValue()<tEndYear1.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  if((mEndYear1.intValue()>=tStartYear1.intValue())&&mEndYear1.intValue()<tEndYear1.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  if((mStartYear1.intValue()<=tStartYear1.intValue())&&mEndYear1.intValue()>=tEndYear1.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  //end
			  }
			  if(tempLARateCommisionSchema1.getRiskCode()
					  .equals(tempLARateCommisionSchema2.getRiskCode())
					  &&tempLARateCommisionSchema1.getCurYear()
					  ==tempLARateCommisionSchema2.getCurYear()
					  &&tempLARateCommisionSchema1.getPayIntv()
					  .equals(tempLARateCommisionSchema2.getPayIntv())
					  &&tempLARateCommisionSchema1.getF01()
					  .equals(tempLARateCommisionSchema2.getF01())
					  &&tempLARateCommisionSchema1.getF03()
					  ==tempLARateCommisionSchema2.getF03()
					  &&tempLARateCommisionSchema1.getF04()
					  ==tempLARateCommisionSchema2.getF04()
					  &&tempLARateCommisionSchema1.getManageCom()
					  .equals(tempLARateCommisionSchema2.getManageCom())&&(tempLARateCommisionSchema1.getYear()==tempLARateCommisionSchema2.getYear())){
				  CError tError = new CError();
			      tError.moduleName = "LABRCSetWNZXBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
		  }
		  	  
		  
		  if(this.mOperate.equals("INSERT")){
			  tSB=new StringBuffer();
			  tSB=tSB.append("select distinct 1 from laratecommision where riskcode='")
			  .append(tempLARateCommisionSchema1.getRiskCode())
			  .append("' and curyear=")
			  .append(tempLARateCommisionSchema1.getCurYear())
			  .append(" and payintv='")
			  .append(tempLARateCommisionSchema1.getPayIntv())
			  .append("' and f03=")
			  .append(tempLARateCommisionSchema1.getF03())
			  .append(" and f04=")
			  .append(tempLARateCommisionSchema1.getF04())
			  .append(" and f01='")
			  .append(tempLARateCommisionSchema1.getF01())
			   .append("' and f05 = '")
			  .append(tempLARateCommisionSchema1.getF05())
			  .append("' and managecom = '")
			  .append(tempLARateCommisionSchema1.getManageCom())
			  .append("'")
			  .append(" and branchtype='")
			  .append(tempLARateCommisionSchema1.getBranchType())
			  .append("'")
			  .append(" and branchtype2='")
			  .append(tempLARateCommisionSchema1.getBranchType2())
			  .append("'")
			  .append(" and agentcom is null")
			  .append(" and year = ")
			  .append(tempLARateCommisionSchema1.getYear());
			  tResult=tExeSQL.getOneValue(tSB.toString());
			  if(tResult!=null&&tResult.equals("1")){
				  CError tError = new CError();
			      tError.moduleName = "LABRCSetWNDSBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
		  }
		  if(this.mOperate.equals("UPDATE")){
			  SSRS tSSRS=new SSRS();
			  tSB=new StringBuffer();
			  tSB=tSB.append("select distinct idx from laratecommision where riskcode='")
			  .append(tempLARateCommisionSchema1.getRiskCode())
			  .append("' and curyear=")
			  .append(tempLARateCommisionSchema1.getCurYear())
			  .append(" and payintv='")
			  .append(tempLARateCommisionSchema1.getPayIntv())
			  .append("' and f03=")
			  .append(tempLARateCommisionSchema1.getF03())
			  .append(" and f04=")
			  .append(tempLARateCommisionSchema1.getF04())
			  .append(" and f01='")
			  .append(tempLARateCommisionSchema1.getF01())
			  .append("' and managecom = '")
			  .append(tempLARateCommisionSchema1.getManageCom())
			  .append("' and f05 = '")
			  .append(tempLARateCommisionSchema1.getF05())
			  .append("'")
			  .append(" and branchtype='")
			  .append(tempLARateCommisionSchema1.getBranchType())
			  .append("'")
			  .append(" and branchtype2='")
			  .append(tempLARateCommisionSchema1.getBranchType2())
			  .append("'")
			  .append(" and agentcom is null")
			  .append(" and year = ")
			  .append(tempLARateCommisionSchema1.getYear());
			  tSSRS=tExeSQL.execSQL(tSB.toString());
			  for(int m=1;m<=tSSRS.getMaxRow();m++){
				  if(tempLARateCommisionSchema1.getIdx()!=Integer.parseInt(tSSRS.GetText(m,1))){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNDSBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
			  tLARateCommisionDB.setIdx(tempLARateCommisionSchema1.getIdx());
			  tempLARateCommisionSchema3 = tLARateCommisionDB.query().get(1);
			  System.out.println(tempLARateCommisionSchema3.getPayIntv());
			  if(!tempLARateCommisionSchema1.getRiskCode().equals(tempLARateCommisionSchema3.getRiskCode())||
				  !tempLARateCommisionSchema1.getManageCom().equals(tempLARateCommisionSchema3.getManageCom())||
				  !tempLARateCommisionSchema1.getPayIntv().equals(tempLARateCommisionSchema3.getPayIntv())||
				  tempLARateCommisionSchema1.getCurYear()!=tempLARateCommisionSchema3.getCurYear()||
				  tempLARateCommisionSchema1.getYear()!=tempLARateCommisionSchema3.getYear()||
				  tempLARateCommisionSchema1.getF03()!=tempLARateCommisionSchema3.getF03()||
				  tempLARateCommisionSchema1.getF04()!=tempLARateCommisionSchema3.getF04()||
				  !tempLARateCommisionSchema1.getF01().equals(tempLARateCommisionSchema3.getF01())||
				  !tempLARateCommisionSchema1.getF06().equals(tempLARateCommisionSchema3.getF06())	  
			  ){
				  CError tError = new CError();
			      tError.moduleName = "LABRCSetFHCTBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "只能修改佣金比率";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
		  }
		  
		  
		//验证重复年期
		  //add by wrx 2015-07-23 
		  String mSQL = "select F03,F04 from laratecommision where riskcode = '"+tempLARateCommisionSchema1.getRiskCode()+"'" +
		  		 		" and managecom = '"+tempLARateCommisionSchema1.getManageCom()+"'"+
		  		 		" and payintv = '"+tempLARateCommisionSchema1.getPayIntv()+"'"+
		  		 		" and F05 = '"+tempLARateCommisionSchema1.getF05()+"'"+
		  		 		" and curyear = '"+tempLARateCommisionSchema1.getCurYear()+"'"+
		  		 		" and year = '"+tempLARateCommisionSchema1.getYear()+"'"+
		  		 		" and branchtype = '3' and branchtype2 = '01'";
		  if(this.mOperate.equals("UPDATE")){
			  mSQL +=" and idx <> '"+tempLARateCommisionSchema1.getIdx()+"'";
		  }
		  SSRS mSSRS=new SSRS();		 		
		  mSSRS=tExeSQL.execSQL(mSQL);
		  if(mSSRS!=null&&mSSRS.getMaxRow()>0){
			  for(int k=1;k<=mSSRS.getMaxRow();k++){
				  Integer tStartYear =Integer.valueOf(mSSRS.GetText(k, 1));
				  Integer tEndYear = Integer.valueOf(mSSRS.GetText(k, 2));
				  Integer mStartYear = Integer.valueOf(tempLARateCommisionSchema1.getF03());
				  Integer mEndYear = Integer.valueOf(tempLARateCommisionSchema1.getF04());
				  if((mStartYear.intValue()>=tStartYear.intValue())&&mStartYear.intValue()<tEndYear.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  if((mEndYear.intValue()>=tStartYear.intValue())&&mEndYear.intValue()<tEndYear.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  if((mStartYear.intValue()<=tStartYear.intValue())&&mEndYear.intValue()>=tEndYear.intValue()){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetWNZXBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
		  }
		  //验证重复年期end
	  }
	  }
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LABRCSetWNDSBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
        LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
        LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          LARateCommisionSchema tLARateCommisionSchemaold = new
              LARateCommisionSchema();
          LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
          LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();

          tLARateCommisionDB.setIdx(mLARateCommisionSet.get(i).getIdx());
          tLARateCommisionDB.setRiskCode(mLARateCommisionSet.get(i).getRiskCode());
          tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
          tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
          //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
          tLARateCommisionSchemanew.setMakeDate(tLARateCommisionSchemaold.
                                                getMakeDate());
          tLARateCommisionSchemanew.setF01(mLARateCommisionSet.get(i).
                  getF01());
          tLARateCommisionSchemanew.setMakeTime(tLARateCommisionSchemaold.
                                                getMakeTime());
          tLARateCommisionSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionSchemanew.setModifyTime(CurrentTime);
          tLARateCommisionSchemanew.setVersionType("11");
          tLARateCommisionSet.add(tLARateCommisionSchemanew);
          LARateCommisionBSchema tLARateCommisionBSchema = new
              LARateCommisionBSchema();
          ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
          //获取最大的ID号
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select int(max(idx)) from laratecommisionb order by 1 desc ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
          }
          tMaxIdx++;
          tLARateCommisionBSchema.setIdx(tMaxIdx);
          if(i>1){
            tLARateCommisionBSchema.setIdx(tLARateCommisionBSet.get(i - 1).
                                           getIdx() + 1);
          }
          tLARateCommisionBSchema.setEdorNo(tLARateCommisionSchemaold.getIdx()+"");
          if(mOperate.equals("UPDATE"))
            tLARateCommisionBSchema.setEdorType("02");
          else
            tLARateCommisionBSchema.setEdorType("01");
          tLARateCommisionBSet.add(tLARateCommisionBSchema);

        }
        map.put(tLARateCommisionSet, mOperate);
        map.put(tLARateCommisionBSet, "INSERT");
      }
      else {

    	  LARateCommisionSet tLARateCommisionSet = new
          LARateCommisionSet();
  for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
      LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
      tLARateCommisionSchemanew = mLARateCommisionSet.get(i);

      String agentSQL =
      "select idx from laratecommision where riskcode='"+tLARateCommisionSchemanew.getRiskCode()+"'"
      +" and managecom='"+tLARateCommisionSchemanew.getManageCom()+"'"
      +" and curyear="+tLARateCommisionSchemanew.getCurYear()+""
      +" and payintv='"+tLARateCommisionSchemanew.getPayIntv()+"'"
      +" and f03="+tLARateCommisionSchemanew.getF03()+""
      +" and f04="+tLARateCommisionSchemanew.getF04()+""
      +" and f01='"+tLARateCommisionSchemanew.getF01()+"'"
      +" and f05='"+tLARateCommisionSchemanew.getF05()+"'"
      +" and year='"+tLARateCommisionSchemanew.getYear()+"'"
      +" and branchtype='"+tLARateCommisionSchemanew.getBranchType()+"'"
      +" and branchtype2='"+tLARateCommisionSchemanew.getBranchType2()+"'"
      +" and agentcom is null";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(agentSQL);
       if(tSSRS.getMaxRow()>0)
       {
               // @@错误处理
       this.mErrors.copyAllErrors(tSSRS.mErrors);
       CError tError = new CError();
       tError.moduleName = "LABRCSetWNDSBL";
       tError.functionName = "submitDat";
       tError.errorMessage = "已经保存了相同险种的佣金率内容，请重新录入!";
       this.mErrors.addOneError(tError);
       return false;
       }

      tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
tLARateCommisionSchemanew.setMakeDate(CurrentDate);
tLARateCommisionSchemanew.setMakeTime(CurrentTime);
tLARateCommisionSchemanew.setModifyDate(CurrentDate);
tLARateCommisionSchemanew.setModifyTime(CurrentTime);
tLARateCommisionSchemanew.setVersionType("11");
tLARateCommisionSet.add(tLARateCommisionSchemanew);

  }
  map.put(tLARateCommisionSet, mOperate);
}
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetWNDSBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LABRCSetWNDSBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
      System.out.println(
          "end  LABRCSetWNDSBL.prepareOutputData.........");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetWNDSBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
