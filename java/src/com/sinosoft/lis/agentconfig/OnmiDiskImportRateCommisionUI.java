package com.sinosoft.lis.agentconfig;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author lihai
 * @version 1.1
 */
public class OnmiDiskImportRateCommisionUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private OnmiDiskImportRateCommisionBL mOnmiDiskImportRateCommisionBL = null;

    public OnmiDiskImportRateCommisionUI()
    {
        mOnmiDiskImportRateCommisionBL = new OnmiDiskImportRateCommisionBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	System.out.println("UI submited");
    	if(!mOnmiDiskImportRateCommisionBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mOnmiDiskImportRateCommisionBL.mErrors);
            return false;
        }
        
        if(mOnmiDiskImportRateCommisionBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mOnmiDiskImportRateCommisionBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mOnmiDiskImportRateCommisionBL.getImportPersons();
    }
    public StringBuffer getUnImportRecords()
    {
        return mOnmiDiskImportRateCommisionBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	OnmiDiskImportRateCommisionUI zDiskImportRateCommisionUI = new OnmiDiskImportRateCommisionUI();
    }
}
