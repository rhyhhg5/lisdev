package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LATrainerIndexSet;
import com.sinosoft.lis.schema.LATrainerIndexSchema;
import com.sinosoft.lis.db.LATrainerIndexDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LATrainerCommisionBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author WangQingmin
 * @version 1.0
 */
public class LATrainerCommisionBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LATrainerIndexSet mLATrainerIndexSet = new LATrainerIndexSet();
  public LATrainerCommisionBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LATrainerCommisionBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LATrainerCommisionBL.check.........1"+mLATrainerIndexSet.size());	 
	  	LATrainerIndexSchema tempLATrainerIndexSchema1; 
	  	LATrainerIndexSchema tempLATrainerIndexSchema2;
	  	LATrainerIndexSchema tLATrainerIndexSchemaNew = new LATrainerIndexSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  LATrainerIndexDB tLATrainerIndexDB = new LATrainerIndexDB();
		  for(int i=1;i<=this.mLATrainerIndexSet.size();i++){
			  tempLATrainerIndexSchema1=this.mLATrainerIndexSet.get(i);
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLATrainerIndexSet.size();j++){
					  tempLATrainerIndexSchema2 = this.mLATrainerIndexSet.get(j);
					  if(tempLATrainerIndexSchema1.getPManageCom().equals(tempLATrainerIndexSchema2.getPManageCom())){
							 BuildError("dealData","第"+i+","+j+"行管理机构相同请检查！");
		                     return false; 
					  }
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LATrainerIndex where  pmanagecom='")
				  .append(tempLATrainerIndexSchema1.getPManageCom())
				  .append("' and wagecode = '")
				  .append(tempLATrainerIndexSchema1.getWageCode())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LATrainerCommisionBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "第"+i+"行该机构下基本工资标准已录入";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
			 if(mOperate.equals("UPDATE")){
				 tLATrainerIndexDB.setIdx(tempLATrainerIndexSchema1.getIdx());
				 tLATrainerIndexSchemaNew = tLATrainerIndexDB.query().get(1);
				 if(!tempLATrainerIndexSchema1.getPManageCom().equals(tLATrainerIndexSchemaNew.getPManageCom())){
					 BuildError("dealData","第"+i+"行管理机构不能修改！修改时只能修改基本工资标准！");
                     return false;
				 }
			 }
		  
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LATrainerCommisionBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLATrainerIndexSet.set( (LATrainerIndexSet) cInputData.getObjectByObjectName("LATrainerIndexSet",0));
      System.out.println("LATrainerIndexSet get"+mLATrainerIndexSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LATrainerCommisionBL.dealData........."+mOperate);
    try {
    	LATrainerIndexSchema  tLATrainerIndexSchemaNew = new  LATrainerIndexSchema();
    	LATrainerIndexSchema  tLATrainerIndexSchemaOld = new  LATrainerIndexSchema();
    	LATrainerIndexDB tLATrainerIndexDB =new  LATrainerIndexDB();
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LATrainerCommisionBL.dealData.........INSERT"+mLATrainerIndexSet.size());
	        LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
	        for (int i = 1; i <= mLATrainerIndexSet.size(); i++) {
	        	tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);	
		          ExeSQL tExe = new ExeSQL();
		          String tSql =
		              "select max(int(idx)) from LATrainerIndex order by 1 desc";
		          String strIdx = "";
		          int tMaxIdx = 0;
		          strIdx = tExe.getOneValue(tSql);
		          if (strIdx == null || strIdx.trim().equals("")) {
		            tMaxIdx = 0;
		          }
		          else {
		            tMaxIdx = Integer.parseInt(strIdx);
		          }
		      tMaxIdx += i;
		      String sMaxIdx =tMaxIdx+"";
		      tLATrainerIndexSchemaNew.setIdx(sMaxIdx);
		      tLATrainerIndexSchemaNew.setMakeDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setMakeTime(CurrentTime);
		      tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
		      tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
      }
        	map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("UPDATE")) {
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
    		 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("DELETE")){
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){	
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
		      
    	 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LATrainerCommisionBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
