/*
 * <p>ClassName: OLAAssessIndexUI </p>
 * <p>Description: LAAssessIndexUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAssessIndexSchema;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class OLAAssessIndexUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessIndexSchema mLAAssessIndexSchema = new LAAssessIndexSchema();
    private LAAssessIndexSet mLAAssessIndexSet = new LAAssessIndexSet();
    public OLAAssessIndexUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        OLAAssessIndexBL tOLAAssessIndexBL = new OLAAssessIndexBL();
        System.out.println("Start OLAAssessIndex UI Submit...");
        tOLAAssessIndexBL.submitData(mInputData, mOperate);
        System.out.println("End OLAAssessIndex UI Submit...");

        //如果有需要处理的错误，则返回
        if (tOLAAssessIndexBL.mErrors.needDealError())
        {
            // @@错误处理

            this.mErrors.copyAllErrors(tOLAAssessIndexBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLAAssessIndexUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.indexOf("QUERY") != -1)
        {
            this.mResult.clear();
            this.mResult = tOLAAssessIndexBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        LAAssessIndexSchema tLAAssessIndexSchema = new LAAssessIndexSchema();
        tLAAssessIndexSchema.setBranchType("02");
        tLAAssessIndexSchema.setCalCode("30");
        tLAAssessIndexSchema.setCalType("0");
        tLAAssessIndexSchema.setIndexName("trival");
        tLAAssessIndexSchema.setIndexCode("");

        GlobalInput tG = new GlobalInput();
        tG.Operator = "Admin";
        tG.ManageCom = "001";
        tVData.clear();
        tVData.addElement(tLAAssessIndexSchema);
        tVData.addElement(tG);
        String cOperate = "QUERY||MAIN";

        OLAAssessIndexUI tOLAAssessIndexUI = new OLAAssessIndexUI();
        tOLAAssessIndexUI.submitData(tVData, cOperate);

        //查询显示数据
        VData tResult = new VData();
        LAAssessIndexSet tLAAssessIndexSet = new LAAssessIndexSet();
        tResult = tOLAAssessIndexUI.getResult();
        tLAAssessIndexSet.set((LAAssessIndexSet) tResult.getObjectByObjectName(
                "LAAssessIndexSet", 0));
        int tCount = tLAAssessIndexSet.size();
        for (int i = 1; i <= tCount; i++)
        {
            tLAAssessIndexSchema = tLAAssessIndexSet.get(i);
            System.out.println("----jg---" + tLAAssessIndexSchema.getIndexCode());
        }

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLAAssessIndexSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessIndexUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAssessIndexSchema.setSchema((LAAssessIndexSchema) cInputData.
                                            getObjectByObjectName(
                "LAAssessIndexSchema", 0));
        System.out.println("------1");
        if (mGlobalInput == null)
        {
            System.out.println("------2");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessIndexUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("------3");
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
