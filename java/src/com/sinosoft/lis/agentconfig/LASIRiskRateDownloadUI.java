package com.sinosoft.lis.agentconfig;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LASIRiskRateDownloadUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LASIRiskRateDownloadBL mLASIRiskRateDownloadBL = null;

    public LASIRiskRateDownloadUI()
    {
    	mLASIRiskRateDownloadBL = new LASIRiskRateDownloadBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLASIRiskRateDownloadBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLASIRiskRateDownloadBL.mErrors);
            return false;
        }
        //
        if(mLASIRiskRateDownloadBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLASIRiskRateDownloadBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLASIRiskRateDownloadBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLASIRiskRateDownloadBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LASIRiskRateDownloadUI zLASIRiskRateDownloadUI = new LASIRiskRateDownloadUI();
    }
}
