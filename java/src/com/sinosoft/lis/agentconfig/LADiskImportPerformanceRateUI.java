package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportPerformanceRateUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportPerformanceRateBL mLADiskImportPerformanceRateBL = null;

    public LADiskImportPerformanceRateUI()
    {
    	mLADiskImportPerformanceRateBL = new LADiskImportPerformanceRateBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportPerformanceRateBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportPerformanceRateBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportPerformanceRateBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportPerformanceRateBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportPerformanceRateBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportPerformanceRateBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportPerformanceRateUI zLADiskImportBRCDSUI = new LADiskImportPerformanceRateUI();
    }
}
