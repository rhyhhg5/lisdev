package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LaratecommisionOnmiSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author lihai
 * @version 1.0
 */
public class LASocialWNRateSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public LASocialWNRateSetBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LASocialWNRateSetBL.java.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LASocialWNRateSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LASocialWNRateSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("endblhere:.........................");
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LASocialWNRateSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
     this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));
      System.out.println("LARateCommisionSet get"+mLARateCommisionSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LASocialWNRateSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  private boolean check(){
	  if(!this.mOperate.equals("DELETE")){
	  LARateCommisionSchema tempLARateCommisionSchema1;
	  LARateCommisionSchema tempLARateCommisionSchema2;
	  ExeSQL tExeSQL=new ExeSQL();
	  StringBuffer tSB;
	  StringBuffer tSB1;
	  String tJudgePremType="04,05";
	  String tJudgeSQL ="select * from lmriskapp where taxoptimal ='Y' with ur ";
	  LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
	  LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
	  tLMRiskAppSet = tLMRiskAppDB.executeQuery(tJudgeSQL);
	  
	  String tResult;
	  for(int i=1;i<=this.mLARateCommisionSet.size();i++){
		  tempLARateCommisionSchema1=this.mLARateCommisionSet.get(i);
		  for (int k = 1; k <= tLMRiskAppSet.size(); k++)
		  {
			  LMRiskAppSchema tLMRiskAppSchema = new LMRiskAppSchema();
			  tLMRiskAppSchema = tLMRiskAppSet.get(k);
			  if(tLMRiskAppSchema.getRiskCode().equals(tempLARateCommisionSchema1.getRiskCode()))
			  {
				  //险种为税优只能录年缴的缴费比例
				  if(!"12".equals(tempLARateCommisionSchema1.getPayIntv()))
				  {
					  System.out.println("...........erro1");
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行的险种为税优险种时，缴费方式只能取年缴的提奖比例！";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  //税优险种产品的保费类型只能录帐户保费；风险保费
				  if(tJudgePremType.indexOf(tempLARateCommisionSchema1.getF01()) == -1)
				  {
					  System.out.println("...........erro1");
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行的险种为税优险种时，保费类型只能为：04-帐户保费，05-风险保费";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
				  
			  }
		  }
		  for(int j=i+1;j<=this.mLARateCommisionSet.size();j++){
			  tempLARateCommisionSchema2=this.mLARateCommisionSet.get(j);
			  if(tempLARateCommisionSchema1.getRiskCode()
					  .equals(tempLARateCommisionSchema2.getRiskCode())
					  &&tempLARateCommisionSchema1.getCurYear()
					  ==tempLARateCommisionSchema2.getCurYear()
					  &&tempLARateCommisionSchema1.getPayIntv()
					  .equals(tempLARateCommisionSchema2.getPayIntv())
					   &&tempLARateCommisionSchema1.getF01()
					  .equals(tempLARateCommisionSchema2.getF01())
					  &&tempLARateCommisionSchema1.getF03()
					  ==tempLARateCommisionSchema2.getF03()
					  &&tempLARateCommisionSchema1.getF04()
					  ==tempLARateCommisionSchema2.getF04()
					  &&tempLARateCommisionSchema1.getF05()
					  ==tempLARateCommisionSchema2.getF05()
					  &&tempLARateCommisionSchema1.getManageCom()
					  .equals(tempLARateCommisionSchema2.getManageCom())
					  &&(tempLARateCommisionSchema1.getYear()==tempLARateCommisionSchema2.getYear())){
				  System.out.println("...........erro1");
				  CError tError = new CError();
			      tError.moduleName = "LASocialRateSetBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
			  if(tempLARateCommisionSchema1.getRiskCode()
					  .equals(tempLARateCommisionSchema2.getRiskCode())
					  &&tempLARateCommisionSchema1.getCurYear()
					  ==tempLARateCommisionSchema2.getCurYear()
					  &&tempLARateCommisionSchema1.getPayIntv()
					  .equals(tempLARateCommisionSchema2.getPayIntv())
					   &&tempLARateCommisionSchema1.getF01()
					  .equals(tempLARateCommisionSchema2.getF01())
					  &&tempLARateCommisionSchema1.getManageCom()
					  .equals(tempLARateCommisionSchema2.getManageCom()))
			  {
				  //添加缴费年期区间重复的判断 --add by wrx
				  if(tempLARateCommisionSchema2.getF03()<tempLARateCommisionSchema1.getF04()&&
						  tempLARateCommisionSchema1.getF03()<tempLARateCommisionSchema2.getF04()){
					  System.out.println("...........erro2");
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行缴费年期区间重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
		  }
		  //添加缴费年期区间重复的判断 --copy by wrx
		  tSB1=new StringBuffer();
		  tSB1=tSB1.append("select f03,f04,idx from laratecommision where riskcode='")
		  .append(tempLARateCommisionSchema1.getRiskCode())
		  .append("' and curyear=")
		  .append(tempLARateCommisionSchema1.getCurYear())
		  .append(" and payintv='")
		  .append(tempLARateCommisionSchema1.getPayIntv())
		  .append("' and f01= '")
		  .append(tempLARateCommisionSchema1.getF01())
		  .append("' and f05='")
		  .append(tempLARateCommisionSchema1.getF05())
		  .append("' and managecom = '")
		  .append(tempLARateCommisionSchema1.getManageCom())
		  .append("'")
		  .append(" and branchtype='")
		  .append(tempLARateCommisionSchema1.getBranchType())
		  .append("'")
		  .append(" and branchtype2='")
		  .append(tempLARateCommisionSchema1.getBranchType2())
		  .append("'")
		  .append(" and agentcom is null")
		  .append(" and year = ")
		  .append(tempLARateCommisionSchema1.getYear())
		  .append(" and versiontype='")
		  .append(tempLARateCommisionSchema1.getVersionType())
		  .append("'");
		  SSRS tpayYearsSSRS = new SSRS();
		  tpayYearsSSRS=tExeSQL.execSQL(tSB1.toString());
		  
		  if(this.mOperate.equals("INSERT")){
			  tSB=new StringBuffer();
			  tSB=tSB.append("select distinct 1 from laratecommision where riskcode='")
			  .append(tempLARateCommisionSchema1.getRiskCode())
			  .append("' and curyear=")
			  .append(tempLARateCommisionSchema1.getCurYear())
			  .append(" and payintv='")
			  .append(tempLARateCommisionSchema1.getPayIntv())
			  .append("' and f01= '")
			  .append(tempLARateCommisionSchema1.getF01())
			  .append("' and f03=")
			  .append(tempLARateCommisionSchema1.getF03())
			  .append(" and f04='")
			  .append(tempLARateCommisionSchema1.getF04())
			  .append("' and f05='")
			  .append(tempLARateCommisionSchema1.getF05())
			  .append("' and managecom = '")
			  .append(tempLARateCommisionSchema1.getManageCom())
			  .append("'")
			  .append(" and branchtype='")
			  .append(tempLARateCommisionSchema1.getBranchType())
			  .append("'")
			  .append(" and branchtype2='")
			  .append(tempLARateCommisionSchema1.getBranchType2())
			  .append("'")
			  .append(" and agentcom is null")
			  .append(" and year = ")
			  .append(tempLARateCommisionSchema1.getYear())
			  .append(" and versiontype='")
			  .append(tempLARateCommisionSchema1.getVersionType())
			  .append("'");;
			  tResult=tExeSQL.getOneValue(tSB.toString());
			  if(tResult!=null&&tResult.equals("1")){
				  System.out.println("...........erro2");
				  CError tError = new CError();
			      tError.moduleName = "LASocialRateSetBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
			  }
			  for(int k=1;k<=tpayYearsSSRS.getMaxRow();k++){
				  if(Integer.parseInt(tpayYearsSSRS.GetText(i,k))<tempLARateCommisionSchema1.getF04()&&
						  tempLARateCommisionSchema1.getF03()<Integer.parseInt(tpayYearsSSRS.GetText(i,k+1))){
					  System.out.println("...........erro2");
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据与数据库中已有数据缴费区间重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;  
				  }
			  }
		  }
		  if(this.mOperate.equals("UPDATE")){
			  SSRS tSSRS=new SSRS();
			  tSB=new StringBuffer();
			  tSB=tSB.append("select distinct idx from laratecommision where riskcode='")
			  .append(tempLARateCommisionSchema1.getRiskCode())
			  .append("' and curyear=")
			  .append(tempLARateCommisionSchema1.getCurYear())
			  .append(" and payintv='")
			  .append(tempLARateCommisionSchema1.getPayIntv())
			  .append("' and f01= '")
			  .append(tempLARateCommisionSchema1.getF01())
			  .append("' and f03=")
			  .append(tempLARateCommisionSchema1.getF03())
			  .append(" and f04='")
			  .append(tempLARateCommisionSchema1.getF04())
			  .append("' and f05='")
			  .append(tempLARateCommisionSchema1.getF05())
			  .append("' and managecom = '")
			  .append(tempLARateCommisionSchema1.getManageCom())
			  .append("'")
			  .append(" and branchtype='")
			  .append(tempLARateCommisionSchema1.getBranchType())
			  .append("'")
			  .append(" and branchtype2='")
			  .append(tempLARateCommisionSchema1.getBranchType2())
			  .append("'")
			  .append(" and agentcom is null")
			  .append(" and year = ")
			  .append(tempLARateCommisionSchema1.getYear())			  
			  .append(" and versiontype='")
			  .append(tempLARateCommisionSchema1.getVersionType())
			  .append("'");
			  tSSRS=tExeSQL.execSQL(tSB.toString());
			  for(int m=1;m<=tSSRS.getMaxRow();m++){
				  System.out.println("...........tSSRS.GetText(m,1)"+tSSRS.GetText(m,1));
				  if(tempLARateCommisionSchema1.getIdx()!=Integer.parseInt(tSSRS.GetText(m,1))){
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
			  for(int k=1;k<=tpayYearsSSRS.getMaxRow();k++){
				  if(Integer.parseInt(tpayYearsSSRS.GetText(k,1))<tempLARateCommisionSchema1.getF04()&&
						  tempLARateCommisionSchema1.getF03()<Integer.parseInt(tpayYearsSSRS.GetText(k,2))&&
						  tempLARateCommisionSchema1.getIdx()!=Integer.parseInt(tpayYearsSSRS.GetText(k,3))){
					  System.out.println("...........erro2");
					  CError tError = new CError();
				      tError.moduleName = "LASocialRateSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据与数据库中已有数据缴费区间重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;  
				  }
			  }
		  }
	  }
	 }
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LASocialWNRateSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
        System.out.println("Begin LASocialWNRateSetBL.dealData.........1"+mLARateCommisionSet.size());
        LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
        LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          System.out.println("Begin LASocialWNRateSetBL.dealData.........2");
          LARateCommisionSchema tLARateCommisionSchemaold = new
              LARateCommisionSchema();
          LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
          LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
           System.out.println("Begin LASocialWNRateSetBL.dealData.........3");
            System.out.println("++++++++"+mLARateCommisionSet.get(i).getRiskCode());
          System.out.println("++++++++"+mLARateCommisionSet.get(i).getIdx());


          tLARateCommisionDB.setIdx(mLARateCommisionSet.get(i).getIdx());
//          tLARateCommisionDB.setRiskCode(mLARateCommisionSet.get(i).getRiskCode());
          tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
          tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
          //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
          tLARateCommisionSchemanew.setMakeDate(tLARateCommisionSchemaold.
                                                getMakeDate());
          tLARateCommisionSchemanew.setF01(mLARateCommisionSet.get(i).
                  getF01());
          tLARateCommisionSchemanew.setMakeTime(tLARateCommisionSchemaold.
                                                getMakeTime());
          tLARateCommisionSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionSchemanew.setModifyTime(CurrentTime);
//          tLARateCommisionSchemanew.setVersionType("11");
          tLARateCommisionSet.add(tLARateCommisionSchemanew);
          LARateCommisionBSchema tLARateCommisionBSchema = new
              LARateCommisionBSchema();
          ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
          //获取最大的ID号
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select int(max(idx)) from laratecommisionb order by 1 desc ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
            //System.out.println(tMaxIdx);
          }
          tMaxIdx++;
          tLARateCommisionBSchema.setIdx(tMaxIdx);
          if(i>1){
            tLARateCommisionBSchema.setIdx(tLARateCommisionBSet.get(i - 1).
                                           getIdx() + 1);
          }
          tLARateCommisionBSchema.setEdorNo(tLARateCommisionSchemaold.getIdx()+"");
          if(mOperate.equals("UPDATE"))
            tLARateCommisionBSchema.setEdorType("02");
          else
            tLARateCommisionBSchema.setEdorType("01");
          tLARateCommisionBSet.add(tLARateCommisionBSchema);

        }
        map.put(tLARateCommisionSet, mOperate);
        map.put(tLARateCommisionBSet, "INSERT");
      }
      else {

    	  LARateCommisionSet tLARateCommisionSet = new
          LARateCommisionSet();
  for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
      LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
      tLARateCommisionSchemanew = mLARateCommisionSet.get(i);

      String agentSQL =
      "select idx from laratecommision where riskcode='"+tLARateCommisionSchemanew.getRiskCode()+"'"
      +" and managecom='"+tLARateCommisionSchemanew.getManageCom()+"'"
      +" and curyear="+tLARateCommisionSchemanew.getCurYear()+""
      +" and payintv='"+tLARateCommisionSchemanew.getPayIntv()+"'"
      +" and f03="+tLARateCommisionSchemanew.getF03()+""
      +" and f04="+tLARateCommisionSchemanew.getF04()+""
      +" and f01='"+tLARateCommisionSchemanew.getF01()+"'"
      +" and branchtype='"+tLARateCommisionSchemanew.getBranchType()+"'"
      +" and branchtype2='"+tLARateCommisionSchemanew.getBranchType2()+"'";

       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(agentSQL);
       if(tSSRS.getMaxRow()>0)
       {
               // @@错误处理
       this.mErrors.copyAllErrors(tSSRS.mErrors);
       CError tError = new CError();
       tError.moduleName = "LASocialWNRateSetBL";
       tError.functionName = "submitDat";
       tError.errorMessage = "已经保存了相同险种的提奖比例，请重新录入!";
       this.mErrors.addOneError(tError);
       return false;
       }

      tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
tLARateCommisionSchemanew.setMakeDate(CurrentDate);
tLARateCommisionSchemanew.setMakeTime(CurrentTime);
tLARateCommisionSchemanew.setModifyDate(CurrentDate);
tLARateCommisionSchemanew.setModifyTime(CurrentTime);
//tLARateCommisionSchemanew.setVersionType("11");
tLARateCommisionSet.add(tLARateCommisionSchemanew);

  }
  map.put(tLARateCommisionSet, mOperate);
}
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LASocialWNRateSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LASocialWNRateSetBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LASocialWNRateSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
      System.out.println(
          "end  LASocialWNRateSetBL.prepareOutputData.........");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LASocialWNRateSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
