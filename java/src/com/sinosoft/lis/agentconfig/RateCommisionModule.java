package com.sinosoft.lis.agentconfig;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class RateCommisionModule
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public RateCommisionModule()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	System.out.println("................DimissionReportBL.java begin dealData()");
//        ExeSQL tExeSQL = new ExeSQL();
//        System.out.println("BL->dealDate()");
//        System.out.println(mSql);
//        System.out.println(mOutXmlPath);
//        SSRS tSSRS = tExeSQL.execSQL(mSql);
//
//        if(tExeSQL.mErrors.needDealError())
//        {
//            System.out.println(tExeSQL.mErrors.getErrContent());
//
//            CError tError = new CError();
//            tError.moduleName = "RateCommisionBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "没有查询到需要下载的数据";
//            mErrors.addOneError(tError);
//            System.out.println(tError.errorMessage);
//            return false;
//        }
       String[][] mToExcel = new String[1][8];
       //String[] mToExcelTitle=new String[8];
        mToExcel[0][0] = "管理机构代码";
        mToExcel[0][1] = "险种编码";
        mToExcel[0][2] = "险种名称";
        mToExcel[0][3] = "保单年度";
        mToExcel[0][4] = "缴费方式";
        mToExcel[0][5] = "佣金比率";
        mToExcel[0][6] = "保险期间始期";
        mToExcel[0][7] = "保险期间止期";
//        mToExcelTitle[0]="管理机构代码";
//        mToExcelTitle[1]="险种编码";
//        mToExcelTitle[2]="险种名称";
//        mToExcelTitle[3]="保单年度";
//        mToExcelTitle[4]="缴费方式";
//        mToExcelTitle[5]="佣金比率";
//        mToExcelTitle[6]="保险期间始期";
//        mToExcelTitle[7]="保险期间止期";
//        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
//        {
//            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
//            {
//                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
//            }
//        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setLocked(true);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "RateCommisionBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "RateCommisionBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
    	RateCommisionModule tRateCommisionModule = new
    	RateCommisionModule();
    }
}
