package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateStandPremSet;
import com.sinosoft.lis.vschema.LARateStandPremBSet;
import com.sinosoft.lis.schema.LARateStandPremSchema;
import com.sinosoft.lis.schema.LARateStandPremBSchema;

import com.sinosoft.lis.db.LARateStandPremDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LAZTGroupStandPremBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author Elsa
 * @version 1.0
 */
public class LAZTGroupStandPremBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private int tMaxIdx = 0;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateStandPremSet mLARateStandPremSet = new LARateStandPremSet();
  private Reflections ref = new Reflections();


  public LAZTGroupStandPremBL() {

  }

	public static void main(String[] args)
	{
		LAZTGroupStandPremBL LAZTGroupStandPremBL = new LAZTGroupStandPremBL();
	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAZTGroupStandPremBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    mErrors.clearErrors();
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAZTGroupStandPremBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {

      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));

     this.mLARateStandPremSet.set( (LARateStandPremSet) cInputData.getObjectByObjectName("LARateStandPremSet",0));

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAZTGroupStandPremBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  private boolean check(){
	  LARateStandPremSchema tLARateStandPremSchema1 = new LARateStandPremSchema();
	  LARateStandPremSchema tLARateStandPremSchema2 = new LARateStandPremSchema();	  
	  if(!this.mOperate.equals("DELETE")){
		  for(int i=1;i<=this.mLARateStandPremSet.size();i++)
	      {
			  tLARateStandPremSchema1=  mLARateStandPremSet.get(i);
			  for(int j=i+1;j<=this.mLARateStandPremSet.size();j++)
		      {
				 tLARateStandPremSchema2=mLARateStandPremSet.get(j);
			   if(tLARateStandPremSchema1.getRiskCode().equals(tLARateStandPremSchema2.getRiskCode())
				 &&tLARateStandPremSchema1.getPayIntv().equals(tLARateStandPremSchema2.getPayIntv())
				 &&tLARateStandPremSchema1.getManageCom().equals(tLARateStandPremSchema2.getManageCom())
				 &&tLARateStandPremSchema1.getBranchType().equals(tLARateStandPremSchema2.getBranchType())
				 &&tLARateStandPremSchema1.getBranchType2().equals(tLARateStandPremSchema2.getBranchType2())
				){
				   if(tLARateStandPremSchema1.getF01().equals(tLARateStandPremSchema2.getF01())
					 &&tLARateStandPremSchema1.getF02().equals(tLARateStandPremSchema2.getF02())
				  ){
					  CError tError = new CError();
				      tError.moduleName = "LAZTGroupStandPremBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				   }
	                if((Integer.parseInt(tLARateStandPremSchema2.getF01())<Integer.parseInt(tLARateStandPremSchema1.getF02()))
	                        &&(Integer.parseInt(tLARateStandPremSchema1.getF01())<Integer.parseInt(tLARateStandPremSchema1.getF02()))		
	                      ){
	        				  CError tError = new CError();
	      			      tError.moduleName = "LAZTGroupStandPremBL";
	      			      tError.functionName = "check()";
	      			      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行保险年期区间重复。";
	      			      this.mErrors.clearErrors();
	      			      this.mErrors.addOneError(tError);
	      			      return false;  	
	                      }
					
				}  				  
		      }
			  
	      }
	  }
	  
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAZTGroupStandPremBL.dealData........."+mOperate);
    ExeSQL tExe = new ExeSQL();
//         String tSql =
//             "select int(max(edorno)) from laratestandpremb order by 1 desc ";
//         String strIdx = "";
//         strIdx = tExe.getOneValue(tSql);
//         if (strIdx == null || strIdx.trim().equals("")) {
//           tMaxIdx = 0;
//         }
//         else {
//           tMaxIdx = Integer.parseInt(strIdx);
//         }


    try {
      if (mOperate.equals("UPDATE") ) {
        LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
//        LARateStandPremBSet tLARateStandPremBSet = new LARateStandPremBSet();


        for (int i = 1; i <= mLARateStandPremSet.size(); i++) {

          LARateStandPremSchema tLARateStandPremSchemaold = new  LARateStandPremSchema();
          LARateStandPremSchema tLARateStandPremSchemanew = new   LARateStandPremSchema();
          LARateStandPremDB tLARateStandPremDB = new LARateStandPremDB();
          tLARateStandPremDB.setRiskCode(mLARateStandPremSet.get(i).getRiskCode());
          tLARateStandPremDB.setBranchType(mLARateStandPremSet.get(i).getBranchType());
          tLARateStandPremDB.setBranchType2(mLARateStandPremSet.get(i).getBranchType2());
          tLARateStandPremDB.setPayIntv(mLARateStandPremSet.get(i).getPayIntv());
          tLARateStandPremDB.setF01(mLARateStandPremSet.get(i).getF01());
          tLARateStandPremDB.setF02(mLARateStandPremSet.get(i).getF02());
          tLARateStandPremDB.setManageCom(mLARateStandPremSet.get(i).getManageCom());
          tLARateStandPremDB.setCalType(mLARateStandPremSet.get(i).getCalType());
          tLARateStandPremSchemaold = tLARateStandPremDB.query().get(1);



          tLARateStandPremSchemanew = mLARateStandPremSet.get(i);
          tLARateStandPremSchemanew.setOperator(mGlobalInput.Operator);
          tLARateStandPremSchemanew.setMakeDate(tLARateStandPremSchemaold.getMakeDate());
          tLARateStandPremSchemanew.setMakeTime(tLARateStandPremSchemaold.getMakeTime());
          tLARateStandPremSchemanew.setModifyDate(CurrentDate);
          tLARateStandPremSchemanew.setModifyTime(CurrentTime);
          tLARateStandPremSet.add(tLARateStandPremSchemanew);


//          LARateStandPremBSchema tLARateStandPremBSchema = new    LARateStandPremBSchema();
//          ref.transFields(tLARateStandPremBSchema, tLARateStandPremSchemaold);
          //获取最大的ID号
//          tMaxIdx=tMaxIdx+1;
//          tLARateStandPremBSchema.setEdorNo(String.valueOf(tMaxIdx));
//          if(mOperate.equals("UPDATE"))
//            tLARateStandPremBSchema.setEdorType("02");
//          else
//            tLARateStandPremBSchema.setEdorType("01");
//          tLARateStandPremBSchema.setStartDate(CurrentDate);
//          tLARateStandPremBSchema.setEndDate(CurrentDate);
//          tLARateStandPremBSet.add(tLARateStandPremBSchema);
        }
        map.put(mLARateStandPremSet,"DELETE");
        map.put(tLARateStandPremSet,"INSERT");
//        map.put(tLARateStandPremBSet, "INSERT");
      }
      else if ( mOperate.equals("DELETE"))
      {
       LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
//       LARateStandPremBSet tLARateStandPremBSet = new LARateStandPremBSet();
       for (int i = 1; i <= mLARateStandPremSet.size(); i++) {
         LARateStandPremSchema tLARateStandPremSchemaold = new
         LARateStandPremSchema();
         LARateStandPremSchema tLARateStandPremSchemanew = new
             LARateStandPremSchema();
         LARateStandPremDB tLARateStandPremDB = new LARateStandPremDB();


         tLARateStandPremDB.setRiskCode(mLARateStandPremSet.get(i).getRiskCode());
         tLARateStandPremSchemaold = tLARateStandPremDB.query().get(1);
         tLARateStandPremSchemanew = mLARateStandPremSet.get(i);
         tLARateStandPremSchemanew.setOperator(mGlobalInput.Operator);
         tLARateStandPremSchemanew.setMakeDate(tLARateStandPremSchemaold.
                                               getMakeDate());
         tLARateStandPremSchemanew.setMakeTime(tLARateStandPremSchemaold.
                                               getMakeTime());
         tLARateStandPremSchemanew.setModifyDate(CurrentDate);
         tLARateStandPremSchemanew.setModifyTime(CurrentTime);
         tLARateStandPremSet.add(tLARateStandPremSchemanew);


//         LARateStandPremBSchema tLARateStandPremBSchema = new
//             LARateStandPremBSchema();
//         ref.transFields(tLARateStandPremBSchema, tLARateStandPremSchemaold);
//
//         tMaxIdx=tMaxIdx+1;
//         tLARateStandPremBSchema.setEdorNo(String.valueOf(tMaxIdx));
//         if(mOperate.equals("UPDATE"))
//           tLARateStandPremBSchema.setEdorType("02");
//         else
//           tLARateStandPremBSchema.setEdorType("01");
//         tLARateStandPremBSchema.setStartDate(CurrentDate);
//         tLARateStandPremBSchema.setEndDate(CurrentDate);
//         tLARateStandPremBSet.add(tLARateStandPremBSchema);

       }

       map.put(tLARateStandPremSet,"DELETE");
//       map.put(tLARateStandPremBSet, "INSERT");

      }
      else {
        LARateStandPremSet tLARateStandPremSet = new
            LARateStandPremSet();
        for (int i = 1; i <= mLARateStandPremSet.size(); i++) {
          LARateStandPremSchema tLARateStandPremSchemanew = new
              LARateStandPremSchema();
          tLARateStandPremSchemanew = mLARateStandPremSet.get(i);

          String agentSQL =
          "select '1' from laratestandprem "
          +" where riskcode='"+tLARateStandPremSchemanew.getRiskCode()+"'"
          +" and managecom='"+tLARateStandPremSchemanew.getManageCom()+"'"
          +" and payintv='"+tLARateStandPremSchemanew.getPayIntv()+"'"
          +" and f01='"+tLARateStandPremSchemanew.getF01()+"'"
          +" and f02='"+tLARateStandPremSchemanew.getF02()+"'"
          +" and branchtype='"+tLARateStandPremSchemanew.getBranchType()+"'"
          +" and branchtype2='"+tLARateStandPremSchemanew.getBranchType2()+"'"
          +" and caltype='"+tLARateStandPremSchemanew.getCalType()+"'";

          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(agentSQL);
          if(tSSRS.getMaxRow()>0)
          {
               // @@错误处理
            this.mErrors.copyAllErrors(tSSRS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAZTGroupStandPremBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "已经保存了相同险种的折标信息内容，请重新录入!";
            this.mErrors.addOneError(tError);
            return false;
           }
          
          String agentSQL1 =
              "select f01,f02 from laratestandprem "
              +" where riskcode='"+tLARateStandPremSchemanew.getRiskCode()+"'"
              +" and managecom='"+tLARateStandPremSchemanew.getManageCom()+"'"
              +" and payintv='"+tLARateStandPremSchemanew.getPayIntv()+"'"
              +" and branchtype='"+tLARateStandPremSchemanew.getBranchType()+"'"
              +" and branchtype2='"+tLARateStandPremSchemanew.getBranchType2()+"'";
              SSRS tSSRS1 = new SSRS();
              ExeSQL tExeSQL1 = new ExeSQL();
              tSSRS1 = tExeSQL1.execSQL(agentSQL1);
            
              if(tSSRS1.getMaxRow()>0)
              {
            	  for(int k=1;k<=tSSRS1.getMaxRow();k++){
            		  if((Integer.parseInt(tSSRS1.GetText(k,1))<Integer.parseInt(tLARateStandPremSchemanew.getF02()))
            		    &&(Integer.parseInt(tLARateStandPremSchemanew.getF01())<Integer.parseInt(tSSRS1.GetText(k,2)))		  
            		    ){
    					  CError tError = new CError();
    				      tError.moduleName = "LASocialRateSetBL";
    				      tError.functionName = "check()";
    				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据与数据库中已有数据保险区间重复。";
    				      this.mErrors.clearErrors();
    				      this.mErrors.addOneError(tError);
    				      return false;  
            		  }
            	  }

               }
          

          tLARateStandPremSchemanew.setOperator(mGlobalInput.
                                                Operator);
          tLARateStandPremSchemanew.setMakeDate(CurrentDate);
          tLARateStandPremSchemanew.setMakeTime(CurrentTime);
          tLARateStandPremSchemanew.setModifyDate(CurrentDate);
          tLARateStandPremSchemanew.setModifyTime(CurrentTime);

          tLARateStandPremSet.add(tLARateStandPremSchemanew);

        }
        map.put(tLARateStandPremSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAZTGroupStandPremBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAZTGroupStandPremBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
