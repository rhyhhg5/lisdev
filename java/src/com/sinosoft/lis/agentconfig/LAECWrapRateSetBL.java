package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAECWrapRateSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAECWrapRateSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();

  public LAECWrapRateSetBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAECWrapRateSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAECWrapRateSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAECWrapRateSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验
	  System.out.println("Begin LAECWrapRateSetBL.check.........1"+mLARateCommisionSet.size());
      if(!this.mOperate.equals("DELETE"))
      {
    	  LARateCommisionSchema tempLARateCommisionSchema1;
    	  LARateCommisionSchema tempLARateCommisionSchema2;
    	  ExeSQL tExeSQL=new ExeSQL();
    	  StringBuffer tSB;
    	  String tResult;
	      for(int i=1;i<=this.mLARateCommisionSet.size();i++)
          {
	    	  tempLARateCommisionSchema1=this.mLARateCommisionSet.get(i);
		      for(int j=i+1;j<=this.mLARateCommisionSet.size();j++){
		    	  tempLARateCommisionSchema2=this.mLARateCommisionSet.get(j);
			  if(tempLARateCommisionSchema1.getManageCom().equals(tempLARateCommisionSchema2.getManageCom())
					  &&tempLARateCommisionSchema1.getRiskCode().equals(tempLARateCommisionSchema2.getRiskCode())
					  &&tempLARateCommisionSchema1.getAgentCom().equals(tempLARateCommisionSchema2.getAgentCom())
					  ){
				           CError tError = new CError();
			               tError.moduleName = "LAECWrapRateSetBL";
			               tError.functionName = "check()";
			               tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
			               this.mErrors.clearErrors();
			               this.mErrors.addOneError(tError);
			               return false;
			           }
		     }
		  if(this.mOperate.equals("INSERT")){
			  LARateCommisionSchema tLARateCommisionSchema = new LARateCommisionSchema();
			  tLARateCommisionSchema = mLARateCommisionSet.get(i);
		      String sql = "select count(1) from laratecommision where managecom = '"+tLARateCommisionSchema.getManageCom()+"' and " 
			  +" riskcode = '"+tLARateCommisionSchema.getRiskCode()+"' and agentcom = '"+tLARateCommisionSchema.getAgentCom()+"' and " 
		      +" branchtype ='"+tLARateCommisionSchema.getBranchType()+"' and branchtype2= '"+tLARateCommisionSchema.getBranchType2()+"' ";
		  //ExeSQL tExeSQL = new ExeSQL();
		  String count = tExeSQL.getOneValue(sql);
		  System.out.println("..............count"+count);
		  if(count.compareTo("1")>=0)
		  {
			  CError tError = new CError();
		      tError.moduleName = "LAECWrapRateSetBL";
		      tError.functionName = "check";
		      tError.errorMessage = "数据库已经存在同样情况的比例,或者存在于有效起期和有效止期有交叉的记录,请执行修改或者删除!";
		      this.mErrors.addOneError(tError);
		      return false;
		  }
		}
		  if(this.mOperate.equals("UPDATE")){
			  LARateCommisionSchema tLARateCommisionSchema = new LARateCommisionSchema();
			  tLARateCommisionSchema = mLARateCommisionSet.get(i);
			  SSRS tSSRS=new SSRS();
			  String sql = " select count(1) from laratecommision "
		    	  +" where managecom='"+tLARateCommisionSchema.getManageCom()+"'" 
		    	  +" and riskcode='"+tLARateCommisionSchema.getRiskCode()+"'" 
		    	  +" and agentcom ='"+tLARateCommisionSchema.getAgentCom()+"' and branchtype ='"+tLARateCommisionSchema.getBranchType()+"'" 
		    	  +" and branchtype2='"+tLARateCommisionSchema.getBranchType2()+"'";
			  String count = tExeSQL.getOneValue(sql);
			  if(count.compareTo("0")==0)
			  {
				  CError tError = new CError();
			      tError.moduleName = "LAECWrapRateSetBL";
			      tError.functionName = "check";
			      tError.errorMessage = "只能修改手续费比例!";
			      this.mErrors.addOneError(tError);
			      return false;
			  }
			 
		  }
      }
    }
	return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAECWrapRateSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

      this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));
      System.out.println("LARateCommisionSet get"+mLARateCommisionSet.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAECWrapRateSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAECWrapRateSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") ) {
        LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
        	LARateCommisionSchema mLARateCommisionSchema=new LARateCommisionSchema();
        	mLARateCommisionSchema=mLARateCommisionSet.get(i);
        	LARateCommisionSchema tLARateCommisionSchemaold = new   LARateCommisionSchema();
        	LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
        	tLARateCommisionDB.setIdx(mLARateCommisionSchema.getIdx());
    	

        	tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
        	String tsql="update laratecommision set rate="+mLARateCommisionSchema.getRate()+"  "
        	+",  managecom='"+mLARateCommisionSchema.getManageCom()+"'"
        	+",modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"' "
        	+" where idx ='"+mLARateCommisionSchema.getIdx()+"'";
        	
        	map.put(tsql, mOperate);
        	
            LARateCommisionBSchema tLARateCommisionBSchema = new LARateCommisionBSchema();
            ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tLARateCommisionBSchema.setEdorNo(tEdorNo);
         
        	  tLARateCommisionBSchema.setEdorType("02");
         
          tLARateCommisionBSet.add(tLARateCommisionBSchema);

        }
        
        map.put(tLARateCommisionBSet, "INSERT");
      }
      
      else if( mOperate.equals("DELETE"))
      {
    	  LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
          LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
          for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          	LARateCommisionSchema mLARateCommisionSchema=new LARateCommisionSchema();
          	mLARateCommisionSchema=mLARateCommisionSet.get(i);
          	LARateCommisionSchema tLARateCommisionSchemaold = new   LARateCommisionSchema();
          	LARateCommisionSchema tLARateCommisionSchemanew = new   LARateCommisionSchema();
          	LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
          	tLARateCommisionDB.setIdx(mLARateCommisionSchema.getIdx());
          	          	

          	tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);          	
          	tLARateCommisionSet.add(tLARateCommisionSchemaold);
          	
              LARateCommisionBSchema tLARateCommisionBSchema = new LARateCommisionBSchema();
              ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
              String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
              tLARateCommisionBSchema.setEdorNo(tEdorNo);
          	  tLARateCommisionBSchema.setEdorType("01");
              tLARateCommisionBSet.add(tLARateCommisionBSchema);

          }
          map.put(tLARateCommisionSet, mOperate);
          map.put(tLARateCommisionBSet, "INSERT");  
      }
      
      
      else {
    	  LARateCommisionSet tLARateCommisionSet = new  LARateCommisionSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
        	LARateCommisionSchema tLARateCommisionSchemanew = new LARateCommisionSchema();
        	tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
        	tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
        	tLARateCommisionSchemanew.setMakeDate(CurrentDate);
        	tLARateCommisionSchemanew.setMakeTime(CurrentTime);
        	tLARateCommisionSchemanew.setModifyDate(CurrentDate);
        	tLARateCommisionSchemanew.setModifyTime(CurrentTime);
        	tLARateCommisionSet.add(tLARateCommisionSchemanew);
        }
        map.put(tLARateCommisionSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAECWrapRateSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAECWrapRateSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  public static void main(String[] args)
  {
  	LAECWrapRateSetBL tLAECWrapRateSetBL = new   LAECWrapRateSetBL();
  	String tNo = PubFun1.CreateMaxNo("ENo", 10); 
       System.out.println("LAXBTQueryUI"+tNo); 
  }
}
