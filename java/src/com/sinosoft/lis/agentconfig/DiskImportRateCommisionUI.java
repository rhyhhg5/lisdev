package com.sinosoft.lis.agentconfig;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportRateCommisionUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportRateCommisionBL mDiskImportRateCommisionBL = null;

    public DiskImportRateCommisionUI()
    {
        mDiskImportRateCommisionBL = new DiskImportRateCommisionBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mDiskImportRateCommisionBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportRateCommisionBL.mErrors);
            return false;
        }
        //
        if(mDiskImportRateCommisionBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportRateCommisionBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportRateCommisionBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mDiskImportRateCommisionBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
        DiskImportRateCommisionUI zDiskImportRateCommisionUI = new DiskImportRateCommisionUI();
    }
}
