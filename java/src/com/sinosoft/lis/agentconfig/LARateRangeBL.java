package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LaratecommisionSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LARateRangeBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();
  public LARateRangeBL()
  {

  }
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LARateRangeBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if (!check()) {
      return false;
    }

    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LARateRangeBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LARateRangeBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 添加校验
   * /
   * @return boolean
   */
  private boolean check()
  {
      //删除不校验
      if(mLARateCommisionSet==null || mLARateCommisionSet.size()<0)
      {
          BuildError("check","没有需要操作的数据!");
          return false;
      }
      else
      {
         for (int i = 1; i <= mLARateCommisionSet.size(); i++)
         {
             LARateCommisionSchema tLARateCommisionSchema = new
                     LARateCommisionSchema();
             tLARateCommisionSchema = mLARateCommisionSet.get(i);
             String tRiskCode = tLARateCommisionSchema.getRiskCode();
             double tRate = tLARateCommisionSchema.getRate();
             String tManageCom = tLARateCommisionSchema.getManageCom();
             String tF06 = tLARateCommisionSchema.getF06();
             int tIdx = tLARateCommisionSchema.getIdx();
             if (tRiskCode == null || tRiskCode.equals("")) {
                 BuildError("check", "险种编码不能为空!");
                 return false;
             }
             if (tManageCom == null || tManageCom.equals("")) {
                 BuildError("check", "管理机构不能为空!");
                 return false;
             }
             if (tF06 == null || tF06.equals("")) {
                 BuildError("check", "数据不完整!");
                 return false;
             }
             for (int k = 1; k <= mLARateCommisionSet.size(); k++)
             {
                 if(k != i)//同一条不需要校验
                 {
                     String tRiskCode1 = mLARateCommisionSet.get(k).getRiskCode();
                     String tManageCom1 = mLARateCommisionSet.get(k).getManageCom();
                     String tF06_1 = mLARateCommisionSet.get(k).getF06();
                     if (tRiskCode1 == null || tRiskCode1.equals("")) {
                         BuildError("check", "险种编码不能为空!");
                         return false;
                     }
                     if (tManageCom1 == null || tManageCom1.equals("")) {
                         BuildError("check", "管理机构不能为空!");
                         return false;
                     }
                     if (tF06_1 == null || tF06_1.equals("")) {
                         BuildError("check", "数据不完整!");
                         return false;
                     }
                    if(tManageCom1.equals("86")) //总公司，需要录入两条，最大比例和最小比例
                    {
                        if (tRiskCode.equals(tRiskCode1) &&
                            tManageCom.equals(tManageCom1) &&
                            tF06.equals(tF06_1)) {
                            BuildError("check", "同一公司不能录入相同的险种!");
                            return false;
                        }
                    }
                    else//分公司
                    {
                        if (tRiskCode.equals(tRiskCode1) &&
                            tManageCom.equals(tManageCom1) ) {
                            BuildError("check", "同一公司不能录入相同的险种!");
                            return false;
                        }

                    }
                 }
             }
         }
      }
      return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LaratecommisionSetBL.dealData........."+mOperate);
    try {
        if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {

            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);  //总公司需要备份两条，最大值和最小值
         //   String tEdorNoMax = PubFun1.CreateMaxNo("EdorNo", 20);
            System.out.println("Begin LaratecommisionSetBL.dealData.........1" +
                               mLARateCommisionSet.size());
            LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
            LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
            for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
                System.out.println(
                        "Begin LaratecommisionSetBL.dealData.........2");
                LARateCommisionSchema tLARateCommisionSchemaold = new
                        LARateCommisionSchema();
                LARateCommisionSchema tLARateCommisionSchemanew = new
                        LARateCommisionSchema();
                LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
                tLARateCommisionDB.setIdx(mLARateCommisionSet.get(i).getIdx());
                tLARateCommisionDB.setRiskCode(mLARateCommisionSet.get(i).
                                               getRiskCode());
                tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
                tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
                //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
                tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
                tLARateCommisionSchemanew.setMakeDate(tLARateCommisionSchemaold.
                        getMakeDate());
                tLARateCommisionSchemanew.setMakeTime(tLARateCommisionSchemaold.
                        getMakeTime());
                tLARateCommisionSchemanew.setModifyDate(CurrentDate);
                tLARateCommisionSchemanew.setModifyTime(CurrentTime);
                tLARateCommisionSet.add(tLARateCommisionSchemanew);
                LARateCommisionBSchema tLARateCommisionBSchema = new
                        LARateCommisionBSchema();
                ref.transFields(tLARateCommisionBSchema,
                                tLARateCommisionSchemaold);
                //获取最大的ID号
                String OldIdx=String.valueOf(tLARateCommisionSchemaold.getIdx());
                ExeSQL tExe = new ExeSQL();
                String tSql ="select int(max(idx)) from laratecommisionb where edorno='"
                             +OldIdx+"'  order by 1 desc ";//原来的表的idx在B表中的edorno，报表中一个edorno可对应多条idx
                String strIdx = "";
                int tMaxIdx = 0;
                strIdx = tExe.getOneValue(tSql);
                if (strIdx == null || strIdx.trim().equals("")) {
                  tMaxIdx = 0;
                }
                else {
                  tMaxIdx = Integer.parseInt(strIdx);
                  //System.out.println(tMaxIdx);
                }
                tMaxIdx++;
                tLARateCommisionBSchema.setIdx(tMaxIdx);
                tLARateCommisionBSchema.setEdorNo(tLARateCommisionSchemaold.getIdx()+"");

                if (mOperate.equals("UPDATE"))
                {
                    tLARateCommisionBSchema.setEdorType("02");
                }
                else
                {
                    tLARateCommisionBSchema.setEdorType("01");
                }
                tLARateCommisionBSet.add(tLARateCommisionBSchema);
            }

            map.put(tLARateCommisionSet, mOperate);
            map.put(tLARateCommisionBSet, "INSERT");
        }
        else
        {
            LARateCommisionSet tLARateCommisionSet = new
                    LARateCommisionSet();
            for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
                LARateCommisionSchema tLARateCommisionSchemanew = new
                        LARateCommisionSchema();


                tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
                String tRiskCode = tLARateCommisionSchemanew.getRiskCode();
                String tBranchType = tLARateCommisionSchemanew.getBranchType();
                String tBranchType2 = tLARateCommisionSchemanew.getBranchType2();
                double tRate = tLARateCommisionSchemanew.getRate();
                String tManageCom = tLARateCommisionSchemanew.getManageCom();
                String tF06 = tLARateCommisionSchemanew.getF06();
                LARateCommisionSet tLARateCommisionoldSet = new
                    LARateCommisionSet();
                LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();
                tLARateCommisionDB.setBranchType(tBranchType);
                tLARateCommisionDB.setBranchType2(tBranchType2);
                tLARateCommisionDB.setRiskCode(tRiskCode);
                tLARateCommisionDB.setManageCom(tManageCom);
                tLARateCommisionDB.setF06(tF06);
                tLARateCommisionoldSet=tLARateCommisionDB.query();
                if(tLARateCommisionoldSet!=null && tLARateCommisionoldSet.size()>0)
                {
                    BuildError("dealData","机构"+tManageCom+"的险种"+tRiskCode+"提奖比例已经存在，不能重复录入！");
                    return false;
                }
                tLARateCommisionSchemanew.setOperator(mGlobalInput.
                        Operator);
                //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
                tLARateCommisionSchemanew.setMakeDate(CurrentDate);
                tLARateCommisionSchemanew.setMakeTime(CurrentTime);
                tLARateCommisionSchemanew.setModifyDate(CurrentDate);
                tLARateCommisionSchemanew.setModifyTime(CurrentTime);
                tLARateCommisionSet.add(tLARateCommisionSchemanew);

            }
            map.put(tLARateCommisionSet, mOperate);
        }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
 private void BuildError(String tFun,String tMess)
 {
     CError tError = new CError();
     tError.moduleName = "LARateRangeBL";
     tError.functionName = tFun;
     tError.errorMessage = tMess;
     this.mErrors.addOneError(tError);

 }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LARateRangeBL prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LARateRangeBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
