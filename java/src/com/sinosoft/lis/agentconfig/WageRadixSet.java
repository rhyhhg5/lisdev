package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAWageRadixSchema;
import com.sinosoft.lis.vdb.LAWageRadixDBSet;
import com.sinosoft.lis.vschema.LAWageRadixSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class WageRadixSet
{
//错误处理类
    public static CErrors mErrors = new CErrors();
//业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();
    private LAWageRadixSet mLAWageRadixSet = new LAWageRadixSet();

    public WageRadixSet()
    {
    }

    public static void main(String[] args)
    {
        WageRadixSet WageRadixSet1 = new WageRadixSet();
    }

    public boolean saveData()
    {
        LAWageRadixSet tSet = new LAWageRadixSet();
        LAWageRadixDBSet tDBSet = new LAWageRadixDBSet();

        ExeSQL tExe = new ExeSQL();
        String tSql = "select max(idx) from lawageradix";
        String tIdx = "";
        int tMaxIdx = 0;

        tIdx = tExe.getOneValue(tSql);
        System.out.println("----Idx----" + tIdx);
        if (tIdx == null || tIdx.trim().equals(""))
        {
            tMaxIdx = 0;
            System.out.println("---here---" + tMaxIdx);
        }
        else
        {
            tMaxIdx = Integer.parseInt(tIdx.trim());
            System.out.println("----MaxIdx----" + tMaxIdx);
        }

        int tCount = this.mLAWageRadixSet.size();
        System.out.println("-----保存条数：" + tCount);

        tSet.clear();
        for (int i = 1; i <= tCount; i++)
        {
            tMaxIdx += 1;
            System.out.println("-----for---" + i + "----");
            LAWageRadixSchema tSch = new LAWageRadixSchema();
            tSch.setSchema(this.mLAWageRadixSet.get(i));
            tSch.setIdx(tMaxIdx);

            tSet.add(tSch);
        }

        tDBSet.set(tSet);
        if (!tDBSet.insert())
        {
            System.out.println("--Save--Error--" +
                               tDBSet.mErrors.getFirstError().toString().trim());
            dealError("saveData", tDBSet.mErrors.getFirstError().toString());
            return false;
        }
        return true;
    }

    public boolean updateData()
    {
        LAWageRadixDBSet tDBSet = new LAWageRadixDBSet();
        tDBSet.set(this.mLAWageRadixSet);
        if (!tDBSet.update())
        {
            dealError("updateD0ata", tDBSet.mErrors.getFirstError().toString());
            return false;
        }
        return true;
    }

    public boolean deleteData()
    {
        LAWageRadixDBSet tDBSet = new LAWageRadixDBSet();
        tDBSet.add(this.mLAWageRadixSet);
        if (!tDBSet.delete())
        {
            dealError("deleteData", tDBSet.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mLAWageRadixSet = (LAWageRadixSet) tInputData.
                                   getObjectByObjectName(
                                           "LAWageRadixSet", 0);
            this.mGlobalInput = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception ex)
        {
            dealError("getInputData", "传入数据失败！");
            return false;
        }

        int tCount = mLAWageRadixSet.size();
        System.out.println("-----InputData----set.count = " + tCount);
        if (tCount == 0)
        {
            dealError("getInputData", "传入mLAWageRadixSet为空！");
            return false;
        }
        return true;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "LAWageRadixSet";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

}
