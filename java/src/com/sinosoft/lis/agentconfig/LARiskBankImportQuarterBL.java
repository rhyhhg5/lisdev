package com.sinosoft.lis.agentconfig;

import java.io.File;

import com.sinosoft.lis.db.LADiscountDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LADiscountSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LADiscountSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LARiskBankImportQuarterBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LADiscountDiskBankImport.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
    LAComDB mLAComDB=new LAComDB();
	LAComSet mLAComSet=new LAComSet();
	
    private LADiscountSet mLADiscountSetFinal=new LADiscountSet();
    
    private LADiscountSet mLADiscountSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public LARiskBankImportQuarterBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        //System.out.println("begin import");
        //从磁盘导入数据
        LADiscountDiskImporter importer = new LADiscountDiskImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("DiskImportRateCommisionBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LADiscount")) {
            mLADiscountSet = (LADiscountSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LADiscount")) {
            if (mLADiscountSet == null) {
                mErrors.addOneError("导入佣金率信息失败！没有得到要导入的数据或者导入模版不是最新，请下载最新的模版。");
                return false;
            } else {
            	LDCodeDB tLDCodeDB=new LDCodeDB();
            	LDCodeSet tLDCodeSet=new LDCodeSet();
            
            	String tempSQL="select * from ldcode where codetype='bankriskcode'";
            	tLDCodeSet=tLDCodeDB.executeQuery(tempSQL);
            	tempSQL="select * from ldcom where (length(trim(comcode))=4 or length(trim(comcode))=8)";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LADiscountDB mTempLADiscountDB=new LADiscountDB();
            	//LARateCommisionSet mLARateCommisionSet=new LARateCommisionSet();
            	LADiscountSchema mTempLADiscountSchema=new LADiscountSchema();
            	LADiscountSchema mTempLADiscountSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLADiscountSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLADiscountSchema=mLADiscountSet.get(i);
                    if (mTempLADiscountSchema.getManageCom() == null ||
                    		mTempLADiscountSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    }
                    if (mTempLADiscountSchema.getManageCom().length()<this.mGlobalInput.ManageCom.length() 
                    		|| !mTempLADiscountSchema.getManageCom().substring(0,this.mGlobalInput.ManageCom.length())
                    		.equals(this.mGlobalInput.ManageCom)) {
                        this.merrorNum++;
                        this.merrorInfo.append("权限不够，设置该条提奖失败/");
                    }
                    if (mTempLADiscountSchema.getRiskCode() == null ||
                    		mTempLADiscountSchema.getRiskCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种编码不能为空/");
                    } 
//                    if (mTempLADiscountSchema.getCurYear() < 0 ) {
//                        this.merrorNum++;
//                        this.merrorInfo.append("保单年度不能为空/");
//                    } 
                    if (mTempLADiscountSchema.getPayIntv() == null ||
                    		mTempLADiscountSchema.getPayIntv().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费方式不能为空/");
                    } 
                    if (mTempLADiscountSchema == null ||
                    		mTempLADiscountSchema.getInsureYear().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("保费类型不能为空/");
                    } 
                    if (mTempLADiscountSchema.getCode4() == null ||
                    		mTempLADiscountSchema.getCode4().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("保险期间不能为空/");
                    } 
                    if (mTempLADiscountSchema.getRate() < 0 
                    		||mTempLADiscountSchema.getRate()>1||mTempLADiscountSchema.getRate()==0) {
                    	System.out.println("DiskImportRateCommisionBL.java: getRate:"+mLADiscountSet.get(i).getRate());
                        this.merrorNum++;
                        this.merrorInfo.append("佣金比率应介于０与１之间且不能为空/");
                    } 
                    if (mTempLADiscountSchema.getCode1()< 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("最低缴费年期不能为空/");
                    }
                    if (mTempLADiscountSchema.getCode2()< 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费年期小于不能为空或0/");
                    }
                    if (mTempLADiscountSchema.getCode1() >=0
                    		&&mTempLADiscountSchema.getCode2()>=0 ) {
                    	double intStartDate =mTempLADiscountSchema.getCode1();
                    	double intEndDate =mTempLADiscountSchema.getCode2();
                        if (intStartDate > intEndDate) {
                            this.merrorNum++;
                            this.merrorInfo.append("最低缴费年期不能大于缴费年期小于/");
                        }
                    }
                    if(mTempLADiscountSchema.getManageCom()!=null
                    		&&!mTempLADiscountSchema.getManageCom().equals("")
                    		&&!mTempLADiscountSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLADiscountSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }
                    if(mTempLADiscountSchema.getManageCom()!=null
                    		&&!mTempLADiscountSchema.getManageCom().equals("")
                    		&&mTempLADiscountSchema.getCode6()!=null
                    		&&!mTempLADiscountSchema.getCode6().equals("")){
                    	int tempFlag3=0;
                    	tempSQL="select * from lacom where managecom like '"+mTempLADiscountSchema.getManageCom()+"%'";
                    	mLAComSet=mLAComDB.executeQuery(tempSQL);
                        System.out.println(mLAComSet.size());
                        for(int k=1;k<=mLAComSet.size();k++){
                        	if(mLAComSet.get(k).getAgentCom()
                        			.equals(mTempLADiscountSchema.getCode6())){
                        		tempFlag3=1;
                        		break;
                        	}
                        }
                        if(tempFlag3==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构/"+mTempLADiscountSchema.getManageCom()+"下不存在代理机构"+mTempLADiscountSchema.getCode6());
                        }
                    }
                    int tempFlag2=0;
                    if(mTempLADiscountSchema.getRiskCode()!=null
                    		&&!mTempLADiscountSchema.getRiskCode().equals("")){
                    	for(int n=1;n<=tLDCodeSet.size();n++){
                    		System.out.println("...............tLDCodeSet.get(n).getCode()"
                    				+tLDCodeSet.get(n).getCode());
                    		System.out.println("...............	()"
                    					+mTempLADiscountSchema.getRiskCode());
                    		if(tLDCodeSet.get(n).getCode()
                        			.equals(mTempLADiscountSchema.getRiskCode())){
                        		
                        		tempFlag2=1;
                        		break;
                        	}
                        }
                        if(tempFlag2==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("该险种不存在或该险种不为银代险种/");
                        }
                    }
                    if(this.merrorNum==0){
//                    	for(int j = i+1; j <= mLADiscountSet.size(); j++){
//                        	mTempLADiscountSchema2=mLADiscountSet.get(j);
//                        	if(mTempLADiscountSchema.getManageCom()
//                        			.equals(mTempLADiscountSchema2.getManageCom())
//                        			&&mTempLADiscountSchema.getRiskCode()
//                        			.equals(mTempLADiscountSchema2.getRiskCode())
//                        			&&mTempLADiscountSchema.getCurYear()
//                        			==mTempLADiscountSchema2.getCurYear()
//                        			&&mTempLADiscountSchema.getPayIntv()
//                        			.equals(mTempLADiscountSchema2.getPayIntv())
//                        			&&mTempLADiscountSchema.getF03()
//                        			==mTempLADiscountSchema2.getF03()
//                        			&&mTempLADiscountSchema.getF04()
//                        			==mTempLADiscountSchema2.getF04()){
//                        		this.merrorNum++;
//                                this.merrorInfo.append("与第"+j+"行数据重复/");
//                        	}
//                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLADiscountSchema,mTempLADiscountDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LADiscountSchema mLADiscountSchema,LADiscountDB mLADiscountDB){

	LADiscountSet tempLADiscountSet=new LADiscountSet();
	String tempSql="select * from ladiscount where managecom='"
		+mLADiscountSchema.getManageCom()+"' and code6 ='" 
		+mLADiscountSchema.getCode6()+"' and riskcode='"
		+mLADiscountSchema.getRiskCode()+"' and payintv='"
		+mLADiscountSchema.getPayIntv()+"' and insureyear='"
		+mLADiscountSchema.getInsureYear()+ "' and code1="
		+mLADiscountSchema.getCode1()+" and code2="
		+mLADiscountSchema.getCode2()+" and code4='"
		+mLADiscountSchema.getCode4()+"' and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")
		+"' and discounttype = '21'  and code5 = '1'"
		;
	System.out.println("checkExist:tempsql"+tempSql);
	tempLADiscountSet=mLADiscountDB.executeQuery(tempSql);
	if(tempLADiscountSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LADiscountSchema tempLADiscountSchema=new LADiscountSchema();
		tempLADiscountSchema.setManageCom(mLADiscountSchema.getManageCom());
		tempLADiscountSchema.setAgentGrade(mLADiscountSchema.getAgentGrade());
		tempLADiscountSchema.setCode6(mLADiscountSchema.getCode6());//代理机构
		tempLADiscountSchema.setRiskCode(mLADiscountSchema.getRiskCode());
		tempLADiscountSchema.setRate(mLADiscountSchema.getRate());
		tempLADiscountSchema.setInsureYear(mLADiscountSchema.getInsureYear());
		tempLADiscountSchema.setCode1(mLADiscountSchema.getCode1());
		tempLADiscountSchema.setCode2(mLADiscountSchema.getCode2());
		tempLADiscountSchema.setCode4(mLADiscountSchema.getCode4());
		tempLADiscountSchema.setPayIntv(mLADiscountSchema.getPayIntv());
		//tempLARateCommisionSchema=mLARateCommisionSchema;
		mLADiscountSetFinal.add(tempLADiscountSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
   	      ExeSQL tExe = new ExeSQL();
	      String tSql = "";
        if (diskimporttype.equals("LADiscount")) {
        	System.out.println("prepareData:doing");
            importPersons=mLADiscountSetFinal.size();
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLADiscountSetFinal!=null&&mLADiscountSetFinal.size()>0){
             	//importPersons = mLARateCommisionSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLADiscountSetFinal.size(); i++) {
            		tSql =  "select int(max(idx)) from ladiscount ";
      	            String strIdx = "";
    	            int tMaxIdx = 0;
    	            strIdx = tExe.getOneValue(tSql);
    	            if (strIdx == null || strIdx.trim().equals("")) {
    	        	   tMaxIdx = 0;
    	            }
    	            else {
    	        	   tMaxIdx = Integer.parseInt(strIdx);
    	               //System.out.println(tMaxIdx);
    	            }
    	            tMaxIdx += i;
		          	System.out.println("preparedata : managecom"+mLADiscountSetFinal.get(i).getManageCom());
		          	String branchtype=(String) mTransferData.getValueByName("branchtype");
		          	//System.out.println("BL:branchtype  "+branchtype);
		          	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
		          	//System.out.println("BL:branchtype2  "+branchtype2);
		          	mLADiscountSetFinal.get(i).setOperator(mGlobalInput.Operator);
		          	mLADiscountSetFinal.get(i).setIdx(tMaxIdx);
		          	mLADiscountSetFinal.get(i).setBranchType(branchtype);
		          	mLADiscountSetFinal.get(i).setBranchType2(branchtype2); 
		          	mLADiscountSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
		          	mLADiscountSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
		          	mLADiscountSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
		          	mLADiscountSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
		          	mLADiscountSetFinal.get(i).setCode5("1");//保单年度默认为1 只提取首期。
		          	mLADiscountSetFinal.get(i).setDiscountType("21");//员工制银行代理标识
		          	mmap.put(mLADiscountSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportRateCommisionBL d = new DiskImportRateCommisionBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
