package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportBRCDSUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportBRCDSBL mLADiskImportBRCDSBL = null;

    public LADiskImportBRCDSUI()
    {
    	mLADiskImportBRCDSBL = new LADiskImportBRCDSBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportBRCDSBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportBRCDSBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportBRCDSBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportBRCDSBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportBRCDSBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportBRCDSBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportBRCDSUI zLADiskImportBRCDSUI = new LADiskImportBRCDSUI();
    }
}
