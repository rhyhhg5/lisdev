package com.sinosoft.lis.agentconfig;


import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SchemaSet;

/**
 * <p>Title: 销售代理人相关信息磁盘导入类</p>
 * <p>Description: 只会从一个sheet中导入，导入数据分别导入不同的表</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author luomin
 * @version 1.0
 */

public class LAGetOtherChargeDiskImporter
{
    public CErrors mErrrors = new CErrors();

    private String tableName="";
    /** 磁盘导入临时表的schema */
    private  String schemaClassName ="";

    /** 磁盘导入临时表的set */
    private  String schemaSetClassName ="";

    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;

    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */

    public LAGetOtherChargeDiskImporter(String fileName, String configFileName,
            String sheetName)
    {
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    public void setTableName(String tablename)
    {
         this.tableName=tablename;
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport()
    {
        schemaClassName="com.sinosoft.lis.schema."+this.tableName+"Schema";
        schemaSetClassName="com.sinosoft.lis.vschema."+this.tableName+"Set";
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        //System.out.println("seted schema and set");
        if (!importer.doImport())
        {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet()
    {
        return importer.getSchemaSet();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
//    public static void main(String[] args)
//    {
//
//    }
}
