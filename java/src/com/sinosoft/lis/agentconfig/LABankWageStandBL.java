package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LABankIndexRadixBSet;
import com.sinosoft.lis.vschema.LABankIndexRadixSet;
import com.sinosoft.lis.schema.LABankIndexRadixBSchema;
import com.sinosoft.lis.schema.LABankIndexRadixSchema;
import com.sinosoft.lis.db.LABankIndexRadixDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author wangrx 2015-9-23
 * @version 1.0
 */
public class LABankWageStandBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String tFlag = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
//  private MMap newMap = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LABankIndexRadixSet mLABankIndexRadixSet = new LABankIndexRadixSet();
  private Reflections ref = new Reflections();
  public LABankWageStandBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LABankWageStandBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABankWageStandBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LABankWageStandBL.check.........1"+mLABankIndexRadixSet.size());	 
	      LABankIndexRadixSchema tempLABankIndexRadixSchema1; 
	      LABankIndexRadixSchema tempLABankIndexRadixSchema2;
//	      LAActiveChargeSchema tLAActiveChargeSchemaNew = new LAActiveChargeSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
//		  LAActiveChargeDB tLAActiveChargeDB = new LAActiveChargeDB();
		  for(int i=1;i<=this.mLABankIndexRadixSet.size();i++){
			  tempLABankIndexRadixSchema1=this.mLABankIndexRadixSet.get(i);
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLABankIndexRadixSet.size();j++){
					  tempLABankIndexRadixSchema2 = this.mLABankIndexRadixSet.get(j);
					  if(tempLABankIndexRadixSchema1.getManageCom().equals(tempLABankIndexRadixSchema2.getManageCom())
					  ){
							 BuildError("dealData","第"+i+","+j+"行管理机构相同！");
		                     return false; 
					  }
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LABankIndexRadix where  managecom='")
				  .append(tempLABankIndexRadixSchema1.getManageCom())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LAAssessMixFYCBL";
				      tError.functionName = "check()";				
				      tError.errorMessage = "第"+i+"行当前管理机构的标准已录入";				      
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
//			 if(mOperate.equals("UPDATE")){
////				 tLAActiveChargeDB.setIdx(tempLAActiveChargeSchema1.getIdx());
//				 tLAActiveChargeSchemaNew = tLAActiveChargeDB.query().get(1);
//				 if(!tempLAActiveChargeSchema1.getManageCom().equals(tLAWageRadixAllSchemaNew.getManageCom())||
//					!tempLAActiveChargeSchema1.getAgentGrade().equals(tLAWageRadixAllSchemaNew.getAgentGrade())
//				 ){
//					 BuildError("dealData","第"+i+"行管理机构,职级代码,不能修改！");
//                     return false;
//				 }
//			 }
		  
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABankWageStandBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLABankIndexRadixSet.set( (LABankIndexRadixSet) cInputData.getObjectByObjectName("LABankIndexRadixSet",0));
      System.out.println("LABankIndexRadixSet get"+mLABankIndexRadixSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankWageStandBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LABankWageStandBL.dealData........."+mOperate);
    try {
    	LABankIndexRadixSchema  tLABankIndexRadixSchemaNew = new  LABankIndexRadixSchema();
    	LABankIndexRadixSchema  tLABankIndexRadixSchemaOld = new  LABankIndexRadixSchema();
    	LABankIndexRadixDB tLABankIndexRadixDB =new  LABankIndexRadixDB();
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LABankWageStandBL.dealData.........INSERT"+mLABankIndexRadixSet.size());
	        LABankIndexRadixSet tLABankIndexRadixSet = new LABankIndexRadixSet();
	        for (int i = 1; i <= mLABankIndexRadixSet.size(); i++) {
	          ExeSQL tExe = new ExeSQL();
	          String tSql =
	              "select max(int(idx)) from labankindexradix";
	          String strIdx = "";
	          int tMaxIdx = 0;
	          strIdx = tExe.getOneValue(tSql);
	          if (strIdx == null || strIdx.trim().equals("")) {
	            tMaxIdx = 0;
	          }
	          else {
	            tMaxIdx = Integer.parseInt(strIdx);
	            //System.out.println(tMaxIdx);
	          }
	  	        tMaxIdx += i;
	        	tLABankIndexRadixSchemaNew = mLABankIndexRadixSet.get(i);	
	        	tLABankIndexRadixSchemaNew.setIdx(tMaxIdx+"");
	        	tLABankIndexRadixSchemaNew.setMakeDate(CurrentDate);
	        	tLABankIndexRadixSchemaNew.setMakeTime(CurrentTime);
	        	tLABankIndexRadixSchemaNew.setModifyDate(CurrentDate);
	        	tLABankIndexRadixSchemaNew.setModifyTime(CurrentTime);
	        	tLABankIndexRadixSchemaNew.setOperator(mGlobalInput.Operator); 
	        	tLABankIndexRadixSet.add(tLABankIndexRadixSchemaNew);
      }
        	map.put(tLABankIndexRadixSet, mOperate);
      }
      if(mOperate.equals("UPDATE")) {
    	  System.out.println("111111---"+mLABankIndexRadixSet.size());
    	  LABankIndexRadixSet tLABankIndexRadixSet = new LABankIndexRadixSet();
    	  LABankIndexRadixBSet tLABankIndexRadixBSet = new LABankIndexRadixBSet();
    	 for(int i = 1;i<=mLABankIndexRadixSet.size();i++){
    	     LABankIndexRadixBSchema  tLABankIndexRadixBSchema = new  LABankIndexRadixBSchema();
    		 tLABankIndexRadixDB.setIdx(mLABankIndexRadixSet.get(i).getIdx());
    		 tLABankIndexRadixDB.setManageCom(mLABankIndexRadixSet.get(i).getManageCom());
    		 tLABankIndexRadixSchemaOld = tLABankIndexRadixDB.query().get(1); 
    		 tLABankIndexRadixSchemaNew = mLABankIndexRadixSet.get(i);
    		 tLABankIndexRadixSchemaNew.setMakeDate(tLABankIndexRadixSchemaOld.getMakeDate());
    		 tLABankIndexRadixSchemaNew.setMakeTime(tLABankIndexRadixSchemaOld.getMakeTime());
    		 tLABankIndexRadixSchemaNew.setModifyDate(CurrentDate);
    		 tLABankIndexRadixSchemaNew.setModifyTime(CurrentTime);
    		 tLABankIndexRadixSchemaNew.setOperator(mGlobalInput.Operator); 
    		 //备份数据
		     String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    		 tLABankIndexRadixBSchema.setEdorNo(tEdorNo);
    		 tLABankIndexRadixBSchema.setEdorType("1");
    		 tLABankIndexRadixBSchema.setIdx(tLABankIndexRadixSchemaOld.getIdx());
    		 tLABankIndexRadixBSchema.setManageCom(tLABankIndexRadixSchemaOld.getManageCom());
    		 tLABankIndexRadixBSchema.setWageFlag(tLABankIndexRadixSchemaOld.getWageFlag());
    		 tLABankIndexRadixBSchema.setAssessFlag(tLABankIndexRadixSchemaOld.getAssessFlag());
    		 tLABankIndexRadixBSchema.setRuleFlag(tLABankIndexRadixSchemaOld.getRuleFlag());
    		 tLABankIndexRadixBSchema.setAgentType(tLABankIndexRadixSchemaOld.getAgentType());
    		 tLABankIndexRadixBSchema.setBranchType(tLABankIndexRadixSchemaOld.getBranchType());
    		 tLABankIndexRadixBSchema.setBranchType2(tLABankIndexRadixSchemaOld.getBranchType2());
    		 tLABankIndexRadixBSchema.setOperator(tLABankIndexRadixSchemaOld.getOperator());
    		 tLABankIndexRadixBSchema.setMakeDate(tLABankIndexRadixSchemaOld.getMakeDate());
    		 tLABankIndexRadixBSchema.setMakeTime(tLABankIndexRadixSchemaOld.getMakeTime());
    		 tLABankIndexRadixBSchema.setModifyDate(tLABankIndexRadixSchemaOld.getModifyDate());
    		 tLABankIndexRadixBSchema.setModifyTime(tLABankIndexRadixSchemaOld.getModifyTime());
    		 tLABankIndexRadixSet.add(tLABankIndexRadixSchemaNew);
    		 tLABankIndexRadixBSet.add(tLABankIndexRadixBSchema);
    	 }
    	 map.put(tLABankIndexRadixSet, mOperate);
         map.put(tLABankIndexRadixBSet, "INSERT");
      }
      if(mOperate.equals("DELETE")){
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankWageStandBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankWageStandBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LABankWageStandBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
