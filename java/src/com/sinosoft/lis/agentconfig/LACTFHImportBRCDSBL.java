package com.sinosoft.lis.agentconfig;

import java.io.File;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LACTFHImportBRCDSBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "BKRateCommisionDiskImport2.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	LAComSet mLAComSet=new LAComSet();
	LAComDB mLAComDB=new LAComDB();
	
    private LARateCommisionSet mLARateCommisionSetFinal=new LARateCommisionSet();
    
    private LARateCommisionSet mLARateCommisionSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private int NumJ=0;//
    private StringBuffer merrorInfo=new StringBuffer();
    public LACTFHImportBRCDSBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        //System.out.println("begin import");
        //从磁盘导入数据
        LARateCommisionDiskImporter importer = new LARateCommisionDiskImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("DiskImportRateCommisionBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LARateCommision")) {
            mLARateCommisionSet = (LARateCommisionSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LARateCommision")) {
            if (mLARateCommisionSet == null) {
                mErrors.addOneError("导入佣金率信息失败！没有得到要导入的数据或者导入模版不是最新，请下载最新的模版。");
                return false;
            } else {
            	LDCodeDB tLDCodeDB=new LDCodeDB();
            	LDCodeSet tLDCodeSet=new LDCodeSet();
            	String tempSQL="select * from ldcode where code not in (" +
            			"select riskcode from lmriskapp where risktype4='4') " +
            			"and codetype='bankriskcode'";
            	tLDCodeSet=tLDCodeDB.executeQuery(tempSQL);
            	tempSQL="select * from ldcom where (length(trim(comcode))=4 or length(trim(comcode))=8)";
       
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LARateCommisionDB mTempLARateCommisionDB=new LARateCommisionDB();
            	//LARateCommisionSet mLARateCommisionSet=new LARateCommisionSet();
            	LARateCommisionSchema mTempLARateCommisionSchema=new LARateCommisionSchema();
            	LARateCommisionSchema mTempLARateCommisionSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
                	this.merrorNum=0;
                	this.NumJ=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLARateCommisionSchema=mLARateCommisionSet.get(i);
                	String aF05= 'J'+"";
                	mTempLARateCommisionSchema.setF05(aF05);
                    if (mTempLARateCommisionSchema.getManageCom() == null ||
                    		mTempLARateCommisionSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getManageCom().length()<this.mGlobalInput.ManageCom.length() 
                    		|| !mTempLARateCommisionSchema.getManageCom().substring(0,this.mGlobalInput.ManageCom.length())
                    		.equals(this.mGlobalInput.ManageCom)) {
                        this.merrorNum++;
                        this.merrorInfo.append("权限不够，设置该条提奖失败/");
                    }
                    if (mTempLARateCommisionSchema.getRiskCode() == null ||
                    		mTempLARateCommisionSchema.getRiskCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种编码不能为空/");
                    } 
                    if (mTempLARateCommisionSchema.getCurYear() <= 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("保单年度不能为空/");
                    } 
                    if (mTempLARateCommisionSchema.getPayIntv() == null ||
                    		mTempLARateCommisionSchema.getPayIntv().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费方式不能为空/");
                    } 
                    if(!mTempLARateCommisionSchema.getPayIntv().equals("0")
                    		&&!mTempLARateCommisionSchema.getPayIntv().equals("1")
                    		&&!mTempLARateCommisionSchema.getPayIntv().equals("3")
                    		&&!mTempLARateCommisionSchema.getPayIntv().equals("6")
                    		  &&!mTempLARateCommisionSchema.getPayIntv().equals("12")){
                    	this.merrorNum++;
                        this.merrorInfo.append("不能录入此缴费方式/");
                    }
                    if(mTempLARateCommisionSchema.getYear()<=0){
                		this.merrorNum++;
                		this.merrorInfo.append("保险期间不能为空");
                    }
                    if(mTempLARateCommisionSchema.getRate()==0){
                    	this.merrorNum++;
                    	this.merrorInfo.append("佣金比率不能为空");
                    }
                    if (mTempLARateCommisionSchema.getRate() < 0 
                    		||mTempLARateCommisionSchema.getRate()>1) {
                    	System.out.println("DiskImportRateCommisionBL.java: getRate:"+mLARateCommisionSet.get(i).getRate());
                        this.merrorNum++;
                        this.merrorInfo.append("佣金比率应介于０与１之间/");
                    } 
//                    if (String.valueOf(mTempLARateCommisionSchema.getF03())==null ||
//                    		String.valueOf(mTempLARateCommisionSchema.getF03()).equals("")) {
//                        this.merrorNum++;
//                        this.merrorInfo.append("保险期间始期不能为空/");
//                    }
                    if (mTempLARateCommisionSchema.getF03()<0) {
                        this.merrorNum++;
                        this.merrorInfo.append("保险期间始期不能小于0/");
                    }
                    if (mTempLARateCommisionSchema.getF04()<= 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("保险期间止期不能为空或0/");
                    }
                    if (mTempLARateCommisionSchema.getF03() >=0
                    		&&mTempLARateCommisionSchema.getF04()>=0 ) {
                    	int intStartDate =mTempLARateCommisionSchema.getF03();
                    	int intEndDate =mTempLARateCommisionSchema.getF04();
                        if (intStartDate > intEndDate) {
                            this.merrorNum++;
                            this.merrorInfo.append("保险期间始期不能大于保险期间止期/");
                        }
                    }
                    if (mTempLARateCommisionSchema.getCurYear() >=0
                    		&&mTempLARateCommisionSchema.getYear()>=0 ) {
                    	int intStartDate1 =mTempLARateCommisionSchema.getCurYear();
                    	int intEndDate1 =mTempLARateCommisionSchema.getYear();
                        if (intStartDate1 > intEndDate1) {
                            this.merrorNum++;
                            this.merrorInfo.append("保单年度不能大于保险期间/");
                        }
                    }

                    if (mTempLARateCommisionSchema.getF06() == null ||
                    		mTempLARateCommisionSchema.getF06().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("执行起期不能为空/");
                    } 
                    
                    
                    //判断执行起期格式 add by wrx 2015-8-20---七夕 
                    if(mTempLARateCommisionSchema.getF06()!=null&&
                    		!mTempLARateCommisionSchema.getF06().equals("")
                    ){
                    	String mDate = mTempLARateCommisionSchema.getF06();
                    	if(mDate.trim().length()==10){
 	                       int mYear =Integer.valueOf(mDate.trim().substring(0, 4)).intValue();
 	                       String mYM = mDate.trim().substring(4,5);
 	                       int mMonth = Integer.valueOf(mDate.trim().substring(5, 7)).intValue();
 	                       String mMD = mDate.trim().substring(7,8);
	                       int mDay = Integer.valueOf(mDate.trim().substring(8, 10)).intValue();
	                       if(mYear<2000||mYear>2100||mMonth<01||mMonth>12||mDay<01||mDay>31||!mYM.equals("-")||!mMD.equals("-")){
	                           this.merrorNum++;
	                           this.merrorInfo.append("执行起期不正确/");
	                       }
	                       if((mYear%4==0&&mYear%100!=0)||mYear%400==0){
	                    	   if(mMonth==02&&mDay>29){
		                           this.merrorNum++;
		                           this.merrorInfo.append("执行起期不正确/");
	                    	   }
	                       }else {
	                    	   if(mMonth==02&&mDay>28){
		                           this.merrorNum++;
		                           this.merrorInfo.append("执行起期不正确/");
	                    	   }
	                       }
                    	}else{
                    		this.merrorNum++;
                    		this.merrorInfo.append("执行起期格式应为YYYY-MM-DD，请检查");
                    	}
                    }
                    
                    if(mTempLARateCommisionSchema.getManageCom()!=null
                    		&&!mTempLARateCommisionSchema.getManageCom().equals("")
                    		&&!mTempLARateCommisionSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLARateCommisionSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }
                    
                    
                    int tempFlag2=0;
                    if(mTempLARateCommisionSchema.getRiskCode()!=null
                    		&&!mTempLARateCommisionSchema.getRiskCode().equals("")){
                    	for(int n=1;n<=tLDCodeSet.size();n++){
                    		System.out.println("...............tLDCodeSet.get(n).getCode()"
                    				+tLDCodeSet.get(n).getCode());
                    		System.out.println("...............mTempLARateCommisionSchema.getRiskCode()"
                    					+mTempLARateCommisionSchema.getRiskCode());
                    		if(tLDCodeSet.get(n).getCode()
                        			.equals(mTempLARateCommisionSchema.getRiskCode())){
                        		
                        		tempFlag2=1;
                        		break;
                        	}
                        }
                        if(tempFlag2==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("该险种不存在或该险种不为银代险种/");
                        }
                    }

                                                      
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLARateCommisionSet.size(); j++){
                        	mTempLARateCommisionSchema2=mLARateCommisionSet.get(j);
                        	System.out.println(mTempLARateCommisionSchema.getF01());
                        	System.out.println(mTempLARateCommisionSchema2.getF01());
                        	//add by wrx 2015-7
                        	//添加--交验---要导入的数据中相互间是否存在重复年期 （此代码基本无用EXCEL中添加了数据校验）
              			  if(mTempLARateCommisionSchema.getRiskCode().equals(mTempLARateCommisionSchema2.getRiskCode())&&
              					mTempLARateCommisionSchema.getManageCom().equals(mTempLARateCommisionSchema2.getManageCom())&&
              					mTempLARateCommisionSchema.getPayIntv().equals(mTempLARateCommisionSchema2.getPayIntv())&&
             // 					mTempLARateCommisionSchema.getF01().equals(mTempLARateCommisionSchema2.getF01())&&
              					mTempLARateCommisionSchema.getCurYear()==mTempLARateCommisionSchema2.getCurYear()&&
              					mTempLARateCommisionSchema.getYear()==mTempLARateCommisionSchema2.getYear()&&
              					mTempLARateCommisionSchema.getF06().equals(mTempLARateCommisionSchema2.getF06())){	
          				  Integer tStartYear1 =Integer.valueOf(mTempLARateCommisionSchema2.getF03());
        				  Integer tEndYear1 = Integer.valueOf(mTempLARateCommisionSchema2.getF04());
        				  Integer mStartYear1 = Integer.valueOf(mTempLARateCommisionSchema.getF03());
        				  Integer mEndYear1 = Integer.valueOf(mTempLARateCommisionSchema.getF04());
        				  if((mStartYear1.intValue()>=tStartYear1.intValue())&&mStartYear1.intValue()<tEndYear1.intValue()){
//        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+j+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！/");
        				  }
        				  if((mEndYear1.intValue()>tStartYear1.intValue())&&mEndYear1.intValue()<tEndYear1.intValue()){
//        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+j+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！/");
        				  }
        				  if((mStartYear1.intValue()<=tStartYear1.intValue())&&mEndYear1.intValue()>=tEndYear1.intValue()){
        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+j+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"和"+j+"行重复年期！/");
        				  }
              			  } 
        				  //end
                        	if(mTempLARateCommisionSchema.getManageCom()
                        			.equals(mTempLARateCommisionSchema2.getManageCom())
                        			&&mTempLARateCommisionSchema.getRiskCode()
                        			.equals(mTempLARateCommisionSchema2.getRiskCode())
                        			&&mTempLARateCommisionSchema.getCurYear()
                        			==mTempLARateCommisionSchema2.getCurYear()
                        			&&mTempLARateCommisionSchema.getPayIntv()
                        			.equals(mTempLARateCommisionSchema2.getPayIntv())
                        			&&mTempLARateCommisionSchema.getF03()
                        			==mTempLARateCommisionSchema2.getF03()
                        			&&mTempLARateCommisionSchema.getF04()
                        			==mTempLARateCommisionSchema2.getF04()
                        			&&mTempLARateCommisionSchema.getYear()==
                        			mTempLARateCommisionSchema2.getYear()
                        	){
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        	
                        }
                    }
                    //判断批量导入数据与已导入的数据中年期是否有重复区间
                    //add by wrx 2015-7-24
                    String mSQL = "select F03,F04 from laratecommision where riskcode = '"+mTempLARateCommisionSchema.getRiskCode()+"'" +
	  		 		" and managecom = '"+mTempLARateCommisionSchema.getManageCom()+"'"+
	  		 		" and payintv = '"+mTempLARateCommisionSchema.getPayIntv()+"'"+
	  		 		" and year = '"+mTempLARateCommisionSchema.getYear()+"'"+
	  		 		" and F05 = '"+mTempLARateCommisionSchema.getF05()+"'"+
	  		 		" and curyear = '"+mTempLARateCommisionSchema.getCurYear()+"'"+
	  		 		" and branchtype = '3' and branchtype2 = '01'";
                    SSRS mSSRS=new SSRS();
                    ExeSQL tExeSQL=new ExeSQL();
                    mSSRS=tExeSQL.execSQL(mSQL);
          		  	if(mSSRS!=null&&mSSRS.getMaxRow()>0){
        			  for(int k=1;k<mSSRS.getMaxRow();k++){
        				  Integer tStartYear =Integer.valueOf(mSSRS.GetText(k, 1));
        				  Integer tEndYear = Integer.valueOf(mSSRS.GetText(k, 2));
        				  Integer mStartYear = Integer.valueOf(mTempLARateCommisionSchema.getF03());
        				  Integer mEndYear = Integer.valueOf(mTempLARateCommisionSchema.getF04());
        				  if((mStartYear.intValue()>=tStartYear.intValue())&&mStartYear.intValue()<tEndYear.intValue()){
//        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！/");
        				  }
        				  if((mEndYear.intValue()>tStartYear.intValue())&&mEndYear.intValue()<tEndYear.intValue()){
//        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！/");
        				  }
        				  if((mStartYear.intValue()<=tStartYear.intValue())&&mEndYear.intValue()>=tEndYear.intValue()){
//        					  CError tError = new CError();
//        				      tError.moduleName = "LABRCSetFHCTBL";
//        				      tError.functionName = "check()";
//        				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！";
//        				      this.mErrors.clearErrors();
//        				      this.mErrors.addOneError(tError);
//        				      return false;
        					  this.merrorNum++;
        					  this.merrorInfo.append("在验证操作数据时出错。在选中的数据中第"+i+"行重复年期！/");
        				  }
        			  }
          		  	}
        		  //验证重复年期end
                    
                    if(this.merrorNum==0){
                       checkExist(mTempLARateCommisionSchema,mTempLARateCommisionDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LARateCommisionSchema mLARateCommisionSchema,LARateCommisionDB mLARateCommisionDB){

	LARateCommisionSet tempLARateCommisionSet=new LARateCommisionSet();
	String tempSql="select * from laratecommision where managecom='"
		+mLARateCommisionSchema.getManageCom()+"' and riskcode='"
		+mLARateCommisionSchema.getRiskCode()+"' and payintv='"
		+mLARateCommisionSchema.getPayIntv()+"' and curyear="
		+mLARateCommisionSchema.getCurYear()+" and f03="
		+mLARateCommisionSchema.getF03()+" and f04="
		+mLARateCommisionSchema.getF04()+" and year= " +
		+mLARateCommisionSchema.getYear()+" and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")
		+"' and (agentcom is null or agentcom ='') ";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLARateCommisionSet=mLARateCommisionDB.executeQuery(tempSql);
	if(tempLARateCommisionSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LARateCommisionSchema tempLARateCommisionSchema=new LARateCommisionSchema();
		tempLARateCommisionSchema.setManageCom(mLARateCommisionSchema.getManageCom());
		tempLARateCommisionSchema.setRiskCode(mLARateCommisionSchema.getRiskCode());
		tempLARateCommisionSchema.setRate(mLARateCommisionSchema.getRate());
		tempLARateCommisionSchema.setCurYear(mLARateCommisionSchema.getCurYear());
		tempLARateCommisionSchema.setF03(mLARateCommisionSchema.getF03());
		tempLARateCommisionSchema.setF04(mLARateCommisionSchema.getF04());
		tempLARateCommisionSchema.setF05(mLARateCommisionSchema.getF05());
		tempLARateCommisionSchema.setF06(mLARateCommisionSchema.getF06());
		tempLARateCommisionSchema.setPayIntv(mLARateCommisionSchema.getPayIntv());
		tempLARateCommisionSchema.setYear(mLARateCommisionSchema.getYear());
		//tempLARateCommisionSchema=mLARateCommisionSchema;
		mLARateCommisionSetFinal.add(tempLARateCommisionSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;
    	String idxTempCompare = (String) mTransferData.getValueByName("idxCompare");
    	int idxCompare=0;
    	if(idxTempCompare!=null&&!idxTempCompare.equals("")){
    		idxCompare=Integer.parseInt(idxTempCompare)+1;
    	}else{
    		idxCompare=1;
    	}
    	System.out.println("prepareData:idxTempCompare"+idxCompare);
        if (diskimporttype.equals("LARateCommision")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARateCommisionSetFinal.size();
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLARateCommisionSetFinal!=null&&mLARateCommisionSetFinal.size()>0){
             	//importPersons = mLARateCommisionSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLARateCommisionSetFinal.size(); i++) {
              	System.out.println("preparedata : managecom"+mLARateCommisionSetFinal.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	//System.out.println("BL:branchtype  "+branchtype);
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	//System.out.println("BL:branchtype2  "+branchtype2);
              	mLARateCommisionSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLARateCommisionSetFinal.get(i).setIdx(idx++);
              	mLARateCommisionSetFinal.get(i).setBranchType(branchtype);
              	mLARateCommisionSetFinal.get(i).setBranchType2(branchtype2); 
              	mLARateCommisionSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARateCommisionSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
//             	mLARateCommisionSetFinal.get(i).setYear("0");
              	mLARateCommisionSetFinal.get(i).setAppAge(0);
              	mLARateCommisionSetFinal.get(i).setF01("20");
              	mLARateCommisionSetFinal.get(i).setF02("A");
//              	if(mLARateCommisionSetFinal.get(i).getF05()==null
//              			||mLARateCommisionSetFinal.get(i).getF05().equals("")){
//              		mLARateCommisionSetFinal.get(i).setF05("0");
//              	}
              	mLARateCommisionSetFinal.get(i).setF05("J");
//              	mLARateCommisionSetFinal.get(i).setF06("0");
              	mLARateCommisionSetFinal.get(i).setsex("A");
              	//mLARateCommisionSet.get(i).setRateType("11");
              	mLARateCommisionSetFinal.get(i).setVersionType("11");
              	mmap.put(mLARateCommisionSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportRateCommisionBL d = new DiskImportRateCommisionBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
