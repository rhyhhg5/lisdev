package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LAActiveChargeSet;
import com.sinosoft.lis.schema.LAActiveChargeSchema;
import com.sinosoft.lis.db.LAActiveChargeDB;
import com.sinosoft.lis.schema.LAActiveChargeBSchema;
import com.sinosoft.lis.vschema.LAActiveChargeBSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author wangrx 2015-9-23
 * @version 1.0
 */
public class LAAssessAgencStandPremBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String tFlag = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
//  private MMap newMap = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAActiveChargeSet mLAActiveChargeSet = new LAActiveChargeSet();
  private Reflections ref = new Reflections();
  public LAAssessAgencStandPremBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAAssessAgencStandPremBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAAssessAgencStandPremBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验   
	  System.out.println("Begin LAAssessAgencStandPremBL.check.........1"+mLAActiveChargeSet.size());	 
	  	  LAActiveChargeSchema tempLAActiveChargeSchema1; 
	      LAActiveChargeSchema tempLAActiveChargeSchema2;
//	      LAActiveChargeSchema tLAActiveChargeSchemaNew = new LAActiveChargeSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
//		  LAActiveChargeDB tLAActiveChargeDB = new LAActiveChargeDB();
		  for(int i=1;i<=this.mLAActiveChargeSet.size();i++){
			  tempLAActiveChargeSchema1=this.mLAActiveChargeSet.get(i);
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLAActiveChargeSet.size();j++){
					  tempLAActiveChargeSchema2 = this.mLAActiveChargeSet.get(j);
					  if(tempLAActiveChargeSchema1.getAgentCode().equals(tempLAActiveChargeSchema2.getAgentCode())
						&&tempLAActiveChargeSchema1.getWageNo().equals(tempLAActiveChargeSchema2.getWageNo())
					  ){
							 BuildError("dealData","第"+i+","+j+"行业务员代码,月份相同请检查！");
		                     return false; 
					  }
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LAActiveCharge where  managecom='")
				  .append(tempLAActiveChargeSchema1.getManageCom())
				  .append("' and agentcode = '")
				  .append(tempLAActiveChargeSchema1.getAgentCode())
				  .append("' and wageno = '")
				  .append(tempLAActiveChargeSchema1.getWageNo())
				  .append("' and branchtype='")
				  .append(tempLAActiveChargeSchema1.getBranchType())
				  .append("' and branchtype2='")
				  .append(tempLAActiveChargeSchema1.getBranchType2())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LAAssessAgencStandPremBL";
				      tError.functionName = "check()";				
				      tError.errorMessage = "第"+i+"行业务员当前月份的交叉销售保费已录入";				      
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
				  tSB = new StringBuffer();
				  tSB= tSB.append("select managecom from laagent where agentcode = '"+tempLAActiveChargeSchema1.getAgentCode()+"'");
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(!tResult.equals(tempLAActiveChargeSchema1.getManageCom())){
					  CError tError = new CError();
				      tError.moduleName = "LAAssessMixAnnualPremBL";
				      tError.functionName = "check()";				
				      tError.errorMessage = "第"+i+"行管理机构与业务员不匹配！";				      
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false; 
				  }	  
		  }
//			 if(mOperate.equals("UPDATE")){
////				 tLAActiveChargeDB.setIdx(tempLAActiveChargeSchema1.getIdx());
//				 tLAActiveChargeSchemaNew = tLAActiveChargeDB.query().get(1);
//				 if(!tempLAActiveChargeSchema1.getManageCom().equals(tLAWageRadixAllSchemaNew.getManageCom())||
//					!tempLAActiveChargeSchema1.getAgentGrade().equals(tLAWageRadixAllSchemaNew.getAgentGrade())
//				 ){
//					 BuildError("dealData","第"+i+"行管理机构,职级代码,不能修改！");
//                     return false;
//				 }
//			 }
		  
	  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAAssessAgencStandPremBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLAActiveChargeSet.set( (LAActiveChargeSet) cInputData.getObjectByObjectName("LAActiveChargeSet",0));
      System.out.println("LAActiveChargeSet get"+mLAActiveChargeSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAssessAgencStandPremBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAAssessAgencStandPremBL.dealData........."+mOperate);
    try {
    	LAActiveChargeSchema  tLAActiveChargeSchemaNew = new  LAActiveChargeSchema();
    	LAActiveChargeSchema  tLAActiveChargeSchemaOld = new  LAActiveChargeSchema();
    	LAActiveChargeDB tLAActiveChargeDB =new  LAActiveChargeDB();
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LAAssessAgencStandPremBL.dealData.........INSERT"+mLAActiveChargeSet.size());
	        LAActiveChargeSet tLAActiveChargeSet = new LAActiveChargeSet();
	        LAActiveChargeBSet tLAActiveChargeBSet = new LAActiveChargeBSet();
	        for (int i = 1; i <= mLAActiveChargeSet.size(); i++) {
	        	tLAActiveChargeSchemaNew = mLAActiveChargeSet.get(i);	
//		          ExeSQL tExe = new ExeSQL();
//		          String tSql =
//		              "select max(int(idx)) from LAWageRadixAll order by 1 desc";
//		          String strIdx = "";
//		          int tMaxIdx = 0;
//		          strIdx = tExe.getOneValue(tSql);
//		          if (strIdx == null || strIdx.trim().equals("")) {
//		            tMaxIdx = 0;
//		          }
//		          else {
//		            tMaxIdx = Integer.parseInt(strIdx);
//		          }
//		      tMaxIdx += i;
//		      String sMaxIdx =tMaxIdx+"";
//		      tLAWageRadixAllSchemaNew.setIdx(sMaxIdx);
		      tLAActiveChargeSchemaNew.setMakeDate(CurrentDate);
		      tLAActiveChargeSchemaNew.setMakeTime(CurrentTime);
		      tLAActiveChargeSchemaNew.setModifyDate(CurrentDate);
		      tLAActiveChargeSchemaNew.setModifyTime(CurrentTime);
		      tLAActiveChargeSchemaNew.setOperator(mGlobalInput.Operator); 
		      tLAActiveChargeSet.add(tLAActiveChargeSchemaNew);
      }
        	map.put(tLAActiveChargeSet, mOperate);
//        	map.put(tLAActiveChargeBSet, "INSERT");
      }
      if(mOperate.equals("UPDATE")) {
    	  System.out.println("111111---"+mLAActiveChargeSet.size());
    	  LAActiveChargeSet tLAActiveChargeSet = new LAActiveChargeSet();
    	  LAActiveChargeBSet tLAActiveChargeBSet = new LAActiveChargeBSet();
    	 for(int i = 1;i<=mLAActiveChargeSet.size();i++){
    		
//    		 tLAWageRadixAllDB.setIdx(mLAWageRadixAllSet.get(i).getIdx());
    		 tLAActiveChargeDB.setManageCom(mLAActiveChargeSet.get(i).getManageCom());
    		 tLAActiveChargeDB.setAgentCode(mLAActiveChargeSet.get(i).getAgentCode());
    		 tLAActiveChargeDB.setWageNo(mLAActiveChargeSet.get(i).getWageNo());
    		 tLAActiveChargeSchemaOld = tLAActiveChargeDB.query().get(1); 
    		 tLAActiveChargeSchemaNew = mLAActiveChargeSet.get(i);
    		 tLAActiveChargeSchemaNew.setMakeDate(tLAActiveChargeSchemaOld.getMakeDate());
    		 tLAActiveChargeSchemaNew.setMakeTime(tLAActiveChargeSchemaOld.getMakeTime());
    		 tLAActiveChargeSchemaNew.setModifyDate(CurrentDate);
    		 tLAActiveChargeSchemaNew.setModifyTime(CurrentTime);
    		 tLAActiveChargeSchemaNew.setOperator(mGlobalInput.Operator); 
    		 tLAActiveChargeSet.add(tLAActiveChargeSchemaNew);
		      //插入备份轨迹表
    		 LAActiveChargeBSchema  tLAActiveChargeBSchema = new  LAActiveChargeBSchema();
    		 System.out.println("2222");
              ref.transFields(tLAActiveChargeBSchema, tLAActiveChargeSchemaOld);
              System.out.println("333");
		      String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		      tLAActiveChargeBSchema.setEdorNo(tEdorNo);
//		      tLAWageRadixAllTraceSchema.setEdorType("01");//”修改“标识
//		      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
//		      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
//		      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
//		      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
//		      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
//		      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
//		      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
//		      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
//		      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
//		      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
//		      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
//		      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
//		      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
//		      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
//		      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
              tLAActiveChargeBSet.add(tLAActiveChargeBSchema);
              System.out.println(tLAActiveChargeBSchema.getAgentCode()+"121212");
    	 }
    	 map.put(tLAActiveChargeSet, mOperate);
    	 map.put(tLAActiveChargeBSet, "INSERT");
      }
      if(mOperate.equals("DELETE")){
    	  LAActiveChargeSet tLAActiveChargeSet = new LAActiveChargeSet();
    	  LAActiveChargeBSet tLAActiveChargeBSet = new LAActiveChargeBSet();
    	 for(int i = 1;i<=mLAActiveChargeSet.size();i++){	
    		 tLAActiveChargeDB.setManageCom(mLAActiveChargeSet.get(i).getManageCom());
    		 tLAActiveChargeDB.setAgentCode(mLAActiveChargeSet.get(i).getAgentCode());
    		 tLAActiveChargeDB.setWageNo(mLAActiveChargeSet.get(i).getWageNo());
    		 tLAActiveChargeSchemaOld = tLAActiveChargeDB.query().get(1); 
    		 tLAActiveChargeSchemaNew = mLAActiveChargeSet.get(i);
    		 tLAActiveChargeSchemaNew.setMakeDate(tLAActiveChargeSchemaOld.getMakeDate());
    		 tLAActiveChargeSchemaNew.setMakeTime(tLAActiveChargeSchemaOld.getMakeTime());
    		 tLAActiveChargeSchemaNew.setModifyDate(CurrentDate);
    		 tLAActiveChargeSchemaNew.setModifyTime(CurrentTime);
    		 tLAActiveChargeSchemaNew.setOperator(mGlobalInput.Operator); 
    		 tLAActiveChargeSet.add(tLAActiveChargeSchemaNew);
		      //插入备份轨迹表
    		 LAActiveChargeBSchema	   tLAActiveChargeBSchema = new  LAActiveChargeBSchema();
             ref.transFields(tLAActiveChargeBSchema, tLAActiveChargeSchemaOld);
		     String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		     tLAActiveChargeBSchema.setEdorNo(tEdorNo);
//		      tLAWageRadixAllTraceSchema.setEdorType("02");//“删除“标识
//		      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
//		      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
//		      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
//		      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
//		      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
//		      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
//		      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
//		      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
//		      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
//		      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
//		      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
//		      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
//		      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
//		      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
//		      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
             tLAActiveChargeBSet.add(tLAActiveChargeBSchema);
             System.out.println(tLAActiveChargeBSchema.getAgentCode()+"121212");
    	 }
    	 map.put(tLAActiveChargeSet, mOperate);
    	 map.put(tLAActiveChargeBSet, "INSERT");
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAssessAgencStandPremBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAAssessMixAnnualPremBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LAAssessAgencStandPremBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
