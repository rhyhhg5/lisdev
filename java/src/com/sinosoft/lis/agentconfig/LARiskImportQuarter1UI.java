package com.sinosoft.lis.agentconfig;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LARiskImportQuarter1UI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LARiskImportQuarter1BL mLARiskImportQuarter1BL = null;

    public LARiskImportQuarter1UI()
    {
    	mLARiskImportQuarter1BL = new LARiskImportQuarter1BL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLARiskImportQuarter1BL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLARiskImportQuarter1BL.mErrors);
            return false;
        }
        //
        if(mLARiskImportQuarter1BL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLARiskImportQuarter1BL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLARiskImportQuarter1BL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLARiskImportQuarter1BL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LARiskImportQuarter1UI zLARiskImportQuarterUI = new LARiskImportQuarter1UI();
    }
}
