package com.sinosoft.lis.agentconfig;

import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LABRCSetBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz 2009-1-23
 * @version 1.0
 */
public class LABRCSetBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
  private Reflections ref = new Reflections();

  public LABRCSetBL() {

  }

//	public static void main(String[] args)
//	{
//		LABRCSetBL LABRCSetBL = new LABRCSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LABRCSetBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check())
    {
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABRCSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	  //进行重复数据校验
	  System.out.println("Begin LABRCSetBL.check.........1"+mLARateCommisionSet.size());
	  if(!this.mOperate.equals("DELETE")){
		  LARateCommisionSchema tempLARateCommisionSchema1;
		  LARateCommisionSchema tempLARateCommisionSchema2;
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
		  for(int i=1;i<=this.mLARateCommisionSet.size();i++){
			  tempLARateCommisionSchema1=this.mLARateCommisionSet.get(i);
			  for(int j=i+1;j<=this.mLARateCommisionSet.size();j++){
				  tempLARateCommisionSchema2=this.mLARateCommisionSet.get(j);
				  if(tempLARateCommisionSchema1.getRiskCode()
						  .equals(tempLARateCommisionSchema2.getRiskCode())
						  &&tempLARateCommisionSchema1.getAgentCom()
						  .equals(tempLARateCommisionSchema2.getAgentCom())
						  &&tempLARateCommisionSchema1.getCurYear()
						  ==tempLARateCommisionSchema2.getCurYear()
						  &&tempLARateCommisionSchema1.getYear()
						  ==tempLARateCommisionSchema2.getYear()
						  &&tempLARateCommisionSchema1.getF03()
						  ==tempLARateCommisionSchema2.getF03()
						  &&tempLARateCommisionSchema1.getF04()
						  ==tempLARateCommisionSchema2.getF04()
						  &&tempLARateCommisionSchema1.getPayIntv()
						  ==tempLARateCommisionSchema2.getPayIntv()
						  &&tempLARateCommisionSchema1.getManageCom()
						  .equals(tempLARateCommisionSchema2.getManageCom())){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from laratecommision where riskcode='")
				  .append(tempLARateCommisionSchema1.getRiskCode())
				  .append("' and curyear=")
				  .append(tempLARateCommisionSchema1.getCurYear())
				  .append(" and f01='")
				  .append(tempLARateCommisionSchema1.getF01())
				  .append("' and year=")
				  .append(tempLARateCommisionSchema1.getYear())
				  .append(" and f03=")
				  .append(tempLARateCommisionSchema1.getF03())
				  .append(" and f04=")
				  .append(tempLARateCommisionSchema1.getF04())
				  .append(" and F05='")
				  .append(tempLARateCommisionSchema1.getF05())
				  .append("' and payintv='")
				  .append(tempLARateCommisionSchema1.getPayIntv())
				  .append("' and managecom = '")
				  .append(tempLARateCommisionSchema1.getManageCom());
				  if(tempLARateCommisionSchema1.getAgentCom()!=null&&!tempLARateCommisionSchema1.getAgentCom().equals("")){
					  tSB=tSB.append("'and agentcom = '")
					  .append(tempLARateCommisionSchema1.getAgentCom())
					  .append("' and branchtype='");
				  }else{
					  tSB=tSB.append("'and (agentcom is null or agentcom='')")
					  .append(" and branchtype='");
				  }
				  tSB=tSB.append(tempLARateCommisionSchema1.getBranchType())
				  .append("'")
				  .append(" and branchtype2='")
				  .append(tempLARateCommisionSchema1.getBranchType2())
				  .append("' and versiontype='")
				  .append(tempLARateCommisionSchema1.getVersionType())
				  .append("' ");
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LABRCSetBL";
				      tError.functionName = "check()";
				      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
				  }
			  }
			  if(this.mOperate.equals("UPDATE")){
				  SSRS tSSRS=new SSRS();
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct idx from laratecommision where riskcode='")
				  .append(tempLARateCommisionSchema1.getRiskCode())
				  .append("' and curyear=")
				  .append(tempLARateCommisionSchema1.getCurYear())
				  .append(" and f01='")
				  .append(tempLARateCommisionSchema1.getF01())
				  .append("' and year=")
				  .append(tempLARateCommisionSchema1.getYear())
				  .append(" and f03=")
				  .append(tempLARateCommisionSchema1.getF03())
				  .append(" and f04=")
				  .append(tempLARateCommisionSchema1.getF04())
				  .append(" and f05='")
				  .append(tempLARateCommisionSchema1.getF05())
				  .append("' and payintv='")
				  .append(tempLARateCommisionSchema1.getPayIntv())
				  .append("' and managecom = '")
				  .append(tempLARateCommisionSchema1.getManageCom());
				  if(tempLARateCommisionSchema1.getAgentCom()!=null&&!tempLARateCommisionSchema1.getAgentCom().equals("")){
					  tSB=tSB.append("'and agentcom = '")
					  .append(tempLARateCommisionSchema1.getAgentCom())
					  .append("' and branchtype='");
				  }else{
					  tSB=tSB.append("'and (agentcom is null or agentcom='')")
					  .append(" and branchtype='");
				  }
				  tSB=tSB.append(tempLARateCommisionSchema1.getBranchType())
				  .append("'")
				  .append(" and branchtype2='")
				  .append(tempLARateCommisionSchema1.getBranchType2())
				  .append("' and versiontype='")
				  .append(tempLARateCommisionSchema1.getVersionType())
				  .append("' ");
				  tSSRS=tExeSQL.execSQL(tSB.toString());
				  for(int m=1;m<=tSSRS.getMaxRow();m++){
					  if(tempLARateCommisionSchema1.getIdx()!=Integer.parseInt(tSSRS.GetText(m,1))){
						  CError tError = new CError();
					      tError.moduleName = "LABRCSetBL";
					      tError.functionName = "check()";
					      tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
					      this.mErrors.clearErrors();
					      this.mErrors.addOneError(tError);
					      return false;
					  }
				  }
			  }
		  }
		  }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABRCSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.getObjectByObjectName("LARateCommisionSet",0));
      System.out.println("LARateCommisionSet get"+mLARateCommisionSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LABRCSetBL.dealData........."+mOperate);
    try {
      if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
        System.out.println("Begin LABRCSetBL.dealData.........1"+mLARateCommisionSet.size());
        LARateCommisionSet tLARateCommisionSet = new LARateCommisionSet();
        LARateCommisionBSet tLARateCommisionBSet = new LARateCommisionBSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          LARateCommisionSchema tLARateCommisionSchemaold = new
              LARateCommisionSchema();
          LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
          LARateCommisionDB tLARateCommisionDB = new LARateCommisionDB();

          tLARateCommisionDB.setIdx(mLARateCommisionSet.get(i).getIdx());
          //tLARateCommisionDB.setRiskCode(mLARateCommisionSet.get(i).getRiskCode());
          tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
          tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
          //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionSchemanew.setOperator(mGlobalInput.Operator);
          tLARateCommisionSchemanew.setMakeDate(tLARateCommisionSchemaold.
                                                getMakeDate());
          tLARateCommisionSchemanew.setMakeTime(tLARateCommisionSchemaold.
                                                getMakeTime());
          tLARateCommisionSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionSchemanew.setModifyTime(CurrentTime);
        //  tLARateCommisionSchemanew.setVersionType("11");
          tLARateCommisionSet.add(tLARateCommisionSchemanew);
          LARateCommisionBSchema tLARateCommisionBSchema = new
              LARateCommisionBSchema();
          ref.transFields(tLARateCommisionBSchema, tLARateCommisionSchemaold);
          //获取最大的ID号
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select int(max(idx)) from laratecommisionb order by 1 desc ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
            //System.out.println(tMaxIdx);
          }
          tMaxIdx++;
          tLARateCommisionBSchema.setIdx(tMaxIdx);
          if(i>1){
            tLARateCommisionBSchema.setIdx(tLARateCommisionBSet.get(i - 1).
                                           getIdx() + 1);
          }
          tLARateCommisionBSchema.setEdorNo(tLARateCommisionSchemaold.getIdx()+"");
          if(mOperate.equals("UPDATE"))
            tLARateCommisionBSchema.setEdorType("02");
          else
            tLARateCommisionBSchema.setEdorType("01");
          tLARateCommisionBSet.add(tLARateCommisionBSchema);

        }
        map.put(tLARateCommisionSet, mOperate);
        map.put(tLARateCommisionBSet, "INSERT");
      }
      else {
        LARateCommisionSet tLARateCommisionSet = new
            LARateCommisionSet();
        for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
          LARateCommisionSchema tLARateCommisionSchemanew = new
              LARateCommisionSchema();
          tLARateCommisionSchemanew = mLARateCommisionSet.get(i);
          tLARateCommisionSchemanew.setOperator(mGlobalInput.
                                                Operator);
          //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
          tLARateCommisionSchemanew.setMakeDate(CurrentDate);
          tLARateCommisionSchemanew.setMakeTime(CurrentTime);
          tLARateCommisionSchemanew.setModifyDate(CurrentDate);
          tLARateCommisionSchemanew.setModifyTime(CurrentTime);
        // tLARateCommisionSchemanew.setVersionType("11");
          tLARateCommisionSet.add(tLARateCommisionSchemanew);

        }
        map.put(tLARateCommisionSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABRCSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
