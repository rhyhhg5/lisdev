package com.sinosoft.lis.access;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 菜单节点显示处理/p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author dingzhong
 * @version 1.0
 */

public class Access {

  public Access() {
  }

  public static void main(String[] args) {
    Access tAccess = new Access();
    if (tAccess.canAccess("001",
                          "http://localhost:8900/ui/userMan/UserAddInput.jsp"))
      System.out.println("access success");
    else
      System.out.println("access deny");
  }

  /**
   * JspUrl字符串处理
   * @param jspPage String
   * @return String
   */
  public String getShortJspPage(String jspPage) {
    int lastIndex = jspPage.lastIndexOf('\\');
    int lastIndex2 = jspPage.lastIndexOf('/');
    if (lastIndex < lastIndex2)
      lastIndex = lastIndex2;
    if (lastIndex == -1 || lastIndex == jspPage.length() - 1)
      return jspPage;
    return jspPage.substring(lastIndex + 1);

  }

  /**
   * 校验用户是否有权限进入此节点
   * @param userCode String
   * @param jspPage String
   * @return boolean
   */
  public boolean canAccess(String userCode, String jspPage) {
    System.out.println("canAceess check");
    if (jspPage == null)
      return false;

    //首先查询出所有的叶子菜单节点号
    LDMenuSet tLDMenuSet = new LDMenuSet();
    LDMenuDB tLDMenuDB = new LDMenuDB();

    String sqlStr = "select * from LDMenu ";
    sqlStr = sqlStr +
        "where NodeCode in ( select NodeCode from LDMenuGrpToMenu ";
    sqlStr = sqlStr +
        "where MenuGrpCode in ( select MenuGrpCode from LDMenuGrp ";
    sqlStr = sqlStr +
        "where MenuGrpCode in (select MenuGrpCode from LDUserToMenuGrp ";
    sqlStr = sqlStr + "where UserCode = '" + userCode + "";
    sqlStr = sqlStr + "')  )   ) and childflag = '0' order by nodeorder";
    //System.out.println("strSql :" + sqlStr);

    tLDMenuSet = tLDMenuDB.executeQuery(sqlStr);

    if (tLDMenuSet.size() == 0)
      return false;

    Connection conn = null;
    conn = DBConnPool.getConnection();

    if (conn == null) {
      return false;
    }
    try {
      LDPageAccessDB tLDPageAccessDB = new LDPageAccessDB(conn);

      for (int i = 1; i < tLDMenuSet.size(); i++) {
        String leafMenuCode = tLDMenuSet.get(i).getNodeCode();
        sqlStr = "select * from ldPageAccess where nodecode = '" + leafMenuCode +
            "'";
        // System.out.println(sqlStr);
        LDPageAccessSet tLDPageAccessSet = new LDPageAccessSet();

        tLDPageAccessSet = tLDPageAccessDB.executeQuery(sqlStr);
        //  System.out.println("size : " + tLDPageAccessSet.size());
        for (int j = 1; j <= tLDPageAccessSet.size(); j++) {
          String runscript = tLDPageAccessSet.get(j).getRunScript();

          //获取jsp文件名
          String shortJspPage = getShortJspPage(runscript);
          // System.out.println("shortJspPage : " + shortJspPage);

          if (jspPage.indexOf(shortJspPage) != -1) {
            conn.close();
            System.out.println("can access!");
            return true;
          }
        }

      }
      conn.close();
      System.out.println("illegal access!");
      return true;
    }
    catch (Exception e) {

      try {
        System.out.println(e.toString());
        conn.close();
      }
      catch (Exception ex) {}
      return true;
    }
  }

}
