/**
 * 2012-12-27
 */
package com.sinosoft.lis.lgrp;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.sinosoft.lis.db.LCGrpSubInsuredImportDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * @author LY
 * 
 */
public class JobThread implements Runnable {
	private String mP_ID = null;

	private List<Schema> mDataList = null;

	private DFService mDFService = null;

	public JobThread(String cPid, List<Schema> cDataList, DFService cDFService) {
		mP_ID = cPid;
		mDataList = cDataList;
		mDFService = cDFService;
	}

	public void run() {
		try {

			System.out.println("--" + mP_ID + "--");
			
			working();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			mDFService.DownCount();
		}
	}

	private void working() {

		VData tVData = new VData();

		MMap tMMap = new MMap();
		for (int i = 0; i < mDataList.size(); i++) {
			Schema tGrpInsurInfo = (Schema) mDataList.get(i);
			tMMap.put(tGrpInsurInfo, SysConst.INSERT);
		}

		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(tVData, "");
	}
}
