/**
 * 2012-12-26
 */
package com.sinosoft.lis.lgrp;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sinosoft.utility.Schema;
import com.sinosoft.utility.TransferData;

/**
 * 调度入口（Service）
 * 
 * @author LY
 * 
 */
public class DFService {

	private TransferData mTransferData = new TransferData();

	private int mCurThreadCount = 0;

	public static final int Job_MaxCount = new Integer(LGrpThreadConfig.threadMaxCount);// 线程数

	public static final int Job_MaxDataCount = new Integer(LGrpThreadConfig.threadDataSize);// 取数数量

	private static int idx = 0;

	private static int count = 0;

	private String path;

	private DFService(String path, TransferData tTransferData) {
		this.path = path;
		this.mTransferData = tTransferData;
	}

	private boolean run() {
		ExecutorService exec = null;

		try {
			exec = Executors.newFixedThreadPool(DFService.Job_MaxCount);

			DataCatchService tSrcDatas = new DataCatchService(this.path, this.mTransferData);

			while (true) {

				// 此处开始获取数据，直到空，则停止处理。
				tSrcDatas.loadDatas();
				if (!tSrcDatas.hasNext()) {
					break;
				}

				while (tSrcDatas.hasNext()) {

					List<Schema> tCurDatas = null;
					// 取数
					tCurDatas = tSrcDatas.getDatas(DFService.Job_MaxDataCount);

					this.UpCount();
					// runJobsGroup(exec, tCurDatas);
					Runnable run = new JobThread(String.valueOf(idx), tCurDatas, this);
					exec.submit(run);

					count++;

					if (this.getCurThreadCount() >= DFService.Job_MaxCount) {
						boolean tFlag = true;
						while (tFlag) {
							Thread.sleep(100);
							if (this.getCurThreadCount() < DFService.Job_MaxCount) {
								tFlag = false;
							}
						}
					}
				}
			}

			while (true) {
				if (this.getCurThreadCount() == 0) {
					break;
				}
			}

			System.out.println("共运行任务：" + count);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Error e){
			e.printStackTrace();
		} finally {
			if (exec != null) {
				exec.shutdown();
			}
		}

		return true;
	}
	
	/*
	 *  最早的每条一个线程处理的函数
	 private void runJobsGroup(ExecutorService cExec, List<Schema> cCurDatas) {
		try {
			List<Schema> tCurDatas = cCurDatas;

			int tCurPoolNo = tCurDatas.size();

			CountDownLatch mRealy = new CountDownLatch(tCurPoolNo);

			CountDownLatch mBegin = new CountDownLatch(1);

			CountDownLatch mEnd = new CountDownLatch(tCurPoolNo);

			for (int index = 0; index < tCurDatas.size(); index++) {
				count++;
				System.out.println(count);

				// Runnable run = new JobThread(String.valueOf(idx),
				// tCurDatas.get(index), mRealy, mBegin, mEnd);
				// cExec.submit(run);
			}

			mRealy.await();

			System.out.println("Start ... ");
			mBegin.countDown();

			mEnd.await();

			System.out.println("End ... ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

	public static DFService getService(String path, TransferData tTransferData) {
		return new DFService(path, tTransferData);
	}

	public boolean startService() {
		boolean tResultFlag = false;

		try {
			tResultFlag = run();
		} catch (Exception e) {
			e.printStackTrace();
			tResultFlag = false;
		}

		return tResultFlag;
	}

	public int getCurThreadCount() {
		return mCurThreadCount;
	}

	public void UpCount() {
		synchronized (this) {
			mCurThreadCount++;
		}
	}

	public void DownCount() {
		synchronized (this) {
			mCurThreadCount--;
		}
	}

}
