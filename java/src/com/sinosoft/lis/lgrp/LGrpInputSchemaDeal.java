package com.sinosoft.lis.lgrp;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.utility.TransferData;

/**
 * 大团单excel行解析处理类
 * 
 * @author 张成轩
 */
public class LGrpInputSchemaDeal {

	/**
	 * 转换excel行为Schema
	 * 
	 * @param hRow 行对象
	 * @param rowNo 行数
	 * @param tTransferData
	 * @return
	 */
	public static LCGrpSubInsuredImportSchema setScheml(XSSFRow hRow, int rowNo, TransferData tTransferData) {

		try {
			String tGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
			String tPrtNo = (String) tTransferData.getValueByName("PrtNo");
			String tBatchNo = (String) tTransferData.getValueByName("BatchNo");
			String tSeqNo = (String) tTransferData.getValueByName("SeqNo");
			GlobalInput tGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");

			LCGrpSubInsuredImportSchema lgrpSchema = new LCGrpSubInsuredImportSchema();

			lgrpSchema.setSeqNo(tSeqNo + rowNo);

			lgrpSchema.setGrpContNo(tGrpContNo);
			lgrpSchema.setPrtNo(tPrtNo);

			lgrpSchema.setInsuredAppAge(getVal(hRow.getCell(5)));
			lgrpSchema.setContPlanCode(getVal(hRow.getCell(2)));
			lgrpSchema.setCValiDate(getVal(hRow.getCell(3)));
			lgrpSchema.setCInValiDate(getVal(hRow.getCell(4)));
			lgrpSchema.setStateFlag("00");
			lgrpSchema.setDataChkFlag("00");
			lgrpSchema.setAppFlag("0");
			lgrpSchema.setName(getVal(hRow.getCell(8)));
			lgrpSchema.setSex(getVal(hRow.getCell(9)));
			lgrpSchema.setBirthday(getVal(hRow.getCell(10)));
			lgrpSchema.setIDType(getVal(hRow.getCell(11)));
			lgrpSchema.setIDNo(getVal(hRow.getCell(12)));
			lgrpSchema.setIDStartDate(getVal(hRow.getCell(13)));
			lgrpSchema.setIDEndDate(getVal(hRow.getCell(14)));
			lgrpSchema.setOthIDType(getVal(hRow.getCell(15)));
			lgrpSchema.setOthIDNo(getVal(hRow.getCell(16)));
			lgrpSchema.setOccupationType(getVal(hRow.getCell(17)));
			lgrpSchema.setOccupationCode(getVal(hRow.getCell(18)));
			lgrpSchema.setOperator(tGlobalInput.Operator);
			lgrpSchema.setMakeDate(PubFun.getCurrentDate());
			lgrpSchema.setMakeTime(PubFun.getCurrentTime());
			lgrpSchema.setModifyDate(PubFun.getCurrentDate());
			lgrpSchema.setModifyTime(PubFun.getCurrentTime());
			lgrpSchema.setRetire(getVal(hRow.getCell(19)));
			lgrpSchema.setEmployeeName(getVal(hRow.getCell(6)));
			lgrpSchema.setRelation(getVal(hRow.getCell(7)));
			lgrpSchema.setBankCode(getVal(hRow.getCell(20)));
			lgrpSchema.setBankAccNo(getVal(hRow.getCell(21)));
			lgrpSchema.setAccName(getVal(hRow.getCell(22)));

			lgrpSchema.setCalState("00");

			lgrpSchema.setBatchNo(tBatchNo);
			lgrpSchema.setMainInsuID(getVal(hRow.getCell(1)));

			return lgrpSchema;
		} catch (RuntimeException e) {

			e.printStackTrace();

			return null;
		}
	}

	/**
	 * 解析单元格对象
	 * 
	 * @param hColl 单元格对象
	 * @return
	 */
	private static String getVal(XSSFCell hColl) {
		String tVal = null;

		if (hColl == null) {
			return null;
		}

		if (hColl.getCellType() == XSSFCell.CELL_TYPE_BOOLEAN) {
			tVal = String.valueOf(hColl.getBooleanCellValue());
		} else if (hColl.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
			tVal = String.valueOf(hColl.getNumericCellValue());
		} else {
			tVal = String.valueOf(hColl.getStringCellValue());
		}

		return tVal;
	}
}
