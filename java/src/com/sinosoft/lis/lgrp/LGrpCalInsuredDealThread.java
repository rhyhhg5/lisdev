/**
 * 2012-12-27
 */
package com.sinosoft.lis.lgrp;

import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;

/**
 * 大团单算费处理类
 * 
 * @author 张成轩
 */
public class LGrpCalInsuredDealThread implements Runnable {
	private String mP_ID = null;

	private LCGrpSubInsuredImportSet mDataList = null;

	private LGrpCalInseredDealMainService mDFService = null;

	private LGrpInsuredDeal mDeal = null;

	public LGrpCalInsuredDealThread(String cPid, LGrpInsuredDeal tDeal, LCGrpSubInsuredImportSet cDataList, LGrpCalInseredDealMainService cService) {
		mP_ID = cPid;
		mDataList = cDataList;
		mDFService = cService;
		mDeal = tDeal;
	}

	public void run() {
		try {

			System.out.println("--" + mP_ID + "--");

			working();
			
			System.out.println(mP_ID + "线程处理完毕");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			mDFService.DownCount();
		}
	}

	/**
	 * 算费处理
	 */
	private void working() {
		mDeal.dealData(mDataList);
	}
}
