package com.sinosoft.lis.lgrp;

import java.util.Date;

import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpSubInsuredImportDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 大团单算费标识串处理类
 * 
 * @author 张成轩
 */
public class LGrpCalSignDeal {

	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

	private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mGrpContNo;

	public LGrpCalSignDeal(TransferData tTransferData) {

		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");

		// 初始化保单信息
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpContNo);
		if (tLCGrpContDB.getInfo()) {
			mLCGrpContSchema = tLCGrpContDB.getSchema();
		}

		// 初始化保障信息
		String querySql = "select * from LCContPlanDutyParam where grpcontno = '" + mGrpContNo + "' and contplancode != '11' ";
		System.out.println(querySql);
		mLCContPlanDutyParamSet = new LCContPlanDutyParamDB().executeQuery(querySql);

		String riskQuerySql = "select * from LCContPlanRisk where grpcontno = '" + mGrpContNo + "' and contplancode != '11' ";
		System.out.println(querySql);
		mLCContPlanRiskSet = new LCContPlanRiskDB().executeQuery(riskQuerySql);
	}

	public boolean dealData(LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet) {

		// 提交数据库map集合
		MMap tMap = new MMap();

		for (int index = 1; index <= tLCGrpSubInsuredImportSet.size(); index++) {

			LCGrpSubInsuredImportSchema tInsured = tLCGrpSubInsuredImportSet.get(index);

			// 获取算费标识串
			String calSign = getCalSign(tInsured);

			if (calSign == null || "".equals(calSign)) {
				// 获取算费字符串失败
				tMap.put(getErrorMap(tInsured, calSign), SysConst.UPDATE);
			} else {
				// 添加更新Map
				tMap.put(getCalMap(tInsured, calSign), SysConst.UPDATE);
			}

		}

		// 提交数据库
		if (!save(tMap)) {
			System.out.println("保存被保人算费信息时出错了");
			return false;
		}

		return true;
	}

	/**
	 * 待更新Schema更新sql处理
	 * 
	 * @param tInsured
	 * @param calSign
	 * @return
	 */
	private String getCalMap(LCGrpSubInsuredImportSchema tInsured, String calSign) {

		// 不使用Schema方式进行处理，改为直接使用更新sql，经测试会大幅提高效率
		String updateSql = "update LCGrpSubInsuredImport set" +
				" CalState='01'," +
				" CalRemark='" + calSign + "'," +
				" Operator='" + mGlobalInput.Operator + "'," +
				" ModifyDate=current date," +
				" ModifyTime=current time " +
				" where seqno='" + tInsured.getSeqNo() + "'";
	
		return updateSql;
	}

	/**
	 * 待更新异常Schema更新sql处理
	 * 
	 * @param tInsured
	 * @param calSign
	 * @return
	 */
	private String getErrorMap(LCGrpSubInsuredImportSchema tInsured, String calSign) {
		
		// 不使用Schema方式进行处理，改为直接使用更新sql，经测试会大幅提高效率
		String updateSql = "update LCGrpSubInsuredImport set" +
				" CalState='03'," +
				" CalRemark='算费失败，获取算费标识失败'," +
				" Operator='" + mGlobalInput.Operator + "'," +
				" ModifyDate=current date," +
				" ModifyTime=current time " +
				" where seqno='" + tInsured.getSeqNo() + "'";

		return updateSql;
	}

	/**
	 * 获取算费串
	 * 
	 * @param tInsured
	 * @return
	 */
	private String getCalSign(LCGrpSubInsuredImportSchema tInsured) {

		String contPlanCode = tInsured.getContPlanCode();

		if (contPlanCode == null) {
			return null;
		}

		String calSign = contPlanCode;

		for (int index = 1; index <= mLCContPlanRiskSet.size(); index++) {

			LCContPlanRiskSchema tLCContPlanRiskSchema = mLCContPlanRiskSet.get(index);

			if (contPlanCode.equals(tLCContPlanRiskSchema.getContPlanCode())) {
				
				String riskCode = tLCContPlanRiskSchema.getRiskCode();
				
				// 为保证后续拆分正常，不管算费串的描述中有没有险种，都要加上险种编码
				//calSign += ("|" + riskCode + "_" + getRiskCalSign(riskCode, tInsured));
				// 数据库那个字段太短了。。。先用这个
				calSign += ("|" + getRiskCalSign(riskCode, tInsured));
			}
		}

		return calSign;
	}

	/**
	 * 获取单个险种算费串
	 * 
	 * @param tRiskCode
	 * @param tInsured
	 * @return
	 */
	private String getRiskCalSign(String tRiskCode, LCGrpSubInsuredImportSchema tInsured) {

		String tCalParam = "";
		double sumAmnt = 0;
		double sumPrem = 0;
		Calculator mCalculator = new Calculator();
		mCalculator.setCalCode("BG0001");
		mCalculator.addBasicFactor("RiskCode", tRiskCode);
		mCalculator.addBasicFactor("Sex", tInsured.getSex());
		mCalculator.addBasicFactor("AppAge", String.valueOf(tInsured.getInsuredAppAge()));
		mCalculator.addBasicFactor("Job", tInsured.getOccupationType());
		mCalculator.addBasicFactor("PayIntv", String.valueOf(mLCGrpContSchema.getPayIntv()));

		for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {

			LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = mLCContPlanDutyParamSet.get(i);

			if (tInsured.getContPlanCode().equals(tLCContPlanDutyParamSchema.getContPlanCode())
					&& tRiskCode.equals(tLCContPlanDutyParamSchema.getRiskCode())) {

				if ("Amnt".equals(tLCContPlanDutyParamSchema.getCalFactor())
						&& !"".equals(StrTool.cTrim(tLCContPlanDutyParamSchema.getCalFactorValue()))) {

					// 保额为累加保额
					sumAmnt = sumAmnt + Double.parseDouble(tLCContPlanDutyParamSchema.getCalFactorValue());
				} else if ("Prem".equals(tLCContPlanDutyParamSchema.getCalFactor())
						&& !"".equals(StrTool.cTrim(tLCContPlanDutyParamSchema.getCalFactorValue()))) {

					// 保费为累加保费
					sumPrem = sumPrem + Double.parseDouble(tLCContPlanDutyParamSchema.getCalFactorValue());
				} else {

					// 其他项只置一个
					mCalculator.addBasicFactor(tLCContPlanDutyParamSchema.getCalFactor(), tLCContPlanDutyParamSchema.getCalFactorValue());
				}
			}
		}

		mCalculator.addBasicFactor("Get", String.valueOf(sumAmnt));
		mCalculator.addBasicFactor("Prem", String.valueOf(sumPrem));

		tCalParam = mCalculator.calculate();

		return tCalParam;
	}

	/**
	 * 提交数据库
	 * 
	 * @param tMap 提交map集合
	 * @return
	 */
	private boolean save(MMap tMap) {
		VData mInputData = new VData();
		mInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("---------提交数据库失败！-----------" + tPubSubmit.mErrors.getLastError());
			return false;
		}

		return true;
	}

	public static void main(String[] arr) {

		String grpContNo = "1400308220";

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "test";

		TransferData tTransferData = new TransferData();

		tTransferData.setNameAndValue("GrpContNo", grpContNo);
		tTransferData.setNameAndValue("GlobalInput", tGlobalInput);

		LGrpCalSignDeal tLGrpCalSignDeal = new LGrpCalSignDeal(tTransferData);

		LCGrpSubInsuredImportDB tInsuredDB = new LCGrpSubInsuredImportDB();
		LCGrpSubInsuredImportSet tInsuredSet = tInsuredDB.executeQuery("select * from LCGrpSubInsuredImport where grpcontno='" + grpContNo
				+ "' fetch first 5000 rows only");

		long tStart2 = System.nanoTime();
		tLGrpCalSignDeal.dealData(tInsuredSet);
		long tEnd2 = System.nanoTime();
		System.out.println("耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
	}
}
