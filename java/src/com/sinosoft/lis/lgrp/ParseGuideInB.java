/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.lgrp;

// import java.io.*;
// import java.util.*;

import java.util.HashMap;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCGrpPremPlanSchema;
import com.sinosoft.lis.schema.LCGrpRiskPremPlanSchema;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.schema.LCGrpSubInsuredSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.tb.ContPlanQryBL;
import com.sinosoft.lis.tb.GrpPolImpInfo;
import com.sinosoft.lis.tb.ProposalBL;
import com.sinosoft.lis.vbl.LCBnfBLSet;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCGrpRiskPremPlanSet;
import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ParseGuideInB {
	// @Fields
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public CErrors logErrors = new CErrors();

	/** 往前面传输数据的容器 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private TransferData mTransferData = new TransferData();

	private String mGrpContNo;

	private Reflections tReflections = new Reflections();

	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	private LCPolBL mainPolBL = new LCPolBL();

	private GrpPolImpInfo m_GrpPolImpInfo = new GrpPolImpInfo();

	private String retire;

	private String avgage;

	private String InsuredNum;

	public ParseGuideInB() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ParseGuideInB-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 得到传入数据
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		this.mGrpContNo = (String) this.mTransferData.getValueByName("GrpContNo");
		return true;
	}

	/**
	 * 校验传输数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		if (mGlobalInput == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "操作超时，请重新登录!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "获取算费保单信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mGrpContNo == null || "".equals(mGrpContNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "获取算费保单号码失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		String tSQL = "select 1 from lcgrpcont where grpcontno = '" + mGrpContNo + "' and ContPrintType = '3' ";
		String tBContFlag = new ExeSQL().getOneValue(tSQL);
		if (!"1".equals(tBContFlag)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "团体保单属性不是大团单，无法采用大团单算费!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {

		// 在被保人循环外获取保障计划的险种，提升效率
		// 获取保单数据
		String tGrpContSQL = "select * from lcgrpcont where grpcontno = '" + mGrpContNo + "' ";
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tGrpContSQL);
		if (tLCGrpContSet == null || tLCGrpContSet.size() != 1) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "大团单算费时，无法采用团单数据!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mLCGrpContSchema = tLCGrpContSet.get(1);

		String tGrpPolSQL = "select * from lcgrppol where grpcontno = '" + mGrpContNo + "' ";
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(tGrpPolSQL);
		if (tLCGrpPolSet == null || tLCGrpContSet.size() < 1) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "大团单算费时，无法获取团单险种数据!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 获取保障计划编码
		String tContPlanSQL = "select distinct contplancode from LCGrpSubInsuredImport where grpcontno = '" + mGrpContNo
				+ "' and CalState = '00' order by ContPlanCode ";
		SSRS tContPlanSSRS = new ExeSQL().execSQL(tContPlanSQL);
		// 获取保障计划要素
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
		LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
		String tSQL = "select * from LCContPlanDutyParam where grpcontno = '" + mGrpContNo
				+ "' and contplancode != '11' order by contplancode,riskcode ";
		tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(tSQL);
		// 获取每个保障计划下的险种
		LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
		String tContRisk = "select * from LCContPlanRisk where grpcontno = '" + mGrpContNo
				+ "' and contplancode != '11' order by contplancode,riskcode ";
		LCContPlanRiskSet tAllLCContPlanRiskSet = tLCContPlanRiskDB.executeQuery(tContRisk);
		if (tAllLCContPlanRiskSet == null || tAllLCContPlanRiskSet.size() < 1) {
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "checkData";
			tError.errorMessage = "大团单算费时，无法获取保障计划信息!";
			this.mErrors.addOneError(tError);
			return false;
		}
		HashMap tContPlanRisk = new HashMap();
		for (int i = 1; i <= tContPlanSSRS.MaxRow; i++) {
			String tContPlanCode = tContPlanSSRS.GetText(i, 1);
			LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
			for (int j = 1; j <= tAllLCContPlanRiskSet.size(); j++) {
				LCContPlanRiskSchema tLCContPlanRiskSchema = tAllLCContPlanRiskSet.get(j);
				if (tContPlanCode.equals(tLCContPlanRiskSchema.getContPlanCode())) {
					mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
				}
			}
			tContPlanRisk.put(tContPlanCode, mLCContPlanRiskSet);
		}
		// 对每个人进行算费
		RSWrapper rswrapper = new RSWrapper();
		String tSql = "select * from LCGrpSubInsuredImport where grpcontno = '" + mGrpContNo + "' and CalState = '00' and datachkflag='01' ";
		LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = new LCGrpSubInsuredImportSet();
		rswrapper.prepareData(tLCGrpSubInsuredImportSet, tSql);
		try {
			do {
				rswrapper.getData();
				MMap tALLMMap = new MMap();
				HashMap tPremParams = new HashMap();
				for (int i = 1; i <= tLCGrpSubInsuredImportSet.size(); i++) {
					LCGrpSubInsuredImportSchema tLCGrpSubInsuredImportSchema = tLCGrpSubInsuredImportSet.get(i);
					MMap tOnePersonMap = dealOnePerson(tLCGrpSubInsuredImportSchema, tContPlanRisk, tLCContPlanDutyParamSet, tLCGrpPolSet,
							tPremParams);
					if (tOnePersonMap == null) {
						tLCGrpSubInsuredImportSchema.setCalState("03");
						tLCGrpSubInsuredImportSchema.setCalRemark(this.mErrors.getFirstError());
						tALLMMap.put(tLCGrpSubInsuredImportSchema, SysConst.UPDATE);
						continue;
					} else {
						tALLMMap.add(tOnePersonMap);
					}

				}
				// 保存数据
				if (!saveAllPerson(tALLMMap)) {
					System.out.println("保存被保人算费信息时出错了");
				}
			} while (tLCGrpSubInsuredImportSet.size() > 0);
			rswrapper.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			rswrapper.close();
		}
		return true;
	}

	private String getCalParam(String aRiskCode, LCContPlanDutyParamSet aLCContPlanDutyParamSet,
			LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {
		String tCalParam = "";
		double sumAmnt = 0;
		double sumPrem = 0;
		Calculator mCalculator = new Calculator();
		String tCalCodeSQL = "select othercalcode from LDRiskManage where riskcode = '" + aRiskCode + "' and CheckType = 'lgrprisk' ";
		String tCalCode = new ExeSQL().getOneValue(tCalCodeSQL);
		mCalculator.setCalCode(tCalCode);
		// mCalculator.setCalCode("BG0001");
		mCalculator.addBasicFactor("RiskCode", aRiskCode);
		mCalculator.addBasicFactor("Sex", aLCGrpSubInsuredImportSchema.getSex());
		mCalculator.addBasicFactor("AppAge", String.valueOf(aLCGrpSubInsuredImportSchema.getInsuredAppAge()));
		mCalculator.addBasicFactor("Job", aLCGrpSubInsuredImportSchema.getOccupationType());
		mCalculator.addBasicFactor("PayIntv", String.valueOf(mLCGrpContSchema.getPayIntv()));
		for (int i = 1; i <= aLCContPlanDutyParamSet.size(); i++) {
			LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = aLCContPlanDutyParamSet.get(i);
			if (aLCGrpSubInsuredImportSchema.getContPlanCode().equals(tLCContPlanDutyParamSchema.getContPlanCode())
					&& aRiskCode.equals(tLCContPlanDutyParamSchema.getRiskCode())) {

				if ("Amnt".equals(tLCContPlanDutyParamSchema.getCalFactor())
						&& !"".equals(StrTool.cTrim(tLCContPlanDutyParamSchema.getCalFactorValue()))) {
					sumAmnt = sumAmnt + Double.parseDouble(tLCContPlanDutyParamSchema.getCalFactorValue());
				} else if ("Prem".equals(tLCContPlanDutyParamSchema.getCalFactor())
						&& !"".equals(StrTool.cTrim(tLCContPlanDutyParamSchema.getCalFactorValue()))) {
					sumPrem = sumPrem + Double.parseDouble(tLCContPlanDutyParamSchema.getCalFactorValue());
				} else {
					mCalculator.addBasicFactor(tLCContPlanDutyParamSchema.getCalFactor(), tLCContPlanDutyParamSchema.getCalFactorValue());
				}
			}
		}
		mCalculator.addBasicFactor("Get", String.valueOf(sumAmnt));
		mCalculator.addBasicFactor("Prem", String.valueOf(sumPrem));

		tCalParam = mCalculator.calculate();
		return tCalParam;
	}

	private VData prepareContPlanData(LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema, LCGrpPolSchema aLCGrpPolSchema,
			LCContPlanRiskSchema tLCContPlanRiskSchema) {

		VData tNewVData = new VData();
		MMap tmpMap = new MMap();
		String strRiskCode = aLCGrpPolSchema.getRiskCode();
		if (aLCGrpPolSchema == null) {
			buildError("prepareContPlanData", strRiskCode + "险种对应的集体投保单没有找到!");
			return null;
		}

		// 拷贝一份，避免修改本地数据
		LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		tLCGrpPolSchema.setSchema(aLCGrpPolSchema);

		LCContSchema contSchema = getLCContSchema(aLCGrpPolSchema, aLCGrpSubInsuredImportSchema);
		tNewVData.add(contSchema);

		LCInsuredSchema insuredSchema = getLInsured(aLCGrpPolSchema, aLCGrpSubInsuredImportSchema);
		tNewVData.add(insuredSchema);

		LCPolSchema tLCPolSchema = getLCPolSchema(aLCGrpPolSchema, contSchema, insuredSchema, aLCGrpSubInsuredImportSchema.getContPlanCode());

		ContPlanQryBL contPlanQryBL = new ContPlanQryBL();
		VData tVData = new VData();
		tVData.add(tLCContPlanRiskSchema);
		boolean b = contPlanQryBL.submitData(tVData, "");
		if (!b || contPlanQryBL.mErrors.needDealError()) {
			CError.buildErr(this, "查询计划要约出错:", contPlanQryBL.mErrors);
			return null;
		}
		tVData = contPlanQryBL.getResult();
		LCDutySet tDutySet = (LCDutySet) tVData.getObjectByObjectName("LCDutySet", 0);
		LCGetSet tGetSet = (LCGetSet) tVData.getObjectByObjectName("LCGetSet", 0);
		tNewVData.add(tGetSet);
		if (tDutySet == null) {
			LCDutySchema tDutySchema = (LCDutySchema) tVData.getObjectByObjectName("LCDutySchema", 0);
			if (tDutySchema == null) {
				CError.buildErr(this, "查询计划要约出错:责任信息不能为空");
				return null;
			}
			setPolInfoByDuty(tLCPolSchema, tDutySchema);
			tDutySet = new LCDutySet();
			tDutySet.add(tDutySchema);
		}
		if (tDutySet == null || tDutySet.size() <= 0) {
			CError.buildErr(this, strRiskCode + "要约信息生成错误出错");
			return null;
		}
		/**
		 * 将险种的交费频次存入责任中
		 */
		if (tLCPolSchema.getPayIntv() > 0) {
			for (int i = 1; i <= tDutySet.size(); i++) {
				tDutySet.get(i).setPayIntv(this.mLCGrpContSchema.getPayIntv());
			}
		}
		/**
		 * 将险种的交费频次存入责任
		 */
		tNewVData.add(tDutySet);
		// 保费
		LCPremSet tPremSet = new LCPremSet();
		tPremSet = (LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0);
		tNewVData.add(tPremSet);

		// 加入对应险种的集体投保单信息,险种承保描述信息
		LMRiskAppSchema tLMRiskAppSchema = m_GrpPolImpInfo.findLMRiskAppSchema(strRiskCode);
		if (tLMRiskAppSchema == null) {
			buildError("prepareContPlanData", strRiskCode + "险种对应的险种承保描述没有找到!");
			return null;
		}
		LMRiskSchema tLMRiskSchema = m_GrpPolImpInfo.findLMRiskSchema(strRiskCode);
		if (tLMRiskSchema == null) {
			buildError("prepareContPlanData", strRiskCode + "险种对应的险种承保描述没有找到!");
			return null;
		}

		LDGrpSchema ttLDGrpSchema = m_GrpPolImpInfo.findLDGrpSchema(tLCGrpPolSchema.getCustomerNo());
		if (ttLDGrpSchema == null) {
			buildError("prepareContPlanData", tLCGrpPolSchema.getCustomerNo() + "对应的集体客户信息没有找到!");
			return null;
		}

		String Industry = getIndustry(this.mLCGrpContSchema.getBusinessType());
		String InsuredState = insuredSchema.getInsuredStat();
		// 其他信息
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("samePersonFlag", "0");
		tTransferData.setNameAndValue("GrpImport", 1);
		tTransferData.setNameAndValue("RetireRate", retire);
		tTransferData.setNameAndValue("AvgAge", avgage);
		tTransferData.setNameAndValue("InsuredNum", InsuredNum);
		tTransferData.setNameAndValue("Industry", Industry);
		tTransferData.setNameAndValue("InsuredState", InsuredState);
		// 增加“领取频次”要素
		// tTransferData.setNameAndValue("GrpGetIntv", mGrpGetIntv);

		tNewVData.add(mGlobalInput);
		tNewVData.add(tLCGrpPolSchema);
		tNewVData.add(tLMRiskAppSchema);
		tNewVData.add(tLMRiskSchema);
		tNewVData.add(ttLDGrpSchema);
		tNewVData.add(m_GrpPolImpInfo.getLCGrpAppntSchema());
		tNewVData.add(tLCPolSchema);
		tNewVData.add(tTransferData);
		tNewVData.add(this.mainPolBL);
		tNewVData.add(this.mLCGrpContSchema);
		tNewVData.add(tmpMap);

		return tNewVData;
	}

	/**
	 * 生成保单信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCContSchema getLCContSchema(LCGrpPolSchema aLCGrpPolSchema, LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {

		LCContSchema tLCContSchema = new LCContSchema();
		// 设置主被保险人信息
		tLCContSchema.setContNo(aLCGrpSubInsuredImportSchema.getSeqNo());
		tLCContSchema.setInsuredNo(aLCGrpSubInsuredImportSchema.getCustomerNo());
		tLCContSchema.setInsuredName(aLCGrpSubInsuredImportSchema.getName());
		tLCContSchema.setInsuredSex(aLCGrpSubInsuredImportSchema.getSex());
		tLCContSchema.setInsuredBirthday(aLCGrpSubInsuredImportSchema.getBirthday());
		tLCContSchema.setInsuredIDNo(aLCGrpSubInsuredImportSchema.getIDNo());
		tLCContSchema.setInsuredIDType(aLCGrpSubInsuredImportSchema.getIDType());
		tLCContSchema.setBankAccNo(aLCGrpSubInsuredImportSchema.getBankAccNo());
		tLCContSchema.setBankCode(aLCGrpSubInsuredImportSchema.getBankCode());
		tLCContSchema.setAccName(aLCGrpSubInsuredImportSchema.getAccName());
		tLCContSchema.setPayIntv(this.mLCGrpContSchema.getPayIntv());
		tLCContSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
		tLCContSchema.setContType("2");
		tLCContSchema.setAgentCode(aLCGrpPolSchema.getAgentCode());
		tLCContSchema.setAgentCom(aLCGrpPolSchema.getAgentCom());
		tLCContSchema.setAppntName(aLCGrpPolSchema.getGrpName());
		tLCContSchema.setAppntNo(aLCGrpPolSchema.getCustomerNo());
		tLCContSchema.setPrtNo(aLCGrpPolSchema.getPrtNo());
		tLCContSchema.setGrpContNo(aLCGrpPolSchema.getGrpContNo());
		tLCContSchema.setOperator(mGlobalInput.Operator);
		tLCContSchema.setManageCom(mGlobalInput.ManageCom);
		tLCContSchema.setMakeDate(aLCGrpPolSchema.getMakeDate());
		tLCContSchema.setMakeTime(aLCGrpPolSchema.getMakeTime());
		tLCContSchema.setModifyDate(aLCGrpPolSchema.getModifyDate());
		tLCContSchema.setModifyTime(aLCGrpPolSchema.getModifyTime());
		tLCContSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
		;
		// tLCContSchema.setHandlerDate(tLCInsuredListSchema.getHandlerDate());
		// tLCContSchema.setHandler(tLCInsuredListSchema.getHandler());
		// tLCContSchema.setHandlerPrint(tLCInsuredListSchema.getHandlerPrint());

		return tLCContSchema;

	}

	/**
	 * 生成被保人信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCInsuredSchema getLInsured(LCGrpPolSchema aLCGrpPolSchema, LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {

		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setInsuredNo(aLCGrpSubInsuredImportSchema.getCustomerNo());
		tLCInsuredSchema.setName(aLCGrpSubInsuredImportSchema.getName());
		tLCInsuredSchema.setSex(aLCGrpSubInsuredImportSchema.getSex());
		tLCInsuredSchema.setBirthday(aLCGrpSubInsuredImportSchema.getBirthday());
		tLCInsuredSchema.setIDType(aLCGrpSubInsuredImportSchema.getIDType());
		tLCInsuredSchema.setIDNo(aLCGrpSubInsuredImportSchema.getIDNo());
		tLCInsuredSchema.setOccupationCode(aLCGrpSubInsuredImportSchema.getOccupationCode());
		tLCInsuredSchema.setOccupationType(aLCGrpSubInsuredImportSchema.getOccupationType());
		tLCInsuredSchema.setContPlanCode(aLCGrpSubInsuredImportSchema.getContPlanCode());
		tLCInsuredSchema.setGrpContNo(aLCGrpSubInsuredImportSchema.getGrpContNo());

		return tLCInsuredSchema;

	}

	/**
	 * 生成险种信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCPolSchema getLCPolSchema(LCGrpPolSchema aLCGrpPolSchema, LCContSchema contSchema, LCInsuredSchema insuredSchema, String aContPlanCode) {

		LCPolSchema tLCPolSchema = new LCPolSchema();

		if (aLCGrpPolSchema == null) {
			buildError("formatLCPol", "找不到指定险种所对应的集体保单，险种为：" + aLCGrpPolSchema.getRiskCode());
			return null;
		}

		tLCPolSchema.setPrtNo(aLCGrpPolSchema.getPrtNo());
		tLCPolSchema.setGrpPolNo(aLCGrpPolSchema.getGrpPolNo());
		if ("Y".equals(tLCPolSchema.getSpecifyValiDate()) && tLCPolSchema.getCValiDate() != null) {
			// 如果磁盘倒入指定生效日期，且生效日期字段有值,那么就用 生效日期字段的值
		} else {
			// 否则一律用集体单的生效日期
			tLCPolSchema.setCValiDate(contSchema.getCValiDate());
		}

		tLCPolSchema.setManageCom(aLCGrpPolSchema.getManageCom());
		tLCPolSchema.setSaleChnl(aLCGrpPolSchema.getSaleChnl());
		tLCPolSchema.setAgentCom(aLCGrpPolSchema.getAgentCom());
		tLCPolSchema.setAgentCode(aLCGrpPolSchema.getAgentCode());
		tLCPolSchema.setAgentGroup(aLCGrpPolSchema.getAgentGroup());
		tLCPolSchema.setAgentCode1(aLCGrpPolSchema.getAgentCode1());
		tLCPolSchema.setGrpContNo(aLCGrpPolSchema.getGrpContNo());
		tLCPolSchema.setPayIntv(aLCGrpPolSchema.getPayIntv());
		// tLCPolSchema.set
		tLCPolSchema.setContType("2");
		// tLCPolSchema.setPolTypeFlag("2");
		tLCPolSchema.setPolTypeFlag(contSchema.getPolType());

		// 根据合同表帐户类型标志，对应险种帐户类型标志。
		tLCPolSchema.setAccType(contSchema.getAccType());
		// --------------------------------------

		tLCPolSchema.setInsuredPeoples(contSchema.getPeoples());
		tLCPolSchema.setRiskCode(aLCGrpPolSchema.getRiskCode());
		tLCPolSchema.setContNo(insuredSchema.getContNo());
		tLCPolSchema.setInsuredSex(insuredSchema.getSex());
		tLCPolSchema.setInsuredBirthday(insuredSchema.getBirthday());
		tLCPolSchema.setInsuredName(insuredSchema.getName());
		tLCPolSchema.setContPlanCode(aContPlanCode);
		tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());

		return tLCPolSchema;

	}

	/**
	 * 通过duty设置一些lcpol的元素
	 * 
	 * @param tLCPolSchema LCPolSchema
	 * @param dutySchema LCDutySchema
	 */
	private void setPolInfoByDuty(LCPolSchema tLCPolSchema, LCDutySchema dutySchema) {

		tLCPolSchema.setInsuYear(dutySchema.getInsuYear());
		tLCPolSchema.setInsuYearFlag(dutySchema.getInsuYearFlag());
		tLCPolSchema.setPrem(dutySchema.getPrem());
		tLCPolSchema.setAmnt(dutySchema.getAmnt());
		tLCPolSchema.setPayEndYear(dutySchema.getPayEndYear());
		tLCPolSchema.setPayEndYearFlag(dutySchema.getPayEndYearFlag());
		tLCPolSchema.setGetYear(dutySchema.getGetYear());
		tLCPolSchema.setGetYearFlag(dutySchema.getGetYearFlag());
		tLCPolSchema.setAcciYear(dutySchema.getAcciYear());
		tLCPolSchema.setAcciYearFlag(dutySchema.getAcciYearFlag());
		tLCPolSchema.setMult(dutySchema.getMult());
		// 计算方向,在按分数卖的保单，切记算方向为O的时候
		if (dutySchema.getMult() > 0 && "O".equals(StrTool.cTrim(dutySchema.getPremToAmnt()))) {
			tLCPolSchema.setPremToAmnt(dutySchema.getPremToAmnt());
		}
		tLCPolSchema.setStandbyFlag1(dutySchema.getStandbyFlag1());
		tLCPolSchema.setStandbyFlag2(dutySchema.getStandbyFlag2());
		tLCPolSchema.setStandbyFlag3(dutySchema.getStandbyFlag3());
	}

	private LCInsuredSet getLCInsured(LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {
		LCInsuredSet tInsuredSet = new LCInsuredSet();
		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setContNo(aLCGrpSubInsuredImportSchema.getSeqNo());
		tLCInsuredSchema.setPrtNo(aLCGrpSubInsuredImportSchema.getPrtNo());
		tLCInsuredSchema.setName(aLCGrpSubInsuredImportSchema.getName());
		tLCInsuredSchema.setSex(aLCGrpSubInsuredImportSchema.getSex());
		tLCInsuredSchema.setInsuredStat(aLCGrpSubInsuredImportSchema.getRetire());
		tLCInsuredSchema.setBirthday(aLCGrpSubInsuredImportSchema.getBirthday());
		tLCInsuredSchema.setIDType(aLCGrpSubInsuredImportSchema.getIDType());
		tLCInsuredSchema.setIDNo(aLCGrpSubInsuredImportSchema.getIDNo());
		tLCInsuredSchema.setContPlanCode(aLCGrpSubInsuredImportSchema.getContPlanCode());
		tLCInsuredSchema.setOccupationType(aLCGrpSubInsuredImportSchema.getOccupationType());
		tLCInsuredSchema.setBankAccNo(aLCGrpSubInsuredImportSchema.getBankAccNo());
		tLCInsuredSchema.setBankCode(aLCGrpSubInsuredImportSchema.getBankCode());
		tLCInsuredSchema.setAccName(aLCGrpSubInsuredImportSchema.getAccName());
		tLCInsuredSchema.setRelationToMainInsured(aLCGrpSubInsuredImportSchema.getRelation());
		tLCInsuredSchema.setOthIDNo(aLCGrpSubInsuredImportSchema.getOthIDNo());
		tLCInsuredSchema.setOthIDType(aLCGrpSubInsuredImportSchema.getOthIDType());
		tLCInsuredSchema.setCreditGrade("0");
		tInsuredSet.add(tLCInsuredSchema);
		return tInsuredSet;
	}

	/**
	 * 提交到proposalBL ,做数据生成和准备
	 * 
	 * @param tNewVData VData
	 * @param tLCInsuredListSet LCInsuredListSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @return boolean
	 */
	private MMap submiDatattoProposalBL(VData tNewVData, LCContPlanRiskSchema tLCContPlanRiskSchema) {
		// tNewVData 里面还有一些要创建的信息，因此需要重新获得
		MMap map = (MMap) tNewVData.getObjectByObjectName("MMap", 0);
		if (map == null) {
			map = new MMap();
		}
		String strID = (String) ((TransferData) tNewVData.getObjectByObjectName("TransferData", 0)).getValueByName("ID");
		ProposalBL tProposalBL = new ProposalBL();
		if (!tProposalBL.PrepareSubmitData(tNewVData, "INSERT||PROPOSAL")) {
			this.mErrors.copyAllErrors(tProposalBL.mErrors);
			// m_GrpPolImpInfo.logError(strID, tLCPolSchema, false,
			// this.mErrors,
			// mGlobalInput);
			return null;
		} else {
			VData pData = tProposalBL.getSubmitResult();
			if (pData == null) {
				CError.buildErr(this, "保单提交计算失败!");
				return null;
			}

			// LCContSchema rcontSchema =
			// (LCContSchema)pData.getObjectByObjectName("LCContSchema",2);
			LCPolSchema rPolSchema = (LCPolSchema) pData.getObjectByObjectName("LCPolSchema", 0);
			if (tLCContPlanRiskSchema != null && tLCContPlanRiskSchema.getRiskAmnt() > 0) {
				rPolSchema.setAmnt(tLCContPlanRiskSchema.getRiskAmnt());
			}
			LCGrpPolSchema rGrpPol = (LCGrpPolSchema) pData.getObjectByObjectName("LCGrpPolSchema", 0);
			LCGrpContSchema rGrpCont = (LCGrpContSchema) pData.getObjectByObjectName("LCGrpContSchema", 0);
			LCPremBLSet rPremBLSet = (LCPremBLSet) pData.getObjectByObjectName("LCPremBLSet", 0);

			if (rPremBLSet == null) {
				CError.buildErr(this, "保单提交计算保费项数据准备有误!");
				return null;
			}
			LCPremSet rPremSet = new LCPremSet();
			for (int i = 1; i <= rPremBLSet.size(); i++) {
				rPremSet.add(rPremBLSet.get(i));
			}

			LCDutyBLSet rDutyBLSet = (LCDutyBLSet) pData.getObjectByObjectName("LCDutyBLSet", 0);
			if (rDutyBLSet == null) {
				CError.buildErr(this, "保单提交计算责任项数据准备有误!");
				return null;
			}
			LCDutySet rDutySet = new LCDutySet();
			for (int i = 1; i <= rDutyBLSet.size(); i++) {
				rDutySet.add(rDutyBLSet.get(i));
			}

			LCGetBLSet rGetBLSet = (LCGetBLSet) pData.getObjectByObjectName("LCGetBLSet", 0);
			if (rGetBLSet == null) {
				CError.buildErr(this, "保单提交计算给付项数据准备有误!");
				return null;
			}
			LCGetSet rGetSet = new LCGetSet();
			for (int i = 1; i <= rGetBLSet.size(); i++) {
				rGetSet.add(rGetBLSet.get(i));
			}

			LCBnfBLSet rBnfBLSet = (LCBnfBLSet) pData.getObjectByObjectName("LCBnfBLSet", 0);
			LCBnfSet rBnfSet = new LCBnfSet();
			if (rBnfBLSet != null) {
				for (int i = 1; i <= rBnfBLSet.size(); i++) {
					rBnfSet.add(rBnfBLSet.get(i));
				}
			}
			LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet) pData.getObjectByObjectName("LCInsuredRelatedSet", 0);
			// if ( tLCInsuredRelatedSet!=null && LCInsuredRelatedSet.size()>0)

			// 更新个单合同金额相关，更新的时候同时更新缓存中的数据
			LCContSet tContSet = new LCContSet();

			// 更新集体险种金额相关
			String riskCode = rPolSchema.getRiskCode();
			LCGrpPolSchema tLCGrpPolSchema = m_GrpPolImpInfo.findLCGrpPolSchema(riskCode);
			if (!StrTool.cTrim(rPolSchema.getAppFlag()).equals("2")) {
				tLCGrpPolSchema.setPrem(PubFun.setPrecision(tLCGrpPolSchema.getPrem() + rPolSchema.getPrem(), "0.00"));
				tLCGrpPolSchema.setAmnt(tLCGrpPolSchema.getAmnt() + rPolSchema.getAmnt());
			}
			tLCGrpPolSchema.setPayEndDate(PubFun.getLaterDate(tLCGrpPolSchema.getPayEndDate(), rPolSchema.getPayEndDate()));
			tLCGrpPolSchema.setPaytoDate(PubFun.getLaterDate(tLCGrpPolSchema.getPaytoDate(), rPolSchema.getPaytoDate()));
			tLCGrpPolSchema.setFirstPayDate(PubFun.getBeforeDate(tLCGrpPolSchema.getFirstPayDate(), rPolSchema.getFirstPayDate()));
			rGrpPol.setPrem(tLCGrpPolSchema.getPrem());
			rGrpPol.setAmnt(tLCGrpPolSchema.getAmnt());
			rGrpPol.setPayEndDate(tLCGrpPolSchema.getPayEndDate());
			rGrpPol.setPaytoDate(tLCGrpPolSchema.getPaytoDate());
			rGrpPol.setFirstPayDate(tLCGrpPolSchema.getFirstPayDate());
			rGrpPol.setCValiDate(rPolSchema.getCValiDate());
			// 更新团单合同金额相关
			LCGrpContSchema sGrpContSchema = m_GrpPolImpInfo.getLCGrpContSchema();
			// if (!StrTool.cTrim(rPolSchema.getAppFlag()).equals("2")) {
			// sGrpContSchema.setPrem(PubFun.setPrecision(sGrpContSchema.
			// getPrem() +
			// rPolSchema.getPrem(), "0.00"));
			// sGrpContSchema.setAmnt(sGrpContSchema.getAmnt() +
			// rPolSchema.getAmnt());
			// }
			rGrpCont.setPrem(sGrpContSchema.getPrem());
			rGrpCont.setSaleChnlDetail("01");
			rGrpCont.setAmnt(sGrpContSchema.getAmnt());

			rPolSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
			map.put(tContSet, "UPDATE");
			map.put(rPolSchema.getSchema(), "INSERT");
			map.put(rPremSet, "INSERT");
			map.put(rDutySet, "INSERT");
			map.put(rGetSet, "INSERT");
			map.put(rBnfSet, "INSERT");
			map.put(tLCInsuredRelatedSet, "INSERT");
			// 缓存保存的主险保单信息
			m_GrpPolImpInfo.cachePolInfo(strID, rPolSchema);
		}

		return map;
	}

	/**
	 * getIndustry
	 * 
	 * @param tIndustry String
	 * @return String
	 */
	private String getIndustry(String tIndustry) {
		String sql = "select codealias from ldcode where codetype='businesstype' and code='" + tIndustry + "'";
		System.out.println("+++++++" + (new ExeSQL()).getOneValue(sql));
		if (StrTool.cTrim((new ExeSQL()).getOneValue(sql)).equals("")) {
			return "";
		} else {
			return (new ExeSQL()).getOneValue(sql);
		}
	}

	/**
	 * 创建错误日志
	 * 
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ParseGuideIn";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		this.logErrors.addOneError(cError);
	}

	/**
	 * 处理一个人
	 * 
	 * @return MMap
	 */
	private MMap dealOnePerson(LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema, HashMap aContPlanRisk,
			LCContPlanDutyParamSet aLCContPlanDutyParamSet, LCGrpPolSet aLCGrpPolSet, HashMap aPremParams) {
		MMap tPersonMMap = new MMap();

		String tInsuredNo = "";
		LCContPlanRiskSet tLCContPlanRiskSet = (LCContPlanRiskSet) aContPlanRisk.get(aLCGrpSubInsuredImportSchema.getContPlanCode());
		if (tLCContPlanRiskSet == null || tLCContPlanRiskSet.size() < 1) {
			aLCGrpSubInsuredImportSchema.setCalState("03");
			aLCGrpSubInsuredImportSchema.setCalRemark("获取该保障计划下险种信息失败！");
			tPersonMMap.put(aLCGrpSubInsuredImportSchema, SysConst.UPDATE);
			return tPersonMMap;
		}
		LCBnfSet tLCBnfSet = new LCBnfSet();
		LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
		LCInsuredSet tInsuredSet = getLCInsured(aLCGrpSubInsuredImportSchema);
		if (!m_GrpPolImpInfo.init(aLCGrpSubInsuredImportSchema.getBatchNo(), this.mGlobalInput, tInsuredSet, tLCBnfSet, tLCInsuredRelatedSet,
				mLCGrpContSchema)) {
			aLCGrpSubInsuredImportSchema.setCalState("03");
			aLCGrpSubInsuredImportSchema.setCalRemark("初始化算费信息失败！");
			tPersonMMap.put(aLCGrpSubInsuredImportSchema, SysConst.UPDATE);
			return tPersonMMap;
		}
		LCContPlanRiskSet mainPlanRiskSet = new LCContPlanRiskSet();
		LCContPlanRiskSet subPlanRiskSet = new LCContPlanRiskSet();
		LCContPlanRiskSchema contPlanRiskSchema = null;
		for (int t = 1; t <= tLCContPlanRiskSet.size(); t++) {
			contPlanRiskSchema = tLCContPlanRiskSet.get(t);
			if (contPlanRiskSchema.getRiskCode().equals(contPlanRiskSchema.getMainRiskCode())) {
				mainPlanRiskSet.add(contPlanRiskSchema);
				mainPolBL.setRiskCode(contPlanRiskSchema.getRiskCode());
			} else {
				subPlanRiskSet.add(contPlanRiskSchema);
			}

		}
		mainPlanRiskSet.add(subPlanRiskSet);

		String tAllCalParam = aLCGrpSubInsuredImportSchema.getContPlanCode();

		HashMap tRiskPremPlanCalMap = new HashMap();
		for (int j = 1; j <= mainPlanRiskSet.size(); j++) {
			String tempRiskCode = mainPlanRiskSet.get(j).getRiskCode();
			String tCalParam = getCalParam(tempRiskCode, aLCContPlanDutyParamSet, aLCGrpSubInsuredImportSchema);
			tRiskPremPlanCalMap.put(tempRiskCode, tCalParam);
			tAllCalParam = tAllCalParam + "|" + tCalParam;
		}
		String tPrem = "";
		tPrem = (String) aPremParams.get(tAllCalParam);
		if ("".equals(StrTool.cTrim(tPrem))) {
			String tPremSql = "select prem from LCGrpPremPlan where grpcontno = '" + mGrpContNo + "' and PremPlanCalFlag = '" + tAllCalParam + "' ";
			tPrem = new ExeSQL().getOneValue(tPremSql);
		}

		if ("".equals(StrTool.cTrim(tPrem))) {// 没有匹配的费用类别，需算费
		// 根据险种计划生成险种保单相关信息
			double sumPrem = 0;
			LCGrpRiskPremPlanSet tLCGrpRiskPremPlanSet = new LCGrpRiskPremPlanSet();
			for (int j = 1; j <= mainPlanRiskSet.size(); j++) {
				LCContPlanRiskSchema tLCContPlanRiskSchema = mainPlanRiskSet.get(j);
				String tRiskCode = mainPlanRiskSet.get(j).getRiskCode();
				LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
				for (int m = 1; m <= aLCGrpPolSet.size(); m++) {
					if (tRiskCode.equals(aLCGrpPolSet.get(m).getRiskCode())) {
						tLCGrpPolSchema = aLCGrpPolSet.get(m);
						break;
					}
				}

				/** 添加在职退休比,平均年龄等计算要素 */
				// retire = getRetire(mainPlanRiskSet.get(j));
				// avgage = getInsuredAvgAge(mainPlanRiskSet.get(j));
				// InsuredNum = getInsuredNum(mainPlanRiskSet.get(j));
				VData tData = prepareContPlanData(aLCGrpSubInsuredImportSchema, tLCGrpPolSchema, tLCContPlanRiskSchema);
				if (tData == null) {
					CError tError = new CError();
					tError.moduleName = "ParseGuideInB";
					tError.functionName = "prepareData";
					tError.errorMessage = "大团单算费时，被保人：" + aLCGrpSubInsuredImportSchema.getName() + ",险种：" + tRiskCode + "，获取算费数据失败。";
					this.mErrors.addOneError(tError);
					return null;
				}

				// 提交生成数据
				MMap oneRisk = submiDatattoProposalBL(tData, tLCContPlanRiskSchema);
				if (oneRisk == null) {
					// 失败
					CError tError = new CError();
					tError.moduleName = "ParseGuideInB";
					tError.functionName = "prepareData";
					tError.errorMessage = "大团单算费时，被保人：" + aLCGrpSubInsuredImportSchema.getName() + ",险种：" + tRiskCode + "，算费失败。";
					this.mErrors.addOneError(tError);
					return null;
				} else {
					LCPolSchema tempLCPolSchema = (LCPolSchema) oneRisk.getObjectByObjectName("LCPolSchema", 0);
					tInsuredNo = tempLCPolSchema.getInsuredNo();
					LCGrpRiskPremPlanSchema tLCGrpRiskPremPlanShema = new LCGrpRiskPremPlanSchema();
					String tRiskPremSeqNo = PubFun1.CreateMaxNo("RiskPremSeqNo", 20);
					tLCGrpRiskPremPlanShema.setSeqNo(tRiskPremSeqNo);
					tLCGrpRiskPremPlanShema.setPremPlanCalFlag(tAllCalParam);
					tLCGrpRiskPremPlanShema.setRiskPremPlanCalFlag((String) tRiskPremPlanCalMap.get(tRiskCode));
					tLCGrpRiskPremPlanShema.setRiskCode(tRiskCode);
					tLCGrpRiskPremPlanShema.setPrem(tempLCPolSchema.getPrem());
					tLCGrpRiskPremPlanShema.setOperator(mGlobalInput.Operator);
					tLCGrpRiskPremPlanShema.setMakeDate(PubFun.getCurrentDate());
					tLCGrpRiskPremPlanShema.setMakeTime(PubFun.getCurrentTime());
					tLCGrpRiskPremPlanShema.setModifyDate(PubFun.getCurrentDate());
					tLCGrpRiskPremPlanShema.setModifyTime(PubFun.getCurrentTime());
					tLCGrpRiskPremPlanSet.add(tLCGrpRiskPremPlanShema);

					sumPrem = sumPrem + tempLCPolSchema.getPrem();

					System.out.println("被保人：" + aLCGrpSubInsuredImportSchema.getName() + ",险种：" + tRiskCode + "，算费成功！");
				}
			}

			String tPremPlanCode = PubFun1.CreateMaxNo("PremPlanCode", 20);
			LCGrpPremPlanSchema tLCGrpPremPlanSchema = new LCGrpPremPlanSchema();
			tLCGrpPremPlanSchema.setPremPlanCode(tPremPlanCode);
			tLCGrpPremPlanSchema.setGrpContNo(mGrpContNo);
			tLCGrpPremPlanSchema.setPremPlanCalFlag(tAllCalParam);
			tLCGrpPremPlanSchema.setPrem(sumPrem);
			tLCGrpPremPlanSchema.setOperator(mGlobalInput.Operator);
			tLCGrpPremPlanSchema.setMakeDate(PubFun.getCurrentDate());
			tLCGrpPremPlanSchema.setMakeTime(PubFun.getCurrentTime());
			tLCGrpPremPlanSchema.setModifyDate(PubFun.getCurrentDate());
			tLCGrpPremPlanSchema.setModifyTime(PubFun.getCurrentTime());

			tPersonMMap.put(tLCGrpRiskPremPlanSet, SysConst.INSERT);
			tPersonMMap.put(tLCGrpPremPlanSchema, SysConst.INSERT);

			if (!aPremParams.containsKey(tAllCalParam)) {
				aPremParams.put(tAllCalParam, String.valueOf(sumPrem));
			}

		}

		if ("".equals(StrTool.cTrim(tInsuredNo))) {
			String tInsuredNoSQL = "select customerno from ldperson where name = '" + aLCGrpSubInsuredImportSchema.getName() + "' " + "and sex = '"
					+ aLCGrpSubInsuredImportSchema.getSex() + "' " + "and birthday = '" + aLCGrpSubInsuredImportSchema.getBirthday() + "' "
					+ "and idtype = '" + aLCGrpSubInsuredImportSchema.getIDType() + "' " + "and idno = '" + aLCGrpSubInsuredImportSchema.getIDNo()
					+ "' ";
			tInsuredNo = new ExeSQL().getOneValue(tInsuredNoSQL);
			if ("".equals(StrTool.cTrim(tInsuredNo))) {
				tInsuredNo = PubFun1.CreateMaxNo("CustomerNo", "SN");
				LDPersonSchema tLDPersonSchema = new LDPersonSchema();
				tReflections.transFields(tLDPersonSchema, aLCGrpSubInsuredImportSchema);
				tLDPersonSchema.setCustomerNo(tInsuredNo);
				tLDPersonSchema.setOperator(mGlobalInput.Operator);
				tLDPersonSchema.setMakeDate(PubFun.getCurrentDate());
				tLDPersonSchema.setMakeTime(PubFun.getCurrentTime());
				tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
				tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());

				tPersonMMap.put(tLDPersonSchema, SysConst.INSERT);
			}
		}

		aLCGrpSubInsuredImportSchema.setCustomerNo(tInsuredNo);
		aLCGrpSubInsuredImportSchema.setCalState("01");
		aLCGrpSubInsuredImportSchema.setCalRemark("算费成功！");

		LCGrpSubInsuredSchema tLCGrpSubInsuredSchema = new LCGrpSubInsuredSchema();
		tReflections.transFields(tLCGrpSubInsuredSchema, aLCGrpSubInsuredImportSchema);
		tLCGrpSubInsuredSchema.setCustomerNo(tInsuredNo);
		tLCGrpSubInsuredSchema.setPremPlanCode(tAllCalParam);
		tLCGrpSubInsuredSchema.setOperator(mGlobalInput.Operator);
		tLCGrpSubInsuredSchema.setMakeDate(PubFun.getCurrentDate());
		tLCGrpSubInsuredSchema.setMakeTime(PubFun.getCurrentTime());
		tLCGrpSubInsuredSchema.setModifyDate(PubFun.getCurrentDate());
		tLCGrpSubInsuredSchema.setModifyTime(PubFun.getCurrentTime());

		tPersonMMap.put(aLCGrpSubInsuredImportSchema, SysConst.UPDATE);
		tPersonMMap.put(tLCGrpSubInsuredSchema, SysConst.INSERT);
		return tPersonMMap;
	}

	private boolean saveAllPerson(MMap aALLMMap) {
		VData mInputData = new VData();
		mInputData.add(aALLMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ParseGuideInB Submit...");
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ParseGuideInB";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * getRetire
	 * 
	 * @return Double
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 */
	private String getRetire(LCContPlanRiskSchema tLCContPlanRiskSchema) {
		String insured = "select count(1) from LCGrpSubInsuredImport where grpcontno='" + this.mGrpContNo + "' and contplancode = '"
				+ tLCContPlanRiskSchema.getContPlanCode() + "'";
		String no = (new ExeSQL()).getOneValue(insured);
		System.out.println("ExeSQL : " + insured);
		if (StrTool.cTrim(no).equals("") || StrTool.cTrim(no).equals("null") || StrTool.cTrim(no).equals("0")) {
			buildError("getRetire", "参保人数为空！");
			return "0";
		}
		String sql = "select DECIMAL((select count(1) from LCGrpSubInsuredImport where retire='2' and grpcontno='" + this.mGrpContNo
				+ "' and contplancode='" + tLCContPlanRiskSchema.getContPlanCode() + "'),12,2)/" + no + " from dual";
		String retire = (new ExeSQL()).getOneValue(sql);
		return retire;
	}

	/**
	 * getInsuredAvgAge
	 * 
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @return String
	 */
	private String getInsuredAvgAge(LCContPlanRiskSchema tLCContPlanRiskSchema) {
		String strSql = "select round(avg((to_date(a.CValiDate)-to_date(b.birthday))/365),2) from "
				+ " LCGrpSubInsuredImport b,lcgrpcont a where a.grpcontno=b.grpcontno and a.grpcontno='" + this.mGrpContNo + "'and b.contplancode='"
				+ tLCContPlanRiskSchema.getContPlanCode() + "'";
		String AvgAge = (new ExeSQL()).getOneValue(strSql);
		return AvgAge;
	}

	/**
	 * getInsuredNum
	 * 
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @return String
	 */
	private String getInsuredNum(LCContPlanRiskSchema tLCContPlanRiskSchema) {
		String strSql = "select count(1) from LCGrpSubInsuredImport where grpcontno='" + this.mGrpContNo + "' and contplancode='"
				+ tLCContPlanRiskSchema.getContPlanCode() + "'";

		return (new ExeSQL()).getOneValue(strSql);
	}

	public static void main(String[] arrs) {
		ParseGuideInB tParseGuideInB = new ParseGuideInB();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "001";
		tGlobalInput.ManageCom = "86110000";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", "1400308166");

		VData cInputData = new VData();
		cInputData.add(tGlobalInput);
		cInputData.add(tTransferData);

		tParseGuideInB.submitData(cInputData, "");

	}
}
