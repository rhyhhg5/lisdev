package com.sinosoft.lis.lgrp;

import com.sinosoft.utility.VData;

/**
 * 大团单校验处理主线程控制类
 * 
 * @author 张成轩
 */
public class LGrpCheckMainThread implements Runnable {

	private VData mVData;

	private LGrpLock lock;

	/**
	 * 构造函数
	 * 
	 * @param tVData 
	 * @param lock 业务锁处理对象
	 */
	public LGrpCheckMainThread(VData tVData, LGrpLock lock) {
		this.mVData = tVData;
		this.lock = lock;
	}

	public void run() {
		
		// 线程起始校验处理		
		if (beginTread()) {
			try {
				long tStart2 = System.nanoTime();
				LGrpCheckInsuredBL tLGrpCheckInsuredBL = new LGrpCheckInsuredBL();
				tLGrpCheckInsuredBL.submitData(mVData, "");

				long tEnd2 = System.nanoTime();
				System.out.println("大团单被保人校验，耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
			} catch (RuntimeException e) {
				e.printStackTrace();
			} finally {
				// 线程完毕处理
				finalThread();
			}
		}
	}

	/**
	 * 线程起始校验处理
	 * 
	 * @return
	 */
	public boolean beginTread() {
		System.out.println("----校验锁定-----");
		return lock.checklock();
	}
	
	/**
	 * 线程完毕处理
	 */
	public void finalThread() {
		System.out.println("----解除锁定-----");
		lock.unlock();
	}
}
