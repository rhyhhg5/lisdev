package com.sinosoft.lis.lgrp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;

/**
 * 大团单业务锁处理类
 * 
 * @author 张成轩
 */
public class LGrpLock {

	private int lockType;

	private String mPrtNo;

	/**
	 * @param lockType 锁标记 1-导入 2-校验 3-算费 4-算费完毕
	 * @param prtNo
	 */
	public LGrpLock(int lockType, String prtNo) {
		this.lockType = lockType;
		this.mPrtNo = prtNo;
	}

	/**
	 * 对数据进行锁定
	 * 
	 * @return
	 */
	public boolean lock() {

		String updateSql = "update lwmission set missionprop10='" + lockType + "' where activityid='0000002098' and missionprop1='" + mPrtNo
				+ "' and (missionprop10 is null or missionprop10 = '0')";

		int updateCount = execUpdate(updateSql);

		if (updateCount < 1) {
			return false;
		} else if (updateCount > 1) {
			System.out.println("-----加锁了多条数据？暂时不返回false，出现一定要改-----");
		}
		return true;
	}

	/**
	 * 检查锁定信息是否正常 尽量在提数数据前处理
	 * 
	 * @return
	 */
	public boolean checklock() {
		String querySql = "select 1 from lwmission where activityid='0000002098' and missionprop1='" + mPrtNo + "'  and missionprop10='" + lockType
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		String check = tExeSQL.getOneValue(querySql);
		if ("1".equals(check)) {
			return true;
		}
		return false;
	}

	/**
	 * 接触锁定 提交成功后处理
	 * 
	 * @return
	 */
	public boolean unlock() {
		String updateSql = "update lwmission set missionprop10='0' where activityid='0000002098' and missionprop1='" + mPrtNo
				+ "' and missionprop10='" + lockType + "'";

		int updateCount = execUpdate(updateSql);

		if (updateCount < 1) {
			return false;
		} else if (updateCount > 1) {
			System.out.println("-----加锁了多条数据？暂时不返回false，出现一定要改-----");
		}
		return true;
	}

	public String getLockType() {
		String querySql = "select case missionprop10 when '1' then '被保人导入处理中' when '2' then '被保人校验处理中' when '3' then '被保人算费处理中' when '4' then '算费完毕处理中' end from lwmission where activityid='0000002098' and missionprop1='" + mPrtNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		String type = tExeSQL.getOneValue(querySql);
		
		if(type == null){
			return "";
		} 
		return type;
	}

	/**
	 * 更新数据，返回更新条数
	 * 
	 * @param sql
	 * @return
	 */
	private int execUpdate(String sql) {

		// 更新条数
		int updateCount = -1;

		System.out.println("SQL: " + sql);

		Connection con = DBConnPool.getConnection();
		PreparedStatement pstmt = null;

		try {
			pstmt = con.prepareStatement(StrTool.GBKToUnicode(sql), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
			updateCount = pstmt.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			// 更新异常是返回-1
			updateCount = -1;
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return updateCount;
	}
}
