package com.sinosoft.lis.lgrp;

import java.util.HashMap;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCGrpPremPlanSchema;
import com.sinosoft.lis.schema.LCGrpRiskPremPlanSchema;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.tb.ContPlanQryBL;
import com.sinosoft.lis.tb.GrpPolImpInfo;
import com.sinosoft.lis.tb.ProposalBL;
import com.sinosoft.lis.vbl.LCBnfBLSet;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCGrpRiskPremPlanSet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 大团单算费处理类
 * 
 * @author 张成轩
 *
 */
public class LGrpCalGetPersonPrem {

	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	private String mGrpContNo;

	private GlobalInput mGlobalInput = new GlobalInput();

	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	private HashMap mContPlanRisk = new HashMap();

	private GrpPolImpInfo m_GrpPolImpInfo = new GrpPolImpInfo();

	/**
	 * 初始化保单相关信息，保存到缓存中
	 */
	public LGrpCalGetPersonPrem(TransferData tTransferData) {

		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpContNo);
		if (tLCGrpContDB.getInfo()) {
			mLCGrpContSchema = tLCGrpContDB.getSchema();
		}

		mLCGrpPolSet = new LCGrpPolDB().executeQuery("select * from lcgrppol where grpcontno = '" + mGrpContNo + "' ");

		SSRS tContPlanSSRS = new ExeSQL().execSQL("select distinct contplancode from LCGrpSubInsuredImport where grpcontno = '" + mGrpContNo
				+ "' and CalState = '01' order by ContPlanCode ");
		LCContPlanRiskSet tAllLCContPlanRiskSet = new LCContPlanRiskDB().executeQuery("select * from LCContPlanRisk where grpcontno = '" + mGrpContNo
				+ "' and contplancode != '11' order by contplancode,riskcode ");
		for (int i = 1; i <= tContPlanSSRS.MaxRow; i++) {
			String tContPlanCode = tContPlanSSRS.GetText(i, 1);
			LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
			for (int j = 1; j <= tAllLCContPlanRiskSet.size(); j++) {
				LCContPlanRiskSchema tLCContPlanRiskSchema = tAllLCContPlanRiskSet.get(j);
				if (tContPlanCode.equals(tLCContPlanRiskSchema.getContPlanCode())) {
					mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
				}
			}
			mContPlanRisk.put(tContPlanCode, mLCContPlanRiskSet);
		}
	}
	
	/**
	 * 处理一个人
	 * 
	 * @return MMap
	 */
	public MMap dealData(LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema, String calSign) {
		MMap tPersonMMap = new MMap();

		LCContPlanRiskSet tLCContPlanRiskSet = (LCContPlanRiskSet) mContPlanRisk.get(aLCGrpSubInsuredImportSchema.getContPlanCode());
		if (tLCContPlanRiskSet == null || tLCContPlanRiskSet.size() < 1) {
			return null;
		}
		LCBnfSet tLCBnfSet = new LCBnfSet();
		LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
		LCInsuredSet tInsuredSet = getLCInsured(aLCGrpSubInsuredImportSchema);
		if (!m_GrpPolImpInfo.init(aLCGrpSubInsuredImportSchema.getBatchNo(), this.mGlobalInput, tInsuredSet, tLCBnfSet, tLCInsuredRelatedSet,
				mLCGrpContSchema)) {
			return null;
		}
		LCContPlanRiskSet mainPlanRiskSet = new LCContPlanRiskSet();
		LCContPlanRiskSet subPlanRiskSet = new LCContPlanRiskSet();
		LCContPlanRiskSchema contPlanRiskSchema = null;
		LCPolBL mainPolBL = new LCPolBL();
		for (int t = 1; t <= tLCContPlanRiskSet.size(); t++) {
			contPlanRiskSchema = tLCContPlanRiskSet.get(t);
			if (contPlanRiskSchema.getRiskCode().equals(contPlanRiskSchema.getMainRiskCode())) {
				mainPlanRiskSet.add(contPlanRiskSchema);
				mainPolBL.setRiskCode(contPlanRiskSchema.getRiskCode());
			} else {
				subPlanRiskSet.add(contPlanRiskSchema);
			}

		}
		mainPlanRiskSet.add(subPlanRiskSet);

		// 根据险种计划生成险种保单相关信息
		double sumPrem = 0;
		LCGrpRiskPremPlanSet tLCGrpRiskPremPlanSet = new LCGrpRiskPremPlanSet();
		for (int j = 1; j <= mainPlanRiskSet.size(); j++) {
			LCContPlanRiskSchema tLCContPlanRiskSchema = mainPlanRiskSet.get(j);
			String tRiskCode = mainPlanRiskSet.get(j).getRiskCode();
			LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
			for (int m = 1; m <= mLCGrpPolSet.size(); m++) {
				if (tRiskCode.equals(mLCGrpPolSet.get(m).getRiskCode())) {
					tLCGrpPolSchema = mLCGrpPolSet.get(m);
					break;
				}
			}

			/** 添加在职退休比,平均年龄等计算要素 */
			VData tData = prepareContPlanData(aLCGrpSubInsuredImportSchema, tLCGrpPolSchema, tLCContPlanRiskSchema, mainPolBL);
			if (tData == null) {
				return null;
			}

			// 提交生成数据
			MMap oneRisk = submiDatattoProposalBL(tData, tLCContPlanRiskSchema);
			if (oneRisk == null) {
				return null;
			} else {
				
				LCPolSchema tempLCPolSchema = (LCPolSchema) oneRisk.getObjectByObjectName("LCPolSchema", 0);
				
				LCGrpRiskPremPlanSchema tLCGrpRiskPremPlanShema = new LCGrpRiskPremPlanSchema();
				String tRiskPremSeqNo = PubFun1.CreateMaxNo("RiskPremSeqNo", 20);
				tLCGrpRiskPremPlanShema.setSeqNo(tRiskPremSeqNo);
				tLCGrpRiskPremPlanShema.setPremPlanCalFlag(calSign);
				tLCGrpRiskPremPlanShema.setRiskCode(tRiskCode);
				tLCGrpRiskPremPlanShema.setPrem(tempLCPolSchema.getPrem());
				tLCGrpRiskPremPlanShema.setOperator(mGlobalInput.Operator);
				tLCGrpRiskPremPlanShema.setMakeDate(PubFun.getCurrentDate());
				tLCGrpRiskPremPlanShema.setMakeTime(PubFun.getCurrentTime());
				tLCGrpRiskPremPlanShema.setModifyDate(PubFun.getCurrentDate());
				tLCGrpRiskPremPlanShema.setModifyTime(PubFun.getCurrentTime());
				
				// 获取险种字符串
				String riskSign = getRiskPremSign(calSign, tRiskCode);
				if(riskSign == null){
					return null;
				}
				tLCGrpRiskPremPlanShema.setRiskPremPlanCalFlag(riskSign);
				
				tLCGrpRiskPremPlanSet.add(tLCGrpRiskPremPlanShema);

				sumPrem = sumPrem + tempLCPolSchema.getPrem();

				System.out.println("被保人：" + aLCGrpSubInsuredImportSchema.getName() + ",险种：" + tRiskCode + "，算费成功！");
			}
		}

		String tPremPlanCode = PubFun1.CreateMaxNo("PremPlanCode", 20);
		LCGrpPremPlanSchema tLCGrpPremPlanSchema = new LCGrpPremPlanSchema();
		tLCGrpPremPlanSchema.setPremPlanCode(tPremPlanCode);
		tLCGrpPremPlanSchema.setGrpContNo(mGrpContNo);
		tLCGrpPremPlanSchema.setPremPlanCalFlag(calSign);
		tLCGrpPremPlanSchema.setPrem(sumPrem);
		tLCGrpPremPlanSchema.setOperator(mGlobalInput.Operator);
		tLCGrpPremPlanSchema.setMakeDate(PubFun.getCurrentDate());
		tLCGrpPremPlanSchema.setMakeTime(PubFun.getCurrentTime());
		tLCGrpPremPlanSchema.setModifyDate(PubFun.getCurrentDate());
		tLCGrpPremPlanSchema.setModifyTime(PubFun.getCurrentTime());

		tPersonMMap.put(tLCGrpRiskPremPlanSet, SysConst.INSERT);
		tPersonMMap.put(tLCGrpPremPlanSchema, SysConst.INSERT);
		
		return tPersonMMap;
	}
	
	/**
	 * 拆分算费串至险种
	 */
	private String getRiskPremSign(String calSign, String riskCode){
		
		try {
			String tempStr =  calSign.substring(calSign.indexOf("|" + riskCode) + 1);
			
			int index = tempStr.indexOf("|");
			if(index == -1){
				index = tempStr.length();
			}
			tempStr = tempStr.substring(0, index);
			
			return tempStr;
			
		} catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}

	/**
	 * 准备提交ProposalBL的数据
	 * 
	 * @param aLCGrpSubInsuredImportSchema
	 * @param aLCGrpPolSchema
	 * @param tLCContPlanRiskSchema
	 * @param mainPolBL
	 * @return
	 */
	private VData prepareContPlanData(LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema, LCGrpPolSchema aLCGrpPolSchema,
			LCContPlanRiskSchema tLCContPlanRiskSchema, LCPolBL mainPolBL) {

		VData tNewVData = new VData();
		MMap tmpMap = new MMap();
		String strRiskCode = aLCGrpPolSchema.getRiskCode();
		if (aLCGrpPolSchema == null) {
			return null;
		}

		// 拷贝一份，避免修改本地数据
		LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		tLCGrpPolSchema.setSchema(aLCGrpPolSchema);

		LCContSchema contSchema = getLCContSchema(aLCGrpPolSchema, aLCGrpSubInsuredImportSchema);
		tNewVData.add(contSchema);

		LCInsuredSchema insuredSchema = getLInsured(aLCGrpPolSchema, aLCGrpSubInsuredImportSchema);
		tNewVData.add(insuredSchema);

		LCPolSchema tLCPolSchema = getLCPolSchema(aLCGrpPolSchema, contSchema, insuredSchema, aLCGrpSubInsuredImportSchema.getContPlanCode());

		ContPlanQryBL contPlanQryBL = new ContPlanQryBL();
		VData tVData = new VData();
		tVData.add(tLCContPlanRiskSchema);
		boolean b = contPlanQryBL.submitData(tVData, "");
		if (!b || contPlanQryBL.mErrors.needDealError()) {
			CError.buildErr(this, "查询计划要约出错:", contPlanQryBL.mErrors);
			return null;
		}
		tVData = contPlanQryBL.getResult();
		LCDutySet tDutySet = (LCDutySet) tVData.getObjectByObjectName("LCDutySet", 0);
		LCGetSet tGetSet = (LCGetSet) tVData.getObjectByObjectName("LCGetSet", 0);
		tNewVData.add(tGetSet);
		if (tDutySet == null) {
			LCDutySchema tDutySchema = (LCDutySchema) tVData.getObjectByObjectName("LCDutySchema", 0);
			if (tDutySchema == null) {
				CError.buildErr(this, "查询计划要约出错:责任信息不能为空");
				return null;
			}
			setPolInfoByDuty(tLCPolSchema, tDutySchema);
			tDutySet = new LCDutySet();
			tDutySet.add(tDutySchema);
		}
		if (tDutySet == null || tDutySet.size() <= 0) {
			CError.buildErr(this, strRiskCode + "要约信息生成错误出错");
			return null;
		}
		/**
		 * 将险种的交费频次存入责任中
		 */
		if (tLCPolSchema.getPayIntv() > 0) {
			for (int i = 1; i <= tDutySet.size(); i++) {
				tDutySet.get(i).setPayIntv(this.mLCGrpContSchema.getPayIntv());
			}
		}
		/**
		 * 将险种的交费频次存入责任
		 */
		tNewVData.add(tDutySet);
		// 保费
		LCPremSet tPremSet = new LCPremSet();
		tPremSet = (LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0);
		tNewVData.add(tPremSet);

		// 加入对应险种的集体投保单信息,险种承保描述信息
		LMRiskAppSchema tLMRiskAppSchema = m_GrpPolImpInfo.findLMRiskAppSchema(strRiskCode);
		if (tLMRiskAppSchema == null) {
			return null;
		}
		LMRiskSchema tLMRiskSchema = m_GrpPolImpInfo.findLMRiskSchema(strRiskCode);
		if (tLMRiskSchema == null) {
			return null;
		}

		LDGrpSchema ttLDGrpSchema = m_GrpPolImpInfo.findLDGrpSchema(tLCGrpPolSchema.getCustomerNo());
		if (ttLDGrpSchema == null) {
			return null;
		}

		String Industry = getIndustry(this.mLCGrpContSchema.getBusinessType());
		String InsuredState = insuredSchema.getInsuredStat();
		// 其他信息
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("samePersonFlag", "0");
		tTransferData.setNameAndValue("GrpImport", 1);
		tTransferData.setNameAndValue("RetireRate", null);
		tTransferData.setNameAndValue("AvgAge", null);
		tTransferData.setNameAndValue("InsuredNum", null);
		tTransferData.setNameAndValue("Industry", Industry);
		tTransferData.setNameAndValue("InsuredState", InsuredState);
		// 增加“领取频次”要素
		// tTransferData.setNameAndValue("GrpGetIntv", mGrpGetIntv);

		tNewVData.add(mGlobalInput);
		tNewVData.add(tLCGrpPolSchema);
		tNewVData.add(tLMRiskAppSchema);
		tNewVData.add(tLMRiskSchema);
		tNewVData.add(ttLDGrpSchema);
		tNewVData.add(m_GrpPolImpInfo.getLCGrpAppntSchema());
		tNewVData.add(tLCPolSchema);
		tNewVData.add(tTransferData);
		tNewVData.add(mainPolBL);
		tNewVData.add(this.mLCGrpContSchema);
		tNewVData.add(tmpMap);

		return tNewVData;
	}

	/**
	 * 提交proposalBL
	 * 
	 * @param tNewVData VData
	 * @param tLCInsuredListSet LCInsuredListSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @return boolean
	 */
	private MMap submiDatattoProposalBL(VData tNewVData, LCContPlanRiskSchema tLCContPlanRiskSchema) {
		// tNewVData 里面还有一些要创建的信息，因此需要重新获得
		MMap map = (MMap) tNewVData.getObjectByObjectName("MMap", 0);
		if (map == null) {
			map = new MMap();
		}
		String strID = (String) ((TransferData) tNewVData.getObjectByObjectName("TransferData", 0)).getValueByName("ID");
		ProposalBL tProposalBL = new ProposalBL();
		if (!tProposalBL.PrepareSubmitData(tNewVData, "INSERT||PROPOSAL")) {
			return null;
		} else {
			VData pData = tProposalBL.getSubmitResult();
			if (pData == null) {
				CError.buildErr(this, "保单提交计算失败!");
				return null;
			}

			LCPolSchema rPolSchema = (LCPolSchema) pData.getObjectByObjectName("LCPolSchema", 0);
			if (tLCContPlanRiskSchema != null && tLCContPlanRiskSchema.getRiskAmnt() > 0) {
				rPolSchema.setAmnt(tLCContPlanRiskSchema.getRiskAmnt());
			}
			LCGrpPolSchema rGrpPol = (LCGrpPolSchema) pData.getObjectByObjectName("LCGrpPolSchema", 0);
			LCGrpContSchema rGrpCont = (LCGrpContSchema) pData.getObjectByObjectName("LCGrpContSchema", 0);
			LCPremBLSet rPremBLSet = (LCPremBLSet) pData.getObjectByObjectName("LCPremBLSet", 0);

			if (rPremBLSet == null) {
				CError.buildErr(this, "保单提交计算保费项数据准备有误!");
				return null;
			}
			LCPremSet rPremSet = new LCPremSet();
			for (int i = 1; i <= rPremBLSet.size(); i++) {
				rPremSet.add(rPremBLSet.get(i));
			}

			LCDutyBLSet rDutyBLSet = (LCDutyBLSet) pData.getObjectByObjectName("LCDutyBLSet", 0);
			if (rDutyBLSet == null) {
				CError.buildErr(this, "保单提交计算责任项数据准备有误!");
				return null;
			}
			LCDutySet rDutySet = new LCDutySet();
			for (int i = 1; i <= rDutyBLSet.size(); i++) {
				rDutySet.add(rDutyBLSet.get(i));
			}

			LCGetBLSet rGetBLSet = (LCGetBLSet) pData.getObjectByObjectName("LCGetBLSet", 0);
			if (rGetBLSet == null) {
				CError.buildErr(this, "保单提交计算给付项数据准备有误!");
				return null;
			}
			LCGetSet rGetSet = new LCGetSet();
			for (int i = 1; i <= rGetBLSet.size(); i++) {
				rGetSet.add(rGetBLSet.get(i));
			}

			LCBnfBLSet rBnfBLSet = (LCBnfBLSet) pData.getObjectByObjectName("LCBnfBLSet", 0);
			LCBnfSet rBnfSet = new LCBnfSet();
			if (rBnfBLSet != null) {
				for (int i = 1; i <= rBnfBLSet.size(); i++) {
					rBnfSet.add(rBnfBLSet.get(i));
				}
			}
			LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet) pData.getObjectByObjectName("LCInsuredRelatedSet", 0);
			// if ( tLCInsuredRelatedSet!=null && LCInsuredRelatedSet.size()>0)

			// 更新个单合同金额相关，更新的时候同时更新缓存中的数据
			LCContSet tContSet = new LCContSet();

			// 更新集体险种金额相关
			String riskCode = rPolSchema.getRiskCode();
			LCGrpPolSchema tLCGrpPolSchema = m_GrpPolImpInfo.findLCGrpPolSchema(riskCode);
			if (!StrTool.cTrim(rPolSchema.getAppFlag()).equals("2")) {
				tLCGrpPolSchema.setPrem(PubFun.setPrecision(tLCGrpPolSchema.getPrem() + rPolSchema.getPrem(), "0.00"));
				tLCGrpPolSchema.setAmnt(tLCGrpPolSchema.getAmnt() + rPolSchema.getAmnt());
			}
			tLCGrpPolSchema.setPayEndDate(PubFun.getLaterDate(tLCGrpPolSchema.getPayEndDate(), rPolSchema.getPayEndDate()));
			tLCGrpPolSchema.setPaytoDate(PubFun.getLaterDate(tLCGrpPolSchema.getPaytoDate(), rPolSchema.getPaytoDate()));
			tLCGrpPolSchema.setFirstPayDate(PubFun.getBeforeDate(tLCGrpPolSchema.getFirstPayDate(), rPolSchema.getFirstPayDate()));
			rGrpPol.setPrem(tLCGrpPolSchema.getPrem());
			rGrpPol.setAmnt(tLCGrpPolSchema.getAmnt());
			rGrpPol.setPayEndDate(tLCGrpPolSchema.getPayEndDate());
			rGrpPol.setPaytoDate(tLCGrpPolSchema.getPaytoDate());
			rGrpPol.setFirstPayDate(tLCGrpPolSchema.getFirstPayDate());
			rGrpPol.setCValiDate(rPolSchema.getCValiDate());
			// 更新团单合同金额相关
			LCGrpContSchema sGrpContSchema = m_GrpPolImpInfo.getLCGrpContSchema();
			rGrpCont.setPrem(sGrpContSchema.getPrem());
			rGrpCont.setSaleChnlDetail("01");
			rGrpCont.setAmnt(sGrpContSchema.getAmnt());

			rPolSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
			map.put(tContSet, "UPDATE");
			map.put(rPolSchema.getSchema(), "INSERT");
			map.put(rPremSet, "INSERT");
			map.put(rDutySet, "INSERT");
			map.put(rGetSet, "INSERT");
			map.put(rBnfSet, "INSERT");
			map.put(tLCInsuredRelatedSet, "INSERT");
			// 缓存保存的主险保单信息
			m_GrpPolImpInfo.cachePolInfo(strID, rPolSchema);
		}

		return map;
	}
	
	/**
	 * 准备保费计算的LCInsuredSet
	 * 
	 * @param tLCGrpSubInsuredImportSchema
	 * @return
	 */
	private LCInsuredSet getLCInsured(LCGrpSubInsuredImportSchema tLCGrpSubInsuredImportSchema) {

		LCInsuredSet tInsuredSet = new LCInsuredSet();

		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setContNo(tLCGrpSubInsuredImportSchema.getSeqNo());
		tLCInsuredSchema.setPrtNo(tLCGrpSubInsuredImportSchema.getPrtNo());
		tLCInsuredSchema.setName(tLCGrpSubInsuredImportSchema.getName());
		tLCInsuredSchema.setSex(tLCGrpSubInsuredImportSchema.getSex());
		tLCInsuredSchema.setInsuredStat(tLCGrpSubInsuredImportSchema.getRetire());
		tLCInsuredSchema.setBirthday(tLCGrpSubInsuredImportSchema.getBirthday());
		tLCInsuredSchema.setIDType(tLCGrpSubInsuredImportSchema.getIDType());
		tLCInsuredSchema.setIDNo(tLCGrpSubInsuredImportSchema.getIDNo());
		tLCInsuredSchema.setContPlanCode(tLCGrpSubInsuredImportSchema.getContPlanCode());
		tLCInsuredSchema.setOccupationType(tLCGrpSubInsuredImportSchema.getOccupationType());
		tLCInsuredSchema.setBankAccNo(tLCGrpSubInsuredImportSchema.getBankAccNo());
		tLCInsuredSchema.setBankCode(tLCGrpSubInsuredImportSchema.getBankCode());
		tLCInsuredSchema.setAccName(tLCGrpSubInsuredImportSchema.getAccName());
		tLCInsuredSchema.setRelationToMainInsured(tLCGrpSubInsuredImportSchema.getRelation());
		tLCInsuredSchema.setOthIDNo(tLCGrpSubInsuredImportSchema.getOthIDNo());
		tLCInsuredSchema.setOthIDType(tLCGrpSubInsuredImportSchema.getOthIDType());
		tLCInsuredSchema.setCreditGrade("0");

		tInsuredSet.add(tLCInsuredSchema);

		return tInsuredSet;
	}

	/**
	 * 生成保单信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCContSchema getLCContSchema(LCGrpPolSchema aLCGrpPolSchema, LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {

		LCContSchema tLCContSchema = new LCContSchema();
		// 设置主被保险人信息
		tLCContSchema.setContNo(aLCGrpSubInsuredImportSchema.getSeqNo());
		tLCContSchema.setInsuredNo(aLCGrpSubInsuredImportSchema.getCustomerNo());
		tLCContSchema.setInsuredName(aLCGrpSubInsuredImportSchema.getName());
		tLCContSchema.setInsuredSex(aLCGrpSubInsuredImportSchema.getSex());
		tLCContSchema.setInsuredBirthday(aLCGrpSubInsuredImportSchema.getBirthday());
		tLCContSchema.setInsuredIDNo(aLCGrpSubInsuredImportSchema.getIDNo());
		tLCContSchema.setInsuredIDType(aLCGrpSubInsuredImportSchema.getIDType());
		tLCContSchema.setBankAccNo(aLCGrpSubInsuredImportSchema.getBankAccNo());
		tLCContSchema.setBankCode(aLCGrpSubInsuredImportSchema.getBankCode());
		tLCContSchema.setAccName(aLCGrpSubInsuredImportSchema.getAccName());
		tLCContSchema.setPayIntv(this.mLCGrpContSchema.getPayIntv());
		tLCContSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
		tLCContSchema.setContType("2");
		tLCContSchema.setAgentCode(aLCGrpPolSchema.getAgentCode());
		tLCContSchema.setAgentCom(aLCGrpPolSchema.getAgentCom());
		tLCContSchema.setAppntName(aLCGrpPolSchema.getGrpName());
		tLCContSchema.setAppntNo(aLCGrpPolSchema.getCustomerNo());
		tLCContSchema.setPrtNo(aLCGrpPolSchema.getPrtNo());
		tLCContSchema.setGrpContNo(aLCGrpPolSchema.getGrpContNo());
		tLCContSchema.setOperator(mGlobalInput.Operator);
		tLCContSchema.setManageCom(mGlobalInput.ManageCom);
		tLCContSchema.setMakeDate(aLCGrpPolSchema.getMakeDate());
		tLCContSchema.setMakeTime(aLCGrpPolSchema.getMakeTime());
		tLCContSchema.setModifyDate(aLCGrpPolSchema.getModifyDate());
		tLCContSchema.setModifyTime(aLCGrpPolSchema.getModifyTime());
		tLCContSchema.setCValiDate(mLCGrpContSchema.getCValiDate());

		return tLCContSchema;
	}

	/**
	 * 生成被保人信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param tLCContPlanRiskSchema LCContPlanRiskSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCInsuredSchema getLInsured(LCGrpPolSchema aLCGrpPolSchema, LCGrpSubInsuredImportSchema aLCGrpSubInsuredImportSchema) {

		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setInsuredNo(aLCGrpSubInsuredImportSchema.getCustomerNo());
		tLCInsuredSchema.setName(aLCGrpSubInsuredImportSchema.getName());
		tLCInsuredSchema.setSex(aLCGrpSubInsuredImportSchema.getSex());
		tLCInsuredSchema.setBirthday(aLCGrpSubInsuredImportSchema.getBirthday());
		tLCInsuredSchema.setIDType(aLCGrpSubInsuredImportSchema.getIDType());
		tLCInsuredSchema.setIDNo(aLCGrpSubInsuredImportSchema.getIDNo());
		tLCInsuredSchema.setOccupationCode(aLCGrpSubInsuredImportSchema.getOccupationCode());
		tLCInsuredSchema.setOccupationType(aLCGrpSubInsuredImportSchema.getOccupationType());
		tLCInsuredSchema.setContPlanCode(aLCGrpSubInsuredImportSchema.getContPlanCode());
		tLCInsuredSchema.setGrpContNo(aLCGrpSubInsuredImportSchema.getGrpContNo());

		return tLCInsuredSchema;

	}

	/**
	 * 生成险种信息
	 * 
	 * @param insuredSchema LCInsuredSchema
	 * @param contSchema LCContSchema
	 * @return LCPolSchema
	 */
	private LCPolSchema getLCPolSchema(LCGrpPolSchema aLCGrpPolSchema, LCContSchema contSchema, LCInsuredSchema insuredSchema, String aContPlanCode) {

		LCPolSchema tLCPolSchema = new LCPolSchema();

		if (aLCGrpPolSchema == null) {
			return null;
		}

		tLCPolSchema.setPrtNo(aLCGrpPolSchema.getPrtNo());
		tLCPolSchema.setGrpPolNo(aLCGrpPolSchema.getGrpPolNo());
		if ("Y".equals(tLCPolSchema.getSpecifyValiDate()) && tLCPolSchema.getCValiDate() != null) {
			// 如果磁盘倒入指定生效日期，且生效日期字段有值,那么就用 生效日期字段的值
		} else {
			// 否则一律用集体单的生效日期
			tLCPolSchema.setCValiDate(contSchema.getCValiDate());
		}
		tLCPolSchema.setManageCom(aLCGrpPolSchema.getManageCom());
		tLCPolSchema.setSaleChnl(aLCGrpPolSchema.getSaleChnl());
		tLCPolSchema.setAgentCom(aLCGrpPolSchema.getAgentCom());
		tLCPolSchema.setAgentCode(aLCGrpPolSchema.getAgentCode());
		tLCPolSchema.setAgentGroup(aLCGrpPolSchema.getAgentGroup());
		tLCPolSchema.setAgentCode1(aLCGrpPolSchema.getAgentCode1());
		tLCPolSchema.setGrpContNo(aLCGrpPolSchema.getGrpContNo());
		tLCPolSchema.setPayIntv(aLCGrpPolSchema.getPayIntv());
		tLCPolSchema.setContType("2");
		tLCPolSchema.setPolTypeFlag(contSchema.getPolType());
		tLCPolSchema.setAccType(contSchema.getAccType());
		tLCPolSchema.setInsuredPeoples(contSchema.getPeoples());
		tLCPolSchema.setRiskCode(aLCGrpPolSchema.getRiskCode());
		tLCPolSchema.setContNo(insuredSchema.getContNo());
		tLCPolSchema.setInsuredSex(insuredSchema.getSex());
		tLCPolSchema.setInsuredBirthday(insuredSchema.getBirthday());
		tLCPolSchema.setInsuredName(insuredSchema.getName());
		tLCPolSchema.setContPlanCode(aContPlanCode);
		tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());

		return tLCPolSchema;

	}

	/**
	 * 通过duty设置一些lcpol的元素
	 * 
	 * @param tLCPolSchema LCPolSchema
	 * @param dutySchema LCDutySchema
	 */
	private void setPolInfoByDuty(LCPolSchema tLCPolSchema, LCDutySchema dutySchema) {

		tLCPolSchema.setInsuYear(dutySchema.getInsuYear());
		tLCPolSchema.setInsuYearFlag(dutySchema.getInsuYearFlag());
		tLCPolSchema.setPrem(dutySchema.getPrem());
		tLCPolSchema.setAmnt(dutySchema.getAmnt());
		tLCPolSchema.setPayEndYear(dutySchema.getPayEndYear());
		tLCPolSchema.setPayEndYearFlag(dutySchema.getPayEndYearFlag());
		tLCPolSchema.setGetYear(dutySchema.getGetYear());
		tLCPolSchema.setGetYearFlag(dutySchema.getGetYearFlag());
		tLCPolSchema.setAcciYear(dutySchema.getAcciYear());
		tLCPolSchema.setAcciYearFlag(dutySchema.getAcciYearFlag());
		tLCPolSchema.setMult(dutySchema.getMult());
		// 计算方向,在按分数卖的保单，切记算方向为O的时候
		if (dutySchema.getMult() > 0 && "O".equals(StrTool.cTrim(dutySchema.getPremToAmnt()))) {
			tLCPolSchema.setPremToAmnt(dutySchema.getPremToAmnt());
		}
		tLCPolSchema.setStandbyFlag1(dutySchema.getStandbyFlag1());
		tLCPolSchema.setStandbyFlag2(dutySchema.getStandbyFlag2());
		tLCPolSchema.setStandbyFlag3(dutySchema.getStandbyFlag3());
	}
	
	/**
	 * getIndustry
	 * 
	 * @param tIndustry String
	 * @return String
	 */
	private String getIndustry(String tIndustry) {
		String sql = "select codealias from ldcode where codetype='businesstype' and code='" + tIndustry + "'";
		System.out.println("+++++++" + (new ExeSQL()).getOneValue(sql));
		if (StrTool.cTrim((new ExeSQL()).getOneValue(sql)).equals("")) {
			return "";
		} else {
			return (new ExeSQL()).getOneValue(sql);
		}
	}
}
