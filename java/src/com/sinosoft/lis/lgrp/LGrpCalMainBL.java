package com.sinosoft.lis.lgrp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;

/**
 * 大团单算费处理主调入口类
 * 
 * @author 张成轩
 */
public class LGrpCalMainBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	private LGrpLock lock;
	
	/** 团体合同号 */
	private String mGrpContNo;
	
	private GlobalInput mGlobalInput;

	public boolean submitData(TransferData tTransferData, String lockPrtNo) {

		System.out.println("---------大团单算费处理入口类---------");
		
		// 初始化业务锁
		lock = new LGrpLock(3, lockPrtNo);
		
		// 锁定
		if(!lock.lock()){
			mErrors.addOneError("该保单正在进行其它业务处理，请在处理完毕后进行算费操作。保单状态会在页面进行显示，可以点击【刷新】按钮。");
			return false;
		}
		
		try {

			if (!getInputData(tTransferData)) {
				lock.unlock();
				return false;
			}
			
			// 线程调用前的校验
			if (!checkData()) {
				return false;
			}

			// 开始调用线程
			if (!(dealData())) {
				return false;
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
			mErrors.addOneError("算费失败：程序异常终止！");
			
			lock.unlock();
			
			return false;
		}

		return true;
	}
	
	/**
	 * 获取处理相关信息
	 * 
	 * @param tTransferData
	 * @return
	 */
	private boolean getInputData(TransferData tTransferData) {

		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mGlobalInput  = (GlobalInput) tTransferData.getValueByName("GlobalInput");

		if (isNull(mGrpContNo) || mGlobalInput == null) {
			System.out.println("导入被保人失败：传入的保单信息缺失！");
			mErrors.addOneError("导入被保人失败：传入的保单信息缺失！");
			return false;
		}

		return true;
	}

	/**
	 * 线程调用类
	 * 
	 * @param tVData
	 * @return
	 */
	private boolean dealData() {

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
		tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
		
		ExecutorService exec = Executors.newFixedThreadPool(1);

		LGrpCalMainThread tLGrpCalMainThread = new LGrpCalMainThread(tTransferData, lock);
		exec.submit(tLGrpCalMainThread);
		
		return true;
	}

	private boolean checkData() {
		return true;
	}

	public CErrors getError() {
		return mErrors;
	}
	
	/**
	 * 校验字段是否为空
	 * 
	 * @param chkStr
	 * @return
	 */
	private boolean isNull(String chkStr) {
		if (chkStr == null || "".equals(chkStr) || "null".equals(chkStr)) {
			return true;
		}
		return false;
	}
}
