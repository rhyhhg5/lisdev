package com.sinosoft.lis.lgrp;

import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;

/**
 * 大团单算费数据获取类
 * 
 * @author 张成轩
 */
public class LGrpCalGetDataThread {

	private RSWrapper rswrapper = new RSWrapper();

	private LCGrpSubInsuredImportSet tLCGrpSubInsuredImportSet = new LCGrpSubInsuredImportSet();

	/**
	 * 构造函数
	 * 
	 * @param tTransferData
	 * @param dealFlag 1--算费第一步，获取导入表被保人串  2--算费第三步，生成被保人信息表
	 */
	public LGrpCalGetDataThread(TransferData tTransferData, int dealFlag) {
		if (dealFlag == 1) {
			getCalSignData(tTransferData);
		} else if (dealFlag == 2) {
			getCalInsuredData(tTransferData);
		}

	}

	private void getCalSignData(TransferData tTransferData) {

		String tGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		String tSql = "select * from LCGrpSubInsuredImport where grpcontno = '" + tGrpContNo + "' and CalState = '00' and datachkflag='01' ";
		rswrapper.prepareData(tLCGrpSubInsuredImportSet, tSql);
	}
	
	private void getCalInsuredData(TransferData tTransferData) {

		String tGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		String tSql = "select * from LCGrpSubInsuredImport where grpcontno = '" + tGrpContNo + "' and CalState = '01' ";
		rswrapper.prepareData(tLCGrpSubInsuredImportSet, tSql);
	}

	/**
	 * 获取被保人信息
	 * 
	 * @param data Schema集合对象
	 * @param startLine 起始行数
	 * @param size 获取数量
	 * @return
	 */
	public LCGrpSubInsuredImportSet getListData() {

		LCGrpSubInsuredImportSet tSchemaSet = new LCGrpSubInsuredImportSet();

		tSchemaSet.set(rswrapper.getData());

		if (tSchemaSet == null) {
			rswrapper.close();
		}

		return tSchemaSet;
	}
}
