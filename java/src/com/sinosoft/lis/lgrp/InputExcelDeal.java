package com.sinosoft.lis.lgrp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.TransferData;

/**
 * 大团单excel处理类
 * 
 * @author 张成轩
 */
public class InputExcelDeal {

	private InputStream is = null;

	private XSSFWorkbook wb = null;

	private XSSFSheet hs = null;

	private TransferData mTransferData;

	/**
	 * 构造函数，初始化流信息
	 * 
	 * @param path
	 * @param tTransferData
	 */
	public InputExcelDeal(String path, TransferData tTransferData) {

		this.mTransferData = tTransferData;

		try {
			is = new FileInputStream(path);
			wb = new XSSFWorkbook(is);
			// 获取文件XSSFSheet对象
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(wb != null){
				try {
					hs = wb.getSheetAt(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			wb = null;
		}
	}

	/**
	 * 获取excel信息
	 * 
	 * @param data Schema集合对象
	 * @param startLine 起始行数
	 * @param size 获取数量
	 * @return
	 */
	public int getListData(List<Schema> data, int startLine, int size) {

		try {

			if (hs == null) {
				return -1;
			}

			int rownum = startLine;
			int endLine = startLine + size - 1;

			// 不读第一行
			for (; rownum <= hs.getLastRowNum() && rownum <= endLine; rownum++) {
				XSSFRow hRow = hs.getRow(rownum);

				// 获取对应行的Schema
				LCGrpSubInsuredImportSchema lgrpSchema = LGrpInputSchemaDeal.setScheml(hRow, rownum, mTransferData);

				// 判断是否出现异常
				if(lgrpSchema == null){
					
				} else {
					data.add(lgrpSchema);
				}
			}

			return rownum;

		} catch (Exception e) {

			e.printStackTrace();
			return -1;
		}
	}
}
