/**
 * 2012-12-27
 */
package com.sinosoft.lis.lgrp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sinosoft.utility.Schema;
import com.sinosoft.utility.TransferData;

/**
 * @author LY
 * 
 */
public class DataCatchService {
	private final static int mSize = LGrpThreadConfig.dataCatchSize; // 数据缓存队列大小

	private List<Schema> tDataColls = Collections.synchronizedList(new ArrayList<Schema>());

	private int readLineNumber = 2;

	private InputExcelDeal ied = null;
	
	public DataCatchService(String path, TransferData tTransferData) {
		
		this.ied = new InputExcelDeal(path, tTransferData);
	}

	public void loadDatas() throws Exception {

		readLineNumber = ied.getListData(tDataColls, readLineNumber, mSize);
		
		if(readLineNumber == -1){
			throw new Exception("error,获取清单数据异常！");
		}
	}

	public boolean hasNext() {
		return tDataColls.size() > 0 ? true : false;
	}

	public List<Schema> getDatas(int cMaxRowNum) {
		List<Schema> tResultVals = null;

		synchronized (this) {
			int tMaxRowNum = cMaxRowNum;

			int tCurMaxRowNum = tDataColls.size() > tMaxRowNum ? tMaxRowNum : tDataColls.size();

			tResultVals = Collections.synchronizedList(new ArrayList<Schema>());

			for (int i = 0; i < tCurMaxRowNum; i++) {
				tResultVals.add(tDataColls.get(0));
				tDataColls.remove(0);
			}
		}

		return tResultVals;
	}

}
