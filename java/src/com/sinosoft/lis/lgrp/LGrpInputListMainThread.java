package com.sinosoft.lis.lgrp;

import com.sinosoft.utility.TransferData;

/**
 * 大团单被保人导入处理主线程控制类
 * 
 * @author 张成轩
 */
public class LGrpInputListMainThread implements Runnable {

	private String filePath;

	private TransferData mTransferData;

	private LGrpLock lock;

	/**
	 * 构造函数
	 * 
	 * @param filePath 导入文件路径
	 * @param tTransferData
	 * @param lock 业务锁对象
	 */
	public LGrpInputListMainThread(String filePath, TransferData tTransferData, LGrpLock lock) {
		this.filePath = filePath;
		this.mTransferData = tTransferData;
		this.lock = lock;
	}

	public void run() {
		
		// 线程起始校验处理
		if (beginTread()) {
			try {
				long tStart2 = System.nanoTime();

				// 这段以后去掉 重置线程配置
				// LGrpThreadConfig.reload();

				DFService service = DFService.getService(filePath, mTransferData);
				service.startService();

				long tEnd2 = System.nanoTime();
				System.out.println("大团单导入被保人，耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
			} catch (RuntimeException e) {
				e.printStackTrace();
			} finally {
				// 线程完毕处理
				finalThread();
			}
		}
	}
	
	/**
	 * 线程起始校验处理
	 * 
	 * @return
	 */
	public boolean beginTread() {
		System.out.println("----校验锁定-----");
		return lock.checklock();
	}
	
	/**
	 * 线程完毕处理
	 */
	public void finalThread() {
		System.out.println("----解除锁定-----");
		lock.unlock();
	}
}
