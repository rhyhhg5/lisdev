package com.sinosoft.lis.lgrp;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LCGrpSubInsuredImportDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 大团单保单算费串处理
 * 
 * @author 张成轩
 */
public class LGrpCalSignInsuredDeal {

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mGrpContNo;

	private List mErrorSign = new ArrayList();

	public LGrpCalSignInsuredDeal(TransferData tTransferData) {
		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");
	}

	public boolean dealData() {

		// 处理算费串
		if (!dealCalSign()) {
			// return false;
		}

		if (!dealError()) {
			// return false;
		}

		return true;
	}

	/**
	 * 保单算费串处理
	 * 
	 * @return
	 */
	private boolean dealCalSign() {

		// 获取待处理的CalSian串
		SSRS tSSRS = getContCalSign();

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", mGrpContNo);
		tTransferData.setNameAndValue("GlobalInput", mGlobalInput);

		// 初始化算费类
		LGrpCalGetPersonPrem tPrem = null;

		if (tSSRS.MaxRow != 0) {
			tPrem = new LGrpCalGetPersonPrem(tTransferData);
		}

		for (int index = 1; index <= tSSRS.MaxRow; index++) {

			// 随机取一个人人进行算费处理
			// 获取待更新的LCGrpRiskPremPlan、LCGrpPremPlan
			MMap tMMap = new MMap();

			MMap premMap = getGrpPrem(tPrem, tSSRS.GetText(index, 1));

			if (premMap == null) {

				mErrorSign.add(tSSRS.GetText(index, 1));

			} else {

				tMMap.add(getGrpPrem(tPrem, tSSRS.GetText(index, 1)));

				// 是统一提交好还是一个一个提交好后续还待考证
				// 存在效率问题和后续处理问题
				// 一个一个提交效率肯定遍地 但后续更新失败后很容易进行处理
				if (!save(tMMap)) {
					mErrorSign.add(tSSRS.GetText(index, 1));
				}
			}

		}

		return true;
	}

	/**
	 * 处理获取算费串错误的信息
	 * 
	 * @return
	 */
	private boolean dealError() {

		MMap tMMap = new MMap();

		for (int index = 0; index < mErrorSign.size(); index++) {

			// 统一更新错误信息
			String updateSql = "update LCGrpSubInsuredImport set" + " CalState='03'," + " CalRemark='算费失败，获取保费失败！'," + " Operator='"
					+ mGlobalInput.Operator + "'," + " ModifyDate=current date," + " ModifyTime=current time " + " where grpcontno='" + mGrpContNo
					+ "' " + " and calremark='" + mErrorSign.get(index) + "' " + " and calstate='01'";
			tMMap.put(updateSql, SysConst.UPDATE);
		}

		if (tMMap.size() != 0) {
			return save(tMMap);
		}
		return true;
	}

	/**
	 * 获取算费成功的被保人导入信息
	 * 
	 * @return
	 */
	private LCGrpSubInsuredImportSet getSuccInsured() {

		String query = "select * from LCGrpSubInsuredImport csii " + " where grpcontno='" + mGrpContNo + "' " + " and calstate='01' "
				+ " and exists (select 1 from LCGrpPremPlan where grpcontno=csii.grpcontno and premplancalflag=csii.calremark)";

		LCGrpSubInsuredImportDB tLCGrpSubInsuredImportDB = new LCGrpSubInsuredImportDB();
		return tLCGrpSubInsuredImportDB.executeQuery(query);
	}

	/**
	 * 获取全部待算费清单的算费串
	 * 
	 * @return
	 */
	private SSRS getContCalSign() {

		ExeSQL tExeSQL = new ExeSQL();
		String query = "select distinct calremark from LCGrpSubInsuredImport csii " + " where grpcontno='" + mGrpContNo + "' "
				+ " and calstate='01' "
				+ " and not exists (select 1 from LCGrpPremPlan where grpcontno=csii.grpcontno and premplancalflag=csii.calremark)";

		return tExeSQL.execSQL(query);
	}

	/**
	 * 获取算费待更新的Schema 包含LCGrpRiskPremPlan、LCGrpPremPlan
	 * 
	 * @param tSSRS
	 * @return
	 */
	private MMap getGrpPrem(LGrpCalGetPersonPrem tPrem, String calSign) {

		String query = "select * from LCGrpSubInsuredImport where grpcontno='" + mGrpContNo + "' and calremark='" + calSign
				+ "' and calstate='01' fetch first 1 rows only";
		System.out.println(query);

		LCGrpSubInsuredImportDB tLSIIDB = new LCGrpSubInsuredImportDB();
		LCGrpSubInsuredImportSet tLSIISet = tLSIIDB.executeQuery(query);

		if (tLSIISet.size() == 0) {
			return null;
		}

		return tPrem.dealData(tLSIISet.get(1), calSign);
	}

	/**
	 * 提交数据库
	 * 
	 * @param tMap 提交map集合
	 * @return
	 */
	private boolean save(MMap tMap) {
		VData mInputData = new VData();
		mInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("---------提交数据库失败！-----------" + tPubSubmit.mErrors.getLastError());
			return false;
		}

		return true;
	}

	public static void main(String[] arr) {

		String grpContNo = "1400308220";

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "test";
		tGlobalInput.ComCode = "86110000";
		tGlobalInput.ManageCom = "86110000";

		TransferData tTransferData = new TransferData();

		tTransferData.setNameAndValue("GrpContNo", grpContNo);
		tTransferData.setNameAndValue("GlobalInput", tGlobalInput);

		LGrpCalSignInsuredDeal tLGrpCalSignDeal = new LGrpCalSignInsuredDeal(tTransferData);

		long tStart2 = System.nanoTime();
		tLGrpCalSignDeal.dealData();
		long tEnd2 = System.nanoTime();
		System.out.println("耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
	}
}
