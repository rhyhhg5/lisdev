package com.sinosoft.lis.lgrp;

import com.sinosoft.lis.db.LCGrpSubInsuredImportDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpSubInsuredImportSchema;
import com.sinosoft.lis.schema.LCGrpSubInsuredSchema;
import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 大团单被保人处理
 * 
 * @author 张成轩
 *
 */
public class LGrpInsuredDeal {
	
	/** 团体合同号 */
	private String mGrpContNo;
	
	private GlobalInput mGlobalInput;
	
	public LGrpInsuredDeal(TransferData tTransferData){
		this.mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
		this.mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");
	}
	
	/**
	 * 处理被保险人信息表
	 * 
	 * @return
	 */
	public boolean dealData(LCGrpSubInsuredImportSet tInsuredSet){
		
		MMap tMMap = new MMap();
		Reflections tReflections = new Reflections();
		
		for(int index = 1; index <= tInsuredSet.size(); index++){
			
			LCGrpSubInsuredImportSchema tInsured = tInsuredSet.get(index);
			
			// 不使用Schema方式进行处理，改为直接使用更新sql，经测试会大幅提高效率
			String updateSql = "update LCGrpSubInsuredImport set" +
					" CalState='02'," +
					" CalRemark='算费成功！'," +
					" Operator='" + mGlobalInput.Operator + "'," +
					" ModifyDate=current date," +
					" ModifyTime=current time " +
					" where seqno='" + tInsured.getSeqNo() + "'";
			
			LCGrpSubInsuredSchema tLCGrpSubInsuredSchema = new LCGrpSubInsuredSchema();
			tReflections.transFields(tLCGrpSubInsuredSchema, tInsured);
			tLCGrpSubInsuredSchema.setPremPlanCode(tInsured.getCalRemark());
			tLCGrpSubInsuredSchema.setOperator(mGlobalInput.Operator);
			tLCGrpSubInsuredSchema.setMakeDate(PubFun.getCurrentDate());
			tLCGrpSubInsuredSchema.setMakeTime(PubFun.getCurrentTime());
			tLCGrpSubInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCGrpSubInsuredSchema.setModifyTime(PubFun.getCurrentTime());
			
			tMMap.put(updateSql, SysConst.UPDATE);
			tMMap.put(tLCGrpSubInsuredSchema, SysConst.INSERT);
		}
		
		if(tMMap.size() != 0){
			return save(tMMap);
		}
		
		return false;
	}
	
	/**
	 * 获取算费成功的被保人导入信息
	 * 
	 * @return
	 */
	public LCGrpSubInsuredImportSet getSuccInsured(){
		
		String query = "select * from LCGrpSubInsuredImport csii " 
				+ " where grpcontno='" + mGrpContNo + "' "
				+ " and calstate='01' "
				+ " and exists (select 1 from LCGrpPremPlan where grpcontno=csii.grpcontno and premplancalflag=csii.calremark)";
		
		LCGrpSubInsuredImportDB tLCGrpSubInsuredImportDB = new LCGrpSubInsuredImportDB();
		return tLCGrpSubInsuredImportDB.executeQuery(query);
	}
	
	/**
	 * 提交数据库
	 * 
	 * @param tMap 提交map集合
	 * @return
	 */
	private boolean save(MMap tMap) {
		VData mInputData = new VData();
		mInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			System.out.println("---------提交数据库失败！-----------" + tPubSubmit.mErrors.getLastError());
			return false;
		}

		return true;
	}
}
