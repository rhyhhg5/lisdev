/**
 * 2012-12-26
 */
package com.sinosoft.lis.lgrp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sinosoft.lis.vschema.LCGrpSubInsuredImportSet;
import com.sinosoft.utility.TransferData;

/**
 * 大团单算费调度入口
 * 
 * @author 张成轩
 * 
 */
public class LGrpCalInseredDealMainService {

	private TransferData mTransferData = new TransferData();

	private int mCurThreadCount = 0;

	public static final int Job_MaxCount = new Integer(LGrpThreadConfig.threadMaxCount);// 线程数

	public static final int Job_MaxDataCount = new Integer(LGrpThreadConfig.threadDataSize);// 取数数量

	private static int idx = 0;

	private static int count = 0;

	private LGrpCalInseredDealMainService(TransferData tTransferData) {
		this.mTransferData = tTransferData;
	}

	private boolean run() {
		System.out.println("-------------------------");
		ExecutorService exec = null;

		try {
			exec = Executors.newFixedThreadPool(LGrpCalInseredDealMainService.Job_MaxCount);

			// 初始化获取被保人数据类
			LGrpCalGetDataThread tSrcDatas = new LGrpCalGetDataThread(this.mTransferData, 2);
			// 初始化被保人计算处理类
			LGrpInsuredDeal tDeal = new LGrpInsuredDeal(this.mTransferData);

			boolean finishFlag = false;

			while (true) {

				if (finishFlag) {
					break;
				}

				while (!finishFlag) {

					LCGrpSubInsuredImportSet tCurDatas = null;
					// 获取被保人
 					tCurDatas = tSrcDatas.getListData();

					if (tCurDatas == null || tCurDatas.size() == 0) {
						finishFlag = true;
						break;
					}

					this.UpCount();

					// 启动算费处理线程
					Runnable run = new LGrpCalInsuredDealThread(String.valueOf(idx++), tDeal, tCurDatas, this);
					exec.submit(run);

					count++;

					System.out.println("线程数" + this.getCurThreadCount());
					
					// 控制线程数量
					if (this.getCurThreadCount() >= LGrpCalInseredDealMainService.Job_MaxCount) {
						boolean tFlag = true;
						while (tFlag) {
							Thread.sleep(100);
							if (this.getCurThreadCount() < LGrpCalInseredDealMainService.Job_MaxCount) {
								tFlag = false;
							}
						}
					}
				}
			}

			while (true) {
				if (this.getCurThreadCount() == 0) {
					break;
				}
			}

			System.out.println("共运行任务：" + count);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (exec != null) {
				exec.shutdown();
			}
		}

		return true;
	}

	/**
	 * 获取主调类
	 * 
	 * @param tTransferData
	 * @return
	 */
	public static LGrpCalInseredDealMainService getService(TransferData tTransferData) {
		return new LGrpCalInseredDealMainService(tTransferData);
	}

	/**
	 * 启动线程处理
	 * 
	 * @return
	 */
	public boolean startService() {
		boolean tResultFlag = false;

		try {
			tResultFlag = run();
		} catch (Exception e) {
			e.printStackTrace();
			tResultFlag = false;
		}

		return tResultFlag;
	}

	public int getCurThreadCount() {
		return mCurThreadCount;
	}

	public void UpCount() {
		synchronized (this) {
			mCurThreadCount++;
		}
	}

	public void DownCount() {
		synchronized (this) {
			mCurThreadCount--;
		}
	}

}
