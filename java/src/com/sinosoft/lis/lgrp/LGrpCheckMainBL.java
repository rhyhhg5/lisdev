package com.sinosoft.lis.lgrp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 大团单校验处理主调入口类
 * 
 * @author 张成轩
 */
public class LGrpCheckMainBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	private CErrors mErrors = new CErrors();

	private LGrpLock lock;
	
	public boolean submitData(VData tVData, String lockPrtNo) {

		System.out.println("---------大团单校验处理入口类---------");
		
		// 初始化业务锁
		lock = new LGrpLock(2, lockPrtNo);
		
		// 锁定
		if(!lock.lock()){
			mErrors.addOneError("该保单正在进行其它业务处理，请在处理完毕后进行算费操作。保单状态会在页面进行显示，可以点击【刷新】按钮。");
			return false;
		}
		
		try {

			// 线程调用前的校验
			if (!checkData()) {
				return false;
			}
			
			// 开始调用线程
			if (!(dealData(tVData))) {
				return false;
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
			mErrors.addOneError("算费失败：程序异常终止！");
			
			lock.unlock();
			
			return false;
		}

		return true;
	}
	
	/**
	 * 线程调用类
	 * 
	 * @param tVData
	 * @return
	 */
	private boolean dealData(VData tVData) {

		ExecutorService exec = Executors.newFixedThreadPool(1);
		
		LGrpCheckMainThread tLGrpCheckMainThread = new LGrpCheckMainThread(tVData, lock);
		exec.submit(tLGrpCheckMainThread);
		
		return true;
	}
	
	private boolean checkData() {
		return true;
	}

	public CErrors getError() {
		return mErrors;
	}
}
