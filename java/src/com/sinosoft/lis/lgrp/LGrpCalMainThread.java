package com.sinosoft.lis.lgrp;

import com.sinosoft.utility.TransferData;

/**
 * 大团单算费处理主线程控制类
 * 
 * @author 张成轩
 */
public class LGrpCalMainThread implements Runnable {

	private TransferData mTransferData;

	private LGrpLock lock;

	/**
	 * 构造函数
	 * 
	 * @param tVData 
	 * @param lock 业务锁处理对象
	 */
	public LGrpCalMainThread(TransferData tTransferData, LGrpLock lock) {
		this.mTransferData = tTransferData;
		this.lock = lock;
	}

	public void run() {

		// 线程起始校验处理
		if (beginTread()) {
			try {
				long tStart2 = System.nanoTime();

				// 大团单算费串处理
				LGrpCalDealMainService service = LGrpCalDealMainService.getService(mTransferData);
				service.startService();
				
				// 大团单被保人处理 
				LGrpCalSignInsuredDeal tLGrpCalInsuredDeal = new LGrpCalSignInsuredDeal(mTransferData);
				tLGrpCalInsuredDeal.dealData();
				
//				 大团单算费串处理
				LGrpCalInseredDealMainService insService = LGrpCalInseredDealMainService.getService(mTransferData);
				insService.startService();

				long tEnd2 = System.nanoTime();
				System.out.println("大团单算费，耗时：[" + ((tEnd2 - tStart2) / 1000d / 1000d / 1000d) + "s]");
			} catch (RuntimeException e) {
				e.printStackTrace();
			} finally {
				// 线程完毕处理
				finalThread();
			}
		}
	}

	/**
	 * 线程起始校验处理
	 * 
	 * @return
	 */
	public boolean beginTread() {
		System.out.println("----校验锁定-----");
		return lock.checklock();
	}

	/**
	 * 线程完毕处理
	 */
	public void finalThread() {
		System.out.println("----解除锁定-----");
		lock.unlock();
	}

}
