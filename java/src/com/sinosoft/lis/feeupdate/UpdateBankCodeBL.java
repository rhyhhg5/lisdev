package com.sinosoft.lis.feeupdate;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UpdateBankCodeBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputVData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	// 录入
	private String tBankCodeNo = "";
	private String AfterBankUniteCode = "";
	private String AfterBankUniteName = "";

	private MMap mMap = new MMap();

	public UpdateBankCodeBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		// 判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdateBankCodeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start QYModifyMarketTypeBL Submit...");
		if (!tPubSubmit.submitData(mInputVData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputVData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);

		tBankCodeNo = (String) tTransferData.getValueByName("tBankCodeNo");
		if (tBankCodeNo == null || "".equals(tBankCodeNo)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "银行编码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		AfterBankUniteCode = (String) tTransferData.getValueByName("AfterBankUniteCode");
		if (AfterBankUniteCode == null || "".equals(AfterBankUniteCode)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "银联编码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		AfterBankUniteName = (String) tTransferData.getValueByName("AfterBankUniteName");
		if (AfterBankUniteName == null || "".equals(AfterBankUniteName)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "银联名称为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData() {
		
		String mSql="update ldbankunite set modifydate=current date,modifytime=current time,bankunitecode='"+AfterBankUniteCode+"',bankunitename='"+AfterBankUniteName+"' where bankcode='"+tBankCodeNo+"' and bankunitecode in ('7705','7706')" ;		
		mMap.put(mSql, SysConst.UPDATE);
		return true;

	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputVData = new VData();
			this.mInputVData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
