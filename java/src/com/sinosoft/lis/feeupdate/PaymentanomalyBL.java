package com.sinosoft.lis.feeupdate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PaymentanomalyBL {
	public CErrors mErrors = new CErrors();
	 private VData Result = new VData();
	 /** 往后面传输数据的容器 */
	 private VData tVData;
	
	 /** 业务处理相关变量 */
	 private String Flag,appntname,paycode,temp,serialno;
	 //数据库查询对象
	 public ExeSQL exeSql = new ExeSQL();
	 TransferData tTransferData = new TransferData();
	 PubSubmit tPubSubmit = new PubSubmit();
	 MMap tmap = new MMap();
	 /**
		 * 数据提交：外部数据通过该接口传递进来进行业务处理
		 * @param cInputData
		 * @return
		 */
		public boolean submitData(VData cInputData) {
			this.tVData = cInputData;
			//1、获取数据
			if(!getInputData()) {
				return false;
			}
			//2、检查数据
			if(!checkData()) {
				return false;
			}
			//3、处理数据
			if(!dealData()) {
				CError tError = new CError();
				tError.moduleName = "UpdateBankCodeBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;	
		}
		 /**
		 * 获取传递过来的数据
		 * @return
		 */
		private boolean getInputData() {
			System.out.println("开始获取数据...");
			tTransferData = (TransferData)tVData.getObjectByObjectName("TransferData", 0);
			Flag = (String)tTransferData.getValueByName("Flag");
			if("0".equals(Flag)){
				paycode = (String)tTransferData.getValueByName("paycode1");
				serialno = (String)tTransferData.getValueByName("serialno1");
			}else{
				serialno = (String)tTransferData.getValueByName("serialno");
				paycode = (String)tTransferData.getValueByName("paycode");
				String sql1 = "select otherno from ljspay where getnoticeno='"+paycode+"'";
				String otherno = new ExeSQL().getOneValue(sql1);
				if(otherno == null || otherno ==""){
					return false;
				}
				String sql2 = "select appntname from lccont where prtno='"+otherno+"'";
				appntname = new ExeSQL().getOneValue(sql2);
				if(appntname == null || appntname ==""){
					return false;
				}
			    temp = (String)tTransferData.getValueByName("temp");
			}
			System.out.println("数据获取完毕...");
		   	Result.clear();
			return true;
		}
		 /**
		 * 检查是什么业务形式
		 * @return
		 */
		private boolean checkData() {
			System.out.println("开始检查数据...");
			if("1".equals(Flag)) {
				System.out.println("业务形式：修改发送银行盘记录表账户名！");
			}
			if("2".equals(Flag)) {
				System.out.println("业务形式：修改应收总表账户名！");
			}
			if("3".equals(Flag)) {
				System.out.println("业务形式：修改应收总表和发送银行盘记录表账户名！");
			}
			if("0".equals(Flag)){
				System.out.println("业务形式：修改已付费的发送银行盘记录表！");
			}
			System.out.println("数据检查完毕...");
			return true;
		}
		/**
		 * 业务处理
		 * 1、直接添加补扫影像件
		 * 2、替换原有扫描件
		 * 3、删除指定页码影像件
		 * @return
		 */
		private boolean dealData() {
			if("1".equals(Flag)) {
				return updateb();
			}
			if("2".equals(Flag)) {
				return updates();
			}
			if("3".equals(Flag)) {
				return updatea();
			}
			if("0".equals(Flag)){
				return update();
			}
			return true;
		}
	private boolean updateb(){
		String sql = "update lysendtobank set accname='"+appntname+"',modifydate=current date, modifytime=current time where serialno='"+serialno+"' and paycode='"+paycode+"'";
		tmap.put(sql, "UPDATE");
		Result.add(tmap);
		if(!tPubSubmit.submitData(Result, "")){
			return false;
		}
		if("1".equals(temp)){
			updatet();
		}
	  return true;
	}
	private boolean updates(){
		String sql = "update ljspay set accname='"+appntname+"',modifydate=current date, modifytime=current time where getnoticeno='"+paycode+"'";
		tmap.put(sql, "UPDATE");
		Result.add(tmap);
		if(!tPubSubmit.submitData(Result, "")){
			return false;
		}
		if("1".equals(temp)){
			updatet();
		}
	  return true;
	}
	private boolean updatea(){
		String sqlb = "update lysendtobank set accname='"+appntname+"',modifydate=current date, modifytime=current time where serialno='"+serialno+"' and paycode='"+paycode+"'";
		String sqls = "update ljspay set accname='"+appntname+"' modifydate=current date, modifytime=current time where getnoticeno='"+paycode+"'";
		tmap.put(sqlb, "UPDATE");
		tmap.put(sqls, "UPDATE");
		Result.add(tmap);
		if("1".equals(temp)){
			updatet();
		}
		if(!tPubSubmit.submitData(Result, "")){
			return false;
		}
		/*if(!updateb()){
			return false;
		}
		if(!updates()){
			return false;
		}*/
	  return true;
	}
	private boolean updatet(){
		String sql = "update ljtempfeeclass set accname='"+appntname+"',modifydate=current date, modifytime=current time where tempfeeno='"+paycode+"'";
		tmap.put(sql, "UPDATE");
		Result.add(tmap);
		if(!tPubSubmit.submitData(Result, "")){
			return false;
		}
	  return true;
	}
    private boolean update(){
    	String sql = "select sumgetmoney from ljaget where actugetno='"+paycode+"'";
    	String money = new ExeSQL().getOneValue(sql);
    	
    	//查询收费时间
    	String sqldate = "select enteraccdate from ljfiget where actugetno='"+paycode+"'";
    	String date = new ExeSQL().getOneValue(sqldate);
    	if(money == null || money ==""){
    		return false;
    	}
    	String sqlb = "update lybanklog set totalmoney=totalmoney-'"+money+"',totalnum=totalnum-'"+1+"',modifydate=current date, modifytime=current time where serialno='"+serialno+"'";
    	String sqld = "delete from lysendtobank where serialno='"+serialno+"' and paycode='"+paycode+"'";
    	String sql1 = "update ljaget set enteraccdate='"+date+"', modifydate=current date, modifytime=current time where actugetno='"+paycode+"'";
    	tmap.put(sqlb, "UPDATE");
    	tmap.put(sqld, "DELETE");
    	tmap.put(sql1, "UPDATE");
		Result.add(tmap);
		if(!tPubSubmit.submitData(Result, "")){
			return false;
		}
	  return true;
    }

}
