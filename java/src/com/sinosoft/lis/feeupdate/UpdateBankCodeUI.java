package com.sinosoft.lis.feeupdate;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class UpdateBankCodeUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public UpdateBankCodeUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();
		UpdateBankCodeBL updateBankCodeBL = new UpdateBankCodeBL();
		try {
			if (!updateBankCodeBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(updateBankCodeBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "LAContReceiveUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
