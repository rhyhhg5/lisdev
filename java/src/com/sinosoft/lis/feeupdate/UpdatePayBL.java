/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.feeupdate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJFIGetDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UpdatePayBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	// 录入
	private String LActuGetNo = "";
	private String LManageCom = "";
	private String LInsBankCode = "";
	private String LInsBankAccNo = "";
	private String LMakeDate = "";
	private String LConfDate = "";
	private String flag = null;
	// private String AfterTempFeeType = "";

	private MMap mMap = new MMap();

	public UpdatePayBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		// 判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start QYModifyMarketTypeBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);

		LActuGetNo = (String) tTransferData.getValueByName("TActuGetNo");
		if (LActuGetNo == null || "".equals(LActuGetNo)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "付费号码为空！";
			this.mErrors.addOneError(tError);
			System.out.println(LActuGetNo);
			return false;
		}
		LManageCom = (String) tTransferData.getValueByName("TManageCom");
		if (LManageCom == null || "".equals(LManageCom)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "付费机构为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		LInsBankCode = (String) tTransferData.getValueByName("TInsBankCode");
		if (LInsBankCode == null || "".equals(LInsBankCode)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保险公司帐号开户行为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		LInsBankAccNo = (String) tTransferData.getValueByName("TInsBankAccNo");
		if (LInsBankAccNo == null || "".equals(LInsBankAccNo)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保险公司开户账号为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		LMakeDate = (String) tTransferData.getValueByName("TConfDate");
/*		if (LConfDate == null || "".equals(LConfDate)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "财务确认日期为空！";
			this.mErrors.addOneError(tError);
			return false;
		}*/
		flag = (String) tTransferData.getValueByName("flag");
		if (flag == null || "".equals(flag)) {
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "标记为空！";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}



	private boolean dealData() {

		if ("0".equals(flag)) {
			ExeSQL mExeSQL = new ExeSQL();
			String sqlPaymode = "select paymode from LJFIGet where ActuGetNo = '"
					+ LActuGetNo + "'";
			SSRS ss =  mExeSQL.execSQL(sqlPaymode);
			String payMode = ss.GetText(1, 1);
			

			String mSql = " update ljaget set InsBankCode= '" + LInsBankCode + "',InsBankAccNo= '"
					+ LInsBankAccNo + "' and paymode ='"+payMode+"'  where ActuGetNo = '" + LActuGetNo + "'";
			String mSql1= " update ljfiget set ManageCom= '" + LManageCom
					+ "'  where ActuGetNo = '" + LActuGetNo + "'";

			// LConfDate


			mMap.put(mSql, SysConst.UPDATE);
			mMap.put(mSql1, SysConst.UPDATE);


			return true;
		} else if ("1".equals(flag)) {
			// 查询LJfiget表
			String sql1 = "select * from LJFIGet where ActuGetNo = '"
					+ LActuGetNo + "'";
			Reflections ref = new Reflections();

			LJFIGetDB mLJFIGetDB = new LJFIGetDB();
			LJFIGetSet mLJFIGetSet = new LJFIGetSet();
			LJFIGetSet mLJFIGetSetNew = new LJFIGetSet();
			mLJFIGetSet = mLJFIGetDB.executeQuery(sql1);

			if (mLJFIGetSet.size() != 0) {
				for (int i = 0; i < mLJFIGetSet.size(); i++) {
					LJFIGetSchema ljFIGetSchemaF = new LJFIGetSchema();
					LJFIGetSchema ljFIGetSchemaZ = new LJFIGetSchema();
					// 冲负
					ref.transFields(ljFIGetSchemaF, mLJFIGetSet.get(i + 1));
					ljFIGetSchemaF.setActuGetNo(ljFIGetSchemaF.getActuGetNo()
							+ "F");
					ljFIGetSchemaF.setGetMoney(-ljFIGetSchemaF.getGetMoney());
					ljFIGetSchemaF.setConfDate(LMakeDate);
					ljFIGetSchemaF.setMakeDate(PubFun.getCurrentDate());
					ljFIGetSchemaF.setMakeTime(PubFun.getCurrentTime());
					ljFIGetSchemaF.setModifyDate(PubFun.getCurrentDate());
					ljFIGetSchemaF.setModifyTime(PubFun.getCurrentTime());

					// 冲正
					ref.transFields(ljFIGetSchemaZ, mLJFIGetSet.get(i + 1));
					ljFIGetSchemaZ.setActuGetNo(ljFIGetSchemaZ.getActuGetNo()
							+ "Z");
					ljFIGetSchemaZ.setGetMoney(ljFIGetSchemaZ.getGetMoney());
					ljFIGetSchemaZ.setConfDate(LMakeDate);
					ljFIGetSchemaZ.setManageCom(LManageCom);
					ljFIGetSchemaZ.setMakeDate(PubFun.getCurrentDate());
					ljFIGetSchemaZ.setMakeTime(PubFun.getCurrentTime());
					ljFIGetSchemaZ.setModifyDate(PubFun.getCurrentDate());
					ljFIGetSchemaZ.setModifyTime(PubFun.getCurrentTime());
					
					mLJFIGetSetNew.add(ljFIGetSchemaF);
					mLJFIGetSetNew.add(ljFIGetSchemaZ);
				}
			}

			// 查询LJaget表
			String sql2 = "select * from ljaget where ActuGetNo = '"
					+ LActuGetNo + "'";
			LJAGetDB mLJAGetDB = new LJAGetDB();
			LJAGetSet mLJAGetSet = new LJAGetSet();
			LJAGetSet mLJAGetSetNew = new LJAGetSet();
			mLJAGetSet = mLJAGetDB.executeQuery(sql2);

			if (mLJAGetSet.size() != 0) {
				for (int i = 0; i < mLJAGetSet.size(); i++) {
					LJAGetSchema ljAGetSchemaF = new LJAGetSchema();
					LJAGetSchema ljAGetSchemaZ = new LJAGetSchema();
					// 冲负
					ref.transFields(ljAGetSchemaF, mLJAGetSet.get(i + 1));
					ljAGetSchemaF.setActuGetNo(ljAGetSchemaF.getActuGetNo()
							+ "F");
					ljAGetSchemaF.setSumGetMoney(-ljAGetSchemaF
							.getSumGetMoney());
					ljAGetSchemaF.setMakeDate(LMakeDate);
					ljAGetSchemaF.setMakeTime(PubFun.getCurrentTime());
					ljAGetSchemaF.setModifyDate(PubFun.getCurrentDate());
					ljAGetSchemaF.setModifyTime(PubFun.getCurrentTime());

					// 冲正
					ref.transFields(ljAGetSchemaZ, mLJAGetSet.get(i + 1));
					ljAGetSchemaZ.setActuGetNo(ljAGetSchemaZ.getActuGetNo()
							+ "Z");
					ljAGetSchemaZ.setMakeDate(LMakeDate);
					ljAGetSchemaZ.setMakeTime(PubFun.getCurrentTime());
					ljAGetSchemaZ.setModifyDate(PubFun.getCurrentDate());
					ljAGetSchemaZ.setModifyTime(PubFun.getCurrentTime());
					ljAGetSchemaZ.setSumGetMoney(ljAGetSchemaZ
							.getSumGetMoney());
					ljAGetSchemaZ.setInsBankAccNo(LInsBankAccNo);
					ljAGetSchemaZ.setInsBankCode(LInsBankCode);
					//ljAGetSchemaZ.setManageCom(LManageCom);
					
					mLJAGetSetNew.add(ljAGetSchemaF);
					mLJAGetSetNew.add(ljAGetSchemaZ);

				}
			}
			mMap.put(mLJFIGetSetNew, "INSERT");
			mMap.put(mLJAGetSetNew, "INSERT");
			return true;
		}

		else {
			return false;
		}
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdatePayBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
