package com.sinosoft.lis.feeupdate;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UpdatePadToBatchBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    private String tGetnodiceNo;
    
    private String AfterNo1;


    /**
     * submitData
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

        return true;
    }

    /**
     * getInputData
     * 
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {

            TransferData mTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            tGetnodiceNo = (String) mTransferData.getValueByName("GetnodiceNo");
            AfterNo1 = (String) mTransferData.getValueByName("afterNo1");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        if (!dealPadToBatch())
        {
            return false;
        }

        return true;
    }

    /**
     * @return
     */
    private boolean dealPadToBatch()
    {
        map.put("update ljspay set Cansendbank='"+AfterNo1+"',modifydate=current date ,modifytime=current time  where Getnoticeno='"+tGetnodiceNo+"'", "UPDATE");
        
        return true;
    }
    
	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdatePadToBatchBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}
}
