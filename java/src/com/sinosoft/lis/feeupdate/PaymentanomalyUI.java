package com.sinosoft.lis.feeupdate;

import com.sinosoft.lis.feeupdate.PaymentanomalyBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PaymentanomalyUI {
public CErrors mErrors = new CErrors();
	
	private PaymentanomalyBL pdBL;
	
	public PaymentanomalyUI() {
		this.pdBL = new PaymentanomalyBL();
	}
	
	public boolean submiteData(VData cInputData) {
		if(!pdBL.submitData(cInputData))
        {
            mErrors.copyAllErrors(pdBL.mErrors);
            return false;
        }
        return true;
	}
	

}
