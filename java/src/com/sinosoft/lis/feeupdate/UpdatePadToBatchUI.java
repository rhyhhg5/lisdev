package com.sinosoft.lis.feeupdate;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class UpdatePadToBatchUI {
	/** �������� */
    public CErrors mErrors = new CErrors();

    public UpdatePadToBatchUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	UpdatePadToBatchBL tUpdatePadToBatchBL = new UpdatePadToBatchBL();
            if (!tUpdatePadToBatchBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tUpdatePadToBatchBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
