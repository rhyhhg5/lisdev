/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.feeupdate;

import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UpdateFanChongTempFeeTypeBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	// 录入
	private String tTempFeeNo = "";
	private String tPayMoney = "";
	private String AfterTempFeeType = "";
	private String AfterOtherNoType = "";

	private MMap mMap = new MMap();

	public UpdateFanChongTempFeeTypeBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		// 判断所需参数是否为空
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpdateTempFeeTypeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start QYModifyMarketTypeBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);

		tTempFeeNo = (String) tTransferData.getValueByName("tTempFeeNo");
		if (tTempFeeNo == null || "".equals(tTempFeeNo)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "暂交费收据号码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		/*
		 * tPayMoney = (String) tTransferData.getValueByName("tPayMoney"); if
		 * (tTempFeeNo == null || "".equals(tPayMoney)) { CError tError = new
		 * CError(); tError.moduleName = "QYModifyAmntBL"; tError.functionName =
		 * "getInputData"; tError.errorMessage = "暂交费金额为空！";
		 * this.mErrors.addOneError(tError); return false; }
		 */
		AfterTempFeeType = (String) tTransferData
				.getValueByName("AfterTempFeeType");
		if (AfterTempFeeType == null || "".equals(AfterTempFeeType)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "暂交费收据号类型为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		AfterOtherNoType = (String) tTransferData
				.getValueByName("AfterOtherNoType");
		if (AfterOtherNoType == null || "".equals(AfterOtherNoType)) {
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "其他号类型为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData() {

		// 查询ljtempfee
		String sql1 = "select * from ljtempfee where TempFeeNo = '"
				+ tTempFeeNo + "'";

		Reflections ref = new Reflections();
		// ref.transFields(tLIWrapExListSchema, tWFWrapExListSchema);

		LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
		LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
		LJTempFeeSet mLJTempFeeSetNew = new LJTempFeeSet();
		mLJTempFeeSet = mLJTempFeeDB.executeQuery(sql1);

		if (mLJTempFeeSet.size() != 0) {
			for (int i = 0; i < mLJTempFeeSet.size(); i++) {
				LJTempFeeSchema ljTempFeeSchemaF = new LJTempFeeSchema();
				LJTempFeeSchema ljTempFeeSchemaZ = new LJTempFeeSchema();
				// 冲负
				ref.transFields(ljTempFeeSchemaF, mLJTempFeeSet.get(i + 1));
				ljTempFeeSchemaF.setPayMoney(-ljTempFeeSchemaF.getPayMoney());
				ljTempFeeSchemaF.setTempFeeNo(ljTempFeeSchemaF.getTempFeeNo()
						+ "F");
				ljTempFeeSchemaF.setConfMakeDate(CurrentDate);
				ljTempFeeSchemaF.setConfMakeTime(CurrentTime);
				ljTempFeeSchemaF.setModifyDate(CurrentDate);
				ljTempFeeSchemaF.setModifyTime(CurrentTime);
				ljTempFeeSchemaF.setMakeDate(CurrentDate);
				ljTempFeeSchemaF.setMakeTime(CurrentTime);

				/*
				 * ljTempFeeSchemaF.setCONF(ljTempFeeSchemaF.getTempFeeNo() +
				 * "F");
				 */
				// 冲正
				ref.transFields(ljTempFeeSchemaZ, mLJTempFeeSet.get(i + 1));
				ljTempFeeSchemaZ.setPayMoney(ljTempFeeSchemaZ.getPayMoney());
				ljTempFeeSchemaZ.setTempFeeNo(ljTempFeeSchemaZ.getTempFeeNo()
						+ "Z");
				ljTempFeeSchemaZ.setTempFeeType(AfterTempFeeType);
				ljTempFeeSchemaZ.setOtherNoType(AfterOtherNoType);
				ljTempFeeSchemaZ.setConfMakeDate(CurrentDate);
				ljTempFeeSchemaZ.setConfMakeTime(CurrentTime);
				ljTempFeeSchemaZ.setModifyDate(CurrentDate);
				ljTempFeeSchemaZ.setModifyTime(CurrentTime);
				ljTempFeeSchemaZ.setMakeDate(CurrentDate);
				ljTempFeeSchemaZ.setMakeTime(CurrentTime);
				mLJTempFeeSetNew.add(ljTempFeeSchemaZ);
				mLJTempFeeSetNew.add(ljTempFeeSchemaF);
				System.out.println(mLJTempFeeSetNew.size());

			}

			// 查询ljtempfeeclass
			String sql2 = "select * from ljtempfeeclass where TempFeeNo = '"
					+ tTempFeeNo + "'";
			LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
			LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
			LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
			mLJTempFeeClassSet = mLJTempFeeClassDB.executeQuery(sql2);
			if (mLJTempFeeClassSet.size() != 0) {
				for (int i = 0; i < mLJTempFeeClassSet.size(); i++) {
					LJTempFeeClassSchema ljTempFeeClassSchemaF = new LJTempFeeClassSchema();
					LJTempFeeClassSchema ljTempFeeClassSchemaZ = new LJTempFeeClassSchema();
					// 冲负
					ref.transFields(ljTempFeeClassSchemaF,
							mLJTempFeeClassSet.get(i + 1));
					ljTempFeeClassSchemaF.setConfMakeDate(CurrentDate);
					ljTempFeeClassSchemaF.setModifyDate(CurrentDate);
					ljTempFeeClassSchemaF.setModifyTime(CurrentTime);
					ljTempFeeClassSchemaF.setMakeDate(CurrentDate);
					ljTempFeeClassSchemaF.setMakeTime(CurrentTime);
					ljTempFeeClassSchemaF.setPayMoney(-ljTempFeeClassSchemaF
							.getPayMoney());
					ljTempFeeClassSchemaF.setTempFeeNo(ljTempFeeClassSchemaF
							.getTempFeeNo() + "F");

					// 冲正
					ref.transFields(ljTempFeeClassSchemaZ,
							mLJTempFeeClassSet.get(i + 1));
					ljTempFeeClassSchemaZ.setConfMakeDate(CurrentDate);
					ljTempFeeClassSchemaZ.setModifyDate(CurrentDate);
					ljTempFeeClassSchemaZ.setModifyTime(CurrentTime);
					ljTempFeeClassSchemaZ.setMakeDate(CurrentDate);
					ljTempFeeClassSchemaZ.setMakeTime(CurrentTime);
					ljTempFeeClassSchemaZ.setPayMoney(ljTempFeeClassSchemaZ
							.getPayMoney());
					ljTempFeeClassSchemaZ.setTempFeeNo(ljTempFeeClassSchemaZ
							.getTempFeeNo() + "Z");
					mLJTempFeeClassSetNew.add(ljTempFeeClassSchemaF);
					mLJTempFeeClassSetNew.add(ljTempFeeClassSchemaZ);

				}
			}

			mMap.put(mLJTempFeeSetNew, "INSERT");
			mMap.put(mLJTempFeeClassSetNew, "INSERT");
		}

		return true;

	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
