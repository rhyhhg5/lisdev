package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.health.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LHScanSinglPrtBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private SSRS mSSRS;
    private VData mResult = new VData();
    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LHScanInfoSet mLHScanInfoSet = new LHScanInfoSet();
    private LHScanInfoSchema mLHScanInfoSchema = new LHScanInfoSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private String mOperate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    public LHScanSinglPrtBL() {}

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("LHScanSinglPrtBL Start");
        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT") )
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        // 准备所有要打印的数据
        if (!getPrintData())
            return false;

        if(cOperate.equals("PRINT") )
        {
            if (!dealPrintMag())
            {
                return false;
            }
        }

        System.out.println("LHScanSinglPrtBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LHScanSinglPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLDSysVarSchema = (LDSysVarSchema) cInputData.getObjectByObjectName("LDSysVarSchema", 0);
        if( mLDSysVarSchema == null || mGlobalInput == null)
        {
            buildError("getInputData", "mLDSysVarSchema或mGlobalInput为空！");
            return false;
        }
        mLHScanInfoSchema = (LHScanInfoSchema) cInputData.getObjectByObjectName("LHScanInfoSchema", 0);
        if (mLHScanInfoSchema == null )
        {
            buildError("getInputData", "mLHScanInfoSchema为空--打印入口没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private boolean getPrintData()
    {
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LHScanInfo.vts", "printer");
//        setFixedInfo();
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(mLHScanInfoSchema.getOtherNo());

        textTag.add("SerialNo", mLHScanInfoSchema.getSerialNo());
        textTag.add("CustomerNo", mLHScanInfoSchema.getOtherNo());
        System.out.println("-----------------"+tLDPersonDB.query().get(1).getName());
        textTag.add("CustomerName", tLDPersonDB.query().get(1).getName());
        textTag.add("BarCode1", mLHScanInfoSchema.getSerialNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("GetNoticeNo" + mLHScanInfoSchema.getSerialNo());

        if (textTag.size() > 0)
        {
            xmlexport.addTextTag(textTag);
        }

//        String[] title = {"流水号", "客户号码", "客户姓名"};
//        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\","LHScanSinglPrt");
        mResult.clear();
        mResult.addElement(xmlexport);


        if(mOperate.equals("PRINT"))
        {
            //放入打印列表
            mLOPRTManagerSchema.setPrtSeq(mLHScanInfoSchema.getSerialNo());
            mLOPRTManagerSchema.setOtherNo(mLHScanInfoSchema.getOtherNo());
            mLOPRTManagerSchema.setOtherNoType(mLHScanInfoSchema.getOtherNoType());
            mLOPRTManagerSchema.setCode("HM01");
            mLOPRTManagerSchema.setReqCom(mGlobalInput.ComCode);
            mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        }
        mResult.add(mLOPRTManagerSchema);
        return true;
    }


    private void setFixedInfo()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLHScanInfoSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++)
        {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++)
            {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.SignDate ")
            .append("from LBCont a, LCRnewStateLog b ")
            .append("where a.contNo = b.newContNo ")
            .append("   and a.contNo = '")
            .append("' order by b.newContNo ");
        ExeSQL e = new ExeSQL();
        String sourceSignDate = e.getOneValue(sql.toString());

        if(sourceSignDate.equals("null"))
        {
            sourceSignDate = "";
        }

        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL("");
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LHScanSinglPrtBL--MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] a)
    {
        LJSPayBDB mLHScanInfoSet = new LJSPayBDB();
        mLHScanInfoSet.setGetNoticeNo("31000001965");
        mLHScanInfoSet.getInfo();

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo(mLHScanInfoSet.getOtherNo());

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tLCContSchema);
        tVData.addElement(tG);
        tVData.addElement(mLHScanInfoSet);

        LHScanSinglPrtBL bl = new LHScanSinglPrtBL();
        if(bl.submitData(tVData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag()
    {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false)
        {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
