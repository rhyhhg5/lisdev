package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

public class LARenewCountReportBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的险种号
    private String mRiskCode = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得前台传入结束日期
    private String mEndMonth = "";
 //取得前台传入起始日期
    private String mStartMonth = "";
    //业务处理相关变量
    /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private String  mdate = PubFun.getCurrentDate();
   private String  mtime = PubFun.getCurrentTime();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mAgentCode = (String)cInputData.get(2);
    	mRiskCode = (String)cInputData.get(3);
    	mStartMonth = (String)cInputData.get(4);
    	mEndMonth = (String)cInputData.get(5);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LARenewCount.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LARenewal");
        String[] title = {"", "", "", "", "","","","","","","","","",""};
       //根据前台传入的月份
        ExeSQL tExeSQL = new ExeSQL();
        String startDate = mStartMonth.substring(0,4)+"-"+mStartMonth.substring(4,6)+"-01";//统计起期月份的1号      
        String sql = "select date('"+startDate+"')-14 month from dual";
        String laststartdate = tExeSQL.getOneValue(sql);
        //追溯14个月后月份加1个月减1天
        String enddate = mEndMonth.substring(0,4)+"-"+mEndMonth.substring(4,6)+"-01";
        String tSQL = "select date('"+enddate+"')-13 month from dual";
        String  lastdate = tExeSQL.getOneValue(tSQL);
        String ttSQL = "select date('"+lastdate+"')-1 day from dual";
          //获得追溯14个月后的当月最后一天
         String lastenddate = tExeSQL.getOneValue(ttSQL);
         //当前时间的最后止期
         String currenlastdate = "";
         //String curr = mdate.substring(0,7).replace("-","");
         String curr = AgentPubFun.formatDate(mdate, "yyyyMM");        
         if(Integer.parseInt(mEndMonth)>=Integer.parseInt(curr))
         {
          currenlastdate = mdate;
         }
         else
         {
          String currendateSQL = "select date('"+enddate+"')+1 month from dual";
          String currdd =  tExeSQL.getOneValue(currendateSQL);
          String tttSQL = "select date('"+currdd+"')-1 day from dual";
          currenlastdate = tExeSQL.getOneValue(tttSQL);
         }
        msql = "select a.contno,a.p14,a.p11,"
        	  + " a.riskcode,a.transmoney,getunitecode(b.agentcode),"
        	  + "(select name from laagent where laagent.agentcode=a.agentcode),"
        	  +" (select agentgrade from latree where latree.agentcode=a.agentcode),"
        	  + "(select branchattr from labranchgroup  where labranchgroup.agentgroup=a.branchcode) h,"
        	  + "(select name from labranchgroup  where labranchgroup.agentgroup=a.branchcode),"
        	  + "a.CValidate,a.polno"  //保单在统计区间续期判断 
        	  + " from lacommision a,lccont b  "
        	  + " where a.contno=b.contno and a.branchtype='1' and a.branchtype2='01' and "
        	  + " a.renewcount=0 and a.payyear=0  and "
        	  + " a.CValidate>='"+laststartdate+"' and  a.CValidate<='"+lastenddate+"' and a.managecom like '"+mManageCom+"%'" +
        	  		" and a.riskcode in (select riskcode from lmriskapp where riskperiod='M') ";

       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and b.agentcode=getAgentCode('"+this.mAgentCode+"') ";  	   
       }
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
    	   msql+= " and a.RiskCode='"+this.mRiskCode+"'";    	   
       }
       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
       {
    	   msql+=" and a.branchattr like '"+mBranchAttr+"%'";
       }
       msql += " order by h,b.agentcode";
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的保单信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 //根据查询出来的保单，再查询其他信息   	  
    	  for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    		  String phone = "";
    		  String flag = "";
    		  String address = "";
    		String xbSQL = "select  case (select value(sum(transmoney),0) from lacommision f where  f.renewcount>0 and f.polno='"+mSSRS.GetText(i, 12)+"'" +
    				" and f.tmakedate>='"+startDate+"' and f.tmakedate<='"+currenlastdate+"' )when 0 then '否' else '是'  end from dual"; 
    		
    		String lxSQL = "select e.postaladdress from lcaddress e " +
    				" where e.customerno = (select appntno from lccont where contno='"+mSSRS.GetText(i, 1)+"') and e.addressno=(select addressno from lcappnt where lcappnt.contno = '"+mSSRS.GetText(i, 1)+"')";		
    	    
    		String adSQL = "select e.mobile from lcaddress e where e.customerno = (select appntno from lccont where contno='"+mSSRS.GetText(i, 1)+"') and e.addressno=(select addressno from lcappnt where lcappnt.contno = '"+mSSRS.GetText(i, 1)+"')";
    	  ExeSQL tmExeSQL = new ExeSQL();
    	  phone = tmExeSQL.getOneValue(adSQL);

    	 String bzflag = tmExeSQL.getOneValue(xbSQL);
    	  address = tmExeSQL.getOneValue(lxSQL);

    	  String Info[] = new String[14];
    	 System.out.println(address);
    	 System.out.println(flag);
    	  Info[0] = mSSRS.GetText(i, 1);
    	  System.out.println(phone);
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = address;
    	  Info[4] = phone;
    	  Info[5] = mSSRS.GetText(i, 4);
    	  Info[6] = mSSRS.GetText(i, 5);
    	  Info[7] = mSSRS.GetText(i, 6);
    	  Info[8] = mSSRS.GetText(i, 7);
    	  Info[9] = mSSRS.GetText(i, 8);
    	  Info[10] = mSSRS.GetText(i, 9);
    	  Info[11] = mSSRS.GetText(i, 10);
    	  Info[12] = mSSRS.GetText(i, 11);
    	  Info[13] = bzflag;    	  
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的孤儿单信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
         String manageComName = getName(mManageCom);
         texttag.add("StartMonth", mStartMonth);  
         texttag.add("EndMonth", mEndMonth);
         texttag.add("ManageCom", manageComName);
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
   private String getName(String ComCode)
   {
	   String ComSQL = "select name from ldcom where comcode = '"+ComCode+"'";
	   ExeSQL mExeSQL = new ExeSQL();
	   String name = mExeSQL.getOneValue(ComSQL);
	   if(mExeSQL.mErrors.needDealError())
	   {
		   mErrors.copyAllErrors(mExeSQL.mErrors);
           return "";		   
	   }
	   return name;
   }
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
