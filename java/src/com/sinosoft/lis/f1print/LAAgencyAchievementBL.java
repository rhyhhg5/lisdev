package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

class LAAgencyAchievementBL {

    public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate = "";
       private String mEndDate = "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
//       private XmlExport mXmlExport = null;
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;
       private String  tMakeDate = mPubFun.getCurrentDate();
       private String tMakeTime = mPubFun.getCurrentTime();
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }
       // 得到外部传入的数据，将数据备份到本类中
       if (!getInputData(mInputData)) {
             return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }
         return true;
     }

     public VData getResult() {
         return mResult;
    }

     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAAgencyAchievementBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

      private boolean getInputData(VData cInputData)
      {

          try {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));

              //页面传入的数据 3个
              this.mManageCom = (String) cInputData.get(1);
              this.mStartDate = (String) cInputData.get(2);
              this.mEndDate = (String) cInputData.get(3);
              System.out.println(mManageCom);
          }
           catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

      }

  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAAgencyAchievementBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



  private boolean queryData()
  {
      try {
          // 查询数据
          if (!getDataList()) {
              return false;
          }
          if(mListTable==null||mListTable.equals(""))
          {
              CError.buildErr(this, "没有满足条件的值！");
              return false;
          }  
          //新建一个TextTag的实例
          TextTag tTextTag = new TextTag();
          String tTitle[] ={"","","","","",""};
          System.out.print("dayin252");
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
 //         tTextTag.add("tName", mManageName);

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAAgencyAchievement .vts", "printer"); //初始化xml文档
          if (tTextTag.size() > 0)
              xmlexport.addTextTag(tTextTag); //添加动态文本标签
          mListTable.setName("WorkEfficencyReport");
          xmlexport.addListTable(mListTable,tTitle); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new1");
          this.mResult.clear();
          mResult.addElement(xmlexport);
          return true;

      } catch (Exception ex) {
          buildError("queryData", "LABL发生错误，准备数据时出错！");
          return false;
      }
  }
  private boolean getDataList()
   {
       
       String  sql = "";
       //查询中介销售机构
       sql = "select agentcom from lacom where branchtype='2' and branchtype2='02' and sellflag='Y'";
      if(mManageCom!=null&&!mManageCom.equals(""))
      {
          sql += " and managecom like '"+mManageCom+"%'";
      }
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = new SSRS();
      tSSRS = tExeSQL.execSQL(sql);
      if(tSSRS.getMaxRow()>0)
      {    
          for(int i=1;i<=tSSRS.getMaxRow();i++)
          {                
              String info[] = new String[6];
              info[0] = tSSRS.GetText(i, 1);
              info[1] = getAgent(tSSRS.GetText(i, 1));
              info[2] = getName(info[1]);
              info[3] = getPrem(tSSRS.GetText(i, 1));
              info[4] = getappntno(tSSRS.GetText(i, 1));
              info[5] = getp12no(tSSRS.GetText(i, 1));
              mListTable.add(info);    
        }
      }
      
       return true;
   }
      
  private String getAgent(String tAgentCom)
  {
      String AgentCode = "";
      String SQL = "select getUniteCode(agentcode) from lacomtoagent where agentcom='"+tAgentCom+"'";
      ExeSQL tExeSQL = new ExeSQL();
      AgentCode = tExeSQL.getOneValue(SQL);
      if(AgentCode==null||AgentCode.equals(""))
      {
          return "";
      }
      return AgentCode;
  }
  private String getName(String cAgentCode)
  {
      String Name = "";
      String SQL = "select name from laagent where groupAgentcode='"+cAgentCode+"' and branchtype='2'";
      ExeSQL tExeSQL = new ExeSQL();
      Name = tExeSQL.getOneValue(SQL);
      if(Name==null||Name.equals(""))
      {
          return "";
      }
      return Name;
  }
  private String getPrem(String cAgentCom)
  {
      String Prem = "";
      String year = tMakeDate.substring(0, 4);
      String tstartdate = year + "-01-01";
      String SQL  = "";
      SQL = "select value(sum(transmoney),0) from lacommision where agentcom='"+cAgentCom+"' and grpcontno<>'00000000000000000000' and branchtype='2' and branchtype2='02'";
      if (mStartDate == null || mStartDate.equals("")) {
          SQL += " and tmakedate>='" + tstartdate + "'";
      } else {
          SQL += " and tmakedate>='" + mStartDate + "'";
      }
      if (mEndDate == null || mEndDate.equals("")) {
          SQL += " and tmakedate<='" + tMakeDate + "'";
      } else {
          SQL += " and tmakedate<= '" + mEndDate + "'";
      }
      ExeSQL tExeSQL = new ExeSQL();
      Prem = tExeSQL.getOneValue(SQL);
      if(Prem==null||Prem.equals(""))
      {
          return "";
      }
      return Prem;
  }
   private String getappntno(String cAgentCom)
   {
       String t1SQL = "";
       String  t2SQL = "";
       int num = 0;
       String year = tMakeDate.substring(0, 4);
       String tstartdate = year + "-01-01";
       t1SQL = "select count(distinct appntno)"
               +" FROM lacommision "
               +" where agentcom = '"+cAgentCom+"'"
               +" and grpcontno<>'00000000000000000000'"
               +" and transtype='ZC'";   
       if (mStartDate == null || mStartDate.equals("")) {
           t1SQL += " and tmakedate>='" + tstartdate + "'";
       } else {
           t1SQL += " and tmakedate>='" + mStartDate + "'";
       }
       if (mEndDate == null || mEndDate.equals("")) {
           t1SQL += " and tmakedate<='" + tMakeDate + "'";
       } else {
           t1SQL += " and tmakedate <= '" + mEndDate + "'";
       }
       ExeSQL ttExeSQL = new ExeSQL();
       String t1 = ttExeSQL.getOneValue(t1SQL);
       
       t2SQL = "select count(distinct appntno)"
           +" FROM  LACommision"
           +" where agentcom = '"+cAgentCom+"'"
           +" and grpcontno<>'00000000000000000000'"
           +" and transtype='WT'";   
   if (mStartDate == null || mStartDate.equals("")) {
       t2SQL += " and tmakedate>='" + tstartdate + "'";
   } else {
       t2SQL += " and tmakedate>='" + mStartDate + "'";
   }
   if (mEndDate == null || mEndDate.equals("")) {
       t2SQL += " and tmakedate<='" + tMakeDate + "'";
   } else {
       t2SQL += " and tmakedate <= '" + mEndDate + "'";
   }
   ExeSQL tExeSQL = new ExeSQL();
   String t2 = tExeSQL.getOneValue(t2SQL);
   num = Integer.parseInt(t1)-Integer.parseInt(t2);
    return String.valueOf(num);
   }
   private String getp12no(String cAgentCom)
   {
       String t1SQL = "";
       String  t2SQL = "";
       int num = 0;
       String year = tMakeDate.substring(0, 4);
       String tstartdate = year + "-01-01";
       t1SQL = "select count(distinct appntno)"
               +" FROM  LACommision"
               +" where agentcom = '"+cAgentCom+"'"
               +" and grpcontno ='00000000000000000000'"
               +" and transtype='ZC'";   
       if (mStartDate == null || mStartDate.equals("")) {
           t1SQL += " and tmakedate>='" + tstartdate + "'";
       } else {
           t1SQL += " and tmakedate>='" + mStartDate + "'";
       }
       if (mEndDate == null || mEndDate.equals("")) {
           t1SQL += " and tmakedate<='" + tMakeDate + "'";
       } else {
           t1SQL += " and tmakedate <= '" + mEndDate + "'";
       }
       ExeSQL ttExeSQL = new ExeSQL();
       String t1 = ttExeSQL.getOneValue(t1SQL);
       
       t2SQL = "select count(distinct appntno)"
           +" FROM  LACommision"
           +" where agentcom = '"+cAgentCom+"'"
           +" and grpcontno ='00000000000000000000'"
           +" and transtype='WT'";   
   if (mStartDate == null || mStartDate.equals("")) {
       t2SQL += " and tmakedate>='" + tstartdate + "'";
   } else {
       t2SQL += " and tmakedate>='" + mStartDate + "'";
   }
   if (mEndDate == null || mEndDate.equals("")) {
       t2SQL += " and tmakedate<='" + tMakeDate + "'";
   } else {
       t2SQL += " and tmakedate <= '" + mEndDate + "'";
   }
   ExeSQL tExeSQL = new ExeSQL();
   String t2 = tExeSQL.getOneValue(t2SQL);
   num = Integer.parseInt(t1)-Integer.parseInt(t2);
    return String.valueOf(num);
   }



}


