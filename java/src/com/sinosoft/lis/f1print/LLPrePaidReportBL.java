package com.sinosoft.lis.f1print;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLPrePaidReportBL {

	  public CErrors mErrors = new CErrors();

	    private VData mResult = new VData();

	    /** 全局变量 */
	    private GlobalInput mGlobalInput = new GlobalInput();

	    private String mManageCom = "";

	    private String mStartDate = "";

	    private String mEndDate = "";
	    

	    private String mOperator = "";

	    private String tCurrentDate = "";

	    private VData mInputData = new VData();

	    private String mOperate = "";

	    private PubFun mPubFun = new PubFun();

	    private ListTable mListTable = new ListTable();

	    private TransferData mTransferData = new TransferData();

	    private XmlExport mXmlExport = null;

	    private String mManageComNam = "";

	    private String mFileNameB = "";

	    private String mMakeDate = "";

	    private String mContType = "";

	    private String mContNo = "";

	    private String mRgtNo = "";
	    
	    private String mRiskCode = "";

	    private CSVFileWrite mCSVFileWrite = null;
	    private String mFilePath = "";
	    private String mFileName = "";
	    /**
	     * 传输数据的公共方法
	     */
	    public boolean submitData(VData cInputData, String cOperate) {

	        mOperate = cOperate;
	        mInputData = (VData) cInputData;
	        if (mOperate.equals("")) {
	            this.bulidError("submitData", "数据不完整");
	            return false;
	        }

	        if (!mOperate.equals("PRINT")) {
	            this.bulidError("submitData", "数据不完整");
	            return false;
	        }

	        // 得到外部传入的数据，将数据备份到本类中
	        if (!getInputData(mInputData)) {
	            return false;
	        }
	        System.out.println("PrePaidXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
	                           + this.mManageCom);
	        
	    
	        // 进行数据查询
	        if (!queryDataToCVS()){
	            return false;
	        } else {

	        }

	        System.out.println("dayinchenggong1232121212121");

	        return true;
	    }

	    private void bulidError(String cFunction, String cErrorMsg) {

	        CError tCError = new CError();

	        tCError.moduleName = "LLCasePolicy";
	        tCError.functionName = cFunction;
	        tCError.errorMessage = cErrorMsg;

	        this.mErrors.addOneError(tCError);

	    }

	    /**
	     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	     *
	     * @return boolean
	     */
	    private boolean getInputData(VData cInputData) {
	        // tCurrentDate = mPubFun.getCurrentDate();
	        try {
	            mGlobalInput.setSchema((GlobalInput) cInputData
	                                   .getObjectByObjectName("GlobalInput", 0));
	            mTransferData = (TransferData) cInputData.getObjectByObjectName(
	                    "TransferData", 0);
	            // 页面传入的数据 三个
	            mOperator = (String) mTransferData.getValueByName("tOperator");
	            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
	          
	            this.mManageCom = (String) mTransferData
	                              .getValueByName("MngCom");

	            System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
	                               + this.mManageCom);
	            this.mStartDate = (String) mTransferData
	                              .getValueByName("tStartDate");//结案起始日期
	            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案截止日期
	            
	            this.mContNo = (String) mTransferData.getValueByName("tContNo");//保单号
	            
	            if (mManageCom == null || mManageCom.equals("")) {
	            	 this.mErrors.addOneError("管理机构获取失败");
	            	 return false;
	            }
	            if ("8600".equals(mManageCom)) {
	            	mManageCom="86";
	            }
	            mManageComNam=getManagecomName(mManageCom);
	            

	            System.out
	                    .println(
	                            "XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
	                            + mManageCom);
	        } catch (Exception ex) {
	            this.mErrors.addOneError("");
	            return false;
	        }
	        return true;
	    }
	 
	    private boolean queryDataToCVS()
	    {
	    	
	      int datetype = -1;
	      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	    	tLDSysVarDB.setSysVar("LPCSVREPORT");

	    	if (!tLDSysVarDB.getInfo()) {
	    		buildError("queryData", "查询文件路径失败");
	    		return false;
	    	}
	    	
	    	mFilePath = tLDSysVarDB.getSysVarValue(); 
	        //   	本地测试
//	    	mFilePath="F:\\";
	    	if (mFilePath == null || "".equals(mFilePath)) {
	    		buildError("queryData", "查询文件路径失败");
	    		return false;
	    	}
	    	System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:"+mFilePath);
	    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
	    	String tDate = PubFun.getCurrentDate2();

	    	mFileName = "YFPK" + mGlobalInput.Operator + tDate + tTime;
	    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
	    
	    				
					

	    	String[][] tTitle = new String[4][];
	    	tTitle[0] = new String[] { "","","","","","","预付赔款统计表" };
	    	tTitle[1] = new String[] { "机构："+mManageComNam,"","","","","","","","","统计时间："+mStartDate+"至"+mEndDate };
	    	tTitle[2] = new String[] { "制表人："+mOperator,"","","","","","","","","制表时间："+mPubFun.getCurrentDate() };
	    	tTitle[3] = new String[] { "机构编码","机构名称","投保人","客户号","保单号","险种代码","险种名称","承保时间","生效时间","失效时间","承保保费","统计期内实收保费",
	    			"累计实收保费","统计期内预付赔款金额","累计预付赔款金额","统计期内结案赔款金额","累计结案赔款金额","统计期内给付确认赔款金额","累计给付确认赔款金额","统计期内回销预付赔款金额",
	    			"累计回销预付赔款金额","统计期内回收预付赔款金额","累计回收预付赔款金额","统计期内给付确认直接赔款金额","累计给付确认直接赔款金额","预付赔款余额","统计期已结案未回销预付赔款金额","累计已结案未回销预付赔款金额"};
	    	String[] tContentType = {"String","String","String","String","String","String","String","String","String","String","Number","Number",
	    			"Number","Number","Number","Number","Number","Number","Number","Number",
	    			"Number","Number","Number","Number","Number","Number","Number","Number"};
	    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
	    		return false;
	    	}
	      if (!getDataListToCVS(datetype))
	      {
	        return false;
	      }

	      mCSVFileWrite.closeFile();

	    	return true;
	    }
	    /*
	     * 统计案件结果
	     */
	    private boolean getDataListToCVS(int datetype)
	    {
	    	ExeSQL tExeSQL = new ExeSQL();
	    	
	        String sql = "select comcode,comname,grpname,appntno,grpcontno,riskcode,riskname,signdate,cvalidate,cinvalidate,signpay," 
	        	+"term_sumactupaymoney,sumactupaymoney,prepaid,sum_prepaid,endpay,sum_endpay,claimpay,sum_claimpay,hxpay,sum_hxpay," 
	        	+"hspay,sum_hspay,claimpay-hxpay,sum_claimpay-sum_hxpay,sum_prepaid-sum_hxpay-sum_hspay,whx,sum_whx "
	        	+"from (select  a.managecom comcode,  "
	        	+"(select name from ldcom where comcode=a.managecom) comname, "
	        	+"a.GrpName grpname,a.Appntno appntno,a.grpcontno grpcontno,b.riskcode riskcode,(select riskname from lmriskapp where riskcode=b.riskcode) riskname,  "
	        	+"a.signdate signdate,a.cvalidate cvalidate,a.cinvalidate cinvalidate,  "
	        	+"(select coalesce(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=a.grpcontno and riskcode=b.riskcode and getnoticeno is null) signpay, "
	        	+"(select coalesce(sum(f.sumactupaymoney),0) from lqclaimactug f  where f.grppolno = d.grppolno and f.makedate between '"+mStartDate+"'and '"+mEndDate+"') term_sumactupaymoney,  "
	        	+"(select coalesce(sum(f.sumactupaymoney),0) from lqclaimactug f  where f.grppolno = d.grppolno and f.makedate <='"+mEndDate+"') sumactupaymoney,  "
	        	+"(select coalesce(sum(money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and baladate between '"+mStartDate+"' and '"+mEndDate+"' and moneytype='SQ' and state='1') prepaid,   "
	        	+"(select coalesce(sum(money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and baladate <= '"+mEndDate+"' and moneytype='SQ' and state='1') sum_prepaid,   "
	        	+"(select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=d.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mStartDate+"' and '"+mEndDate+"') endpay, "
	        	+"(select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=d.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate <= '"+mEndDate+"') sum_endpay, "
	        	+"(select coalesce(sum(pay),0) from ljagetclaim where grppolno=d.grppolno and othernotype<>'Y' and makedate between '"+mStartDate+"' and '"+mEndDate+"') claimpay, "
	        	+"(select coalesce(sum(pay),0) from ljagetclaim where grppolno=d.grppolno and othernotype<>'Y' and makedate <= '"+mEndDate+"') sum_claimpay, " 
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='C' and moneytype='PK' and baladate between '"+mStartDate+"' and '"+mEndDate+"'  and state='1') hxpay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='C' and moneytype='PK' and baladate <= '"+mEndDate+"'  and state='1') sum_hxpay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and moneytype='HS' and baladate between '"+mStartDate+"' and '"+mEndDate+"'  and state='1') hspay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and moneytype='HS' and baladate <= '"+mEndDate+"'  and state='1') sum_hspay, "
	        	//统计期已结案未回销预付赔款金额
	        	+" (select nvl(sum(a.realpay),0) from llclaimdetail a,llcase b,llregister c  "                   
	        	+" where a.grppolno=d.grppolno  and a.caseno=b.caseno and a.rgtno=c.rgtno and b.rgtstate='09' "
	        	+" and b.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'"
	        	+" and (  "                                                                                     
	        	+" (substr(varchar(a.rgtno),1,1)<> 'P' and b.prepaidflag='1')  "                                  
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('3','4') and c.prepaidflag='1')  "
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('1','2') and b.prepaidflag='1')  "
	        	+" ))  whx ," 
	        	//累计已结案未回销预付赔款金额
	        	+" (select nvl(sum(a.realpay),0) from llclaimdetail a,llcase b,llregister c  "                   
	        	+" where a.grppolno=d.grppolno  and a.caseno=b.caseno and a.rgtno=c.rgtno and b.rgtstate='09' "
	        	+" and b.endcasedate <= '"+mEndDate+"' "
	        	+" and (  "                                                                                     
	        	+" (substr(varchar(a.rgtno),1,1)<> 'P' and b.prepaidflag='1')  "                                  
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('3','4') and c.prepaidflag='1')  "
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('1','2') and b.prepaidflag='1')  "
	        	+" )) sum_whx " 
	        	+"from lcgrpcont a ,llprepaidgrppol b ,lcgrppol d "
	        	+"where a.grpcontno= b.grpcontno  "
	        	+"and b.riskcode=d.riskcode and a.grpcontno=d.grpcontno and a.managecom like '"+mManageCom+"%' ";
	           if(mContNo!=null&&!"".equals(mContNo))
	        	   sql=sql+"and a.grpcontno='"+mContNo+"' ";
	           sql=sql+"group by a.managecom,a.GrpName,a.Appntno,a.grpcontno,b.riskcode,a.signdate,a.cvalidate,a.cinvalidate,d.grppolno "
	        	+"union "
	        	+"select  a.managecom comcode,  "
	        	+"(select name from ldcom where comcode=a.managecom) comname, "
	        	+"a.GrpName grpname,a.Appntno appntno,a.grpcontno grpcontno,b.riskcode riskcode,(select riskname from lmriskapp where riskcode=b.riskcode) riskname,  "
	        	+"a.signdate signdate,a.cvalidate cvalidate,a.cinvalidate cinvalidate,  "
	        	+"(select coalesce(sum(sumactupaymoney),0) from ljapaygrp where grpcontno=a.grpcontno and riskcode=b.riskcode and getnoticeno is null) signpay,  "
	        	+"(select coalesce(sum(f.sumactupaymoney),0) from lqclaimactug f  where f.grppolno = d.grppolno and f.makedate between '"+mStartDate+"' and '"+mEndDate+"') term_sumactupaymoney,  "
	        	+"(select coalesce(sum(f.sumactupaymoney),0) from lqclaimactug f  where f.grppolno = d.grppolno and f.makedate <='"+mEndDate+"') sumactupaymoney,  "
	        	+"(select coalesce(sum(money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and baladate between '"+mStartDate+"' and '"+mEndDate+"' and moneytype='SQ' and state='1') prepaid,   "
	        	+"(select coalesce(sum(money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and baladate <= '"+mEndDate+"' and moneytype='SQ' and state='1') sum_prepaid,   "
	        	+"(select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=d.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mStartDate+"' and '"+mEndDate+"') endpay, "
	        	+"(select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=d.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate <= '"+mEndDate+"') sum_endpay, "
	        	+"(select coalesce(sum(pay),0) from ljagetclaim where grppolno=d.grppolno and othernotype<>'Y' and makedate between '"+mStartDate+"' and '"+mEndDate+"') claimpay, "
	        	+"(select coalesce(sum(pay),0) from ljagetclaim where grppolno=d.grppolno and othernotype<>'Y' and makedate <= '"+mEndDate+"') sum_claimpay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='C' and moneytype='PK' and baladate between '"+mStartDate+"' and '"+mEndDate+"'  and state='1') hxpay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='C' and moneytype='PK' and baladate <= '"+mEndDate+"'  and state='1') sum_hxpay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and moneytype='HS' and baladate between '"+mStartDate+"' and '"+mEndDate+"'  and state='1') hspay, "
	        	+"(select coalesce(sum(-money),0) from llprepaidtrace where grppolno=d.grppolno and othernotype='Y' and moneytype='HS' and baladate <= '"+mEndDate+"'  and state='1') sum_hspay, "
 	        	//统计期已结案未回销预付赔款金额
	        	+" (select nvl(sum(a.realpay),0) from llclaimdetail a,llcase b,llregister c  "                   
	        	+" where a.grppolno=d.grppolno  and a.caseno=b.caseno and a.rgtno=c.rgtno and b.rgtstate='09' "
	        	+" and b.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'"
	        	+" and (  "                                                                                     
	        	+" (substr(varchar(a.rgtno),1,1)<> 'P' and b.prepaidflag='1')  "                                  
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('3','4') and c.prepaidflag='1')  "
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('1','2') and b.prepaidflag='1')  "
	        	+" ))  whx ," 
	        	//累计已结案未回销预付赔款金额
	        	+" (select nvl(sum(a.realpay),0) from llclaimdetail a,llcase b,llregister c  "                   
	        	+" where a.grppolno=d.grppolno  and a.caseno=b.caseno and a.rgtno=c.rgtno and b.rgtstate='09' "
	        	+" and b.endcasedate <= '"+mEndDate+"' "
	        	+" and (  "                                                                                     
	        	+" (substr(varchar(a.rgtno),1,1)<> 'P' and b.prepaidflag='1')  "                                  
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('3','4') and c.prepaidflag='1')  "
	        	+" or(substr(varchar(a.rgtno),1,1)='P' and c.TogetherFlag in ('1','2') and b.prepaidflag='1')  "
	        	+" )) sum_whx "
	        	+"from lbgrpcont a ,llprepaidgrppol b ,lbgrppol d "
	        	+"where a.grpcontno= b.grpcontno  "
	        	+"and b.riskcode=d.riskcode and a.grpcontno=d.grpcontno  and a.managecom like '"+mManageCom+"%' ";
	        	if(mContNo!=null&&!"".equals(mContNo))
		           sql=sql+"and a.grpcontno='"+mContNo+"' ";
	            sql=sql+"group by a.managecom,a.GrpName,a.Appntno,a.grpcontno,b.riskcode,a.signdate,a.cvalidate,a.cinvalidate,d.grppolno "
	        	+")as aa "
	        	+"with ur ";
	        	

	        System.out.println("SQL:" + sql);
	        RSWrapper rsWrapper = new RSWrapper();
	        if (!rsWrapper.prepareData(null, sql)) {
	            System.out.println("预付赔款数据准备失败! ");
	            return false;
	        }

	        try {
	            SSRS tSSRS = new SSRS();
	            do {
	                tSSRS = rsWrapper.getSSRS();
	                if (tSSRS != null || tSSRS.MaxRow > 0) {

	                    String tContent[][] = new String[tSSRS.getMaxRow()][];
	                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
	                        String ListInfo[] = new String[28];
	                        for (int m = 0; m < ListInfo.length; m++) {
	                            ListInfo[m] = "";
	                        }
	                        
	                        //机构编码
	                        ListInfo[0] = tSSRS.GetText(i, 1);
	                        //机构名称,
	                        ListInfo[1] = tSSRS.GetText(i, 2);
	                        //投保人
	                        ListInfo[2] = tSSRS.GetText(i, 3);
	                        //客户号
	                        ListInfo[3] = tSSRS.GetText(i, 4);
	                        //保单号码
	                        ListInfo[4] = tSSRS.GetText(i, 5);
	                        //险种代码
	                        ListInfo[5] = tSSRS.GetText(i, 6);
	                        //险种名称
	                        ListInfo[6] = tSSRS.GetText(i, 7);
	                        //承保时间
	                        ListInfo[7] = tSSRS.GetText(i, 8);
	                        //生效时间,
	                        ListInfo[8] = tSSRS.GetText(i, 9);
	                        //失效时间,
	                        ListInfo[9] = tSSRS.GetText(i, 10);
	                        //承保保费,
	                        ListInfo[10] = tSSRS.GetText(i, 11);
	                        //统计期内实收保费
	                        ListInfo[11] = tSSRS.GetText(i, 12);
	                        //累计实收保费
	                        ListInfo[12] = tSSRS.GetText(i, 13);
	                        //统计期内预付赔款金额,
	                        ListInfo[13] = tSSRS.GetText(i, 14);
	                        //累计预付赔款金额
	                        ListInfo[14] = tSSRS.GetText(i, 15);
	                        //统计期内结案赔款金额,
	                        ListInfo[15] = tSSRS.GetText(i, 16);
	                        //累计结案赔款金额
	                        ListInfo[16] = tSSRS.GetText(i, 17);
	                        //统计期内给付确认赔款金额
	                        ListInfo[17] = tSSRS.GetText(i, 18);
	                        //累计给付确认赔款金额,
	                        ListInfo[18] = tSSRS.GetText(i, 19);
	                        //统计期内回销预付赔款余额
	                        ListInfo[19] = tSSRS.GetText(i, 20);
	                        //累计回销预付赔款金额
	                        ListInfo[20] = tSSRS.GetText(i, 21);
	                        //统计期内回收预付赔款金额,
	                        ListInfo[21] = tSSRS.GetText(i, 22);
	                        //累计回收预付赔款金额
	                        ListInfo[22] = tSSRS.GetText(i, 23);
	                        //统计期内给付确认直接赔款金额
	                        ListInfo[23] = tSSRS.GetText(i, 24);
	                        //累计给付确认直接赔款金额
	                        ListInfo[24] = tSSRS.GetText(i, 25);
	                        //预付赔款余额
	                        ListInfo[25] = tSSRS.GetText(i, 26);
	                        //统计期已结案未回销预付赔款金额
	                        ListInfo[26] = tSSRS.GetText(i, 27);
	                        //累计已结案未回销预付赔款金额
	                        ListInfo[27] = tSSRS.GetText(i, 28);

	         //  mListTable.add(ListInfo);
	           tContent[i - 1] = ListInfo;
	       }
	       mCSVFileWrite.addContent(tContent);
	       if (!mCSVFileWrite.writeFile()) {
				mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
				return false;
			}
	   }else{
		String tContent[][] = new String[1][];
	   String ListInfo[]={"","","","","","","","","","","","","","","","","","","","","","","","","","","",""};	
	   ListInfo[0]="0";
	   ListInfo[1]="0";
	   ListInfo[2]="0";
	   ListInfo[3]="0";
	   ListInfo[4]="0";
	   ListInfo[5]="0";
	   ListInfo[6]="0";
	   ListInfo[7]="0";
	   ListInfo[8]="0";
	   ListInfo[9]="0";
	   ListInfo[10]="0";
	   ListInfo[11]="0";
	   ListInfo[12]="0";
	   ListInfo[13]="0";
	   ListInfo[14]="0";
	   ListInfo[15]="0";
	   ListInfo[16]="0";
	   ListInfo[17]="0";
	   ListInfo[18]="0";
	   ListInfo[19]="0";
	   ListInfo[20]="0";
	   ListInfo[21]="0";
	   ListInfo[22]="0";
	   ListInfo[23]="0";
	   ListInfo[24]="0";
	   ListInfo[25]="0";
	   ListInfo[26]="0";
	   ListInfo[27]="0";
	   tContent[0]=ListInfo;
	   mCSVFileWrite.addContent(tContent);
	   if (!mCSVFileWrite.writeFile()) {
			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
			return false;
		}

	   }
	} while (tSSRS != null && tSSRS.MaxRow > 0);
	} catch (Exception ex) {

	} finally {
	rsWrapper.close();
	}
	return true;
	      }
	 

	    private String getYear(String pmDate) {
	        String mYear = "";
	        String tSQL = "";
	        SSRS tSSRS = new SSRS();
	        ExeSQL tExeSQL = new ExeSQL();
	        tSSRS = tExeSQL.execSQL(tSQL);
	        mYear = tSSRS.GetText(1, 1);
	        return mYear;
	    }

	  
	    /**
	     * 获取险种类型查询条件
	     * @param aContType String
	     * @return String
	     */
	    private String getContType(String aContType) {
	        if (!"".equals(aContType)) {
	            String tContTypeSQL =
	                    " and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskprop='" +
	                    aContType + "' ) ";
	            return tContTypeSQL;
	        } else {
	            return "";
	        }
	    }

	    /**
	     * 获取批次号查询条件
	     * @param aRgtNo String
	     * @return String
	     */
	    private String getRgtNo(String aRgtNo) {
	        if (!"".equals(aRgtNo)) {
	            String tRgtSQL = " and a.rgtno='" + aRgtNo + "'";
	            return tRgtSQL;
	        } else {
	            return "";
	        }
	    }

	    /**
	     * 获取保单号查询条件
	     * @param aContNo String
	     * @return String
	     */
	    private String getContNo(String aContNo) {
	        if (!"".equals(aContNo)) {
	            String tContSQL = " and (b.grpcontno='" + aContNo +
	                              "' or b.contno='" + aContNo + "')";
	            return tContSQL;
	        } else {
	            return "";
	        }

	    }
	    
	    /**
	     * 获取险种查询条件
	     * @param aRgtNo String
	     * @return String
	     */
	    private String getRiskCode(String aRiskCode) {
	        if (!"".equals(aRiskCode)) {
	            String tRiskCodeSQL = " and b.riskcode='" + aRiskCode + "'";
	            return tRiskCodeSQL;
	        } else {
	            return "";
	        }
	    }

	    /**
	     * @return VData
	     */
	    public VData getResult() {
	        return mResult;
	    }
		public String getMFilePath() {
			return mFilePath;
		}

		public void setMFilePath(String filePath) {
			mFilePath = filePath;
		}

		public String getMFileName() {
			return mFileName;
		}

		public void setMFileName(String fileName) {
			mFileName = fileName;
		}
		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "LLCasePolicyBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			System.out.println(szFunc + "--" + szErrMsg);
			this.mErrors.addOneError(cError);
		}
	    /**
	     * 得到xml的输入流
	     * @return InputStream
	     */
	    public InputStream getInputStream() {
	        return mXmlExport.getInputStream();
	    }
		public String getManagecomName(String com) {
			String sq="select name from ldcom where comcode='"+com+"' with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
			return tGrpTypeSSRS.GetText(1, 1);
		}


}
