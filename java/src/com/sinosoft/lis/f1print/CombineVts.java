/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import com.sinosoft.utility.F1PrintParser;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * 格式化数据流
 * <p>Description: </p>
 * 格式化数据流并返回
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author 朱向峰
 * @version 1.0
 */
public class CombineVts
{
    private F1PrintParser f1p = null;
    private String strTemplatePath = null;

    /**
     * 根据模板格式化数据流
     * @param ins InputStream
     * @param strPath String
     */
    public CombineVts(InputStream ins, String strPath)
    {
        try
        {
            strTemplatePath = strPath;
            if (ins == null)
            {
                XmlExport xmlExport = new XmlExport();
                xmlExport.createDocument("nofound.vts", "printer");
                f1p = new F1PrintParser(xmlExport.getInputStream(), strTemplatePath);
            }
            else
            {
                f1p = new F1PrintParser(ins, strTemplatePath);
            }
        }
        catch (Exception e)
        {
            System.out.println("F1PrintKernelJ1.jsp : fail to parse print data");
        }
    }

    /**
     * 输出格式化后的数据流
     * @param dataStream ByteArrayOutputStream
     */
    public void output(ByteArrayOutputStream dataStream)
    {
        //同步F1PrintParser，独占解析，防止由于并发引起问题
        //lanjun 2006/09/12
    	//Deleted By Qisl At 2009/05/06 修改formula one生成的临时文件名规则，使得并发粒度在毫秒级别以上的都不需要同步。避免了并发
        synchronized(F1PrintParser.class)
        {
            if (!f1p.output(dataStream))
            {
                System.out.println("F1PrintKernelJ1.jsp : fail to parse print data");
            }
        }

    }

    public static void main(String[] args)
    {
        Thread t1 = new Thread(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                try {
                    String strTemplatePath = "d:/print/";
                    String strVFPathName = "1.vts";
                    FileInputStream fis = new FileInputStream("d:/print/1.xml");
                    CombineVts tcombineVts = new CombineVts(fis,
                            strTemplatePath);
                    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                    tcombineVts.output(dataStream);

                    //把dataStream存储到磁盘文件
                    System.out.println("存储文件到" + strTemplatePath);
                    AccessVtsFile.saveToFile(dataStream, strVFPathName);
                    System.out.println("==> Write VTS file to disk ");
                    //tcombineVts.output(fis);
                } catch (Exception ex) {

                }
            }

        });
        
        Thread t2 = new Thread(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                try {
                    String strTemplatePath = "d:/print/";
                    String strVFPathName = "1.vts";
                    FileInputStream fis = new FileInputStream("d:/print/1.xml");
                    CombineVts tcombineVts = new CombineVts(fis,
                            strTemplatePath);
                    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                    tcombineVts.output(dataStream);

                    //把dataStream存储到磁盘文件
                    System.out.println("存储文件到" + strTemplatePath);
                    AccessVtsFile.saveToFile(dataStream, strVFPathName);
                    System.out.println("==> Write VTS file to disk ");
                    //tcombineVts.output(fis);
                } catch (Exception ex) {

                }
            }

        });
        
        t1.start();
        t2.start();
    }
}
