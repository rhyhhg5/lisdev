package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :续期保费银行转账失败清单
 * @version 1.0
 * @date 2004-4-27
 */

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAppntIndSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewErrXQPremBankBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();
    public PremBankPubFun mPremBankPubFun = new PremBankPubFun();

    private LYBankLogSet mLYBankLogSet = new LYBankLogSet();
    private LDBankSchema mLDBankSchema = new LDBankSchema();
    private LDCode1Schema mLDCode1Schema = new LDCode1Schema();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();

    //初始化全局变量，从前台承接数据
    private String strStartDate = ""; //开始日期
    private String strEndDate = ""; //结束日期
    private String strAgentState = ""; //业务员的状态(1为在职单，0为孤儿单)
    private String strPremType = ""; //首续期的标志
    private String strFlag = ""; //S or F(S为银行代收，F为银行代付)
    private String strComCode = ""; //系统登陆的机构(查询银行日志表)
    private String strStation = "";

    private String strBillNo = ""; //批次号码
    private String mBankName = ""; //银行名称
    private String mErrorReason = ""; //失败原因
    private String mChkSuccFlag = ""; //银行校验成功标志；
    private String mChkFailFlag = ""; //银行校验失败标志；
    private String mAgentGroup = "";
    private String mAgentState = "";
    private double mMoney = 0.00;
    private double mAppendMoney = 0.00;

    private int mCount = 0;
    private VData mInputData;
    public NewErrXQPremBankBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        System.out.println("NewErrXQPremBankBL");
        if (!cOperate.equals("PRINT"))
        {
            mPremBankPubFun.buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!CaseFunPub.checkDate(strStartDate, strEndDate))
        {
            mPremBankPubFun.buildError("submitData", "开始日期比结束日期晚,请从新录入");
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getPrintData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NewPrintXQBankErr.vts", "printer");

        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");
        String mManageCom = "";
        if (strComCode.length() < 4)
        {
            mManageCom = strComCode.substring(0, 2);
        }
        if (strComCode.length() >= 4)
        {
            mManageCom = strComCode.substring(0, 4);
        }
        System.out.println("lybanklog的管理机构匹配的值是" + mManageCom);
        //在LYReturnFromBank和LYReturnFromBankB中查询出所有的符合日期条件的记录
        String main_sql = "";
        main_sql = "select paycode,BankSuccFlag,BankCode ,BankDealDate from lyreturnfrombank where serialno in "
                   + " ( select serialno from lybanklog where startdate >='" +
                   strStartDate + "' and startdate <='" + strEndDate +
                "' and ReturnDate is not null and LogType = 'S' and ComCode like '" +
                   mManageCom + "%') "
                   + " union all select paycode,BankSuccFlag ,BankCode ,BankDealDate from lyreturnfrombankb where serialno in "
                   + " ( select serialno from lybanklog where startdate >='" +
                   strStartDate + "' and startdate <='" + strEndDate +
                "' and ReturnDate is not null and LogType = 'S' and ComCode like '" +
                   mManageCom + "%') ";
        System.out.println("main_sql的查询语句是" + main_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = new SSRS();
        main_ssrs = main_exesql.execSQL(main_sql);
//    System.out.println("main_ssrs查询的结果是"+main_ssrs.getMaxRow());
        if (main_ssrs.getMaxRow() > 0)
        {
            for (int main_count = 1; main_count <= main_ssrs.getMaxRow();
                                  main_count++)
            {
                String tReturnFlag = mPremBankPubFun.getBankSuccFlag(main_ssrs.
                        GetText(main_count, 3));
                //System.out.println("返回的标志是"+tReturnFlag);
                boolean tSuccFlag = mPremBankPubFun.verifyBankSuccFlag(
                        tReturnFlag, main_ssrs.GetText(main_count, 2));
                //System.out.println("返回的标志是"+tSuccFlag);
                if (!tSuccFlag)
                {
                    //若是正确的，则继续下列的操作:查询出是续期交费的主险
                    String sec_sql = "";
                    sec_sql = "select distinct(getnoticeno) from ljspayperson where paycount >'1' and  getnoticeno = '" +
                              main_ssrs.GetText(main_count, 1) +
                              "' and ManageCom like '" + strComCode + "%' "
                              + " union all select distinct(getnoticeno) from ljapayperson where paycount > '1' and getnoticeno = '" +
                              main_ssrs.GetText(main_count, 1) +
                              "' and ManageCom like '" + strComCode + "%' ";
//          System.out.println("sec_sql的查询语句是"+sec_sql);
                    //并关联该主险的所有交费信息，包括附加险的续保信息
                    ExeSQL sec_exesql = new ExeSQL();
                    SSRS sec_ssrs = sec_exesql.execSQL(sec_sql);
                    System.out.println("sec_sql的查询结果是" + sec_ssrs.getMaxRow());
                    if (sec_ssrs.getMaxRow() > 0)
                    {
                        for (int sec_count = 1; sec_count <= sec_ssrs.getMaxRow();
                                             sec_count++)
                        {
                            mAppendMoney = 0.0;
//              System.out.println("getnoticeno="+sec_ssrs.GetText(sec_count,1));
                            //查询出主险的信息
                            String thr_sql = "";
                            thr_sql = " select AgentCode,AgentGroup,AppntNo,PolNo,PayCount,BankCode,BankAccNo,LastPayToDate,sum(SumActuPayMoney)"
                                      +
                                    " from ljspayperson where getnoticeno = '" +
                                      sec_ssrs.GetText(sec_count, 1)
                                      + "' group by AgentCode,AgentGroup,AppntNo,PolNo,PayCount,BankCode,BankAccNo,LastPayToDate order by polno ";
//              System.out.println("thr_sql的查询语句是"+thr_sql);
                            ExeSQL thr_exesql = new ExeSQL();
                            SSRS thr_ssrs = thr_exesql.execSQL(thr_sql);
//              System.out.println("thr_ssrs的查询结果是"+thr_ssrs.getMaxRow());
                            if (thr_ssrs.getMaxRow() > 0)
                            {
                                //添加孤儿单和在职单的查询
                                String fou_sql = "";
                                if (strAgentState.equals("1"))
                                {
                                    fou_sql =
                                            " select Name,Mobile from LAAgent where AgentCode = '" +
                                            thr_ssrs.GetText(1, 1) +
                                            "' and AgentState in ('01','02') ";
                                }
                                if (strAgentState.equals("0"))
                                {
                                    fou_sql =
                                            " select Name,Mobile from LAAgent where AgentCode = '" +
                                            thr_ssrs.GetText(1, 1) +
                                            "' and AgentState not in ('01','02') ";
                                }
//                System.out.println("孤儿单或在职单的查询语句是"+fou_sql);
                                ExeSQL fou_exesql = new ExeSQL();
                                SSRS fou_ssrs = fou_exesql.execSQL(fou_sql);
//                System.out.println("查询的结果是"+fou_ssrs.getMaxRow());
                                if (fou_ssrs.getMaxRow() <= 0)
                                {
                                    continue;
                                }
                                System.out.println("开始执行");
                                for (int thr_count = 2;
                                        thr_count <= thr_ssrs.getMaxRow();
                                        thr_count++)
                                {
//                  System.out.println("附加险1");
                                    mAppendMoney = mAppendMoney +
                                            Double.
                                            parseDouble(thr_ssrs.GetText(thr_count,
                                            9));
//                  System.out.println("附加险的金额是"+mAppendMoney);
                                }
                                String[] cols = new String[17];
                                LCAppntIndSchema tLCAppntIndSchema = new
                                        LCAppntIndSchema();
                                tLCAppntIndSchema.setSchema(mPremBankPubFun.
                                        getAppntInfo((thr_ssrs.GetText(1, 3))));
                                LCPolDB tLCPolDB = new LCPolDB();
                                tLCPolDB.setPolNo(thr_ssrs.GetText(1, 4));
                                tLCPolDB.getInfo();

                                cols[0] = mPremBankPubFun.getAgentGroup(
                                        thr_ssrs.GetText(1, 2)); //业务员组别
                                cols[1] = fou_ssrs.GetText(1, 1); //业务员姓名
                                cols[2] = fou_ssrs.GetText(1, 2); //业务员电话
                                cols[3] = tLCAppntIndSchema.getName(); //投保人姓名
                                cols[4] = tLCAppntIndSchema.getPostalAddress(); //投保人地址
                                cols[5] = tLCAppntIndSchema.getMobile(); //投保人邮编
                                cols[6] = thr_ssrs.GetText(1, 4); //主险保单号码
                                cols[7] = thr_ssrs.GetText(1, 5); //应缴期数
                                //cols[7] = tLCPolDB.getSchema().getInsuredName();//被保人
                                cols[8] = mPremBankPubFun.getBankInfo(thr_ssrs.
                                        GetText(1, 6)).getBankName(); //开户行
                                cols[9] = thr_ssrs.GetText(1, 7); //账号
                                cols[10] = thr_ssrs.GetText(1, 8); //应缴日期
                                cols[11] = String.valueOf(thr_ssrs.GetText(1, 9)); //主险实交金额
                                cols[12] = String.valueOf(mAppendMoney); //附件险实金额合计
                                cols[13] = String.valueOf(Double.parseDouble(
                                        thr_ssrs.GetText(1, 9)) + mAppendMoney);
                                cols[14] = main_ssrs.GetText(main_count, 4); //交费日期
                                //添加划账次数及不成功的原因
                                LJSPayDB tLJSPayDB = new LJSPayDB();
                                tLJSPayDB.setGetNoticeNo(sec_ssrs.GetText(
                                        sec_count, 1));
                                tLJSPayDB.getInfo();
                                cols[15] = String.valueOf(tLJSPayDB.getSchema().
                                        getSendBankCount());
                                //查询出错误的原因
                                LDCode1DB tLDCode1DB = new LDCode1DB();
                                tLDCode1DB.setCodeType("bankerror");
                                tLDCode1DB.setCode(thr_ssrs.GetText(1, 6)); //银行代码
                                tLDCode1DB.setCode1(main_ssrs.GetText(
                                        main_count, 2)); //返回标志
                                System.out.println("查询银行的错误信息");
                                System.out.println("codetype  " +
                                        tLDCode1DB.getCodeType());
                                System.out.println("银行代码  " +
                                        tLDCode1DB.getCode());
                                System.out.println("返回值是" + tLDCode1DB.getCode1());
                                tLDCode1DB.getInfo();
                                if (!((tLDCode1DB.getSchema().getCodeName() == null) ||
                                      (tLDCode1DB.getSchema().getCodeName().
                                       equals(""))))
                                {

                                    cols[16] = tLDCode1DB.getSchema().
                                               getCodeName();
                                }
                                else
                                {
                                    cols[16] = "原因不详，请和银行联系";
                                }
                                alistTable.add(cols);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            mPremBankPubFun.buildError("submitData", "在银行信息表中没有该时间端的信息！！");
            return false;
        }
        System.out.println("开始执行最外部分的循环");
        String[] b_col = new String[17];
        xmlexport.addDisplayControl("displayinfo");
        xmlexport.addListTable(alistTable, b_col);
        texttag.add("AgentState", mAgentState);
        texttag.add("StartDate", strStartDate);
        texttag.add("EndDate", strEndDate);
        texttag.add("ComCode", strComCode);
        texttag.add("SumMoney", mMoney);
        texttag.add("SumCount", mCount);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","NewXQPremBankErrorBL");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        strStartDate = (String) mInputData.get(0);
        strEndDate = (String) mInputData.get(1);
        strAgentState = (String) mInputData.get(2);
        strPremType = (String) mInputData.get(3); //首期还是续期
        strFlag = (String) mInputData.get(4); //F or S
        strComCode = (String) mInputData.get(5);
        //  strStation = (String)mInputData.get(6);
        System.out.println("strStartDate" + strStartDate);
        System.out.println("strEndDate" + strEndDate);
        System.out.println("strAgentState" + strAgentState);
        System.out.println("strPremType" + strPremType);
        System.out.println("strFlag" + strFlag);

        System.out.println("strComCode" + strComCode);
        System.out.println("strStation" + strStation);
        if (strAgentState.equals("1"))
        {
            mAgentState = "在职单";
        }
        else
        {
            mAgentState = "孤儿单";
        }
        strStartDate = strStartDate.trim();
        strEndDate = strEndDate.trim();
        strAgentState = strAgentState.trim();
        strPremType = strPremType.trim();
        strFlag = strFlag.trim();
        return true;
    }

    public static void main(String[] args)
    {
        String strStartDate = "2004-8-9"; //开始日期
        String strEndDate = "2004-8-13"; //结束日期
        String strAgentState = "1"; //业务员的状态(1为在职单，0为孤儿单)
        String strPremType = "X"; //首续期的标志
        String strFlag = "S"; //S or F(S为银行代收，F为银行代付)
        String strComCode = "8611"; //系统登陆的机构(查询银行日志表)
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";

        VData tVData = new VData();
        tVData.addElement(strStartDate);
        tVData.addElement(strEndDate);
        tVData.addElement(strAgentState);
        tVData.addElement(strPremType);
        tVData.addElement(strFlag);
        tVData.addElement(strComCode);
        tVData.addElement(tG);
        NewErrXQPremBankBL aNewErrXQPremBankBL = new NewErrXQPremBankBL();
        aNewErrXQPremBankBL.submitData(tVData, "PRINT");
    }
}
