package com.sinosoft.lis.f1print;

/**
 * Copyright (c) 2008 sinosoft  Co. Ltd.
 * All right reserved.
 */

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LRPolSet;
import com.sinosoft.lis.f1print.PDFPrintBatchManagerBL;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.certify.CertifyFunc;

/*
 * <p>ClassName: FeeBatchInvoiceF1PBL </p>
 * <p>Description: FeeBatchInvoiceF1PBL类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @Database: Sun yu
 * @CreateDate：2008-12-11
 */
public class FeeBatchInvoiceF1PBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput globalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String strOperate = "";

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LZCardSchema mLZCardSchema = new LZCardSchema();
    private String mCertifyFlag = "";

    //业务处理相关变量
    /** 全局数据 */

    public FeeBatchInvoiceF1PBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 提交数据处理方法
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Come in BL.Submit..............");
        this.strOperate = cOperate;
        mResult.clear();
        if (strOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput globalInput = new GlobalInput();
        globalInput.ManageCom = "86";
        globalInput.Operator = "zhangb";

        // prepare main plan
        // 准备传输数据 VData
        VData vData = new VData();
        TransferData tTransferData = new TransferData();
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
        for (int i = 1; i <= this.mLJAPaySet.size(); i++) {
            CertifyFunc tCertifyFunc = new CertifyFunc();
            String tStartNo = "";
            if ("1".equals(mCertifyFlag)) {
                tStartNo = tCertifyFunc.getCertifyCode(mLZCardSchema,
                        i - 1);
            } else {
                tStartNo = "00000000";
            }
            System.out.println(tStartNo);
            String tStandbyFlag1 = (String) mTransferData.getValueByName(
                    "CertifyCode")
                                   + "," + tStartNo;
            LJAPaySchema tLJAPaySchema = mLJAPaySet.get(i);
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            String tLimit = PubFun.getNoLimit(this.globalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLJAPaySchema.getPayNo());
            tLOPRTManagerSchema.setOtherNoType("21");
            tLOPRTManagerSchema.setCode("invoice");
            tLOPRTManagerSchema.setManageCom(this.globalInput.ManageCom);
            tLOPRTManagerSchema.setAgentCode(tLJAPaySchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(this.globalInput.ManageCom);
            tLOPRTManagerSchema.setReqOperator(this.globalInput.Operator);
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setStandbyFlag1(tStandbyFlag1);
            tLOPRTManagerSchema.setStandbyFlag2((String) mTransferData.
                                                getValueByName("HPerson"));
            tLOPRTManagerSchema.setStandbyFlag3((String) mTransferData.
                                                getValueByName("CPerson"));
            tLOPRTManagerSchema.setStandbyFlag4((String) mTransferData.
                                                getValueByName("Remark"));
            tLOPRTManagerSchema.setOldPrtSeq((String) mTransferData.getValueByName("FPDM")); //临时存储发票代码
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            String tInvoiceState=(String) mTransferData.getValueByName("InvoiceState");
            tLOPRTManagerSchema.setStandbyFlag5(tInvoiceState);
            if("5".equals(tInvoiceState)){
            	String tPrintAmount = (String) mTransferData.
                        				getValueByName("PrintAmount");
            	String tPayer = (String) mTransferData.
                        			getValueByName("Payer");
            	tLOPRTManagerSchema.setPrintAmount(tPrintAmount);
                tLOPRTManagerSchema.setPayer(tPayer);
            }
            mLOPRTManagerSet.add(tLOPRTManagerSchema);
        }
        VData tVData = new VData();
        tVData.add(this.globalInput);
        tVData.add(mLOPRTManagerSet);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new
                PDFPrintBatchManagerBL();
        if (!tPDFPrintBatchManagerBL.submitData(tVData, strOperate)) {
            mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        globalInput.setSchema((GlobalInput) cInputData.
                              getObjectByObjectName(
                                      "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLJAPaySet = (LJAPaySet) cInputData.getObjectByObjectName(
                "LJAPaySet", 0);
        mLZCardSchema = (LZCardSchema) cInputData.getObjectByObjectName(
                "LZCardSchema", 0);
        mCertifyFlag = (String) mTransferData.getValueByName("CertifyFlag");
        return true;
    }


    /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReNewContDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }

    /**
     * 公共方法
     * 用于数据校验
     *
     * @return
     */
    private boolean checkData() {
        if ("1".equals(mCertifyFlag)) {
            String tCertifyCode = mLZCardSchema.getCertifyCode();
            String tStartNo = mLZCardSchema.getStartNo();
            String tEndNo = mLZCardSchema.getEndNo();
            int tNum = 0;
            try {
                tNum = Integer.parseInt(tEndNo) - Integer.parseInt(tStartNo) +
                       1;
            } catch (Exception ex) {
                buildError("Certify", "输入的发票号格式错误！");
                return false;
            }
            if (mLJAPaySet.size() != tNum) {
                buildError("Certify", "输入的发票号数量与待打印数据不符！");
                return false;
            }

            String SQL = "select * from LZCard where CertifyCode = '" +
                         tCertifyCode + "' "
                         + "and StartNo <= '" + tStartNo + "' and EndNo >= '"
                         + tEndNo + "' and stateflag in ('0','7') "
                         + " and ReceiveCom='B" + this.globalInput.Operator +
                         "'";
            LZCardDB tLZCardDB = new LZCardDB();
            LZCardSet mLZCardSet = tLZCardDB.executeQuery(SQL);
            if (mLZCardSet.size() != 1) {
                buildError("Certify", "输入的发票号错误！请输入在同一号段内的已发放发票号！");
                return false;
            }
        }
        return true;
    }
}
