/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.EasyScanQueryBL;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAppntGrpSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCSpecSchema;
import com.sinosoft.lis.tb.ProposalQueryBL;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.COracleBlob;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataBlob;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

/*
 * <p>ClassName: LCPolF1PBL </p>
 * <p>Description: LCPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class LCPolF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSet mLCPolSet = new LCPolSet();
    private String mPrtNo = "";
    private XMLDatasets mXMLDatasets = new XMLDatasets();
    private String mOperate = "";

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vPolNo = new Vector();

    public LCPolF1PBL()
    {
        mXMLDatasets.createDocument();
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            if (!mOperate.equals("PRINT")
                && !mOperate.equals("REPRINT")
                && !mOperate.equals("PRINTEX"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 打印保单的操作
            if (mOperate.equals("PRINT"))
            {
                // 准备所有要打印的数据
                if (!getPrintData())
                {
                    return false;
                }
            }

            // 补打保单的操作
            if (mOperate.equals("REPRINT"))
            {
                if (!getPrintData())
                {
                    return false;
                }
            }

            // 前台保单打印的操作
            if (mOperate.equals("PRINTEX"))
            {
                // 准备所有要打印的数据
                if (!getPrintData())
                {
                    return false;
                }

                // 因为数据格式不同，进行一下转换。
                Document doc = mXMLDatasets.getDocument();
                Element ele = doc.getRootElement().getChild("DATASET");
                doc = new Document(ele);
                countPrint(doc);
                mResult.clear();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                XMLOutputter outputter = new XMLOutputter("  ", true, "UTF-8");
                outputter.output(doc, baos);

                mResult.add(new ByteArrayInputStream(baos.toByteArray()));
            }

            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        if (mOperate.equals("PRINT"))
        { // 打印保单
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName(
                    "LCPolSet", 0));

        }
        else if (mOperate.equals("REPRINT"))
        { // 补打保单
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName(
                    "LCPolSet", 0));

        }
        else if (mOperate.equals("PRINTEX"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName(
                    "LCPolSet", 0));
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolSchema tLCPolSchema = null;

        m_vPolNo.clear(); // Clear contents of m_vPolNo

        for (int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)
        {
            tLCPolSchema = mLCPolSet.get(nIndex + 1);

            // Have been printed, continue ...
            if (m_vPolNo.contains(tLCPolSchema.getPolNo()))
            {
                continue;
            }

            if (!getPolSet(tLCPolSchema, tLCPolSet))
            {
                return false;
            }

            // 校验并记录下本次打过的所有保单号
            for (int n = 0; n < tLCPolSet.size(); n++)
            {
                LCPolSchema tempLCPolSchema = tLCPolSet.get(n + 1);

                String strPolNo = tempLCPolSchema.getPolNo();

                if (!tempLCPolSchema.getAppFlag().equals("1"))
                {
//                    buildError("getPolSet", strPolNo + "号投保单还没有签单");
//                    return false;
                }
                /*Lis5.3 upgrade get
                        if( tempLCPolSchema.getPrintCount() >= 1 ) {
                          buildError("getPolSet", strPolNo + "号保单已经打印过了");
                          return false;
                        }
                 */
                m_vPolNo.add(strPolNo);
            }

            boolean bFlag = false;

//            if (tLCPolSet.get(1).getPrintCount() == -1)
//            {
//                // 如果是重打
//                bFlag = getPolDataSetEx(tLCPolSet);
//            }
//            else
//            {
            // 如果是正常打印
            bFlag = getPolDataSet(tLCPolSet);
//            }
            if (!bFlag)
            {
                return false;
            }

        } // end of "for(int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)"

        LCPolSet tempLCPolSet = new LCPolSet();

        for (int nIndex = 0; nIndex < m_vPolNo.size(); nIndex++)
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo((String) m_vPolNo.get(nIndex));
            tLCPolDB.getInfo();
            if (mOperate.equals("PRINT") || mOperate.equals("REPRINT"))
            {
                /*Lis5.3 upgrade set
                         tLCPolDB.setPrintCount(1);
                 */
                tempLCPolSet.add(tLCPolDB);
            }
        }

        // 准备要保存的数据
        mResult.add(tempLCPolSet);
        mResult.add(mXMLDatasets);
        mResult.add(mGlobalInput);

        LCPolF1PBLS tLCPolF1PBLS = new LCPolF1PBLS();
        tLCPolF1PBLS.submitData(mResult, mOperate);
        if (tLCPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }

        mResult.clear();

        System.out.println("add inputstream to mResult");
        mResult.add(mXMLDatasets.getInputStream());
        return true;
    }

    private void getCashValue(XMLDataset xmlDataset, LCPolSchema aLCPolSchema) throws
            Exception
    {
        XMLDataList xmlDataList = new XMLDataList();

        xmlDataList.setDataObjectID("CashValue");

        xmlDataList.addColHead("Age");
        xmlDataList.addColHead("Cash");
        xmlDataList.buildColHead();

        // 得到现金价值的算法描述
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();

        tLMCalModeDB.setRiskCode(aLCPolSchema.getRiskCode());
        tLMCalModeDB.setType("X");

        LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

        // 解析得到的SQL语句
        String strSQL = "";

        if (tLMCalModeSet.size() == 1)
        {
            strSQL = tLMCalModeSet.get(1).getCalSQL();
        }

        // 这个险种不需要取现金价值的数据
        if (strSQL.equals(""))
        {
            xmlDataset.addDataObject(new XMLDataTag("CashValueFlag", "N"));
        }
        else
        {
            xmlDataset.addDataObject(new XMLDataTag("CashValueFlag", "Y"));

            Calculator calculator = new Calculator();

            // 设置基本的计算参数
            calculator.addBasicFactor("InsuredSex", aLCPolSchema.getInsuredSex());
            calculator.addBasicFactor("InsuredAppAge",
                                      String.valueOf(aLCPolSchema.
                    getInsuredAppAge()));
            calculator.addBasicFactor("PayIntv",
                                      String.valueOf(aLCPolSchema.getPayIntv()));
            calculator.addBasicFactor("PayEndYear",
                                      String.valueOf(aLCPolSchema.getPayEndYear()));
            calculator.addBasicFactor("PayEndYearFlag",
                                      String.valueOf(aLCPolSchema.
                    getPayEndYearFlag()));
            calculator.addBasicFactor("PayYears",
                                      String.valueOf(aLCPolSchema.getPayYears()));
            calculator.addBasicFactor("InsuYear",
                                      String.valueOf(aLCPolSchema.getInsuYear()));
            calculator.addBasicFactor("Prem",
                                      String.valueOf(aLCPolSchema.getPrem()));
            calculator.addBasicFactor("Amnt",
                                      String.valueOf(aLCPolSchema.getAmnt()));
            calculator.addBasicFactor("FloatRate",
                                      String.valueOf(aLCPolSchema.getFloatRate()));
            //add by yt 2004-3-10
            calculator.addBasicFactor("InsuYearFlag",
                                      String.valueOf(aLCPolSchema.
                    getInsuYearFlag()));
            calculator.addBasicFactor("GetYear",
                                      String.valueOf(aLCPolSchema.getGetYear()));
            calculator.addBasicFactor("GetYearFlag",
                                      String.valueOf(aLCPolSchema.
                    getGetYearFlag()));

            calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
            strSQL = calculator.getCalSQL();

            System.out.println(strSQL);

            Connection conn = DBConnPool.getConnection();

            if (null == conn)
            {
                throw new Exception("连接数据库失败");
            }

            Statement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(strSQL);

                int nCount = 0;

                while (rs.next())
                {
                    xmlDataList.setColValue("Age", rs.getString(1).trim());
                    xmlDataList.setColValue("Cash", format(rs.getDouble(2)));
                    xmlDataList.insertRow(0);
                    nCount++;
                }

                // 加入现金价值记录的总的条数
                xmlDataset.addDataObject(new XMLDataTag("CashValueCount",
                        nCount));

                rs.close();
                stmt.close();
                conn.close();
            }
            catch (Exception ex)
            {
                if (null != rs)
                {
                    rs.close();
                }
                stmt.close();
                try
                {
                    conn.close();
                }
                catch (Exception e)
                {}
                ;
                throw ex;
            }
        }

        xmlDataset.addDataObject(xmlDataList);
    }

    /**
     * 查询影印件数据
     * @param xmlDataset XMLDataset
     * @param aLCPolSchema LCPolSchema
     * @throws Exception
     */
    private void getScanPic(XMLDataset xmlDataset, LCPolSchema aLCPolSchema) throws
            Exception
    {
        XMLDataList xmlDataList = new XMLDataList();

        xmlDataList.setDataObjectID("PicFile");

        xmlDataList.addColHead("FileUrl");
        xmlDataList.addColHead("PageIndex");
        xmlDataList.buildColHead();

        VData vData = new VData();
        vData.add(aLCPolSchema.getPrtNo());

        EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();

        if (!tEasyScanQueryBL.submitData(vData, "QUERY||MAIN"))
        {
            System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
        }
        else
        {
            vData.clear();
            vData = tEasyScanQueryBL.getResult();

            String strFileName = "";
            for (int nIndex = 0; nIndex < vData.size(); nIndex++)
            {

                if (nIndex == 3)
                {
                    continue; // 去掉第四页扫描件
                }

                strFileName = (String) vData.get(nIndex);
//      strFileName = strFileName.substring(strFileName.lastIndexOf("/") + 1);
                strFileName = strFileName.substring(0,
                        strFileName.lastIndexOf(".")) + ".tif";
//      strFileName += ".tif";

                xmlDataList.setColValue("FileUrl", strFileName);
                xmlDataList.setColValue("PageIndex", nIndex);
                xmlDataList.insertRow(0);
            }
        }
        xmlDataset.addDataObject(xmlDataList);
    }

    private void getClauseFile(XMLDataset xmlDataset, LCPolSchema aLCPolSchema) throws
            Exception
    {
        XMLDataList xmlDataList = new XMLDataList("ClauseFile");

        xmlDataList.addColHead("FileUrl");
        xmlDataList.buildColHead();

        xmlDataset.addDataObject(xmlDataList);
    }

    /**
     * 取得责任
     * @param xmlDataset
     * @param aLCPolSchema
     * @throws Exception
     */
    private void getDutyList(XMLDataset xmlDataset, LCPolSchema aLCPolSchema) throws
            Exception
    {
        XMLDataList xmlDataList = new XMLDataList();

        xmlDataList.setDataObjectID("DutyList");

        xmlDataList.addColHead("COL1");
        xmlDataList.addColHead("COL2");
        xmlDataList.addColHead("COL3");
        xmlDataList.addColHead("COL4");
        xmlDataList.buildColHead();

        String strSQL =
                "select lmdutyget.getdutyname, standmoney from lcget, lmdutyget"
                + " where lmdutyget.getdutycode = lcget.getdutycode"
                + " and polno = '" + aLCPolSchema.getPolNo() + "'";

        ExeSQL exeSQL = new ExeSQL();

        SSRS rs = exeSQL.execSQL(strSQL);

        for (int n = 0; n < (rs.getMaxRow() + 1) / 2; n++)
        {
            xmlDataList.setColValue("COL1", rs.GetText(n * 2 + 1, 1));
            xmlDataList.setColValue("COL2", rs.GetText(n * 2 + 1, 2));

            if (2 * n + 2 <= rs.getMaxRow())
            {
                xmlDataList.setColValue("COL3", rs.GetText(n * 2 + 2, 1));
                xmlDataList.setColValue("COL4", rs.GetText(n * 2 + 2, 2));
            }
            xmlDataList.insertRow(0);
        }
        xmlDataset.addDataObject(xmlDataList);
    }

    /**
     * 正常打印的取数流程
     * @param aLCPolSet LCPolSet 主险附加险保单数据。其中，第一个元素是主险保单，其它元素是附加险保单。
     * @return boolean
     */
    private boolean getPolDataSet(LCPolSet aLCPolSet)
    {
        ProposalQueryBL proposalQueryBL = new ProposalQueryBL();

        VData vData = new VData();
        vData.addElement(aLCPolSet.get(1));

        if (!proposalQueryBL.submitData(vData, "QUERY||DETAIL"))
        {
            if (proposalQueryBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(proposalQueryBL.mErrors);
                return false;
            }
            else
            {
                buildError("getPrintData",
                           "在调用ProposalQueryBL时发生错误，但是没有提供详细的错误信息!");
                return false;
            }
        }

        // 得到查询结果
        vData = proposalQueryBL.getResult();

        XMLDataset xmlDataset = mXMLDatasets.createDataset();

        LCAppntSchema tLCAppntSchema = (LCAppntSchema) vData.
                                       getObjectByObjectName(
                                               "LCAppntSchema", 0);

        xmlDataset.addDataObject(new XMLDataTag("LCPol.IDNo",
                                                tLCAppntSchema.getIDNo()));

        xmlDataset.addSchema(tLCAppntSchema);

        LCPolSchema tLCPolSchema = (LCPolSchema) vData.getObjectByObjectName(
                "LCPolSchema", 0);

        // 获取打印的模板名，需要在LMRiskForm中描述信息
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMRiskFormDB.setFormType("PP");
        if (!tLMRiskFormDB.getInfo())
        {
            buildError("getPrintData",
                       "没有该险种保单的打印模板信息。险种" + tLCPolSchema.getRiskCode());
            return false;
        }

        // 个单模板为OtherSign的第一个字符
        xmlDataset.setTemplate(tLMRiskFormDB.getFormName());

        // 签单机构为管理机构的前四位
        tLCPolSchema.setSignCom(tLCPolSchema.getManageCom().substring(0, 4));

        // 查询年金型和分红型的标志
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();

        tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMRiskAppDB.getInfo();

        if (tLMRiskAppDB.getRiskType2() == null ||
            !tLMRiskAppDB.getRiskType2().equals("Y"))
        {
            tLCPolSchema.setGetYear( -100);
            tLCPolSchema.setGetYearFlag(null);
        }

        if (tLMRiskAppDB.getBonusFlag() == null ||
            !tLMRiskAppDB.getBonusFlag().equals("Y"))
        {
            tLCPolSchema.setBonusGetMode(null);
        }

        // 对趸交的保单，将交至日期设为保单的生效日期
        if (tLCPolSchema.getPayIntv() == 0)
        {
            tLCPolSchema.setPaytoDate(tLCPolSchema.getCValiDate());
        }
        else
        {
            tLCPolSchema.setPaytoDate(prevDay(tLCPolSchema.getPaytoDate()));
        }

        // 设置保单的签单日期
        tLCPolSchema.setSignDate(prevDay(tLCPolSchema.getCValiDate()));

        // 设置保单的代理机构
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(tLCPolSchema.getAgentCom());
        tLAComDB.getInfo();
        tLCPolSchema.setAgentCom(tLAComDB.getName());

        xmlDataset.addSchema(tLCPolSchema);

        // 加入代理人姓名的信息
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("getPolDataSet", "在获取代理人数据时发生错误");
            System.out.println(tLAAgentDB.mErrors.getFirstError());
            return false;
        }

        xmlDataset.addDataObject(new XMLDataTag("LCPol.AgentName",
                                                tLAAgentDB.getName()));

        xmlDataset.addSchema((LCAppntGrpSchema) vData.getObjectByObjectName(
                "LCAppntGrpSchema", 0));

        xmlDataset.addSchemaSet((LCInsuredSet) vData.getObjectByObjectName(
                "LCInsuredSet", 0), "");
        xmlDataset.addSchemaSet((LCDutySet) vData.getObjectByObjectName(
                "LCDutySet", 0), "");
        xmlDataset.addSchemaSet((LCBnfSet) vData.getObjectByObjectName(
                "LCBnfSet", 0), "");
        xmlDataset.addSchemaSet((LCCustomerImpartSet) vData.
                                getObjectByObjectName("LCCustomerImpartSet", 0),
                                "");

        /**
         * 如果没有特别约定，打印“此栏空白”
         */
        LCSpecSet tLCSpecSet = (LCSpecSet) vData.getObjectByObjectName(
                "LCSpecSet", 0);
        LCSpecSet tNewLCSpecSet = new LCSpecSet();

        // 将有效的特约信息放到新的Set中
        for (int nIndex = 0; nIndex < tLCSpecSet.size(); nIndex++)
        {
            LCSpecSchema tLCSpecSchema = tLCSpecSet.get(nIndex + 1);
            if (tLCSpecSchema.getSpecContent() != null
                && !tLCSpecSchema.getSpecContent().equals(""))
            {
                tNewLCSpecSet.add(tLCSpecSchema);
            }
        }

        // 如果没有任何特约，打印“此栏空白”
        if (tNewLCSpecSet.size() == 0)
        {
            LCSpecSchema tLCSpecSchema = new LCSpecSchema();

            tLCSpecSchema.setSpecContent("此栏空白");
            tNewLCSpecSet.add(tLCSpecSchema);
        }

        xmlDataset.addSchemaSet(tNewLCSpecSet, "");
        xmlDataset.addSchemaSet((LMDutySet) vData.getObjectByObjectName(
                "LMDutySet", 0), "");

        // 加入保费合计和伤残责任以及医院的信息
        double dPremSum = 0;

        String strInjuryGetFlag = "N";
        String strHospitalFlag = "N";
        String strRiskCode = "";
        String strTemp = "";

        for (int i = 0; i < aLCPolSet.size(); i++)
        {
            dPremSum += aLCPolSet.get(i + 1).getPrem();

            strRiskCode = aLCPolSet.get(i + 1).getRiskCode();

            tLMRiskAppDB = new LMRiskAppDB();

            tLMRiskAppDB.setRiskCode(strRiskCode);
            if (!tLMRiskAppDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
                return false;
            }

            strTemp = tLMRiskAppDB.getNeedPrintGet();
            if (strTemp != null && strTemp.equals("1"))
            {
                strInjuryGetFlag = "Y";
            }

            strTemp = tLMRiskAppDB.getNeedPrintHospital();
            if (strTemp != null && strTemp.equals("1"))
            {
                strHospitalFlag = "Y";
            }
        }

        // 加入医院列表和伤残给付责任表的信息
        xmlDataset.addDataObject(new XMLDataTag("InjuryGetFlag",
                                                strInjuryGetFlag));
        xmlDataset.addDataObject(new XMLDataTag("HospitalFlag", strHospitalFlag));

        xmlDataset.addDataObject(new XMLDataTag("PremSum", format(dPremSum)));

        // Kevin 2002-03-14
        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.PayMoneySum",
                                                format(dPremSum)));
        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.PayMoneySum1",
                                                PubFun.getChnMoney(dPremSum)));

        try
        {
            //获取险种信息
//            genRiskInfo(xmlDataset, aLCPolSet);
            //获取现金价值
            getCashValue(xmlDataset, aLCPolSet.get(1));
            //获取各类影印件
            getScanPic(xmlDataset, aLCPolSet.get(1));
            //获取责任
            getDutyList(xmlDataset, aLCPolSet.get(1));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getPolDataSet", ex.getMessage());
            return false;
        }

        return true;
    }

    private void genXMLFile()
    {
        try
        {
            FileWriter writer = new FileWriter("LCPolData.xml");
            mXMLDatasets.output(writer);
            writer.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 获取主险/附加险列表和暂交费数据
     * @param xmlDataset
     * @param aLCPolSet
     * @throws Exception
     */
    private void genRiskInfo(XMLDataset xmlDataset, LCPolSet aLCPolSet) throws
            Exception
    {
        XMLDataList xmlDataList = new XMLDataList("RiskInfo");

        xmlDataList.addColHead("COL9");
        xmlDataList.addColHead("COL90");
        xmlDataList.addColHead("COL10");
        xmlDataList.addColHead("COL111");
        xmlDataList.addColHead("COL110");
        xmlDataList.addColHead("COL109");
        xmlDataList.addColHead("COL108");
        xmlDataList.addColHead("COL43");
        xmlDataList.addColHead("COL40");
        xmlDataList.addColHead("COL33");
        xmlDataList.addColHead("COL1");
        xmlDataList.addColHead("COL57");
        xmlDataList.addColHead("COL58");

        xmlDataList.buildColHead();
        xmlDataList.setAutoCol(false);

        double fPayMoneySum = 0f;

        for (int nIndex = 0; nIndex < aLCPolSet.size(); nIndex++)
        {
            LCPolSchema tLCPolSchema = aLCPolSet.get(nIndex + 1);

            xmlDataList.setColValue("COL58", String.valueOf(nIndex));
            xmlDataList.setColValue("COL9", tLCPolSchema.getRiskCode());

            // 加入主附险标志
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
            if (!tLMRiskAppDB.getInfo())
            {
                throw new Exception("险种描述取数失败");
            }

            xmlDataList.setColValue("COL90", tLMRiskAppDB.getSubRiskFlag());
            xmlDataList.setColValue("COL10", tLCPolSchema.getRiskVersion());

            xmlDataList.setColValue("COL111", tLCPolSchema.getInsuYear());
            xmlDataList.setColValue("COL110", tLCPolSchema.getInsuYearFlag());
            if (tLCPolSchema.getInsuYear() == 1000 &&
                tLCPolSchema.getInsuYearFlag().equals("A"))
            {
                xmlDataList.setColValue("COL111", "终身");
                xmlDataList.setColValue("COL110", "");
            }

            xmlDataList.setColValue("COL109", tLCPolSchema.getPayEndYear());
            xmlDataList.setColValue("COL108", tLCPolSchema.getPayEndYearFlag());

            xmlDataList.setColValue("COL43", tLCPolSchema.getAmnt());
            xmlDataList.setColValue("COL40", format(tLCPolSchema.getPrem()));

            // 对趸交的保单，将交费期满日设为保单的生效日期
            if (tLCPolSchema.getPayIntv() != 0)
            {
                xmlDataList.setColValue("COL33",
                                        prevDay(tLCPolSchema.getPayEndDate()));
            }
            else
            {
                xmlDataList.setColValue("COL33",
                                        prevDay(tLCPolSchema.getCValiDate()));
            }

            // 2003-04-28
            // 收据上每一个险种所对应的金额就是保费
            xmlDataList.setColValue("COL1", format(tLCPolSchema.getPrem()));
            xmlDataList.setColValue("COL57", tLCPolSchema.getPayIntv());

            xmlDataList.insertRow(0);
        }

        xmlDataset.addDataObject(xmlDataList);

        String strSQL = "SELECT TempFeeNo, Operator FROM LJTempFee WHERE ( OtherNoType = '0' OR OtherNoType = '4' ) AND OtherNo = '" +
                        aLCPolSet.get(1).getPolNo() + "'";

        SSRS ssRs = new ExeSQL().execSQL(strSQL);

        if (ssRs.ErrorFlag || ssRs.MaxRow == 0)
        {
            throw new Exception("找不到该个人保单的暂交费数据");
        }

        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.TempFeeNo",
                                                ssRs.GetText(1, 1)));
        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.Handler",
                                                ssRs.GetText(1, 2)));
    }

    private String prevDay(String strDate)
    {
        Date dt = PubFun.calDate(new FDate().getDate(strDate), -1, "D", null);
        return new FDate().getString(dt);
    }

    // 获取补打保单的数据
    private boolean getRePrintData()
    {

        String strCurDate = PubFun.getCurrentDate();
        String strCurTime = PubFun.getCurrentTime();

        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = null;

        // 利用LCPolSet来传递数据，其中包含的数据并不是LCPol的数据
        LCPolSet tLCPolSet = new LCPolSet();

        for (int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema.setSchema(mLCPolSet.get(nIndex + 1));

            ssrs = exeSQL.execSQL(
                    "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '" +
                    tLCPolSchema.getPolNo() + "'");

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                return false;
            }

            if (ssrs.MaxRow < 1)
            {
                buildError("getRePrintData", "找不到原来的打印数据，可能传入的不是主险保单号！");
                return false;
            }
            /*Lis5.3 upgrade set
                  tLCPolSchema.setPrintCount(ssrs.GetText(1, 1));
             */
            tLCPolSchema.setModifyDate(strCurDate);
            tLCPolSchema.setModifyTime(strCurTime);

            tLCPolSet.add(tLCPolSchema);
        }

        mResult.clear();
        mResult.add(tLCPolSet);

        LCPolF1PBLS tLCPolF1PBLS = new LCPolF1PBLS();

        if (!tLCPolF1PBLS.submitData(mResult, "REPRINT"))
        {
            if (!tLCPolF1PBLS.mErrors.needDealError())
            {
                buildError("getRePrint", "保存数据失败，但是没有提供详细信息");
                return false;
            }
            else
            {
                mErrors.copyAllErrors(tLCPolF1PBLS.mErrors);
                return false;
            }
        }

        // 取打印数据
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            DOMBuilder domBuilder = new DOMBuilder();
            Element rootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();
            stmt = conn.createStatement();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }
            String tSQL = "";
            java.sql.Blob tBlob = null;
            COracleBlob tCOracleBlob = new COracleBlob();
            for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
            {
                LCPolSchema tLCPolSchema = tLCPolSet.get(nIndex + 1);
                // modify by yt
                rs = stmt.executeQuery(
                        "SELECT PolInfo FROM LCPolPrint WHERE MainPolNo = '" +
                        tLCPolSchema.getPolNo() + "'");
                tSQL = " and MainPolNo = '" + tLCPolSchema.getPolNo() + "'";
                tBlob = tCOracleBlob.SelectBlob("LCPolPrint", "PolInfo", tSQL,
                                                conn);
                if (tBlob == null)
                {
                    throw new Exception("找不到打印数据");
                }
                Element ele = domBuilder.build(tBlob.getBinaryStream()).
                              getRootElement();
                ele = new Element("DATASET").setMixedContent(ele.
                        getMixedContent());
                rootElement.addContent(ele);

                rs.close();
            }

            Document doc = new Document(rootElement);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            XMLOutputter xmlOutputter = new XMLOutputter("  ", true, "UTF-8");
            xmlOutputter.output(doc, baos);

            mResult.clear();
            mResult.add(new ByteArrayInputStream(baos.toByteArray()));

            if (rs != null)
            {
                rs.close();
            }
            if (stmt != null)
            {
                stmt.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
            buildError("getRePrintData", ex.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 通过传入的保单号，得到主附险保单列表
     * 其中aLCPolSet中的第一个元素是主险保单，其余的元素是附加险保单
     */
    private boolean getPolSet(LCPolSchema aLCPolSchema, LCPolSet aLCPolSet)
    {
        LCPolDB tLCPolDB = new LCPolDB();

        tLCPolDB.setPolNo(aLCPolSchema.getPolNo());

        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            return false;
        }

        String szPrtNo = tLCPolDB.getPrtNo();

        tLCPolDB = new LCPolDB();
        // tLCPolDB.setPrtNo( szPrtNo );
        // 排序险种列表

        LCPolSet tLCPolSet =
                tLCPolDB.executeQuery("SELECT * FROM LCPol WHERE PrtNo = '" +
                                      szPrtNo + "' ORDER BY PolNo");

        if (tLCPolDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            return false;
        }

        String szMainPolNo = "";

        // 清空要返回的结果集
        aLCPolSet.clear();

        // 第一个元素放的是主险保单，其余元素放的是附加险保单
        for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(nIndex + 1);

            // 如果是主险保单
            if (tLCPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo()))
            {
                if (!szMainPolNo.equals(""))
                {
                    buildError("getPolSet", "同一个印刷号存在两个主险保单");
                    return false;
                }
                aLCPolSet.add(tLCPolSchema.getSchema());
                szMainPolNo = tLCPolSchema.getPolNo();
            }
        }

        // 判断是否找到了主险保单
        if (szMainPolNo.equals(""))
        {
            buildError("getPolSet", "找不到要打印的主险保单，印刷号为：" + szPrtNo);
            return false;
        }

        // 加入附加险的保单
        for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(nIndex + 1);

            if (!tLCPolSchema.getPolNo().equals(szMainPolNo))
            {
                aLCPolSet.add(tLCPolSchema.getSchema());
            }
        }

        return true;
    }


    /**
     * 保单重打的取数流程
     * @param aLCPolSet
     * @return
     */
    private boolean getPolDataSetEx(LCPolSet aLCPolSet)
    {
        XMLDataset xmlDataset = mXMLDatasets.createDataset();

        // 取打印数据
        Connection conn = null;

        try
        {
            conn = DBConnPool.getConnection();
            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            LCPolSchema tLCPolSchema = aLCPolSet.get(1);
            COracleBlob tCOracleBlob = new COracleBlob();
            String tSQL = "";
            java.sql.Blob tBlob = null;
            tSQL = " and MainPolNo = '" + tLCPolSchema.getPolNo() + "'";
            tBlob = tCOracleBlob.SelectBlob("LCPolPrint", "PolInfo", tSQL, conn);
            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            //BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            System.out.println("Get Blob");
            XMLDataBlob xmlDataBlob = new XMLDataBlob(tBlob.getBinaryStream());
            xmlDataset.addDataObject(xmlDataBlob);

            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
            buildError("getPolDataSetEx", ex.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 2003-04-28 Kevin
     * 格式化浮点型数据
     * @param dValue
     * @return
     */
    private String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private String getName(String strType, String strCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCodeType(strType);
        tLDCodeDB.setCode(strCode);

        if (!tLDCodeDB.getInfo())
        {
            return "";
        }
        else
        {
            return tLDCodeDB.getCodeName();
        }
    }

    /**
     * some special process for count print
     * @param doc
     */
    private void countPrint(Document doc)
    {
        Element ele = doc.getRootElement();

        String strOrg = "";

        //
        // 终止日期减一天
        //
        strOrg = ele.getChild("LCPol.EndDate").getText();
        strOrg = strOrg.substring(0, 4) + "-"
                 + strOrg.substring(5, 7) + "-"
                 + strOrg.substring(8, 10);
        strOrg = prevDay(strOrg);
        strOrg = strOrg.substring(0, 4) + "年"
                 + strOrg.substring(5, 7) + "月"
                 + strOrg.substring(8, 10) + "日";
        ele.getChild("LCPol.EndDate").setText(strOrg);

        // 代码到名字的转换
        //
        strOrg = ele.getChild("LCAppntInd.Sex").getText();
        ele.getChild("LCAppntInd.Sex").setText(getName("sex", strOrg));

        strOrg = ele.getChild("LCPol.InsuredSex").getText();
        ele.getChild("LCPol.InsuredSex").setText(getName("sex", strOrg));

        List list = ele.getChild("LCBnf").getChildren("ROW");
        for (int n = 0; n < list.size(); n++)
        {
            Element eleList = (Element) list.get(n);

            // 将受益比例设为百分比的形式
            strOrg = eleList.getChild("COL5").getText();
            strOrg = String.valueOf(
                    Double.parseDouble(strOrg) * 100) + "%";
            eleList.getChild("COL5").setText(strOrg);

            strOrg = eleList.getChild("COL9").getText();
            eleList.getChild("COL9").setText(getName("sex", strOrg));

            strOrg = eleList.getChild("COL4").getText();
            eleList.getChild("COL4").setText(getName("relation", strOrg));
        }

        // 如果没有受益人，打印“此栏空白”。
        if (list.size() == 0)
        {
            ele.getChild("LCBnf").addContent(
                    new Element("ROW").addContent(
                            new Element("COL8").addContent("此栏空白")));
        }

        // 设置管理机构的信息
        strOrg = ele.getChild("LCPol.ManageCom").getText();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(strOrg);
        tLDComDB.getInfo();
        ele.addContent(new Element("ManageCom.Phone").setText(tLDComDB.getPhone()));

        ele.getChild("LCPol.ManageCom").setText(getName("station", strOrg));

        // 添加管理机构的电话

    }
}
