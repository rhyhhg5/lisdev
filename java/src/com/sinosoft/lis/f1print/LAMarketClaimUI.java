package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAMarketClaimUI {
	public CErrors mErrors = new CErrors();

	private String mOperate;

	private VData mInputData = new VData();

	private LAMarketClaimBL mLAMarketClaimBL = new LAMarketClaimBL();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		mInputData = (VData) cInputData.clone();
		System.out.println("String LAMarketClaimBL begin...");
		LAMarketClaimBL tLLCustomerMessageBL = new LAMarketClaimBL();

		if (!tLLCustomerMessageBL.submitData(mInputData, mOperate)) {
			if (tLLCustomerMessageBL.mErrors.needDealError()) {
				mErrors.copyAllErrors(tLLCustomerMessageBL.mErrors);
				return false;
			} else {
				buildError("submitData", "LLCaseStatisticsUI发生错误，但是没有提供详细的出错信息");
				return false;
			}
		}

		else {
			mInputData = tLLCustomerMessageBL.getResult();
			return true;
		}

	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LLCustomerMessageUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mInputData;
	}

}
