package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TransferData;


public class PrintPDFManagerBL {
    // 常量定义
    // OtherNoType　其它号码类型
    public static final String ONT_INDPOL = "00"; // 个人保单号
    public static final String ONT_GRPPOL = "01"; // 集体保单号
    public static final String ONT_CONT = "02"; // 合同号
    public static final String ONT_EDOR = "03"; // 批单号
    public static final String ONT_GET = "04"; // 实付收据号
    public static final String ONT_PRT = "05"; // 保单印刷号
    public static final String ONT_EDORACCEPT = "06"; // 保全接受号
    
    public static final String ONT_CC_GRPCONT = "11";//卡折结算单团体合同号

    // Code　单据类型
    public static final String CODE_DECLINE = "00"; // 拒保通知书
    public static final String CODE_OUTPAY = "01"; // 首期溢交保费通知书
    public static final String CODE_BALANCE = "02"; // 退余额的打印格式
    public static final String CODE_PE = "03"; // 体检通知书
    public static final String CODE_MEET = "04"; // 面见通知书
    public static final String CODE_UW = "05"; // 核保通知书
    public static final String CODE_ISSUE = "85"; //个单问题件通知书
    public static final String CODE_DEFER = "06"; // 延期承保通知书
    public static final String CODE_FIRSTPAY = "07"; // 首期交费通知书
    public static final String CODE_REFUND = "08"; // 新契约退费通知书
    
    public static final String CODE_WITHDRAW = "09"; // 撤单通知书

    public static final String CODE_URGE_FP = "10"; // 首期交费通知书催办通知书
    public static final String CODE_URGE_PE = "11"; // 体检通知书催办通知书
    public static final String CODE_URGE_UW = "12"; // 核保通知书催办通知书
    public static final String CODE_URGE_MEET = "14"; // 面见通知书催办通知书
    public static final String CODE_URGE_GC = "13"; // 收款收据

    public static final String CODE_AGEN_QUEST = "14"; // 业务员问题件通知书
    public static final String CODE_URGE_PB = "15"; // 缴费催办通知书

    public static final String CODE_PEdorDECLINE = "20"; // 保全拒保通知书
    public static final String CODE_PEdorPE = "23"; // 保全体检通知书
    public static final String CODE_PEdorMEET = "24"; // 保全面见通知书
    public static final String CODE_PEdorUW = "25"; // 保全核保通知书
    public static final String CODE_PEdorDEFER = "26"; // 保全延期承保通知书

    public static final String CODE_PRnewDECLINE = "40"; // 续保拒保通知书
    public static final String CODE_PRnewPE = "43"; // 续保体检通知书
    public static final String CODE_PRnewMEET = "44"; // 续保面见通知书
    public static final String CODE_PRnewUW = "45"; // 续保核保通知书
    public static final String CODE_PRnewDEFER = "46"; // 续保延期承保通知书
    public static final String CODE_PRnewNotice = "47"; // 续保催收通知书
    public static final String CODE_PRnewSure = "49"; // 续保催收通知书


    public static final String CODE_BONUSPAY = "30"; // 个人红利派发通知书
    public static final String CODE_REPAY = "31"; // 续期缴费通知书
    public static final String CODE_PINVOICE = "32"; // 个人发票
    public static final String CODE_GINVOICE = "33"; // 团体发票
    public static final String CODE_GRPBONUSPAY = "34"; // 团体红利派发通知书
    public static final String CODE_INVOICE = "35"; // 发票(包括个团和保全)

    public static final String CODE_GRP_UW = "50"; // 团体核保通知书
    public static final String CODE_GRP_DECLINE = "51"; // 团体拒保通知书
    public static final String CODE_GRP_WITHDRAW = "52"; // 团体撤单通知书
    public static final String CODE_GRP_DEFER = "53"; // 团体延期承保通知书
    public static final String CODE_GRP_GRPFIRSTPAY = "57"; // 团体首期交费通知书
    
    public static final String CODE_CC_GRP_FIRSTPAY = "LC01";// 卡折业务结算单缴费凭证


    public static final String ASK_GRP_DECLINE = "60"; // 团体询价拒保通知书
    public static final String ASk_GRP_SUCESS = "61"; // 团体询价成功通知书
    public static final String Ask_GRP_WITHDRAW = "62"; // 团体询价撤单通知书
    public static final String ASK_GRP_DEFER = "63"; // 团体询价延期通知书
    public static final String ASK_GRP_INFO = "64"; // 团体询价补充材料通知书
    public static final String ASK_GRP_TRACK = "65"; // 团单询价跟踪通知书

    public static final String CODE_GRP_PE = "73"; // 团体体检通知书
    public static final String CODE_GRP_MEET = "74"; // 团体契调通知书

    public static final String CODE_PAY = "90"; //团险续期保险费对帐单
    public static final String CODE_INFORM = "91"; //团险续期续保交费通知书
    public static final String CODE_INDIPAY = "92"; //个人续期续保保险费对帐单
    public static final String CODE_INDIINFORM = "93"; //个人续期续保缴费通知书
    public static final String CODE_XB = "94"; //特需续保批单
    public static final String CODE_MJ = "95"; //特需满期终止批单
    public static final String CODE_CAL = "96"; //特需满期结算单
    public static final String CODE_INDIPAYH = "97"; //个人对帐单医院信息
    public static final String CODE_EXPIRY= "98"; //团险满期通知书
    public static final String CODE_SE= "XB001"; //少儿险个人续期续保缴费通知书
    public static final String CODE_MQ= "bq001"; //少儿险个人满期给付通知书
    public static final String CODE_TQ= "bq002"; //团险满期给付通知书

    // PrtType 打印方式
    public static final String PT_FRONT = "0"; // 前台打印
    public static final String PT_BACK = "1"; // 后台打印

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private String strUrl = "";
    private String fileName = "";
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private TransferData tTransferData = new TransferData();
    private String tRePrintFlag;

    public PrintPDFManagerBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("CONFIRM")
            && !cOperate.equals("REQUEST")
            && !cOperate.equals("PRINT")
            && !cOperate.equals("REQ")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        // 保存数据
        String tOperate = "UPDATE";
        VData tVData = new VData();
        tVData.add(mLOPRTManagerSchema);
        PrintManagerBLS tPrintManagerBLS = new PrintManagerBLS();
        if (!tPrintManagerBLS.submitData(tVData, tOperate)) {
            mErrors.copyAllErrors(tPrintManagerBLS.mErrors);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        TransferData ttTransferData = new TransferData();
        ttTransferData.setNameAndValue("RePrintFlag", "RePrintFlag");
        tGlobalInput.ComCode = "8611";
        tGlobalInput.ManageCom = "8611";
        tGlobalInput.Operator = "005";
        String mstrUrl = "E:\\workspace\\ui\\";
        String mfileName = "81000046101";
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

        tLOPRTManagerSchema.setPrtSeq("81000046101");

        VData vData = new VData();

        vData.addElement(tGlobalInput);
        vData.addElement(tLOPRTManagerSchema);
        vData.addElement(mstrUrl);
        vData.addElement(mfileName);
        vData.add(ttTransferData);
        PrintPDFManagerBL tPrintPDFManagerBL = new PrintPDFManagerBL();

        tPrintPDFManagerBL.submitData(vData, "PRINT");

        System.out.println(tPrintPDFManagerBL.mErrors.getFirstError());
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("PRINT")) { // 打印之后所执行的操作
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
            if (!tLOPRTManagerDB.getInfo()) {
                mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                buildError("dealData", "打印数据出错,请维护!");
                return false;
            }
            //查询打印队列的信息
            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
            if (!callPrintService(mLOPRTManagerSchema)) {
                return false;
            }
            // 打印后的处理
            if (("0").equals(mLOPRTManagerSchema.getStateFlag())) { //&&StrTool.cTrim(this.tRePrintFlag).equals(""))
                mLOPRTManagerSchema.setStateFlag("1");
            }
            mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
            mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
        } else {
            buildError("dealData", "不支持的操作字符串");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        strUrl = (String) cInputData.get(2);
        fileName = (String) cInputData.get(3);
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        System.out.println("tTransferData" + tTransferData);
        tRePrintFlag = (String) tTransferData.getValueByName("RePrintFlag");
        System.out.println("tRePrintFlag" + tRePrintFlag);
        if (StrTool.cTrim(tRePrintFlag).equals("null")) {
            tRePrintFlag = "";
        }
        if (mGlobalInput == null || mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrintManagerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用打印服务
     * @param aLOPRTManagerSchema
     * @return
     */
    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {
        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";

        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);
        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);
        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();
            VData vData = new VData();
            vData.add(mGlobalInput);
            vData.add(aLOPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }
            mResult = ps.getResult();
            XmlExport txmlExport = (XmlExport) mResult.getObjectByObjectName(
                    "XmlExport", 0);
            if (txmlExport == null) {
                System.out.println("没有所需的打印数据文件");
            }
            txmlExport.outputDocumentToFile(strUrl + "/printdata/data/brief/",
                                            "0" + aLOPRTManagerSchema.getCode() +
                                            "-" + fileName);
        } catch (Exception ex) {
            ex.printStackTrace();
            mErrors.addOneError("调用打印服务失败");
            buildError("callPrintService", ex.toString());
            return false;
        }
        return true;
    }
}
