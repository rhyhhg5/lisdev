package com.sinosoft.lis.f1print;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 保全核保问题件通知书</p>
* <p>Description:保全核保问题件通知书</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PEdorUWF1PUI
{
    PEdorUWF1PBL mPEdorUWF1PBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PEdorUWF1PUI(GlobalInput gi, String prtSeq)
    {
        mPEdorUWF1PBL = new PEdorUWF1PBL(gi, prtSeq);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorUWF1PBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorUWF1PBL.mErrors.getFirstError();
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPEdorUWF1PBL.getInputStream();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String args[])
    {
//        GlobalInput gi = new GlobalInput();
//        gi.Operator = "endor";
//        gi.ManageCom = "86";
//        PEdorUWF1PUI tPEdorBodyCheckPrintUI =
//                new PEdorUWF1PUI(gi, "81000001595");
//        if (!tPEdorBodyCheckPrintUI.submitData())
//        {
//            System.out.println(tPEdorBodyCheckPrintUI.getError());
//        }
    }
}
