package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author YuQikang
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LAManagecomPol2Save1BL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDay = "";
    private String mEndDay = "";
    private String mYear = "";
    private String[][] mShowDataList = null;
    private String[] mDataList = null;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }

  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      if(!mStartDay.substring(0,4).equals(mEndDay.substring(0,4)))
      {
          buildError("check", "[期初]与[期末]必须是同一年！");
          return false;
      }

      mYear = mStartDay.substring(0,4);

      return true;
  }

  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL  = "select * from ldcom where Sign='1'";
      tSQL += "   and comcode like '86%' and length(trim(comcode))=4";
      tSQL += "   and comcode <> '86' and   (comgrade='02'  or comgrade='01') order by comcode";
      LDComDB tLDComDB = new LDComDB();
      LDComSet tLDComSet = new LDComDBSet();
      tLDComSet = tLDComDB.executeQuery(tSQL);
      if(tLDComSet.size()==0)
      {
          buildError("queryData", "没有附和条件的机构！");
          return false;
      }

      // 2、查询需要表示的数据
      String[][] tShowDataList = new String[tLDComSet.size()][9];
      for(int i=1;i<=tLDComSet.size();i++)
      {// 循环机构进行统计
          LDComSchema tLDComSchema = new LDComSchema();
          tLDComSchema = tLDComSet.get(i);
          tShowDataList[i-1][0] = tLDComSchema.getName() ;
          System.out.println(tLDComSchema.getComCode()+"/"+tShowDataList[i-1][0]);

          // 查询需要表示的数据
          if(!queryOneDataList(tLDComSchema.getComCode(),tShowDataList[i-1]))
          {
              System.out.println("["+tLDComSchema.getComCode()+"] 本机构数据查询失败！");
              return false;
          }
          mShowDataList = tShowDataList;
      }

      // 3、进行排序整理

      return true;
  }
  private int getAgentCountByManageCom(String pmDate,String pmManageCom)
    {
        String tSQL = "";
        int tRtValue = 0;

        tSQL  = "select count(b.agentcode)";
        tSQL += "  from laagent b";
        tSQL += " where b.ManageCom like '" + pmManageCom + "%'";
        tSQL += "   and b.employdate<='" + pmDate + "'";
        tSQL += "   and (b.outworkdate>'" + pmDate + "' or b.outworkdate is null )";
        tSQL += "   and b.Branchtype='1' and b.BranchType2='01'";

        try{
            tRtValue = (int) execQuery(tSQL);
        }catch(Exception ex)
        {
            System.out.println("getAgentCountByManageCom 出错！");
        }

        return tRtValue;
  }
   /**
   * 取得本期平均人力
   * @param pmValue1 int
   * @param pmValue2 int
   * @return double
   */
  private String getAverageAgentCount(String pmValue1,String pmValue2)
  {
      String tSQL = "";
      String tRtValue = "";

      // 判断被除数是0
      if("0".equals(pmValue2))
      {
          return "0";
      }

      // 取得平均值
//      tSQL = "select DECIMAL((" + pmValue1 + " + " + pmValue2 +
//             ") / 2 ,12,1) from dual";
      tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + " + " + pmValue2 +
             ",12,2) / 2,12,2) from dual";

      try{
          tRtValue = String.valueOf(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAverageAgentCount 出错！");
      }

      return tRtValue;
  }


 /**
   * 查询本期累计件数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getYearCaseCount(String pmStartDate,String pmEndDate,String pmManageCom)
  {
      String tSQL1 = "";
      String tSQL2 = "";
      String tSQL3 = "";
      String tSQL4 = "";
      String tSQL5 = "";
      String tSQL6 = "";

      String tRtValue = "";
      double sumprem;
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL1  = "select count(distinct a.contno)  from  lcpol a,lccont b  where a.contno=b.contno  and b.salechnl='01' ";
      tSQL1 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
      //tSQL1 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
      tSQL1 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "' ";
      tSQL1 += "   and  a.polno not in ";
      tSQL1 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";

       System.out.println(tSQL1);
       tSQL2  = "select count(distinct a.contno)  from  lbpol a,lbcont b  where a.contno=b.contno  and b.salechnl='01' ";
       tSQL2 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
       //tSQL2 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
       tSQL2 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  ";
       tSQL2 += "   and  a.polno not in ";
       tSQL2 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";


     System.out.println(tSQL2);
      tSQL3  = "select count(distinct a.contno)  from  lbpol a,lccont b  where a.contno=b.contno  and b.salechnl='01' ";
       tSQL3 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
       //tSQL3 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
       tSQL3 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "' ";
       tSQL3 += "   and  a.polno not in ";
       tSQL3 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";


    System.out.println(tSQL3);
/**
    tSQL4  = "select count(distinct a.contno)  from  lcpol a,lccont b,es_doc_main c  where a.contno=b.contno ";
    tSQL4 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
    tSQL4 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
    tSQL4 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
    tSQL4 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";


      System.out.println(tSQL4);
    tSQL5  = "select count(distinct a.contno)  from  lbpol a,lbcont b,es_doc_main c  where a.contno=b.contno ";
    tSQL5 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
    tSQL5 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
    tSQL5 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
    tSQL5 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";


   System.out.println(tSQL5);
   tSQL6  = "select count(distinct a.contno)  from  lbpol a,lccont b,es_doc_main c  where a.contno=b.contno ";
   tSQL6 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
   tSQL6 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
   tSQL6 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
   tSQL6 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";

   System.out.println(tSQL6);
*/
    try{

        //sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3)+execQuery(tSQL4)+execQuery(tSQL5)+execQuery(tSQL6);
        sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3);

        System.out.println(sumprem);
       tRtValue = tRtValue = "" + tDF.format(sumprem);;

      }

      catch(Exception ex)
      {
          System.out.println("getYearCaseCount 出错！");
      }

      return tRtValue;
  }

  /**本期累计保费
   *
   */
  private String getYearCaseFee(String pmStartDate,String pmEndDate,String pmManageCom)
  {
      String tSQL1 = "";
      String tSQL2 = "";
      String tSQL3 = "";
      String tSQL4 = "";
      String tSQL5 = "";
      String tSQL6 = "";

      String tRtValue = "";
      double sumprem;
      DecimalFormat tDF = new DecimalFormat("0.######");


      tSQL1  = "select value(sum(a.prem),0)  from  lcpol a,lccont b  where a.contno=b.contno  and b.salechnl='01' ";
      tSQL1 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
      //tSQL1 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
      tSQL1 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  ";
      tSQL1 += "   and  a.polno not in ";
      tSQL1 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";

       System.out.println(tSQL1);
       tSQL2  = "select value(sum(a.prem),0)  from  lbpol a,lbcont b  where a.contno=b.contno  and b.salechnl='01' ";
       tSQL2 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
       //tSQL2 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
       tSQL2 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  ";
       tSQL2 += "   and  a.polno not in ";
       tSQL2 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";


     System.out.println(tSQL2);
      tSQL3  = "select value(sum(a.prem),0)  from  lbpol a,lccont b  where a.contno=b.contno  and b.salechnl='01' ";
       tSQL3 += " and (substr(b.managecom,1,4)='" + pmManageCom + "'  or substr(b.managecom,1,2)='" + pmManageCom + "' )";
      // tSQL3 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  and cardflag<>'0'  ";
       tSQL3 += "   and  b.makedate<='" + pmEndDate + "' and  b.makedate>='" + pmStartDate + "'  ";
       tSQL3 += "   and  a.polno not in ";
       tSQL3 += "   (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000'  )";


    System.out.println(tSQL3);
/**
    tSQL4  = "select value(sum(a.prem),0)  from  lcpol a,lccont b,es_doc_main c  where a.contno=b.contno ";
    tSQL4 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
    tSQL4 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
    tSQL4 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
    tSQL4 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";


      System.out.println(tSQL4);
    tSQL5  = "select value(sum(a.prem),0)  from  lbpol a,lbcont b,es_doc_main c  where a.contno=b.contno ";
    tSQL5 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
    tSQL5 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
    tSQL5 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
    tSQL5 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";


   System.out.println(tSQL5);
   tSQL6  = "select value(sum(a.prem),0)  from  lbpol a,lccont b,es_doc_main c  where a.contno=b.contno ";
   tSQL6 += " and b.salechnl='01' and (substr(b.managecom,1,4)= '" + pmManageCom + "'  or substr(b.managecom,1,2)= '" + pmManageCom + "')";
   tSQL6 += "   and  c.makedate<='" + pmEndDate + "' and  c.makedate>='" + pmStartDate + "'  and  c.doccode=a.prtno ";
   tSQL6 += "  and  c.subtype='TB01' and (cardflag='0' or cardflag is null)and  a.polno not in ";
   tSQL6 += "  (select aa.polno from LCRnewStateLog aa  where aa.grpcontno='00000000000000000000')";

   System.out.println(tSQL6);
*/
    try{

       // sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3)+execQuery(tSQL4)+execQuery(tSQL5)+execQuery(tSQL6);
               sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3);
        System.out.println(sumprem);
          tRtValue = tDF.format(Arith.div(sumprem,10000));
      }
      catch(Exception ex)
      {
          System.out.println("getFirstPrem 出错！");
      }

      return tRtValue;

  }

   /**
   * 件均保费
   * @param pmYearMoney String
   * @param pmCaseCount String
   * @return String
   */
  private String getYearAverageByCase(String pmYearMoney,String pmCaseCount)
  {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      if(0 == Integer.parseInt(pmCaseCount))
      {
          return "0";
      }

//      tSQL = "select DECIMAL(" + pmFirstYearMoney + " / " + pmCaseCount +
//             " ,12,2) from dual";
      tSQL = "select DECIMAL(DECIMAL(" + pmYearMoney + "*10000,12,2) / DECIMAL(" +
             pmCaseCount + ",12,2),12,2) from dual";

      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getYearAverageByCase 出错！");
      }

      return tRtValue;
  }

  /**
   * 计算人均保费
   * @param pmFirstYearMoney String
   * @param pmAgentCont String
   * @return String
   */
  private String getYearAverageMoney(String pmYearMoney,String pmAgentCont)
  {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      if(0.5 > Double.parseDouble(pmAgentCont))
      {
          return "0.0";
      }

      tSQL = "select DECIMAL(DECIMAL(" + pmYearMoney + "*10000,12,2) / DECIMAL(" +
             pmAgentCont + ",12,2),12,2) from dual";

      try{
          tRtValue =  "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getYearAverageMoney 出错！");
      }



      return tRtValue;
  }
  private String getHaveAgentCount(String pmStartDate,String pmEndDate,String pmManageCom)
    {
        String tSQL = "";
        int tRtValue = 0;

        tSQL = "select count(distinct b.AgentCode) from  laagent b";
        tSQL += " where b.ManageCom like '" + pmManageCom + "%'";
        tSQL += "   and b.employdate >= '" + pmStartDate + "'";
        tSQL += "   and b.employdate <= '" + pmEndDate + "'";
        tSQL += "   and b.Branchtype='1' and b.BranchType2='01'";

        try {
            tRtValue = (int) execQuery(tSQL);
        } catch (Exception ex) {
            System.out.println("getHaveCaseAgentCount 出错！");
        }

        return "" + tRtValue;
    }
  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList(String pmComCode,String[] pmOneDataList)
  {
      try{
          // 0、机构名称
          // 1、本期累计保费
          pmOneDataList[1] = getYearCaseFee(mStartDay, mEndDay, pmComCode);
          // 2、本期累计件数
          pmOneDataList[2] = getYearCaseCount(mStartDay, mEndDay, pmComCode);
          // 3、件均保费
          pmOneDataList[3] = getYearAverageByCase(pmOneDataList[1],pmOneDataList[2]);
          // 4、期初人力
          pmOneDataList[4] = "" + getAgentCountByManageCom(mStartDay, pmComCode);
          // 5、期末人力
          pmOneDataList[5] = "" + getAgentCountByManageCom(mEndDay, pmComCode);
          // 6、本期人力
          pmOneDataList[6] = getHaveAgentCount(mStartDay,mEndDay,pmComCode);
          // 7、平均人力
          pmOneDataList[7] = getAverageAgentCount(pmOneDataList[4], pmOneDataList[5]);
          // 8、人均保费
          pmOneDataList[8] = getYearAverageMoney(pmOneDataList[1],pmOneDataList[7]);

       }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      String tSQL = "";
      String CurrentDate = PubFun.getCurrentDate();//得到当天日期
      //String strArr[] = null;

      try{
          // 1、查询数据
          if(!getDataList())
          {
              return false;
          }

         System.out.println(mShowDataList.length);
         System.out.println(mShowDataList[0].length);
          this.mDataList = new String[mShowDataList[0].length];


          // 2、追加 合计 行
          if(!setAddRow(mShowDataList.length + 1))
          {
             buildError("queryData", "进行合计计算时出错！");
             return false;
          }

          // 3、设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Order");

          for(int i=0;i<mShowDataList.length;i++)
          {
              tlistTable.add(mShowDataList[i]);
          }
          tlistTable.add(mDataList);

          TextTag texttag = new TextTag();    //新建一个TextTag的实例
          texttag.add("StartYear", this.mStartDay.substring(0,4));   //输入制表时间
          texttag.add("StartMonth", this.mStartDay.substring(5,7));  //输入制表时间
          texttag.add("StartDay", this.mStartDay.substring(8,10));   //输入制表时间
          texttag.add("EndYear", this.mEndDay.substring(0,4));       //输入制表时间
          texttag.add("EndMonth", this.mEndDay.substring(5,7));      //输入制表时间
          texttag.add("EndDay", this.mEndDay.substring(8,10));       //输入制表时间
          texttag.add("CurrentDate", this.currentDate);              //输入制表时间
          texttag.add("CurrentTime", this.currentTime);              //输入制表时间

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAManagecomPol2Report2.vts", "printer"); //最好紧接着就初始化xml文档
          if (texttag.size() > 0)
              xmlexport.addTextTag(texttag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          mResult.addElement(xmlexport);
      }catch (Exception ex)
      {
          buildError("queryData", "LAManagecomPol2ReportBL发生错误，准备数据时出错！");
          return false;
      }

      return true;
  }

  /**
   * 追加一条合计行
   * @return boolean
   */
  private boolean setAddRow(int pmRow)
  {
      System.out.println("合计行的行数是：" + pmRow);

      mDataList[0] = "合  计";
      mDataList[1] = dealSum(1);
      mDataList[2] = dealSum(2);
      mDataList[3] = getYearAverageByCase(mDataList[1], mDataList[2]);
      mDataList[4] = dealSum(4);
      mDataList[5] = dealSum(5);
      mDataList[6] = dealSum(6);
      mDataList[7] = getAverageAgentCount(mDataList[4], mDataList[5]);
      mDataList[8] = getYearAverageMoney(mDataList[1],mDataList[7]);

       return true;
  }

  /**
   * 对传入的数组进行求和处理
   * @param pmArrNum int
   * @return String
   */
  private String dealSum(int pmArrNum)
  {
      String tReturnValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");
      String tSQL = "select 0";

      for(int i=0;i<this.mShowDataList.length;i++)
      {
          tSQL += " + " + this.mShowDataList[i][pmArrNum];
      }

      tSQL += " + 0 from dual";

      tReturnValue = "" + tDF.format(execQuery(tSQL));

      return tReturnValue;
  }

  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
     mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
            "GlobalInput", 0));
      mStartDay = (String) pmInputData.get(0);
      mEndDay = (String) pmInputData.get(1);
      System.out.println(mStartDay+" / "+mEndDay);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAManagecompol2Report2BL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }

  /**
   * 执行SQL文查询结果
   * @param sql String
   * @return double
   */
  private double execQuery(String sql)
  {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
    }

  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
 }
