package com.sinosoft.lis.f1print;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

import java.util.*;
/**
 * <p>Title: 保全统计 </p>
 * <p>Description: 保单工作状况统计月报表 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author yanchao
 * @version 1.0
 * @date 2005-11.18
 */

public class LLPendingBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ListTable tlistTable;
    private ListTable klistTable;
    private VData mResult = new VData();
    private String mFileNameB = "";
    private String moperator = "";
	private String mMakeDate = "";
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    SSRS lSSRS = new SSRS();
    SSRS xSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String StartDate = "";
    String EndDate = "";
    String organcode="";
    String organname="";
    String handlecode="";
    String handlename="";
    String handler="";
    String organcode1="";
    String caseType = "";
    String partSQL1 = "";
    String partSQL2 = "";
    public LLPendingBL()
    {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }else{
			TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", moperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if(!tLLPrintSave.submitData(tVData,"")){
				return false;
			     }
		}

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLPendingBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**s
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        organcode = (String) tTransferData.getValueByName("organcode");
        handlecode = (String) tTransferData.getValueByName("handlecode");
        mFileNameB = (String)tTransferData.getValueByName("tFileNameB");
        caseType = (String) tTransferData.getValueByName("tCaseType");
        if("1".equals(caseType)){
        	partSQL1 = " and  exists  (select 1 from llregister where rgtno=c.rgtno and (applyertype='0' or applyertype is null)) ";
        	partSQL2 = "and  exists  (select 1 from llregister r,llcase l where l.caseno=o.caseno and r.rgtno=l.rgtno and (applyertype='0' or applyertype is null)) ";
        }else if("2".equals(caseType)){
        	partSQL1 = " and accdentdesc='磁盘导入'";
        	partSQL2 = " and exists (select 1 from llcase where caseno=o.caseno and accdentdesc='磁盘导入') ";
        	
        }else if("3".equals(caseType)){
        	partSQL1 = " and caseprop='09' ";
        	partSQL2 = " and exists (select 1 from llcase where caseno=o.caseno and caseprop='09') ";
        	
        }else if("4".equals(caseType)){
        	partSQL1 = " and accdentdesc='批量受理' ";
        	partSQL2 = " and exists (select 1 from llcase where caseno=o.caseno and accdentdesc='批量受理') " ;
        }else if("5".equals(caseType)){//TODO:新增预付赔款
        	partSQL1 = " and prepaidflag='1' ";
        }else{
        	partSQL1 = partSQL2 ="";
        }
        moperator = mGlobalInput.Operator;
        System.out.println(StartDate);
        System.out.println(EndDate);
        System.out.println(organcode);

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    public String getState(String CaseNo) {

        String SQL2 = "select rgtstate from llcaseoptime where caseno='" + CaseNo + "'and enddate= '" + EndDate +
                      "' order by endtime desc with ur ";
        lSSRS = tExeSQL.execSQL(SQL2);
        String State2="";
        if (lSSRS.getMaxRow()==0) {
          String  SQL1 = "select Startdate,Starttime from llcaseoptime where caseno ='" +CaseNo + "' and '"  + EndDate  +
                   "'>=Startdate and '" + EndDate+"' < enddate order by enddate,endtime with ur  ";
            xSSRS = tExeSQL.execSQL(SQL1);
            if (xSSRS.getMaxRow() >0 ) {
                State2 = StrTool.cTrim(xSSRS.GetText(1, 1));
                if(!(State2.equals("10")||State2.equals("01"))){
                    SQL2 = "select rgtstate from llcaseoptime where caseno ='" +
                           CaseNo + "' and "
                           + " enddate ='" + xSSRS.GetText(1, 1) +
                           "' and endtime ='" + xSSRS.GetText(1, 2) + "'";
                    State2 = tExeSQL.getOneValue(SQL2);
                }
            } else {
                 SQL1 = "select rgtstate from llcaseoptime where caseno ='" + CaseNo +
                       "' order by enddate desc ,endtime desc with ur ";
                lSSRS = tExeSQL.execSQL(SQL1);
                if(lSSRS.getMaxRow() >0 )
                    State2 = StrTool.cTrim(lSSRS.GetText(1, 1));
            }
        } else {
            State2 = StrTool.cTrim(lSSRS.GetText(1, 1));
        }
        System.out.println("案件明细"+CaseNo+"状态"+State2);
        return State2;
    }

    public String  getCount(int tDate,String hand,String Condition) {
        String tCount = "";
        System.out.println("理赔人"+hand+Condition+"天数"+tDate);
        String SQL =
                "select caseno from llcase c where days('"+EndDate+"')-days(rgtdate) "+Condition
                + tDate + " and rgttype='1' " + "  and handler='"+hand+"'"
                +" and endcasedate>'"+EndDate+"'"
                +partSQL1
                +" union "
                + "select caseno from llcase c where days('"+EndDate+"')-days(rgtdate) "+Condition
                + tDate + " and rgttype='1' " + "  and handler='"+hand+"'"
                +" and rgtstate='14' and cancledate>'"+EndDate+"' "
                +partSQL1
//                +" and caseno not in (select caseno from llcase where endcasedate <= '"+EndDate+"')"
//                +" and caseno not in (select caseno from llcase where cancledate <= '"+EndDate+"' and rgtstate='14') "
                
                +" with ur ";
        tSSRS = tExeSQL.execSQL(SQL);
        String CaseNo = "";
        String State ="";
        int count = 0;
        for (int k = 1; k <= tSSRS.getMaxRow(); k++) {

            CaseNo = tSSRS.GetText(k, 1);
            String SQL3="select rgtstate from llcase c where caseno='"+CaseNo+"' with ur ";
            String State1 = tExeSQL.getOneValue(SQL3);

            if (State1.equals("13")) {
                continue;
            }
            //新增“理赔二核”
            if (State1.equals("01") || State1.equals("02") || State1.equals("03") || State1.equals("04") ||
                State1.equals("06") || State1.equals("07") || State1.equals("08") || State1.equals("16")) {
                count = count + 1;
                continue;
            }
            State =getState(CaseNo);
            if (!(State.equals("10") || State.equals("05"))) {
                    count = count + 1;
                }
        }

        String SQL4= "select count(c.caseno) from llcase c where days('"+EndDate+"')-days(c.rgtdate) "+Condition
                 + tDate + " and c.rgttype='1' and c.rgtstate in ('05','10')"
                 +partSQL1
                 + "  and c.dealer='" + hand + "' with ur";
        String countSC= StrTool.cTrim(tExeSQL.getOneValue(SQL4));
        if(!countSC.equals("")){
            count=count+Integer.parseInt(countSC);
        }

        String SQL5="select distinct t.caseno from llcaseoptime t,llcase c  where t.rgtstate in ('10','06') "
                    +" and t.caseno=c.caseno and c.rgttype='1'and c.endcasedate is not null  and t.enddate is not null "
                    +" and days('"+EndDate+"')-days(c.rgtdate) "+Condition + tDate
                    +" and '" +EndDate  +"'>=t.Startdate and '" + EndDate+"' <= t.enddate and t.operator='"+hand+"'"
                    +" and not exists (select 1 from llcase where caseno=c.caseno and endcasedate<='"+EndDate+"')"
                    +" and not exists (select 1 from llcase where caseno=c.caseno and cancledate<= '"+EndDate+"' and rgtstate='14' )"
//                    +" and c.caseno not in (select caseno from llcase where endcasedate <= '"+EndDate+"')"
//                    +" and c.caseno not in (select caseno from llcase where cancledate <= '"+EndDate+"' and rgtstate='14') "
                    +partSQL1
                    +" union select distinct t.caseno  from llcaseoptime t,llcase c  where t.rgtstate in ('05')"
                    +" and t.caseno=c.caseno and c.rgttype='1' and c.rgtstate='14' and t.enddate is not null "
                    +" and days('"+EndDate+"')-days(c.rgtdate) "+Condition + tDate +" and c.dealer='"+hand+"'"
//                    +" and c.caseno not in (select caseno from llcase where endcasedate <= '"+EndDate+"')"
                    +" and not exists (select 1 from llcase where caseno=c.caseno and endcasedate<='"+EndDate+"') "
                    +partSQL1
                    +" and not exists (select 1 from llcase where caseno=c.caseno and cancledate<= '"+EndDate+"' and rgtstate='14' )" 
                    +" with ur  " ;
        tSSRS = new SSRS();
        System.out.println(SQL5);
        tSSRS = tExeSQL.execSQL(SQL5);
        for (int k = 1; k <= tSSRS.getMaxRow(); k++) {
             CaseNo = tSSRS.GetText(k, 1);
             State =getState(CaseNo);
             if (State.equals("10")||State.equals("05")) {
                   count = count + 1;
               }
        }
        System.out.println("理赔人对应日期的件数"+count);
       tCount=String.valueOf(count) ;

        return tCount;
    }
    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLPendingLis.vts", "printer"); //最好紧接着就初始化xml文档
        String handler1="";

        if (!handlecode.equals("")) {
            handler = " and handler ='" + handlecode + "' ";
            handler1 = " and o.operator ='" + handlecode + "' ";
        }
        if (organcode.equals("86000000")) {
            organcode1 = " and  u.comcode='86' ";
        }else {
            organcode1= " and  u.comcode like '"+organcode+"%' ";
        }
        
    //按受理人统计
    klistTable = new ListTable();
    klistTable.setName("ORGANP");
    int count1=0;
    int count2=0;
    int count3=0;
    int count4=0;
    int count5 = 0;
    int count6 = 0;
    int count7 = 0;
    int count8 = 0;
    int count9 = 0;
    int count10 = 0;
    int count11 = 0;
    int count12= 0;
    int count13 = 0;
    int count14= 0;

//    String SQLA =
//                "select distinct c.handler m,u.username from llcase c,llclaimuser u where  "
//                + " c.rgttype='1' and c.handler=u.usercode "+ handler + organcode1
//                + " and c.caseno not in (select caseno from llcase where endcasedate < '"+EndDate+"')"
//                + " and c.caseno not in (select caseno from llcase where cancledate <= '"+EndDate+"' and rgtstate='14') "
//                +partSQL1
//                + " and c.rgtdate <='"+EndDate + "' union "
//                + " select distinct o.operator m,u.username from llcaseoptime o,llclaimuser u where  "
//                + " o.rgtstate in ('06','10') and o.operator=u.usercode "+ handler1+organcode1
//                + " and o.caseno not in (select caseno from llcase where endcasedate < '"+EndDate+"')"
//                + "  and o.caseno not in (select caseno from llcase where cancledate <= '"+EndDate+"' and rgtstate='14') "
//                +partSQL2
//                + " and o.caseno not in (select caseno from llcase where rgtdate > '"+EndDate+"' )"
//                + "  union select distinct c.handler m,u.username from llcase c,llclaimuser u  where c.rgtstate in ('12','11','09') "
//                + " and c.handler=u.usercode "+organcode1+" and c.endcasedate between '"+StartDate+"' and  '"+EndDate+"'"
//                +partSQL1
//                + " order by m with ur";
	String SQLA = "select usercode,username from llclaimuser where comcode like '"
		+ organcode + "%' and StateFlag='1' and ClaimDeal='1' and HandleFlag='1' order by usercode with ur";


    System.out.println(SQLA);
    kSSRS = tExeSQL.execSQL(SQLA);

    for (int i = 1; i <= kSSRS.getMaxRow(); i++) {

        String[] strArr = new String[16];
        strArr[0] = kSSRS.GetText(i, 1);
        strArr[1] = kSSRS.GetText(i, 2);
        strArr[2] = getCount(0, kSSRS.GetText(i, 1),"=");
        count1 += Integer.parseInt(strArr[2]);
        strArr[3] = getCount(1, kSSRS.GetText(i, 1),"=");
        count2 += Integer.parseInt(strArr[3]);
        strArr[4] = getCount(2, kSSRS.GetText(i, 1),"=");
        count3 += Integer.parseInt(strArr[4]);
        strArr[5] = getCount(3, kSSRS.GetText(i, 1),"=");
        count4 += Integer.parseInt(strArr[5]);
        strArr[6] = getCount(4, kSSRS.GetText(i, 1),"=");
        count5 += Integer.parseInt(strArr[6]);
        strArr[7] = getCount(5, kSSRS.GetText(i, 1),"=");
        count6 += Integer.parseInt(strArr[7]);
        strArr[8] = getCount(6, kSSRS.GetText(i, 1),"=");
        count7 += Integer.parseInt(strArr[8]);
        strArr[9] = getCount(7, kSSRS.GetText(i, 1),"=");
        count8 += Integer.parseInt(strArr[9]);
        strArr[10] = getCount(8, kSSRS.GetText(i, 1),"=");
        count9 += Integer.parseInt(strArr[10]);
        strArr[11]= getCount(9, kSSRS.GetText(i, 1),"=");
        count10 += Integer.parseInt(strArr[11]);
        strArr[12]= getCount(9, kSSRS.GetText(i, 1),">");
        count11 += Integer.parseInt(strArr[12]);
        int x=Integer.parseInt(strArr[12])+Integer.parseInt(strArr[2])+Integer.parseInt(strArr[3])
              +Integer.parseInt(strArr[4])+Integer.parseInt(strArr[5])+Integer.parseInt(strArr[6])
              +Integer.parseInt(strArr[7])+Integer.parseInt(strArr[8])+Integer.parseInt(strArr[9])
              +Integer.parseInt(strArr[10])+Integer.parseInt(strArr[11]);
        String  ToCount = String.valueOf(x);
        strArr[13]= ToCount ;
        count12 += Integer.parseInt(strArr[13]);
        String SQL8 =
                "select caseno from llcase c where rgtdate between '"+StartDate+"' and '"+EndDate+"' and rgttype='1'  "
                +  "  and handler='" + kSSRS.GetText(i, 1)
//                +"' and caseno not in (select caseno from llcase where cancledate <= '"+EndDate+"' and rgtstate='14') "
                +"' and not exists (select 1 from llcase where caseno=c.caseno and rgtstate='14' and cancledate <= '"+EndDate+"')"
                +partSQL1
                + " with ur ";
        tSSRS = tExeSQL.execSQL(SQL8);
        System.out.print("SQL8=" + SQL8);
        strArr[14]= String.valueOf(tSSRS.getMaxRow()) ;
        count13 += Integer.parseInt(strArr[14]);
        String SQL3="select caseno from llcase c where rgtstate in ('12','11','09') "
                    +"and endcasedate between '"+StartDate+"' and  '"+EndDate+"' and handler='"+kSSRS.GetText(i, 1)+"'"
                    +partSQL1;
          tSSRS = tExeSQL.execSQL(SQL3);
        strArr[15]=String.valueOf(tSSRS.getMaxRow()) ;
        count14 += Integer.parseInt(strArr[15]);



        klistTable.add(strArr);

         }
        String[] strArrHead = new String[16];
        strArrHead[0] = "操作步骤";
        strArrHead[1] = "1天";
        strArrHead[2] = "2天";
        strArrHead[3] = "3天";
        strArrHead[4] = "4天";
        strArrHead[5] = "5天";
        strArrHead[6] = "6天";
        strArrHead[7] = "7天";
        strArrHead[8] = "8天";
        strArrHead[9] = "9天";
        strArrHead[10] = "10天";
        strArrHead[11] = "10天以上";
        strArrHead[12] = "合计";
        strArrHead[13] = "新增";
        strArrHead[14] = "完成";
        strArrHead[15] = "成";

        xmlexport.addListTable(klistTable, strArrHead);

        texttag.add("count1", count1);
        texttag.add("count2", count2);
        texttag.add("count3", count3);
        texttag.add("count4", count4);
        texttag.add("count5", count5);
        texttag.add("count6", count6);
        texttag.add("count7", count7);
        texttag.add("count8", count8);
        texttag.add("count9", count9);
        texttag.add("count10", count10);
        texttag.add("count11", count11);
        texttag.add("count12", count12);
        texttag.add("count13", count13);
        texttag.add("count14", count14);

        String SQL4="select name from ldcom where comcode='"+organcode+"'";
        organname=tExeSQL.getOneValue(SQL4);
        String SQL6="select username from llclaimuser where usercode='"+handlecode+"'";
        handlename=tExeSQL.getOneValue(SQL6);
        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);
        texttag.add("organcode", organcode);
        texttag.add("organname", organname);
        texttag.add("handlecode", handlecode);
        texttag.add("handlename", handlename);
        texttag.add("operator", mGlobalInput.Operator);

        String MakeDate = PubFun.getCurrentDate();
        MakeDate = AgentPubFun.formatDate(MakeDate, "yyyy-MM-dd");
        texttag.add("MakeDate", MakeDate);
        mMakeDate = MakeDate;
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }


//        xmlexport.outputDocumentToFile("d:\\", "TaskPrint"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

       String   tEndDate="2006-7-15";
       String   tOrgancode="8611";
       String  tHandlecode="";
       String tStartDate="2006-7-05";

       VData tVData = new VData();
       GlobalInput tG = new GlobalInput();
       LLPendingBL LLPendingBL = new LLPendingBL();
       TransferData tTransferData = new TransferData();
       tTransferData.setNameAndValue("StartDate", tStartDate);
       tTransferData.setNameAndValue("EndDate", tEndDate);
       tTransferData.setNameAndValue("organcode", tOrgancode);
        tTransferData.setNameAndValue("handlecode", tHandlecode);
       tVData.addElement(tG);
       tVData.addElement(tTransferData);

       LLPendingBL.submitData(tVData,"PRINT");


    }

    private void jbInit() throws Exception {
    }

}
