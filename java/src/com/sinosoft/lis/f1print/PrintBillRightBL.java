package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function ://打印“银行代扣正确清单”
 * @version 1.0
 * @date 2003-04-01
 */

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LYReturnFromBankBDB;
import com.sinosoft.lis.db.LYReturnFromBankDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LYReturnFromBankBSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintBillRightBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;

    private String mBankName; //银行名称
    private String mErrorReason; //失败原因
    private String mChkSuccFlag; //银行校验成功标志；
    private String mChkFailFlag; //银行校验失败标志；
    private double mSumDuePayMoney = 0;
    public PrintBillRightBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData begin");
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        strMngCom = (String) cInputData.get(2);
        System.out.println("在PrintBillRightBL.java中得到的批次号码是" + strBillNo);
        System.out.println("在PrintBillRightBL.java中得到的银行代码是" + strBankCode);
        System.out.println("在PrintBillRightBL.java中得到的管理机构是" + strMngCom);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PrintBillRightBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        System.out.println("根据银行代码查询出银行名称！！！");

        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(strBankCode);
        LDBankSet tLDBankSet = new LDBankSet();
        tLDBankSet.set(tLDBankDB.query());
        int bank_count = tLDBankSet.size();
        if (bank_count == 0)
        {
            this.mErrors.copyAllErrors(tLDBankDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintBillBL";
            tError.functionName = "submitData";
            tError.errorMessage = "银行信息表查询失败，没有该银行代码！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LDBankSchema tLDBankSchema = new LDBankSchema();
        tLDBankSchema.setSchema(tLDBankSet.get(1));
        mBankName = tLDBankSchema.getBankName();
        System.out.println("打印出银行信息和批次号码！！！");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        System.out.println("根据批次号对银行返回盘记录表的查询");
        LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
        String y_sql = "select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode '"
                       + strBankCode.trim()
                       + "' and ComCode like '"
                       + strMngCom.trim()
                       + "%'";

        LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
        tLYReturnFromBankSet = tLYReturnFromBankDB.executeQuery(y_sql);
        System.out.println("返回盘中执行的查询语句是" + y_sql);
        int n = tLYReturnFromBankSet.size();
        if (n == 0)
        {
            System.out.println("该次交易可能被核销，在银行返回盘记录备份表中进行查询");
            LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
            LYReturnFromBankBSet tLYReturnFromBankBSet = new
                    LYReturnFromBankBSet();
            String t_sql = "select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and BankCode = '"
                           + strBankCode.trim()
                           + "' and ComCode like '"
                           + strMngCom.trim()
                           + "%'";
            tLYReturnFromBankBSet = (tLYReturnFromBankBDB.executeQuery(t_sql));
            System.out.println("备份表中所执行的语句是" + t_sql);
            int m = tLYReturnFromBankBSet.size();
            int count = 0; //成功的总笔数
            if (m == 0)
            {
                this.mErrors.copyAllErrors(tLYReturnFromBankBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillRightBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有满足条件的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("对成功标志进行判断！！！");
            int b_count;
            b_count = 1;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintBillRight.vts", "printer");
            ListTable alistTable = new ListTable();
            alistTable.setName("INFO");
            for (int j = 1; j <= m; j++)
            {
                System.out.println("￥￥￥￥￥在银行返回盘记录备份表的处理中此时的J的数值是" + j);
                String[] cols = new String[5];
                LYReturnFromBankBSchema tLYReturnFromBankBSchema = new
                        LYReturnFromBankBSchema();
                tLYReturnFromBankBSchema.setSchema(tLYReturnFromBankBSet.get(j));
                String tbPayCode = tLYReturnFromBankBSchema.getPayCode();
                String b_BankSuccFlag;
                if (tLYReturnFromBankBSchema.getBankSuccFlag() == null ||
                    tLYReturnFromBankBSchema.getBankSuccFlag().equals(""))
                {
                    b_BankSuccFlag = "0";
                }
                else
                {
                    b_BankSuccFlag = tLYReturnFromBankBSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + b_BankSuccFlag);
                System.out.println("2003-04-25");
                System.out.println("采用了胡博的方法");
                String hq_flag_b = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag_b);
                boolean jy_flag_b = verifyBankSuccFlag(hq_flag_b,
                        b_BankSuccFlag);

                System.out.println("校验后的成功标志是" + jy_flag_b);
                if (!jy_flag_b)
                {
                    continue;
                }
                count++; //成功的笔数
                System.out.println("此时的Count的值是" + count);

                b_count++;
                System.out.println("b_count的值是" + b_count);
                System.out.println("…………在进行银行代扣正确清单的打印中成功的标记个数是" + b_count +
                                   "-1条记录！！！");
                cols[0] = tLYReturnFromBankBSchema.getAgentCode();
                cols[3] = tLYReturnFromBankBSchema.getAccNo();
                cols[4] = tLYReturnFromBankBSchema.getAccName();

                System.out.println("代理人编码是" + cols[0]);
                System.out.println("当J的值是" + j + "时，银行的账号是" + cols[3]);
                System.out.println("当J的值是" + j + "时，银行的账号名是" + cols[4]);
                System.out.println("2003-04-23添加的程序，查询出应收表中的总金额！！！");
                mSumDuePayMoney = mSumDuePayMoney +
                                  tLYReturnFromBankBSchema.getPayMoney();
                if (!(cols[0].equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";
                    ExeSQL exesql = new ExeSQL();
                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                    }
                }

                System.out.println("在应收总表中查询出总金额");

                alistTable.add(cols);
            }
            if (b_count == 1)
            {
                System.out.println("返回提示信息");
                this.mErrors.copyAllErrors(tLDBankDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillRightBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有成功的信息！！！不能进行打印！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String[] col = new String[5];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("SumDuePayMoney", mSumDuePayMoney);
            texttag.add("Count", count);
            texttag.add("Date", PubFun.getCurrentDate());
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }

            //xmlexport.outputDocumentToFile("e:\\","printbillright");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
            System.out.println("￥￥￥￥￥￥b_count的值是====" + b_count);
        }
        else
        {
            int _count;
            _count = 1;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintBillRight.vts", "printer");
            ListTable blistTable = new ListTable();
            blistTable.setName("INFO");
            int t_count = 0;
            for (int i = 1; i <= n; i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = new
                        LYReturnFromBankSchema();
                tLYReturnFromBankSchema.setSchema(tLYReturnFromBankSet.get(i));
                //记录账号名和账户
                String[] cols = new String[5];
                String mBankSuccFlag;
                String _BankSuccFlag;
                if (tLYReturnFromBankSchema.getBankSuccFlag().equals("") ||
                    tLYReturnFromBankSchema.getBankSuccFlag() == null)
                {
                    _BankSuccFlag = "0";
                }
                else
                {
                    _BankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                System.out.println("采用了胡博的方法后回盘表的数据");
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                String hq_flag = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag);
                boolean jy_flag = verifyBankSuccFlag(hq_flag, _BankSuccFlag);
                System.out.println("银行的校验标志是" + jy_flag);
                System.out.println("2003-04-25");
                if (!jy_flag)
                {
                    continue;
                }
                t_count++; //记录成功的笔数
                System.out.println("成功的笔数是" + t_count);
//        if(!_BankSuccFlag.equals("1"))
//          continue;
                _count++;
                cols[0] = tLYReturnFromBankSchema.getAgentCode();
                cols[3] = tLYReturnFromBankSchema.getAccNo();
                cols[4] = tLYReturnFromBankSchema.getAccName();
                System.out.println("代理人代码是" + cols[0]);
                System.out.println("银行的账号是" + cols[3]);
                System.out.println("银行的账户名称是" + cols[4]);
                mSumDuePayMoney = mSumDuePayMoney +
                                  tLYReturnFromBankSchema.getPayMoney();

                if (!(cols[0].equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    ExeSQL exesql = new ExeSQL();
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";
                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);

                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                    }
                }
                blistTable.add(cols);
                System.out.println("getPrintData end");
            }
            String[] b_col = new String[5];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(blistTable, b_col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("SumDuePayMoney", mSumDuePayMoney);
            texttag.add("Count", t_count);
            texttag.add("Date", PubFun.getCurrentDate());
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            mResult.clear();
            mResult.addElement(xmlexport);

            System.out.println("对银行信息表的操作，根据银行代码查找出银行名称");
            System.out.println("对“应收总表”进行查询，查询出代理人的信息");
            System.out.println("getPrintData begin");
        }
        return true;
    }

    //获取银行的成功标志
    public String getBankSuccFlag(String tBankCode)
    {
        System.out.println("银行代码是" + tBankCode);
        try
        {
            LDBankSchema tLDBankSchema = new LDBankSchema();
            tLDBankSchema.setBankCode(tBankCode);
            tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));
            return tLDBankSchema.getAgentPaySuccFlag();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new NullPointerException("获取银行扣款成功标志信息失败！(getBankSuccFlag) " +
                                           e.getMessage());
        }
    }

    //校验成功标志的方法
    public boolean verifyBankSuccFlag(String bankSuccFlag, String bBankSuccFlag)
    {
        int i;
        boolean passFlag = false;

        String[] arrSucc = PubFun.split(bankSuccFlag, ";");
        String tSucc = bBankSuccFlag;
        for (i = 0; i < arrSucc.length; i++)
        {
            if (arrSucc[i].equals(tSucc))
            {
                passFlag = true;
                break;
            }
        }
        return passFlag;
    }

    public static void main(String[] args)
    {
        String tBillNo = "00000000000000000003";
        String tBankCode = "0701";
        String tMngCom = "86";
        VData tVData = new VData();
        tVData.addElement(tBillNo);
        tVData.addElement(tBankCode);
        tVData.addElement(tMngCom);
        PrintBillRightUI tPrintBillRightUI = new PrintBillRightUI();
        tPrintBillRightUI.submitData(tVData, "PRINT");
    }
}