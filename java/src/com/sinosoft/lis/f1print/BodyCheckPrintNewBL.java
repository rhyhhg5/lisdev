package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPENoticeDB;
import com.sinosoft.lis.db.LCPENoticeItemDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDHospitalDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LZSysCertifyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPENoticeItemSchema;
import com.sinosoft.lis.schema.LCPENoticeSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LZSysCertifySchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;
import com.sinosoft.lis.vschema.LDHospitalSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.agentbranch.LABranchGroup;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDOperatorInsideDB;
import com.sinosoft.lis.vschema.LDOperatorInsideSet;

public class BodyCheckPrintNewBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    private String mContNo = "";

    private String HealthName = "";

    //输入的查询sql语句
    private String msql = "";

    //取得的延期承保原因
    private String mUWError = "";

    //取得的代理人编码
    private String mAgentCode = "";

    private String hospitalAdress = "";

    private MMap map = new MMap();
     private String mflag = null;

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCContSet mLCContSet = new LCContSet();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();

    private LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

    private static int sumCount = 3;

    private TransferData mTransferData = new TransferData();

    public BodyCheckPrintNewBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
//        if (!cOperate.equals("PRINT"))
//        {
//            buildError("submitData", "不支持的操作字符串");
//            return false;
//        }
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        //        if (!sendSysCertify())
        //        {
        //            return false;
        //        }


        //准备打印管理表数据
        if (!dealPrintMag()) {
         return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(this.map);
        if (!tPubSubmit.submitData(tVData, ""))
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
//        if (mLOPRTManagerSchema.getPrtSeq() == null)
//        {
//            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
//            return false;
//        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private String getDate(Date date)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    private boolean getPrintData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        //        String flag = (String) mTransferData.getValueByName(
        //                "flag");
        //        System.out.println("zhuzhu" + flag);
        //根据印刷号查询打印队列中的纪录
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例

        String PrtNo = mLOPRTManagerSchema.getOldPrtSeq(); //打印流水号
        System.out.println("PrtNo=" + PrtNo);
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getOldPrtSeq());
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        //        mLOPRTManagerSchema.setStateFlag("1");

        //        map.put(this.mLOPRTManagerSchema,"UPDATE");
        if (mLOPRTManagerSchema.getCode().equals("16"))
        {
            xmlexport.createDocuments("PENoticeBack.vts", mGlobalInput); //最好紧接着就初始化xml文档
        }
        else
        {
            xmlexport.createDocuments("PENotice.vts", mGlobalInput);
        } //最好紧接着就初始化xml文档
        boolean PEFlag = false; //打印体检件部分的判断标志
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        int m, i;
        if (!tLCContDB.getInfo())
        {
            mErrors.copyAllErrors(tLCContDB.mErrors);
            buildError("outputXML", "在取得LCCont的数据时发生错误");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        //投保人地址和邮编
        LCAppntDB tLCAppntDB = new LCAppntDB();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        if (mLCContSchema.getContType().equals("1"))
        {
            tLCAppntDB.setContNo(mLCContSchema.getContNo());
            if (tLCAppntDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCAppntDB.mErrors);
                buildError("outputXML", "在取得打印队列中数据时发生错误");
                return false;
            }
            tLCAddressDB.setCustomerNo(mLCContSchema.getAppntNo());
            tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
            if (tLCAddressDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCAddressDB.mErrors);
                buildError("outputXML", "在取得打印队列中数据时发生错误");
                return false;
            }
            mLCAddressSchema = tLCAddressDB.getSchema();
        }

        mAgentCode = mLCContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        //2-体检信息
        //2-1 查询体检主表
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeDB.setProposalContNo(mLCContSchema.getProposalContNo());
        if (mLOPRTManagerSchema.getCode().equals("16"))
        {
            tLCPENoticeDB.setPrtSeq(mLOPRTManagerSchema.getStandbyFlag2());
        }
        else
        {
            tLCPENoticeDB.setPrtSeq(PrtNo);
        }
        tLCPENoticeDB.setCustomerNo(mLOPRTManagerSchema.getStandbyFlag1());

        if (!tLCPENoticeDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得体检通知的数据时发生错误");
            return false;
        }

        tLCPENoticeSchema.setSchema(tLCPENoticeDB.getSchema());
        HealthName = tLCPENoticeSchema.getName(); //保存体检人名称

        String PEDate = tLCPENoticeSchema.getPEDate(); //保存体检日期
        String NeedLimosis = "";
        if (tLCPENoticeSchema.getPEBeforeCond().equals("Y"))
        { //是否需要空腹
            NeedLimosis = "是";
        }
        else
        {
            NeedLimosis = "否";
        }
        String PEAddress = tLCPENoticeSchema.getPEAddress(); //体检地点代码

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("hospitalcode");
        tLDCodeDB.setCode(PEAddress);
        if (tLDCodeDB.getInfo())
        {
            PEAddress = tLDCodeDB.getCodeName();
        }
        else
        {
            tLDCodeDB.setCodeType("hospitalcodeuw");
            tLDCodeDB.setCode(PEAddress);
            if (tLDCodeDB.getInfo())
            {
                PEAddress = tLDCodeDB.getCodeName();
            }
            else
            {
                PEAddress = "该医院代码尚未建立，请确认！";
            }
        }

        //
        //tLDHospitalDB.setHospitCode(tLCPENoticeSchema.getPEAddress());
        //if (tLDHospitalDB.getInfo())
        {
            //        String hospitalAdress = tLDHospitalDB.getAddress();
        }
        System.out.print("hospitalAdress=" + hospitalAdress);

        //2-1 查询体检子表
        String[] PETitle = new String[2];
        PETitle[0] = "ID";
        PETitle[1] = "CHECKITEM";
        ListTable tPEListTable = new ListTable();
        String strPE[] = null;
        tPEListTable.setName("CHECKITEM"); //对应模版体检部分的行对象名
        LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
        LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
        tLCPENoticeItemDB.setContNo(mLCContSchema.getContNo());
        if (this.mLOPRTManagerSchema.getCode().equals("16"))
        {
            tLCPENoticeItemDB.setPrtSeq(mLOPRTManagerSchema.getStandbyFlag2());
        }
        else
        {
            tLCPENoticeItemDB.setPrtSeq(PrtNo);
        }
        tLCPENoticeItemDB.setFreePE("N");
        tLCPENoticeItemSet.set(tLCPENoticeItemDB.query());
        //结束标记

        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        /**
         * 将体检项目拼成字符串显示在一个单元格中
         */
        String PEItem = "";
        if (tLCPENoticeItemSet == null || tLCPENoticeItemSet.size() == 0)
        {
            PEFlag = false;
        }
        else
        {
            PEFlag = true;
            LCPENoticeItemSchema tLCPENoticeItemSchema;
            for (i = 1; i <= tLCPENoticeItemSet.size(); i++)
            {
                tLCPENoticeItemSchema = new LCPENoticeItemSchema();
                tLCPENoticeItemSchema.setSchema(tLCPENoticeItemSet.get(i));
                if(i == 1)    PEItem = tLCPENoticeItemSchema.getTestGrpCode() + " : ";//添加体检套餐代码
                strPE = new String[2];
                strPE[0] = (new Integer(i)).toString(); //序号
                strPE[1] = tLCPENoticeItemSchema.getPEItemName(); //序号对应的内容
                PEItem += strPE[1] + ", ";
                /* if (i % 7 == 0) {
                 PEItem += "\n";
                 }*/
                //tPEListTable.add(strPE);
            }
        }
        /**
         * 体检医院及地址显示
         */
        String strSql_1 = "select c.* from lhgroupcont a,lhcontitem b,ldhospital c"
                + " where a.Contrano = b.Contrano and "
                + " b.DutyItemCode = 'HB0001'  and   Dutystate = '1' "
                + " and   c.HospitCode = a.hospitcode and c.managecom='"
                + this.mLCContSchema.getManageCom().substring(0, 4) + "'";
        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        LDHospitalSet tLDHospitalSet = tLDHospitalDB.executeQuery(strSql_1);
        String[] Hospital = new String[4];
        Hospital[0] = "HosptialName";
        Hospital[1] = "Address";
        Hospital[2] = "Blank1";
        Hospital[3] = "Blank2";
        ListTable tHospitalListTable = new ListTable();
        tHospitalListTable.setName("Hospital");
        String[] detail = null;
        boolean hospitalFlag = false;
        if (tLDHospitalSet != null)
        {
            for (int n = 1; n <= tLDHospitalSet.size(); n++)
            {
                hospitalFlag = true;
                detail = new String[6];
                detail[0] = tLDHospitalSet.get(n).getHospitName();
                if (tLDHospitalSet.get(n).getAddress() != null)
                {
                    detail[1] = tLDHospitalSet.get(n).getAddress();
                }
                else
                {
                    detail[1] = "";
                }
                detail[2] = "";
                detail[3] = "";
                detail[5] = "";
                detail[4] = tLDHospitalSet.get(n).getPhone() == null ? ""
                        : tLDHospitalSet.get(n).getPhone();
                tHospitalListTable.add(detail);
            }
        }

        String strsql1 = "SELECT hospitname FROM LDHospital WHERE hospitcode='"
                + tLCPENoticeSchema.getPEAddress() + "'";
        SSRS HAddress = tExeSQL.execSQL(strsql1.toString());
        String HAddress1 = null;
        if (HAddress.getMaxRow() > 0)
        {
            HAddress1 = HAddress.GetText(1, 1);
        }

        System.out.println("BABA:" + HAddress1);

        /** 动态行数，为了将显示等分 */
//        ListTable tCountTable = new ListTable();
//        String[] tCount = new String[1];
//        tCountTable.setName("Blank");
//        tCount[0] = "Blank";
//        int count = sumCount - tLDHospitalSet.size();
//        if (count > 0)
//        {
//            for (i = 1; i <= count; i++)
//            {
//                String[] blank = { "" };
//                tCountTable.add(blank);
//            }
//        }
//        if (tCountTable.size() > 0)
//        {
//            xmlexport.addListTable(tCountTable, tCount);
//        }

        //体检人性别和年龄

        LDPersonDB tLDPersonDB = new LDPersonDB();

        tLDPersonDB.setCustomerNo(tLCPENoticeSchema.getCustomerNo());

        if (tLDPersonDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLDPersonDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLDPersonSchema = tLDPersonDB.getSchema();
        /**
         * 取出客户相关信息
         */
        String tIDType = mLDPersonSchema.getIDType();
        String strSql = "select codename from ldcode where codetype='idtype' and code='"
                + tIDType + "'";
        String tIDTypeName = tExeSQL.getOneValue(strSql);
        String tIDNo = mLDPersonSchema.getIDNo();
        FDate fdate = new FDate();
        fdate.getDate(PubFun.getCurrentDate());

        //其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        texttag.add("JetFormType", "03");
       //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
       String sqlusercom = "select comcode from lduser where usercode='" +
                         mGlobalInput.Operator + "' with ur";
       String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
           comcode = "86";
            } else if (comcode.length() >= 4) {
           comcode = comcode.substring(0, 4);
         } else {
           buildError("getInputData", "操作员机构查询出错！");
           return false;
         }
         String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);

       texttag.add("ManageComLength4", printcode);
       texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
       if(mflag.equals("batch")){
           texttag.add("previewflag", "0");
       }else{
           texttag.add("previewflag", "1");
       }


        //取被保人信息
       LCInsuredDB tLCInsuredDB = new LCInsuredDB();

       tLCInsuredDB.setContNo(this.mLCContSchema.getContNo());
       tLCInsuredDB.setInsuredNo(this.mLCContSchema.getInsuredNo());

       if (tLCInsuredDB.getInfo() == false)
       {
           mErrors.copyAllErrors(tLDPersonDB.mErrors);
           buildError("outputXML", "在取被保人信息时发生错误");
           return false;
       }

	    String tMarriage = tLCInsuredDB.getMarriage();
	    String tSex = tLCInsuredDB.getSex();
	    String tBirthday = tLCInsuredDB.getBirthday();


        //生成-年-月-日格式的日期
        //        StrTool tSrtTool = new StrTool();
        String PEDate1 = "";
        PEDate1 = PEDate + "-";
        Date SysDate = (new FDate()).getDate(PEDate);
        Date MakeDate = (new FDate()).getDate(PEDate);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar
                .setTime((new FDate()).getDate(tLCPENoticeSchema.getMakeDate()));
        calendar.add(Calendar.DATE, 10);
        texttag.add("CheckDate", getDate(calendar.getTime()));

        //模版自上而下的元素
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            return false;
        }
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getServicePhone());

        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                    "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("Post", mLCAddressSchema.getZipCode()); //投保人邮政编码
        texttag.add("Address", mLCAddressSchema.getPostalAddress()); //投保人地址
        texttag.add("XI_AppntNo", mLCContSchema.getAppntNo());
        texttag.add("AppntName", mLCContSchema.getAppntName());
        texttag.add("LCCont.AppntName", mLCContSchema.getAppntName()); //投保人名称
        texttag.add("LCCont.ContNo", mLCContSchema.getContNo()); //投保单号
        texttag.add("HealthName", HealthName); //被保险人名称
        texttag.add("Sex", getSexName(tSex)); //体检人性别
        texttag.add("Marriage", getMarriageName(tMarriage)); //体检人性别
        texttag.add("Age", PubFun.calInterval(tBirthday,
                PubFun.getCurrentDate(), "Y")); //体检人年龄
        texttag.add("NeedLimosis", NeedLimosis); //是否需要空腹
        texttag.add("Hospital", PEAddress); //体检地点
        texttag.add("PEBrachAddress", hospitalAdress);
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("AgentCode", mLAAgentSchema.getGroupAgentCode()); //代理人业务号
        texttag.add("LCCont.ManageCom",
                getComName(mLCContSchema.getManageCom())); //营业机构
        texttag.add("PrtNo", mLCContSchema.getPrtNo()); //刘水号
        texttag.add("LCCont.PrtNo", mLCContSchema.getPrtNo()); //印刷号
        texttag.add("LCCont.UWCode", tLCPENoticeDB.getOperator()); // 核保师代码
        texttag.add("SysDate", getDate((new FDate()).getDate(tLCPENoticeSchema
                .getMakeDate())));
        texttag.add("PEITEM", PEItem);
        texttag.add("IDType", tIDTypeName);
        texttag.add("IDNo", tIDNo);
        texttag.add("HAddress1", HAddress1);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(mLAAgentSchema.getAgentGroup());
        if (!tLABranchGroupDB.getInfo())
        {
            buildError("getPrintData", "查询代理机构组别失败！");
            return false;
        }
        texttag.add("AgentGroupName", tLABranchGroupDB.getBranchAttr());
        if (!StrTool.cTrim(mLAAgentSchema.getMobile()).equals(""))
        {
            texttag.add("AgentPhone", mLAAgentSchema.getMobile());
        }
        else if (!StrTool.cTrim(mLAAgentSchema.getPhone()).equals(""))
        {
            texttag.add("AgentPhone", mLAAgentSchema.getPhone());
        }
        else
        {
            texttag.add("AgentPhone", "          　");
        }
        /**
         * 体检专员,各个分公司不同目前没有定因体检专员权限
         * 暂时写死以后更新
         * @author Yanmging 2005-08-25
         */
        LDOperatorInsideDB tLDOperatorInsideDB = new LDOperatorInsideDB();
        tLDOperatorInsideDB.setUserType("02");
        tLDOperatorInsideDB.setManageCom(mLCContSchema.getManageCom());
        LDOperatorInsideSet tLDOperatorInsideSet = tLDOperatorInsideDB.query();
        if (tLDOperatorInsideSet != null && tLDOperatorInsideSet.size() > 0)
        {
            texttag.add("PEName", tLDOperatorInsideSet.get(1).getUserName());
            texttag.add("PEPhone", tLDOperatorInsideSet.get(1).getPhone());
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8611"))
        {
            texttag.add("PEName", "陈红晓");
            texttag.add("PEPhone", "010-58790688");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8694"))
        {
            texttag.add("PEName", "曹惠娟");
            texttag.add("PEPhone", "0532-83079832");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8631"))
        {
            texttag.add("PEName", "张钧实");
            texttag.add("PEPhone", "021-61001166-8213");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8653"))
        {
            texttag.add("PEName", "刘晓琳");
            texttag.add("PEPhone", "0871-5165988-8138");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8695"))
        {
            texttag.add("PEName", "张磊");
            texttag.add("PEPhone", "0755-83830185");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8633"))
        {
            texttag.add("PEName", "付清枫");
            texttag.add("PEPhone", "0571-28918898-8082");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8637"))
        {
            texttag.add("PEName", "庄朝辉");
            texttag.add("PEPhone", "0531-89819857");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8632"))
        {
            texttag.add("PEName", "张迎新");
            texttag.add("PEPhone", "025-83212999-8702");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8621"))
        {
            texttag.add("PEName", "许艳丽");
            texttag.add("PEPhone", "024-62251776");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8635"))
        {
            texttag.add("PEName", "丘松英");
            texttag.add("PEPhone", "0591-87672826");
        }
        else if (StrTool.cTrim(this.mLCContSchema.getManageCom()).startsWith(
                "8612"))
        {
            texttag.add("PEName", "王辰");
            texttag.add("PEPhone", "022-83867518转7871");
        }

        System.out.println("wanbo==" + mLCContSchema.getCValiDate());
        texttag.add("HealthNo", mLOPRTManagerSchema.getPrtSeq());
        if (mLCContSchema.getCValiDate() != null)
        {
            String[] CvaliDate = mLCContSchema.getCValiDate().split("-");
            texttag.add("Cavidate", CvaliDate[0] + "年" + CvaliDate[1] + "月"
                    + CvaliDate[2] + "日"); //体检日期
        }
        texttag.add("Reason", tLCPENoticeSchema.getRemark());

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + mLCContSchema.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //保存体检信息
        if (PEFlag == true)
        {
            xmlexport.addListTable(tPEListTable, PETitle); //保存体检信息
        }

        if (hospitalFlag == true)
        {
            xmlexport.addListTable(tHospitalListTable, Hospital); //保存体检信息
        }

        //结束标记
        xmlexport.addListTable(tEndListTable);
        xmlexport.outputDocumentToFile("C:\\", "bodycheckprint");
        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println("return true");
        return true;

    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode
     * @return
     * @throws Exception
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    private String getMarriageName(String StrMarriage)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(StrMarriage);
        tLDCodeDB.setCodeType("marriage");
        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    private String getSexName(String StrSex)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(StrSex);
        tLDCodeDB.setCodeType("sex");
        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    private boolean dealPrintMag() {
//           LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//           tLOPRTManagerSchema.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
//           tLOPRTManagerSchema.setOtherNo(mLOPRTManagerSchema.getOtherNo()); //存放前台传来的caseno
//           tLOPRTManagerSchema.setOtherNoType("09");
//           tLOPRTManagerSchema.setCode("lp005");
//           tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
//           tLOPRTManagerSchema.setAgentCode("");
//           tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
//           tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
//           tLOPRTManagerSchema.setPrtType("0");
//           tLOPRTManagerSchema.setStateFlag("0");
//           tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
//           tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//           tLOPRTManagerSchema.setStandbyFlag2(mLOPRTManagerSchema.getOtherNo());

           mResult.addElement(mLOPRTManagerSchema);
           return true;
    }

    private boolean sendSysCertify()
    {
        LZSysCertifyDB tLZSysCertifyDB = new LZSysCertifyDB();
        tLZSysCertifyDB.setCertifyCode("8888");
        tLZSysCertifyDB.setCertifyNo(this.mLOPRTManagerSchema.getPrtSeq());
        if (tLZSysCertifyDB.getInfo())
        {
            return true;
        }
        String sendInsrued = "";
        sendInsrued = "D" + tLCPENoticeSchema.getCustomerNo();
        String sendCom = "A" + tLCPENoticeSchema.getManageCom();
        LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
        tLZSysCertifySchema.setCertifyCode("8888");
        tLZSysCertifySchema.setCertifyNo(this.mLOPRTManagerSchema.getPrtSeq());
        tLZSysCertifySchema.setSendOutCom(sendCom);
        tLZSysCertifySchema.setReceiveCom(sendInsrued);
        tLZSysCertifySchema.setHandler("SYS");
        tLZSysCertifySchema.setHandleDate(PubFun.getCurrentDate());
        tLZSysCertifySchema.setSendNo(PubFun1.CreateMaxNo("TAKEBACKNO", ""));
        tLZSysCertifySchema.setStateFlag("0");
        tLZSysCertifySchema.setMakeDate(PubFun.getCurrentDate());
        tLZSysCertifySchema.setMakeTime(PubFun.getCurrentTime());
        tLZSysCertifySchema.setModifyDate(PubFun.getCurrentDate());
        tLZSysCertifySchema.setModifyTime(PubFun.getCurrentTime());
        tLZSysCertifySchema.setOperator(this.mGlobalInput.Operator);
        this.map.put(tLZSysCertifySchema, "INSERT");
        return true;
    }
}
