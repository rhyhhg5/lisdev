package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LYReturnFromBankBDB;
import com.sinosoft.lis.db.LYReturnFromBankDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LYReturnFromBankBSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;

public class PrintBillYFBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;
    private GlobalInput mG = new GlobalInput();

    private String mBankName; //银行名称
    private String mErrorReason; //失败原因
    private String mChkSuccFlag; //银行校验成功标志；
    private String mChkFailFlag; //银行校验失败标志；
    public PrintBillYFBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData begin");
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        //strMngCom = (String) cInputData.get(2);
        mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        strMngCom = mG.ManageCom;
        System.out.println("在PrintBillBL.java中得到的批次号码是" + strBillNo);
        System.out.println("在PrintBillBL.java中得到的银行代码是" + strBankCode);
        System.out.println("在PrintBillBL.java中得到的管理机构是" + strMngCom);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PayNoticeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        System.out.println("查询银行名称的函数！！！");
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(strBankCode);
        LDBankSet tLDBankSet = new LDBankSet();
        tLDBankSet.set(tLDBankDB.query());
        int bank_count = tLDBankSet.size();
        if (bank_count == 0)
        {
            this.mErrors.copyAllErrors(tLDBankDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintBillBL";
            tError.functionName = "submitData";
            tError.errorMessage = "银行信息表查询失败，没有该银行代码！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LDBankSchema tLDBankSchema = new LDBankSchema();
        tLDBankSchema.setSchema(tLDBankSet.get(1));
        mBankName = tLDBankSchema.getBankName();
        System.out.println("打印出银行信息和批次号码！！！");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        System.out.println("对银行返回盘记录表的查询");
        LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
        LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
        String y_sql = "select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode = '"
                       + strBankCode.trim()
                       + "' and ComCode like '"
                       + strMngCom.trim()
                       + "%' "
                       +" union select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode in (select bankcode from ldbankunite where bankunitecode = '"
                       + strBankCode.trim()
                       + "') and ComCode like '"
                       + strMngCom.trim()
                       + "%'";
        tLYReturnFromBankSet = tLYReturnFromBankDB.executeQuery(y_sql);
        System.out.println("在返回盘中所执行的操作是" + y_sql);
        int n = tLYReturnFromBankSet.size();
        if (n == 0)
        {
            //定义总金额和总笔数的变量
            double SumMoney_b = 0;
            int SumCount_b = 0;
            System.out.println("该次交易可能被核销，在银行返回盘记录备份表中进行查询");
            LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
            LYReturnFromBankBSet tLYReturnFromBankBSet = new
                    LYReturnFromBankBSet();
            String t_sql = "select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and  BankCode = '"
                           + strBankCode.trim()
                           + "' and ComCode like '"
                           + strMngCom.trim()
                           + "%' "
                           +"union select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and BankCode in (select bankcode from ldbankunite where bankunitecode = '"
                           + strBankCode.trim()
                           + "') and ComCode like '"
                           + strMngCom.trim()
                           + "%'";
            tLYReturnFromBankBSet = tLYReturnFromBankBDB.executeQuery(t_sql);
            System.out.println("在备份表中所执行的查询语句是" + t_sql);
            int m = tLYReturnFromBankBSet.size();
            if (m == 0)
            {
                this.mErrors.copyAllErrors(tLYReturnFromBankBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有满足条件的代付错误清单的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
            int b_count;
            b_count = 1;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintYFBill.vts", "printer");
            ListTable alistTable = new ListTable();
            alistTable.setName("INFO");
            for (int j = 1; j <= m; j++)
            {
                System.out.println("在银行返回盘记录备份表的处理中此时的J的数值是￥￥￥￥" + j);
                String[] cols = new String[13];
                LYReturnFromBankBSchema tLYReturnFromBankBSchema = new
                        LYReturnFromBankBSchema();
                tLYReturnFromBankBSchema.setSchema(tLYReturnFromBankBSet.get(j));
                String tbPayCode = tLYReturnFromBankBSchema.getPayCode();
                String b_BankSuccFlag;
                if (tLYReturnFromBankBSchema.getBankSuccFlag() == null ||
                    tLYReturnFromBankBSchema.getBankSuccFlag().equals(""))
                {
                    b_BankSuccFlag = "1";
                }
                else
                {
                    b_BankSuccFlag = tLYReturnFromBankBSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + b_BankSuccFlag);
                System.out.println("采用了胡博的方法");
                String hq_flag_b = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag_b);
                boolean jy_flag_b = verifyBankSuccFlag(hq_flag_b,
                        b_BankSuccFlag);

                System.out.println("校验后的成功标志是" + jy_flag_b);
                if (jy_flag_b)
                {
                    continue;
                }

                b_count++;
                SumCount_b++;
                LDCode1DB tLDCode1DB = new LDCode1DB();
                tLDCode1DB.setCodeType("bankerror");
                tLDCode1DB.setCode(strBankCode);
                tLDCode1DB.setCode1(b_BankSuccFlag);
                LDCode1Set tLDCode1Set = new LDCode1Set();
                LDCode1Schema tLDCode1Schema = new LDCode1Schema();
                tLDCode1Set.set(tLDCode1DB.query());
                int k = tLDCode1Set.size();
                String b_errorReason;
                if (k == 0)
                {
                    b_errorReason = "没有指明错误原因";
                }
                else
                {
                    tLDCode1Schema.setSchema(tLDCode1Set.get(1));
                    b_errorReason = tLDCode1Schema.getCodeName();
                }
                cols[0] = tLYReturnFromBankBSchema.getAgentCode();
                cols[3] = tLYReturnFromBankBSchema.getAccNo();
                cols[4] = tLYReturnFromBankBSchema.getAccName();
                cols[6] = b_errorReason;
                cols[9] = tLYReturnFromBankBSchema.getPolNo();
                cols[11] = String.valueOf(tLYReturnFromBankBSchema.getPayMoney());
                cols[12]=getManagecom(cols[0]);
                //******************************************
                SumMoney_b = SumMoney_b + tLYReturnFromBankBSchema.getPayMoney();
                System.out.println("失败原因是" + tLDCode1Schema.getCodeName());
                System.out.println("银行的账号是" + tLYReturnFromBankBSchema.getAccNo());
                System.out.println("银行的账号名是" +
                                   tLYReturnFromBankBSchema.getAccName());
                System.out.println("代理人编码是" + cols[0]);
                System.out.print("当J的值是" + j + "时，银行的账号是" + cols[3]);
                System.out.println("当J的值是" + j + "时，银行的账号名是" + cols[4]);
                System.out.println("当J的值是" + j + "时，错误原因是" + cols[6]);

                ExeSQL exesql = new ExeSQL();
                if (!StrTool.cTrim((cols[0])).equals("") )
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";

                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                        
                        //将AgentCode转换为GroupAgentCode
    			        //统一工号修改
    			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
    			        ExeSQL aExeSQL = new ExeSQL();
    			        SSRS aSSRS = new SSRS();
    			        aSSRS = aExeSQL.execSQL(SQL);
    				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
    				  		cols[0] = aSSRS.GetText(1, 1);
    				  	}
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                    }
                }

                LJAGetDB tLJAGetDB = new LJAGetDB();
                tLJAGetDB.setActuGetNo(tbPayCode);


                //tLJAGetDB.setBankCode(strBankCode);
                LJAGetSet tLJAGetSet = new LJAGetSet();
                tLJAGetSet.set(tLJAGetDB.query());
                int a = tLJAGetSet.size();
                if (a != 0)
                {
                    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                    tLJAGetSchema.setSchema(tLJAGetSet.get(1));
                    cols[5] = String.valueOf(tLJAGetSchema.getSendBankCount());
                    System.out.println("总失败次数是" +
                                       tLJAGetSchema.getSendBankCount());
                }
                //对投保人和投保人的联系方式进行附值
                //查询出投保人的客户号码
                String AppntNo_sql = "";
                String t_NoType = tLYReturnFromBankBSchema.getNoType();
                if (!(t_NoType == null || t_NoType.equals("")))
                //if(!(t_NoType.equals("")||t_NoType==null))
                {
                    if (t_NoType.equals("0"))
                    {
                        AppntNo_sql = "select '000000' from dual fetch first 1 rows only";
                    }
                    System.out.println("2003-11-06标志是" + t_NoType);
                    if (t_NoType.equals("1"))
                    {
                        System.out.println("NoType==1时代表集体保单号码");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where GrpPolNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("2"))
                    {
                        System.out.println("NoType==2时代表的是个人保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PolNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    //          if(t_NoType.equals("3"))
                    //          {
                    //            System.out.println("NoType==3时代表的是个人批改号");
                    //          }
                    if (t_NoType.equals("4"))
                    {
                        System.out.println("NoType==4时代表的是合同投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ContNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";

                    }
                    //          if(t_NoType.equals("5"))
                    //          {
                    //            System.out.println("NoType==5时代表的是集体投保单号");
                    //
                    //          }
                    if (t_NoType.equals("6"))
                    {
                        System.out.println("NoType==6时代表的是个人投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ProposalNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    //        if(t_NoType.equals("7"))
                    //        {
                    //          System.out.println("NoType==7时代表的是表示合同印刷号");
                    //        }
                    //        if(t_NoType.equals("8"))
                    //        {
                    //          System.out.println("NoType==8时代表的是表示集体印刷号");
                    //        }
                    if (t_NoType.equals("9"))
                    {
                        System.out.println("NoType==9时代表的是表示个人印刷号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PrtNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }

//          else
//          {
//
//          }
                }

//        String AppntNo_sql = "Select AppntNo From LJAGet Where ActuGetNo = '"
//                         +tLYReturnFromBankBSchema.getPayCode()+"'";
                System.out.println("2003-11-07出错之前的信息");
                //System.out.println("AppntNo_sql :" +AppntNo_sql);
                SSRS AppntNo_ssrs = exesql.execSQL(AppntNo_sql);
                System.out.println("2003-11-07查询到的结果集是" +
                                   AppntNo_ssrs.getMaxRow());
                //  System.out.println("备份表中的投保人客户号是"+AppntNo_ssrs.GetText(1,1));
                //查询出投保人的名称和联系方式

                String AppntName_sql =
                        "select a.appntname,b.mobile from lcappnt a ,lcaddress b where a.addressno=b.addressno and a.appntno= '"
                        + AppntNo_ssrs.GetText(1, 1) + "'";

                SSRS AppntName_ssrs = exesql.execSQL(AppntName_sql);

                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[7] = "无";
                }
                else
                {
                    cols[7] = AppntName_ssrs.GetText(1, 1);
                    System.out.println("投保人的信息如下：投保人的姓名是" +
                                       AppntName_ssrs.GetText(1, 1));
                }
                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[8] = "无";
                }
                else
                {
                    cols[8] = AppntName_ssrs.GetText(1, 2);
                    System.out.println("投保人的电话是" + AppntName_ssrs.GetText(1, 2));
                }

                     String Type_sql =
                            "Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
                            + tbPayCode + "'";
                    SSRS Type_ssrs = exesql.execSQL(Type_sql);
                    cols[10] = Type_ssrs.GetText(1, 1);
                    for (int i=0; i<cols.length; i++) {
                    	if (cols[i] == null) {
                    
                    	cols[i] = "";
                    	
                    	}
                    }

                alistTable.add(cols);
            }
            String[] col = new String[12];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("Date", PubFun.getCurrentDate());
            texttag.add("SumMoney", SumMoney_b);
            texttag.add("SumCount", SumCount_b);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }

            //xmlexport.outputDocumentToFile("e:\\","PrintYFBill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);

            System.out.println("￥￥￥￥￥￥b_count的值是====" + b_count);
            if (b_count == 1)
            {
                this.mErrors.copyAllErrors(tLDBankDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有银行返回代扣失败清单记录！！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            //定义记录总金额和总笔数的变量
            double SumMoney = 0;
            int SumCount = 0;
            int _count;
            _count = 1;

            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintYFBill.vts", "printer");
            ListTable blistTable = new ListTable();
            blistTable.setName("INFO");
            for (int i = 1; i <= n; i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = new
                        LYReturnFromBankSchema();
                tLYReturnFromBankSchema.setSchema(tLYReturnFromBankSet.get(i));
                //记录账号名和账户
                String[] cols = new String[12];
                String mBankSuccFlag;
                String _BankSuccFlag;
                if (tLYReturnFromBankSchema.getBankSuccFlag().equals("") ||
                    tLYReturnFromBankSchema.getBankSuccFlag() == null)
                {
                    _BankSuccFlag = "1";
                }
                else
                {
                    _BankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                System.out.println("采用了胡博的方法后回盘表的数据");
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                String hq_flag = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag);
                boolean jy_flag = verifyBankSuccFlag(hq_flag, _BankSuccFlag);
                System.out.println("银行的校验标志是" + jy_flag);
                if (jy_flag)
                {
                    continue;
                }
                _count++;
                SumCount++;
                String tPayCode = tLYReturnFromBankSchema.getPayCode();
                mBankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                LDCode1DB mLDCode1DB = new LDCode1DB();
                mLDCode1DB.setCodeType("bankerror");
                mLDCode1DB.setCode(strBankCode);
                mLDCode1DB.setCode1(mBankSuccFlag);
                //mLDCode1DB.setCodeName(mBankSuccFlag);
                LDCode1Set mLDCode1Set = new LDCode1Set();
                LDCode1Schema mLDCode1Schema = new LDCode1Schema();
                mLDCode1Set.set(mLDCode1DB.query());
                int c = mLDCode1Set.size();
                String errorReason;
                if (c == 0)
                {
                    errorReason = "没有指明错误原因";
                }
                else
                {
                    mLDCode1Schema.setSchema(mLDCode1Set.get(1));
                    errorReason = mLDCode1Schema.getCodeName();
                    System.out.println("查询应收总表，查询出代理人信息");
                }
                System.out.println("失败原因是" + errorReason);
                cols[0] = tLYReturnFromBankSchema.getAgentCode();
                cols[3] = tLYReturnFromBankSchema.getAccNo();
                cols[4] = tLYReturnFromBankSchema.getAccName();
                cols[6] = errorReason;
                cols[9] = tLYReturnFromBankSchema.getPolNo();
                cols[11] = String.valueOf(tLYReturnFromBankSchema.getPayMoney());
                
                SumMoney = SumMoney + tLYReturnFromBankSchema.getPayMoney();
                if (!(cols[0].equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";
                    ExeSQL exesql = new ExeSQL();
                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                        
                        //将AgentCode转换为GroupAgentCode
    			        //统一工号修改
    			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
    			        ExeSQL aExeSQL = new ExeSQL();
    			        SSRS aSSRS = new SSRS();
    			        aSSRS = aExeSQL.execSQL(SQL);
    				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
    				  		cols[0] = aSSRS.GetText(1, 1);
    				  	}
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                    }
                }

                LJAGetDB tLJAGetDB = new LJAGetDB();
                tLJAGetDB.setActuGetNo(tPayCode);
                tLJAGetDB.setBankCode(strBankCode);
                LJAGetSet tLJAGetSet = new LJAGetSet();
                tLJAGetSet.set(tLJAGetDB.query());
                int a = tLJAGetSet.size();
                if (a != 0)
                {
                    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                    tLJAGetSchema.setSchema(tLJAGetSet.get(1));
                    cols[5] = String.valueOf(tLJAGetSchema.getSendBankCount());
                    System.out.println("总失败次数是" +
                                       tLJAGetSchema.getSendBankCount());
                }
                //对投保人和投保人的联系方式进行附值
                //查询出投保人的客户号码
                ExeSQL exesql = new ExeSQL();
                String AppntNo_sql = "";
                String t_NoType = tLYReturnFromBankSchema.getNoType();
                if (!(t_NoType.equals("") || t_NoType == null))
                {
                    if (t_NoType.equals("0"))
                    {
                        AppntNo_sql = "select '000000' from dual ";
                    }
                    if (t_NoType.equals("1"))
                    {
                        System.out.println("NoType==1时代表集体保单号码");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where GrpPolNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("2"))
                    {
                        System.out.println("NoType==2时代表的是个人保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PolNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("4"))
                    {
                        System.out.println("NoType==4时代表的是合同投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ContNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("6"))
                    {
                        System.out.println("NoType==6时代表的是个人投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ProposalNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("9"))
                    {
                        System.out.println("NoType==9时代表的是表示个人印刷号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PrtNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }

                }

                SSRS AppntNo_ssrs = exesql.execSQL(AppntNo_sql);
                System.out.println("备份表中的投保人客户号是" + AppntNo_ssrs.GetText(1, 1));
                //查询出投保人的名称和联系方式
                String AppntName_sql =
                        "select a.appntname,b.mobile from lcappnt a ,lcaddress b where a.addressno=b.addressno and a.appntno= '"
                        + AppntNo_ssrs.GetText(1, 1) + "'";
                SSRS AppntName_ssrs = exesql.execSQL(AppntName_sql);
                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[7] = "无";
                }
                else
                {
                    cols[7] = AppntName_ssrs.GetText(1, 1);
                    System.out.println("投保人的信息如下：投保人的姓名是" +
                                       AppntName_ssrs.GetText(1, 1));
                }
                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[8] = "无";
                }
                else
                {
                    cols[8] = AppntName_ssrs.GetText(1, 2);
                    System.out.println("投保人的名称是" + AppntName_ssrs.GetText(1, 2));
                }

                    String Type_sql =
                            "Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
                            + tPayCode + "'";
                    SSRS Type_ssrs = exesql.execSQL(Type_sql);
                    cols[10] = Type_ssrs.GetText(1, 1);
                    for (int j=0; j<cols.length; j++) {
                    	if (cols[j] == null) {
                    
                    	cols[j] = "";
                    	
                    	}
                    }
                blistTable.add(cols);
                
                System.out.println("getPrintData end");
            }
            String[] b_col = new String[12];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(blistTable, b_col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("Date", PubFun.getCurrentDate());
            texttag.add("SumMoney", SumMoney);
            texttag.add("SumCount", SumCount);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
        // xmlexport.outputDocumentToFile("e:\\","PrintYFBill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
            System.out.println("getPrintData begin");
        }
        //对变量进行附值
        return true;
    }

    //获取银行的成功标志
    public String getBankSuccFlag(String tBankCode)
    {
        try
        {
            LDBankSchema tLDBankSchema = new LDBankSchema();

            tLDBankSchema.setBankCode(tBankCode);
            tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));

            return tLDBankSchema.getAgentGetSuccFlag();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new NullPointerException("获取银行扣款成功标志信息失败！(getBankSuccFlag) " +
                                           e.getMessage());
        }
    }

//校验成功标志的方法
    public boolean verifyBankSuccFlag(String bankSuccFlag, String bBankSuccFlag)
    {
        int i;
        boolean passFlag = false;

        String[] arrSucc = PubFun.split(bankSuccFlag, ";");
        String tSucc = bBankSuccFlag;

        for (i = 0; i < arrSucc.length; i++)
        {
            if (arrSucc[i].equals(tSucc))
            {
                passFlag = true;
                break;
            }
        }

        return passFlag;
    }
    public String getManagecom(String agentcode){
    	String Msql="select managecom from laagent where agentcode='"+agentcode+"'";
    	ExeSQL exesql = new ExeSQL();
    	SSRS Type_ssrs = exesql.execSQL(Msql);
        String managecom = Type_ssrs.GetText(1, 1);
        System.out.println("aaaaaaaaaa"+Msql);
        System.out.println("bbbbbbbbbb"+managecom);
    	return managecom;
    }

    public static void main(String[] args)
    {
    }
}
