package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

public class LAMonthPrizeReportBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得前台传入结束日期
    private String mEndMonth = "";
 //取得前台传入起始日期
    private String mStartMonth = "";
    
    private String startDate  ="";
    private String EndDate = "";
    private SSRS managSSRS = new SSRS();//管理机构容器
    private String branchattr = "";//所在的机构外部编码
	private String name = "";//所在机构的名称
	private String AgentGroup = "";
    //业务处理相关变量
    /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private String  mdate = PubFun.getCurrentDate();
   private String  mtime = PubFun.getCurrentTime();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
        	return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
  public boolean checkData()
   {
	  //判断时间间隔是否大于12
	  String startDate2 = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
	  String endDate2 = this.mEndMonth.substring(0,4)+"-"+this.mEndMonth.substring(4,6)+"-01";
	 System.out.println("startDate2"+startDate2);
	 System.out.println("endDate2"+endDate2);
	  int inteval = PubFun.calInterval(startDate2, endDate2, "M");
	  System.out.println("inteval"+inteval);
	  if(inteval>12||inteval<0)
	  {
		  CError.buildErr(this, "统计时间间隔不能大于12！");
		  return false;
	  }
	  return true;
	}
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mAgentCode = (String)cInputData.get(2);
    	mStartMonth = (String)cInputData.get(3);
    	mEndMonth = (String)cInputData.get(4);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        
        ListTable tListTable = new ListTable();
        ListTable allListTable = new ListTable();
        tListTable.setName("LMP");
        allListTable.setName("ALL");
        System.out.println("mAgentCode"+mAgentCode);
        System.out.println("mBranchAttr"+mBranchAttr);
        String startDate2 = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
  	    String endDate2 = this.mEndMonth.substring(0,4)+"-"+this.mEndMonth.substring(4,6)+"-01";
  	    int inteval = PubFun.calInterval(startDate2, endDate2, "M")+1;
        int n = 1;
       if((this.mAgentCode==null||this.mAgentCode.equals(""))&&
    		  (this.mBranchAttr==null||this.mBranchAttr.equals("")))
       {
    	   String sql = "";
          //判断是否有输入机构，输入机构按机构查询，未输入按总公司查询
    	   if(this.mManageCom==null||this.mManageCom.equals(""))
    	   {
    		   sql = "select * from ldcom where comcode like '86%' and length(trim(comcode))=8 order by comcode";
    	   }
    	   else
    		   sql ="select * from ldcom where comcode like '"+this.mManageCom+"%' and length(trim(comcode))=8 order by comcode";
    	   System.out.println(sql);
    	   LDComDB tLDComDB = new LDComDB();
    	   LDComSet tLDComSet = new LDComSet();
    	   tLDComSet = tLDComDB.executeQuery(sql);
    	   //机构循环
    	   if(tLDComSet.size()>=1)
    	   {    		   
    		   //查询机构下的所有销售机构		  
    		   for(int i=1;i<=tLDComSet.size();i++)
    		   {
    			String jgSQL = "";
    			//中介、公司业务不包括在内
    			jgSQL = "select * from labranchgroup where managecom like '"+tLDComSet.get(i).getComCode()+"%' and (state is null or state='0') " +
    					" and branchlevel<>'31' and branchtype='2' and branchtype2='01'";
    			System.out.println(jgSQL);
    			LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    			LABranchGroupSet tLABranchGroupSet =new LABranchGroupSet();
    			tLABranchGroupSet = tLABranchGroupDB.executeQuery(jgSQL);
    			if(tLABranchGroupSet.size()>=1)
    			{   			
    				for(int j=1;j<=tLABranchGroupSet.size();j++)
    				{
    					//获取地区类型
    				   				
    					String personSQL = "SELECT * from latree where  agentgroup='"+tLABranchGroupSet.get(j).getAgentGroup()+"'" +
    							" and agentcode in (select agentcode from lawage where indexcalno >= '"+mStartMonth+"' and indexcalno<='"+mEndMonth+"' and branchtype='2' and branchtype2='01') " ;    									
    				    System.out.println(personSQL);
    					LATreeDB tLATreeDB = new LATreeDB();
    				   LATreeSet tLATreeSet = new LATreeSet();
    				   tLATreeSet = tLATreeDB.executeQuery(personSQL);
    				   //进行人员循环
    				   if(tLATreeSet.size()>=1)
    				   {  
    					  
    					   for(int k=1;k<=tLATreeSet.size();k++)
    					   {
    						   ExeSQL tExeSQL = new ExeSQL();
    						   //个人总的提奖
//  							  String perAllSQL = "select sum(K20) from lawage where  agentcode='"+tLATreeSet.get(k).getAgentCode()+"'" +
// 							   		" and indexcalno>='"+this.mStartMonth+"' and indexcalno<='"+this.mEndMonth+"'";
//  							  
//  							  String  perAllprize = tExeSQL.getOneValue(perAllSQL);  	
//  						
  							 //查询代理人姓名
    						   String AgentInfo = "select name from laagent where agentcode='"+tLATreeSet.get(k).getAgentCode()+"'";   					   
    						   String name = tExeSQL.getOneValue(AgentInfo);
//    						 查询统一工号： 2014-11-21  解青青
    						   ExeSQL tttExeSQL = new ExeSQL();
 							  String tttGAC = "select groupagentcode from laagent where agentcode='"+tLATreeSet.get(k).getAgentCode()+"'";
 							  String tttGroupAgentCode = tttExeSQL.getOneValue(tttGAC);
    						   //根据统计日期进行循环
    						   
    							   String prizeArray[] = new String[inteval];
    							  for(int b=0;b<inteval;b++)
    							  { 					  
    								  //每月的提奖
    								  String startDate1 = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
    								System.out.println("startDate1"+startDate1);
    								  String indexcalnoMonth = String.valueOf(PubFun.calDate(startDate1, b, "M", ""));
//    								  String indexcalno1 = indexcalnoMonth.replace("-", "");
//    								  String indexcalno = indexcalno1.substring(0, 6);
    								  String indexcalno = AgentPubFun.formatDate(indexcalnoMonth, "yyyyMM");
    								  System.out.println("indexcalno"+indexcalno);
    								  
    								  String prizeSQL = "select K20 from lawage where  agentcode='"+tLATreeSet.get(k).getAgentCode()+"'" +
  							   		" and indexcalno='"+indexcalno+"'";    					  
    								  String prize = tExeSQL.getOneValue(prizeSQL);
    								  prizeArray[b] = prize;
    							  }		
    							  //在lawage表中循环查找期末职级
    							 
    							  String lastgrade = "";
    						  lastgrade = getLastGrade(tLATreeSet.get(k).getAgentCode(),this.mEndMonth);
   						      System.out.println("~~~~~~~~~~~~~");	      
   						      System.out.println();
						      String Info[] = new String[9+inteval];   
						      Info[0] = String.valueOf(n);
						      Info[1] = tLDComSet.get(i).getName();
						      Info[2] = tLDComSet.get(i).getComCode();
						      Info[3] = tLABranchGroupSet.get(j).getName();
						      Info[4] = tLABranchGroupSet.get(j).getBranchAttr();
						      Info[5] = name;
//						      Info[6] = tLATreeSet.get(k).getAgentCode();
						      Info[6] = tttGroupAgentCode;
						      Info[7] = lastgrade;
						      double perAllprize = 0.00;
						      for(int c = 0;c<inteval;c++)
						      { 
						    	  System.out.println(prizeArray[c]);
						    	  Info[8+c] = prizeArray[c] ;
						    	  if(prizeArray[c]==null||prizeArray[c].equals(""))
						    	  {
						    		  prizeArray[c] = String.valueOf(0);
						    	  }
						    	  perAllprize = Arith.add(perAllprize, Double.parseDouble(prizeArray[c]));
						    	  perAllprize = Arith.round(perAllprize, 2);
						    	  System.out.println("perAllprize"+perAllprize);
						      }
						     
						      Info[8+inteval] = String.valueOf(Arith.round(perAllprize, 2));
						      //tListTable.
						      tListTable.add(Info); 
					    	  n++;
					    	  System.out.println("n:"+n);
					    	  
    					    }				
    					   
    					  }
    				   
    			       }
    		       }
    	         }
    		 
              } 	   
    	   }
       else
       {
    	   String treeSQL  = "";
    	   String branchAttr = "";
    	   
    	   SSRS tSSRS = new SSRS();
    	   if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
    	   {
    		//查询机构里的人员
    		   String agentGroupSQL = "select name,branchattr, agentgroup from labranchgroup where branchattr='"+this.mBranchAttr+"' and branchtype='2' and branchtype2='01' and endflag='N'";
    		   ExeSQL tExeSQL = new ExeSQL();    		   
    		   tSSRS = tExeSQL.execSQL(agentGroupSQL);    		  
    		   treeSQL = "SELECT * from latree where  agentgroup='"+tSSRS.GetText(1, 3)+"'" +
				" and agentcode in (select agentcode from lawage where indexcalno >= '"+mStartMonth+"' and indexcalno<='"+mEndMonth+"' and branchtype='2' and branchtype2='01') " ;
    	      
    	      
    	   }
    	   if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
    	   {
    		   treeSQL = "select * from latree where agentcode='"+this.mAgentCode+"'";
    		   //查询人员的机构信息
    		   String branchSQL = "select name,branchattr from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"+this.mAgentCode+"' and branchtype='2' and branchtype2='01')";
    		   ExeSQL tmExeSQL = new ExeSQL();  
    		   tSSRS = tmExeSQL.execSQL(branchSQL);
    		   
    	   }
    	   LATreeDB tLATreeDB = new LATreeDB();
    	   LATreeSet tLATreeSet = new LATreeSet();
    	   tLATreeSet = tLATreeDB.executeQuery(treeSQL);
    	   if(tLATreeSet.size()>0)
    	   {
	    	   for(int j=1;j<=tLATreeSet.size();j++)
	    	   {
	    		   ExeSQL tExeSQL = new ExeSQL();
				   //个人总的提奖
					  String perAllSQL = "select sum(K20) from lawage where  agentcode='"+tLATreeSet.get(j).getAgentCode()+"'" +
					   		" and indexcalno>='"+this.mStartMonth+"' and indexcalno<='"+this.mEndMonth+"'";
					  
					  String  perAllprize = tExeSQL.getOneValue(perAllSQL);  	
				
					 //查询代理人姓名
				   String AgentInfo = "select name from laagent where agentcode='"+tLATreeSet.get(j).getAgentCode()+"'";   					   
				   String name = tExeSQL.getOneValue(AgentInfo);
				   //查询统一工号： 2014-11-21  解青青
				   ExeSQL tttExeSQL = new ExeSQL();
					  String tttGAC = "select groupagentcode from laagent where agentcode='"+tLATreeSet.get(j).getAgentCode()+"'";
					  String tttGroupAgentCode = tttExeSQL.getOneValue(tttGAC);
				   //根据统计日期进行循环
					   String prizeArray[] = new String[inteval];
					  for(int b=0;b<inteval;b++)
					  { 
						  
						  //每月的提奖
						  String startDate1 = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
						//  String endDate1 = this.EndDate.substring(0,4)+"-"+this.EndDate.substring(4,6)+"-01";
						  String indexcalnoMonth = String.valueOf(PubFun.calDate(startDate1, b, "M", ""));
//						  String indexcalno1 = indexcalnoMonth.replace("-", "");
//						  String indexcalno = indexcalno1.substring(0, 6);
						  String indexcalno = AgentPubFun.formatDate(indexcalnoMonth, "yyyyMM");
						  System.out.println("indexcalno"+indexcalno);
						  String prizeSQL = "select K20 from lawage where  agentcode='"+tLATreeSet.get(j).getAgentCode()+"'" +
					   		" and indexcalno='"+indexcalno+"'";
						  
						  String prize = tExeSQL.getOneValue(prizeSQL);
						  prizeArray[b] = prize;
						  
					  }						   
				      System.out.println("~~~~~~~~~~~~~");	  
				      
				   //获取管理机构的名称
				      String nameSQL = "select name from ldcom where comcode='"+tLATreeSet.get(j).getManageCom()+"'";
				      ExeSQL nameExeSQL = new ExeSQL();
				      String managename = nameExeSQL.getOneValue(nameSQL);
				      String lastgrade = "";
				      lastgrade = getLastGrade(tLATreeSet.get(j).getAgentCode(),this.mEndMonth);
			      String Info[] = new String[9+inteval];   
			      Info[0] = String.valueOf(n);
			      Info[1] = managename;
			      Info[2] = tLATreeSet.get(j).getManageCom();
			      Info[3] = tSSRS.GetText(1, 1);
			      Info[4] = tSSRS.GetText(1, 2);
			      Info[5] = name;
			      Info[6] = tttGroupAgentCode;
//			      Info[6] = tLATreeSet.get(j).getAgentCode();
			      Info[7] = lastgrade;
			      for(int c = 0;c<inteval;c++)
			      { 
			    	  System.out.println(prizeArray[c]);
			    	  Info[8+c] = prizeArray[c];
			      }
			      Info[8+inteval] = perAllprize;
			      tListTable.add(Info); 
		    	  n++;
		    	  System.out.println("n:"+n); 
	    	   }
    	   }
       }       
       if(tListTable==null||tListTable.equals(""))
       {
    	   CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }
       //求合计
	   String allprize[] = new String[inteval];
	//   double all[] = new double[inteval];
	   for(int b=0;b<inteval;b++)
		  { 
		     ExeSQL tExeSQL = new ExeSQL();
		     String prizeSQL = "";
			  //每月的提奖总和
		     String startDate1 = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
			 // String endDate1 = this.EndDate.substring(0,4)+"-"+this.EndDate.substring(4,6)+"-01";
			  String indexcalnoMonth = String.valueOf(PubFun.calDate(startDate1, b, "M", ""));
//			  String indexcalno1 = indexcalnoMonth.replace("-", "");
//			  String indexcalno = indexcalno1.substring(0, 6);
			  String indexcalno = AgentPubFun.formatDate(indexcalnoMonth, "yyyyMM");
			  System.out.println("indexcalno"+indexcalno);
			  if(this.mManageCom!=null&&!this.mManageCom.equals(""))
			  {
			   prizeSQL = "select sum(K20) from lawage where  indexcalno='"+indexcalno+"' and branchtype='2' and branchtype2='01' and managecom like '"+this.mManageCom+"%'" +
			   		" and agentgroup in (select agentgroup from labranchgroup where branchlevel<>'31')";		
			  }
			   if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
			  {
				  prizeSQL =  "select sum(K20) from lawage where  indexcalno='"+indexcalno+"' and branchattr='"+this.mBranchAttr+"' and branchtype='2' and branchtype2='01'";	
			  }
			  if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
			  {
				  prizeSQL =  "select sum(K20) from lawage where  indexcalno='"+indexcalno+"'  and agentcode='"+this.mAgentCode+"' and branchtype='2' and branchtype2='01'";
			  }
			  if((this.mManageCom==null||this.mManageCom.equals(""))&&
					  (this.mBranchAttr==null||this.mBranchAttr.equals(""))
					  &&(this.mAgentCode==null||this.mAgentCode.equals("")))
			  {
				  prizeSQL = "select sum(K20) from lawage where  indexcalno='"+indexcalno+"' and branchtype='2' and branchtype2='01' and agentgroup in (select agentgroup from labranchgroup where branchlevel<>'31')";
			  }
		      String prize = tExeSQL.getOneValue(prizeSQL);
			  System.out.println("prize"+prize);
			 if(prize==null||prize.equals(""))
				 prize = "0";
			  allprize[b] = prize;
		  }			
	     String allInfo[] = new String[9+inteval];   
	     allInfo[0] = "合计";
	     allInfo[1] = " ";
         allInfo[2]=" ";
         allInfo[3]=" ";
         allInfo[4] =" ";
	     if(this.mManageCom!=null&&!this.mManageCom.equals(""))
	     {
	    	 allInfo[1] = getName(mManageCom);
	         allInfo[2]=this.mManageCom;
	         allInfo[3]=" ";
	         allInfo[4] =" ";
	     }
	     if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
	     {
	    	 allInfo[1] = getName(getManage(mBranchAttr));
	    	 allInfo[2] = getManage(mBranchAttr);
	    	 allInfo[3]=getBranchName(mBranchAttr,"2","01");
		     allInfo[4]=mBranchAttr;
	     }
	     if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
	     {
	    	 String Branchattr = AgentPubFun.getAgentBranchAttr(mAgentCode);
	    	 allInfo[1] = getName(getManage(Branchattr));
	    	 allInfo[2] = getManage(Branchattr);
	    	 allInfo[3]=getBranchName(Branchattr,"2","01");
		     allInfo[4]=Branchattr;
	     }	  
	     allInfo[5] = String.valueOf(n-1);
	     allInfo[6]=" ";
	     allInfo[7]=" ";
	     double sum = 0.0;
	     for(int m = 0;m<inteval;m++)
	      { 
	    	  System.out.println(allprize[m]);
	    	  allInfo[8+m] = allprize[m] ;	    
	    	  if(allprize[m]==null||allprize[m].equals(""))
	    	  {
	    		  allprize[m]=String.valueOf(0); 	
	    	  }
	    	  sum = Arith.add(sum, Double.parseDouble(allprize[m]));
	      }	
	     System.out.println("sum"+Arith.round(sum, 2));
	     allInfo[8+inteval]= String.valueOf(Arith.round(sum, 2));
	     allListTable.add(allInfo);
	     
       TextTag texttag = new TextTag(); //新建一个TextTag的实例
       XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
      //据时间间隔找模版
       String ymstartDate = this.mStartMonth.substring(0,4)+"-"+this.mStartMonth.substring(4,6)+"-01";
       if(inteval==1)
       {
    	   txmlexport.createDocument("LAMonthprize1.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", mStartMonth.substring(4,6));
       }
       if(inteval==2)
       {
    	   txmlexport.createDocument("LAMonthprize2.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5,7));
       }
       if(inteval==3)
       {
    	   txmlexport.createDocument("LAMonthprize3.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","",""};
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5,7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5,7));
       }
       if(inteval==4)
       {
    	   txmlexport.createDocument("LAMonthprize4.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
       }
       if(inteval==5)
       {
    	   txmlexport.createDocument("LAMonthprize5.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5,7));
       }
       if(inteval==6)
       {
    	   txmlexport.createDocument("LAMonthprize6.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
       }
       if(inteval==7)
       {
    	   txmlexport.createDocument("LAMonthprize7.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
       }
       if(inteval==8)
       {
    	   txmlexport.createDocument("LAMonthprize8.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
    	   texttag.add("Month8", String.valueOf(PubFun.calDate(ymstartDate, 7, "M","")).substring(5, 7));
       }
       if(inteval==9)
       {
    	   txmlexport.createDocument("LAMonthprize9.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
    	   texttag.add("Month8", String.valueOf(PubFun.calDate(ymstartDate, 7, "M","")).substring(5, 7));
    	   texttag.add("Month9", String.valueOf(PubFun.calDate(ymstartDate, 8, "M","")).substring(5, 7));
       }
       if(inteval==10)
       {
    	   txmlexport.createDocument("LAMonthprize10.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
    	   texttag.add("Month8", String.valueOf(PubFun.calDate(ymstartDate, 7, "M","")).substring(5, 7));
    	   texttag.add("Month9", String.valueOf(PubFun.calDate(ymstartDate, 8, "M","")).substring(5, 7));
    	   texttag.add("Month10", String.valueOf(PubFun.calDate(ymstartDate,9, "M","")).substring(5, 7));
       }
       if(inteval==11)
       {
    	   txmlexport.createDocument("LAMonthprize11.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate,1, "M","")).substring(5, 7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate,2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
    	   texttag.add("Month8", String.valueOf(PubFun.calDate(ymstartDate, 7, "M","")).substring(5, 7));
    	   texttag.add("Month9", String.valueOf(PubFun.calDate(ymstartDate, 8, "M","")).substring(5, 7));
    	   texttag.add("Month10", String.valueOf(PubFun.calDate(ymstartDate, 9, "M","")).substring(5, 7));
    	   texttag.add("Month11", String.valueOf(PubFun.calDate(ymstartDate, 10, "M","")).substring(5, 7));
       }
       if(inteval==12)
       {
    	   txmlexport.createDocument("LAMonthprize12.vts", "printer"); //最好紧接着就初始化xml文档
    	   String[] title = {"","","","","","","","","","","","","","","","","","","","",""}; 
    	   txmlexport.addListTable(tListTable,title);
    	   txmlexport.addListTable(allListTable,title);
    	   texttag.add("Month1", this.mStartMonth.substring(4,6));
    	   texttag.add("Month2", String.valueOf(PubFun.calDate(ymstartDate, 1, "M","")).substring(5,7));
    	   texttag.add("Month3", String.valueOf(PubFun.calDate(ymstartDate, 2, "M","")).substring(5, 7));
    	   texttag.add("Month4", String.valueOf(PubFun.calDate(ymstartDate, 3, "M","")).substring(5, 7));
    	   texttag.add("Month5", String.valueOf(PubFun.calDate(ymstartDate, 4, "M","")).substring(5, 7));
    	   texttag.add("Month6", String.valueOf(PubFun.calDate(ymstartDate, 5, "M","")).substring(5, 7));
    	   texttag.add("Month7", String.valueOf(PubFun.calDate(ymstartDate, 6, "M","")).substring(5, 7));
    	   texttag.add("Month8", String.valueOf(PubFun.calDate(ymstartDate, 7, "M","")).substring(5, 7));
    	   texttag.add("Month9", String.valueOf(PubFun.calDate(ymstartDate, 8, "M","")).substring(5, 7));
    	   texttag.add("Month10", String.valueOf(PubFun.calDate(ymstartDate,9, "M","")).substring(5, 7));
    	   texttag.add("Month11", String.valueOf(PubFun.calDate(ymstartDate, 10, "M","")).substring(5, 7));
    	   texttag.add("Month12", String.valueOf(PubFun.calDate(ymstartDate, 11, "M","")).substring(5, 7));
       }
  //      String manageComName = getName(mManageCom);
         texttag.add("StartMonth", mStartMonth);  
         texttag.add("EndMonth", mEndMonth);
 //       texttag.add("ManageCom", manageComName);
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
    /**
     * 求代理人的期末职级
     * @param cAgentCode-代理人编码;cEndMonth-最后统计月份
     * @return
     */
    private String getLastGrade(String cAgentCode,String cEndMonth)
    {
     	 String lastgrade = "";
    	 String gradeSQL = "select agentgrade from lawage where indexcalno='"+cEndMonth+"' and agentcode='"+cAgentCode+"'"; 
    	 ExeSQL  tExeSQL = new ExeSQL();
    	 lastgrade = tExeSQL.getOneValue(gradeSQL);
    	 if(lastgrade==null||lastgrade.equals(""))
    	 {
    		 String endDate1 = cEndMonth.substring(0,4)+"-"+cEndMonth.substring(4,6)+"-01";		
       	     String indexcalnoMonth = String.valueOf(PubFun.calDate(endDate1,-1, "M", ""));
       		 String indexcalno = AgentPubFun.formatDate(indexcalnoMonth, "yyyyMM"); 
    		 lastgrade = getLastGrade(cAgentCode,indexcalno);
    	 }   	    	  
    	return lastgrade;
    }
   private String getName(String ComCode)
   {
	   String ComSQL = "select name from ldcom where comcode = '"+ComCode+"'";
	   ExeSQL mExeSQL = new ExeSQL();
	   String name = mExeSQL.getOneValue(ComSQL);
	   if(mExeSQL.mErrors.needDealError())
	   {
		   mErrors.copyAllErrors(mExeSQL.mErrors);
           return "";		   
	   }
	   return name;
   }
   /**
    * 查询销售机构的名称
    * @param cBranchattr 销售机构外部编码
    * @param cBranchType 
    * @param cBranchType2
    * @return String 销售机构的名称
    */
   private  String getBranchName(String cBranchattr,String cBranchType,String cBranchType2)
   {
   	String Name = "";
   	String nameSQL = "select name from labranchgroup where branchattr='"+cBranchattr+"'" +
   			" and branchtype='"+cBranchType+"' and branchtype2='"+cBranchType2+"'";
   	ExeSQL tExeSQL = new ExeSQL();
   	Name = tExeSQL.getOneValue(nameSQL);
   	if(Name==null||Name.equals(""))
   	{
   		return "";
   	}
   	else
     return Name;
   }
   /**
    * 根据销售机构获得管理机构
    */
   private String getManage(String Branchattr)
   {
	   String managecom = "";
	   String manageSQL = "select managecom from labranchgroup where branchattr='"+Branchattr+"'";
	   ExeSQL tmExeSQL = new ExeSQL();
	   managecom = tmExeSQL.getOneValue(manageSQL);
	   if(managecom==null||managecom.equals(""))
	   {
		   return "";
	   }
	   else
	  return managecom;
   }
  
}
