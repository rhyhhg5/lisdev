package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author z
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LOBonusGrpPolParmDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.ChangeCodetoName;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LOBonusGrpPolParmSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BonusNoticeGrpBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象
    public BonusNoticeGrpBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        try
        {
            if (!cOperate.equals("CONFIRM") &&
                !cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (cOperate.equals("CONFIRM"))
            {
                mResult.clear();
                // 准备所有要打印的数据
                getPrintData();
            }
            else if (cOperate.equals("PRINT"))
            {
                if (!saveData(cInputData))
                {
                    return false;
                }
            }
            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq("86110020040810002451");
        VData tVData = new VData();
        VData mResult = new VData();

        tVData.addElement(tG);
        tVData.addElement(tLOPRTManagerSchema);

        BonusNoticeGrpUI tBonusNoticeGrpUI = new BonusNoticeGrpUI();
        if (!tBonusNoticeGrpUI.submitData(tVData, "CONFIRM"))
        {

        }
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

//得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "RefuseAppF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

// 准备所有要打印的数据
    private void getPrintData() throws Exception
    {
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        xmlExport.createDocument("BonusNoticeGrp.vts", ""); //最好紧接着就初始化xml文档

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LOBonusGrpPolParmDB tLOBonusGrpPolParmDB = new LOBonusGrpPolParmDB();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        tLCGrpPolDB.setGrpPolNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCGrpPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
            throw new Exception("在获取团体保单信息时出错！");
        }
        LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolDB.getSchema();

        tLOBonusGrpPolParmDB.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        tLOBonusGrpPolParmDB.setFiscalYear(Integer.parseInt(StrTool.getYear()) -
                                           1);
        if (!tLOBonusGrpPolParmDB.getInfo())
        {
            mErrors.copyAllErrors(tLOBonusGrpPolParmDB.mErrors);
            throw new Exception("在获取团体保单红利参数表信息时出错！");
        }
        LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = tLOBonusGrpPolParmDB.
                getSchema();

        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            buildError("getprintData", "无效的打印状态");
        }
        else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
        {
            buildError("getprintData", "该打印请求不是在请求状态");
        }

        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
        }

        TextTag texttag = new TextTag();
        texttag.add("GrpPolNo", tLCGrpPolSchema.getGrpPolNo());
        texttag.add("GrpName", tLCGrpPolSchema.getGrpName());
        texttag.add("RiskCode",
                    ChangeCodetoName.getRiskName(tLCGrpPolSchema.getRiskCode()));
        texttag.add("CValiDate", tLCGrpPolSchema.getCValiDate());
        texttag.add("FiscalYear", tLOBonusGrpPolParmSchema.getFiscalYear());
        texttag.add("SGetDate", tLOBonusGrpPolParmSchema.getSGetDate());

        //计算上年结转
        double LastYearSumMoney = 0.0;
        LastYearSumMoney = calLastYearSumMoney(tLOBonusGrpPolParmSchema);
        texttag.add("LastYearSumMoney", LastYearSumMoney);
        //计算本年交费
        double CurrentYearFee = 0.0;
        CurrentYearFee = calCurrentYearFee(tLOBonusGrpPolParmSchema,
                                           tLCGrpPolSchema);
        texttag.add("CurrentYearFee", CurrentYearFee);
        texttag.add("ManageFeeRate", tLCGrpPolSchema.getManageFeeRate());
        texttag.add("ManageFee",
                    tLCGrpPolSchema.getManageFeeRate() *
                    tLCGrpPolSchema.getSumPrem());
        //计算上年度利息
        double InterestMoney = 0.0;
        InterestMoney = calInterestMoney(tLOBonusGrpPolParmSchema);
        texttag.add("InterestMoney", InterestMoney);
        texttag.add("BonuseMoney", tLOBonusGrpPolParmSchema.getSumBonus());
        //求有效个人帐户资金余额
        double SumPolLeaving = 0.0;
        SumPolLeaving = calSumPolLeaving(tLOBonusGrpPolParmSchema);
        texttag.add("SumPolLeaving", SumPolLeaving);
        System.out.println("求有效个人帐户资金余额:" + SumPolLeaving);
        //求有效个人帐户资金余额-个人交，集体交
        double PolFee = 0.0;
        double GrpPolFee = 0.0;
        PolFee = calPolFee(tLOBonusGrpPolParmSchema);
        GrpPolFee = calGrpPolFee(tLOBonusGrpPolParmSchema);
        texttag.add("PolFee", PolFee);
        texttag.add("GrpPolFee", GrpPolFee);
        //个人账户总数，有效数
        int SumPolAcc = 0;
        int SumValidPolAcc = 0;
        SumPolAcc = calSumPolAcc(tLOBonusGrpPolParmSchema);
        SumValidPolAcc = calSumValidPolAcc(tLOBonusGrpPolParmSchema);
        texttag.add("SumPolAcc", SumPolAcc);
        texttag.add("SumValidPolAcc", SumValidPolAcc);

//    SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
//    texttag.add("Today", df.format(new Date()));

        if (texttag.size() > 0)
        {
            xmlExport.addTextTag(texttag);
        }

        mResult.clear();
        mResult.addElement(xmlExport);

    }

    private boolean saveData(VData mInputData)
    {
        //根据印刷号查询打印队列中的纪录
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName("LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);
        BonusNoticeGrpBLS tBonusNoticeGrpBLS = new BonusNoticeGrpBLS();
        tBonusNoticeGrpBLS.submitData(mResult, mOperate);
        if (tBonusNoticeGrpBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tBonusNoticeGrpBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;

    }

    /**
     * 求上年度利息
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private double calInterestMoney(LOBonusGrpPolParmSchema
                                    tLOBonusGrpPolParmSchema)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(Money) from LCInsureAccTrace where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo()
                + "' and MoneyType='LX'"
                + " and InsuAccNo in ('000002','000003') "
                + " and PayDate='" + tLOBonusGrpPolParmSchema.getFiscalYear() +
                "-12-31" + "'";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);
        return sumMoney;

    }

    /**
     * 求账户金额
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private double calSumPolLeaving(LOBonusGrpPolParmSchema
                                    tLOBonusGrpPolParmSchema)
    {

        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(InsuAccBala) from LCInsureAcc where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and InsuAccNo in ('000002','000003') ";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);
        return sumMoney;
    }

    /**
     * 求个人交费账户金额
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private double calPolFee(LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema)
    {

        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(InsuAccBala) from LCInsureAcc where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and InsuAccNo in ('000003') ";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);
        return sumMoney;
    }

    /**
     * 求集体交费账户金额
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private double calGrpPolFee(LOBonusGrpPolParmSchema
                                tLOBonusGrpPolParmSchema)
    {

        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(InsuAccBala) from LCInsureAcc where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and InsuAccNo in ('000002') ";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);
        return sumMoney;
    }

    /**
     * 个人账户总数
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private int calSumPolAcc(LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select count(distinct polno) from LCInsureAcc where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strAcc = tSSRS.GetText(1, 1);
        int sumAcc = Integer.parseInt(strAcc);
        return sumAcc;
    }

    /**
     * 个人账户有效数
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private int calSumValidPolAcc(LOBonusGrpPolParmSchema
                                  tLOBonusGrpPolParmSchema)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select count(distinct polno) from LCInsureAcc where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and (state='0' or state is null)";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strAcc = tSSRS.GetText(1, 1);
        int sumAcc = Integer.parseInt(strAcc);
        return sumAcc;
    }

    /**
     * 求上年结转金额
     * @param tLOBonusGrpPolParmSchema
     * @return
     */
    private double calLastYearSumMoney(LOBonusGrpPolParmSchema
                                       tLOBonusGrpPolParmSchema)
    {
        String LastPolDate = PubFun.calDate(tLOBonusGrpPolParmSchema.
                                            getSGetDate(),
                                            -1, "Y", null);
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(Money) from LCInsureAccTrace where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and (PayDate<'" + LastPolDate + "' or (PayDate='" +
                LastPolDate +
                "' and MoneyType='HL'))";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);
        return sumMoney;
    }

    /**
     * 求本年保费转入--含管理费
     * @return
     */
    private double calCurrentYearFee(LOBonusGrpPolParmSchema
                                     tLOBonusGrpPolParmSchema,
                                     LCGrpPolSchema tLCGrpPolSchema)
    {

        String LastPolDate = PubFun.calDate(tLOBonusGrpPolParmSchema.
                                            getSGetDate(),
                                            -1, "Y", null);
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL =
                "select sum(Money) from LCInsureAccTrace where  GrpPolNo='" +
                tLOBonusGrpPolParmSchema.getGrpPolNo() + "'"
                + " and (PayDate>='" + LastPolDate + "' and PayDate<'" +
                tLOBonusGrpPolParmSchema.getSGetDate() + "')"
                + " and MoneyType='BF'";
        System.out.println(strSQL);
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        String strMoney = tSSRS.GetText(1, 1);
        double sumMoney = Double.parseDouble(strMoney);

        return sumMoney / (1 - tLCGrpPolSchema.getManageFeeRate());

    }

}
