package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class AppArchiveQueryPrintUI
{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        AppArchiveQueryPrintBL tAppArchiveQueryPrintBL = new AppArchiveQueryPrintBL();

        if (!tAppArchiveQueryPrintBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tAppArchiveQueryPrintBL.mErrors);
            this.mResult.clear();
            return false;
        } else {
            this.mResult = tAppArchiveQueryPrintBL.getResult();
        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }

}
