package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.operfee.IndiDueFeeListPrintBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class OmnipAnnalsPrtListUI {

        /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public OmnipAnnalsPrtListUI() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        OmnipAnnalsPrtListBL tOmnipAnnalsPrtListBL = new OmnipAnnalsPrtListBL();

        if (!tOmnipAnnalsPrtListBL.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(tOmnipAnnalsPrtListBL.mErrors);
            return false;
        }
        mResult = tOmnipAnnalsPrtListBL.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
