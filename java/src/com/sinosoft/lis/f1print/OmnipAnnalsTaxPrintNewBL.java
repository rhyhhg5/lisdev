package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;

/**
 * <p>Title: 税优万能年报打印（单打） </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Lichang
 * @version 1.0
 */
public class OmnipAnnalsTaxPrintNewBL implements PrintService {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    SSRS mSSRS = new SSRS();

    SSRS tSSRS = new SSRS();

    private String mPolYear = "";

    private String mPolMonth ="";

    private String mOperate = "";

    private String mRunDateStart = ""; //批处理起期

    private String mRunDateEnd = "" ;   //

    private String mTempPolYear ="";

    private String mStartDate ="";

    private String mEndDate ="";

    private String mSumMoney ="";
    
    private ExeSQL mExeSQL = new ExeSQL();
    
    DecimalFormat df = new DecimalFormat("#.00");

    public OmnipAnnalsTaxPrintNewBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("==OmnipAnnalsTaxPrintNewBL start==");

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        //加入到打印列表
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("==OmnipAnnalsTaxPrintNewBL end==");

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if( mGlobalInput==null || mLOPRTManagerSchema==null )
        {
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsPrintNewBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mPolMonth = mLOPRTManagerSchema.getStandbyFlag1();
        mPolYear  = mLOPRTManagerSchema.getStandbyFlag3();
        System.out.println("保单的结算年度："+mPolYear+"年，结算月度："+mPolMonth+"'月");

        mRunDateStart = mLOPRTManagerSchema.getStandbyFlag2();

        mRunDateEnd   = mLOPRTManagerSchema.getStandbyFlag4();

        
        System.out.println("==获取险种信息开始==");
        System.out.println("==otherno:'"+mLOPRTManagerSchema.getOtherNo()+"'==");

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());
        if(!tLCPolDB.getInfo()){
        	 CError tError = new CError();
             tError.moduleName = "OmnipAnnalsPrintNewBL.java";
             tError.functionName = "dealPrintMag";
             tError.errorMessage = "获得险种信息失败！";
             mErrors.addOneError(tError);
             return false;
        }
        mLCPolSchema = tLCPolDB.getSchema();
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPolSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsPrintNewBL.java";
            tError.functionName = "dealPrintMag";
            tError.errorMessage = "获得保单信息失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }



    private boolean getPrintData()
    {
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("OmnipAnnalsTax.vts", "printer");
		textTag.add("JetFormType", "SY002");

        String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr(this,"操作员机构查询出错！");
            return false;
        }

         String printcom ="select codename from ldcode where codetype='pdfprintcom' and code='" +comcode + "' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);
         textTag.add("ManageComLength4", printcode);
         textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
         if ("batch".equals(mOperate)) {
             textTag.add("previewflag", "0");
         } else {
             textTag.add("previewflag", "1");
         }

         textTag.add("Operator", mGlobalInput.Operator); //操作员
         
         textTag.add("BarCode1", createWorkNo()); //条形码
         textTag.add("BarCodeParam1",
                     "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");


        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,"投保人表中缺少数据");
            return false;
        }

        String sql =" select * from LCAddress where CustomerNo='"+ mLCContSchema.getAppntNo() +"' AND AddressNo = '"
                   +  tLCAppntDB.getAddressNo() + "'"
                   ;
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).getSchema();
        textTag.add("AppntName",tLCAppntDB.getAppntName()); //投保人姓名

        textTag.add("ZipCode", mLCAddressSchema.getZipCode());
        System.out.println("ZipCode=" + mLCAddressSchema.getZipCode());
        textTag.add("Address", mLCAddressSchema.getPostalAddress());

        textTag.add("PrintDate",CommonBL.decodeDate(PubFun.getCurrentDate()));//打印日期
        
        //结算年度：
        mTempPolYear = new ExeSQL().getOneValue(
                "select polYear From lcinsureaccbalance where  year(DueBalaDate - 1 days) =" +
                mPolYear +
                " and polmonth = 12 and contno ='" +
                mLCContSchema.getContNo() +
                "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        //报告期间
        if("1".equals(mTempPolYear))
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select min(DueBalaDate) From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        else
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        String baladate = "select max(DueBalaDate)-1 day,max(DueBalaDate) From lcinsureaccbalance where  PolYear = " 
     	   + mTempPolYear + " and polmonth = 12 and contno = '" + mLCContSchema.getContNo() 
     	   + "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ";
		SSRS dateSSRS = mExeSQL.execSQL(baladate);
		mEndDate = dateSSRS.GetText(1, 1);
		String tBalaDate = dateSSRS.GetText(1, 2);// 结算日期

		textTag.add("StartDate", CommonBL.decodeDate(mStartDate));// 报告期间起
		textTag.add("EndDate", CommonBL.decodeDate(mEndDate));// 报告期间止
     
		textTag.add("CustomerName",mLCPolSchema.getAppntName());//客户姓名
		
        
        if(!addNewInfo())
        {
        	return false;
        }

        //添加节点
        if (textTag.size() > 0) {
            xmlexport.addTextTag1(textTag);
        }

        
        xmlexport.outputDocumentToFile("F:\\", "OmnipAnnalsTax");
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private boolean dealPrintMag()
    {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("SY002");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//        mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
//        mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        String tempMoney ="";
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    private ListTable getListTable(SSRS tSSRS)
    {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                    info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("LIST");
        return tListTable;
    }


    /**
     * 根据账户保单号码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    private String getInsuAccNo() {
        String sql = "select InsuAccNo from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }

    private String getPolno ()
    {
        String sql = "select polno from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }
    
    /**
     * 生成流水号
     * @return String
     */
    private String createWorkNo() {

        String str = "16";
        String allString = "";
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(2, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        allString = str + tDate;
        String tWorkNo = tDate + PubFun1.CreateMaxNo("GOOD" + allString, 6);
        str += tWorkNo;
        return str;
    }
  
    private boolean addNewInfo()
    {
    	System.out.println("12个月详细报文内容开始————————————————————————");
        String tContNo = mLCContSchema.getContNo();
        String sql1 = "select (select riskname from lmrisk where riskcode = a.riskcode),cvalidate, "
        	        + "a.riskcode,prem,payintv,amnt,polno,stateflag from lcpol a "
        	        + "where contno = '" + tContNo + "' and exists (select 1 from lmriskapp "
        	        + "where riskcode = a.riskcode and risktype4 = '" + BQ.RISKTYPE1_ULI + "' and taxoptimal='Y')"
        	        + " and polno ='"+mLCPolSchema.getPolNo()+"' with ur";
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = mExeSQL.execSQL(sql1);
        textTag.add("RiskCodeName", tSSRS1.GetText(1, 1)); //产品名称
        textTag.add("ContNo",mLCContSchema.getContNo()); //保险单编号

        String contstate="";
        if(tSSRS1.GetText(1, 8)!=null&&!"".equals(tSSRS1.GetText(1, 8))&&"1".equals(tSSRS1.GetText(1, 8))){
        	contstate="有效";
        } else{
        	contstate="无效";
        }
        textTag.add("ContState",contstate);//保单状态（有效/无效）
        
        textTag.add("AppntName",mLCPolSchema.getAppntName());//投保人姓名
        textTag.add("InsuredName",mLCPolSchema.getInsuredName());//被保险人姓名
        
        
        //得到保单生效日的SQL   
        String CValiDateSQL = "select min(temp.cvalidate) "
		        	     	+ "from (select cvalidate cvalidate,cinvalidate cinvalidate "
		        	     	+ "from lccont "
		        	     	+ "where conttype='1' and appflag='1' and contno='"+ tContNo +"' "
		        	     	+ "union all select cvalidate cvalidate,cinvalidate cinvalidate "
		        	     	+ "from lbcont where conttype='1' and appflag='1' and edorno like 'xb%' and contno "
		        	     	+ "in (select newcontno from lcrnewstatelog where contno='"+ tContNo +"' and state='6')) temp "
//		        	     	+ "where temp.cvalidate <= date('"+ mPolYear +"'||'-12'||'-31') "
		        	     	+ "with ur";
        
        String CValiDate = mExeSQL.getOneValue(CValiDateSQL);
        if(null == CValiDate || "".equals(CValiDate) ){
        	CValiDate = mLCContSchema.getCValiDate();
        }
        textTag.add("CValiDate", CommonBL.decodeDate(CValiDate));//保单生效日
        
        
//    	首期缴纳进账户的保险费
    	sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'BF' and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstPrem1 = Double.parseDouble(mExeSQL.getOneValue(sql1));
    	
//    	首期缴纳保险费扣费
    	sql1 = "select nvl(sum(fee),0) from lcinsureaccfeetrace where moneytype in ('GL','RP') and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstFee = Double.parseDouble(mExeSQL.getOneValue(sql1));
        
//    	首期缴纳进账户的保险费
    	double firstPrem = firstPrem1 + firstFee;
    	
        String tRiskCode = tSSRS1.GetText(1, 3);
        sql1 = "select cpayflag from lmrisk where riskcode = '" + tRiskCode + "' with ur"; //cpayflag='Y'可续期
        String tCPayFlag = mExeSQL.getOneValue(sql1);
        
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setAmnt(tSSRS1.GetText(1, 6));
        tLCPolSchema.setContNo(tContNo);
        tLCPolSchema.setPolNo(tSSRS1.GetText(1, 7));        

        
//    	期缴保险费
    	double tPrem = 0.0;
//    	趸缴保险费
    	double dunPrem = 0.0;
    	
        if(!"".equals(tCPayFlag) && "Y".equals(tCPayFlag))
        {
            if("1".equals(mTempPolYear))
            {
            	tPrem = firstPrem;
            }
            else
            {
//            	续期缴纳保险费
            	sql1 = "select nvl(sum(money),0) From lcinsureacctrace a where contno = '" + tContNo 
        			+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype = 'BF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
        			+ "a.contno and year(curpaytodate- 1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
            	tPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));
            }
            dunPrem = 0.0;
        	
        }
        else
        {
        	tPrem = 0.0;
        	dunPrem = firstPrem;
        }
        
        
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'ZF' and contno = '" 
        	+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
        double tbAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//首期追加保险费
 		
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
        	 + tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        double bqAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费
        
        double addPrem = 0.0; 
        if("1".equals(mTempPolYear))
        {
        	addPrem = tbAddPrem + bqAddPrem;
        }
        else
        {
        	addPrem = bqAddPrem;
        } 
        
        double totalPrem = tPrem + dunPrem + addPrem;
        //缴费次数
        String countStr="select count(distinct curpaytodate) from ljapayperson where contno='"+mLCPolSchema.getContNo()+"' ";
        String totalCount=mExeSQL.getOneValue(countStr);
        if(null == totalCount || "".equals(totalCount)){
        	totalCount="0";
        }
        textTag.add("TotalCount", totalCount);//已交费次数
        textTag.add("TotalPrem", PubFun.setPrecision2(totalPrem)); //累计保险费：期缴+趸缴+追加
                
        
//      生成结算利息列表,报告期内各月年化结算利率及结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthRate(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月年化利率失败。");
                return false;
            }
        }
        
		ExeSQL tExeSQL = new ExeSQL();
        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql);
        textTag.add("Year1", tSSRS.GetText(1, 1));
        textTag.add("Quarter1", tSSRS.GetText(1, 2));
        textTag.add("Solvency", tSSRS.GetText(1, 3));
        textTag.add("Flag", tSSRS.GetText(1, 4));
        
        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql2);
        textTag.add("Year2", tSSRS.GetText(1, 1));
        textTag.add("Quarter2", tSSRS.GetText(1, 2));
        textTag.add("RiskRate", tSSRS.GetText(1, 3));
        
//      生成各月结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthList(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月结算信息明细失败。");
                return false;
            }
        }
        System.out.println("12个月详细报文内容结束————————————————————————");
    	return true;
    }

    
//  生成结算利息列表,报告期内各月年化结算利率及结算信息
    private void setMonthRate(String aContNo,String aRiskCode,String aPolMonth)
    {
    	String sql = "";
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("RateMonth"+aPolMonth, decodeDate(tEndDate));
    	
    	sql = "select rate From LMInsuAccRate where riskcode = '" + aRiskCode 
    		+ "' and baladate = '" + tDueBalaDate + "' with ur";
     	String rate = mExeSQL.getOneValue(sql);
     	textTag.add("Rate"+aPolMonth, String.valueOf(Double.valueOf(rate))); //年化结算利率
    }

	//  生成各月结算信息
    private void setMonthList(String aContNo,String aRiskCode,String aPolMonth)
    {
    	textTag.add("PolMonth"+aPolMonth, SysConst.TEG_START);
    	String sql = "";
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
        	sql = "select min(DueBalaDate) From lcinsureaccbalance where polyear = 1 " 
        		+ "and polmonth = 1 and contno = '" + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear = " 
        		+ mTempPolYear + " and polmonth = " + aPolMonth + " and contno = '" + aContNo + "'"
        		+ " and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = mExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("Month"+aPolMonth, decodeDate(tEndDate));
    	
     	
//     	本月期初帐户价值
    	
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
    	
        
        textTag.add("StartMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
        
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
       	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
       	 + tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
    	textTag.add("BQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
    	
    	
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype ='BR' and contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
          	 + tEndDate + "' with ur";
       	textTag.add("BRAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月差额返还     	

       	    	
    	sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
          	 + "nvl(sum(case when moneytype = 'RP' then -money end),0) " 
          	 + "from lcinsureacctrace a where contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()
          	 +"' and otherno in (select sequenceno from lcinsureaccbalance where polYear = " 
         	 + mTempPolYear + " and polmonth = " + aPolMonth + " and contno = a.contno) with ur ";
    	SSRS tSSRS2 = mExeSQL.execSQL(sql);
        textTag.add("LXMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
        textTag.add("RiskPrem"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //风险保险费

        
//      本月期末帐户价值
        sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
        textTag.add("EndMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
        
        textTag.add("PolMonth"+aPolMonth, SysConst.TEG_END);
    }
    /**
     * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
     * @param date String
     * @return String
     */
    private String decodeDate(String date)
    {
        if ((date == null) || (date.equals("")))
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        String data[] = date.split("-");
        if (data.length != 3)
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        return (data[0] + "年" + data[1] + "月");
    }
}
