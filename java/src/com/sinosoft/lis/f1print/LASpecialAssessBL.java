package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author sgh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.utility.*;

public class LASpecialAssessBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得考核月份
    private String mAssessDate = "";
//取得渠道类别
    private String mBranchType = "";
//取得销售渠道类别
    private String mBranchType2 = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(1);
    	mAssessDate = (String)cInputData.get(2);
    	mBranchType = (String)cInputData.get(3);
    	mBranchType2 = (String)cInputData.get(4);
    	System.out.println("mBranchType2"+mBranchType2);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LASpecialAssess.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LASpecialAssess");
        String[] title = {"", "", "", "", "" ,"",""};      
       //查询所有的记录
       msql ="select getUniteCode(a.agentcode),a.name,a.managecom,(select name from ldcom where comcode=a.managecom),"
           + "b.branchattr,"
           + " (select name from labranchgroup where labranchgroup.agentgroup=a.agentgroup), b.indexcalno" 
           +" from laagent a, laassess b "
           + " where a.agentcode= b.agentcode and a.agentstate <='02' and b.agentgrade1='A00' and b.state='2' and b.branchtype='"+this.mBranchType+"' and b.branchtype2='"+this.mBranchType2+"'"
           + " and a.managecom like '"+this.mGlobalInput.ManageCom+"%'";
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))
       {
    	   msql+=" and a.managecom like '"+this.mManageCom+"%'";    	   
       }
       if(this.mAssessDate!=null&&!this.mAssessDate.equals(""))
       {
    	   msql+= " and b.indexcalno='"+this.mAssessDate+"'";
       }
       msql += "order by a.agentcode desc";
       ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      System.out.println("msql:"+msql);
      mSSRS = mExeSQL.execSQL(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";
          this.mErrors.addOneError(tCError);
          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[7];
    	  System.out.println(mSSRS.GetText(i, 1));
    	  Info[0] = mSSRS.GetText(i, 1);
    	  System.out.println(mSSRS.GetText(i, 2));
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = mSSRS.GetText(i, 5);
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);   	  
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "满足条件的信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
}