package com.sinosoft.lis.f1print;

/**
 * <p>Title: CertifySearchPrintBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;
import java.io.InputStream;

public class CertifyReportPrintBL {
    public CertifyReportPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private String mManageCom = "";
    private String mManageComName = "";
    private String moperator = "";
    private String mSQL = "";
    private XmlExport mXmlExport = null;
    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }
        return true;
    }


    private boolean getDataList() {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(mSQL);
        System.out.println(mSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String Info[] = new String[4];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            mListTable.add(Info);
        }
        return true;

    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("CertifyReportPrint.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);

        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", ""};
        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        mManageCom = (String) pmInputData.get(0);
        moperator = (String) pmInputData.get(1);
        mManageComName = (String) pmInputData.get(2);
        mSQL = (String) pmInputData.get(3);
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLCustomerMessageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }


    private void jbInit() throws Exception {
    }
}
