package com.sinosoft.lis.f1print;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LLBranchCaseReportCSVBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mCaseType = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";

	private String mFileNameB = "";

	private CSVFileWrite mCSVFileWrite = null;

	private String mFilePath = "";

	private String mFileName = "";
	
	/** 统计类型 */
	private String mStatsType = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidError("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidError("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryDataToCVS()) {
			return false;
		} else {

		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidError(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLCasePolicy";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			mOperator = (String) mTransferData.getValueByName("tOperator");
			mFileNameB = (String) mTransferData.getValueByName("tFileNameB");

			this.mManageCom = (String) mTransferData.getValueByName("MngCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
			this.mStartDate = (String) mTransferData
					.getValueByName("StartDate");// 统计起期
			this.mEndDate = (String) mTransferData.getValueByName("EndDate");// 统计止期

			this.mCaseType = (String) mTransferData.getValueByName("CaseType");// 理赔类别
			
			this.mStatsType = (String) mTransferData.getValueByName("StatsType");// 统计类别

			if (mManageCom == null || mManageCom.equals("")) {
				this.mErrors.addOneError("管理机构获取失败");
				return false;
			}
			if ("8600".equals(mManageCom)) {
				mManageCom = "86";
			}
			mManageComNam = getManagecomName(mManageCom);

			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryDataToCVS() {

		int datetype = -1;
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("LPCSVREPORT");

		if (!tLDSysVarDB.getInfo()) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		 mFilePath = tLDSysVarDB.getSysVarValue();
		// 本地测试
		//mFilePath = "F:\\picch\\ui\\vtsfile\\";
		if (mFilePath == null || "".equals(mFilePath)) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}
		System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:" + mFilePath);
		String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
		String tDate = PubFun.getCurrentDate2();

		mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
         String Casename="";
         if("01".equals(mCaseType)){
        	 Casename="分公司权限内案件";
         }else if ("02".equals(mCaseType)){
        	 Casename="超分公司权限案件";
         }else if ("03".equals(mCaseType)){
        	 Casename="全部";
         }else if ("04".equals(mCaseType)){
        	 Casename="分公司处理案件";
         }else if ("05".equals(mCaseType)){
        	 Casename="上报总公司审批案件";
         }else if ("06".equals(mCaseType)){
        	 Casename="总公司处理案件";
         }
         //报表业务清分
         String tStatsName = "";
         if("1".equals(mStatsType)){
         	tStatsName = "商业保险";
         }else if("2".equals(mStatsType)){
         	tStatsName = "社会保险";
         }else{
         	tStatsName = "全部";
         }
		String[][] tTitle = new String[4][];
		tTitle[0] = new String[] { "", "", "", "", "", "", "分案件类型工作时效统计表" };
		tTitle[1] = new String[] { "机构：" + mManageComNam, "", "业务类型："+tStatsName, "", "案件类型："+Casename, "",
				"", "", "", "统计时间：" + mStartDate + "至" + mEndDate };
		tTitle[2] = new String[] { "制表人：" + mOperator, "", "", "", "", "", "",
				"", "", "制表时间：" + mPubFun.getCurrentDate() };
		tTitle[3] = new String[] { "机构编码", "机构", "受理－扫描时间（天）", "扫描－账单录入时间（天）",
				"账单录入－检录时间（天）", "检录－审批时间（天）","分公司审批时间","总公司审批时间", "审批－审定时间（天）", "调查时间（天）",
				"理赔二核时间（天）","审定－结案时间（天）", "抽检－结案时间（天）", "结案－通知时间（天）", "通知－给付时间（天）",
				"扫描－结案时间（天）", "平均结案时间（天）" };
		
		String[] tContentType = { "String", "String", "String", "String",
				"String", "String", "String", "String",
				 "String", "String", "String", "String",
				"String", "String","String","String","String" };

		if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
			return false;
		}
		if (!getDataListToCVS(datetype)) {
			return false;
		}

		mCSVFileWrite.closeFile();

		return true;
	}

	/*
	 * 统计案件结果
	 */
	private boolean getDataListToCVS(int datetype) {
		ExeSQL tExeSQL = new ExeSQL();

		String sql = "select X.comcode,X.name " + "from ldcom X "
				+ "where X.comcode like '" + mManageCom + "%' "
				+ "and X.comgrade='02' and X.sign='1'  "
				+ "group by X.comcode,X.name " + "with ur ";

		System.out.println("SQL:" + sql);

		RSWrapper rsWrapper = new RSWrapper();
		if (!rsWrapper.prepareData(null, sql)) {
			System.out.println("机构查询失败! ");
			return false;
		}

		try {
			SSRS tSSRS = new SSRS();
			do {
				tSSRS = rsWrapper.getSSRS();
				System.out.println("tSSRS的长度为："+tSSRS.MaxRow);
				if (tSSRS != null && tSSRS.MaxRow > 0) {

					String tcomcode = "";
					String tname = "";
					String tContent[][] = new String[tSSRS.getMaxRow() + 1][];
					String ListSum[] = new String[] { "", "", "", "", "", "",
							"", "", "", "", "", "", "","","","","" };
					DecimalFormat tDF = new DecimalFormat("0.##");
					double tListSum2 = 0;
					double tListSum3 = 0;
					double tListSum4 = 0;
					double tListSum5 = 0;
					double tListSum6 = 0;
					double tListSum7 = 0;
					double tListSum8 = 0;
					double tListSum9 = 0;
					double tListSum10 = 0;//添加"二核"相关的统计
					double tListSum11 = 0;
					double tListSum12 = 0;
					double tListSum13 = 0;
					double tListSum14 = 0;
					double tListSum15 = 0;
					double tListSum16 = 0;
					double tListSumC2 = 0;
					double tListSumC3 = 0;
					double tListSumC4 = 0;
					double tListSumC5 = 0;
					double tListSumC6 = 0;
					double tListSumC7 = 0;
					double tListSumC8 = 0;
					double tListSumC9 = 0;
					double tListSumC10 = 0;//添加"二核"相关的统计
					double tListSumC11 = 0;
					double tListSumC12 = 0;
					double tListSumC13 = 0;
					double tListSumC14 = 0;
					double tListSumC15 = 0;
					double tListSumC16 = 0;
					double tListCount2 = 0;
					double tListCount3 = 0;
					double tListCount4 = 0;
					double tListCount5 = 0;
					double tListCount6 = 0;
					double tListCount7 = 0;
					double tListCount8 = 0;
					double tListCount9 = 0;
					double tListCount10 = 0;//添加"二核"相关的统计
					double tListCount11 = 0;
					double tListCount12 = 0;
					double tListCount13 = 0;
					double tListCount14 = 0;
					double tListCount15 = 0;
					double tListCount16 = 0;
					ListSum[0] = "";
					ListSum[1] = "合计";
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
						String ListInfo[] = new String[17];
						tcomcode = tSSRS.GetText(i, 1);
						// 机构编码
						ListInfo[0] = tSSRS.GetText(i, 1);
						// 机构名称
						ListInfo[1] = tSSRS.GetText(i, 2);
                       String mCaseTypeSql="";
                       if("01".equals(mCaseType)){
//						 分公司权限内案件：分公司权限内的案件指案件在审批之后，给付金额在分公司理赔用户审批权限内可以结案，不需要上报总公司进行审批的案件，包括被总公司抽检的案件。
                    	   mCaseTypeSql=" and RightsFlag='1' ";
                        }else if ("02".equals(mCaseType)){
//						 超分公司权限案件：超分公司权限案件指案件在分公司理赔人审批后，报总公司理赔人进行审批审定的案件，不含抽检案件。
                        	mCaseTypeSql=" and RightsFlag='2' ";
                        }else if ("03".equals(mCaseType)){
//						 全部：包含以上两种情况。
                        }else if ("04".equals(mCaseType)){
//						分公司处理案件：案件由分公司从受理状态做到结案状态的案件，包括被总公司抽检的案件。
                          mCaseTypeSql=" and ComCaseFlag='1' and RightsFlag='1' ";
                        }else if ("05".equals(mCaseType)){
//						上报总公司审批案件：案件由分公司从受理状态做到审批状态，超分公司权限上报总公司审批的案件。
                          mCaseTypeSql=" and ComCaseFlag='1' and RightsFlag='2'  ";
                        }else if ("06".equals(mCaseType)){
//						 总公司处理案件：案件由分公司受理，总公司从账单录入开始做到结案或审定状态的案件；
                         mCaseTypeSql=" and ComCaseFlag='2' ";
                        }
                        
                        String mStatsTypeSql = "";
                        // 对统计类型进行判断
                        if("1".equals(mStatsType)){//商业保险
                        	mStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=(select rgtno from llcase where caseno=LLCaseStateDate.caseno))) = 'N' ";
                        }else if("2".equals(mStatsType)){//社会保险
                         	mStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=(select rgtno from llcase where caseno=LLCaseStateDate.caseno))) = 'Y' ";
                        }else{
                        	mStatsTypeSql = "";
                        }
						// --受理-扫描时间
						String trgtSQL = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(EsdocDate)-days(rgtno))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(EsdocDate)-days(rgtno))+1)),0)   "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and EsdocDate is not null and rgtno is not null "
							+"with ur";
						System.out.println("2#"+trgtSQL);
						SSRS trgtSSRS = tExeSQL.execSQL(trgtSQL);
						if (trgtSSRS.getMaxRow() > 0) {
							// 受理-扫描时间
							ListInfo[2] = trgtSSRS.GetText(1, 1);
							tListSum2 += Double.parseDouble(trgtSSRS.GetText(1,
									1));
							tListSumC2 += Double.parseDouble(trgtSSRS.GetText(1,
									3));
							tListCount2+=Double.parseDouble(trgtSSRS.GetText(1,
									2));
						}
						// --扫描-账单录入时间
						String tcountcaseSQL = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(FeemainDate)-days(EsdocDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(FeemainDate)-days(EsdocDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and EsdocDate is not null and FeemainDate is not null "
							+"with ur";
						System.out.println("3#"+tcountcaseSQL);
						SSRS tcountcaseSSRS = tExeSQL.execSQL(tcountcaseSQL);
						if (tcountcaseSSRS.getMaxRow() > 0) {
							// 案件量-权限内案件量
							ListInfo[3] = tcountcaseSSRS.GetText(1, 1);
							tListSum3 += Double.parseDouble(tcountcaseSSRS
									.GetText(1, 1));
							tListSumC3 += Double.parseDouble(tcountcaseSSRS
									.GetText(1, 3));
							tListCount3 += Double.parseDouble(tcountcaseSSRS
									.GetText(1, 2));
						}
						// --账单录入-检录时间
						String tcheckSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(RollCallDate)-days(FeemainDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(RollCallDate)-days(FeemainDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and RollCallDate is not null and FeemainDate is not null "
							+"with ur";
						System.out.println("4#"+tcheckSql);
						SSRS tcheckcaseSSRS = tExeSQL.execSQL(tcheckSql);
						if (tcheckcaseSSRS.getMaxRow() > 0) {
							ListInfo[4] = tcheckcaseSSRS.GetText(1, 1);
							tListSum4 += Double.parseDouble(tcheckcaseSSRS
									.GetText(1, 1));
							tListSumC4 += Double.parseDouble(tcheckcaseSSRS
									.GetText(1, 3));
							tListCount4 += Double.parseDouble(tcheckcaseSSRS
									.GetText(1, 2));

						}
						// 检录－审批时间（天）
						String tapproveSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(ApprovalDate1)-days(RollCallDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(ApprovalDate1)-days(RollCallDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and RollCallDate is not null and ApprovalDate1 is not null "
							+"with ur";
						System.out.println("5#"+tapproveSql);
						SSRS tapproveSSRS = tExeSQL.execSQL(tapproveSql);
						if (tapproveSSRS.getMaxRow() > 0) {
							ListInfo[5] = tapproveSSRS.GetText(1, 1);
							tListSum5 += Double.parseDouble(tapproveSSRS
									.GetText(1, 1));
							tListSumC5 += Double.parseDouble(tapproveSSRS
									.GetText(1, 3));
							tListCount5 += Double.parseDouble(tapproveSSRS
									.GetText(1, 2));
						}
						// 分公司审批时间

						String tfenshenSql = "select  "
							+"(case when '"+mCaseType+"'='06' then 0 else coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(ApprovalDate2)-days(ApprovalDate1))+1))/DECIMAL(count(caseno)),2),10,2),0) end),count(caseno),(case when '"+mCaseType+"'='06' then 0 else coalesce(DECIMAL(sum(abs(days(ApprovalDate2)-days(ApprovalDate1))+1)),0) end)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and ApprovalDate2 is not null and ApprovalDate1 is not null "
							+"with ur";
						System.out.println("5-1#"+tfenshenSql);
						SSRS tfenshenSSRS = tExeSQL.execSQL(tfenshenSql);
						if (tfenshenSSRS.getMaxRow() > 0) {
							ListInfo[6] = tfenshenSSRS.GetText(1, 1);
							tListSum6 += Double.parseDouble(tfenshenSSRS
									.GetText(1, 1));
							tListSumC6 += Double.parseDouble(tfenshenSSRS
									.GetText(1, 3));
							tListCount6 += Double.parseDouble(tfenshenSSRS
									.GetText(1, 2));
						}
						// 总公司审批时间
						String tzongshenSql = "select  "
							+"(case when '"+mCaseType+"'='04' then 0 else coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(ValidationDate)-days(case when ApprovalDate2 is null then endcasedate  else ApprovalDate2 end))+1))/DECIMAL(count(caseno)),2),10,2),0) end),count(caseno),(case when '"+mCaseType+"'='04' then 0 else coalesce(DECIMAL(sum(abs(days(ValidationDate)-days(case when ApprovalDate2 is null then endcasedate  else ApprovalDate2 end))+1)),0) end)   "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and (ApprovalDate2 is not null or endcasedate is not null) "
							+"and ValidationDate is not null "
							+"and RightsFlag='2' "
							+"with ur";
						System.out.println("5-2#"+tzongshenSql);
						SSRS tzongshenSSRS = tExeSQL.execSQL(tzongshenSql);
						if (tzongshenSSRS.getMaxRow() > 0) {
							ListInfo[7] = tzongshenSSRS.GetText(1, 1);
							tListSum7 += Double.parseDouble(tzongshenSSRS
									.GetText(1, 1));
							tListSumC7 += Double.parseDouble(tzongshenSSRS
									.GetText(1, 3));
							tListCount7 += Double.parseDouble(tzongshenSSRS
									.GetText(1, 2));
						}
						// 审批－审定时间（天）
						String tcertificateSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(ValidationDate)-days(ApprovalDate1))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(ValidationDate)-days(ApprovalDate1))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and ValidationDate is not null and ApprovalDate1 is not null "
							+"with ur";
						System.out.println("6#"+tcertificateSql);
						SSRS tcertificateSSRS = tExeSQL
								.execSQL(tcertificateSql);
						if (tcertificateSSRS.getMaxRow() > 0) {
							ListInfo[8] = tcertificateSSRS.GetText(1, 1);
							tListSum8 += Double.parseDouble(tcertificateSSRS
									.GetText(1, 1));
							tListSumC8 += Double.parseDouble(tcertificateSSRS
									.GetText(1, 3));
							tListCount8 += Double.parseDouble(tcertificateSSRS
									.GetText(1, 2));
						}
						// 调查时间（天）
						String tinvestSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(EndSurveyDate)-days(StartSurveyDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(EndSurveyDate)-days(StartSurveyDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and StartSurveyDate is not null and EndSurveyDate is not null "
							+"with ur";
						System.out.println("7#"+tinvestSql);
						SSRS tinvestSSRS = tExeSQL.execSQL(tinvestSql);
						if (tinvestSSRS.getMaxRow() > 0) {
							ListInfo[9] = tinvestSSRS.GetText(1, 1);
							tListSum9 += Double.parseDouble(tinvestSSRS
									.GetText(1, 1));
							tListSumC9 += Double.parseDouble(tinvestSSRS
									.GetText(1, 3));
							tListCount9 += Double.parseDouble(tinvestSSRS
									.GetText(1, 2));
						}
						//理赔二核时间（天）--在LLCaseStateDate表中并没有二核的处理时间，此处采用llcaseoptime统计二核
                        String tPartSql = "";
                        // 对统计类型进行判断
                        if("1".equals(mStatsType)){//商业保险
                        	tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=s.rgtno)) = 'N' ";
                        }else if("2".equals(mStatsType)){//社会保险
                        	tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=s.rgtno)) = 'Y' ";
                        }else{
                        	tPartSql = "";
                        }                       
						String tLPSecondSql = "SELECT " +
							"COALESCE(DECIMAL(ROUND(DECIMAL(SUM(ABS(DAYS(CD)-DAYS(E))+1))/DECIMAL(COUNT(C)),2),10,2),0),count(C),coalesce(DECIMAL(sum(abs(days(CD)-days(E))+1)),0) " +
							"FROM " +
							"(SELECT S.MNGCOM M, s.CASENO C, o.SEQUANCE, coalesce(o.ENDDATE,o.STARTDATE) CD, o.STARTDATE E " +
							"FROM LLCASEOPTIME O, LLCASE S " +
							"WHERE s.CASENO LIKE 'C%' AND O.CASENO = S.CASENO AND O.RGTSTATE = '16' " +
							tPartSql +
							"AND O.ENDDATE BETWEEN '"+ mStartDate + "' and '" + mEndDate +"' " +
							"and S.MNGCOM like '"+tcomcode+"%') with ur";
							
						System.out.println("8#"+tLPSecondSql);
						SSRS tLPSecondSSRS = tExeSQL.execSQL(tLPSecondSql);
						if (tLPSecondSSRS.getMaxRow() > 0) {
							ListInfo[10] = tLPSecondSSRS.GetText(1, 1);
							tListSum10 += Double.parseDouble(tLPSecondSSRS
									.GetText(1, 1));
							tListSumC10 += Double.parseDouble(tLPSecondSSRS
									.GetText(1, 3));
							tListCount10 += Double.parseDouble(tLPSecondSSRS
									.GetText(1, 2));
						}
						// 审定－结案时间（天）
						String tendSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(EndCaseDate)-days(ValidationDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(EndCaseDate)-days(ValidationDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and ValidationDate is not null and EndCaseDate is not null "
							+"with ur";
						System.out.println("9#"+tendSql);
						SSRS tendSSRS = tExeSQL.execSQL(tendSql);
						if (tendSSRS.getMaxRow() > 0) {
							ListInfo[11] = tendSSRS.GetText(1, 1);
							tListSum11 += Double.parseDouble(tendSSRS.GetText(1,
									1));
							tListSumC11 += Double.parseDouble(tendSSRS.GetText(1,
									3));
							tListCount11 += Double.parseDouble(tendSSRS.GetText(1,
									2));

						}
						// 抽检－结案时间（天）
						String trandendSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(EndCaseDate)-days(SamplingDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(EndCaseDate)-days(SamplingDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and SamplingDate is not null and EndCaseDate is not null "
							+"with ur";
						System.out.println("10#"+trandendSql);
						SSRS trandendSSRS = tExeSQL.execSQL(trandendSql);
						if (trandendSSRS.getMaxRow() > 0) {
							ListInfo[12] = trandendSSRS.GetText(1, 1);
							tListSum11 += Double.parseDouble(trandendSSRS
									.GetText(1, 1));
							tListSumC12 += Double.parseDouble(trandendSSRS
									.GetText(1, 3));
							tListCount12 += Double.parseDouble(trandendSSRS
									.GetText(1, 2));
						}
						// 结案－通知时间（天）
						String tnoticeSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(NoticeDate)-days(EndCaseDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(NoticeDate)-days(EndCaseDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and NoticeDate is not null and EndCaseDate is not null "
							+"with ur";
						System.out.println("11#"+tnoticeSql);
						SSRS tnoticeSSRS = tExeSQL.execSQL(tnoticeSql);
						if (tnoticeSSRS.getMaxRow() > 0) {
							ListInfo[13] = tnoticeSSRS.GetText(1, 1);
							tListSum13 += Double.parseDouble(tnoticeSSRS
									.GetText(1, 1));
							tListSumC13 += Double.parseDouble(tnoticeSSRS
									.GetText(1, 3));
							tListCount13 += Double.parseDouble(tnoticeSSRS
									.GetText(1, 2));
						}
						// 通知－给付时间（天）
						String tpaySql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(PaymentDate)-days(NoticeDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(PaymentDate)-days(NoticeDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and NoticeDate is not null and PaymentDate is not null "
							+"with ur";
						System.out.println("12#"+tpaySql);
						SSRS tpaySSRS = tExeSQL.execSQL(tpaySql);
						if (tpaySSRS.getMaxRow() > 0) {
							ListInfo[14] = tpaySSRS.GetText(1, 1);
							tListSum14 += Double.parseDouble(tpaySSRS.GetText(
									1, 1));
							tListSumC14 += Double.parseDouble(tpaySSRS.GetText(
									1, 3));
							tListCount14 += Double.parseDouble(tpaySSRS.GetText(
									1, 2));
						}
						// 扫描－结案时间（天）
						String tscanendSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(EndCaseDate)-days(EsdocDate))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(EndCaseDate)-days(EsdocDate))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+"and EndCaseDate is not null and EsdocDate is not null "
							+"with ur";
						System.out.println("13#"+tscanendSql);
						SSRS tscanendSSRS = tExeSQL.execSQL(tscanendSql);
						if (tscanendSSRS.getMaxRow() > 0) {
							ListInfo[15] = tscanendSSRS.GetText(1, 1);
							tListSum15 += Double.parseDouble(tscanendSSRS
									.GetText(1, 1));
							tListSumC15 += Double.parseDouble(tscanendSSRS
									.GetText(1, 3));
							tListCount15 += Double.parseDouble(tscanendSSRS
									.GetText(1, 2));
						}
						// 案平均结案时间（天）
						String tavgSql = "select  "
							+"coalesce(DECIMAL(ROUND(DECIMAL(sum(abs(days(endcasedate)-days(RgtNo))+1))/DECIMAL(count(caseno)),2),10,2),0),count(caseno),coalesce(DECIMAL(sum(abs(days(endcasedate)-days(RgtNo))+1)),0)  "
							+"from LLCaseStateDate  "
							+"where endcasedate between '"+ mStartDate + "' and '" + mEndDate +"' "
							+"and managecom like '"+tcomcode + "%' "
							+ mCaseTypeSql 
							+ mStatsTypeSql
							+" and RgtNo is not null "
							+"with ur";
						System.out.println("14#"+tavgSql);
						SSRS tavgSSRS = tExeSQL.execSQL(tavgSql);
						if (tavgSSRS.getMaxRow() > 0) {
							ListInfo[16] = tavgSSRS.GetText(1, 1);
							tListSum16 += Double.parseDouble(tavgSSRS.GetText(
									1, 1));
							tListSumC16 += Double.parseDouble(tavgSSRS.GetText(
									1, 3));
							tListCount16 += Double.parseDouble(tavgSSRS.GetText(
									1, 2));
						}

						tContent[i - 1] = ListInfo;
						if (i == tSSRS.getMaxRow()) {
							ListSum[2] = tDF.format(Double.isNaN(tListSumC2/tListCount2)?0:tListSumC2/tListCount2);
//							//ListSum[2] = new DecimalFormat("0.00").format( Double.isNaN(tListSumC2/tListCount2)?0:tListSumC2/tListCount2);
							System.out.println("###################"+tDF.format(Double.isNaN(tListSumC2/tListCount2)?0:tListSumC2/tListCount2));
							ListSum[3] = tDF.format(Double.isNaN(tListSumC3/tListCount3)?0:tListSumC3/tListCount3);
							ListSum[4] = tDF.format(Double.isNaN(tListSumC4/tListCount4)?0:tListSumC4/tListCount4);
							ListSum[5] = tDF.format(Double.isNaN(tListSumC5/tListCount5)?0:tListSumC5/tListCount5);
							ListSum[6] = tDF.format(Double.isNaN(tListSumC6/tListCount6)?0:tListSumC6/tListCount6);
							ListSum[7] = tDF.format(Double.isNaN(tListSumC7/tListCount7)?0:tListSumC7/tListCount7);
							ListSum[8] = tDF.format(Double.isNaN(tListSumC8/tListCount8)?0:tListSumC8/tListCount8);
							ListSum[9] = tDF.format(Double.isNaN(tListSumC9/tListCount9)?0:tListSumC9/tListCount9);
							ListSum[10] = tDF.format(Double.isNaN(tListSumC10/tListCount10)?0:tListSumC10/tListCount10);
							ListSum[11] = tDF.format(Double.isNaN(tListSumC11/tListCount11)?0:tListSumC11/tListCount11);
							ListSum[12] = tDF.format(Double.isNaN(tListSumC12/tListCount12)?0:tListSumC12/tListCount12);
							ListSum[13] = tDF.format(Double.isNaN(tListSumC13/tListCount13)?0:tListSumC13/tListCount13);
							ListSum[14] = tDF.format(Double.isNaN(tListSumC14/tListCount14)?0:tListSumC14/tListCount14);
							ListSum[15] = tDF.format(Double.isNaN(tListSumC15/tListCount15)?0:tListSumC15/tListCount15);
							ListSum[16] = tDF.format(Double.isNaN(tListSumC16/tListCount16)?0:tListSumC16/tListCount16);
							System.out.println("###################"+tListSumC2+"iiiiiiii"+tListCount2);
							tContent[i] = ListSum;

						}
					}

					mCSVFileWrite.addContent(tContent);
					if (!mCSVFileWrite.writeFile()) {
						mErrors.addOneError(mCSVFileWrite.mErrors
								.getFirstError());
						return false;
					}

				}
			} while (tSSRS != null && tSSRS.MaxRow > 0);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rsWrapper.close();
		}
		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 得到xml的输入流
	 * 
	 * @return InputStream
	 */
	public InputStream getInputStream() {
		return mXmlExport.getInputStream();
	}

	public String getManagecomName(String com) {
		String sq = "select name from ldcom where comcode='" + com
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
		return tGrpTypeSSRS.GetText(1, 1);
	}
	public String getFileName() {
		return mFileName;
	}

}
