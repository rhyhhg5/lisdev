/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.report.f1report.JRptUtility;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 * 475要修改
 * 508要修改
 * @todo 主要分两个部分，一个是使用listtable方式
 * 另一个是texttag的方式,前者用于动态显示,后者用于
 * 静态显示
 */
public class UWResultPBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    //    private String mPolNo = "";
    //String InsuredName="";
    //输入的查询sql语句
    private String mSql = "";

    //取得的延期承保原因
    //    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    // private String AddFeeInfoReason="";
    private String ReasonInfo1 = "您无需补交保险费！";

    private String lys_Flag = "0";

    private String lys_Flag_main = "0";

    //    private String lys_Flag_ab = "0";

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    //业务处理相关变量
    /** 全局数据 */
    //    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    /**
     * 个人合同表
     */
    private LCContSchema tLCContSchema = null;

    private LCUWMasterSchema tLCUWMasterSchema = null;

    private TransferData mTransferData;

    private String mLoadFlag;

    public UWResultPBL()
    {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        if (mTransferData != null)
        {
            this.mLoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        /**
         * 查询合同信息
         */
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo())
        {
            System.out
                    .println("UWResultPBL.getPrintData()  \n--Line:165  --Author:YangMing");
            //            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            //            return false;
            LCContSet tLCContSet = tLCContDB
                    .executeQuery("select * from lccont where proposalcontno='"
                            + mLOPRTManagerSchema.getOtherNo() + "'");
            if (tLCContSet.size() <= 0)
            {
                // @@错误处理
                System.out.println("UWResultPBLUWResultPBL.java中"
                        + "getPrintData方法报错，" + "在程序154行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "UWResultPBL.java";
                tError.functionName = "getPrintData";
                tError.errorMessage = "查询LCCont失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (tLCContSet.size() == 1)
            {
                tLCContSchema = tLCContSet.get(1).getSchema();
                tLCContDB.setSchema(tLCContSchema);
            }
            else
            {
                // @@错误处理
                System.out.println("UWResultPBLUWResultPBL.java中"
                        + "getPrintData方法报错，" + "在程序154行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "UWResultPBL.java";
                tError.functionName = "getPrintData";
                tError.errorMessage = "查询LCCont失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            tLCContSchema = tLCContDB.getSchema();
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
        	//去除业务员是个险直销的条件  2008-05-13
        	//.executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where branchtype='1' and branchtype2='01' and agentcode='"
        	.executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"
        	+ tLCContDB.getAgentCode() + "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            buildError("getprintData", "没有查到该报单的代理人组别");
        }

        /**
         * 查询投保人信息
         */
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContSchema.getContNo());
        if (tLCAppntDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAppntDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCContSchema.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if (tLCAddressDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAddressDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        /**
         * 查询代理人信息
         */
        mAgentCode = tLCContDB.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        /**
         * 查询保单下全部险种信息
         */
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tempLCPolSet = null;
        if (!StrTool.cTrim(this.mLoadFlag).equals("Defer"))
        {
            tempLCPolSet = tLCPolDB
                    .executeQuery("select * from lcpol where proposalcontno='"
                            + tLCContSchema.getProposalContNo()
                            + "' order by insuredname ");
        }
        if (StrTool.cTrim(this.mLoadFlag).equals("Defer"))
        {
            tempLCPolSet = tLCPolDB
                    .executeQuery("select * from lcpol where proposalcontno='"
                            + tLCContSchema.getProposalContNo()
                            + "' and uwflag not in ('4','9') order by insuredname ");
        }
        if (tempLCPolSet.size() <= 0)
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "查询险种信息失败!未找到该保单下险种信息");
            System.out
                    .println("UWResultPBL.getPrintData()  \n--Line:197  --Author:YangMing");
            return false;
        }
        //完成信息查询
        System.out.println("完成信息查询,开始准备向模板中填入数据!");

        System.out.println("1,动态列表");
        ListTable tUWResultTable = new ListTable();
        String[] UWResultTitle = new String[9];

        tUWResultTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名
        UWResultTitle[0] = "ColNo";
        UWResultTitle[1] = "InsurdName";
        UWResultTitle[2] = "RiskName";
        UWResultTitle[3] = "RiskCode";
        UWResultTitle[4] = "MultOrAmnt";
        UWResultTitle[5] = "Prem";
        UWResultTitle[6] = "UWResult";
        UWResultTitle[7] = "UWResultDetail";
        UWResultTitle[8] = "WriteBack";
        boolean needWriteBack = false;
        ExeSQL tExeSQL = new ExeSQL();
        for (int nIndex = 0; nIndex < tempLCPolSet.size(); nIndex++)
        {
            //查询主险保单
            LCPolSchema mLCPolSchema = tempLCPolSet.get(nIndex + 1).getSchema(); //保存险种投保单信息
            //1-险种信息：
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
                return false;
            }
            //险种名称
            String MainRiskName = tLMRiskDB.getRiskName();
            //保额
            double Amnt = mLCPolSchema.getAmnt();
            //保费
            double Prem = mLCPolSchema.getPrem();
            //档次
            String Mult = mLCPolSchema.getMult() > 0 ? getNumberToCharacter(mLCPolSchema
                    .getMult())
                    + "档"
                    : null;
            String[] UWResult = new String[9];
            //行号
            UWResult[0] = (1 + nIndex) + ""; //模板中的第一列 序号
            UWResult[1] = mLCPolSchema.getInsuredName();
            UWResult[2] = MainRiskName;
            UWResult[3] = mLCPolSchema.getRiskCode();
            UWResult[4] = Mult == null ? String.valueOf(Amnt).substring(0,
                    String.valueOf(Amnt).indexOf(".")) : Mult;
            if (Mult == null)
            {
                UWResult[4] = "保额 " + UWResult[4];
            }
            else
            {
                UWResult[4] = "档次 " + UWResult[4];
            }
            String Sql = "select sum(prem) from lcprem where contno='"
                    + mLCPolSchema.getContNo() + "' and polno='"
                    + mLCPolSchema.getPolNo()
                    + "' and payplancode not like '000000%'";
            Prem = Double.parseDouble(tExeSQL.getOneValue(Sql));
            UWResult[5] = String.valueOf(Prem);
            //核保结论较复杂，需要单独处理
            try
            {
                UWResult[6] = dealUWResult(mLCPolSchema);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

            UWResult[7] = this.tLCUWMasterSchema.getUWIdea() == null ? ""
                    : this.tLCUWMasterSchema.getUWIdea();
            if (UWResult[6].equals("")
                    || tLCUWMasterSchema.getPassFlag().equals("9")
                    ||
                    //                tLCUWMasterSchema.getPassFlag().equals("1") ||
                    tLCUWMasterSchema.getPassFlag().equals("a")
                    || tLCUWMasterSchema.getPassFlag().equals("8")
                    || mLCPolSchema.getUWFlag().equals("a"))
            {
                UWResult[8] = "    --";
            }
            else
            {
                UWResult[8] = "□同意  □不同意";
                needWriteBack = true;
                if (StrTool.cTrim(this.mLoadFlag).equals("Defer"))
                {
                    UWResult[8] = "    --";
                    needWriteBack = false;
                }
            }
            tUWResultTable.add(UWResult); //加入主险信息
        }
        ListTable tBlankListTable = new ListTable();
        String[] BlankListTitle = new String[1];
        tBlankListTable.setName("Blank"); //对应模版投保信息部分的行对象名
        BlankListTitle[0] = "Row1";
        for (int i = 1; i <= 1; i++)
        {
            String[] Row = new String[1];
            Row[0] = "";
            tBlankListTable.add(Row);
        }
        //完成动态填充,开始静态
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息
        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            return false;
        }
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getServicePhone());
        texttag.add("XI_AppntNo", tLCContSchema.getAppntNo());
        texttag.add("AppntName", tLCContSchema.getAppntName());
        String strSql = "select date('"
                + this.mLOPRTManagerSchema.getMakeDate()
                + "') + 5 days from dual ";
        String WriteBack = tExeSQL.getOneValue(strSql);
        texttag
                .add("WriteBack", WriteBack.split("-")[0] + "年"
                        + WriteBack.split("-")[1] + "月"
                        + WriteBack.split("-")[2] + "日");
        texttag.add("AppntZipcode", tLCAddressDB.getZipCode());
        texttag.add("AppntAddr", tLCAddressDB.getPostalAddress());
        texttag.add("PrtNo", tLCContSchema.getPrtNo());
        //2014-11-3   杨阳
        //业务员编码显示为表LAagent中的GroupAgentCode字段
        String groupAgentcode = new ExeSQL().getOneValue("select getUniteCode("+tLCContSchema.getAgentCode()+") from dual");
        texttag.add("AgentCode", groupAgentcode);
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        //add by zhangxing
        texttag.add("AgentGroup", mLAAgentSchema.getAgentGroup()); //代理人组别
        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getBranchAttr());

        texttag.add("GlobalServicePhone", tLDComDB.getServicePhone());

        texttag.add("AgentPhone", getAgentPhone());

        texttag.add("Cavidate", tLCContSchema.getCValiDate().split("-")[0]
                + "年" + tLCContSchema.getCValiDate().split("-")[1] + "月"
                + tLCContSchema.getCValiDate().split("-")[2] + "日");
        texttag.add("Title", tLCContSchema.getAppntSex().equals("0") ? "先生"
                : "女士");
        texttag.add("Today",
                this.mLOPRTManagerSchema.getMakeDate().split("-")[0] + "年"
                        + this.mLOPRTManagerSchema.getMakeDate().split("-")[1]
                        + "月"
                        + this.mLOPRTManagerSchema.getMakeDate().split("-")[2]
                        + "日");
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + tLCContSchema.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("UWResult.vts", "printer");
        //控制页面是否显示
        if (needWriteBack)
        {
            xmlexport.addDisplayControl("displaydescribe");
            xmlexport.addDisplayControl("displaywriteback");
        }
        xmlexport.addListTable(tUWResultTable, UWResultTitle); //保存险种信息及其标题栏
        xmlexport.addListTable(tBlankListTable, BlankListTitle); //保存险种信息及其标题栏
        xmlexport.addTextTag(texttag);
        xmlexport.outputDocumentToFile("D:\\", "UWResult");
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode String
     * @return String
     */
    private String getComName(String strComCode)
    {
        mSql = "select CodeName from LDcode where Code = '" + strComCode
                + "' and CodeType = 'station'";
        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.getOneValue(mSql);
        //        LDCodeDB tLDCodeDB = new LDCodeDB();
        //        tLDCodeDB.setCode(strComCode);
        //        tLDCodeDB.setCodeType("station");
        //        if (!tLDCodeDB.getInfo())
        //        {
        //            mErrors.copyAllErrors(tLDCodeDB.mErrors);
        //            return "";
        //        }
        //        return tLDCodeDB.getCodeName();
    }

    private String getNumberToCharacter(double Num)
    {
        String cNum = String.valueOf((int) Num);
        if (cNum.equals("1"))
        {
            return "一";
        }
        if (cNum.equals("2"))
        {
            return "二";
        }
        if (cNum.equals("3"))
        {
            return "三";
        }
        if (cNum.equals("4"))
        {
            return "四";
        }
        if (cNum.equals("5"))
        {
            return "五";
        }
        if (cNum.equals("6"))
        {
            return "六";
        }
        if (cNum.equals("7"))
        {
            return "七";
        }
        if (cNum.equals("8"))
        {
            return "八";
        }
        return null;
    }

    /**
     *
     * @todo 处理核保结论，主要针对加费免责信息
     * @param schema String
     * @return String
     */
    private String dealUWResult(LCPolSchema schema)
    {

        String dPolNo = schema.getProposalNo();
        String SpecInfo = "";
        String addFeeResult = "";
        String Sresult = "";
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setProposalNo(dPolNo);
        if (!tLCUWMasterDB.getInfo())
        {
            buildError("outputXML", "处理核保结论时，查询核保主表失败！");
            return "";
        }
        tLCUWMasterSchema = tLCUWMasterDB.getSchema();
        if (StrTool.cTrim(tLCContSchema.getUWFlag()).equals("a"))
        {
            return "撤销申请";
        }
        System.out.println("#####:" + tLCUWMasterSchema.getAddPremFlag());
        if (!StrTool.cTrim(tLCUWMasterSchema.getSpecFlag()).equals("0"))
        {
            //有特别约定
            LCSpecDB tLCSpecDB = new LCSpecDB();
            tLCSpecDB.setPolNo(dPolNo);
            LCSpecSet tLCSpecSet = tLCSpecDB.query();
            if (tLCSpecSet.size() <= 0)
            {
                //                buildError("outputXML", "处理核保结论时，查询特约信息失败！");
                //                return "";
            }
            for (int i = 1; i <= tLCSpecSet.size(); i++)
            {
                SpecInfo += tLCSpecSet.get(i).getSpecContent() + ";";
                if (tLCSpecSet.get(i).getSpecStartDate() != null
                        && tLCSpecSet.get(i).getSpecEndDate() != null)
                {
                    SpecInfo += "免责期间为: "
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[2]
                            + "日" + "至"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[2]
                            + "日 ";
                }
                else if (tLCSpecSet.get(i).getSpecEndDate() == null
                        && tLCSpecSet.get(i).getSpecStartDate() != null)
                {
                    SpecInfo += "免责期间自"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[2]
                            + "日" + "开始 ";
                }
            }
        }

        if (!StrTool.cTrim(tLCUWMasterSchema.getAddPremFlag()).equals("0"))
        {
            //有加费
            LCPremDB tLCPremDB = new LCPremDB();
            tLCPremDB.setPolNo(dPolNo);
            LCPremSet tLCPremSet = tLCPremDB.query();
            if (tLCPremSet.size() <= 0)
            {
                buildError("outputXML", "处理核保结论时，查询加费信息失败！");
                return "";
            }
            double addfee = 0.00;
            double addFeeRate = 0.00;
            int taddFeeRate = 0;
            String strSql = "";
            String strSql1 = "";
            String StrarDate = "";
            String EndDate = "";
            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                if (tLCPremSet.get(i).getPayPlanCode().startsWith("000000"))
                {
                    addFeeRate = tLCPremSet.get(i).getRate();
                    addfee = tLCPremSet.get(i).getPrem();
                    strSql = (new ExeSQL()).getOneValue("select int("
                            + addFeeRate * 100 + ") from dual");
                    strSql1 = (new ExeSQL()).getOneValue("select varchar("
                            + addfee + ") from dual");
                    EndDate = tLCPremSet.get(i).getPayEndDate();
                    StrarDate = tLCPremSet.get(i).getPayStartDate();
                    break;
                }
            }
            if (addfee > 0 || addFeeRate > 0)
            {
                addFeeResult = "附加风险保费";
                addFeeResult += (addFeeRate > 0 ? strSql + "％(" + strSql1
                        + "元)" : strSql1 + "元;");
                System.out.println("addFeeResult" + addFeeResult);
                addFeeResult += "加费自" + StrarDate.split("-")[0] + "年"
                        + StrarDate.split("-")[1] + "月"
                        + StrarDate.split("-")[2] + "日" + "开始";
                if (!StrTool.cTrim(EndDate).equals(""))
                {
                    addFeeResult += "至" + EndDate.split("-")[0] + "年"
                            + EndDate.split("-")[1] + "月"
                            + EndDate.split("-")[2] + "日" + "终止";
                }
            }
            System.out.println("@@@@@ : " + addFeeResult);
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getAddPremFlag()).equals("0"))
        {
            //有承保计划变更
        }
        if (tLCUWMasterSchema.getPassFlag().equals("8"))
        {
            //延期承保
            String tPostponeDay = tLCUWMasterSchema.getPostponeDay();
            String tPostponeDate = tLCUWMasterSchema.getPostponeDate();
            String result = "延期承保";
            if (tPostponeDay == null && tPostponeDate == null)
            {
                result = "延期承保";
            }
            else if (tPostponeDay != null)
            {
                result = "延期" + tPostponeDay + "天";
            }
            else if (tPostponeDate != null)
            {
                result = "延期处理至" + tPostponeDate.split("-")[0] + "年"
                        + tPostponeDate.split("-")[1] + "月"
                        + tPostponeDate.split("-")[2] + "日";
            }
            return result;
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getSubMultFlag()).equals("0")
                && tLCUWMasterSchema.getMult() > 0)
        {
            //延期承保
            double subMult = tLCUWMasterSchema.getMult();
            double tMult = schema.getMult();
            /**@author:Yangming 处理降档 */
            Sresult = "档次由" + (int) subMult + "档降至" + (int) tMult + "档";
            // return result;
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getSubAmntFlag()).equals("0")
                && tLCUWMasterSchema.getAmnt() > 0)
        {
            //延期承保
            double subAmnt = tLCUWMasterSchema.getAmnt();
            double tAmnt = schema.getAmnt();
            /**@author:Yangming 处理降档 */
            Sresult = "保额由" + (int) subAmnt + "降至" + (int) tAmnt + "";
            //return result;
        }
        if (tLCUWMasterSchema.getPassFlag().equals("a"))
        {
            return "撤销申请";
        }
        if (tLCUWMasterSchema.getPassFlag().equals("1"))
        {
            return "谢绝承保";
        }
        if (tLCUWMasterSchema.getPassFlag().equals("9"))
        {
            return "正常通过";
        }
        return SpecInfo + addFeeResult + Sresult;
    }

    private String getAgentPhone()
    {
        String phone = !StrTool.cTrim(mLAAgentSchema.getMobile()).equals("") ? mLAAgentSchema
                .getMobile()
                : StrTool.cTrim(mLAAgentSchema.getPhone());
        return phone.equals("") ? "          " : phone;
    }

}
