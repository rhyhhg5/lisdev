package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewBargainCanF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的保单号码
    private String mPolNo = "";
    private String mTempFeeNo = "";
    //String InsuredName="";
    //输入的查询sql语句
    private String msql = "";
    //取得的代理人编码
    private String mAgentCode = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LJAGetTempFeeSchema mLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
    public NewBargainCanF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                "LCPolSchema", 0));
        mLJAGetTempFeeSchema.setSchema((LJAGetTempFeeSchema) cInputData.
                                       getObjectByObjectName(
                "LJAGetTempFeeSchema", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        //int i , m;
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setSchema(mLCPolSchema);
        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            buildError("outputXML", "在取得LCPol的数据时发生错误");
            return false;
        }
        mPolNo = tLCPolDB.getPolNo();
        mTempFeeNo = mLJAGetTempFeeSchema.getTempFeeNo();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSet tLDCodeSet = new LDCodeSet();
        msql = "select * from LDCode where CodeType='GetReasonCode' and Code=(select GetReasonCode from LJAGetTempFee where TempFeeNo=" +
               mTempFeeNo + ")";
        tLDCodeSet.set(tLDCodeDB.executeQuery(msql));
        if (tLDCodeSet.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            buildError("outputXML", "在取得LCUWError的数据时发生错误");
            return false;
        }
        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        tLDCodeSchema.setSchema(tLDCodeSet.get(1));
        /*
             m=tLDCodeSet.size();
             for (i=1;i<=m;i++)
             {
          LDCodeSchema tLDCodeSchema = new LDCodeSchema();
          tLDCodeSchema.setSchema(tLDCodeSet.get(i));
             }
         */
        LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
        LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
        LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
        msql = "select * from LJAGetTempFee where TempFeeNo=" + mTempFeeNo;
        tLJAGetTempFeeSet.set(tLJAGetTempFeeDB.executeQuery(msql));
        tLJAGetTempFeeSchema.setSchema(tLJAGetTempFeeSet.get(1));

        mAgentCode = tLCPolDB.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tLCPolDB.getRiskCode());
        tLMRiskDB.setRiskVer(tLCPolDB.getRiskVersion());
        if (!tLMRiskDB.getInfo())
        {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            buildError("outputXML", "在取得LMRisk的数据时发生错误");
            return false;
        }
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        msql = "select * from LJTempFee where TempFeeNo=" + mTempFeeNo +
               " and RiskCode=" + tLCPolDB.getRiskCode();
        tLJTempFeeSet.set(tLJTempFeeDB.executeQuery(msql));
        tLJTempFeeSchema.setSchema(tLJTempFeeSet.get(1));
        String CancelMoney = "";
        double tGetMoney = tLJAGetTempFeeSchema.getGetMoney();
        CancelMoney = PubFun.getChnMoney(tGetMoney);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NewBargainCancel.vts", "printer"); //最好紧接着就初始化xml文档

        texttag.add("LCPol.AppntName", tLCPolDB.getAppntName());
        texttag.add("LCPol.InsuredName", tLCPolDB.getInsuredName());
        texttag.add("LCPol.PolNo", tLCPolDB.getPolNo());
        texttag.add("LCPol.Prem", tLCPolDB.getPrem());
        texttag.add("CodeName", tLDCodeSchema.getCodeName());
        texttag.add("LJAGetTempFee.GetMoney", tLJAGetTempFeeSchema.getGetMoney());
        texttag.add("LJAGetTempFee.TempFeeNo",
                    tLJAGetTempFeeSchema.getTempFeeNo());
        texttag.add("AgentCode", tLCPolDB.getAgentCode());
        texttag.add("Name", tLAAgentDB.getName());
        texttag.add("RiskName", tLMRiskDB.getRiskName());
        texttag.add("LJTempFee.PayMoney", tLJTempFeeSchema.getPayMoney());
        texttag.add("CancelMoney", CancelMoney);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
        NewBargainCanF1PBL newBargainCanF1PBL1 = new NewBargainCanF1PBL();
    }
}