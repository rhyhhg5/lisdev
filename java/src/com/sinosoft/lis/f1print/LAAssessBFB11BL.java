package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LAAssessBFB11BL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mIndexCalNo = "";
       private String mBranchType = "";
       private String mBranchType2 = "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
//       private XmlExport mXmlExport = null;
       //管理机构名称
       private String mName;
       //业务员编码
       private String mAgentCode;
       //文件暂存路径
       private String mfilePathAndName;
       //考核年
       private String mAssessYear;
       //考核月
       private String mAssessMonth;
       //考核年月
       private String mAssessNo;
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }
       // 得到外部传入的数据，将数据备份到本类中
       if (!getInputData(mInputData)) {
             return false;
        }

        // 进行数据查询
        if (!dealData()) {
            return false;
        }
         return true;
     }
 
     /**
      * 根据前面的输入数据，进行BL逻辑处理
      * 如果在处理过程中出错，则返回false,否则返回true
      */
     private boolean dealData() {
         try{
                 // 查询数据
           if(!getDataList())
           {
         	  System.out.println("LAAssessBFB01BL error getDataList");
               return false;
           }
       //     System.out.println(mShowDataList.length);
       //     System.out.println(mShowDataList[0].length);
//             System.out.print("22222222222222222");
             return true;


             }catch(Exception ex)
             {
                 buildError("queryData", "LABL发生错误，准备数据时出错！");
                 return false;
             }


     }

     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAAssessBFB11BL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {

          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));

               //页面传入的数据
              this.mManageCom = (String) cInputData.get(0);
              this.mBranchType = (String) cInputData.get(1);
              this.mBranchType2 = (String) cInputData.get(2);
              this.mAssessYear = (String) cInputData.get(3);
              this.mAssessMonth = (String) cInputData.get(4);
              this.mAssessNo = mAssessYear+mAssessMonth;
              this.mName = (String)cInputData.get(5);
              this.mAgentCode =(String) cInputData.get(6);
              
              mfilePathAndName=(String)cInputData.get(8);
               System.out.println(mManageCom);

          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

   }

   /**
      * 获取打印所需要的数据
      * @param cFunction String
      * @param cErrorMsg String
      */
     /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAStatisticReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



  private boolean queryData()
  {
      try {
          // 查询数据
          if (!getDataList()) {
              return false;
          }

          System.out.println(mShowDataList.length);
          System.out.println(mShowDataList[0].length);
          // 设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Order");

          for (int i = 0; i < mShowDataList.length; i++) {
              tlistTable.add(mShowDataList[i]);
          }
          //新建一个TextTag的实例
          TextTag tTextTag = new TextTag();
          String tMakeDate = "";
          String tMakeTime = "";

          tMakeDate = mPubFun.getCurrentDate();
          tMakeTime = mPubFun.getCurrentTime();

          System.out.print("dayin252");
          if (!getManageNameB()) {
              return false;
          }
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
          tTextTag.add("tName", mManageName);

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAAssessBFB11.vts", "printer"); //初始化xml文档
          if (tTextTag.size() > 0)
              xmlexport.addTextTag(tTextTag); //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new1");
          this.mResult.clear();
          mResult.addElement(xmlexport);
          System.out.print("22222222222222222");
          return true;

      } catch (Exception ex) {
          buildError("queryData", "LABL发生错误，准备数据时出错！");
          return false;
      }
  }
  private boolean getDataList()
   {
       String tSQL = "";

       // 1、得到全部已开业的机构
       tSQL  = "select a,b,c,d,e,f,g,value(h,0),value(ee,0)," +
       		"case  when value(i,0) <0 then  0  else   value(i,0) end,value(k,0),value(l,0),"
          +" case when value(m,0) <0 then 0 else value(m,0) end,value(n,0),value(p,0), case when value(q,0) <0 then 0 else value(q,0) end,"
          +" value(s,0),value(r,0),case when value(t,0)<0 then 0 else value(t,0) end  " 
          +"   from (select getUniteCode(agentcode) a,"
          +"(select name from laagent b where a.agentcode=b.agentcode) b,"
          +"branchattr c,"
          +"(select name from labranchgroup b where a.agentgroup=b.agentgroup) d,"
          +"agentgrade e,"            
          +"(select b.employdate from laagent b where a.agentcode=b.agentcode) f,"
                +"(select b.startdate from latree b where a.agentcode=b.agentcode) g,"
                +"value(DirMngCount+DRDepMonLabor,0) h,"
                +"20 ee,"
                +"20-value(DirMngCount+DRDepMonLabor,0) i,"
                +"value(TeamCount,0) k,"    
                +"4 l,"           
                +"4-value(TeamCount,0) m,"
                +"decimal((DepFYCSum+DirDepMonAvgFYC+T50),12,2) n,"            
                +"decimal(T65/3*T33,12,2) p,"            
                +"decimal(T65/3*T33,12,2)-decimal((DepFYCSum+DirDepMonAvgFYC+T50),12,2) q,"
                +"value(T53*100,0) s," 
                +"75 r,"
                +"75-value(T53*100,0) t"
             +" from laindexinfo a where indextype='02' "
             +" and indexcalno='"+mAssessNo+"' ";
       tSQL += " and  managecom like  '" + mManageCom + "%'";
       tSQL += "  and   AgentGrade>='B11' and agentgrade<'B21' and branchtype='1' and  branchtype2='01'";
       if(mAgentCode!=null&&!mAgentCode.equals(""))
       {
        tSQL += "  and  agentcode = getAgentCode('" + mAgentCode + "')";
       }
       tSQL += " order by ManageCom,BranchAttr,AgentCode ) as x ";
       System.out.println(tSQL);
       SSRS tSSRS=new SSRS();
       ExeSQL tExeSQL=new ExeSQL();
       tSSRS=tExeSQL.execSQL(tSQL);
      
       int linenum=tSSRS.MaxRow;
       String[][] tFourDataList = new String[linenum+5][27];
    //   tFourDataList[0][0]="中国人民健康保险股份有限公司"+mName+"分公司"+mYear+"年"+mMonth+"月营销业务人员工资明细表";
       WriteToExcel t = new WriteToExcel("");
       t.createExcelFile();
       tFourDataList[1][0]="编制部门:个人销售部 ";
       tFourDataList[2][0]="主管代码 ";
       tFourDataList[2][1]="姓名 ";
       tFourDataList[2][2]="销售单位代码";
       tFourDataList[2][3]="销售单位名称";
       tFourDataList[2][4]="职级";
       tFourDataList[2][5]="入司时间";
       tFourDataList[2][6]="任职时间";
       tFourDataList[2][7]="客户顾问及以上人力";
       tFourDataList[2][8]="维持标准";
       tFourDataList[2][9]="维持差额";
       tFourDataList[2][10]="辖营业处个数";
       tFourDataList[2][11]="维持标准";
       tFourDataList[2][12]="维持差额";
       tFourDataList[2][13]="团队季度累计首年直接佣金";
       tFourDataList[2][14]="维持标准";
       tFourDataList[2][15]="维持差额";
       tFourDataList[2][16]="团队平均13个月继续率";
       tFourDataList[2][17]="维持标准";
       tFourDataList[2][18]="维持差额";

 try{
       	
           
//         String[][] tShowDataList = new String[tSSRS.MaxRow][36];
//         int linenum=tSSRS.MaxRow;
         for(int i=1;i<=linenum;i++)
         {
            if (!queryOneDataList(tSSRS,i,tFourDataList[i+2])){
         	  
         	   return false;
            }
         }
//         tFourDataList[linenum+4][0]="总经理：                       复核 ：                       制表：";
         System.out.println("...............bl java here mfilePathAndName"
      		   +mfilePathAndName);
//         tCreateCSVFile.doWrite(tShowDataList,35);
         String[] sheetName ={PubFun.getCurrentDate()};
         t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
         t.setData(0, tFourDataList);
         t.write(this.mfilePathAndName);//获得文件读取路径
 }catch(Exception exc){
 	exc.printStackTrace();
 	return false;
 }  
       return true;
  }
  
  /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 1);
          pmOneDataList[1] =tSSRS.GetText(i, 2)+"";
          pmOneDataList[2]=tSSRS.GetText(i, 3)+"";
          pmOneDataList[3]=tSSRS.GetText(i, 4)+"";
          pmOneDataList[4] =tSSRS.GetText(i, 5)+"";
          pmOneDataList[5] = tSSRS.GetText(i, 6)+"";
          pmOneDataList[6] = tSSRS.GetText(i, 7)+"";
          pmOneDataList[7] = tSSRS.GetText(i, 8)+"";
          pmOneDataList[8] = tSSRS.GetText(i, 9)+"";
          pmOneDataList[9] =tSSRS.GetText(i, 10)+"";
          pmOneDataList[10] = tSSRS.GetText(i, 11)+"";
          pmOneDataList[11] = tSSRS.GetText(i, 12)+"";
          pmOneDataList[12] = tSSRS.GetText(i, 13)+"";
          pmOneDataList[13] = tSSRS.GetText(i, 14)+"";
          pmOneDataList[14] = tSSRS.GetText(i, 15)+"";
          pmOneDataList[15] = tSSRS.GetText(i, 16)+"";
          pmOneDataList[16] = tSSRS.GetText(i, 17)+"";
          pmOneDataList[17] = tSSRS.GetText(i, 18)+"";
          pmOneDataList[18] = tSSRS.GetText(i, 19)+"";
       
          
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
    private boolean getManageNameB() {

             String sql = "select name from ldcom where comcode='" + mManageCom +
                          "'";

             SSRS tSSRS = new SSRS();

             ExeSQL tExeSQL = new ExeSQL();

             tSSRS = tExeSQL.execSQL(sql);

             if (tExeSQL.mErrors.needDealError()) {

                 this.mErrors.addOneError("销售单位不存在！");

                 return false;

             }

             if (mManageCom.equals("86")) {
                 this.mManageName = "";
             } else {
                 if(mManageCom.length()>4)
                 {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
                 else
                 {this.mManageName = tSSRS.GetText(1, 1);}
             }
             System.out.println("1111111111111111111111");
             System.out.println(mManageName);
             return true;
      }

    /**
       *
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }


   public static void main(String[] args) {
           LAAssessBFB11BL tLAAssessBFB11BL = new LAAssessBFB11BL();
           TransferData tTransferData= new TransferData();
            tTransferData.setNameAndValue("tManageCom","8612");
            tTransferData.setNameAndValue("tStartDate","2007-07-01");
            tTransferData.setNameAndValue("tEndDate","2007-08-03");
          VData tVData = new VData();
          tVData.addElement(tTransferData);



           GlobalInput tG = new GlobalInput();
           tG.Operator = "001";
            tG.ManageCom = "86";
          tVData.addElement(tG);
            System.out.println("111");
           if (tLAAssessBFB11BL.submitData(tVData,"PRINT")) {

               System.out.println("right");
           } else
               System.out.println("error");
       }


}
