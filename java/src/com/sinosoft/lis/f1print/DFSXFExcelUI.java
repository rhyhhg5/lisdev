package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class DFSXFExcelUI {
	public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;
    private String tOutXmlPath = "";
    public DFSXFExcelUI() {
    }
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("PRINT")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            VData vData = new VData();
            if (!prepareOutputData(vData)) {
                return false;
            }

            DFSXFExcelBL tDFSXFExcelBL = new DFSXFExcelBL();
            System.out.println("Start DFSXFExcelBL Submit ...DFSXF");
            if (!tDFSXFExcelBL.submitData(vData, cOperate)) {
                if (tDFSXFExcelBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tDFSXFExcelBL.mErrors);
                    return false;
                } else {
                    buildError("submitData","tDFSXFExcelBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            buildError("submitData", "操作错误");
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mDay);
            vData.add(mGlobalInput);
            vData.add(tOutXmlPath);

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /*
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
    	mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        
        System.out.println("Start DFSXFExcelUI getInputData ...DFSXF");
        System.out.println("打印日期-->"+mDay[0]+"至"+mDay[1]+";tOutXmlPath-->"+tOutXmlPath);
        System.out.println("Operator-->"+mGlobalInput.Operator+";ManageCom-->"+mGlobalInput.ManageCom);
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (tOutXmlPath == null || tOutXmlPath.equals("")) {
            buildError("getInputData", "没有得到文件路径的信息！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "DFSXFExcelUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
    	
    }
}
