package com.sinosoft.lis.f1print;

import java.io.*;
import org.w3c.dom.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;

/**
 * <p>Title: 解析用于打印的xml文件</p>
 * <p>Description: 导入发生错误的数据将不存在数据库中，保存在xml文件中，非即时打印，
 *                 从xml文件生成打印批单以及Excel。 </p>
 * <p>Copyright：(c) 2006</p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 1.0
 */

public class ExportErrCaseList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 保单信息在xml中的结点 */
    private static final String PARSE_PATH_CONTROL = "/DATASET/CONTROL";
    private static final String PARSE_PATH_FAULT = "/DATASET/FAULT";
    /** 配置文件名 */
    private String Config_File_Name = "";
    /** 文件路径 */
    private String path;
    /** Excel文件名 */
    private String fileName;
    private String mRgtNo;
    private GlobalInput mG = new GlobalInput();
    private VData mResult = new VData();
    private String vtsFileName = "";
    private String[] Title = null;
    private ListTable ErrListTable = new ListTable();
    private String xmlFileName = "";

    /**
     * 构造函数
     * @param path String
     * @param fileName String
     */
    public ExportErrCaseList() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
//        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mRgtNo = (String) tTransferData.getValueByName("RgtNo");
        path = (String) tTransferData.getValueByName("Path");
        xmlFileName = path+mRgtNo+".xml";
        if (mRgtNo == null || mRgtNo == "") {
            buildError("getInputData", "没有得到足够的信息：团体批次号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        //根据印刷号查询打印队列中的纪录
        File file = new File(xmlFileName);
        if (!file.exists()) {
        	path = path.substring(0,path.lastIndexOf("E"));
        	xmlFileName = path+mRgtNo+".xml";;
    		System.out.println("从旧的路径下查找错误列表,路径为：" + xmlFileName);
    		File tOldExistsFile = new File(xmlFileName);
    		if(!tOldExistsFile.exists()){
    			 buildError("setFileName", "该批次没有生成错误清单！");
    	            return false;
    		} 
        }
        try{
            parseXml(xmlFileName);
        }catch(Exception ex){
            CError.buildErr(this,"该批次没有生成错误清单!");
            return false;
        }
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        String pathFileNmae=vtsFileName;
        xmlexport.createDocument(pathFileNmae, "print");

        ErrListTable.setName("FAULT");

        //保存信息
        xmlexport.addListTable(ErrListTable, Title); //保存申请资料信息
        xmlexport.outputDocumentToFile("D:\\", "testXX"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 得到Excel文件路径
     * @return String
     */
    private String getFilePath() {
        return path + File.separator + fileName;
    }

    /**
     * 得到配置文件路径
     * @return String
     */
    private String getConfigFilePath() {
        return path + File.separator + Config_File_Name;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    /**
     * 解析xml，把数据保存到VData中
     * @param xmlFileName String  解析的xml文件名
     * @param data VData  存放解析结果的容器
     */
    private void parseXml(String xmlFileName) {
        try {
            //解析保单信息
            XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
            NodeList nodeList = xmlPT.parseN(PARSE_PATH_CONTROL);
            System.out.println(nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String nodeName = node.getNodeName();
                if (nodeName.equals("CONTROL")) {
                    vtsFileName = parseNode(node,"TEMPLATE");
                    System.out.println(vtsFileName);
                    break;
                }
            }
            nodeList = xmlPT.parseN(PARSE_PATH_FAULT);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                String nodeName = node.getNodeName();
                if (nodeName.equals("FAULT")){
                    NodeList arrayNodeList = node.getChildNodes();
                    for(int j=0;j<arrayNodeList.getLength();j++){
                        Node noderow = arrayNodeList.item(j);
                        String rowName = noderow.getNodeName();
                        if (rowName.equals("HEAD")){
                            NodeList rowNodeList = noderow.getChildNodes();
                            Title = parseRow(noderow);
                        }
                        else if (rowName.equals("ROW")){
                            NodeList rowNodeList = noderow.getChildNodes();
                            ErrListTable.add(parseRow(noderow));
                        }
                   }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] parseRow(Node rowNode) {
        String[] rowcoldata = new String[27];
        rowcoldata[0] = parseNode(rowNode, "COL1");
        rowcoldata[1] = parseNode(rowNode, "COL2");
        rowcoldata[2] = parseNode(rowNode, "COL3");
        rowcoldata[3] = parseNode(rowNode, "COL4");
        rowcoldata[4] = parseNode(rowNode, "COL5");
        rowcoldata[5] = parseNode(rowNode, "COL6");
        rowcoldata[6] = parseNode(rowNode, "COL7");
        rowcoldata[7] = parseNode(rowNode, "COL8");
        rowcoldata[8] = parseNode(rowNode, "COL9");
        rowcoldata[9] = parseNode(rowNode, "COL10");
        rowcoldata[10] = parseNode(rowNode, "COL11");
        rowcoldata[11] = parseNode(rowNode, "COL12");
        rowcoldata[12] = parseNode(rowNode, "COL13");
        rowcoldata[13] = parseNode(rowNode, "COL14");
        rowcoldata[14] = parseNode(rowNode, "COL15");
        rowcoldata[15] = parseNode(rowNode, "COL16");
        rowcoldata[16] = parseNode(rowNode, "COL17");
        rowcoldata[17] = parseNode(rowNode, "COL18");
        rowcoldata[18] = parseNode(rowNode, "COL19");
        rowcoldata[19] = parseNode(rowNode, "COL20");
        rowcoldata[20] = parseNode(rowNode, "COL21");
        rowcoldata[21] = parseNode(rowNode, "COL22");
        rowcoldata[22] = parseNode(rowNode, "COL23");
        rowcoldata[23] = parseNode(rowNode, "COL24");
        rowcoldata[24] = parseNode(rowNode, "COL25");
        rowcoldata[25] = parseNode(rowNode, "COL26");
        rowcoldata[26] = parseNode(rowNode, "COL27");
        return rowcoldata;
    }
    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
//        GlobalInput tG = new GlobalInput();
//        tG.ComCode = "8694";
//        tG.Operator = "cm9401";
//        tG.ManageCom = "8694";
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("FilePath", "D:/DEVELOP/ui/temp_lp/");
//       tTransferData.setNameAndValue("RgtNo","P1200070424000001");
//        VData tVData = new VData();
//        tVData.add(tTransferData);
//        tVData.add(tG);
        ExportErrCaseList tExportErrCaseList = new ExportErrCaseList();
        tExportErrCaseList.xmlFileName = "F:\\picch\\ui\\/temp_lp/ErrorList/10.xml";
        tExportErrCaseList.mRgtNo = "10";
        tExportErrCaseList.path = "F:\\picch\\ui\\/temp_lp/ErrorList";
        
        tExportErrCaseList.getPrintData();
    }

    private void jbInit() throws Exception {
    }
}
