package com.sinosoft.lis.f1print;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Hashtable;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPolSpecRelaDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPolSpecRelaSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.lis.vschema.LMRiskSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;

public class BriefContPrintComBL implements PrintService {
	public BriefContPrintComBL() {
	}

	/** 错误处理 */
	public CErrors mErrors = new CErrors();

	/** 传入数据 */
	private VData mInputData;

	/** 操作符 */
	private String mOperate;

	private GlobalInput mGlobalInput;

	/** 打印管理表信息 */
	private LOPRTManagerSchema mLOPRTManagerSchema;

	/** 合同信息 */
	private LCContSchema mLCContSchema;

	private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

	private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();

	private LDComSchema mLDComSchema = new LDComSchema();

	private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

	/** 被保人信息 */
	private LCInsuredSchema mLCInsuredScheam;

	/** 投保人信息 */
	private LCAppntSchema mLCAppntSchema;

	private String mPrtno = "";

	private int mCheckNo = 0;

	private File mFile = null;

	/** 保单打印数据 */
	private XMLDatasets mXMLDatasets;

	private String mTemplatePath;

	private VData mResult = new VData();

	/** 数据库递交Map */
	private MMap map = new MMap();

	public CErrors getErrors() {
		return mErrors;
	}

	public VData getResult() {
		return this.mResult;
	}

	/**
	 * submitData
	 * 
	 * @param vData
	 *            VData
	 * @param string
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData vData, String string) {
		if (getSubmitMap(vData, string) == null) {
			return false;
		}

		/** 准备后台数据 */
		try {
			if (!perpareOutputData()) {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(this.mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		if (!dealPrintMag()) {
			return false;
		}
		return true;
	}

	private boolean dealPrintMag() {
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
		String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
		tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
		tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo()); // 存放前台传来的Contno
		tLOPRTManagerSchema.setOtherNoType("02");
		tLOPRTManagerSchema.setCode("J024");
		tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
		tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
		tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
		tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
		tLOPRTManagerSchema.setPrtType("0");
		tLOPRTManagerSchema.setStateFlag("1");
		tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

		mResult.addElement(tLOPRTManagerSchema);
		return true;
	}

	/**
	 * 得到生成的xml文件
	 * 
	 * @param vData
	 *            VData
	 * @param string
	 *            String
	 * @return MMap
	 */
	public MMap getSubmitMap(VData vData, String string) {
		this.mInputData = vData;
		this.mOperate = string;

		/** 开始获取前台数据 */
		if (!getInputData()) {
			return null;
		}
		/** 教研数据 */
		if (!checkData()) {
			return null;
		}
		/** 生成节点 */
		if (!dealData()) {
			return null;
		}

		map.put(this.mLCContSchema, SysConst.UPDATE);

		return map;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		if (this.mInputData == null) {
			buildError("getInputData", "传入数据为null！");
			return false;
		}
		if (this.mOperate == null) {
			buildError("getInputData", "传入操作符为null！");
			return false;
		}
		mLOPRTManagerSchema = (LOPRTManagerSchema) mInputData
				.getObjectByObjectName("LOPRTManagerSchema", 0);
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);
		LCContDB tLCContDB = new LCContDB();
		LCContSet tLCContSet = new LCContSet();
		tLCContDB.setContNo(this.mLOPRTManagerSchema.getOtherNo());
		tLCContSet = tLCContDB.query();
		if (tLCContSet.size() > 0) {
			mLCContSchema = tLCContSet.get(1);
		}
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='ServerRoot'";// 配置文件路径
		this.mTemplatePath = new ExeSQL().getOneValue(sqlurl);// 配置文件路径
//		mTemplatePath = "E:\\lisdev\\ui\\f1print\\template\\";
		System.out.println("文件的存放路径   " + mTemplatePath); // 调试用－－－－－
		if (mTemplatePath == null || mTemplatePath.equals("")) {
			buildError("getFileUrlName", "获取文件存放路径出错");
			return false;
		}
		mTemplatePath += "f1print/template/";

		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		if (this.mLCContSchema == null) {
			buildError("checkData", "传入合同信息为null！");
			return false;
		}
		if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals("")) {
			buildError("checkData", "传入保单号码为null！");
			return false;
		}
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(this.mLCContSchema.getContNo());
		if (!tLCContDB.getInfo()) {
			buildError("checkData", "未查询到保单信息！");
			return false;
		} else {
			this.mLCContSchema.setSchema(tLCContDB.getSchema());
			mLCContSchema.setPrintCount(1);
		}
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setContNo(mLCContSchema.getContNo());
		LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
		if (tLCInsuredSet.size() <= 0) {
			buildError("checkData", "未查询到被保人信息！");
			return false;
		}
		this.mLCInsuredScheam = tLCInsuredSet.get(1).getSchema();
		/** 一致性校验 */
		if (!this.mLCContSchema.getInsuredNo().equals(
				this.mLCInsuredScheam.getInsuredNo())
				&& !"1".equals(mLCContSchema.getIntlFlag())) {
			buildError("checkData", "被保人数据与保单信息不一致！");
			return false;
		}
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(this.mLCContSchema.getContNo());
		if (!tLCAppntDB.getInfo()) {
			buildError("checkData", "查询投保人信息失败！");
			return false;
		}
		this.mLCAppntSchema = tLCAppntDB.getSchema();
		/** 一致性校验 */
		if (!this.mLCAppntSchema.getAppntNo().equals(
				this.mLCContSchema.getAppntNo())) {
			buildError("checkData", "头保人信息与保单信息存储不一致！");
			return false;
		}
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		// 个单Schema
		LCContSchema tLCContSchema = null;
		// 原则上一次只打印一个个单，当然可以一次多打
		tLCContSchema = this.mLCContSchema;

		try {
			// 取得该个单下的所有投保险种信息
			LCPolSet tLCPolSet = getRiskList(tLCContSchema.getContNo());
			mXMLDatasets = new XMLDatasets();
			mXMLDatasets.createDocument();

			// 正常打印流程，目前只关心这个
			if (!getPolDataSet(tLCPolSet)) {
				return false;
			}
		} catch (Exception ex) {
			buildError("getInfo", "获取保单信息节点失败！");
			return false;
		}

		// 返回数据容器清空
		mResult.clear();
		System.out.println("add inputstream to mResult");
		// 将xml信息放入返回容器
		mResult.add(mXMLDatasets.getInputStream());

		mResult.addElement(mXMLDatasets); // 返回数据文件结果
		return true;
	}

	/**
	 * 正常打印流程
	 * 
	 * @param cLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getPolDataSet(LCPolSet cLCPolSet) throws Exception {
		// xml对象
		XMLDataset tXMLDataset = mXMLDatasets.createDataset();

		// 获取打印类型，感觉无此需要
		LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
		tLMRiskFormDB.setRiskCode("000000");
		tLMRiskFormDB.setFormType("PG");
		if (!tLMRiskFormDB.getInfo()) {
			// 只有当查询出错的时候才报错，如果是空记录无所谓
			if (tLMRiskFormDB.mErrors.needDealError()) {
				buildError("getInfo", "查询打印模板信息失败！");
				return false;
			}
			// 团个单标志，1个单，2团单
			tXMLDataset.setContType("1");
			// 默认填入空
			tXMLDataset.setTemplate("");
		} else {
			// 团个单标志，1个单，2团单
			tXMLDataset.setContType("1");
			// 打印模板信息
			tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
		}

		// 查询个单合同表信息
		LCContDB tLCContDB = new LCContDB();
		// 根据合同号查询
		tLCContDB.setContNo(this.mLCContSchema.getContNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCContDB.getInfo()) {
			buildError("getInfo", "查询个单合同信息失败！");
			return false;
		}
		this.mLCContSchema = tLCContDB.getSchema();

		tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J024"));
		// 保单归档号节点
		String ArchiveNo = new ExeSQL()
				.getOneValue("select ArchiveNo from ES_DOC_Main where DocCode = '"
						+ mLCContSchema.getPrtNo()
						+ "' order by DocId fetch first 1 rows only");
		tXMLDataset.addDataObject(new XMLDataTag("ArchiveNo", ArchiveNo));

		String tManageCom = StrTool.cTrim(mLCContSchema.getManageCom());
		tManageCom = (tManageCom).length() >= 4 ? tManageCom.substring(0, 4)
				: tManageCom;
		tXMLDataset
				.addDataObject(new XMLDataTag("ManageComLength4", tManageCom));
		tXMLDataset.addDataObject(new XMLDataTag("userIP",
				mGlobalInput.ClientIP.replaceAll("\\.", "_")));
		if ("batch".equals(mOperate)) {
			tXMLDataset.addDataObject(new XMLDataTag("previewflag", "0"));
		} else {
			tXMLDataset.addDataObject(new XMLDataTag("previewflag", "1"));
		}
		
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLAAgentDB.getInfo()) {
			buildError("getInfo", "查询销售人员信息失败！");
			return false;
		}
		this.mLAAgentSchema = tLAAgentDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		/*
		 * 增加的更改业务员工号的代码
		 */
		String gropGragent = mLAAgentSchema.getGroupAgentCode();
		String gragent= mLAAgentSchema.getAgentCode();
		this.mLAAgentSchema.setAgentCode(gropGragent);

		this.mLCContSchema.setAgentCode(gropGragent);
		tXMLDataset.addSchema(this.mLCContSchema);
		this.mLCContSchema.setAgentCode(gragent);

		// 查询投保人信息
		LCAppntDB tLCAppntDB = new LCAppntDB();
		// 根据合同号查询
		tLCAppntDB.setContNo(tLCContDB.getContNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCAppntDB.getInfo()) {
			buildError("getInfo", "查询个单投保人信息失败！");
			return false;
		}
		this.mLCAppntSchema = tLCAppntDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLCAppntSchema);

		// 查询投保人的地址信息
		LCAddressDB tLCAddressDB = new LCAddressDB();
		// 根据投保人编码和地址编码查询
		tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
		tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLCAddressDB.getInfo()) {
			buildError("getInfo", "查询个单投保人地址信息失败！");
			return false;
		}
		this.mLCAddressSchema = tLCAddressDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLCAddressSchema);

		// 查询管理机构信息
		LDComDB tLDComDB = new LDComDB();
		// 根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
		tLDComDB.setComCode(tLCContDB.getManageCom());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLDComDB.getInfo()) {
			buildError("getInfo", "查询管理机构信息失败！");
			return false;
		}
		this.mLDComSchema = tLDComDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		if ("Y".equals(mLDComSchema.getInteractFlag())) {
			tLDComDB.setComCode(tLCContDB.getManageCom().substring(0, 4));
			if (!tLDComDB.getInfo()) {
				buildError("getInfo", "查询管理机构信息失败！");
				return false;
			}
			mLDComSchema.setName(tLDComDB.getSchema().getName());
		}
		tXMLDataset.addSchema(this.mLDComSchema);

		tXMLDataset.addSchema(this.mLAAgentSchema);
		
        //专管员代资证编码
        String tLAAgentQualIfno = new ExeSQL().getOneValue("select qualifno from LAQualification where agentcode = '"+gragent+"' ");
        tXMLDataset.addDataObject(new XMLDataTag("LAAgentQualIfno", tLAAgentQualIfno));

		// 增加LACom.Name等结点是否存在的标志性结点
		LAComDB tLAComDB = new LAComDB();
		tLAComDB.setAgentCom(mLCContSchema.getAgentCom());
		if (tLAComDB.getInfo()) {
			tXMLDataset.addDataObject(new XMLDataTag("LAComFlag", "0"));
			tXMLDataset.addSchema(tLAComDB.getSchema());
		} else {
			tXMLDataset.addDataObject(new XMLDataTag("LAComFlag", "1"));
		}

		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
		tLABranchGroupDB.setAgentGroup(this.mLCContSchema.getAgentGroup());
		// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
		if (!tLABranchGroupDB.getInfo()) {
			buildError("getInfo", "查询销售机构信息失败！");
			return false;
		}
		this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
		// 将查询出的schema信息放入到xml对象中
		tXMLDataset.addSchema(this.mLABranchGroupSchema);

		// 中介机构销售人员信息
		String tAgentSaleCode = this.mLCContSchema.getAgentSaleCode();
		if (tAgentSaleCode != null && !"".equals(tAgentSaleCode)) {
			LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
			tLAAgenttempDB.setAgentCode(tAgentSaleCode);
			// 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
			if (!tLAAgenttempDB.getInfo()) {
				buildError("getInfo", "查询中介机构代理业务员信息失败！");
				return false;
			}
			this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
		}
		tXMLDataset.addSchema(this.mLAAgenttempSchema);

		// 专管员代资证编码
		String tLAAgentTempQualIfno = new ExeSQL()
				.getOneValue("select qualifno from LAQualification where agentcode = '"
						+ this.mLAAgenttempSchema.getAgentCode() + "' ");
		tXMLDataset.addDataObject(new XMLDataTag("LAAgentTempQualIfno",
				tLAAgentTempQualIfno));

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		StringBuffer tSBql = new StringBuffer(128);
		tSBql
				.append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
		tSBql.append(this.mLCContSchema.getContNo());
		tSBql.append("' and OtherNoType = '2'");

		tSSRS = tExeSQL.execSQL(tSBql.toString());

		System.out.println(tSSRS.MaxNumber);
		if (tSSRS.MaxNumber == 0) {
			mCheckNo = 1;
			mPrtno = mLCContSchema.getPrtNo().substring(0,
					mLCContSchema.getPrtNo().length() - 2);
			System.out.println(mPrtno);
			String tSBql2 = "select distinct TempFeeNo from LJTempFee where OtherNo in "
					+ "(select contno from lccont where prtno='"
					+ mPrtno
					+ "' and appflag='1') and OtherNoType = '2'";
			tSSRS = tExeSQL.execSQL(tSBql2);
		}
		if (!StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("9")) {
			tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", tSSRS
					.GetText(1, 1))); // 收据号
		}

		// 保单保费合计。
		tSBql = new StringBuffer(128);

		if (com.sinosoft.lis.bq.CommonBL.hasULIRisk(this.mLCContSchema
				.getContNo())) {
			tSBql
					.append(" select nvl(sum(lcp.Prem), 0) + nvl(sum(lcp.SupplementaryPrem), 0) ");
			tSBql.append(" from LCPol lcp ");
			tSBql.append(" where lcp.ContNo = '"
					+ this.mLCContSchema.getContNo() + "' ");
		} else {
			tSBql.append("select sum(prem) from lcpol where ContNo = '");
			tSBql.append(this.mLCContSchema.getContNo());
			tSBql.append("'");
		}
		// -----------------------------------

		tSSRS = tExeSQL.execSQL(tSBql.toString());
		tXMLDataset.addDataObject(new XMLDataTag("SumPremCHS", PubFun
				.getChnMoney(Double.parseDouble(tSSRS.GetText(1, 1))))); // 总保费
		tXMLDataset.addDataObject(new XMLDataTag("SumPrem", format(Double
				.parseDouble(tSSRS.GetText(1, 1))))); // 总保费

		String cDate = PubFun.getCurrentDate().split("-")[0] + "年"
				+ PubFun.getCurrentDate().split("-")[1] + "月"
				+ PubFun.getCurrentDate().split("-")[2] + "日";
		tXMLDataset.addDataObject(new XMLDataTag("Today", cDate));

		// 保险合同生效日
		String tUWDate = mLCContSchema.getUWDate();
		if (tUWDate != null && !"".equals(tUWDate)) {
			// tUWDate = PubFun.calDate(tUWDate, 1, "D", null);
			tUWDate = tUWDate.split("-")[0] + "年" + tUWDate.split("-")[1] + "月"
					+ tUWDate.split("-")[2] + "日";
		}
		tXMLDataset.addDataObject(new XMLDataTag("UWDateNext", tUWDate));
		// --------------------

		// 增加：保单打印时间/签单时间/收费时间
		String tTmpContNo = mLCContSchema.getContNo();
		if (mCheckNo == 1) {
			ExeSQL tExeSQLok = new ExeSQL();
			String SQLok = "select contno from lccont where prtno='" + mPrtno
					+ "'";
			String tCont = tExeSQLok.getOneValue(SQLok);
			tTmpContNo = tCont;
		}
		String tContPrintDate = cDate;
		String tContPrintTime = PubFun.getCurrentTime();
		tContPrintTime = tContPrintTime.split(":")[0] + "时"
				+ tContPrintTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("ContPrintDateTime",
				tContPrintDate + tContPrintTime));

		String tContSignDate = tExeSQL
				.getOneValue("select SignDate from LCCont where ContNo = '"
						+ tTmpContNo + "'");
		tContSignDate = tContSignDate.split("-")[0] + "年"
				+ tContSignDate.split("-")[1] + "月"
				+ tContSignDate.split("-")[2] + "日";
		String tContSignTime = tExeSQL
				.getOneValue("select replace(varchar(SignTime), '.', ':') from LCCont where ContNo = '"
						+ tTmpContNo + "'");
		if (tContSignTime == null || tContSignTime.equals(""))
			tContSignTime = PubFun.getCurrentTime();
		tContSignTime = tContSignTime.split(":")[0] + "时"
				+ tContSignTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime",
				tContSignDate + tContSignTime));

		String tPayConfDate = tExeSQL
				.getOneValue("select max(ConfMakeDate) from LJTempFee where OtherNo = '"
						+ tTmpContNo + "'");
		tPayConfDate = tPayConfDate.split("-")[0] + "年"
				+ tPayConfDate.split("-")[1] + "月" + tPayConfDate.split("-")[2]
				+ "日";
		String tPayConfTime = tExeSQL
				.getOneValue("select replace(varchar(max(ConfMakeTime)), '.', ':') from LJTempFee where OtherNo = '"
						+ tTmpContNo
						+ "' and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where OtherNo = '"
						+ tTmpContNo + "')");
		if (tPayConfTime == null || tPayConfTime.equals("")) {
			tPayConfTime = tExeSQL
					.getOneValue("select replace(varchar(max(MakeTime)), '.', ':') from LJTempFee where OtherNo = '"
							+ tTmpContNo
							+ "' and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where OtherNo = '"
							+ tTmpContNo + "')");
		}
		tPayConfTime = tPayConfTime.split(":")[0] + "时"
				+ tPayConfTime.split(":")[1] + "分";
		tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime",
				tPayConfDate + tPayConfTime));
		// --------------------

		// 套餐名称
		String tWrapName = getWrapName(mLCContSchema.getPrtNo());
		tXMLDataset.addDataObject(new XMLDataTag("WrapName", tWrapName));
		// --------------------

		// 得到套餐名称的全称
		String strWrapName = new ExeSQL()
				.getOneValue("select codename from ldcode where codetype = 'wrapnameall' and code in(select riskwrapcode from lcriskdutywrap where prtno = '"
						+ mLCContSchema.getPrtNo()
						+ "') fetch first 1 rows only");
		tXMLDataset.addDataObject(new XMLDataTag("ComArchiveName", StrTool
				.cTrim(strWrapName)));

		// 处理受益人信息
		if (!DealBnf(tXMLDataset)) {
			return false;
		}
		// 产生个单下明细信息，根据配置文件生成xml数据
		if (!genInsuredList(tXMLDataset)) {
			return false;
		}
		System.out.println("个单下被保人明细准备完毕。。。");

		// 获取适用条款信息
		if (!getTermSpecDoc(tXMLDataset, cLCPolSet)) {
			return false;
		}

		// 获取特约信息
		if (!getSpecDoc(tXMLDataset, cLCPolSet)) {
			return false;
		}

		// 获取现金价值信息，尚未定义
		if (!getCashValue(tXMLDataset, cLCPolSet)) {
			return false;
		}

		XMLDataList tXMLDataList = new XMLDataList();
		tXMLDataList.setDataObjectID("END");
		tXMLDataList.buildColHead();
		tXMLDataset.addDataObject(tXMLDataList);

		return true;
	}

	/**
	 * 获取现金价值表信息
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @param cLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getCashValue(XMLDataset cXmlDataset, LCPolSet ccLCPolSet)
			throws Exception {
		// 特殊标签
		XMLDataList tXMLDataList = new XMLDataList();
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		// 现价存在标志
		boolean tFlag = false;
		int j = 0;

		// 按需求对现价显示顺序进行调整
		if (ccLCPolSet == null || ccLCPolSet.size() == 0) {
			return true; // 若未发现险种，直接跳出
		}
		String tContNo = ccLCPolSet.get(1).getContNo();
		String tStrSql = " select lcp.* "
				+ " from LCPol lcp "
				+ " inner join LCInsured lci on lci.ContNo = lcp.ContNo and lci.InsuredNo = lcp.InsuredNo "
				+ " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
				+ " where 1 = 1 "
				+ " and lcp.ContNo ='"
				+ tContNo
				+ "' "
				+ " order by int(lci.SequenceNo), lmra.SubRiskFlag, int(lcp.RiskSeqNo) ";
		LCPolSet cLCPolSet = new LCPolDB().executeQuery(tStrSql);
		// --------------------

		// 循环处理险种下的现金价值信息
		for (int i = 1; i <= cLCPolSet.size(); i++) {
			String tRiskCode = cLCPolSet.get(i).getRiskCode();

			// 若是绑定型附加险，则不需要打印现金价值表
			if (CommonBL.isAppendRisk(tRiskCode)) {
				continue;
			}

			// 得到现金价值的算法描述
			LMCalModeDB tLMCalModeDB = new LMCalModeDB();
			// 获取险种信息
			tLMCalModeDB.setRiskCode(tRiskCode);
			tLMCalModeDB.setType("X");
			LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

			// 解析得到的SQL语句
			String strSQL = "";
			// 如果描述记录唯一时处理
			if (tLMCalModeSet.size() == 1) {
				strSQL = tLMCalModeSet.get(1).getCalSQL();
			}
			// 这个险种不需要取现金价值的数据
			if (strSQL.equals("")) {
			} else {
				tFlag = true;
				tXMLDataList.setDataObjectID("CashValueInfo"
						+ String.valueOf(j));
				tXMLDataList.addColHead("InsuredName");
				tXMLDataList.addColHead("RiskName");
				tXMLDataList.buildColHead();

				tXMLDataList.setColValue("InsuredName", cLCPolSet.get(i)
						.getInsuredName());
				String tSql = "select riskname from lmriskapp where riskcode = '"
						+ cLCPolSet.get(i).getRiskCode() + "'";
				tSSRS = tExeSQL.execSQL(tSql);

				String riskWrapPlanName = CommonBL
						.getRiskWrapPlanName(tRiskCode); // 绑定型险种套餐名
				tXMLDataList.setColValue("RiskName",
						riskWrapPlanName == null ? tSSRS.GetText(1, 1)
								: riskWrapPlanName);
				tXMLDataList.insertRow(0);
				cXmlDataset.addDataObject(tXMLDataList);

				// 现金价值标签
				tXMLDataList = new XMLDataList();
				tXMLDataList.setDataObjectID("CashValue" + String.valueOf(j));
				tXMLDataList.addColHead("Year");
				tXMLDataList.addColHead("Date");
				tXMLDataList.addColHead("Cash");
				tXMLDataList.buildColHead();
				j = j + 1;

				Calculator calculator = new Calculator();
				// 设置基本的计算参数
				calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
						.getContNo());
				calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
						.getInsuredNo());
				calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
						.getInsuredSex());
				calculator.addBasicFactor("InsuredAppAge", String
						.valueOf(cLCPolSet.get(i).getInsuredAppAge()));
				calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
						.get(i).getPayIntv()));
				calculator.addBasicFactor("PayEndYear", String
						.valueOf(cLCPolSet.get(i).getPayEndYear()));
				calculator.addBasicFactor("PayEndYearFlag", String
						.valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
				calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
						.get(i).getPayYears()));
				calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
						.get(i).getInsuYear()));
				calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
						i).getPrem()));
				calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
						i).getAmnt()));
				calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
						.get(i).getFloatRate()));
				calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
						i).getMult()));
				calculator.addBasicFactor("Copys", String.valueOf(cLCPolSet
						.get(i).getCopys()));
				// add by yt 2004-3-10
				calculator.addBasicFactor("InsuYearFlag", String
						.valueOf(cLCPolSet.get(i).getInsuYearFlag()));
				calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
						.get(i).getGetYear()));
				calculator.addBasicFactor("GetYearFlag", String
						.valueOf(cLCPolSet.get(i).getGetYearFlag()));
				calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
						.get(i).getCValiDate()));
				calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
				strSQL = calculator.getCalSQL();

				// tXMLDataList.setColValue("Year", "0");
				// tXMLDataList.setColValue("Date", cLCPolSet.get(i)
				// .getCValiDate());
				// tXMLDataList.insertRow(0);

				System.out.println(strSQL);
				// 执行现金价值计算sql，获取现金价值列表
				Connection conn = DBConnPool.getConnection();
				if (null == conn) {
					buildError("getConnection", "连接数据库失败！");
					return false;
				}
				Statement stmt = null;
				ResultSet rs = null;
				try {
					stmt = conn.createStatement();
					rs = stmt.executeQuery(strSQL);
					int nCount = 0;
					while (rs.next()) {
						// 往xml对象中添加现金价值信息
						tXMLDataList
								.setColValue("Year", rs.getString(1).trim());
						tXMLDataList
								.setColValue("Date", rs.getString(2).trim());
						tXMLDataList.setColValue("Cash",
								format(rs.getDouble(3)));
						tXMLDataList.insertRow(0);
						nCount++;
					}
					cXmlDataset.addDataObject(tXMLDataList);
					// 加入现金价值记录的总的条数
					// cXmlDataset.addDataObject(new
					// XMLDataTag("CashValueCount",
					// nCount));
					rs.close();
					stmt.close();
					conn.close();
				} catch (Exception ex) {
					if (null != rs) {
						rs.close();
					}
					stmt.close();
					try {
						conn.close();
					} catch (Exception e) {
					}
					;
					// 出错处理
					buildError("executeQuery", "数据库查询失败！");
					return false;
				}
			}
		}
		// 如果有现价，才显示标签
		/*
		 * if (tFlag) { //全部现金价值信息取完毕后，将xmlDataList数据添加到xmlDataset中 }
		 */
		return true;
	}

	/**
	 * 条款信息查询
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getSpecDoc(XMLDataset cXmlDataset, LCPolSet cLCPolSet)
			throws Exception {
		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCSpec");
		// 添加Head单元内的信息
		xmlDataList.addColHead("RowID");
		xmlDataList.addColHead("SpecContent");
		xmlDataList.addColHead("InsuredName");
		xmlDataList.addColHead("RiskName");
		xmlDataList.addColHead("RiskCode");
		xmlDataList.addColHead("InsuredIdNo");
		xmlDataList.buildColHead();
		String SpecID = "";
		String Content = "";
		int LCSpecFlag = 0;// 特约信息标记

		if (cLCPolSet.size() > 0) {

			String sql = "select SpecContent from LCPolSpecRela where contno='"
					+ this.mLCContSchema.getContNo()
					+ "' and polno=(select polno from LCPolSpecRela where contno='"
					+ this.mLCContSchema.getContNo()
					+ "' fetch first 1 rows only) order by specno";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(sql);

			for (int j = 1; j <= tSSRS.getMaxRow(); j++, LCSpecFlag++) {
				SpecID = "注1";
				Content = "(" + j + ")" + tSSRS.GetText(j, 1);

				// 查询被保险人身份证号 2008-4-17
				LCInsuredDB cLCInsuredDB = new LCInsuredDB();
				cLCInsuredDB.setContNo(mLCContSchema.getContNo());
				cLCInsuredDB.setInsuredNo(mLCContSchema.getInsuredNo());
				LCInsuredSet cLCInsuredSet = cLCInsuredDB.query();
				String insuredIdNo = cLCInsuredSet.size() > 0 ? cLCInsuredSet
						.get(1).getIDNo() : "";

				xmlDataList.setColValue("RowID", SpecID);
				xmlDataList.setColValue("SpecContent", Content);
				xmlDataList.setColValue("InsuredName", mLCContSchema
						.getInsuredName());
				xmlDataList.setColValue("RiskName", getWrapName(mLCContSchema
						.getPrtNo()));
				xmlDataList.setColValue("RiskCode", getWrapCode(mLCContSchema
						.getPrtNo()));
				xmlDataList.setColValue("InsuredIdNo", insuredIdNo);
				xmlDataList.insertRow(0);
			}
		}
		cXmlDataset.addDataObject(xmlDataList);
		// 增加特约信息标记节点
		if (LCSpecFlag > 0) {
			cXmlDataset.addDataObject(new XMLDataTag("LCSpecFlag", "1"));
		} else {
			cXmlDataset.addDataObject(new XMLDataTag("LCSpecFlag", "0"));
		}
		return true;
	}

	/**
	 * 根据个单合同号，查询明细信息
	 * 
	 * @param xmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean genInsuredList(XMLDataset xmlDataset) throws Exception {
		String szTemplateFile = "";
		// 团险默认配置文件，mszTemplatePath为模板所在的应用路径
		szTemplateFile = mTemplatePath + "ContPrint.xml";
//		szTemplateFile = "E:\\lisdev\\ui\\f1print\\template\\ContPrint.xml";
		// 校验配置文件是否存在
		mFile = new File(szTemplateFile);
		System.out.println(szTemplateFile);
		if (!mFile.exists()) {
			buildError("genInsuredList", "XML配置文件不存在！");
			return false;
		}
		try {
			Hashtable hashData = new Hashtable();
			// 将变量_ContNo的值赋给xml文件
			hashData.put("_CONTNO", this.mLCContSchema.getContNo());
			// 根据配置文件生成xml数据
			XMLDataMine xmlDataMine = new XMLDataMine(new FileInputStream(
					szTemplateFile), hashData);
			xmlDataset.addDataObject(xmlDataMine);
		} catch (Exception e) {
			// 出错处理
			e.printStackTrace();
			buildError("genInsuredList", "根据XML文件生成报表数据失败！");
			return false;
		}
		return true;
	}

	private boolean DealBnf(XMLDataset xmlDataset) throws Exception {
		VData tVData = new VData();
		TransferData mTransferData = new TransferData();
		System.out.println("ContNo" + mLCContSchema.getContNo());
		mTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
		tVData.add(mTransferData);
		ModifyBnfInfoForPrintError modifybnfinfoforprinterror = new ModifyBnfInfoForPrintError();
		if (!modifybnfinfoforprinterror.submitData(tVData, null)) {
			CError.buildErr(this, "险种特约关联信息插入失败！");
			return false;
		}

		/**
		 * 处理多受益人信息
		 */
		System.out.println("开始处理受益人信息...");
		String strSql = "select distinct Name, "
				+ " sex, "
				+ " CodeName('idtype', idtype) as idtype, birthday, idno, bnfno, "
				+ " bnfgrade, bnflot, "
				+ " CodeName('relation', relationtoinsured) as relationtoinsured "
				+ " from lcbnf " + " where contno = '"
				+ mLCContSchema.getContNo() + "' and bnftype = '1' "
				+ " order by bnfgrade, bnfno " + " with ur ";

		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCBnf");
		// 添加Head单元内的信息
		xmlDataList.addColHead("Name");
		xmlDataList.addColHead("Sex");
		xmlDataList.addColHead("Birthday");
		xmlDataList.addColHead("IDNo");
		xmlDataList.addColHead("BnfGrade");
		xmlDataList.addColHead("BnfLot");
		xmlDataList.buildColHead();

		System.out.println(strSql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS ssrs = tExeSQL.execSQL(strSql);
		if (tExeSQL.mErrors.needDealError()) {
			String str = " 查询受益人信息失败原因是：" + tExeSQL.mErrors.getFirstError();
			buildError("dealBnf", str);
			System.out.println("Error : LCContPrintPdfBL -> dealBnf()");
			return false;
		}
		if (ssrs != null && ssrs.getMaxRow() > 0) {
			for (int i = 1; i <= ssrs.getMaxRow(); i++) {
				xmlDataList.setColValue("Name", ssrs.GetText(i, 1));
				xmlDataList.setColValue("Sex", ssrs.GetText(i, 2));
				xmlDataList.setColValue("IdType", ssrs.GetText(i, 3));
				xmlDataList.setColValue("Birthday", ssrs.GetText(i, 4));
				xmlDataList.setColValue("IDNo", ssrs.GetText(i, 5));
				xmlDataList.setColValue("BnfNo", ssrs.GetText(i, 6));
				xmlDataList.setColValue("BnfGrade", ssrs.GetText(i, 7));
				xmlDataList.setColValue("BnfLot", ssrs.GetText(i, 8));
				xmlDataList
						.setColValue("RelationToInsured", ssrs.GetText(i, 9));
				xmlDataList.insertRow(0);
			}
			xmlDataset.addDataObject(new XMLDataTag("IsBnfFlag", "1"));
		} else {
			xmlDataset.addDataObject(new XMLDataTag("IsBnfFlag", "0"));
		}
		xmlDataset.addDataObject(xmlDataList);
		return true;
	}

	/**
	 * 取得个单下的全部LCPol表数据
	 * 
	 * @param cContNo
	 *            String
	 * @return LCPolSet
	 * @throws Exception
	 */
	private static LCPolSet getRiskList(String cContNo) throws Exception {
		// 查询返回容器
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolSet = tLCPolDB
				.executeQuery("select a.* from LCPol a,LCInsured b where a.ContNo=b.ContNo and a.InsuredNo=b.InsuredNo and a.ContNo ='"
						+ cContNo
						+ "' order by int(b.SequenceNo),int(a.RiskSeqNo) with ur ");
		// 返回个单险种集合
		return tLCPolSet;
	}

	/**
	 * 格式化浮点型数据
	 * 
	 * @param dValue
	 *            double
	 * @return String
	 */
	private static String format(double dValue) {
		return new DecimalFormat("0.00").format(dValue);
	}

	/**
	 * 获取险种套餐名称
	 * 
	 * @param cContNo
	 * @return
	 */
	private String getWrapName(String cPrtNo) {
		String tStrSql = " select wrapname from ldwrap "
				+ "where exists (select 1 from lcriskdutywrap "
				+ "where riskwrapcode = ldwrap.riskwrapcode and prtno='"
				+ cPrtNo + "') ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取险种套餐名称
	 * 
	 * @param cContNo
	 * @return
	 */
	private String getWrapCode(String cPrtNo) {
		String tStrSql = " select distinct riskwrapcode from lcriskdutywrap "
				+ "where prtno='" + cPrtNo + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 适用条款信息查询
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getTermSpecDoc(XMLDataset cXmlDataset, LCPolSet cLCPolSet)
			throws Exception {
		XMLDataList xmlDataList = new XMLDataList();
		// 添加xml一个新的对象Term
		xmlDataList.setDataObjectID("LCTermSpec");
		// 添加Head单元内的信息
		xmlDataList.addColHead("RowID");
		xmlDataList.addColHead("SpecContent");

		String SpecID = "";
		String Content = "";
		int LCTermSpecFlag = 0;// 使用条款标记
		String mainpol = "";
		// 是否是套餐绑定销售

		mainpol = " select distinct lcr.riskwrapcode,(select Wrapname from ldwrap where riskwrapcode=lcr.riskwrapcode) "
				+ " from lcriskdutywrap lcr where contno='"
				+ this.mLCContSchema.getContNo() + "' ";
		SSRS tpolSSRS = new ExeSQL().execSQL(mainpol);
		if (tpolSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tpolSSRS.getMaxRow(); i++) {
				Content = "";
				String sql = " select distinct lcp.riskcode,(select riskname from lmriskapp where riskcode=lcp.riskcode),lcp.riskseqno "
						+ " from lcpol lcp ,lcriskdutywrap lcr "
						+ " where lcp.contno=lcr.contno "
						+ " and lcp.contno='"
						+ this.mLCContSchema.getContNo()
						+ "' "
						+ " and lcr.riskwrapcode ='"
						+ tpolSSRS.GetText(i, 1)
						+ "'"
						+ " and lcp.insuredno=lcr.insuredno "
						+ " and lcp.riskcode=lcr.riskcode "
						+ " order by lcp.riskseqno ";
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = tExeSQL.execSQL(sql);
				for (int j = 1; j <= tSSRS.getMaxRow(); j++, LCTermSpecFlag++) {
					if (j == 1) {
						Content = "“" + tpolSSRS.GetText(i, 2) + "（"
								+ tpolSSRS.GetText(i, 1) + "）”适用《"
								+ tSSRS.GetText(j, 2) + "条款》";
					} else {
						Content += "、《" + tSSRS.GetText(j, 2) + "条款》";
					}

				}
				SpecID = "适用条款说明" + i;
				Content += "。";
				xmlDataList.setColValue("RowID", SpecID);
				xmlDataList.setColValue("SpecContent", Content);
				xmlDataList.insertRow(0);
			}
		}

		cXmlDataset.addDataObject(xmlDataList);

		// 增加特约信息标记节点
		if (LCTermSpecFlag > 0) {
			cXmlDataset.addDataObject(new XMLDataTag("LCTermSpecFlag", "1"));
		} else {
			cXmlDataset.addDataObject(new XMLDataTag("LCTermSpecFlag", "0"));
		}

		return true;
	}

	/**
	 * perpareOutputData
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean perpareOutputData() throws Exception {
		map.put(this.mLCContSchema, "UPDATE");
		this.mInputData.add(map);
		return true;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LCContPrintPdfBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}

	public static void main(String[] args) {
		try {

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
