package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author JavaBean
 * @version 1.0
 */

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLRiskPayRateCSVBL implements PrintService {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 输入的查询sql语句
	private String msql = "";

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private TransferData mTransferData = new TransferData();

	private String mFileNameB = "";

	private String mMakeDate = "";

	private PubFun mPubFun = new PubFun();

	private String mOperator = "";

	private String mFilePath = "";
	
	private String mFileName="";
	
	private CSVFileWrite mCSVFileWrite = null;

	public LLRiskPayRateCSVBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("Come to RiskPayRateBL submitData()..........");

		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();

		// 准备所有要打印的数据
		if (!getPrintData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				this.buildError("LLPrintSave-->submitData", "数据不完整");
				return false;
			}
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		System.out.println("Come to RiskPayRateBL getInputData()..........");
		// 全局变量
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		mTransferData = ((TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0));
		if (mTransferData == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
		mOperator = mGlobalInput.Operator;
		mFilePath = (String) mTransferData.getValueByName("realpath");
		return true;
		
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LLRiskPayRateBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintData() {
        System.out.println("Come to LLRiskPayRateBL getPrintData().................");

        // 统计的起讫时间
        String StartDate   = (String) mTransferData.getValueByName("Begin");
        String EndDate     = (String) mTransferData.getValueByName("End");
        String RiskCode    = (String) mTransferData.getValueByName("RiskCode");
        String RiskName    = (String) mTransferData.getValueByName("RiskName");
        String ManageCom   = (String) mTransferData.getValueByName("ManageCom");
        String HandlerN    = (String) mTransferData.getValueByName("Handler");
        String CurrentDateN   = (String) mTransferData.getValueByName("CurrentDate");
        String RiskNameN   = "";

        System.out.println("***********HandlerN  "+HandlerN);
        System.out.println("***********CurrentDateN "+CurrentDateN);

        try{
            SSRS tSSRS = new SSRS();
            String sql="select riskname from lmrisk where riskcode='"+ RiskCode +"'";
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();
                System.out.println("Execute testSql ..............." + i);
                String[] strCol;
                strCol = new String[1];
                strCol[0] = temp[i][0];

                RiskNameN=strCol[0];
            }
        }
        catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLRiskPayRateBL";
            tError.functionName = "";
            tError.errorMessage = "险种名称查询失败";
            this.mErrors.addOneError(tError);
        }
        
        mFilePath+="vtsfile/";
		System.out.println(mFilePath);
		// 本地测试
        // mFilePath = "F:\\vtsfile\\";
        String ManageComName="";
        try {
        	SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" + ManageCom + "'";
            System.out.println(sql_M);
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            ManageComName=temp_M[0][0];
			
		} catch (Exception e) {
			// TODO: handle exception
		}
 
		String[][] tTitle = new String[3][];
		tTitle[1] = new String[] { "机构：" + ManageComName, "","","","","","","险种："+RiskNameN,"","","","","",
				"统计时间：" + StartDate + "至" + EndDate };
		tTitle[0] = new String[] { "","","","", "","","","分险种赔付率分析表", "","","","","","" };
		tTitle[2] = new String[] { "机构名称", "经过保费赔付率%", "月度1经过赔付率%","月度2经过赔付率%","月度3经过赔付率%","月度4经过赔付率%","月度5经过赔付率%","月度6经过赔付率%","月度7经过赔付率%","月度68经过赔付率%","月度9经过赔付率%","月度10经过赔付率%","月度11经过赔付率%","月度12经过赔付率%" };
		mFileName = "RiskPayRate";
		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
		String[] tContentType = { "String", "String", "String" , "String", "String", "String", "String", "String", "String", "String", "String", "String", "String", "String"};
		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

       if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
       }
       
			
        // 险种档次信息
        ListTable MultTable = new ListTable();
        MultTable.setName("MULT");

        try {
            SSRS tSSRS = new SSRS();
            String sql =  "select A,B,"
                          + "CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                          // +
							// "'f','g','h','i','j','k','l','m','n','o','p','q'"
                          + " from ( "
                          + " select a.comcode A, a.name B,"
                          + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                          + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                          + " and b1.rgtdate between '"+ StartDate +"' and '"+ EndDate +"' "
                          + " and a1.riskcode = '"+ RiskCode +"'"
                          + " ) C,"
                          + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ EndDate +"')-to_date('"+ StartDate +"'))/365 "
                          + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                          + " and DATE(A2.ENDDATE)>=DATE('"+ StartDate +"') AND DATE(A2.CVALIDATE)<=DATE('"+ EndDate +"') "
                          + " and a2.riskcode='"+ RiskCode +"' "
                          + "  ) D "
                          + " from ldcom a "
                          + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                          + "  and sign='1' "
                          + " ) as x order by A ";

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();

            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();
                System.out.println("Execute testSql ..............."+i);
                String[] strCol;
                strCol = new String[3];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                MultTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLRiskPayRateBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        // 得到行数
        int rowNum=0;
        try{
           SSRS tSSRS = new SSRS();
           String sql="select comcode  from ldcom a "
                      +" where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                      +" and sign='1' ";
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(sql);
           rowNum = tSSRS.getMaxRow();
       }
       catch (Exception e) {
           CError tError = new CError();
           tError.moduleName = "LLRiskPayRateBL";
           tError.functionName = "";
           tError.errorMessage = "险种名称查询失败";
           this.mErrors.addOneError(tError);
       }

       

        ListTable MonMult1Table = new ListTable();
        MonMult1Table.setName("MONMULT1");

        ListTable MonMult2Table = new ListTable();
        MonMult2Table.setName("MONMULT2");
        ListTable MonMult3Table = new ListTable();
        MonMult3Table.setName("MONMULT3");
        ListTable MonMult4Table = new ListTable();
        MonMult4Table.setName("MONMULT4");
        ListTable MonMult5Table = new ListTable();
        MonMult5Table.setName("MONMULT5");
        ListTable MonMult6Table = new ListTable();
        MonMult6Table.setName("MONMULT6");
        ListTable MonMult7Table = new ListTable();
        MonMult7Table.setName("MONMULT7");
        ListTable MonMult8Table = new ListTable();
        MonMult8Table.setName("MONMULT8");
        ListTable MonMult9Table = new ListTable();
        MonMult9Table.setName("MONMULT9");
        ListTable MonMult10Table = new ListTable();
        MonMult10Table.setName("MONMULT10");
        ListTable MonMult11Table = new ListTable();
        MonMult11Table.setName("MONMULT11");
        ListTable MonMult12Table = new ListTable();
        MonMult12Table.setName("MONMULT12");

        String[][] monthLong = new String[12][2];
        monthLong=getMonthLong(StartDate,EndDate);
/** *********************************************************************************************************** */

// 1月赔付率
MonMult1Table.setName("MONMULT1");
try {
     if(!monthLong[0][0].equals("-")||!monthLong[0][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select A,B,"
                      + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                      + " from ( "
                      + " select a.comcode A, a.name B,"
            +" (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
            + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
            + " and b1.rgtdate between '" + monthLong[0][0] + "' and '" + monthLong[0][1] + "' "
            + " and a1.riskcode = '"+ RiskCode +"'"
            + " ) C,"
            + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[0][1] + "')-to_date('" + monthLong[0][0] + "'))/365 "
            + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
            + " and DATE(A2.ENDDATE)>=DATE('" + monthLong[0][0] + "') AND DATE(A2.CVALIDATE)<=DATE('" + monthLong[0][1] + "') "
            + " and a2.riskcode='" + RiskCode + "' "+ "  ) D "
            + " from ldcom a "
            + " where substr(a.comcode,1,length('" + ManageCom + "'))='" + ManageCom + "' and LENGTH(TRIM(A.COMCODE))=8 "
            + "  and sign='1' " + " ) as x order by A";

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            System.out.println(count);
            for (int i = 0; i < count; i++) {
              String temp[][] = tSSRS.getAllData();

            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult1Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult1Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 2月赔付率
MonMult2Table.setName("MONMULT2");
try {
    if(!monthLong[1][0].equals("-")||!monthLong[1][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[1][0] +"' and '" + monthLong[1][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[1][1] + "')-to_date('" + monthLong[1][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[1][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[1][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult2Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult2Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


// 3月赔付率
MonMult3Table.setName("MONMULT3");
try {
    if(!monthLong[2][0].equals("-")||!monthLong[2][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[2][0] +
                     "' and '" + monthLong[2][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[2][1] + "')-to_date('" + monthLong[2][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[2][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[2][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult3Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult3Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


// 4月赔付率
MonMult4Table.setName("MONMULT4");
try {
    if(!monthLong[3][0].equals("-")||!monthLong[3][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[3][0] +
                     "' and '" + monthLong[3][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[3][1] + "')-to_date('" + monthLong[3][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[3][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[3][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult4Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult4Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 5月赔付率
MonMult5Table.setName("MONMULT5");
try {
    if(!monthLong[4][0].equals("-")||!monthLong[4][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[4][0] +
                     "' and '" + monthLong[4][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('"+monthLong[4][1] + "')-to_date('" + monthLong[4][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[4][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[4][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult5Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult5Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 6月赔付率
MonMult6Table.setName("MONMULT6");
try {
    if(!monthLong[5][0].equals("-")||!monthLong[5][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[5][0] +
                     "' and '" + monthLong[5][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[5][1] + "')-to_date('" + monthLong[5][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[5][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[5][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult6Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult6Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 7月赔付率
MonMult7Table.setName("MONMULT7");
try {
     if(!monthLong[6][0].equals("-")||!monthLong[6][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select A,B,"
                      + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                      + " from ( "
                      + " select a.comcode A, a.name B,"
            +" (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
            + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
            + " and b1.rgtdate between '" + monthLong[6][0] + "' and '" + monthLong[6][1] + "' "
            + " and a1.riskcode = '"+ RiskCode +"'"
            + " ) C,"
            + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[6][1] + "')-to_date('" + monthLong[6][0] + "'))/365 "
            + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
            + " and DATE(A2.ENDDATE)>=DATE('" + monthLong[6][0] + "') AND DATE(A2.CVALIDATE)<=DATE('" + monthLong[6][1] + "') "
            + " and a2.riskcode='" + RiskCode + "' "+ "  ) D "
            + " from ldcom a "
            + " where substr(a.comcode,1,length('" + ManageCom + "'))='" + ManageCom + "' and LENGTH(TRIM(A.COMCODE))=8 "
            + "  and sign='1' " + " ) as x order by A";

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            System.out.println(count);
            for (int i = 0; i < count; i++) {
              String temp[][] = tSSRS.getAllData();

            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult7Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult7Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 8月赔付率
MonMult8Table.setName("MONMULT8");
try {
    if(!monthLong[7][0].equals("-")||!monthLong[7][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[7][0] +
                     "' and '" + monthLong[7][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" +monthLong[7][1] + "')-to_date('" + monthLong[7][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[7][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[7][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult8Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult8Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 9月赔付率
MonMult9Table.setName("MONMULT9");
try {
    if(!monthLong[8][0].equals("-")||!monthLong[8][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[8][0] +
                     "' and '" + monthLong[8][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" +monthLong[8][1] + "')-to_date('" + monthLong[8][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[8][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[8][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult9Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult9Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 10月赔付率
MonMult10Table.setName("MONMULT10");
try {
    if(!monthLong[9][0].equals("-")||!monthLong[9][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[9][0] +
                     "' and '" + monthLong[9][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[9][1] + "')-to_date('" + monthLong[9][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[9][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[9][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult10Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult10Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 11月赔付率
MonMult11Table.setName("MONMULT11");
try {
    if(!monthLong[10][0].equals("-")||!monthLong[10][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[10][0] +
                     "' and '" + monthLong[10][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" + monthLong[10][1] + "')-to_date('" + monthLong[10][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[10][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[10][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult11Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult11Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

// 12月赔付率
MonMult12Table.setName("MONMULT12");
try {
    if(!monthLong[11][0].equals("-")||!monthLong[11][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql = " select A,B,"
                     + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                     + " from ( "
                     + " select a.comcode A, a.name B,"
                     +
                     " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                     + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                     + " and b1.rgtdate between '" + monthLong[11][0] +
                     "' and '" + monthLong[11][1] + "' "
                     + " and a1.riskcode = '"+ RiskCode +"'"
                     + " ) C,"
                     + " (select (1-0.25)*sum(prem)*decimal(to_date('" +monthLong[11][1] + "')-to_date('" + monthLong[11][0] +
                     "'))/365 "
                     + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                     + " and DATE(A2.ENDDATE)>=DATE('" +
                     monthLong[11][0] +
                     "') AND DATE(A2.CVALIDATE)<=DATE('" +
                     monthLong[11][1] + "') "
                     + " and a2.riskcode='" + RiskCode + "' "
                     + "  ) D "
                     + " from ldcom a "
                     + " where substr(a.comcode,1,length('" +
                     ManageCom + "'))='" + ManageCom +
                     "' and LENGTH(TRIM(A.COMCODE))=8 "
                     + "  and sign='1' "
                     + " ) as x order by A";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult12Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult12Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


/** *********************************************************************************************************** */
 
 // 总计险种档次信息
 ListTable SumMultTable = new ListTable();
 SumMultTable.setName("SUMMULT");
 try {
     SSRS tSSRS = new SSRS();
     String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                  +"( select distinct "
                  + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                  + " from ( "
                  + " select "
                  + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                  + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                  + " and b1.rgtdate between '"+ StartDate +"' and '"+ EndDate +"' "
                  + " and a1.riskcode = '"+ RiskCode +"'"
                  + " ) C,"
                  + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ EndDate +"')-to_date('"+ StartDate +"'))/365 "
                  + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                  + " and DATE(A2.ENDDATE)>=DATE('"+ StartDate +"') AND DATE(A2.CVALIDATE)<=DATE('"+ EndDate +"') "
                  + " and a2.riskcode='"+ RiskCode +"' "
                  + "  ) D "
                  + " from ldcom a "
                  + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                  + "  and sign='1' "
                  + " ) as x  "
                  + " ) as y"
                  ;

     ExeSQL tExeSQL = new ExeSQL();
     tSSRS = tExeSQL.execSQL(sql);
     int count = tSSRS.getMaxRow();

     for (int i = 0; i < count; i++) {
         String temp[][] = tSSRS.getAllData();
         System.out.println("Execute testSql ..............."+i);
         String[] strCol;
         strCol = new String[3];
         strCol[0] = temp[i][0];
         strCol[1] = temp[i][1];
         strCol[2] = temp[i][2];
         SumMultTable.add(strCol);
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 ListTable SumMonMult1Table = new ListTable();
 SumMonMult1Table.setName("SUMMONMULT1");
 ListTable SumMonMult2Table = new ListTable();
 SumMonMult2Table.setName("SUMMONMULT2");
 ListTable SumMonMult3Table = new ListTable();
 SumMonMult3Table.setName("SUMMONMULT3");
 ListTable SumMonMult4Table = new ListTable();
 SumMonMult4Table.setName("SUMMONMULT4");
 ListTable SumMonMult5Table = new ListTable();
 SumMonMult5Table.setName("SUMMONMULT5");
 ListTable SumMonMult6Table = new ListTable();
 SumMonMult6Table.setName("SUMMONMULT6");
 ListTable SumMonMult7Table = new ListTable();
 SumMonMult7Table.setName("SUMMONMULT7");
 ListTable SumMonMult8Table = new ListTable();
 SumMonMult8Table.setName("SUMMONMULT8");
 ListTable SumMonMult9Table = new ListTable();
 SumMonMult9Table.setName("SUMMONMULT9");
 ListTable SumMonMult10Table = new ListTable();
 SumMonMult10Table.setName("SUMMONMULT10");
 ListTable SumMonMult11Table = new ListTable();
 SumMonMult11Table.setName("SUMMONMULT11");
 ListTable SumMonMult12Table = new ListTable();
 SumMonMult12Table.setName("SUMMONMULT12");


 // 月度合计
 // 1月赔付率合计
 try {
      if(!monthLong[0][0].equals("-")||!monthLong[0][1].equals("-"))
      {
          SSRS tSSRS = new SSRS();
          String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                  +"( select distinct "
                  + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                  + " from ( "
                  + " select "
                  + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                  + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                  + " and b1.rgtdate between '"+ monthLong[0][0] +"' and '"+ monthLong[0][1] +"' "
                  + " and a1.riskcode = '"+ RiskCode +"'"
                  + " ) C,"
                  + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[0][1] +"')-to_date('"+ monthLong[0][0] +"'))/365 "
                  + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                  + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[0][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[0][1] +"') "
                  + " and a2.riskcode='"+ RiskCode +"' "
                  + "  ) D "
                  + " from ldcom a "
                  + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                  + "  and sign='1' "
                  + " ) as x  "
                  + " ) as y"
                  ;
             ExeSQL tExeSQL = new ExeSQL();
             tSSRS = tExeSQL.execSQL(sql);
             int count = tSSRS.getMaxRow();
             System.out.println(count);
             for (int i = 0; i < count; i++) {
               String temp[][] = tSSRS.getAllData();

             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult1Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult1Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 // 2月赔付率合计
 try {
     if(!monthLong[1][0].equals("-")||!monthLong[1][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[1][0] +"' and '"+ monthLong[1][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[1][1] +"')-to_date('"+ monthLong[1][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[1][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[1][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult2Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult2Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 3月赔付率合计
 try {
     if(!monthLong[2][0].equals("-")||!monthLong[2][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[2][0] +"' and '"+ monthLong[2][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[2][1] +"')-to_date('"+ monthLong[2][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[2][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[2][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult3Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult3Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 4月赔付率合计
 try {
     if(!monthLong[3][0].equals("-")||!monthLong[3][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[3][0] +"' and '"+ monthLong[3][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[3][1] +"')-to_date('"+ monthLong[3][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[3][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[3][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult4Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult4Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 5月赔付率合计
 try {
     if(!monthLong[4][0].equals("-")||!monthLong[4][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[4][0] +"' and '"+ monthLong[4][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[4][1] +"')-to_date('"+ monthLong[4][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[4][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[4][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult5Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult5Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 6月赔付率合计
 try {
     if(!monthLong[5][0].equals("-")||!monthLong[5][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                  +"( select distinct "
                  + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                  + " from ( "
                  + " select "
                  + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                  + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                  + " and b1.rgtdate between '"+ monthLong[5][0] +"' and '"+ monthLong[5][1] +"' "
                  + " and a1.riskcode = '"+ RiskCode +"'"
                  + " ) C,"
                  + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[5][1] +"')-to_date('"+ monthLong[5][0] +"'))/365 "
                  + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                  + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[5][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[5][1] +"') "
                  + " and a2.riskcode='"+ RiskCode +"' "
                  + "  ) D "
                  + " from ldcom a "
                  + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                  + "  and sign='1' "
                  + " ) as x  "
                  + " ) as y"
                  ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult6Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult6Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }



 // 7月赔付率合计
 try {
      if(!monthLong[6][0].equals("-")||!monthLong[6][1].equals("-"))
      {
          SSRS tSSRS = new SSRS();
          String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[6][0] +"' and '"+ monthLong[6][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[6][1] +"')-to_date('"+ monthLong[6][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[6][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[6][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


             ExeSQL tExeSQL = new ExeSQL();
             tSSRS = tExeSQL.execSQL(sql);
             int count = tSSRS.getMaxRow();
             System.out.println(count);
             for (int i = 0; i < count; i++) {
               String temp[][] = tSSRS.getAllData();

             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult7Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult7Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 // 8月赔付率合计
 try {
     if(!monthLong[7][0].equals("-")||!monthLong[7][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[7][0] +"' and '"+ monthLong[7][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[7][1] +"')-to_date('"+ monthLong[7][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[7][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[7][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult8Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult8Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 9月赔付率合计
 try {
     if(!monthLong[8][0].equals("-")||!monthLong[8][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[8][0] +"' and '"+ monthLong[8][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[8][1] +"')-to_date('"+ monthLong[8][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[8][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[8][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult9Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult9Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

  // 10月赔付率合计
 try {
     if(!monthLong[9][0].equals("-")||!monthLong[9][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[9][0] +"' and '"+ monthLong[9][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[9][1] +"')-to_date('"+ monthLong[9][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[9][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[9][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult10Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult10Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

  // 11月赔付率合计
 try {
     if(!monthLong[10][0].equals("-")||!monthLong[10][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[10][0] +"' and '"+ monthLong[10][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[10][1] +"')-to_date('"+ monthLong[10][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[10][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[10][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;


         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult11Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult11Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 // 12月赔付率合计
 try {
     if(!monthLong[11][0].equals("-")||!monthLong[11][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '9999','总计' ,sum(FF)  from  "
                 +"( select distinct "
                 + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                 + " from ( "
                 + " select "
                 + " (select sum(a1.realpay) from llclaimpolicy a1, llcase b1 "
                 + " where a1.caseno=b1.caseno and SUBSTR(b1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                 + " and b1.rgtdate between '"+ monthLong[11][0] +"' and '"+ monthLong[11][1] +"' "
                 + " and a1.riskcode = '"+ RiskCode +"'"
                 + " ) C,"
                 + "(select (1-0.25)*sum(prem)*decimal(to_date('"+ monthLong[11][1] +"')-to_date('"+ monthLong[11][0] +"'))/365 "
                 + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                 + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[11][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[11][1] +"') "
                 + " and a2.riskcode='"+ RiskCode +"' "
                 + "  ) D "
                 + " from ldcom a "
                 + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                 + "  and sign='1' "
                 + " ) as x  "
                 + " ) as y"
                 ;
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];
             System.out.println("monthLong[11][0] "+monthLong[11][0]+"  *************** strCol[0]  " + strCol[0]);
             SumMonMult12Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult12Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 /** **************** */
 
 String tContent[][] = new String[rowNum+1][];
 
 
 for(int i=0;i<rowNum;i++){
	 
	 String ListInfo[] = new String[14];	 
	 ListInfo[0]=MultTable.get(i)[1];
	 ListInfo[1]=MultTable.get(i)[2];
	 ListInfo[2]=MonMult1Table.get(i)[0];
	 ListInfo[3]=MonMult2Table.get(i)[0];
	 ListInfo[4]=MonMult3Table.get(i)[0];
	 ListInfo[5]=MonMult4Table.get(i)[0];
	 ListInfo[6]=MonMult5Table.get(i)[0];
	 ListInfo[7]=MonMult6Table.get(i)[0];
	 ListInfo[8]=MonMult7Table.get(i)[0];
	 ListInfo[9]=MonMult8Table.get(i)[0];
	 ListInfo[10]=MonMult9Table.get(i)[0];
	 ListInfo[11]=MonMult10Table.get(i)[0];
	 ListInfo[12]=MonMult11Table.get(i)[0];
	 ListInfo[13]=MonMult12Table.get(i)[0];
		 
	 tContent[i] = ListInfo;
	 	 
 }
 
 
    String ListInfo[] = new String[14];
    ListInfo[0]=SumMultTable.get(0)[1];
	ListInfo[1]=SumMultTable.get(0)[2];
	ListInfo[2]=SumMonMult1Table.get(0)[0];
	ListInfo[3]=SumMonMult2Table.get(0)[0];
	ListInfo[4]=SumMonMult3Table.get(0)[0];
	ListInfo[5]=SumMonMult4Table.get(0)[0];
	ListInfo[6]=SumMonMult5Table.get(0)[0];
	ListInfo[7]=SumMonMult6Table.get(0)[0];
	ListInfo[8]=SumMonMult7Table.get(0)[0];
	ListInfo[9]=SumMonMult8Table.get(0)[0];
	ListInfo[10]=SumMonMult9Table.get(0)[0];
    ListInfo[11]=SumMonMult10Table.get(0)[0];
	ListInfo[12]=SumMonMult11Table.get(0)[0];
	ListInfo[13]=SumMonMult12Table.get(0)[0];
	tContent[rowNum] = ListInfo;
 
	
    mCSVFileWrite.addContent(tContent);
    
    if (!mCSVFileWrite.writeFile()) {
		mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
		return false;
	}
 
    mCSVFileWrite.closeFile();

        return true;
    }



	/**
	 * 输入起始日期终止日期 输出：起期与止期之间月经过日期
	 */

	private String[][] getMonthLong(String startDate, String endDate) {
		System.out.println(startDate + "   " + endDate);
		String[][] monthLong = new String[12][2];
		if (startDate.length() != 0 || endDate.length() != 0) {
			String StartYear = startDate.substring(0, 4);
			String EndYear = endDate.substring(0, 4);
			String StartMonth = startDate.substring(5, 7);
			String EndMonth = endDate.substring(5, 7);
			String StartDay = startDate.substring(8, 10);
			String EndDay = endDate.substring(8, 10);

			int iStartYear = Integer.parseInt(StartYear);
			int iEndYear = Integer.parseInt(EndYear);
			int iStartMonth = Integer.parseInt(StartMonth);
			int iEndMonth = Integer.parseInt(EndMonth);
			int iStartDay = Integer.parseInt(StartDay);
			int iEndDay = Integer.parseInt(EndDay);

			int marYear = iEndYear - iStartYear;
			int marMonth = iEndMonth - iStartMonth;

			int monNum = 0;
			if ((marYear) != 0) {
				monNum = marYear * 12 + marMonth + 1;
			} else {
				monNum = marMonth + 1;
			}
			System.out.println("月度数：" + monNum);

			String[] MonthNo = new String[12];

			if (monNum == 1) {
				monthLong[0][0] = iStartYear + "-" + iStartMonth + "-"
						+ iStartDay;
				monthLong[0][1] = iEndYear + "-" + iEndMonth + "-" + iEndDay;
				for (int i = 2; i < 13; i++) {
					monthLong[i - 1][0] = "-";
					monthLong[i - 1][1] = "-";
				}
			} else if (monNum == 2) {
				monthLong[0][0] = iStartYear + "-" + iStartMonth + "-"
						+ iStartDay;
				monthLong[0][1] = iStartYear + "-" + iStartMonth + "-"
						+ getDays(iStartMonth, iStartYear);
				monthLong[1][0] = iEndYear + "-" + iEndMonth + "-" + 1;
				monthLong[1][1] = iEndYear + "-" + iEndMonth + "-" + iEndDay;

				for (int i = 3; i < 13; i++) {
					monthLong[i - 1][0] = "-";
					monthLong[i - 1][1] = "-";
				}
			} else if (monNum > 2) {
				int staMonth = iStartMonth;
				monthLong[0][0] = iStartYear + "-" + iStartMonth + "-"
						+ iStartDay;
				monthLong[0][1] = iStartYear + "-" + iStartMonth + "-"
						+ getDays(iStartMonth, iStartYear);
				int i;
				for (i = 2; i < monNum; i++) {
					staMonth = staMonth + 1;

					if (staMonth > 12) {
						monthLong[i - 1][0] = iEndYear + "-" + staMonth % 12
								+ "-" + 1;
						monthLong[i - 1][1] = iEndYear + "-" + staMonth % 12
								+ "-" + getDays(staMonth % 12, iEndYear);
					} else {
						monthLong[i - 1][0] = iStartYear + "-" + staMonth + "-"
								+ 1;
						monthLong[i - 1][1] = iStartYear + "-" + staMonth + "-"
								+ getDays(staMonth, iStartYear);
						;
					}
				}
				monthLong[monNum - 1][0] = iEndYear + "-" + iEndMonth + "-" + 1;
				monthLong[monNum - 1][1] = iEndYear + "-" + iEndMonth + "-"
						+ iEndDay;

				for (int j = monNum; j < 12; j++) {
					monthLong[j][0] = "-";
					monthLong[j][1] = "-";
				}
			}
			return monthLong;
		} else {
			return null;
		}
	}

	private int getDays(int month, int year) {
		int days = 0;
		int[] daysInMonth = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (2 == month) {
			return ((0 == year % 4) && (0 != (year % 100)))
					|| (0 == year % 400) ? 29 : 28;
		}
		return daysInMonth[month];
	}
	
	
	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}


	public static void main(String[] args) {

		LLRiskPayRateCSVBL tLLRiskBL = new LLRiskPayRateCSVBL();
		String[][] monLong = new String[12][2];
		monLong = tLLRiskBL.getMonthLong("2005-02-03", "2005-12-10");
		for (int i = 0; i < monLong.length; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.print("   monLong[" + i + "][" + j + "]="
						+ monLong[i][j]);
			}
			System.out.println();
		}

		// PubSubmit ps = new PubSubmit();
		// if (ps.submitData(vdata, ""))
		// {
		// System.out.println("succeed in pubsubmit");
		// }

	}

	private void jbInit() throws Exception {
	}

}
