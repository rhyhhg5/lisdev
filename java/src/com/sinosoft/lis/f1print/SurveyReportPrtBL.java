package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: 调查报告打印</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLSurveySet;
import com.sinosoft.utility.*;

public class SurveyReportPrtBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
    private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();

    public SurveyReportPrtBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("Start GetInputData()...");
        //全局变量
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",0);
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getOtherNo() == null)
        {
            buildError("getInputData", "没有得到足够的信息:该案件没有提调信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "SurveyReportPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //根据印刷号查询打印队列中的纪录
        String aSurveyNo = mLOPRTManagerSchema.getOtherNo();
        String aRgtNo = "";
        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        System.out.println(aSurveyNo);
        if (aSurveyNo == null || "".equals(aSurveyNo))
        {
            CError tError = new CError();
            tError.moduleName = "aSurveyNo";
            tError.functionName = "getinfo";
            tError.errorMessage = "获得SurveyNo字段时出错。";
            //mErrors.copyAllErrors(tError);
            //buildError("outputXML", "提调查询编号失败");
            this.mErrors.addOneError(tError);
            return false;
        }
        tLLSurveyDB.setSurveyNo(aSurveyNo);
        LLSurveySet tLLSurveySet = new LLSurveySet();
        int SurvTimes = 0;
        tLLSurveySet = tLLSurveyDB.query();
        SurvTimes = (Integer.parseInt(aSurveyNo.substring(17)));
        if (tLLSurveySet == null || tLLSurveySet.size()<=0)
        {
            mErrors.copyAllErrors(tLLSurveySet.mErrors);
            buildError("outputXML", "提调信息查询失败");
            return false;
        }
        tLLSurveySchema = tLLSurveySet.get(1).getSchema();

        String aCaseNo = tLLSurveySchema.getOtherNo();
        LLCaseDB mLLCaseDB = new LLCaseDB();
        mLLCaseDB.setCaseNo(aCaseNo);
        //mLLCaseDB.setRgtNo(aRgtNo);

        if (!mLLCaseDB.getInfo())
        {
            mErrors.copyAllErrors(mLLCaseDB.mErrors);
            buildError("outputXML", "案件信息查询失败");
            return false;
        }
        mLLCaseSchema.setSchema(mLLCaseDB.getSchema());
        aRgtNo = mLLCaseSchema.getRgtNo(); //立案号

        LLRegisterDB mLLRegisterDB = new LLRegisterDB(); //以下怎么处理
        mLLRegisterDB.setRgtNo(aRgtNo);
        if (mLLRegisterDB.getInfo() == false)
        {
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "团体批次信息查询失败");
            return false;
        }
        mLLRegisterSchema = mLLRegisterDB.getSchema();
        String Marriage="";
        String Occupation="";
        String ComName="";
        String StartMan="";
        String SurveyMan="";
        String ConfMan="";
        String Result = "";
        String ConfNote = "";
        String ComAddress=""+mLLRegisterSchema.getRgtantAddress();
        String aCustomerNo = mLLCaseSchema.getCustomerNo();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSchema aLCAddressSchema = new LCAddressSchema();
        tLCAddressDB.setCustomerNo(aCustomerNo);
        tLCAddressDB.setAddressNo("1");
        if(tLCAddressDB.getInfo()){
            aLCAddressSchema.setSchema(tLCAddressDB.getSchema());
        }
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(aCustomerNo);
        if(!tLDPersonDB.getInfo()){
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "客户信息查询失败");
            return false;
        }
        ComName += tLDPersonDB.getGrpName();
        System.out.println(ComName);
        if(ComName.equals("")||ComName.equals("null") ||ComName==null){
            ComName=""+mLLRegisterSchema.getGrpName();
        }
        if(ComName.equals("")||ComName.equals("null")){
            String tsql = "select b.grpname from lcpol a,lcgrpcont b "+
                          "where b.grpcontno=a.grpcontno and a.insuredno = '"
                          +aCustomerNo+"'";
            ExeSQL texecsql = new ExeSQL();
            String tname = ""+texecsql.getOneValue(tsql);
            if(!tname.equals("null"))
                ComName=tname;
        }
        if(ComAddress.equals("")||ComAddress.equals("null")||ComAddress==null){
            ComAddress = "" + aLCAddressSchema.getCompanyAddress();
        }
        if(ComAddress.equals("")||ComAddress.equals("null")||ComAddress==null){
            String tsql = "select distinct b.grpaddress from lcpol a,lcgrpaddress b "+
                          "where b.customerno=a.appntno and a.insuredno = '"
                          +aCustomerNo+"'";
            ExeSQL texecsql = new ExeSQL();
            String tname = ""+texecsql.getOneValue(tsql);
            if(!tname.equals("null")){
                ComAddress=tname;
            }else{
            	ComAddress="";
            }
        }
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("marriage");
        tLDCodeDB.setCode(tLDPersonDB.getMarriage());
        if(tLDCodeDB.getInfo()){
            Marriage +=tLDCodeDB.getCodeName();
        }
        LDOccupationDB tLDOccupationDB = new LDOccupationDB();
        tLDOccupationDB.setOccupationCode(tLDPersonDB.getOccupationCode());
        if(tLDOccupationDB.getInfo()){
            Occupation += tLDOccupationDB.getOccupationName();
        }
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLLSurveySchema.getStartMan());
        if(tLDUserDB.getInfo()){
            StartMan = tLDUserDB.getUserName();
        }
        tLDUserDB.setUserCode(tLLSurveySchema.getConfer());
        if(tLDUserDB.getInfo()){
            ConfMan = tLDUserDB.getUserName();
        }
        tLDUserDB.setUserCode(tLLSurveySchema.getSurveyOperator());
        if(tLDUserDB.getInfo()){
            SurveyMan = tLDUserDB.getUserName();
        }
        mLLAppClaimReasonDB.setCaseNo(aCaseNo);
        mLLAppClaimReasonSet.set(mLLAppClaimReasonDB.query());
        String AppClaimReason = "";
        if (mLLAppClaimReasonSet == null || mLLAppClaimReasonSet.size() == 0)
        {
            mErrors.copyAllErrors(mLLAppClaimReasonSet.mErrors);
            buildError("outputXML", "该案件没有录入申请原因");
        }
        else
        {
            for (int i = 1; i <= mLLAppClaimReasonSet.size(); i++)
            {
                tLDCodeDB.setCode(mLLAppClaimReasonSet.get(i).getReasonCode());
                tLDCodeDB.setCodeType("llrgtreason");
                if(tLDCodeDB.getInfo())
                    AppClaimReason += i+". "+tLDCodeDB.getCodeName()+" ";
            }
        }
        String ohresult=tLLSurveySchema.getOHresult();
        if(ohresult==null || "null".equals(ohresult)){
        	ohresult="";
        }
        Result = "院内调查信息：\n"+tLLSurveySchema.getresult()+"\n\n"
                 +"院外调查信息:\n"+ohresult;
        ConfNote = tLLSurveySchema.getConfNote();
        //其它模版上单独不成块的信息
        ListTable  appColListTable = new ListTable();
        appColListTable.setName("AFFIX");
        String[] Title = {"CERNAME", "DATE","COPIES"};
        String[] strCol;
        LLInqCertificateDB tLLInqCertificateDB = new LLInqCertificateDB();
        LLInqCertificateSet tLLInqCertificateSet = new LLInqCertificateSet();
        tLLInqCertificateDB.setSurveyNo(aSurveyNo);
        tLLInqCertificateSet = tLLInqCertificateDB.query();
        if(tLLInqCertificateSet.size()>0){
            for (int i=1;i<=tLLInqCertificateSet.size();i++){
                LLInqCertificateSchema aLLInqCertificateSchema = tLLInqCertificateSet.get(i);
                strCol = new String[3];
                strCol[0] = aLLInqCertificateSchema.getRemark();
                strCol[1] = aLLInqCertificateSchema.getMakeDate();
                strCol[2] = ""+aLLInqCertificateSchema.getCerCount();
                appColListTable.add(strCol);
            }
        }

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("SurveyReportPrt.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
//        StrTool tSrtTool = new StrTool();
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";

        //PayAppPrtBL模版元素
        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1", "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("CaseNo", mLLCaseSchema.getCaseNo()); //理赔号
        texttag.add("CustomerName", mLLCaseSchema.getCustomerName()); //客户姓名
        texttag.add("CustomerSex",
                    (mLLCaseSchema.getCustomerSex().equals("0") ? "男" : "女")); //客户性别是不是01
        texttag.add("Marriage", Marriage); //婚姻
        texttag.add("Occupation", Occupation); //职业
        texttag.add("ComName", ComName); //单位名称
        texttag.add("ComAddress", ComAddress); //单位地址
        texttag.add("IDNo", mLLCaseSchema.getIDNo()); //证件号码
        texttag.add("Phone", aLCAddressSchema.getPhone()); //联系电话
        texttag.add("Mobile", aLCAddressSchema.getMobile()); //联系电话
        texttag.add("HomeAddress", aLCAddressSchema.getHomeAddress()); //联系电话
        texttag.add("PostalAddress", mLLCaseSchema.getPostalAddress()); //联系地址
        texttag.add("RgtDate", mLLRegisterSchema.getRgtDate()); //申请时间
        texttag.add("RgtantName", mLLRegisterSchema.getRgtantName()); //申请人姓名
        texttag.add("RgtantPhone", mLLRegisterSchema.getRgtantPhone()); //申请人联系方式
        texttag.add("AppClaimReason", AppClaimReason); //申请原因
        texttag.add("SurvTimes", SurvTimes);
        texttag.add("AccdentDesc", "\n" + tLLSurveySchema.getAccidentDesc()); //事故信息
        texttag.add("StartMan", StartMan); //提调人
        texttag.add("ConfMan", ConfMan); //提调人
        texttag.add("SurveyMan",SurveyMan);
        texttag.add("SurveyStartDate", tLLSurveySchema.getSurveyStartDate()); //调查开始日期
        texttag.add("SurveyEndDate", tLLSurveySchema.getSurveyEndDate()); //调查结束日期
        texttag.add("Content", tLLSurveySchema.getContent()); //提调信息
        texttag.add("Result", Result); //调查信息
        texttag.add("ConfNote", ConfNote); //调查结论
        texttag.add("Operator", mGlobalInput.Operator); //理赔经办人
        texttag.add("SysDate", SysDate); //系统时间

   if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息

//        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args)
    {

        SurveyReportPrtBL tSurveyReportPrtBL = new SurveyReportPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo("C53000602170000031");
        tVData.addElement(tLOPRTManagerSchema);
        tSurveyReportPrtBL.submitData(tVData, "PRINT");
        VData vdata = tSurveyReportPrtBL.getResult();
    }

}
