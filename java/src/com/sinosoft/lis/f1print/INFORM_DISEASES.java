package com.sinosoft.lis.f1print;

import com.cbsws.core.xml.xsch.BaseXmlSch;

public class INFORM_DISEASES extends BaseXmlSch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**年龄*/
	private int age;
	/**体重*/
	private double weight;
	/**身高*/
	private double height;
	/**告知*/
	private String[] item;
	
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public String[] getItem() {
		return item;
	}
	public void setItem(String[] item) {
		this.item = item;
	}
	
	

}
