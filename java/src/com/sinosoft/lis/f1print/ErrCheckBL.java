/**
 * <p>Title:需要输入统计项的的扫描报表 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ErrCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String[] mDay = null;
    private String mScanOper = "";
    private String mDefineCode = "";
    private String mManageCom = "";

    /** 业务处理相关变量，在initCommon()中初始化*/
    private String mNO; //序号
    private String mPrtNO; //印刷号

    /**构造函数*/
    public ErrCheckBL()
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    { //打印

        //全局变量
        mDay = (String[]) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        mScanOper = (String) cInputData.get(2);
        mDefineCode = (String) cInputData.get(3);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ErrCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**传输数据的公共方法*/
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");

            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        if (!getPrintDataPay())
        {
            return false;
        }

        return true;
    }

    private boolean getPrintDataPay()
    {
        System.out.println("报表代码类型：" + mDefineCode);

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ErrorReport.vts", "printer");

        ListTable tlistTable = new ListTable();
        tlistTable.setName("Err");

        String[] strArr = null;
        int nIndex = 4; // 列表数据的列数
        //以录入时间到统计段内的个险（或银代）的主险的首期的录入总件数
        String New_sql =
                "select lcpol.operator from lcpol where  lcpol.grppolno='00000000000000000000'" +
                " and lcpol.salechnl in ('02','03') and  lcpol.RenewCount='0' and lcpol.polno=lcpol.mainpolno" +
                ReportPubFun.getWherePart("lcpol.makedate", mDay[0], mDay[1], 1);

        //差错总条数:以录入时间到统计段内的个险（或银代）的主险的首期的问题件总条数
        String WT_sql = "select lcpol.operator from lcissuepol,lcpol" +
                        " where lcpol.proposalno=lcissuepol.proposalno and lcpol.grppolno='00000000000000000000'" +
                        " and lcpol.salechnl in ('02','03') and  lcpol.RenewCount='0' and lcpol.polno=lcpol.mainpolno" +
                        //" and lcissuepol.backobjtype in ('2','3') "+
                        " and lcissuepol.issuetype='700'" +
                        ReportPubFun.getWherePart("lcpol.makedate", mDay[0],
                                                  mDay[1], 1);

        //以唯一录入人员为查询线索
        String tSql = "select distinct(lcpol.operator) from lcpol " +
                      " where  lcpol.grppolno='00000000000000000000'" +
                      " and lcpol.salechnl in ('02','03') and lcpol.polno=lcpol.mainpolno" +
                      ReportPubFun.getWherePart("lcpol.makedate", mDay[0],
                                                mDay[1], 1);

        SSRS XDSSRS = new SSRS();
        SSRS CCSSRS = new SSRS();

        //************************内部录入清单*********************************************
         if (mDefineCode.equals("InErr"))
         {
             String InErrSql = ReportPubFun.getWherePartLike("lcpol.managecom",
                     mManageCom) +
                               ReportPubFun.getWherePart("lcpol.operator",
                     mScanOper);

             New_sql = New_sql + InErrSql;
             New_sql = New_sql + " UNION ALL " +
                       StrTool.replace(New_sql, "lcpol", "lbpol");
             System.out.println(
                     "-------------------录入件数-------------------------" +
                     New_sql);

             ExeSQL xdExeSQL = new ExeSQL();
             XDSSRS = xdExeSQL.execSQL(New_sql);

             WT_sql = WT_sql + InErrSql;
             WT_sql = WT_sql + " UNION ALL " +
                      StrTool.replace(WT_sql, "lcpol", "lbpol");
             System.out.println(
                     "-------------------差错总条数-------------------------" +
                     WT_sql);

             ExeSQL ccExeSQL = new ExeSQL();
             CCSSRS = ccExeSQL.execSQL(WT_sql);

             tSql = tSql + InErrSql;
             System.out.println("寻找唯一的(操作人）的tsql:" + tSql);
         }

        //************************外包录入组清单*********************************************
         if (mDefineCode.equals("OutErr"))
         {
             String OutErrSql = "";

             //是否选择工号
             if ((mScanOper == null) || mScanOper.equals(""))
             { //没有选择工号：按照外包录入组的所有工号进行统计
                 OutErrSql = " AND lcpol.operator " +
                             " In ( select usercode from lduserTomenuGrp where menugrpcode='" +
                             mManageCom + "')";
             }
             else
             { //选择工号：按照工号进行统计
                 OutErrSql = " AND  lcpol.operator In('" + mScanOper + "') ";
             }

             New_sql = New_sql + OutErrSql;
             New_sql = New_sql + " UNION ALL " +
                       StrTool.replace(New_sql, "lcpol", "lbpol");
             System.out.println(
                     "-------------------录入件数-------------------------" +
                     New_sql);

             ExeSQL xdExeSQL = new ExeSQL();
             XDSSRS = xdExeSQL.execSQL(New_sql);

             WT_sql = WT_sql + OutErrSql;
             WT_sql = WT_sql + " UNION ALL " +
                      StrTool.replace(WT_sql, "lcpol", "lbpol");
             System.out.println(
                     "-------------------差错总条数-------------------------" +
                     WT_sql);

             ExeSQL ccExeSQL = new ExeSQL();
             CCSSRS = ccExeSQL.execSQL(WT_sql);

             tSql = tSql + OutErrSql;

             System.out.println("寻找唯一的(操作人）的tsql:" + tSql);
         }

        ExeSQL kExeSQL = new ExeSQL();
        SSRS kSSRS = new SSRS();
        kSSRS = kExeSQL.execSQL(tSql);

        double RDJ = 0;
        double CCJ = 0;

        for (int i = 1; i <= kSSRS.MaxRow; i++)
        {
            strArr = new String[nIndex];
            strArr[0] = kSSRS.GetText(i, 1); //工号
            strArr[1] = ReportPubFun.getCaseCount(XDSSRS, kSSRS.GetText(i, 1)); //录入件数
            strArr[2] = ReportPubFun.getCaseCount(CCSSRS, kSSRS.GetText(i, 1)); //差错总条数
            strArr[3] = ReportPubFun.functionDivision(strArr[2], strArr[1],
                    "0.0000"); //差错率
            tlistTable.add(strArr);

            RDJ = RDJ + ReportPubFun.functionDouble(strArr[1]);
            CCJ = CCJ + ReportPubFun.functionDouble(strArr[2]);
        }

        strArr = new String[nIndex];
        strArr[0] = "合计";
        strArr[1] = new DecimalFormat("0").format(RDJ);
        strArr[2] = new DecimalFormat("0").format(CCJ);
        strArr[3] = ReportPubFun.functionDivision(strArr[2], strArr[1],
                                                  "0.0000"); //差错率
        tlistTable.add(strArr);
        strArr = new String[nIndex];
        strArr[0] = "GongHao";
        strArr[1] = "RuJS";
        strArr[2] = "CCJS";
        strArr[3] = "CCL";

        xmlexport.addListTable(tlistTable, strArr);

        String CurrentDate = PubFun.getCurrentDate();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);

        if (mManageCom.equals("WBGrp"))
        {
            texttag.add("ManageCom", "外包录入组");
        }
        else
        {
            texttag.add("ManageCom", ReportPubFun.getMngName(mManageCom));
        }

        texttag.add("Operator", mScanOper);
        texttag.add("time", CurrentDate);
        System.out.println("大小" + texttag.size());

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }
}
