package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;
import java.sql.*;

public class LARiskAccountReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private String mStartDay = "";
    private String mEndDay = "";
    private String Year1 = "";
    private String Year2 = "";
    private String Year3 = "";
    private String RiskName = "";
    private String mRiskCode = "";
    private String premiums1 = "";
    private String premiums2 = "";
    private String premiums3 = "";
    private String premiumstotal = "";
    private String Income1 = "";
    private String Income2 = "";
    private String Income3 = "";
    private String Incometotal = "";
    private String Commison1 = "";
    private String Commison2 = "";
    private String Commison3 = "";
    private String Commisontotal = "";
    private String Recovers1 = "";
    private String Recovers2 = "";
    private String Recovers3 = "";
    private String Recoverstotal = "";
    private String Outgo1 = "";
    private String Outgo2 = "";
    private String Outgo3 = "";
    private String Outgototal = "";
    private String total1 = "";
    private String total2 = "";
    private String total3 = "";
    private String total4 = "";
    private String share1 = "";
    private String share2 = "";
    private String share3 = "";
    private String rc1 = "";
    private String rc2 = "";
    private String rc3 = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据的必要验证
        if (!check()) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

        return true;
    }

    /**
     * 验证
     * @return boolean
     */
    private boolean check() {
        return true;
    }

    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean getDataList() {
        DecimalFormat tDF = new DecimalFormat("0.##");
        String premiums1SQL =
                "select sum(A) from (select e.EdorBackFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year1 + "-1-1' and '" + Year1 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
                "select d.CessPrem A from lrpol a, lrpoldetail c, lrpolresult d"
                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
                + " and a.RiskCode = '" + mRiskCode + "'" +
                " and (a.GetDataDate between '" + Year1 + "-1-1' and '" +
                Year1 + "-12-31' and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";
        try {
            premiums1 = "" + tDF.format(execQuery(premiums1SQL));
        } catch (Exception ex) {
            System.out.println("getpremiums1 出错！");
        }
        String premiums2SQL =
                "select sum(A) from (select e.EdorBackFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year2 + "-1-1' and '" + Year2 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
                "select d.CessPrem A from lrpol a, lrpoldetail c, lrpolresult d"
                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
                + " and a.RiskCode = '" + mRiskCode + "'" +
                " and (a.GetDataDate between '" + Year2 + "-1-1' and '" +
                Year2 + "-12-31' and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";
        try {
            premiums2 = "" + tDF.format(execQuery(premiums2SQL));
        } catch (Exception ex) {
            System.out.println("getpremiums2 出错！");
        }
        String premiums3SQL =
                "select sum(A) from (select e.EdorBackFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year3 + "-1-1' and '" + Year3 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
//                "select d.CessPrem A from lrpol a, lrpoldetail c, lrpolresult d"
//                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
//                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
//                + " and a.RiskCode = '" + mRiskCode + "'"
//                + " and((a.GetDataDate between '" + mStartDay +
//                "' and '" + mEndDay + "' and c.GetDataDate  <= '" +
//                mEndDay + "') or (a.GetDataDate >= '" + Year3 +
//                "-1-1' and a.GetDataDate  < '" + mStartDay +
//                "' and c.GetDataDate between '" + mStartDay +
//                "' and '" + mEndDay + "'))) as X  with ur";
                "select d.CessPrem A from lrpol a, lrpolresult d"
                + " where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ReNewCount = d.ReNewCount"
                + " and a.RiskCode = '" + mRiskCode + "'"
                + " and (a.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";

        try {
            premiums3 = "" + tDF.format(execQuery(premiums3SQL));
        } catch (Exception ex) {
            System.out.println("getpremiums3 出错！");
        }

        Income1 = premiums1;
        Income2 = premiums2;
        Income3 = premiums3;

        String Commison1SQL =
                "select sum(A) from (select e.EdorProcFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year1 + "-1-1' and '" + Year1 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
                "select d.ReProcFee A from lrpol a, lrpoldetail c, lrpolresult d"
                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
                + " and a.RiskCode = '" + mRiskCode + "'" +
                " and (a.GetDataDate between '" + Year1 + "-1-1' and '" +
                Year1 + "-12-31' and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";
        try {
            Commison1 = "" + tDF.format(execQuery(Commison1SQL));
        } catch (Exception ex) {
            System.out.println("getIncometotal 出错！");
        }
        String Commison2SQL =
                "select sum(A) from (select e.EdorProcFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year2 + "-1-1' and '" + Year2 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
                "select d.ReProcFee A from lrpol a, lrpoldetail c, lrpolresult d"
                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
                + " and a.RiskCode = '" + mRiskCode + "'" +
                " and (a.GetDataDate between '" + Year2 + "-1-1' and '" +
                Year2 + "-12-31' and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";

        try {
            Commison2 = "" + tDF.format(execQuery(Commison2SQL));
        } catch (Exception ex) {
            System.out.println("getCommison2 出错！");
        }
        String Commison3SQL =
                "select sum(A) from (select e.EdorProcFee A from LRPol a, LRPolEdor c, LRPolEdorResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year3 + "-1-1' and '" + Year3 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' union all " +
//                "select d.ReProcFee A from lrpol a, lrpoldetail c, lrpolresult d"
//                + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
//                + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
//                + " and a.RiskCode = '" + mRiskCode + "'"
//                + " and((a.GetDataDate between '" + mStartDay +
//                "' and '" + mEndDay + "' and c.GetDataDate  <= '" +
//                mEndDay + "') or (a.GetDataDate >= '" + Year3 +
//                "-1-1' and a.GetDataDate  < '" + mStartDay +
//                "' and c.GetDataDate between '" + mStartDay +
//                "' and '" + mEndDay + "'))) as X  with ur";
                "select d.ReProcFee A from lrpol a, lrpolresult d"
                + " where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ReNewCount = d.ReNewCount"
                + " and a.RiskCode = '" + mRiskCode + "'"
                + " and (a.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "')) as X  with ur";

        try {
            Commison3 = "" + tDF.format(execQuery(Commison3SQL));
        } catch (Exception ex) {
            System.out.println("getCommison3 出错！");
        }

        String Recovers1SQL =
                "select sum(e.ClaimBackFee) from LRPol a, LRPolClm c, LRPolClmResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and c.ClmNo = e.ClmNo and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year1 + "-1-1' and '" + Year1 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' with ur";
        try {
            Recovers1 = "" + tDF.format(execQuery(Recovers1SQL));
        } catch (Exception ex) {
            System.out.println("getRecovers1 出错！");
        }
        String Recovers2SQL =
                "select sum(e.ClaimBackFee) from LRPol a, LRPolClm c, LRPolClmResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and c.ClmNo = e.ClmNo and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year2 + "-1-1' and '" + Year2 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' with ur";
        try {
            Recovers2 = "" + tDF.format(execQuery(Recovers2SQL));
        } catch (Exception ex) {
            System.out.println("getRecovers2出错！");
        }
        String Recovers3SQL =
                "select sum(e.ClaimBackFee) from LRPol a, LRPolClm c, LRPolClmResult e" +
                " where a.PolNo = c.PolNo and a.polno = e.Polno and a.RecontCode = e.RecontCode and a.RecontCode = e.RecontCode" +
                " and c.ClmNo = e.ClmNo and a.RiskCode = '" + mRiskCode + "'" +
                " and a.GetDataDate between '" + Year3 + "-1-1' and '" + Year3 +
                "-12-31'" + " and c.GetDataDate between '" + mStartDay +
                "' and '" + mEndDay + "' with ur";
        try {
            Recovers3 = "" + tDF.format(execQuery(Recovers3SQL));
        } catch (Exception ex) {
            System.out.println("getRecovers3 出错！");
        }
        //总计
//        System.out.println(Double.parseDouble(premiums1) +
//                           Integer.parseInt(premiums2));
        premiumstotal = "" + tDF.format(Double.parseDouble(premiums1) +
                                        Double.parseDouble(premiums2) +
                                        Double.parseDouble(premiums3));
        Incometotal = "" + tDF.format(Double.parseDouble(Income1) +
                                      Double.parseDouble(Income2) +
                                      Double.parseDouble(Income3));
        Commisontotal = "" + tDF.format(Double.parseDouble(Commison1) +
                                        Double.parseDouble(Commison2) +
                                        Double.parseDouble(Commison3));
        Recoverstotal = "" + tDF.format(Double.parseDouble(Recovers1) +
                                        Double.parseDouble(Recovers2) +
                                        Double.parseDouble(Recovers3));
        Outgo1 = "" + tDF.format(Double.parseDouble(Commison1) +
                                 Double.parseDouble(Recovers1));
        Outgo2 = "" + tDF.format(Double.parseDouble(Commison2) +
                                 Double.parseDouble(Recovers2));
        Outgo3 = "" + tDF.format(Double.parseDouble(Commison3) +
                                 Double.parseDouble(Recovers3));
        Outgototal = "" + tDF.format(Double.parseDouble(Outgo1) +
                                     Double.parseDouble(Outgo2) +
                                     Double.parseDouble(Outgo3));
        total1 = "" + tDF.format(Double.parseDouble(Income1) -
                                 Double.parseDouble(Outgo1));
        total2 = "" + tDF.format(Double.parseDouble(Income2) -
                                 Double.parseDouble(Outgo2));
        total3 = "" + tDF.format(Double.parseDouble(Income3) -
                                 Double.parseDouble(Outgo3));
        total4 = "" + tDF.format(Double.parseDouble(Incometotal) -
                                 Double.parseDouble(Outgototal));
        String share1SQL = "select double(b.factorvalue)*100 from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                           "and b.factorcode = 'CessionRate' and b.riskcode = '" +
                           mRiskCode +
                           "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                           Year1 +
                           ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                           Year1 + " and year(Rinvalidate) >=" + Year1 +
                           ")) with ur";
        this.share1 = new ExeSQL().getOneValue(share1SQL);
        String share2SQL = "select double(b.factorvalue)*100 from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                           "and b.factorcode = 'CessionRate' and b.riskcode = '" +
                           mRiskCode +
                           "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                           Year2 +
                           ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                           Year2 + " and year(Rinvalidate) >=" + Year2 +
                           ")) with ur";
        this.share2 = new ExeSQL().getOneValue(share2SQL);
        String share3SQL = "select double(b.factorvalue)*100 from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                           "and b.factorcode = 'CessionRate' and b.riskcode = '" +
                           mRiskCode +
                           "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                           Year3 +
                           ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                           Year3 + " and year(Rinvalidate) >=" + Year3 +
                           ")) with ur";
        this.share3 = new ExeSQL().getOneValue(share3SQL);
        String rc1SQL = "select integer(sum(double(b.factorvalue)*100)) from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                        "and b.factorcode in('ReinProcFeeRate','SpeComRate') and b.riskcode = '" +
                        mRiskCode +
                        "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                        Year1 +
                        ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                        Year1 + " and year(Rinvalidate) >=" + Year1 +
                        ")) with ur";
        this.rc1 = new ExeSQL().getOneValue(rc1SQL);
        String rc2SQL = "select integer(sum(double(b.factorvalue)*100)) from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                        "and b.factorcode in('ReinProcFeeRate','SpeComRate') and b.riskcode = '" +
                        mRiskCode +
                        "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                        Year2 +
                        ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                        Year2 + " and year(Rinvalidate) >=" + Year2 +
                        ")) with ur";
        this.rc2 = new ExeSQL().getOneValue(rc2SQL);
        String rc3SQL = "select integer(sum(double(b.factorvalue)*100)) from lrcontinfo a ,lrcalfactorvalue b where a.recontcode = b.recontcode " +
                        "and b.factorcode in('ReinProcFeeRate','SpeComRate') and b.riskcode = '" +
                        mRiskCode +
                        "' and ((a.Rinvalidate is null and year(Rvalidate) <= " +
                        Year3 +
                        ") or (a.Rinvalidate is not null and year(Rvalidate) <=" +
                        Year3 + " and year(Rinvalidate) >=" + Year3 +
                        ")) with ur";
        this.rc3 = new ExeSQL().getOneValue(rc3SQL);
        return true;
    }

    /**
     * 获得要打印数据的年份
     */
    private boolean getYear() {
        Year3 = mStartDay.substring(0, 4);
        Year2 = String.valueOf(Integer.parseInt(Year3) - 1);
        Year1 = String.valueOf(Integer.parseInt(Year3) - 2);
        return true;
    }

    /**
     * 获得险种名称
     */
    private boolean getRiskName() {
        String tSQL = "select riskname from lmriskapp where riskcode='" +
                      this.mRiskCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        RiskName = tExeSQL.getOneValue(tSQL);
        if (RiskName == null || RiskName.equals("")) {
            CError.buildErr(this, "险种描述出错！");
            return false;
        }
        return true;
    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        try {
            //获得需要打印的年份
            if (!getYear()) {
                return false;
            }
            // 1、查询数据
            if (!getDataList()) {
                return false;
            }
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("StartDay", this.mStartDay);
            texttag.add("EndDay", this.mEndDay);
            texttag.add("Year1", this.Year1);
            texttag.add("Year2", this.Year2);
            texttag.add("Year3", this.Year3);
            texttag.add("RiskName", this.mRiskCode);
            texttag.add("premiums1", this.premiums1);
            texttag.add("premiums2", this.premiums2);
            texttag.add("premiums3", this.premiums3);
            texttag.add("premiumstotal", this.premiumstotal);
            texttag.add("Income1", this.Income1);
            texttag.add("Income2", this.Income2);
            texttag.add("Income3", this.Income3);
            texttag.add("Incometotal", this.Incometotal);
            texttag.add("Commison1", this.Commison1);
            texttag.add("Commison2", this.Commison2);
            texttag.add("Commison3", this.Commison3);
            texttag.add("Commisontotal", this.Commisontotal);
            texttag.add("Recovers1", this.Recovers1);
            texttag.add("Recovers2", this.Recovers2);
            texttag.add("Recovers3", this.Recovers3);
            texttag.add("Recoverstotal", this.Recoverstotal);
            texttag.add("Outgo1", this.Outgo1);
            texttag.add("Outgo2", this.Outgo2);
            texttag.add("Outgo3", this.Outgo3);
            texttag.add("Outgototal", this.Outgototal);
            texttag.add("total1", this.total1);
            texttag.add("total2", this.total2);
            texttag.add("total3", this.total3);
            texttag.add("total4", this.total4);
            texttag.add("Share1", this.share1);
            texttag.add("Share2", this.share2);
            texttag.add("Share3", this.share3);
            texttag.add("RC1", this.rc1);
            texttag.add("RC2", this.rc2);
            texttag.add("RC3", this.rc3);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("RiskAccount.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0) {
                xmlexport.addTextTag(texttag); //添加动态文本标签
            }
            mResult.addElement(xmlexport);
        } catch (Exception ex) {
            buildError("queryData", "LAStatisticReportBL发生错误，准备数据时出错！");
            return false;
        }

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mStartDay = (String) pmInputData.get(0);
        mEndDay = (String) pmInputData.get(1);
        mRiskCode = (String) pmInputData.get(2);
        System.out.println(mStartDay + " / " + mEndDay + "/" + mRiskCode);

        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LAStatisticReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql) {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0.00;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0.00;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
              if (!conn.isClosed()) {
                  conn.close();
              }
              try {
                  st.close();
                  rs.close();
              } catch (Exception ex2) {
                  ex2.printStackTrace();
              }
              st = null;
              rs = null;
              conn = null;
            } catch (Exception e) {}

        }
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
