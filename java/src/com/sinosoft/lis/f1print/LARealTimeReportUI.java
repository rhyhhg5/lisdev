/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-11-23 09:58:25
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LARealTimeReportUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();

    public static void main(String[] agrs)
    {
        String StartDay = "2006-01-01";
        String EndDay = "2006-01-18";

        VData tVData = new VData();

        tVData.addElement(StartDay);
        tVData.addElement(EndDay);
        LARealTimeReportUI UI = new LARealTimeReportUI();
        if (!UI.submitData(tVData, "PRINT")) {
            if (UI.mErrors.needDealError()) {
                System.out.println(UI.mErrors.getFirstError());
            } else {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        } else {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
System.out.println("String LARealTimeReportBL...");
        LARealTimeReportBL tLARealTimeReportBL= new LARealTimeReportBL();
        if (!tLARealTimeReportBL.submitData(mInputData, mOperate))
        {
            if (tLARealTimeReportBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLARealTimeReportBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LAStatisticReportUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLARealTimeReportBL.getResult();
            return true;
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAStatisticReportUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
