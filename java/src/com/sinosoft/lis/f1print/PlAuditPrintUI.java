package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlAuditPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private String strBeginDate,strEndDate;
	private GlobalInput mGlobalInput = null;  //完整的操作员信息
	private String mManageCom ;

	public PlAuditPrintUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}

			if (!getInputData(cInputData)) {
				return false;
			}
			VData vData = new VData();

			if (!prepareOutputData(vData)) {
				return false;
			}

			PlAuditPrintBL tPlAuditPrintBL = new PlAuditPrintBL();
			System.out.println("Start PlAuditPrintUI Submit ...");
			if (!tPlAuditPrintBL.submitData(vData, cOperate)) {
				if (tPlAuditPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlAuditPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlAuditPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlAuditPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "PlAuditPrintBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}

	private boolean prepareOutputData(VData vData) {
		vData.clear();
		vData.add(strBeginDate);
		vData.add(strEndDate);
		vData.add(mGlobalInput);
		vData.add(mManageCom);
		return true;
	}

	private boolean getInputData(VData cInputData) {
		
		strBeginDate=(String) cInputData.get(0);
		strEndDate=(String) cInputData.get(1);
		mGlobalInput=(GlobalInput)cInputData.get(2);
		mManageCom = (String)cInputData.get(3);
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlAuditPrintUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}
}
