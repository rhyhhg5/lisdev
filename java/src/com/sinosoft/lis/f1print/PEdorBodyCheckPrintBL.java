package com.sinosoft.lis.f1print;

import java.io.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保体检通知书</p>
 * <p>Description: 保全人工核保体检通知书</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorBodyCheckPrintBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private MMap mMap = new MMap();

    private String mEdorNo = null;

    private String mAppntNo = null;

    private String mAppntName = null;

    private String mInsuredNo = null;

    private String mContNo = null;

    private String mHealthNo = null;

    private String mCheckDate = null;

    private String mHealthName = null;

    private String mPEItem = "";

    private String mNeedLimosis = null;

    private String mManageCom = null;

    private String mManageName = null;

    private String mManageAddress = null;

    private String mManageZipCode = null;

    private String mManageFax = null;

    private String mManagePhone = null;

    private String mAge = null;

    private String mSex = null;

    private String mIDType = null;

    private String mIDNo = null;

    private String mReason = null;

    private String mPost = null;

    private String mAddress = null;

    private String mAgentCode = null;

    private String mAgentName = null;

    private String mAgentPhone = null;

    private String mAgentGroup = null;

    private String mBranchAttr = null;

    private String mPEName = null;

    private String mPEPhone = null;

    private String mSendDate = null;

    private String mSysDate = CommonBL.decodeDate(PubFun.getCurrentDate());

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param prtNo String
     */
    public PEdorBodyCheckPrintBL(GlobalInput gi, String prtNo)
    {
        this.mGlobalInput = gi;
        this.mHealthNo = prtNo;
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!getPrintData())
        {
            return false;
        }
        if (!createXML())
        {
            return false;
        }
        if (!setPrintFlag())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到打印数据
     * @return boolean
     */
    private boolean getPrintData()
    {
        if (!getNoticeData())
        {
            return false;
        }
        if (!getPEItemData())
        {
            return false;
        }
        if (!getAddressData())
        {
            return false;
        }
        if (!getInsuredData())
        {
            return false;
        }
        if (!getManageComAddress())
        {
            return false;
        }
        if (!getAgentData())
        {
            return false;
        }
        if (!getPEPersonData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到体检通知书信息
     * @return boolean
     */
    private boolean getNoticeData()
    {
        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
        tLPPENoticeDB.setPrtSeq(mHealthNo);
        LPPENoticeSet tLPPENoticeSet = tLPPENoticeDB.query();
        if (tLPPENoticeSet.size() == 0)
        {
            mErrors.addOneError("未找到体检通知书信息！");
            return false;
        }
        LPPENoticeSchema tLPPENoticeSchema = tLPPENoticeSet.get(1);
        this.mAppntName = tLPPENoticeSchema.getAppName();
        this.mEdorNo = tLPPENoticeSchema.getEdorNo();
        this.mInsuredNo = tLPPENoticeSchema.getCustomerNo();
        this.mContNo = tLPPENoticeSchema.getContNo();
        this.mReason = tLPPENoticeSchema.getRemark();
        this.mManageCom = tLPPENoticeSchema.getManageCom();
        this.mSendDate = tLPPENoticeSchema.getMakeDate();
        return true;
    }

    /**
     * 得到体检项目信息
     * @return boolean
     */
    private boolean getPEItemData()
    {
        LPPENoticeItemDB tLPPENoticeItemDB = new LPPENoticeItemDB();
        tLPPENoticeItemDB.setPrtSeq(mHealthNo);
        LPPENoticeItemSet tLPPENoticeItemSet = tLPPENoticeItemDB.query();
        if (tLPPENoticeItemSet.size() == 0)
        {
            mErrors.addOneError("未找到体检项目信息！");
            return false;
        }
        //得到体检项目
        for (int i = 1; i <= tLPPENoticeItemSet.size(); i++)
        {
            LPPENoticeItemSchema tLPPENoticeItemSchema
                    = tLPPENoticeItemSet.get(i);
            if (i != tLPPENoticeItemSet.size())
            {
                this.mPEItem += tLPPENoticeItemSchema.getPEItemName() + ",";
            }
            else
            {
                this.mPEItem += tLPPENoticeItemSchema.getPEItemName() + "。";
            }
        }
        //得到空腹标记
        boolean flag = false;
        for (int i = 1; i <= tLPPENoticeItemSet.size(); i++)
        {
            LPPENoticeItemSchema tLPPENoticeItemSchema
                    = tLPPENoticeItemSet.get(i);
            String needLimosis = tLPPENoticeItemSchema.getFreePE();
            if ((needLimosis != null) && (needLimosis.equals("Y")))
            {
                flag = true;
                break;
            }
        }
        if (flag == true)
        {
            this.mNeedLimosis = "是";
        }
        else
        {
            this.mNeedLimosis = "否";
        }
        return true;
    }

    /**
     * 得到投保人联系地址信息
     * @return boolean
     */
    private boolean getAddressData()
    {
        LCAddressSchema tLCAddressSchema = CommonBL.getLCAddress(mContNo);
        if (tLCAddressSchema == null)
        {
            mErrors.addOneError("未找到投保人联系地址信息!");
            return false;
        }
        this.mPost = tLCAddressSchema.getZipCode();
        this.mAddress = tLCAddressSchema.getPostalAddress();
        return true;
    }

    /**
     * 得到体检人信息
     * @return boolean
     */
    private boolean getInsuredData()
    {
        LCInsuredSchema tLCInsuredSchema =
                CommonBL.getLCInsured(mContNo, mInsuredNo);
        if (tLCInsuredSchema == null)
        {
            mErrors.addOneError("未找到体检人信息!");
            return false;
        }
        this.mAppntNo = tLCInsuredSchema.getAppntNo();
        this.mHealthName = tLCInsuredSchema.getName();
        this.mSex = tLCInsuredSchema.getSex();
        this.mIDType = tLCInsuredSchema.getIDType();
        this.mIDNo = tLCInsuredSchema.getIDNo();
        this.mManageCom = tLCInsuredSchema.getManageCom();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        tLCPolDB.setInsuredNo(mInsuredNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() > 0)
        {
            this.mAge = String.valueOf(tLCPolSet.get(1).getInsuredAppAge());
        }
        return true;
    }

    /**
     * 得到管理机构信息
     * @return boolean
     */
    private boolean getManageComAddress()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mManageCom);
        if (!tLDComDB.getInfo())
        {
            mErrors.addOneError("未找到管理机构信息!");
            return false;
        }
        this.mManageName = tLDComDB.getLetterServiceName();
        this.mManageAddress = tLDComDB.getServicePostAddress();
        this.mManageZipCode = tLDComDB.getServicePostZipcode();
        this.mManageFax = tLDComDB.getFax();
        this.mManagePhone = tLDComDB.getServicePhone();
        return true;
    }

    /**
     * 得到代理人信息
     * @return boolean
     */
    private boolean getAgentData()
    {
        LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
        if (tLCContSchema == null)
        {
            mErrors.addOneError("未找到合同信息！");
            return false;
        }
        this.mAgentCode = tLCContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.addOneError("未找到代理人信息！");
            return false;
        }
        this.mAgentName = tLAAgentDB.getName();
        this.mAgentPhone = tLAAgentDB.getPhone();
        this.mAgentGroup = tLAAgentDB.getAgentGroup();
        if (mAgentPhone == null)
        {
            this.mAgentPhone = tLAAgentDB.getMobile();
        }

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(mAgentGroup);
        if (tLABranchGroupDB.getInfo())
        {
            this.mBranchAttr = tLABranchGroupDB.getBranchAttr();
        }
        return true;
    }

    /**
     * 体检专员,各个分公司不同目前没有定因体检专员权限
     * 暂时写死以后更新
     * @return boolean
     */
    private boolean getPEPersonData()
    {
        if (mManageCom.startsWith("8611"))
        {
            mPEName = "陈红晓";
            mPEPhone = "010-58790688";
        }
        else if (mManageCom.startsWith("8694"))
        {
            mPEName = "曹惠娟";
            mPEPhone = "0532-83079832";
        }
        else if (mManageCom.startsWith("8631"))
        {
            mPEName = "张钧实";
            mPEPhone = "021-61001166-8213";
        }
        else if (mManageCom.startsWith("8653"))
        {
            mPEName = "刘晓琳";
            mPEPhone = "0871-5165988-8138";
        }
        else if (mManageCom.startsWith("8695"))
        {
            mPEName = "张磊";
            mPEPhone = "0755-83830185";
        }
        else if (mManageCom.startsWith("8633"))
        {
            mPEName = "付清枫";
            mPEPhone = "0571-28918898-8082";
        }
        else if (mManageCom.startsWith("8637"))
        {
            mPEName = "庄朝辉";
            mPEPhone = "0531-89819857";
        }
        else if (mManageCom.startsWith("8632"))
        {
            mPEName = "张迎新";
            mPEPhone = "025-83212999-8702";
        }
        else if (mManageCom.startsWith("8621"))
        {
            mPEName = "许艳丽";
            mPEPhone = "024-62251776";
        }
        return true;
    }

    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PEdorPENotice.vts", "printer");
        addTags();
        addTableList();
        return true;
    }

    /**
     * 添加打印文本数据
     */
    private void addTags()
    {
        TextTag tag = new TextTag();
        tag.add("EdorNo", StrTool.cTrim(mEdorNo));
        tag.add("HealthNo", StrTool.cTrim(mHealthNo));
        tag.add("AppntNo", mAppntNo);
        tag.add("AppntName", mAppntName);
        tag.add("Reason", StrTool.cTrim(mReason));
        tag.add("CheckDate", StrTool.cTrim(mCheckDate));
        tag.add("PEITEM", StrTool.cTrim(mPEItem));
        tag.add("NeedLimosis", StrTool.cTrim(mNeedLimosis)); //是否需要空腹
        tag.add("ManageCom", StrTool.cTrim(mManageName));
        tag.add("ManageAddress", StrTool.cTrim(mManageAddress));
        tag.add("ManageZipCode", StrTool.cTrim(mManageZipCode));
        tag.add("ManageFax", StrTool.cTrim(mManageFax));
        tag.add("ManagePhone", StrTool.cTrim(mManagePhone));
        tag.add("BarCode1", StrTool.cTrim(mHealthNo));
        tag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        tag.add("Post", StrTool.cTrim(mPost)); //投保人邮政编码
        tag.add("Address", StrTool.cTrim(mAddress)); //投保人地址
        tag.add("AppntName", StrTool.cTrim(mAppntName));
        tag.add("ContNo", StrTool.cTrim(mContNo));
        tag.add("HealthName", StrTool.cTrim(mHealthName)); //被保险人名称
        tag.add("Sex", ChangeCodeBL.getCodeName("sex", mSex)); //体检人性别
        tag.add("Age", StrTool.cTrim(mAge + "岁")); //体检人年龄
        tag.add("IDType", ChangeCodeBL.getCodeName("IdType", StrTool.cTrim(mIDType)));
        tag.add("IDNo", StrTool.cTrim(mIDNo));
        tag.add("AgentPhone", StrTool.cTrim(mAgentPhone));
        tag.add("AgentName", StrTool.cTrim(mAgentName));
	    //2014-11-2    杨阳
	    //业务员号友显示LAagent表中GroupAgentcode字段
	    String groupAgentCode = new ExeSQL().getOneValue("select getUniteCode("+mAgentCode+") from dual with ur");
        tag.add("AgentCode", StrTool.cTrim(groupAgentCode));
        tag.add("BranchAttr", StrTool.cTrim(mBranchAttr));
        tag.add("PEName", StrTool.cTrim(mPEName));
        tag.add("PEPhone", StrTool.cTrim(mPEPhone));
        tag.add("SysDate", StrTool.cTrim(mSysDate));
        tag.add("EndDate", CommonBL.decodeDate(FDate.toString(CommonBL.changeDate(mSendDate, 5)))); //体检截止日期
        xml.addTextTag(tag);
    }

    /**
     * 得到体检医院信息
     */
    private void addTableList()
    {
        String sql = "select c.* from " +
                "lhgroupcont a,lhcontitem b,ldhospital c " +
                "where a.Contrano = b.Contrano " +
                "and b.DutyItemCode = 'HB0001' " +
                "and Dutystate = '1' " +
                "and c.HospitCode = a.hospitcode " +
                "and c.managecom = '" + mManageCom.substring(0, 4) + "'";
        System.out.println(sql);
        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        LDHospitalSet tLDHospitalSet = tLDHospitalDB.executeQuery(sql);
        ListTable tHospitalListTable = new ListTable();
        tHospitalListTable.setName("Hospital");
        for (int n = 1; n <= tLDHospitalSet.size(); n++)
        {
            String[] detail = new String[3];
            detail[0] = tLDHospitalSet.get(n).getHospitName();
            if (tLDHospitalSet.get(n).getAddress() != null)
            {
                detail[1] = tLDHospitalSet.get(n).getAddress();
            }
            else
            {
                detail[1] = "";
            }
            detail[2] = tLDHospitalSet.get(n).getPhone() == null ? "" :
                    tLDHospitalSet.get(n).getPhone();
            tHospitalListTable.add(detail);
        }
        xml.addListTable(tHospitalListTable, new String[3]);
    }

    /**
     * 设置打印标志
     * @return boolean
     */
    private boolean setPrintFlag()
    {
        String sql = "update LOPRTManager " +
                "set StateFlag = '" + BQ.PRINTFLAG_Y + "' " +
                "where PrtSeq = '" + mHealthNo + "'";
        mMap.put(sql, "UPDATE");
        sql = "update LPPENotice " +
                "set PrintFlag = '" + BQ.PRINTFLAG_Y + "', " +
                "    PEState = '" + BQ.PESTATE_PRINT + "' " +
                "where PrtSeq = '" + mHealthNo + "'";
        mMap.put(sql, "UPDATE");
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
