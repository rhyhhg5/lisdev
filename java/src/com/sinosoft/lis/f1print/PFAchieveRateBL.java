package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PFAchieveRateBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
//  应收起始日期
  private String mStartDate="";
  private String mEndDate="";
//  实收起始日期
  private String mActuStartDate="";
  private String mActuEndDate="";
//  其他要素
  private String mSaleChnlType = "";
  private String mRiskType = "";
  private String mSaleChnl = "";
  private String mOrphans = "";
  private String mPayCount = "";
  private String mManageCom=""; 
  
//  子算法SQL
  private String msubpolsql = "";
  private String msubpaypersonsql=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public PFAchieveRateBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
    	//cOperate为1为普通保单,2为万能保单
      if( !cOperate.equals("1")&&!cOperate.equals("2") ) {
        buildError("submitData", "不支持的操作字符串");
        return null;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

   // 获取各项条件SQL
      getsubsql();
      
      if(cOperate.equals("1")){
      // 准备所有要打印的数据
      if( !getPrintData() ) {
        return null;
      }
      }
      else if(cOperate.equals("2")){
          // 准备所有要打印的数据
          if( !OmnigetPrintData() ) {
            return null;
          }}
      else{
    	  buildError("dealdata", "险种类型错误");
          return null;
      }

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  PFAchieveRateBL tbl =new PFAchieveRateBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-08");
      tTransferData.setNameAndValue("EndDate", "2010-10-08");
      tTransferData.setNameAndValue("ManageCom", "8644");
      tGlobalInput.ManageCom="8644";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"1");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }

  /**
   * 获取所有条件子SQL
   */
  private void getsubsql()
  {
//		管理机构
	  msubpolsql=" and lcp.managecom like '"+mManageCom+"%' ";
      msubpaypersonsql=" and ljsp.managecom like '"+mManageCom+"%' ";
      
//		添加对于payintv<>0的校验
	  msubpolsql+=" and lcp.payintv<>0 ";
      msubpaypersonsql+=" and ljsp.payintv<>0 ";
      
//    应收时间
      msubpolsql+=" and lcp.paytodate between '" + mStartDate + "' and '" + mEndDate + "' ";
      msubpaypersonsql+=" and ljsp.lastpaytodate between '" + mStartDate + "' and '" + mEndDate + "' ";
      
//    实收时间
      if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(!(mActuEndDate.equals("")||mActuEndDate==null)))  {
//    	  msubpolsql=" and exists (select 1 from ljapayperson where polno=lcp.polno and confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "') ";
    	  msubpolsql+=" and 1=2 ";
          msubpaypersonsql+=" and ljsp.confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "' ";         
        }
       
//      if(mSaleChnl.equals("99")){
//    	  msubpolsql+=" and lcp.salechnl not in ('01','03','04','07','08','11','13') ";
//          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and salechnl not in ('01','03','04','07','08','11','13')) ";
//      }
//      else 
      if(mSaleChnl.equals("00")){
    	  System.out.println("选择为所有渠道");
    	  msubpolsql+=" and lcp.salechnl in ('01','04','10','13','14','15') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl  in ('01','04','10','13','14','15')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('01','04','10','13','14','15')) ";
      }
      else{
    	  msubpolsql+=" and lcp.salechnl='"+mSaleChnl+"' ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl='"+mSaleChnl+"') ";
      }
      
      if(mSaleChnlType.equals("0")){
    	  msubpolsql+=" and lcp.salechnl in ('01','04','10','13','14','15') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl  in ('01','04','10','13','14','15')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('01','04','10','13','14','15')) ";
      } 
      else if(mSaleChnlType.equals("1")){
    	  msubpolsql+=" and lcp.salechnl in ('01','10') ";
          msubpaypersonsql+="  and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('01','10') union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('01','10'))  ";
      }
      else if(mSaleChnlType.equals("2")){
    	  msubpolsql+=" and lcp.salechnl in ('04','13') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('04','13')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('04','13')) ";
      }
      else if(mSaleChnlType.equals("3")){
    	  msubpolsql+=" and lcp.salechnl in ('14','15') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('14','15')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('14','15')) ";
      }
      
      if(mOrphans.equals("1")){
    	  msubpolsql+="  and not exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = lcp.AgentCode)<=(select SignDate from lccont where ContNo=lcp.ContNo ) ";
          msubpaypersonsql+="  and not exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = ljsp.AgentCode)<=(select SignDate from lccont where ContNo=ljsp.ContNo )  ";
      }
      else if(mOrphans.equals("2")){
    	  msubpolsql+=" and (exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = lcp.AgentCode)>(select SignDate from lccont where ContNo=lcp.ContNo )) ";
          msubpaypersonsql+=" and (exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = ljsp.AgentCode)>(select SignDate from lccont where ContNo=ljsp.ContNo )) ";
      }
      
      if(mPayCount.equals("2")){
    	  msubpolsql+="  and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=2 ";
          msubpaypersonsql+="  and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=2  ";
      }
      else if(mPayCount.equals("3")){
    	  msubpolsql+=" and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=3 ";
          msubpaypersonsql+=" and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=3 ";
      }
      else if(mPayCount.equals("4")){
    	  msubpolsql+=" and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )>3 ";
          msubpaypersonsql+=" and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )>3 ";
      }
      
  }

  
  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
    mStartDate = (String) mTransferData.getValueByName("DueStartDate");
    mEndDate = (String) mTransferData.getValueByName("DueEndDate");
    if( mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期不能为空！");
        return false;
      }
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mManageCom.equals("")||mManageCom==null) {
        buildError("getInputData", "没有得到足够的信息:管理机构不能为空！");
        return false;
      }
    mActuStartDate = (String) mTransferData.getValueByName("ActuStartDate");
    mActuEndDate = (String) mTransferData.getValueByName("ActuEndDate");
    if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(mActuEndDate.equals("")||mActuEndDate==null) ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
        return false;
      }
    if( (!(mActuEndDate.equals("")||mActuEndDate==null))&&(mActuStartDate.equals("")||mActuStartDate==null) ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
        return false;
      }
    mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
    if( mSaleChnl.equals("")||mSaleChnl==null) {
        buildError("getInputData", "没有得到足够的信息:销售渠道不能为空！");
        return false;
      }
    mRiskType = (String) mTransferData.getValueByName("RiskType");
    if( mRiskType.equals("")||mRiskType==null) {
        buildError("getInputData", "没有得到足够的信息:险种类型不能为空！");
        return false;
      }
    mSaleChnlType = (String) mTransferData.getValueByName("SaleChnlType");
    if( mSaleChnlType.equals("")||mSaleChnlType==null) {
        buildError("getInputData", "没有得到足够的信息:保单类型不能为空！");
        return false;
      }
    mOrphans = (String) mTransferData.getValueByName("Orphans");
    if( mOrphans.equals("")||mOrphans==null) {
        buildError("getInputData", "没有得到足够的信息:保单状态不能为空！");
        return false;
      }
    mPayCount = (String) mTransferData.getValueByName("PayCount");
    if( mPayCount.equals("")||mPayCount==null) {
        buildError("getInputData", "没有得到足够的信息:缴费次数不能为空！");
        return false;
      }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","支公司代码","支公司","实收保费","实收件数"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select  " +
		" substr(x.managecode,1,4) 机构代码, " +
  		" (select name from ldcom where comcode=substr(x.managecode,1,4)) 分公司, " +
  		" x.managecode 支公司代码, " +
  		" (select name from ldcom where comcode=x.managecode) 支公司, " +
  		" sum(x.actusum) 实收保费, " +
  		" count(distinct x.actucount) 实收件数  " +
  		" from  " +
  		" (select  " +
  		" ljsp.managecom managecode, " +
  		" ljsp.sumactupaymoney actusum, " +
  		" ljsp.contno actucount " +
  		" from ljapayperson ljsp " +
  		" where 1=1  " +
  		msubpaypersonsql +
  		" and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4!='4') " +
  		" and sumactupaymoney > 0 " +
  		" and exists (select 1 from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate) "+
  		" and exists (select 1 from lpedoritem where edortype in ('FX','MF') and edorno=(select incomeno from ljapay where incometype='10' and payno=ljsp.payno)) " +
  		" ) as x " +
  		"  group by x.managecode with ur ";
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }

  private boolean OmnigetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","支公司代码","支公司","实收保费","实收件数"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
    //获得EXCEL列信息      
      String tSQL="select  " +
		" substr(x.managecode,1,4) 机构代码, " +
		" (select name from ldcom where comcode=substr(x.managecode,1,4)) 分公司, " +
		" x.managecode 支公司代码, " +
		" (select name from ldcom where comcode=x.managecode) 支公司, " +
		" sum(x.actusum) 实收保费, " +
		" count(distinct x.actucount) 实收件数  " +
  		" from  " +
  		" (select  " +
  		" ljsp.managecom managecode, " +
  		" ljsp.sumactupaymoney actusum, " +
  		" ljsp.contno actucount " +
  		" from ljapayperson ljsp " +
  		" where 1=1  " +
  		msubpaypersonsql +
  		" and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4='4') " +
  		" and exists (select 1 from lpedoritem where edortype in ('FX','MF') and edorno=(select incomeno from ljapay where incometype='10' and payno=ljsp.payno)) " +
  		" and exists (select 1 from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate) "+
  		" and sumactupaymoney > 0 " +
  		" ) as x " +
  		"  group by x.managecode with ur ";
      
      
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
          
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }

      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }
  
  /**
   * 得到通过传入参数得到相关信息
   * @param strComCode
   * @return
   * @throws Exception
   */
  private boolean getExcelData()
  {

	  String tSQL="select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'" +
	  		" and managecom like '' and signdate between '' and '' with ur ";
	  ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
          CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
          mErrors.addOneError(tError);
          return false;
      }
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
    	  return false;
      }
      return true;
  }
  
  
}

