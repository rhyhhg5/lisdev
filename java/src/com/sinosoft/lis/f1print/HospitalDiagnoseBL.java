package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
//import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;
//import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.lang.*;
import java.math.*;

/**
 * 医院就诊情况统计
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author liuli
 * @version 1.0
 */
public class HospitalDiagnoseBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    //private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData;
    private String ManageCom; //管理机构
    private String MakeDate1; //入机日期,起始日期
    private String MakeDate2; //入机日期,终止日期
    private String statflag;

    public HospitalDiagnoseBL() {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
//        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
//                "GlobalInput",
//                0);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "HospitalDiagnoseBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        MakeDate1 = (String) mTransferData.getValueByName("MakeDate1"); //统计日期,起始日期
        MakeDate2 = (String) mTransferData.getValueByName("MakeDate2"); //统计日期,终止日期
        //AssociateDate = mLDHospitalSchema.getLastModiDate(); //借用最后修改日期传递合作时间
        statflag = (String) mTransferData.getValueByName("statflag"); //标识打印方法"1"为分机推荐,"2"为分机构分级别统计"3"全系统推荐"4"全系统指定
        ManageCom = (String) mTransferData.getValueByName("ManageCom");

        String md1 = "";
        String md2 = "";
        String makedate = ""; //统计时间,账单日期
        if (  MakeDate1 != null && MakeDate2 != null && !MakeDate2.equals("")&&!MakeDate1.equals("")) {
            makedate = " and a.feedate between '" + MakeDate1 + "' and '" +
                       MakeDate2 + "'";
            md1 = CommonBL.decodeDate(MakeDate1);
            md2 = CommonBL.decodeDate(MakeDate2);
            texttag.add("Date1", md1);
            texttag.add("Date2", md2);
        }
//            else if ((MakeDate1 != null && MakeDate1 != "") &&
//                   (MakeDate2 == null || MakeDate2 == "")) {
//            makedate = " and a.feedate < '" + MakeDate1 + "'";
//            md1 = CommonBL.decodeDate(MakeDate1);
//            texttag.add("Date2", md1);
//        } else if ((MakeDate2 != null && MakeDate2 != "") &&
//                   (MakeDate1 == null || MakeDate1 == "")) {
//            makedate = " and a.feedate < '" + MakeDate2 + "'";
//            md2 = CommonBL.decodeDate(MakeDate2);
//            texttag.add("Date2", md2);
//        }
        if ((MakeDate1.equals("") || MakeDate1 == null) &&
            (MakeDate2.equals("") || MakeDate2 == null)) {
            MakeDate2 = new PubFun().getCurrentDate();
            md2 = CommonBL.decodeDate(MakeDate2);
            texttag.add("Date2", md2);
        }

        String feeitemtype[] = new String[4];
        for (int i = 0; i < feeitemtype.length; i++) {
            switch (i) {
            case 0:
                feeitemtype[0] = "('101','102','121','202','203','204')"; //药费
                break;
            case 1:
                feeitemtype[1] =
                        "('205','107','207','111','209','112','110','214')"; //检查费
                break;
            case 2:
                feeitemtype[2] = "('206','104','221','124','210','211')"; //治疗费
                break;
            case 3:
                feeitemtype[3] = "('208','113','217')"; //手术费
                break;
            }
        }

//        if (AssociateDate == null || AssociateDate == "") {
//            AssociateDate = new PubFun().getCurrentDate();
//            texttag.add("AssociateDate", AssociateDate);
//        } else {
//            texttag.add("AssociateDate", AssociateDate);
//        }
        ListTable tListTable = new ListTable();
        tListTable.setName("tt1");
        ListTable tListTable2 = new ListTable();
        tListTable2.setName("tt2");

        if (statflag.equals("3") || statflag.equals("4")) {
            String sqla = "select distinct substr(Comcode,1,4) as comcode from LDcom where 1 = 1 and Sign='1' and comcode like '86%' order by comcode with ur";
            SSRS tSSRS = new ExeSQL().execSQL(sqla);
            if (tSSRS == null) {
                return false;
            }

            String assoflag = "";
            if (statflag.equals("3")) {
                assoflag = "1";
            } else if (statflag.equals("4")) {
                assoflag = "2";
            }
            String all[] = new String[15];
            for (int h = 0; h < all.length; h++) {
                all[h] = "0"; //初始化
            }

            for (int i = 0; i < tSSRS.getMaxRow(); i++) {

                String mcom = tSSRS.GetText(i + 1, 1); //获取机构号
                if (mcom.equals("8600")) {
                    continue;
                }
                String value[] = new String[15]; //存放一条记录

                for (int h = 0; h < value.length; h++) {
                    value[h] = "0"; //初始化
                }

                String sqlname = "select name from LdCom  where ComCode='" +
                                 mcom + "' with ur";
                SSRS nameSSRS = new ExeSQL().execSQL(sqlname);
                if (nameSSRS != null) {
                    if(!mcom.equals("86")){
                        value[0] = nameSSRS.GetText(1, 1).substring(0, 2); //机构名称
                    }
                }
                //门诊人次
                String sqlb = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='1' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.hospitalcode=h.hospitcode "
                              + " and h.AssociateClass='" + assoflag +
                              "' and h.ManageCom like '" + mcom + "%'"
                              + makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb1 = new ExeSQL().execSQL(sqlb);
                if (sb1 != null) {
                    if ( mcom.equals("86")) {
                        all[1] = sb1.GetText(1, 1);
                    } else {
                        value[1] = sb1.GetText(1, 1);
                    }
                }
                //住院人次
                String sqlc = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode "
                              + " and h.AssociateClass='" + assoflag +
                              "' and h.ManageCom like '" + mcom + "%'"
                              + makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb2 = new ExeSQL().execSQL(sqlc);
                if (sb2 != null) {
                    if (mcom.equals("86")) {
                        all[2] = sb2.GetText(1, 1);
                    } else {
                        value[2] = sb2.GetText(1, 1);
                    }
                }

                //总门诊费
                String sqld = "select value(sum(sumfee),0) from(select distinct a.caseno, a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218105000'='1189218105000' and   a.FeeType='1' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and c.caseno=p.caseno and a.hospitalcode=h.hospitcode and h.AssociateClass='" +
                              assoflag +
                              "' and h.ManageCom like '" + mcom + "%'" +
                              makedate + ") as X "
                              + " fetch first 3000 rows only with ur ";

                String sb3 = new ExeSQL().getOneValue(sqld);
                if (sb3 != "") {
                    if ( mcom.equals("86")) {
                        if(all[1].equals("0")){
                            all[3]=sb3;
                        }else{
                            double h = Double.parseDouble(sb3) /
                                      Integer.parseInt(all[1]);
                            all[3] = Double.toString(CommonBL.carry(h));
                        }
                    } else {
                        if(value[1].equals("0")){
                            value[3]=sb3;
                        }else{
                            double f = Double.parseDouble(sb3) /
                                      Integer.parseInt(value[1]);
                            value[3] = Double.toString(CommonBL.carry(f));
                        }
                    }
                }

                //总住院费
                String sqle = "select value(sum(sumfee),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='" +
                              assoflag
                              + "' and h.ManageCom like '" + mcom + "%'"
                              + makedate + ") as X"
                              + " fetch first 3000 rows only with ur ";

                String sb4 = new ExeSQL().getOneValue(sqle);
                if (sb4 != "") {
                    if (mcom.equals("86")) {
                        if(all[2].equals("0")){
                            all[4] = sb4;
                        }else{
                            double f = Double.parseDouble(sb4) /
                                      Integer.parseInt(all[2]);
                            all[4] = Double.toString(CommonBL.carry(f));
                        }
                    } else {
                        if(value[2].equals("0")){
                            value[4] = sb4;
                        }else{
                            double f = Double.parseDouble(sb4) /
                                      Integer.parseInt(value[2]);
                            value[4] = Double.toString(CommonBL.carry(f));
                        }
                    }
                }

                //费用构成
                String sqlf =
                        "select value(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                        +
                        " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode"
                        + " and h.AssociateClass='" + assoflag
                        + "' and h.ManageCom like '" + mcom + "%'"
                        + makedate
                        + " with ur";
                SSRS sf = new ExeSQL().execSQL(sqlf);
                if (sf != null) {
                    if (mcom.equals("86")) {
                        all[11] = sf.GetText(1,1);
                    }else{
                        value[11] = sf.GetText(1,1); //总钱数
                    }
                }

                //费用明细
                if((value[11].equals("0")&&!all[11].equals("0"))||(!value[11].equals("0")&&!all[11].equals("0"))){
                    String type[] = new String[4];
                    for (int j = 0; j < 4; j++) {
                        String sqlff =
                                "select nvl(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                                +
                                " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode and h.AssociateClass='"+assoflag
                                + "' and h.ManageCom like '" + mcom + "%'"
                                + " and  b.Feeitemcode in " + feeitemtype[j] +
                                makedate
                                + " with ur";
                        SSRS sff = new ExeSQL().execSQL(sqlff);
                        if (sff != null) {
                            type[j] = sff.GetText(1, 1);
                        }

                        if (mcom.equals("86")&&!all[11].equals("0")) {
                            all[5+j] = Double.toString(CommonBL.carry(Double.parseDouble(type[j]) /
                                    Double.parseDouble(all[11])));
                        } else if(!value[11].equals("0")) {
                            value[5+j] = Double.toString(CommonBL.carry(Double.parseDouble(type[j]) /
                                    Double.parseDouble(value[11])));
                        }
                    }
                }
                //住院天数
                String sqlg = "select value(sum(aa),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.realhospdate as aa from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='"
                              +assoflag
                              + "' and h.ManageCom like '" + mcom + "%'" +
                              makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sbg = new ExeSQL().execSQL(sqlg);
                if (sbg != null) {
                     if ( mcom.equals("86")) {
                         if(all[2].equals("0")){
                             all[9] = sbg.GetText(1,1);
                         }else{
                         all[9] = Double.toString(CommonBL.carry(Double.parseDouble(sbg.GetText(1, 1)) /
                                                              Integer.parseInt(all[2])));
                         }
                     }else{
                         if(value[2].equals("0")){
                             value[9] = sbg.GetText(1,1);
                         }else{
                         value[9] = Double.toString(CommonBL.carry(Double.parseDouble(sbg.GetText(1, 1)) /
                                                              Integer.parseInt(value[2])));
                         }
                     }
                }
                if(!value[0].equals("0")){
                    tListTable.add(value);
                }
                //测试
//                for (int p = 0; p < value.length; p++) {
//                    System.out.println(value[p]);
//                }
            }
            tListTable2.add(all);
        }

        String sqlm = "select distinct substr(Comcode,1,4) as comcode from LDcom where 1 = 1 and Sign='1' and comcode like '" +
                      ManageCom + "%' order by comcode with ur";
        String manage = new ExeSQL().getOneValue(sqlm);//机构号
        String sqln = "select name from LdCom  where ComCode='" +
                      manage + "' with ur";
        String name = new ExeSQL().getOneValue(sqln);//机构名
        texttag.add("ManageCom", name);

        //分机构推荐医院统计
        if (statflag.equals("1")) {
            String sqlf =
                    " select hospitcode,hospitname,levelcode from ldhospital where ManageCom like '" +
                    manage
                    + "%' and AssociateClass='1' and levelcode in('10','11','20','21','30','31') with ur";
            SSRS tSSRS = new ExeSQL().execSQL(sqlf);
            if (tSSRS == null) {
                return false;
            }
            for (int i = 0; i < tSSRS.getMaxRow();i++) {
                String value[] = new String[15];
                for(int m=0;m<value.length;m++){
                    value[m]="0";
                }
                String hospitcode = tSSRS.GetText(i+1, 1);
                value[0] = tSSRS.GetText(i+1, 2);
                String level = tSSRS.GetText(i+1, 3);
                if (level.equals("10") || level.equals("11")) {
                    value[1] = "一级"; //医院等级
                } else if (level.equals("20") || level.equals("21")) {
                    value[1] = "二级";
                } else if (level.equals("30") || level.equals("31")) {
                    value[1] = "三级";
                }

                //门诊人次
                String sql1 = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='1' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.hospitalcode=h.hospitcode "
                              +
                              " and h.AssociateClass='1' and h.hospitcode = '" +
                              hospitcode + "'"
                              + makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb1 = new ExeSQL().execSQL(sql1);
                if (sb1 != null) {
                    value[2] = sb1.GetText(1, 1);
                }
                //就诊人次
                String sql2 = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode "
                              +
                              " and h.AssociateClass='1' and h.hospitcode = '" +
                              hospitcode + "'"
                              + makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb2 = new ExeSQL().execSQL(sql2);
                if (sb2 != null) {
                    value[3] = sb2.GetText(1, 1);
                }
                //总门诊费
                String sql3 = "select value(sum(sumfee),0) from(select distinct a.caseno, a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218105000'='1189218105000' and   a.FeeType='1' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and c.caseno=p.caseno and a.hospitalcode=h.hospitcode and h.AssociateClass='1' and h.hospitcode = '"
                              + hospitcode + "'" + makedate + ") as X "
                              + " fetch first 3000 rows only with ur ";
                SSRS sb3 = new ExeSQL().execSQL(sql3);
                if (sb3 != null) {
                    if(value[2].equals("0")){
                        value[4]= sb3.GetText(1,1);
                    }else{
                        double f = Double.parseDouble(sb3.GetText(1,1)) /
                                  Integer.parseInt(value[2]);
                        value[4] = Double.toString(CommonBL.carry(f));
                    }
                }

                //总住院费
                String sql4 = "select value(sum(sumfee),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='1' and h.hospitcode='"
                              + hospitcode + "'"
                              + makedate + ") as X"
                              + " fetch first 3000 rows only with ur ";

                SSRS sb4 = new ExeSQL().execSQL(sql4);
                if (sb4 != null) {
                    if(value[3].equals("0")){
                        value[5] = sb4.GetText(1,1);
                    }else{
                    double f = Double.parseDouble(sb4.GetText(1, 1)) /
                              Integer.parseInt(value[3]);
                    value[5] = Double.toString(CommonBL.carry(f));
                    }
                }

                //费用构成
                String sql5 =
                        "select nvl(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                        +
                        " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode"
                        + " and h.AssociateClass='1' and h.hospitcode='" +
                        hospitcode + "'"
                        + makedate
                        + " with ur";
                SSRS sb5 = new ExeSQL().execSQL(sql5);
                if (sb5 != null) {
                    value[12] = sb5.GetText(1, 1); //总钱数
                }

                //费用明细
                if(value[12] !="0"&&!value[12].equals("0")){
                    String type[] = new String[4];
                    for (int j = 0; j < 4; j++) {
                        String sqlff =
                                "select nvl(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                                +
                                " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                                + " and h.hospitcode='" + hospitcode + "'"
                                + " and  b.Feeitemcode in " + feeitemtype[j] +
                                makedate
                                + " with ur";
                        SSRS sff = new ExeSQL().execSQL(sqlff);
                        if (sff != null) {
                            type[j] = sff.GetText(1, 1);
                        }
                        value[6+j] = Double.toString(CommonBL.carry((Double.parseDouble(type[j]) /
                                                               Double.parseDouble(value[12]))));
                    }
                }
                //住院天数
                String sql6 = "select value(sum(aa),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.realhospdate as aa from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                              + " and h.hospitcode='" + hospitcode + "'" +
                              makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb6 = new ExeSQL().execSQL(sql6);
                if (sb6 != null) {
                    if(value[3].equals("0")){
                        value[10] = sb6.GetText(1,1);
                    }else{
                    value[10] = Double.toString(CommonBL.carry(Double.parseDouble(sb6.GetText(1, 1)) / Integer.parseInt(value[3])));
                    }
                }
//                for(int m=0;m<value.length;m++){
//                    System.out.println(value[m]);
//                }
                tListTable.add(value);
            }
        }

        //分机构分级别统计
        if (statflag.equals("2")) {
           String all[] = new String[15];
           for(int i=0;i<all.length;i++){
               all[i]="0";
           }
            for (int i = 0; i <=2; i++) {
                String value[] = new String[15];
                for(int m=0;m<value.length;m++){
                    value[m]="0";
                }
                String level = "";
                switch(i){
                case 0:value[0]="三级医院";level=" and h.levelcode in ('30','31')";break;
                case 1:value[0]="二级医院";level=" and h.levelcode in ('20','21')";break;
                case 2:value[0]="一级医院";level=" and h.levelcode in ('10','11')";break;
                }

                //门诊人次
                String sql1 = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='1' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.hospitalcode=h.hospitcode "
                              + "and h.ManageCom like '"+manage+"'"+
                              " and h.AssociateClass='1'" +  level  + makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb1 = new ExeSQL().execSQL(sql1);
                if (sb1 != null) {
                    value[1] = sb1.GetText(1, 1);
                    all[1] = ""+(Integer.parseInt(all[1])+Integer.parseInt(value[1]));
                }
                //医院人次
                String sql2 = " select count(*) from (select distinct a.caseno,a.caserelano,a.feedate from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where   a.FeeType='2' and c.caseno = a.caseno and c.rgtstate in ('09','11','12')"
                              + " and a.caserelano = p.caserelano and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode "
                              +"and h.ManageCom like '"+manage+"%' and h.AssociateClass='1'"+level+ makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb2 = new ExeSQL().execSQL(sql2);
                if (sb2 != null) {
                    value[2] = sb2.GetText(1, 1);
                     all[2] = ""+(Integer.parseInt(all[2])+Integer.parseInt(value[2]));
                }

                //总门诊费
                String sql3 = "select value(sum(sumfee),0) from(select distinct a.caseno, a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218105000'='1189218105000' and   a.FeeType='1' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and c.caseno=p.caseno and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                              + " and h.ManageCom like '"+manage+"%'"+ level + makedate + ") as X "
                              + " fetch first 3000 rows only with ur ";

                SSRS sb3 = new ExeSQL().execSQL(sql3);
                if (sb3 != null) {
                    if(value[1].equals("0"))
                    {
                        value[3]=sb3.GetText(1,1);
                    }else{
                    double f = Double.parseDouble(sb3.GetText(1,1)) / Integer.parseInt(value[1]);
                    value[3] = Double.toString(CommonBL.carry(f));
                    }
                    all[3] = ""+(Double.parseDouble(all[3])+Double.parseDouble(sb3.GetText(1,1)));
                }

                //总住院费
                String sql4 = "select value(sum(sumfee),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.sumfee as sumfee from llfeemain a,"
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                              +" and h.ManageCom like '"+manage+"%'"+ level + makedate + ") as X"
                              +" fetch first 3000 rows only with ur ";

                SSRS sb4 = new ExeSQL().execSQL(sql4);
                if (sb4 != null) {
                    if (value[2].equals("0")) {
                        value[4] = sb4.GetText(1, 1);
                    } else {
                        double f = Double.parseDouble(sb4.GetText(1, 1)) /
                                  Integer.parseInt(value[2]);
                        value[4] = Double.toString(CommonBL.carry(f));
                       }
                    all[4] = "" + (Double.parseDouble(all[4])+Double.parseDouble(sb4.GetText(1, 1)));

                }
                //费用构成

                String sql5 =
                        "select value(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                        +
                        " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode"
                        + " and h.AssociateClass='1' and h.ManageCom like '" + manage + "%'"
                        +level+ makedate
                        + " with ur";
                SSRS sb5 = new ExeSQL().execSQL(sql5);
                if (sb5 != null) {
                    value[12] = sb5.GetText(1, 1); //总钱数
                    all[12] = ""+(Double.parseDouble(all[12])+Double.parseDouble(value[12]));
                }

                //费用明细
                if(!value[12].equals("0")){
                    String type[] = new String[4];
                    for (int j = 0; j < 4; j++) {
                        String sqlff =
                                "select value(sum(b.fee),0) from llcasereceipt b,llfeemain a,ldhospital h where"
                                +
                                " b.mainfeeno=a.mainfeeno and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                                + " and h.ManageCom like '" + manage + "%'"
                                + " and  b.Feeitemcode in " + feeitemtype[j] +
                                level + makedate
                                + " with ur";
                        SSRS sff = new ExeSQL().execSQL(sqlff);
                        if (sff != null) {
                            type[j] = sff.GetText(1, 1);
                        }
                        value[5+j] = Double.toString(CommonBL.carry(Double.parseDouble(type[j]) /
                                                               Double.parseDouble(value[12])));
                        all[5+j] = "" + (Double.parseDouble(all[5 + j]) +
                                      Double.parseDouble(type[j]));
                    }
                }
                //住院天数
                String sql6 = "select value(sum(aa),0) from (select distinct a.caseno,a.caserelano,a.feedate,a.realhospdate as aa from llfeemain a, "
                              + "llclaimpolicy p,llcase c,ldhospital h  where '1189218106000'='1189218106000' and  a.FeeType='2' "
                              + " and c.caseno = a.caseno and c.rgtstate in ('09','11','12') and a.caserelano = p.caserelano "
                              + " and a.SumFee != 0 and a.RealHospDate != 0 and a.hospitalcode=h.hospitcode and h.AssociateClass='1'"
                              + " and h.ManageCom like '" + manage + "%'" +level+
                              makedate
                              + ") as X  fetch first 3000 rows only with ur ";
                SSRS sb6 = new ExeSQL().execSQL(sql6);
                if (sb6 != null) {
                    if(value[2].equals("0")){
                        value[9] = sb6.GetText(1,1);
                    }else{
                    value[9] = Double.toString(CommonBL.carry(Double.parseDouble(sb6.GetText(1, 1)) / Integer.parseInt(value[2])));
                    }
                    all[9] = ""+(Double.parseDouble(all[9])+Double.parseDouble(sb6.GetText(1,1)));
                }
                tListTable.add(value);
            }
            if(!all[1].equals("0")){
              all[3] = Double.toString(CommonBL.carry(Double.parseDouble(all[3]) / Double.parseDouble(all[1])));
            }
            if(!all[2].equals("0")){
              all[4]= Double.toString(CommonBL.carry(Double.parseDouble(all[4])/Double.parseDouble(all[2])));
              all[9]= Double.toString(CommonBL.carry(Double.parseDouble(all[9])/Double.parseDouble(all[2])));
            }
            if(!all[12].equals("0")&&!all[12].equals("0.0")&&!all[12].equals("0.00")){
            all[5]= Double.toString(CommonBL.carry(Double.parseDouble(all[5])/Double.parseDouble(all[12])));
            all[6]= Double.toString(CommonBL.carry(Double.parseDouble(all[6])/Double.parseDouble(all[12])));
            all[7]= Double.toString(CommonBL.carry(Double.parseDouble(all[7])/Double.parseDouble(all[12])));
            all[8]= Double.toString(CommonBL.carry(Double.parseDouble(all[8])/Double.parseDouble(all[12])));
            }

            tListTable2.add(all);
        }

        XmlExport xmlexport = new XmlExport();
        if (statflag.equals("1")) {
            xmlexport.createDocument("HospDiagF1.vts", "printer"); //分机构
        } else if (statflag.equals("2")) {
            xmlexport.createDocument("HospDiagF2.vts", "printer"); //分机构分合作级别
        }else if(statflag.equals("3")){
            xmlexport.createDocument("HospDiagA1.vts", "printer"); //全系统推荐医院
        }else if(statflag.equals("4")){
            xmlexport.createDocument("HospDiagA2.vts", "printer"); //全系统指定医院
        }

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tListTable, new String[15]);

        xmlexport.addListTable(tListTable2, new String[15]);

        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件

        mResult.clear();

        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {

        HospitalDiagnoseBL tHospitalDiagnoseBL = new HospitalDiagnoseBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        tHospitalDiagnoseBL.getPrintData();

    }
}
