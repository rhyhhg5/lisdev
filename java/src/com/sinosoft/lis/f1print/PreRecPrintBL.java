package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 * 
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PreRecPrintBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;

	private String mSql = null;

	private String mOutXmlPath = null;

	private String StartDate = "";

	private String EndDate = "";
	
	private String type = "";

	private String mCurDate = PubFun.getCurrentDate();

	SSRS tSSRS = new SSRS();

	public PreRecPrintBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		System.out.println("BL->dealDate()");
		String dSql = "";
		if (StartDate != null && !StartDate.equals("")) {
			dSql = " and gc.cvalidate >='" + StartDate + "'";
		}
		ExeSQL manExeSQL = new ExeSQL();
		String manSql = "select distinct managecom from "
				+ ("T".equals(type) ? "lbgrpcont" : "lcgrpcont") + " gc "
				+ "where gc.payintv = -1 and gc.managecom like '"
				+ mGI.ManageCom
				+ "%' and "
				+ "exists (select 1 from LCGrpPayPlan gpp where gpp.prtno = gc.prtno) "
				+ "and gc.cvalidate <= '" + EndDate + "' " + dSql
				+ " and Gc.signdate is not null "
				+ " order by managecom";
		SSRS manSSRS = manExeSQL.execSQL(manSql);
		if (manExeSQL.mErrors.needDealError()) {
			System.out.println(manExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		if (manSSRS.getMaxRow() == 0) {

			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有符合条件的数据，请重新输入条件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		String[][] mToExcel = new String[10000][30];
		mToExcel[0][0] = "中国人民健康保险股份有限公司";
		mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
				+ "年"
				+ EndDate.substring(EndDate.indexOf('-') + 1,
						EndDate.lastIndexOf('-')) + "月应收保费统计表";
		mToExcel[2][0] = "编制单位：" + getName();
		mToExcel[2][14] = "金额单位：元";

		mToExcel[3][0] = "序号";
		mToExcel[3][1] = "机构代码";
		mToExcel[3][2] = "机构名称";
		mToExcel[3][3] = "险类";
		mToExcel[3][4] = "险种代码";
		mToExcel[3][5] = "险种名称";
		mToExcel[3][6] = "投保单号";
		mToExcel[3][7] = "保单号";
		mToExcel[3][8] = "投保单位名称/个人姓名";
		mToExcel[3][9] = "业务序号";
		mToExcel[3][10] = "生效日期";
		mToExcel[3][11] = "终止日期";
		mToExcel[3][12] = "共保比例";
		mToExcel[3][13] = "保费收入金额";
		mToExcel[3][14] = "实收保费金额";
		mToExcel[3][15] = "应收保费金额";
		mToExcel[3][16] = "未逾期应收保费余额";
		mToExcel[3][17] = "逾期在3个月以内的应收保费余额";
		mToExcel[3][18] = "逾期超过3个月但在6个月内的应收保费余额";
		mToExcel[3][19] = "逾期超过6个月但在1年内的应收保费余额";
		mToExcel[3][20] = "逾期超过1年的应收保费余额";
		mToExcel[3][21] = "理赔金额";
		mToExcel[3][22] = "市场类型";
		mToExcel[3][23] = "业务员";		
        mToExcel[3][24] = "备注";
		String polCom = manSSRS.GetText(1, 1).substring(0, 4) + "0000";
		int printNum = 3;
		double sumsFinMoney = 0;// 总合计应收保费
		double sumaFinMoney = 0;// 总合计实收保费
		double aFinMoney = 0;// 分公司合计
		double sFinMoney = 0;// 分公司合计
		double sumMoney = 0;

		double sumFinMoney1 = 0;// 总合计未逾期
		double sumFinMoney3 = 0;// 总合计逾期-3
		double sumFinMoney6 = 0;// 总合计3-6
		double sumFinMoney12 = 0;// 总合计6-12
		double sumFinMoney1y = 0;// 总合计>12
		double FinMoney1 = 0;// 分公司合计
		double FinMoney3 = 0;// 分公司合计
		double FinMoney6 = 0;// 分公司合计
		double FinMoney12 = 0;// 分公司合计
		double FinMoney1y = 0;// 分公司合计

		for (int comnum = 1; comnum <= manSSRS.getMaxRow(); comnum++) {
			ExeSQL tExeSQL = new ExeSQL();
			mSql = "select * from (select rownumber() over(),gc.managecom,"
					+ "(select name from ldcom where comcode=gc.managecom),(select codename from ldcode where codetype='risktype' and code=substr(gp.riskcode,1,4)),gp.riskcode,"
					+ "(select RiskName from LMRisk where rtrim(RiskCode) = gp.riskcode),gc.prtno,gc.grpcontno,gc.grpname,'团险渠道'"
					+ ",gc.cvalidate,gc.cinvalidate,"
                    //共保比例
					+ "case when gc.CoInsuranceFlag<>'1' then 1 else (select 1-sum(rate) From lccoinsuranceparam  where Grpcontno=Gc.Grpcontno) end , "
					+ "'',"
					+ "(select sum(sumduepaymoney) from ljapaygrp apg where apg.grpcontno=gc.grpcontno and apg.riskcode=gp.riskcode and exists (select 1 from ljapay ap where ap.incomeno=apg.grpcontno and ap.payno=apg.payno))"
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from LCGrpPayPlandetail where riskcode=gp.riskcode and state='2' and prtno=gc.prtno) as totalShouldPay")
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from lcgrppayplandetail gppd where gppd.prtno = gc.prtno and gppd.riskcode = gp.riskcode and gppd.state='2' and current date <= paytodate + 1 month) shouldpay0")
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from lcgrppayplandetail gppd where gppd.prtno = gc.prtno and gppd.riskcode = gp.riskcode and gppd.state='2' and current date > paytodate + 1 month and current date <= paytodate + 3 month) shouldpay1")
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from lcgrppayplandetail gppd where gppd.prtno = gc.prtno and gppd.riskcode = gp.riskcode and gppd.state='2' and current date > paytodate + 3 month and current date <= paytodate + 6 month) shouldpay3")
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from lcgrppayplandetail gppd where gppd.prtno = gc.prtno and gppd.riskcode = gp.riskcode and gppd.state='2' and current date > paytodate + 6 month and current date <= paytodate + 12 month) shouldpay6")
					+ ("T".equals(type) ? ",0" : ""
					+ ",(select sum(prem) from lcgrppayplandetail gppd where gppd.prtno = gc.prtno and gppd.riskcode = gp.riskcode and gppd.state='2' and current date > paytodate + 1 years) shouldpay12")
					+ ",(Select Sum(Pay) From Ljagetclaim Where Grpcontno = Gc.Grpcontno And Riskcode = Gp.Riskcode And Othernotype In ('5', 'C', 'F'))"
					+ ",(Select Codename From Ldcode Where Codetype = 'markettype' And Code = Gc.Markettype),getUniteCode(gc.agentcode)  "
					+ "from " + ("T".equals(type) ? "lbgrpcont" : "lcgrpcont") +" gc "  
					+ "right join " + ("T".equals(type) ? "lbgrppol" : "lcgrppol") +" gp "
					+ "on gc.grpcontno=gp.grpcontno "
					+ "where gc.payintv = -1 and " + ("T".equals(type) ? "" : "Gc.signdate is not null and ") 
					+ "exists (select 1 from LCGrpPayPlan gpp where gpp.prtno = gc.prtno) "
					+ "and gc.managecom='"
					+ manSSRS.GetText(comnum, 1)
					+ "' and gc.cvalidate <= '"
					+ EndDate
					+ "' "
					+ dSql
					+ " order by gc.cvalidate,gc.managecom,gc.grpcontno,gp.riskcode)"
					+ ("T".equals(type) ? "" : " where totalshouldpay <> 0" )
					+ " with ur";
			System.out.println(mSql);
			tSSRS = tExeSQL.execSQL(mSql);
			if(tSSRS.getMaxRow() != 0){
				System.out.println(polCom + "--"
						+ tSSRS.GetText(tSSRS.getMaxRow(), 2));
				if (!polCom.equals(tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0,
						4)
						+ "0000")) {
					mToExcel[printNum + 1][0] = "分公司合计：";
					mToExcel[printNum + 1][13] = PubFun.format(aFinMoney
							+ sFinMoney)
							+ "";
					mToExcel[printNum + 1][14] = PubFun.format(sFinMoney) + "";
					mToExcel[printNum + 1][15] = PubFun.format(aFinMoney) + "";
					mToExcel[printNum + 1][16] = PubFun.format(FinMoney1) + "";
					mToExcel[printNum + 1][17] = PubFun.format(FinMoney3) + "";
					mToExcel[printNum + 1][18] = PubFun.format(FinMoney6) + "";
					mToExcel[printNum + 1][19] = PubFun.format(FinMoney12) + "";
					mToExcel[printNum + 1][20] = PubFun.format(FinMoney1y) + "";
					sFinMoney = 0;
					aFinMoney = 0;
					FinMoney1 = 0;
					FinMoney3 = 0;
					FinMoney6 = 0;
					FinMoney12 = 0;
					FinMoney1y = 0;
					printNum = printNum + 2;
				}
				polCom = tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0, 4)
						+ "0000";

				double ssFinMoney = 0;// 支公司合计应收保费
				double saFinMoney = 0;// 支公司合计实收保费
				double sFinMoney1 = 0;//
				double sFinMoney3 = 0;//
				double sFinMoney6 = 0;//
				double sFinMoney12 = 0;//
				double sFinMoney1y = 0;//
				int no = 1;
				for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
					for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
						if (col != 14) {
							if (tSSRS.GetText(row, col).equals("null")) {
								mToExcel[row + printNum][col - 1] = "";
							} else {
								mToExcel[row + printNum][col - 1] = tSSRS.GetText(
										row, col);
							}
						} else {
							double finMoney = 0;
							if (!tSSRS.GetText(row, col + 1).equals("null")) {
								finMoney = Double.parseDouble(tSSRS.GetText(row,
										col + 1));
								ssFinMoney = ssFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 1));
								sumsFinMoney = sumsFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 1));
								sFinMoney = sFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 1));
							}
							if (!tSSRS.GetText(row, col + 2).equals("null")) {
								finMoney = finMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 2));
								saFinMoney = saFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 2));
								sumaFinMoney = sumaFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 2));
								aFinMoney = aFinMoney
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 2));
							}
							// --------------------------------
							if (!tSSRS.GetText(row, col + 3).equals("null")) {
								sFinMoney1 = sFinMoney1
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 3));
								sumFinMoney1 = sumFinMoney1
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 3));
								FinMoney1 = FinMoney1
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 3));
							}
							if (!tSSRS.GetText(row, col + 4).equals("null")) {
								sFinMoney3 = sFinMoney3
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 4));
								sumFinMoney3 = sumFinMoney3
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 4));
								FinMoney3 = FinMoney3
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 4));
							}
							if (!tSSRS.GetText(row, col + 5).equals("null")) {
								sFinMoney6 = sFinMoney6
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 5));
								sumFinMoney6 = sumFinMoney6
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 5));
								FinMoney6 = FinMoney6
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 5));
							}
							if (!tSSRS.GetText(row, col + 6).equals("null")) {
								sFinMoney12 = sFinMoney12
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 6));
								sumFinMoney12 = sumFinMoney12
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 6));
								FinMoney12 = FinMoney12
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 6));
							}
							if (!tSSRS.GetText(row, col + 7).equals("null")) {
								sFinMoney1y = sFinMoney1y
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 7));
								sumFinMoney1y = sumFinMoney1y
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 7));
								FinMoney1y = FinMoney1y
										+ Double.parseDouble(tSSRS.GetText(row,
												col + 7));
							}
							if (finMoney != 0) {
								mToExcel[row + printNum][col - 1] = PubFun
										.format(finMoney) + "";
								sumMoney = sumMoney + finMoney;
							} else {
								mToExcel[row + printNum][col - 1] = "";
							}
						}
					}
					mToExcel[row + printNum][0] = no + "";
					no++;
				}
				mToExcel[printNum + tSSRS.getMaxRow() + 1][0] = "支公司合计：";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][13] = PubFun
						.format(ssFinMoney + saFinMoney) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][14] = PubFun
						.format(ssFinMoney) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][15] = PubFun
						.format(saFinMoney) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][16] = PubFun
				.format(sFinMoney1) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][17] = PubFun
				.format(sFinMoney3) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][18] = PubFun
				.format(sFinMoney6) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][19] = PubFun
				.format(sFinMoney12) + "";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][20] = PubFun
				.format(sFinMoney1y) + "";
				printNum = printNum + tSSRS.getMaxRow() + 1;
				tSSRS.Clear();
			}
			
		}
		mToExcel[printNum + 1][0] = "分公司合计：";
		mToExcel[printNum + 1][13] = PubFun.format(aFinMoney + sFinMoney) + "";
		mToExcel[printNum + 1][14] = PubFun.format(sFinMoney) + "";
		mToExcel[printNum + 1][15] = PubFun.format(aFinMoney) + "";
		mToExcel[printNum + 1][16] = PubFun.format(FinMoney1) + "";
		mToExcel[printNum + 1][17] = PubFun.format(FinMoney3) + "";
		mToExcel[printNum + 1][18] = PubFun.format(FinMoney6) + "";
		mToExcel[printNum + 1][19] = PubFun.format(FinMoney12) + "";
		mToExcel[printNum + 1][20] = PubFun.format(FinMoney1y) + "";
		mToExcel[printNum + 3][0] = "总合计：";
		mToExcel[printNum + 3][13] = PubFun.format(sumsFinMoney + sumaFinMoney)
				+ "";
		mToExcel[printNum + 3][14] = PubFun.format(sumsFinMoney) + "";
		mToExcel[printNum + 3][15] = PubFun.format(sumaFinMoney) + "";
		mToExcel[printNum + 3][16] = PubFun.format(sumFinMoney1) + "";
		mToExcel[printNum + 3][17] = PubFun.format(sumFinMoney3) + "";
		mToExcel[printNum + 3][18] = PubFun.format(sumFinMoney6) + "";
		mToExcel[printNum + 3][19] = PubFun.format(sumFinMoney12) + "";
		mToExcel[printNum + 3][20] = PubFun.format(sumFinMoney1y) + "";

		mToExcel[printNum + 5][0] = "制表";
		mToExcel[printNum + 5][14] = "日期：" + mCurDate;

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);
		System.out.println(mGI.ManageCom);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		StartDate = (String) tf.getValueByName("StartDate");
		EndDate = (String) tf.getValueByName("EndDate");
		type = (String) tf.getValueByName("type");
		
		System.out.println("type ----" + type);
		
		if (EndDate == null || mOutXmlPath == null) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}

	private String getName() {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86";
		tG.Operator = "cwad";
		tG.ComCode = "86";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
		tTransferData.setNameAndValue("EndDate", "2012-05-01");
		tTransferData.setNameAndValue("StartDate", "");
		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);
		PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
		if (!tPreRecPrintUI.submitData(vData, "")) {
			System.out.print("失败！");
		}
		// System.out.println(PubFun.format(111111111111111.0));
	}
}
