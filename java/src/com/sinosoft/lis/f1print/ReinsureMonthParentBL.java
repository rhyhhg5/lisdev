package com.sinosoft.lis.f1print;

/**
 * X10再保月结单（总公司1000）
 * @date 2008-07-22
 * @author yanghao
 */
import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ReinsureMonthParentBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	String sBeginDate = ""; 

	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

	public ReinsureMonthParentBL() {
	}

	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		System.out.println("进入ReinsureMonthParentBL...");
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		
		if (!getInputData(cInputData)) {
			return false;
		}
		
		mResult.clear();

		if (cOperate.equals("PRINT")) { // 打印提数
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @return  
	 */
	private boolean getInputData(VData cInputData) {
		sBeginDate = (String) cInputData.get(0);// 获得WageNo
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ReinsureMonthParentBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 新的打印数据方法
	 * @return
	 */
	private boolean getPrintData() {
		
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", sBeginDate);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		// String tServerPath = "E:/sinowork/picch/WebRoot/";
		ExeSQL tExeSQL = new ExeSQL();

		String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);

		String manageCom = tSSRS.GetText(1, 1);
		System.out.println("从数据库中获得的值:" + manageCom);
		
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("ReinsureMonthParent.vts", "printer"); // 最好紧接着就初始化xml文档
 
		texttag.add("WageNo", sBeginDate);// 月度
		texttag.add("ManageCom", manageCom);
		
		String[] detailArr = new String[] { "再保公司", "应收分保账款-合同-比例","应付分保账款-合同-比例"};

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"ZBYSYF",//  X10再保月结单（总公司1000）再保月结单-应收分保账款
				"ZBYSYFZ",// X10再保月结单（总公司1000）再保月结单-应付分保账款
		};
		
		for (int i = 0; i < getTypes.length; i++) {
			String msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
			ListTable tDetailListTable = getDetailListTable(msql, getTypes[i]); // 明细ListTable
			xmlexport.addListTable(tDetailListTable, detailArr); // 放入xmlexport对象
		}

		// 往来余额 提取 从xml文件中要提取的SQL的节点 
		String[] ZBYJWLYESQLNode = new String[]{
				"ZBYSYFWLYE",//  X10再保月结单（总公司1000）- 往来余额
		};
		
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int i=0; i<ZBYJWLYESQLNode.length; i++){
			String sql  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZBYJWLYESQLNode[i]);
			String sZBYJWLYE = getZBYJWLYE(sql);
			texttag.add(ZBYJWLYESQLNode[i], sZBYJWLYE);// 对应这种格式 : $=/ZBYSYFWLYE$
		}
		
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}		 
		
		mResult.clear();
		mResult.addElement(xmlexport);
		System.out.println("返回XMLExport。");
		return true;
	}

	/**
	 * 往来余额
	 * @param hzSql
	 * @return
	 */
	private String getZBYJWLYE(String hzSql){
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS  = tExeSQL.execSQL(hzSql.toString());
		String tSumMoney = "0.00";
        if(tSSRS.MaxRow > 0){
        	if ( ! tSSRS.GetText(1, 1).equals("null") ){
        		tSumMoney = tSSRS.GetText(1, 1);
        	}
        }
        return tSumMoney;
	}
	
 
	
	/**
	 * 获得明细ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strSum = "";
		String strArr[] = null;
		tlistTable.setName(tName);
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[tSSRS.MaxCol];// 列的最大值
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j != 1 ) { // 第2列后都代表金钱, 为空的都置为0
					strArr[j - 1] = tSSRS.GetText(i, j);
					strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					continue;
				}
				strArr[j - 1] = tSSRS.GetText(i, j);
			}
			tlistTable.add(strArr);
		}
		return tlistTable;
	}

	/**
	 * 显示无金额的情况,该类暂不使用。
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("ZBYJZ".equals(pName)) {
			result[0] = "异地理赔日结单 合计";
		}
		result[1] = "0";
		return result;
	}

 

}
