package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh yanghao
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FFPayDayF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FFPayDayF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
         if (!getNewPrintData())
         {
            return false;
         }
         
        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FFPayDayF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 遗弃的方法 暂不使用 现在使用 ----> getNewPrintData()
     * @return
     */
    private boolean getPrintData()
    {
//        LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
//        LJFIGetSet tLJFIGetSet = new LJFIGetSet();
        SSRS tSSRS = new SSRS();
//        double SumMoney = 0;
        long SumNumber = 0;
        
//        msql =  "select (case OtherNoType when '2' then '印刷号' when '3' then '团单批单号' when '5' then '理赔赔案号' when '6' then '个单保单号' when '8' then '团单保单号' when '10' then '个单批单号' else '其他' end) " +
//                " ,CodeName,GetMoney,1,OtherNo from LJFIGet,LDCode where CodeType='paymode' and Code=PayMode and PayMode<>'4' and MakeDate>='" +
//                mDay[0] + "' and MakeDate<='" + mDay[1] +
//                "' and ManageCom like'" + mGlobalInput.ManageCom +
//                "%' Order By CodeName,OtherNo ";
        // 付费明细SQL
        msql = "select  (case othernotype when '3' then '团单保全受理号'  when '4' then '缴费通知书号码'  when '5' then '理赔赔款'   when '10' then '个单保全受理号'  when '12' then '余额领取'  when '13' then '定期结算工单号'  when 'C' then '查勘费赔款'  when 'F' then '查勘费费用' else '其他' end), "
        	 + " (case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转帐支票' when '4' then '银行转帐' when '5' then '内部转帐'  when '6' then 'pos机' when '9' then '其他' when '12' then '银行代收' when '13' then '赠送保费'  when '10' then '应收保费' when '11' then '银行汇款'   else '其他' end),"
        	 + "  sum(sumgetmoney), count(otherno), otherno  from ljaget "
        	 + " where confdate>='" + mDay[0]+"' and confdate<='" + mDay[1]+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' "
        	 + " and sumgetmoney <> 0 group by othernotype,otherno,paymode "
        	 + " order by paymode, othernotype, otherno "
        	 ;
                
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("Pay");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[5];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
//                if (j == 1)
//                {
//                    strArr[j - 1] = tSSRS.GetText(i, j);
//                }
//                if (j == 2)
//                {
//                    strArr[j - 1] = tSSRS.GetText(i, j);
//                }
                if (j == 3)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    String strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    continue;
                }
                strArr[j - 1] = tSSRS.GetText(i, j);
//                if (j == 4)
//                {
//                    strArr[j - 1] = tSSRS.GetText(i, j);
//                }
//                if (j == 5)
//                {
//                    strArr[j - 1] = tSSRS.GetText(i, j);
//                }                
            }
            tlistTable.add(strArr);
        }

        strArr = new String[5];
        strArr[0] = "OtherNoType";
        strArr[1] = "PayMode";
        strArr[2] = "Money";
        strArr[3] = "Mult";
        strArr[4] = "OtherNo";        

//        msql =  "select CodeName||'汇总',sum(GetMoney),sum(1) from LJFIGet,LDCode where CodeType='paymode' and Code=PayMode and PayMode<>'4' and MakeDate>='" +
//                mDay[0] + "' and MakeDate<='" + mDay[1] +
//                "' and ManageCom like'" + mGlobalInput.ManageCom +
//                "%' Group By CodeName";
        // 付费汇总
        msql = "select * from ( select (case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转帐支票' when '4' then '银行转帐' when '5' then '内部转帐' "
        	+ "   when '6' then 'pos机' when '9' then '其他' when '12' then '银行代收' when '13' then '赠送保费'  when '10' then '应收保费' when '11' then '银行汇款'   else '其他' end) ||'汇总' as CName,"
        	+ " sum(sumgetmoney) as summoney ,count(1)  as count  from ljaget  "
        	+ " where  confdate>='" + mDay[0] + "' and confdate<='" + mDay[1] + "'  and ManageCom like '" + mGlobalInput.ManageCom +"%'  and sumgetmoney <> 0"
        	+ " Group By paymode union all select trim(insbankaccno) || '汇总' as CName, sum(sumgetmoney) as summoney , count(actugetno)  as count from ljaget "
        	+ " where  paymode in ('2','3','4','11')  and confdate>='" + mDay[0] + "'  and confdate<='" + mDay[1] + "' and managecom like '" + mGlobalInput.ManageCom +"%' and sumgetmoney <> 0 " 
        	+ " group by insbankaccno )as aa order by 1 desc "
        	 ;
                
        tSSRS = tExeSQL.execSQL(msql);
        ListTable alistTable = new ListTable();
        String strSumArr[] = null;
        alistTable.setName("Sum");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strSumArr = new String[3];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
//                if (j == 1)
//                {
//                    strSumArr[j - 1] = tSSRS.GetText(i, j);
//                }
                if (j == 2)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    String strSum = new DecimalFormat("0.00").format(Double.valueOf(strSumArr[j - 1]));
                    strSumArr[j - 1] = strSum;
                    continue;
//                    SumMoney = SumMoney + Double.parseDouble(strSumArr[j - 1]);
                }
//                if (j == 3)
//                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
//                    SumNumber = SumNumber +
//                                Long.valueOf(strSumArr[j - 1]).longValue();
//                }
            }
            alistTable.add(strSumArr);
        }
//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
        strSumArr = new String[]{"付款方式","号码类型","号码","金额","件数"};
//        strArr[0] = "PayMode";
//        strArr[1] = "Money";
//        strArr[2] = "Mult";
        
        nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'";
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        
        // 付费合计
        StringBuffer hzSql = new StringBuffer("select  sum(sumgetmoney) as sumgetmoney, count(1) as actugetno from ljaget");
        hzSql.append(" where confdate >='" + mDay[0] + "' and  confdate <='" + mDay[1] + "'");
        hzSql.append(" and managecom like '"+mGlobalInput.ManageCom+"%' and sumgetmoney <> 0 with ur");
        tSSRS = tExeSQL.execSQL(hzSql.toString());
        String tSumMoney = "0.00";
        if(tSSRS.MaxRow > 0){
        	if ( ! tSSRS.GetText(1, 1).equals("null") ){
        		tSumMoney = tSSRS.GetText(1, 1);
        	}
        	SumNumber = Long.parseLong(tSSRS.GetText(1, 2));
        }
         
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FFPayDay.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", tSumMoney);
        texttag.add("SumNumber", SumNumber);
        
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.addListTable(alistTable, strSumArr);  
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    /**
     * 新方法 X2 -实付日结单  
     * @return
     */
    private boolean getNewPrintData(){
    	
        SSRS tSSRS = new SSRS();
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
       
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //String tServerPath= "E:/sinowork/picch/WebRoot/";
        ExeSQL tExeSQL = new ExeSQL();
        
        String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        TextTag texttag = new TextTag();       //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate",   mDay[1]);
        texttag.add("ManageCom", manageCom);
        xmlexport.createDocument("FFPayDay.vts", "printer"); //最好紧接着就初始化xml文档
        
        String[] detailArr = new String[]{"付款方式","号码类型","号码","金额","件数"};
        String[] hzArr = new String[]{"汇总类型","金额","数量"};

        //  按方式打印 收费明细SQL
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X2SF");
       	ListTable tDetailListTable = getDetailListTable(msql, "Pay");      
       	xmlexport.addListTable(tDetailListTable, detailArr);
        	 
       	// 收费日结汇总SQL
       	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X2SFZ");
       	ListTable tHZListTable = getDetailListTable(msql, "SUM");           
       	xmlexport.addListTable(tHZListTable, hzArr);                            	

       	// 收费合计SQL
    	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X2SFHJ"); 
        String[] sTotal = getHJ(msql);// 收费汇总 总金额 总件数
        
        texttag.add("SumMoney",  sTotal[0]);// $=/SumMoney
        texttag.add("SumNumber", sTotal[1]);// $=/SumMoney  
        
        if (texttag.size() > 0){
            xmlexport.addTextTag(texttag);
        }      	
        
        mResult.clear();
        mResult.addElement(xmlexport);
    	return true;
    }
    
    /**
     * 收费合计
     * @param hjSQL
     * @return
     */
    private String[] getHJ(String hjSQL){
    	SSRS nSSRS = new SSRS();
        nSSRS = new ExeSQL().execSQL(hjSQL);
        String[] total = new String[]{"0","0"};
        if(nSSRS.MaxRow > 0){
        	total[0] = nSSRS.GetText(1, 2);  // $=/SumMoney
        	total[1] = nSSRS.GetText(1, 3); // $=/SumMoney
        }
        return total;
    }
    
	/**
	 * 获得明细ListTable
	 * @param msql - 执行的SQL
	 * @param tName - Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName){
		SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName(tName);			
        for (int i = 1; i <= tSSRS.MaxRow; i++){
            strArr = new String[tSSRS.MaxCol]; // 列数
            for (int j = 1; j <= tSSRS.MaxCol; j++){
                if (j == 3){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    continue;
                }
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        return tlistTable;
	}
}