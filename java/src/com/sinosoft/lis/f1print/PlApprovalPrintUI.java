package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author hdl
 * @version 1.0
 * @date 2012-06-20
 * @function print notice bill User Interface Layer
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlApprovalPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	public PlApprovalPrintUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			PlApprovalPrintBL tPlApprovalPrintBL = new PlApprovalPrintBL();
			System.out.println("Start tPlApprovalPrintUI Submit ...");
			if (!tPlApprovalPrintBL.submitData(cInputData, cOperate)) {
				if (tPlApprovalPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlApprovalPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlApprovalPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlApprovalPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			buildError("submitData", "执行错误，错误原因："+e.toString());
			return false;
		}
	}
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlApprovalPrintUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}
}
