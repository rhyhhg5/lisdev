package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LYReturnFromBankBDB;
import com.sinosoft.lis.db.LYReturnFromBankDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LYReturnFromBankBSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;

public class PrintBillYFRightBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;
    private double mSumGetMoney = 0;
    private GlobalInput mG = new GlobalInput();

    private String mBankName; //银行名称
    private String mErrorReason; //失败原因
    private String mChkSuccFlag; //银行校验成功标志；
    private String mChkFailFlag; //银行校验失败标志；
    public PrintBillYFRightBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData begin");
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        //strMngCom = (String) cInputData.get(2);
        mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        strMngCom = mG.ManageCom;
        System.out.println("在PrintBillRightBL.java中得到的批次号码是" + strBillNo);
        System.out.println("在PrintBillRightBL.java中得到的银行代码是" + strBankCode);
        System.out.println("在PrintBillRightBL.java中得到的管理机构是" + strMngCom);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PrintBillYFRightBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        System.out.println("查询银行名称的函数！！！");
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(strBankCode);
        LDBankSet tLDBankSet = new LDBankSet();
        tLDBankSet.set(tLDBankDB.query());
        int bank_count = tLDBankSet.size();
        if (bank_count == 0)
        {
            this.mErrors.copyAllErrors(tLDBankDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintBillRightBL";
            tError.functionName = "submitData";
            tError.errorMessage = "银行信息表查询失败，没有该银行代码！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LDBankSchema tLDBankSchema = new LDBankSchema();
        tLDBankSchema.setSchema(tLDBankSet.get(1));
        mBankName = tLDBankSchema.getBankName();
        System.out.println("打印出银行信息和批次号码！！！");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        System.out.println("对银行返回盘记录表的查询");
        LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
        LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
        String y_sql = "select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode = '"
                       + strBankCode.trim()
                       + "' and ComCode like '"
                       + strMngCom.trim()
                       + "%'"
                       +" union select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode in (select bankcode from ldbankunite where bankunitecode = '"
                       + strBankCode.trim()
                       + "') and ComCode like '"
                       + strMngCom.trim()
                       + "%'";
        tLYReturnFromBankSet = tLYReturnFromBankDB.executeQuery(y_sql);
        System.out.println("在返回盘中所执行的查询语句是" + y_sql);
        int n = tLYReturnFromBankSet.size();
        if (n == 0)
        {

            System.out.println("该次交易可能被核销，在银行返回盘记录备份表中进行查询");
            LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
            LYReturnFromBankBSet tLYReturnFromBankBSet = new
                    LYReturnFromBankBSet();
            String t_sql = "select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and BankCode = '"
                           + strBankCode.trim()
                           + "' and ComCode like '"
                           + strMngCom.trim()
                           + "%'"
                           +" union select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and BankCode in (select bankcode from ldbankunite where bankunitecode = '"
                           + strBankCode.trim()
                           + "') and ComCode like '"
                           + strMngCom.trim()
                           + "%'";
            tLYReturnFromBankBSet = tLYReturnFromBankBDB.executeQuery(t_sql);
            System.out.println("在备份表中所执行的查询语句是" + t_sql);
            int m = tLYReturnFromBankBSet.size();
            int count = 0;
            if (m == 0)
            {
                this.mErrors.copyAllErrors(tLYReturnFromBankBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有满足条件的代付正确清单信息！！！！";
                this.mErrors.addOneError(tError);
                return false;
            }

            int b_count;
            b_count = 1;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintYFBillRight.vts", "printer");
            ListTable alistTable = new ListTable();
            alistTable.setName("INFO");
            for (int j = 1; j <= m; j++)
            {
                System.out.println("在银行返回盘记录备份表的处理中此时的J的数值是￥￥￥￥" + j);
                String[] cols = new String[8];
                LYReturnFromBankBSchema tLYReturnFromBankBSchema = new
                        LYReturnFromBankBSchema();
                tLYReturnFromBankBSchema.setSchema(tLYReturnFromBankBSet.get(j));
                String tbPayCode = tLYReturnFromBankBSchema.getPayCode();
                String b_BankSuccFlag;
                if (tLYReturnFromBankBSchema.getBankSuccFlag() == null ||
                    tLYReturnFromBankBSchema.getBankSuccFlag().equals(""))
                {
                    b_BankSuccFlag = "0";
                }
                else
                {
                    b_BankSuccFlag = tLYReturnFromBankBSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + b_BankSuccFlag);
                //判断不同银行的成功标志，胡博的方法
                //2003-04-1-25
                System.out.println("采用了胡博的方法");
                String hq_flag_b = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag_b);
                boolean jy_flag_b = verifyBankSuccFlag(hq_flag_b,
                        b_BankSuccFlag);

                System.out.println("校验后的成功标志是" + jy_flag_b);
                if (!jy_flag_b)
                {
                    continue;
                }
                System.out.println("//2003-11-06刘岩松改正");

                //2003-11-06刘岩松改正
                count++;
                //count--;

                b_count++;
                cols[0] = tLYReturnFromBankBSchema.getAgentCode();
                cols[3] = tLYReturnFromBankBSchema.getAccNo();
                cols[4] = tLYReturnFromBankBSchema.getAccName();
                cols[5] = tLYReturnFromBankBSchema.getPayCode();
                cols[7] = Double.toString(tLYReturnFromBankBSchema.getPayMoney());
                System.out.println("当J的值是" + j + "时，银行的账号是" + cols[3]);
                System.out.println("当J的值是" + j + "时，银行的账号名是" + cols[4]);
                mSumGetMoney = mSumGetMoney +
                               tLYReturnFromBankBSchema.getPayMoney();
                ExeSQL exesql = new ExeSQL();
                System.out.println("---------------cols[0]---------"+cols[0]);
                //cols[0]==null，所以不执行下面的程序---亓莹莹修改
                if (!(StrTool.cTrim(cols[0]).equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";
                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                        
                        //将AgentCode转换为GroupAgentCode
    			        //统一工号修改
    			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
    			        ExeSQL aExeSQL = new ExeSQL();
    			        SSRS aSSRS = new SSRS();
    			        aSSRS = aExeSQL.execSQL(SQL);
    				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
    				  		cols[0] = aSSRS.GetText(1, 1);
    				  	}
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                        System.out.println("2003-11-07刘岩松");
                    }
                }
                //亓莹莹添加
                if(cols[0]==null){
                    cols[0]="无";
                    cols[1]="无";
                    cols[2]="无";
                }
                if(cols[3]==null){
                    cols[3]="无";
                }
                if(cols[4]==null){
                    cols[4]="无";
                }
                if(cols[5]==null){
                    cols[5]="无";
                }
                if(cols[7]==null){
                    cols[7]="无";
                }

                    String Type_sql =
                            "Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
                            + tbPayCode + "'";
                    //SSRS Type_ssrs = exesql.execSQL(Type_sql);
                    String strType = exesql.getOneValue(Type_sql);
                    if (StrTool.cTrim(strType).equals(""))
                    {
                       CError tError = new CError();
                       tError.moduleName = "PrintBillBL";
                       tError.functionName = "submitData";
                       tError.errorMessage = "实付表中没有相应的数据信息！";
                       this.mErrors.addOneError(tError);
                       return false;
                    }
                    //cols[5] = Type_ssrs.GetText(1, 1);
                    cols[6] = strType;
                System.out.println("在应收总表中查询出总金额");
                alistTable.add(cols);
            }
            String[] col = new String[8];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("SumDuePayMoney", mSumGetMoney);
            texttag.add("Date", PubFun.getCurrentDate());
            texttag.add("SumCount", count);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }

            //xmlexport.outputDocumentToFile("e:\\","printbill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);

            System.out.println("￥￥￥￥￥￥b_count的值是====" + b_count);
            if (b_count == 1)
            {
                this.mErrors.copyAllErrors(tLDBankDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有银行返回代扣成功清单记录！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            int _count;
            _count = 1;

            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintYFBillRight.vts", "printer");
            ListTable blistTable = new ListTable();
            blistTable.setName("INFO");
            int count = 0;
            for (int i = 1; i <= n; i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = new
                        LYReturnFromBankSchema();
                tLYReturnFromBankSchema.setSchema(tLYReturnFromBankSet.get(i));
                //记录账号名和账户
                String[] cols = new String[8];
                String mBankSuccFlag;
                String _BankSuccFlag;
                if (tLYReturnFromBankSchema.getBankSuccFlag().equals("") ||
                    tLYReturnFromBankSchema.getBankSuccFlag() == null)
                {
                    _BankSuccFlag = "0";
                }
                else
                {
                    _BankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                }
                System.out.println("采用了胡博的方法后回盘表的数据");
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                String hq_flag = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag);
                boolean jy_flag = verifyBankSuccFlag(hq_flag, _BankSuccFlag);
                System.out.println("银行的校验标志是" + jy_flag);
                if (!jy_flag)
                {
                    continue;
                }
                _count++;
                count++;
                cols[0] = tLYReturnFromBankSchema.getAgentCode();
                cols[3] = tLYReturnFromBankSchema.getAccNo();
                cols[4] = tLYReturnFromBankSchema.getAccName();
                cols[5] = tLYReturnFromBankSchema.getPayCode();
                cols[7] = Double.toString(tLYReturnFromBankSchema.getPayMoney());
                System.out.println("银行账号是" + cols[3]);
                System.out.println("银行账号名是" + cols[4]);

                String tPayCode = tLYReturnFromBankSchema.getPayCode();
                mBankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                mSumGetMoney = mSumGetMoney +
                               tLYReturnFromBankSchema.getPayMoney();
                ExeSQL exesql = new ExeSQL();
                if (!(cols[0].equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql =
                            "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                            + cols[0] + "'";

                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql =
                            "Select Name from LABranchGroup Where AgentGroup = '"
                            + AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = "无";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" +
                                           AgentName_ssrs.GetText(1, 1));
                        
                        //将AgentCode转换为GroupAgentCode
    			        //统一工号修改
    			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
    			        ExeSQL aExeSQL = new ExeSQL();
    			        SSRS aSSRS = new SSRS();
    			        aSSRS = aExeSQL.execSQL(SQL);
    				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
    				  		cols[0] = aSSRS.GetText(1, 1);
    				  	}
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" +
                                           AgentGroup_ssrs.GetText(1, 1));
                    }
                }

                    String Type_sql =
                            "Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
                            + tPayCode + "'";
                    //SSRS Type_ssrs = exesql.execSQL(Type_sql);
                    String StrType = exesql.getOneValue(Type_sql);
                    //cols[5] = Type_ssrs.GetText(1, 1);
                    if (StrTool.cTrim(StrType).equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "PrintBillBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "实付表中没有相应的数据信息！";
                        this.mErrors.addOneError(tError);
                       return false;
                    }
                    cols[6] = StrType;

                System.out.println("在应收总表中查询出总金额");
                System.out.println("2003-11-07刘岩松二");

                blistTable.add(cols);
                System.out.println("getPrintData end");
            }
            String[] b_col = new String[8];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(blistTable, b_col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("SumDuePayMoney", mSumGetMoney);
            texttag.add("Date", PubFun.getCurrentDate());
            texttag.add("SumCount", count);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            //xmlexport.outputDocumentToFile("e:\\","PrintYFBill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
            System.out.println("getPrintData begin");
        }
        //对变量进行附值
        return true;
    }

//获取银行的成功标志
    public String getBankSuccFlag(String tBankCode)
    {
        try
        {
            LDBankSchema tLDBankSchema = new LDBankSchema();

            tLDBankSchema.setBankCode(tBankCode);
            tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));

            return tLDBankSchema.getAgentGetSuccFlag();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new NullPointerException("获取银行扣款成功标志信息失败！(getBankSuccFlag) " +
                                           e.getMessage());
        }
    }
   

//校验成功标志的方法
    public boolean verifyBankSuccFlag(String bankSuccFlag, String bBankSuccFlag)
    {
        int i;
        boolean passFlag = false;

        String[] arrSucc = PubFun.split(bankSuccFlag, ";");
        String tSucc = bBankSuccFlag;

        for (i = 0; i < arrSucc.length; i++)
        {
            if (arrSucc[i].equals(tSucc))
            {
                passFlag = true;
                break;
            }
        }

        return passFlag;
    }

    public static void main(String[] args)
    {
    }
}
