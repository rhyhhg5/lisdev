package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.StrTool;
import java.io.File;

/**
 * <p>
 * Title: 保险业务系统
 * </p>
 * <p>
 * Description: 首期缴费通知书批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author zsj
 */
public class NewFirstPayBatchPrtBL {
	private String mOperate;

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput mG = new GlobalInput();

	public NewFirstPayBatchPrtBL() {
	}

	private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

	private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();

	private boolean operFlag = true;

	private String strLogs = "";

	private String Content = "";

	private String mxmlFileName = "";

	private int mCount = 0;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap mMap = new MMap();

	private PubSubmit tPubSubmit = new PubSubmit();

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		cInputData = (VData) cInputData.clone();

		this.mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private boolean getInputData(VData cInputData) {
		mLOPRTManagerSet = (LOPRTManagerSet) cInputData.getObjectByObjectName(
				"LOPRTManagerSet", 0);
		if (mLOPRTManagerSet == null) { // （服务事件关联表）
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FirstHastenBatchPrtBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的数据为空!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData
				.getObjectByObjectName("LDSysVarSchema", 0));
		return true;
	}

	/***************************************************************************
	 * Name :checkdate Function :判断数据逻辑上的正确性 1:判断用户代码和用户姓名的关联是否正确
	 * 2:校验用户和录入的管理机构是否匹配 3:若不是最高的核赔权限的用户一定要有上级用户代码 4:若是新增，判断该用户代码是否已经存在。
	 * 
	 */
	private boolean checkdata() {
		return true;
	}

	public int getCount() {
		return mCount;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
        if(!prepareOutputData()){
        	return false;
        }
		VData tVData = new VData();
		tVData.add(mG);
		tVData.add(mLOPRTManagerSet);
		PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
		if (!tPDFPrintBatchManagerBL.submitData(tVData, mOperate)) {
			mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
			return false;
		}
		return true;
	}

	private boolean prepareOutputData() {
		int cCont = mLOPRTManagerSet.size();
		LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
		for (int i = 1; i <=cCont; i++) {
			LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
//			tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSet.get(i).getPrtSeq());
//			LOPRTManagerSchema tLOPRTManagerSchema = tLOPRTManagerDB.query()
//					.get(1);
//			避免全表查询 by gzh 20130911
			String tSQL = "select * from LOPRTManager where PrtSeq = '"+mLOPRTManagerSet.get(i).getPrtSeq()+"' ";
			LOPRTManagerSet tempLOPRTManagerSet = tLOPRTManagerDB.executeQuery(tSQL);
			if(tempLOPRTManagerSet == null || tempLOPRTManagerSet.size()<1){
				CError tError = new CError();
				tError.moduleName = "NewFirstPayBatchPrtBL";
				tError.functionName = "prepareOutputData";
				tError.errorMessage = "获取打印数据失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			LOPRTManagerSchema tLOPRTManagerSchema = tempLOPRTManagerSet.get(1);
			tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema
					.getPrintTimes() + 1);
			tLOPRTManagerSet.add(tLOPRTManagerSchema);
		}
		mLOPRTManagerSet = tLOPRTManagerSet;
		return true;

	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */

	public void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "IndiDueFeeBatchPrtBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ComCode = "86";
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "001";
		LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
		mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");
		LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		VData aVData = new VData();
		aVData.add(tGlobalInput);
		aVData.add(tLOPRTManagerSet);
		aVData.add(mLDSysVarSchema);
		FirstPayBatchPrtBL tFirstPayBatchPrtBL = new FirstPayBatchPrtBL();
		tFirstPayBatchPrtBL.submitData(aVData, "CONFIRM");
		int tCount = tFirstPayBatchPrtBL.getCount();
		System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
		String tFileName[] = new String[tCount];
		VData tResult = new VData();
		tResult = tFirstPayBatchPrtBL.getResult();
		tFileName = (String[]) tResult.getObject(0);
		System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

	}

	private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {
		// 查找打印服务
		String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
		strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
		strSQL += " AND OtherSign = '0'";
		System.out.println(strSQL);
		LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

		if (tLDCodeSet.size() == 0) {
			buildError("dealData", "找不到对应的打印服务类(Code = '"
					+ aLOPRTManagerSchema.getCode() + "')");
			return false;
		}

		// 调用打印服务
		LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

		try {
			Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
			PrintService ps = (PrintService) cls.newInstance();
			// 准备数据
			String strOperate = tLDCodeSchema.getCodeName();
			VData vData = new VData();
			vData.add(mG);
			vData.add(aLOPRTManagerSchema);

			if (!ps.submitData(vData, strOperate)) {
				mErrors.copyAllErrors(ps.getErrors());
				return false;
			}
			mResult = ps.getResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("callPrintService", ex.toString());
			return false;
		}
		return true;
	}
}
