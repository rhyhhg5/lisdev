package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

class LAMarketClaimBL {
	public LAMarketClaimBL() {

	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mGrpContNo = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LAMarketClaimBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			this.mManageCom = (String) mTransferData
					.getValueByName("tManageCom");
			this.mStartDate = (String) mTransferData
					.getValueByName("tStartDate");
			this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
			this.mGrpContNo = (String) mTransferData
					.getValueByName("tGrpContNo");
			if (mManageCom == null || mManageCom.equals("")) {
				this.mManageCom = "86";
			}
			if (mStartDate == null || mStartDate.equals("")) {
				this.mStartDate = getYear() + "-01-01";
			}
			if (mEndDate == null || mEndDate.equals("")) {
				this.mEndDate = getYear() + "-12-31";
			}

			System.out.println(mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LAMarketClaim.vts", "printer");

		String tMakeDate = "";
		String tMakeTime = "";
		/*
		 * tMakeDate = mPubFun.getCurrentDate(); tMakeTime =
		 * mPubFun.getCurrentTime();
		 */
		System.out.print("dayin252");

		tTextTag.add("ManageComName", mManageCom);
		tTextTag.add("ContNo", mGrpContNo);
		tTextTag.add("StartDate", mStartDate);
		tTextTag.add("EndDate", mEndDate);
		System.out.println("1212121" + tMakeDate);
		if (tTextTag.size() < 1) {
			return false;
		}
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "", "", "", "", "" };
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private String getYear() {
		String date = PubFun.getCurrentDate();
		String mYear = date.substring(0, date.indexOf('-'));
		return mYear;
	}

	private boolean getDataList() {
		String sql = "";
		if (this.mGrpContNo == null || "".equals(this.mGrpContNo)) {
			sql = "select c.managecom,c.grpcontno,p.Peoples2,p.riskcode,p.grppolno,p.prem,p.payintv from lcgrpcont c,lcgrppol p  where c.grpcontno=p.grpcontno and c.salechnl='02' and c.markettype='2' and c.polapplydate>='"
					+ mStartDate
					+ "' and c.polapplydate<='"
					+ mEndDate
					+ "' and c.managecom like '" + mManageCom + "%'with ur";
		} else {
			sql = "select c.managecom,c.grpcontno,p.Peoples2,p.riskcode,p.grppolno,p.prem,p.payintv from lcgrpcont c,lcgrppol p  where c.grpcontno=p.grpcontno and c.salechnl='02' and c.markettype='2' and c.polapplydate>='"
					+ mStartDate
					+ "' and c.polapplydate<='"
					+ mEndDate
					+ "' and c.managecom like '"
					+ mManageCom
					+ "%' and c.grpcontno='" + mGrpContNo + "'with ur";

		}
		SSRS tSSRS = new SSRS();

		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				String peoples2 = tSSRS.GetText(i, 3);
				double prem = Double.parseDouble(tSSRS.GetText(i, 6));

				String intv = tSSRS.GetText(i, 7);
				String grppolno = tSSRS.GetText(i, 5);
				String riskNum = getContOfRiskPeople(grppolno);
				String realpay = getRealpayForDecide(grppolno);
				String ListInfo[] = new String[8];
				ListInfo[0] = tSSRS.GetText(i, 1);
				ListInfo[1] = tSSRS.GetText(i, 2);
				System.out.println("1111111111111" + ListInfo[1]);
				ListInfo[2] = tSSRS.GetText(i, 4);
				ListInfo[3] = riskNum;
				ListInfo[4] = getRateOfRisk(peoples2, riskNum);
				ListInfo[5] = realpay;
				ListInfo[6] = getRateToPay(prem, intv, realpay);
				ListInfo[7] = getRealpayForNotDecide(grppolno);

				mListTable.add(ListInfo);
			}

		} else {
			String ListInfo[] = new String[8];
			ListInfo[0] = " ";
			ListInfo[1] = " ";
			System.out.println("2222222222222222222" + ListInfo[1]);
			ListInfo[2] = " ";
			ListInfo[3] = " ";
			ListInfo[4] = " ";
			ListInfo[5] = " ";
			ListInfo[6] = " ";
			ListInfo[7] = " ";

			mListTable.add(ListInfo);
		}
		return true;

	}

	/**
	 * @total 投保总人数 @ riskNum 出险人数 
	 * @return 百分率计算结果
	 */

	private String getRateOfRisk(String total, String riskNum) {
		double ts = Double.parseDouble(total);
		double rn = Double.parseDouble(riskNum);
		double resultD = rn / ts * 100;
		if (ts != 0) {
			DecimalFormat tDF = new DecimalFormat("0.##");
			String resultS = tDF.format(resultD);
			return resultS + "%";
		} else {
			return "0.00%";
		}

	}

	/**
	 * @grppolno 集体保单险种号码 根据集体保单险种号码查询出已决赔款 即：rgtState 属于‘09’，‘11’，‘12’中任一
	 * @return 已决赔款
	 */

	private String getRealpayForDecide(String grppolno) {
		String sql = "select sum(d.realpay) from llclaimdetail d,llcase c where d.caseno=c.caseno and c.rgtstate in('09','11','12') and d.GrpPolNo='"
				+ grppolno + "'";
		System.out.println("realpay sql:" + sql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS != null) {
			String result = tSSRS.GetText(1, 1);
			if (result != null && !"null".equals(result)) {
				return result;
			}
			return "0";
		}

		return "0";
	}

	/**
	 * @grppolno 集体保单险种号码 根据集体保单险种号码查询出未决赔款 即：rgtState 不属于‘09’，‘11’，‘12’中任一个
	 * @return 未决赔款
	 */
	private String getRealpayForNotDecide(String grppolno) {
		String sql = "select sum(d.realpay) from llclaimdetail d,llcase c where d.caseno=c.caseno and c.rgtstate not in('09','11','12') and d.GrpPolNo='"
				+ grppolno + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS != null) {
			String result = tSSRS.GetText(1, 1);
			if (result != null && !"null".equals(result)) {
				return result;
			} else {
				return "0";
			}
		}

		return "0";
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * @grppolno 集体保单险种号码 
	 * @return 出险人数 
	 */
	private String getContOfRiskPeople(String grppolno) {
		String sql = "select count(distinct caseno)  from llclaimdetail  where grppolno='"
				+ grppolno + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS != null) {
			return tSSRS.GetText(1, 1);

		}
		return "0";
	}

	/**
	 * 
	 * @param prem
	 *            保费
	 * @param payIntv
	 *            交费间隔
	 * @param realpay
	 *            核赔赔付金额
	 * @return 赔付率
	 */

	private String getRateToPay(double prem, String payIntv, String realpay) {
		double realPrem;
		if (payIntv != null) {
			if ("1".equals(payIntv)) {
				realPrem = prem * 12;
			} else if ("3".equals(payIntv)) {
				realPrem = prem * 4;
			} else if ("6".equals(payIntv)) {
				realPrem = prem * 2;
			} else {
				realPrem = prem;
			}
			if (!"0".equals(realpay)) {
				return getRateOfRisk(Double.toString(realPrem), realpay);
			}
		}
		return "0.00%";

	}

	public static void main(String[] args) {
		LAMarketClaimBL lb = new LAMarketClaimBL();
		/*
		 * //System.out.println(lb.getRateOfRisk("70", "13")); VData mResult =
		 * new VData(); //mResult.add();
		 * 
		 * TransferData tTransferData= new TransferData(); // 传参
		 * tTransferData.setNameAndValue("tManageCom","86");
		 * tTransferData.setNameAndValue("tGrpContNo","");
		 * 
		 * tTransferData.setNameAndValue("tStartDate","");
		 * tTransferData.setNameAndValue("tEndDate","");
		 * 
		 * 
		 * VData tVData = new VData();
		 * 
		 * tVData.addElement(tTransferData);
		 */
		System.out.println(lb.getYear());

	}

}
