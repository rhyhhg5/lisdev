package com.sinosoft.lis.f1print;

import java.io.*;
import com.sinosoft.lis.pubfun.*;

/**
* <p>Title: 追加保费清单</p>
* <p>Description:追加保费清单</p>
* <p>Copyright: Copyright (c) 2006</p>
* <p>Company: Sinosoft</p>
* @author QiuYang
* @version 1.0
*/

public class PEdorBodyCheckPrintUI
{
    PEdorBodyCheckPrintBL mPEdorBodyCheckPrintBL = null;

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public PEdorBodyCheckPrintUI(GlobalInput gi, String prtNo)
    {
        mPEdorBodyCheckPrintBL = new PEdorBodyCheckPrintBL(gi, prtNo);
    }

    /**
     * 调用业务逻辑
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mPEdorBodyCheckPrintBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorBodyCheckPrintBL.mErrors.getFirstError();
    }

    /**
     * 得到xml输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return mPEdorBodyCheckPrintBL.getInputStream();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String args[])
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ManageCom = "86";
        PEdorBodyCheckPrintUI tPEdorBodyCheckPrintUI =
                new PEdorBodyCheckPrintUI(gi, "81000001595");
        if (!tPEdorBodyCheckPrintUI.submitData())
        {
            System.out.println(tPEdorBodyCheckPrintUI.getError());
        }
    }
}
