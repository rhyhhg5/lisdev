package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class CustomerContQueryBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//  取得的报表类型
    private String mQYType = "";

//  取得的管理机构名称
    private String mManageComName = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(mBranchAttr!=null&&!mBranchAttr.equals("")){
        	if (!checkData())
            {
                return false;
            }
        }
        
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }
    private boolean checkData(){
    	ExeSQL tExeSQL=new ExeSQL();
    	SSRS tSSRS=new SSRS();
    	String tSql="select * from labranchgroup where agentgroup in " 
    			    +"(select agentgroup from latree where agentcode='"
    			    +mAgentCode
    	            +"') and branchattr ='"+mBranchAttr+"'";
    	tSSRS=tExeSQL.execSQL(tSql);
    	if(tSSRS.getMaxRow()<=0){
    		CError tCError=new CError();
    		tCError.moduleName="CustomerContQueryBL";
    		tCError.functionName="checkData";
    		tCError.errorMessage="输入信息错误，输入的业务员不在指定的销售单位中！";
    		this.mErrors.addOneError(tCError);
    		return false;
    	}
    	return true;
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
     	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mAgentCode = (String)cInputData.get(2);
    	mQYType = (String)cInputData.get(3);
    	mManageComName = (String)cInputData.get(4);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CustomerContQueryBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
    	System.out.println("你好无聊啊啊啊啊啊啊啊啊！");
    	if(mQYType.equals("1")){
    		TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
            txmlexport.createDocument("AppntCustomerContInfo.vts", "printer"); //最好紧接着就初始化xml文档
            ListTable tListTable = new ListTable();
            tListTable.setName("AppntCustomerContInfo");
            String[] title = {"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" ,""};
           
//        	String tAgentGroup = "";
//           //前台传入的参数为外部编码，因此需要做转换
//           if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
//           {
//            String GroupSQL = "select agentgroup from labranchgroup where branchtype='1' and branchattr='"+this.mBranchAttr+"'";  
//        	ExeSQL tExeSQL = new ExeSQL();
//        	tAgentGroup = tExeSQL.getOneValue(GroupSQL);
//           }
           //查询所有的记录
            msql = "select distinct b.contno A, b.signdate , "
         	   +"( select sum(prem) from lcpol where prtno=b.prtno and uwflag in ('4', '9') and conttype='1' and appflag='1' ),"
         	   +"case b.StateFlag when '2' then '无效' when '3' then '满期' else '有效' end,"
         	   +"b.appntno, b.appntname, a.appntbirthday,"
         	   +"(select codename from ldcode where codetype='payintv' and code=char(b.payintv)),"
         	   +"case a.appntsex when '0' then '男' when '1' then '女' else '其他' end,"
         	   +"a.OccupationCode,c.HomePhone,c.CompanyPhone,c.Mobile,c.PostalAddress,a.AccName,a.BankAccNo,"
         	   +"(select bankname from ldbank where bankcode=a.bankcode and cansendflag='1'),"
         	   +"(select name from labranchgroup where agentgroup=b.agentgroup) ,getUniteCode(b.agentcode),"
         	   +"(select name from laagent where agentcode=b.agentcode),"
         	   +"(select Mobile from laagent where agentcode=b.agentcode),"
         	   +"(select distinct 'Y' from lcpol where riskcode in ( select riskcode from lmriskapp where riskprop='I' and risktype2='5' ) and prtno=b.prtno and conttype='1'  and appflag='1' and uwflag in ( '4' , '9' ) ), "
         	   +"case when (select maketype from laascription where contno=b.contno and agentnew=b.agentcode "
         	   +"order by modifydate desc fetch first 1 rows only) is null then '自有保单' else "
         	   +"(select case maketype when '02' then '孤儿单自动分配' when '01' then '保单手工分配' else '未知来源' end from laascription where contno=b.contno and agentnew=b.agentcode "
         	   +"order by modifydate desc fetch first 1 rows only) end "
         	   +"from lcappnt a, lccont b, lcaddress c "
         	   +"where a.prtno=b.prtno and b.appflag='1' and b.conttype='1' and a.appntno=b.appntno"
         	   +" and a.appntno=c.customerno and c.addressno in"
         	   +"( select max ( addressno ) from lcaddress a ,lccont b where c.customerno=a.customerno     and b.appflag='1' and b.conttype='1' and b.appntno=a.customerno )"
         	   +"and b.appntno=c.customerno";
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))    
            {
         	   
         	   msql += " and b.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and b.agentcode='"+this.mAgentCode+"'";  	   
            }
            if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
            {
         	   msql+= " and b.agentcode in (select agentcode from labranchgroup where branchattr ='"+this.mBranchAttr+"')";    	   
            }
            msql += " order by A with ur"; 
          ExeSQL mExeSQL = new ExeSQL();
          SSRS mSSRS = new SSRS();
          mSSRS = mExeSQL.execSQL(msql);
          System.out.println(msql);
          if (mSSRS.mErrors.needDealError()) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "查询XML数据出错！";

              this.mErrors.addOneError(tCError);

              return false;

          }
          if (mSSRS.getMaxRow() <= 0) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";

              this.mErrors.addOneError(tCError);

              return false;
          }
          if(mSSRS.getMaxRow()>=1)
          {
        	 for(int i=1;i<=mSSRS.getMaxRow();i++)
        	 {
        	   
        	  String Info[] = new String [23];
        	  Info=mSSRS.getRowData(i);
//        	  System.out.println(mSSRS.GetText(i, 1));
//        	  Info[0] = mSSRS.GetText(i, 1);
//        	  System.out.println(mSSRS.GetText(i, 2));
//        	  Info[1] = mSSRS.GetText(i, 2);
//        	  Info[2] = mSSRS.GetText(i, 3);
//        	  Info[3] = mSSRS.GetText(i, 4);
//        	  Info[4] = mSSRS.GetText(i, 5);
//        	  Info[5] = mSSRS.GetText(i, 6);
//        	  Info[6] = mSSRS.GetText(i, 7);
//        	  Info[7] = mSSRS.GetText(i, 8);
//        	  Info[8] = mSSRS.GetText(i, 9);
//        	  Info[9] = mSSRS.GetText(i, 10);
//        	  Info[10] = mSSRS.GetText(i, 11);
//        	  Info[11] = mSSRS.GetText(i, 12);
//        	  Info[12] = mSSRS.GetText(i, 13);
//        	  Info[13] = mSSRS.GetText(i, 14);
//        	  Info[14] = mSSRS.GetText(i, 15);
//        	  Info[15] = mSSRS.GetText(i, 16);
//        	  Info[16] = mSSRS.GetText(i, 17);
//        	  Info[17] = mSSRS.GetText(i, 18);
//        	  Info[18] = mSSRS.GetText(i, 19);
//        	  Info[19] = mSSRS.GetText(i, 20);
//        	  Info[20] = mSSRS.GetText(i, 21);
//        	  Info[21] = mSSRS.GetText(i, 22);
        	  tListTable.add(Info);
        	 }
          }
          else {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";
              this.mErrors.addOneError(tCError);
              return false;
          }
          	System.out.println("测试打印模版: "+txmlexport);
            mdate = PubFun.getCurrentDate();
            try{
            	mManageComName=new String(mManageComName.getBytes("ISO8859-1"),"GBK");
            }catch(Exception ex){
            	ex.printStackTrace();
            	CError tCError = new CError();
                tCError.moduleName = "CustomerContQueryBL";
                tCError.functionName = "getPrintData";
                tCError.errorMessage = "字符转换错误";

                this.mErrors.addOneError(tCError);

                return false;
            }
            texttag.add("ManageComName", mManageComName);  //管理机构名称
            texttag.add("MakeDate", mdate);
            if (texttag.size() > 0)
            {
                txmlexport.addTextTag(texttag);
            }
            txmlexport.addListTable(tListTable,title);
            
            //txmlexport.outputDocumentToFile("c:\\", "news2");
            mResult.clear();
            mResult.addElement(txmlexport);
    		return true;
    	}else if(mQYType.equals("2")){
    		TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
            txmlexport.createDocument("InsuredCustomerContInfo.vts", "printer"); //最好紧接着就初始化xml文档
            ListTable tListTable = new ListTable();
            tListTable.setName("InsuredCustomerContInfo");
            String[] title = {"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "",""};
           
//        	String tAgentGroup = "";
//           //前台传入的参数为外部编码，因此需要做转换
//           if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
//           {
//            String GroupSQL = "select agentgroup from labranchgroup where branchtype='1' and branchattr='"+this.mBranchAttr+"'";  
//        	ExeSQL tExeSQL = new ExeSQL();
//        	tAgentGroup = tExeSQL.getOneValue(GroupSQL);
//           }
           //查询所有的记录
            msql = "select distinct a.contno A, a.signdate , "
         	   +"case a.StateFlag when '2' then '无效' when '3' then '满期' else '有效' end,"
         	   +"(select appntname from lccont where prtno=a.prtno and appflag='1' and conttype='1'),"
         	   +"a.insuredno,a.Insuredname,case a.InsuredSex when '0' then '男' when '1' then '女' else '其他' end,"
         	   +"(select distinct 'Y' from lcpol where riskcode in (select riskcode from lmriskapp where riskprop='I' and risktype2='5') and prtno=a.prtno and conttype='1' and appflag='1' and uwflag in('4','9')),"
         	   +"(select OccupationCode from lcinsured where insuredno=a.insuredno and prtno=a.prtno),"
         	   +"c.Mobile,c.PostalAddress,c.EMail,(select name from laagent where agentcode=a.agentcode),"
         	   +"(select Mobile from laagent where agentcode=a.agentcode),"
         	   +"case when (select maketype from laascription where contno=a.contno and agentnew=a.agentcode "
        	   +"order by modifydate desc fetch first 1 rows only) is null then '自有保单' else "
        	   +"(select case maketype when '02' then '孤儿单自动分配' when '01' then '保单手工分配' else '未知来源' end from laascription where contno=a.contno and agentnew=a.agentcode "
        	   +"order by modifydate desc fetch first 1 rows only) end "
        	   +"from lcpol a,lcaddress c"
         	   +" where a.appflag='1' and a.conttype='1' and a.insuredno=c.customerno   and "
         	   +" c.addressno in (select varchar(max(int(AddressNo))) from lcaddress a,lcpol b where c.customerno=a.customerno and  b.appflag='1' and b.conttype='1' and  b.insuredno=a.customerno) ";
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))    
            {
         	   
         	   msql += " and a.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and a.agentcode='"+this.mAgentCode+"'";  	   
            }
            if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
            {
         	   msql+= " and a.agentcode in (select agentcode from labranchgroup where branchattr ='"+this.mBranchAttr+"')";    	   
            }
            msql += " order by A with ur"; 
          ExeSQL mExeSQL = new ExeSQL();
          SSRS mSSRS = new SSRS();
          mSSRS = mExeSQL.execSQL(msql);
          System.out.println(msql);
          if (mSSRS.mErrors.needDealError()) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "查询XML数据出错！";

              this.mErrors.addOneError(tCError);

              return false;

          }
          if (mSSRS.getMaxRow() <= 0) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";

              this.mErrors.addOneError(tCError);

              return false;
          }
          if(mSSRS.getMaxRow()>=1)
          {
        	 for(int i=1;i<=mSSRS.getMaxRow();i++)
        	 {
        	   
        	  String Info[] = null;
        	  Info=mSSRS.getRowData(i);
//        	  System.out.println(mSSRS.GetText(i, 1));
//        	  Info[0] = mSSRS.GetText(i, 1);
//        	  System.out.println(mSSRS.GetText(i, 2));
//        	  Info[1] = mSSRS.GetText(i, 2);
//        	  Info[2] = mSSRS.GetText(i, 3);
//        	  Info[3] = mSSRS.GetText(i, 4);
//        	  Info[4] = mSSRS.GetText(i, 5);
//        	  Info[5] = mSSRS.GetText(i, 6);
//        	  Info[6] = mSSRS.GetText(i, 7);
//        	  Info[7] = mSSRS.GetText(i, 8);
//        	  Info[8] = mSSRS.GetText(i, 9);
//        	  Info[9] = mSSRS.GetText(i, 10);
//        	  Info[10] = mSSRS.GetText(i, 11);
//        	  Info[11] = mSSRS.GetText(i, 12);
//        	  Info[12] = mSSRS.GetText(i, 13);
//        	  Info[13] = mSSRS.GetText(i, 14);
//        	  Info[14] = mSSRS.GetText(i, 15);
//        	  Info[15] = mSSRS.GetText(i, 16);
//        	  Info[16] = mSSRS.GetText(i, 17);
//        	  Info[17] = mSSRS.GetText(i, 18);
//        	  Info[18] = mSSRS.GetText(i, 19);
//        	  Info[19] = mSSRS.GetText(i, 20);
//        	  Info[20] = mSSRS.GetText(i, 21);
//        	  Info[21] = mSSRS.GetText(i, 22);
        	  tListTable.add(Info);
        	 }
          }
          else {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";
              this.mErrors.addOneError(tCError);
              return false;
          }
          	System.out.println("测试打印模版: "+txmlexport);
            mdate = PubFun.getCurrentDate();
            try{
            	mManageComName=new String(mManageComName.getBytes("ISO8859-1"),"GBK");
            }catch(Exception ex){
            	ex.printStackTrace();
            	CError tCError = new CError();
                tCError.moduleName = "CustomerContQueryBL";
                tCError.functionName = "getPrintData";
                tCError.errorMessage = "字符转换错误";

                this.mErrors.addOneError(tCError);

                return false;
            }
            texttag.add("ManageComName", mManageComName);  //管理机构名称
            texttag.add("MakeDate", mdate);
            if (texttag.size() > 0)
            {
                txmlexport.addTextTag(texttag);
            }
            txmlexport.addListTable(tListTable,title);
            
            //txmlexport.outputDocumentToFile("c:\\", "news2");
            mResult.clear();
            mResult.addElement(txmlexport);
    		return true;
    	}else if(mQYType.equals("3")){
    		TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
            txmlexport.createDocument("InsuredPremCustomerContInfo.vts", "printer"); //最好紧接着就初始化xml文档
            ListTable tListTable = new ListTable();
            tListTable.setName("InsuredPremCustomerContInfo");
            String[] title = {"" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "" , "",""};
           
//        	String tAgentGroup = "";
//           //前台传入的参数为外部编码，因此需要做转换
//           if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
//           {
//            String GroupSQL = "select agentgroup from labranchgroup where branchtype='1' and branchattr='"+this.mBranchAttr+"'";  
//        	ExeSQL tExeSQL = new ExeSQL();
//        	tAgentGroup = tExeSQL.getOneValue(GroupSQL);
//           }
            
           //查询所有的记录
            msql = "select distinct a.contno A, a.signdate , "
         	   +"case a.StateFlag when '2' then '无效' when '3' then '满期' else '有效' end,"
         	   +"(select appntname from lccont where prtno=a.prtno and appflag='1' and conttype='1'),"
         	   +"a.Insuredname,(select sum(prem) from lcpol where prtno=a.prtno and a.insuredno=insuredno and appflag='1' and conttype='1' and uwflag in('4','9')) ," 
         	   +"case a.InsuredSex when '0' then '男' when '1' then '女' else '其他' end,"
         	   +"(select distinct 'Y' from lcpol where riskcode in (select riskcode from lmriskapp where riskprop='I' and risktype2='5') and prtno=a.prtno and conttype='1' and appflag='1' and uwflag in('4','9')),"
         	   +"(select OccupationCode from lcinsured where insuredno=a.insuredno and prtno=a.prtno),"
         	   +"c.Mobile,c.PostalAddress,c.EMail,(select name from laagent where agentcode=a.agentcode),"
         	   +"(select Mobile from laagent where agentcode=a.agentcode),"
         	   +"case when (select maketype from laascription where contno=a.contno and agentnew=a.agentcode "
         	   +"order by modifydate desc fetch first 1 rows only) is null then '自有保单' else "
         	   +"(select case maketype when '02' then '孤儿单自动分配' when '01' then '保单手工分配' else '未知来源' end from laascription where contno=a.contno and agentnew=a.agentcode "
         	   +"order by modifydate desc fetch first 1 rows only) end "
         	   +"from lcpol a,lcaddress c"
         	   +" where a.appflag='1' and a.conttype='1' and a.insuredno=c.customerno   and "
         	   +" c.addressno in (select varchar(max(int(AddressNo))) from lcaddress a,lcpol b where c.customerno=a.customerno and  b.appflag='1' and b.conttype='1' and  b.insuredno=a.customerno) ";
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))    
            {
         	   
         	   msql += " and a.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and a.agentcode='"+this.mAgentCode+"'";  	   
            }
            if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
            {
         	   msql+= " and a.agentcode in (select agentcode from labranchgroup where branchattr ='"+this.mBranchAttr+"')";    	   
            }
            msql += " order by A with ur"; 
          ExeSQL mExeSQL = new ExeSQL();
          SSRS mSSRS = new SSRS();
          mSSRS = mExeSQL.execSQL(msql);
          System.out.println(msql);
          if (mSSRS.mErrors.needDealError()) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "查询XML数据出错！";

              this.mErrors.addOneError(tCError);

              return false;

          }
          if (mSSRS.getMaxRow() <= 0) {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";

              this.mErrors.addOneError(tCError);

              return false;
          }
          if(mSSRS.getMaxRow()>=1)
          {
        	 for(int i=1;i<=mSSRS.getMaxRow();i++)
        	 {
        	   
        	  String Info[] = null;
        	  Info=mSSRS.getRowData(i);
//        	  System.out.println(mSSRS.GetText(i, 1));
//        	  Info[0] = mSSRS.GetText(i, 1);
//        	  System.out.println(mSSRS.GetText(i, 2));
//        	  Info[1] = mSSRS.GetText(i, 2);
//        	  Info[2] = mSSRS.GetText(i, 3);
//        	  Info[3] = mSSRS.GetText(i, 4);
//        	  Info[4] = mSSRS.GetText(i, 5);
//        	  Info[5] = mSSRS.GetText(i, 6);
//        	  Info[6] = mSSRS.GetText(i, 7);
//        	  Info[7] = mSSRS.GetText(i, 8);
//        	  Info[8] = mSSRS.GetText(i, 9);
//        	  Info[9] = mSSRS.GetText(i, 10);
//        	  Info[10] = mSSRS.GetText(i, 11);
//        	  Info[11] = mSSRS.GetText(i, 12);
//        	  Info[12] = mSSRS.GetText(i, 13);
//        	  Info[13] = mSSRS.GetText(i, 14);
//        	  Info[14] = mSSRS.GetText(i, 15);
//        	  Info[15] = mSSRS.GetText(i, 16);
//        	  Info[16] = mSSRS.GetText(i, 17);
//        	  Info[17] = mSSRS.GetText(i, 18);
//        	  Info[18] = mSSRS.GetText(i, 19);
//        	  Info[19] = mSSRS.GetText(i, 20);
//        	  Info[20] = mSSRS.GetText(i, 21);
//        	  Info[21] = mSSRS.GetText(i, 22);
        	  tListTable.add(Info);
        	 }
          }
          else {
              CError tCError = new CError();
              tCError.moduleName = "CustomerContQueryBL";
              tCError.functionName = "getPrintData";
              tCError.errorMessage = "没有符合条件的客户保单信息！";
              this.mErrors.addOneError(tCError);
              return false;
          }
          	System.out.println("测试打印模版: "+txmlexport);
            mdate = PubFun.getCurrentDate();
            try{
            	mManageComName=new String(mManageComName.getBytes("ISO8859-1"),"GBK");
            }catch(Exception ex){
            	ex.printStackTrace();
            	CError tCError = new CError();
                tCError.moduleName = "CustomerContQueryBL";
                tCError.functionName = "getPrintData";
                tCError.errorMessage = "字符转换错误";

                this.mErrors.addOneError(tCError);

                return false;
            }
            texttag.add("ManageComName", mManageComName);  //管理机构名称
            texttag.add("MakeDate", mdate);
            if (texttag.size() > 0)
            {
                txmlexport.addTextTag(texttag);
            }
            txmlexport.addListTable(tListTable,title);
            
            //txmlexport.outputDocumentToFile("c:\\", "news2");
            mResult.clear();
            mResult.addElement(txmlexport);
    		return true;
    	}else {
    		CError tCError = new CError();
            tCError.moduleName = "CustomerContQueryBL";
            tCError.functionName = "getPrintData";
            tCError.errorMessage = "错误的客户保单报表打印类型！";
            this.mErrors.addOneError(tCError);
    		return false;
    	}
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}


























