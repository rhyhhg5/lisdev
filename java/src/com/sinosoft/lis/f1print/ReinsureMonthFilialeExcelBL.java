 
package com.sinosoft.lis.f1print;

/**
 * X9再保月结单（分公司）
 * @date 2013-09-22
 * @author yan
 */
import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ReinsureMonthFilialeExcelBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	String sWageNo = ""; // 月结单 格式：200807 yyyymm
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	String tOutXmlPath = "";
	String msql = "";

	public ReinsureMonthFilialeExcelBL() {
	}

	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		System.out.println("进入 ReinsureMonthFilialeExcelBL...X9-再保月结");
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}	
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) { // 打印提数
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @return  
	 */
	private boolean getInputData(VData cInputData) {
		String sBeginDate = (String) cInputData.get(0);// 获得WageNo
		sWageNo = sBeginDate;
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		System.out.println("tOutXmlPath--"+tOutXmlPath);
		
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径的信息！");
			return false;
		}
		return true;
	}
	
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ReinsureMonthFilialeExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
    
     /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
      
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", sWageNo);

        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
      //tServerPath= "E:/lisdev/ui/";
        
        int no = 5;  //初始化打印行数

        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[] { 
                "ZBYJ",//  再保月结单-明细
                "ZBYJZ",// 再保月结单-汇总
        };
        
        for (int i = 0; i < getTypes.length; i++) {
            msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = tExeSQL.execSQL(msql);
            
        }
        no = no+tSSRS.MaxRow;
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

	/**
	 * 新的打印数据方法
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", sWageNo);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//tServerPath= "E:/lisdev/ui/";
		
		String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		
		int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+10][4];
		mToExcel[1][0] = "再保月结单";
		mToExcel[2][0] = "机构："+manageCom;
		mToExcel[2][2] = "月度："+sWageNo;
		mToExcel[4][0] = "险种";
		mToExcel[4][1] = "分出保费";
		mToExcel[4][2] = "摊回分保手续费";
		mToExcel[4][3] = "摊回分保赔款";
		
		int no = 5;  //初始化打印行数

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"ZBYJ",//  再保月结单-明细
				"ZBYJZ",// 再保月结单-汇总
		};
		
		for (int i = 0; i < getTypes.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int row = 1; row <= tSSRS.MaxRow; row++){
				for(int col = 1; col <= tSSRS.MaxCol; col++){
					if(col == 2){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					if(col == 3){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					if(col == 4){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
				}
			}
		}
		no = no+tSSRS.MaxRow;
        // 往来余额 提取 从xml文件中要提取的SQL的节点 
		String[] ZBYJWLYESQLNode = new String[]{
				"ZBYJWLYE",//  往来余额
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int i=0; i<ZBYJWLYESQLNode.length; i++){
			msql  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZBYJWLYESQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			mToExcel[no][0] = "往来余额";
			mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
		}
		try{
			System.out.println("X9-再保月结tOutXmlPath:"+tOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = {PubFun.getCurrentDate()};
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(tOutXmlPath);
			System.out.println("生成X9-再保月结文件完成");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			buildError("getPrintData", e.toString());
		}
		return true;
	}

}
