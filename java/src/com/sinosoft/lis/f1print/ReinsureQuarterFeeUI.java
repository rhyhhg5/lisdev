/*
 * <p>ClassName: ReinsureQuarterTabUI </p>
 * <p>Description: ReinsureQuarterTabUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-08-22
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ReinsureQuarterFeeUI {
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();

    public static void main(String[] agrs) {
        String tStartDay = "2006-10-01";
        String tEndDay = "2006-12-31";
        String tFeeNo = "123";
        String tRecontCode = "GER20050501TDSSG001";
        String tReComName = "科隆同";
        String tReContName = "科隆再重疾溢额合同";

        VData tVData = new VData();

        tVData.addElement(tFeeNo);
        tVData.addElement(tReComName);
        tVData.addElement(tStartDay);
        tVData.addElement(tEndDay);
        tVData.addElement(tReContName);
        ReinsureQuarterFeeUI UI = new ReinsureQuarterFeeUI();
        if (!UI.submitData(tVData, "PRINT")) {
            if (UI.mErrors.needDealError()) {
                System.out.println(UI.mErrors.getFirstError());
            } else {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        } else {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();
        System.out.println("String LLCaseStatisticsBL...");
        ReinsureQuarterFeeBL tReinsureQuarterFeeBL = new ReinsureQuarterFeeBL();
        if (!tReinsureQuarterFeeBL.submitData(mInputData, mOperate)) {
            if (tReinsureQuarterFeeBL.mErrors.needDealError()) {
                mErrors.copyAllErrors(tReinsureQuarterFeeBL.mErrors);
                return false;
            } else {
                buildError("submitData", "LLComCaseUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        } else {
            mResult = tReinsureQuarterFeeBL.getResult();
            return true;
        }
    }


    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLComCaseUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
