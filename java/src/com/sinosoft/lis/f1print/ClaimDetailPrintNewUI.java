package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔赔付明细打印类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-03-03
 */
public class ClaimDetailPrintNewUI implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    public ClaimDetailPrintNewUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        ClaimDetailPrintNewBL
                tClaimDetailPrintNewBL = new ClaimDetailPrintNewBL();

        if (!tClaimDetailPrintNewBL.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tClaimDetailPrintNewBL.mErrors);
            mResult.clear();
            return false;
        }
        else
        {
            mResult = tClaimDetailPrintNewBL.getResult();
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {

        ClaimDetailPrintNewUI tClaimDetailPrintUI = new ClaimDetailPrintNewUI();

        VData tVData = new VData();

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "001";

        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo("C1100050909000004");


        tVData.add(tGlobalInput);
        tVData.add(tLOPRTManagerSchema);
        tClaimDetailPrintUI.submitData(tVData, "PRINT");
    }

    public CErrors getErrors() {
        return null;
    }

}
