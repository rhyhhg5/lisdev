package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收页面传入的数据，调用PrintPauseListBL.java生成失效清单并返回页面
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrintPauseNoticeUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    public PrintPauseNoticeUI()
    {
    }

    /**
     *
     * @param cInputData VData：包含
     1、	TransferData对象：包括查询Sql
     2、	GlobalInput对象：完整的操作员信息。
     * @param operate String：操作方式，可为空串“”
     * @return XmlExport:打印清单数据，失败为null
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate)
    {
        PrintPauseNoticeBL bl = new PrintPauseNoticeBL();

        XmlExport xml = bl.getXmlExport(cInputData, cOperate);
        if(xml == null)
        {
            mErrors = bl.mErrors;
            return null;
        }

        return xml;
    }
}
