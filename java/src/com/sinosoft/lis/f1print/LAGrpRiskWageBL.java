package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.Arith;
import java.text.DecimalFormat;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAGrpRiskWageBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();
    private String FRateFORMATMODOL = "0.00"; //浮动费率计算出来后的精确位数
    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象
    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mRiskCode = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mBranchType2 = "";
    private String mSaleChnl[] ={ "", ""};
    private String mBranchOrigin= "";
    private String mBranchPoint = "";
    private String mAgentGrade = "";
    private String mCustomerNO = "";

    private String mManageCom = "";

    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();
    private SSRS mSSRS2 = new SSRS();
    private SSRS mSSRS3 = new SSRS();
    private SSRS mSSRS4 = new SSRS();
    private SSRS mSSRS5 = new SSRS();
    private double mPrem=0.00;
    private double mStandPrem=0;
    private double mNo=0;
    private int mMin=0;
    private int mMax=0;



    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAGrpRiskWageBL() {
    }
    public static void main(String[] args)
    {
        //xjh add
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";



        LAGrpRiskWageBL tLAGrpRiskWageBL = new LAGrpRiskWageBL();
       // tLAGrpRiskWageBL.submitData(tVData, "");

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mRiskCode = (String) mTransferData.getValueByName("tRiskCode");
        this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
        this.mEndDate =  (String) mTransferData.getValueByName("tEndDate");
        this.mBranchType2 =(String) mTransferData.getValueByName("tBranchType2");
        this.mBranchOrigin= (String) mTransferData.getValueByName("tBranchOrigin");
        this.mBranchPoint = (String) mTransferData.getValueByName("tBranchPoint");
        this.mAgentGrade = (String) mTransferData.getValueByName("tAgentGrade");
        this.mCustomerNO = (String) mTransferData.getValueByName("tCustomerNO");
        this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
        if(mCustomerNO.equals("01"))
        {
            mMin=0;
            mMax=20;
        }
        else if(mCustomerNO.equals("02"))
        {
            mMin=20;
            mMax=50;
        }
        else if(mCustomerNO.equals("03"))
        {
            mMin=50;
            mMax=100;
        }
        else if(mCustomerNO.equals("04"))
        {
            mMin=100;
            mMax=300;
        }
        else if(mCustomerNO.equals("05"))
        {
            mMin=300;
            mMax=500;
        }
        else if(mCustomerNO.equals("06"))
        {
            mMin=500;
            mMax=1000;
        }
        else if(mCustomerNO.equals("07"))
        {
            mMin=1000;
            mMax=2000;
        }
        else if(mCustomerNO.equals("08"))
        {
            mMin=2000;
            mMax=999999999;
        }
        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getManageCom())
      {
         return false;
      }
      if (!getRiskCode ())
      {
         return false;
      }

      return true;
   }
   /**
    * 得到分公司
    * @return boolean
   */
   private boolean getManageCom() {

       ExeSQL tExeSQL = new ExeSQL();
       String tSql="select comcode,name  from  ldcom  where comcode  like '"+mManageCom+"%'  and sign='1' and comgrade='02' order by comcode";
       mSSRS1 = tExeSQL.execSQL(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if(mSSRS1.getMaxRow()<=0)
       {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的机构！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       return true;

   }
   /**
    * 得到险种
    * @return boolean
   */
   private boolean getRiskCode() {
       ExeSQL tExeSQL = new ExeSQL();
       String tSql="";
       if(mRiskCode.equals("99"))
       {
           //查询全部团险险种
           tSql="select riskcode,riskname  from  lmriskapp  where riskprop='G' order by riskcode ";
       }
       else
       {
           tSql = "select riskcode,riskname   from  lmriskapp  where riskprop='G' and riskcode ='" +
                   mRiskCode + "'  order by riskcode ";
       }
       mSSRS2 = tExeSQL.execSQL(tSql);

       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }

       return true;

   }
   /**
    * 得到规模保费
    * @return boolean
   */
   private boolean getPrem(String tManageCom,String tRiskCode) {
       ExeSQL tExeSQL = new ExeSQL();
       double tMoney1=0.00;
       double tMoney2=0.00;
       String tSql="";//查询契约和续期数据
       String tSql1="";//查询保全数据
       //查询全部
       if(mBranchType2.equals("99") && mBranchOrigin.equals("99") && mBranchPoint.equals("99") && mAgentGrade.equals("99") && mCustomerNO.equals("99"))
       {
           tSql =  "select value(sum(sumactupaymoney),0)  from  ljapaygrp  where managecom  like '" +
                   tManageCom + "%' and makedate<='" + mEndDate +
                   "' and makedate>= '" +
                   mStartDate + "' and riskcode='" + tRiskCode +
                   "' and (endorsementno  is null or endorsementno='')    ";
           tSql1="select sum(getmoney)  from  ljagetendorse  where managecom  like '"+
                      tManageCom+"%' and riskcode='"+
                  tRiskCode+"'  and makedate<='"+mEndDate+"' and makedate>= '"+mStartDate+"'   ";
       }
       tSql =  "select value(sum(sumactupaymoney),0)  from  ljapaygrp a where managecom  like '" +
               tManageCom + "%' and makedate<='" + mEndDate +
               "' and makedate>= '" +mStartDate + "' and riskcode='" + tRiskCode +
               "' and (endorsementno  is null or endorsementno='')   ";
       tSql1="select sum(getmoney)  from  ljagetendorse a  where managecom  like '"+
                  tManageCom+"%' and riskcode='"+
              tRiskCode+"'  and makedate<='"+mEndDate+"' and makedate>= '"+mStartDate+   "'   ";
       //------------------------------------------------------------------------
       //业务性质

       if (mBranchType2.equals("02"))
       {
           tSql+="     and agentcom is not null ";
           tSql1+="  and agentcom is not null ";
       }
       else if(mBranchType2.equals("01"))
       {
           tSql+="   and agentcom is   null ";
           tSql1+="     and agentcom is   null ";
       }
       //------------------------------------------------------------------------
       //业务来源
       if(mBranchOrigin.equals("01"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='01' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='01' ) ";
       }
       else if(mBranchOrigin.equals("02"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='02' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='02' ) ";
       }
       else if(mBranchOrigin.equals("03"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where substr(agentcode,5,6)='aaaaaa' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where substr(agentcode,5,6)='aaaaaa' ) ";
       }
       else if(mBranchOrigin.equals("04"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where agentcode = '' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where agentcode ='' ) ";
       }
       //------------------------------------------------------------------------
       //业务特点
       if(mBranchPoint.equals("01"))
       {
           tSql+="  and grpcontno in ( select grpcontno from lcgrpcont b where b.markettype<>'2' "+
                   " and b.bigprojectflag<>'1'   "+
                   " union select grpcontno from lbgrpcont b where b.markettype<>'2' "+
                  " and b.bigprojectflag<>'1') ";

          tSql1+="  and grpcontno in ( select grpcontno from lcgrpcont b where b.markettype<>'2' "+
                  " and b.bigprojectflag<>'1'   "+
                  " union select grpcontno from lbgrpcont b where b.markettype<>'2' "+
                 " and b.bigprojectflag<>'1') ";

       }
       else if(mBranchPoint.equals("02"))
       {
           tSql+=" and grpcontno in ( select grpcontno from lcgrpcont where  bigprojectflag='1' "
                   +" union select grpcontno from lbgrpcont where  bigprojectflag='1' ) ";
           tSql1+=" and grpcontno in ( select grpcontno from lcgrpcont where  bigprojectflag='1' "
                   +" union select grpcontno from lbgrpcont where  bigprojectflag='1' ) ";
       }
       else if(mBranchPoint.equals("03"))
       {
           tSql+=" and grppolno in ( select grppolno from lcgrppol where  markettype='2' "
                   +" union select grpcontno from lbgrpcont where  markettype='2' ) ";
           tSql1+=" and grppolno in ( select grppolno from lcgrppol where  markettype='2' "
                   +" union select grpcontno from lbgrpcont where  markettype='2' ) ";
       }
       //------------------------------------------------------------------------
       //销售队伍
       if(!mAgentGrade.equals("99"))
       {
           tSql+= " and agentcode in (select agentcode from latree where agentgrade='"+mBranchPoint+"' ) ";
           tSql1+=" and agentcode in (select agentcode from latree where agentgrade='"+mBranchPoint+"' ) ";
       }
       //------------------------------------------------------------------------
       //客户规模
       if(!mCustomerNO.equals("99"))
       {
           tSql+= " and (select count(*) from  lcpol where  grppolno=a.grppolno) between "+mMin+" and  "+mMax+" ";
           tSql1+= " and (select count(*) from  lcpol where  grppolno=a.grppolno) between "+mMin+" and  "+mMax+" ";
       }
       tSql+= " with ur ";
       tSql1+= "  and  feeoperationtype in ('WT','ZT','CT','XT','NI')  with ur ";
       mSSRS3 = tExeSQL.execSQL(tSql);
       System.out.println(mSSRS3.GetText(1,1));
       if(mSSRS3.getMaxRow()<=0)
       {
         tMoney1=0;
       }
       else
       {
         tMoney1=Double.parseDouble(mSSRS3.GetText(1,1))  ;
       }
       String tvalue2 = tExeSQL.getOneValue(tSql1);
       if(tvalue2==null || tvalue2.equals("") )
       {
         tMoney2=0;
       }
       else
       {
         tMoney2=Double.parseDouble(tvalue2)  ;
       }
       mPrem=tMoney2+tMoney1;
       mPrem=Arith.round(mPrem,2);
       return true;
   }
   /**
    * 得到标准保费
    * @return boolean
   */
   private boolean getStandPrem(String tManageCom,String tRiskCode) {
       ExeSQL tExeSQL = new ExeSQL();
       String tSql="select    rate,caltype    from  laratestandprem     where   managecom='"+
                   tManageCom+"'    and  branchtype = '2'   and    branchtype2='01'  and  riskcode  ='"+tRiskCode+"' with ur ";
       mSSRS4 = tExeSQL.execSQL(tSql);
       if(mSSRS4.getMaxRow()<=0)
       {
            tSql="select    rate,caltype    from  laratestandprem     where   managecom='86'    and  branchtype = '2'   and   branchtype2='01'  and  riskcode  ='"+tRiskCode+"'  with ur";
            mSSRS4 = tExeSQL.execSQL(tSql);
        }

        if(mSSRS4.getMaxRow()<=0)
        {
              mStandPrem=0;
              return true;
        }
        String tvalue1="0";
        SSRS tSSRS  = new SSRS();
        if (mSSRS4.GetText(1,2).equals("02"))
        {
        //按管理费和实收保费 计算
          if(mBranchType2.equals("99") && mBranchOrigin.equals("99") && mBranchPoint.equals("99") && mAgentGrade.equals("99") && mCustomerNO.equals("99"))
          {
              tSql =  "select value(sum(a.p7),0)  from  lacommision  where managecom  like '" +
                      tManageCom + "%' and makedate<='" + mEndDate +
                      "' and makedate>= '" +
                      mStartDate + "' and riskcode='" + tRiskCode +
                      "'      ";
          }
          //------------------------------------------------------------------------
          //业务性质
          tSql =  "select  value(sum(a.p7),0)   from  lacommision a where managecom  like '" +
                   tManageCom + "%' and tmakedate<='" + mEndDate +
                   "' and tmakedate>= '" +mStartDate + "' and riskcode='" + tRiskCode + "' ";
          if (mBranchType2.equals("02"))
          {
              tSql +=  "   and agentcom is not null ";

          }
          else if(mBranchType2.equals("01"))
          {
              tSql +=  "   and agentcom is   null ";

          }
          //------------------------------------------------------------------------
          //业务来源
          if(mBranchOrigin.equals("01"))
          {
              tSql += " and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='01' ) ";

          }
          else if(mBranchOrigin.equals("02"))
          {
              tSql += " and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='02' ) ";

          }
          else if(mBranchOrigin.equals("03"))
          {
              tSql += " and agentcode in ( select agentcode from laagent where substr(agentcode,5,6)='aaaaaa' ) ";

          }
          else if(mBranchOrigin.equals("04"))
          {
              tSql +=
                      " and agentcode in ( select agentcode from laagent where agentcode = '' ) ";

          }
          //------------------------------------------------------------------------
          //业务特点
          if(mBranchPoint.equals("01"))
          {
              tSql+="  and grpcontno in ( select grpcontno from lcgrpcont b where b.markettype<>'2' "+
                   " and b.bigprojectflag<>'1'   "+
                   " union select grpcontno from lbgrpcont b where b.markettype<>'2' "+
                  " and b.bigprojectflag<>'1') ";
          }
          else if(mBranchPoint.equals("02"))
          {
              tSql +=
                      " and grpcontno in ( select grpcontno from lcgrpcont where  bigprojectflag='1' "
                      +
                      " union select grpcontno from lbgrpcont where  bigprojectflag='1' ) ";

          }
          else if(mBranchPoint.equals("03"))
          {
              tSql +=
                      " and grppolno in ( select grppolno from lcgrppol where  markettype='2' "
                      +
                      " union select grpcontno from lbgrpcont where  markettype='2' ) ";

          }
          //------------------------------------------------------------------------
          //销售队伍
          if(!mAgentGrade.equals("99"))
          {
              tSql +=
                      " and agentcode in (select agentcode from latree where agentgrade='" +
                      mBranchPoint + "' ) ";

          }
          //------------------------------------------------------------------------
          //客户规模
          if(!mCustomerNO.equals("99"))
          {
              tSql +=
                      " and (select count(*) from  lcpol where  grppolno=a.grppolno) between " +
                      mMin + " and  " + mMax + " ";

          }
          tSql+= " with ur ";



          tvalue1=tExeSQL.getOneValue(tSql);

        }
        if (tvalue1==null || tvalue1.equals(""))
        {
            tvalue1="0";
        }
       // Double.parseDouble(mFRDecimalFormat.format(mPrem));
        tSql="select getstandprem('"+mFRDecimalFormat.format(mPrem)+"','"+tvalue1+"','"+mSSRS4.GetText(1,1)+"','"+mSSRS4.GetText(1,2)+"' ) from dual ";

        String tStandPrem=tExeSQL.getOneValue(tSql);
        if (tStandPrem==null || tStandPrem.equals("") )
        {
            mStandPrem=0;
        }
        else
        {
            mStandPrem = round(Double.parseDouble(tStandPrem),2);
        }
       return true;
   }
   /**
    * 得到客户数
    * @return boolean
   */
   private boolean getCustomerno(String tManageCom,String tRiskCode) {
       ExeSQL tExeSQL = new ExeSQL();
       String tSql= " ";
       String tSql1= " ";
       if(mBranchType2.equals("99") && mBranchOrigin.equals("99") && mBranchPoint.equals("99") && mAgentGrade.equals("99") && mCustomerNO.equals("99"))
       {
           tSql =  "select count(distinct appntno)   from  ljapaygrp  where managecom  like '" +
                   tManageCom + "%' and makedate<='" + mEndDate +
                   "' and makedate>= '" +
                   mStartDate + "' and riskcode='" + tRiskCode +
                   "' and (endorsementno  is null or endorsementno='')    ";
           tSql1="select count(distinct appntno)   from  ljagetendorse  where managecom  like '"+
                      tManageCom+"%' and riskcode='"+
                  tRiskCode+"'  and makedate<='"+mEndDate+"' and makedate>= '"+mStartDate+
                  "'  and FeeOperationType = 'WT' and FeeFinaType = 'TF' ";
       }
       tSql =  "select count(distinct appntno)  from  ljapaygrp a where managecom  like '" +
               tManageCom + "%' and makedate<='" + mEndDate +
               "' and makedate>= '" +mStartDate + "' and riskcode='" + tRiskCode +
               "' and (endorsementno  is null or endorsementno='')   ";
       tSql1="select count(distinct appntno) from  ljagetendorse a  where managecom  like '"+
                  tManageCom+"%' and riskcode='"+
              tRiskCode+"'  and makedate<='"+mEndDate+"' and makedate>= '"+mStartDate+
                  "'   and FeeOperationType = 'WT' and FeeFinaType = 'TF' ";
       //------------------------------------------------------------------------
       //业务性质
       if (mBranchType2.equals("02"))
       {
           tSql +=  "  and agentcom is not null ";
           tSql1+="    and agentcom is not null  ";
       }
       else if(mBranchType2.equals("01"))
       {
           tSql +=  "  and agentcom is   null ";
           tSql1+="    and agentcom is   null  ";
       }
       //------------------------------------------------------------------------
       //业务来源
       if(mBranchOrigin.equals("01"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='01' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='01' ) ";
       }
       else if(mBranchOrigin.equals("02"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='02' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where branchtype='2' and branchtype2='02' ) ";
       }
       else if(mBranchOrigin.equals("03"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where substr(agentcode,5,6)='aaaaaa' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where substr(agentcode,5,6)='aaaaaa' ) ";
       }
       else if(mBranchOrigin.equals("04"))
       {
           tSql+=" and agentcode in ( select agentcode from laagent where agentcode = '' ) ";
           tSql1+=" and agentcode in ( select agentcode from laagent where agentcode ='' ) ";
       }
       //------------------------------------------------------------------------
       //业务特点
       if(mBranchPoint.equals("01"))
       {
           tSql+=" and grpcontno in ( select grpcontno from lcgrpcont b where b.markettype<>'2' "+
                   " and b.bigprojectflag<>'1'   "+
                   " union select grpcontno from lbgrpcont b where b.markettype<>'2' "+
                  " and b.bigprojectflag<>'1') ";
          tSql1+="  and grpcontno in ( select grpcontno from lcgrpcont b where b.markettype<>'2' "+
                  " and b.bigprojectflag<>'1'   "+
                  " union select grpcontno from lbgrpcont b where b.markettype<>'2' "+
                 " and b.bigprojectflag<>'1') ";

       }
       else if(mBranchPoint.equals("02"))
       {
           tSql+=" and grpcontno in ( select grpcontno from lcgrpcont where  bigprojectflag='1' "
                   +" union select grpcontno from lbgrpcont where  bigprojectflag='1' ) ";
           tSql1+=" and grpcontno in ( select grpcontno from lcgrpcont where  bigprojectflag='1' "
                   +" union select grpcontno from lbgrpcont where  bigprojectflag='1' ) ";
       }
       else if(mBranchPoint.equals("03"))
       {
           tSql+=" and grppolno in ( select grppolno from lcgrppol where  markettype='2' "
                   +" union select grpcontno from lbgrpcont where  markettype='2' ) ";
           tSql1+=" and grppolno in ( select grppolno from lcgrppol where  markettype='2' "
                   +" union select grpcontno from lbgrpcont where  markettype='2' ) ";
       }
       //------------------------------------------------------------------------
       //销售队伍
       if(!mAgentGrade.equals("99"))
       {
           tSql+= " and agentcode in (select agentcode from latree where agentgrade='"+mBranchPoint+"' ) ";
           tSql1+=" and agentcode in (select agentcode from latree where agentgrade='"+mBranchPoint+"' ) ";
       }
       //------------------------------------------------------------------------
       //客户规模
       if(!mCustomerNO.equals("99"))
       {
           tSql+= " and (select count(*) from  lcpol where  grppolno=a.grppolno) between "+mMin+" and  "+mMax+" ";
           tSql1+= " and (select count(*) from  lcpol where  grppolno=a.grppolno) between "+mMin+" and  "+mMax+" ";
       }
       tSql+= " with ur ";
       tSql1+= " with ur ";


       String tNo = tExeSQL.getOneValue(tSql);
       String tMaxNo= tExeSQL.getOneValue(tSql1);
       if(tNo==null || tNo.equals("") )
       {
           tNo="0";
       }
       if(tMaxNo==null || tMaxNo.equals("") )
       {
           tMaxNo="0";
       }
       int t1=Integer.parseInt(tNo);
       int t2=Integer.parseInt(tMaxNo);

       mNo= t1-t2;
       return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LAGrpRiskWage.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {" ", " ", " ", " ", " " ,"11"};

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0)
        {
            double dFirst = 0;
            double dTwo = 0;
            double dThree = 0;
            double dFour = 0;
            if(mManageCom.equals("86"))
            {
                if (mSSRS2.getMaxRow() <= 0) {
                    CError tCError = new CError();
                    tCError.moduleName = "CreateXml";
                    tCError.functionName = "creatFile";
                    tCError.errorMessage = "没有符合条件的险种！";
                    this.mErrors.addOneError(tCError);
                    return false;
                }
                for (int j = 1; j <= mSSRS2.getMaxRow(); j++) {
                    String Info[] = new String[6];
                    Info[0] = "总公司";
                    Info[1] = mSSRS2.GetText(j, 1);

                    Info[2] = mSSRS2.GetText(j, 2);

                    if (!getPrem("86", mSSRS2.GetText(j, 1))) {
                        return false;
                    }

                    if (!getStandPrem("86",
                                      mSSRS2.GetText(j, 1))) {
                        return false;
                    }

                    if (!getCustomerno("86",
                                       mSSRS2.GetText(j, 1))) {
                        return false;
                    }
                    Info[3] = String.valueOf(Arith.round(
                            mPrem, 2));
                    Info[4] = String.valueOf(mStandPrem);
                    Info[5] = String.valueOf(mNo);
                    mListTable.add(Info);
                }
            }


            for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {

                System.out.print("111"+mSSRS2.getMaxRow());
                if(mSSRS2.getMaxRow()<=0)
                {
                    CError tCError = new CError();
                    tCError.moduleName = "CreateXml";
                    tCError.functionName = "creatFile";
                    tCError.errorMessage = "没有符合条件的险种！";
                    this.mErrors.addOneError(tCError);
                      return false;
                }
                for (int j = 1; j <= mSSRS2.getMaxRow(); j++) {
                    String Info[] = new String[6];
                    Info[0] = mSSRS1.GetText(i,2);
                    Info[1] = mSSRS2.GetText(j,1);

                    Info[2] = mSSRS2.GetText(j,2);

                    if (!getPrem(mSSRS1.GetText(i,1),mSSRS2.GetText(j,1)))
                    {
                       return false;
                    }

                    if (!getStandPrem(mSSRS1.GetText(i,1),mSSRS2.GetText(j,1)))
                    {
                       return false;
                    }

                    if (!getCustomerno(mSSRS1.GetText(i,1),mSSRS2.GetText(j,1)))
                    {
                       return false;
                    }
                    Info[3] = String.valueOf(Arith.round(
                                             mPrem,2));
                    Info[4] = String.valueOf(mStandPrem);
                    Info[5] = String.valueOf(mNo);
                    mListTable.add(Info);


                }
            }


            mListTable.setName("ZT");

        } else {

            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的机构！";

            this.mErrors.addOneError(tCError);

            return false;

        }

        return true;

    }

    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LAGrpRiskWageBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }
    public static double round(double v,int scale)
    {
      if(scale<0)
      {
          throw new IllegalArgumentException("The scale must be a positive integer or zero");
      }
      BigDecimal b = new BigDecimal(Double.toString(v));
      BigDecimal one = new BigDecimal("1");
      return b.divide(one,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
    }


    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
