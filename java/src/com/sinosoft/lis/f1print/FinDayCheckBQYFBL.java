package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 */

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinDayCheckBQYFBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //业务处理相关变量
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private LMRiskAppSet mLMRiskAppSet;
    private String mRiskName = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public FinDayCheckBQYFBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        /* 准备所有要打印的数据
             if (cOperate.equals("PRINTGET")) //打印付费
             {
          if( !getPrintDataGet() )
          {
            return false;
          }
             }
         */
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        this.mLMRiskAppSet = tLMRiskAppDB.query();
        if (cOperate.equals("PRINTGET")) //打印保全应付
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86110000";
        VData vData = new VData();
        String[] tDay = new String[2];
        tDay[0] = "2004-1-13";
        tDay[1] = "2004-1-13";
        vData.addElement(tDay);
        vData.addElement(tG);

        FinDayCheckBQYFBL tF = new FinDayCheckBQYFBL();
        tF.submitData(vData, "PRINTGET");

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        //11-26
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBQYFBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
       private boolean getPrintDataPay()
                {
        SSRS tSSRS = new SSRS();
        double SumMoney = 0;
//        String msql = "select endorsementno,sum(b) from ("+
//                      "select a.endorsementno,abs(sum(a.getmoney)) b from LJAGetEndorse a,ljaget b where a.MakeDate>='"+mDay[0]+"'"+
//                      "and a.MakeDate<='"+mDay[1]+"' and a.ManageCom like '"+ mGlobalInput.ManageCom+"%'  and FeeFinaType!='GB' "+
//                      "and FeeFinaType!='HK' and FeeFinaType!='HD'and FeeFinaType != 'TJ' and FeeFinaType!='YHSL'and  FeeFinaType != 'BF' and FeeFinaType!='YEI' "+
//                      "and a.actugetno = b.actugetno and b.sumgetmoney <> 0 and b.paymode not in ('B','J','13') and b.othernotype <> '13' and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) group by a.endorsementno "+
//                      "union all select a.endorsementno,abs(sum(a.getmoney)) b from LJAGetEndorse a,ljapay b where a.MakeDate >='"+mDay[0]+"' "+
//                      "and a.MakeDate<='"+mDay[1]+"' and a.ManageCom like '"+mGlobalInput.ManageCom+"%' and FeeFinaType!='LX' and FeeFinaType!='GB' and FeeFinaType!='HK' "+
//                      "and FeeFinaType!='HD' and FeeFinaType != 'TJ' and  FeeFinaType != 'BF' and FeeFinaType!='YHSL' and FeeFinaType!='YEI' and a.actugetno = b.payno "+
//                      "and b.sumactupaymoney <> 0 and b.incometype not in('B','J','13') and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) group by a.endorsementno) c "+
//                      "group by endorsementno";
        GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "BQYF");
        
        System.out.println(msql);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("RISK");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[2];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    System.out.println(strArr[j - 1]);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    String strSum = new DecimalFormat("0.00").format(Double.
                         valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    System.out.println(strArr[j - 1]);
                    SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);
                }
            }
            tlistTable.add(strArr);
        }

        strArr = new String[2];
        strArr[0] = "RISK";
        strArr[1] = "Money";

        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FinDayCheckBQYF.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", SumMoney);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
                }

}
