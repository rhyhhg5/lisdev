package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 */
public class QueryDrawNoticeBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;
    String strStartDate = "";
    String strEndDate = "";
    private GlobalInput mG = new GlobalInput();
    public QueryDrawNoticeBL()
    {}


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        QueryDrawNoticeBL tQueryDrawNoticeBL = new QueryDrawNoticeBL();
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (cOperate.equals("QUERY"))
        {
            if (!queryData())
            {
                return false;
            }
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("在QueryDrawNoticeBL.java中获得的数据是");
        strStartDate = (String) cInputData.get(0);
        strEndDate = (String) cInputData.get(1);
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println("开始日期是" + strStartDate);
        System.out.println("结束日期是" + strEndDate);
        return true;
    }

    private boolean queryData()
    {
        LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
        String t_sql = "Select * From LJSGetDraw Where GetDate >= '"
                       + strStartDate + "' And GetDate <= '"
                       + strEndDate + "' And ManageCom like '"
                       + mG.ManageCom.trim() + "%' ";
        System.out.println("所执行的sql语句是" + t_sql);
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawSet.set(tLJSGetDrawDB.executeQuery(t_sql));
        if (tLJSGetDrawSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSGetDrawSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "在该时间段内没有生存领取的数据信息！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetDrawSet aLJSGetDrawSet = new LJSGetDrawSet(); //用来存放准备的数据
        for (int count = 1; count <= tLJSGetDrawSet.size(); count++)
        {
            LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
            tLJSGetDrawSchema.setSchema(tLJSGetDrawSet.get(count));
            if (!(tLJSGetDrawSchema.getInsuredNo() == null ||
                  tLJSGetDrawSchema.getInsuredNo().equals("")))
            {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(tLJSGetDrawSchema.getInsuredNo());
                LDPersonSet tLDPersonSet = new LDPersonSet();
                tLDPersonSet.set(tLDPersonDB.query());
                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                if (tLDPersonSet.size() == 0)
                {
                    tLJSGetDrawSchema.setContNo("无"); //用ContNo记录被保险人的名称
                }
                else
                {
                    tLDPersonSchema.setSchema(tLDPersonSet.get(1));
                    tLJSGetDrawSchema.setContNo(tLDPersonSchema.getName());
                }
            }
            if (!(tLJSGetDrawSchema.getAppntNo() == null ||
                  tLJSGetDrawSchema.getAppntNo().equals("")))
            {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(tLJSGetDrawSchema.getAppntNo());
                LDPersonSet tLDPersonSet = new LDPersonSet();
                tLDPersonSet.set(tLDPersonDB.query());
                if (tLDPersonSet.size() == 0)
                {
                    tLJSGetDrawSchema.setGrpName("无");
                }
                else
                {
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tLDPersonSchema.setSchema(tLDPersonSet.get(1));
                    tLJSGetDrawSchema.setGrpName(tLDPersonSchema.getName());
                }
            }
            System.out.println("给付通知书号码是" + tLJSGetDrawSchema.getGetNoticeNo());
            System.out.println("被保险人是" + tLJSGetDrawSchema.getContNo());
            System.out.println("投保人是" + tLJSGetDrawSchema.getGrpName());
            aLJSGetDrawSet.add(tLJSGetDrawSchema);
        }
        mResult.clear();
        mResult.add(aLJSGetDrawSet);
        return true;
    }

    public static void main(String[] args)
    {
        QueryDrawNoticeBL aQueryDrawNoticeBL = new QueryDrawNoticeBL();
        String StartDate = "2003-01-01";
        String EndDate = "2005-01-01";
        VData tVData = new VData();
        tVData.addElement(StartDate);
        tVData.addElement(EndDate);
        aQueryDrawNoticeBL.submitData(tVData, "QUERY");
    }
}