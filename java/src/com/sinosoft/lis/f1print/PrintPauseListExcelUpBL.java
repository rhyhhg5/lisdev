package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PrintPauseListExcelUpBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  private String mManageCom=""; 
  
  private String mStartDate=""; 
  
  private String mEndDate=""; 
  
  private String mLoadFlag=""; 
  
  private String mSql=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public PrintPauseListExcelUpBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData)
    {
      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

      // 准备所有要打印的数据
      if( !getPrintData() ) {
    	  buildError("getPrintData", "下载失败");
    	  return null;
      }  

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
    public static void main(String[] args)
    {
        
    	PrintPauseListEXCELBL tbl =new PrintPauseListEXCELBL();
        GlobalInput tGlobalInput = new GlobalInput();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", "2012-5-08");
        tTransferData.setNameAndValue("EndDate", "2012-6-08");
        tTransferData.setNameAndValue("ManageCom", "8644");
        tGlobalInput.ManageCom="8644";
//        tGlobalInput.Operator="xp";
        VData tData = new VData();
        tData.add(tGlobalInput);
        tData.add(tTransferData);

        CreateExcelList tCreateExcelList=new CreateExcelList();
        tCreateExcelList=tbl.getsubmitData(tData);
        if(tCreateExcelList==null){
      	  System.out.println("112321231");
        }
        else{
        try{
      	  tCreateExcelList.write("c:\\cytzpare.xls");
        }catch(Exception e)
        {
      	  System.out.println("EXCEL生成失败！");
        }}
    }
   
 
  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
 
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mManageCom.equals("")||mManageCom==null) {
        buildError("getInputData", "没有得到足够的信息:管理机构不能为空");
        return false;
      }
    
    mStartDate = (String) mTransferData.getValueByName("StartDate");
    if( mStartDate.equals("")||mStartDate==null) {
        buildError("getInputData", "没有得到足够的信息:开始日期不能为空");
        return false;
      }
    
    mEndDate = (String) mTransferData.getValueByName("EndDate");
    if( mEndDate.equals("")||mEndDate==null) {
        buildError("getInputData", "没有得到足够的信息:结束日期不能为空");
        return false;
      }
    
    mLoadFlag = (String) mTransferData.getValueByName("LoadFlag");
    
    mSql = (String) mTransferData.getValueByName("SQL");
    
    if( mLoadFlag.equals("")||mLoadFlag==null) {
    	mLoadFlag="H";
      }
    
    if( mSql.equals("")||mSql==null) {
    	buildError("getInputData", "获取清单查询语句失败");
        return false;
      }
    
    
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      //管理机构、保单号、投保人姓名、投保人联系电话、投保人联系地址、贷款金额、贷款起息日期、应还款日期。
      String[][] tTitle = {{"抽档日期 ","管理机构","营销部门","保单号","投保人","投保人电话","投保人联系地址",
    	  					"应收记录号","待收保费","待收时间 ","交费截止日期 ","交费方式 ","失效/暂停通知书号",
    	  					"保单失效/暂停日","类型 ","代理人编码 ","代理人电话 ","代理人姓名 ","失效日期 ","保单归属状态"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      
//      String tSQL="select " +
//      		"b.agentCode, " + // 1.代理人编码
//      		"(select mobile from LAAGent where agentCode = b.agentCode), " + //2.代理人电话
//      		"(select name from LAAGent where agentCode = b.agentCode), " + //3.代理人姓名
//      		"a.makedate," + //4.失效日期
//      		" (case   when " +
//      		"(select agentstate from laagent where agentcode = b.agentcode) >= '06' " +
//      		"then    '孤儿单'   else    '业务员在职' end) " + //5.保单归属状态
//      		"  from LOPRTManager a, LCCont b, LJSPayB c, LCAppnt d  " +
//      		"where a.otherNo = b.ContNo    " +
//      		"and b.contNo = c.otherNo   " +
//      		" and b.contNo = d.contNo   " +
//      		" and a.standbyFlag3 = c.getNoticeNo  " +
//      		"  and a.ManageCom like '"+mManageCom+"%'   " +
//      		" and a.MakeDate >= '"+mStartDate+"' " +
//      		"   and a.MakeDate <= '"+mEndDate+"'   " +
//      		"  AND (a.Code = '42' OR a.Code = '21') " +
//      		"union " +
//      		"select" +
//      		" b.agentCode," + // 1.代理人编码
//      		" (select mobile from LAAGent where agentCode = b.agentCode), " + //2.代理人电话
//      		"(select name from LAAGent where agentCode = b.agentCode), " + //3.代理人姓名
//      		"a.makedate, " + //4.失效日期
//      		"(case   when " +
//      		"(select agentstate from laagent where agentcode = b.agentcode) >= '06'" +
//      		" then    '孤儿单'   else    '业务员在职' end)  " + //5.保单归属状态
//      		" from LOPRTManager a, LCCont b, LCAppnt d  " +
//      		"where a.otherNo = b.ContNo  " +
//      		"  and b.contNo = d.contNo  " +
//      		"  and a.code = '21'    " +
//      		"  and a.ManageCom like '"+mManageCom+"%'   " +
//      		" and a.MakeDate >= '"+mStartDate+"' " +
//      		"   and a.MakeDate <= '"+mEndDate+"'   " +
//      		" AND (a.Code = '42' OR a.Code = '21')";
//      
//      
//      
//      if(mLoadFlag.equals("G"))
//      {
//    	  
//    	  tSQL=" select " +
//    	  		" b.agentCode, " +
//    	  		" (select mobile from LAAGent where agentCode = b.agentCode), " +
//    	  		" (select name from LAAGent where agentCode = b.agentCode)," +
//    	  		" a.makedate, " +
//    	  		" (case   when " +
//    	  		" (select agentstate from laagent where agentcode = b.agentcode) >= '06' " +
//    	  		" then    '孤儿单'   else    '业务员在职' end) " +
//    	  		"  from LOPRTManager a, LCGrpCont b, LJSPayB c, LCGrpAppnt d  " +
//    	  		" where a.otherNo = b.GrpContNo   " +
//    	  		"  and b.GrpcontNo = d.GrpcontNo   " +
//    	  		"  and a.standbyFlag3 = c.getNoticeNo   " +
//          		"  and a.ManageCom like '"+mManageCom+"%'   " +
//          		" and a.MakeDate >= '"+mStartDate+"' " +
//          		"   and a.MakeDate <= '"+mEndDate+"'   " +
//    	  		"   AND (a.Code = '42' OR a.Code = '21' OR a.Code = '58')" +
//    	  		" union " +
//    	  		" select " +
//    	  		" b.agentCode, " +
//    	  		" (select mobile from LAAGent where agentCode = b.agentCode)," +
//    	  		" (select name from LAAGent where agentCode = b.agentCode), " +
//    	  		" a.makedate, " +
//    	  		"( case   when " +
//    	  		" (select agentstate from laagent where agentcode = b.agentcode) >= '06' " +
//    	  		" then    '孤儿单'   else    '业务员在职' end) " +
//    	  		"  from LOPRTManager a, LCGrpCont b, LCGrpAppnt d  " +
//    	  		" where a.otherNo = b.GrpContNo  " +
//    	  		"  and b.GrpcontNo = d.GrpcontNo " +
//    	  		"   and a.code = '21'  " +
//          		"  and a.ManageCom like '"+mManageCom+"%'   " +
//          		" and a.MakeDate >= '"+mStartDate+"' " +
//          		"   and a.MakeDate <= '"+mEndDate+"'   " +
//    	  		"   AND (a.Code = '42' OR a.Code = '21' OR a.Code = '58') " ;
//    	  
//      }
      
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(mSql);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  } 
}

