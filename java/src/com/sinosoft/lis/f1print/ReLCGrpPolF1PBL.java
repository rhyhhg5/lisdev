/*
 * <p>ClassName: LCGrpPolF1PBL </p>
 * <p>Description: LCGrpPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.f1print;

import java.util.Vector;

import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ReLCGrpPolF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mOperate = "";
    private String mPrtNo = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vGrpPolNo = new Vector();

    public ReLCGrpPolF1PBL()
    {
    }

    /**
                  传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 准备所有要打印的数据
            if (cOperate.equals("PRINT"))
            {
                getPrintData();
            }
            else if (cOperate.equals("CONFIRM"))
            {
                if (!getConfirm())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                "LCGrpPolSet", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void getPrintData() throws Exception
    {
        String strSql = "";
        LCGrpPolSet tLCGrpPolSet = null;
        LCGrpPolSchema tLCGrpPolSchema = null;
        for (int nIndex = 0; nIndex < mLCGrpPolSet.size(); nIndex++)
        {
            tLCGrpPolSchema = mLCGrpPolSet.get(nIndex + 1);
            // 记录下已经打印过的集体保单号
            if (m_vGrpPolNo.contains(tLCGrpPolSchema.getGrpPolNo()))
            {
                continue;
            }
            strSql = "SELECT * FROM LCGrpPol WHERE GrpPolNo = '" +
                     tLCGrpPolSchema.getGrpPolNo() + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCGrpPolSet = new LCGrpPolDB().executeQuery(strSql);

            if (tLCGrpPolSet.size() == 0)
            {
                buildError("getPrintData", "所指定的保单不符合重打的条件");

            }

            tLCGrpPolSchema = tLCGrpPolSet.get(1);

            // 查询保单信息的时候需要加上印刷号的信息
            mPrtNo = tLCGrpPolSchema.getPrtNo();

            // 如果这个保单不是主险保单
            // 如果是主险保单，该保单的主险保单号等于自身的保单号
            //if( !tLCGrpPolSchema.getMainPolNo().equals(tLCGrpPolSchema.getPolNo()) ) {
            //查出同一印刷号的所有保单
            strSql = "SELECT * FROM LCGrpPol WHERE PrtNo = '" + mPrtNo + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCGrpPolSet = new LCGrpPolDB().executeQuery(strSql);
            if (tLCGrpPolSet.size() == 0)
            { // 没有查询到同一印刷号的符合打印条件的主险保单
                buildError("getPrintData", "找不到同一印刷号的符合重打条件的主险保单");
            }
            else
            { // 将查询到的主险保单放在mLCPolSchema中
                tLCGrpPolSchema = tLCGrpPolSet.get(1);
            }

            /**
             * 校验所有的附加险保单是否符合打印的条件
             * 记录下本次打印过的所有保单号
             */
            m_vGrpPolNo.add(tLCGrpPolSchema.getGrpPolNo());

            for (int n = 0; n < tLCGrpPolSet.size(); n++)
            {
                LCGrpPolSchema tempLCGrpPolSchema = tLCGrpPolSet.get(n + 1);

                m_vGrpPolNo.add(tempLCGrpPolSchema.getGrpPolNo());

                /*Lis5.3 upgrade get
                         if( !tempLCGrpPolSchema.getAppFlag().equals("1")
                    || tempLCGrpPolSchema.getPrintCount() == 0 ) {
                  buildError("getPrintData", "附加险保单不符合重打条件");

                  //return false;
                         }
                 */
            }
        } // end of "for(int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)"
        LCGrpPolSet tempLCGrpPolSet = new LCGrpPolSet();
        for (int nIndex = 0; nIndex < m_vGrpPolNo.size(); nIndex++)
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo((String) m_vGrpPolNo.get(nIndex));
            if (!tLCGrpPolDB.getInfo())
            {
                throw new Exception("找不到要更新数据的团单数据");
            }
            /*Lis5.3 upgrade set
                   tLCGrpPolDB.setPrintCount(-1);
             */
            tempLCGrpPolSet.add(tLCGrpPolDB);
        }
        mResult.add(tempLCGrpPolSet);
        ReLCGrpPolF1PBLS tReLCGrpPolF1PBLS = new ReLCGrpPolF1PBLS();
        tReLCGrpPolF1PBLS.submitData(mResult, mOperate);
        if (tReLCGrpPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCGrpPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
        }
    }

    private boolean getConfirm() throws Exception
    {
        String strSql = "";
        LCGrpPolSet tLCGrpPolSet = null;
        LCGrpPolSchema tLCGrpPolSchema = null;
        for (int nIndex = 0; nIndex < mLCGrpPolSet.size(); nIndex++)
        {
            tLCGrpPolSchema = mLCGrpPolSet.get(nIndex + 1);
            // 记录下已经打印过的集体保单号
            if (m_vGrpPolNo.contains(tLCGrpPolSchema.getGrpPolNo()))
            {
                continue;
            }
            strSql = "SELECT * FROM LCGrpPol WHERE GrpPolNo = '" +
                     tLCGrpPolSchema.getGrpPolNo() + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCGrpPolSet = new LCGrpPolDB().executeQuery(strSql);

            if (tLCGrpPolSet.size() == 0)
            {
                buildError("getPrintData", "所指定的保单不符合重打的条件");
            }

            tLCGrpPolSchema = tLCGrpPolSet.get(1);

            // 查询保单信息的时候需要加上印刷号的信息
            mPrtNo = tLCGrpPolSchema.getPrtNo();
            //查出同一印刷号的所有保单
            strSql = "SELECT * FROM LCGrpPol WHERE PrtNo = '" + mPrtNo + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCGrpPolSet = new LCGrpPolDB().executeQuery(strSql);
            if (tLCGrpPolSet.size() == 0)
            { // 没有查询到同一印刷号的符合打印条件的主险保单
                buildError("getPrintData", "找不到同一印刷号的符合重打条件的主险保单");
                return false;
            }
            else
            { // 将查询到的主险保单放在mLCPolSchema中
                tLCGrpPolSchema = tLCGrpPolSet.get(1);
            }

            /**
             * 校验所有的附加险保单是否符合打印的条件
             * 记录下本次打印过的所有保单号
             */
            m_vGrpPolNo.add(tLCGrpPolSchema.getGrpPolNo());

            for (int n = 0; n < tLCGrpPolSet.size(); n++)
            {
                LCGrpPolSchema tempLCGrpPolSchema = tLCGrpPolSet.get(n + 1);

                m_vGrpPolNo.add(tempLCGrpPolSchema.getGrpPolNo());

                if (!tempLCGrpPolSchema.getAppFlag().equals("1"))
                {
                    /*Lis5.3 upgrade get
                             if( !tempLCGrpPolSchema.getAppFlag().equals("1")
                        || tempLCGrpPolSchema.getPrintCount() == 0 ) {
                     */
                    buildError("getPrintData", "附加险保单不符合重打条件");
                    return false;
                }
            }
        } // end of "for(int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)"
        LCGrpPolSet tempLCGrpPolSet = new LCGrpPolSet();
        for (int nIndex = 0; nIndex < m_vGrpPolNo.size(); nIndex++)
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo((String) m_vGrpPolNo.get(nIndex));
            if (!tLCGrpPolDB.getInfo())
            {
                throw new Exception("找不到要更新数据的团单数据");
            }
            /*Lis5.3 upgrade set
                   tLCGrpPolDB.setPrintCount(0);
             */
            tempLCGrpPolSet.add(tLCGrpPolDB);
        }
        mResult.add(tempLCGrpPolSet);
        ReLCGrpPolF1PBLS tReLCGrpPolF1PBLS = new ReLCGrpPolF1PBLS();
        tReLCGrpPolF1PBLS.submitData(mResult, mOperate);
        if (tReLCGrpPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCGrpPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }
}
