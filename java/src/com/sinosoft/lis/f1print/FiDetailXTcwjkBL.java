package com.sinosoft.lis.f1print;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYPremSeparateDetailSchema;
import com.sinosoft.lis.tb.GrpPayPlanDetailBL;
import com.sinosoft.lis.vschema.LCGrpPayPlanDetailSet;
import com.sinosoft.lis.vschema.LYPremSeparateDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FiDetailXTcwjkBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 开始日期*/
	private String mStartDate;
	/** 结束日期*/
	private String mEndDate;
	/** 印刷号*/
	private String mPrtNo;
	//业务处理相关变量
	private MMap mMap = new MMap();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(mCurrentDate+":开始提取协议退保价税分离数据。");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tFiDetailXTcwjkBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败tFiDetailXTcwjkBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// 保存数据
		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetTempFeeSeparateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		System.out.println(mCurrentDate+":提取协议退保管理费价税分离数据结束。");
		return true;
	}

	// 获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) tTransferData.getValueByName("StartDate");
		mEndDate = (String) tTransferData.getValueByName("EndDate");
		return true;
	}
	
//  业务处理
	private boolean dealData() {
		LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
		ExeSQL tExeSQL = new ExeSQL();
		String tWhereSQL = "";
		tWhereSQL ="select ddjs.code1,ddjs.code2,ddjs.code3,ddjs.code4,ddjs.code5,ddjs.code6,ddjs.code7,ddjs.code8,ddjs.code9,ddjs.code10,ddjs.code11,ddjs.code12 from "+
			" (select a.endorsementno as code1,a.actugetno as code2,a.OtherNoType as code3,(case when a.GrpContNo = '00000000000000000000' then a.contno else a.grpcontno end ) as code4,a.GrpPolNo as code5,a.PolNo as code6,a.RiskCode as code7,a.ManageCom as code8, " +
				" NVL(((case when LF_risktype (a.riskcode) = '1' then (select b.lastaccbala from lbinsureacc b where b.contno = a.contno and b.polno = a.polno union all select b.lastaccbala from lcinsureacc b"+
				" where b.contno = a.contno and b.polno = a.polno fetch first 1 rows only) else ((select sum(b.fee) from LBInsureAccFeeTrace b where b.GrpContNo = a.GrpContNo and b.polno = a.polno) +"+
				"(select sum(b.Money) from LBInsureAccTrace b where b.GrpContNo = a.GrpContNo and b.polno = a.polno and b.moneytype in ('BF', 'ZF'))) end)+(select sum(f.getmoney) from ljagetendorse f where f.actugetno = a.actugetno and f.feeoperationtype = 'XT'"+
				" and f.FeeFinaType in ('TB', 'TF') and f.makedate = a.makedate)),0) as code9 ,a.otherno as code10,a.feeoperationtype as code11,a.feefinatype as code12 " +
				" from ljagetendorse a where a.feeoperationtype = 'XT' and LF_risktype (a.riskcode) in ('1', '2', '3') and makedate between '"+mStartDate+"' and '"+mEndDate+"'  and a.FeeFinaType in ('TB', 'TF') and a.getmoney <> 0 " +
				" and not exists (select 1 from LYPremSeparateDetail lyp where lyp.BusiNo = 'CWXT'|| trim(a.endorsementno) || ',' || trim(a.actugetno) || ',' || trim(a.riskcode) and lyp.feeoperationtype = 'XT')) as ddjs where ddjs.code9 <> 0" ;
//		a.dutycode,a.PAYPLANCODE,
		System.out.println(tWhereSQL);
		SSRS tWhereSSRS = tExeSQL.execSQL(tWhereSQL);
		if(tWhereSSRS != null && tWhereSSRS.getMaxRow()>0){
			System.out.println("====  协议退保数据价税分离： "+tWhereSSRS.getMaxRow());			
			for (int j = 1; j <= tWhereSSRS.getMaxRow(); j++) {
				
			            LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();
						tLYPremSeparateDetailSchema.setBusiNo("CWXT"+tWhereSSRS.GetText(j, 1)+"," + tWhereSSRS.GetText(j, 2)+","+tWhereSSRS.GetText(j, 7));
						tLYPremSeparateDetailSchema.setTempFeeNo(tWhereSSRS.GetText(j, 1));
						tLYPremSeparateDetailSchema.setTempFeeType("CW");
						tLYPremSeparateDetailSchema.setOtherNo(tWhereSSRS.GetText(j, 5));
						tLYPremSeparateDetailSchema.setOtherNoType(tWhereSSRS.GetText(j, 3));
						tLYPremSeparateDetailSchema.setPrtNo(tWhereSSRS.GetText(j, 6));
						tLYPremSeparateDetailSchema.setPolicyNo(tWhereSSRS.GetText(j, 4));
						tLYPremSeparateDetailSchema.setRiskCode(tWhereSSRS.GetText(j, 7));
						tLYPremSeparateDetailSchema.setBusiType("05");
						tLYPremSeparateDetailSchema.setManageCom(tWhereSSRS.GetText(j, 8));
						tLYPremSeparateDetailSchema.setPayMoney(tWhereSSRS.GetText(j, 9));
						double a = Double.parseDouble(tWhereSSRS.GetText(j, 9));
						if ( a < 0 ){
							tLYPremSeparateDetailSchema.setState("02");
							tLYPremSeparateDetailSchema.setMoneyNoTax(tWhereSSRS.GetText(j, 9));
							tLYPremSeparateDetailSchema.setMoneyTax("0.00");
							tLYPremSeparateDetailSchema.setTaxRate("0.00");
						}else{
							tLYPremSeparateDetailSchema.setState("01");	
						}
						
						tLYPremSeparateDetailSchema.setOtherState("13");
						tLYPremSeparateDetailSchema.setMoneyNo(tWhereSSRS.GetText(j, 2));
						tLYPremSeparateDetailSchema.setMoneyType("02");	
						tLYPremSeparateDetailSchema.setFeeOperationType(tWhereSSRS.GetText(j, 11));
						tLYPremSeparateDetailSchema.setFeeFinaType(tWhereSSRS.GetText(j, 12));
						tLYPremSeparateDetailSchema.setMakeDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setMakeTime(mCurrentTime);
						tLYPremSeparateDetailSchema.setModifyDate(mCurrentDate);
						tLYPremSeparateDetailSchema.setModifyTime(mCurrentTime);
						tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
			}
			}
						
						else{
			System.out.println("===没有需要价税分离的协议退保管理费数据！===");
		}
		mMap.put(tLYPremSeparateDetailSet, SysConst.INSERT);
		
		return true;
	}
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FiDetailXTcwjkBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	
		public static String convertLongToString(long value){
	        String msg="";  
	        Date date = new Date(value);  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  
	        msg+=sdf.format(date);  
	        return msg;
		}
	public static void main(String[] args) {
		long startTime1 = System.currentTimeMillis();
		String s = "";
		String [] arr = null;
		arr = s.split(",");
		System.out.println("--------------------   "+arr.length);
		for(int i = 0; i < arr.length;i++){
			FiDetailXTcwjkBL tFiDetailXTcwjkBL = new FiDetailXTcwjkBL();
			System.out.println("=============== -------------"+i);
			VData cInputData = new VData();
			TransferData tTransferData = new TransferData();
			String tStartDate = "";
			String tEndDate = "";
			tTransferData.setNameAndValue("StartDate", tStartDate);
			tTransferData.setNameAndValue("EndDate", tEndDate);
			cInputData.add(tTransferData);
			tFiDetailXTcwjkBL.submitData(cInputData, "");
		}
		long startTime2 = System.currentTimeMillis();
		System.out.println("======666666"+((startTime2 -startTime1)/1000)+"秒;"+((startTime2 -startTime1)/1000/60)+"分钟");
	}
}
