package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class XBAchieveRateBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
//  应收起始日期
  private String mStartDate="";
  private String mEndDate="";
//  实收起始日期
  private String mActuStartDate="";
  private String mActuEndDate="";
//  其他要素
  private String mSaleChnlType = "";
  private String mCountArea = "";
  private String mSaleChnl = "";
  private String mOrphans = "";
  private String mPayCount = "";
  private String mManageCom=""; 
  
//  子算法SQL
  private String msubpolsql = "";
  private String msubpaypersonsql=""; 
  private String mactupaypersonsql=" 1=1 "; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public XBAchieveRateBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
    	//cOperate为0是省级分公司,1为地市级分机构
      if( !cOperate.equals("0")&&!cOperate.equals("1") ) {
        buildError("submitData", "不支持的操作字符串");
        return null;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

   // 获取各项条件SQL
      getsubsql();
      
      if(cOperate.equals("0")){
      // 准备所有要打印的数据
      if( !getPrintData() ) {
        return null;
      }
      }
      else if(cOperate.equals("1")){
          // 准备所有要打印的数据
          if( !OmnigetPrintData() ) {
            return null;
          }}
      else{
    	  buildError("dealdata", "险种类型错误");
          return null;
      }

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  PAchieveRateBL tbl =new PAchieveRateBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-08");
      tTransferData.setNameAndValue("EndDate", "2010-10-08");
      tTransferData.setNameAndValue("ManageCom", "8644");
      tGlobalInput.ManageCom="8644";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"1");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }

  /**
   * 获取所有条件子SQL
   */
  private void getsubsql()
  {
//		管理机构
	  msubpolsql=" and lcp.managecom like '"+mManageCom+"%' ";
      msubpaypersonsql=" and ljsp.managecom like '"+mManageCom+"%' ";
      
//    应收时间
      msubpolsql+=" and lcp.paytodate between '" + mStartDate + "' and '" + mEndDate + "' ";
      msubpaypersonsql+=" and ljsp.lastpaytodate between '" + mStartDate + "' and '" + mEndDate + "' ";
      
//    实收时间
      if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(!(mActuEndDate.equals("")||mActuEndDate==null)))  {
//    	  msubpolsql=" and exists (select 1 from ljapayperson where polno=lcp.polno and confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "') ";
//    	  msubpolsql+=" and 1=2 ";
//        msubpaypersonsql+=" and ljsp.confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "' ";
//    	  实收的保单仅影响实收费用
    	  mactupaypersonsql = "  ljsp.confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "' ";
        }
       
      if(mSaleChnl.equals("99")){
    	  msubpolsql+=" and lcp.salechnl not in ('01','03','04','07','08','11','13') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and salechnl not in ('01','03','04','07','08','11','13') union select 1 from lbcont where contno=ljsp.contno and salechnl not in ('01','03','04','07','08','11','13')) ";
      }
      else if(mSaleChnl.equals("00")){
    	  System.out.println("选择为所有渠道");
      }
      else{
    	  msubpolsql+=" and lcp.salechnl='"+mSaleChnl+"' ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl='"+mSaleChnl+"' union select 1 from lbcont where contno=ljsp.contno and  salechnl='"+mSaleChnl+"') ";
      }
//      统计范围，省级为4位机构，地市级为8位机构
      if(mCountArea.equals("0")){
    	  msubpolsql+=" and length(trim(lcp.managecom))=4 ";
    	  msubpaypersonsql+=" and length(trim(ljsp.managecom))=4 ";
      }else if(mCountArea.equals("1")){
    	  msubpolsql+=" and length(trim(lcp.managecom))=8 ";
    	  msubpaypersonsql+=" and length(trim(ljsp.managecom))=8 ";
      }
      if(mSaleChnlType.equals("1")){
    	  msubpolsql+=" and lcp.salechnl not in ('04','13') ";
          msubpaypersonsql+="  and exists (select 1 from lccont where contno=ljsp.contno and  salechnl not in ('04','13') union select 1 from lbcont where contno=ljsp.contno and  salechnl not in ('04','13'))  ";
      }
      else if(mSaleChnlType.equals("2")){
    	  msubpolsql+=" and lcp.salechnl in ('04','13') ";
          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl  in ('04','13')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('04','13')) ";
      }
      
      if(mOrphans.equals("1")){
    	  msubpolsql+="  and not exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = lcp.AgentCode)<=(select SignDate from lccont where ContNo=lcp.ContNo union select SignDate from lbcont where ContNo=lcp.ContNo fetch first 1 row only) ";
          msubpaypersonsql+="  and not exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = ljsp.AgentCode)<=(select SignDate from lccont where ContNo=ljsp.ContNo  union select SignDate from lbcont where ContNo=ljsp.ContNo fetch first 1 row only)  ";
      }
      else if(mOrphans.equals("2")){
    	  msubpolsql+=" and (exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = lcp.AgentCode)>(select SignDate from lccont where ContNo=lcp.ContNo  union select SignDate from lbcont where ContNo=lcp.ContNo fetch first 1 row only)) ";
          msubpaypersonsql+=" and (exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = ljsp.AgentCode)>(select SignDate from lccont where ContNo=ljsp.ContNo  union select SignDate from lbcont where ContNo=ljsp.ContNo fetch first 1 row only)) ";
      }
      
      if(mPayCount.equals("2")){
    	  msubpolsql+="  and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=2 ";
          msubpaypersonsql+="  and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=2  ";
      }
      else if(mPayCount.equals("3")){
    	  msubpolsql+=" and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=3 ";
          msubpaypersonsql+=" and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=3 ";
      }
      else if(mPayCount.equals("4")){
    	  msubpolsql+=" and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )>3 ";
          msubpaypersonsql+=" and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )>3 ";
      }
      
  }

  
  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
    mStartDate = (String) mTransferData.getValueByName("DueStartDate");
    mEndDate = (String) mTransferData.getValueByName("DueEndDate");
    if( mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期不能为空！");
        return false;
      }
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mManageCom.equals("")||mManageCom==null) {
        buildError("getInputData", "没有得到足够的信息:管理机构不能为空！");
        return false;
      }
    mActuStartDate = (String) mTransferData.getValueByName("ActuStartDate");
    mActuEndDate = (String) mTransferData.getValueByName("ActuEndDate");
    if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(mActuEndDate.equals("")||mActuEndDate==null) ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
        return false;
      }
    if( (!(mActuEndDate.equals("")||mActuEndDate==null))&&(mActuStartDate.equals("")||mActuStartDate==null) ) {
        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
        return false;
      }
    mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
    if( mSaleChnl.equals("")||mSaleChnl==null) {
        buildError("getInputData", "没有得到足够的信息:销售渠道不能为空！");
        return false;
      }
    mCountArea = (String) mTransferData.getValueByName("CountArea");
    if( mCountArea.equals("")||mCountArea==null) {
        buildError("getInputData", "没有得到足够的信息:险种类型不能为空！");
        return false;
      }
    mSaleChnlType = (String) mTransferData.getValueByName("SaleChnlType");
    if( mSaleChnlType.equals("")||mSaleChnlType==null) {
        buildError("getInputData", "没有得到足够的信息:保单类型不能为空！");
        return false;
      }
    mOrphans = (String) mTransferData.getValueByName("Orphans");
    if( mOrphans.equals("")||mOrphans==null) {
        buildError("getInputData", "没有得到足够的信息:保单状态不能为空！");
        return false;
      }
    mPayCount = (String) mTransferData.getValueByName("PayCount");
    if( mPayCount.equals("")||mPayCount==null) {
        buildError("getInputData", "没有得到足够的信息:缴费次数不能为空！");
        return false;
      }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","应收保费",
    	  					"应收件数","实收保费","实收件数","退保保费","退保件数",
    	  					"失效保费","失效件数","保费达成率","件数达成率","保费退保率","保费失效率"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select  " +
		" substr(x.managecode,1,4) 机构代码, " +
  		" (select name from ldcom where comcode=substr(x.managecode,1,4)) 分公司, " +
//  		" x.managecode 支公司代码, " +
//  		" (select name from ldcom where comcode=x.managecode) 支公司, " +
	" sum((case when x.premsum is null then 0.0 else x.premsum end)) 应收保费_合计, " +
	" count(distinct(case when x.premcount<>'' then x.premcount end)) 应收件数_合计, " +
	" sum((case when x.actusum is null then 0.0 else x.actusum end)) 实收保费_合计, " +
	" count(distinct(case when x.actucount<>'' then x.actucount end)) 实收件数_合计, " +
  		" sum(x.ctprem) 退保保费_合计, " +
  		" count(distinct(case when x.ctcount<>'' then x.ctcount end)) 退保件数_合计, " +
  		" sum(x.delaysum) 失效保费_合计, " +
  		" count(distinct(case when x.delaycount<>'' then x.delaycount end)) 失效件数_合计, " +
  		" 0.0  保费达成率, " +
  		" 0.0  件数达成率, " +
  		" 0.0  保费退保率, " +
  		" 0.0  保费失效率  " +
  		" from  " +
  		" (select  " +
  		" ljsp.managecom managecode, " +
  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate and payplancode=ljsp.payplancode group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
  		" ljsp.contno premcount, " +
  		" (case when "+mactupaypersonsql+" then ljsp.sumactupaymoney else 0.0 end) actusum, " +
  		" (case when "+mactupaypersonsql+" then ljsp.contno else '' end) actucount, " +
  		" 0.0 ctprem, " +
  		" '' ctcount, " +
  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
  		" from ljapayperson ljsp " +
  		" where paytypeflag='1'  and payintv='12'  " +          //paytypeflag为空则是首期，0是续期，1是续保
  		msubpaypersonsql +
  		" and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
  		" and sumactupaymoney > 0 " +
  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno  and paytype not like 'YE%') > 0 " +
  		" union all " +
  		" select  " +
  		" lcp.managecom managecode, " +
//  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
  		" (select sum(prem) from lcpol where polno = lcp.polno fetch first 1 row only) premsum," +
  		" contno premcount, " +
  		" 0.0 actusum, " +
  		" '' actucount, " +
  		" 0.0 ctprem, " +
  		" '' ctcount, " +
  		" (case when stateflag='2' then prem else 0.0 end) delaysum, " +
  		" (case when stateflag='2' then contno else '' end) delaycount " +
  		"  from lcpol lcp  " +
  		" where 1=1 " +
  		" and conttype='1'  and payintv='12' " +
  		msubpolsql +
//  		" and stateflag='1' " +
  		" and lcp.rnewFlag != 2 and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
  		" and prem <> 0 " +
  		" and paytodate=payenddate " +
  		" union all " +
  		" select  " +
  		" lcp.managecom managecode, " +
  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
  		" contno premcount, " +
  		" 0.0 actusum, " +
  		" '' actucount, " +
  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) ctprem, " +
  		" contno ctcount, " +
  		" 0.0 delaysum, " +
  		" '' delaycount " +
  		"  from lbpol lcp where 1=1  and payintv='12' " +
  		msubpolsql +
  		" and lcp.rnewFlag != 2  and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
  		" and exists (select 1 from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate) " +
  		" and exists (select 1 from lpedoritem where edorno=lcp.edorno and edortype in ('XT','WT','CT')) " +
  		"  and prem <> 0 " +
  		"  and paytodate=payenddate) as x " +
  		"  group by x.managecode with ur ";
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }

      for (int j = 0; j < mExcelData.length;j++) {
    	  System.out.println(mExcelData.length);
//    	  System.out.println(mExcelData[0][1]);
//    	  System.out.println(mExcelData[1][0]);
    	  if(Double.parseDouble(mExcelData[j][2])!=0)
    	  {
    	  mExcelData[j][10]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][4])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  mExcelData[j][12]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][6])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  mExcelData[j][13]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][8])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  }
    	  if(Double.parseDouble(mExcelData[j][3])!=0)
    	  {
    	  mExcelData[j][11]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][5])/Double.parseDouble(mExcelData[j][3]), "0.0000"));
    	  }
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }

  private boolean OmnigetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"支公司代码","支公司","应收保费",
			"应收件数","实收保费","实收件数","退保保费","退保件数",
			"失效保费","失效件数","保费达成率","件数达成率","保费退保率","保费失效率"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
    //获得EXCEL列信息      
      String tSQL="select  " +
//    			" substr(x.managecode,1,4) 机构代码, " +
//    	  		" (select name from ldcom where comcode=substr(x.managecode,1,4)) 分公司, " +
    	  		" distinct x.managecode 支公司代码, " +
    	  		" (select name from ldcom where comcode=x.managecode) 支公司, " +
    	  		" sum((case when x.premsum is null then 0.0 else x.premsum end)) 应收保费_合计, " +
    	  		" count(distinct(case when x.premcount<>'' then x.premcount end)) 应收件数_合计, " +
    	  		" sum((case when x.actusum is null then 0.0 else x.actusum end)) 实收保费_合计, " +
    	  		" count(distinct(case when x.actucount<>'' then x.actucount end)) 实收件数_合计, " +
    	  		" sum(x.ctprem) 退保保费_合计, " +
    	  		" count(distinct(case when x.ctcount<>'' then x.ctcount end)) 退保件数_合计, " +
    	  		" sum(x.delaysum) 失效保费_合计, " +
    	  		" count(distinct(case when x.delaycount<>'' then x.delaycount end)) 失效件数_合计, " +
    	  		" 0.0  保费达成率, " +
    	  		" 0.0  件数达成率, " +
    	  		" 0.0  保费退保率, " +
    	  		" 0.0  保费失效率  " +
    	  		" from  " +
    	  		" (select  " +
    	  		" ljsp.managecom managecode, " +
    	  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate and payplancode=ljsp.payplancode group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
    	  		" ljsp.contno premcount, " +
    	  		" (case when "+mactupaypersonsql+" then ljsp.sumactupaymoney else 0.0 end) actusum, " +
    	  		" (case when "+mactupaypersonsql+" then ljsp.contno else '' end) actucount, " +
    	  		" 0.0 ctprem, " +
    	  		" '' ctcount, " +
    	  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
    	  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
    	  		" from ljapayperson ljsp " +
    	  		" where paytypeflag='1' and payintv='12'  " +
    	  		msubpaypersonsql +
    	  		" and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
    	  		" and sumactupaymoney > 0 " +
    	  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno  and paytype not like 'YE%') > 0 " +
    	  		" union all " +
    	  		" select  " +
    	  		" lcp.managecom managecode, " +
//    	  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
                " (select sum(prem) from lcpol where polno = lcp.polno fetch first 1 row only) premsum," +
    	  		" contno premcount, " +
    	  		" 0.0 actusum, " +
    	  		" '' actucount, " +
    	  		" 0.0 ctprem, " +
    	  		" '' ctcount, " +
    	  		" (case when stateflag='2' then prem else 0.0 end) delaysum, " +
    	  		" (case when stateflag='2' then contno else '' end) delaycount " +
    	  		"  from lcpol lcp  " +
    	  		" where 1=1 " +
    	  		" and conttype='1' and payintv='12'  " +
    	  		msubpolsql +
//    	  		" and stateflag='1' " +
    	  		" and lcp.rnewFlag != 2  and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
    	  		" and prem <> 0 " +
    	  		" and paytodate=payenddate " +
    	  		" union all " +
    	  		" select  " +
    	  		" lcp.managecom managecode, " +
    	  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
    	  		" contno premcount, " +
    	  		" 0.0 actusum, " +
    	  		" '' actucount, " +
    	  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only) ctprem, " +
    	  		" contno ctcount, " +
    	  		" 0.0 delaysum, " +
    	  		" '' delaycount " +
    	  		"  from lbpol lcp where 1=1  and payintv='12'  " +
    	  		msubpolsql +
    	  		" and lcp.rnewFlag != 2  and riskcode in (select riskcode from lmriskapp where riskType5 = '2' and riskcode not in('320106','120706')) " +
    	  		" and exists (select 1 from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate) " +
    	  		" and exists (select 1 from lpedoritem where edorno=lcp.edorno and edortype in ('XT','WT','CT')) " +
    	  		"  and prem <> 0 " +
    	  		"  and paytodate=payenddate) as x " +
    	  		"  group by x.managecode with ur ";
      
      
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
          
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }

      for (int j = 0; j < mExcelData.length;j++) {
    	  System.out.println(mExcelData.length);
//    	  System.out.println(mExcelData[0][1]);
//    	  System.out.println(mExcelData[1][0]);
    	  System.out.println("输出："+mExcelData[j][2]);
    	  if(Double.parseDouble(mExcelData[j][2])!=0)
    	  {
    	  mExcelData[j][10]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][4])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  mExcelData[j][12]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][6])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  mExcelData[j][13]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][8])/Double.parseDouble(mExcelData[j][2]), "0.0000"));
    	  }
    	  if(Double.parseDouble(mExcelData[j][3])!=0)
    	  {
    	  mExcelData[j][11]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][5])/Double.parseDouble(mExcelData[j][3]), "0.0000"));
    	  }
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }
  
  /**
   * 得到通过传入参数得到相关信息
   * @param strComCode
   * @return
   * @throws Exception
   */
  private boolean getExcelData()
  {

	  String tSQL="select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'" +
	  		" and managecom like '' and signdate between '' and '' with ur ";
	  ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
          CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
          mErrors.addOneError(tError);
          return false;
      }
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
    	  return false;
      }
      return true;
  }
  
  
}

