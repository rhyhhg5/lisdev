/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
public class UWResultPUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private TransferData tTransferData;

    public UWResultPUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("CONFIRM")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行业务处理
        if (!dealData()) {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData)) {
            return false;
        }

        UWResultPBL tUWResultPBL = new UWResultPBL();
        System.out.println("Start UWF1P UI Submit ...");

        if (!tUWResultPBL.submitData(vData, cOperate)) {
            if (tUWResultPBL.mErrors.needDealError()) {
                mErrors.copyAllErrors(tUWResultPBL.mErrors);
                return false;
            } else {
                buildError("submitData", "UWF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        } else {
            mResult = tUWResultPBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @param vData VData
     * @return boolean
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mLOPRTManagerSchema);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) {
        UWResultPUI tUWF1PUI = new UWResultPUI();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//        tLOPRTManagerSchema.setOtherNo("13000002628");
        tLOPRTManagerSchema.setPrtSeq("16000000626901");
        VData tVData = new VData();
        tVData.addElement(tLOPRTManagerSchema);

        tUWF1PUI.submitData(tVData, "CONFIRM");
    }
}
