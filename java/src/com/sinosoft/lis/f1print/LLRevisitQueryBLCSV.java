package com.sinosoft.lis.f1print;

import java.io.InputStream;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.RSWrapper;

public class LLRevisitQueryBLCSV {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";

    private String mStartDate = "";

    private String mEndDate = "";
    

    private String mOperator = "";

    private String tCurrentDate = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private PubFun mPubFun = new PubFun();

    private ListTable mListTable = new ListTable();

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mManageComNam = "";

    private String mFileNameB = "";

    private String mMakeDate = "";

    private String mContType = "";

    private String mContNo = "";

    private String mRgtNo = "";
    
    private String mRiskCode = "";

    private CSVFileWrite mCSVFileWrite = null;
    private String mFilePath = "";
    private String mFileName = "";
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                           + this.mManageCom);
        
    
        // 进行数据查询
        if (!queryDataToCVS()){
            return false;
        } else {

        }

        System.out.println("dayinchenggong1232121212121");

        return true;
    }

    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLCasePolicy";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
     *
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        // tCurrentDate = mPubFun.getCurrentDate();
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据 三个
            mOperator = (String) mTransferData.getValueByName("tOperator");
            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
          
            this.mManageCom = (String) mTransferData
                              .getValueByName("tManageCom");

            System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                               + this.mManageCom);
            this.mStartDate = (String) mTransferData
                              .getValueByName("tStartDate");//结案起始日期
            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案截止日期
            
            
            if (mManageCom == null || mManageCom.equals("")) {
            	 return false;
            }
            mManageComNam=getManagecomName(mManageCom);
            
            /*if (mStartDate == null || mStartDate.equals("")) {
                this.mStartDate = getYear(tCurrentDate) + "-01-01";
            }
            if (mEndDate == null || mEndDate.equals("")) {
                this.mEndDate = getYear(tCurrentDate) + "-12-31";
            }*/

            System.out
                    .println(
                            "XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
                            + mManageCom);
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }
 
    private boolean queryDataToCVS()
    {
    	
      int datetype = -1;
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    	tLDSysVarDB.setSysVar("LPCSVREPORT");

    	if (!tLDSysVarDB.getInfo()) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	
    	mFilePath = tLDSysVarDB.getSysVarValue(); 
        //   	本地测试
    	//mFilePath="E:\\picch\\ui\\vtsfile\\";
    	if (mFilePath == null || "".equals(mFilePath)) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:"+mFilePath);
    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
    	String tDate = PubFun.getCurrentDate2();

    	mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
    
    
    	String[][] tTitle = new String[5][];
    	tTitle[0] = new String[] { "","","","","","","","","","","理赔回访报表" };
    	tTitle[1] = new String[] { "机构："+mManageComNam,"","","","理赔审批日期："+mStartDate+"至"+mEndDate };
    	tTitle[2] = new String[] { "制表人："+mOperator,"","","","制表时间："+mPubFun.getCurrentDate() };
    	tTitle[3] = new String[] { "","保单资料","","","","受益人资料","","","","","","被保人资料","","","","","","","","","理赔资料","","","","","","","回访人员记录","","","","","","后续处理人员记录","","回访结果记录"};
    	tTitle[4] = new String[] { "序号","保单号","所属机构","理赔申请日期","数据抽档日期","理赔受益人姓名","理赔受益人性别","出生日期","单位电话","住宅电话","移动电话","客户号码","被保险人姓名","性别","出生日期","险种名称","保额","档次","期交保费","交费频次","出险情况","理赔申请日期","案件号","理赔结论","给付金额","结案日期","处理状态","回访人员","回访次数","一次回访结果","二次回访结果","三次回访结果","首次回访日期","数据接收人员","接收日期","回访成功人员","回访成功日期","是否拒访","是否收到理赔通知书","是否收到理赔金","理赔申请办理手续是否满意","理赔处理时效是否满意","赔付金额到账速度是否满意","工作人员服务态度是否满意","理赔整体服务是否满意","意见或建议"};
    	String[] tContentType = {"String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","Number","String","Number","String","String","String","String","String","Number","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String","String"};
    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
    		return false;
    	}
      if (!getDataListToCVS(datetype))
      {
        return false;
      }

      mCSVFileWrite.closeFile();

    	return true;
    }
    /*
     * 统计案件结果
     */
    private boolean getDataListToCVS(int datetype)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	
  

        String sql = "select "
                     + " (select name from ldcom where comcode=a.mngcom), "
                     + " (case when b.grpcontno='00000000000000000000' then b.contno else b.grpcontno end),"
                     + " b.contno,a.rgtno,a.caseno, "
                     + " codename('llrgtstate',a.rgtstate), "
                     + " a.customerno,a.customername,a.custbirthday, "
                     + " codename('sex',a.customersex), "
                     + " codename('idtype',a.idtype), "
                     + " a.idno,b.riskcode,sum(b.realpay), "
                     + " codename('llclaimdecision',b.givetype), "
                     + " a.rgtdate,a.rigister,a.endcasedate,a.handler,b.grpcontno,b.grppolno,b.polno "
                //     + " ,a.CustomerAge,(select getdutyname from lmdutygetclm where getdutycode=b.getdutycode and getdutykind=b.getdutykind), "
                     + " ,a.CustomerAge,'', "
                     + " (select min(m.accdate) from llsubreport m,llcaserela n where m.subrptno=n.subrptno and n.caseno=a.caseno),current date,(select drawer from ljaget where otherno=a.caseno fetch first 1 rows only), "
                     + "(select riskname from lmriskapp where riskcode=b.riskcode) "
                     + " from llcase a,llclaimdetail b,llclaim d "
                     + " where a.caseno=b.caseno and a.caseno=d.caseno  and a.mngcom like '" + mManageCom + "%' "
                     + " and a.UWDate between '" + mStartDate + "' and '" +   mEndDate + "' "
                     + " group by a.mngcom,b.grpcontno,b.contno,a.rgtno, "
                     + " a.caseno,a.rgtstate,a.customerno,a.customername, "
                     + " a.custbirthday,a.customersex,a.idtype,a.idno, "
                     + " b.riskcode,b.givetype,a.rgtdate,a.rigister, "
                     + " a.endcasedate,a.handler,b.grppolno,b.polno,a.CustomerAge order by a.caseno with ur ";

        System.out.println("SQL:" + sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql)) {
            System.out.println("数据准备失败! ");
            return false;
        }
     
        try {
            SSRS tSSRS = new SSRS();
            do {
                tSSRS = rsWrapper.getSSRS();
                if (tSSRS != null || tSSRS.MaxRow > 0) {
                
                    String tCaseNo = "";
                    String tContNo = "";
                    String tCustomerNo = "";
                    String tRiskCode = "";
                    String tContent[][] = new String[tSSRS.getMaxRow()][];
          
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        String ListInfo[] = new String[46];
         
                        for (int m = 0; m < ListInfo.length; m++) {
                            ListInfo[m] = "";
                        }
                        tCaseNo = tSSRS.GetText(i, 5);
                        System.out.println(tCaseNo);
                        tCustomerNo = tSSRS.GetText(i, 7);
                        tContNo = tSSRS.GetText(i, 3);
                        tRiskCode = tSSRS.GetText(i, 13);
                        //序号
                        ListInfo[0] = String.valueOf(i);
                        //管理机构
                        ListInfo[2] = tSSRS.GetText(i, 1);
             
                        //保单号码
                        ListInfo[1] = tSSRS.GetText(i, 2);
                        
                        //获取团体保单号
                        String grpcontno=tSSRS.GetText(i, 20);
                        //团体险种号
                        String grppolno= tSSRS.GetText(i, 21);
                        String tPolNo = tSSRS.GetText(i, 22);
                       //理赔申请日期
                        ListInfo[3]= tSSRS.GetText(i, 16);
                        //数据抽档日期
                        ListInfo[4]= tSSRS.GetText(i, 26);
                        //理赔受益人姓名
                        ListInfo[5] = tSSRS.GetText(i, 27);
                        //理赔号
                        ListInfo[22] = tSSRS.GetText(i, 5);
                        //案件状态
                        ListInfo[26] = tSSRS.GetText(i, 6);
                        //客户号
                        ListInfo[11] = tSSRS.GetText(i, 7);
                        //客户姓名
                        ListInfo[12] = tSSRS.GetText(i, 8);


                        //理赔险种
                        ListInfo[15] = tSSRS.GetText(i, 28);
  
                        //赔付金额
                        ListInfo[24] = tSSRS.GetText(i, 14);
                        //赔付结论
                        ListInfo[23] = tSSRS.GetText(i, 15);
                        //理赔申请日期
                        ListInfo[21]= tSSRS.GetText(i, 16);
                        // 结案日期
                        ListInfo[25]= tSSRS.GetText(i, 18);
                        
                        String tInsuredSQL = "select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " a.AgentCode, "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), " +
                                             " a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype),codename('sex',b.sex),b.Birthday,a.Mult "

                                             + " from lcpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " a.AgentCode, "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype),codename('sex',b.sex),b.Birthday,a.Mult "
                                             + " from lbpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " a.AgentCode, "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype),codename('sex',b.sex),b.Birthday,a.Mult "
                                             + " from lbpol a,lbinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' fetch first 1 rows only"
                                             + " with ur";

                        System.out.println(tInsuredSQL);
                        SSRS tInsuredSSRS = tExeSQL.execSQL(tInsuredSQL);
                        if (tInsuredSSRS.getMaxRow() > 0) {

                            //性别
                        	ListInfo[13] = tInsuredSSRS.GetText(1, 20);
                        	//出生日期
                        	ListInfo[14] = tInsuredSSRS.GetText(1, 21);
                        	//保额
                        	ListInfo[16] = tInsuredSSRS.GetText(1, 5);
                        	//档次
                        	ListInfo[17] = tInsuredSSRS.GetText(1, 22);
                        	//期交保费
                        	ListInfo[18] = tInsuredSSRS.GetText(1, 6);
                        	//交费频次
                        	ListInfo[19] = tInsuredSSRS.GetText(1, 7);
                        	//出险情况
                        	ListInfo[20] = "已出险";
                        }

                 
         //  mListTable.add(ListInfo);
           tContent[i - 1] = ListInfo;
       }
       mCSVFileWrite.addContent(tContent);
       if (!mCSVFileWrite.writeFile()) {
			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
			return false;
		}
   }else{
	String tContent[][] = new String[1][];
   String ListInfo[]={"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};	
   ListInfo[1]="0"; 
   ListInfo[2]="0"; 
   ListInfo[3]="0"; 
   ListInfo[4]="0"; 
   ListInfo[5]="0"; 
   ListInfo[6]="0"; 
   ListInfo[7]="0"; 
   ListInfo[8]="0"; 
   ListInfo[9]="0"; 
   ListInfo[10]="0";
   ListInfo[11]="0";
   ListInfo[12]="0";
   ListInfo[13]="0";
   ListInfo[14]="0";
   ListInfo[15]="0";
   ListInfo[16]="0";
   ListInfo[17]="0";
   ListInfo[18]="0";
   ListInfo[19]="0";
   ListInfo[20]="0";
   ListInfo[21]="0";
   ListInfo[22]="0";
   ListInfo[23]="0";
   ListInfo[24]="0";
   ListInfo[25]="0";
   ListInfo[26]="0";
   ListInfo[27]="0";
   ListInfo[28]="0";
   ListInfo[29]="0";
   ListInfo[30]="0";
   ListInfo[31]="0";
   ListInfo[32]="0";
   ListInfo[33]="0";
   ListInfo[34]="0";
   ListInfo[35]="0";
   ListInfo[36]="0";
   ListInfo[37]="0";
   ListInfo[38]="0";
   ListInfo[39]="0";
   ListInfo[40]="0";
   ListInfo[41]="0";
   ListInfo[42]="0";
   ListInfo[43]="0";
   ListInfo[44]="0";
   ListInfo[45]="0";
   ListInfo[0]="0";

   tContent[0]=ListInfo;
   mCSVFileWrite.addContent(tContent);
   if (!mCSVFileWrite.writeFile()) {
		mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
		return false;
	}

   }
} while (tSSRS != null && tSSRS.MaxRow > 0);
} catch (Exception ex) {

} finally {
rsWrapper.close();
}
return true;
      }
 

    private String getYear(String pmDate) {
        String mYear = "";
        String tSQL = "";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        mYear = tSSRS.GetText(1, 1);
        return mYear;
    }

  
    /**
     * 获取险种类型查询条件
     * @param aContType String
     * @return String
     */
    private String getContType(String aContType) {
        if (!"".equals(aContType)) {
            String tContTypeSQL =
                    " and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskprop='" +
                    aContType + "' ) ";
            return tContTypeSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取批次号查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRgtNo(String aRgtNo) {
        if (!"".equals(aRgtNo)) {
            String tRgtSQL = " and a.rgtno='" + aRgtNo + "'";
            return tRgtSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取保单号查询条件
     * @param aContNo String
     * @return String
     */
    private String getContNo(String aContNo) {
        if (!"".equals(aContNo)) {
            String tContSQL = " and (b.grpcontno='" + aContNo +
                              "' or b.contno='" + aContNo + "')";
            return tContSQL;
        } else {
            return "";
        }

    }
    
    /**
     * 获取险种查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRiskCode(String aRiskCode) {
        if (!"".equals(aRiskCode)) {
            String tRiskCodeSQL = " and b.riskcode='" + aRiskCode + "'";
            return tRiskCodeSQL;
        } else {
            return "";
        }
    }

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }
	public String getManagecomName(String com) {
		String sq="select name from ldcom where comcode='"+com+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
		return tGrpTypeSSRS.GetText(1, 1);
	}
}
