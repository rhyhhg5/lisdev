package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;


//程序名称： GrpExpirBenefitPrintBL.java
//程序功能： 团险给付单个打印
//创建日期：2008-11-11
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
public class GrpExpirBenefitPrintNewBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = null;
    private String mOperate = "";
    private LCGrpContSchema mLCGrpContSchema = null;
    private String mGrpContNo = "";
    private String mPrtSeqNo = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    public GrpExpirBenefitPrintNewBL() {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("GrpExpirBenefitPrintNewBL begin");
//        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT"))
//        {
//            buildError("submitData", "不支持的操作字符串");
//            return false;
//        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("GrpExpirBenefitPrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                              getObjectByObjectName("LOPRTManagerSchema", 0);
        if (mLOPRTManagerSchema == null) {
            mLCGrpContSchema = (LCGrpContSchema) cInputData.
                               getObjectByObjectName("LCGrpContSchema", 0);
        } else {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
            if (!tLCGrpContDB.getInfo()) {
                return false;
            }
            mLCGrpContSchema = tLCGrpContDB.getSchema();
        }
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
//        if("PRINT".equals(mOperate))
//        {
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpExpirBenefitPrint", "printer");
        textTag.add("JetFormType", "bq002");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        textTag.add("ManageComLength4", printcode);
        System.out.println(mGlobalInput.ClientIP);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }

        textTag.add("GrpContNo", mGrpContNo); //保单号

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setCode("bq002");
        tLOPRTManagerDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
        if (tLOPRTManagerSet.size()>0) {
            mPrtSeqNo = tLOPRTManagerSet.get(1).getPrtSeq();
        } else {
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            mPrtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        }
        textTag.add("TaskNo", mPrtSeqNo); //处理号
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mGrpContNo);
        tLCGrpAppntDB.getInfo();
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        tLCGrpAddressDB.getInfo();
        String tGrpZipCode = tLCGrpAddressDB.getLinkManZipCode1();
        if (tGrpZipCode == null || "".equals(tGrpZipCode) ||
            "null".equals(tGrpZipCode)) {
            tGrpZipCode = tLCGrpAddressDB.getGrpZipCode();
        }
        textTag.add("GrpZipCode", tGrpZipCode); //客户联系人邮编
        String tGrpAddress = tLCGrpAddressDB.getLinkManAddress1();
        if (tGrpAddress == null || "".equals(tGrpAddress) ||
            "null".equals(tGrpAddress)) {
            tGrpAddress = tLCGrpAddressDB.getGrpAddress();
        }
        textTag.add("GrpAddress", tGrpAddress); //客户联系人地址
        textTag.add("LinkMan1", tLCGrpAddressDB.getLinkMan1()); //收件人、联系人
        String tPhone = " ";
        String tPhone1 = tLCGrpAddressDB.getPhone1();
        if (tPhone1 != null && !"".equals(tPhone1) && !"null".equals(tPhone1)) {
            tPhone += tPhone1 + " ";
        }
        String tMobile1 = tLCGrpAddressDB.getMobile1();
        if (tMobile1 != null && !"".equals(tMobile1) && !"null".equals(tMobile1)) {
            tPhone += tMobile1;
        }
        textTag.add("Phone", tPhone); //联系人电话
        String sql = "select min(PayEndDate) from LCGrpPol where GrpContNo = '" +
                     mGrpContNo + "' "
                     + "and riskcode in ('170206','570106') with ur";
        String tPayEndDate = new ExeSQL().getOneValue(sql);
        textTag.add("PayToDate", CommonBL.decodeDate(tPayEndDate)); //保单应交费日期
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate())); //通知书生成日期
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLAAgentDB.getInfo();
        textTag.add("AgentName", tLAAgentDB.getName()); //本保单服务业务员
        textTag.add("AgentCode", tLAAgentDB.getGroupAgentCode()); //业务员业务代码
        String tAgentPhone = "";
        if (tAgentPhone != null || !"".equals(tAgentPhone) ||
            !"null".equals(tAgentPhone)) {
            tAgentPhone = tLAAgentDB.getPhone();
        } else {
            tAgentPhone = tLAAgentDB.getMobile();
        }
        textTag.add("AgentPhone", tAgentPhone); //业务员联系电话
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                           " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                           " (select agentgroup from laagent where agentcode ='"
                           + tLAAgentDB.getAgentCode() + "'))";
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName()); //业务员所属部门
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone()); //分公司电话
        textTag.add("ServiceName", tLDComDB.getLetterServiceName()); //分公司名
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress()); //分公司地址
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode()); //分公司邮编
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("Fax", tLDComDB.getFax()); //分公司传真
        textTag.add("CInValiDate", CommonBL.decodeDate(tPayEndDate)); //**年**月**日满期
        textTag.add("AppntName", tLCGrpAppntDB.getName()); //投保人名称
        textTag.add("CustomerNo", tLCGrpAppntDB.getCustomerNo()); //客户号
        sql = "select a.Peoples2 from LCGrpPol a "
              + "where a.GrpContNo = '" + mGrpContNo + "' "
              + "and riskcode = '170206' with ur";
        String peoples2 = new ExeSQL().getOneValue(sql);
        textTag.add("Yuliu1", peoples2); //预留1,被保人数peoples2
        String sqlmoney = "select sum(Amnt) from LCGrpPol where GrpContNo = '"+ mGrpContNo + "' and riskcode ='170206' with ur";
        String SumMoney = new ExeSQL().getOneValue(sqlmoney);
        textTag.add("Yuliu2", SumMoney); //预留2,满期金所有的合计
        textTag.add("Yuliu3", "Yuliu3"); //预留3
        textTag.add("Yuliu4", "Yuliu4"); //预留4
        textTag.add("Yuliu5", "Yuliu5"); //预留5
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        String[] title = {"险种代码", "险种名称", "被保险人数", "满期金合计"};
        xmlexport.addListTable(getListTable(), title);


        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);

        xmlexport.outputDocumentToFile("C:\\", "zmgtest");
        mResult.clear();
        mResult.addElement(xmlexport);
//        }
//        else if("INSERT".equals(mOperate))
//        {
////    		放入打印列表
//                        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
//            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
//            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
//            tLOPRTManagerSchema.setOtherNo(mGrpContNo);
//            tLOPRTManagerSchema.setOtherNoType("01");
//            tLOPRTManagerSchema.setCode("bq002");
//            tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
//            tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
//            tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
//            tLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
//            tLOPRTManagerSchema.setPrtType("0");
//            tLOPRTManagerSchema.setStateFlag("0");
//            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
//            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//            MMap tMap = new MMap();
//            tMap.put(tLOPRTManagerSchema, "INSERT");
//            VData tVData = new VData();
//            tVData.add(tMap);
//            PubSubmit tPubSubmit = new PubSubmit();
//            if (tPubSubmit.submitData(tVData, "") == false)
//            {
//                this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
//                return false;
//            }
//
//        }
        return true;
    }

    //获取列表数据
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        if (getListData()) {
            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
                String[] info = new String[10];
                for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                    info[j - 1] = mSSRS.GetText(i, j);
                }
                tListTable.add(info);
            }
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    //查询列表显示数据
    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.RiskCode ,(select RiskName from LMRiskApp where RiskCode = a.RiskCode), ")
                .append(
                        "a.Peoples2,(case a.RiskCode when '170206' then Amnt else 0 end) ")
                .append("from LCGrpPol a ")
                .append("where a.GrpContNo = '" + mGrpContNo + "' ")
                .append("and riskcode in ('170206','570106') with ur");
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL(sql.toString());
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
//        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(mPrtSeqNo);
        tLOPRTManagerSchema.setOtherNo(mGrpContNo); //存放前台传来的caseno
        tLOPRTManagerSchema.setOtherNoType("01");
        tLOPRTManagerSchema.setCode("bq002");
        tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        tLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }


}
