/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LCContF1PUI </p>
 * <p>Description: LCContF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class ReLCContF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    public ReLCContF1PUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start ReLCContF1PUI Submit ...");

        ReLCContF1PBL tReLCContF1PBL = new ReLCContF1PBL();
        try
        {
            if (!tReLCContF1PBL.submitData(cInputData, cOperate))
            {
                if (tReLCContF1PBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tReLCContF1PBL.mErrors);
                }
                else
                {
                    buildError("sbumitData", "tReLCContF1PBL发生错误，但是没有提供详细的出错信息");
                }
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    public static void main(String[] args)
    {
        ReLCContF1PUI tReLCContF1PUI = new ReLCContF1PUI();
        LCContSet tLCPolSet = new LCContSet();
        LCContSchema tLCContSchema = new LCContSchema();

        tLCContSchema.setContNo("230110000000070");
        tLCPolSet.add(tLCContSchema);

        VData vData = new VData();

        vData.addElement(new GlobalInput());
        vData.addElement(tLCPolSet);

        if (!tReLCContF1PUI.submitData(vData, "PRINT"))
        {
            System.out.println("fail to get print data");
        }
        else
        {
            vData = tReLCContF1PUI.getResult();
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
