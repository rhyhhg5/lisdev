package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAContComBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LAContComUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAContComUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAContComBL tLAContComBL = new LAContComBL();

        if (!tLAContComBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAContComBL.mErrors);

            return false;
        } else {
            this.mResult = tLAContComBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
