/**
 * <p>Title:需要输入统计项的的扫描报表</p>
 * <p>Description: 1张报表</p>
 * <p>bq1：保全日结清单（个险）</p>
 * <p>bq2: 保全月结清单（个险)</p>
 * <p>bq3: 保全日结清单（法人)</p>
 * <p>bq4: 保全月结清单（法人)</p>
 * <p>bq5: 保全日结清单（银代)</p>
 * <p>bq6: 保全月结清单(（银代)</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * <p>add date and  月结 加金额</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BQCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mBQCode = "";
    private String[] mDay = null;
    private String mScanOper = "";
    private String mDefineCode = "";
    private String mManageCom = "";
    private String mOpt = "";

    /**构造函数*/
    public BQCheckBL()
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    { //打印

        //全局变量
        mDay = (String[]) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        mBQCode = (String) cInputData.get(2);
        mDefineCode = (String) cInputData.get(3);
        mOpt = (String) cInputData.get(4);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));
        if (mDay == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }
        if (mDefineCode == "")
        {
            buildError("mDefineCode", "没有得到足够的信息！");

            return false;
        }
        if (mOpt == "")
        {
            buildError("mOpt", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BQCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**传输数据的公共方法*/
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");

            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        if (!getPrintDataPay())
        {
            return false;
        }

        return true;
    }

    private boolean getPrintDataPay()
    {
        System.out.println("报表代码类型qqqq：" + mDefineCode);

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        FinDayTool tFinDayTool = new FinDayTool();
        ListTable tlistTable = new ListTable();
        String[] strArr = null;
        String xmlname = "保全报表" + mDefineCode;
        String displayname = "display" + mDefineCode;
        System.out.println("displayneme:" + displayname);

        //set:
        LDCodeDB tLDcodeDB = new LDCodeDB();
        String msql = "select * from LDcode";
        LDCodeSet tLDCodeSet = tLDcodeDB.executeQuery(msql);
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();

        tlistTable.setName("BQ");

        ExeSQL BQExeSQL = new ExeSQL();
        SSRS BQSSRS = new SSRS();
        double TotalMoney = 0.0;
        int TotalNum = 0;
        String bq_sql = "";
        if (mDefineCode.equals("bq7") || mDefineCode.equals("bq8"))
        {
            String tBq_lcp =
                    "select lpedormain.edortype,lpedormain.grppolno,lpedormain.edorno,lcpol.AppntName,"
                    + "sum(lpedormain.getMoney),lcpol.agentcode,lpedormain.Operator,lpedormain.ModifyDate,"
                    +
                    "substr(lcpol.Managecom,1,4),lcpol.Managecom,lcpol.riskcode"
                    + " from lpedormain,lcpol,ldcode"
                    +
                    " where lpedormain.polno=lcpol.polno and lpedormain.edortype=ldcode.code"
                    + " and lpedormain.EdorState='0' and lcpol.appflag='1'"
                    + ReportPubFun.getBQCodeTypeSql(mDefineCode)
                    + ReportPubFun.getBQChnlSql(mDefineCode, "lcpol")
                    + ReportPubFun.getWherePart("lpedormain.Operator", mOpt)
                    + ReportPubFun.getWherePart("lpedormain.edortype", mBQCode)
                    + ReportPubFun.getWherePartLike("lpedormain.ManageCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("lpedormain.ModifyDate", mDay[0],
                                                mDay[1], 1);
            String tBq_lcp_groupby =
                    " group by lpedormain.edortype,lpedormain.grppolno,lpedormain.edorno,"
                    +
                    "lcpol.AppntName,lcpol.agentcode,lpedormain.Operator,lpedormain.ModifyDate,"
                    +
                    "substr(lcpol.Managecom,1,4),lcpol.Managecom,lcpol.riskcode";
            String tBq_lbp = StrTool.replace(tBq_lcp, "lcpol", "lbpol");
            String tBq_lbp_groupby = StrTool.replace(tBq_lcp_groupby, "lcpol",
                    "lbpol");
            bq_sql = tBq_lcp + tBq_lcp_groupby + " union all (" + tBq_lbp
                     + tBq_lbp_groupby + ") "
                     +
                     ReportPubFun.getBQtSql(mBQCode, mDefineCode, mDay[0],
                                            mDay[1],
                                            mManageCom)
                     + " order by 1,9,10,11";
            xmlexport.createDocument("BQDayEndNoFinace.vts", "printer");
            xmlexport.addDisplayControl(displayname);
            System.out.println("保全sql:" + bq_sql);
            BQSSRS = BQExeSQL.execSQL(bq_sql);
            System.out.println("大小：" + BQSSRS.getMaxRow());
            for (int i = 1; i <= BQSSRS.getMaxRow(); i++)
            {
                strArr = new String[12];
                strArr[0] = String.valueOf(i);
                strArr[1] = ReportPubFun.getBqItemName(BQSSRS.GetText(i, 1),
                        mDefineCode, tLDCodeSet); //变更项目
                strArr[2] = BQSSRS.GetText(i, 2); //集体保险单号
                strArr[3] = BQSSRS.GetText(i, 3); //批单号
                strArr[4] = BQSSRS.GetText(i, 4); //投保人
                strArr[5] = BQSSRS.GetText(i, 5); //金额
                strArr[6] = BQSSRS.GetText(i, 6); //代理人编码
                strArr[7] = BQSSRS.GetText(i, 7); //操作人编码
                strArr[8] = BQSSRS.GetText(i, 8); //修改时间
                strArr[9] = ReportPubFun.getMngName(BQSSRS.GetText(i, 9)); //分公司机构
                strArr[10] = ReportPubFun.getMngName(BQSSRS.GetText(i, 10)); //管理机构
                strArr[11] = ReportPubFun.getRiskName(BQSSRS.GetText(i, 11),
                        tLMRiskAppSet); //险种名称 　
                TotalNum = i;
                TotalMoney = TotalMoney + Double.parseDouble(strArr[5]);
                tlistTable.add(strArr);
            }
            System.out.println("TotalMoney:" + TotalMoney);
            strArr = new String[12];
            strArr[0] = "总计";
            strArr[1] = "";
            strArr[2] = "件数";
            strArr[3] = String.valueOf(TotalNum) + "件";
            strArr[4] = "金额";
            strArr[5] = ReportPubFun.functionJD(TotalMoney, "0.00") + "元";
            strArr[6] = "";
            strArr[7] = "";
            strArr[8] = "";
            strArr[9] = "";
            strArr[10] = "";
            strArr[11] = "";
        }
        else
        {
            //c表：
            String bq_lcp =
                    "select lpedormain.edortype,lpedormain.polno,lpedormain.edorno,lcpol.AppntName,"
                    + "lcpol.insuredname,lpedormain.getMoney,lcpol.agentcode,lpedormain.Operator,lpedormain.ModifyDate,"
                    +
                    "substr(lcpol.Managecom,1,4),lcpol.Managecom,lcpol.riskcode"
                    + " from lpedormain,lcpol,ldcode"
                    +
                    " where lpedormain.polno=lcpol.polno and lpedormain.edortype=ldcode.code "
                    + " and lpedormain.EdorState='0' and lcpol.appflag='1'"
                    + ReportPubFun.getBQCodeTypeSql(mDefineCode)
                    + ReportPubFun.getBQChnlSql(mDefineCode, "lcpol")
                    + ReportPubFun.getWherePart("lpedormain.Operator",
                                                mOpt)
                    + ReportPubFun.getWherePart("lpedormain.edortype",
                                                mBQCode)
                    + ReportPubFun.getWherePartLike("lpedormain.ManageCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("lpedormain.ModifyDate",
                                                mDay[0], mDay[1], 1);

            //b表：
            String bq_lbp = StrTool.replace(bq_lcp, "lcpol", "lbpol");
            bq_sql = "(" + bq_lcp + ") union all (" + bq_lbp + ") "
                     +
                     ReportPubFun.getBQSql(mBQCode, mDefineCode, mDay[0],
                                           mDay[1],
                                           mManageCom)
                     + " order by 1,10,11,12";
            xmlexport.createDocument("BQDayEnd.vts", "printer");
            xmlexport.addDisplayControl(displayname);
            System.out.println("保全sql:" + bq_sql);
            BQSSRS = BQExeSQL.execSQL(bq_sql);
            System.out.println("大小：" + BQSSRS.getMaxRow());

            for (int i = 1; i <= BQSSRS.getMaxRow(); i++)
            {
                strArr = new String[13];
                strArr[0] = String.valueOf(i);
                strArr[1] = ReportPubFun.getBqItemName(BQSSRS.GetText(i, 1),
                        mDefineCode, tLDCodeSet); //变更项目
                strArr[2] = BQSSRS.GetText(i, 2); //保险单号
                strArr[3] = BQSSRS.GetText(i, 3); //批单号
                strArr[4] = BQSSRS.GetText(i, 4); //投保人
                strArr[5] = BQSSRS.GetText(i, 5); //被保险人
                strArr[6] = BQSSRS.GetText(i, 6); //合计金额
                strArr[7] = BQSSRS.GetText(i, 7); //代理人编码
                strArr[8] = BQSSRS.GetText(i, 8); //操作人编码
                strArr[9] = BQSSRS.GetText(i, 9); //日期
                strArr[10] = ReportPubFun.getMngName(BQSSRS.GetText(i, 10)); //分公司机构
                strArr[11] = ReportPubFun.getMngName(BQSSRS.GetText(i, 11)); //管理机构
                strArr[12] = ReportPubFun.getRiskName(BQSSRS.GetText(i, 12),
                        tLMRiskAppSet); //险种名称
                TotalNum = i;
                TotalMoney = TotalMoney + Double.parseDouble(strArr[6]);
                tlistTable.add(strArr);
            }
            System.out.println("TotalMoney:" + TotalMoney);
            strArr = new String[13];
            strArr[0] = "总计";
            strArr[1] = "";
            strArr[2] = "";
            strArr[3] = "件数";
            strArr[4] = String.valueOf(TotalNum) + "件";
            strArr[5] = "金额";
            strArr[6] = ReportPubFun.functionJD(TotalMoney, "0.00") + "元";
            strArr[7] = "";
            strArr[8] = "";
            strArr[9] = "";
            strArr[10] = "";
            strArr[11] = "";
            strArr[12] = "";
        }
        tlistTable.add(strArr);
        xmlexport.addListTable(tlistTable, strArr);

        String CurrentDate = PubFun.getCurrentDate();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("sManageCom", ReportPubFun.getMngName(mManageCom));
        texttag.add("dManageCom", mGlobalInput.ManageCom);
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("time", CurrentDate);
        System.out.println("大小" + texttag.size());
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //xmlexport.outputDocumentToFile("e:\\",xmlname);//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }
}
