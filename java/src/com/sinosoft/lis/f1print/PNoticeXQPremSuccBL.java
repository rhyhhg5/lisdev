package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :续期保费银行转账失败清单
 * @version 1.0
 * @date 2004-5-26
 */

import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PNoticeXQPremSuccBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public PremBankPubFun mPremBankPubFun = new PremBankPubFun();

    private LCPolSet mLCPolSet = new LCPolSet();

    //初始化全局变量，从前台承接数据
    private String strMainPolNo = "";
    private String strAppntName = "";
    private String strAccName = "";
    private String strDate = "";
    private String strBankAccNo = "";
    private String strStation = "";
    private String strGetNoticeNo = "";

    private double mMoney = 0.00;
    private int count = 0;

    private VData mInputData;
    public PNoticeXQPremSuccBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        if (!cOperate.equals("PRINT"))
        {
            mPremBankPubFun.buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getPrintData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("XQBankSuccP.vts", "printer");

        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");

        String main_sql = "select PolNo,PayCount,SumActuPayMoney,RiskCode from LJSPayPerson where GetNoticeNo = '" +
                          strGetNoticeNo + "' union all "
                          + " select PolNo,PayCount,SumActuPayMoney,RiskCode  from LJAPayPerson where GetNoticeNo = '" +
                          strGetNoticeNo + "' ";
        System.out.println("查询的语句是" + main_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = new SSRS();
        main_ssrs = main_exesql.execSQL(main_sql);
        System.out.println("查询的结果是" + main_ssrs.getMaxRow());
        if (main_ssrs.getMaxRow() > 0)
        {
            for (int main_count = 1; main_count <= main_ssrs.getMaxRow();
                                  main_count++)
            {
                String[] cols = new String[4];
                cols[0] = main_ssrs.GetText(main_count, 1); //保单号码
                cols[1] = getRiskName(main_ssrs.GetText(main_count, 4)); //险种名称
                cols[3] = main_ssrs.GetText(main_count, 3); //保费
                cols[2] = main_ssrs.GetText(main_count, 2); //交费期数
                mMoney = mMoney + Double.parseDouble(cols[3]);
                count = count + 1;
                alistTable.add(cols);
            }
        }
        String[] b_col = new String[4];
        xmlexport.addDisplayControl("displayinfo");
        xmlexport.addListTable(alistTable, b_col);
        texttag.add("AppntName", strAppntName);
        texttag.add("AccName", strAccName);
        texttag.add("MainPolNo", strMainPolNo);
        texttag.add("Date", strDate);
        texttag.add("SumMoney", mMoney);
        texttag.add("BankAccNo", strBankAccNo);
        texttag.add("Date1", PubFun.getCurrentDate());
        texttag.add("ComName", getComName(strStation));
        texttag.add("SumCount", count);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("e:\\", "PNoticeXQPremSuccBL"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        strMainPolNo = (String) mInputData.get(0);
        strAppntName = (String) mInputData.get(1);
        strAccName = (String) mInputData.get(2);
        strDate = (String) mInputData.get(3);
        strBankAccNo = (String) mInputData.get(4);
        strStation = (String) mInputData.get(5);
        strGetNoticeNo = (String) mInputData.get(6);
        System.out.println("主险的保单号码是" + strMainPolNo);
        System.out.println("交费通知书号码" + strGetNoticeNo);
        return true;
    }

    public String getRiskName(String aRiskCode)
    {
        String strRiskName = "";
        LMRiskSchema aLMRiskSchema = new LMRiskSchema();
        LMRiskDB aLMRiskDB = new LMRiskDB();
        aLMRiskDB.setRiskCode(aRiskCode);
        aLMRiskDB.getInfo();
        aLMRiskSchema.setSchema(aLMRiskDB.getSchema());
        strRiskName = aLMRiskSchema.getRiskName();
        return strRiskName;
    }

    public String getComName(String ComCode)
    {
        String strComName = "";
        LDComDB aLDComDB = new LDComDB();
        aLDComDB.setComCode(ComCode);
        aLDComDB.getInfo();
        strComName = aLDComDB.getSchema().getName();
        return strComName;
    }

    public static void main(String[] args)
    {
    }
}
