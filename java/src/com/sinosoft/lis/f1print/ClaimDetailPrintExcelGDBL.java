package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLSecurityReceiptSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.vschema.LLCaseDrugSet;
import com.sinosoft.lis.schema.LLCaseDrugSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LLAppealSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.llcase.LLCaseCommon;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔给付细目表（Excel）打印类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author：Xx
 * @version：1.0
 */
public class ClaimDetailPrintExcelGDBL {
    public CErrors mErrors = new CErrors();
    /** 前台传入的封装数据，本次未使用*/
    private VData mInputData;
    private VData mResult = new VData();
    /** 前台传入的操作符，本次未使用*/
    private String mOperate;
    
    /** 数据封装集合 */
    private TransferData mTransferData = new TransferData();

    /** xml生成工具 */
    private XmlExport xml = new XmlExport();
   
    /** 理赔号 */
    private String mCaseNo;
    /** 赔案号 */
    private String mClmNo;
    /** 立案号 */
    private String mRgtNo;
    private String mRgtDate;
    /** 案件类型 */
    private String mRgtClass;
    /** 特别提示栏 */
    private String mRemarkDesc = "";
    /** 案件事件关联号 */
    private String mCaseRelaNo;
    /** 被保人姓名 */
    private String mCustomerName;
    /** 备注信息-第一行逻辑数组 */
    private int[] mremarktag = {0, 0, 0, 0, 0};
    /** 备注信息-第一行内容 */
    private String mRemark = "";
    /**被保人客户号*/
    private String mCustomerIDNo;
    /** 低段责任金额 */
    private double lowamnt = 0;
    /** 中段责任金额 */
    private double midamnt = 0;
    /** 高段一责任金额 */
    private double highamnt1 = 0;
    /** 高段二责任金额 */
    private double highamnt2 = 0;
    /** 超高段责任金额 */
    private double supinhosamnt = 0;
    /** 大额门诊责任金额 */
    private double supdooramnt = 0;
    /** 门急诊费用 */
    private double emergdooramnt = 0;
    /** 小额门诊费用 */
    private double smalldooramnt = 0;
    
    /** 系统全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 执行SQL的容器 */
    ExeSQL tExeSQL = new ExeSQL();
    /** 存放查询结果 */
    SSRS tSSRS = new SSRS();
    /** 案件受理机构 */
    private String MngCom = "";
    /** 被保险人年龄 */
    String mAge = "";
    /** 被保人状态 */
    private String mInsuredstat = "";
    /** 投保单位 */
    String mGrpName = "";
    /** 立案人/申请人地址 */
    String mRgtantAddress = "";
    /** 批次打印标记,excel打印并未使用 */
    private String mflag = null;
    /** 团体批次导入标记 */
    private String applyertype = "";
    
    /** 保单层数组 */
    private Vector mRiskInfo =  new Vector();
    /** 责任层数组 */
    private Vector mClaimInfo =  new Vector();
    /** 备注层数组 */
    private Vector mRemarkInfo =  new Vector();
    
    /** 业务员代码 */
    private String mAgentCode = "";
    /** 业务员名称 */
    private String mAgentName = "";
    /** 业务员组别 */
    private String mAgentGroup = "";
    
    /** 整个通知书总金额-中文 */
    private String mAllRealmoneyCN = "";
    /** 整个通知书总金额-数字 */
    private String mAllRealmoneyMA = "";
    
    /** 将生成的文件路径名称记录 */
    private String mFilePath = "";
    /** 批次导入的‘扣除明细’ */
    private String mNewRemark = "";

    public ClaimDetailPrintExcelGDBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        System.out.println("mInputData:"+mInputData.toString());
        mOperate = cOperate;
        System.out.println("mOperate:"+mOperate);
        mflag = cOperate;
        System.out.println("mflag:"+mflag);
        
        //从前端获取数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //从数据库获得数据
        if (!getBaseData()) {
            return false;
        }

        //准备需要打印的数据
        if (!preparePrintData()) {
            return false;
        }

        //准备打印管理表数据
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }

    private boolean getBaseData() {
    	//查询分案信息
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "案件信息查询失败");
            return false;
        }
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        MngCom = tLLCaseSchema.getMngCom();
        mCustomerName = tLLCaseSchema.getCustomerName();
        mCustomerIDNo = tLLCaseSchema.getIDNo();
        mRgtDate = tLLCaseSchema.getRgtDate();
        mAge = String.valueOf(tLLCaseSchema.getCustomerAge());
        mRgtNo = tLLCaseSchema.getRgtNo();
        if(!"".equals(tLLCaseSchema.getRemark()) && tLLCaseSchema.getRemark()!=null && 
        		!"null".equals(tLLCaseSchema.getRemark())){
        	mNewRemark = tLLCaseSchema.getRemark();
        }

        //查询立案信息
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "立案信息查询失败");
            return false;
        }
        mRgtClass = tLLRegisterDB.getRgtClass();
        System.out.println("案件类型："+mRgtClass);
        mGrpName = tLLRegisterDB.getGrpName();
        mRgtantAddress = tLLRegisterDB.getRgtantAddress();
        applyertype = tLLRegisterDB.getApplyerType();
        
        return true;
    }

    private boolean preparePrintData() {
        System.out.println("开始打印了");
        
        String Age = "　";//年龄
        String insuredstat = "　　";//被保人状态
        String titleInpatient = "　";//（社保）发生费用时间
        String FeeDate = "　";//账单日期
        String SubDate = "";//出险事件信息，发生时间
        String InpatientDate = "　";//住院起始日期
        String titledays = "　";//（社保）持续天数
        String days = "　";//实际住院天数
        String hospitalname = "";//医院名称
        String hosgrade = "";//医院级别
        String feeatti = "　";//医院属性
        
        //申诉、纠错处理，需要查找申诉纠错类案件的事件信息
        boolean tAppealFlag = false;
        LLAppealDB tLLAppealDB = new LLAppealDB();
        LLAppealSet tLLAppealSet = new LLAppealSet();
        String tOrigCaseNo = "";

        tLLAppealDB.setCaseNo(mCaseNo);
        tLLAppealSet = tLLAppealDB.query();
        if (tLLAppealSet.size() > 0) {
            tOrigCaseNo = mCaseNo;
            mCaseNo = tLLAppealSet.get(1).getAppealNo();//原始的C案件变为R、S案件
            tAppealFlag = true;
        }

        if (mCaseNo.substring(0, 1).equals("R") ||
            mCaseNo.substring(0, 1).equals("S")) {
            tLLAppealDB = new LLAppealDB();
            tLLAppealDB.setAppealNo(mCaseNo);
            tLLAppealSet = tLLAppealDB.query();
            tOrigCaseNo = tLLAppealSet.get(1).getCaseNo();
            tAppealFlag = true;
        }
        xml.createDocuments("ClaimDetailPrintExcelGD.htm", mGlobalInput); //初始化xml文档
              
        //案件下逐事件打印
        String SQL = "select distinct b.caserelano from llclaimdetail a ,llcaserela b where a.caseno ='"
            + mCaseNo
            + "' and a.Caseno = b.CaseNo  and a.CaseRelaNo = b.CaseRelaNo with ur";
        System.out.println("SQL:"+SQL);
        tSSRS = tExeSQL.execSQL(SQL);   
        String caserelano = "";
        int num = tSSRS.MaxRow; 
        System.out.println("num:" + num);
        
        double mAllRealPay = 0;   
        //理赔案件的赔付细目信息，此处直接通过向Excel写文件的形式完成最终的输出，不需要模板
    	//保单号、险种名等的集合，暂时留作记录用
		String[] tRiskListTitle = new String[7];
        tRiskListTitle[0] = "ContNo"; //保单号
        tRiskListTitle[1] = "RiskName"; //险种名称
        tRiskListTitle[2] = "SubDate"; //事件时间
        tRiskListTitle[3] = "RealDays"; //连续天数
        tRiskListTitle[4] = "HospitName"; //就诊医院（医院级别）
        tRiskListTitle[5] = "ClaimInfo"; //与其下的责任赔付关联
        tRiskListTitle[6] = "AllRealMoney"; //小计
        if (tSSRS != null && tSSRS.getMaxRow() > 0) { 
        	for (int k = 1; k <= num; k++) {  
        		
            	mremarktag[0] = 0;
            	mremarktag[1] = 0;
            	mremarktag[2] = 0;
            	mremarktag[3] = 0;
            	mremarktag[4] = 0;
            	
                String strLine0[] = new String[7];//保单号、险种名等的集合 		
                String tSql1 =
                        "select distinct grpcontno,contno from llclaimdetail where caseno = '" +
                        mCaseNo + "'";
                ExeSQL exesql1 = new ExeSQL();
                SSRS resu = exesql1.execSQL(tSql1);
                if (resu != null) {
                    for (int j = 1; j <= resu.getMaxRow(); j++) {
                        if (resu.GetText(j, 1).equals("00000000000000000000")) {
                        	strLine0[0] = resu.GetText(j, 2);
                        } else {
                        	strLine0[0] = resu.GetText(j, 1);
                        }
                        String tRiskSql =
                                "select distinct B.RiskName from llclaimdetail A,lmrisk B "
                                + " where B.Riskcode =A.riskcode and caseno = '" +
                                mCaseNo
                                + "' and contno = '" + resu.GetText(j, 2) + "'";
                        SSRS risktb = exesql1.execSQL(tRiskSql);
                        System.out.println("tRiskSql:"+tRiskSql);
                        String RiskName = "";
                        if (resu != null) {
                            for (int m = 1; m <= risktb.getMaxRow(); m++) {
                                RiskName += "《" + risktb.GetText(m, 1) + "》";
                                if (m != risktb.getMaxRow()) {
                                    RiskName += "、";
                                }
                            }
                            strLine0[1] = RiskName;                            
                        }                       
                    }
                }
        		
        		//一个一个事件的打印账单
        		caserelano = tSSRS.GetText(k, 1);
        		             
                /*分案诊疗明细*/
                LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
                LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
                tLLCaseCureDB.setCaseNo(mCaseNo);
                tLLCaseCureDB.setCaseRelaNo(caserelano);
                tLLCaseCureSet.set(tLLCaseCureDB.query());

                //查询帐单信息
                LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
                LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
                tLLFeeMainDB.setCaseNo(mCaseNo);
                tLLFeeMainDB.setCaseRelaNo(caserelano);
                tLLFeeMainSet.set(tLLFeeMainDB.query());
                if (tLLFeeMainDB.mErrors.needDealError() == true) {
                    CError.buildErr(this, "帐单信息查询失败");
                    return false;
                }
                
                //账单主表的记录条数
                int count_2 = tLLFeeMainSet.size();               
                if (count_2 > 0) {
                	//从账单主表中取出险人年龄，若为0，取立案时报案人年龄
                	Age = String.valueOf(tLLFeeMainSet.get(1).getAge());
                	if ("0".equals(Age)) {
                        Age = mAge;
                    }
                	mAge = Age;
                	//从账单主表中取出险人工作状态
                    if (tLLFeeMainSet.get(1).getInsuredStat() != null) {
                        if (tLLFeeMainSet.get(1).getInsuredStat().equals("1")) {
                            insuredstat = "在职";
                            mInsuredstat= "在职";
                        }
                        if (tLLFeeMainSet.get(1).getInsuredStat().equals("2")) {
                            insuredstat = "退休";
                            mInsuredstat = "退休";
                        }
                    }
                    //从账单主表中取账单日期
                    FeeDate = tLLFeeMainSet.get(1).getFeeDate();
                    //医院名称
                    hospitalname = tLLFeeMainSet.get(1).getHospitalName();
                    //住院起始日期
                    InpatientDate = tLLFeeMainSet.get(1).getHospStartDate();
                    //实际住院天数
                    days = String.valueOf(tLLFeeMainSet.get(1).getRealHospDate());                  
                    //查询账单属性，目前打印结果上没有该字段展示
                    String tCaseRelaSQL = "";
                    if (!(caserelano.equals("") || caserelano == null)) {
                    	tCaseRelaSQL = " and caserelano='" + caserelano + "'";
                    }
                    String sqlx1 =
                        "select distinct FeeAtti from llfeemain where caseno='" +
                        mCaseNo + "' " + tCaseRelaSQL;
                    SSRS tSSRS = tExeSQL.execSQL(sqlx1);
                    for (int i = 1; i <= tSSRS.MaxRow; i++) {
                        if (tSSRS.GetText(i, 1).equals("0")) {
                            feeatti += "  医院  ";
                        }
                        if (tSSRS.GetText(i, 1).equals("1")) {
                            feeatti += "  社保结算  ";
                        }
                        if (tSSRS.GetText(i, 1).equals("2")) {
                            feeatti += "  手工报销  ";
                        }
                        if (tSSRS.GetText(i, 1).equals("3")) {
                            feeatti += "  社保补充  ";
                        }
                    }
                    //查询账单的住院内容，目前打印结果上没有该字段展示
                    if ("1".equals(tLLFeeMainSet.get(1).getFeeType())) {
                        titleInpatient = "（社保）发生费用时间";
                        titledays = "连续天数";
                    } else {
                        titleInpatient = "入院时间";
                        titledays = "住院天数";
                    }
                    //查询医院级别
                    if (tLLFeeMainSet.get(1).getHosGrade() != null &&
                        !tLLFeeMainSet.get(1).getHosGrade().equals("")
                            ) {
                        String sql =
                                "select codename from ldcode where codetype='levelcode' and code ='" +
                                tLLFeeMainSet.get(1).getHosGrade().substring(0, 1) +
                                "'";
                        ExeSQL exesql = new ExeSQL();
                        hosgrade = exesql.getOneValue(sql);
                    }                    
                }else {
                	//账单主表中无记录，取医院名称、住院起始日期、医院级别
                    if (tLLCaseCureSet.size() > 0) {
                        if (tLLCaseCureSet.get(1).getHospitalName() != null) {
                            hospitalname = tLLCaseCureSet.get(1).getHospitalName();
                        }
                        if (tLLCaseCureSet.get(1).getDiagnoseDate() != null) {
                            InpatientDate = tLLCaseCureSet.get(1).getDiagnoseDate();
                        }
                        if (tLLCaseCureSet.get(1).getHospitalCode() != null) {
                            String sql3 = "select codename from ldcode where codetype='levelcode' and code =(select substr(levelcode,1,1) from ldhospital where HospitCode='" +
                                          tLLCaseCureSet.get(1).getHospitalCode() +
                                          "')";
                            hosgrade = tExeSQL.getOneValue(sql3);
                        }
                    }
                }
                
                //若账单主表中无出险人工作状态，从被保险人表中取
                if ("".equals(insuredstat)) {
                    String sql1 = "select insuredstat from lcinsured where insuredno=(select customerno from llcase where caseno='" +
                                  mCaseNo + "')";
                    String tinsuredstat = tExeSQL.getOneValue(sql1);
                    if (tinsuredstat.equals("1")) {
                        insuredstat = "在职";
                        mInsuredstat = "在职";
                    }
                    if (tinsuredstat.equals("2")) {
                        insuredstat = "退休";
                        mInsuredstat = "退休";
                    }
                }
                               
                //社保手工报销明细
                LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
                LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
                String tSql = "";
                if (caserelano != null && !caserelano.equals("") &&
                    !caserelano.equals("null")) {
                	tSql = "select * from LLSecurityReceipt where mainfeeno in " +
                          "(select mainfeeno from llfeemain where caseno='" + mCaseNo +
                          "' and caserelano='" + caserelano + "')";
                } else {
                	tSql = "select * from LLSecurityReceipt where mainfeeno in " +
                          "(select mainfeeno from llfeemain where caseno='" + mCaseNo +
                          "')";
                }
                tLLSecurityReceiptSet.set(tLLSecurityReceiptDB.executeQuery(tSql));
                
                double selfpay1 = 0;//自负1，打印样式未使用
                double planfee = 0;//本次统筹基金医疗费用，打印样式未使用
                double FeeInSecurity = 0;//医保目录内金额，打印样式未使用
                double getlimit = 0;//起付限，打印样式未使用
                double supInHosFee = 0; //本次住院大额医疗费用，打印样式未使用                
                double selfpay2 = 0;//自负2，分类自负                
                double selfamnt = 0;//自负               
            
//                tAppealFlag=false;
                if (tLLSecurityReceiptSet != null && tLLSecurityReceiptSet.size() > 0 &&
                        !tAppealFlag) {
                	getlimit = tLLSecurityReceiptSet.get(1).getGetLimit();
                	System.out.println("GetLimit:"+getlimit);
                    if (tLLSecurityReceiptSet.size() > 0) {
                        int SRcount = tLLSecurityReceiptSet.size();
                        for (int i = 1; i <= SRcount; i++) {
                            LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                                    LLSecurityReceiptSchema();
                            tLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(i);
                            selfpay1 += tLLSecurityReceiptSchema.getSelfPay1();
                            selfpay2 += tLLSecurityReceiptSchema.getSelfPay2();
                            selfamnt += tLLSecurityReceiptSchema.getSelfAmnt();
                            planfee += tLLSecurityReceiptSchema.getPlanFee();
                            FeeInSecurity += tLLSecurityReceiptSchema.getFeeInSecu();
                            lowamnt += tLLSecurityReceiptSchema.getLowAmnt();
                            midamnt += tLLSecurityReceiptSchema.getMidAmnt();
                            highamnt1 += tLLSecurityReceiptSchema.getHighAmnt1();
                            highamnt2 += tLLSecurityReceiptSchema.getHighAmnt2();
                            supinhosamnt += tLLSecurityReceiptSchema.getSuperAmnt();
                            smalldooramnt += tLLSecurityReceiptSchema.getSmallDoorPay();
                            emergdooramnt += tLLSecurityReceiptSchema.getEmergencyPay();
                            supdooramnt += tLLSecurityReceiptSchema.getHighDoorAmnt();
                            supInHosFee += tLLSecurityReceiptSchema.getSupInHosFee();
                        }
                    }
                }
                                                            
                /*分案事件关联*/
                LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
                LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
                tLLCaseRelaDB.setCaseNo(mCaseNo);
                tLLCaseRelaDB.setCaseRelaNo(caserelano);
                tLLCaseRelaSet.set(tLLCaseRelaDB.query());                
                String sql2 = "select accdate from llsubreport where subrptno='" +
                              tLLCaseRelaSet.get(1).getSubRptNo() + "'";
                SubDate = tExeSQL.getOneValue(sql2);//事件出险日期      
                
                //整理RiskInfo中的数据
                strLine0[2] = SubDate;//事件出险日期  
                strLine0[3] = days;//连续天数
                strLine0[4] = hospitalname + "（"+hosgrade+"）";//就诊医院（医院级别）
                strLine0[5] = "ClaimInfo"+k;//和另一个数组关联的标记

                //查询赔案信息
                LLClaimSet tLLClaimSet = new LLClaimSet();
                LLClaimSchema tLLClaimSchema = new LLClaimSchema();
                LLClaimDB tLLClaimDB = new LLClaimDB();
                tLLClaimDB.setCaseNo(mCaseNo);
                tLLClaimSet.set(tLLClaimDB.query());
                if (tLLClaimDB.mErrors.needDealError() == true ||
                    tLLClaimSet == null || tLLClaimSet.size() == 0) {
                    CError.buildErr(this, "赔案信息查询失败");
                    return false;
                }
                tLLClaimSchema.setSchema(tLLClaimSet.get(1));
                mClmNo = tLLClaimSchema.getClmNo();
                
                //查询赔付明细信息
                LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
                LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
                tLLClaimDetailDB.setClmNo(mClmNo);
                tLLClaimDetailDB.setCaseRelaNo(caserelano);
                tLLClaimDetailSet.set(tLLClaimDetailDB.query());
                if (tLLClaimDetailDB.mErrors.needDealError() == true) {
                    CError.buildErr(this, "赔付明细信息查询失败");
                    return false;
                }
                    
                //责任账单明细
                String[] tClaimListTitle = new String[18];
                tClaimListTitle[0] = "GetDutyCode"; //责任项目
                tClaimListTitle[1] = "FeeDate"; //帐单日期
                tClaimListTitle[2] = "TabFee"; //帐单金额
                tClaimListTitle[3] = "FeeInSecurity"; //社保内
                tClaimListTitle[4] = "FeePayed"; //已报销
                tClaimListTitle[5] = "RefuseAmnt"; //不合理费用
                tClaimListTitle[6] = "SelfPay1"; //自负一
                tClaimListTitle[7] = "RiskSeqNo"; //险种序号
                tClaimListTitle[8] = "PayFreeAmnt"; //免赔额
                tClaimListTitle[9] = "ClaimMoney"; //理算金额
                tClaimListTitle[10] = "RealPay"; //实赔金额
                tClaimListTitle[11] = "Remark"; //备注
                tClaimListTitle[12] = "GetRate"; //给付比例
                tClaimListTitle[13] = "FeeOutSecurity"; //社保外
                tClaimListTitle[14] = "TabFee2"; //上海社保账单
                tClaimListTitle[15] = "FeeOutSecu2"; //上海社保账单
                tClaimListTitle[16] = "tempx"; //临时
                tClaimListTitle[17] = "ClaimInfo"; //和之前的保单险种对应
               
                String strLine[] = null;
                double AllFee = 0;
                double AllClaimMoney = 0;//一个账单，理赔理算明细下全部的理算金额
                double AllRealPay = 0;//一个账单，理赔理算明细下全部的理算金额
                
                //赔案明细的记录条数
                int count_1 = tLLClaimDetailSet.size();
                LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
                LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
                if (!tAppealFlag) {
                    for (int i = 1; i <= count_1; i++) {
                        strLine = new String[18];
                        //赔付明细还有记录
                        tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
                        String strGetDutyName = getGetDutyName(
                                tLLClaimDetailSchema.getGetDutyCode());
                        String strGetDutyKind = "";
                        if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
                                1).equals("1")) {
                            strGetDutyKind = " and feetype='2' ";
                        }
                        if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
                                1).equals("2")) {
                            strGetDutyKind = "and feetype='1'";
                        }

                        if (strGetDutyName == null) {
                            return false;
                        }
                        int indexb = -1;
                        indexb = String.valueOf(strGetDutyName).indexOf("保");
                        if (indexb < 0) {
                            strLine[0] = String.valueOf(strGetDutyName);//责任项目
                        } else {
                            strLine[0] = String.valueOf(strGetDutyName).substring(0,
                                    indexb); //责任项目
                        }
                        
                        LCPolDB tLCPolDB = new LCPolDB();
                        tLCPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
                        if (!tLCPolDB.getInfo()) {
                            LBPolDB tLBPolDB = new LBPolDB();
                            tLBPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
                            if (!tLBPolDB.getInfo()) {
                                CError.buildErr(this, "保单信息查询失败");
                                return false;
                            } else {
                                tLCPolDB.setRiskSeqNo(tLBPolDB.getRiskSeqNo());
                            }
                        }
                        strLine[7] = "" + tLCPolDB.getRiskSeqNo();
                        strLine[1] = "";
                        strLine[2] = "0.00";
                        strLine[3] = "0.00";
                        strLine[4] = "0.00";
                        strLine[5] = "0.00";
                        strLine[13] = "0.00";
                        strLine[14] = "0.00";
                        strLine[15] = "0.00";
                        strLine[16] = "";
                        strLine[17] = "ClaimInfo"+k;
                      
                        if (tLLFeeMainSet.size() > 0) {
                            tLLFeeMainSchema = tLLFeeMainSet.get(1);
                        }
                        //帐单还有记录备注信息
                        if (!tLLFeeMainSchema.equals("")) {
                            if (tLLFeeMainSchema.getFeeDate() != null) {
                                strLine[1] = tLLFeeMainSchema.getFeeDate(); //帐单日期
                            }
                        }

                        double acountMoney = 0;
                        double FIS = 0;
                        double FOS = 0;
                        double RefuseFee = 0;
                        double PayedFee = 0;

                        String sql10 =
                                "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='" +
                                tLLClaimDetailSchema.getCaseNo() + "' "
                                +
                                " and  mainfeeno in ( select mainfeeno from llfeemain where caserelano ='" +
                                tLLClaimDetailSchema.getCaseRelaNo()
                                + "' " + strGetDutyKind + " )";

                        ExeSQL tExeSQL = new ExeSQL();
                        SSRS tSSRS = new SSRS();
                        tSSRS = tExeSQL.execSQL(sql10);
                        acountMoney = Double.parseDouble(tSSRS.GetText(1, 1));
                        FIS = Double.parseDouble(tSSRS.GetText(1, 2));
                        FOS = Double.parseDouble(tSSRS.GetText(1, 3));
                        RefuseFee = Double.parseDouble(tSSRS.GetText(1, 4));
                        if (acountMoney == 0) {
                            String sql11 =
                                    "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='" +
                                    tLLClaimDetailSchema.getCaseNo()
                                    + "' and  mainfeeno in ( "
                                    +
                                    " select mainfeeno from llfeemain where caserelano ='" +
                                    tLLClaimDetailSchema.getCaseRelaNo()
                                    + "' " + strGetDutyKind + " )";

                            SSRS fSSRS = new SSRS();
                            fSSRS = tExeSQL.execSQL(sql11);
                            acountMoney = Double.parseDouble(fSSRS.GetText(1, 1));
                            FIS = Double.parseDouble(fSSRS.GetText(1, 2));
                            FOS = Double.parseDouble(fSSRS.GetText(1, 3));
                            RefuseFee = Double.parseDouble(fSSRS.GetText(1, 4));

                        }
                        if (acountMoney == 0) {
                            String sql12 =
                                    "select coalesce(sum(sumfee),0) from llfeemain where caseno='" +
                                    mCaseNo
                                    + "' and  mainfeeno in ( "
                                    +
                                    " select mainfeeno from llfeemain where caserelano ='" +
                                    tLLClaimDetailSchema.getCaseRelaNo()
                                    + "' " + strGetDutyKind + " )";
                            SSRS fSSRS = new SSRS();
                            fSSRS = tExeSQL.execSQL(sql12);
                            acountMoney = Double.parseDouble(fSSRS.GetText(1, 1));
                        }
                        if (acountMoney == 0) {
                            if (tLLClaimDetailSchema.getTabFeeMoney() > 0) {
                                acountMoney = tLLClaimDetailSchema.getTabFeeMoney();
                            }
                        }
                        if (tLLClaimDetailSchema.getGetDutyCode().equals("1605")) {
                            if (!tLLFeeMainSchema.equals("")) {
                                PayedFee += tLLFeeMainSchema.getSumFee() -
                                        tLLFeeMainSchema.getRemnant();
                            }
                        }
                        strLine[2] = String.valueOf(acountMoney); //帐单金额
                        strLine[14] = String.valueOf(Arith.round(acountMoney, 2));//上海社保账单->存放账单金额
                        strLine[13] = String.valueOf(Arith.round(FOS, 2)); //社保外
                        double fos2 = Arith.round(acountMoney - FOS - RefuseFee, 2);
                        strLine[15] = String.valueOf(fos2); //上海社保账单->存放社保外
                        
                        if("5".equals(applyertype)||"2".equals(applyertype)){
                        	strLine[5] = String.valueOf(tLLFeeMainSchema.getRefuseAmnt());//上海修改，全国通用 不合理费用,批次取导入模板中的“不合理费用”。
                        }else{
                        	strLine[5] = String.valueOf(Arith.round(RefuseFee, 2)); //不合理费用
                        }
                        
                        strLine[13] = String.valueOf(Arith.round(FOS, 2)); //社保外
                        
                        if("8631".equals(tLLClaimSchema.getMngCom().substring(0, 4))){
                        	if("5".equals(applyertype)||"2".equals(applyertype)){
                        		strLine[3] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //实际对应模板中的“已报销费用”(批次导入的，取导入的“统筹支付”和“大额救助支付”字段合计金额)    		
                        	}else{
                        		strLine[3] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //已报销费用(个案处理的，取“统筹”、“附加”，也相当于批次导入 modify by Houyd 20140422)		     		
                        	}
                        }else{
                        	strLine[3] = String.valueOf(Arith.round(FIS, 2)); //已报销费用
                        }
                        //System.out.println("机构："+tLLClaimSchema.getMngCom().substring(0, 4));
                        if("8631".equals(tLLClaimSchema.getMngCom().substring(0, 4))){               	
                        	strLine[13] = String.valueOf(Arith.round(selfamnt+selfpay2, 2)); //社保外 modify by Houyd 20140422
                        	if("5".equals(applyertype)||"2".equals(applyertype)){
                        		
                        	}else{
                        		strLine[5] = String.valueOf("0.00");//上海修改 不合理费用,个案不合理费用不对应
                        	}
                        }else{
                        	strLine[4] = String.valueOf(Arith.round(PayedFee, 2)); //不对应模板中的任一字段
                        }
                        
                        AllFee += acountMoney;
                        if (count_2 <= 0) {
                            strLine[1] = "　　　";
                            strLine[2] = "0.00";
                            strLine[3] = "0.00";
                            strLine[4] = "0.00";
                            strLine[5] = "0.00";
                            strLine[13] = "0.00";
                        }
                        strLine[6] = "";
                        if (tLLClaimDetailSchema.getGetDutyCode().trim().equals(
                                "207201")) {
                            strLine[8] = "3天"; //免赔天数
                            strLine[3] = "0.00";
                            strLine[5] = "0.00";
                            strLine[13] = "0.00";
                            strLine[15] = "0.00";

                        } else {
                            strLine[8] = String.valueOf(tLLClaimDetailSchema.
                                                        getOutDutyAmnt()); //免赔
                        }

                        strLine[9] = String.valueOf(tLLClaimDetailSchema.getClaimMoney()); //理算金额
                        strLine[10] = String.valueOf(tLLClaimDetailSchema.getRealPay()); //实赔金额
                        strLine[11] = trunGiveType(tLLClaimDetailSchema.getGiveType()); //备注
                        strLine[12] = String.valueOf(tLLClaimDetailSchema.
                                                     getOutDutyRate()); //给付比例
                        AllClaimMoney += tLLClaimDetailSchema.getClaimMoney();
                        AllRealPay += tLLClaimDetailSchema.getRealPay();//小计
                        
//                        String tREMARK = ""+tLLClaimDetailSchema.getREMARK();
//                        if(tLLClaimDetailSchema.getREMARK()==null){
//                        	tREMARK = "";
//                        }
                                               
                        
                        //备注信息的整理
                        if (tLLClaimDetailSchema.getGiveType().equals("3") ||
                            tLLClaimDetailSchema.getGiveType().equals("4") ||
                            tLLClaimDetailSchema.getGiveType().equals("5")) {
                            mRemarkDesc += strLine[0] + "责任" +
                                    tLLClaimDetailSchema.getGiveTypeDesc() + "原因:"
                                    + tLLClaimDetailSchema.getGiveReasonDesc() + "\n";
                        } else {
                            System.out.println("LLClaimDetail:" +
                                               tLLClaimDetailSchema.getGiveReasonDesc());
                            if (tLLClaimDetailSchema.getGiveType().equals("1")) {
                                if (tLLClaimDetailSchema.getGiveReasonDesc() != null) {
                                    mRemarkDesc += strLine[0] + "责任" +
                                            tLLClaimDetailSchema.getGiveTypeDesc() +
                                            "原因:"
                                            + tLLClaimDetailSchema.getGiveReasonDesc() +
                                            "\n"+"备注框:"+"\n";
                                }
                            }
                        }
                        
                        //对于万能险种，备注信息不同
                        if (LLCaseCommon.chenkWN(tLLClaimDetailSchema.getRiskCode())) {
                            String tWNAccSQL = "select elementname,elementvalue from "
                                               +
                                    " LLElementDetail a,ldcode1 b where b.codetype='ClaimCalParam' "
                                               +
                                    " and a.calcode=b.code and b.othersign='1' "
                                               + " and a.elementcode=b.code1 and caseno = '" +
                                               tLLClaimDetailSchema.getCaseNo()
                                               + "' and rgtno = '" +
                                               tLLClaimDetailSchema.getRgtNo()
                                               + "' and caserelano = '" +
                                               tLLClaimDetailSchema.getCaseRelaNo()
                                               + "' and polno = '" +
                                               tLLClaimDetailSchema.getPolNo()
                                               + "' and dutycode = '" +
                                               tLLClaimDetailSchema.getDutyCode()
                                               + "' and getdutycode = '" +
                                               tLLClaimDetailSchema.getGetDutyCode()
                                               + "' and getdutykind = '" +
                                               tLLClaimDetailSchema.getGetDutyKind()
                                               + "' with ur";
                            SSRS tWNSSRS = tExeSQL.execSQL(tWNAccSQL);
                            for (int m = 1; m <= tWNSSRS.getMaxRow(); m++) {
                                if (Double.parseDouble(tWNSSRS.GetText(m, 2)) > 0) {
                                    mRemarkDesc += strLine[0] + "责任给付" +
                                            tLLClaimDetailSchema.getRealPay()
                                            + "元";
                                    break;
                                }
                            }

                            for (int n = 1; n <= tWNSSRS.getMaxRow(); n++) {
                                if (n == 1) {
                                    mRemarkDesc += ",其中";
                                }
                                if (Double.parseDouble(tWNSSRS.GetText(n, 2)) > 0 &&
                                    n != tWNSSRS.getMaxRow()) {
                                    mRemarkDesc += tWNSSRS.GetText(n, 1) +
                                            tWNSSRS.GetText(n, 2) + "元";
                                }
                                if (n == tWNSSRS.getMaxRow()) {
                                    mRemarkDesc += tWNSSRS.GetText(n, 1) +
                                            tWNSSRS.GetText(n, 2) + "元。\n";
                                }
                            }
                        }
                        mClaimInfo.add(strLine);//将理赔细目的信息整合到ClaimInfo                                                                   
                    }
                    //存放小计
                    strLine0[6] = Double.toString(AllRealPay);
                    mRiskInfo.add(strLine0);//将险种的信息整合到RiskInfo
                    
                } else {
                    strLine = new String[18];
                    strLine[1] = "";
                    strLine[2] = "0.00";
                    strLine[3] = "";
                    strLine[4] = "";
                    strLine[5] = "";
                    strLine[6] = "";
                    strLine[7] = "";
                    strLine[8] = "";
                    strLine[9] = "";
                    strLine[10] = "0.0";
                    strLine[11] = "";
                    strLine[12] = "";
                    strLine[13] = "";
                    strLine[14] = "0.00";
                    strLine[15] = "";
                    strLine[16] = "";
                    strLine[17] = "ClaimInfo"+k;

                    String tFeeMainSQL =
                            "select coalesce(sum(sumfee),0) from llfeemain where caseno='" +
                            mCaseNo + "'";
                    strLine[2] = tExeSQL.getOneValue(tFeeMainSQL);
                    strLine[14] = strLine[2];

                    String tRealPaySQL =
                            "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
                            tOrigCaseNo + "')";
                    AllRealPay = Double.parseDouble(tExeSQL.getOneValue(
                            tRealPaySQL));

                    String tAppealSQL = "SELECT b.appealno,coalesce(sum(c.realpay),0) FROM llappeal a,llappeal b,llclaim c "
                                        +
                            "WHERE a.caseno=b.caseno AND b.appealno=c.caseno AND a.appealno='"
                                        + mCaseNo + "' AND b.appealno<>'" + mCaseNo +
                                        "' group by b.appealno WITH UR";
                    SSRS tAppSSRS = tExeSQL.execSQL(tAppealSQL);
                    for (int j = 1; j <= tAppSSRS.getMaxRow(); j++) {
                        AllRealPay += Double.parseDouble(tAppSSRS.GetText(j, 2));
                    }
                    tRealPaySQL =
                            "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
                            mCaseNo + "')";

                    double tRealPay = Double.parseDouble(tExeSQL.getOneValue(
                            tRealPaySQL));
                    AllRealPay += tRealPay;

                    DecimalFormat tDF = new DecimalFormat("0.##");
                    String tRturn = tDF.format(AllRealPay);
                    strLine[10] = tRturn;
                     
                    mClaimInfo.add(strLine);//将理赔细目的信息整合到ClaimInfo
                    //存放小计
                    strLine0[6] = Double.toString(AllRealPay);
                    mRiskInfo.add(strLine0);//将险种的信息整合到RiskInfo
                }
                
                                
                //药品明细
                LLCaseDrugDB tLLCaseDrugDB = new LLCaseDrugDB();
                LLCaseDrugSet tLLCaseDrugSet = new LLCaseDrugSet();
                String tCaseDrugSQL = "select * from llcasedrug where caseno='"
                                      + mCaseNo +"' and inputtype is null";
                tLLCaseDrugSet = tLLCaseDrugDB.executeQuery(tCaseDrugSQL);
                String drugdesc = "";
                if (tLLCaseDrugSet.size() > 0 ) {
                    drugdesc = "\n扣除费用：\n";
                    for (int l = 1; l <= tLLFeeMainSet.size(); l++) {
                        tLLCaseDrugSet = new LLCaseDrugSet();
                        tLLCaseDrugDB.setCaseNo(mCaseNo);
                        tLLCaseDrugDB.setMainFeeNo(tLLFeeMainSet.get(l).getMainFeeNo());
                        tLLCaseDrugSet = tLLCaseDrugDB.query();
                        if (tLLCaseDrugSet.size() > 0) {
                            drugdesc += tLLFeeMainSet.get(l).getFeeDate() + "的"
                                    + tLLFeeMainSet.get(l).getReceiptNo() + "号账单：\n";
                            for (int kk = 1; kk <= tLLCaseDrugSet.size(); kk++) {
                                LLCaseDrugSchema tLLCaseDrugSchema = new
                                        LLCaseDrugSchema();
                                tLLCaseDrugSchema = tLLCaseDrugSet.get(kk);
                                drugdesc += "    " + tLLCaseDrugSchema.getDrugName();
                                if (tLLCaseDrugSchema.getSecuFee() >= 0.01) {
                                    drugdesc += " 扣除 " + tLLCaseDrugSchema.getSecuFee() +
                                            "元，属于统筹内金额 ";
                                }
                                
                                if (tLLCaseDrugSchema.getSelfPay2() >= 0.01) {
                                    drugdesc += " 扣除 " + tLLCaseDrugSchema.getSelfPay2() +
                                            "元，属于部分自付 ";
                                }

                                if (tLLCaseDrugSchema.getSelfFee() >= 0.01) {
                                    drugdesc += " 扣除 " + tLLCaseDrugSchema.getSelfFee() +
                                            "元，属于自费 ";
                                }

                                if (tLLCaseDrugSchema.getUnReasonableFee() >= 0.01) {
                                	 String tREMARK = ""+tLLCaseDrugSchema.getRemark();
                                     if(tLLCaseDrugSchema.getRemark()==null){
                                     	tREMARK = "";
                                     }
                                     
                                     drugdesc += "  扣除 " +
                                             tLLCaseDrugSchema.getUnReasonableFee() +
                                             "元 属于 不合理费用,原因: " + tREMARK;

                                }

                                drugdesc += "\n";
                            }
                        }
                    }
                }
                //对广东的多事件申诉纠错，调整备注信息，调整合计金额
                if (tAppealFlag) {
                	mAllRealPay = AllRealPay;
                	
                	String tRealPaySQL =
	                        "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
	                        tOrigCaseNo + "')";
	                double tRealPay1 = Double.parseDouble(tExeSQL.getOneValue(
	                        tRealPaySQL));
	
	                mRemarkDesc = "特别提示栏：\n本次给付是对案件号为" + tOrigCaseNo
	                              + "的补充给付。案件号为" + tOrigCaseNo +
	                              "的给付金额为" + Arith.round(tRealPay1, 2) + "元。";
	
	                String tAppealSQL = "SELECT b.appealno,coalesce(sum(c.realpay),0) FROM llappeal a,llappeal b,llclaim c "
	                                    +
	                        "WHERE a.caseno=b.caseno AND b.appealno=c.caseno AND a.appealno='"
	                                    + mCaseNo + "' AND b.appealno<>'" + mCaseNo +
	                                    "' group by b.appealno WITH UR";
	                SSRS tAppSSRS = tExeSQL.execSQL(tAppealSQL);
	                for (int j = 1; j <= tAppSSRS.getMaxRow(); j++) {
	                	tRealPay1 += Double.parseDouble(tAppSSRS.GetText(j, 2));
	                    mRemarkDesc += "已纠错案件" + tAppSSRS.GetText(j, 1) + ",给付金额" +
	                            tAppSSRS.GetText(j, 2) + "元。";
	                }
	                tRealPaySQL =
	                        "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
	                        mCaseNo + "')";
	
	                double tRealPay2 = Double.parseDouble(tExeSQL.getOneValue(
	                        tRealPaySQL));
	                tRealPay1 += tRealPay2;
	
	                DecimalFormat tDF = new DecimalFormat("0.##");
	                String tRturn = tDF.format(tRealPay1);
	                
	                //存放备注
	                mRemarkDesc += "本次给付" + tRealPay2 + "元，合计实付" + tRturn + "元。";    
               	
                }else{
                	//金额汇总
                    mAllRealPay += AllRealPay;//对每个事件的实赔金额进行汇总
                }               
                
//              #2162 批次导入的“扣除明细”内容，请在“理赔给付细目表”中增加显示
                if("".equals(drugdesc) && !"".equals(mNewRemark)){
                	drugdesc = "\n扣除费用：\n" + mNewRemark + "\n";
                }else if(!"".equals(drugdesc) && !"".equals(mNewRemark)){
                	drugdesc += mNewRemark + "\n";
                }
                mRemarkDesc += drugdesc;
                
                //此处需要对备注信息制作增量模式
                String[] tRemarkDesc = new String[2];
                tRemarkDesc[0] = mRemark;               
                tRemarkDesc[1] = mRemarkDesc;
                //判断备注信息中是否有重复的记录，若有，只显示一次,针对申述纠错案件

                if(k==1){
                    //初始化全局变量
                    mRemark = "";
                    mRemarkDesc = "";
                    mRemarkInfo.add(tRemarkDesc);
                }else{
                    if(tAppealFlag){
                        //初始化全局变量
                        mRemark = "";
                        mRemarkDesc = "";
                    }else{
                        //初始化全局变量
                        mRemark = "";
                        mRemarkDesc = "";
                        mRemarkInfo.add(tRemarkDesc);
                    }
                }               
            }       	
        }

        //年月日
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";

        TextTag tTextTag = new TextTag();
        tTextTag.add("JetFormType", "ClaimDetailPrintExcelGDBL");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTextTag.add("ManageComLength4", printcode);
        tTextTag.add("BarCode1", mCaseNo);
        tTextTag.add("BarCodeParam1"
                     , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        tTextTag.add("CustomerName", mCustomerName); //被保人姓名
        tTextTag.add("CustomerIDNo", mCustomerIDNo);//客户证件号码
        tTextTag.add("Age", Age);//年龄
        tTextTag.add("InsuredStat", insuredstat);//人员类别
        tTextTag.add("StartDate", FeeDate);
        tTextTag.add("SubDate", SubDate);//事件时间
        tTextTag.add("CompName", mGrpName);
        tTextTag.add("WorkPlace", mRgtantAddress);
        tTextTag.add("TitleDays", titledays);
        tTextTag.add("TInpatientDate", titleInpatient);
        tTextTag.add("InpatientDate", InpatientDate);//就诊时间
        tTextTag.add("RealDays", days);
        tTextTag.add("HospitName", hospitalname);
        tTextTag.add("HospitGrade", hosgrade);
        tTextTag.add("RgtDate", mRgtDate);
        tTextTag.add("FeeAtti", feeatti);//医院属性
        tTextTag.add("Remark", mRemark);
        tTextTag.add("RemarkDesc", mRemarkDesc);

        tTextTag.add("XI_ContNo", "");
        tTextTag.add("XI_ManageCom", MngCom);

        System.out.println(mCustomerName);
        tTextTag.add("CaseNo", mCaseNo); //理赔号

        mAllRealPay = Double.parseDouble(new DecimalFormat("0.00").format(
                mAllRealPay));
        tTextTag.add("AllCountMoney",
                     String.valueOf(PubFun.getChnMoney(mAllRealPay)));//整个通知书总金额-中文
        tTextTag.add("AllRealMoney", new DecimalFormat("0.00").format(mAllRealPay));//整个通知书总金额-数字
        tTextTag.add("Today", SysDate);
        
        mAllRealmoneyCN = String.valueOf(PubFun.getChnMoney(mAllRealPay));
        mAllRealmoneyMA = new DecimalFormat("0.00").format(mAllRealPay);
        
        //业务员相关信息
        String[] Title = {"AgentName", "AgentCode", "AgentGroup"};//业务员信息
        System.out.println(Title);
        String tAgentSQL = "SELECT DISTINCT b.name,getUniteCode(a.agentcode),c.name "
                           + " FROM llclaimdetail a,laagent b,labranchgroup c "
                           +
                           " WHERE a.agentcode=b.agentcode AND b.agentgroup=c.agentgroup "
                           + " AND a.caseno='" + mCaseNo +
                           "' WITH UR";

        SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
        if (tAgentSSRS.getMaxRow() <= 0) {
        	mAgentName = "";
            mAgentCode = "";
            mAgentGroup = "";
        }
        for (int j = 1; j <= tAgentSSRS.getMaxRow(); j++) {
        	mAgentName = tAgentSSRS.GetText(j, 1);
        	mAgentCode = tAgentSSRS.GetText(j, 2);
        	mAgentGroup = tAgentSSRS.GetText(j, 3);
        }
        tTextTag.add("AgentName", mAgentName);
        tTextTag.add("AgentCode", mAgentCode);
        tTextTag.add("AgentGroup", mAgentGroup);
        
        String tGrpNameSQL =
            "SELECT DISTINCT appntname FROM llclaimpolicy WHERE caseno='" +
            mCaseNo + "'AND grpcontno<>'00000000000000000000' WITH UR";
	    ExeSQL tExeSQL = new ExeSQL();
	    SSRS tSSRS = tExeSQL.execSQL(tGrpNameSQL);
	    String tGrpName = "";
	    if (tSSRS.getMaxRow() > 0) {
	        tGrpName = tSSRS.GetText(1, 1);
	        if("".equals(tGrpName)||"null".equals(tGrpName)||tGrpName==null){
            	tGrpName="";
            }
	    }
	    mGrpName = tGrpName;//这个才是实际的团体单位名称
        tTextTag.add("GrpName", tGrpName);
        
        //#1077 
        //add by GY 2013-1-24
        //1.查询案件号llclaimdetail表的contno字段
        String CaseNoSQL="select contno from llclaimdetail where caseno='"+mCaseNo+"'";
        String ContNo=tExeSQL.getOneValue(CaseNoSQL);
        //2.查询contno是否为卡折
        String CheckCardSQL="(select 1 from lcgrpcont where grpcontno = " +
        		"(select grpcontno from lccont where contno = '"+ContNo+"') and cardflag = '2')";
        String CheckCard=tExeSQL.getOneValue(CheckCardSQL);
        if(CheckCard.equals("1")){
            tTextTag.add("CustomerName", mCustomerName);
        }else{
            tTextTag.add("GrpName", tGrpName);
        }
        
        if (tTextTag.size() > 0) {
            xml.addTextTag(tTextTag);
        }

        xml.outputDocumentToFile("d:\\", "ClaimDetailPrintExcelGD");
        mResult.clear();
        mResult.addElement(xml);
        System.out.println("传输结束");
        
        //之前的xml生成保留，现在开始采用写数据的方式进行Excel生成
        if(!parseDataToExcel()){
        	return false;
        }        
        return true;
    }

    /**
     * 将整理好的全局变量转化为Excel
     * @return
     */
    private boolean parseDataToExcel() {
		int tCount=0;//循环部分的总行数，用于确定其在Excel中显示的位置
		tCount = mRiskInfo.size() * mClaimInfo.size() + mRemarkInfo.size();
		System.out.println("循环变量部分的大小："+tCount);
		
		Calendar calendar = Calendar.getInstance();//可以对每个时间域单独修改
		int tYear = calendar.get(Calendar.YEAR);
		int tMonth = calendar.get(Calendar.MONTH);
		System.out.println("统计时间" + tYear + "-" + tMonth);
		
		//创建对象
		String[][] tToExcel = new String[tCount + 250][13];
		
		tToExcel[0][0] = "理赔案件号："+mCaseNo;
		
		tToExcel[1][0] = "理赔给付细目表";
		
		tToExcel[2][0] = "被保险人";
		tToExcel[2][3] = mCustomerName;
		tToExcel[2][6] = "证件号码";
		tToExcel[2][9] = mCustomerIDNo;
		
		tToExcel[3][0] = "年龄";
		tToExcel[3][3] = mAge;
		tToExcel[3][6] = "人员类别";
		tToExcel[3][9] = mInsuredstat;
		
		//现将表头部分整理，接下来开始循环部分
		int RiskInfoRow = 4;//RiskInfo循环开始的Excel行
		for (int row = 0; row < mRiskInfo.size(); row++) {
			RiskInfoRow++;
			String [] tRiskInfo = (String [])mRiskInfo.get(row);
			
			tToExcel[RiskInfoRow][0] = "保单号为"+tRiskInfo[0]+"险种"+tRiskInfo[1]+"的理赔如下：";
			
			tToExcel[RiskInfoRow+1][0] = "事件时间：";
			tToExcel[RiskInfoRow+1][1] = tRiskInfo[2];
			tToExcel[RiskInfoRow+1][4] = "连续天数：";
			tToExcel[RiskInfoRow+1][5] = tRiskInfo[3];
			tToExcel[RiskInfoRow+1][8] = "就诊医院：";
			tToExcel[RiskInfoRow+1][9] = tRiskInfo[4];
			
			int claimInfoRow = RiskInfoRow+2;//claimInfo循环开始的Excel行
			tToExcel[claimInfoRow][0] = "险种序号";
			tToExcel[claimInfoRow][1] = "责任项目";
			tToExcel[claimInfoRow][2] = "账单日期";
			tToExcel[claimInfoRow][3] = "账单金额";
			tToExcel[claimInfoRow][4] = "社保内";
			tToExcel[claimInfoRow][5] = "社保外";
			tToExcel[claimInfoRow][6] = "已报销";
			tToExcel[claimInfoRow][7] = "不合理费用";
			tToExcel[claimInfoRow][8] = "理算金额";
			tToExcel[claimInfoRow][9] = "免赔";
			tToExcel[claimInfoRow][10] = "给付比例";
			tToExcel[claimInfoRow][11] = "实赔金额";
			tToExcel[claimInfoRow][12] = "备注";	
			for(int i=0;i<mClaimInfo.size();i++){
				String [] tClaimInfo = (String [])mClaimInfo.get(i);				
				if(tRiskInfo[5].equals(tClaimInfo[17])){//将RiskInfo和ClaimInfo关联
					claimInfoRow++;
					tToExcel[claimInfoRow][0] = tClaimInfo[7];
					tToExcel[claimInfoRow][1] = tClaimInfo[0];
					tToExcel[claimInfoRow][2] = tClaimInfo[1];
					tToExcel[claimInfoRow][3] = tClaimInfo[2];
					tToExcel[claimInfoRow][4] = tClaimInfo[16];
					tToExcel[claimInfoRow][5] = tClaimInfo[13];
					tToExcel[claimInfoRow][6] = tClaimInfo[3];
					tToExcel[claimInfoRow][7] = tClaimInfo[5];
					tToExcel[claimInfoRow][8] = tClaimInfo[9];
					tToExcel[claimInfoRow][9] = tClaimInfo[8];
					tToExcel[claimInfoRow][10] = tClaimInfo[12];
					tToExcel[claimInfoRow][11] = tClaimInfo[10];
					tToExcel[claimInfoRow][12] = tClaimInfo[11];
				}						
			}
			//小计
			tToExcel[claimInfoRow+1][0] = "小计：";
			tToExcel[claimInfoRow+1][10] = tRiskInfo[6];
			RiskInfoRow = claimInfoRow+1;
		}
		System.out.println("执行完成循环后，目前Excel行数："+RiskInfoRow+1);
		//循环结束，空一行，进行后续内容填充
		int tEndOfRow = RiskInfoRow+1;
		tToExcel[tEndOfRow][0] = "本次理赔申请实赔保险金总计金额人民币 "+mAllRealmoneyCN+"（￥"+mAllRealmoneyMA+"）";
		
		tToExcel[tEndOfRow+2][0] = "备注信息：";	
		for(int i=0;i<mRemarkInfo.size();i++){
			String [] tRemarkInfo = (String [])mRemarkInfo.get(i);
			tToExcel[tEndOfRow+3+i][0]="";//对单元格初始化，避免null
			tToExcel[tEndOfRow+3+i][0]+=tRemarkInfo[0]+"\n"+tRemarkInfo[1];	
			//System.out.println(tRemarkInfo[0].toString());
			System.out.println(tToExcel[tEndOfRow+3+i][0].toString());
		}
		
		tEndOfRow = tEndOfRow+3+mRemarkInfo.size();
		System.out.println("备注信息循环结束行："+tEndOfRow);
		
		tToExcel[tEndOfRow+15][0] = "投保单位：";
		tToExcel[tEndOfRow+15][1] = mGrpName;
		tToExcel[tEndOfRow+15][3] = "业务员姓名：";
		tToExcel[tEndOfRow+15][4] = mAgentName;
		tToExcel[tEndOfRow+15][6] = "业务员代码：";
		tToExcel[tEndOfRow+15][7] = mAgentCode;
		tToExcel[tEndOfRow+15][9] = "业务员所属部门：";
		tToExcel[tEndOfRow+15][10] = mAgentGroup;
		
		System.out.println("最后一行："+(tEndOfRow+15));
		try {
			String mOutXmlPath = CommonBL.getUIRoot();
			//调试用
			//mOutXmlPath = "F:\\picch\\ui\\";
			mOutXmlPath += "vtsfile/";
			
			System.out.println(mOutXmlPath);
			String name = "LP"+mGlobalInput.Operator+mCaseNo+".xls";
			System.out.println("字符变更前名称："+name);
			String tAllName = mOutXmlPath + name;
			System.out.println("文件名称====："+tAllName);
			
			WriteToExcel t = new WriteToExcel(name);
			t.createExcelFile();
			t.setAlignment("CENTER");
			t.setFontName("宋体");
			String[] sheetName = { new String("Data") };
			t.addSheet(sheetName);
			t.setData(0, tToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
			
			//文件正常生成后，可以对每一行设置单独的样式，1）首先尝试在生成时添加样式，2）还是获取文件后修改吧
			//获取文件设置格式
			FileInputStream is = new FileInputStream(new File(mOutXmlPath + name)); //获取文件      
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0); //获取sheet
			
			//设置字体-标题
			HSSFFont tTitleFont = wb.createFont();
			tTitleFont.setFontName("宋体");
			tTitleFont.setFontHeightInPoints((short) 22);//设置字体大小
			
			//设置字体-正文内容
			HSSFFont tFont = wb.createFont();
			tFont.setFontName("宋体");
			tFont.setFontHeightInPoints((short) 10);//设置字体大小
			
			//设置字体-表格内容
			HSSFFont tTableFont = wb.createFont();
			tTableFont.setFontName("宋体");
			tTableFont.setFontHeightInPoints((short) 12);//设置字体大小
			
			//设置字体-加粗
			HSSFFont tBoldFont = wb.createFont();
			tBoldFont.setFontName("宋体");
			tBoldFont.setFontHeightInPoints((short) 10);//设置字体大小
			tBoldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//加粗
			
			//设置字体-备注
			HSSFFont tRemarkFont = wb.createFont();
			tRemarkFont.setFontName("宋体");
			tRemarkFont.setFontHeightInPoints((short) 8);//设置字体大小
			
			//设置样式-标题
			HSSFCellStyle tTitleStyle = wb.createCellStyle();
			tTitleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tTitleStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);//背景色
			tTitleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);//填充单元格
			tTitleStyle.setFont(tTitleFont);
			
			//设置样式-样式加深
			HSSFCellStyle tDeepStyle = wb.createCellStyle();
			tDeepStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 左对齐
			tDeepStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);//背景色
			tDeepStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);//填充单元格
			tDeepStyle.setFont(tBoldFont);
			tDeepStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			tDeepStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tDeepStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tDeepStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			
			//设置样式-右对齐
			HSSFCellStyle rightStyle = wb.createCellStyle();
			rightStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT); // 右对齐
			rightStyle.setFont(tFont);
			
			//设置样式-左对齐-加粗
			HSSFCellStyle leftStyle = wb.createCellStyle();
			leftStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 左对齐
			leftStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);//表格线-实线
			leftStyle.setFont(tBoldFont);
			
			//设置样式-左对齐
			HSSFCellStyle leftStyleN = wb.createCellStyle();
			leftStyleN.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 左对齐
			leftStyleN.setBorderBottom(HSSFCellStyle.BORDER_THIN);//表格线-实线
			leftStyleN.setFont(tFont);
			
			//设置样式-表格
			HSSFCellStyle tableStyle = wb.createCellStyle();
			tableStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
			tableStyle.setFont(tTableFont);
			tableStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);//表格线-实线
			tableStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			tableStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			tableStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			
			//设置样式-备注详细
			HSSFCellStyle tRemarkStyle = wb.createCellStyle();
			tRemarkStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 左对齐
			tRemarkStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_JUSTIFY); //垂直方向-顶端对其      
			tRemarkStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);//表格线-实线
			tRemarkStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			tRemarkStyle.setWrapText(true);//自动换行
			tRemarkStyle.setFont(tRemarkFont);
			
			//设置样式-业务员
			HSSFCellStyle tAgentStyle = wb.createCellStyle();
			tAgentStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 左对齐
			tAgentStyle.setFont(tRemarkFont);
			
			//设置样式-格式线
			HSSFCellStyle tLineStyle = wb.createCellStyle();
			tLineStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);//表格线-实线
			
			//为第三列“账单日期”设置宽一些的格式
			sheet.setColumnWidth((short) 2, (short) (256*12));//256为列的像素数
			
			//合并单元格-共12列
			//Region((short)rowFrom,(short)columnFrom,(short)rowTo,(short)columnTo); 
			sheet.addMergedRegion(new Region(0, (short) 0, 0, (short) 12));
			sheet.addMergedRegion(new Region(1, (short) 0, 1, (short) 12));
			sheet.addMergedRegion(new Region(2, (short) 0, 2, (short) 2));
			sheet.addMergedRegion(new Region(2, (short) 3, 2, (short) 5));
			sheet.addMergedRegion(new Region(2, (short) 6, 2, (short) 8));
			sheet.addMergedRegion(new Region(2, (short) 9, 2, (short) 12));
			sheet.addMergedRegion(new Region(3, (short) 0, 3, (short) 2));
			sheet.addMergedRegion(new Region(3, (short) 3, 3, (short) 5));
			sheet.addMergedRegion(new Region(3, (short) 6, 3, (short) 8));
			sheet.addMergedRegion(new Region(3, (short) 9, 3, (short) 12));
			//循环变量处的合并单元格
			//sheet.addMergedRegion(new Region(tDisUserMouCount + 8, (short) 0, tDisUserMouCount + 8, (short) 11));

			
			
			//为第一行设置样式
			HSSFRow tRow0 = sheet.getRow((short) 0);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRow0.getCell((short) i);
				tRowCell.setCellStyle(rightStyle);
			}
			//为第二行设置样式
			HSSFRow tRow1 = sheet.getRow((short) 1);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRow1.getCell((short) i);
				tRowCell.setCellStyle(tTitleStyle);
			}
			//为第3、4行设置表格
			HSSFRow tRow2 = sheet.getRow((short) 2);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRow2.getCell((short) i);
				tRowCell.setCellStyle(tableStyle);
			}
			HSSFRow tRow3 = sheet.getRow((short) 3);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRow3.getCell((short) i);
				tRowCell.setCellStyle(tableStyle);
			}
			//循环变量处的格式设置，整个设置中最复杂的部分
			int RiskStyleRow = 4;//RiskInfo循环开始的Excel行
			//空白行合并单元格
			sheet.addMergedRegion(new Region(4, (short) 0, 4, (short) 12));
			
			for (int row = 0; row < mRiskInfo.size(); row++) {
				String [] tRiskInfo = (String [])mRiskInfo.get(row);
				RiskStyleRow++;
				//合并单元格-险种
				sheet.addMergedRegion(new Region(RiskStyleRow, (short) 0, RiskStyleRow, (short) 12));
				//样式-险种
				HSSFRow tRiskRow = sheet.getRow((short) RiskStyleRow);
				for(int i=0;i<13;i++){
					HSSFCell tRowCell = tRiskRow.getCell((short) i);
					tRowCell.setCellStyle(leftStyle);
				}
				//样式-日期
				HSSFRow tDateRow = sheet.getRow((short) RiskStyleRow+1);
				for(int i=0;i<13;i++){
					HSSFCell tRowCell = tDateRow.getCell((short) i);
					tRowCell.setCellStyle(leftStyleN);
				}
				
				int claimStyleRow = RiskStyleRow+2;//claimInfo循环开始的Excel行
				for(int i=0;i<mClaimInfo.size();i++){
					String [] tClaimInfo = (String [])mClaimInfo.get(i);				
					if(tRiskInfo[5].equals(tClaimInfo[17])){//将RiskInfo和ClaimInfo关联
						//样式-表头
						HSSFRow tHeadRow = sheet.getRow((short) claimStyleRow);
						for(int j=0;j<13;j++){
							HSSFCell tRowCell = tHeadRow.getCell((short) j);
							tRowCell.setCellStyle(leftStyleN);
						}
						claimStyleRow++;	
						//样式-责任
						HSSFRow tClaimRow = sheet.getRow((short) claimStyleRow);
						for(int j=0;j<13;j++){
							HSSFCell tRowCell = tClaimRow.getCell((short) j);
							tRowCell.setCellStyle(tLineStyle);
						}
					}							
				}
				//小计
				sheet.addMergedRegion(
						new Region(claimStyleRow+1, 
						(short) 0, 
						claimStyleRow+1, 
						(short) 9));
				sheet.addMergedRegion(
						new Region(claimStyleRow+1, 
						(short) 10, 
						claimStyleRow+1, 
						(short) 12));
				//样式-小计
				HSSFRow tTotalRow = sheet.getRow((short) claimStyleRow+1);
				for(int m=0;m<13;m++){
					HSSFCell tRowCell = tTotalRow.getCell((short) m);
					tRowCell.setCellStyle(leftStyle);
				}
				RiskStyleRow = claimStyleRow+1;
				
				//空白行合并单元格
				//sheet.addMergedRegion(new Region(RiskStyleRow+1, (short) 0, RiskStyleRow+1, (short) 12));
			}
			int tEndOfStyleRow = RiskInfoRow+1;
			//总金额-合并单元格
			sheet.addMergedRegion(new Region(tEndOfStyleRow, (short) 0, tEndOfStyleRow, (short) 12));
			//样式
			HSSFRow tRowTotal = sheet.getRow((short) tEndOfStyleRow);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRowTotal.getCell((short) i);
				tRowCell.setCellStyle(tDeepStyle);
			}
			//空白行合并单元格
			sheet.addMergedRegion(new Region(tEndOfStyleRow+1, (short) 0, tEndOfStyleRow+1, (short) 12));
			
			//备注-合并单元格
			sheet.addMergedRegion(new Region(tEndOfStyleRow+2, (short) 0, tEndOfStyleRow+2, (short) 12));
			//样式
			HSSFRow tRowDesc = sheet.getRow((short) tEndOfStyleRow+2);
			for(int i=0;i<13;i++){
				HSSFCell tRowCell = tRowDesc.getCell((short) i);
				tRowCell.setCellStyle(leftStyle);
			}
			
			//备注详细-合并单元格
			int EndStyleRow = tEndOfStyleRow+2;
			//空白行合并单元格
			sheet.addMergedRegion(new Region(EndStyleRow, (short) 0, EndStyleRow, (short) 12));
			for(int i=0;i<mRemarkInfo.size();i++){
				EndStyleRow++;
				sheet.addMergedRegion(new Region(EndStyleRow, (short) 0, EndStyleRow, (short) 12));    
				//样式
				HSSFRow tRowRemarkDesc = sheet.getRow((short) EndStyleRow);
				tRowRemarkDesc.setHeight((short)(256*10));//256表示行高为一个像素
				for(int j=0;j<13;j++){
					HSSFCell tRowCell = tRowRemarkDesc.getCell((short) j);
					tRowCell.setCellStyle(tRemarkStyle);  
				}
			}
			//空白行合并单元格
			sheet.addMergedRegion(new Region(EndStyleRow+1, (short) 0, EndStyleRow+1, (short) 12));
			//业务员信息-样式
			HSSFRow tRowRemarkDesc = sheet.getRow((short) EndStyleRow+16);
			for(int j=0;j<13;j++){
				HSSFCell tRowCell = tRowRemarkDesc.getCell((short) j);
				tRowCell.setCellStyle(tAgentStyle);
			}
			
			
			FileOutputStream fileOut = new FileOutputStream(tAllName);
			wb.write(fileOut);
			fileOut.close();
			mFilePath = tAllName;
			
		}catch (Exception ex) {
			this.mErrors.addOneError(new CError("广东给付细目表打印失败",
					"ClaimDetailPrintExcelGDBL", "parseDataToExcel"));
			ex.toString();
			ex.printStackTrace();
			return false;
		}
	
			return true;
	}

	private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput",0));
        System.out.println("处理全局变量结束");
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mCaseNo  = (String) mTransferData.getValueByName("caseno");// 理赔号   
        
        if (mCaseNo == null || mCaseNo.equals("")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClaimDetailPrintExcelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入理赔号！";
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public String getGetDutyName(String strGetDutyCode) {
        LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
        tLMDutyGetDB.setGetDutyCode(strGetDutyCode);
        if (!tLMDutyGetDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMDutyGetDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimDetailPrintBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "给付责任名称查询失败!";
            this.mErrors.addOneError(tError);

            return null;
        }
        return tLMDutyGetDB.getGetDutyName();
    }

    public String trunGiveType(String GiveType) {
        String reGiveType = "";
        if ("1".equals(GiveType)) {
            reGiveType = "A";
            if (mremarktag[0] < 1) {
            	mRemark += "A、正常给付";
            }
            mremarktag[0] = 1;
        }
        if ("2".equals(GiveType)) {
            reGiveType = "B";
            if (mremarktag[1] < 1) {
            	mRemark += "B、部分给付";
            }
            mremarktag[1] = 1;
        }
        if ("3".equals(GiveType)) {
            reGiveType = "C";
            if (mremarktag[2] < 1) {
            	mRemark += "C、全额拒付";
            }
            mremarktag[2] = 1;
        }
        if ("5".equals(GiveType)) {
            reGiveType = "D";
            if (mremarktag[3] < 1) {
            	mRemark += "D、通融给付";
            }
            mremarktag[3] = 1;
        }
        if ("4".equals(GiveType)) {
            reGiveType = "E";
            if (mremarktag[4] < 1) {
            	mRemark += "E、协议给付";
            }
            mremarktag[4] = 1;
        }

        return reGiveType;
    }

    public double getselfamnt(String getdutyname) {
        System.out.println("&&&&&" + getdutyname);
        double amnt = 0.0;
        if (getdutyname.equals("低段保险金")) {
            amnt = lowamnt;
        }
        if (getdutyname.equals("中段保险金")) {
            amnt = midamnt;
        }
        if (getdutyname.equals("高段保险金1")) {
            amnt = highamnt1;
        }
        if (getdutyname.equals("高段保险金2")) {
            amnt = highamnt2;
        }
        if (getdutyname.equals("超高段保险金")) {
            amnt = supinhosamnt;
        }
        if (getdutyname.equals("大额门急诊保险金")) {
            amnt = supdooramnt;
        }
        if (getdutyname.equals("小额门急诊保险金")) {
            amnt = smalldooramnt;
        }
        if (getdutyname.equals("门急诊保险金")) {
            amnt = emergdooramnt;
        }
        System.out.println("******" + amnt);
        amnt = Double.parseDouble(new DecimalFormat("0.00").format(amnt));
        return amnt;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(mCaseNo); //存放前台传来的caseno
        tLOPRTManagerSchema.setOtherNoType("5");
        tLOPRTManagerSchema.setCode("lp001");
        tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setAgentCode("");
        tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag1(mCaseRelaNo);

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    //添加一个列表，参数为ListTag和动态列表的表头数组
    /**
     * 将一个ListTable添加到另一个ListTable中，作为其子节点
     */
    public void addFatherAndChildListTable(ListTable fatherListTable, String[] fatherColvalue,
    		ListTable childListTable, String[] childColvalue)
    {
        int fatherCol = fatherColvalue.length;
        Element DataSetElement = xml.getDocument().getRootElement();
        Element fatherTable = new Element(fatherListTable.getName());
        DataSetElement.addContent(fatherTable);
        Element fatherHead = new Element("HEAD");
        fatherTable.addContent(fatherHead);
        //建立表头名
        for (int m = 0; m < fatherColvalue.length+1; m++)
        {
            int n = m + 1;
            String colnum = "COL" + n;
            
            if(m == fatherColvalue.length){//在父ListTable节点的头结点中添加子ListTable的头结点
                Element childTable = new Element(childListTable.getName());
                fatherHead.addContent(childTable);
                Element childHead = new Element("HEAD");
                childTable.addContent(childHead);
                for (int j = 0; j < childColvalue.length; j++)
                {
                    int k = j + 1;
                    String tColnum = "COL" + k;
                    childHead.addContent(new Element(tColnum).addContent(childColvalue[j]));
                }                
            }else{
            	fatherHead.addContent(new Element(colnum).addContent(fatherColvalue[m]));
            }           
        }

        //遍历父ListTable的fatherListTable,同时遍历子ListTable的childListTable
        int fatherTableSize = fatherListTable.size();
        for (int i = 0; i <= fatherTableSize - 1; i++)
        {
       	 	Element fatherRow = new Element("ROW");
       	 	fatherTable.addContent(fatherRow);
       	 	String[] fatherTempArray = new String[fatherCol];
       	 	fatherTempArray = fatherListTable.get(i);
       	 	for (int m = 0; m < fatherTempArray.length+1; m++)
       	 	{      	 		
       	 		int n = m + 1;
       	 		String colnum = "COL" + n;
       	 		
	       	 	if(m == fatherTempArray.length){//同时遍历子ListTable的childListTable
	            	int childListTableSize = childListTable.size();
	            	for (int j = 0; j <= childListTableSize - 1; j++)
	                {
	            		int childCol = childColvalue.length;
	            		Element childTable = new Element(childListTable.getName());
	            		fatherRow.addContent(childTable);
	            		String[] childTempArray = new String[childCol];
	            		childTempArray = childListTable.get(j);
	                    Element tRow = new Element("ROW");
	                    childTable.addContent(tRow);
	                    for (int k = 0; k < childTempArray.length; k++)
	                    {
	                        int o = k + 1;
	                        String tColnum = "COL" + o;
	                        tRow.addContent(new Element(tColnum).addContent(childTempArray[k]));
	                    }
	                }
	            }else{
	            	fatherRow.addContent(new Element(colnum).addContent(fatherTempArray[m]));	                 
	            }                  	 		           
       	 	}       	 	            
        }
    }
	
    public static void main(String[] args) {

        ClaimDetailPrintExcelGDBL tClaimDetailPrintBL = new ClaimDetailPrintExcelGDBL();

        VData tVData = new VData();

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        String caserelano = "86310000718458";
//        String caseno = "C3100131125000003";
        String caseno = "C4400140828000030";
//        String caseno = "C1100140718000003";
//        String caseno = "C4400140828000019";
//        String caseno = "R4400140829000001";
        TransferData PrintElement = new TransferData();
        PrintElement.setNameAndValue("caserelano", caserelano);
        PrintElement.setNameAndValue("caseno", caseno);
        tVData.add(tGlobalInput);
        tVData.add(PrintElement);
        tClaimDetailPrintBL.submitData(tVData, null);
    }

    private void jbInit() throws Exception {
    }

	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

}
