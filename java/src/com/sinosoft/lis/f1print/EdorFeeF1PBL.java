package com.sinosoft.lis.f1print;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJSGetEndorseDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LMEdorItemDB;
import com.sinosoft.lis.db.LPAppntDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.db.LPGrpAppntDB;
import com.sinosoft.lis.db.LPGrpEdorItemDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LMEdorItemSet;
import com.sinosoft.lis.vschema.LPAppntSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPEdorMainSet;
import com.sinosoft.lis.vschema.LPGrpAppntSet;
import com.sinosoft.lis.vschema.LPGrpEdorItemSet;
import com.sinosoft.lis.vschema.LPGrpEdorMainSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class EdorFeeF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();

    //打印相关变量
    private String tEdorAcceptNo = "";
    private String tEdorNo = "";
    private String tGetNoticeNo = "";
    private String tAppntNo = "";
    private String tManageCom = "";
    private String tManagePhone = "";
    private String tSumDuePayMoney = "";
    private String tAgentcode = "";
    private String tEdorAppDate = "";
    private String tEdorNames = "";
    private String tAppName = "";
    private String tContNo = "";
    private double tGetMoneyForPay = 0; //收费明细总和
    private double tGetMoneyForGet = 0; //退费明细总和
    private double tGetMoneyBF = 0;
    private double tGetMoneyLX = 0;
    private double tGetMoneyGB = 0;
    private double tGetMoneyTB = 0;


    public EdorFeeF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPEdorMainSchema.setSchema((LPEdorMainSchema) cInputData
                                    .getObjectByObjectName("LPEdorMainSchema",
                0));

        //GetNoticeNo = (String) cInputData.getObject(1);
        //AppntNo = (String) cInputData.getObject(2);
        //SumDuePayMoney = (String) cInputData.getObject(3);
        //Agentcode = (String) cInputData.getObject(4);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "EdorFeeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        tEdorNo = mLPEdorMainSchema.getEdorNo();
        String strSql = "select * from ljspay where otherno = '" + tEdorNo +
                        "'";
        LJSPayDB tPayDB = new LJSPayDB();
        LJSPaySet tPaySet = tPayDB.executeQuery(strSql);
        if (tPaySet != null && tPaySet.size() == 1)
        {
            tGetNoticeNo = tPaySet.get(1).getGetNoticeNo();
            tAppntNo = tPaySet.get(1).getAppntNo();
            tSumDuePayMoney = Double.toString(tPaySet.get(1).getSumDuePayMoney());
            tAgentcode = tPaySet.get(1).getAgentCode();
            tManageCom = tPaySet.get(1).getManageCom();
        }
        else
        {
            buildError("getPrintData", "LJSPay表中没有相关的记录！");
            return false;
        }
        //取得机构电话
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tManageCom);
        if (!tLDComDB.getInfo())
        {
            buildError("getPrintData", "LDCom表中没有相关的机构记录！");
        }
        else
        {
            tManagePhone = tLDComDB.getPhone();
        }

        //取得款项明细，收费包括:保险费BF，利息LX，工本费GB，退费包括：退保金TB
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setGetNoticeNo(tGetNoticeNo);
        tLJSGetEndorseDB.setEndorsementNo(tEdorNo);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
        for (int i = 0; i < tLJSGetEndorseSet.size(); i++)
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
            tLJSGetEndorseSchema = tLJSGetEndorseSet.get(i + 1);
            if ("BF".equals(tLJSGetEndorseSchema.getFeeFinaType()))
            {
                tGetMoneyBF += tLJSGetEndorseSchema.getGetMoney();
            }
            if ("LX".equals(tLJSGetEndorseSchema.getFeeFinaType()))
            {
                tGetMoneyLX += tLJSGetEndorseSchema.getGetMoney();
            }
            if ("GB".equals(tLJSGetEndorseSchema.getFeeFinaType()))
            {
                tGetMoneyGB += tLJSGetEndorseSchema.getGetMoney();
            }
            if ("TB".equals(tLJSGetEndorseSchema.getFeeFinaType()))
            {
                tGetMoneyTB += tLJSGetEndorseSchema.getGetMoney();
            }
        }
        tGetMoneyForPay = tGetMoneyBF + tGetMoneyLX + tGetMoneyGB;
        tGetMoneyForGet = tGetMoneyTB;

        //取得投保人姓名或单位名称
        String tEdorFlag = tEdorNo.substring(11, 13);

        //判断保全是团单还是个单，true--团单，false--个单
        boolean isGrp = true;
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        tLPGrpEdorMainDB.setEdorNo(tEdorNo);
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
        if ((tLPGrpEdorMainSet == null) || (tLPGrpEdorMainSet.size() == 0))
        {
            isGrp = false;
        }

        //取打印所需的值
        if (!isGrp)
        { //个单
            LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
            tLPEdorMainDB.setEdorNo(tEdorNo);
            LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
            if ((tLPEdorMainSet == null) || (tLPEdorMainSet.size() == 0))
            {
                buildError("getPrintData", "在取得LPEdorMain的数据时发生错误");
                return false;
            }

            LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(1);
            tEdorAppDate = tLPEdorMainSchema.getEdorAppDate();
            tContNo = tLPEdorMainSchema.getContNo();
            //取申请人名称，如果没有取投保人名称
            tAppName = tLPEdorMainSchema.getEdorAppName();
            if (tAppName == null || "".equals(tAppName))
            {
                LPAppntDB tLPAppntDB = new LPAppntDB();
                tLPAppntDB.setEdorNo(tEdorNo);
                tLPAppntDB.setContNo(tContNo);
                LPAppntSet tLPAppntSet = tLPAppntDB.query();

                if (tLPAppntSet.size() < 1)
                {
                    LCAppntDB tLCAppntDB = new LCAppntDB();
                    tLCAppntDB.setContNo(tContNo);
                    if (!tLCAppntDB.getInfo())
                    {
                        mErrors.copyAllErrors(tLCAppntDB.mErrors);
                        buildError("getPrintData", "在取得LCAppnt的数据时发生错误");

                        return false;
                    }
                    tAppName = tLCAppntDB.getAppntName();
                }
                else
                {
                    tAppName = tLPAppntSet.get(1).getAppntName();
                }
            }
            //取保全名称
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(tEdorNo);
            tLPEdorItemDB.setGrpContNo(tContNo);
            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            if ((tLPEdorItemSet == null) || (tLPEdorItemSet.size() == 0))
            {
                buildError("getPrintData", "在取得LPEdorItemSet的数据时发生错误");
                return false;
            }
            LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
            for (int i = 0; i < tLPEdorItemSet.size(); i++)
            {
                tLMEdorItemDB.setEdorCode(tLPEdorItemSet.get(i + 1).
                                          getEdorType());
                tLMEdorItemDB.setAppObj("I");
                LMEdorItemSet tLMEdorItemSet = tLMEdorItemDB.query();
                if (tLMEdorItemSet.size() > 0)
                {
                    tEdorNames += tLMEdorItemSet.get(i + 1).getEdorName() + ",";
                }
            }

        }
        else
        { //团单
            LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
            tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);
            tEdorAppDate = tLPGrpEdorMainSchema.getEdorAppDate();
            tContNo = tLPGrpEdorMainSchema.getGrpContNo();
            //取申请人名称，如果没有取投保人名称
            tAppName = tLPGrpEdorMainSchema.getEdorAppName();
            if (tAppName == null || "".equals(tAppName))
            {
                LPGrpAppntDB tLPGrpAppntDB = new LPGrpAppntDB();
                tLPGrpAppntDB.setEdorNo(tEdorNo);
                tLPGrpAppntDB.setGrpContNo(tContNo);
                LPGrpAppntSet tLPGrpAppntSet = tLPGrpAppntDB.query();

                if (tLPGrpAppntSet.size() < 1)
                {
                    LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
                    tLCGrpAppntDB.setGrpContNo(tContNo);
                    if (!tLCGrpAppntDB.getInfo())
                    {
                        mErrors.copyAllErrors(tLCGrpAppntDB.mErrors);
                        buildError("getPrintData", "在取得LCGrpAppnt的数据时发生错误");
                        return false;
                    }
                    tAppName = tLCGrpAppntDB.getName();
                }
                else
                {
                    tAppName = tLPGrpAppntSet.get(1).getName();
                }
            }
            //取保全名称
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(tEdorNo);
            tLPGrpEdorItemDB.setGrpContNo(tContNo);
            LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
            if ((tLPGrpEdorItemSet == null) || (tLPGrpEdorItemSet.size() == 0))
            {
                buildError("getPrintData", "在取得LPGrpEdorItemSet的数据时发生错误");
                return false;
            }
            LMEdorItemDB tLMEdorItemDB = new LMEdorItemDB();
            for (int i = 0; i < tLPGrpEdorItemSet.size(); i++)
            {
                tLMEdorItemDB.setEdorCode(tLPGrpEdorItemSet.get(i + 1).
                                          getEdorType());
                tLMEdorItemDB.setAppObj("G");
                LMEdorItemSet tLMEdorItemSet = tLMEdorItemDB.query();
                if (tLMEdorItemSet.size() > 0)
                {
                    tEdorNames += tLMEdorItemSet.get(i + 1).getEdorName() + ",";
                }
            }

        }

        String tDSumMoney = "";
        double SumGetMoney = Double.valueOf(tSumDuePayMoney).doubleValue();
        tDSumMoney = PubFun.getChnMoney(SumGetMoney);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("EdorFeeNotice.vts", "printer"); //最好紧接着就初始化xml文档

        texttag.add("GetNoticeNo", tGetNoticeNo); //通知书号
        texttag.add("DSumMoney", tDSumMoney); //大写金额
        texttag.add("XSumMoney", tSumDuePayMoney); //小写金额
        texttag.add("AppName", tAppName); //客户名称
        texttag.add("EdorNames", tEdorNames); //保全名称
        texttag.add("EdorAppDate", tEdorAppDate); //
        texttag.add("EdorAppNo", tEdorNo); //保全申请日期
        texttag.add("ContNo", tContNo); //保单号
        texttag.add("Agentcode", tAgentcode); //代理机构号
        texttag.add("ManagePhone", tManagePhone); //机构电话
        texttag.add("GetMoneyForPay", tGetMoneyForPay); //收费明细总额
        texttag.add("GetMoneyForGet", tGetMoneyForGet); //退费明细总额
        texttag.add("GetMoneyBF", tGetMoneyBF); //保险费
        texttag.add("GetMoneyLX", tGetMoneyLX); //利息
        texttag.add("GetMoneyGB", tGetMoneyGB); //工本费
        texttag.add("GetMoneyTB", tGetMoneyTB); //退保金
        texttag.add("NowDate", PubFun.getCurrentDate()); //日期

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
        mLPEdorMainSchema.setEdorNo("410000000000133");
        tVData.addElement(mLPEdorMainSchema);
        EdorFeeF1PBL edorFeeF1PBL1 = new EdorFeeF1PBL();
        edorFeeF1PBL1.submitData(tVData, "PRINT");
    }
}
