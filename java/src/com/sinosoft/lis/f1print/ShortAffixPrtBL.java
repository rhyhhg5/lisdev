package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.Vector;

import com.sinosoft.lis.db.LLAffixDB;
import com.sinosoft.lis.db.LLAppClaimReasonDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LLAffixSet;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.utility.*;

public class ShortAffixPrtBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    private String mContNo = "";

    //输入的查询sql语句
    private String msql = "";

    //取得的延期承保原因
    private String mUWError = "";

    //取得的代理人编码
    private String mAgentCode = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();
    private LCContSet mLCContSet = new LCContSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
    private LLAppealSchema mLLAppealSchema = new LLAppealSchema(); //理赔申诉错误表
    private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();
    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();

    public ShortAffixPrtBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mLLCaseSchema.setSchema((LLCaseSchema) cInputData.getObjectByObjectName(
                "LLCaseSchema", 0));
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        if (mLLCaseSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if (mLLCaseSchema.getCaseNo() == null)
        {
            buildError("getInputData", "没有得到足够的信息:分案号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        boolean PEFlag = false; //打印理赔申请回执的判断标志
        boolean shortFlag = false; //打印备注信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //根据印刷号查询打印队列中的纪录
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aRgtNo = "";

        LLCaseDB mLLCaseDB = new LLCaseDB();
        mLLCaseDB.setCaseNo(aCaseNo);
        //mLLCaseDB.setRgtNo(aRgtNo);

        if (!mLLCaseDB.getInfo())
        {
            mErrors.copyAllErrors(mLLCaseDB.mErrors);
            buildError("outputXML", "mLLCaseDB在取得打印队列中数据时发生错误");
            return false;

        }
        mLLCaseSchema.setSchema(mLLCaseDB.getSchema());
        aRgtNo = mLLCaseSchema.getRgtNo(); //立案号

        LLRegisterDB mLLRegisterDB = new LLRegisterDB(); //以下怎么处理
        mLLRegisterDB.setRgtNo(aRgtNo);
        if (mLLRegisterDB.getInfo() == false)
        {
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "mLLRegisterDB在取得打印队列中数据时发生错误");
            return false;
        }
        mLLRegisterSchema = mLLRegisterDB.getSchema();

        //ShortAffixPrtBL
        ListTable tReaListTable = new ListTable();
        String[] reasonTitle =
                {
                "APPEALREASON0", "APPEALREASON0"};
        tReaListTable.setName("APPEALREASON");
        mLLAppClaimReasonDB.setCaseNo(aCaseNo);
        mLLAppClaimReasonDB.setRgtNo(aRgtNo);
        mLLAppClaimReasonSet.set(mLLAppClaimReasonDB.query());
        if (mLLAppClaimReasonSet == null || mLLAppClaimReasonSet.size() == 0)
        {
            PEFlag = false;
        }
        else
        {
            PEFlag = true;
//      texttag.add("type1","");
//   texttag.add("type2","");
//   texttag.add("type3","");
//   texttag.add("type4","");
//   texttag.add("type5","");
//   texttag.add("type6","");
            int QQSize = mLLAppClaimReasonSet.size();
            if (mLLAppClaimReasonSet.size() > 6)
            {
                QQSize = 6;
            }
            for (int qq = 0; qq < QQSize; qq++)
            {
                texttag.add("type" + String.valueOf(qq + 1),
                            mLLAppClaimReasonSet.get(qq + 1).getReason());
            }

//      int reasonSize = mLLAppClaimReasonSet.size();
//      if (reasonSize > 6) {
//        reasonSize = 6;
//      }
//      Vector vectReaListTable = new Vector();
//      for (int j = 1; j <= reasonSize; j++) {
//        mLLAppClaimReasonSchema.setSchema(mLLAppClaimReasonSet.get(j));
//        vectReaListTable.addElement(mLLAppClaimReasonSchema.getReason());
        }
//      if (vectReaListTable.size() != 0) {
//        vectReaListTable.addElement("");
//      }
//      String[] reason = new String[2];
//      int m = 0;
//      System.out.print(vectReaListTable.size());
//      for (m = 0; m < vectReaListTable.size(); m += 2) {
//        reason[0] = (String) vectReaListTable.get(m);
//        if ( (m + 1) < vectReaListTable.size()) {
//          reason[1] = (String) vectReaListTable.get(m + 1);
//        }
//        else {
//          reason[1] = "";
//        }
//        tReaListTable.add(reason);
//      }


//    }
/////////////
        ListTable tACRListTable = new ListTable();
        ListTable tACRListTableShort = new ListTable();
        String[] ACRTitle = new String[2];
        String[] ACRTitleShort = new String[2];
        ACRTitle[0] = "AFFIX0";
        ACRTitle[1] = "AFFIX1";
        ACRTitleShort[0] = "AFFIXSHORT0";
        ACRTitleShort[1] = "AFFIXSHORT1";

        String[] strACR = new String[2];
        String[] strACRShort = new String[2];
        tACRListTable.setName("AFFIX"); //对应模版理赔申请事故原因部分的行对象名
        tACRListTableShort.setName("AFFIXSHORT");
        LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
        LLAppClaimReasonSet aLLAppClaimReasonSet = new LLAppClaimReasonSet();
        LLAffixDB mLLAffixDB = new LLAffixDB();
        LLAffixSet mLLAffixSet = new LLAffixSet();
        int count = 0; //申请资料的总数
        int countShort = 0; //需要补充的资料总数

//         mLLAffixDB.setCaseNo(mLLCaseDB.getCaseNo());
//         mLLAffixDB.setRgtNo(mLLCaseDB.getRgtNo());

        String sql = "select * from LLAffix where RgtNo='" + mLLCaseDB.getRgtNo() +
                     "' and CaseNo='" + mLLCaseDB.getCaseNo() + "'";

        // mLLAffixSet.set(mLLAffixDB.query());
        mLLAffixSet = mLLAffixDB.executeQuery(sql);
        //mLLAffixSet=mLLAffixDB.query();
        LLAffixSchema mLLAffixSchema = new LLAffixSchema();
        Vector vector = new Vector();
        Vector vectorShort = new Vector();
        if (mLLAffixSet == null || mLLAffixSet.size() == 0)
        {
            PEFlag = false;
        }
        else
        {
            PEFlag = true;

            for (int j = 1; j <= mLLAffixSet.size(); j++)
            {
                mLLAffixSchema.setSchema(mLLAffixSet.get(j));
                if (mLLAffixSchema.getShortCount() > 0)
                {
                    vectorShort.addElement(mLLAffixSchema.getAffixName());
                }
                else
                {
                    vector.addElement(mLLAffixSchema.getAffixName());
                }
            }
        }

        if (vector.size() % 2 != 0)
        {
            vector.addElement("");
        }
        for (int i = 0; i < vector.size(); i += 2)
        {
            strACR[0] = (i + 1) + "、" + (String) vector.get(i);
//      if ( i + 1 < vector.size()) {
            if (vector.get(i + 1) != "")
            {
                strACR[1] = (i + 2) + "、" + (String) vector.get(i + 1);
            }
            else
            {
                strACR[1] = "";
            }
//      }
//      else {
//        strACR[1] = "";
//      }
            tACRListTable.add(strACR);
            strACR = new String[2];
        }

        if (vectorShort.size() > 0)
        {
            shortFlag = true;

        }
        if (vectorShort.size() % 2 != 0)
        {
            vectorShort.addElement("");

        }

        for (int i = 0; i < vectorShort.size(); i += 2)
        {
            strACRShort[0] = (i + 1) + "、" + (String) vectorShort.get(i);
//      if ( (i + 1) < vectorShort.size()) {
            if (vectorShort.get(i + 1) != "")
            {
                strACRShort[1] = (i + 2) + "、" + (String) vectorShort.get(i + 1);
            }
            else
            {
                strACRShort[1] = "";
            }
//      }
//      else {
//        strACRShort[1] = "";
//      }
            tACRListTableShort.add(strACRShort);
            strACRShort = new String[2];
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ShortAffixPrt.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
//        StrTool tSrtTool = new StrTool();
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";

        //ShortAffixPrtBL模版元素
        //texttag.add("BarCodeParam1","");


        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("LLCase.CustomerName", mLLCaseSchema.getCustomerName());
        texttag.add("LLCase.CustomerSex",
                    (mLLCaseSchema.getCustomerSex() == "0" ? "男" : "女")); //性别是不是01？
        texttag.add("IDNo", mLLCaseSchema.getIDNo());
        texttag.add("CustBirthday", mLLCaseSchema.getCustBirthday());
        texttag.add("Phone", mLLCaseSchema.getPhone());
        texttag.add("PostalAddress", mLLCaseSchema.getPostalAddress());
        texttag.add("LLRegister.RgtantName", mLLRegisterSchema.getRgtantName());
        texttag.add("RgtantPhone", mLLRegisterSchema.getRgtantPhone());
        texttag.add("RgtDate", mLLRegisterSchema.getRgtDate());
        texttag.add("AccStartDate", mLLRegisterSchema.getAccStartDate());
        texttag.add("CaseGetMode", mLLRegisterSchema.getCaseGetMode());
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("SysDate", SysDate);
        if (shortFlag)
        {
            texttag.add("Supply1", "1、根据保险合同的约定，您所提供的资料不齐，请根据上述提示补齐资料后再行申请；");
            texttag.add("Supply2",
                        "2、本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
        }
        else
        {
            texttag.add("Supply1",
                        "本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
            texttag.add("Supply2", "");
        }
        ////////////
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        if (PEFlag == true)
        {
            xmlexport.addListTable(tReaListTable, reasonTitle); //保存申请资料信息

            xmlexport.addListTable(tACRListTable, ACRTitle); //保存申请资料信息
            xmlexport.addListTable(tACRListTableShort, ACRTitleShort); //保存申请资料信息
        }
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args)
    {

        ShortAffixPrtBL tShortAffixPrtBL = new ShortAffixPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("550000000000112");
        tVData.addElement(tLLCaseSchema);
        tShortAffixPrtBL.submitData(tVData, "PRINT");
        VData vdata = tShortAffixPrtBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

}
