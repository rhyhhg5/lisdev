package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPGrpEdorMainSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.COracleBlob;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


public class TemplateSaveBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    private String mOperate = "";
//  private String CurrentDate = PubFun.getCurrentDate();

    public TemplateSaveBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        boolean tReturn = false;
        boolean aReturn = false;
        mOperate = cOperate;
        System.out.println("Start TemplateSaveBL Submit...");

        if (cOperate.equals("PRINT"))
        {
            tReturn = getPrintData(cInputData);
        }
        if (cOperate.equals("CONFIRM"))
        {
            aReturn = getPrintData2(cInputData);
        }

        if (tReturn)
        {
            System.out.println(" sucessful print1");
        }
        else if (aReturn)
        {
            System.out.println(" sucessful print2");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End TemplateSaveBL Submit...");
        return true;
    }

    public static void main(String[] args)
    {
    }

//取模板数据操作
    private boolean getPrintData(VData cInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        String strErrInfo = "";

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPGrpEdorMainSchema.setSchema((LPGrpEdorMainSchema) cInputData.
                                       getObjectByObjectName(
                "LPGrpEdorMainSchema", 0));
        String strEdorNo = mLPGrpEdorMainSchema.getEdorNo();

        Connection conn = DBConnPool.getConnection();
        COracleBlob tCOracleBlob = new COracleBlob();
        if (conn == null)
        {
            strErrInfo = "连接数据库失败";
            return false;
        }

        try
        {
            String szSQL = "";
            szSQL = " and EdorNo = '" + strEdorNo + "'";
            System.out.println("----sql:" + szSQL);
            Blob tBlob = tCOracleBlob.SelectBlob("LPEDORPRINT", "EdorInfo",
                                                 szSQL, conn);

            if (tBlob != null)
            {
//          BLOB blob = ((OracleResultSet)rs).getBLOB("edorinfo");//modify by yt about Blob type
                //BLOB blob = (oracle.sql.BLOB)tBlob;
                mResult.clear();
                mResult.add(tBlob.getBinaryStream());

                InputStream tInputStream = (InputStream) mResult.get(0);
                System.out.println("get stream object");
            }
            else
            {
                //buildError("getPrintData","从数据库中读取批单数据失败");
                throw new Exception("从数据库中读取批单数据失败！");
            }
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, ex.toString());
            try
            {
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    private boolean getPrintData2(VData cInputData)
    {
        boolean aReturn = true;
        System.out.println("Start Save...");
        String strErrInfo = "";
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPGrpEdorMainSchema.setSchema((LPGrpEdorMainSchema) cInputData.
                                       getObjectByObjectName(
                "LPGrpEdorMainSchema", 0));
        String strEdorNo = mLPGrpEdorMainSchema.getEdorNo();

        Connection conn = DBConnPool.getConnection();
        COracleBlob tCOracleBlob = new COracleBlob();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TemplateSaveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "连接数据库失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            String szSQL = "";
            szSQL = " and EdorNo = '" + strEdorNo + "'";
            System.out.println("----sql:" + szSQL);
            Blob tBlob = tCOracleBlob.SelectBlob("LPEDORPRINT2", "EdorInfo",
                                                 szSQL, conn);
            if (tBlob != null)
            {
                //BLOB blob = (oracle.sql.BLOB)tBlob;
                mResult.clear();
                mResult.add(tBlob.getBinaryStream());

                System.out.println("get stream object");
            }
            else
            {
                throw new Exception("从数据库中读取批单数据失败！");
            }
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TemplateSaveBL";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "TemplateSaveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }
}

