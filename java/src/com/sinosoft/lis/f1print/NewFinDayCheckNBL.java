package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys yh
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.util.*;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.certify.SysOperatorNoticeBL;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewFinDayCheckNBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();


	// 取得的时间和操作参数
	private String mDay[] = null;
	private String strOperation="";

	// 业务处理相关变量
	private String mRiskCode;

	private String mRiskType;

	private String mXsqd;

	private String mXzdl;

	private String mSxq;

	private String mDqj;

	private LMRiskAppSet mLMRiskAppSet;

	private String mRiskName = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public NewFinDayCheckNBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		strOperation=cOperate;
		
		if (!cOperate.equals("SS") && !cOperate.equals("SF") && !cOperate.equals("SXF")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();
		
        if (!dealPrint()) {
			return false;
		}
		return true;
	}
	/** 处理日结单打印函数 */
    private boolean dealPrint(){
    	if(strOperation.equals("SS")){
    		if(!dealPrint_SS()){
    			return false;
    		}
    	}else if(strOperation.equals("SF")){
    		if(!dealPrint_SF()){
    			return false;
    		}
    	}else if(strOperation.equals("SXF")){
    		if(!dealPrint_SXF()){
    			return false;
    		}
    	}else{
    		buildError("dealPrint","错误的操作类型");
    	}
    	return true;
    }
	/** X1实收日结单打印函数 */
    private boolean dealPrint_SS(){
    	ArrayList Parameters = new ArrayList();
  	 
    	 //公共信息封装
    	 ArrayList Global=new ArrayList();
    	 Global.add(mGlobalInput.ManageCom);      //管理机构
    	 Global.add(mDay[0]);                     //打印起始日期
    	 Global.add(mDay[1]);                   //打印终止日期
    	 Parameters.add(Global);
    	 
    	 //添加sql
    	 ArrayList type1=new ArrayList();
    	 String[] detail1 = new String[]{"付款方式","号码类型","号码","收费日期","发票号码","印刷号","成本中心","金额","件数"};
    	 String[] getFormateOrder1= new String[]{"8"};
    	 String[] getType1 = new String[] {
  				"X1SSMX", // 按方式打印 收费明细SQL
  				"X1SSMXZ" // 收费日结汇总SQL
  		 };
    	 type1.add(detail1);
    	 type1.add(getType1);
    	 type1.add(getFormateOrder1);
    	 Parameters.add(type1);

    	 
    	 //添加汇总sql
    	 ArrayList type2=new ArrayList();
    	 String[] detail2 = new String[]{"总金额","总件数"};
    	 String[] getFormateOrder2= new String[]{"1"};
    	 String[] getTypes2 = new String[] { 
   				"X1SSHJ" // 收费合计SQL
   		};
    	 type2.add(detail2);
    	 type2.add(getTypes2);
    	 type2.add(getFormateOrder2);
    	 Parameters.add(type2);
    	 
    	 //调用函数
    	 print(Parameters,"NewFinDayCheckN_SS.vts","SS");
    	 
    	return true;
    }
    
    /** X2实收日结单打印函数 */
    private boolean dealPrint_SF(){
    	ArrayList Parameters = new ArrayList();
     	 
   	 //公共信息封装
   	 ArrayList Global=new ArrayList();
   	 Global.add(mGlobalInput.ManageCom);      //管理机构
   	 Global.add(mDay[0]);                     //打印起始日期
   	 Global.add(mDay[1]);                   //打印终止日期
   	 Parameters.add(Global);
   	 
   	 //添加sql
   	 ArrayList type1=new ArrayList();
   	 String[] detail1 =new String[]{"付款方式","号码类型","号码","金额","件数"};
   	 String[] getFormateOrder1= new String[]{"4"};
   	 String[] getType1 = new String[] {
 				"X2SF" // 按方式打印 收费明细SQL
 		 };
   	 type1.add(detail1);
   	 type1.add(getType1);
   	 type1.add(getFormateOrder1);
   	Parameters.add(type1);

   	 
   	 //添加汇总sql
   	 ArrayList type2=new ArrayList();
   	 String[] detail2 = new String[]{"汇总类型","金额","数量"};
   	 String[] getFormateOrder2= new String[]{"3"};
   	 String[] getTypes2 = new String[] { 
  				"X2SFZ" // 收费合计SQL
  		};
   	 type2.add(detail2);
   	 type2.add(getTypes2);
   	 type2.add(getFormateOrder2);
   	Parameters.add(type2);
   	 
   	 
   	 //添加汇总sql
   	 ArrayList type3=new ArrayList();
   	 String[] detail3 = new String[]{"总金额","总件数"};
     String[] getFormateOrder3= new String[]{"1"};
   	 String[] getType3 = new String[] { 
  				"X2SFHJ" // 收费合计SQL
  		};
   	 type3.add(detail3);
   	 type3.add(getType3);
   	 type3.add(getFormateOrder3);
     Parameters.add(type3);
   	 
   	 //调用函数
   	 print(Parameters,"NewFinDayCheckN_SF.vts","SF");
   	 
    	return true;
    }
    
    /** X9实收日结单打印函数 */
    private boolean dealPrint_SXF(){
    	ArrayList Parameters = new ArrayList();
     	 
   	 //公共信息封装
   	 ArrayList Global=new ArrayList();
   	 Global.add(mGlobalInput.ManageCom);      //管理机构
   	 Global.add(mDay[0]);                     //打印起始日期
   	 Global.add(mDay[1]);                   //打印终止日期
   	 Parameters.add(Global);
   	 
   	 //添加sql
   	 ArrayList type1=new ArrayList();
   	 String[] detail1 = new String[] { "中介机构", "保单号码", "成本中心","险种代码", "投保人", "保费收入","手续费率","手续费金额","代理机构" };
   	 String[] getType1 = new String[] {
           "SXFDSF"  //X9手续费明细
 		 };
   	 String[] getFormateOrder1= new String[]{"7","8","9"};
   	 type1.add(detail1);
   	 type1.add(getType1);
   	 type1.add(getFormateOrder1);
   	 Parameters.add(type1);

   	 
   	 //添加汇总sql
   	 ArrayList type2=new ArrayList();
   	 String[] detail2 = new String[]{"总金额"};
   	 String[] getTypes2 = new String[] { 
  				"SXFZBF" // X9手续费汇总
  		};
   	 String[] getFormateOrder2= new String[]{ "1" };
   	 type2.add(detail2);
   	 type2.add(getTypes2);
   	 type2.add(getFormateOrder2);
   	 Parameters.add(type2);
   	 
   	 //调用函数
   	 print(Parameters,"NewFinDayCheckN_SXF.vts","SXF");
   	 
     return true;
    }
    
    private boolean print(ArrayList Parameters,String vts,String printType){
    	int t=0;
    	SSRS tSSRS = new SSRS();
    	GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
    	ExeSQL tExeSQL = new ExeSQL();
    	TextTag texttag = new TextTag();       //新建一个TextTag的实例
    	XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    	xmlexport.createDocument(vts, "printer"); //最好紧接着就初始化xml文档
    	
    	String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //tServerPath= "D:/workspace/picc/WebContent/";
        
    	String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
    	tSSRS = tExeSQL.execSQL(nsql);
        String tmanageCom = tSSRS.GetText(1, 1);
        
    	Iterator it=Parameters.iterator();
    	while(it.hasNext()){
    		ArrayList value=(ArrayList)it.next();
    		if(t==0){
    			System.out.println("ManageCom:"+value.get(0));
    			System.out.println("BeginDate:"+value.get(1));
    			System.out.println("EndDate:"+value.get(2));
    	        tGetSQLFromXML.setParameters("ManageCom", (String)value.get(0));
    	        tGetSQLFromXML.setParameters("BeginDate", (String)value.get(1));
    	        tGetSQLFromXML.setParameters("EndDate",   (String)value.get(2));
    	        
    	        texttag.add("StartDate", (String)value.get(1));
    	        texttag.add("EndDate",   (String)value.get(2));
    	        texttag.add("ManageCom", tmanageCom);
    	         
    	        t++;
    		}else{
    			int size=value.size();
    			ListTable tDetailListTable=null;
    			String[] detailArr=null;
    			String[] getTypes=null;
    			String[] getFormateOrder=null;
    			if(size==2){
					detailArr=(String[])value.get(0);
					getTypes=(String[])value.get(1);
    			}else{
    				detailArr=(String[])value.get(0);
					getTypes=(String[])value.get(1);
					getFormateOrder=(String[])value.get(2);
    			}
    			value.size();
    			for (int i = 0; i < getTypes.length; i++) {
    				String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", getTypes[i]);
    				System.out.println(msql);
    				if(size==2){
    					tDetailListTable = getDetailListTable(msql, getTypes[i],null); // 明细ListTable
    				}else{
    					tDetailListTable = getDetailListTable(msql, getTypes[i],getFormateOrder); // 明细ListTable
    				}
    				xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable放入xmlexport对象
    			}
    			t++;
    			
    		}
    	}
    	if (texttag.size() > 0){
            xmlexport.addTextTag(texttag);
        }  
        mResult.clear();
        mResult.addElement(xmlexport);
    	return true;
    }
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		
	    if (mDay == null)
        {
            buildError("getInputData", "没有得到日结单打印起始日期！");
            return false;
        }
    	System.out.println("打印起始日期：" + mDay[0]);
    	System.out.println("打印终止日期：" + mDay[1]);
    	
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到公共信息！");
            return false;
        }
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "NewFinDayCheckNBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 显示总计无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("QYSSZ".equals(pName)) {
			result[0] = "契约实收保费 小计";
		} else if ("XQSSZ".equals(pName)) {
			result[0] = "续期实收 小计";
		} else if ("CXYSZ".equals(pName)) {
			result[0] = "长险应收保费 小计";
		}else if ("DXYSZ".equals(pName)) {
            result[0] = "短险应收保费 小计";
        } else if ("YNQFCYSZ".equals(pName)) {
			result[0] = "一年期分次缴费业务应收冲销 小计";
		} else if ("QYSSGLFZ".equals(pName)) {
			result[0] = "契约实收保费-帐户管理费 小计";
		} else if ("QYSSBFCJZ".equals(pName)) {
			result[0] = "契约实收保费-保户储金 小计";
		} else if ("XQSSGLFZ".equals(pName)) {
			result[0] = "续期实收保费-帐户管理费 小计";
		} else if ("XQSSBFCJZ".equals(pName)) {
			result[0] = "续期实收保费-保户储金 小计";
		} else if ("XQHTZ".equals(pName)) {
			result[0] = "续期回退 小计";
		} else if ("XQYSZFZ".equals(pName)) {
			result[0] = "续期应收作废 小计";
		}
		result[1] = "0";
		return result;
	}

	/**
	 * 获得明细ListTable,格式都一直，所以写在一个ListTable里面。
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName,String[] formateOrder) {
		SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        tlistTable.setName(tName);
        String strSum="";
        String strArr[] = null;
        for (int i = 1; i <= tSSRS.MaxRow; i++){
            strArr = new String[tSSRS.MaxCol]; // 列数
            for (int j = 1; j <= tSSRS.MaxCol; j++){
            	for(int t=0;t<formateOrder.length;t++)
            	{
            		if(j==Integer.parseInt(formateOrder[t].trim())){
            			strArr[j - 1] = tSSRS.GetText(i, j);
        				if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
        				strSum = new DecimalFormat("0.00").format(Double
        						.valueOf(strArr[j - 1]));
        				strArr[j - 1] = strSum;}
        				break;
            		}else{
            			strArr[j - 1] = tSSRS.GetText(i, j);
            		}
            	}
            }
            tlistTable.add(strArr);
        }
        return tlistTable;
	}

}

