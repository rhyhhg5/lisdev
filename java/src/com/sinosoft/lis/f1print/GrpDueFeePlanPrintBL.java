package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDueFeePlanPrintBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema
    private String mflag = null;
    public GrpDueFeePlanPrintBL(){}

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        System.out.println("GrpDueFeePrintBL begin");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mflag = cOperate;
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        if(cOperate.equals("INSERT") && needPrt==0)
        {
            if (!dealPrintMag()) {
                return false;
            }
        }
        System.out.println("GrpDueFeePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
//PDF入口
            //取得LJSPayBDB
            LJSPayBDB mLJSPayBDB = new LJSPayBDB();
            mLJSPayBDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
            if(!mLJSPayBDB.getInfo())
            {
                mErrors.addOneError("LJSPayBDB传入的数据不完整。");
                return false;
            }
            tLJSPayBDB = mLJSPayBDB.getDB();

            //取得tLCGrpContSchema
            LCGrpContDB mLCGrpContDB = new LCGrpContDB();
            mLCGrpContDB.setGrpContNo(mLJSPayBDB.getOtherNo());
            if(!mLCGrpContDB.getInfo())
            {
                buildError("getInputData", "LCGrpContDB没有得到足够的信息！");
                return false;
            }
            tLCGrpContSchema = mLCGrpContDB.getSchema();
            return true;
        
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }


    private boolean getPrintData()
    {
    	String GrpContNo= tLCGrpContSchema.getGrpContNo();
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        textTag.add("JetFormType", "99");
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

          textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
        	textTag.add("previewflag", "0");
        }else{
        	textTag.add("previewflag", "1");
        }
        setFixedInfo();
        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(GrpContNo);

        if (!tLCGrpAppntDB.getInfo())
        {
            CError.buildErr(this,
                            "团单投保人表中缺少数据");
            return false;
        }

        String sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where grpcontno='" +
              GrpContNo + "') AND AddressNo = '" + tLCGrpAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        tLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).
                              getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        showPaymode = "自行交费";
        xmlexport.createDocument("GrpDueFeePlanPrint.vts", "printer"); //最好紧接着就初始化xml文档

        //交费频次，交费方式设置
        textTag.add("PayMode", showPaymode);

        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        System.out.println("GrpZipCode=" + tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        String appntPhoneStr = "";
        if (tLCGrpAddressSchema.getPhone1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getPhone1();
        } else if (tLCGrpAddressSchema.getMobile1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getMobile1();
        }
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        textTag.add("AppntNo", tLCGrpContSchema.getAppntNo());
        textTag.add("GrpContNo", GrpContNo);
        System.out.println("GrpContNo=" + "" + GrpContNo);
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("GetNoticeNo", tLJSPayBDB.getGetNoticeNo());
        textTag.add("MakeDate", CommonBL.decodeDate(tLJSPayBDB.getMakeDate()));
        textTag.add("DeadLine", tLJSPayBDB.getPayDate());
        textTag.add("PayCanDate", tLJSPayBDB.getPayDate());
        textTag.add("Prem", getPrem());
        textTag.add("AccGetBala", getAccGetBala()); //可抵扣帐户余额
        textTag.add("SumPrem",
                    CommonBL.bigDoubleToCommonString
                    (tLJSPayBDB.getSumDuePayMoney(), "0.00")); //本次应交费
        
        //业务员信息
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        textTag.add("Phone", tLaAgentDB.getPhone());

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSchema.getAgentGroup());
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", tLABranchGroupDB.getName());
        textTag.add("XI_ManageCom", tLCGrpContSchema.getManageCom());

        textTag.add("BarCode1", tLJSPayBDB.getGetNoticeNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("GetNoticeNo" + tLJSPayBDB.getGetNoticeNo());

        if (textTag.size() > 0)
        {
            xmlexport.addTextTag(textTag);
        }

        String[] title =
                {"保障计划", "被保人数", "投保时间", "缴费频次", "本次应交金额"};
        xmlexport.addListTable(getListTable(), title);
        
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);

        xmlexport.outputDocumentToFile("c:\\", "TaskPrintPlan"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
            //放入打印列表
            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
            tLOPRTManagerDB.setCode("99");
            tLOPRTManagerSet = tLOPRTManagerDB.query();
            needPrt = tLOPRTManagerSet.size();

            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
                mLOPRTManagerSchema.setOtherNoType("01");
                mLOPRTManagerSchema.setCode("99");
                mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo()); //这里存放 （也为交费收据号）
            }else{
            	mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
            	mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
            }
        mResult.add(mLOPRTManagerSchema);
        return true;
    }

    /**
     * 得到本次的期交保费
     * @return String
     */
    private String getPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select varchar(sum(sumDuePayMoney)) ")
                .append("from LJSPayGrpB ")
                .append("where getNoticeNo = '")
                .append(tLJSPayBDB.getGetNoticeNo())
                .append("'  and payType = 'ZC' ");
        String prem = new ExeSQL().getOneValue(sql.toString());
        if (prem.equals("") || prem.equals("null"))
        {
            return "";
        }
        return prem;
    }

    /**
     * 得到投保人帐户余额
     * @return String：余额
     */
    private String getAccGetBala()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select varchar(sum(abs(sumActuPayMoney))) ")
                .append("from LJSPayGrpB ")
                .append("where getNoticeNo = '")
                .append(tLJSPayBDB.getGetNoticeNo())
                .append("'  and payType = 'YEL' ");
        String accGetBala = new ExeSQL().getOneValue(sql.toString());
        if (accGetBala.equals("") || accGetBala.equals("null"))
        {
            return "0.00";
        }
        return accGetBala;
    }

    private void setFixedInfo()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCGrpContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();

        StringBuffer sql = new StringBuffer();
        sql.append("select a.contPlanCode, a.peoples2, ")
                .append("  (select handlerDate from LCGrpCont where grpcontNO = a.grpContNo), ")
                .append(" '约定缴费', ")
                .append(" (select prem from lcgrppayactu where contplancode=a.contPlanCode and getnoticeno='"+tLJSPayBDB.getGetNoticeNo()+"') ")
                .append("from LCContPlan a ")
                .append("where grpContNo = '")
                .append(tLJSPayBDB.getOtherNo())
                .append("'  and contPlanCode != '11' ");
        System.out.println(sql.toString());
        SSRS tSSRS = new ExeSQL().execSQL(sql.toString());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[5];
            info[0] = tSSRS.GetText(i, 1);
            info[1] = tSSRS.GetText(i, 2);
            info[2] = tSSRS.GetText(i, 3);
            info[3] = tSSRS.GetText(i, 4);
            info[4] = tSSRS.GetText(i, 5);
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    public static void main(String[] args)
    {
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();

        GrpDueFeePrintBL p = new GrpDueFeePrintBL();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000001457");

        if(!tLJSPayBDB.getInfo())
        {
            p.mErrors.addOneError("LJSPayBDB传入的数据不完整。");
            return ;
        }

        LOPRTManagerDB ttLOPRTManagerDB = new LOPRTManagerDB();
        ttLOPRTManagerDB.setCode("99");
        ttLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
        String a = ttLOPRTManagerDB.query().get(1).getPrtSeq();
        ttLOPRTManagerDB.setPrtSeq(a);

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement( ttLOPRTManagerDB.getSchema());
        p.submitData(tVData, "PRINT");

    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT
     * @return
     */
    private boolean dealPrintMag()
    {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false)
        {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
