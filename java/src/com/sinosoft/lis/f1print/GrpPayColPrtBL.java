package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;

public class GrpPayColPrtBL
     implements PrintService{
 /** 错误处理类，每个需要错误处理的类中都放置该类 */
 public CErrors mErrors = new CErrors();

 private VData mResult = new VData();


 //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
  public GrpPayColPrtBL()
    {
    }
  public boolean submitData(VData cInputData, String cOperate) {
   if (!cOperate.equals("PRINT")) {
     buildError("submitData", "不支持的操作字符串");
     return false;
   }

   // 得到外部传入的数据，将数据备份到本类中
   if (!getInputData(cInputData)) {
     return false;
   }
   mResult.clear();

   // 准备所有要打印的数据
   if (!getPrintData()) {
     return false;
   }
   return true;
 }

 /**
  * 根据前面的输入数据，进行BL逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData() {
   return true;
 }

 private void buildError(String Fun, String Msg) {
   CError cError = new CError();
   cError.moduleName = "LCContF1PBL";
   cError.functionName = Fun;
   cError.errorMessage = Msg;
   this.mErrors.addOneError(cError);
 }

 private boolean getInputData(VData cInputData) {
   //全局变量
   mLLRegisterSchema.setSchema( (LLRegisterSchema) cInputData.getObjectByObjectName(
       "LLRegisterSchema", 0));
   mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
       0);
   if (mLLRegisterSchema == null) {
     buildError("getInputData", "没有得到足够的信息！");
     return false;
   }

   if (mLLRegisterSchema.getRgtNo() == null) {
     buildError("getInputData", "没有得到足够的信息:团体号不能为空！");
     return false;
   }
   return true;
 }

 public VData getResult() {
    return mResult;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  private boolean getPrintData() {
      LLRegisterDB mLLRegisterDB = new LLRegisterDB();
      mLLRegisterDB.setSchema(mLLRegisterSchema);

      if (mLLRegisterDB.getInfo() == false)
        {
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "请先保存该团体批次信息！");
            return false;
        }
      mLLRegisterSchema= mLLRegisterDB.getSchema();

      String strSQL = "select Username from lduser where usercode='" + mGlobalInput.Operator + "'";
      ExeSQL exesql1 = new ExeSQL();
      String OperName = exesql1.getOneValue(strSQL);
      strSQL = "select Username from lduser where usercode='" + mLLRegisterSchema.getOperator() + "'";
      String CommitName = exesql1.getOneValue(strSQL);


      //其它模版上单独不成块的信息
      TextTag texttag = new TextTag();
      XmlExport xmlexport = new XmlExport();
      xmlexport.createDocument("GrpPayAppPrt.vts", "printer");
      //生成-年-月-日格式的日期
      StrTool tSrtTool = new StrTool();
      String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
          tSrtTool.getDay() + "日";
      //GrpPayColPrtBL模版元素
      texttag.add("BarCode1", mLLRegisterSchema.getRgtNo());
      texttag.add("BarCodeParam1"
                  , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
      texttag.add("GrpName",mLLRegisterSchema.getGrpName());            //投保单位名称
      texttag.add("RgtObjNo",mLLRegisterSchema.getRgtObjNo());     //团体号码
      texttag.add("RgtantName",mLLRegisterSchema.getRgtantName()); //联系人
      texttag.add("PostCode",mLLRegisterSchema.getPostCode());     //邮政编码
      texttag.add("RgtantPhone", mLLRegisterSchema.getRgtantPhone());      //投保单位联系电话
      texttag.add("RgtantAddress",mLLRegisterSchema.getRgtantAddress());   //投保单位地址
      texttag.add("Handler1Phone",mLLRegisterSchema.getHandler1Phone());   //提交人联系电话
      texttag.add("Operator", CommitName);            //提交人
      texttag.add("AppPeoples",mLLRegisterSchema.getAppPeoples());         //申请人数
      texttag.add("Remark",mLLRegisterSchema.getRemark());                 //备注
//      texttag.add("RgtantName", mLLRegisterSchema.getRgtantName());        //客户签字
      texttag.add("Handler",OperName);            //经办人
      texttag.add("date",SysDate);     //日期

      if (texttag.size() > 0) {
        xmlexport.addTextTag(texttag);
      }
      //保存信息
     // xmlexport.outputDocumentToFile("f:\\", "testHZM"); //输出xml文档到文件
      mResult.clear();
      mResult.addElement(xmlexport);

      return true;

}


  public static void main(String[] args) {

   GrpPayColPrtBL tGrpPayColPrtBL = new GrpPayColPrtBL();
   VData tVData = new VData();
   GlobalInput tGlobalInput = new GlobalInput();
   tGlobalInput.ManageCom = "86";
   tGlobalInput.Operator = "001";
   tVData.addElement(tGlobalInput);
   LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
   tLLRegisterSchema.setRgtNo("510000000000230");
   tVData.addElement(tLLRegisterSchema);
   tGrpPayColPrtBL.submitData(tVData, "PRINT");
   VData vdata = tGrpPayColPrtBL.getResult();
 }


}
