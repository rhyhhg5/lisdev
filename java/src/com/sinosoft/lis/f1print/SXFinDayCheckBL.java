package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class SXFinDayCheckBL {



		/** 错误处理类，每个需要错误处理的类中都放置该类 */
		public CErrors mErrors = new CErrors();

		private VData mResult = new VData();

		private String mDay[] = null; // 获取时间

		private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

		public SXFinDayCheckBL() {
		}

		public static void main(String[] args) {
			GlobalInput tG = new GlobalInput();
			tG.Operator = "cwad1";
			tG.ManageCom = "86";
			VData vData = new VData();
			String[] tDay = new String[4];
			tDay[0] = "2010-10-01";
			tDay[1] = "2010-10-01";
			vData.addElement(tDay);
			vData.addElement(tG);

			SXFinDayCheckBL sXFinDayCheckBL = new SXFinDayCheckBL();
			sXFinDayCheckBL.submitData(vData, "PRINT");

		}

		/**
		 * 传输数据的公共方法
		 */
		public boolean submitData(VData cInputData, String cOperate) {
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}
			mResult.clear();

			if (cOperate.equals("PRINT")) { // 打印提数
				if (!getPrintData()) {
					return false;
				}
			}
			return true;
		}

		/**
		 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		 */
		private boolean getInputData(VData cInputData) { // 打印付费
			// 全局变量
			mDay = (String[]) cInputData.get(0);
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0));
			if (mGlobalInput == null) {
				buildError("getInputData", "没有得到足够的信息！");
				return false;
			}
			return true;
		}

		public VData getResult() {
			return this.mResult;
		}

		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "SXFinDayCheckBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			this.mErrors.addOneError(cError);
		}

		/**
		 * 新的打印数据方法
		 *
		 * @return
		 */
		private boolean getPrintData() {
			SSRS tSSRS = new SSRS();

			GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
			tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
//			System.out.println(mDay[0]);
//			System.out.println(mDay[1]);
			tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
			tGetSQLFromXML.setParameters("EndDate", mDay[1]);
			tGetSQLFromXML.setParameters("RiskCode", "险种代码");
//			String tServerPath = "E:/sinowork/picch/WebRoot/";
			ExeSQL tExeSQL = new ExeSQL();

			String nsql = "select Name from LDCom where ComCode='"
					+ mGlobalInput.ManageCom + "'"; // 管理机构
			tSSRS = tExeSQL.execSQL(nsql);
			String manageCom = tSSRS.GetText(1, 1);
			TextTag texttag = new TextTag(); // 新建一个TextTag的实例
			XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
			xmlexport.createDocument("SXFeeDayPrint.vts", "printer"); // 最好紧接着就初始化xml文档
			texttag.add("StartDate", mDay[0]);
			texttag.add("EndDate", mDay[1]);
			texttag.add("ManageCom", manageCom);
			texttag.add("RiskCode", "险种代码");
			if (texttag.size() > 0) {
				xmlexport.addTextTag(texttag);
			}
			String[] detailArr = new String[] { "中介机构", "保单号码", "险种代码", "投保人", "保费收入","手续费率","手续费金额","代理机构" };
			String[] totalArr = new String[1];
			String tname = "SXFDSF";
		    String pname="SXFZBF";

			ListTable tTotalListTabel=getDetailListTable(detailArr,tname);
			xmlexport.addListTable(tTotalListTabel,detailArr);
			ListTable sTotalListTabel=getHZListTable(totalArr,pname);
			xmlexport.addListTable(sTotalListTabel,totalArr);
			mResult.clear();
			mResult.addElement(xmlexport);
			return true;
		}

		/**
		 * 获得明细ListTable
		 *
		 * @param msql -
		 *            执行的SQL
		 * @param tName -
		 *            Table Name
		 * @return
		 */
		private ListTable getDetailListTable(String[] detailArr,String tname) {
			SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			String msql="select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.appntname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom from ljaget a,lacharge" +
					" b,lccont c where  a.otherno=b.receiptno and b.contno=c.contno and a.othernotype in('BC','AC') and " +
					"(b.grpcontno='00000000000000000000' or b.grpcontno is null) and "
			+"b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' and b.transmoney<>0 " +
					"and b.transmoney is not null and b.charge<>0 and chargestate in('1','3')"
			+"union"+" select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.appntname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom from ljaget a,lacharge"
			+" b,lbcont c where  a.otherno=b.receiptno and b.contno=c.contno and a.othernotype in ('BC','AC') and " +
					"(b.grpcontno='00000000000000000000' or b.grpcontno is null) and "
			+"b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' and b.transmoney<>0 " +
					"and b.transmoney is not null and b.charge<>0 and chargestate in('1','3')"
			+"union"
			+"  select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.grpname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom"
			                  +"   from ljaget a,lacharge b,lcgrpcont c"
			                  +"   where a.otherno=b.receiptno and b.grpcontno=c.grpcontno and a.othernotype in ('BC','AC') and "
			                  +"  b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' " +
			                  		"and"
			                  +"  b.transmoney<>0 and b.transmoney is not null"
			                   +" and b.grpcontno <> '00000000000000000000'"
			                    +"  and b.grpcontno is not null and b.charge<>0 and chargestate in('1','3')"
			+"  union" +
					" select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.grpname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom "
			                   +"  from ljaget a,lacharge b,lbgrpcont c"
			                   +"  where a.otherno=b.receiptno and b.grpcontno=c.grpcontno and a.othernotype in ('BC','AC') and "
			                   +"  b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' " +
			                   		"and "
			                   +"  b.transmoney<>0 and b.transmoney is not null"
			                   +"  and b.grpcontno <> '00000000000000000000'"
			                   +"  and b.grpcontno is not null and b.charge<>0 and chargestate in('1','3') with ur";
			tSSRS = tExeSQL.execSQL(msql);
			ListTable tlistTable = new ListTable();
			tlistTable.setName(tname);
			String strSum = "";
			String strArr[] = null;
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				strArr = new String[9];
				for (int j = 1; j <= tSSRS.MaxCol; j++) {
					if (j == 6) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						//System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa"+strArr[j - 1]);
						continue;
					}
					if (j == 7) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.000").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						//System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"+strArr[j - 1]);
						continue;
					}
					if (j == 8) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						//System.out.println("cccccccccccccccccccccccccccccccccc"+strArr[j - 1]);
						continue;
					}
					strArr[j - 1] = tSSRS.GetText(i, j);
				}
				tlistTable.add(strArr);
			}
			return tlistTable;
		}

		/**
		 * 获得汇总ListTable
		 *
		 * @param msql -
		 *            执行的SQL
		 * @param tName -
		 *            Table Name
		 * @return
		 */
		private ListTable getHZListTable(String[] totalArr,String pname) {
		    SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			String msql="select COALESCE(sum(tt.charge),0) from (select distinct b.* from ljaget a,lacharge b where a.otherno=b.receiptno and b.chargestate in('1','3') and a.othernotype in ('BC','AC')" +
			" and b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"') as tt with ur";
			tSSRS = tExeSQL.execSQL(msql);
			ListTable tlistTable = new ListTable();
			tlistTable.setName(pname);
			String strSum = "";
			String strSSRS = (String)tSSRS.GetText(1, 1);
			strSum = new DecimalFormat("0.00").format(Double.valueOf(strSSRS));
			totalArr[0] = strSum;
			tlistTable.add(totalArr);
			return tlistTable;

//			System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+totalArr[1]);
//			if (tSSRS.MaxRow > 0) {
//				for (int j = 1; j <= tSSRS.MaxCol; j++) {
//					totalArr[j] = tSSRS.GetText(1, j);
//					System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa"+totalArr[j]);
//					if(totalArr[j]!=null && (!"".equals(totalArr[j]))){
//						System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbb"+totalArr[j]);
//
//					}
//				}
//			}

		}

//		public static void main(String[] args){
//
//			SXFinDayCheckBL sSXFinDayCheckBL = new SXFinDayCheckBL();
//			VData t = new VData();
//			String[] mDay = {"2010-10-10","2010-10-10"};
//			t.add(mDay);
//			t.add("SXFDDS");
//			GlobalInput tG = new GlobalInput();
//			tG.ManageCom = "86";
//			t.add(tG);
//
//		}

		/**
		 * 以前的打印方法
		 *
		 * @return
		 */
		/*private boolean getPrintDataOld() {
			// String msql = "select otherno,sum(pay) a from LJAGetClaim where
			// MakeDate>='"+mDay[0]+"' and MakeDate<='"+mDay[1]+"' "
			// + "and ManageCom like '"+mGlobalInput.ManageCom+"%' and RiskCode not
			// in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode
			// not in ('170101','170301')) or RiskCode is null group by otherno"
			// + " union all select tempfeeno,sum(getmoney) from LJAGetTempFee where
			// MakeDate>='"+mDay[0]+"'"
			// + " and MakeDate<='"+mDay[1]+"' and ManageCom like
			// '"+mGlobalInput.ManageCom+"%' and RiskCode not in (Select RiskCode
			// From LMRiskApp Where Risktype3 = '7' and riskcode not in
			// ('170101','170301')) group by tempfeeno";
			GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
			tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
			tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
			tGetSQLFromXML.setParameters("EndDate", mDay[1]);
			String tServerPath = (new ExeSQL())
					.getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
			String msql = tGetSQLFromXML.getSql(tServerPath
					+ "f1print/picctemplate/FeePrintSql.xml", "ClaimDayBalance");

			if (!productXml(msql))
				return false;
			else
				return true;
		}
*/
		// 生成XML函数
		/*private boolean productXml(String msql) {
			SSRS tSSRS = new SSRS();
			SSRS nSSRS = new SSRS();
			double ESumMoney = 0;
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(msql);
			ListTable tlistTable = new ListTable();
			String strSum = "";
			String strArr[] = null;
			tlistTable.setName("CLAIM");
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				strArr = new String[2];
				for (int j = 1; j <= tSSRS.MaxCol; j++) {
					if (j == 1) {
						strArr[j - 1] = tSSRS.GetText(i, j);
					}
					if (j == 2) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;
						ESumMoney = ESumMoney + Double.parseDouble(strArr[j - 1]);
					}
				}
				tlistTable.add(strArr);
			}
			ESumMoney = PubFun.setPrecision(ESumMoney, "0.00");
			strArr = new String[2];
			strArr[0] = "CaseNo";
			strArr[1] = "Money";

			String nsql = "select Name from LDCom where ComCode='"
					+ mGlobalInput.ManageCom + "'";
			ExeSQL nExeSQL = new ExeSQL();
			nSSRS = nExeSQL.execSQL(nsql);
			String manageCom = nSSRS.GetText(1, 1);

			TextTag texttag = new TextTag(); // 新建一个TextTag的实例
			XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
			xmlexport.createDocument("ClaimDayCheck.vts", "printer"); // 最好紧接着就初始化xml文档
			texttag.add("StartDate", mDay[0]);
			texttag.add("EndDate", mDay[1]);
			texttag.add("ManageCom", manageCom);
			texttag.add("SumMoney", ESumMoney);

			if (texttag.size() > 0) {
				xmlexport.addTextTag(texttag);
			}
			xmlexport.addListTable(tlistTable, strArr);
			mResult.clear();
			mResult.addElement(xmlexport);
			xmlexport.outputDocumentToFile("E:\\", "ClaimDayBalance");
			return true;
		}*/
}
