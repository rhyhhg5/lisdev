package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys yh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GrpRiskPreReceiveExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// 取得的时间
	private String mDay[] = null;
	//取得公司代码
	private String sManageCom = "";
	//取得文件路径
	private String tOutXmlPath="";
	private String busiDate = "";
	private String sql = "";
	private String sql1 = "";
	private String sql2 = "";
	private String sql3 = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public GrpRiskPreReceiveExcelBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		sManageCom = (String)cInputData.get(2);
		tOutXmlPath=(String)cInputData.get(3);
		String[] endDate = mDay[1].split("-");
		for(int l = 0;l < endDate.length; l ++){
			busiDate = busiDate+endDate[l];
		}
		
		System.out.println("公司代码："+sManageCom+";打印日期："+mDay[0]+"至"+mDay[1]+";busiDate："+busiDate);
		
		if (sManageCom == null || sManageCom=="") {
			buildError("getInputData", "没有获得打印机构信息！");
			return false;
		}
		if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "GrpRiskPreReceiveExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintDataPay() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String Msql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(Msql);
		String manageCom = tSSRS.GetText(1, 1);
		
		String[][] mToExcel = new String[10000][10];
		mToExcel[0][0] = "应收保费报表(集团风险)";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mDay[0]+"至"+mDay[1];
		mToExcel[2][0] = "统计机构：";
		mToExcel[2][1] = manageCom;
		mToExcel[4][0] = "BUSI_DT";
		mToExcel[4][1] = "REPORT_ORGAN_CODE";
		mToExcel[4][2] = "BRANCH_CODE";
		mToExcel[4][3] = "PRODUCT_TYPE";
		mToExcel[4][4] = "CHANNEL";
		mToExcel[4][5] = "TERM_TYPE";
		mToExcel[4][6] = "POLICY_ORIENT";
		mToExcel[4][7] = "PAY_TYPE";
		mToExcel[4][8] = "AGING_INTERVAL";
		mToExcel[4][9] = "PREMIUM_RECEIVABLE";
		
		int no=5;  //初始打印行数
		if(!sManageCom.equals("86")){
			String nSql = "SELECT DISTINCT codealias FROM FiCODETRANS WHERE CODETYPE='ManageCom' AND CODE LIKE '"+sManageCom+"%'";
			tSSRS = tExeSQL.execSQL(nSql);
			String CodeAlias = "";
			for(int c = 1;c <= tSSRS.getMaxRow(); c++){
				if(c == 1){
					CodeAlias = "'"+tSSRS.GetText(c, 1)+"'";
				}else{
					CodeAlias = CodeAlias+",'"+tSSRS.GetText(c, 1)+"'";
				}
			}
			System.out.println("公司代码为："+CodeAlias);
			sql2 = "and managecom in("+CodeAlias+") ";
		}
		
		sql1 = "select busi_dt,report_organ_code,branch_code,product_type,channel,term_type,policy_orient,pay_type,'-' as aging_interval,sum(premium_receivable) as premium_receivable from("
				+"select '"+busiDate+"' as busi_dt,'000085' as report_organ_code,(select codealias from ficodetrans where codetype='FXManageCom' and a.managecom=code) as branch_code,"
				+"(case (select risktype from lmriskapp where riskcode=a.riskcode) when 'A' then '240' when 'L' then '215'"
				+"when 'H' then (case when (select risktype1 from lmriskapp where riskcode=a.riskcode) in('1','3','5','6','9') then '232'"
				+"when (select risktype1 from lmriskapp where riskcode=a.riskcode) ='2' then '231'"
				+"when (select risktype1 from lmriskapp where riskcode=a.riskcode) ='4' then '234'"
				+"else '233' end) else '290' end) as product_type,"
				+"(select codealias from ficodetrans where codetype='FXSaleChanl' and code=a.salechnl) as channel,"
				+"(case (select risktype5 from lmriskapp where riskcode=a.riskcode) when '1' then 'O' when '2' then 'S' when '4' then 'W' " 
				+"when '3' then (case (case listflag when '1' then"
				+"(select PayEndYearFlag from lcpol where contno=a.contno and riskcode=a.riskcode union select PayEndYearFlag from lbpol where contno=a.contno and riskcode=a.riskcode fetch first 1 rows only)"
				+"else"
				+"(select PayEndYearFlag from lcpol where grpcontno=a.contno and riskcode=a.riskcode union select PayEndYearFlag from lbpol where grpcontno=a.contno and riskcode=a.riskcode fetch first 1 rows only)"
				+"end) when 'Y' then 'Y' when 'A' then 'A' else 'B' end) else 'S' end) as term_type,"
				+"(case when (select 1 from LCRnewStateLog where contno=a.contno or newcontno=a.contno fetch first 1 rows only) is not null then '2' else '1' end) as policy_orient,"
				+"(case(select payintv from FIAbStandardData where serialno=a.serialno fetch first 1 rows only)" 
				+"when '0' then '1' when '1' then '2' when '3' then '3' when '6' then '4' when '12' then '5' when '-1' then '6' else '9' end) as pay_type,"
				+"(case finitemtype when 'D' then summoney else -summoney end) as premium_receivable "
				+"from fivoucherdatadetail a "
				+"where accountdate between '"+mDay[0]+"' and '"+mDay[1]+"' "
				+"and accountcode='1122010000' and a.checkflag = '00'" ;
		sql3 = "and length(managecom)=4 )as bb where 1=1 "
				+"group by busi_dt,report_organ_code,branch_code,product_type,channel,term_type,policy_orient,pay_type "
				+"having sum(premium_receivable)<>0 ";
		
		if(sManageCom.equals("86")){
			sql = sql1+sql3;
		}else{
			sql = sql1+sql2+sql3;
		}
		tSSRS = tExeSQL.execSQL(sql);
		int maxrow=tSSRS.getMaxRow();
		for (int row = 1; row <= maxrow; row++)
        {
            for (int col = 1; col <=10; col++)
            { 
            	if (col==10) {
            		mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
				}else {
					mToExcel[no+row-1][col - 1] = tSSRS.GetText(row, col);  
				}
             }
        }
		no=no+maxrow+2;

		//添加制表员和审核员
		mToExcel[no+1][0] = "制表员：";
		mToExcel[no+1][2] = "审核员：";
		 try
	        {
	            System.out.println(tOutXmlPath);
	            WriteToExcel t = new WriteToExcel("");
	            t.createExcelFile();
	            String[] sheetName = { PubFun.getCurrentDate() };
	            t.addSheet(sheetName);
	            t.setData(0, mToExcel);
	            t.write(tOutXmlPath);
	            System.out.println("生成文件完成");
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	            buiError("dealData", ex.toString());
	            return false;
	        }
		return true;
	}

    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "GrpRiskPreReceiveExcelBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
}
