package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;

public class LLRsuCompareBL
    implements PrintService {

/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

private VData mResult = new VData();

//取得的保单号码
private String mContNo = "";

//输入的查询sql语句
private String msql = "";

//取得的延期承保原因
private String mUWError = "";

//取得的代理人编码
private String mAgentCode = "";

//业务处理相关变量
/** 全局数据 */
private GlobalInput mGlobalInput = new GlobalInput();
private TransferData mTransferData = new TransferData();
private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

private LCContSchema mLCContSchema = new LCContSchema();
private LCContSet mLCContSet = new LCContSet();
private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
private LLAppealSchema mLLAppealSchema = new LLAppealSchema(); //理赔申诉错误表
private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
        LLAppClaimReasonSchema();
private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
private String mFileNameB = "";
private String moperator = "";
private String mMakeDate = "";
public LLRsuCompareBL() {
}

/**
 传输数据的公共方法
 */
public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
        buildError("submitData", "不支持的操作字符串");
        return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
        return false;
    }
    System.out.println("1 ...");
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
        return false;
    }else{
		TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("tFileNameB",mFileNameB );
		tTransferData.setNameAndValue("tMakeDate", mMakeDate);
		tTransferData.setNameAndValue("tOperator", moperator);
		LLPrintSave tLLPrintSave = new LLPrintSave();
		VData tVData = new VData();
		tVData.addElement(tTransferData);
		if(!tLLPrintSave.submitData(tVData,"")){
			return false;
		     }
	}
    return true;
}

/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData() {
    return true;
}

/**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) {
    //全局变量
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    mTransferData = ((TransferData) cInputData.getObjectByObjectName(
            "TransferData", 0));

    if (mTransferData == null) {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }
    mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
    moperator = mGlobalInput.Operator;
    return true;
}

public VData getResult() {
    return mResult;
}

public CErrors getErrors() {
    return mErrors;
}

private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

private boolean getPrintData() {
    boolean PEFlag = false; //打印理赔申请回执的判断标志
    boolean shortFlag = false; //打印备注信息
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    //根据印刷号查询打印队列中的纪录
    String[] strCol;
    String SpanType = (String) mTransferData.getValueByName("SpanType");
    String MStartDate = (String) mTransferData.getValueByName("MStartDate");
    String MEndDate = (String) mTransferData.getValueByName("MEndDate");
    String YStartDate = (String) mTransferData.getValueByName("YStartDate");
    String YEndDate = (String) mTransferData.getValueByName("YEndDate");

    String Year = (String) mTransferData.getValueByName("Year");
    String Month = (String) mTransferData.getValueByName("Month");
    String Days = (String) mTransferData.getValueByName("Days");
    String NStartDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ "01";
    String NEndDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ Days;
    String OYStartDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ "01" +"-"+"01";
   String OYEndDate =  String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ Days;

    ListTable appColListTable = new ListTable();
    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    System.out.print("MStartDate"+MStartDate+"NStartDate"+NStartDate+"MEndDate"+MEndDate+"NEndDate"+NEndDate);
    appColListTable.setName("Report");

    String sql0 = "select  d.comcode,d.name from ldcom d  where comcode in (select distinct mngcom from llcase) or comcode='86' order by comcode desc";

    tSSRS = tExeSQL.execSQL(sql0);

    System.out.println("row=" + tSSRS.getMaxRow());

    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        String[] strArr = new String[25];
if(SpanType.equals("1")){
        String Sql1 =
                "SELECT COUNT(B5.CASENO),coalesce(coalesce(SUM(C5.REALPAY),0),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'" +
                " AND C5.GIVETYPE='1' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql1);
        String NormalPayCount = kSSRS.GetText(1, 1);
        String NormalPayMoney = kSSRS.GetText(1, 2);

        String Sql2 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'" +
                " AND C5.GIVETYPE='2' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql2);
        String PartPayCount = kSSRS.GetText(1, 1);
        String PartPayMoney = kSSRS.GetText(1, 2);

        String Sql3 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'" +
                " AND C5.GIVETYPE='3' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql3);
        String AccomPayCount = kSSRS.GetText(1, 1);
        String AccomPayMoney = kSSRS.GetText(1, 2);

        String Sql4 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'" +
                " AND C5.GIVETYPE='4' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql4);
        String FullPayCount = kSSRS.GetText(1, 1);
        String FullPayMoney = kSSRS.GetText(1, 2);

        String Sql5 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'" +
                " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql5);
        String ProtocolPayCount = kSSRS.GetText(1, 1);
        String ProtocolPayMoney = kSSRS.GetText(1, 2);

        String Sql6 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' " +
                " AND C5.GIVETYPE='1' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql6);
        String NNormalPayCount = kSSRS.GetText(1, 1);
        String NNormalPayMoney = kSSRS.GetText(1, 2);

        String Sql7 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' " +
                " AND C5.GIVETYPE='2' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql7);
        String NPartPayCount = kSSRS.GetText(1, 1);
        String NPartPayMoney = kSSRS.GetText(1, 2);

        String Sql8 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' " +
                " AND C5.GIVETYPE='3' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql8);
        String NAccomPayCount = kSSRS.GetText(1, 1);
        String NAccomPayMoney = kSSRS.GetText(1, 2);

        String Sql9 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' " +
                " AND C5.GIVETYPE='4' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql9);
        String NFullPayCount = kSSRS.GetText(1, 1);
        String NFullPayMoney = kSSRS.GetText(1, 2);

        String Sql10 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' "+
                " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql10);
        String NProtocolPayCount = kSSRS.GetText(1, 1);
        String NProtocolPayMoney = kSSRS.GetText(1, 2);
        String Sql11 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"' "+
                " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
        kSSRS = tExeSQL.execSQL(Sql11);
        String AllPayCount = kSSRS.GetText(1, 1);
        String AllPayMoney = kSSRS.GetText(1, 2);
        String Sql12 =
                "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
                " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
                tSSRS.GetText(i, 1) + "%'" +
                " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' "+
                " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14'";
        kSSRS = tExeSQL.execSQL(Sql12);
        String NAllPayCount = kSSRS.GetText(1, 1);
        String NAllPayMoney = kSSRS.GetText(1, 2);
        String Sql13 =
                "SELECT COUNT(DISTINCT b.CUSTOMERNO) FROM LLCASE b "+
                " WHERE b.ENDCASEDATE IS NOT NULL and b.mngcom like ' "+tSSRS.GetText(i, 1) + "%'" +
                " AND b.RGTTYPE='1'  AND b.ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"' AND b.RGTSTATE<>'14' " ;
        kSSRS = tExeSQL.execSQL(Sql13);
        String PeopleCount = kSSRS.GetText(1, 1);

        String Sql14 =
                "SELECT COUNT(DISTINCT b.CUSTOMERNO) FROM LLCASE b "+
               " WHERE b.ENDCASEDATE IS NOT NULL and b.mngcom like ' "+tSSRS.GetText(i, 1) + "%'" +
               " AND b.RGTTYPE='1'  AND b.ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"' AND b.RGTSTATE<>'14' " ;
        kSSRS = tExeSQL.execSQL(Sql14);
        String NPeopleCount = kSSRS.GetText(1, 1);
        String Sql16 =
                 "  select coalesce(sum(B),0) from (select ManageCom,case when payintv<>0 then  (days('"+NEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+NEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+NStartDate+"' and '"+NEndDate+"' and enddate>'"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'    union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+NStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+NStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+NStartDate+"' and enddate  between '"+NStartDate+"' and '"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+NEndDate+"')-days('"+NStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+NEndDate+"')-days('"+NStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+NStartDate+"' and enddate >'"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+NStartDate+"' and '"+NEndDate+"' and enddate<='"+NEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='1'  union all "+

                 " select ManageCom,case when payintv<>0 then  (days('"+NEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+NEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+NStartDate+"' and '"+NEndDate+"' and enddate>'"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'   union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+NStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+NStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+NStartDate+"' and enddate  between '"+NStartDate+"' and '"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+NEndDate+"')-days('"+NStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+NEndDate+"')-days('"+NStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+NStartDate+"' and enddate >'"+NEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+NStartDate+"' and '"+NEndDate+"' and enddate<='"+NEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='2'  " +

                 " ) Y  where   SUBSTR(ManageCOM,1,LENGTH(TRIM('"+tSSRS.GetText(i, 1)+"')))='"+tSSRS.GetText(i, 1)+"'";

          kSSRS = tExeSQL.execSQL(Sql16);
         String NRate  = kSSRS.GetText(1, 1);
        System.out.print("Sql16="+Sql16+"NRate"+NRate);
        String Sql15 =
                "  select coalesce(sum(B),0) from (select ManageCom,case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'    union all "+

                " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                " select ManageCom,case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='1'  union all "+

                " select ManageCom,case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'   union all "+

                " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                " select ManageCom,case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='2'  " +

                " ) Y  where   SUBSTR(ManageCOM,1,LENGTH(TRIM('"+tSSRS.GetText(i, 1)+"')))='"+tSSRS.GetText(i, 1)+"'";

         kSSRS = tExeSQL.execSQL(Sql15);
        String Rate  = kSSRS.GetText(1, 1);
        System.out.print("Sql15="+Sql15+"Rate"+Rate);
        String Sql17 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12' "+
                " and b.caseGetMode in ('1','2')  and b.rgttype='1' "+
                " and  b.endcasedate  between '"+MStartDate+"' AND '"+MEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14'" ;

        kSSRS = tExeSQL.execSQL(Sql17);
        String CashCount  = kSSRS.GetText(1, 1);
        String CashMoney  = kSSRS.GetText(1, 2);
        String Sql18 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12' "+
                " and b.caseGetMode in ('1','2')  and b.rgttype='1' "+
                " and  b.endcasedate  between '"+NStartDate+"' AND '"+NEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

        kSSRS = tExeSQL.execSQL(Sql18);
        String NCashCount  = kSSRS.GetText(1, 1);
        String NCashMoney  = kSSRS.GetText(1, 2);
        String Sql19 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12'"+
                " and b.caseGetMode in ('4','11')  and b.rgttype='1' "+
                " and  b.endcasedate  between '"+MStartDate+"' AND '"+MEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

        kSSRS = tExeSQL.execSQL(Sql19);
        String TransfersCount  = kSSRS.GetText(1, 1);
        String TransfersMoney  = kSSRS.GetText(1, 2);
        String Sql120 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12'"+
                " and b.caseGetMode in ('4','11')  and b.rgttype='1' "+
                " and  b.endcasedate  between '"+NStartDate+"' AND '"+NEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

        kSSRS = tExeSQL.execSQL(Sql120);
        String NTransfersCount  = kSSRS.GetText(1, 1);
        String NTransfersMoney  = kSSRS.GetText(1, 2);
        String Sql21 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate <> '12'"+
                " and b.rgttype='1' "+
                " and  b.endcasedate  between '"+MStartDate+"' AND '"+MEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

        kSSRS = tExeSQL.execSQL(Sql21);
        String UnfinishedCount   = kSSRS.GetText(1, 1);
        String UnfinishedMoney   = kSSRS.GetText(1, 2);
        String Sql22 =
                "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
                " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate<>'12' "+
                " and b.rgttype='1' "+
                " and  b.endcasedate  between '"+NStartDate+"' AND '"+NEndDate+"' "+
                " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

        kSSRS = tExeSQL.execSQL(Sql22);
        String NUnfinishedCount   = kSSRS.GetText(1, 1);
        String NUnfinishedMoney   = kSSRS.GetText(1, 2);




        if (tSSRS.GetText(i, 1).equals("86")) {
            strArr[1] = " ";
        } else {
            strArr[0] = tSSRS.GetText(i, 1);
        }
        if (tSSRS.GetText(i, 2).equals("总公司"))
        {
            strArr[1]="总   计";
        }else{
            strArr[1] = tSSRS.GetText(i, 2);
        }
        if (NNormalPayCount.equals("0")||NormalPayCount.equals("0")) {
            strArr[2] = "0";
        } else {
            strArr[2] = String.valueOf(Arith.round(Double.parseDouble(NormalPayCount) /
                                       Double.parseDouble(NNormalPayCount),2));
        }
        System.out.print(NNormalPayMoney);
        if (NNormalPayMoney.equals("0")||NormalPayMoney .equals("0")) {
            strArr[3] = "0";
        } else {
            strArr[3] = String.valueOf(Arith.round(Double.parseDouble(NormalPayMoney) /
                                       Double.parseDouble(NNormalPayMoney),2));
        }
        if (NPartPayCount.equals("0")||PartPayCount.equals("0")) {
            strArr[4] = "0";
        } else {
            strArr[4] = String.valueOf(Arith.round(Double.parseDouble(PartPayCount) /
                                       Double.parseDouble(NPartPayCount),2));
        }
        if (NPartPayMoney.equals("0")||PartPayMoney.equals("0")) {
            strArr[5] = "0";
        } else {
            strArr[5] = String.valueOf(Arith.round(Double.parseDouble(PartPayMoney) /
                                       Double.parseDouble(NPartPayMoney),2));
        }
        if (NAccomPayCount.equals("0")||AccomPayCount.equals("0")) {
            strArr[6] = "0";
        } else {
            strArr[6] = String.valueOf(Arith.round(Double.parseDouble(AccomPayCount) /
                                       Double.parseDouble(NAccomPayCount),2));
        }
        if (NAccomPayMoney.equals("0")||AccomPayMoney.equals("0")) {
            strArr[7] = "0";
        } else {
            strArr[7] = String.valueOf(Arith.round(Double.parseDouble(AccomPayMoney) /
                                       Double.parseDouble(NAccomPayMoney),2));
        }
        if (NFullPayCount.equals("0")||FullPayCount.equals("0")) {
            strArr[8] = "0";
        } else {
            strArr[8] = String.valueOf(Arith.round(Double.parseDouble(FullPayCount) /
                                       Double.parseDouble(NFullPayCount),2));
        }
        if (NFullPayMoney.equals("0")||FullPayMoney.equals("0")) {
            strArr[9] = "0";
        } else {
            strArr[9] = String.valueOf(Arith.round(Double.parseDouble(FullPayMoney) /
                                       Double.parseDouble(NFullPayMoney),2));
        }
        if (NProtocolPayCount.equals("0")||ProtocolPayCount.equals("0")) {
            strArr[10] = "0";
        } else {
            strArr[10] = String.valueOf(Arith.round(Double.parseDouble(ProtocolPayCount) /
                                       Double.parseDouble(NProtocolPayCount),2));
        }
        if (NProtocolPayMoney.equals("0")||ProtocolPayMoney.equals("0")) {
            strArr[11] = "0";
        } else {
            strArr[11] = String.valueOf(Arith.round(Double.parseDouble(ProtocolPayMoney) /
                                       Double.parseDouble(NProtocolPayMoney),2));

        }
        if (NAllPayCount.equals("0") || AllPayCount.equals("0")) {
            strArr[12] = "0";
        } else {
            strArr[12] = String.valueOf(Arith.round(Double.parseDouble(AllPayCount) /
                                        Double.parseDouble(NAllPayCount),2));

        }
        if (NAllPayMoney.equals("0") || AllPayMoney.equals("0")) {
            strArr[13] = "0";
        } else {
            strArr[13] = String.valueOf(Arith.round(Double.parseDouble(AllPayMoney) /
                                        Double.parseDouble(NAllPayMoney),2));

        }
        if (NPeopleCount.equals("0") || PeopleCount.equals("0")) {
            strArr[14] = "0";
        } else {
            strArr[14] = String.valueOf(Arith.round(Double.parseDouble(PeopleCount) /
                                        Double.parseDouble(NPeopleCount),2));

        }
        if (NRate.equals("0") || Rate.equals("0") ) {
            strArr[15] = "0";
        } else {
            strArr[15] = String.valueOf(Arith.round((Double.parseDouble(AllPayMoney) /Double.parseDouble(Rate) )/
                                        (Double.parseDouble(NAllPayMoney) /Double.parseDouble(NRate)),2));

        }
        if (NCashCount.equals("0") || CashCount.equals("0")) {
           strArr[16] = "0";
       } else {
           strArr[16] = String.valueOf(Arith.round(Double.parseDouble(CashCount) /
                                       Double.parseDouble(NCashCount),2));

       }
       if (NCashMoney.equals("0") || CashMoney.equals("0")) {
           strArr[17] = "0";
       } else {
           strArr[17] = String.valueOf(Arith.round(Double.parseDouble(CashMoney) /
                                       Double.parseDouble(NCashMoney),2));

       }
       if (NTransfersCount.equals("0") || TransfersCount.equals("0")) {
           strArr[18] = "0";
       } else {
           strArr[18] = String.valueOf(Arith.round(Double.parseDouble(TransfersCount) /
                                       Double.parseDouble(NTransfersCount),2));

       }
       if (NTransfersMoney.equals("0") || TransfersMoney.equals("0")) {
           strArr[19] = "0";
       } else {
           strArr[19] = String.valueOf(Arith.round(Double.parseDouble(TransfersMoney) /
                                       Double.parseDouble(NTransfersMoney),2));

       }
       if (NUnfinishedCount.equals("0") || UnfinishedCount.equals("0")) {
           strArr[20] = "0";
       } else {
           strArr[20] = String.valueOf(Arith.round(Double.parseDouble(UnfinishedCount) /
                                       Double.parseDouble(NUnfinishedCount),2));

       }
       if (NUnfinishedMoney.equals("0") || UnfinishedMoney.equals("0")) {
           strArr[21] = "0";
       } else {
           strArr[21] = String.valueOf(Arith.round(Double.parseDouble(UnfinishedMoney) /
                                       Double.parseDouble(NUnfinishedMoney),2));

       }
}else{
    String Sql1 =
               "SELECT COUNT(B5.CASENO),coalesce(coalesce(SUM(C5.REALPAY),0),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'" +
               " AND C5.GIVETYPE='1' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql1);
       String NormalPayCount = kSSRS.GetText(1, 1);
       String NormalPayMoney = kSSRS.GetText(1, 2);

       String Sql2 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'" +
               " AND C5.GIVETYPE='2' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql2);
       String PartPayCount = kSSRS.GetText(1, 1);
       String PartPayMoney = kSSRS.GetText(1, 2);

       String Sql3 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'" +
               " AND C5.GIVETYPE='3' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql3);
       String AccomPayCount = kSSRS.GetText(1, 1);
       String AccomPayMoney = kSSRS.GetText(1, 2);

       String Sql4 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'" +
               " AND C5.GIVETYPE='4' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql4);
       String FullPayCount = kSSRS.GetText(1, 1);
       String FullPayMoney = kSSRS.GetText(1, 2);

       String Sql5 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'" +
               " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql5);
       String ProtocolPayCount = kSSRS.GetText(1, 1);
       String ProtocolPayMoney = kSSRS.GetText(1, 2);

       String Sql6 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' " +
               " AND C5.GIVETYPE='1' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql6);
       String NNormalPayCount = kSSRS.GetText(1, 1);
       String NNormalPayMoney = kSSRS.GetText(1, 2);

       String Sql7 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' " +
               " AND C5.GIVETYPE='2' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql7);
       String NPartPayCount = kSSRS.GetText(1, 1);
       String NPartPayMoney = kSSRS.GetText(1, 2);

       String Sql8 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' " +
               " AND C5.GIVETYPE='3' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql8);
       String NAccomPayCount = kSSRS.GetText(1, 1);
       String NAccomPayMoney = kSSRS.GetText(1, 2);

       String Sql9 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' " +
               " AND C5.GIVETYPE='4' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql9);
       String NFullPayCount = kSSRS.GetText(1, 1);
       String NFullPayMoney = kSSRS.GetText(1, 2);

       String Sql10 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' "+
               " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql10);
       String NProtocolPayCount = kSSRS.GetText(1, 1);
       String NProtocolPayMoney = kSSRS.GetText(1, 2);
       String Sql11 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"' "+
               " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14' ";
       kSSRS = tExeSQL.execSQL(Sql11);
       String AllPayCount = kSSRS.GetText(1, 1);
       String AllPayMoney = kSSRS.GetText(1, 2);
       String Sql12 =
               "SELECT COUNT(B5.CASENO),coalesce(SUM(C5.REALPAY),0) FROM  LLCASE B5 ,LLCLAIM C5  " +
               " WHERE  B5.CASENO=C5.CASENO AND B5.ENDCASEDATE IS NOT NULL AND B5.MNGCOM like'" +
               tSSRS.GetText(i, 1) + "%'" +
               " AND B5.RGTTYPE='1'  AND B5.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' "+
               " AND C5.GIVETYPE='5' AND B5.RGTSTATE<>'14'";
       kSSRS = tExeSQL.execSQL(Sql12);
       String NAllPayCount = kSSRS.GetText(1, 1);
       String NAllPayMoney = kSSRS.GetText(1, 2);
       String Sql13 =
               "SELECT COUNT(DISTINCT b.CUSTOMERNO) FROM LLCASE b "+
               " WHERE b.ENDCASEDATE IS NOT NULL and b.mngcom like ' "+tSSRS.GetText(i, 1) + "%'" +
               " AND b.RGTTYPE='1'  AND b.ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"' AND b.RGTSTATE<>'14' " ;
       kSSRS = tExeSQL.execSQL(Sql13);
       String PeopleCount = kSSRS.GetText(1, 1);

       String Sql14 =
               "SELECT COUNT(DISTINCT b.CUSTOMERNO) FROM LLCASE b "+
              " WHERE b.ENDCASEDATE IS NOT NULL and b.mngcom like ' "+tSSRS.GetText(i, 1) + "%'" +
              " AND b.RGTTYPE='1'  AND b.ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"' AND b.RGTSTATE<>'14' " ;
       kSSRS = tExeSQL.execSQL(Sql14);
       String NPeopleCount = kSSRS.GetText(1, 1);
       String Sql16 =
                "  select coalesce(sum(B),0) from (select ManageCom,case when payintv<>0 then  (days('"+OYEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+OYEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+OYStartDate+"' and '"+OYEndDate+"' and enddate>'"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'    union all "+

                " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+OYStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+OYStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+OYStartDate+"' and enddate  between '"+OYStartDate+"' and '"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                " select ManageCom,case  when payintv<>0 then (days('"+OYEndDate+"')-days('"+OYStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+OYEndDate+"')-days('"+OYStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+OYStartDate+"' and enddate >'"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

                " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+OYStartDate+"' and '"+OYEndDate+"' and enddate<='"+OYEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='1'  union all "+

                " select ManageCom,case when payintv<>0 then  (days('"+OYEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+OYEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+OYStartDate+"' and '"+OYEndDate+"' and enddate>'"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'   union all "+

                " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+OYStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+OYStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+OYStartDate+"' and enddate  between '"+OYStartDate+"' and '"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                " select ManageCom,case  when payintv<>0 then (days('"+OYEndDate+"')-days('"+OYStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+OYEndDate+"')-days('"+OYStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+OYStartDate+"' and enddate >'"+OYEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

                " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+OYStartDate+"' and '"+OYEndDate+"' and enddate<='"+OYEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='2'  " +

                " ) Y  where   SUBSTR(ManageCOM,1,LENGTH(TRIM('"+tSSRS.GetText(i, 1)+"')))='"+tSSRS.GetText(i, 1)+"'";

         kSSRS = tExeSQL.execSQL(Sql16);
        String NRate  = kSSRS.GetText(1, 1);
       System.out.print("Sql16="+Sql16+"NRate"+NRate);
       String Sql15 =
               "  select coalesce(sum(B),0) from (select ManageCom,case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'    union all "+

               " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

               " select ManageCom,case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='1'  union all "+

               " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='1'  union all "+

               " select ManageCom,case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'   union all "+

               " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate)) when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

               " select ManageCom,case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate))  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode<>'1605' and conttype='2'  union all "+

               " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.65/payintv/(days(enddate)-days(cvalidate))  when payintv=0  then (days(enddate)-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode<>'1605'  and conttype='2'  " +

               " ) Y  where   SUBSTR(ManageCOM,1,LENGTH(TRIM('"+tSSRS.GetText(i, 1)+"')))='"+tSSRS.GetText(i, 1)+"'";

        kSSRS = tExeSQL.execSQL(Sql15);
       String Rate  = kSSRS.GetText(1, 1);
       System.out.print("Sql15="+Sql15+"Rate"+Rate);
       String Sql17 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12' "+
               " and b.caseGetMode in ('1','2')  and b.rgttype='1' "+
               " and  b.endcasedate  between '"+YStartDate+"' AND '"+YEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14'" ;

       kSSRS = tExeSQL.execSQL(Sql17);
       String CashCount  = kSSRS.GetText(1, 1);
       String CashMoney  = kSSRS.GetText(1, 2);
       String Sql18 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12' "+
               " and b.caseGetMode in ('1','2')  and b.rgttype='1' "+
               " and  b.endcasedate  between '"+OYStartDate+"' AND '"+OYEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

       kSSRS = tExeSQL.execSQL(Sql18);
       String NCashCount  = kSSRS.GetText(1, 1);
       String NCashMoney  = kSSRS.GetText(1, 2);
       String Sql19 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12'"+
               " and b.caseGetMode in ('4','11')  and b.rgttype='1' "+
               " and  b.endcasedate  between '"+YStartDate+"' AND '"+YEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

       kSSRS = tExeSQL.execSQL(Sql19);
       String TransfersCount  = kSSRS.GetText(1, 1);
       String TransfersMoney  = kSSRS.GetText(1, 2);
       String Sql120 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate = '12'"+
               " and b.caseGetMode in ('4','11')  and b.rgttype='1' "+
               " and  b.endcasedate  between '"+OYStartDate+"' AND '"+OYEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

       kSSRS = tExeSQL.execSQL(Sql120);
       String NTransfersCount  = kSSRS.GetText(1, 1);
       String NTransfersMoney  = kSSRS.GetText(1, 2);
       String Sql21 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate <> '12'"+
               " and b.rgttype='1' "+
               " and  b.endcasedate  between '"+YStartDate+"' AND '"+YEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

       kSSRS = tExeSQL.execSQL(Sql21);
       String UnfinishedCount   = kSSRS.GetText(1, 1);
       String UnfinishedMoney   = kSSRS.GetText(1, 2);
       String Sql22 =
               "SELECT COUNT(b.caseno),coalesce(sum(c.realpay),0) from llcase b, llclaim c"+
               " where b.caseno = c.caseno and b.endcasedate is not null and b.rgtstate<>'12' "+
               " and b.rgttype='1' "+
               " and  b.endcasedate  between '"+OYStartDate+"' AND '"+OYEndDate+"' "+
               " and c.mngcom like'"+tSSRS.GetText(i, 1) + "%' AND b.RGTSTATE<>'14' " ;

       kSSRS = tExeSQL.execSQL(Sql22);
       String NUnfinishedCount   = kSSRS.GetText(1, 1);
       String NUnfinishedMoney   = kSSRS.GetText(1, 2);




       if (tSSRS.GetText(i, 1).equals("86")) {
           strArr[1] = " ";
       } else {
           strArr[0] = tSSRS.GetText(i, 1);
       }
       if (tSSRS.GetText(i, 2).equals("总公司"))
       {
           strArr[1]="总   计";
       }else{
           strArr[1] = tSSRS.GetText(i, 2);
       }
       if (NNormalPayCount.equals("0")||NormalPayCount.equals("0")) {
           strArr[2] = "0";
       } else {
           strArr[2] = String.valueOf(Arith.round(Double.parseDouble(NormalPayCount) /
                                      Double.parseDouble(NNormalPayCount),2));
       }
       System.out.print(NNormalPayMoney);
       if (NNormalPayMoney.equals("0")||NormalPayMoney .equals("0")) {
           strArr[3] = "0";
       } else {
           strArr[3] = String.valueOf(Arith.round(Double.parseDouble(NormalPayMoney) /
                                      Double.parseDouble(NNormalPayMoney),2));
       }
       if (NPartPayCount.equals("0")||PartPayCount.equals("0")) {
           strArr[4] = "0";
       } else {
           strArr[4] = String.valueOf(Arith.round(Double.parseDouble(PartPayCount) /
                                      Double.parseDouble(NPartPayCount),2));
       }
       if (NPartPayMoney.equals("0")||PartPayMoney.equals("0")) {
           strArr[5] = "0";
       } else {
           strArr[5] = String.valueOf(Arith.round(Double.parseDouble(PartPayMoney) /
                                      Double.parseDouble(NPartPayMoney),2));
       }
       if (NAccomPayCount.equals("0")||AccomPayCount.equals("0")) {
           strArr[6] = "0";
       } else {
           strArr[6] = String.valueOf(Arith.round(Double.parseDouble(AccomPayCount) /
                                      Double.parseDouble(NAccomPayCount),2));
       }
       if (NAccomPayMoney.equals("0")||AccomPayMoney.equals("0")) {
           strArr[7] = "0";
       } else {
           strArr[7] = String.valueOf(Arith.round(Double.parseDouble(AccomPayMoney) /
                                      Double.parseDouble(NAccomPayMoney),2));
       }
       if (NFullPayCount.equals("0")||FullPayCount.equals("0")) {
           strArr[8] = "0";
       } else {
           strArr[8] = String.valueOf(Arith.round(Double.parseDouble(FullPayCount) /
                                      Double.parseDouble(NFullPayCount),2));
       }
       if (NFullPayMoney.equals("0")||FullPayMoney.equals("0")) {
           strArr[9] = "0";
       } else {
           strArr[9] = String.valueOf(Arith.round(Double.parseDouble(FullPayMoney) /
                                      Double.parseDouble(NFullPayMoney),2));
       }
       if (NProtocolPayCount.equals("0")||ProtocolPayCount.equals("0")) {
           strArr[10] = "0";
       } else {
           strArr[10] = String.valueOf(Arith.round(Double.parseDouble(ProtocolPayCount) /
                                      Double.parseDouble(NProtocolPayCount),2));
       }
       if (NProtocolPayMoney.equals("0")||ProtocolPayMoney.equals("0")) {
           strArr[11] = "0";
       } else {
           strArr[11] = String.valueOf(Arith.round(Double.parseDouble(ProtocolPayMoney) /
                                      Double.parseDouble(NProtocolPayMoney),2));

       }
       if (NAllPayCount.equals("0") || AllPayCount.equals("0")) {
           strArr[12] = "0";
       } else {
           strArr[12] = String.valueOf(Arith.round(Double.parseDouble(AllPayCount) /
                                       Double.parseDouble(NAllPayCount),2));

       }
       if (NAllPayMoney.equals("0") || AllPayMoney.equals("0")) {
           strArr[13] = "0";
       } else {
           strArr[13] = String.valueOf(Arith.round(Double.parseDouble(AllPayMoney) /
                                       Double.parseDouble(NAllPayMoney),2));

       }
       if (NPeopleCount.equals("0") || PeopleCount.equals("0")) {
           strArr[14] = "0";
       } else {
           strArr[14] = String.valueOf(Arith.round(Double.parseDouble(PeopleCount) /
                                       Double.parseDouble(NPeopleCount),2));

       }
       if (NRate.equals("0") || Rate.equals("0") ) {
           strArr[15] = "0";
       } else {
           strArr[15] = String.valueOf(Arith.round((Double.parseDouble(AllPayMoney) /Double.parseDouble(Rate) )/
                                       (Double.parseDouble(NAllPayMoney) /Double.parseDouble(NRate)),2));

       }
       if (NCashCount.equals("0") || CashCount.equals("0")) {
          strArr[16] = "0";
      } else {
          strArr[16] = String.valueOf(Arith.round(Double.parseDouble(CashCount) /
                                      Double.parseDouble(NCashCount),2));

      }
      if (NCashMoney.equals("0") || CashMoney.equals("0")) {
          strArr[17] = "0";
      } else {
          strArr[17] = String.valueOf(Arith.round(Double.parseDouble(CashMoney) /
                                      Double.parseDouble(NCashMoney),2));

      }
      if (NTransfersCount.equals("0") || TransfersCount.equals("0")) {
          strArr[18] = "0";
      } else {
          strArr[18] = String.valueOf(Arith.round(Double.parseDouble(TransfersCount) /
                                      Double.parseDouble(NTransfersCount),2));

      }
      if (NTransfersMoney.equals("0") || TransfersMoney.equals("0")) {
          strArr[19] = "0";
      } else {
          strArr[19] = String.valueOf(Arith.round(Double.parseDouble(TransfersMoney) /
                                      Double.parseDouble(NTransfersMoney),2));

      }
      if (NUnfinishedCount.equals("0") || UnfinishedCount.equals("0")) {
          strArr[20] = "0";
      } else {
          strArr[20] = String.valueOf(Arith.round(Double.parseDouble(UnfinishedCount) /
                                      Double.parseDouble(NUnfinishedCount),2));

      }
      if (NUnfinishedMoney.equals("0") || UnfinishedMoney.equals("0")) {
          strArr[21] = "0";
      } else {
          strArr[21] = String.valueOf(Arith.round(Double.parseDouble(UnfinishedMoney) /
                                      Double.parseDouble(NUnfinishedMoney),2));

      }

}

        appColListTable.add(strArr);

    }


        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLRsuCompare.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        String StaDate = "";
        if (SpanType.equals("1")) {
            texttag.add("TableName", "月理赔同比");
            StaDate = Year + "年" + Month + "月";
        }
        if (SpanType.equals("2")) {
            texttag.add("TableName", "年理赔同比");
            StaDate = Year + "年";
        }
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
        //PayColPrtBL模版元素
        texttag.add("SysDate", SysDate);
        texttag.add("StaDate", StaDate);
        String Operator=mGlobalInput.Operator;
        texttag.add("Operator", Operator);
        String CurrentDate = PubFun.getCurrentDate();
        CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
        System.out.print("CurrentDate"+CurrentDate);
        mMakeDate = CurrentDate;
        texttag.add("CurrentDate", CurrentDate);
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        String[] Title = {
                     "ComCode", "ComName", "MCaseCount", "MSubRptCount",
                     "MPayAmnt", "MPayRate", "MSpan","YCaseCount",
                     "YSubRptCount","YPayAmnt", "YPayRate","YSpan"};

        //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
//    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

       return true;
    }


public static void main(String[] args) {
    TransferData PrintElement = new TransferData();
    PrintElement.setNameAndValue("MStartDate", "2006-6-1");
    PrintElement.setNameAndValue("MEndDate", "2006-6-30");
    PrintElement.setNameAndValue("YStartDate", "2006-1-1");
    PrintElement.setNameAndValue("YEndDate", "2006-6-30");
    PrintElement.setNameAndValue("Year", "2006");
    PrintElement.setNameAndValue("Month", "6");
    PrintElement.setNameAndValue("Days", "30");
    PrintElement.setNameAndValue("SpanType", "1");
    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86110000";
    tG.Operator = "001";
    VData tVData = new VData();
    tVData.addElement(PrintElement);
    tVData.addElement(tG);

    LLRsuCompareUI tLLRsuCompareUI = new LLRsuCompareUI();
    tLLRsuCompareUI.submitData(tVData, "PRINT");
}
}
