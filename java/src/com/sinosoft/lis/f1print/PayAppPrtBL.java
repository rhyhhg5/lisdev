package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 * @author liuli 2007-11-23
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;

public class PayAppPrtBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private String mCaseNo;
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema(); //事件关联表
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema(); //事件表
    private LLAppealSchema mLLAppealSchema = new LLAppealSchema(); //理赔申诉错误表
    private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();
    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
    private String mflag = null;

    public PayAppPrtBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
//    if (!cOperate.equals("PRINT")&&!cOperate.equals("INSERT")) {
//      buildError("submitData", "不支持的操作字符串");
//      return false;
//    }
        mflag = cOperate;
        System.out.println("bl:submitdata:::::" + cOperate);

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        //准备打印管理表数据
        if (!dealPrintMag()) {
         return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(mCaseNo); //存放前台传来的caseno
        tLOPRTManagerSchema.setOtherNoType("09");
        tLOPRTManagerSchema.setCode("lp005");
        tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setAgentCode("");
        tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag2(mCaseNo);

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);

        mCaseNo = ttLOPRTManagerSchema.getOtherNo();
        if (mCaseNo == null || mCaseNo.equals("")) {
            buildError("getInputData", "没有得到足够的信息:分案号不能为空！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        boolean PEFlag = false; //打印理赔申请回执的判断标志
        boolean shortFlag = false; //打印备注信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        texttag.add("JetFormType", "lp005");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
            texttag.add("previewflag", "0");
        }else{
            texttag.add("previewflag", "1");
        }
        //根据印刷号查询打印队列中的纪录
        String aCaseNo = mCaseNo;
        String aRgtNo = "";
        String aSubRptNo = "";
        String AccDate = "";

        String CaseTitle = "";
        if ("S".equals(aCaseNo.substring(0, 1))) {
            CaseTitle = "理赔申诉通知";
        } else {
            CaseTitle = "理赔申请回执";
        }
        LLCaseDB mLLCaseDB = new LLCaseDB();
        mLLCaseDB.setCaseNo(aCaseNo);
        //mLLCaseDB.setRgtNo(aRgtNo);

        if (!mLLCaseDB.getInfo()) {
            mErrors.copyAllErrors(mLLCaseDB.mErrors);
            buildError("outputXML", "mLLCaseDB在取得打印队列中数据时发生错误");
            return false;

        }
        mLLCaseSchema.setSchema(mLLCaseDB.getSchema());
        aRgtNo = mLLCaseSchema.getRgtNo(); //立案号

        LLRegisterDB mLLRegisterDB = new LLRegisterDB(); //以下怎么处理
        mLLRegisterDB.setRgtNo(aRgtNo);
        if (mLLRegisterDB.getInfo() == false) {
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "mLLRegisterDB在取得打印队列中数据时发生错误");
            return false;
        }
        mLLRegisterSchema = mLLRegisterDB.getSchema();

        LLSubReportDB mLLSubReportDB = new LLSubReportDB();
        LLCaseRelaDB mLLCaseRelaDB = new LLCaseRelaDB();
        mLLCaseRelaDB.setCaseNo(aCaseNo);
        mLLCaseRelaSet.set(mLLCaseRelaDB.query());
        if (mLLCaseRelaSet == null || mLLCaseRelaSet.size() == 0) {
            PEFlag = false;
        } else {
            PEFlag = true;
            int SubRptCount = mLLCaseRelaSet.size();
            for (int i = 1; i <= SubRptCount; i++) {
                aSubRptNo = mLLCaseRelaSet.get(i).getSubRptNo();
                mLLSubReportDB.setSubRptNo(aSubRptNo);
                if (mLLSubReportDB.getInfo() == false) {
                    mErrors.copyAllErrors(mLLSubReportDB.mErrors);
                    buildError("outputXML", "mLLSubReportDB在取得打印队列中数据时发生错误");
                    return false;
                }
                AccDate = mLLSubReportDB.getSchema().getAccDate();
            }
        }

        //PayAppPrtBL
        ListTable tReaListTable = new ListTable();
        String[] reasonTitle = {
                               "APPEALREASON0", "APPEALREASON0"};
        tReaListTable.setName("APPEALREASON");
        mLLAppClaimReasonDB.setCaseNo(aCaseNo);
        mLLAppClaimReasonDB.setRgtNo(aRgtNo);
        mLLAppClaimReasonSet.set(mLLAppClaimReasonDB.query());
        if (mLLAppClaimReasonSet == null || mLLAppClaimReasonSet.size() == 0) {
            PEFlag = false;
        } else {
            PEFlag = true;
            int QQSize = mLLAppClaimReasonSet.size();
            if (mLLAppClaimReasonSet.size() > 6) {
                QQSize = 6;
            }
            for (int qq = 0; qq < QQSize; qq++) {
                String aReasonCode = mLLAppClaimReasonSet.get(qq + 1).
                                     getReasonCode();
                String strSQL =
                        "select codename from ldcode where codetype = 'llrgtreason' and code = '" +
                        aReasonCode + "'";
                ExeSQL exesql = new ExeSQL();
                String res = exesql.getOneValue(strSQL);
                texttag.add("type" + String.valueOf(qq + 1),
                            res);
            }
        }

        ListTable tACRListTable = new ListTable();
        ListTable tACRListTableShort = new ListTable();
        String[] ACRTitle = new String[2];
        String[] ACRTitleShort = new String[2];
        ACRTitle[0] = "AFFIX0";
        ACRTitle[1] = "AFFIX1";
        ACRTitleShort[0] = "AFFIXSHORT0";
        ACRTitleShort[1] = "AFFIXSHORT1";

        String[] strACR = new String[2];
        String[] strACRShort = new String[2];
        tACRListTable.setName("AFFIX"); //对应模版理赔申请事故原因部分的行对象名
        tACRListTableShort.setName("AFFIXSHORT");
        LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
        LLAppClaimReasonSet aLLAppClaimReasonSet = new LLAppClaimReasonSet();
        LLAffixDB mLLAffixDB = new LLAffixDB();
        LLAffixSet mLLAffixSet = new LLAffixSet();
        int count = 0; //申请资料的总数
        int countShort = 0; //需要补充的资料总数

//         mLLAffixDB.setCaseNo(mLLCaseDB.getCaseNo());
//         mLLAffixDB.setRgtNo(mLLCaseDB.getRgtNo());

        String sql = "select * from LLAffix where RgtNo='" + mLLCaseDB.getRgtNo() +
                     "' and CaseNo='" + mLLCaseDB.getCaseNo() + "'";

        // mLLAffixSet.set(mLLAffixDB.query());
        mLLAffixSet = mLLAffixDB.executeQuery(sql);
        //mLLAffixSet=mLLAffixDB.query();
        LLAffixSchema mLLAffixSchema = new LLAffixSchema();
        Vector vector = new Vector();
        Vector vectorShort = new Vector();
        if (mLLAffixSet == null || mLLAffixSet.size() == 0) {
            PEFlag = false;
        } else {
            PEFlag = true;

            for (int j = 1; j <= mLLAffixSet.size(); j++) {
                mLLAffixSchema.setSchema(mLLAffixSet.get(j));
                if (mLLAffixSchema.getShortCount() > 0) {
                    vectorShort.addElement(mLLAffixSchema.getAffixName());
                } else {
                    vector.addElement(mLLAffixSchema.getAffixName() + "　" +
                                      String.valueOf(mLLAffixSchema.getCount()) +
                                      "份");

                }
            }
        }

        if (vector.size() % 2 != 0) {
            vector.addElement("");
        }
        for (int i = 0; i < vector.size(); i += 2) {
            strACR[0] = (i + 1) + "、" + (String) vector.get(i);
//      if ( i + 1 < vector.size()) {
            if (vector.get(i + 1) != "") {
                strACR[1] = (i + 2) + "、" + (String) vector.get(i + 1);
            } else {
                strACR[1] = "";
            }
//      }
//      else {
//        strACR[1] = "";
//      }
            System.out.println("11111111111111" + strACR);
            tACRListTable.add(strACR);
            strACR = new String[2];
        }

        if (vectorShort.size() > 0) {
            shortFlag = true;

        }
        if (vectorShort.size() % 2 != 0) {
            vectorShort.addElement("");

        }

        for (int i = 0; i < vectorShort.size(); i += 2) {
            strACRShort[0] = (i + 1) + "、" + (String) vectorShort.get(i);
//      if ( (i + 1) < vectorShort.size()) {
            if (vectorShort.get(i + 1) != "") {
                strACRShort[1] = (i + 2) + "、" + (String) vectorShort.get(i + 1);
            } else {
                strACRShort[1] = "";
            }
//      }
//      else {
//        strACRShort[1] = "";
//      }
            tACRListTableShort.add(strACRShort);
            strACRShort = new String[2];
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PayAppPrt.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";

        //PayAppPrtBL模版元素
        //texttag.add("BarCodeParam1","");
        String strSQL = "select Username from lduser where usercode='" +
                        mGlobalInput.Operator + "'";
        ExeSQL exesql1 = new ExeSQL();
        String OperName = exesql1.getOneValue(strSQL);

        strSQL =
                "select codename from ldcode where codetype='llgetmode' and code='" +
                mLLRegisterSchema.getGetMode() + "'";
        String CaseGetMode = exesql1.getOneValue(strSQL);

        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("CaseTitle", CaseTitle);
        texttag.add("LLCase.CustomerName", mLLCaseSchema.getCustomerName());
        
        texttag.add("LLCase.CustomerSex",
                    "0".equals((String) mLLCaseSchema.getCustomerSex()) ? "男" :
                    "1".equals((String) mLLCaseSchema.getCustomerSex()) ? "女" :
                    "不详"); //性别0是男，1是女，2是不详
        texttag.add("IDNo", mLLCaseSchema.getIDNo());
        texttag.add("CustBirthday", mLLCaseSchema.getCustBirthday());
        texttag.add("Phone", mLLCaseSchema.getPhone());
        texttag.add("ZipCode", mLLRegisterSchema.getPostCode());
        texttag.add("PostalAddress", mLLCaseSchema.getPostalAddress());
        texttag.add("LLRegister.RgtantName", mLLRegisterSchema.getRgtantName());
        texttag.add("RgtantPhone", mLLRegisterSchema.getRgtantPhone());
        texttag.add("RgtDate", mLLRegisterSchema.getRgtDate());
        texttag.add("AccStartDate", AccDate);
        texttag.add("CaseGetMode", CaseGetMode);
        texttag.add("Operator", OperName);
        texttag.add("SysDate", SysDate);
        texttag.add("XI_ContNo", "");
        texttag.add("XI_ManageCom", mLLCaseSchema.getMngCom());
        texttag.add("XI_CustomerNo", mLLCaseSchema.getCustomerNo());
        if (shortFlag) {
            texttag.add("Supply1", "1、根据保险合同的约定，您所提供的资料不齐，请根据上述提示补齐资料后再行申请；");
            texttag.add("Supply2",
                        "2、本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
        } else {
            texttag.add("Supply1",
                        "本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
            texttag.add("Supply2", "");
        }
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        if (PEFlag == true) {
            xmlexport.addListTable(tReaListTable, reasonTitle); //保存申请资料信息

            xmlexport.addListTable(tACRListTable, ACRTitle); //保存申请资料信息
            xmlexport.addListTable(tACRListTableShort, ACRTitleShort); //保存申请资料信息
        }
        xmlexport.outputDocumentToFile("d:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

        PayAppPrtBL tPayAppPrtBL = new PayAppPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C1100060616000003");
        tVData.addElement(tLLCaseSchema);
        tPayAppPrtBL.submitData(tVData, "PRINT");
    }

}
