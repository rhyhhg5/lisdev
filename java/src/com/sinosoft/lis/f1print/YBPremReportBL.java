package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  hzy lys
 * @date:2003-06-04
 * @version 1.0
 */
import java.io.File;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class YBPremReportBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public YBPremReportBL()
    {
        mErrors=new CErrors();
        mResult=new VData();
        mDay=null;
        mGlobalInput=new GlobalInput();

    }

    public static void main(String[] args)
    {
//        GlobalInput tG = new GlobalInput();
//        tG.Operator = "001";
//        tG.ManageCom = "86110000";
//        VData vData = new VData();
//        String[] tDay = new String[2];
//        tDay[0] = "2007-01-14";
//        tDay[1] = "2007-07-14";
//        vData.addElement(tDay);
//        vData.addElement(tG);
//
//        TeXuDayCheckBL tF = new TeXuDayCheckBL();
//        tF.submitData(vData, "PRINTGET");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINTGET")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }else{
            if (!getPrintDataPay1())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        double SumMoney = 0;
        int SumNum=0;
        String salechnl="";
        if(!mDay[2].equals("")){
            salechnl=" and a.salechnl='"+mDay[2]+"' ";
        } 
                String msql="select (select name from lacom where agentcom=a.agentcom), "
                    + "a.contno,"   
                    + "a.AppntName,"    
                    + "(select  name from labranchgroup where agentgroup=a.agentgroup),"    
                    + "(select  branchattr from labranchgroup where agentgroup=a.agentgroup),"  
                    + "a.agentcode ,"   
                    + "(select name from laagent where agentcode=a.agentcode)," 
                    + "b.riskcode,"
                    + "b.prem,"
                    + "c.confmakedate,"
                    + "a.cvalidate,"
                    + "a.cinvalidate  "
                    + "from lccont a,lcpol b ,ljtempfee c "
                    + "where a.contno=b.contno   "
                    + "and c.TempFeeType in ('1','5','16') "
                    + "and c.othernotype = '4' "
                    + "and a.prtno=c.otherno "
                    + "and c.confdate is null  "
                    + "and a.salechnl in ('03','04') "
                    +salechnl
                    + "and a.managecom like '"+mGlobalInput.ManageCom+"%' "
                    + "and c.confmakedate<='"+mDay[1]+"' and c.confmakedate>='"+mDay[0]+"' "
                    + "union select (select name from lacom where agentcom=a.agentcom),"
                    + "a.contno,"
                    + "a.AppntName,"  
                    + "(select  name from labranchgroup where agentgroup=a.agentgroup),"
                    + "(select  branchattr from labranchgroup where agentgroup=a.agentgroup),"
                    + "a.agentcode ,"
                    + "(select name from laagent where agentcode=a.agentcode),"
                    + "b.riskcode,"
                    + "b.prem,"
                    + "c.confmakedate,"
                    + "a.cvalidate,"
                    + "a.cinvalidate  "
                    + "from lccont a,lcpol b ,ljtempfee c "
                    + "where a.contno=b.contno  "
                    + "and  a.contno=c.otherno "
                    + "and c.TempFeeType in ('1','5','16') "
                    + "and c.othernotype <> '4' "
                    + "and a.salechnl in ('03','04') "
                    +salechnl
                    + "and a.managecom like '"+mGlobalInput.ManageCom+"%'"
                    + "and c.confmakedate<='"+mDay[1]+"' and c.confmakedate>='"+mDay[0]+"' "
                    + "with ur" ;    


        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        SSRS nSSRS = new SSRS();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);
        
      ExeSQL tExeSQL = new ExeSQL();
      System.out.println("EXC SQL："+msql);
      tSSRS = tExeSQL.execSQL(msql);
      String strSum="";
      String strArr[][] = null;
      SumNum=tSSRS.MaxRow+6;
      strArr=new String[SumNum][tSSRS.MaxCol];
      for (int i = 4; i < SumNum-2; i++)
      {
          for (int j = 1; j <= tSSRS.MaxCol; j++)
          {
              if (j == 9 )
              {
                  strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                  if(strArr[i][j - 1]!=null && !strArr[i][j - 1].trim().equals("") && !strArr[i][j - 1].equals("null")){
                      strSum = new DecimalFormat("0.00").format(Double.
                              valueOf(strArr[i][j - 1]));
                      strArr[i][j - 1] = strSum;
                      SumMoney = SumMoney + Double.parseDouble(strArr[i][j - 1]);
                  }else{
                      strArr[i][j - 1]="0.00";
                  }
                  
              }else{
                  strArr[i][j - 1] = tSSRS.GetText(i-3, j);
              }
          }
      }
//        
        try {
            String tFileName="银保保费"+mGlobalInput.ManageCom+mGlobalInput.Operator+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".xls";
            WriteToExcel t = new WriteToExcel(tFileName);
            t.createExcelFile();
            String[] sheetName = {
                "sheet1"};
            t.addSheet(sheetName);
            
            strArr[0][0]="统计日期：";
            strArr[0][1]=mDay[0];
            strArr[0][2]="至";
            strArr[0][3]=mDay[1];
            
            strArr[1][0]="统计机构：";
            strArr[1][1]=manageCom;
            
            String[] title = {
                "管理机构", "合同号", "客户姓名","代理机构"
                , "部门", "代理人编码", "代理人姓名",
                "险种编码","保费","缴费日期", "生效日期", "合同终止日期"};
            for(int i=0;i<title.length;i++){
                strArr[3][i]=title[i];
            }
            strArr[SumNum-1][0]="合计：";
            strArr[SumNum-1][1]=String.valueOf(SumMoney);
            
            t.setData(0, strArr);
            String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
            File f=new File(tServerPath+"web/Generated/"+tFileName);
            f.delete();
            t.write(tServerPath+"web/Generated/");
            
            String tURL = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
            String resultURL=tURL+"web/Generated/"+tFileName;
            mResult.clear();
            mResult.addElement(resultURL);
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        return true;
    }

    private void initCommon()
    {
    }
    private boolean getPrintDataPay1()
    {
        SSRS tSSRS = new SSRS();
        double SumMoney = 0;
        int SumNum=0;
        String salechnl="";
        if(!mDay[2].equals("")){
            salechnl=" and exists (select '1' from lbcont where salechnl='"+mDay[2]+"' and contno=LJAGetEndorse.contno and conttype='1') ";
        } 
                String msql= "select (select name from lacom where agentcom=LJAGetEndorse.agentcom)"                                
                    + ",contno,(select appntname from lbcont where contno=LJAGetEndorse.contno)"                                
                    + ",(select   branchattr from labranchgroup where agentgroup=LJAGetEndorse.agentgroup)"                             
                    + ",(select   name from labranchgroup where agentgroup=LJAGetEndorse.agentgroup)"                               
                    + ",agentcode,(select   name from laagent where agentcode=LJAGetEndorse.agentcode)"                             
                    + ",riskcode"                               
                    + ",getmoney"                               
                    + ",getconfirmdate"                             
                    + ",(select cvalidate  from lbcont where contno=LJAGetEndorse.contno)"                              
                    + ",(select cinvalidate  from lbcont where contno=LJAGetEndorse.contno)"                                
                    + "from LJAGetEndorse "
                    + "where agentcom is not null " 
                    + "and GrpPolNo = '00000000000000000000' "  
                    + "and FeeOperationType = 'WT' "
                    +salechnl
                    + "and managecom like '"+mGlobalInput.ManageCom+"%' "
                    +" and getconfirmdate<='"+mDay[1]+"' and getconfirmdate>='"+mDay[0]+"' "
                    + "with ur "; 
                String nsql = "select Name from LDCom where ComCode='" +
                mGlobalInput.ManageCom + "'";
         ExeSQL nExeSQL = new ExeSQL();
         SSRS nSSRS = new SSRS();
         nSSRS = nExeSQL.execSQL(nsql);
         String manageCom = nSSRS.GetText(1, 1);
         
       ExeSQL tExeSQL = new ExeSQL();
       System.out.println("EXC SQL："+msql);
       tSSRS = tExeSQL.execSQL(msql);
       String strSum="";
       String strArr[][] = null;
       SumNum=tSSRS.MaxRow+6;
       strArr=new String[SumNum][tSSRS.MaxCol];
       for (int i = 4; i < SumNum-2; i++)
       {
           for (int j = 1; j <= tSSRS.MaxCol; j++)
           {
               if (j == 9 )
               {
                   strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                   if(strArr[i][j - 1]!=null && !strArr[i][j - 1].trim().equals("") && !strArr[i][j - 1].equals("null")){
                       strSum = new DecimalFormat("0.00").format(Double.
                               valueOf(strArr[i][j - 1]));
                       strArr[i][j - 1] = strSum;
                       SumMoney = SumMoney + Double.parseDouble(strArr[i][j - 1]);
                   }else{
                       strArr[i][j - 1]="0.00";
                   }
                   
               }else{
                   strArr[i][j - 1] = tSSRS.GetText(i-3, j);
               }
           }
       }
//         
         try {
             String tFileName="银保犹豫期退费"+mGlobalInput.ManageCom+mGlobalInput.Operator+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".xls";
             WriteToExcel t = new WriteToExcel(tFileName);
             t.createExcelFile();
             String[] sheetName = {
                 "sheet1"};
             t.addSheet(sheetName);
             
             strArr[0][0]="统计日期：";
             strArr[0][1]=mDay[0];
             strArr[0][2]="至";
             strArr[0][3]=mDay[1];
             
             strArr[1][0]="统计机构：";
             strArr[1][1]=manageCom;
             
             String[] title = {
                 "管理机构", "合同号", "客户姓名","代理机构"
                 , "部门", "代理人编码", "代理人姓名",
                 "险种编码","保费","缴费日期", "生效日期", "合同终止日期"};
             for(int i=0;i<title.length;i++){
                 strArr[3][i]=title[i];
             }
             strArr[SumNum-1][0]="合计：";
             strArr[SumNum-1][1]=String.valueOf(SumMoney);
             
             t.setData(0, strArr);
             String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
             File f=new File(tServerPath+"web/Generated/"+tFileName);
             f.delete();
             t.write(tServerPath+"web/Generated/");
             String tURL = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
             String resultURL=tURL+"web/Generated/"+tFileName;
             mResult.clear();
             mResult.addElement(resultURL);
           }
           catch (Exception ex) {
             ex.printStackTrace();
           }
        return true;
    }


}
