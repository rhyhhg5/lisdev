package com.sinosoft.lis.f1print;

/**
 * <p>Title:FenFinDayBQPremExcelBL </p>
 * <p>Description:X4-保全保费日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author	yan
 * @version 1.0
 */

import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

public class FenFinDayBQPremExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// 取得的时间
	private String mDay[] = null;
	private String tOutXmlPath = "";
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public FenFinDayBQPremExcelBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}	
		// 准备所有要打印的数据
		if (cOperate.equals("PRINTPAY"))
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[2];
		tDay[0] = "2004-1-13";
		tDay[1] = "2004-1-13";
		vData.addElement(tDay);
		vData.addElement(tG);

		FenFinDayBQPremExcelBL tF = new FenFinDayBQPremExcelBL();
		tF.submitData(vData, "PRINTPAY");

	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径的信息！");
			return false;
		}
		
		System.out.println("打印日期-->"+mDay[0]+"至"+mDay[1]);
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FenFinDayBQPremExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
    
	/**
	 * 新的打印方法
	 * 
	 * @return
	 */
	private boolean getPrintDataPay() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath ="C:/Users/db2admin.qqing-pc/Picch/ui/";
//		tServerPath= "E:/DYL-workspace/Picch/ui/";
		
		String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
        
        String[][] mToExcel = new String[65536][7];
		mToExcel[1][0] = "保全日结单";
		mToExcel[2][0] = "统计日期：";
		mToExcel[2][1] = mDay[0]+"至"+mDay[1];
		mToExcel[3][0] = "管理机构：";
		mToExcel[3][1] = manageCom;
		mToExcel[5][0] = "类型";
		mToExcel[5][1] = "渠道";
		mToExcel[5][2] = "批单号码";
		mToExcel[5][3] = "保单号码";
		mToExcel[5][4] = "金额";
		mToExcel[5][5] = "税后金额";
		mToExcel[5][6] = "税额";
		
		int no=6;  //初始化打印行数
		
		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"BQTF", // 保全退费
//				"YGBJ", //预估结余返还数据
				"BQTBJ", // 退保金
				"BQYEI", // YEI
				"BQMJ", // 满期金
				"BQMJLX",// 满期利息
				"BQSSBF", // 实收保费
				"BQTJ", // 体检费
				"BQFX", // 保单复效利息
				"BQCJ", // 保户储金及投资款
				"BQTZLX", // 投资款业务利息
				"BQTZLX-H",//委托管理-帐号管理费
				"BQGL", // 账户管理费
				"BQGB", // 工本费
				"WNSSBF_BJCJ3",//万能实收保费-保户储金
				"WNSSBF_ZHGLF3",//万能实收保费-账户管理费
				"WNFX_CSKF", //万能复效-初始扣费
				"WNFX_FXBF", //万能复效-风险保费
				"XTXSSBF_BHCJ3",//新特需实收保费-保户储金
				"XTXSSBF_ZHGLF3",//新特需实收保费-账户管理费
				"WNYJ_ZHLX3",//万能月结-账户利息
				"WNYJ_FXBF3",//万能月结-风险保费
				"WNYJ_BDGLF3",//万能月结-保单管理费
//				"WNBFLQ_ZHGLF3",//万能部分领取-账户管理费
//				"WNBQTB_BDGLF3",//万能保全退保-保单管理费
				"WNYJ_SYCEFH",//万能月结-税优差额返还 
				"WNBQTB_FXKF3",//万能保全退保-风险扣费
				"WNBQTB_ZHLX3",//万能保全退保-账户利息
				"WNBQTB_ZHGLF3",//万能保全退保-账户管理费
//				"XTXBQXYTB_JYGLF3",//新特需保全协议退保-解约管理费				
				"BQCM",			//万能资料变更
				"BQDQJSJE",		//定期结算
//				"XTXBFLQ_ZHGLF3",//新特需部分领取-账户管理费
//				"XTXBQTB_BDGLF3",//新特需保全退保-保单管理费
//				"XTXBQTB_FXKF3",//新特需保全退保-风险扣费
//				"XTXBQTB_ZHLX3",//新特需保全退保-账户利息
//				"XTXBQTB_ZHGLF3"//新特需保全退保-账户管理费
				"WNBJ_BFLQ",       //万能——本金——部分领取
				"WNGLF_BFLQ",      //万能——利息——部分领取
				"XTXBJ_BFLQ",      //新特需——本金——部分领取
				"XTXGLF_BFLQ",     //新特需——本金——部分领取
				"WN_CXJJ",         //万能-持续奖金
				"WN_JYGLF",        //万能-解约管理费
				"BDZF",            //保单期缴作废-反冲应收
				"YDTB_FC",		//约定缴费退保应收反冲
				"HL_XJ",           //分红险红利结算-现金领取：包括周年日领取和退保时临时结算领取
				"HL_LX",            //保单红利支出—累积生息：包括周年日进账户和退保时临时结算账户利息
				"BQPR",         //保全万能保单迁移本金
				"BQYS",			//保全预收保费-账户余额
				"YGBJ", //预估结余返还数据
				"YGBJFC", //预估结余返还数据
				"BQMJJT", // 满期金计提
				"BQMJJT-F",// 满期金计提反冲
				"BQTB_BDHKBF",//保全退保-保单还款保费
				"BQTB_BDHKLX"//保全退保-保单还款利息
		};
		
		for (int i = 0; i < getTypes.length; i++) {
		String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
		System.out.println("节点是:"+getTypes[i]+"    sql是："+msql);
		
		String[] split = msql.split(";");
		SSRS mSSRS = new SSRS();
		int maxrowexcel = 0;
		double sumMoney=0.00;
		double moneynotax=0.00;
		double moneytax=0.00;

		for(int s = 0 ;s<split.length;s++){
			
		System.out.println("节点是:"+getTypes[i]+"里的第" +(s+1) +"个分SQL,sql是："+split[s].toString());
		mSSRS = tExeSQL.execSQL(split[s].toString());
		int maxrow = mSSRS.getMaxRow();
		maxrowexcel = maxrowexcel + maxrow;
			for (int row = 1; row <= maxrow; row++)
            {
				System.out.println("循环开始");
				


			  for (int col = 1; col <=7; col++)
                { 
                	if (col==5) {

                		mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
						sumMoney=sumMoney+Double.parseDouble(mSSRS.GetText(row,col));
						System.out.println("节点金额目前累计为：" + sumMoney );
                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
					}else if (col==6){

						if (mSSRS.GetText(row,col) != null && !"".equals(mSSRS.GetText(row,col))){
							mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
							moneynotax=moneynotax+Double.parseDouble(mSSRS.GetText(row,col));
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}else{
							mToExcel[no+row-1][col - 1]="0.00";
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}
						
					}else if(col==7){
						if (mSSRS.GetText(row,col) != null && !"".equals(mSSRS.GetText(row,col))){
							mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
							moneytax=moneytax+Double.parseDouble(mSSRS.GetText(row,col));
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}else{
							mToExcel[no+row-1][col - 1]="0.00";
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}
						}
				
                	else {
                		System.out.println("col 为1、2、3、4");
						mToExcel[no+row-1][col - 1] = mSSRS.GetText(row, col);  
                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
					}
                    }
                }
			no=no+maxrow;

		}
			mToExcel[no][0] =getChineseNameWithPY(getTypes[i]);
			mToExcel[no][3] ="金额合计：";
			mToExcel[no][4] =new DecimalFormat("0.00").format(Double.valueOf(sumMoney));
			mToExcel[no][5] =new DecimalFormat("0.00").format(Double.valueOf(moneynotax));
			mToExcel[no][6] =new DecimalFormat("0.00").format(Double.valueOf(moneytax));
			no=no+2;
		}		

		
//excel处理		
		System.out.println("X4文件打印行数->"+no);
		mToExcel[no+1][0] = "制表员：";
		mToExcel[no+1][2] = "审核员：";
		try
        {
            System.out.println("X4--tOutXmlPath:"+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X4文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }
		return true;
	}

	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String getChineseNameWithPY(String pName) {
		String result ="";
		if ("BQTF".equals(pName)) {
			result = "保全退费小计";
		} else if ("BQTBJ".equals(pName)) {
			result = "退保金小计";
		} else if ("BQYEI".equals(pName)) {
			result = "保单余额小计";
		} else if ("BQMJ".equals(pName)) {
			result = "满期金小计";
		} else if ("BQMJLX".equals(pName)) {
			result = "满期利息小计";
		} else if ("BQSSBF".equals(pName)) {
			result = "保全实收保费小计";
		} else if ("BQTJ".equals(pName)) {
			result = "体检费小计";
		} else if ("BQFX".equals(pName)) {
			result = "保单复效利息小计";
		} else if ("BQCJ".equals(pName)) {
			result = "保户储金及投资款小计";
		} else if ("BQTZLX".equals(pName)) {
			result = "投资款业务利息或帐号管理费小计";
		} else if ("BQTZLX-H".equals(pName)) {
			result = "委托管理-帐号管理费小计";
		} else if ("BQGL".equals(pName)) {
			result = "账户管理费小计";
		} else if ("BQGB".equals(pName)) {
			result = "工本费小计";
		}else if ("WNSSBF_BJCJ3".equals(pName)) {
			result = "万能实收保费-保户储金小计";
		} else if ("WNSSBF_ZHGLF3".equals(pName)) {
			result = "万能实收保费-账户管理费小计";
		}else if ("WNFX_CSKF".equals(pName)) {
			result = "万能实收保费-复效初始扣费小计";
		}else if ("WNFX_FXBF".equals(pName)) {
			result = "万能保单复效-风险保费小计";
		} else if ("XTXSSBF_BHCJ3".equals(pName)) {
			result = "新特需实收保费-保户储金小计";
		} else if ("XTXSSBF_ZHGLF3".equals(pName)) {
			result = "新特需实收保费-账户管理费小计";
		} else if ("WNYJ_ZHLX3".equals(pName)) {
			result = "万能月结-账户利息小计";
		} else if ("WNYJ_FXBF3".equals(pName)) {
			result = "万能月结-风险保费小计";
		} else if ("WNYJ_BDGLF3".equals(pName)) {
			result = "万能月结-保单管理费小计";
		}else if ("WNYJ_SYCEFH".equals(pName)) {
			result = "万能月结-税优差额返还小计";
		}  else if ("WNBQTB_FXKF3".equals(pName)) {
			result = "万能保全退保-风险扣费小计";
		} else if ("WNBQTB_ZHLX3".equals(pName)) {
			result = "万能保全退保-账户利息小计";
		} else if ("WNBQTB_ZHGLF3".equals(pName)) {
			result = "万能保全退保-账户管理费小计";
		} else if ("BQCM".equals(pName)) {
			result = "万能客户资料变更小计";
		} else if ("BQDQJSJE".equals(pName)) {
			result = "定期结算小计";	
		}else if ("WNBJ_BFLQ".equals(pName)) {
			result = "万能保全部分领取—本金小计";	
		}else if ("WNGLF_BFLQ".equals(pName)) {
			result = "万能保全部分领取-利息小计";	
		}else if ("XTXBJ_BFLQ".equals(pName)) {
			result = "新特需保全部分领取-本金 小计";	
		}else if ("XTXGLF_BFLQ".equals(pName)) {
			result = "新特需保全部分领取-管理费小计";	
		}else if ("WN_CXJJ".equals(pName)) {
			result = "万能-持续奖金小计";	
		}else if ("WN_JYGLF".equals(pName)) {
			result = "万能-解约管理费小计";	
		}else if ("BDZF".equals(pName)) {
			result = "保单作废小计";	
		}else if ("YDTB_FC".equals(pName)) {
			result = "约定缴费退保反冲小计";
		}else if ("HL_XJ".equals(pName)) {
			result = "分红险红利结算-现金领取小计";	
		}else if ("HL_LX".equals(pName)) {
			result = "分红险红利结算-累积生息小计";	
		}else if ("BQPR".equals(pName)) {
			result = "保全万能保单迁移小计";	
		}else if ("BQYS".equals(pName)) {
			result = "保全预收保费-账户余额小计";	
		}else if ("YGBJ".equals(pName)) {
			result = "预估结余返还数据小计";
		}else if ("YGBJFC".equals(pName)) {
			result = "预估结余返还反冲数据小计";
		}else if ("BQMJJT".equals(pName)) {
			result = "满期金计提小计";
		}else if ("BQMJJT-F".equals(pName)) {
			result = "满期金计提反冲小计";
		}else if ("BQTB_BDHKBF".equals(pName)) {
			result = "保全退保-保单还款保费小计";
		}else if ("BQTB_BDHKLX".equals(pName)) {
			result = "保全退保-保单还款利息 小计";
		}

		return result;
	}

}
