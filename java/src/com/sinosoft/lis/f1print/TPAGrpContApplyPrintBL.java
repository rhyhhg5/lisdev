/**
 * 2009-2-26
 */
package com.sinosoft.lis.f1print;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * @author LY
 *
 */
public class TPAGrpContApplyPrintBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private String mLoadFlag;

    private String mflag = null;

    public TPAGrpContApplyPrintBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            mResult.clear();

            // 准备所有要打印的数据
            if (!getPrintData())
            {
                return false;
            }

            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    private String getDate(Date date)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        if (!loadPrintManager())
        {
            return false;
        }

        TransferData tTransferData = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        if (tTransferData != null)
        {
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }
        //只赋给schema一个prtseq

        mflag = mOperate;

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CCSFirstPayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport();
        TextTag tTextTag = new TextTag();

        xmlExport.createDocument("TPAGrpContApply", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        // 处理团体保单层信息。
        if (!dealGrpContInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        if (tTextTag.size() > 0)
        {
            xmlExport.addTextTag(tTextTag);
        }

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }

    /**
     * 产生PDF打印相关基本信息。
     * @param cTextTag
     * @return
     */
    private boolean dealPdfBaseInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        // 单证类型标志。
        tTmpTextTag.add("JetFormType", "TPAapp");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTmpTextTag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if (mflag.equals("batch"))
        {
            tTmpTextTag.add("previewflag", "0");
        }
        else
        {
            tTmpTextTag.add("previewflag", "1");
        }
        // -------------------------------

        return true;
    }

    /**
     * 处理团体保单层信息。
     * @param cTextTag
     * @return
     */
    private boolean dealGrpContInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        tTmpTextTag.add("prtno", mLOPRTManagerSchema.getOtherNo());

        return true;
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private boolean loadPrintManager()
    {
        //LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        //        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        //        if (!tLOPRTManagerDB.getInfo())
        //        {
        //            tLOPRTManagerDB = new LOPRTManagerDB();
        //            tLOPRTManagerDB.setOtherNo(aOtherNo);
        //        }
        //        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        String tPrtNo = PubFun1.CreateMaxNo("TPAPRTNO", null);
        mLOPRTManagerSchema.setPrtSeq(tPrtNo);
        mLOPRTManagerSchema.setOtherNo(tPrtNo);
        mLOPRTManagerSchema.setOtherNoType("05");

        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");

        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        return true;
    }

    public CErrors getErrors()
    {
        return null;
    }
}
