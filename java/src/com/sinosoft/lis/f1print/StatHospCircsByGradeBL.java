/**
 * <p>ClassName: StatHospCircsByGradeBL.java </p>
 * <p>Description: 分合作级别医院设置情况统计 </p>
 * <p>Copyright: Copyright (c) 2010 </p>
 * <p>Company: sinosoft</p>
 * @author 石和平
 * @version 1.0
 * @CreateDate：2010-04-01
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class StatHospCircsByGradeBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private TransferData mTransferData = new TransferData();

	/** 统计开始日期 */
	private String mStartDate = "";

	/** 统计结束日期 */
	private String mEndDate = "";

	/** 统计机构编码 */
	private String mManageComCode = "";

	/** 统计机构名称 */
	private String mManageComName = "";

	public StatHospCircsByGradeBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(this.mErrors);
			return false;
		}

		mInputData = null;
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {

		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			CError cError = new CError();
			cError.moduleName = "PrintOrderReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mGlobalInput 为空值";
			mErrors.addOneError(cError);
			return false;
		}
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			CError cError = new CError();
			cError.moduleName = "PrintOrderReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mTransferData 为空值";
			mErrors.addOneError(cError);
			return false;
		}
		/** 统计开始日期 */
		mStartDate = (String) mTransferData.getValueByName("StartDate");
		/** 统计结束日期 */
		mEndDate = (String) mTransferData.getValueByName("EndDate");
		/** 统计机构编码 */
		mManageComCode = (String) mTransferData.getValueByName("ManageCom");
		/** 统计机构名称 */
		mManageComName = (String) mTransferData.getValueByName("ComName");

		return true;
	}

	private boolean checkData() {
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		// 报表打印
		if (!PrintData()) {
			return false;
		}
		return true;
	}

	/**
	 * 报表打印主程序
	 * 
	 * @return
	 */
	private boolean PrintData() {

		XmlExport xmlExport = new XmlExport(); // 新建一个XmlExport的实例
		xmlExport.createDocument("StatHospCircsByGrade.vts", ""); // 最好紧接着就初始化xml文档
		ListTable tlistTable = new ListTable();
		String strArr[] = null;
		
		//Create a Array to save the data of sum
		int[] sum = new int[27];
		
		for(int i = 0; i < 27 ; i++){
			sum[i] = 0 ;
		}
		
		tlistTable.setName("HMReport");
		//
		String tStartDateCondition = "";
		if (mStartDate != null && !mStartDate.equals("")) {
			tStartDateCondition = " and ab.ModifyDate >='" + mStartDate + "'";
		}
		String tEndDateCondition = "";
		if (mEndDate != null && !mEndDate.equals("")) {
			tEndDateCondition = " and ab.ModifyDate <='" + mEndDate + "'";
		}
		
		String tManageComCondition = "";
		if(mManageComCode != null && !"".equals(mManageComCode)) {
			
			if(mManageComCode.equals("86")) {
				tManageComCondition = " and x.ComCode like '" + mManageComCode + "%' and x.Comgrade='02' ";
			} else if(mManageComCode.length() == 4) {
				tManageComCondition = " and x.ComCode like '" + mManageComCode + "%' and x.Comgrade='03'";
			} else {
				tManageComCondition = " and x.ComCode like '" + mManageComCode + "%' ";
			}
		}
		
		String sql =" select (select name from ldcom where comcode=xx.comCode),"
					+" case when xx.AssociateClass = '1' then "
					+" '推荐医院' "
					+" when xx.AssociateClass = '2' then "
					+" '指定医院' "
					+" when xx.AssociateClass = '3' then "
					+" '非指定医院' "
					+" end,sum(xx.sd),sum(xx.fsd),sum(xx.sj),sum(xx.fsj),sum(xx.ej),sum(xx.fej),"
					+" sum(xx.yj),sum(xx.fyj),sum(xx.wfj),sum(xx.total)"
					+" from (select x.comCode comCode ,ab.AssociateClass AssociateClass,"
					+" (select count(1) count from ldhospital aa  where 1 = 1 "
					+" and aa.CommunFixFlag = '1' "
					+" and aa.hospitcode = ab.hospitcode) as sd, " 
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.CommunFixFlag <> '1' "
					+" and aa.hospitcode = ab.hospitcode ) as fsd,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '31' "
					+" and aa.hospitcode = ab.hospitcode ) as sj,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '30' "
					+" and aa.hospitcode = ab.hospitcode) as fsj,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '21' "
					+" and aa.hospitcode = ab.hospitcode) as ej,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '20' "
					+" and aa.hospitcode = ab.hospitcode) as fej,"
					+"(select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '11' "
					+" and aa.hospitcode = ab.hospitcode) as yj,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '10' "
					+" and aa.hospitcode = ab.hospitcode) as fyj,"
					+" (select count(1) count from ldhospital aa where 1 = 1 "
					+" and aa.LevelCode = '00' "
					+" and aa.hospitcode = ab.hospitcode) as wfj, "
					+" (select count(1) count from ldhospital aa where 1 = 1"
					+"  and aa.hospitcode = ab.hospitcode )as total "
					+" from ldcom x,ldhospital ab "
					+ " where x.comCode = ab.ManageCom  and ab.AssociateClass in('1','2','3') "
					+" and x.Sign='1'"
					+ tManageComCondition
					+ tStartDateCondition 
					+ tEndDateCondition 
					+ " group by ab.AssociateClass,x.comCode,ab.hospitcode) as xx group by xx.comCode,xx.AssociateClass"
					+ " order by xx.comCode ,xx.AssociateClass with ur";
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			strArr = new String[12];
			strArr[0] = tSSRS.GetText(i, 1);
			strArr[1] = tSSRS.GetText(i, 2);
			strArr[2] = tSSRS.GetText(i, 3);//
			strArr[3] = tSSRS.GetText(i, 4);//
			strArr[4] = tSSRS.GetText(i, 5);//
			strArr[5] = tSSRS.GetText(i, 6);//
			strArr[6] = tSSRS.GetText(i, 7);//
			strArr[7] = tSSRS.GetText(i, 8);//
			strArr[8] = tSSRS.GetText(i, 9);//
			strArr[9] = tSSRS.GetText(i, 10);//
			strArr[10] = tSSRS.GetText(i, 11);//
			strArr[11] = tSSRS.GetText(i, 12);//
			
			if("推荐医院".equals(tSSRS.GetText(i, 2))){
				sum[0] += Integer.parseInt(tSSRS.GetText(i, 3)) ;
				sum[1] += Integer.parseInt(tSSRS.GetText(i, 4)) ;
				sum[2] += Integer.parseInt(tSSRS.GetText(i, 5)) ;
				sum[3] += Integer.parseInt(tSSRS.GetText(i, 6)) ;
				sum[4] += Integer.parseInt(tSSRS.GetText(i, 7)) ;
				sum[5] += Integer.parseInt(tSSRS.GetText(i, 8)) ;
				sum[6] += Integer.parseInt(tSSRS.GetText(i, 9)) ;
				sum[7] += Integer.parseInt(tSSRS.GetText(i, 10)) ;
				sum[8] += Integer.parseInt(tSSRS.GetText(i, 11)) ;
			}
			
			if("指定医院".equals(tSSRS.GetText(i, 2))){
				sum[9] += Integer.parseInt(tSSRS.GetText(i, 3)) ;
				sum[10] += Integer.parseInt(tSSRS.GetText(i, 4)) ;
				sum[11] += Integer.parseInt(tSSRS.GetText(i, 5)) ;
				sum[12] += Integer.parseInt(tSSRS.GetText(i, 6)) ;
				sum[13] += Integer.parseInt(tSSRS.GetText(i, 7)) ;
				sum[14] += Integer.parseInt(tSSRS.GetText(i, 8)) ;
				sum[15] += Integer.parseInt(tSSRS.GetText(i, 9)) ;
				sum[16] += Integer.parseInt(tSSRS.GetText(i, 10)) ;
				sum[17] += Integer.parseInt(tSSRS.GetText(i, 11)) ;
			}
			
			if("非指定医院".equals(tSSRS.GetText(i, 2))){
				sum[18] += Integer.parseInt(tSSRS.GetText(i, 3)) ;
				sum[19] += Integer.parseInt(tSSRS.GetText(i, 4)) ;
				sum[20] += Integer.parseInt(tSSRS.GetText(i, 5)) ;
				sum[21] += Integer.parseInt(tSSRS.GetText(i, 6)) ;
				sum[22] += Integer.parseInt(tSSRS.GetText(i, 7)) ;
				sum[23] += Integer.parseInt(tSSRS.GetText(i, 8)) ;
				sum[24] += Integer.parseInt(tSSRS.GetText(i, 9)) ;
				sum[25] += Integer.parseInt(tSSRS.GetText(i, 10)) ;
				sum[26] += Integer.parseInt(tSSRS.GetText(i, 11)) ;
			}
			tlistTable.add(strArr);
		}
		
		strArr = new String[12];

		TextTag texttag = new TextTag();
		if (mStartDate != null && !mStartDate.equals("")) {
			mStartDate = mStartDate.replaceFirst("-", "年").replaceFirst("-",
					"月")
					+ "日";
		}
		if (mEndDate != null && !mEndDate.equals("")) {
			mEndDate = mEndDate.replaceFirst("-", "年").replaceFirst("-", "月")
					+ "日";
		}

		mManageComName = new ExeSQL()
				.getOneValue("select Name from LDCom where ComCode='"
						+ mManageComCode + "'");

		texttag.add("StartDate", mStartDate);// 开始日期
		texttag.add("EndDate", mEndDate);// 结束日期
		texttag.add("ManageComName", mManageComName);// 统计机构
		texttag.add("Operator", mGlobalInput.Operator);
		texttag.add("MakeDate", PubFun.getCurrentDate());
		// System.out.println("**********mManageComName= " + mManageComName);
		texttag.add("sum1",sum[0]);
		texttag.add("sum2",sum[1]);
		texttag.add("sum3",sum[2]);
		texttag.add("sum4",sum[3]);
		texttag.add("sum5",sum[4]);
		texttag.add("sum6",sum[5]);
		texttag.add("sum7",sum[6]);
		texttag.add("sum8",sum[7]);
		texttag.add("sum9",sum[8]);
		texttag.add("sum10",sum[9]);
		texttag.add("sum11",sum[10]);
		texttag.add("sum12",sum[11]);
		texttag.add("sum13",sum[12]);
		texttag.add("sum14",sum[13]);
		texttag.add("sum15",sum[14]);
		texttag.add("sum16",sum[15]);
		texttag.add("sum17",sum[16]);
		texttag.add("sum18",sum[17]);
		texttag.add("sum19",sum[18]);
		texttag.add("sum20",sum[19]);
		texttag.add("sum21",sum[20]);
		texttag.add("sum22",sum[21]);
		texttag.add("sum23",sum[22]);
		texttag.add("sum24",sum[23]);
		texttag.add("sum25",sum[24]);
		texttag.add("sum26",sum[25]);
		texttag.add("sum27",sum[26]);
		
		int[] m = new int[9];
		for(int a = 0 ; a < 9 ; a++ ){
			m[a] = sum[a] + sum[a+9]+sum[a+18] ;
		}
		texttag.add("sum28",m[0]);
		texttag.add("sum29",m[1]);
		texttag.add("sum30",m[2]);
		texttag.add("sum31",m[3]);
		texttag.add("sum32",m[4]);
		texttag.add("sum33",m[5]);
		texttag.add("sum34",m[6]);
		texttag.add("sum35",m[7]);
		texttag.add("sum36",m[8]);
		System.out.println("****************************************");
		int[] n = new int[4];
		for(int b=0 ; b < 3 ; b++){
			n[b] = sum[b*9]+sum[b*9+1];
		}
		n[3] = m[0]+m[1];
		texttag.add("total1", n[0]);
		texttag.add("total2", n[1]);
		texttag.add("total3", n[2]);
		texttag.add("total4", n[3]);
		
		if (texttag.size() > 0) {
			xmlExport.addTextTag(texttag);
		}
		
		xmlExport.addListTable(tlistTable, strArr);
		xmlExport.outputDocumentToFile("e:\\", "test");
		mResult.clear();
		mResult.addElement(xmlExport);
		mResult.addElement(mTransferData);
		return true;
	}

	private boolean prepareOutputData() {
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	public static void main(String[] args) {

	}
}
