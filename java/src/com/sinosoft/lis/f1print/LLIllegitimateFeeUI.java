/*
 * <p>ClassName: LLIllegitimateFeeUI </p>
 * <p>Description: LLIllegitimateFeeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-08-22
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLIllegitimateFeeUI {
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();

    public static void main(String[] agrs) {
        String StartDay = "2006-01-01";
        String EndDay = "2006-01-18";

        VData tVData = new VData();

        tVData.addElement(StartDay);
        tVData.addElement(EndDay);
        LLIllegitimateFeeUI UI = new LLIllegitimateFeeUI();
        if (!UI.submitData(tVData, "PRINT")) {
            if (UI.mErrors.needDealError()) {
                System.out.println(UI.mErrors.getFirstError());
            } else {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        } else {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();
        System.out.println("LLIllegitimateFeeBL...");
        LLIllegitimateFeeBL tLLIllegitimateFeeBL = new LLIllegitimateFeeBL();
        if (!tLLIllegitimateFeeBL.submitData(mInputData, mOperate)) {
            if (tLLIllegitimateFeeBL.mErrors.needDealError()) {
                mErrors.copyAllErrors(tLLIllegitimateFeeBL.mErrors);
                return false;
            } else {
                buildError("submitData", "LLIllegitimateFeeUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        } else {
            mResult = tLLIllegitimateFeeBL.getResult();
            return true;
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLBranchCaseReportUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
