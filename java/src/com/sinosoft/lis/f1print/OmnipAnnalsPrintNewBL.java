package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;

/**
 * <p>Title: 万能年报打印（单打） </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class OmnipAnnalsPrintNewBL implements PrintService {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    SSRS mSSRS = new SSRS();

    SSRS tSSRS = new SSRS();

    private String mPolYear = "";

    private String mPolMonth ="";

    private String mOperate = "";

    private String mRunDateStart = ""; //批处理起期

    private String mRunDateEnd = "" ;   //

    private String mTempPolYear ="";

    private String mStartDate ="";

    private String mEndDate ="";

    private String mSumMoney ="";
    
    private ExeSQL mExeSQL = new ExeSQL();
    
    DecimalFormat df = new DecimalFormat("#.00");

    public OmnipAnnalsPrintNewBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("==OmnipAnnalsPrintNewBL start==");

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

//        if (!getListData()) {
//            return false;
//        }

//        if (!getList(getPolno (),getInsuAccNo()))
//        {
//            return false;
//        }

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        //加入到打印列表
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("==OmnipAnnalsPrintNewBL end==");

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if( mGlobalInput==null || mLOPRTManagerSchema==null )
        {
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsPrintNewBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mPolMonth = mLOPRTManagerSchema.getStandbyFlag1();
        mPolYear  = mLOPRTManagerSchema.getStandbyFlag3();
        System.out.println("保单的结算年度："+mPolYear+"年，结算月度："+mPolMonth+"'月");

        mRunDateStart = mLOPRTManagerSchema.getStandbyFlag2();

        mRunDateEnd   = mLOPRTManagerSchema.getStandbyFlag4();

        //20161107多个万能账户处理 
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());
        if(!tLCPolDB.getInfo()){
        	 CError tError = new CError();
             tError.moduleName = "OmnipAnnalsPrintNewBL.java";
             tError.functionName = "dealPrintMag";
             tError.errorMessage = "获得险种信息失败！";
             mErrors.addOneError(tError);
             return false;
        }
        mLCPolSchema = tLCPolDB.getSchema();
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPolSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsPrintNewBL.java";
            tError.functionName = "dealPrintMag";
            tError.errorMessage = "获得保单信息失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }

    private boolean getListData()
    {
        String sql =" select  (select riskname from lmrisk where riskcode= b.RiskCode), b.cvalidate "
             +" ,(select amnt from lcpol where b.polno = polno) "
             +" ,(select codename from ldcode where codetype ='payintv' and code = cast(payintv as char(2))) "
             +",(select prem from lcpol where b.polno = polno) "
             +",polyear from LCPOL b, LCInsureAccBalance c where 1 = 1  "
             +" and c.RunDate>='"+mRunDateStart+"'  and c.RunDate<='"+mRunDateEnd+"' "
             +" and b.ContNo = c.ContNo    and b.polno = c.polno "
             +" and year(DueBalaDate - 1 days) = "+mPolYear+""
             +" and polmonth = "+mPolMonth+""
             +" and c.BalaCount > 0 "
             +"and b.contno ='"+mLCContSchema.getContNo()+"'"
             ;

        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(sql.toString());
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "getListData";
            tError.functionName = "getListData";
            tError.errorMessage = "获取打印数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSumMoney = mSSRS.GetText(1,5);
        return true;
    }

    private boolean getPrintData()
    {
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
//        textTag.add("Phone", agntPhone);
        textTag.add("AgentPhone", agntPhone);

         XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
         xmlexport.createDocument("OmnipAnnalsFX.vts", "printer");
         textTag.add("JetFormType", "OM001");


        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr(this,"操作员机构查询出错！");
            return false;
        }

         String printcom ="select codename from ldcode where codetype='pdfprintcom' and code='"
                          +comcode + "' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);
         textTag.add("ManageComLength4", printcode);
         textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
         if ("batch".equals(mOperate)) {
             textTag.add("previewflag", "0");
         } else {
             textTag.add("previewflag", "1");
         }

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                           " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                           " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,"投保人表中缺少数据");
            return false;
        }

        String sql =" select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='"
                   +  mLCContSchema.getContNo() + "') AND AddressNo = '"
                   +  tLCAppntDB.getAddressNo() + "'"
                   ;
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).getSchema();
        textTag.add("AppntName",tLCAppntDB.getAppntName()); //投保人姓名

        setFixedInfo();
        textTag.add("GrpZipCode", mLCAddressSchema.getZipCode());
        System.out.println("GrpZipCode=" + mLCAddressSchema.getZipCode());
        textTag.add("GrpAddress", mLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if (mLCAddressSchema.getPhone() != null &&
            !mLCAddressSchema.getPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getPhone() + "、";
        }
        if (mLCAddressSchema.getHomePhone() != null &&
            !mLCAddressSchema.getHomePhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getHomePhone() + "、";
        }
        if (mLCAddressSchema.getCompanyPhone() != null &&
            !mLCAddressSchema.getCompanyPhone().equals("")) {
            appntPhoneStr += mLCAddressSchema.getCompanyPhone() + "、";
        }
        if (mLCAddressSchema.getMobile() != null &&
            !mLCAddressSchema.getMobile().equals("")) {
            appntPhoneStr += mLCAddressSchema.getMobile() + "、";
        }


//        textTag.add("PrintDate",PubFun.getCurrentDate2());//打印日期
        textTag.add("ContNo",mLCContSchema.getContNo());

        //被保人信息
        textTag.add("InsuredName",mLCPolSchema.getInsuredName());

        //结算年度：
        mTempPolYear = new ExeSQL().getOneValue(
                "select polYear From lcinsureaccbalance where  year(DueBalaDate - 1 days) =" +
                mPolYear +
                " and polmonth = 12 and contno ='" +
                mLCContSchema.getContNo() +
                "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        //报告期间
        if("1".equals(mTempPolYear))
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select min(DueBalaDate) From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }
        else
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear =" +
                    mTempPolYear +
                    " and polmonth = 1 and contno ='" +
                    mLCContSchema.getContNo() +
                    "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ");
        }

        String baladate = "select max(DueBalaDate)-1 day,max(DueBalaDate) From lcinsureaccbalance where  PolYear = " 
        	   + mTempPolYear + " and polmonth = 12 and contno = '" + mLCContSchema.getContNo() 
        	   + "' and polno ='"+mLCPolSchema.getPolNo()+"' with ur ";
        SSRS dateSSRS = mExeSQL.execSQL(baladate);
        mEndDate = dateSSRS.GetText(1, 1);
        String tBalaDate = dateSSRS.GetText(1, 2);//结算日期

        textTag.add("startDate", CommonBL.decodeDate(mStartDate));//报告期间起
        textTag.add("endDate", CommonBL.decodeDate(mEndDate));//报告期间止

//        if (!getList(getPolno(), getInsuAccNo())) {
//            return false;
//        }
//        String startMoney = mExeSQL.getOneValue("select insuaccbalabefore From lcinsureaccbalance where polYear = " 
//     			   + mTempPolYear + " and polmonth = 1 and contno ='" + mLCContSchema.getContNo()  
//    			   + "' order by int(balacount) with ur ");
//
//        String endMoney = mExeSQL.getOneValue("select InsuAccBalaAfter From lcinsureaccbalance where polYear = " 
//        		+ mTempPolYear + " and polmonth = 12 and contno ='" + mLCContSchema.getContNo()
//                + "' order by int(balacount) desc with ur ");
        
//      本报告期期初个人账户价值
//        String startMoney = mExeSQL.getOneValue("select sum(money) From lcinsureacctrace where contno = '" 
//        		+ mLCContSchema.getContNo() + "' and paydate <= '" + mStartDate + "' with ur ");
//      本报告期期末个人账户价值
//        String endMoney = mExeSQL.getOneValue("select sum(money) From lcinsureacctrace where contno = '" 
//        		+ mLCContSchema.getContNo() + "' and paydate <= '" + tBalaDate + "' with ur ");
//        
//        if(startMoney.equals("")||startMoney==null)
//        {
//            CError.buildErr(this, "获得年度初账户价值失败！");
//            return false;
//        }
//        
//        if(endMoney.equals("")||endMoney==null)
//        {
//            CError.buildErr(this, "获得年度末账户价值失败！");
//            return false;
//        }
        textTag.add("BarCode1", createWorkNo()); //条形码
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

//        DecimalFormat df = new DecimalFormat("#.00");
//        textTag.add("startMoney",df.format(Double.parseDouble(startMoney))); //本报告期期初个人账户价值
//        textTag.add("endMoney", df.format(Double.parseDouble(endMoney))); //本报告期期末个人账户价值

        //获得全年风险保费
//        String PRPrem = new ExeSQL().getOneValue("select sum(MONEY) from lcinsureacctrace where  moneytype IN('RP','CM') and contno ='"+mLCContSchema.getContNo()+"'");
//        String bakString ="";
        //如果本年的账户风险保费大于账户价值则打印此说明
//        if (Math.abs(Double.parseDouble(PRPrem))>Double.parseDouble(endMoney))
//        {
//            bakString = "本报告期末个人账户价值已不能保证本保单在下一保单年度继续有效，请您及时缴交所缓交的期交基本保险费，以保障您的权益。";
//            textTag.add("bak1",bakString);
//        }else
//        {
//             textTag.add("bak1",bakString);
//        }
//        textTag.add("sumMoney",mSumMoney);
        
//      获得契约初始扣费 add by xp 090714
//        String BFPrem = new ExeSQL().getOneValue("select fee from lcinsureaccfeetrace where  moneytype='GL' and contno ='"+mLCContSchema.getContNo()+"'");
//        textTag.add("BFPrem",BFPrem);
        
//      20091118 req00000573 新增内容 zhanggm
        if(!addNewInfo())
        {
        	return false;
        }

        //添加节点
        if (textTag.size() > 0) {
            xmlexport.addTextTag1(textTag);
        }

//        String[] title = {"险种名称", "险种生效日", "报告期末保险金额","交费方式（年缴/趸缴）","报告期末保险费"};
//        xmlexport.addListTable(getListTable(), title);
//        ListTable tEndTable = new ListTable();
//        tEndTable.setName("RiskEND");
//        String[] tEndTitle = new String[0];
//        xmlexport.addListTable(tEndTable, tEndTitle);

//        SSRS t = new SSRS();
//        String[] title2 = {"交易时间","交易类别","金额"};
//        xmlexport.addListTable(getListTable(tSSRS),title2);
//        ListTable xEndTable = new ListTable();
//        xEndTable.setName("END");
//        String[] xEndTitle = new String[0];
//        xmlexport.addListTable(xEndTable, xEndTitle);
        
        xmlexport.outputDocumentToFile("C:\\", "OmnipAnnalsFX");
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    private boolean dealPrintMag()
    {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("OM001");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//        mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
//        mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        String tempMoney ="";
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    private ListTable getListTable(SSRS tSSRS)
    {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                    info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("LIST");
        return tListTable;
    }
    /**
     * 保单个人账户摘要
     * @return boolean
     */
    private boolean getList(String polno ,String InsuaAccNO)
    {
        String sql ="   select w.h 日期,w.i 类别,w.j 类型,w.k 金额 from "
                   +" ( "
                   + " select  "
                   + " char(a.PayDate) h "
                   + " ,a.MoneyType i "
                   + " ,(select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType) j "
                   + " ,a.money k,a.paydate "
                   + " from LCInsureAccTrace a, LCInsureAcc b  "
                   + " where a.PolNo = '"+polno+"' "
                   + " and a.InsuAccNo = '"+InsuaAccNO+"' "
                   + " and a.PolNo = b.PolNo "
                   + " and a.InsuAccNo = b.InsuAccNo "
                   + " and a.paydate between '"+mStartDate+"' and '"+mEndDate+"'"
//                   + " and a.OtherType not in ('1', '5') "
                   + " union all "
                   + " select "
                   + " x.a h,'' i,x.c j,x.d k,x.b "
                   + " from "
                   + " (select  "
                   + " '小计' a "
                   + " ,char(paydate) b "
                   + " ,'' c "
                   + " ,sum(a.money) d "
                   + " from LCInsureAccTrace a, LCInsureAcc b "
                   + " where a.PolNo = '"+polno+"' "
                   + " and a.InsuAccNo = '"+InsuaAccNO+"' "
                   + " and a.PolNo = b.PolNo "
                   + " and a.paydate between '"+mStartDate+"' and '"+mEndDate+"'"
                   + " and a.InsuAccNo = b.InsuAccNo "
                   + " group by paydate "
                   + " ) x "
                   + " order by 5,1) w "
                  ;

       tSSRS = new ExeSQL().execSQL(sql);
       if (tSSRS.getMaxRow() == 0) {
           CError tError = new CError();
           tError.moduleName = "getList";
           tError.functionName = "getList";
           tError.errorMessage = "获取打印数据失败！";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
    }

    /**
     * 根据账户保单号码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    private String getInsuAccNo() {
        String sql = "select InsuAccNo from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }

    private String getPolno ()
    {
        String sql = "select polno from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }
    
    /**
     * 生成流水号
     * @return String
     */
    private String createWorkNo() {

        String str = "16";
        String allString = "";
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(2, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        allString = str + tDate;
        String tWorkNo = tDate + PubFun1.CreateMaxNo("GOOD" + allString, 6);
        str += tWorkNo;
        return str;
    }
//  20091118 req00000573 新增内容 zhanggm    
    private boolean addNewInfo()
    {
    	System.out.println("20091118 req00000573 新增内容新增内容开始————————————————————————");
        String tContNo = mLCContSchema.getContNo();
        String sql1 = "select (select riskname from lmrisk where riskcode = a.riskcode),cvalidate, "
        	        + "a.riskcode,prem,payintv,amnt,polno from lcpol a "
        	        + "where contno = '" + tContNo + "' and exists (select 1 from lmriskapp "
        	        + "where riskcode = a.riskcode and risktype4 = '" + BQ.RISKTYPE1_ULI + "')"
        	        + " and polno ='"+mLCPolSchema.getPolNo()+"' with ur";
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = mExeSQL.execSQL(sql1);
        textTag.add("RiskCodeName", tSSRS1.GetText(1, 1)); //产品名称
        textTag.add("CValiDate", CommonBL.decodeDate(tSSRS1.GetText(1, 2))); //保单生效日
        
//    	首期缴纳进账户的保险费
    	sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'BF' and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstPrem1 = Double.parseDouble(mExeSQL.getOneValue(sql1));
    	
//    	首期缴纳保险费扣费
    	sql1 = "select nvl(sum(fee),0) from lcinsureaccfeetrace where moneytype = 'GL' and contno = '" 
    		+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
    	double firstFee = Double.parseDouble(mExeSQL.getOneValue(sql1));
        
//    	首期缴纳进账户的保险费
    	double firstPrem = firstPrem1 + firstFee;
    	
        String tRiskCode = tSSRS1.GetText(1, 3);
        sql1 = "select cpayflag from lmrisk where riskcode = '" + tRiskCode + "' with ur"; //cpayflag='Y'可续期
        String tCPayFlag = mExeSQL.getOneValue(sql1);
        
//      报告期间保险金额
        String tAmnt = null;
/*		由2010-12-22日17:35邮件需求将所有万能年报的“报告期间保险金额” 都改成打印年报时的系统当前时刻保额
 *        if(!"".equals(tCPayFlag) && "Y".equals(tCPayFlag))
        {
//        	得到本期缴费起始时间
            if("1".equals(mTempPolYear))
            {
            	sql1 = "select paydate from lcinsureacctrace where contno = '" + tContNo 
        		+ "' and moneytype = 'BF' and othertype = '1' with ur";
            }
            else
            {
            	sql1 = "select paydate + 1 day From lcinsureacctrace a where contno = '" + tContNo 
    			+ "' and moneytype = 'BF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
    			+ "a.contno and year(curpaytodate - 1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
            }
            String startPaydate = mExeSQL.getOneValue(sql1);

            if("".equals(startPaydate))
            {
            	tAmnt = tSSRS1.GetText(1, 6); 
            }
            else
            {
            	sql1 = "select edoracceptno from lpedoritem a where edortype = 'BA' and exists (select 1 from lpedorapp where edoracceptno = " 
            		+ "a.edoracceptno and edorstate = '0') and edorvalidate >= '" + startPaydate + "'  and contno = '" 
            		+ tContNo + "' order by edorvalidate asc with ur";
            	String tEdorNo = mExeSQL.getOneValue(sql1);
           		if("".equals(tEdorNo))
           		{
           			tAmnt = tSSRS1.GetText(1, 6); 
           		}
           		else
           		{
           			sql1 = "select amnt from lppol where edorno = '" + tEdorNo + "' and edortype = 'BA' and contno = '" 
           			     + tContNo + "' and riskcode = '" + tRiskCode + "' with ur";
           			tAmnt = mExeSQL.getOneValue(sql1);
           		}
            }
            if("".equals(tAmnt))
            {
            	tAmnt = tSSRS1.GetText(1, 6); 
            }
        }
        else
        {
        	tAmnt = tSSRS1.GetText(1, 6); 
        }*/
        tAmnt = tSSRS1.GetText(1, 6); 
        //新加保额算投保时的保额
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setAmnt(tSSRS1.GetText(1, 6));
        tLCPolSchema.setContNo(tContNo);
        tLCPolSchema.setPolNo(tSSRS1.GetText(1, 7));        
        double qianAmnt=CommonBL.getTBAmnt(tLCPolSchema);
        
        
        
        
        textTag.add("Amnt", PubFun.setPrecision2(tAmnt)); //报告期间保险金额
        
//    	期缴保险费
    	double tPrem = 0.0;
//    	基本保险费
    	double payPrem = 0.0;
//    	额外保险费
    	double otherPrem = 0.0;
//    	趸缴保险费
    	double dunPrem = 0.0;
    	
        if(!"".equals(tCPayFlag) && "Y".equals(tCPayFlag))
        {
            if("1".equals(mTempPolYear))
            {
            	tPrem = firstPrem;
            }
            else
            {
//            	续期缴纳保险费
            	sql1 = "select nvl(sum(money),0) From lcinsureacctrace a where contno = '" + tContNo 
        			+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype = 'BF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
        			+ "a.contno and year(curpaytodate- 1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
            	tPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));
            }
            dunPrem = 0.0;
            String sqlCode="select 1 from ldcode where codetype='uliprint' and code='"+tRiskCode+"' and codealias='2'";
            String rs = new ExeSQL().getOneValue(sqlCode);
        	if(!rs.equals("") && !rs.equals("null"))
        	{
        		System.out.println("健康宝(330801，331501,331801)的基本保费为3者中最小值：1、期交保费，2、6000 元，3、本结算年度的基本保险金额（如果基本保险金额本年度有调整，以调整后的为准）除以20。");
        		String tPayIntv = tSSRS1.GetText(1, 5);
        		double prem1 = tPrem; //期交保费
        		double prem2 = 6000 * Double.parseDouble(tPayIntv) / 12; //6000 元
        		double prem3 = qianAmnt * Double.parseDouble(tPayIntv) / (20*12); //本结算年度的基本保险金额除以20
        		payPrem = prem1<=prem2?(prem1<=prem3?prem1:prem3):(prem2<=prem3?prem2:prem3);//取3者中最小值
        		otherPrem = prem1-payPrem; //额外保险费=期缴-基本
        		System.out.println("基本保险费="+CommonBL.carry(payPrem)+",额外保险费="+CommonBL.carry(otherPrem));//基本保险费
        	}
        	else
        	{
        		payPrem = tPrem;
        		otherPrem = 0.0;
        	}
        }
        else
        {
        	tPrem = 0.0;
        	payPrem = 0.0;
        	otherPrem = 0.0;
        	dunPrem = firstPrem;
        }
        
        textTag.add("Prem", PubFun.setPrecision2(tPrem)); //期缴保险费
    	textTag.add("PayPrem", PubFun.setPrecision2(payPrem)); //基本保险费
    	textTag.add("OtherPrem", PubFun.setPrecision2(otherPrem)); //额外保险费
    	textTag.add("DunPrem", PubFun.setPrecision2(dunPrem)); //趸缴保险费
        
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace where moneytype = 'ZF' and contno = '" 
        	+ tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and othertype = '1' with ur";
        double tbAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//首期追加保险费
 		
        sql1 = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
        	 + tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        double bqAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费
        
        double addPrem = 0.0; 
        if("1".equals(mTempPolYear))
        {
        	addPrem = tbAddPrem + bqAddPrem;
        }
        else
        {
        	addPrem = bqAddPrem;
        }
        textTag.add("AddPrem", PubFun.setPrecision2(addPrem)); //追加保险费：本结算年度的追加保费之和  
        
        double totalPrem = tPrem + dunPrem + addPrem;
        textTag.add("TotalPrem", PubFun.setPrecision2(totalPrem)); //累计保险费：期缴+趸缴+追加
        
        if("1".equals(mTempPolYear))
        {
        	sql1 = "select sum(fee) from lcinsureaccfeetrace where contno = '" + tContNo 
        		+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype in ('GL','KF') and othertype = '1' with ur";
        }
        else
        {
        	sql1 = "select nvl(sum(fee),0) From lcinsureaccfeetrace a where contno = '" + tContNo 
    			+ "' and polno ='"+mLCPolSchema.getPolNo()+"' and moneytype = 'KF' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
    			+ "a.contno and year(curpaytodate-1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
        }
        double fee1 = Double.parseDouble(mExeSQL.getOneValue(sql1));//续期or首期费用
        
        sql1 = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'KF' and contno = '" 
         	 + tContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
        double fee2 = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费扣费
        
        double fee = fee1 + fee2;
        textTag.add("Fee", PubFun.setPrecision2(fee));//初始费用（扣除项）：本结算年度的初始费用之和
        
        sql1 = "select nvl(sum(b.money),0) "
        	+ "from LCInsureAccbalance a , lcinsureacctrace b "
        	+ "where a.contno = '" + tContNo + "' and a.polno ='"+mLCPolSchema.getPolNo()+"' and a.polyear = " + mTempPolYear 
        	+ " and b.moneytype = 'B' and a.sequenceno = b.otherno with ur ";
        textTag.add("BMoney", PubFun.setPrecision2(mExeSQL.getOneValue(sql1)));//持续奖金：本结算年度的持续奖金之和
        
        
//      生成结算利息列表,报告期内各月年化结算利率及结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthRate(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月年化利率失败。");
                return false;
            }
        	
        }
        
		ExeSQL tExeSQL = new ExeSQL();
        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql);
        textTag.add("Year1", tSSRS.GetText(1, 1));
        textTag.add("Quarter1", tSSRS.GetText(1, 2));
        textTag.add("Solvency", tSSRS.GetText(1, 3));
        textTag.add("Flag", tSSRS.GetText(1, 4));
        
        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql2);
        textTag.add("Year2", tSSRS.GetText(1, 1));
        textTag.add("Quarter2", tSSRS.GetText(1, 2));
        textTag.add("RiksRate", tSSRS.GetText(1, 3));
        
//      生成各月结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthList(tContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tContNo+"第"+mTempPolYear+"年"+tPolMonth+"月结算信息明细失败。");
                return false;
            }
        }
        System.out.println("20091118 req00000573 新增内容新增内容结束————————————————————————");
    	return true;
    }
/*    //得到保额
    private double getTBAmnt(LCPolSchema aLCPolSchema)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	double tAmnt = aLCPolSchema.getAmnt();
    	try 
    	{
			String tContNo = aLCPolSchema.getContNo();
			String sql = "select edoracceptno from LPEdorItem a where ContNo = '" + tContNo + "' and EdorType = 'BA' "
				       + "and exists (select 1 from LPEdorApp where EdorAcceptNo = a.EdorAcceptNo and EdorState = '0') " 
				       + "order by edoracceptno ";
			String firstEdorNo = tExeSQL.getOneValue(sql);
			if(!firstEdorNo.equals(""))
			{
				sql = "select Amnt from LPPol where EdorNo = '" + firstEdorNo  + "' and PolNo = '" + aLCPolSchema.getPolNo() 
				    + "' and EdorType = 'BA' ";
				String sAmnt = tExeSQL.getOneValue(sql);
				tAmnt = Double.parseDouble(sAmnt);
			}
			
		} 
    	catch (RuntimeException e)
    	{
			e.printStackTrace();
			return tAmnt;
		}
    	return tAmnt;
    }*/
    
//  生成结算利息列表,报告期内各月年化结算利率及结算信息
    private void setMonthRate(String aContNo,String aRiskCode,String aPolMonth)
    {
    	String sql = "";
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("RateMonth"+aPolMonth, decodeDate(tEndDate));
    	
    	sql = "select rate From LMInsuAccRate where riskcode = '" + aRiskCode 
    		+ "' and baladate = '" + tDueBalaDate + "' with ur";
     	String rate = mExeSQL.getOneValue(sql);
     	textTag.add("Rate"+aPolMonth, String.valueOf(Double.valueOf(rate))); //年化结算利率
    }

	//  生成各月结算信息
    private void setMonthList(String aContNo,String aRiskCode,String aPolMonth)
    {
    	textTag.add("PolMonth"+aPolMonth, SysConst.TEG_START);
    	String sql = "";
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
        	sql = "select min(DueBalaDate) From lcinsureaccbalance where polyear = 1 " 
        		+ "and polmonth = 1 and contno = '" + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance where polyear = " 
        		+ mTempPolYear + " and polmonth = " + aPolMonth + " and contno = '" + aContNo + "'"
        		+ " and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = mExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance where polYear = " 
			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
			   + "' and polno ='"+mLCPolSchema.getPolNo()+"' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("Month"+aPolMonth, decodeDate(tEndDate));
    	
//    	sql = "select insuaccbalabefore From lcinsureaccbalance where polYear = " 
//			   + mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo 
//			   + "' order by int(balacount) with ur ";
//    	textTag.add("StartMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
//    	
//    	sql = "select InsuAccBalaAfter From lcinsureaccbalance where  polYear = " 
//    		+ mTempPolYear + " and polmonth = " + aPolMonth + " and contno ='" + aContNo
//            + "' order by int(balacount) desc with ur ";
//    	textTag.add("EndMoney"+aPolMonth, CommonBL.carry(mExeSQL.getOneValue(sql))); //本月期末帐户价值
     	
//     	本月期初帐户价值
    	
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
    	
        
        textTag.add("StartMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
        
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '" 
       	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
       	 + tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
    	textTag.add("BQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
    	
    	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype ='KF' and contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" + tStartDate + "' and '"
          	 + tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_P + "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
       	textTag.add("BQAddPremFee"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费扣费
       	
       	if("12".equals(aPolMonth))
       	{
       		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype = 'BF' and contno = '" 
             	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" 
             	 + tStartDate + "' and '"+ tEndDate + "' and othertype = '2' with ur";
          	textTag.add("NewPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //下期期缴保费
          
          	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'GL' and contno = '" 
            	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate between '" 
            	 + tStartDate + "' and '"+ tEndDate + "' and othertype = '2' with ur";
         	textTag.add("NewPremFee"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //下期期缴保费初始费用
       	}
       	    	
    	sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
          	 + "nvl(sum(case when moneytype = 'RP' then -money end),0), " 
          	 + "nvl(sum(case when moneytype = 'MF' then -money end),0) " 
          	 + "from lcinsureacctrace a where contno = '" 
          	 + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()
          	 +"' and otherno in (select sequenceno from lcinsureaccbalance where polYear = " 
         	 + mTempPolYear + " and polmonth = " + aPolMonth + " and contno = a.contno) with ur ";
    	SSRS tSSRS2 = mExeSQL.execSQL(sql);
        textTag.add("LXMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
        textTag.add("RPMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //风险保险费
        textTag.add("MFMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 3))); //保单管理费
        
        sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype = 'LQ' and othertype = '"
     	     + BQ.NOTICETYPE_P + "' and contno = '" + aContNo + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate >= '"
     	     + tStartDate + "' and paydate <= '" + tEndDate + "' with ur";
        textTag.add("LQMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql)));//部分领取
        
//      本月期末帐户价值
        sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
        textTag.add("EndMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
        
        textTag.add("PolMonth"+aPolMonth, SysConst.TEG_END);
    }
    /**
     * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
     * @param date String
     * @return String
     */
    private String decodeDate(String date)
    {
        if ((date == null) || (date.equals("")))
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        String data[] = date.split("-");
        if (data.length != 3)
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        return (data[0] + "年" + data[1] + "月");
    }
}
