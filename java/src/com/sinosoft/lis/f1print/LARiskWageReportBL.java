package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LARiskWageReportBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
       private String mBranchAttr = "";
       private String mAgentGroup= "";
       private String mBranchLevel= "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;
       private String[] mDataList= null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }
       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(mInputData)) {
             return false;
        }
      // 进行数据查询
      if (!queryData()) {
            return false;
        }
    System.out.println("dayinchenggong1232121212121");
       return true;
     }


     private void bulidErrorB(String cFunction, String cErrorMsg)
     {
             CError tCError = new CError();

             tCError.moduleName = "LARiskWageReportBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);
    }

     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {
       try
       {
       mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
       mTransferData = (TransferData) cInputData.getObjectByObjectName(
                      "TransferData", 0);
       //页面传入的数据 三个
       this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
       this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
       this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
       this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
       //  System.out.println(mManageCom);
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
       String tSQL = "select agentgroup,branchlevel from labranchgroup where branchattr='" + mBranchAttr +"' and branchtype='1' and branchtype2='01'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       this.mAgentGroup = tSSRS.GetText(1, 1);
       this.mBranchLevel=tSSRS.GetText(1, 2);
       System.out.println("AgentGroup="+mAgentGroup+"  BranchLevel="+mBranchLevel);
          }

       }
       catch (Exception ex) {
       this.mErrors.addOneError("");
       return false;
       }
       return true;
   }


     /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LARiskWageReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



    private boolean queryData()
      {
        try{
          // 查询数据
          if(!getDataList())
          {
            return false;
          }
//            System.out.println(mShowDataList.length);
//            System.out.println(mShowDataList[0].length);
          this.mDataList = new String[mShowDataList[0].length];
             // 设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Order");
          TextTag  tTextTag = new TextTag();
          String tMakeDate = "";
          String tMakeTime = "";
          tMakeDate = mPubFun.getCurrentDate();
          tMakeTime = mPubFun.getCurrentTime();

          for(int i=0;i<mShowDataList.length;i++)
          {
            tlistTable.add(mShowDataList[i]);
          }
         if(!setAddRow(mShowDataList.length + 1))
          {
            buildError("queryData", "进行合计计算时出错！");
            return false;
          }
          else  { tlistTable.add(mDataList);  }
            //新建一个TextTag的实例
          System.out.print("dayin252");
          if (!getManageNameB()) {
           return false;
          }
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
          tTextTag.add("tName", mManageName);
          tTextTag.add("StartDate",mStartDate);
          tTextTag.add("EndDate",mEndDate);

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LARiskWageF.vts", "PRINT"); //初始化xml文档
          if (tTextTag.size() > 0)
          xmlexport.addTextTag(tTextTag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new2");
          this.mResult.clear();
          mResult.addElement(xmlexport);
          System.out.print("22222222222222222");
          return true;
          }
          catch(Exception ex)
          {
              buildError("queryData", "LABL发生错误，准备数据时出错！");
              return false;
          }
  }


  private boolean getDataList()
   {
       String tSQL = "";
       // 1、得到全部已开业的机构
       tSQL  = "select distinct riskcode from  lacommision   where renewcount=0 and payyear=0  ";
       tSQL += "    and managecom like  '" + mManageCom + "%'";
       tSQL += "    and branchtype='1' and  branchtype2='01'";
    //	   tSQL += "    and riskcode in (select riskcode from lmriskapp where riskprop='I')  ";
       tSQL += "   and tmakedate >= '" + mStartDate + "'";
       tSQL += "   and tmakedate <= '" + mEndDate + "'";
       if (mBranchAttr != null && !mBranchAttr.equals(""))
      {
          if(mBranchLevel.equals("03"))
          {
           tSQL+= " and branchseries like '" + mAgentGroup + "%' ";
           }
          else if( mBranchLevel.equals("01"))
          {
            tSQL+= " and agentgroup ='" + mAgentGroup + "' ";
          }
           else
           tSQL+= " and branchseries like '%" + mAgentGroup + "%' ";
       }

       tSQL += " order by riskcode";
       System.out.println(tSQL);

       SSRS tSSRS3 = new SSRS();
       ExeSQL tExeSQL3 = new ExeSQL();
       tSSRS3 = tExeSQL3.execSQL(tSQL);
      /* LACommisionDB tLACommisionDB = new LACommisionDB ();
       LACommisionSet tLACommisionSet = new LACommisionSet();
       tLACommisionSet = tLACommisionDB.executeQuery(tSQL);
       if(tLACommisionSet.size()==0)
       {
           buildError("queryData", "没有符合条件的机构！");
           return false;
       }
*/
       if(tSSRS3.getMaxRow()==0)
       {
           buildError("queryData", "没有符合条件的信息！");
           return false;
       }

       // 2、查询需要表示的数据
       String[][] tShowDataList = new String[tSSRS3.getMaxRow()][7];
       for(int i=1;i<=tSSRS3.getMaxRow();i++)
       {// 循环机构进行统计
          // LACommisionSchema tLACommisionSchema = new LACommisionSchema();
           //tLACommisionSchema= tLACommisionSet.get(i);
       tShowDataList[i-1][0] = tSSRS3.GetText(i,1);

       // 查询需要表示的数据
       if (!queryOneDataList( tSSRS3.GetText(i,1),tShowDataList[i - 1]))
       {
       // System.out.println("[" + tLACommisionSchema.getAgentGroup() +
       //                    "] 本机构数据查询失败！");
          return false;
       }
       }
       mShowDataList = tShowDataList;

       return true;
  }

  /**
    * 查询填充表示数据
    * @return boolean
    */
   private boolean queryOneDataList(String pmRiskCode,String[] pmOneDataList)
   {
       try{
           String sum="";
          //1、险种编码
           pmOneDataList[0] = pmRiskCode;
           // 2、险种名称
           pmOneDataList[1] = getRiskName(pmOneDataList[0]);
           // 3、新单保费
           pmOneDataList[2] = getNewPrem(pmOneDataList[0]);
             // 4、件数
           pmOneDataList[3] = getNewCont(pmOneDataList[0]);
           // 5、退保保费
           pmOneDataList[4] = getReturnPrem(pmOneDataList[0]);
            // 6、退保件数
           pmOneDataList[5] =getReturnCont(pmOneDataList[0]);
             // 7、有效客户数
           //pmOneDataList[6] = getAgentNo(pmOneDataList[0]);
           // 8、保费占比
           sum=getSumPrem();
           pmOneDataList[6] = getAvgPremAct(pmOneDataList[2],sum);

       }
       catch(Exception ex)
       {
           buildError("queryOneDataList", "准备数据时出错！");
           System.out.println(ex.toString());
           return false;
       }

       return true;
   }

   /**
      * riskname
      * 根据
      * @param
      * @return String
      */
     private String getRiskName(String pmRiskCode)
     {
       String tSQL = "";
       tSQL  = "select riskname from lmrisk ";
       tSQL += "  where   riskcode='" + pmRiskCode+ "'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       return  tSSRS.GetText(1,1);

    }

  /**
    * 查询新单保费
    * 根据
    * @param
    * @return String
    */
   private String getNewPrem(String pmRiskCode)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.##");


     tSQL  = "select sum(c.transmoney)   from  lacommision c";
     tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
     tSQL += "  and  c.riskcode='"+pmRiskCode+"'";
     tSQL += "   and  c.renewcount=0 and c.payyear=0   and c.transtype = 'ZC'";
     tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
     tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
     tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
     if (mBranchAttr != null && !mBranchAttr.equals(""))
      {
          if(mBranchLevel.equals("03"))
          {
           tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
           }
          else if( mBranchLevel.equals("01"))
          {
            tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
          }
           else
           tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
       }


     System.out.println(tSQL);
     try{
         tRtValue = tDF.format(execQuery(tSQL));

     }
     catch(Exception ex)
     {
         System.out.println("getNewPrem 出错！");
     }

     return tRtValue;

  }


  /**
   * 查询件数
   * @param  pmBranchSeries String
   * @return String
   */
  private String getNewCont(String pmRiskCode)
  {
    String tSQL = "";
    int tRtValue = 0;
    tSQL  = "select count(distinct c.contno)   from  lacommision c";
    tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
    tSQL += "  and  c.riskcode='"+pmRiskCode+"'";
    tSQL += "   and  c.renewcount=0 and c.payyear=0   and c.transtype = 'ZC'";
    tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
    tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
    tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
    if (mBranchAttr != null && !mBranchAttr.equals(""))
      {
          if(mBranchLevel.equals("03"))
          {
           tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
           }
          else if( mBranchLevel.equals("01"))
          {
             tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
          }
           else
           tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
       }


    try{
        tRtValue = (int)execQuery(tSQL);
    }
    catch(Exception ex)
    {
        System.out.println("getNewCont出错！");
    }

    return ""+tRtValue;

 }

 /**
   * 查询退保保费
   * 根据
   * @param
   * @return String
   */
  private String getReturnPrem(String pmRiskCode)
  {
    String tSQL = "";
    String tRtValue = "";
    DecimalFormat tDF = new DecimalFormat("0.##");


    tSQL  = "select sum(c.transmoney)   from  lacommision c";
    tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
    tSQL += "  and  c.riskcode='"+pmRiskCode+"'";
    tSQL += "   and  c.renewcount=0 and c.payyear=0   and c.transtype = 'WT'";
    tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
    tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
    tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
    if (mBranchAttr != null && !mBranchAttr.equals(""))
      {
          if(mBranchLevel.equals("03"))
          {
           tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
           }
          else if( mBranchLevel.equals("01"))
          {
             tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
          }
           else
           tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
       }


    System.out.println(tSQL);
    try{
        tRtValue = tDF.format(execQuery(tSQL));
//        System.out.println(tRtValue);

    }
    catch(Exception ex)
    {
        System.out.println("getReturnPrem( 出错！");
    }

    return tRtValue;

 }

 /**
    * 查询退保件数
    * @param  pmBranchSeries String
    * @return String
    */
   private String getReturnCont(String pmRiskCode)
   {
     String tSQL = "";
     int tRtValue = 0;
     tSQL  = "select count(distinct  c.contno)   from  lacommision c";
     tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
     tSQL += "  and  c.riskcode='"+pmRiskCode+"'";
     tSQL += "   and  c.renewcount=0 and c.payyear=0   and c.transtype = 'WT'";
     tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
     tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
     tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
     if (mBranchAttr != null && !mBranchAttr.equals(""))
       {
           if(mBranchLevel.equals("03"))
           {
            tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
            }
           else if( mBranchLevel.equals("01"))
           {
              tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
           }
            else
            tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
        }




     System.out.println(tSQL);
     try{
         tRtValue = (int)execQuery(tSQL);
//        System.out.println(tRtValue);
     }
     catch(Exception ex)
     {
         System.out.println("getReturnCont(出错！");
     }

     return ""+tRtValue;

  }


  /**
      * 查询有效客户数
      * @param
      * @return String
      */
     private String getAgentNo(String pmRiskCode)
     {
       String tSQL = "";
       int tRtValue = 0;
       tSQL  = "select count(distinct c.p12)   from  lacommision c";
       tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
       tSQL += "  and  c.riskcode='"+pmRiskCode+"'";
       tSQL += "   and  c.renewcount=0 and c.payyear=0   ";
       tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
       tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
       tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
       if (mBranchAttr != null && !mBranchAttr.equals(""))
       {
           if(mBranchLevel.equals("03"))
           {
            tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
            }
           else if( mBranchLevel.equals("01"))
           {
              tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
           }
            else
            tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
        }


       System.out.println(tSQL);
       try{
           tRtValue = (int)execQuery(tSQL);
//        System.out.println(tRtValue);
       }
       catch(Exception ex)
       {
           System.out.println("getAgentNo( 出错！");
       }

       return ""+tRtValue;

  }



  private String getSumPrem()
  {
    String tSQL = "";
    String tRtValue = "";
    DecimalFormat tDF = new DecimalFormat("0.##");


    tSQL  = "select sum(c.transmoney)   from  lacommision c";
    tSQL += " where c.ManageCom like '" + mManageCom+ "%'";
    tSQL += "   and  c.renewcount=0 and c.payyear=0  ";
    tSQL += "  and  c.branchtype='1'   and c.branchtype2='01'";
    tSQL += "   and c.tmakedate >= '" + mStartDate + "'";
    tSQL += "   and c.tmakedate <= '" + mEndDate + "'";
    if (mBranchAttr != null && !mBranchAttr.equals(""))
       {
           if(mBranchLevel.equals("03"))
           {
            tSQL+= " and c.branchseries like '" + mAgentGroup + "%' ";
            }
           else if( mBranchLevel.equals("01"))
           {
             tSQL+= " and c.agentgroup ='" + mAgentGroup + "' ";
           }
            else
            tSQL+= " and c.branchseries like '%" + mAgentGroup + "%' ";
        }

    System.out.println(tSQL);
    try{
        tRtValue = tDF.format(execQuery(tSQL));
//        System.out.println(tRtValue);

    }
    catch(Exception ex)
    {
        System.out.println("getSumPrem( 出错！");
    }

    return tRtValue;

 }


 /**
    * 计算保费占比
    * @param
    * @param
    * @return String
    */
   private String getAvgPremAct(String pmNewPrem,String pmSumPrem)
   {
       String tSQL = "";
       String tRtValue = "";
       DecimalFormat tDF = new DecimalFormat("0.##");

       if("0".equals(pmSumPrem))
       {
           return "0";
       }

       else
           {
               tSQL = "select DECIMAL(DECIMAL(" + pmNewPrem + ",12,2) / DECIMAL(" +
              pmSumPrem + ",12,2) * 100,12,2) from dual";}
//          System.out.println(tSQL);
       try{
           tRtValue = "" + tDF.format(execQuery(tSQL));
//              System.out.println(tRtValue);
       }
       catch(Exception ex)
       {
           System.out.println("getAvgPremAct( 出错！");
       }

       return tRtValue;
   }


   /**
     * 追加一条合计行
     * @return boolean
     */
    private boolean setAddRow(int pmRow)
    {
        System.out.println("合计行的行数是："+pmRow);

        mDataList[0] = "合  计";
        mDataList[1] = "--";
        mDataList[2] = dealSum(2);
        mDataList[3] =dealSum(3);
        mDataList[4] =dealSum(4);
        System.out.println("行数是："+mDataList[0]);
        mDataList[5] = dealSum(5);
        mDataList[6] = "100";
//        mDataList[7] = dealSum(7);
        System.out.println("行数是："+pmRow);


        return true;
    }

    /**
     * 对传入的数组进行求和处理
     * @param pmArrNum int
     * @return String
     */
    private String dealSum(int pmArrNum)
    {
        String tReturnValue = "";
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tSQL = "select 0";

        for(int i=0;i<this.mShowDataList.length;i++)
        {
            tSQL += " + " + this.mShowDataList[i][pmArrNum];
        }

        tSQL += " + 0 from dual";

        tReturnValue = "" + tDF.format(execQuery(tSQL));

        return tReturnValue;
  }



   /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
        finally {
            try {
            if (!conn.isClosed()) {
                conn.close();
            }
            try {
                st.close();
                rs.close();
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }
            st = null;
            rs = null;
            conn = null;
          } catch (Exception e) {}

        }
    }



    private boolean getManageNameB() {

             String sql = "select name from ldcom where comcode='" + mManageCom +
                          "'";

             SSRS tSSRS = new SSRS();

             ExeSQL tExeSQL = new ExeSQL();

             tSSRS = tExeSQL.execSQL(sql);

             if (tExeSQL.mErrors.needDealError()) {

                 this.mErrors.addOneError("销售单位不存在！");

                 return false;

             }

             if (mManageCom.equals("86")) {
                 this.mManageName = "";
             }
             else {
                 if(mManageCom.length()>4)
                 {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
                 else
                 {this.mManageName = tSSRS.GetText(1, 1);}
             }
             System.out.println("1111111111111111111111");
             System.out.println(mManageName);
             return true;
      }






      /**
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }
}
