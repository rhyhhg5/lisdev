package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

class LISaleStatusStatisticBL {

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String year = "";

	private String month = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private SSRS mSSRS1 = new SSRS();

	// private XmlExport mXmlExport = null;
	private PubFun mPubFun = new PubFun();

	private String mManageName = "";

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private String[][] mShowDataList = null;

	private String tMakeDate = mPubFun.getCurrentDate();

	private String tMakeTime = mPubFun.getCurrentTime();

	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LISaleStatusStatisticBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LISaleStatusStatisticBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	private boolean getInputData(VData cInputData) {

		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));

			// 页面传入的数据 3个
			this.mManageCom = (String) cInputData.get(1);
			this.mStartDate = (String) cInputData.get(2);
			this.mEndDate = (String) cInputData.get(3);
			this.year = this.mEndDate.substring(0, this.mEndDate.indexOf("-"));
			this.month = this.mEndDate.substring(
					this.mEndDate.indexOf("-") + 1, this.mEndDate
							.lastIndexOf("-"));
			System.out.println("mManageCom:" + mManageCom + " mStartDate: "
					+ mStartDate + " mEndDate: " + mEndDate + " year: " + year
					+ " month:" + month);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}

		return true;

	}

	private boolean queryData() {
		try {
			// 查询数据
			if (!getDataList()) {
				return false;
			}
			if (mListTable == null || mListTable.equals("")) {
				CError.buildErr(this, "没有满足条件的值！");
				return false;
			}

			return true;

		} catch (Exception ex) {
			buildError("queryData", "LABL发生错误，准备数据时出错！");
			return false;
		}
	}

	private boolean getDataList() {
		
		double SumB = 0.0;
		double SumC = 0.0;
		double SumD = 0.0;
		double SumG = 0.0;
		double SumN = 0.0;
		double SumO = 0.0;
		double SumP = 0.0;
		double SumS = 0.0;
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("LISaleStatusStatistic.vts", "printer"); // 初始化xml文档
		mListTable.setName("EXEXUTE");

		String sql = "select a.showname, a.comcode from ldcom a, "
					+"(select substr(comcode,1,4) comcode from ldcom group by substr(comcode,1,4)) b "
					+" where a.comcode=b.comcode and a.comcode <> '86' and a.comcode <> '8600'";
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			CError tError = new CError();
			tError.moduleName = "";
			tError.functionName = "getDataList";
			tError.errorMessage = "没有查到信息!";
			this.mErrors.addOneError(tError);
			return false;
		}

		for (int i = 1; i <= tSSRS.MaxRow+1; i++) {
			String[] dateSet = new String[25];
			String comcode = "";
			
			if(i == tSSRS.MaxRow){
				comcode = "86320500";
				dateSet[0] = "苏州";
			}else if(i == tSSRS.MaxRow + 1){
				comcode = "86420500";
				dateSet[0] = "三峡（宜昌）";
			}else{
				comcode = tSSRS.GetText(i, 2);
				dateSet[0] = tSSRS.GetText(i, 1);
			}
			String str1 = getColB(comcode);
			dateSet[1] = "null".equals(str1)?"":str1;
			String str2 = getColC(comcode);
			dateSet[2] = "null".equals(str2)?"":str2;
			String str3 = getColD(comcode);
			dateSet[3] = "null".equals(str3)?"":str3;
			dateSet[4] = "";
			dateSet[5] = "";
			String str6 = getColG(comcode);
			dateSet[6] = "null".equals(str6)?"":str6;
			dateSet[7] = "";
			dateSet[8] = "";
			dateSet[9] = "";
			dateSet[10] = "";
			dateSet[11] = "";
			dateSet[12] = "";
			String str13 = getColN(comcode);
			String str14 = getColO(comcode);
			String str15 = getColP(comcode);
			dateSet[13] = "null".equals(str13)?"":str13;
			dateSet[14] = "null".equals(str14)?"":str14;
			dateSet[15] = "null".equals(str15)?"":str15;
			dateSet[16] = "";
			dateSet[17] = "";
			dateSet[18] = "null".equals(str14)?"":str14;
			dateSet[19] = "";
			dateSet[20] = "";
			dateSet[21] = "";
			dateSet[22] = "";
			dateSet[23] = "";
			dateSet[24] = "";
			if(dateSet[1] != ""){
				SumB = SumB + Double.parseDouble(dateSet[1]);
			}
			if(dateSet[2] != ""){
				SumC = SumC + Double.parseDouble(dateSet[2]);
			}
			if(dateSet[3] != ""){
				SumD = SumD + Double.parseDouble(dateSet[3]);
			}
			if(dateSet[6] != ""){
				SumG = SumG + Double.parseDouble(dateSet[6]);
			}
			if(dateSet[13] != ""){
				SumN = SumN + Double.parseDouble(dateSet[13]);
			}
			if(dateSet[14] != ""){
				SumO = SumO + Double.parseDouble(dateSet[14]);
			}
			if(dateSet[15] != ""){
				SumP = SumP + Double.parseDouble(dateSet[15]);		
			}
			SumS = SumO;
			mListTable.add(dateSet);
		}

		xmlexport.addListTable(mListTable, new String[5]); // 添加列表
		// 新建一个TextTag的实例
		TextTag tTextTag = new TextTag();
		tTextTag.add("CurrentDate", tMakeDate);
		tTextTag.add("StartDate", mStartDate);
		tTextTag.add("EndDate", mEndDate);
		tTextTag.add("Year", year);
		tTextTag.add("Month", month);
		tTextTag.add("SumB", PubFun.setPrecision1(SumB, "#0.00"));
		tTextTag.add("SumC", PubFun.setPrecision1(SumC, "#0.00"));
		tTextTag.add("SumD", PubFun.setPrecision1(SumD, "#0.00"));
		tTextTag.add("SumG", PubFun.setPrecision1(SumG, "#0.00"));
		tTextTag.add("SumN", PubFun.setPrecision1(SumN, "#0.00"));
		tTextTag.add("SumO", PubFun.setPrecision1(SumO, "#0.00"));
		tTextTag.add("SumP", PubFun.setPrecision1(SumP, "#0.00"));
		tTextTag.add("SumS", PubFun.setPrecision1(SumS, "#0.00"));

		if (tTextTag.size() > 0)
			xmlexport.addTextTag(tTextTag); // 添加动态文本标签

		// xmlexport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

	//应向建行客户支付的保险资金总额
	public String getColB(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+" paymode='4' and makedate between '" + mStartDate + "' and '" + mEndDate + "'"
				+" and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and substr(managecom,1,4)='" + comcode + "'";
		}else{
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+" paymode='4' and makedate between '" + mStartDate + "' and '" + mEndDate + "'"
				+" and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and managecom='" + comcode + "'";
		}
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		
		return tSSRS.GetText(1, 1);
	}
	
	//实际通过建行渠道代付的保险资金总额
	public String getColC(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+ " paymode='4' and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and substr(managecom,1,4)='" + comcode + "'"
				+" and confdate between '" + mStartDate + "' and '" + mEndDate + "'";
		}else{
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+ " paymode='4' and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and managecom='" + comcode + "'"
				+" and confdate between '" + mStartDate + "' and '" + mEndDate + "'";
		}
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
	//贵公司在该地区代付的保险资金总额
	public String getColD(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(aa.b) from ("
				+ " select sum(sumgetmoney)/10000 as b from ljaget "
				+ " where paymode='4' and confdate between '" +mStartDate+"' and '"+mEndDate+"'"  
				+ " and othernotype<>'BC' and othernotype<>'AC' and managecom<>'86000000'"
				+ " and substr(managecom,1,4) ='" + comcode + "'"
				+" ) aa ,ldcom bb where bb.comcode ='" + comcode + "'||'0000'";
		}else{
			sql = "select sum(aa.b) from ("
				+ " select sum(sumgetmoney)/10000 as b from ljaget "
				+ " where paymode='4' and confdate between '" +mStartDate+"' and '"+mEndDate+"'"  
				+ " and othernotype<>'BC' and othernotype<>'AC'"
				+ " and managecom ='" + comcode + "'"
				+" ) aa ,ldcom bb where bb.comcode ='" + comcode + "'";
		}
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
	//建行批量系统
	public String getColG(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+ " paymode='4' and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and substr(managecom,1,4)='" + comcode + "'"
				+" and confdate between '" + mStartDate + "' and '" + mEndDate + "'";
		}else{
			sql = "select sum(sumgetmoney)/10000 from ljaget where "
				+ " paymode='4' and bankcode like '03%' and othernotype<>'BC' and othernotype<>'AC' "
				+" and managecom='" + comcode + "'"
				+" and confdate between '" + mStartDate + "' and '" + mEndDate + "'";
		}
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
	//代收建行客户的保险资金总额
	public String getColN(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(paymoney)/10000 from ljtempfee a where "
				+" exists (select 1 from ljtempfeeclass where tempfeeno=a.tempfeeno and paymode='4' and bankcode like '03%' "
				+" and substr(managecom,1,4)='" + comcode + "'"
				+" and confmakedate between '" + mStartDate + "' and '" + mEndDate + "')";
		}else{
			sql = "select sum(paymoney)/10000 from ljtempfee a where "
				+" exists (select 1 from ljtempfeeclass where tempfeeno=a.tempfeeno and paymode='4' and bankcode like '03%' "
				+" and managecom='" + comcode + "'"
				+" and confmakedate between '" + mStartDate + "' and '" + mEndDate + "')";
		}
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
	//通过建行渠道代收的保险资金总额
	public String getColO(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select  sum(aa.b)/10000 as mm from ( select sum(paymoney) as b from ljtempfee where "
				  + " tempfeetype='17' and confmakedate between '" + mStartDate + "' and '" + mEndDate + "'" 
				  + " and managecom <> '86000000' and substr(managecom,1,4)='" + comcode + "'"
				  + " union "
				  + " select sum(paymoney) as b from ljtempfee a where exists ( "
				  + " select 1 from ljtempfeeclass where tempfeeno=a.tempfeeno and paymode='4' and bankcode like '03%' "
				  + " and confmakedate between '" + mStartDate + "' and '" + mEndDate + "'"
				  + " )  and managecom <> '86000000' and substr(managecom,1,4)='" + comcode + "' )  as aa";
		}else{
			sql = "select  sum(aa.b)/10000 as mm from ( select sum(paymoney) as b from ljtempfee where "
				  + " tempfeetype='17' and confmakedate between '" + mStartDate + "' and '" + mEndDate + "'" 
				  + " and managecom <> '86000000' and managecom='" + comcode + "'"
				  + " union "
				  + " select sum(paymoney) as b from ljtempfee a where exists ( "
				  + " select 1 from ljtempfeeclass where tempfeeno=a.tempfeeno and paymode='4' and bankcode like '03%' "
				  + " and confmakedate between '" + mStartDate + "' and '" + mEndDate + "'"
				  + " ) and managecom='" + comcode + "' ) as aa";
		}
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
	//贵公司在该地区代收的保险资金总额
	public String getColP(String comcode){
		String sql = "";
		if(null != comcode && comcode.length() == 4){
			sql = "select sum(mm.aa) from ( "
				+" select sum(paymoney)/10000 as aa from ljtempfee where "
				+" confmakedate between '" + mStartDate + "' and '" + mEndDate + "'"  
				+" and managecom<>'86000000' and substr(managecom,1,4) ='" + comcode + "' "
				+" ) mm , ldcom nn where nn.comcode='" + comcode + "'||'0000' ";
		}else{
			sql = "select sum(mm.aa) from ( "
				+" select sum(paymoney)/10000 as aa from ljtempfee where "
				+" confmakedate between '" + mStartDate + "' and '" + mEndDate + "'"  
				+" and managecom<>'86000000' and managecom ='" + comcode + "' "
				+" ) mm , ldcom nn where nn.comcode='" + comcode + "'";
		}
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow <= 0) {
			return null;
		}
		return tSSRS.GetText(1, 1);
	}
	
}
