package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;

public class PlPayMoneyMainPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	public PlPayMoneyMainPrintUI() {
	}

	public boolean submitData(TransferData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			PlPayMoneyMainPrintBL tPlPayMoneyMainPrintBL = new PlPayMoneyMainPrintBL();
			System.out.println("Start PlPayMoneyMainPrintUI Submit ...");
			if (!tPlPayMoneyMainPrintBL.submitData(cInputData, cOperate)) {
				if (tPlPayMoneyMainPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlPayMoneyMainPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlPayMoneyMainPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlPayMoneyMainPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			buildError("Submit", "执行错误，原因是："+e.toString());
			return false;
		}
	}
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlPayMoneyMainPrintUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}


}
