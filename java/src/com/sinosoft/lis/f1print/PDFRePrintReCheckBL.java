package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class PDFRePrintReCheckBL {
	
	private GlobalInput mGlobalInput = new GlobalInput();
    /**打印管理表信息*/
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /**结果变量*/
    private VData mResult = new VData();
	private String mflag = null;
	private MMap map = new MMap();
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
	
	public boolean submitData(VData cInputData, String flag) {
		System.out.println("---PDFPrintManagerBL---start---");
		// 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = flag;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
     // 保存数据
        if (this.dealData() == false)
        {
            return false;
        }
        
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");
        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start tPRnewManualDunBLS Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "PDFRePrintReCheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("---commitData---");
        return true;
	}

	/**
     * getInputData
     * 从前台获取打印所需要数据，用户信息和单证信息
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                 getObjectByObjectName("LOPRTManagerSchema", 0));

        if (mGlobalInput == null || mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
      return true;
    }
	
    /**
     * saveData
     * 获取服务类生成的打印管理表数据。
     * 判断打印管理表该条打印信息是否存在。已存在，则修改打印次数;不存在，则插入新数据
     * @return boolean
     */
     private boolean dealData() {
        //打印状态标记：将打印状态置为1！）
        LOPRTManagerDB tLOPRTManagerDB = new  LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
        tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
        tLOPRTManagerDB.setCode(mLOPRTManagerSchema.getCode());

        StringBuffer condition = new StringBuffer();
        condition.append(" 1=1 ");
        if(mLOPRTManagerSchema.getPrtSeq()!=null && !"".equals(mLOPRTManagerSchema.getPrtSeq()) && !"null".equals(mLOPRTManagerSchema.getPrtSeq())){
        	condition.append(" and PrtSeq = '"+mLOPRTManagerSchema.getPrtSeq()+"'");
        }
        if(mLOPRTManagerSchema.getOtherNo()!=null && !"null".equals(mLOPRTManagerSchema.getOtherNo()) && !"".equals(mLOPRTManagerSchema.getOtherNo())){
        	condition.append(" and otherno = '"+mLOPRTManagerSchema.getOtherNo()+"'");
        }
        if(mLOPRTManagerSchema.getCode()!=null && !"".equals(mLOPRTManagerSchema.getCode()) && !"null".equals(mLOPRTManagerSchema.getCode())){
        	condition.append(" and PrtSeq = '"+mLOPRTManagerSchema.getPrtSeq()+"'");
        }
        String SQL = "select * from LOPRTManager where " +condition.toString();
        System.out.println(SQL);
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        tLOPRTManagerSet = tLOPRTManagerDB.executeQuery(SQL);
        if(tLOPRTManagerSet.size()==1){
            mLOPRTManagerSchema.setPrtSeq(tLOPRTManagerSet.get(1).getPrtSeq());
            String ReprtSql="update LOPRTManager set DoneDate='"+PubFun.getCurrentDate()+"',"
            		+ "DoneTime='"+PubFun.getCurrentTime()+"',"
            				+ "StateFlag='0' where "+condition.toString();
            System.out.println(ReprtSql);
            map.put(ReprtSql, SysConst.UPDATE);
        }else{
        	mLOPRTManagerSchema.setPrtSeq(tLOPRTManagerSet.get(1).getPrtSeq());
            String ReprtSql="update LOPRTManager set DoneDate='"+PubFun.getCurrentDate()+"',"
            		+ "DoneTime='"+PubFun.getCurrentTime()+"',"
            				+ "StateFlag='0' where "+condition.toString();
            System.out.println(ReprtSql);
            map.put(ReprtSql, SysConst.UPDATE);
            buildError("saveData","打印管理表信息出错，请维护！");
        }
        return true;
     }
    
     private void prepareOutputData()
     {

         mInputData.clear();
         mInputData.add(map);
         mResult.clear();
     }
     
	private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PDFRePrintReCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult()
    {
        return mResult;
    }
}
