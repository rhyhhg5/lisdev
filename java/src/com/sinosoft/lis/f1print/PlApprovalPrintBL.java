package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author hdl
 * function :职场管理-》基本信息统计表
 * @version 1.0
 * @date 2012-06-20
 */


import com.sinosoft.utility.*;
public class PlApprovalPrintBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String placeno = "";
    private String todate = "";
    private String strMngCom = "";
    private String comLevel = "";
    private String useState = "";  //1-全部/2-在用
    

    public PlApprovalPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData) {
//    	placeno = (String) cInputData.get(0);
    	strMngCom = (String) cInputData.get(0);
//    	todate = (String) cInputData.get(2);
    	comLevel = (String)cInputData.get(1);
    	useState = (String)cInputData.get(2);
    	System.out.println("职场基本BL:strMngCom->"+strMngCom+";comLevel->"+comLevel+";useState->"+useState);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PlApprovalPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean getPrintData() 
    {
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("PlApprovalPrint.vts", "printer"); // 最好紧接着就初始化xml文档
		String strArr[] = new String[26];
		if (texttag.size() > 0) 
		{
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] {"序号","机构名称","机构级别","编码","地址","租赁起止期","租期","合计","团险销售部","个险销售部","银行保险部","健康管理部","运营管理部","小计","总经理室","人事行政部","计划财务部","公共区域","联合办公","其他","租金总成本","物业费总成本","装修总成本","违约成本","距到期日","是否在用"};
		{
			String sql = "select * from " +
					"(SELECT char(rownumber() over(order by Placeno)) as rolnum,a.Comname,(case a.comlevel when '1' then '省级分公司' when '2' then '地市级公司' when '3' then '四级机构-区' else '四级机构-县' end),a.Placeno,a.Address,char(a.Begindate) || '至' || char(a.Enddate),trim(to_char(to_date(a.Actualenddate) - to_date(a.Begindate) + 1)) || '天',"+
                    "(nvl(a.Sigarea, 0) + nvl(a.Grparea, 0) + nvl(a.Bankarea, 0) +nvl(a.Healtharea, 0) +nvl(a.Servicearea, 0) + nvl(a.Manageroffice, 0) +nvl(a.Departpersno, 0) + nvl(a.Planfinance, 0) +nvl(a.Publicarea, 0) + nvl(a.Jointarea, 0) +nvl(a.Otherarea, 0)),"+
                    "nvl(a.Grparea, 0),nvl(a.Sigarea, 0),nvl(a.Bankarea, 0),nvl(a.Healtharea, 0),nvl(a.Servicearea, 0),nvl(a.Manageroffice + a.Departpersno + a.Planfinance, 0),nvl(a.Manageroffice, 0),nvl(a.Departpersno, 0),nvl(a.Planfinance, 0),"+
                    "nvl(a.Publicarea, 0),nvl(a.Jointarea, 0),nvl(a.Otherarea, 0),"+
                    "(select nvl(sum(Money), 0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '01'),"+
                    "(select nvl(sum(Money), 0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '02'),"+
                    "(select nvl(sum(Money), 0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '03'),"+
                    "(case when (select 1 from LIPlaceChangeTrace where Placenorefer=a.placeno and Changetype='02' fetch first row only) is not null then 0 "+
                    " when  (select 1 from LIPlaceChangeTrace where Placenorefer=a.placeno and Changetype in('03','04') and Changestate='03' fetch first 1 rows only) is not null then (select Apenaltyfee from LIPlaceChangeTrace where Placenorefer=a.placeno and Changetype in('03','04') and Changestate='03' fetch first 1 rows only) "+
                    " else a.Penaltyfee end),"+
                    " case when (a.actualenddate - current date) >= 0  then to_char(DAYS(a.actualenddate) - DAYS(current date)) else '已到期' end,"+
                    " case when current date between a.Begindate and a.actualenddate then '在用' when current date<a.Begindate then '未启用' else '已停用' end "+
                    " FROM LIPlaceRentInfo a where 1 = 1";
//			if(!"".equals(placeno))
//			{
//				sql += " and placeno='"+placeno+"'";
//			}
			if(!"".equals(comLevel)){
				sql += " and a.comLevel = '"+comLevel+"'";
			}
			if(!"".equals(strMngCom))
			{
				sql += " and managecom like '"+strMngCom+"%'";
			}
//			if(!"".equals(todate))
//			{
//				sql += " and to_char(DAYS(a.actualenddate) - DAYS(current date))='"+todate+"'";
//			}
			if("2".equals(useState)){
				sql += " and current date between a.Begindate and a.actualenddate";
			}
			
			sql += " and exists(select 1 from LIPlaceChangeTrace where Placeno=a.placeno and Changestate='03')) as aa";
			
			System.out.println("sql:"+sql);
			ExeSQL main_exesql = new ExeSQL();
		    SSRS main_ssrs = main_exesql.execSQL(sql);
			ListTable tDetailListTable = new ListTable(); // 明细ListTable
			tDetailListTable.setName("INFO");
			for(int i=1;i<=main_ssrs.MaxRow;i++)
			{
				strArr = new String[26];
				for(int j=1;j<=main_ssrs.MaxCol;j++)
				{
					strArr[j-1]=main_ssrs.GetText(i,j);
				}
				tDetailListTable.add(strArr);
			}
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);

		return true;

    }
}
