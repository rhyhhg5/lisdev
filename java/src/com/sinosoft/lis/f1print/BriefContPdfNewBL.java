package com.sinosoft.lis.f1print;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Hashtable;

import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefContPdfNewBL implements PrintService {
    public BriefContPdfNewBL() {
    }

    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    /** 传入数据 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 合同信息 */
    private LCContSchema mLCContSchema;

    /** 被保人信息 */
    private LCInsuredSchema mLCInsuredScheam;

    /** 投保人信息 */
    private LCAppntSchema mLCAppntSchema;

    /** 保单打印数据 */
    private XMLDatasets mXMLDatasets;

    /** 处理Xml配置文件 */
    private File mFile = null;

    /** 路径信息 */
    private TransferData mTransferData;

    private String mTemplatePath;

    private String mOutXmlPath;

    private LCPolSet tLCPolSet;

    /** 受益人信息 */
    private LCBnfSet mLCBnfSet;

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private GlobalInput mGlobalInput = new GlobalInput();


    private VData mResult = new VData();

    /** 数据库递交Map */
    private MMap map = new MMap();

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 返回信息
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }


    /**
     * submitData
     *
     * @param vData VData
     * @param string String
     * @return boolean
     */
    public boolean submitData(VData vData, String string) {
        this.mInputData = vData;
        this.mOperate = string;
        /** 开始获取前台数据 */
        if (!getInputData()) {
            return false;
        }
        /** 教研数据 */
        if (!checkData()) {
            return false;
        }
        /** 生成节点 */
        if (!dealData()) {
            return false;
        }

        if (!dealPrintMag()) {
            return false;
        }

        /** 准备后台数据 */
        try {
            if (!perpareOutputData()) {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mInputData, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("开始getInputData......");
        if (this.mInputData == null) {
            buildError("getInputData", "传入数据为null！");
            return false;
        }
        if (this.mOperate == null) {
            buildError("getInputData", "传入操作符为null！");
            return false;
        }
//        mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
//                "LCContSchema", 0);
//        mTransferData = (TransferData) mInputData.getObjectByObjectName(
//                "TransferData", 0);
//        this.mTemplatePath = (String) mTransferData
//                             .getValueByName("TemplatePath");
//        this.mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");

        mGlobalInput.setSchema((GlobalInput) mInputData
                               .getObjectByObjectName("GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) mInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        String sqlurl =
                "select sysvarvalue from LDSYSVAR  where Sysvar='ServerRoot'"; //生成文件的存放路径
        mTemplatePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
//        mTemplatePath = "D:\\lis6.0\\ui\\f1print\\template\\";
        System.out.println("文件的存放路径   " + mTemplatePath); //调试用－－－－－
        if (mTemplatePath == null || mTemplatePath.equals("")) {
            buildError("getFileUrlName", "获取文件存放路径出错");
            return false;
        }
        mTemplatePath += "f1print/template/";

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo()) {
            System.out
                    .println("getInfo" + tLCContDB.mErrors.getErrContent());
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(this.mLCContSchema.getContNo());
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() <= 0) {
            buildError("dealInsured", "查询险种信息失败！");
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("开始checkData......");
        if (this.mLCContSchema == null) {
            buildError("checkData", "传入合同信息为null！");
            return false;
        }
        if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals("")) {
            buildError("checkData", "传入保单号码为null！");
            return false;
        }
//        LCContDB tLCContDB = new LCContDB();
//        tLCContDB.setContNo(this.mLCContSchema.getContNo());
//        if (!tLCContDB.getInfo()) {
//            buildError("checkData", "未查询到保单信息！");
//            return false;
//        } else {
//            this.mLCContSchema.setSchema(tLCContDB.getSchema());
//            mLCContSchema.setPrintCount(1);
//        }
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLCContSchema.getContNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() <= 0) {
            buildError("checkData", "未查询到被保人信息！");
            return false;
        }
        //        if (tLCInsuredSet.size() > 1) {
        //            buildError("checkData", "统一合同系的被保人信息不是唯一信息！");
        //            return false;
        //        }
        this.mLCInsuredScheam = tLCInsuredSet.get(1).getSchema();
        /** 一致性校验 */
        if (!this.mLCContSchema.getInsuredNo().equals(
                this.mLCInsuredScheam.getInsuredNo())) {
            buildError("checkData", "被保人数据与保单信息不一致！");
            return false;
        }
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(this.mLCContSchema.getContNo());
        if (!tLCAppntDB.getInfo()) {
            buildError("checkData", "查询投保人信息失败！");
            return false;
        }
        this.mLCAppntSchema = tLCAppntDB.getSchema();
        /** 一致性校验 */
        if (!this.mLCAppntSchema.getAppntNo().equals(
                this.mLCContSchema.getAppntNo())) {
            buildError("checkData", "头保人信息与保单信息存储不一致！");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** Xml对象 */
        mXMLDatasets = new XMLDatasets();
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        tXMLDataset.addDataObject(new XMLDataTag("JetFormType",
                                                 mLOPRTManagerSchema.getCode()));
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", printcode));
        tXMLDataset.addDataObject(new XMLDataTag("userIP",
                                                 mGlobalInput.ClientIP.
                                                 replaceAll("\\.", "_")));
        if ("batch".equals(mOperate)) {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "0"));
        } else {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "1"));
        }

        if (!dealCont(tXMLDataset)) {
            return false;
        }
        if (!dealInsured(tXMLDataset)) {
            return false;
        }
        if (!dealAppnt(tXMLDataset)) {
            return false;
        }
        if (!dealManageCom(tXMLDataset)) {
            return false;
        }
        if (!dealBnf(tXMLDataset)) {
            return false;
        }
        /** 针对同合同类型生成个性化的节点 */
        if (!dealDiffCont(tXMLDataset)) {
            return false;
        }
        /** 取消抵达国家 */
        //        if (!dealNation(tXMLDataset)) {
        //            return false;
        //        }
        /** 取消救援卡 */
        //        try {
        //            if (!genInsuredCard(tXMLDataset)) {
        //                return false;
        //            }
        //        } catch (Exception ex) {
        //            buildError("dealData", "出错！" + ex.getMessage());
        //            return false;
        //        }
        try {
            for (int i = 1; i <= tLCPolSet.size(); i++) {
                if (!getRiskInfo(tXMLDataset, tLCPolSet.get(i).getRiskCode())) {
                    return false;
                }
                if (!dealCashValue(tXMLDataset, tLCPolSet.get(i))) {
                    return false;
                }
            }
        } catch (Exception ex) {
            buildError("dealData", "生成xml数据失败" + ex.getMessage());
            return false;
        }

        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("END");
        tXMLDataList.buildColHead();
        tXMLDataset.addDataObject(tXMLDataList);

        mResult.add(mXMLDatasets); //xml数据流
        mResult.add(mGlobalInput); //全局变量
        return true;
    }

    /**
     * dealDiffCont
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealDiffCont(XMLDataset tXMLDataset) {
        if (this.mLCContSchema.getCardFlag().equals("5")) {
            tXMLDataset.addDataObject(new XMLDataTag("LCCont.Operator",
                    this.mLCContSchema.getOperator()));
            String tRelationToInsured = "null";
            if (mLCBnfSet != null && mLCBnfSet.size() > 0) {
                tRelationToInsured = mLCBnfSet.get(1).getRelationToInsured();
                tXMLDataset.addDataObject(new XMLDataTag(
                        "LCBnf.RelationToInsured", ChangeCodeBL.getCodeName(
                                "relation", tRelationToInsured, null)));
            } else {
                tXMLDataset.addDataObject(new XMLDataTag(
                        "LCBnf.RelationToInsured", ""));
            }

            tXMLDataset.addDataObject(new XMLDataTag("LCInsured.Birthday",
                    this.mLCInsuredScheam.getBirthday()));
        }
        return true;
    }

    /**
     * dealCashValue
     *
     * @return boolean
     */
    private boolean dealCashValue(XMLDataset tXMLDataset,
                                  LCPolSchema tLCPolSchema) {
        String tRiskCode = tLCPolSchema.getRiskCode();
        //得到现金价值的算法描述
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        //获取险种信息
        tLMCalModeDB.setRiskCode(tRiskCode);
        tLMCalModeDB.setType("X");
        LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();
        //解析得到的SQL语句
        String strSQL = "";
        //如果描述记录唯一时处理
        XMLDataList tXMLDataList = new XMLDataList();
        if (tLMCalModeSet.size() == 1) {
            strSQL = tLMCalModeSet.get(1).getCalSQL();
            tXMLDataList.setDataObjectID("CashValueInfo" + tRiskCode);
            tXMLDataList.addColHead("InsuredName");
            tXMLDataList.addColHead("RiskName");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("InsuredName", tLCPolSchema
                                     .getInsuredName());
            String tSql = "select riskname from lmriskapp where riskcode = '"
                          + tRiskCode + "'";
            tXMLDataList.setColValue("RiskName", (new ExeSQL())
                                     .getOneValue(tSql));
            tXMLDataList.insertRow(0);
            tXMLDataset.addDataObject(tXMLDataList);

            //现金价值标签
            tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("CashValue" + tRiskCode);
            tXMLDataList.addColHead("Year");
            tXMLDataList.addColHead("Date");
            tXMLDataList.addColHead("Cash");
            tXMLDataList.buildColHead();

            Calculator calculator = new Calculator();
            //设置基本的计算参数
            calculator.addBasicFactor("InsuredSex", tLCPolSchema
                                      .getInsuredSex());
            calculator.addBasicFactor("InsuredAppAge", String
                                      .valueOf(tLCPolSchema.getInsuredAppAge()));
            calculator.addBasicFactor("PayIntv", String.valueOf(tLCPolSchema
                    .getPayIntv()));
            calculator.addBasicFactor("PayEndYear", String.valueOf(tLCPolSchema
                    .getPayEndYear()));
            calculator.addBasicFactor("PayEndYearFlag", String
                                      .valueOf(tLCPolSchema.getPayEndYearFlag()));
            calculator.addBasicFactor("PayYears", String.valueOf(tLCPolSchema
                    .getPayYears()));
            calculator.addBasicFactor("InsuYear", String.valueOf(tLCPolSchema
                    .getInsuYear()));
            calculator.addBasicFactor("Prem", String.valueOf(tLCPolSchema
                    .getPrem()));
            calculator.addBasicFactor("Amnt", String.valueOf(tLCPolSchema
                    .getAmnt()));
            calculator.addBasicFactor("FloatRate", String.valueOf(tLCPolSchema
                    .getFloatRate()));
            //add by yt 2004-3-10
            calculator.addBasicFactor("InsuYearFlag", String
                                      .valueOf(tLCPolSchema.getInsuYearFlag()));
            calculator.addBasicFactor("GetYear", String.valueOf(tLCPolSchema
                    .getGetYear()));
            calculator.addBasicFactor("GetYearFlag", String
                                      .valueOf(tLCPolSchema.getGetYearFlag()));
            calculator.addBasicFactor("CValiDate", String.valueOf(tLCPolSchema
                    .getCValiDate()));
            calculator.addBasicFactor("ContNo", String.valueOf(tLCPolSchema
                    .getContNo()));
            //Added By Liuyp 2009-7-21
            calculator.addBasicFactor("Mult", String.valueOf(tLCPolSchema
                    .getMult()));
            calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
            strSQL = calculator.getCalSQL();

            //            tXMLDataList.setColValue("Year", "0");
            //            tXMLDataList.setColValue("Date", tLCPolSchema.getCValiDate());
            //            tXMLDataList.insertRow(0);
            System.out.println(strSQL);
            ExeSQL tExeSQL = new ExeSQL();
            SSRS ssrs = tExeSQL.execSQL(strSQL);
            if (tExeSQL.mErrors.needDealError()) {
                String str = " 查询失败现金价值失败原因是："
                             + tExeSQL.mErrors.getFirstError();
                buildError("dealCashValue", str);
                System.out
                        .println("在程序LCContPrintPdfBL.dealCashValue() - 326 : "
                                 + str);
                return false;
            }
            if (ssrs != null && ssrs.getMaxRow() > 0) {
                int i = 0;
                for (i = 1; i <= ssrs.getMaxRow(); i++) {
                    //往xml对象中添加现金价值信息
                    tXMLDataList.setColValue("Year", ssrs.GetText(i, 1));
                    tXMLDataList.setColValue("Date", ssrs.GetText(i, 2));
                    tXMLDataList.setColValue("Cash", CommonBL
                                             .bigDoubleToCommonString(Double.
                            parseDouble(ssrs
                                        .GetText(i, 3)), "0.00"));
                    tXMLDataList.insertRow(0);
                }
                //                for (; i <= 10; i++) {
                //                    //往xml对象中添加现金价值信息
                //                    tXMLDataList.setColValue("Year", "-");
                //                    tXMLDataList.setColValue("Date", "-");
                //                    tXMLDataList.setColValue("Cash", "-");
                //                    tXMLDataList.insertRow(0);
                //                }
            } else {
                String str = "打印现金价值失败!";
                buildError("dealCashValue", str);
                System.out
                        .println("在程序LCContPrintPdfBL.dealCashValue() - 333 : "
                                 + str);
                return false;
            }
            tXMLDataset.addDataObject(tXMLDataList);
        }

        return true;
    }

    /**
     * dealNation
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealNation(XMLDataset tXMLDataset) {
        String nationName = "";
        String strSql = "select EnglishName from lcnation where contno='"
                        + this.mLCContSchema.getContNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(strSql);
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            nationName += ssrs.GetText(i, 1);
            if (i != ssrs.getMaxRow()) {
                nationName += ",";
            }
        }
        tXMLDataset.addDataObject(new XMLDataTag("Nation", nationName));
        return true;
    }

    /**
     * getRiskInfo
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getRiskInfo(XMLDataset tXMLDataset, String tRiskCode) throws
            Exception {
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "RiskInfo" + StrTool.cTrim(tRiskCode)
                        + ".xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            //buildError("genInsuredCard", "XML配置文件不存在！");
            return true;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_CONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            tXMLDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     *
     * @param tXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset tXmlDataset) throws Exception {
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "InsuredCard.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_CONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            tXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * dealBnf
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealBnf(XMLDataset tXMLDataset) {
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setContNo(this.mLCContSchema.getContNo());
        tLCBnfDB.setInsuredNo(this.mLCContSchema.getInsuredNo());
        tLCBnfDB.setInsuredNo("1"); //身故受益人
        mLCBnfSet = tLCBnfDB.query();
        if (mLCBnfSet != null && mLCBnfSet.size() > 0) {
            /** 受益人姓名 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Name", mLCBnfSet
                    .get(1).getName()));
            /** 受益人性别 */
            String sex = mLCBnfSet.get(1).getSex().equals("0") ? "男" : "女";
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Sex", sex));
            /** 受益人证件号码 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.IDNo", mLCBnfSet
                    .get(1).getIDNo()));

        } else {
            /** 受益人姓名 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Name", ""));
            /** 受益人性别 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Sex", ""));
            /** 受益人证件号码 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.IDNo", ""));
        }

        /**
         * 处理多受益人信息
         */
        System.out.println("开始处理受益人信息...");
        String strSql = "select distinct Name, "
                        +
                        " case sex when '1' then '女' when '0' then '男' end as sex, "
                        +
                        " CodeName('idtype', idtype) as idtype, birthday, idno, bnfno, "
                        + " bnfgrade, bnflot, "
                        +
                        " CodeName('relation', relationtoinsured) as relationtoinsured "
                        + " from lcbnf " + " where contno = '"
                        + this.mLCContSchema.getContNo() + "' "
                        + " order by bnfgrade, bnfno " + " with ur ";

        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCBnf");
        tXMLDataList.addColHead("Name");
        tXMLDataList.addColHead("Sex");
        tXMLDataList.addColHead("IdType");
        tXMLDataList.addColHead("Birthday");
        tXMLDataList.addColHead("IdNo");
        tXMLDataList.addColHead("BnfNo");
        tXMLDataList.addColHead("BnfGrade");
        tXMLDataList.addColHead("BnFlot");
        tXMLDataList.addColHead("RelationToInsured");
        tXMLDataList.buildColHead();

        System.out.println(strSql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(strSql);
        if (tExeSQL.mErrors.needDealError()) {
            String str = " 查询受益人信息失败原因是：" + tExeSQL.mErrors.getFirstError();
            buildError("dealBnf", str);
            System.out.println("Error : LCContPrintPdfBL -> dealBnf()");
            return false;
        }
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                tXMLDataList.setColValue("Name", ssrs.GetText(i, 1));
                tXMLDataList.setColValue("Sex", ssrs.GetText(i, 2));
                tXMLDataList.setColValue("IdType", ssrs.GetText(i, 3));
                tXMLDataList.setColValue("Birthday", ssrs.GetText(i, 4));
                tXMLDataList.setColValue("IdNo", ssrs.GetText(i, 5));
                tXMLDataList.setColValue("BnfNo", ssrs.GetText(i, 6));
                tXMLDataList.setColValue("BnfGrade", ssrs.GetText(i, 7));
                tXMLDataList.setColValue("BnFlot", ssrs.GetText(i, 8));
                tXMLDataList.setColValue("RelationToInsured", ssrs
                                         .GetText(i, 9));
                tXMLDataList.insertRow(0);
            }
        }
        tXMLDataset.addDataObject(tXMLDataList);
        System.out.println("受益人信息处理完成...");

        return true;
    }

    /**
     * dealManageCom
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealManageCom(XMLDataset tXMLDataset) {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(this.mLCContSchema.getManageCom());
        if (!tLDComDB.getInfo()) {
            buildError("dealManageCom", "没有查旬到管理机构代码！");
            return false;
        }
        /** 管理机构名称 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.SignCom", tLDComDB
                                                 .getLetterServiceName()));
        /** 服务电话 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.ServicePhone", tLDComDB
                                                 .getPhone()));
        /** 公司地址 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.ServicePostAddress",
                                                 tLDComDB.getServicePostAddress()));
        return true;
    }

    /**
     * dealAppnt
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealAppnt(XMLDataset tXMLDataset) {
        /** 特别约定 */
        tXMLDataset.addDataObject(new XMLDataTag("LCAppnt.EnglishName",
                                                 this.mLCAppntSchema.
                                                 getEnglishName()));

        return true;
    }

    /**
     * dealInsured
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealInsured(XMLDataset tXMLDataset) {
        /** 被保人拼音 */
        tXMLDataset.addDataObject(new XMLDataTag("LCInsured.EnglishName",
                                                 this.mLCInsuredScheam.
                                                 getEnglishName()));
        /** 被保人投保年龄 */
        tXMLDataset.addDataObject(new XMLDataTag("LCPol.InsuredAge", tLCPolSet
                                                 .get(1).getInsuredAppAge()));
        tXMLDataset.addDataObject(new XMLDataTag("LCInsured.IDNo",
                                                 this.mLCInsuredScheam.getIDNo()));
        tXMLDataset.addDataObject(new XMLDataTag("LCInsured.RelationToAppnt",
                                                 getRelationToAppntName(
                mLCInsuredScheam.getRelationToAppnt())));

        return true;
    }

    /**
     * dealCont
     *
     * @return boolean
     * @param tXMLDataset XMLDataset
     */
    private boolean dealCont(XMLDataset tXMLDataset) {
        /** 保单号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.ContNo",
                                                 this.mLCContSchema.getContNo()));
        /** 印刷号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.PrtNo",
                                                 this.mLCContSchema.getPrtNo()));
        /** 投保人号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AppntNo",
                                                 this.mLCContSchema.getAppntNo()));
        /** 投保人 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.GrpName",
                                                 this.mLCContSchema.
                                                 getAppntName()));
        /** 签单日期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SignDate", PubFun
                                                 .getCurrentDate()));
        /** 生效时期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.CValiDate",
                                                 this.mLCContSchema.
                                                 getCValiDate()));
        /** 总保费 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SumPrem", String
                                                 .valueOf(this.mLCContSchema.
                getPrem())));
        /** 特别约定 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.Remark", StrTool
                                                 .cTrim(this.mLCContSchema.
                getRemark())));
        /** 失效时期 */
        String InValiDate = (new ExeSQL()).getOneValue("select date('"
                + mLCContSchema.getCInValiDate() + "') - 1 day from dual");
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.CInValiDate",
                                                 InValiDate));
        /** 保险期间 */
        String insuYearFlag = tLCPolSet.get(1).getInsuYearFlag();
        if("A".equals(insuYearFlag))
            insuYearFlag = "岁";
        else if("Y".equals(insuYearFlag))
            insuYearFlag = "年";
        else if("M".equals(insuYearFlag))
            insuYearFlag = "月";
        else if("D".equals(insuYearFlag))
            insuYearFlag = "日";
        tXMLDataset.addDataObject(new XMLDataTag("InsuYear",
            tLCPolSet.get(1).getInsuYear() + insuYearFlag));
        /** 被保险人姓名 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.InsuredName",
                                                 this.mLCContSchema.
                                                 getInsuredName()));

        /** 被保人性别 */
        String sex = this.mLCContSchema.getInsuredSex().equals("0") ? "男" : "女";
        tXMLDataset.addDataObject(new XMLDataTag("LCcont.InsuredSex", sex));
        /** 管理机构 */
        tXMLDataset.addDataObject(new XMLDataTag("XI_ManageCom",
                                                 mLCContSchema.getManageCom()));
        /** 签单日期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SignDate", PubFun
                                                 .getCurrentDate()));
        /** 单次/多次 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.DegreeType",
                                                 this.mLCContSchema.
                                                 getDegreeType()));
        /** 单次/多次 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AppntName",
                                                 this.mLCContSchema.
                                                 getAppntName()));
        /** 单次/多次 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AppntIDNo",
                                                 this.mLCContSchema.
                                                 getAppntIDNo()));
        ExeSQL tExeSQL = new ExeSQL();
        String res = tExeSQL.getOneValue("select getUniteCode("+this.mLCContSchema.
                getAgentCode()+") from dual");
        
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AgentCode",res));
                                                 
        /** 银行编码 */
        tXMLDataset.addDataObject(new XMLDataTag("BankCode", this.mLCContSchema
                                                 .getBankCode()));
        /** 复核员 */
        tXMLDataset.addDataObject(new XMLDataTag("ApproveCode",
                                                 this.mLCContSchema.
                                                 getApproveCode()));
        /** 出单员 */
        tXMLDataset.addDataObject(new XMLDataTag("SignCode", this.mLCContSchema
                                                 .getOperator()));

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo()) {
            buildError("dealCont", "没有查询到正确的代理人信息！");
            return false;
        }
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AgentName", tLAAgentDB
                                                 .getName()));

        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AgentCom",
                                                 mLCContSchema.getAgentCom()));
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(mLCContSchema.getAgentCom());
        tLAComDB.getInfo();
        //生成五福同享档次的节点
        String strSQL = " select calfactortype,riskcode from lcriskdutywrap where contno= '"+this.mLCContSchema.getContNo()+"'" 
                +" and riskwrapcode = 'WR0008'  ";
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = mExeSQL.execSQL(strSQL);
        if (tSSRS != null && tSSRS.getMaxRow() > 0){
        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                if(tSSRS.GetText(i, 2).equals("1601")){
                	tXMLDataset.addDataObject(new XMLDataTag("Mult1", tSSRS.GetText(i, 1)));
                }
                if(tSSRS.GetText(i, 2).equals("1607")){
                	tXMLDataset.addDataObject(new XMLDataTag("Mult2", tSSRS.GetText(i, 1)));
                }
                if(tSSRS.GetText(i, 2).equals("5503")){
                	tXMLDataset.addDataObject(new XMLDataTag("Mult3", tSSRS.GetText(i, 1)));
                }
                if(tSSRS.GetText(i, 2).equals("5601")){
                	tXMLDataset.addDataObject(new XMLDataTag("Mult4", tSSRS.GetText(i, 1)));
                }
            }
        }
        
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AgentComName",
                                                 tLAComDB.getName()));
        //保单补打添加节点
        tXMLDataset.addDataObject(new XMLDataTag("LostTimes",
                                                 this.mLCContSchema.
                                                 getLostTimes()));
        //
        tXMLDataset.addDataObject(new XMLDataTag("BQRePrintDate",
                                                 com.sinosoft.lis.bq.CommonBL.
                                                 decodeDate(PubFun.
                getCurrentDate())
                                  ));

        return true;
    }

    /**
     * perpareOutputData
     *
     * @return boolean
     * @throws Exception
     */
    private boolean perpareOutputData() throws Exception {
//        String XmlFile = null;
//        if (this.mLCContSchema.getCardFlag().equals("2")) {
//            XmlFile = mOutXmlPath + "/printdata/data/brief/J05-"
//                      + this.mLCContSchema.getContNo() + ".xml";
//        }
//        if (this.mLCContSchema.getCardFlag().equals("5")) {
//            XmlFile = mOutXmlPath + "/printdata/data/brief/J06-"
//                      + this.mLCContSchema.getContNo() + ".xml";
//        }
//        if (this.mLCContSchema.getCardFlag().equals("6")) {
//            XmlFile = mOutXmlPath + "/printdata/data/brief/J006-"
//                      + this.mLCContSchema.getContNo() + ".xml";
//        }
//
//        //        String XmlFile_Card = mOutXmlPath + "/printdata/data/brief/J04-" +
//        //                              this.mLCContSchema.getContNo() + ".xml";
//        creatXmlFile(XmlFile);
//        //        creatXmlFile(XmlFile_Card);
//        if (this.mOperate.equals("REPRINT")) {
//            if (!deleteFile(XmlFile)) {
//                return false;
//            }
//            //            if (!deleteFile(XmlFile_Card)) {
//            //                return false;
//            //            }
//        }
        map.put(this.mLCContSchema, "UPDATE");
        this.mInputData.add(map);
        return true;
    }

    /**
     * deleteFile
     *
     * @param XmlFile String
     * @return boolean
     */
    private boolean deleteFile(String XmlFile) {
        String file = StrTool.replaceEx(XmlFile, ".xml", ".pdf");
        file = StrTool.replaceEx(file, "brief", "briefpdf");
        try {
            File tFile = new File(file);
            if (tFile.exists()) {
                tFile.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("deleteFile", "重打保单失败，原因是，ｐｄｆ文件存在但是删除原ｐｄｆ文件失败！");
            return false;
        }
        return true;
    }

    /**
     * creatXmlFile
     *
     * @param XmlFile String
     * @throws Exception
     * @return boolean
     */
    private boolean creatXmlFile(String XmlFile) {
        InputStream ins = mXMLDatasets.getInputStream();

        try {
            FileOutputStream fos = new FileOutputStream(XmlFile);
            int n = 0;
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1) {
                fos.write(c, 0, n);
            }
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("creatXmlFile", "生成Xml文件失败！");
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContPrintPdfBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) {
        try {

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getRelationToAppntName(String tRelationToAppnt) {
        String RelationToAppntName = new ExeSQL()
                                     .getOneValue(
                                             "select codename from ldcode where codetype='relation' and code='"
                                             + tRelationToAppnt + "'");
        return RelationToAppntName;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("02");
        mLOPRTManagerSchema.setCode(mLOPRTManagerSchema.getCode());
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);
//        MMap tMap = new MMap();
//        tMap.put(mLOPRTManagerSchema, "INSERT");
//        VData tVData = new VData();
//        tVData.add(tMap);
//        PubSubmit tPubSubmit = new PubSubmit();
//        if (tPubSubmit.submitData(tVData, "") == false) {
//            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
//            return false;
//        }
        return true;
    }


}
