package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class bqWorkFinLisBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private ListTable tlistTable;
  private ListTable klistTable;
  private VData mResult = new VData();

  String StartDate = "";
  String EndDate = "";
  String organcode="";
  String managecom="";
  String contType = "";
  
    public bqWorkFinLisBL() {
    }
    /**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
       if (!cOperate.equals("PRINT")) {
           buildError("submitData", "不支持的操作字符串");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }

       mResult.clear();

       // 准备所有要打印的数据
       if (!getPrintData()) {
           return false;
       }

       return true;
   }

   private void buildError(String szFunc, String szErrMsg) {
       CError cError = new CError();

       cError.moduleName = "LCContF1PBL";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;
       this.mErrors.addOneError(cError);
   }

   /**
    * 从输入数据中得到所有对象
    * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
       //全局变量
       mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 0));

       TransferData tTransferData = (TransferData) cInputData.
                                    getObjectByObjectName("TransferData", 0);
       StartDate = (String) tTransferData.getValueByName("StartDate");
       EndDate = (String) tTransferData.getValueByName("EndDate");
       organcode = (String) tTransferData.getValueByName("organcode");
       contType = (String) tTransferData.getValueByName("contType");
       
       System.out.println(StartDate);
       System.out.println(EndDate);
       System.out.println(organcode);
       System.out.println("contType------"+contType);

       return true;
   }

   public VData getResult() {
       return mResult;
   }

   public CErrors getErrors() {
       return mErrors;
   }


   private boolean getPrintData() {

       TextTag texttag = new TextTag(); //新建一个TextTag的实例
       XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
       xmlexport.createDocument("bqWorkFinLis.vts", "printer"); //最好紧接着就初始化xml文档

       SSRS tSSRS = new SSRS();
       SSRS lSSRS = new SSRS();
       SSRS kSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();

       texttag.add("StartDate", StartDate);
       texttag.add("EndDate", EndDate);
       
       String contTypeName = "全部";
       String subSQL = "";
       if("1".equals(contType)){
    	   subSQL = " and otherNoType = '1'";
    	   contTypeName = "个人";
       }else if("2".equals(contType)){
    	   subSQL = " and otherNoType = '2'";
    	   contTypeName = "团体";
       }
       texttag.add("ContType", contTypeName);

       if(organcode.length()>4)
       {
        managecom =" and ComCode ='"+organcode+"'";
       }
       else if(organcode.length() == 4){
        managecom =" and ComCode like '"+organcode+"%' and (comgrade in ('02','03') or length(trim(ComCode))=6 ) ";
       }
       else
       {
//         managecom  =" and ComCode  like '"+organcode+"%' and comgrade in ('01','02') ";
    	   //modify by lzy 20150813总公司打印报表时查询所有二级机构和三级机构
    	   managecom  =" and ComCode  like '"+organcode+"%' and (comgrade in ('01','02','03') or length(trim(ComCode))=6 ) order by comcode";
       }

//       String sql1 = "select a.managecom from lpedorapp a, ldcom b "+
//                     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
//                     managecom +
//                     "group by a.managecom"+
//                     " union "+
//                     "select a.managecom from lobedorapp a, ldcom b "+
//                     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
//                     managecom+
//                     "group by a.managecom";
       //之前的统计方式存在漏统计
       String sql1 ="select ComCode from ldcom where 1=1 and Sign='1' "
    	   			+ managecom ;

       tSSRS = tExeSQL.execSQL(sql1);
       System.out.println(sql1);
       String strA = "select a.managecom from lpedorapp a, ldcom b "+
			     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
			     " and a.managecom like '"+ organcode +"%'" +
			     " group by a.managecom"+
			     " union "+
			     "select a.managecom from lobedorapp a, ldcom b "+
			     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
			     " and a.managecom like '"+ organcode +"%'" +
			     " group by a.managecom";
       String noData = tExeSQL.getOneValue(strA);
       System.out.println("row=" + noData);

//完成情况

       tlistTable = new ListTable();
       tlistTable.setName("FIN");

       if(null==noData || ""==noData)
       {
            String[] strArr = new String[16];
           strArr[0] = "0";
           System.out.print(strArr[0]);
           strArr[1] = "0";
           strArr[2] = "0";
           strArr[3] = "0";
           strArr[4] = "0";
           strArr[5] = "0";
           strArr[6] = "0";
           System.out.print(strArr[6]);
           strArr[7] = "0";
           strArr[8] = "0";
           strArr[9] = "0";
           strArr[10] = "0";
           strArr[11] = "0";
           strArr[12] = "0";
           strArr[13] = "0";
           strArr[14] = "0";
           strArr[15] = "0";
           tlistTable.add(strArr);
       }


       for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
    	   String manageStr=" and managecom like '"+tSSRS.GetText(i, 1)+"%'";
    	   if(tSSRS.GetText(i, 1).trim().length()==6){
    		   manageStr=" and managecom = '"+tSSRS.GetText(i, 1)+"'";
    	   }
    	   //20150813 在报表中添加四个字段：二级机构代码、三级机构、三级机构代码、总工单数
           String[] strArr = new String[16];
           String comCode=tSSRS.GetText(i, 1).trim();
           String comCode1=comCode;//总公司/二级机构代码
           String comCode2="";//三级机构代码
           String comName1=" ";//总公司/二级机构名称
           String comName2=" ";//三级机构名称
           if(comCode.length()>4){
        	   comCode1=comCode.substring(0, 4);
           }
           String sql2 ="select name from ldcom where comcode='" + comCode1 + "'";
           comName1= tExeSQL.getOneValue(sql2);
           strArr[0]=comName1;
           strArr[1]=comCode1;
           comCode2=comCode;
           sql2 ="select name from ldcom where comcode='" + comCode + "'";
           comName2= tExeSQL.getOneValue(sql2);
           strArr[2]=comName2;
           strArr[3]=comCode2;
           
           String all= "select count(edoracceptno) m from lpedorapp "+
			           "where 1=1 "+manageStr+
			           subSQL+
			           " and makedate between '"+StartDate+"' and '"+EndDate+"'";
           strArr[4] = tExeSQL.getOneValue(all);

           String sql3 =
                        "select sum(m) from ("+
                        "select count(edoracceptno) m from lpedorapp "+
//                        "where managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                        "where 1=1 "+manageStr+
                        subSQL+
                        " and makedate between '"+StartDate+"' and '"+EndDate+"'"+
                        " union all "+
                        "select count(edoracceptno) m from lobedorapp "+
//                        "where managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                        "where 1=1 "+manageStr+
                        subSQL+
                        " and makedate between '"+StartDate+"' and '"+EndDate+"') k";

           lSSRS = tExeSQL.execSQL(sql3);
           strArr[5] = lSSRS.GetText(1, 1);
           
           String allNum=lSSRS.GetText(1, 1);
           System.out.println("allNum"+allNum);
           //若分公司没有保全记录则不再统计
           if(null==allNum || ""==allNum || "0".equals(allNum)){
         	  continue;
           }
           
           
           String sql4 =
                       "select count(edoracceptno) from lpedorapp  where edorstate='0'"+
                       "and edoracceptno not in (select workno from lgwork where workno=edoracceptno and statusno='8')"+
//                       " and managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       subSQL+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          lSSRS = tExeSQL.execSQL(sql4);
          strArr[6] = lSSRS.GetText(1, 1);
          String FinNum=lSSRS.GetText(1, 1);
          String sql5 =
                       "select count(edoracceptno) from lpedorapp  where edorstate!='0'"+
                       "and edoracceptno not in (select workno from lgwork where workno=edoracceptno and statusno='8')"+
//                       " and managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       subSQL+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          lSSRS = tExeSQL.execSQL(sql5);
          strArr[7] = lSSRS.GetText(1, 1);
          String unFinNum=lSSRS.GetText(1, 1);
          double t1= Double.parseDouble(FinNum)/Double.parseDouble(allNum);
          String rate1="";
          if(allNum==null||allNum.equals("")||allNum=="0")
        {
          rate1="0";
        }else{
             rate1=String.valueOf(PubFun.setPrecision(t1,"0.00"));
        }

          System.out.println("rate="+rate1);
          strArr[8] = rate1;
          String sql6 =
                       "select sum(m) from ( "+
                       "select count(edoracceptno) m from lpedorapp   where "+
                       " edoracceptno  in (select workno from lgwork where workno=edoracceptno and statusno='8' and operator<>'group')"+
//                       " and managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       subSQL+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'"+
                       " union all "+
                       "select count(edoracceptno) m from lobedorapp   where "+
                       " edoracceptno  in (select workno from lgwork where workno=edoracceptno and statusno='8' and operator<>'group')"+
//                       " and managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       subSQL+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"') k";

          lSSRS = tExeSQL.execSQL(sql6);
          String backNum=lSSRS.GetText(1, 1);
          strArr[9] = backNum;
          double t2= Double.parseDouble(backNum)/Double.parseDouble(allNum);
          String rate2="";
        if(allNum==null||allNum.equals("")||allNum=="0")
      {
        rate2="0";
      }else{
    	  	//modify by lzy #2476保留5位小数的精度
            rate2=String.valueOf(PubFun.setPrecision(t2,"0.00000"));
      }


          strArr[10] = rate2;
      /*       String sql10=
            "select sum(m) from ( "+
            "select count(edoracceptno) m  from lpedorapp   where "+
           " edoracceptno  in (select workno from lgwork where workno=edoracceptno and statusno='8')"+
           " and reasoncode in ('201','101','102') "+
           " and managecom='"+tSSRS.GetText(i, 1)+"'"+
           " and makedate between '"+StartDate+"' and '"+EndDate+"'"+
           "union all "+
           "select count(edoracceptno) m from lobedorapp   where "+
           " edoracceptno  in (select workno from lgwork where workno=edoracceptno and statusno='8')"+
           " and reasoncode in ('201','101','102') "+
           " and managecom='"+tSSRS.GetText(i, 1)+"'"+
           " and makedate between '"+StartDate+"' and '"+EndDate+"') k";
         String temp1=tExeSQL.getOneValue(sql10);
         double t3= Double.parseDouble(temp1)/Double.parseDouble(allNum);
         String rate3=String.valueOf(PubFun.setPrecision(t3,"0.00"));
       */
         strArr[11]="0";
          String sql9=
                        "select AVG(double(days(modifydate)-days(makedate))) from lpedorapp "+
//                        "where managecom like '"+tSSRS.GetText(i, 1)+"%'"+
                        "where 1=1 "+manageStr+
                        subSQL+
                        " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          String x1=tExeSQL.getOneValue(sql9);
          if(null ==x1 || "".equals(x1)){
        	  x1="0";
          }
          String x2=String.valueOf(PubFun.setPrecision(Double.parseDouble(x1),"0.00000"));

          strArr[12]=x2;
          
          //20151228 保全失效新的统计口径 1、只统计已经结案的工单	2、不包括收费时效
          String sql10=
              "select AVG(double(days((select min(makedate) from lgtracenodeop where workno=a.edoracceptno  " +
              " and operatortype='7' and remark like '%保全确认成功%' )) - days(makedate))) from lpedorapp a "+
              " where 1=1 "+manageStr+
              subSQL+
              " and edorstate='0'" +
              " and makedate between '"+StartDate+"' and '"+EndDate+"'";
		String NewAvg=tExeSQL.getOneValue(sql10);
		if(null ==NewAvg || "".equals(NewAvg)){
			NewAvg="0";
		}
		NewAvg=String.valueOf(PubFun.setPrecision(Double.parseDouble(NewAvg),"0.00000"));
		
		strArr[13]=NewAvg;
		
		//2018-3-26新增两个字段“操作人员原因引起的保全撤销件数”、“保全差错率（监管报送口径）”
		 String sql11 =
             "select count(edoracceptno) m from lobedorapp   where "+
             " edoracceptno  in (select workno from lgwork where workno=edoracceptno and statusno='8' and operator<>'group')"+
             " and reasoncode in ('101','102')"+
             manageStr+
             subSQL+
             " and makedate between '"+StartDate+"' and '"+EndDate+"'";
		 lSSRS = tExeSQL.execSQL(sql11);
         String cancelNum=lSSRS.GetText(1, 1);
         strArr[14] = cancelNum;
         double t3= Double.parseDouble(cancelNum)/Double.parseDouble(allNum);
         String rate3="";
         if(allNum==null||allNum.equals("")||allNum=="0"){
        	 rate2="0";
         }else{
           rate3=String.valueOf(PubFun.setPrecision(t3,"0.00000"));
         }
         strArr[15] = rate3;

           tlistTable.add(strArr);
       }

       String[] strArrHead = new String[16];
       strArrHead[0] = "机构";
       strArrHead[1] = "机构代码";
       strArrHead[2] = "三级机构名称";
       strArrHead[3] = "三级机构代码";
       strArrHead[4] = "总共单数";
       strArrHead[5] = "受理件数";
       strArrHead[6] = "处理完成件数";
       strArrHead[7] = "未完成件数";
       strArrHead[8] = "完成率";
       strArrHead[9] = "回退件数";
       strArrHead[10] = "保全撤销率";
       strArrHead[11] = "变更回退件数";
       strArrHead[12] = "平均时效";
       strArrHead[13] = "平均时效（新）";
       strArrHead[14] = "操作人员原因引起的保全撤销件数";
       strArrHead[15] = "保全差错率（监管报送口径）";
       

       xmlexport.addListTable(tlistTable, strArrHead);

//受理渠道

//       String sql0 = "select a.managecom from lpedorapp a ,ldcom b "+
//                     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
//                      managecom+
//                     "group by a.managecom";
       String sql0 = "select ComCode from ldcom where 1=1 and Sign='1' "
    	   			+ managecom ;

       kSSRS = tExeSQL.execSQL(sql0);
       
       String StrB = "select a.managecom from lpedorapp a ,ldcom b "+
				     " where  a.makedate between '"+StartDate+"' and '"+EndDate+"' and a.managecom = b.comcode "+
				     " and  a.managecom like '"+ organcode +"%'" +
				     " group by a.managecom";
       String noDataB=tExeSQL.getOneValue(StrB);
       System.out.println(StrB);

       System.out.println("row=" + noDataB);



       klistTable = new ListTable();
       klistTable.setName("ACCEPT");



       if(null==noDataB || ""==noDataB )
       {
           String[] strArr1 = new String[15];
           strArr1[0]="0";
           System.out.print(strArr1[0]);
           strArr1[1]="0";
           strArr1[2]="0";
           strArr1[3]="0";
           strArr1[4]="0";
           strArr1[5]="0";
           strArr1[6]="0";
           strArr1[7]="0";
           strArr1[8]="0";
           strArr1[9]="0";
           System.out.print(strArr1[6]);
           klistTable.add(strArr1);
       }

       for (int i = 1; i <= kSSRS.getMaxRow(); i++) {
    	   String manageCode=kSSRS.GetText(i, 1).trim();
    	   String manageStr=" and managecom like '"+kSSRS.GetText(i, 1)+"%'";
    	   if(tSSRS.GetText(i, 1).trim().length()==6){
    		   manageStr=" and managecom = '"+kSSRS.GetText(i, 1)+"'";
    	   }
          String[] strArr1 = new String[15];
          
          String comCode1=manageCode;
          if(manageCode.length()>4){
        	  comCode1=manageCode.substring(0,4);
          }
          String sql2="select name from ldcom where comcode='" + comCode1 + "'";
          strArr1[0] = tExeSQL.getOneValue(sql2);
          strArr1[1] = comCode1;
          sql2="select name from ldcom where comcode='" + manageCode + "'";
          strArr1[2] = tExeSQL.getOneValue(sql2);
          strArr1[3] = manageCode;
           String sql3 =
                        "select count(edoracceptno) from lpedorapp where  apptype='1'"+
//                        "and managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                        manageStr+
                        " and makedate between '"+StartDate+"' and '"+EndDate+"'";
           lSSRS = tExeSQL.execSQL(sql3);
           strArr1[4] = lSSRS.GetText(1, 1);
           String sql4 =
                       "select count(edoracceptno) from lpedorapp  where apptype='2'"+
//                       " and managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          lSSRS = tExeSQL.execSQL(sql4);
          strArr1[5] = lSSRS.GetText(1, 1);
          String sql5 =
                       "select count(edoracceptno) from lpedorapp  where apptype='0'"+
//                       " and managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          lSSRS = tExeSQL.execSQL(sql5);
          strArr1[6] = lSSRS.GetText(1, 1);
          String sql6 =
                       "select count(edoracceptno) from lpedorapp  where apptype='3'"+
//                       " and managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                       manageStr+
                       " and makedate between '"+StartDate+"' and '"+EndDate+"'";
          lSSRS = tExeSQL.execSQL(sql6);
          strArr1[7] = lSSRS.GetText(1, 1);
          String sql7 =
                      "select count(edoracceptno) from lpedorapp where (apptype not in ('1','2','3','0') or apptype is null)"+
//                      " and managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                      manageStr+
                      " and makedate between '"+StartDate+"' and '"+EndDate+"'";
         lSSRS = tExeSQL.execSQL(sql7);
         strArr1[8] = lSSRS.GetText(1, 1);
         String sql8 =
                      "select count(edoracceptno) from lpedorapp  where "+
//                      " managecom like '"+kSSRS.GetText(i, 1)+"%'"+
                      " 1=1 "+manageStr+
                      " and makedate between '"+StartDate+"' and '"+EndDate+"'";
         lSSRS = tExeSQL.execSQL(sql8);
         strArr1[9] = lSSRS.GetText(1, 1);
         String strALL=lSSRS.GetText(1, 1);
         if(null==strALL || ""==strALL || "0".equals(strALL)){
        	  continue;
          }

           klistTable.add(strArr1);
       }

       String[] strArrHead1 = new String[15];
       strArrHead1[0] = "机构";
       strArrHead1[1] = "机构代码";
       strArrHead1[2] = "三级机构名称";
       strArrHead1[3] = "三级机构代码";
       strArrHead1[4] = "业务员代办";
       strArrHead1[5] = "信函服务";
       strArrHead1[6] = "柜台受理";
       strArrHead1[7] = "电话服务";
       strArrHead1[8] = "其它";
       strArrHead1[9] = "合计";

       xmlexport.addListTable(klistTable, strArrHead1);

       if (texttag.size() > 0)
       {
           xmlexport.addTextTag(texttag);
       }


       xmlexport.outputDocumentToFile("d:\\", "TaskPrint"); //输出xml文档到文件
       mResult.clear();
       mResult.addElement(xmlexport);

       return true;
   }



   public static void main(String[] args) {

       bqWorkFinLisBL tbqWorkFinLisBL = new bqWorkFinLisBL();
       VData tVData = new VData();
       TransferData tTransferData = new TransferData();
       tTransferData.setNameAndValue("StartDate", "2006-01-17");
       tTransferData.setNameAndValue("EndDate", "2006-02-17");
       tTransferData.setNameAndValue("organcode", "8611");
       tVData.addElement(tTransferData);
       GlobalInput tGlobalInput = new GlobalInput();

       tGlobalInput.ManageCom = "8611";
       tGlobalInput.Operator = "endor";
       tVData.addElement(tGlobalInput);

       tbqWorkFinLisBL.submitData(tVData, "PRINT");

       VData vdata = tbqWorkFinLisBL.getResult();



   }

}


