package com.sinosoft.lis.f1print;

import java.sql.Connection;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vdb.LCUWMasterDBSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class UWF1PCONFIRMBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    public UWF1PCONFIRMBLS()
    {
    }

    public static void main(String[] args)
    {
        UWF1PCONFIRMBLS mUWF1PCONFIRMBLS1 = new UWF1PCONFIRMBLS();
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start UWF1PCONFIRM BLS Submit...");

        tReturn = save(cInputData);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }

        System.out.println("End UWF1PCONFIRM BLS Submit...");

        return tReturn;
    }

//保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWF1PCONFIRMBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

// 核保通知书(update)
            System.out.println("Start 核保通知书...");

            LCUWMasterDBSet tLCUWMasterDBSet = new LCUWMasterDBSet(conn);
            tLCUWMasterDBSet.set((LCUWMasterSet) mInputData.
                                 getObjectByObjectName("LCUWMasterSet", 0));
            if (!tLCUWMasterDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWMasterDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWF1PCONFIRMBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "核保通知书表数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

//问题件表
//      System.out.println("Start 问题件...");
//
//      LCIssuePolDBSet tLCIssuePolDBSet=new LCIssuePolDBSet(conn);
//      tLCIssuePolDBSet.set((LCIssuePolSet)mInputData.getObjectByObjectName("LCIssuePolSet",0));
//      if (!tLCIssuePolDBSet.update())
//      {
//          // @@错误处理
//          this.mErrors.copyAllErrors(tLCIssuePolDBSet.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "UWF1PCONFIRMBLS";
//          tError.functionName = "saveData";
//          tError.errorMessage = "问题件表数据更新失败!";
//          this.mErrors .addOneError(tError) ;
//          conn.rollback() ;
//          conn.close();
//          return false;
//      }

// 打印管理表
            System.out.println("Start 打印管理表...");
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
            tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                      getObjectByObjectName(
                    "LOPRTManagerSchema", 0));
            if (!tLOPRTManagerDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWF1PCONFIRMBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "打印管理表数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWF1PCONFIRMBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }
        return tReturn;
    }
}