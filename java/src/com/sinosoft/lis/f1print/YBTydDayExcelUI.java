package com.sinosoft.lis.f1print;

/**
 * <p>Title: YBTydDayExcelUI</p>
 * <p>Description:X7-银保通异地代收日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : yan
 * @date:2013-09-16
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class YBTydDayExcelUI {
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;
    private String tOutXmlPath = "";
    public YBTydDayExcelUI() {
    }
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("PRINT")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            VData vData = new VData();
            if (!prepareOutputData(vData)) {
                return false;
            }

            YBTydDayExcelBL tYBTydDayExcelBL = new YBTydDayExcelBL();
            System.out.println("Start YBTydDayExcelBL Submit ...X7");
            if (!tYBTydDayExcelBL.submitData(vData, cOperate)) {
                if (tYBTydDayExcelBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tYBTydDayExcelBL.mErrors);
                    return false;
                } else {
                    buildError("submitData","tYBTydDayExcelBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "tYBTydDayExcelBL";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mDay);
            vData.add(mGlobalInput);
            vData.add(tOutXmlPath);

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /*
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        
        System.out.println("Start YBTydDayExcelUI getInputData ...X7");
        System.out.println("打印日期-->"+mDay[0]+"至"+mDay[1]+";tOutXmlPath-->"+tOutXmlPath);
        System.out.println("Operator-->"+mGlobalInput.Operator+";Managecom-->"+mGlobalInput.ManageCom);

        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (tOutXmlPath == null || tOutXmlPath.equals("")) {
            buildError("getInputData", "没有得到文件路径的信息！");
            return false;
        }
        return true;
    }
    
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "YBTydDayExcelUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        YBTydDayExcelUI fin = new YBTydDayExcelUI();
        String ttDay[] = new String[2];
        ttDay[0] = "2006-3-1";
        ttDay[1] = "2006-3-5";
        VData tVData = new VData();
        tVData.addElement(ttDay);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.Operator = "001";
        tVData.addElement(tG);
        fin.submitData(tVData, "PRINT");
    }
}
