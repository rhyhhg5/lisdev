package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.ListTable;

import java.io.InputStream;
import java.util.ArrayList;

//程序名称：OmDayDownloadBL.java
//程序功能：万能可转投资净额日结报表
//创建日期：2009-6-9
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class OmDayDownloadBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private TransferData mTransferData = new TransferData();

    private GlobalInput mGI = new GlobalInput();

    private SSRS mSSRS = new SSRS();
    
    private String mManageCom = "";
    
    private String mRiskCode = "";
    
    private String mComName = "";

    private String mStartDate = "";

    private String mEndDate = ""; 
    
    private ExeSQL mExeSQL = new ExeSQL();
    
    private XmlExport xml = new XmlExport();

    public OmDayDownloadBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	System.out.println("-----OmDayPrintBL download start-----");
        if (!getInputData(cInputData))
        {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        if (!getListData())
        {
            return false;
        }

        //获取打印数据
        if (!getPrintData())
        {
            return false;
        }
        System.out.println("-----OmDayPrintBL download end-----");
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
    	mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        if( mGI==null || mTransferData==null )
        {
            CError tError = new CError();
            tError.moduleName = "OmDayPrintBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mManageCom = (String)mTransferData.getValueByName("ManageCom");
        mRiskCode = (String)mTransferData.getValueByName("RiskCode");
        mStartDate = (String)mTransferData.getValueByName("StartDate");
        mEndDate = (String)mTransferData.getValueByName("EndDate"); 
        String sql = "select name from ldcom where comcode = '"+mManageCom+"'";
        mComName = mExeSQL.getOneValue(sql);
        return true;
    }

    
    private boolean getListData()
    {
    	String riskpart = "";
    	if(!"".equals(mRiskCode) && !"null".equals(mRiskCode) && mRiskCode != null )
    	{
    		riskpart = "and x.riskcode = '" + mRiskCode + "' " ;
    	}
        StringBuffer sql = new StringBuffer(128);
        sql.append("with tmp1(comcode,riskcode,moneytype,money) as ( ")
        .append("select managecom,riskcode,moneytype,sum(money) ")
        .append("from lcinsureacctrace ")
        .append("where  paydate between '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
        .append("group by managecom,riskcode,moneytype ")
        .append("union all ")
        .append("select managecom,riskcode,moneytype,sum(money) ")
        .append("from lbinsureacctrace ")
        .append("where  paydate between  '") .append(mStartDate).append("' and '").append(mEndDate).append("' ")
        .append("group by managecom,riskcode,moneytype ")
        .append("),tmp2(comcode,riskcode,bf_zf_money,b_money,lx_money,wt_money,ct_money,tm_money,fm_money,lq_money,gl_money) as ( ")
        .append("select comcode,riskcode, ")
        .append("sum(case when moneytype in ('BF','ZF') then money else 0 end) bf_zf_money, ")
        .append("sum(case when moneytype in ('B') then money else 0 end) b_money, ")
        .append("sum(case when moneytype in ('LX') then money else 0 end) lx_money, ")
        .append("sum(case when moneytype in ('WT') then -money else 0 end) wt_money, ")
        .append("sum(case when moneytype in ('CT') then -money else 0 end) ct_money, ")
        .append("sum(case when moneytype in ('TM') then -money else 0 end) tm_money, ")
        .append("sum(case when moneytype in ('MF') then -money else 0 end) fm_money, ")
        .append("sum(case when moneytype in ('LQ') then -money else 0 end) lq_money, ")
        .append("sum(case when moneytype in ('GL') then -money else 0 end) gl_money ")
        .append("from tmp1  ")
        .append("group by comcode,riskcode ")
        .append("),tmp3(comcode,riskcode,feefinatype,pay) as ( ")
        .append("select managecom,riskcode,managecom,sum(pay) ")
        .append("from LJAGetClaim ")
        .append("where confdate between  '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
        .append("group by managecom,riskcode,feefinatype ")
        .append("),tmp4(comcode,riskcode,sc,sw) as ( ")
        .append("select comcode,riskcode, ")
        .append("sum(case when  feefinatype = 'SC' then pay else 0 end) sc, ")
        .append("sum(case when  feefinatype = 'SW' then pay else 0 end) sw ")
        .append("from tmp3  ")
        .append("group by comcode,riskcode ")
        .append("),tmp5(comcode,riskcode,fee) as ( ")
        .append("select managecom,riskcode,sum(fee) from ( ")
        .append("select managecom,riskcode,fee ")
        .append("from lcinsureaccfeetrace ")
        .append("where paydate between  '").append(mStartDate).append("' and '").append(mEndDate).append("' and moneytype= 'GL'  ")
        .append("union all  ")
        .append("select managecom,riskcode,fee ")
        .append("from lbinsureaccfeetrace ")
        .append("where paydate between  '").append(mStartDate).append("' and '").append(mEndDate).append("' and moneytype= 'GL'  ")
        .append(") as xx ")
        .append("group by managecom,riskcode ")
        .append("),tmp6(comcode,comname,riskcode,riskname, risktype4) ")
        .append("as( ")
        .append("select comcode,name,riskcode,riskname,risktype4 from lmriskapp,ldcom), ")
        .append("tmp7(comcode,comname,riskcode,riskname,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11) ")
        .append("as( ")
        .append("select x.comcode,x.comname,x.riskcode,x.riskname, ")
        .append("(nvl(bf_zf_money,0)+nvl(fee,0)) c1, ") //保费收入
        .append("nvl(b_money,0) c2, ") //持续奖金
        .append("nvl(lx_money,0) c3, ") //保单利息
        .append("(nvl(gl_money,0)+nvl(fee,0)) c4, ") //初始费用
        .append("nvl(wt_money,0) c5, ") //犹豫期退保
        .append("nvl(ct_money,0) c6, ") //退保金
        .append("nvl(tm_money,0) c7, ") //退保费用
        .append("nvl(fm_money,0) c8, ") //保单管理费
        .append("nvl(lq_money,0) c9, ") //部分领取
        .append("nvl(sc,0) c10, ") //重疾保险金
        .append("nvl(sw,0) c11 ") //身故保险金
        .append("from tmp6 x  ")
        .append("left join tmp2 a on x.riskcode=a.riskcode and x.comcode=a.comcode ")
        .append("left join tmp4 b on x.riskcode=b.riskcode and x.comcode=b.comcode ")
        .append("left join tmp5 c on x.riskcode=c.riskcode and x.comcode=c.comcode  ")
        .append("where x.risktype4 = '4'  ")
        .append("and x.comcode like '") .append(mManageCom).append("%' ") .append(riskpart)
        .append("and length(trim(x.comcode)) = 8 order by x.comcode,x.riskcode) ")
        .append("select comcode,comname,riskcode,riskname,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,(c1+c2+c3-c4-c5-c6-c7-c8-c9-c10-c11) from tmp7 ");
        System.out.println(sql.toString());

        mSSRS = mExeSQL.execSQL(sql.toString());
        if (mExeSQL.mErrors.needDealError()) 
        {
            CError tError = new CError();
            tError.moduleName = "OmDayDownloadBL";
            tError.functionName = "getListData";
            tError.errorMessage = "获取打印数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将数据存放到XmlExport中
     * @return boolean
     */
    private boolean getPrintData()
    {
        xml.createDocument("OmDayDownload.htm", "printer");
        TextTag textTag = new TextTag();
        textTag.add("ManageName", mComName); 
        ArrayList arrayList = (ArrayList) mTransferData.getValueByName("arrayList");
        if(arrayList != null)
        {
            for (int i = 0; i < arrayList.size(); i++)
            {
                textTag.add((String) arrayList.get(i++), (String) arrayList.get(i));
            }
        }

        if (textTag.size() > 1)
        {
            xml.addTextTag(textTag);
        }

        String[] title = {"机构编码", "机构名称", "险种代码", "险种名称", "保费收入", "持续奖金", 
            	"保单利息", "初始费用", "犹豫期退保", "退保金", "退保费用", "保单管理费", 
            	"部分领取", "重疾保险金", "身故保险金", "可转投资净额"};
        xml.addListTable(this.getListTableData(title.length), title);
        xml.addTextTag(getSum());
        xml.outputDocumentToFile("c:\\", "zgmtest");
        mResult.clear();
        mResult.add(xml);

        return true;
    }

    /**
     * 将数据存放到ListTable中
     * @return ListTable
     */
    private ListTable getListTableData(int infoLength)
    {
        ListTable listTable = new ListTable();
        listTable.setName("BB");
        if (mSSRS.getMaxRow() > 0)
        {
            for (int i = 1; i <= mSSRS.getMaxRow(); i++)
            {
                String info[] = new String[infoLength];
                for(int j = 0; j < infoLength; j++)
                {
                    info[j] = mSSRS.GetText(i, j + 1);
                }
                listTable.add(info);
            }
        }
        return listTable;
    }
    
    /**
     * 取得总计金额
     * @return ListTable
     */
    private TextTag getSum()
    {
    	TextTag textTag = new TextTag();
        if (mSSRS.getMaxRow() > 0)
        {
            for(int j = 0; j < 12; j++)
            {
                double temp = 0.0;
                for (int i = 1; i <= mSSRS.getMaxRow(); i++)
                {
                	temp += Double.parseDouble(mSSRS.GetText(i, j + 5));
                }
                textTag.add("sum"+String.valueOf(j), CommonBL.bigDoubleToCommonString(temp, "0.00"));
            }
        }
        return textTag;
    }

    /**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg)
    {
        CError error = new CError();
        error.moduleName = "OmDayDownloadBL";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;
        this.mErrors.addOneError(error);
    }

    /**
     * 获得结果
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }
    
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    public static void main(String[] args)
    {
    }
}
