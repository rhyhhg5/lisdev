package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LAMaContReportBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(mInputData)) {
             return false;
         }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

    System.out.println("dayinchenggong1232121212121");

         return true;
     }


     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAMaContReportBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {

          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
              mTransferData = (TransferData) cInputData.getObjectByObjectName(
                      "TransferData", 0);
               //页面传入的数据 三个
               this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
               this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
               this.mEndDate = (String) mTransferData.getValueByName("tEndDate");

//               System.out.println(mManageCom);

          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

   }

   /**
      * 获取打印所需要的数据
      * @param cFunction String
      * @param cErrorMsg String
      */
     /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAStatisticReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



    private boolean queryData()
      {
          try{
              // 查询数据
              if(!getDataList())
              {
                  return false;
              }

//            System.out.println(mShowDataList.length);
//            System.out.println(mShowDataList[0].length);


             // 设置报表属性
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Order");

            for(int i=0;i<mShowDataList.length;i++)
              {
                  tlistTable.add(mShowDataList[i]);
              }
            //新建一个TextTag的实例
              TextTag  tTextTag = new TextTag();
              String tMakeDate = "";
              String tMakeTime = "";

              tMakeDate = mPubFun.getCurrentDate();
              tMakeTime = mPubFun.getCurrentTime();

              System.out.print("dayin252");
            if (!getManageNameB()) {
              return false;
              }
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
          tTextTag.add("tName", mManageName);
          tTextTag.add("StartDate",mStartDate);
          tTextTag.add("EndDate",mEndDate);


          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAMaContB.vts", "printer"); //初始化xml文档
          if (tTextTag.size() > 0)
          xmlexport.addTextTag(tTextTag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new1");
          this.mResult.clear();
          mResult.addElement(xmlexport);
          System.out.print("22222222222222222");
          return true;


          }catch(Exception ex)
          {
              buildError("queryData", "LABL发生错误，准备数据时出错！");
              return false;
          }


  }


  private boolean getDataList()
   {
       String tSQL = "";

       // 1、得到全部已开业的机构
       tSQL  = "select *  from labranchgroup where ";
       tSQL += "    managecom like  '" + mManageCom + "%'";
       tSQL += "    and branchtype='1' and  branchtype2='01'";
       tSQL += " order by branchattr";

       LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
       LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
       tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
       if(tLABranchGroupSet.size()==0)
       {
           buildError("queryData", "没有符合条件的机构！");
           return false;
       }

       // 2、查询需要表示的数据
       String[][] tShowDataList = new String[tLABranchGroupSet.size()][9];
       for(int i=1;i<=tLABranchGroupSet.size();i++)
       {// 循环机构进行统计
           LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
           tLABranchGroupSchema = tLABranchGroupSet.get(i);
           tShowDataList[i-1][0] = tLABranchGroupSchema.getBranchAttr();
           System.out.println(tLABranchGroupSchema.getAgentGroup()+"/"+tShowDataList[i-1][0]);

           // 查询需要表示的数据
           if (!queryOneDataList(tLABranchGroupSchema.getBranchAttr(),
                                 tLABranchGroupSchema.getBranchSeries(),
                                 tLABranchGroupSchema.getName(),
                                 tLABranchGroupSchema.getBranchManagerName(),
                                 tShowDataList[i - 1]))
           {
               System.out.println("[" + tLABranchGroupSchema.getAgentGroup() +
                                  "] 本机构数据查询失败！");
               return false;
           }
       }
       mShowDataList = tShowDataList;

       return true;
  }



  /**
    * 查询填充表示数据
    * @return boolean
    */
   private boolean queryOneDataList(String pmBranchAttr,String pmBranchSeries,
                                    String pmName,String pmBranchManageName,String[] pmOneDataList)
   {
       try{
           String fPerson="";
           String mPerson="";
           String cPerson="";
           pmOneDataList[0] = pmBranchAttr;
           // 2、管理机构名称
           pmOneDataList[1] = pmName;
             // 3、主管名称
           pmOneDataList[2] = pmBranchManageName==null?"":pmBranchManageName;
             // 4、总保费
           pmOneDataList[3] = getSumPrem(pmBranchSeries);
           // 5、总件数
           pmOneDataList[4] = getSumCont(pmBranchSeries);
            // 6、平均保费
            pmOneDataList[5] = getAvgPrem(pmBranchSeries);
             // 7、平均人力
           fPerson=""+getAgentCountByManageCom(mStartDate,pmBranchSeries);
           mPerson=""+getAgentCountByManageCom(mEndDate,pmBranchSeries);
           pmOneDataList[6] = getTransPreson(fPerson,mPerson);
           // 8、人均保费
           pmOneDataList[7] = getAvgMoney(pmOneDataList[3],pmOneDataList[6]);
           // 9、活动率
           cPerson = getHaveCaseAgentCount(pmBranchSeries);
           pmOneDataList[8] = getAct(cPerson,pmOneDataList[6]);
       }catch(Exception ex)
       {
           buildError("queryOneDataList", "准备数据时出错！");
           System.out.println(ex.toString());
           return false;
       }

       return true;
   }


  /**
    * 查询总保费
    * 根据机构序列编号  上级机构包括下级机构的保费
    * @param pmBranchSeries String
    * @return String
    */
   private String getSumPrem(String pmBranchSeries)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.##");


     tSQL  = "select sum(a.Prem) from lamakecont a";
     tSQL += " where a.ManageCom like '" + mManageCom+ "%'";
     tSQL += "   and substr(a.branchseries,1,length(trim('" + pmBranchSeries+ "')) )= '" + pmBranchSeries+ "'";
     tSQL += "   and a.Inputdate >= '" + mStartDate + "'";
     tSQL += "   and a.InputDate <= '" + mEndDate + "'";
     System.out.println(tSQL);
     try{
         tRtValue = tDF.format(execQuery(tSQL));
 //        System.out.println(tRtValue);

     }catch(Exception ex)
     {
         System.out.println("getSumPrem 出错！");
     }

     return tRtValue;

  }


  /**
   * 查询总件数
   * @param  pmBranchSeries String
   * @return String
   */
  private String getSumCont(String pmBranchSeries)
  {
    String tSQL = "";
    int tRtValue = 0;
    tSQL  = "select count(a.Proposalcontno) from lamakecont a";
    tSQL += " where a.ManageCom like '" + mManageCom+ "%'";
    tSQL += "   and substr(a.branchseries,1,length(trim('" + pmBranchSeries+ "')) )= '" + pmBranchSeries+ "'";
    tSQL += "   and a.Inputdate >= '" + mStartDate + "'";
    tSQL += "   and a.InputDate <= '" + mEndDate + "'";
    System.out.println(tSQL);
    try{
        tRtValue = (int)execQuery(tSQL);
//        System.out.println(tRtValue);
    }catch(Exception ex)
    {
        System.out.println("getFirstYearMoneyByManageCom 出错！");
    }

    return ""+tRtValue;

 }



 /**
   * 查询平均保费
   * @param pmBranchSeries String
   * @return String
   */
  private String getAvgPrem(String pmBranchSeries)
  {
    String tSQL = "";
    String tRtValue = "";
    DecimalFormat tDF = new DecimalFormat("0.##");


    tSQL  = "select avg(a.Prem) from lamakecont a";
    tSQL += " where a.ManageCom like '" + mManageCom+ "%'";
    tSQL += "   and substr(a.branchseries,1,length(trim('" + pmBranchSeries+ "')) )= '" + pmBranchSeries+ "'";
    tSQL += "   and a.Inputdate >= '" + mStartDate + "'";
    tSQL += "   and a.InputDate <= '" + mEndDate + "'";
//     System.out.println(tSQL);
    try{
        tRtValue = tDF.format(execQuery(tSQL));
//        System.out.println(tRtValue);
    }catch(Exception ex)
    {
        System.out.println("getFirstYearMoneyByManageCom 出错！");
    }

    return tRtValue;

 }

  /**
   * 查询管理机构下的期初、期末人数
   * @return int
   */
  private int getAgentCountByManageCom(String pmDate,String pmBranchSeries)
  {
      String tSQL = "";
      int tRtValue = 0;

      tSQL  = "select count(*)";
      tSQL += "  from laagent a";
      tSQL += " where a.ManageCom like '" + mManageCom + "%'";
      tSQL += "   and a.agentgroup  in  ";
      tSQL += "   (select c.agentgroup from labranchgroup c  where substr(c.branchseries,1,length(trim('" + pmBranchSeries + "')) )='" + pmBranchSeries + "'  and  c.managecom like '" + mManageCom + "%')";
      tSQL += "   and a.employdate<='" + pmDate + "'";
      tSQL += "   and (a.outworkdate>'" + pmDate + "' or a.outworkdate is null )";
      tSQL += "   and a.Branchtype='1' and a.BranchType2='01'";
//         System.out.println(tSQL);
      try{
          tRtValue = (int) execQuery(tSQL);
//           System.out.println(tRtValue);
      }catch(Exception ex)
      {
          System.out.println("getAgentCountByManageCom 出错！");
      }

      return tRtValue;
  }

  /**
    * 取得平均人力
    * @param pmValue1
    * @param pmValue2
    * @return String
    */
   private String getTransPreson(String pmValue1,String pmValue2)
   {
       String tSQL = "";
       String tRtValue = "";
       tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + " + " + pmValue2 +
              ",12,2) / 2,12,2) from dual";
//         System.out.println(tSQL);
       try{
           tRtValue = String.valueOf(execQuery(tSQL));
//            System.out.println(tRtValue);
       }catch(Exception ex)
       {
           System.out.println("getTransPreson 出错！");
       }

       return tRtValue;
   }

   /**
     * 计算人均保费
     * @param pmAgentCont
     * @param pmSumMoney
     * @return String
     */
    private String getAvgMoney(String pmSumMoney,String pmAgentCont)
    {
        String tSQL = "";
        String tRtValue = "";
        double tDbValue = 0.00;

        if(0.5 > Double.parseDouble(pmAgentCont))
        {
            return "0.0";
        }

        tSQL = "select DECIMAL(DECIMAL(" + pmSumMoney + ",12,2) / DECIMAL(" +
               pmAgentCont + ",12,2),12,2) from dual";
 //         System.out.println(tSQL);
        try{
            tDbValue = execQuery(tSQL);
//             System.out.println(tRtValue);
        }catch(Exception ex)
        {
            System.out.println("getAvgMoney 出错！");
        }

        tRtValue = String.valueOf(tDbValue);

        return tRtValue;
    }



    /**
      * 统计出单人数
      * @param pmBranchSeries String
      * @return String
      */
     private String getHaveCaseAgentCount(String pmBranchSeries)
     {
         String tSQL = "";
         int tRtValue = 0;

         tSQL  = "select count(distinct a.AgentCode) from lamakecont a";
         tSQL += " where a.ManageCom like '" + mManageCom + "%'";
         tSQL += "   and a.inputdate >= '" + mStartDate + "'";
         tSQL += "   and a.agentgroup  in  ";
         tSQL += "   (select c.agentgroup from labranchgroup c  where substr(c.branchseries,1,length(trim('" + pmBranchSeries + "')) )='" + pmBranchSeries + " ' and  c.managecom like '" + mManageCom + "%')";
         tSQL += "   and a.inputdate <= '" + mEndDate + "'";
         System.out.println(tSQL);
         try{
             tRtValue = (int) execQuery(tSQL);
//              System.out.println(tRtValue);
         }catch(Exception ex)
         {
             System.out.println("getHaveCaseAgentCount 出错！");
         }

         return "" + tRtValue;
  }

    /**
      * 计算活动率
      * @param pmCaseAgentCount String
      * @param pmAgentCountInit String
      * @return String
      */
     private String getAct(String pmCaseAgentCount,String pmAgentCountInit)
     {
         String tSQL = "";
         String tRtValue = "";
         DecimalFormat tDF = new DecimalFormat("0.##");

         if("0.0".equals(pmAgentCountInit))
         {
             return "0";
         }

         tSQL = "select DECIMAL(DECIMAL(" + pmCaseAgentCount + ",12,2) / DECIMAL(" +
                pmAgentCountInit + ",12,2) * 100,12,2) from dual";
//          System.out.println(tSQL);
         try{
             tRtValue = "" + tDF.format(execQuery(tSQL));
//              System.out.println(tRtValue);
         }catch(Exception ex)
         {
             System.out.println("getWorkRateAgent 出错！");
         }

         return tRtValue;
     }




   /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
              if (!conn.isClosed()) {
                  conn.close();
              }
              try {
                  st.close();
                  rs.close();
              } catch (Exception ex2) {
                  ex2.printStackTrace();
              }
              st = null;
              rs = null;
              conn = null;
            } catch (Exception e) {}

        }
    }



    private boolean getManageNameB() {

             String sql = "select name from ldcom where comcode='" + mManageCom +
                          "'";

             SSRS tSSRS = new SSRS();

             ExeSQL tExeSQL = new ExeSQL();

             tSSRS = tExeSQL.execSQL(sql);

             if (tExeSQL.mErrors.needDealError()) {

                 this.mErrors.addOneError("销售单位不存在！");

                 return false;

             }

             if (mManageCom.equals("86")) {
                 this.mManageName = "";
             } else {
                 if(mManageCom.length()>4)
                 {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
                 else
                 {this.mManageName = tSSRS.GetText(1, 1);}
             }
             System.out.println("1111111111111111111111");
             System.out.println(mManageName);
             return true;
      }






      /**
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }

/**
   public static void main(String[] args) {
           LAMaContReportBL tLAMaContReportBL = new LAMaContReportBL();
           TransferData tTransferData= new TransferData();
            tTransferData.setNameAndValue("tManageCom","8612");
            tTransferData.setNameAndValue("tStartDate","2007-07-01");
            tTransferData.setNameAndValue("tEndDate","2007-08-03");
          VData tVData = new VData();
          tVData.addElement(tTransferData);



           GlobalInput tG = new GlobalInput();
           tG.Operator = "001";
            tG.ManageCom = "86";
          tVData.addElement(tG);
            System.out.println("111");
           if (tLAMaContReportBL.submitData(tVData,"PRINT")) {

               System.out.println("right");
           } else
               System.out.println("error");
       }

*/
}
