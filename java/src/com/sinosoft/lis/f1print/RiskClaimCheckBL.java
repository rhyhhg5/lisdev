/**
 * <p>Title:需要输入统计项的的清单报表</p>
 * <p>Description: 4张报表</p>
 * <p>claim3：险种赔付状况</p>
 * <p>claim12：核赔人工作量统计</p>
 * <p>claim13：理赔月结案清单（分审核人）</p>
 * <p>claim14：理赔月未结案清单（分审核人）</p>
 * <p>add:</p>
 * <p>claim2：人身险赔付率</p>
 * <p>claim4：理赔经过时间</p>
 * <p>claim8：临时保单出险状况</p>
 * <p>claim9：短险保单出险状况</p>
 * <p>add:</p>
 * <p>claim1：人身险理赔概况</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class RiskClaimCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mManageCom = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDefineCode = ""; //统计输入的报表代码
    private String[] mDay = null; //统计输入的时间项

    public RiskClaimCheckBL()
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    { //打印

        //全局变量
        mDay = (String[]) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        mDefineCode = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));

        //test:
        System.out.println("大小：" + cInputData.size());
        System.out.println("starttime：" + mDay[0]);
        System.out.println("endtime：" + mDay[1]);
        System.out.println("操作员：" + mGlobalInput.Operator);
        System.out.println("操作机构：" + mGlobalInput.ManageCom);
        System.out.println("被统计的机构为：" + mManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "RiskClaimBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**传输数据的公共方法*/
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");

            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        if (!getPrintDataPay())
        {
            return false;
        }

        return true;
    }

    private boolean getPrintDataPay()
    {
        System.out.println("报表代码类型：" + mDefineCode);

        String xmlname = "";
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        FinDayTool tFinDayTool = new FinDayTool();
        FinDayTool ttFinDayTool = new FinDayTool();
        ListTable tlistTable = new ListTable();
        String[] strArr = null;

        ///////////////////////////////////人身险理赔概况/////////////////////////////////
        if (mDefineCode.equals("claim1"))
        {
            xmlexport.createDocument("RiskClaim.vts", "printer");

            String[][] mRiskType =
                    {
                    {"L", "人寿险"},
                    {"H", "健康险"},
                    {"A", "意外险"}
            };
            String Sel_sum_sql =
                    "select sum(llclaimunderwrite.realpay),lmriskapp.risktype";
            String Sel_count_sql =
                    "select count(distinct llclaim.clmno), lmriskapp.risktype";

            //理赔状态为签批同意
            String From_Where_sql = " from llclaimunderwrite,lmriskapp,llclaim"
                                    + " where llclaimunderwrite.riskcode=lmriskapp.riskcode and llclaim.clmno=llclaimunderwrite.clmno"
                                    + " and llclaim.clmstate in('2','3')";

            //给付件
            String JF_Contion_sql =
                    " and llclaimunderwrite.clmdecision in('1','2')";

            //拒付件
            String JUF_Contion_sql =
                    " and llclaimunderwrite.clmdecision in('0')";
            String ReportPunFun_sql = ReportPubFun.getWherePartLike(
                    "llclaimunderwrite.mngcom",
                    mManageCom)
                                      +
                                      ReportPubFun.getWherePart("llclaim.EndCaseDate",
                    mDay[0],
                    mDay[1], 1);
            String GroupBy_sql = " group by lmriskapp.risktype ";

            System.out.println(
                    "-------------------给付件数--------------------------");

            ExeSQL t_exesql = new ExeSQL();

            SSRS t_ssrs = t_exesql.execSQL(Sel_count_sql + From_Where_sql
                                           + JF_Contion_sql + ReportPunFun_sql
                                           + GroupBy_sql);

            if (t_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            System.out.println(
                    "-------------------给付件金额--------------------------");

            ExeSQL m_exesql = new ExeSQL();
            SSRS m_ssrs = m_exesql.execSQL(Sel_sum_sql + From_Where_sql
                                           + JF_Contion_sql + ReportPunFun_sql
                                           + GroupBy_sql);

            if (m_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            System.out.println("-------------------拒付件数---------------------");

            ExeSQL s_exesql = new ExeSQL();
            SSRS s_ssrs = s_exesql.execSQL(Sel_count_sql + From_Where_sql
                                           + JUF_Contion_sql + ReportPunFun_sql
                                           + GroupBy_sql);

            if (s_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            System.out.println("-------------------拒付金额---------------------");

            ExeSQL n_exesql = new ExeSQL();
            SSRS n_ssrs = n_exesql.execSQL(Sel_sum_sql + From_Where_sql
                                           + JUF_Contion_sql + ReportPunFun_sql
                                           + GroupBy_sql);

            if (n_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            double SumMoney = 0.0;
            double SumNum = 0.0;
            double JFSumMoney = 0.0;
            double JFSumNum = 0.0;

            for (int i = 0; i < mRiskType.length; i++)
            {
                String Flag = mRiskType[i][0];
                strArr = new String[5];
                strArr[0] = mRiskType[i][1];
                strArr[1] = ReportPubFun.getColSSRS(2, 1, t_ssrs, Flag);
                strArr[2] = ReportPubFun.getColSSRS(2, 1, m_ssrs, Flag);
                strArr[3] = ReportPubFun.getColSSRS(2, 1, s_ssrs, Flag);
                strArr[4] = ReportPubFun.getColSSRS(2, 1, n_ssrs, Flag);
                tlistTable.add(strArr);
                SumMoney = ReportPubFun.functionDouble(strArr[2]) + SumMoney;
                SumNum = ReportPubFun.functionDouble(strArr[1]) + SumNum;
                JFSumMoney = ReportPubFun.functionDouble(strArr[4])
                             + JFSumMoney;
                JFSumNum = ReportPubFun.functionDouble(strArr[3]) + JFSumNum;
            }

            strArr = new String[5];
            strArr[0] = "总计";
            strArr[1] = ReportPubFun.functionJD(SumNum, "0") + "件";
            strArr[2] = ReportPubFun.functionJD(SumMoney, "0.00") + "元";
            strArr[3] = ReportPubFun.functionJD(JFSumNum, "0") + "件";
            strArr[4] = ReportPubFun.functionJD(JFSumMoney, "0.00") + "元";
            tlistTable.add(strArr);
            tlistTable.setName("claim1");

            strArr = new String[5];
            strArr[0] = "Riskcode";
            strArr[1] = "js";
            strArr[2] = "money";
            strArr[3] = "JFjs";
            strArr[4] = "JFmoney";
            xmlexport.addListTable(tlistTable, strArr);
        }

        /////////////////////////////////////////////////////人身险赔付率//////////////////////////////////////////////////////
        if (mDefineCode.equals("claim2"))
        {
            xmlexport.createDocument("RiskClaimLifePfl.vts", "printer");

            String sql =
                    "select sum(llclaimpolicy.realpay) from llclaimpolicy,llclaim"
                    + " where llclaimpolicy.clmno=llclaim.clmno"
                    + " and llclaim.clmstate in('2','3')"
                    +
                    ReportPubFun.getWherePartLike("llclaim.mngcom", mManageCom)
                    + ReportPubFun.getWherePart("llclaim.EndCaseDate", mDay[0],
                                                mDay[1], 1);
            String long_sql = sql
                              + " and llclaimpolicy.riskcode in (select riskcode from lmriskapp where riskperiod in('L'))";
            String short_sql = sql
                               + " and  llclaimpolicy.riskcode in (select riskcode from lmriskapp where riskperiod in('M','S'))";
            String pfl = "select sum(sumactupaymoney) from ljapayperson"
                         + " where 1=1"
                         +
                         ReportPubFun.getWherePartLike("managecom", mManageCom)
                         + ReportPubFun.getWherePart("makedate", mDay[0],
                    mDay[1], 1);
            String pfl_lc = pfl
                            +
                    " and riskcode in(select riskcode from lmriskapp where riskperiod in('L'))";
            String pfl_sc = pfl
                            +
                    " and  riskcode in(select riskcode from lmriskapp where riskperiod in('M','S'))";
            System.out.println("-------------------短险赔付金额--------------------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(short_sql);
            System.out.println(
                    "---------------------长险赔付金额-----------------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(long_sql);
            System.out.println(
                    "-----------------------长险保费--------------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(pfl_lc);
            System.out.println(
                    "---------------------短险保费----------------------------");

            ExeSQL qExeSQL = new ExeSQL();
            SSRS qSSRS = new SSRS();
            qSSRS = qExeSQL.execSQL(pfl_sc);

            double longRealPay = ReportPubFun.functionDouble(nSSRS.GetText(1, 1));
            double shortRealPay = ReportPubFun.functionDouble(mSSRS.GetText(1,
                    1));
            double logMoney = ReportPubFun.functionDouble(pSSRS.GetText(1, 1));
            double shortMoney = ReportPubFun.functionDouble(qSSRS.GetText(1, 1));
            String logPfl = ReportPubFun.functionDivision(longRealPay * 100.0,
                    logMoney, "0.0")
                            + "%";
            String shortPfl = ReportPubFun.functionDivision(shortRealPay *
                    100.0,
                    shortMoney, "0.0")
                              + "%";
            double sumRealPay = longRealPay + shortRealPay;
            strArr = new String[5];
            strArr[0] = String.valueOf(longRealPay);
            strArr[1] = logPfl;
            strArr[2] = String.valueOf(shortRealPay);
            strArr[3] = shortPfl;
            strArr[4] = String.valueOf(sumRealPay);
            tlistTable.add(strArr);
            tlistTable.setName("claim2");
            strArr = new String[5];
            strArr[0] = "longRealPay";
            strArr[1] = "logPfl";
            strArr[2] = "shortRealPay";
            strArr[3] = "shortPfl";
            strArr[4] = "sumRealPay";
            xmlexport.addListTable(tlistTable, strArr);
        }

        ///////////////////////////////////////////////////////险种赔付状况//////////////////////////////////////////////////////
        if (mDefineCode.equals("claim3"))
        {
            xmlexport.createDocument("RiskClaimCheckBL.vts", "printer"); //xmlname="险种赔付状况";

            String tSql =
                    "select * from LJAPayPerson where PayAimClass='1' and paytype='ZC'"
                    + ReportPubFun.getWherePartLike("ManageCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("ConfDate",
                                                mDay[0], mDay[1], 1);
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(
                    tSql);

            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();

            String Sql =
                    "select  llclaimunderwrite.riskcode,llclaimunderwrite.realpay "
                    + " from llclaimunderwrite,llclaim "
                    + " where llclaimunderwrite.clmno=llclaim.clmno "
                    + " and llclaim.clmstate in('2','3')"
                    + ReportPubFun.getWherePartLike("llclaimunderwrite.MngCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                                                mDay[0], mDay[1], 1);
            ExeSQL claim_exesql = new ExeSQL();
            SSRS Claim_ssrs = claim_exesql.execSQL(Sql);

            for (int j = 1; j <= Claim_ssrs.getMaxRow(); j++)
            {
                String[] tRow = new String[1];
                String[] tColumn = new String[1];
                String[] mRow = new String[3];
                String[] mColumn = new String[1];
                tRow[0] = Claim_ssrs.GetText(j, 1); //险种
                mRow[0] = Claim_ssrs.GetText(j, 2); //金额
                mRow[1] = "1"; //件数
                mRow[2] = ReportPubFun.getPolmoney(tLJAPayPersonSet,
                        Claim_ssrs.GetText(j, 1)); //根据当前险种代码求保费

                FinCheckKey tF = new FinCheckKey(tRow, tColumn);
                FinCheckKey mF = new FinCheckKey(mRow, mColumn);
                tFinDayTool.enterBasicInfo(tF, mF);
            }

            tlistTable.setName("Claim3");

            Vector tv = tFinDayTool.getAllRiskCode();
            System.out.println("key.size:" + tv.size());

            Enumeration e = tv.elements();

            while (e.hasMoreElements())
            {
                String tRiskCode = (String) e.nextElement();
                String[] ttRow = new String[1];
                String[] ttColumn = new String[1];
                ttRow[0] = tRiskCode;

                FinCheckKey ttFinCheckKey = new FinCheckKey(ttRow, ttColumn);
                strArr = new String[5];
                strArr[0] = tRiskCode; //险种
                strArr[1] = ReportPubFun.getRiskName(tRiskCode, tLMRiskAppSet); //险种名称
                strArr[2] = ReportPubFun.functionJD(tFinDayTool.getCheckValue(
                        ttFinCheckKey,
                        1),
                        "0"); //赔附件数
                strArr[3] = ReportPubFun.functionJD(tFinDayTool.getCheckValue(
                        ttFinCheckKey,
                        0),
                        "0.00"); //赔付金额
                strArr[4] = ReportPubFun.functionDivision(tFinDayTool
                        .getCheckValue(ttFinCheckKey,
                                       0) * 100.0,
                        tFinDayTool
                        .getCheckValue(ttFinCheckKey,
                                       2),
                        "0.00") + "%"; //该险种赔付率
                tlistTable.add(strArr);
            }

            strArr = new String[5];
            strArr[0] = "RiskCode";
            strArr[1] = "RiskName";
            strArr[2] = "Pfjs";
            strArr[3] = "Pfje";
            strArr[4] = "Pfl";
            xmlexport.addListTable(tlistTable, strArr);
        }

        ////////////////////////////////////////////////////理赔经过时间/////////////////////////////////////////////
        if (mDefineCode.equals("claim4"))
        {
            xmlexport.createDocument("RiskClaimTime.vts", "printer"); //xmlname="理赔经过时间";

            String endjs_sql =
                    "select distinct ClmNo from llclaimuwmain where appactiontype='1'"
                    + ReportPubFun.getWherePartLike("MngCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("MakeDate",
                                                mDay[0], mDay[1], 1);
            String surjs_sql =
                    "select distinct ClmCaseNo from llsurvey where surveyflag='2'"
                    + ReportPubFun.getWherePartLike("MngCom",
                    mManageCom)
                    + ReportPubFun.getWherePart("MakeDate",
                                                mDay[0], mDay[1], 1);
            String endcasetime =
                    "select sum(llclaimuwmain.makedate-llregister.rgtdate)"
                    + " from llclaimuwmain,llregister"
                    + " where llclaimuwmain.rgtno =llregister.rgtno"
                    + " and  appactiontype='1'"
                    + ReportPubFun.getWherePart("llclaimuwmain.makedate",
                                                mDay[0], mDay[1], 1)
                    + ReportPubFun.getWherePartLike("llregister.mngcom",
                    mManageCom);

            String endcaseOneDay = "select count(llclaimuwmain.rgtno)"
                                   + " from llclaimuwmain,llregister"
                                   +
                    " where llclaimuwmain.rgtno =llregister.rgtno"
                                   + " and  appactiontype='1'"
                                   +
                    " and  llclaimuwmain.makedate=llregister.rgtdate"
                                   +
                                   ReportPubFun.getWherePart("llclaimuwmain.makedate",
                    mDay[0],
                    mDay[1], 1)
                                   +
                                   ReportPubFun.getWherePartLike("llregister.mngcom",
                    mManageCom);

            String surtime = "select  sum(ModifyDate-makedate)"
                             + " from llsurvey where surveyflag='2'"
                             + ReportPubFun.getWherePart("llsurvey.makedate",
                    mDay[0], mDay[1], 1)
                             + ReportPubFun.getWherePartLike("MngCom",
                    mManageCom);
            String surOneDay = "select  count(rgtno)"
                               + " from llsurvey where surveyflag='2'"
                               + " and ModifyDate=makedate"
                               + ReportPubFun.getWherePart("llsurvey.makedate",
                    mDay[0], mDay[1], 1)
                               + ReportPubFun.getWherePartLike("MngCom",
                    mManageCom);

            String claimtime =
                    "select sum(llclaimuwmain.makedate-llreport.rptdate)"
                    + " from llclaimuwmain,llregister,llreport"
                    + " where llclaimuwmain.rgtno =llregister.rgtno"
                    + " and llregister.rptno=llreport.rptno"
                    + " and  appactiontype='1'"
                    + ReportPubFun.getWherePart("llclaimuwmain.makedate",
                                                mDay[0], mDay[1], 1)
                    + ReportPubFun.getWherePartLike("llregister.mngcom",
                    mManageCom);

            String claimOneDay = "select count(llregister.rgtno)"
                                 + " from llclaimuwmain,llregister,llreport"
                                 +
                                 " where llclaimuwmain.rgtno =llregister.rgtno"
                                 + " and llregister.rptno=llreport.rptno"
                                 + " and  appactiontype='1'"
                                 +
                    "  and  llclaimuwmain.makedate=llreport.rptdate"
                                 +
                                 ReportPubFun.getWherePart("llclaimuwmain.makedate",
                    mDay[0], mDay[1], 1)
                                 +
                                 ReportPubFun.getWherePartLike("llregister.mngcom",
                    mManageCom);

            System.out.println("-----------结案件数----------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(endjs_sql);
            System.out.println("--------提起调查的件数-------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(surjs_sql);
            System.out.println("-------------结案时间---------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(endcasetime);
            System.out.println("-----------调查时间--------");

            ExeSQL qExeSQL = new ExeSQL();
            SSRS qSSRS = new SSRS();
            qSSRS = qExeSQL.execSQL(surtime);
            System.out.println("-----------理赔经过时间时间--------------");

            ExeSQL rExeSQL = new ExeSQL();
            SSRS rSSRS = new SSRS();
            rSSRS = rExeSQL.execSQL(claimtime);

            System.out.println("-------------当天结案时间---------");

            ExeSQL sExeSQL = new ExeSQL();
            SSRS sSSRS = new SSRS();
            sSSRS = sExeSQL.execSQL(endcaseOneDay);
            System.out.println("-----------当天调查时间--------");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(surOneDay);
            System.out.println("-----------当天理赔经过时间时间--------------");

            ExeSQL vExeSQL = new ExeSQL();
            SSRS vSSRS = new SSRS();
            vSSRS = vExeSQL.execSQL(claimOneDay);

            int endcase = nSSRS.MaxRow;
            int surcase = mSSRS.MaxRow;
            double endcaseday = Double.parseDouble(pSSRS.GetText(1, 1));
            double surday = Double.parseDouble(qSSRS.GetText(1, 1));
            double claimday = Double.parseDouble(rSSRS.GetText(1, 1));
            double eOneDay = Double.parseDouble(sSSRS.GetText(1, 1));
            double sOneDay = Double.parseDouble(tSSRS.GetText(1, 1));
            double cOneDay = Double.parseDouble(vSSRS.GetText(1, 1));

            String avgCaseTime = ReportPubFun.functionDivision(endcaseday
                    + (eOneDay * 0.5),
                    endcase, "0.00");
            String avgSurTime = ReportPubFun.functionDivision(surday,
                    surcase
                    + (sOneDay * 0.5),
                    "0.00");
            String avgClaimTime = ReportPubFun.functionDivision(claimday
                    + (cOneDay * 0.5),
                    endcase, "0.00");
            strArr = new String[3];
            strArr[0] = avgCaseTime;
            strArr[1] = avgSurTime;
            strArr[2] = avgClaimTime;
            tlistTable.add(strArr);
            tlistTable.setName("claim4");
            strArr = new String[3];
            strArr[0] = "avgCaseTime";
            strArr[1] = "avgSurTime";
            strArr[2] = "avgClaimTime";
            xmlexport.addListTable(tlistTable, strArr);
        }

        ///////////////////////////////////////////七日结案率///////////////////////////////
        if (mDefineCode.equals("claim5"))
        {
            xmlexport.createDocument("RiskClaimSevenDays.vts", "printer"); //最好紧接着就初始化xml文档

            String mSql = "select mngcom from llclaim"
                          + " where clmstate in('2','3')"
                          + ReportPubFun.getWherePart("endcasedate", mDay[0],
                    mDay[1], 1)
                          + ReportPubFun.getWherePartLike("mngcom", mManageCom);
            ;
            System.out.println(
                    "---------------------已结案的案件数的----------------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(mSql);

            String kSql =
                    "select llregister.mngcom from llclaimuwmain,llregister "
                    + " where (llclaimuwmain.makedate-llregister.rgtdate)<=7"
                    +
                    " and llclaimuwmain.rgtno =llregister.rgtno and llclaimuwmain.appactiontype='1'"
                    + ReportPubFun.getWherePart("llclaimuwmain.makedate",
                                                mDay[0], mDay[1], 1)
                    + ReportPubFun.getWherePartLike("llclaimuwmain.mngcom",
                    mManageCom);
            ;
            System.out.println(
                    "-------------------7日结案数-----------------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(kSql);

            String UWJ_sql = "select comCode from ldcom where 1=1"
                             + ReportPubFun.getWherePartLike("comCode",
                    mManageCom);
            System.out.println("-------------获得唯一管理机构的sql语句是-----------");

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(UWJ_sql);
            tlistTable.setName("claim5"); //xmlname="七日结案率";

            double sas = 0;
            double jas = 0;

            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[3];
                strArr[0] = ReportPubFun.getMngName(kSSRS.GetText(i, 1));
                strArr[1] = kSSRS.GetText(i, 1); //机构代码
                sas = ReportPubFun.getCounts(pSSRS, kSSRS.GetText(i, 1));
                jas = ReportPubFun.getCounts(mSSRS, kSSRS.GetText(i, 1));
                strArr[2] = ReportPubFun.functionDivision(sas, jas, "0.000"); //案率
                tlistTable.add(strArr);
            }

            tlistTable.add(strArr);
            strArr = new String[3];
            strArr[0] = "Mange";
            strArr[1] = "MangeCode";
            strArr[2] = "Seven";
            xmlexport.addListTable(tlistTable, strArr);
        }

        /////////////////////////////////////三日理赔调查完成率//////////////////////////////////////////
        if (mDefineCode.equals("claim6"))
        {
            xmlexport.createDocument("RiskClaimThreeDays.vts", "printer"); //最好紧接着就初始化xml文档
            System.out.println("---------------结案中的调查件----------------");

            String nSql = "select llsurvey.mngcom from llsurvey,llclaim"
                          + " where llsurvey.rgtno=llclaim.rgtno"
                          + " and llclaim.clmstate in('2','3')"
                          + ReportPubFun.getWherePart("llclaim.endcasedate",
                    mDay[0], mDay[1], 1);

            SSRS nSSRS = new SSRS();
            ExeSQL nExeSQL = new ExeSQL();
            nSSRS = nExeSQL.execSQL(nSql);
            System.out.println("--------------三日完成数-----------------");

            String mSql = "select mngcom from llsurvey"
                          + " where (ModifyDate-makedate)<=3"
                          +
                          ReportPubFun.getWherePart("makedate", mDay[0], "", 1)
                          + ReportPubFun.getWherePart("ModifyDate", "",
                    mDay[1], 1);
            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(mSql);

            String UWJ_sql =
                    "select comCode from ldcom where length(trim(comCode))=4";
            System.out.println("-------------获得唯一的4位管理机构的sql语句是-----------");

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(UWJ_sql);
            tlistTable.setName("claim6"); //xmlname="三日调查完成率";
            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[5];
                strArr[0] = ReportPubFun.getMngName(kSSRS.GetText(i, 1));
                strArr[1] = kSSRS.GetText(i, 1); //机构代码
                strArr[2] = ReportPubFun.getCount(nSSRS, kSSRS.GetText(i, 1)); //理赔调查件数
                strArr[3] = ReportPubFun.getCount(mSSRS, kSSRS.GetText(i, 1)); //三日理赔调查完成件数
                strArr[4] = ReportPubFun.functionDivision(strArr[2], strArr[3],
                        "0.000"); //三日理赔调查完成率
                tlistTable.add(strArr);
            }

            strArr = new String[5];
            strArr[0] = "Mange";
            strArr[1] = "MangeCode";
            strArr[2] = "ThreeSumCount";
            strArr[3] = "ThreeExplorCount";
            strArr[4] = "ThreeExplor";
            xmlexport.addListTable(tlistTable, strArr);
        }

        /////////////////////////////////////////////////////临时保单出险状况/////////////////////////////////////////////////////////////
        if (mDefineCode.equals("claim8"))
        {
            xmlexport.createDocument("RiskClaimLS.vts", "printer"); //最好紧接着就初始化xml文档

            String LS_JS =
                    "select distinct llclaimpolicy.clmno from llclaimpolicy,llclaim"
                    + " where llclaimpolicy.clmno=llclaim.clmno"
                    +
                    " and  llclaimpolicy.poltype='0'  and llclaim.clmstate in('2','3')"
                    + ReportPubFun.getWherePartLike("llclaim.mngcom",
                    mManageCom)
                    + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                                                mDay[0], mDay[1], 1);

            String LS_JE =
                    "select sum(llclaimpolicy.RealPay) from llclaimpolicy,llclaim"
                    + " where llclaimpolicy.clmno=llclaim.clmno"
                    +
                    " and  llclaimpolicy.poltype='0'  and llclaim.clmstate in('2','3')"
                    + ReportPubFun.getWherePartLike("llclaim.mngcom",
                    mManageCom)
                    + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                                                mDay[0], mDay[1], 1);
            String JF_JS = "select  distinct llclaimunderwrite.clmno "
                           + " from llclaimunderwrite,llclaim"
                           + " where llclaimunderwrite.clmno=llclaim.clmno "
                           +
                    " and  llclaim.clmstate in('4') and llclaimunderwrite.clmdecision='0'"
                           +
                    " and  llclaim.clmno in (select clmno from llclaimpolicy where poltype='0')"
                           + ReportPubFun.getWherePartLike("llclaim.mngcom",
                    mManageCom)
                           + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                    mDay[0], mDay[1], 1);
            System.out.println("-------------------临时保单出险件数------------------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(LS_JS);
            System.out.println(
                    "----------------------临时保单出险赔款金额-------------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(LS_JE);
            System.out.println(
                    "------------------------拒付件数sql-------------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(JF_JS);

            int LSNum = nSSRS.MaxRow;
            int JFNum = pSSRS.MaxRow;
            double LSMoney = Double.parseDouble(mSSRS.GetText(1, 1));
            String LSJFV = ReportPubFun.functionDivision(JFNum, LSNum, "0.00");
            strArr = new String[4];
            strArr[0] = String.valueOf(LSNum) + "件";
            strArr[1] = String.valueOf(LSMoney) + "件";
            strArr[2] = String.valueOf(JFNum) + "元";
            strArr[3] = LSJFV + "%";
            tlistTable.setName("claim8"); //xmlname="临时保单出险状况";
            tlistTable.add(strArr);
            strArr = new String[4];
            strArr[0] = "LSNum";
            strArr[1] = "JFNum";
            strArr[2] = "LSMoney";
            strArr[3] = "LSJFV";
            xmlexport.addListTable(tlistTable, strArr);
        }

        //////////////////////////////////////////////////////短险出险状况////////////////////////////////////////////////////////
        if (mDefineCode.equals("claim9"))
        {
            xmlexport.createDocument("RiskClaimDX.vts", "printer"); //最好紧接着就初始化xml文档

            String DQ_JS = "select  distinct clmno from llclaim"
                           + " where  CasePayType='1' " //短期给付
                           + " and   clmstate in('2','3')"
                           + " and   clmno in (select clmno from llclaimunderwrite where riskcode in(select riskcode from lmriskapp where  riskperiod='L'))" //两年以上
                           + ReportPubFun.getWherePartLike("mngcom", mManageCom)
                           + ReportPubFun.getWherePart("EndCaseDate", mDay[0],
                    mDay[1], 1);
            String DQ_JE = "select  Sum(realpay) from llclaim"
                           + " where  CasePayType='1' " //短期给付
                           + " and   clmstate in('2','3')"
                           + " and   clmno in (select clmno from llclaimunderwrite where riskcode in(select riskcode from lmriskapp where  riskperiod='L'))" //两年以上
                           + ReportPubFun.getWherePartLike("mngcom", mManageCom)
                           + ReportPubFun.getWherePart("EndCaseDate", mDay[0],
                    mDay[1], 1);
            String DQ_JF =
                    "select  distinct llclaim.clmno from llclaimunderwrite,llclaim"
                    + " where llclaimunderwrite.clmno=llclaim.clmno and llclaimunderwrite.clmdecision='0' "
                    +
                    " and llclaim.clmstate in('4') and llclaim.CasePayType='1'"
                    + " and llclaimunderwrite.riskcode in(select riskcode from lmriskapp where riskperiod='L')"
                    + ReportPubFun.getWherePartLike("llclaim.mngcom",
                    mManageCom)
                    + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                                                mDay[0], mDay[1], 1);
            System.out.println(
                    "------------------短期出险件数-----------------------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(DQ_JS);
            System.out.println("------------------短期出险赔款金额----------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(DQ_JE);
            System.out.println(
                    "--------------------拒付件数sql--------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(DQ_JF);

            int DXNum = nSSRS.MaxRow;
            int JFNum = pSSRS.MaxRow;
            double DXMoney = Double.parseDouble(mSSRS.GetText(1, 1));
            String DXJFV = ReportPubFun.functionDivision(JFNum, DXNum, "0.00");
            strArr = new String[4];
            strArr[0] = String.valueOf(DXNum) + "件";
            strArr[1] = String.valueOf(DXMoney) + "件";
            strArr[2] = String.valueOf(JFNum) + "元";
            strArr[3] = DXJFV + "%";
            tlistTable.setName("claim9"); //xmlname="短险出险状况";
            tlistTable.add(strArr);
            strArr = new String[4];
            strArr[0] = "DXNum";
            strArr[1] = "JFNum";
            strArr[2] = "DXMoney";
            strArr[3] = "DXJFV";
            xmlexport.addListTable(tlistTable, strArr);
        }

        //////////////////////////////////////////核赔人工作量统计//////////////////////////////
        if (mDefineCode.equals("claim12"))
        {
            xmlexport.createDocument("RiskClaimWorkForce.vts", "printer"); //最好紧接着就初始化xml文档

            String nSql = "select llregister.handler from llregister,llclaim"
                          + " where llclaim.rgtno=llregister.rgtno"
                          + ReportPubFun.getWherePartLike("llregister.mngcom",
                    mManageCom)
                          + ReportPubFun.getWherePart("llregister.rgtdate",
                    mDay[0], mDay[1], 1);
            System.out.println(
                    "--------------求核赔人为审核人（立案时）的审核案件总数-----------------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(nSql);

            String mSql = "select llregister.handler from llregister ,llclaim"
                          + " where llclaim.rgtno=llregister.rgtno"
                          + " and llclaim.clmstate in('2','3'))"
                          + ReportPubFun.getWherePartLike("llclaim.mngcom",
                    mManageCom)
                          + ReportPubFun.getWherePart("llclaim.EndCaseDate",
                    mDay[0], mDay[1], 1);
            System.out.println(
                    "---------------------已结案(签批同意)的案件数的---------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(mSql);

            String sSql = "select llregister.handler from llregister"
                          +
                    " where (rgtno in(select rgtno from llclaim where clmstate not in('2','3'))"
                          + ReportPubFun.getWherePartLike("llregister.mngcom ",
                    mManageCom)
                          + ReportPubFun.getWherePart("llregister.rgtdate",
                    mDay[0], mDay[1], 1);
            System.out.println(
                    "------------------未结案的案件数（立案时）------------------------");

            ExeSQL sExeSQL = new ExeSQL();
            SSRS sSSRS = new SSRS();
            sSSRS = sExeSQL.execSQL(sSql);

            String kSql =
                    "select llregister.handler from  llclaim,llregister where (llclaim.endcasedate-llregister.rgtdate)>7"
                    +
                    " and llclaim.rgtno =llregister.rgtno and llclaim.clmstate  in('2','3')"
                    + ReportPubFun.getWherePart("llregister.rgtdate",
                                                mDay[0], "", 1)
                    + ReportPubFun.getWherePart("llclaim.endcasedate",
                                                "", mDay[1], 1)
                    + ReportPubFun.getWherePartLike("llregister.mngcom ",
                    mManageCom);
            System.out.println(
                    "------------------7日未结案数（立案时）------------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(kSql);

            String qSql = "select surveyor from llsurvey where SurveyFlag='2' "
                          + ReportPubFun.getWherePartLike("mngcom ", mManageCom)
                          + ReportPubFun.getWherePart("makedate", mDay[0],
                    mDay[1], 1);
            System.out.println(
                    "----------------调查件数（调查入机时间）--------------------");

            ExeSQL qExeSQL = new ExeSQL();
            SSRS qSSRS = new SSRS();
            qSSRS = qExeSQL.execSQL(qSql);

            String tSql = "select distinct(handler) from llregister"
                          + ReportPubFun.getWherePartLike("mngcom", mManageCom)
                          + ReportPubFun.getWherePart("rgtdate", mDay[0],
                    mDay[1], 1);
            System.out.println("寻找唯一的核赔人(审核人）的tsql:" + tSql);

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(tSql);

            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[7];
                strArr[0] = ReportPubFun.getLdUserInfo(kSSRS.GetText(i, 1))
                            .getUserName(); //姓名
                strArr[1] = ReportPubFun.getMngName(ReportPubFun.getLdUserInfo(
                        kSSRS
                        .GetText(i,
                                 1))
                        .getComCode()); //机构
                strArr[2] = ReportPubFun.getCaseCount(nSSRS, kSSRS.GetText(i, 1)); //审核案件总数
                strArr[3] = ReportPubFun.getCaseCount(mSSRS, kSSRS.GetText(i, 1)); //已结案件数
                strArr[4] = ReportPubFun.getCaseCount(sSSRS, kSSRS.GetText(i, 1)); //未结案件数
                strArr[5] = ReportPubFun.getCaseCount(pSSRS, kSSRS.GetText(i, 1)); //7日未结案数
                strArr[6] = ReportPubFun.getCaseCount(qSSRS, kSSRS.GetText(i, 1)); //调查件数
                tlistTable.add(strArr);
            }

            tlistTable.setName("claim12"); //xmlname="核赔人工作量统计";
            strArr = new String[7];
            strArr[0] = "username";
            strArr[1] = "Mange";
            strArr[2] = "SumCaseCount";
            strArr[3] = "EndCaseCount";
            strArr[4] = "NoCaseCount";
            strArr[5] = "NoCaseSevendays";
            strArr[6] = "CheckCaseCount";
            xmlexport.addListTable(tlistTable, strArr);
        }

        ////////////////////////////////////////////理赔月结案清单（分审核人）//////////////////////////////////
        if (mDefineCode.equals("claim13"))
        {
            //set:
            LDCodeDB tLDcodeDB = new LDCodeDB();
            String msql = "select * from LDcode";
            LDCodeSet tLDCodeSet = tLDcodeDB.executeQuery(msql);

            xmlexport.createDocument("RiskClaimEndCaseMonth.vts", "printer"); //最好紧接着就初始化xml文档

            String Handler_sql =
                    "select distinct(handler) from llregister where 1=1"
                    + ReportPubFun.getWherePartLike("mngcom",
                    mManageCom);
            System.out.println("获取唯一的审核人的sql语句是：" + Handler_sql);

            ExeSQL t_exesql = new ExeSQL();
            SSRS t_ssrs = t_exesql.execSQL(Handler_sql);
            System.out.println("查询得到的审核人的个数是" + t_ssrs.getMaxRow());

            if (t_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            for (int i = 1; i <= t_ssrs.getMaxRow(); i++)
            {
                String EndCaseMonth_sql =
                        "select llregister.rgtno, llregister.Handler,llclaimuwmain.clmuwer,"
                        + " llregister.rgtdate,llclaimuwmain.makedate"
                        + " from llclaimuwmain,llregister"
                        +
                        " where llclaimuwmain.rgtno =llregister.rgtno and appactiontype='1'"
                        + ReportPubFun.getWherePartLike("llregister.mngcom",
                        mManageCom)
                        + ReportPubFun.getWherePart("llclaimuwmain.makedate",
                        mDay[0], mDay[1], 1) //签批同意时间
                        + " and llregister.handler='"
                        + t_ssrs.GetText(i, 1) + "'";
                ExeSQL EndCaseMonth_exesql = new ExeSQL();
                SSRS EndCaseMonth_ssrs = EndCaseMonth_exesql.execSQL(
                        EndCaseMonth_sql);
                System.out.println("按照该审核人查询相关信息的条数是"
                                   + EndCaseMonth_ssrs.getMaxRow());

                if (EndCaseMonth_ssrs.getMaxRow() == 0)
                {
                    buildError("getPrintDataPay()", "查询的结果是0");
                }

                for (int j = 1; j <= EndCaseMonth_ssrs.getMaxRow(); j++)
                {
                    String[] tRow = new String[1];
                    String[] tColumn = new String[1];
                    String[] mRow = new String[8];
                    String[] mColumn = new String[1];
                    tRow[0] = EndCaseMonth_ssrs.GetText(j, 2); //审核人
                    tColumn[0] = EndCaseMonth_ssrs.GetText(j, 1); //案件号(立案号)
                    mRow[0] = ReportPubFun.getLLCaseInfo(EndCaseMonth_ssrs
                            .GetText(j, 1)); //事故者姓名
                    mRow[1] = ReportPubFun.getDutyKindName(ReportPubFun
                            .getLLClaimInfo(EndCaseMonth_ssrs
                                            .GetText(j,
                            1)),
                            "getdutykind",
                            tLDCodeSet); //借用变更项目--求给付类型名称
                    mRow[2] = EndCaseMonth_ssrs.GetText(j, 3); //签批人
                    mRow[3] = EndCaseMonth_ssrs.GetText(j, 4); //立案日期
                    mRow[4] = EndCaseMonth_ssrs.GetText(j, 5); //结案日期
                    mRow[5] = ReportPubFun.getPayMoney(EndCaseMonth_ssrs
                            .GetText(j, 1), mDay[0],
                            mDay[1], "1"); //给付金额
                    mRow[6] = ReportPubFun.getPayMoney(EndCaseMonth_ssrs
                            .GetText(j, 1), mDay[0],
                            mDay[1], "0"); //拒付金额
                    mRow[7] = "1";

                    FinCheckKey tF = new FinCheckKey(tRow, tColumn);
                    FinCheckKey mF = new FinCheckKey(mRow, mColumn);
                    tFinDayTool.enterBasicInfo(tF, mF);
                }
            }

            double SumPayMoney = 0.0;
            double SumPayNoMoney = 0.0;
            double SumNum = 0;
            Vector tv = tFinDayTool.getAllRowKey(0); //审核人
            Vector mv = tFinDayTool.getAllColumnKey(0); //案件号
            System.out.println("keyrow.size:" + tv.size());
            System.out.println("keycolumn.size:" + mv.size());

            Enumeration e = tv.elements();

            while (e.hasMoreElements())
            {
                String tHandler = (String) e.nextElement();
                Enumeration en = mv.elements();

                while (en.hasMoreElements())
                {
                    String tCaseNO = (String) en.nextElement();
                    String[] tttRow = new String[1];
                    String[] tttColumn = new String[1];
                    tttRow[0] = tHandler;
                    tttColumn[0] = tCaseNO;

                    FinCheckKey tttFinCheckKey = new FinCheckKey(tttRow,
                            tttColumn);
                    strArr = new String[10];
                    strArr[0] = ReportPubFun.getLdUserInfo(tHandler)
                                .getUserName(); //姓名
                    strArr[1] = ReportPubFun.getMngName(ReportPubFun.
                            getLdUserInfo(tHandler)
                            .getComCode()); //机构
                    strArr[2] = tCaseNO; ///案件号
                    strArr[3] = tFinDayTool.getUniqueValue(tttFinCheckKey, 0); //事故者姓名
                    strArr[4] = tFinDayTool.getUniqueValue(tttFinCheckKey, 1); //给付类型
                    strArr[5] = ReportPubFun.getLdUserInfo(tFinDayTool
                            .getUniqueValue(tttFinCheckKey,
                                            2))
                                .getUserName(); //签批人
                    strArr[6] = tFinDayTool.getUniqueValue(tttFinCheckKey, 3); //立案日期
                    strArr[7] = tFinDayTool.getUniqueValue(tttFinCheckKey, 4); //结案日期
                    strArr[8] = tFinDayTool.getUniqueValue(tttFinCheckKey, 5); //给付金额
                    strArr[9] = tFinDayTool.getUniqueValue(tttFinCheckKey, 6); //拒付金额

                    if (strArr[3].equals("") || strArr[4].equals("")
                        || strArr[5].equals("") || strArr[6].equals("")
                        || strArr[7].equals("") || strArr[8].equals("")
                        || strArr[9].equals(""))
                    {
                        continue;
                    }

                    tlistTable.add(strArr);
                }

                //求审核人统计小计
                String[] ttRow = new String[1];
                String[] ttColumn = new String[1];
                ttRow[0] = tHandler;

                FinCheckKey ttFinCheckKey = new FinCheckKey(ttRow, ttColumn);
                double tTotalPayMoney = tFinDayTool.getTotalValue(ttFinCheckKey,
                        5);
                double tTotalPayNoMoney = tFinDayTool.getTotalValue(
                        ttFinCheckKey,
                        6);
                double tTotalNum = tFinDayTool.getTotalValue(ttFinCheckKey, 7);
                strArr = new String[10];
                strArr[0] = "小计";
                strArr[1] = new DecimalFormat("0").format(tTotalNum) + "件";
                strArr[2] = "";
                strArr[3] = "";
                strArr[4] = "";
                strArr[5] = "";
                strArr[6] = "";
                strArr[7] = "";
                strArr[8] = new DecimalFormat("0.00").format(tTotalPayMoney)
                            + "元";
                strArr[9] = new DecimalFormat("0.00").format(tTotalPayNoMoney)
                            + "元";
                tlistTable.add(strArr);
                SumPayMoney = SumPayMoney + tTotalPayMoney;
                SumPayNoMoney = SumPayNoMoney + tTotalPayNoMoney;
                SumNum = SumNum + tTotalNum;
            }

            strArr = new String[10];
            strArr[0] = "总计";
            strArr[1] = new DecimalFormat("0").format(SumNum) + "件";
            strArr[2] = "";
            strArr[3] = "";
            strArr[4] = "";
            strArr[5] = "";
            strArr[6] = "";
            strArr[7] = "";
            strArr[8] = new DecimalFormat("0.00").format(SumPayMoney) + "元";
            strArr[9] = new DecimalFormat("0.00").format(SumPayNoMoney) + "元";
            tlistTable.add(strArr);
            tlistTable.setName("claim13");
            xmlname = "理赔月结案清单（分审核人）";
            strArr = new String[10];
            strArr[0] = "Handler";
            strArr[1] = "MangeCode";
            strArr[2] = "CaseNO";
            strArr[3] = "CustomerName";
            strArr[4] = "GetDutyKind";
            strArr[5] = "Clmuwer";
            strArr[6] = "StartTime";
            strArr[7] = "EndTime";
            strArr[8] = "Money";
            strArr[9] = "NoMoney";
            xmlexport.addListTable(tlistTable, strArr);
        }

        //////////////////////////////理赔月未结案时清单（分审核人）//////////////////////////////////////////////////
        if (mDefineCode.equals("claim14"))
        {
            xmlexport.createDocument("RiskClaimNoEndCaseMonth.vts", "printer"); //最好紧接着就初始化xml文档

            String Handler_sql =
                    "select distinct(handler) from llregister where 1=1"
                    + ReportPubFun.getWherePartLike("mngcom",
                    mManageCom);
            System.out.println("获取未结案唯一的审核人的sql语句是：" + Handler_sql);

            ExeSQL t_exesql = new ExeSQL();
            SSRS t_ssrs = t_exesql.execSQL(Handler_sql);
            System.out.println("查询未结案时得到的审核人的个数是" + t_ssrs.getMaxRow());

            if (t_ssrs.getMaxRow() == 0)
            {
                buildError("getPrintDataPay", "查询的结果是0");
            }

            for (int i = 1; i <= t_ssrs.getMaxRow(); i++)
            {
                String EndCaseMonth_sql =
                        "select llregister.rgtno,llclaim.clmno, llregister.Handler,llregister.rgtdate,llregister.Operator"
                        + " from  llregister,llclaim"
                        +
                        " where  llclaim.rgtno =llregister.rgtno and llclaim.clmstate not in('2','3')"
                        + ReportPubFun.getWherePartLike("llregister.mngcom",
                        mManageCom)
                        + ReportPubFun.getWherePart("llregister.rgtdate",
                        mDay[0], mDay[1], 1) //签批同意时间
                        + " and llregister.handler='"
                        + t_ssrs.GetText(i, 1) + "'";
                ExeSQL EndCaseMonth_exesql = new ExeSQL();
                SSRS EndCaseMonth_ssrs = EndCaseMonth_exesql.execSQL(
                        EndCaseMonth_sql);
                System.out.println("按照未结案时该审核人查询相关信息的条数是"
                                   + EndCaseMonth_ssrs.getMaxRow());

                if (EndCaseMonth_ssrs.getMaxRow() == 0)
                {
                    buildError("getPrintDataPay", "查询的结果是0");
                }

                for (int j = 1; j <= EndCaseMonth_ssrs.getMaxRow(); j++)
                {
                    String[] tRow = new String[1];
                    String[] tColumn = new String[1];
                    String[] mRow = new String[7];
                    String[] mColumn = new String[1];
                    tRow[0] = EndCaseMonth_ssrs.GetText(j, 3); //审核人
                    tColumn[0] = EndCaseMonth_ssrs.GetText(j, 1); //立案号
                    mRow[0] = ReportPubFun.getLLCaseInfo(EndCaseMonth_ssrs
                            .GetText(j, 1)); //事故者姓名
                    mRow[1] = CaseFunPub.getCaseStateByRgtNo(EndCaseMonth_ssrs
                            .GetText(j, 1)); //理赔状态
                    mRow[2] = ReportPubFun.getLLClaimUWMainInfo(
                            EndCaseMonth_ssrs
                            .GetText(j, 2)); //签批人
                    mRow[3] = EndCaseMonth_ssrs.GetText(j, 4); //立案日期
                    mRow[4] = EndCaseMonth_ssrs.GetText(j, 5); //立案人
//                    mRow[5] = ReportPubFun.getLLSurveyInfo(EndCaseMonth_ssrs
//                                                           .GetText(j, 1)); //调查人
                    mRow[6] = "1";

                    FinCheckKey tF = new FinCheckKey(tRow, tColumn);
                    FinCheckKey mF = new FinCheckKey(mRow, mColumn);
                    tFinDayTool.enterBasicInfo(tF, mF);
                }
            }

            double SumNum = 0;
            Vector tv = tFinDayTool.getAllRowKey(0); //审核人
            Vector mv = tFinDayTool.getAllColumnKey(0); //案件号
            System.out.println("keyrow.size:" + tv.size());
            System.out.println("keycolumn.size:" + mv.size());

            Enumeration e = tv.elements();

            while (e.hasMoreElements())
            {
                String tHandler = (String) e.nextElement();
                Enumeration en = mv.elements();

                while (en.hasMoreElements())
                {
                    String tCaseNO = (String) en.nextElement();
                    String[] tttRow = new String[1];
                    String[] tttColumn = new String[1];
                    tttRow[0] = tHandler;
                    tttColumn[0] = tCaseNO;

                    FinCheckKey tttFinCheckKey = new FinCheckKey(tttRow,
                            tttColumn);
                    strArr = new String[9];
                    strArr[0] = ReportPubFun.getLdUserInfo(tHandler)
                                .getUserName(); //姓名
                    strArr[1] = ReportPubFun.getMngName(ReportPubFun.
                            getLdUserInfo(tHandler)
                            .getComCode()); //机构
                    strArr[2] = tCaseNO; ///案件号
                    strArr[3] = tFinDayTool.getUniqueValue(tttFinCheckKey, 0); //事故者姓名
                    strArr[4] = tFinDayTool.getUniqueValue(tttFinCheckKey, 1); //理赔状态
                    strArr[5] = ReportPubFun.getLdUserInfo(tFinDayTool
                            .getUniqueValue(tttFinCheckKey,
                                            2))
                                .getUserName(); //签批人
                    strArr[6] = tFinDayTool.getUniqueValue(tttFinCheckKey, 3); //立案日期
                    strArr[7] = ReportPubFun.getLdUserInfo(tFinDayTool
                            .getUniqueValue(tttFinCheckKey,
                                            4))
                                .getUserName(); //立案人
                    strArr[8] = tFinDayTool.getUniqueValue(tttFinCheckKey, 5); //调查人可以为空

                    if (strArr[3].equals("") || strArr[4].equals("")
                        || strArr[5].equals("") || strArr[6].equals("")
                        || strArr[7].equals(""))
                    {
                        continue;
                    }

                    tlistTable.add(strArr);
                }

                //求审核人统计小计
                String[] ttRow = new String[1];
                String[] ttColumn = new String[1];
                ttRow[0] = tHandler;

                FinCheckKey ttFinCheckKey = new FinCheckKey(ttRow, ttColumn);
                double tTotalNum = tFinDayTool.getTotalValue(ttFinCheckKey, 6);
                strArr = new String[9];
                strArr[0] = "小计";
                strArr[1] = new DecimalFormat("0").format(tTotalNum) + "件";
                strArr[2] = "";
                strArr[3] = "";
                strArr[4] = "";
                strArr[5] = "";
                strArr[6] = "";
                strArr[7] = "";
                strArr[8] = "";
                tlistTable.add(strArr);
                SumNum = SumNum + tTotalNum;
            }

            strArr = new String[9];
            strArr[0] = "总计";
            strArr[1] = new DecimalFormat("0").format(SumNum) + "件";
            strArr[2] = "";
            strArr[3] = "";
            strArr[4] = "";
            strArr[5] = "";
            strArr[6] = "";
            strArr[7] = "";
            strArr[8] = "";
            tlistTable.add(strArr);
            tlistTable.setName("claim14");
            xmlname = "理赔月未结案清单（分审核人）";
            strArr = new String[9];
            strArr[0] = "Handler";
            strArr[1] = "MangeCode";
            strArr[2] = "CaseNO";
            strArr[3] = "CustomerName";
            strArr[4] = "ClaimState";
            strArr[5] = "Clmuwer";
            strArr[6] = "StartTime";
            strArr[7] = "Reger";
            strArr[8] = "Surveyor";
            xmlexport.addListTable(tlistTable, strArr);
        }

        String CurrentDate = PubFun.getCurrentDate();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", ReportPubFun.getMngName(mManageCom));
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("time", CurrentDate);
        System.out.println("大小" + texttag.size());

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //xmlexport.outputDocumentToFile("e:\\",xmlname);//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private String getName(String riskType)
    {
        String Name = "";

        if (riskType.equals("L"))
        {
            Name = "人寿险";
        }

        if (riskType.equals("H"))
        {
            Name = "健康险";
        }

        if (riskType.equals("A"))
        {
            Name = "意外险";
        }

        return Name;
    }

    public static void main(String[] args)
    {
    }
}
