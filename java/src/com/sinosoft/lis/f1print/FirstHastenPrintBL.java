package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author z
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import java.io.InputStream;
import java.io.FileOutputStream;

public class FirstHastenPrintBL implements PrintService {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCContSchema mLCContSchema;
    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    private String mRePrintFlag;

    private double fPremSum = 0;
    private double fPremAddSum = 0;
    private double fPrem = 0;
    private double fPremAdd = 0;
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private TransferData tTransferData;
    /** 总行数 */
    private static int sumCount = 10;
    private String mLoadFlag;

    public FirstHastenPrintBL() {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        try {
            if (!cOperate.equals("CONFIRM") &&
                !cOperate.equals("PRINT")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            System.out.println("Come to BL");

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData)) {
                return false;
            }

            if (cOperate.equals("CONFIRM")) {
                mResult.clear();
                // 准备所有要打印的数据
                getPrintData();
            } else if (cOperate.equals("PRINT")) {
                if (!saveData(cInputData)) {
                    return false;
                }
            }

            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }


    public static void main(String[] args) {
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    tLOPRTManagerSchema.setPrtSeq("81000001418");
    tLOPRTManagerSchema.setOtherNo("13000041125");
    VData tVData = new VData();
    tVData.addElement(tLOPRTManagerSchema);

    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86";
    tG.Operator = "001";
    tVData.addElement(tG);
    FirstHastenPrintBL tFirstHastenPrintBL = new FirstHastenPrintBL();
    if (!tFirstHastenPrintBL.submitData(tVData, "CONFIRM")){
       if (tFirstHastenPrintBL.mErrors.needDealError()) {
           System.out.println(tFirstHastenPrintBL.mErrors.getFirstError());
       } else {
           System.out.println("tURGENoticeUI发生错误，但是没有提供详细的出错信息");
       }
   }

   else {
       VData vData = tFirstHastenPrintBL.getResult();
       XmlExport xe = (XmlExport) vData.get(0);

       try {
           InputStream ins = xe.getInputStream();
           FileOutputStream fos = new FileOutputStream("LCPolData.xml");
           int n = 0;

           while ((n = ins.read()) != -1) {
               fos.write(n);
           }

           fos.close();
       } catch (Exception ex) {
           ex.printStackTrace();
       }
   }

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    private String getDate(Date date) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        tTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        if (tTransferData != null) {
            this.mRePrintFlag = (String) tTransferData.getValueByName("RePrintFlag");
        }
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                              getObjectByObjectName("LOPRTManagerSchema", 0);
//只赋给schema一个prtseq

        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

//得到返回值
    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "RefuseAppF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

// 准备所有要打印的数据
    private void getPrintData() throws Exception {
        /** 首先封装打印管理信息数据 */
        LOPRTManagerDB mLOPRTManagerDB = getPrintManager();
        //根据印刷号查询打印队列中的纪录
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("getPrintData", "在取得打印队列中数据时发生错误");
           // return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //保存打印队列数据
        if (mLOPRTManagerSchema.getStateFlag() == null) {
            buildError("getprintData", "无效的打印状态");
        }
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

        //LCContDB tLCContDB = getContInfo();
        //String strContNo = tLCContDB.getContNo();

        // 查询打印队列的信息
        //mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        /******************************************************************************************/
        /** 将数据添加进TextTag */
        TextTag texttag = new TextTag();
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        SSRS tSSRS = new SSRS();
        SSRS tSSRS1 = new SSRS();
        SSRS tSSRS2 = new SSRS();
        SSRS tSSRS3 = new SSRS();
        SSRS tSSRS4 = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String tPrtNo = mLOPRTManagerSchema.getOtherNo();
        String appName = "";
        String appAddress = "";
        String appZipCode = "";
        String appCValiDate = "";
        String prem ="";
        String comLetter = "";
        String comAddres = "";
        String comZipCode = "";
        String comFax = "";
        String comPhone = "";
        String cuSex = "";
        String agentNo = "";
        String applyDate = "";
        String uWDate = "";
        String agentName = "";
        String agentPhone = "";
        String currentDate = PubFun.getCurrentDate();
        currentDate = currentDate.replace('-', '月');
        currentDate = currentDate.replaceFirst("月", "年") + "日";
        String getNoticeNo = "";
        String firstSendDate = "";
        String endHastenDate = "";
        String bankName = "";
        String bankAccNo = "";
        String accName = "";
        String failReason = "";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContDB.setPrtNo(tPrtNo);
        tLCContSet = tLCContDB.query();
        String strContNo;
        if (tLCContSet.size() > 0) {
            strContNo = tLCContSet.get(1).getContNo();
            appCValiDate= tLCContSet.get(1).getCValiDate();
            appCValiDate = appCValiDate.replace('-', '月');
            appCValiDate = appCValiDate.replaceFirst("月", "年") + "日";
            prem= String.valueOf(tLCContSet.get(1).getPrem());
            String strSQL1 = "select a.AppntName , zipcode , b.PostalAddress , a.PolApplyDate, a.UWDate ,a.BankAccNo,a.AccName,"
                             + " (select bankname from LDbank where bankcode=a.bankcode),(case when a.AppntSex='1' then '女士' else '先生' end)"
                             + " from lccont a ,lcaddress b , LCAppnt c "
                             + " where a.appntno=b.customerno and a.prtno='" +
                             tPrtNo +
                    "' and a.contno=c.contno and c.addressno=b.addressno";
            tSSRS = tExeSQL.execSQL(strSQL1);
            for (int m1 = 1; m1 <= tSSRS.getMaxRow(); m1++) {
                appName = tSSRS.GetText(m1, 1);
                appZipCode = tSSRS.GetText(m1, 2);
                appAddress = tSSRS.GetText(m1, 3);
                applyDate = tSSRS.GetText(m1, 4);
                applyDate = applyDate.replace('-', '月');
                applyDate = applyDate.replaceFirst("月", "年") + "日";
                uWDate = tSSRS.GetText(m1, 5);
                uWDate = uWDate.replace('-', '月');
                uWDate = uWDate.replaceFirst("月", "年") + "日";
                bankAccNo = tSSRS.GetText(m1, 6);
                accName = tSSRS.GetText(m1, 7);
                bankName = tSSRS.GetText(m1, 8);
                cuSex = tSSRS.GetText(m1, 9);
            }

            String strSQL2 = "select LetterServiceName,ServicePostAddress,LetterServicePostZipcode,Fax,Phone from LDCom"
                             + " where comcode = '" +
                             tLCContSet.get(1).getManageCom() + "' ";
            tSSRS1 = tExeSQL.execSQL(strSQL2);
            for (int m2 = 1; m2 <= tSSRS1.getMaxRow(); m2++) {
                comLetter = tSSRS1.GetText(m2, 1);
                comAddres = tSSRS1.GetText(m2, 2);
                comZipCode = tSSRS1.GetText(m2, 3);
                comFax = tSSRS1.GetText(m2, 4);
                comPhone = tSSRS1.GetText(m2, 5);
            }
            if(StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("1")){
                String strSQL3 =
                        "select max(a.makedate) from LOPrtManager a"
                        + " where a.code = '75' and a.otherno='" + tPrtNo +
                        "' "; //得到首期保费催缴通知下发日期 ??
                tSSRS2 = tExeSQL.execSQL(strSQL3);
                for (int m3 = 1; m3 <= tSSRS2.getMaxRow(); m3++) {
                    firstSendDate = tSSRS2.GetText(m3, 1);
                    firstSendDate = firstSendDate.replace('-', '月');
                    firstSendDate = firstSendDate.replaceFirst("月", "年") + "日";
                }
                endHastenDate = new ExeSQL().getOneValue("select max(a.ForMakeDate) from LOPrtManager a where a.code = '07' and a.otherno='" + tLCContSet.get(1).getProposalContNo() +"'");
                getNoticeNo = "";
            }else{
                String strSQL3 =
                        "select max(a.makedate), b.startpaydate,b.GetNoticeNo from LOPrtManager a,ljspay b"
                        + " where a.code = '65' and a.otherno='" + tPrtNo +
                        "' and a.otherno=b.otherno group by b.startpaydate,b.GetNoticeNo "; //得到首期保费催缴通知下发日期 ??
                tSSRS2 = tExeSQL.execSQL(strSQL3);
                for (int m3 = 1; m3 <= tSSRS2.getMaxRow(); m3++) {
                    firstSendDate = tSSRS2.GetText(m3, 1);
                    firstSendDate = firstSendDate.replace('-', '月');
                    firstSendDate = firstSendDate.replaceFirst("月", "年") + "日";
                    endHastenDate = tSSRS2.GetText(m3, 2);
                    getNoticeNo = tSSRS2.GetText(m3, 3);
                }
            }
            String strSQL4 = "select Agentcode,name , case when (Mobile='' or Mobile is null) then Phone else Mobile end "
                    + " from LAAgent where agentcode = '"+tLCContSet.get(1).getAgentCode()+"'";
            tSSRS3=  tExeSQL.execSQL(strSQL4);
            for (int m4 = 1; m4 <= tSSRS3.getMaxRow(); m4++) {
                agentNo = tSSRS3.GetText(m4, 1);
                agentName = tSSRS3.GetText(m4, 2);
                agentPhone = tSSRS3.GetText(m4, 3);
            }

           String strSQL5 = "select a.bankunsuccreason from lyreturnfrombankb a, LJSpay c"
                    + " where a.paycode=c.getnoticeno "
                    + " and a.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=c.getnoticeno)"
                    + " and c.otherno='"+tPrtNo+"'";
           String strSQL6 = "select b.codename from lyreturnfrombankb a,ldcode1 b, LJSpay c"
                    + " where a.paycode=c.getnoticeno and b.codetype='bankerror' and b.code=a.bankcode and b.code1=a.banksuccflag"
                    + " and a.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=c.getnoticeno)"
                    + " and c.otherno='"+tPrtNo+"'";
            String tBankReason=new ExeSQL().getOneValue(strSQL6);
            tSSRS4 = tExeSQL.execSQL(strSQL5);
            for (int m5 = 1; m5 <= tSSRS4.getMaxRow(); m5++) {
                if(!StrTool.cTrim(tSSRS4.GetText(m5, 1)).equals("")){
                    failReason=tSSRS4.GetText(m5, 1);
                }else if(!StrTool.cTrim(tBankReason).equals("")){
                    failReason = tBankReason;
                }else{
                    failReason ="";
                }
            }

            // 设置合计项
            fPremSum = 0;
            fPremAddSum = 0;
            // 查出所有的险种投保单
            String strsql =
                    "SELECT a.* FROM LCPol a,LCUWMaster b WHERE a.ContNo = '" +
                    strContNo +
                    "' and a.polno=b.polno and a.UWFlag in ('4','9') "
                    + " order by a.PolNo";
            if (StrTool.cTrim(tLCContSet.get(1).getCardFlag()).equals("1")) {
                strsql =
                        "SELECT a.* FROM LCPol a WHERE a.ContNo = '" +
                        strContNo + "' order by a.PolNo";
            }
            LCPolDB tempLCPolDB = new LCPolDB();
            System.out.println("$$ : " + strsql);
            LCPolSet tLCPolSet = tempLCPolDB.executeQuery(strsql);

            if (tempLCPolDB.mErrors.needDealError()) {
                mErrors.copyAllErrors(tempLCPolDB.mErrors);
                throw new Exception("在获取附加险投保单时出错！");
            }
            String[] RiskInfoTitle = new String[10];

            RiskInfoTitle[0] = "RiskName";
            RiskInfoTitle[1] = "RiskCode";
            RiskInfoTitle[2] = "InsuredName";
            RiskInfoTitle[3] = "Prem";
            RiskInfoTitle[4] = "PremAdd";
            RiskInfoTitle[5] = "PremSum";
            RiskInfoTitle[6] = "YearsIntv";
            RiskInfoTitle[7] = "PayYearsIntv";
            RiskInfoTitle[8] = "AM";
            RiskInfoTitle[9] = "PayIntv";

            ListTable tListTable = new ListTable();
            String StrRiskInfo[] = null;
            tListTable.setName("RiskInfo");

            for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++) {
                mLCPolSchema = tLCPolSet.get(nIndex + 1).getSchema();
                StrRiskInfo = new String[10];

                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
                if (!tLMRiskDB.getInfo()) {
                    mErrors.copyAllErrors(tLMRiskDB.mErrors);
                    buildError("outputXML", "在取得主险LMRisk的数据时发生错误");

                }
                StrRiskInfo[0] = tLMRiskDB.getRiskName();

                // StrRiskInfo[1] = (new Double(mLCPolSchema.getAmnt())).toString();
                //StrRiskInfo[2] = (new Integer(mLCPolSchema.getPayYears())).toString();
                StrRiskInfo[1] = mLCPolSchema.getRiskCode();
                StrRiskInfo[2] = mLCPolSchema.getInsuredName();

                String strSQL = "SELECT SUM(Prem) FROM LCPrem WHERE"
                                + " PolNo = '" + mLCPolSchema.getPolNo() + "'"
                                + " AND PayPlanCode NOT LIKE '000000%'";

                ExeSQL exeSQL = new ExeSQL();

                SSRS ssrs = exeSQL.execSQL(strSQL);

                if (exeSQL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(exeSQL.mErrors);
                    throw new Exception("取标准保费时出错");
                }

                if (!(ssrs.GetText(1, 1).equals("0") ||
                      ssrs.GetText(1, 1).trim().equals("") ||
                      ssrs.GetText(1, 1).equals("null"))) {
                    fPrem = Double.parseDouble(ssrs.GetText(1, 1));
                } else {
                    fPrem = 0.0;
                }

                StrRiskInfo[3] = new Double(fPrem).toString();
                fPremSum += fPrem;
                // 取加费
                String strSql = "SELECT SUM(Prem) FROM LCPrem WHERE"
                                + " PolNo = '" + mLCPolSchema.getPolNo() + "'"
                                + " AND PayPlanCode LIKE '000000%'";

                exeSQL = new ExeSQL();

                ssrs = exeSQL.execSQL(strSql);

                if (exeSQL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(exeSQL.mErrors);
                    throw new Exception("取加费时出错");
                }

                if (!(ssrs.GetText(1, 1).equals("0") ||
                      ssrs.GetText(1, 1).trim().equals("") ||
                      ssrs.GetText(1, 1).equals("null"))) {
                    fPremAdd = Double.parseDouble(ssrs.GetText(1, 1));
                } else {
                    fPremAdd = 0.0;
                }
                StrRiskInfo[4] = StrTool.cTrim(new Double(fPremAdd).toString());

                fPremAddSum += fPremAdd;
                double fSum = fPremAdd + fPrem;
                StrRiskInfo[5] = StrTool.cTrim(String.valueOf(fSum));

                //StrRiskInfo[5] = (setPrecision(mLCPolSchema.getPrem(),"0.00")).toString();
                StrRiskInfo[5] = StrTool.cTrim((new Double(mLCPolSchema.getPrem())).
                                               toString());

                strSQL = "select rtrim(char(InsuYear))||case InsuYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                         + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
                exeSQL = new ExeSQL();
                ssrs = exeSQL.execSQL(strSQL);
                if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A") &&
                    tLCPolSet.get(nIndex + 1).getInsuYear() == 1000) {
                    StrRiskInfo[6] = "终身";
                }
                if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A") &&
                    tLCPolSet.get(nIndex + 1).getInsuYear() != 1000) {
                    StrRiskInfo[6] = "至" +
                                     tLCPolSet.get(nIndex + 1).getInsuYear() +
                                     "岁";
                }
                if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase().
                    equals("M")) {
                    StrRiskInfo[6] = tLCPolSet.get(nIndex + 1).getInsuYear() +
                                     "月";
                }
                if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase().
                    equals("Y")) {
                    StrRiskInfo[6] = tLCPolSet.get(nIndex + 1).getInsuYear() +
                                     "年";
                }
                if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase().
                    equals("D")) {
                    StrRiskInfo[6] = tLCPolSet.get(nIndex + 1).getInsuYear() +
                                     "日";
                }
                if (tLCPolSet.get(nIndex + 1).getPayIntv() == 0) {
                    StrRiskInfo[7] = "-";
                } else {
                    strSQL = "select rtrim(char(PayEndYear))||case PayEndYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                             + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
                    exeSQL = new ExeSQL();
                    ssrs = exeSQL.execSQL(strSQL);
                    StrRiskInfo[7] = StrTool.cTrim(ssrs.GetText(1, 1));
                    if (StrRiskInfo[7].endsWith("岁")) {
                        StrRiskInfo[7] = "至" + StrRiskInfo[7];
                    }
                }
                strSQL =
                        "select case Mult when 0 then Amnt else integer(Mult) end from LCPol where"
                        + " PolNo = '" + mLCPolSchema.getPolNo() + "'";

                exeSQL = new ExeSQL();
                ssrs = exeSQL.execSQL(strSQL);
                if (ssrs.GetText(1, 1).length() == 1) {
                    StrRiskInfo[8] = ssrs.GetText(1, 1) + '档';
                } else {
                    StrRiskInfo[8] = StrTool.cTrim(ssrs.GetText(1, 1)) + "元";
                }
                /**@author:Yangming 添加缴费频次 */
                strSQL =
                        "select codename from ldcode a,lcpol b where b.polno='" +
                        mLCPolSchema.getPolNo() +
                        "' and a.code=char(b.payintv) and a.codetype='payintv' and conttype='1'";
                String tPayIntv = exeSQL.getOneValue(strSQL);
                if (tPayIntv == null || tPayIntv.equals("") ||
                    tPayIntv.equals("null")) {
                    buildError("getPrintData",
                               "查找险种" + mLCPolSchema.getRiskCode() +
                               "缴费频次失败！");
                    throw new Exception("查找险种" + mLCPolSchema.getRiskCode() +
                                        "缴费频次失败！");
                }
                StrRiskInfo[9] = tPayIntv;
                tListTable.add(StrRiskInfo);
            }
            if (StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("4")) {
                texttag.add("EndHastenDate", endHastenDate);
                xmlExport.createDocument("FirstHastenPrint.vts", "printer");
            } else {
               texttag.add("EndHastenDate", endHastenDate);
                xmlExport.createDocument("FirstHastenPrintXJ.vts", "printer");
            }
            if (tListTable.size() > 0) {
                xmlExport.addListTable(tListTable, RiskInfoTitle);
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupDB.setAgentGroup(tLCContSet.get(1).getAgentGroup());
            tLABranchGroupSet = tLABranchGroupDB.query();
            texttag.add("Agentgroup", tLABranchGroupSet.get(1).getBranchAttr());
            String FirstPayMakedate = (new ExeSQL()).getOneValue(
                    "select max(makedate) from loprtmanager where otherno='" +
                    tLCContSet.get(1).getProposalContNo() + "' and code='07'");
            texttag.add("FirstPayMakedate", FirstPayMakedate);
            texttag.add("PremSum", String.valueOf(fPremSum + fPremAddSum));
        }

        texttag.add("AppntName", appName);
        texttag.add("AppntZipcode", appZipCode);
        texttag.add("AppntAddr", appAddress);
        texttag.add("ManageCom", comLetter);
        texttag.add("ManageAddress", comAddres);
        texttag.add("ManageZipCode", comZipCode);
        texttag.add("ManageFax", comFax);
        texttag.add("ManagePhone", comPhone);
        texttag.add("AppntName", appName);
        texttag.add("PrtNo", tPrtNo);
        texttag.add("Cavidate1", appCValiDate);
		//2014-11-3   杨阳
		//业务员编码显示为表LAagent中的GroupAgentCode字段
		String groupAgentcode = new ExeSQL().getOneValue("select getUniteCode("+agentNo+") from dual");
        texttag.add("Agentcode", groupAgentcode);
        texttag.add("Title", cuSex);
        texttag.add("PolApplyDate", applyDate);
        texttag.add("UWDate", uWDate);
        texttag.add("Prem", prem);
        texttag.add("AgentName", agentName);
        texttag.add("AgentPhone", agentPhone);
        texttag.add("Today", firstSendDate);

        texttag.add("GetNoticeNo", getNoticeNo);
        texttag.add("FirstHastenFeeDate", firstSendDate);
        texttag.add("EndHastenDate", endHastenDate);
        texttag.add("FailReason", failReason);

        texttag.add("BankName", bankName);
        texttag.add("BankAccNo", bankAccNo);
        texttag.add("AccName", accName);

        System.out.println(" mLoadFlag : " + mLoadFlag);

        if (texttag.size() > 0) {
            xmlExport.addTextTag(texttag);
        }

        xmlExport.outputDocumentToFile("e:\\", "firtpay");
        mResult.clear();
        mResult.addElement(xmlExport);

    }

    /**********************************************************************************************/
    private String getAgentPhone() {
        String phone = !StrTool.cTrim(tLAAgentSchema.getMobile()).equals("") ?
                       tLAAgentSchema.getMobile() :
                       StrTool.cTrim(tLAAgentSchema.getPhone());
        return phone.equals("") ? "          " : phone;
    }

    /**
     * getPayDate
     *
     * @param tPrtNo String
     * @return String
     */
    private String getPayDate(String tPrtNo) {
        String paydate = (new ExeSQL()).getOneValue(
                "select max(paydate) from ljtempfee where otherno='" + tPrtNo +
                "'");
        if (paydate == null || paydate.equals("") || paydate.equals("null")) {
            return "";
        }
        return getDate((new FDate()).getDate(paydate));
    }

    /**
     * getOperatorName
     *
     * @param tOperatorCode String
     * @return String
     */
    private String getOperatorName(String tOperatorCode) {
        String name = (new ExeSQL()).getOneValue(
                "select username from lduser where usercode='" + tOperatorCode +
                "'");
        if (name == null || name.equals("") || name.equals("null")) {
            return "";
        }
        return name;
    }

    /**
     * getPayMode
     *
     * @param tPayMode String
     * @return String
     */
    private String getPayMode(String tPayMode) {
        return StrTool.cTrim(tPayMode).equals("1") ? "现金" : "银行转帐";
    }

    private void getAgentInfo(LCContDB tLCContDB) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
        if (!tLAAgentDB.getInfo()) {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
        }
        tLAAgentSchema = tLAAgentDB.getSchema();
    }

    /**
     *
     * @return LCContDB
     * @throws Exception
     */
    private LCContDB getContInfo() throws Exception {
        LCContDB tLCContDB = new LCContDB();
        // 打印时传入的是主险投保单的投保单号
        tLCContDB.setProposalContNo(mLOPRTManagerSchema.getOtherNo());
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0) {
            throw new Exception("查询合同信息失败！");
        }
        if (tLCContSet.size() > 1) {
            throw new Exception("查询合同信息失败！");
        }
//        if (!tLCContDB.getInfo()) {
//            mErrors.copyAllErrors(tLCContDB.mErrors);
//            throw new Exception("在获取保单信息时出错！");
//        }
        tLCContDB.setSchema(tLCContSet.get(1).getSchema());
        return tLCContDB;
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private LOPRTManagerDB getPrintManager() throws Exception {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }
        //需要判断是否已经打印？！
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //get all message！
        return tLOPRTManagerDB;
    }

    private boolean saveData(VData mInputData) {

        //根据印刷号查询打印队列中的纪录
        //mLOPRTManagerSchema
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);

        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName("LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);
        FirstPayF1PBLS tFirstPayF1PBLS = new FirstPayF1PBLS();
        tFirstPayF1PBLS.submitData(mResult, mOperate);
        if (tFirstPayF1PBLS.mErrors.needDealError()) {
            mErrors.copyAllErrors(tFirstPayF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;

    }


    // private String getAgentName(String strAgentCode) throws Exception
//    {
//
//        tLAAgentDB.setAgentCode(strAgentCode);
//        if (!tLAAgentDB.getInfo())
//        {
//            mErrors.copyAllErrors(tLAAgentDB.mErrors);
//            throw new Exception("在取得LAAgent的数据时发生错误");
//        }
//        return tLAAgentDB.getName();
//    }

    private String getComName(String strComCode) throws Exception {
        String sql = "select letterservicename from ldcom where comcode='" +
                     strComCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String ManageName = tExeSQL.getOneValue(sql);
        return ManageName;
    }

    private String getBankName(String strBankCode) throws Exception {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strBankCode);
        tLDCodeDB.setCodeType("bank");
        if (tLDCodeDB.getInfo()) {
            return tLDCodeDB.getCodeName();
        } else {
            return null;
        }
    }

    private String getAppNo(String strContNo) throws Exception {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo()) {
            return tLCAppntDB.getAppntNo();
        } else {
            return null;
        }
    }

    private String getAddrNo(String strContNo) throws Exception {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo()) {
            return tLCAppntDB.getAddressNo();
        } else {
            return null;
        }
    }

    private String getAppAddr(String strContNo) throws Exception {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo()) {
            return tLCAddressDB.getPostalAddress();
        } else {
            return null;
        }
    }

    private String getAppZipcode(String strContNo) throws Exception {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo()) {
            return tLCAddressDB.getZipCode();
        } else {
            return null;
        }
    }

    public CErrors getErrors() {
        return null;
    }

}
