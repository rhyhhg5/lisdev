package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAAnnuityDetailBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author weili
 * @version 1.0
 */
public class LAAnnuityDetailUI {

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAAnnuityDetailUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAAnnuityDetailBL tLAAnnuityDetailBL = new LAAnnuityDetailBL();

        if (!tLAAnnuityDetailBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAAnnuityDetailBL.mErrors);

            return false;
        } else {
            this.mResult = tLAAnnuityDetailBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
