package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author shaoax
 * @version 1.0
 */
public class LAMediPremUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAMediPremUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

    	LAMediPremBL tLAMediPremBL = new LAMediPremBL();

        if (!tLAMediPremBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAMediPremBL.mErrors);

            return false;
        } else {
            this.mResult = tLAMediPremBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
