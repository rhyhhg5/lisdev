package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AntiMoneyLaundryBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //接收的数据
    private String mStartDate = "";

    private String mEndDate = "";

    private String mBankType = "";// 管理机构  comCode

    private TransferData mTransferData = new TransferData();

    //定义需要用到的数据
    private String mManageCom = "";//批次号

    public AntiMoneyLaundryBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        if (mBankType.equals("1")) {
            System.out.println("-----1---人民银行---");
            if (!getPeoPrintData()) {
                
                return false;
            }
        }
        else if (mBankType.equals("2"))
        {
            System.out.println("-----2---保监会---");
            if (!getPrintData()) {
                return false;
            }
        }
        else {

            return false;
        }
        
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mStartDate = (String) mTransferData.getValueByName("StartDate");
        mEndDate = (String) mTransferData.getValueByName("EndDate");
        mBankType = (String) mTransferData.getValueByName("BankType");
        mManageCom = (String) mTransferData.getValueByName("ManageCom");
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AntiMoneyLaundryBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    //查询出首期或者是续期的银行代收正确清单
    private boolean getPrintData()
    {

        //定义报表相关的类
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("AntiMoneyLaundry.vts", "printer");
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");
        texttag.add("mStartDate", mStartDate + "至" + mEndDate);
        ExeSQL tExeSQL = new ExeSQL();
        
        //客户身份识别
        String num[] = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
        String sum[] = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
        
        String Value0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%'  with ur");
        String BankValue0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
        num[2] = addStr(Value0,BankValue0);
        
        String Value2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%'  with ur");
        String BankValue2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        num[3] = addStr(Value2,BankValue2);
        
        String Value6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%' with ur");
        String OtherValue6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
        num[4] = addStr(Value6,OtherValue6);
        
        String Value8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1'  and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000  and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        String OtherValue8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        num[5] = addStr(Value8,OtherValue8);
        
        String Value1 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        num[7] = addStr(Value1,"");
        
        String ClaimValue1 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno <> '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur");
        num[10] = addStr(ClaimValue1,"");
        
        String Value7 = tExeSQL.getOneValue("select count(t.cou) from (select distinct endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        num[8] = addStr(Value7,"");
        
        String OtherValue7 = tExeSQL.getOneValue("select count(t.cou) from (select distinct otherno as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur");
        num[11] = addStr(OtherValue7, "");
        
        //String Value14 = tExeSQL.getOneValue("select count(1) from lcgrpcont lcg where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'   and managecom like '" + mManageCom + "%'  with ur");
        String OtherValue14 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lcgrpcont lcg where lcg.grpcontno=lja.grpcontno    and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AC','CM') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        //num[13] = Value14;
        num[14] = OtherValue14;
        num[13]=num[14];
        //String Value16_1 = tExeSQL.getOneValue("select count(1) from lccont lcg where  conttype='1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'   and managecom like '" + mManageCom + "%'  with ur");
        //String Value16_2 = tExeSQL.getOneValue("select sum(t.cou) from ( select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
        String Value16_3 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'  and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        String Value16_4 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'   and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        //num[15] = Value16_1;
        num[16] = addStr(Value16_3, Value16_4);
        num[15]=num[16];
        num[1] = addStr(num[2], num[4]);
        num[6] = addStr(num[7], num[8]);
        num[9] = addStr(num[10], num[11]);
        num[12] = addStr(num[13], num[15]);
        num[0] = addStr(addStr(num[1], num[6]), num[9]);
        
        if(mStartDate.length()>4 && mEndDate.length()>4){
            mStartDate = mStartDate.substring(0, 4) + "-01-01";
            mEndDate = mEndDate.substring(0, 4) + "-12-31";
        } else {
            mStartDate = "2000-1-1";
            mEndDate = "2000-1-1";
        }        
        
        //全年的数据
//        String SumValue0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%'  with ur");
//        String SumBankValue0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
//        sum[2] = addStr(SumValue0,SumBankValue0);
//        
//        String SumValue2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%'  with ur");
//        String SumBankValue2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
//        sum[3] = addStr(SumValue2,SumBankValue2);
//        
//        String SumValue6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%' with ur");
//        String SumOtherValue6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
//        sum[4] = addStr(SumValue6,SumOtherValue6);
//        
//        String SumValue8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1'  and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000  and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
//        String SumOtherValue8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
//        sum[5] = addStr(SumValue8,SumOtherValue8);
//        
//        String SumValue1 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) >= 10000) as t with ur");
//        sum[7] = addStr(SumValue1,"");
//        
//        String SumClaimValue1 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno <> '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur");
//        sum[10] = addStr(SumClaimValue1,"");
//        
//        String SumValue7 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) >= 10000) as t with ur");
//        sum[8] = addStr(SumValue7,"");
//        
//        String SumOtherValue7 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur");
//        sum[11] = addStr(SumOtherValue7, "");
//        
//        String SumValue14 = tExeSQL.getOneValue("select count(1) from lcgrpcont lcg where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%' and exists (select 1 from ljagetendorse lja where lja.grpcontno = lcg.grpcontno and feeoperationtype = 'AE' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) with ur");
//        String SumOtherValue14 = tExeSQL.getOneValue("select count(1) from lcgrpcont lcg where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' and exists (select 1 from ljagetendorse lja where lja.grpcontno = lcg.grpcontno and feeoperationtype = 'AE' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) with ur");
//        sum[13] = addStr(SumValue14,SumOtherValue14);
//        sum[14] = sum[13];
//
//        String SumValue16_1 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcappnt lcap where exists (select 1 from lccont lcc where lcc.contno = lcap.contno and lcc.appntno = lcap.appntno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.contno = Lcap.contno and lja.appntno = lcap.appntno and feeoperationtype = 'AE' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by appntno) as t with ur");
//        String SumValue16_2 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
//        String SumValue16_3 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcappnt lcap where exists (select 1 from lccont lcc where lcc.contno = lcap.contno and lcc.appntno = lcap.appntno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.contno = Lcap.contno and lja.appntno = lcap.appntno and feeoperationtype = 'AE' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by appntno) as t with ur");
//        String SumValue16_4 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
//        sum[15] = addStr(addStr(SumValue16_1,SumValue16_2),addStr(SumValue16_3,SumValue16_4));
//        sum[16] = sum[15];
//        
//        sum[1] = addStr(sum[2], sum[4]);
//        sum[6] = addStr(sum[7], sum[8]);
//        sum[9] = addStr(sum[10], sum[11]);
//        sum[12] = addStr(sum[13], sum[15]);
//        sum[0] = addStr(addStr(sum[1], sum[6]), sum[9]);
        
        
        texttag.add("Num1", num[0]);
        //承保
        texttag.add("Num2", num[1]);
        texttag.add("Num3",  num[2]);
        texttag.add("Num4",  num[3]);
        texttag.add("Num5",  num[4]);
        texttag.add("Num6",  num[5]);
        //退保
        texttag.add("Num7",  num[6]);
        texttag.add("Num8",  num[7]);
        texttag.add("Num9",  num[8]);
        //理赔
        texttag.add("Num10",  num[9]);
        texttag.add("Num11",  num[10]);
        texttag.add("Num12",  num[11]);
//      客户身份重新识别
        texttag.add("Num13", num[12]);
        texttag.add("Num14",  num[13]);
        texttag.add("Num15",  num[14]);
        texttag.add("Num16",  num[15]);
        texttag.add("Num17",  num[16]);
        
//      客户身份识别
       
        //texttag.add("Sum1", sum[0]);
        //承保
//        texttag.add("Sum2", sum[1]);
//        texttag.add("Sum3",  sum[2]);
//        texttag.add("Sum4",  sum[3]);
//        texttag.add("Sum5",  sum[4]);
//        texttag.add("Sum6",  sum[5]);
//        //退保
//        texttag.add("Sum7",  sum[6]);
//        texttag.add("Sum8",  sum[7]);
//        texttag.add("Sum9",  sum[8]);
//        //客户身份重新识别
//        texttag.add("Sum10",  sum[9]);
//        texttag.add("Sum11",  sum[10]);
//        texttag.add("Sum12",  sum[11]);
//        texttag.add("Sum13", sum[12]);
//        texttag.add("Sum14",  sum[13]);
//        texttag.add("Sum15",  sum[14]);
//        texttag.add("Sum16", sum[15]);
//        texttag.add("Sum17",  sum[16]);

        xmlexport.addDisplayControl("displayinfo");
        String[] b_col = new String[9];
        xmlexport.addListTable(alistTable, b_col);
        if (texttag.size() > 0) 
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("d:\\","AntiMoneyLaundryBL"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }
    
    private boolean getPeoPrintData()
    {

        //定义报表相关的类
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("AntiMoneyLaundryPeo.vts", "printer");
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");

        //识别客户
        //对公用户
        String value[] = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
        
        ExeSQL tExeSQL = new ExeSQL();
        
        //新客户 对公
        //总数
        String Value0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%'  with ur");
        String BankValue0 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
        value[0] = addStr(Value0,BankValue0);
        //其他
        String Value1 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String ClaimValue1 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno <> '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur");
        value[1] = addStr(Value1,ClaimValue1);
        //总数 通过第三方识别数
        String Value2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%'  with ur");
        String BankValue2 = tExeSQL.getOneValue("select count(1) from lcgrpcont where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        value[2] = addStr(Value2,BankValue2);
        //其他 通过第三方识别数
        String Value3 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String ClaimValue3 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno <> '00000000000000000000' and exists (select 1 from lcgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') and managecom like '" + mManageCom + "%' union all select 1 from lbgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') ) and managecom like '" + mManageCom + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur");
        value[3] = addStr(Value3,ClaimValue3);
        //收益人
        value[4] = "0";
        value[5] = "0";
        
        //新客户 对私
        //总数
        String Value6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%' with ur");
        String OtherValue6 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' with ur");
        value[6] = addStr(Value6,OtherValue6);
        //其他
        String Value7 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String OtherValue7 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000' and managecom like '" + mManageCom + "%' group by otherno having  sum(pay)  >= 10000 ) as t with ur");
        value[7] = addStr(Value7,OtherValue7);
        //总数 通过第三方识别数
        String Value8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1'  and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode in ('1','14','16') and prem >= 20000  and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        String OtherValue8 = tExeSQL.getOneValue("select count(1) from lccont where conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + mManageCom + "%' with ur");
        value[8] = addStr(Value8,OtherValue8);
        //其他 通过第三方识别数
        String Value9 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String OtherValue9 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000' and exists (select 1 from lccont where contno = lclaim.contno and salechnl in ('03','10') and managecom like '" + mManageCom + "%' union all select 1 from lbcont where contno = lclaim.contno and salechnl in ('03','10') ) and managecom like '" + mManageCom + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur");
        value[9] = addStr(Value9,OtherValue9);
        //居民 总数
        String Value10 = tExeSQL.getOneValue("select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace = 'ML' or lca.NativePlace is null) and lcc.conttype = '1' and lcc.cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and lcc.paymode in ('1','14','16') and lcc.prem >= 20000 and lcc.managecom like '" + mManageCom + "%' group by lca.contno) as t with ur");
        String OtherValue10 = tExeSQL.getOneValue("select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace = 'ML' or lca.NativePlace is null) and lcc.conttype = '1' and lcc.cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and lcc.paymode not in ('1','14','16') and lcc.prem >= 200000 and lcc.managecom like '" + mManageCom + "%' group by lca.contno) as t with ur");
        value[10] = addStr(Value10,OtherValue10);
        //居民 其他
        String Value11 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse lget where exists (select 1 from lcappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace = 'ML' or lca.NativePlace is null) union all  select 1 from lbappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace = 'ML' or lca.NativePlace is null))    and feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String OtherValue11 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000'   and managecom like '" + mManageCom + "%' and exists (select 1 from lcinsured lci where lci.contno = lclaim.contno and (lci.NativePlace = 'ML' or lci.NativePlace is null) union  select 1 from lbinsured lci where lci.contno = lclaim.contno and (lci.NativePlace = 'ML' or lci.NativePlace is null)) group by otherno having  sum(pay)  >= 10000) as t with ur");
        value[11] = addStr(Value11,OtherValue11);
        //非居民 总数
        String Value12 = tExeSQL.getOneValue("select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) and lcc.conttype = '1' and lcc.cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and lcc.paymode in ('1','14','16') and lcc.prem >= 20000 and lcc.managecom like '" + mManageCom + "%' group by lca.contno) as t with ur");
        String OtherValue12 = tExeSQL.getOneValue("select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) and lcc.conttype = '1' and lcc.cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and lcc.paymode not in ('1','14','16') and lcc.prem >= 200000 and lcc.managecom like '" + mManageCom + "%' group by lca.contno) as t with ur");
        value[12] = addStr(Value12,OtherValue12);
        //非居民 其他
        String Value13 = tExeSQL.getOneValue("select count(t.cou) from (select endorsementno as cou from ljagetendorse lget where exists (select 1 from lcappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) union all  select 1 from lbappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null))    and feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + mStartDate + "' and '" + mEndDate + "' and managecom like '" + mManageCom + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur");
        String OtherValue13 = tExeSQL.getOneValue("select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + mStartDate + "' and '" + mEndDate + "' and grpcontno = '00000000000000000000'   and managecom like '" + mManageCom + "%' and exists (select 1 from lcinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' and lci.NativePlace is not null) union  select 1 from lbinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' and lci.NativePlace is not null)) group by otherno having  sum(pay)  >= 10000) as t with ur");
        value[13] = addStr(Value13,OtherValue13);
        
        
        //重新识别客户
        //对公 总数
        String Value14 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lcgrpcont lcg where lcg.grpcontno=lja.grpcontno    and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AC','CM') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno)as t with ur");
        //String OtherValue14 = tExeSQL.getOneValue("select count(1) from lcgrpcont lcg where cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + mManageCom + "%' and exists (select 1 from ljagetendorse lja where lja.grpcontno = lcg.grpcontno and feeoperationtype = 'AE' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) with ur");
        //value[14] = addStr(Value14,OtherValue14);
        value[14] = Value14;
        //对公 涉及收益人
        value[15] = "0";
        //新客户
        //新客户总数        
        String Value16_1 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno  and conttype='1'    and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        String Value16_2 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'   and managecom like '" + mManageCom + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        //String Value16_3 = tExeSQL.getOneValue("select count(t.cou) from (select appntno as cou from lcappnt lcap where exists (select 1 from lccont lcc where lcc.contno = lcap.contno and lcc.appntno = lcap.appntno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.contno = Lcap.contno and lja.appntno = lcap.appntno and feeoperationtype in ('AE','CM','CC','PC') and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by appntno) as t with ur");
        //String Value16_4 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
        //value[16] = addStr(addStr(Value16_1,Value16_2),addStr(Value16_3,Value16_4));
        value[16] = addStr(Value16_1,Value16_2);
        //居民
        String Value17_1 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'    and managecom like '" + mManageCom + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace = 'ML' or NativePlace is null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        String Value17_2 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'   and managecom like '" + mManageCom + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace = 'ML' or NativePlace is null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        //String Value17_3 = tExeSQL.getOneValue("select count(t.cou) from (select appntno as cou from lcappnt lcap where (lcap.NativePlace = 'ML' or lcap.NativePlace is null ) and exists (select 1 from lccont lcc where lcc.contno = lcap.contno and lcc.appntno = lcap.appntno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.contno = lcap.contno and lja.appntno = lcap.appntno and feeoperationtype in ('AE','CM','CC','PC') and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by appntno) as t with ur");
        //String Value17_4 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where exists (select 1 from lcappnt where contno = lcc.contno and appntno = lcc.appntno and (NativePlace = 'ML' or NativePlace is null )) and  lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
        //value[17] = addStr(addStr(Value17_1,Value17_2),addStr(Value17_3,Value17_4));
        value[17] = addStr(Value17_1,Value17_2);
        //非居民
        String Value18_1 = tExeSQL.getOneValue("select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'    and managecom like '" + mManageCom + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace <> 'ML' and NativePlace is not null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        String Value18_2 = tExeSQL.getOneValue("select sum(t.num)from  (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1'    and managecom like '" + mManageCom + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace <> 'ML' and NativePlace is not null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + mStartDate + "' and '" + mEndDate + "' group by lja.edoracceptno) as t with ur");
        //String Value18_3 = tExeSQL.getOneValue("select count(t.cou) from (select appntno as cou from lcappnt lcap where (lcap.NativePlace <> 'ML' and lcap.NativePlace is not null ) and exists (select 1 from lccont lcc where lcc.contno = lcap.contno and lcc.appntno = lcap.appntno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.contno = lcap.contno and lja.appntno = lcap.appntno and feeoperationtype in ('AE','CM','CC','PC') and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by appntno) as t with ur");
        //String Value18_4 = tExeSQL.getOneValue("select sum(t.cou) from (select count(1) as cou from lcbnf lcb where exists (select 1 from lccont lcc where exists (select 1 from lcappnt where contno = lcc.contno and appntno = lcc.appntno and (NativePlace <> 'ML' and NativePlace is not null )) and  lcc.contno = lcb.contno and lcc.insuredno = lcb.insuredno and conttype = '1' and cvalidate between '" + mStartDate + "' and '" + mEndDate + "'  and paymode not in ('1','14','16') and prem >= 20000 and managecom like '" + mManageCom + "%') and exists (select 1 from ljagetendorse lja where lja.polno = lcb.polno and lja.insuredno = lcb.insuredno and feeoperationtype = 'BC' and makedate between '" + mStartDate + "' and '" + mEndDate + "' ) group by polno,insuredno) as t with ur");
        //value[18] = addStr(addStr(Value18_1,Value18_2),addStr(Value18_3,Value18_4));
        value[18] = addStr(Value18_1,Value18_2);
        
        
        texttag.add("mStartDate", mStartDate + "至" + mEndDate);
        texttag.add("NewSumNum", value[0]);
        texttag.add("SumNum", value[1]);
        texttag.add("NewOtherNum",  value[2]);
        texttag.add("OtherNum",  value[3]);
        texttag.add("NewNum",  value[4]);
        texttag.add("Num",  value[5]);
        //对私用户
        texttag.add("NewSumNum2",  value[6]);
        texttag.add("SumNum2",  value[7]);
        texttag.add("NewOtherNum2",  value[8]);
        texttag.add("OtherNum2",  value[9]);
        texttag.add("NewResNum",  value[10]);
        texttag.add("ResNum",  value[11]);
        texttag.add("NewNotResNum", value[12]);
        texttag.add("NotResNum",  value[13]);
        
        //非识别客户
        texttag.add("ImpSum",  value[14]);
        texttag.add("ImpNum",  value[15]);
        texttag.add("NewImpSum",  value[16]);
        texttag.add("NewImpResNum",  value[17]);
        texttag.add("NewImpNotResNum",  value[18]);

        xmlexport.addDisplayControl("displayinfo");
        String[] b_col = new String[1];
        xmlexport.addListTable(alistTable, b_col);
        if (texttag.size() > 0) 
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("d:\\","AntiMoneyLaundryBL"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }
    
    private String addStr(String addStr1, String addStr2){
        int add1 = 0;
        try{
            add1 = Integer.parseInt(addStr1);
        } catch(Exception ex) {
            add1 = 0;
        }
        int add2 = 0;
        try{
            add2 = Integer.parseInt(addStr2);
        } catch(Exception ex) {
            add2 = 0;
        }
        return add1 + add2 + "";
    }
    
    public static void main(String[] arr){
        System.out.println("2012-03-23".substring(0, 4) + "-01-01");
        
//        AntiMoneyLaundryBL tAntiMoneyLaundryBL = new AntiMoneyLaundryBL();
//        System.out.println(tAntiMoneyLaundryBL.addStr(null,"10"));
    }
}
