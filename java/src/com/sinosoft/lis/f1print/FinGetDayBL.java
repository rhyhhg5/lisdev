package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.io.File;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinGetDayBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String mOperate="";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FinGetDayBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate=cOperate;
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
         if (!getPrintData())
         {
            return false;
         }
         
        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FinGetDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String strArr[][] = null;
        double SumMoney = 0;
        int SumNum=0;
        String strSum="";
        if(this.mOperate.equals("PRINT")){
            
            msql ="select a.contno A,a.AppntName, "+
							"(select  name from labranchgroup where agentgroup=a.agentgroup), "+
							"(select  substr(branchattr,1,10) from labranchgroup where agentgroup=a.agentgroup) || '&' || substr(a.agentcode,1,10) , "+
							"(select name from laagent where agentcode=a.agentcode), "+
							"b.riskcode ,b.paymoney ," +
							"(select value(sum(prem),0) from lcpol where contno=a.contno and riskcode=b.riskcode)," +
							"(select value(sum(supplementaryprem),0) from lcpol where contno=a.contno and riskcode=b.riskcode),"+
							"(select codename from ldcode where codetype='payintv' and code=char(a.payintv)),"+
							"b.enteraccdate ,a.CValiDate, "+
							"case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' "+
							"when '4' then '银行转账' when '5' then '内部转帐' when '6' then '银行托收' "+
							"when '7' then '其他' when '8' then '银行转帐(银代险事后转账)' "+
							"when '9' then '现金缴纳(银代险)' when '0' then '自动银行代付' end , "+
							"case a.paymode when '4' then (select bankname from ldbank where bankcode=a.bankcode) "+
							"else '' end,'首期' from lccont a,ljtempfee b where   "+
							"b.otherno=a.contno and a.conttype='1' and b.tempfeetype in('1','11') "+
							"and a.ManageCom like'" + mGlobalInput.ManageCom +"%' "+
            	"and b.confmakedate>='"+mDay[0]+"' and b.confmakedate<='"+mDay[1]+"'  "+
            	"union all "+
            	"select a.grpcontno A,a.grpName, "+
							"(select  name from labranchgroup where agentgroup=a.agentgroup), "+
							"(select  substr(branchattr,1,10) from labranchgroup where agentgroup=a.agentgroup) || '&' || substr(a.agentcode,1,10) , "+
							"(select name from laagent where agentcode=a.agentcode), "+
							"b.riskcode ,b.paymoney ,b.paymoney,0," +
							"(select codename from ldcode where codetype='payintv' and code=char(a.payintv)),"+
							"b.enteraccdate ,a.CValiDate, "+
							"case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' "+
							"when '4' then '银行转账' when '5' then '内部转帐' when '6' then '银行托收' "+
							"when '7' then '其他' when '8' then '银行转帐(银代险事后转账)' "+
							"when '9' then '现金缴纳(银代险)' when '0' then '自动银行代付' end , "+
							"case a.paymode when '4' then (select bankname from ldbank where bankcode=a.bankcode) "+
							"else '' end,'首期' from lcgrpcont a,ljtempfee b where   "+
							"b.otherno=a.grpcontno  and b.tempfeetype in('1','11') "+
							"and a.ManageCom like'" + mGlobalInput.ManageCom +"%'  "+
            	"and b.confmakedate>='"+mDay[0]+"' and b.confmakedate<='"+mDay[1]+"'  "+
              "union all "+
            	"select a.contno A,a.AppntName, "+
							"(select  name from labranchgroup where agentgroup=a.agentgroup), "+
							"(select  substr(branchattr,1,10) from labranchgroup where agentgroup=a.agentgroup) || '&' || substr(a.agentcode,1,10) , "+
							"(select name from laagent where agentcode=a.agentcode), "+
							"b.riskcode ,b.paymoney ," +
							"(select value(sum(prem),0) from lcpol where contno=a.contno and riskcode=b.riskcode)," +
							"(select value(sum(supplementaryprem),0) from lcpol where contno=a.contno and riskcode=b.riskcode),"+
							"(select codename from ldcode where codetype='payintv' and code=char(a.payintv)),"+
							"b.enteraccdate ,a.CValiDate, "+
							"case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' "+
							"when '4' then '银行转账' when '5' then '内部转帐' when '6' then '银行托收' "+
							"when '7' then '其他' when '8' then '银行转帐(银代险事后转账)' "+
							"when '9' then '现金缴纳(银代险)' when '0' then '自动银行代付' end , "+
							"case a.paymode when '4' then (select bankname from ldbank where bankcode=a.bankcode) "+
							"else '' end,'续期' from lccont a,ljtempfee b,ljspayb c where b.tempfeeno=c.getnoticeno  "+
							"and c.otherno=a.contno and a.conttype='1' and b.tempfeetype='2' and c.othernotype='2' "+
							"and a.ManageCom like'" + mGlobalInput.ManageCom +"%' "+
            	"and b.confmakedate>='"+mDay[0]+"' and b.confmakedate<='"+mDay[1]+"' "+
            	"union all "+
            	"select a.grpcontno A,a.grpName, "+
							"(select  name from labranchgroup where agentgroup=a.agentgroup), "+
							"(select  substr(branchattr,1,10) from labranchgroup where agentgroup=a.agentgroup) || '&' || substr(a.agentcode,1,10) , "+
							"(select name from laagent where agentcode=a.agentcode), "+
							"b.riskcode ,b.paymoney ,b.paymoney ,0," +
							"(select codename from ldcode where codetype='payintv' and code=char(a.payintv)),"+
							"b.enteraccdate ,a.CValiDate, "+
							"case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' "+
							"when '4' then '银行转账' when '5' then '内部转帐' when '6' then '银行托收' "+
							"when '7' then '其他' when '8' then '银行转帐(银代险事后转账)' "+
							"when '9' then '现金缴纳(银代险)' when '0' then '自动银行代付' end , "+
							"case a.paymode when '4' then (select bankname from ldbank where bankcode=a.bankcode) "+
							"else '' end,'续期' from lcgrpcont a,ljtempfee b,ljspayb c where b.tempfeeno=c.getnoticeno  "+
							"and c.otherno=a.grpcontno  and b.tempfeetype='2' and c.othernotype='1' "+
							"and a.ManageCom like'" + mGlobalInput.ManageCom +"%' "+
            	"and b.confmakedate>='"+mDay[0]+"' and b.confmakedate<='"+mDay[1]+"' order by A  ";
            tSSRS = tExeSQL.execSQL(msql+"with ur ");
            SumNum=tSSRS.MaxRow+6;
            strArr=new String[SumNum][tSSRS.MaxCol];
            for (int i = 4; i < SumNum-2; i++)
            {
                for (int j = 1; j <= tSSRS.MaxCol; j++)
                {
                    if (j == 7 || j==9)
                    {
                        strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                        if(strArr[i][j - 1]!=null && !strArr[i][j - 1].trim().equals("") && !strArr[i][j - 1].equals("null")){
                            strSum = new DecimalFormat("0.00").format(Double.
                                    valueOf(strArr[i][j - 1]));
                            strArr[i][j - 1] = strSum;
                            SumMoney = SumMoney + Double.parseDouble(strArr[i][j - 1]);
                        }else{
                            strArr[i][j - 1]="0.00";
                        }
                        
                    }else{
                        strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                    }
                }
            }
        }else{
            msql =  "select b.contno,b.appntname ,(select name from labranchgroup where agentgroup=a.agentgroup) "+
                    ",(select substr(branchattr,1,10) from labranchgroup "+
                    "where agentgroup=a.agentgroup) || '&' || substr(a.agentcode,1,10) "+                      
                    ",(select name from laagent where agentcode=a.agentcode),riskcode,getmoney," +
                    "(select value(sum(-prem),0) from lcpol where polno=a.polno and contno=b.contno)," +
                    "(select value(sum(-supplementaryprem),0) from lcpol where polno=a.polno and contno=b.contno),"+
					"(select codename from ldcode where codetype='payintv' and code=char(b.payintv)),"+
                    "getconfirmdate   "+ 
                    ",case b.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票'"+
                    "when '4' then '银行转账' when '5' then '内部转帐' when '6' then '银行托收'"+
                    "when '7' then '其他' when '8' then '银行转帐(银代险事后转账)'"+
                    " when '9' then '现金缴纳(银代险)' when '0' then '自动银行代付' end     "+                      
                    ",case    b.paymode when '4' then (select bankname from ldbank where bankcode=b.bankcode) else '' end "+
                    "from LJAGetEndorse a,lbcont b   where a.contno=b.contno  "+
                    "and FeeOperationType = 'WT'  "+
                    "and a.ManageCom like'" + mGlobalInput.ManageCom +"%'"+
                    "and a.getconfirmdate>='"+mDay[0]+"' and a.getconfirmdate<='"+mDay[1]+"'";
            tSSRS = tExeSQL.execSQL(msql+" with ur");
            SumNum=tSSRS.MaxRow+6;
            strArr=new String[SumNum][tSSRS.MaxCol];
            for (int i = 4; i < SumNum-2; i++)
            {
                for (int j = 1; j <= tSSRS.MaxCol; j++)
                {
                    if (j == 7 || j==8 || j==9)
                    {
                        strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                        if(strArr[i][j - 1]!=null && !strArr[i][j - 1].trim().equals("") && !strArr[i][j - 1].equals("null")){
                            strSum = new DecimalFormat("0.00").format(Double.
                                    valueOf(strArr[i][j - 1]));
                            strArr[i][j - 1] = strSum;
                            SumMoney = SumMoney + Double.parseDouble(strArr[i][j - 1]);
                        }else{
                            strArr[i][j - 1]="0.00";
                        }
                        
                    }else{
                        strArr[i][j - 1] = tSSRS.GetText(i-3, j);
                    }
                }
            }
        }
        try {
            WriteToExcel t =null;
            String[] title =null;
            String tFileName="";
            if(this.mOperate.equals("PRINT")){
            	tFileName="BF"+mGlobalInput.ManageCom+mGlobalInput.Operator+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".xls";
                t = new WriteToExcel(tFileName);
                title=new String[15];
                title[0]="保单合同号码";
                title[1]="投保人";
                title[2]="业务部";
                title[3]="业务部系统代码";
                title[4]="销售人员";
                title[5]="险种代码";
                title[6]="保费收入";
                title[7]="基本保费";
                title[8]="追加保费";
                title[9]="缴费间隔";
                title[10]="到帐日期";
                title[11]="生效日期";
                title[12]="缴费方式";
                title[13]="代理银行";
                title[14]="收费类型";
                t.createExcelFile();
                String[] sheetName = {
                    "sheet1"};
                t.addSheet(sheetName);
                
                strArr[0][0]="统计日期：";
                strArr[0][1]=mDay[0];
                strArr[0][2]="至";
                strArr[0][3]=mDay[1];
                
                strArr[1][0]="统计机构：";
                strArr[1][1]=mGlobalInput.ManageCom;
                
                for(int i=0;i<title.length;i++){
                    strArr[3][i]=title[i];
                }
                strArr[SumNum-1][0]="合计：";
                strArr[SumNum-1][1]=String.valueOf(SumMoney);
                
                t.setData(0, strArr);
                String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
                File f=new File(tServerPath+"web/Generated/"+tFileName);
                f.delete();
                t.write(tServerPath+"web/Generated/");
                
                String tURL = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
                String resultURL=tURL+"web/Generated/"+tFileName;
                mResult.clear();
                mResult.add(0,resultURL);
            }else{
            	 tFileName="YYQTF"+mGlobalInput.ManageCom+mGlobalInput.Operator+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".xls";
                t = new WriteToExcel(tFileName);
                title=new String[13];
                title[0]="保单合同号码";
                title[1]="投保人";
                title[2]="业务部";
                title[3]="业务部系统代码";
                title[4]="销售人员";
                title[5]="险种代码";
                title[6]="保费收入";
                title[7]="基本保费";
                title[8]="追加保费";
                title[9]="缴费间隔";
                title[10]="确认日期";
                title[11]="缴费方式";
                title[12]="代理银行";
                t.createExcelFile();
                String[] sheetName = {
                    "sheet1"};
                t.addSheet(sheetName);
                
                strArr[0][0]="统计日期：";
                strArr[0][1]=mDay[0];
                strArr[0][2]="至";
                strArr[0][3]=mDay[1];
                
                strArr[1][0]="统计机构：";
                strArr[1][1]=mGlobalInput.ManageCom;
                
                for(int i=0;i<title.length;i++){
                    strArr[3][i]=title[i];
                }
                strArr[SumNum-1][0]="合计：";
                strArr[SumNum-1][1]=String.valueOf(SumMoney);
                
                t.setData(0, strArr);
                String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
                File f=new File(tServerPath+"web/Generated/"+tFileName);
                f.delete();
                t.write(tServerPath+"web/Generated/");
                
                String tURL = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
                String resultURL=tURL+"web/Generated/"+tFileName;
                mResult.clear();
                mResult.add(0,resultURL);
            }
            
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        return true;
    }

}