package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;

public class SurvFeePrtBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    private String mYear = "";
    private String mMonth = "";
    private String mBatchNo = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private TransferData mTransferData = new TransferData(); //理赔申诉错误表

    public SurvFeePrtBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
        mYear = (String) mTransferData.getValueByName("Year");
        mMonth = (String) mTransferData.getValueByName("Month");
        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mYear == null || mYear.equals("") || mYear.equals("null")) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mMonth == null || mMonth.equals("") || mMonth.equals("null")) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        boolean PEFlag = false; //打印理赔申请回执的判断标志
        boolean shortFlag = false; //打印备注信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //ListTable
        ListTable payListTable = new ListTable();
        payListTable.setName("PayInfo");
//        ListTable tEndListTable = new ListTable();
//        tEndListTable.setName("End");
        String[] pTitle = {"Actugetno","CaseNo","SurveyNo", "pInqFee", "pIndirectFee", "cInqFee","cIndirectFee"};
        String[] pstrCol;       
        String payCSql = "select X.actugetno,X.otherno,coalesce(sum(X.CZJ),0),coalesce(sum(X.CJJ),0),coalesce(sum(X.FZJ),0),coalesce(sum(X.FJJ),0),X.getnoticeno" 
        				+" from (select a.actugetno actugetno,b.otherno otherno,b.getnoticeno getnoticeno,"
        				+"(select sum(b.pay) from ljagetclaim where otherno=b.otherno and actugetno=a.actugetno and feefinatype='ZJ' and othernotype='C') CZJ,"
        				+"(select sum(b.pay) from ljagetclaim where otherno=b.otherno and actugetno=a.actugetno and feefinatype='JJ' and othernotype='C') CJJ,"
        				+"(select sum(b.pay) from ljagetclaim where otherno=b.otherno and actugetno=a.actugetno and feefinatype='ZJ' and othernotype='F') FZJ,"
        				+"(select sum(b.pay) from ljagetclaim where otherno=b.otherno and actugetno=a.actugetno and feefinatype='JJ' and othernotype='F') FJJ"
        				+" from ljaget a,ljagetclaim b "
        				+" where a.actugetno=b.actugetno and a.otherno='"+mBatchNo+"'"
        				+" group by b.otherno,b.getnoticeno,a.actugetno order by b.otherno) as X "
        				+" group by X.otherno,X.getnoticeno,X.actugetno with ur";       
        System.out.println(payCSql);
        SSRS sr = new SSRS();
        ExeSQL texesql = new ExeSQL();
        sr = texesql.execSQL(payCSql);
        if (sr.getMaxRow() > 0) 
        {
            for (int x = 1; x <= sr.getMaxRow(); x++) 
            {
                pstrCol = new String[10];
                pstrCol[0] = sr.GetText(x, 1);
                pstrCol[1] = sr.GetText(x, 2); 
                pstrCol[2] = "";
                pstrCol[3] = sr.GetText(x, 3); 
                pstrCol[4] = sr.GetText(x, 4); 
                pstrCol[5] = sr.GetText(x, 5); 
                pstrCol[6] = sr.GetText(x, 6); 
                if ((!sr.GetText(x, 3).equals("0")||!sr.GetText(x, 5).equals("0")) && sr.GetText(x, 7).length() > 17) 
                {
                    pstrCol[2] = sr.GetText(x, 7).substring(17, 18);
                }
                payListTable.add(pstrCol);
            }
        }
       
//        String[] tCTitle = {"ActuGetNo", "CActuGetNo"};
//        String[] tFTitle = {"FActuGetNo"};
//        String[] tEstrCol;
//        String tCaseNo = "";
//        String tActuSQL = "";
//        boolean tActuFlag;
//        tEstrCol = new String[2];
//        ListTable tCActuTable = new ListTable();
//        tCActuTable.setName("CActu");

//        tEstrCol[0] = "给付凭证号";
//        tEstrCol[1] = tCPayActuNo;
//        tCActuTable.add(tEstrCol);

//        for (int i = 0; i < payListTable.size(); i++) {
//            tEstrCol = new String[2];
//            tActuFlag = true;
//            tCaseNo = payListTable.getValue(0, i);
//            tActuSQL = "SELECT actugetno FROM ljagetclaim WHERE otherno='" +
//                       tCaseNo + "' AND feefinatype='ZJ' with ur";
//            ExeSQL texesql = new ExeSQL();
//            tCPayActuNo = texesql.getOneValue(tActuSQL);
//            System.out.println("Find:" + tCPayActuNo);
//
//            for (int j = 0; j < tCActuTable.size(); j++) {
//                if (tCActuTable.getValue(1, j).equals(tCPayActuNo) ||
//                    tCPayActuNo.equals("null") || tCPayActuNo.equals("")) {
//                    tActuFlag = false;
//                    System.out.println(tActuFlag);
//                    break;
//                }
//            }
//
//            if (tActuFlag) {
//                tEstrCol[0] = "给付凭证号";
//                tEstrCol[1] = tCPayActuNo;
//                tCActuTable.add(tEstrCol);
//                System.out.println("Insert:" + tCPayActuNo);
//            }
//        }
//        tOtherNo = "C" + mBatchNo;
//        tLJAGetDB.setOtherNoType("C");
//        tLJAGetDB.setOtherNo(tOtherNo);
//        tLJAGetSet = new LJAGetSet();
//        tLJAGetSet = tLJAGetDB.query();
//        for (int i = 1; i <= tLJAGetSet.size(); i++) {
//            tEstrCol = new String[2];
//            if (tLJAGetSet.get(i).getActuGetNo() != null) {
//                tEstrCol[0] = "给付凭证号";
//                tEstrCol[1] = tLJAGetSet.get(i).getActuGetNo();
//            } else {
//                tEstrCol[0] = "";
//                tEstrCol[1] = "";
//            }
//            tCActuTable.add(tEstrCol);
//        }

//        ListTable tFActuTable = new ListTable();
//        tFActuTable.setName("FActu");
//        tOtherNo = "F" + mBatchNo;
//        tLJAGetDB.setOtherNoType("F");
//        tLJAGetDB.setOtherNo(tOtherNo);
//        tLJAGetSet = new LJAGetSet();
//        tLJAGetSet = tLJAGetDB.query();
////        tFPayActuNo = "";
//        for (int i = 1; i <= tLJAGetSet.size(); i++) {
//            tEstrCol = new String[1];
//            if (tLJAGetSet.get(i).getActuGetNo() != null) {
//                tEstrCol[0] = tLJAGetSet.get(i).getActuGetNo();
//            } else {
//                tEstrCol[0] = "";
//            }
//            tFActuTable.add(tEstrCol);
//        } while (tFActuTable.size() < tCActuTable.size()) {
//            tEstrCol = new String[1];
//            tEstrCol[0] = "";
//            tFActuTable.add(tEstrCol);
//        } while (tCActuTable.size() < tFActuTable.size()) {
//            tEstrCol = new String[2];
//            tEstrCol[0] = "给付凭证号";
//            tEstrCol[1] = "";
//            tCActuTable.add(tEstrCol);
//        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("SurvFeePrint.vts", "print");
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +"月" +tSrtTool.getDay() + "日";

        //PayColPrtBL模版元素
//        texttag.add("BarCode1", tPayActuNo);
//        texttag.add("BarCodeParam1"
//                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        String aFeeDate = mYear + "年" + mMonth + "月";
        texttag.add("FeeDate", aFeeDate);
        texttag.add("SysDate", SysDate);
        texttag.add("BatchNo", mBatchNo);
        texttag.add("ComCode", mGlobalInput.ManageCom);
        //texttag.add("FActuGetNo", tFPayActuNo);
        //texttag.add("CActuGetNo", tCPayActuNo);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
          xmlexport.addListTable(payListTable,pTitle);
//        xmlexport.addListTable(tCActuTable, tCTitle);
//        xmlexport.addListTable(tFActuTable, tFTitle);
//        xmlexport.addListTable(tEndListTable);
        xmlexport.outputDocumentToFile("d:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

        SurvFeePrtBL tSurvFeePrtBL = new SurvFeePrtBL();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("Year", "2010");
        tTransferData.setNameAndValue("Month", "4");
        tTransferData.setNameAndValue("BatchNo", "861100001004023");
        VData tVData = new VData();
        tVData.addElement(tTransferData);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.Operator = "cm1102";
        tVData.addElement(tG);
        tSurvFeePrtBL.submitData(tVData, "PRINT");

    }

}
