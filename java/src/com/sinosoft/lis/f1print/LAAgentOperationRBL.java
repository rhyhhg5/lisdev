/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import com.sinosoft.lis.certify.SysOperatorNoticeBL;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAgentOperationRBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计级别
    private String mOperationBranch = "";
    //统计银代机构
    private String mBranchType = "";
    //统计业务员
    private String mAgentState = "";
    //统计保单状态
    private String mState = "";
    //统计险种
    private String mBranchType2 = "";
    //统计起期
    private String  TAgentState1= "";
    //统计止期
    private String  TAgentState2 = "";
    //交费年限
    private String  AgentState1 = "";
    //需要调用的模版
    private String mFlag = "";
    //转化交费方式
    private String AgentState2;
    //转换交费年限
    private String tName;
    //文件暂存路径
    private String mfilePathAndName;
    //业务员入司职级
    private String mAgentGrade;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAAgentOperationRBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("RBL start submitData");
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
        	System.out.println("RBL error PRINT");
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChnlPremReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
        	  System.out.println("RBL error getDataList");
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LABL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mOperationBranch = (String)cInputData.get(1);
     mBranchType = (String)cInputData.get(2);
     mBranchType2 = (String)cInputData.get(3);
     mAgentState = (String)cInputData.get(4);
     TAgentState1 = (String)cInputData.get(5);
     TAgentState2 = (String)cInputData.get(6);
     AgentState1 = (String)cInputData.get(7);
     AgentState2 = (String)cInputData.get(8);
     tName = (String)cInputData.get(9);
     mState = (String)cInputData.get(10);
     mfilePathAndName=(String)cInputData.get(12);
     mAgentGrade = (String)cInputData.get(13);
     

     System.out.println(mOperationBranch+"/"+mBranchType+"/"+mBranchType2+"/"
    		 +mAgentState+"/"+mfilePathAndName);


     
     return true;
 }




    private boolean getDataList()
  {    String TadSQL="";
  if((mStartDate!=null && !"".endsWith(mStartDate)) && (mStartDate!=null && !"".endsWith(mStartDate) ))
  {
	     TadSQL=" and a.employdate between '"+mStartDate+"' and '"+mEndDate+"' ";
	}else
	{
		TadSQL="";
	}
      //查询人员
    	System.out.println("报表打印的有问题？没走到这个类里？");
      String manageSQL = "SELECT a.groupagentcode,a.NAME,a.sex,a.BIRTHDAY,a.IDNO,(SELECT codename FROM ldcode WHERE codetype='nationality'  "
    	  +"AND  code=a.Nationality ),(SELECT codename FROM ldcode  WHERE codetype='nativeplacebak' AND code=a.NativePlace),"
    	  +"(SELECT codename FROM ldcode WHERE codetype='polityvisage' AND code=a.PolityVisage ), (SELECT codename FROM ldcode WHERE "          
    	  +"codetype='nativeplacebak' AND  code=a.RgtAddress  ), (SELECT  codename FROM ldcode WHERE codetype='degree' AND code=a.Degree ),"       
    	  +"a.graduateschool, a.SPECIALITY, a.homeaddress, a.zipcode,a.phone, a.mobile, a.email, a.OLDCOM, a.TRAINPERIODS, a.EmployDate,"
    	  +" b.agentgrade,  e.GRADENAME, b.IntroAgency, (SELECT d.NAME FROM laagent d WHERE  d.AGENTCODE = b.IntroAgency ), c.branchattr, c.name,"
    	  +" (select name from labranchgroup where agentgroup=substr(c.branchseries,1,12))," 
    	  +"(SELECT distinct getUniteCode(branchmanager) FROM labranchgroup f  WHERE substr(c.branchseries,27,12)=f.agentgroup AND   f.branchlevel='01' and  f.branchtype='1' AND    f.branchtype2 = '01' and f.endflag ='N'  ) , " 
    	  +"(SELECT distinct branchmanagername  FROM labranchgroup f  WHERE   substr(c.branchseries,27,12)=f.agentgroup AND  f.branchlevel='01' and   f.branchtype='1' AND   f.branchtype2 = '01' and f.endflag ='N'   ) , "
    	  +"(SELECT distinct getUniteCode(branchmanager) FROM   labranchgroup f   WHERE  substr(c.branchattr,1,12)=f.branchattr AND f.branchlevel='02'  and   f.branchtype='1'"
    	  +" AND  f.branchtype2 = '01' and f.endflag ='N' ),  (SELECT  distinct branchmanagername  FROM  labranchgroup f WHERE substr(c.branchattr,1,12)=f.branchattr AND f.branchlevel='02'"
    	  +" and  f.branchtype='1' AND  f.branchtype2 = '01' and f.endflag ='N'  ), (SELECT distinct getUniteCode(branchmanager) FROM  labranchgroup f  WHERE  substr(c.branchattr,1,10)=f.branchattr AND "
    	  +" f.branchlevel='03' and  f.branchtype='1' AND    f.branchtype2 = '01' and f.endflag ='N'  ),  (SELECT  distinct branchmanagername  FROM  labranchgroup f  WHERE"
    	  +" substr(c.branchattr,1,10)=f.branchattr AND f.branchlevel='03' and  f.branchtype='1' AND  f.branchtype2 = '01' and f.endflag ='N' ), (case  when a.agentstate>='03' then (select applydate from ladimission "
    	  +" where agentcode=a.agentcode order by applydate desc fetch first 1 rows only) else null end), a.outworkdate, (SELECT  codename  FROM LDCODE WHERE codetype"
    	  +" ='agentstate'  AND code=a.agentstate  ) ,value(a.noworkflag,'N'),a.traindate,a.managecom,(select name from ldcom where comcode=a.managecom),  "
    	  +" (select qualifno from LAQUALIFICATION where agentcode = a.agentcode order by makedate desc fetch first 1 rows only ),(select certifno from LACERTIFICATION  where agentcode = a.agentcode order by makedate desc fetch first 1 rows only )"
    	  +" FROM laagent a,  latree b, labranchgroup c, laagentgrade e WHERE a.agentcode=b.agentcode AND "
    	  +" b.agentgroup=c.agentgroup AND   e.gradecode = b.agentgrade AND       a.branchtype='1' AND       a.branchtype2='01' AND    a.managecom like"
    	  +" '"+mManageCom+"%'  AND (c.BRANCHATTR like '"+mOperationBranch+"%' or '"+mState+"'='1')   AND   (a.agentstate <'"+TAgentState1+"' or '"+AgentState1+"'='1')"
    	  +"  AND (a.agentstate >= '"+TAgentState2+"' or '"+AgentState2+"'='2')  ORDER BY   a.managecom,c.branchattr,a.agentcode ";
          
         
        System.out.println(manageSQL);
        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
        tCreateCSVFile.initFile(this.mfilePathAndName);
        String[][] tFirstDataList = new String[1][1];
        String[][] tTheardDataList = new String[1][1];
        String[][] tFourDataList = new String[1][42];
        tFirstDataList[0][0]="中国人民健康保险股份有限公司"+tName+"分公司业务员基础信息表";
        
       
        tTheardDataList[0][0]="编制部门:个人销售部 ";
        tFourDataList[0][0]="营销员代码 ";
        tFourDataList[0][1]="姓     名";
        tFourDataList[0][2]="性 别";
        tFourDataList[0][3]="出生日期";
        tFourDataList[0][4]="证件号码";
        tFourDataList[0][5]="民族";
        tFourDataList[0][6]="籍贯";
        tFourDataList[0][7]="政治面貌";
        tFourDataList[0][8]="户口所在地";
        tFourDataList[0][9]="学历";
        tFourDataList[0][10]="毕业院校";
        tFourDataList[0][11]="专业";
        tFourDataList[0][12]="家庭住址";
        tFourDataList[0][13]="邮政编码";
        tFourDataList[0][14]="电话";
        tFourDataList[0][15]="手机";
        tFourDataList[0][16]="电子邮件";
        tFourDataList[0][17]="原工作单位";
        tFourDataList[0][18]="培训期数";
        tFourDataList[0][19]="入司时间";
        tFourDataList[0][20]="营销员职级";
        tFourDataList[0][21]="职级名称";
        tFourDataList[0][22]="推荐人代码";
        tFourDataList[0][23]="推荐人姓名";
        tFourDataList[0][24]="销售单位";
        tFourDataList[0][25]="销售单位名称";
        tFourDataList[0][26]="营销部名称";
        tFourDataList[0][27]="处经理代码";
        tFourDataList[0][28]="处经理姓名";
        tFourDataList[0][29]="区经理代码";
        tFourDataList[0][30]="区经理姓名";
        tFourDataList[0][31]="部经理代码";
        tFourDataList[0][32]="部经理姓名";
        tFourDataList[0][33]="离职登记日期";
        tFourDataList[0][34]="离职确认日期";
        tFourDataList[0][35]="代理人状态";
        tFourDataList[0][36]="筹备标记";
        tFourDataList[0][37]="筹备开始日期";
        tFourDataList[0][38]="管理机构编码";
        tFourDataList[0][39]="管理机构名称";
        tFourDataList[0][40]="资格证编码";
        tFourDataList[0][41]="展业证编码";
        
                          
                          

        tCreateCSVFile.doWrite(tFirstDataList,0);
        tCreateCSVFile.doWrite(tTheardDataList,0);
        tCreateCSVFile.doWrite(tFourDataList,41);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(manageSQL);
        try{
        	
               
                String[][] tShowDataList = new String[tSSRS.MaxRow][42];
                int linenum=tSSRS.MaxRow;
                for(int i=1;i<=linenum;i++)
                {
                   if (!queryOneDataList(tSSRS,i,tShowDataList[i-1])){
                	  
                	   return false;
                   }
                }
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
                tCreateCSVFile.doWrite(tShowDataList,41);
        	
        }catch(Exception exc){
        	exc.printStackTrace();
        	return false;
        }
        String[][] tlast1DataList = new String[1][1];
        tlast1DataList[0][0]="总经理：                       复核 ：                       制表：";
        tCreateCSVFile.doWrite(tlast1DataList,0);
        tCreateCSVFile.doClose();
        return true;
 }

 /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 1);
          pmOneDataList[1] =tSSRS.GetText(i, 2);
          pmOneDataList[2]=tSSRS.GetText(i, 3);
          pmOneDataList[3]=tSSRS.GetText(i, 4);
          pmOneDataList[4] =tSSRS.GetText(i, 5)+" 	";
          pmOneDataList[5] = tSSRS.GetText(i, 6);
          pmOneDataList[6] = tSSRS.GetText(i, 7);
          pmOneDataList[7] = tSSRS.GetText(i, 8);
          pmOneDataList[8] = tSSRS.GetText(i, 9);
          pmOneDataList[9] =tSSRS.GetText(i, 10);
          pmOneDataList[10] = tSSRS.GetText(i, 11);
          pmOneDataList[11]  = tSSRS.GetText(i, 12);
          pmOneDataList[12]  = tSSRS.GetText(i, 13);
          pmOneDataList[13]  = tSSRS.GetText(i, 14);
          pmOneDataList[14]  = tSSRS.GetText(i, 15);
          pmOneDataList[15]  =tSSRS.GetText(i, 16);
          pmOneDataList[16]  = tSSRS.GetText(i, 17);
          pmOneDataList[17]  = tSSRS.GetText(i, 18);
          pmOneDataList[18]  =tSSRS.GetText(i, 19);
          pmOneDataList[19]  =tSSRS.GetText(i, 20);
          pmOneDataList[20]  = tSSRS.GetText(i, 21);
          pmOneDataList[21]  =tSSRS.GetText(i, 22);
          pmOneDataList[22]  =tSSRS.GetText(i, 23);
          pmOneDataList[23]  =tSSRS.GetText(i, 24);
          pmOneDataList[24]  =tSSRS.GetText(i, 25)+" 	";
          pmOneDataList[25]  =tSSRS.GetText(i, 26);
          pmOneDataList[26]  =tSSRS.GetText(i, 27);
          pmOneDataList[27]  =tSSRS.GetText(i, 28);
          pmOneDataList[28]  =tSSRS.GetText(i, 29);
          pmOneDataList[29]  =tSSRS.GetText(i, 30);
          pmOneDataList[30]  =tSSRS.GetText(i, 31);
          pmOneDataList[31]  =tSSRS.GetText(i, 32);
          pmOneDataList[32]  =tSSRS.GetText(i, 33);
          pmOneDataList[33]  =tSSRS.GetText(i, 34);
          pmOneDataList[34]  =tSSRS.GetText(i, 35);
          pmOneDataList[35]  =tSSRS.GetText(i, 36);
          pmOneDataList[36]  =tSSRS.GetText(i, 37);
          pmOneDataList[37]  =tSSRS.GetText(i, 38);
          pmOneDataList[38]  =tSSRS.GetText(i, 39); 
          pmOneDataList[39]  =tSSRS.GetText(i, 40);
          pmOneDataList[40]  =tSSRS.GetText(i, 41)+" 	";
          pmOneDataList[41]  =tSSRS.GetText(i, 42)+" 	";

      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
 
 
    public VData getResult()
    {
        return mResult;
    }
}
