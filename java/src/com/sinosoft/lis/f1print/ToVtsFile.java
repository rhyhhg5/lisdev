package com.sinosoft.lis.f1print;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ToVtsFile {
	private String mManageCom = "";
	private ListTable mListTable = new ListTable();
	private VData mInputData = new VData();
	public CErrors mErrors = new CErrors();
	public boolean submitData(VData cInputData, String cOperate) {
		mInputData = (VData) cInputData;
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!getTile()) {
			return false;
		}
		return true;
	}

	private boolean getTile() {
		TextTag tTextTag = new TextTag();
		XmlExport mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLErr.vts", "printer");
		String sql1 = "select codename from ldcode1 where codetype='llimportfeeitem' and code = '"
				+ mManageCom + "' order by code1";
		ExeSQL texesql = new ExeSQL();
		System.out.println(sql1);
		SSRS tss = texesql.execSQL(sql1);
		int len = tss.getMaxRow();
		for (int i = 1; i <= len; i++) {
			tTextTag.add("title" + (i), tss.GetText(i, 1));
		}
		tTextTag.add("title" + (tss.getMaxRow() + 1), "出错原因");
		tTextTag.add("title" + (tss.getMaxRow() + 2), "$/");
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "", "", "", "", "", "", "", "", "", "",
				"","","", "", "", "", "", "", "", "", "", "", "", "","","","" };
		mListTable.setName("FAULT");
		String ListInfo[] = new String[len+3];
		for (int i = 0; i <= len+1; i++) {
			if(i!=len+1){
			ListInfo[i]="$*/FAULT/ROW/COL"+(i+1);
			}else{
				ListInfo[i]="$/";
			}

		}
		mListTable.add(ListInfo);
		String ListInfo1[] = new String[len+3];
		for (int i = 0; i <= len+2; i++) {
			if(i!=len+1){
			ListInfo1[i]="$/";
			}

		}
		mListTable.add(ListInfo1);
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mInputData.clear();
		mInputData.addElement(mXmlExport);

		return true;
	}

	private boolean getInputData(VData cInputData) {
		TransferData mTransferData = new TransferData();
		VData mResult = new VData();
		mTransferData = ((TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0));
		mManageCom = (String) mTransferData.getValueByName("tManageCom");
		System.out.println(mManageCom);
		return true;

	}

	public VData getResult() {
		return mInputData;
	}
	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLSurveyBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}
}
