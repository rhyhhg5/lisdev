package com.sinosoft.lis.f1print;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLUserControlBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mFileNameB = "";

	private String mMakeDate = "";

	private String mContType = "";
	
	private String mStartDate = "";
	
	private String mEndDate = "";
	
	private String mUserCode = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				this.bulidErrorB("LLPrintSave-->submitData", "数据不完整");
				return false;
			}
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLUserControlBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			mOperator = (String)mTransferData.getValueByName("tOperator");
			mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
			mContType = (String) mTransferData.getValueByName("tContType");
			mStartDate = (String) mTransferData.getValueByName("tStartDate");
			mEndDate = (String) mTransferData.getValueByName("tEndDate");
			mUserCode = (String) mTransferData.getValueByName("UserCode");
			this.mManageCom = (String) mTransferData
					.getValueByName("tManageCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
						
			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLUserControl.vts", "printer");

		String tMakeDate = PubFun.getCurrentDate();
		System.out.print("dayin252");

		mMakeDate = tMakeDate;
		System.out.println("1212121" + tMakeDate);
		String[] title = { "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "" ,"" ,""};//理赔二核
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private boolean getDataList() {

		SSRS tSSRS = new SSRS();
		String tUserSQL = "";
		if (!"".equals(mUserCode)&&mUserCode!=null) {
			tUserSQL = " and usercode='"+mUserCode+"'";
		}
		String sql = "select usercode,username from llclaimuser where StateFlag='1' "
			+tUserSQL+" order by usercode with ur";

		System.out.println("查询用户"	+ sql);
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);

		String userCode = "";
		long sumCaseNum[] = new long[14];
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String ListInfo[] = new String[16];
			userCode = tSSRS.GetText(i, 1);
			ListInfo[0] = userCode;
			ListInfo[1] = tSSRS.GetText(i, 2);
			System.out.println("用户编码：" + ListInfo[0]);
			String tPartSQL = "";
			if ("1".equals(mContType)) {
				tPartSQL = " and exists (select 1 from llregister where rgtno=a.rgtno and (applyertype='0' or applyertype is null)) ";
			} else if ("2".equals(mContType)) {
				tPartSQL = " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5') ";
			} else if ("3".equals(mContType)) {
				tPartSQL = " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='1') ";
			} else if("4".equals(mContType)) {
				tPartSQL = " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='2') ";
			} 
			
			String tCaseSQL = " select a.handler, "
					+ " sum(case when a.rgtstate='01' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='02' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='03' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='04' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='06' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='07' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='08' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='14' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='13' then 1 else 0 end), "
                    + " sum(case when a.rgtstate='16' then 1 else 0 end) "//二核报表改动
					+ " from llcase a " + " where a.mngcom like '" + mManageCom
					+ "%' and a.rgtdate between '" + mStartDate + "' and '"
					+ mEndDate + "' and a.handler='" + userCode
					+ "' "
					+ tPartSQL
					+ " group by a.handler with ur";
			System.out.println(tCaseSQL);
			SSRS tCaseSSRS = tExeSQL.execSQL(tCaseSQL);
			if (tCaseSSRS.getMaxRow() > 0) {
				//受理
				ListInfo[2] = tCaseSSRS.GetText(1, 2);
				//扫描
				ListInfo[3] = tCaseSSRS.GetText(1, 3);
				//检录
				ListInfo[4] = tCaseSSRS.GetText(1, 4);
				//理算
				ListInfo[5] = tCaseSSRS.GetText(1, 5);
				//审定
				ListInfo[7] = tCaseSSRS.GetText(1, 6);
				//调查
				ListInfo[8] = tCaseSSRS.GetText(1, 7);
				//查讫
				ListInfo[9] = tCaseSSRS.GetText(1, 8);
				//撤件
				ListInfo[11] = tCaseSSRS.GetText(1, 9);
				//延迟
				ListInfo[12] = tCaseSSRS.GetText(1, 10);
				//理赔二核
                ListInfo[15] = tCaseSSRS.GetText(1, 11);
			} else {
				ListInfo[2] = "0";
				ListInfo[3] = "0";
				ListInfo[4] = "0";
				ListInfo[5] = "0";
				ListInfo[7] = "0";
				ListInfo[8] = "0";
				ListInfo[9] = "0";
				ListInfo[11] = "0";
				ListInfo[12] = "0";
                ListInfo[15] = "0";
			}
			
			String tUWMainSQL = " select b.appclmuwer, "
					+ " sum(case when a.rgtstate='05' then 1 else 0 end), "
					+ " sum(case when a.rgtstate='10' then 1 else 0 end) "
					+ " from llcase a,llclaimuwmain b "
					+ " where a.caseno=b.caseno " + " and a.rgtdate between '"
					+ mStartDate + "' and '" + mEndDate
					+ "' and a.mngcom like '" + mManageCom
					+ "%' and b.appclmuwer='" + userCode
					+ "' group by b.appclmuwer with ur";
			SSRS tUWSSRS = tExeSQL.execSQL(tUWMainSQL);
			System.out.println(tUWMainSQL);
			if (tUWSSRS.getMaxRow() > 0) {
				//审批
				ListInfo[6] = tUWSSRS.GetText(1, 2);
				//抽检
				ListInfo[10] = tUWSSRS.GetText(1, 3);
			} else {
				ListInfo[6] = "0";
				ListInfo[10] = "0";
			}
			
			//回退
			String tBackSQL = " select count(a.caseno) "
					+ " from llcase a "
					+ " where a.rgtdate between '"
					+ mStartDate
					+ "' and '"
					+ mEndDate
					+ "' and a.mngcom like '"
					+ mManageCom
					+ "%' and a.rgtstate not in ('09','11','12','14','16') "//理赔二核不可案件回退
					+ " and exists (select 1 from llcaseback where caseno=a.caseno and backtype is null and operator='"
					+ userCode + "' ) "
					+ tPartSQL
					+ " with ur";
			System.out.println(tBackSQL);
			String tBackNum = tExeSQL.getOneValue(tBackSQL);
			if (tBackNum != null && !"".equals(tBackNum) && !"null".equals(tBackNum)) {
				ListInfo[13] = tBackNum;
			} else {
				ListInfo[13] = "0";
			}
			
			ListInfo[14] = Long.toString(Long.parseLong(ListInfo[2])+Long.parseLong(ListInfo[3])+Long.parseLong(ListInfo[4])
			+Long.parseLong(ListInfo[5])+Long.parseLong(ListInfo[6])+Long.parseLong(ListInfo[7])+Long.parseLong(ListInfo[8])
			+Long.parseLong(ListInfo[9])+Long.parseLong(ListInfo[10])+Long.parseLong(ListInfo[12])+Long.parseLong(ListInfo[15]));//理赔二核
			sumCaseNum[0] = sumCaseNum[0]+Long.parseLong(ListInfo[2]);
			sumCaseNum[1] = sumCaseNum[1]+Long.parseLong(ListInfo[3]);
			sumCaseNum[2] = sumCaseNum[2]+Long.parseLong(ListInfo[4]);
			sumCaseNum[3] = sumCaseNum[3]+Long.parseLong(ListInfo[5]);
			sumCaseNum[4] = sumCaseNum[4]+Long.parseLong(ListInfo[6]);
			sumCaseNum[5] = sumCaseNum[5]+Long.parseLong(ListInfo[7]);
			sumCaseNum[6] = sumCaseNum[6]+Long.parseLong(ListInfo[8]);
			sumCaseNum[7] = sumCaseNum[7]+Long.parseLong(ListInfo[9]);
			sumCaseNum[8] = sumCaseNum[8]+Long.parseLong(ListInfo[10]);
			sumCaseNum[9] = sumCaseNum[9]+Long.parseLong(ListInfo[11]);
			sumCaseNum[10] = sumCaseNum[10]+Long.parseLong(ListInfo[12]);
			sumCaseNum[11] = sumCaseNum[11]+Long.parseLong(ListInfo[13]);
			sumCaseNum[12] = sumCaseNum[12]+Long.parseLong(ListInfo[14]);
            sumCaseNum[13] = sumCaseNum[13]+Long.parseLong(ListInfo[15]);//理赔二核
			mListTable.add(ListInfo);
		}
		TextTag tTextTag = new TextTag();
		for(int i=0;i<sumCaseNum.length;i++){
			tTextTag.add("RgtState"+(i+1),sumCaseNum[i] );
			mXmlExport.addTextTag(tTextTag);
		}

		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql);
		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {

	}

}
