/*
 * <p>ClassName: OLAAccountsUI </p>
 * <p>Description: OLAAccountsUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAChannelReportUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LAAccountsSchema mLAAccountsSchema = new LAAccountsSchema();
    public LAChannelReportUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        //得到外部传入的数据,将数据备份到本类中

       mInputData=(VData)cInputData.clone() ;
        //进行业务处理
        if (!dealData())
        {
            return false;
        }


        LAChannelReportBL tLAChannelReportBL = new LAChannelReportBL();

        //System.out.println("Start OLAAccounts UI Submit...");
        tLAChannelReportBL.submitData(mInputData, "PRINT");
        //System.out.println("End OLAAccounts UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLAChannelReportBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAChannelReportBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLAAccountsUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }        
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }



    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mLAAccountsSchema.setSchema((LAAccountsSchema) cInputData.
                                         getObjectByObjectName(
                "LAAccountsSchema", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
