package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLClaimESReportBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLClaimerESReportBL {
    public LLClaimerESReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mStartDate = "";

    private String mEndDate = "";

    private String mManageCom = "";

    private String mManageComName = "";

    private String mCaseType = "";

    private String mCaseTypeName = "";

    private String mOperator = "";

    private String[][] mShowDataList = null;

    private XmlExport mXmlExport = null;

    private String[] mDataList = null;

    private ListTable mListTable = new ListTable();

    private String currentDate = PubFun.getCurrentDate();

    private String mFileNameB = "";

    private String mMakeDate = "";

    private TransferData mTransferData = new TransferData();


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        } else {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("tFileNameB", mFileNameB);
            tTransferData.setNameAndValue("tMakeDate", mMakeDate);
            tTransferData.setNameAndValue("tOperator", mOperator);
            LLPrintSave tLLPrintSave = new LLPrintSave();
            VData tVData = new VData();
            tVData.addElement(tTransferData);
            if (!tLLPrintSave.submitData(tVData, "")) {
                return false;
            }
        }

        return true;
    }

    private boolean getDataList() {
        SSRS tSSRS = new SSRS();
        String sql =
                "select usercode,username from llclaimuser where comcode like '"
                + mManageCom + "%' and StateFlag='1' order by usercode with ur";

        ExeSQL tExeSQL = new ExeSQL();
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS.getMaxRow() < 1) {
            buildError("getDataList", "该机构下无理赔用户");
            return false;
        }

        String tCaseTypeSQL = "";

        if ("1".equals(mCaseType)) {
            tCaseTypeSQL =
                    " and not exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5') ";
        } else if ("2".equals(mCaseType)) {
            tCaseTypeSQL =
                    " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5') ";
        }

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String tUserCode = "";
            String Info[] = new String[54];
            tUserCode = tSSRS.GetText(i, 1);
            Info[0] = tUserCode;
            Info[1] = tSSRS.GetText(i, 2);
            if (tUserCode == null || "".equals(tUserCode)) {
                buildError("getDataList", "用户编码空");
                return false;
            }
            String t0ESSQL = "select count(1) from llcase a "
                             + " where a.endcasedate between '" + mStartDate +
                             "' and '" + mEndDate + "' "
                             + " and a.rgtstate in ('09','11','12')"
                             + " and not exists (select 1 from es_doc_main b where b.doccode=a.caseno and b.busstype='LP' and subtype='LP01') "
                             + tCaseTypeSQL
                             + " and a.handler = '" + tUserCode + "' with ur";

            String t0ESCount = tExeSQL.getOneValue(t0ESSQL);

            Info[2] = t0ESCount;

            String tClaimESSQL = "select "
                                 +
                                 " coalesce(sum((case when b.numpages = 1 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 2 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 3 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 4 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 5 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 6 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 7 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 8 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 9 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 10 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 11 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 12 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 13 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 14 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 15 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 16 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 17 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 18 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 19 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 20 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 21 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 22 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 23 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 24 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 25 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 26 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 27 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 28 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 29 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 30 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 31 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 32 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 33 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 34 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 35 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 36 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 37 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 38 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 39 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 40 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 41 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 42 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 43 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 44 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 45 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 46 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 47 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 48 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 49 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages = 50 then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when b.numpages >50 then 1 else 0 end)),0)"
                                 + " from llcase a,es_doc_main b "
                                 + " where a.caseno=b.doccode "
                                 + " and a.endcasedate between '" + mStartDate +
                                 "' and '" + mEndDate + "'"
                                 + " and a.rgtstate in ('09','11','12') "
                                 + " and b.busstype='LP' and subtype='LP01' "
                                 + tCaseTypeSQL
                                 + " and a.handler = '" + tUserCode +
                                 "' with ur";
            System.out.println(tClaimESSQL);
            SSRS tESSSRS = tExeSQL.execSQL(tClaimESSQL);
            if (tESSSRS.getMaxRow() <= 0) {
                for (int j = 3; j <= 53; j++) {
                    Info[j] = "0";
                }
            } else {

                for (int m = 3; m <= 53; m++) {
                    Info[m] = tESSSRS.GetText(1, m - 2);
                }
            }

            mListTable.add(Info);
        }
        return true;
    }

    /**
     * 进行数据查询
     *
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LLClaimerESReport.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("CaseTypeName", mCaseTypeName);
        tTextTag.add("Operator", mOperator);
        tTextTag.add("MakeDate", currentDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", "", "",
                         "", "", "", "", "", "", "", "", "", "",
                         "", "", "", "", "", "", "", "", "", "",
                         "", "", "", "", "", "", "", "", "", "",
                         "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
        if (!getDataList()) {
            return false;
        }

        mListTable.setName("CLAIM");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     *
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        // 全局变量
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        mManageComName = (String) pmInputData.get(3);
        mCaseType = (String) pmInputData.get(4);

        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
                "TransferData", 0);
        mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mMakeDate = currentDate;
        mOperator = mGlobalInput.Operator;

        if (mStartDate == null || "".equals(mStartDate)) {
            buildError("getInputData", "统计起期不能为空");
            return false;
        }

        if (mEndDate == null || "".equals(mEndDate)) {
            buildError("getInputData", "统计止期不能为空");
            return false;
        }

        if (mManageCom == null || "".equals(mManageCom)) {
            buildError("getInputData", "统计止期不能为空");
            return false;
        }

        if ("1".equals(mCaseType)) {
            mCaseTypeName = "普通案件";
        } else if ("2".equals(mCaseType)) {
            mCaseTypeName = "批量处理案件";
        } else {
            mCaseTypeName = "全部";
        }

        return true;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLClaimerESReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {

        VData tVData = new VData();
        LLClaimerESReportBL tClientRegisterBL = new LLClaimerESReportBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "lptest";
        String fileNameB = "lptest_0.vts";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("tFileNameB", fileNameB);
        tVData.addElement("2009-1-1");
        tVData.addElement("2009-5-31");
        tVData.addElement("86");
        tVData.addElement("lptest");
        tVData.addElement(ReportPubFun.getMngName("86"));
        tVData.addElement("I");
        tVData.addElement("个险");
        tVData.addElement(tTransferData);

        tVData.add(mGlobalInput);
        tClientRegisterBL.submitData(tVData, "");
        tVData.clear();
        tVData = tClientRegisterBL.getResult();
    }
}
