/**
 * 
 */
package com.sinosoft.lis.f1print;

import java.io.*;

import com.sinosoft.utility.CErrors;

/**
 * @author maning
 * 
 */
public class CSVFileWrite {

	/**
	 * 创建文件名和路径
	 * 
	 * @param aFileName
	 * @param aFilePath
	 */
	public CSVFileWrite(String aFilePath, String aFileName) {
		mFileName = aFileName;
		mFilePath = aFilePath;
	}

	public CErrors mErrors = new CErrors();

	private String mFileName = null;

	private String mFilePath = null;

	private String mTitle[][] = null;

	private String mContentType[] = null;

	private String mContent[][] = null;

	private long mCount = 0;

	private File mCSVFile = null;

	private BufferedWriter mBufferedWriter = null;

	/**
	 * 添加标题行
	 * 
	 * @param aTitle
	 * @return
	 */
	public boolean addTitle(String aTitle[][], String[] aContentType) {
		mTitle = aTitle;
		mContentType = aContentType;
		mCount = 0;
		return true;
	}

	/**
	 * 添加内容
	 * 
	 * @param aContent
	 * @return
	 */
	public boolean addContent(String[][] aContent) {
		mContent = aContent;
		mCount++;
		return true;
	}

	/**
	 * 输出文件
	 * 
	 * @return
	 */
	public boolean writeFile() {
		if (mTitle == null) {
			System.out.println("未获取标题行");
			return false;
		}

		if (mContent == null) {
			System.out.println("未获取内容");
			return false;
		}

		if (mCount == 1) {
			if (!creatFile()) {
				return false;
			}
		}

		if (!writeContent()) {
			return false;
		}

		return true;
	}

	/**
	 * 生成文件、添加标题
	 * 
	 * @return
	 */
	private boolean creatFile() {
		mCSVFile = new File(mFilePath + mFileName + ".csv");
		try {
			mBufferedWriter = new BufferedWriter(new FileWriter(mCSVFile));
			for (int i = 0; i < mTitle.length; i++) {
				String tTitleInfo[] = null;
				tTitleInfo = mTitle[i];
				StringBuffer tTitle = new StringBuffer();
				for (int j = 0; j < tTitleInfo.length; j++) {
					tTitle.append("\"\t");
					tTitle.append(checkKey(tTitleInfo[j]));
					tTitle.append("\",");
				}
				mBufferedWriter.write(tTitle.toString());
				mBufferedWriter.newLine();
			}

		} catch (Exception ex) {
			try {
				mBufferedWriter.close();
			} catch (Exception IOEX) {
				IOEX.printStackTrace();
			}
			System.out.println("写文件失败");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @return
	 */
	private boolean writeContent() {
		try {
			for (int i = 0; i < mContent.length; i++) {
				StringBuffer tContent = new StringBuffer();
				String tContentInfo[] = null;
				tContentInfo = mContent[i];
				if (tContentInfo.length != mContentType.length) {
					mErrors.addOneError("报表单元数量与类型数量不匹配");
					return false;
				}
				for (int j = 0; j < tContentInfo.length; j++) {
					if ("Number".equals(mContentType[j])) {
						tContent.append("\"");
					} else {
						tContent.append("\"\t");
					}
					tContent.append(checkKey(tContentInfo[j]));
					tContent.append("\",");
				}
				mBufferedWriter.write(tContent.toString());
				mBufferedWriter.flush();
				mBufferedWriter.newLine();

			}
		} catch (Exception ex) {
			try {
				mBufferedWriter.close();
			} catch (Exception IOEX) {
				IOEX.printStackTrace();
			}
			System.out.println("写文件失败");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean closeFile() {
		try {
			if (mBufferedWriter != null) {
				mBufferedWriter.close();
			}
		} catch (Exception IOEX) {
			System.out.println("关闭文件失败");
			IOEX.printStackTrace();
			return false;
		}
		return true;
	}

	private StringBuffer checkKey(String aInfo) {
		String tInfo = aInfo.replaceAll("\"", "\"\"");
		StringBuffer tNewString = new StringBuffer(tInfo);
		return tNewString;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CSVFileWrite tCSVFileWrite = new CSVFileWrite("D:\\", "maning");
		String[][] tTitle = new String[3][];
		tTitle[0] = new String[1];
		tTitle[0][0] = "承保赔付汇总表";

		tTitle[1] = new String[1];
		tTitle[1][0] = "统计时间：2011-1-1至2011-4-26";

		tTitle[2] = new String[5];
		tTitle[2][0] = "姓名";
		tTitle[2][1] = "性别";
		tTitle[2][2] = "年龄";
		tTitle[2][3] = "保单号";
		tTitle[2][4] = "出生日期";

		String[] tContentType = { "String", "String", "Number", "String",
				"String" };
		tCSVFileWrite.addTitle(tTitle, tContentType);
		String[][] tContent = new String[2][5];
		tContent[0][0] = "张三,";
		tContent[0][1] = "女";
		tContent[0][2] = "12";
		tContent[0][3] = "00001";
		tContent[0][4] = "1999-3-1";
		tContent[1][0] = "李四\"";
		tContent[1][1] = "男";
		tContent[1][2] = "23";
		tContent[1][3] = "00002";
		tContent[1][4] = "1988-1-23";

		tCSVFileWrite.addContent(tContent);
		tCSVFileWrite.writeFile();

		tCSVFileWrite.closeFile();
	}

}
