package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

public class NBCGPrintBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    private String mContNo = "";

    //String InsuredName="";
    //输入的查询sql语句
    private String msql = "";

    //取得的延期承保原因
    private String mUWError = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的实付号码
    private String mActuGetNo = "";

//取得的集体合同号码
    private String mGrpContNo = "";

//取得的电话号码
    private String telnumber = "";

//取得的退款数额
    private double money = 0;

//取得的单位名称
    private String mGrpName = "";

//取得的管理机构代码
    private String mManageCom = "";
//
    private String mdate = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LDComSchema mLDComSchema = new LDComSchema();

    public NBCGPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mLJAGetSchema.setSchema((LJAGetSchema) cInputData.getObjectByObjectName(
                "LJAGetSchema", 0));
        mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null && mLJAGetSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        mActuGetNo = mLJAGetSchema.getActuGetNo();
        mGrpContNo = mLCGrpContSchema.getGrpContNo(); //集体保单号
        if (mActuGetNo.length() != 0)
        {
            System.out.println("ActuGetNo = " + mActuGetNo);
            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setSchema(mLJAGetSchema);
            if (tLJAGetDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLJAGetDB.mErrors);
                buildError("outputXML", "在取得打印队列1中数据时发生错误");
                return false;
            }
            mLJAGetSchema = tLJAGetDB.getSchema();
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setSchema(mLCGrpContSchema);
            if (tLCGrpContDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得打印队列2中数据时发生错误");
                return false;
            }
            mLCGrpContSchema = tLCGrpContDB.getSchema();
            mManageCom = mLCGrpContSchema.getManageCom();
            mGrpName = mLCGrpContSchema.getGrpName();
            money = mLJAGetSchema.getSumGetMoney();

            mLDComSchema.setComCode(mManageCom);
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setSchema(mLDComSchema);
            if (tLDComDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLDComDB.mErrors);
                buildError("outputXML", "在取得打印队列3中数据时发生错误");
                return false;
            }
            mLDComSchema = tLDComDB.getSchema();
            telnumber = mLDComSchema.getPhone();

            System.out.println("GrpContNo = " + mGrpContNo);
            System.out.println("GrpName = " + mGrpName);
            System.out.println("money = " + money);
            System.out.println("ActuGetNo = " + mActuGetNo);
            System.out.println("telnumber = " + telnumber);
        }
        else
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setSchema(mLCGrpContSchema);
            if (tLCGrpContDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得打印队列1中数据时发生错误");
                return false;
            }
            mLCGrpContSchema = tLCGrpContDB.getSchema();
            mManageCom = mLCGrpContSchema.getManageCom();
            mGrpName = mLCGrpContSchema.getGrpName(); //投保单位名称

            mLDComSchema.setComCode(mManageCom);
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setSchema(mLDComSchema);
            if (tLDComDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLDComDB.mErrors);
                buildError("outputXML", "在取得打印队列2中数据时发生错误");
                return false;
            }
            mLDComSchema = tLDComDB.getSchema();
            telnumber = mLDComSchema.getPhone(); //电话号码

            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setOtherNo(mGrpContNo);
            LJAGetSet tLJAGetSet = new LJAGetSet();
            tLJAGetSet = tLJAGetDB.query();
            if (tLJAGetSet.size() > 0)
            {
                mLJAGetSchema = tLJAGetSet.get(1);
                money = mLJAGetSchema.getSumGetMoney(); //钱
            }
            else
            {
                mErrors.copyAllErrors(tLJAGetDB.mErrors);
                buildError("outputXML", "退费表中未找到相关数据！请先退费");
                return false;

            }
            System.out.println("GrpContNo = " + mGrpContNo);
            System.out.println("GrpName = " + mGrpName);
            System.out.println("money = " + money);
            System.out.println("ActuGetNo = " + mActuGetNo);
            System.out.println("telnumber = " + telnumber);

        }
//其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NBCIpayback.vts", "printer"); //最好紧接着就初始化xml文档
//        PubFun tPubFun = new PubFun();
        mdate = PubFun.getCurrentDate();
        texttag.add("GrpName", mGrpName); //投保单位名称
        texttag.add("GrpContNo", mGrpContNo); //投保单号
        texttag.add("money", money); //退费金额
        texttag.add("money1", money); //退费金额
        texttag.add("date", mdate); //日期
        texttag.add("tel", telnumber);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        String[] PETitle = new String[2];
        PETitle[0] = "ID";
        PETitle[1] = "CHECKITEM";
        ListTable tPEListTable = new ListTable();
        tPEListTable.setName("CHECKITEM");
        String[] strPE = new String[2];
        tPEListTable.add(strPE);
        xmlexport.addListTable(tPEListTable, PETitle);

//保存保险计划调整
        //保存其它信息
//xmlexport.outputDocumentToFile("d:\\","testHZM");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
