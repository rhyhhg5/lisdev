package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/**
 * <p>Title: 理赔计算结果明细</p>
 * <p>Description: 理赔计算结果明细清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author huxl
 * @version 1.0
 * @date 2007-03-31
 */

public class ReinsureClaimListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    String StartDate = "";
    String EndDate = "";
    String ReRiskSort = "";
    String RecontCode = "";
    String ReRiskCode = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    public ReinsureClaimListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadClaimData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        ReRiskSort = (String) tTransferData.getValueByName("ReRiskSort");
        RecontCode = (String) tTransferData.getValueByName("RecontCode");
        ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean downloadClaimData() {
        try {
            SysPath = SysPath + "reinsure/";
            String Path = PubFun.getCurrentDate() + "-" + PubFun.getCurrentTime() +
                          ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel tWriteToExcel = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "ReClaimData" +
                           ((RecontCode == null || RecontCode.equals("")) ? "" :
                            "_" + RecontCode) +
                           ((ReRiskCode == null || ReRiskCode.equals("")) ? "" :
                            "_" + ReRiskCode) +
                           ".xls";
            tWriteToExcel.createExcelFile();
            String[] sheetName = {StartDate + "__" + EndDate};
            tWriteToExcel.addSheet(sheetName);

            if (ReRiskSort.equals("1")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += "and c.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += "and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 =
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'', " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                        "a.OccupationType,a.Amnt,c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate," +
                        "c.ConfDate,c.GetDataDate,c.LeaveHospDate," +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,varchar(d.CessionRate),'',e.ClaimBackFee " +
                        "from LRPol a,LCCont b,LRPolClm c,LRPolResult d,LRPolClmResult e  " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno  " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and d.PayCount = 1 and a.ContType = '1' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'', " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                        "a.OccupationType,a.Amnt,c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate," +
                        "c.ConfDate,c.GetDataDate,c.LeaveHospDate," +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,varchar(d.CessionRate),'',e.ClaimBackFee " +
                        "from LRPol a,LBCont b,LRPolClm c,LRPolResult d,LRPolClmResult e  " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno  " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and d.PayCount = 1 and a.ContType = '1' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'), " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,varchar(d.CessionRate),'',e.ClaimBackFee  " +
                        "from LRPol a,LCGrpCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and d.PayCount = 1 and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,varchar(d.CessionRate),'',e.ClaimBackFee  " +
                        "from LRPol a,LBGrpCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and d.PayCount = 1 and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " + //这里处理理赔存储续保后NewPolNo情况下的摊回清单
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'', " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                        "a.OccupationType,a.Amnt,c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate," +
                        "c.ConfDate,c.GetDataDate,c.LeaveHospDate," +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,(select FactorValue from LRCalFactorValue " +
                        "where RecontCode = c.RecontCode and FactorCode = 'CessionRate' and RiskCode = a.RiskCode),'',e.ClaimBackFee " +
                        "from LRPol a,LBCont b,LRPolClm c,LRPolClmResult e,LCRNewStateLog f " +
                        "where a.ContNo = f.ContNo and b.contno = f.NewContNo and a.PolNo = f.PolNo and c.Polno = f.NewPolNo and c.polno = e.Polno " +
                        " and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount " +
                        " and a.ContType = '1' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " + //这里处理无名单摊回问题。
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.EndDate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'), " +
                        "b.AppntName,b.AppntNo,b.InsuredNo,b.InsuredName,char(b.InsuredBirthday),b.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = b.InsuredNo),b.OccupationType,b.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,(select FactorValue from LRCalFactorValue " +
                        "where RecontCode = a.RecontCode and FactorCode = 'CessionRate' and RiskCode = a.RiskCode),'',e.ClaimBackFee " +
                        "from LRPol a,LCPol b,LRPolClm c,LRPolClmResult e   " +
                        "where a.polno = b.masterpolno and b.PolNo = c.Polno and b.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount " +
                        "and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        "and exists (select 1 from LCPol where GrpPolNo = a.GrpPolNo and PolTypeFlag = '1') " +
                        sql +
                        "union all " + //这里处理无名单摊回问题。
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.enddate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'), " +
                        "b.AppntName,b.AppntNo,b.InsuredNo,b.InsuredName,char(b.InsuredBirthday),b.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = b.InsuredNo),b.OccupationType,b.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,(select FactorValue from LRCalFactorValue " +
                        "where RecontCode = a.RecontCode and FactorCode = 'CessionRate' and RiskCode = a.RiskCode),'',e.ClaimBackFee " +
                        "from LRPol a,LBPol b,LRPolClm c,LRPolClmResult e   " +
                        "where a.polno = b.masterpolno and b.PolNo = c.Polno and b.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount " +
                        "and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        "and exists (select 1 from LBPol where GrpPolNo = a.GrpPolNo and PolTypeFlag = '1') " +
                        sql +
                        "union all " + //0900902新增适应卡折的保单.卡折的保单有分保提数没有分保结果.
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'), " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,(select FactorValue from LRCalFactorValue " +
                        "where RecontCode = a.RecontCode and FactorCode = 'CessionRate' and RiskCode = a.RiskCode),'',e.ClaimBackFee  " +
                        "from LRPol a,LCGrpCont b,LRPolClm c,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno  and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode  and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and b.cardflag='2' " +
                        " and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.Diagnoses,c.AccDesc,c.AccidentDate,c.RgtDate,c.ConfDate, " +
                        "c.GetDataDate,c.LeaveHospDate,   " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,(select FactorValue from LRCalFactorValue " +
                        "where RecontCode = a.RecontCode and FactorCode = 'CessionRate' and RiskCode = a.RiskCode),'',e.ClaimBackFee  " +
                        "from LRPol a,LBGrpCont b,LRPolClm c,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and b.cardflag='2' " +
                        "and a.ContType = '2' and a.RiskCalSort = '1' and c.ClmNo = e.ClmNo " +
                        sql +
                        " with ur";
                String[][] strArr1Head = new String[1][29];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "缴费方式";
                strArr1Head[0][7] = "团体名称";
                strArr1Head[0][8] = "投保人客户号";
                strArr1Head[0][9] = "被保人客户号";

                strArr1Head[0][10] = "被保险人人数/姓名";
                strArr1Head[0][11] = "平均年龄/生日";
                strArr1Head[0][12] = "性别";
                strArr1Head[0][13] = "身份证号码";
                strArr1Head[0][14] = "职业类别";
                strArr1Head[0][15] = "保额";
                strArr1Head[0][16] = "索赔额";
                strArr1Head[0][17] = "赔付金额";
                strArr1Head[0][18] = "诊断";

                strArr1Head[0][19] = "出险原因";
                strArr1Head[0][20] = "出险日期";
                strArr1Head[0][21] = "索赔日期";
                strArr1Head[0][22] = "赔付日期";
                strArr1Head[0][23] = "结案日期";
                strArr1Head[0][24] = "出院日期";
                strArr1Head[0][25] = "分保类型";
                strArr1Head[0][26] = "分保分保比例";
                strArr1Head[0][27] = "分保分保保额";
                strArr1Head[0][28] = "分保摊回赔款";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("2")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 =
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'', " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),    " +
                        "a.OccupationType,varchar(a.DiseaseAddFeeRate),a.Amnt,c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.GiveTypeDesc,c.ConfDate, " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,d.CessionAmount,e.ClaimBackFee      " +
                        "from LRPol a,LCCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode    " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '1' and a.RiskCalSort = '2' and c.ClmNo = e.ClmNo " +
                        sql +
                        " union all " +
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'', " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),    " +
                        "a.OccupationType,varchar(a.DiseaseAddFeeRate),a.Amnt,c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.GiveTypeDesc,c.ConfDate, " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,d.CessionAmount,e.ClaimBackFee      " +
                        "from LRPol a,LBCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode    " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '1' and a.RiskCalSort = '2' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,'',a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.GiveTypeDesc,c.ConfDate, " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,d.CessionAmount,e.ClaimBackFee      " +
                        "from LRPol a,LCGrpCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode    " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '2' and a.RiskCalSort = '2' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),    " +
                        "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                        "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,'',a.Amnt, " +
                        "c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.GiveTypeDesc,c.ConfDate, " +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,d.CessionAmount,e.ClaimBackFee  " +
                        "from LRPol a,LBGrpCont b,LRPolClm c,LRPolResult d,LRPolClmResult e   " +
                        "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno   " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode    " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '2' and a.RiskCalSort = '2' and c.ClmNo = e.ClmNo " +
                        sql +
                        " with ur";
                String[][] strArr1Head = new String[1][29];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "缴费方式";
                strArr1Head[0][7] = "团体名称";
                strArr1Head[0][8] = "投保人客户号";
                strArr1Head[0][9] = "被保人客户号";

                strArr1Head[0][10] = "被保险人人数/姓名";
                strArr1Head[0][11] = "平均年龄/生日";
                strArr1Head[0][12] = "性别";
                strArr1Head[0][13] = "身份证号码";
                strArr1Head[0][14] = "职业类别";
                strArr1Head[0][15] = "加费率";
                strArr1Head[0][16] = "保额";

                strArr1Head[0][17] = "索赔额";
                strArr1Head[0][18] = "赔付金额";
                strArr1Head[0][19] = "出险原因";
                strArr1Head[0][20] = "出险日期";
                strArr1Head[0][21] = "结案日期";
                strArr1Head[0][22] = "索赔日期";
                strArr1Head[0][23] = "理赔结论";
                strArr1Head[0][24] = "赔付日期";
                strArr1Head[0][25] = "分保类型";
                strArr1Head[0][26] = "溢额分保比例";
                strArr1Head[0][27] = "溢额分保保额";
                strArr1Head[0][28] = "分保摊回赔款";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("3")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 =
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,''," +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                        "(select max(CurPaytoDate) from LRPolDetail where polno = a.Polno),char(a.InsuYear)||a.InsuYearFlag," +
                        "char(a.PayEndYear)||a.PayEndYearFlag,Year(a.GetDataDate - a.CValidate)+1 保单年度,c.UWFlag," +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                        "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                        "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno), " +
                        "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt," +
                        "c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.ConfDate,c.RgtDate,c.GiveTypeDesc,a.AppntName,(select IdNo from LDPerson where CustomerNo = a.AppntNo), " +
                        "'','',a.InsuredNo,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                        "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                        "d.CessionRate,d.CessionAmount,e.ClaimBackFee " +
                        "from LRPol a,LCCont b,LRPolClm c,LRPolResult d,LRPolClmResult e " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '1' and a.RiskCalSort = '3' and c.ClmNo = e.ClmNo " +
                        sql +
                        "union all " +
                        "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,''," +
                        "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                        "(select max(CurPaytoDate) from LRPolDetail where polno = a.Polno),char(a.InsuYear)||a.InsuYearFlag," +
                        "char(a.PayEndYear)||a.PayEndYearFlag,Year(a.GetDataDate - a.CValidate)+1 保单年度,c.UWFlag," +
                        "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                        "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                        "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno), " +
                        "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt," +
                        "c.ClaimMoney,c.RealPay,c.AccDesc,c.AccidentDate,c.GetDataDate, " +
                        "c.RgtDate,c.ConfDate,c.RgtDate,c.GiveTypeDesc,a.AppntName,(select IdNo from LDPerson where CustomerNo = a.AppntNo), " +
                        "'','',a.InsuredNo,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                        "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                        "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                        "d.CessionRate,d.CessionAmount,e.ClaimBackFee " +
                        "from LRPol a,LBCont b,LRPolClm c,LRPolResult d,LRPolClmResult e " +
                        "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                        "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                        "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                        "and a.ContType = '1' and a.RiskCalSort = '3' and c.ClmNo = e.ClmNo " +
                        sql + " with ur ";
                String[][] strArr1Head = new String[1][44];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "缴费方式";
                strArr1Head[0][7] = "交至日";
                strArr1Head[0][8] = "保险期间";
                strArr1Head[0][9] = "缴费期间";
                strArr1Head[0][10] = "保单年度";
                strArr1Head[0][11] = "核保结论";

                strArr1Head[0][12] = "团体名称";
                strArr1Head[0][13] = "投保人客户号";
                strArr1Head[0][14] = "被保人客户号";
                strArr1Head[0][15] = "被保险人人数/姓名";
                strArr1Head[0][16] = "平均年龄/生日";
                strArr1Head[0][17] = "性别";
                strArr1Head[0][18] = "身份证号码";
                strArr1Head[0][19] = "职业类别";
                strArr1Head[0][20] = "吸烟状态";
                strArr1Head[0][21] = "死亡加费率";
                strArr1Head[0][22] = "重疾加费率";
                strArr1Head[0][23] = "原始保额";

                strArr1Head[0][24] = "索赔额";
                strArr1Head[0][25] = "赔付金额";
                strArr1Head[0][26] = "出险原因";
                strArr1Head[0][27] = "出险日期";
                strArr1Head[0][28] = "结案日期";
                strArr1Head[0][29] = "索赔日期";
                strArr1Head[0][30] = "赔付日期";
                strArr1Head[0][31] = "索赔申请日期";
                strArr1Head[0][32] = "理赔结论";
                strArr1Head[0][33] = "投保人姓名";
                strArr1Head[0][34] = "投保人证件号码";
                strArr1Head[0][35] = "受益人姓名"; //
                strArr1Head[0][36] = "受益人证件号码"; //
                strArr1Head[0][37] = "被保人姓名";
                strArr1Head[0][38] = "被保人证件号码";

                strArr1Head[0][39] = "分保类型";
                strArr1Head[0][40] = "自留额";
                strArr1Head[0][41] = "溢额分保比例";
                strArr1Head[0][42] = "溢额分保保额";
                strArr1Head[0][43] = "分保摊回赔款";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            }
            tWriteToExcel.write(SysPath);
            String NewPath = PubFun.getCurrentDate() + "-" +
                             PubFun.getCurrentTime() + ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = SysPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath!  : " + NewPath);
        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }


    public static void main(String[] args) {

    }
}
