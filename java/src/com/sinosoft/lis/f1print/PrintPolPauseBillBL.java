package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title:PrintPolPauseBillBL </p>
 *
 * <p>Description:PrintPolPauseBillBL类文件 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrintPolPauseBillBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    public VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    public VData mResult = new VData();
    /** 数据操作字符串 */
    public String mOperate = "";

    private TransferData mTransferData = new TransferData();

    private SSRS mSSRS = new SSRS();

    private String startDate = "";
    private String endDate = "";
    private String orderSql = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    public PrintPolPauseBillBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData(mInputData)) {
            this.bulidErrors("getInputData", "数据不完整");
            return false;
        }

        if (!getListData()) {
            this.bulidErrors("getListData", "查询XML数据出错！");
            return false;
        }
        if (!getPrintData()) {
            this.bulidErrors("getPrintData", "数据不完整");
            return false;
        }

        return true;
    }

    /**
     * 查询数据
     * @return boolean
     */
    private boolean getListData() {
        StringBuffer sql = new StringBuffer();

        sql.append(
                "SELECT a.StandbyFlag2,b.AppntName,a.OtherNo,min(c.CurPayToDate),");
        sql.append("a.MakeDate,a.MakeDate,a.ReqOperator,getAgentName(a.AgentCode) AgentCode,ShowManageName(a.ManageCom) ManageCom");
        sql.append(
                " From LOPRTManager a,LCCont b,LJSPayPersonB c  WHERE a.code = '42' AND");
        sql.append(
                " a.StateFlag = '0'  AND a.OtherNoType = '01'  AND a.OtherNo = b.ContNo");
        sql.append(" AND appflag='1'  AND  a.otherNo = c.GrpContNo");

        if (!startDate.equals("")) {
            sql.append(" and a.MakeDate >='" + startDate + "'");
        }

        if (!endDate.equals("")) {
            sql.append(" and a.MakeDate <='" + endDate + "'");
        }

        sql.append(" GROUP BY a.StandbyFlag2,b.AppntName,a.OtherNo,a.MakeDate,");
        sql.append("a.MakeDate,a.ReqOperator,a.AgentCode,a.ManageCom");

        sql.append(orderSql);

        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(sql.toString());

        if (tExeSQL.mErrors.needDealError()) {
            return false;
        }

        return true;
    }

    /**
     * 往后台传送数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
        } catch (Exception ex) {
            mErrors.addOneError(ex.getMessage());
            return false;
        }

        startDate = (String) mTransferData.getValueByName("StartDate");
        endDate = (String) mTransferData.getValueByName("EndDate");
        orderSql = (String) mTransferData.getValueByName("OrderSql");

        System.out.println("45454545454545454545454545454545" + orderSql);
        return true;
    }

    /**
     * 需要打印的数据
     * @param cInputData VData
     * @return boolean
     */
    public boolean getPrintData() {
        //新建一个XmlExport的实例
        XmlExport tXmlExport = new XmlExport();
        //调用模版
        tXmlExport.createDocument("PPolPauseListBill.vts", "printer");
        //添加表头
        String[] title = {"失效通知书号", "投保人", "保单号", "应收日期",
                         "失效日期", "生成时间", "经办人", "业务员", "业务部门"};

        tXmlExport.addListTable(this.getListTable(), title);

//        tXmlExport.outputDocumentToFile("c:\\", "PolPauseBill");

        mResult.clear();

        mResult.add(tXmlExport);

        return true;
    }

    /**
     * 数据添加到列表中
     * @return ListTable
     */
    private ListTable getListTable() {
        //生成列表
        ListTable tListTable = new ListTable();

        //判断记录集条数
        if (mSSRS.getMaxRow() > 0) {
            //循环加入列表
            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
                String[] info = new String[9];

                info[0] = mSSRS.GetText(i, 1);
                info[1] = mSSRS.GetText(i, 2);
                info[2] = mSSRS.GetText(i, 3);
                info[3] = mSSRS.GetText(i, 4);
                info[4] = mSSRS.GetText(i, 5);
                info[5] = mSSRS.GetText(i, 6);
                info[6] = mSSRS.GetText(i, 7);
                info[7] = mSSRS.GetText(i, 8);
                info[8] = mSSRS.GetText(i, 9);

                tListTable.add(info);

            }
        }
        tListTable.setName("ZT");
        //返回列表
        return tListTable;

    }

    /**
     * 错误信息
     * @param cEroFunction String
     * @param cEroMsg String
     */
    private void bulidErrors(String cEroFunction, String cEroMsg) {
        CError error = new CError();

        error.moduleName = "PrintPolPauseBillBL";
        error.functionName = cEroFunction;
        error.errorMessage = cEroMsg;
        this.mErrors.addOneError(error);
    }

    /**
     * 返回数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
