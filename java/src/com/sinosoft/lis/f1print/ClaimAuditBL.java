package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLSurveySet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.lis.vschema.LLClaimUWMainSet;

public class ClaimAuditBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    //取得的延期承保原因
    private String mUWError = "";
    
    private String mdirect;

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema(); //事件关联表
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();  //事件表
    private LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
    private LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public ClaimAuditBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        
        mdirect = "";
        mdirect += mLOPRTManagerSchema.getStandbyFlag2();
        mLLCaseSchema.setCaseNo(mLOPRTManagerSchema.getOtherNo());

        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        String aCaseNo = mLLCaseSchema.getCaseNo();
        String AccDate="";
        String AccSite="";
        String AccDesc="";
        String RemarkSP="";
        String RemarkSD="";
        String SPer = "";
        String SDer = "";
        System.out.println("CaseNo=" + aCaseNo);
        boolean Flag = false;
        //立案分案
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(aCaseNo);
        if (!tLLCaseDB.getInfo())
        {
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            buildError("outputXML", "在取得LLCaseDB的数据时发生错误");
            return false;
        }
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        //立案/申请登记总表
        String aRgtNo = tLLCaseDB.getRgtNo();
        String aCustomerNo = tLLCaseDB.getCustomerNo();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(tLLCaseDB.getRgtNo());
        if (!tLLRegisterDB.getInfo())
        {
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            buildError("outputXML", "在取得LLRegisterDB的数据时发生错误");
            return false;
        }

        String Sqlt = "select DiagnoseDate from LLCaseCure where caseno ='tCaseNo'  and SeriousFlag = '1'";
        String Sqly = "select DianoseDate from LLCaseInfo where caseno='tCaseNo' and DeformityGrade = '1'";
        SSRS tSSRS = new SSRS();
        SSRS ySSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(Sqlt);
        ySSRS = tExeSQL.execSQL(Sqly);

        //查询事件信息
        LLSubReportDB mLLSubReportDB = new LLSubReportDB();
        LLCaseRelaDB mLLCaseRelaDB = new LLCaseRelaDB();
        mLLCaseRelaDB.setCaseNo(aCaseNo);
        mLLCaseRelaSet.set(mLLCaseRelaDB.query());
        if (mLLCaseRelaSet == null || mLLCaseRelaSet.size() == 0) {
          Flag = false;
        }
        else {
            Flag = true;
            int SubRptCount=mLLCaseRelaSet.size();
            for(int i=1;i<=SubRptCount;i++)
            {
              String aSubRptNo = mLLCaseRelaSet.get(i).getSubRptNo();
              mLLSubReportDB.setSubRptNo(aSubRptNo);
              if (mLLSubReportDB.getInfo() == false) {
                mErrors.copyAllErrors(mLLSubReportDB.mErrors);
                buildError("outputXML", "mLLSubReportDB在取得打印队列中数据时发生错误");
                return false;
              }
              AccDate = mLLSubReportDB.getSchema().getAccDate();
              AccSite = mLLSubReportDB.getSchema().getAccPlace();
              AccDesc = mLLSubReportDB.getSchema().getAccDesc();
            }
       }
        //查赔案明细表
        LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        tLLClaimPolicyDB.setCaseNo(aCaseNo);
        tLLClaimPolicySet = tLLClaimPolicyDB.query();

        //险种名称
        String[] RiskTitle = {"ID", "GETRISKNAME"};
        ListTable tRiskListTable = new ListTable();
        tRiskListTable.setName("GETRISKNAME");
        String GetRiskName = "" + "\n";
        if (tLLClaimPolicySet == null || tLLClaimPolicySet.size() == 0)
        {
            Flag = false;
        }
        else
        {
            Flag = true;
            tLLClaimPolicySchema = tLLClaimPolicySet.get(1);
             for (int i = 1; i <= tLLClaimPolicySet.size(); i++)
                {
                 tLLClaimPolicySchema=tLLClaimPolicySet.get(i);
                            LMRiskDB tLMRiskDB = new LMRiskDB();
                            tLMRiskDB.setRiskCode(tLLClaimPolicySchema.getRiskCode());
                            if (!tLMRiskDB.getInfo())
                            {
                                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                                buildError("outputXML", "在取得tLMRiskDB的数据时发生错误");
                                return false;
                            }
                    GetRiskName += "   " + tLMRiskDB.getRiskName() + "\n" +
                            "\n";
                    System.out.println(GetRiskName);
                }
        }

        //赔付明细
        String[] strClm;
        String[] ClmTitle =
                {
                "ID", "GETDUTYNAME"};
        ListTable tClmListTable = new ListTable();
        tClmListTable.setName("GETDUTYNAME");
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        tLLClaimDetailDB.setRgtNo(aRgtNo);
        tLLClaimDetailDB.setCaseNo(aCaseNo);
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        tLLClaimDetailSet.set(tLLClaimDetailDB.query());
        String GetDutyName = "" + "\n";

        if (tLLClaimDetailSet == null || tLLClaimDetailSet.size() == 0)
        {
            Flag = false;
        }
        else
        {
            Flag = true;
            LLClaimDetailSchema tLLClaimDetailSchema;
            for (int i = 1; i <= tLLClaimDetailSet.size(); i++)
            {
                tLLClaimDetailSchema = new LLClaimDetailSchema();
                tLLClaimDetailSchema.setSchema(tLLClaimDetailSet.get(i));
                String getDutyCode = tLLClaimDetailSchema.getGetDutyCode();
                String getDutyKind = tLLClaimDetailSchema.getGetDutyKind();
                LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
                tLMDutyGetClmDB.setGetDutyCode(getDutyCode);
                tLMDutyGetClmDB.setGetDutyKind(getDutyKind);
                if (!tLMDutyGetClmDB.getInfo())
                {
                    mErrors.copyAllErrors(tLMDutyGetClmDB.mErrors);
                    buildError("outputXML", "在取得tLMDutyGetClmDB的数据时发生错误");
                    return false;
                }
//                strClm = new String[2];
//                strClm[0] = (new Integer(i)).toString(); //序号
//                strClm[1] = tLMDutyGetClmDB.getGetDutyName()+"实赔金额"+tLLClaimDetailSchema.getRealPay()+"元";
//                tClmListTable.add(strClm);
                GetDutyName += "   " + tLMDutyGetClmDB.getGetDutyName() +
                        "实赔金额" + tLLClaimDetailSchema.getRealPay() + "元" + "\n" +
                        "\n";
                System.out.println(GetDutyName);
            }
        }

        //调查报告
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        LLSurveySet tLLSurveySet = new LLSurveySet();
        tLLSurveyDB.setOtherNo(aCaseNo);
        tLLSurveyDB.setOtherNoType("1");
        tLLSurveySet = tLLSurveyDB.query();

        String strSQL = "Select count(*) from LLCase where customerno = '" +
                        aCustomerNo + "'";
        ExeSQL exesql = new ExeSQL();
        String Occur = exesql.getOneValue(strSQL);

        //审批审定结论
        LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
        tLLClaimUWMainDB.setCaseNo(aCaseNo);
        tLLClaimUWMainSet = tLLClaimUWMainDB.query();
        if (tLLClaimUWMainSet == null || tLLClaimUWMainSet.size() == 0)
        {
            Flag = false;
        }
        else
        {
            Flag = true;
            mLLClaimUWMainSchema = tLLClaimUWMainSet.get(1);
        }
        RemarkSP = mLLClaimUWMainSchema.getRemark1();
        RemarkSD = mLLClaimUWMainSchema.getRemark2();
        strSQL = "select Username from lduser where usercode='" + mLLClaimUWMainSchema.getOperator() + "'";
        ExeSQL exesql1 = new ExeSQL();
        SPer = exesql1.getOneValue(strSQL);
        strSQL = "select Username from lduser where usercode='" + mLLClaimUWMainSchema.getAppClmUWer() + "'";
        exesql1 = new ExeSQL();
        SDer = exesql1.getOneValue(strSQL);

        System.out.print(tClmListTable.toString() + "jjjjjj" +
                         ClmTitle.toString());

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ClaimCase.vts", "printer"); //最好紧接着就初始化xml文档

        StrTool tSrtTool = new StrTool();
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";
        
        String sqlusercom = "select comcode from lduser where usercode='" +
        mGlobalInput.Operator + "' with ur";
		String comcode = new ExeSQL().getOneValue(sqlusercom);
		texttag.add("JetFormType", "lp015");
		if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
		comcode = "86";
		} else if (comcode.length() >= 4){
		comcode = comcode.substring(0, 4);
		} else {
		buildError("getInputData", "操作员机构查询出错！");
		return false;
		}
		String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
		String printcode = new ExeSQL().getOneValue(printcom);
        
        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));      
        texttag.add("previewflag", "1");

        //生成-年-月-日格式的日期
        strSQL = "Select sum(RealPay) from LLClaimPolicy where caseno = '" +
                        tLLCaseDB.getCaseNo() + "'";
        exesql = new ExeSQL();
        String realpay = exesql.getOneValue(strSQL);

        int Age = (int)tLLCaseDB.getCustomerAge();
        //模版自上而下的元素
        texttag.add("ManageCom", mGlobalInput.ManageCom);
        texttag.add("CaseNo", tLLCaseDB.getCaseNo());
        texttag.add("Name", tLLCaseDB.getCustomerName()); //被保险人姓名
        System.out.println("性别是："+mLLCaseSchema.getCustomerSex());
        texttag.add("Sex", "0".equals((String)mLLCaseSchema.getCustomerSex()) ? "男" : "女");
        texttag.add("Age", Age);
        texttag.add("AccidentDate", AccDate);  //事件发生日
        texttag.add("AccidentSite", AccSite); //地点
        String aCustState = tLLCaseDB.getCustState();
        strSQL =
                "select codename from ldcode where codetype = 'llcuststatus' and code = '" +
                aCustState + "'";
        exesql = new ExeSQL();
        String res = exesql.getOneValue(strSQL);
        texttag.add("CustState",res);

        if (tSSRS.getMaxCol() >= 2)
        {
            if (!tSSRS.GetText(1, 1).equals("0") ||
                tSSRS.GetText(1, 1).trim().equals("") ||
                tSSRS.GetText(1, 1).equals("null"))
            {
                texttag.add("DianoseDate", tSSRS.GetText(1, 1)); //确诊日期
            }
            else
            {
                texttag.add("DianoseDate", ""); //确诊日期
            }
        }
        else
        {
            texttag.add("DianoseDate", ""); //确诊日期
        }
        if (ySSRS.getMaxCol() >= 2)
        {
            if (!ySSRS.GetText(1, 1).equals("0") ||
                ySSRS.GetText(1, 1).trim().equals("") ||
                ySSRS.GetText(1, 1).equals("null"))
            {
                texttag.add("DiseaseDianoseDate", ySSRS.GetText(1, 1)); //残疾日期
            }
            else
            {
                texttag.add("DiseaseDianoseDate", ""); //残疾日期
            }
        }
        texttag.add("DiseaseDianoseDate", ""); //残疾日期
        texttag.add("DeathDate", tLLCaseDB.getDeathDate()); //身故日期

        texttag.add("AccdentDesc", AccDesc); //事故描述

        texttag.add("bRgtDate", tLLRegisterDB.getRgtDate()); //报案日

        texttag.add("RgtantName", tLLRegisterDB.getRgtantName()); //理赔申请人姓名
        strSQL = "select codename from ldcode where codetype = 'llaskmode' and code = '" +
                tLLRegisterDB.getRgtType() + "'";
        exesql = new ExeSQL();
        String rgttype = exesql.getOneValue(strSQL);
        texttag.add("RgtType", rgttype); //受理方式
        texttag.add("sRgtDate", tLLRegisterDB.getRgtDate()); //理陪申请日期
        texttag.add("LinkMan", tLLCaseDB.getCustomerName()); //联系人
        texttag.add("Phone", tLLCaseDB.getPhone()); //联系电话
        texttag.add("slRgtDate", tLLCaseDB.getRgtDate()); //理陪受理日期
        texttag.add("AffixGetDate", tLLCaseDB.getAffixGetDate()); //材料补齐日期
        texttag.add("CaseState", getComName(tLLCaseDB.getRgtState())); //案件状态
        texttag.add("GiveTypeDesc", tLLClaimPolicySchema.getGiveTypeDesc()); //理赔结论描述
        texttag.add("Occur",Occur);
        texttag.add("GETRISKNAME", GetRiskName); //险种名称
        texttag.add("RealPay", realpay); //给付保险金
        texttag.add("SysDate", SysDate);
        texttag.add("FreePrem", "0.0");
        texttag.add("GivePrem", "0.0");
        texttag.add("GETDUTYNAME", GetDutyName);
        texttag.add("RemarkSP",RemarkSP);
        texttag.add("RemarkSD",RemarkSD);
        texttag.add("SPer",SPer);
        texttag.add("SDer",SDer);
        if (tLLSurveySet != null && tLLSurveySet.size() != 0)
        {
            texttag.add("Result", tLLSurveySet.get(1).getresult()); //调查报告
        }
        else
        {
            texttag.add("Result", ""); //调查报告
        }

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        if (Flag == true)
        {

            xmlexport.addListTable(tRiskListTable,RiskTitle);
            xmlexport.addListTable(tClmListTable, ClmTitle); //保存体检信息
        }

        xmlexport.outputDocumentToFile("e:\\", "ClaimCase"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("llrgtstate");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    
    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(mLLCaseSchema.getCaseNo()); //存放前台传来的caseno
        tLOPRTManagerSchema.setOtherNoType("5");
        tLOPRTManagerSchema.setCode("lp015");
        tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setAgentCode("");
        tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag2(mLLCaseSchema.getCaseNo());

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }
    
    public static void main(String[] args)
    {

        ClaimAuditBL tClaimAuditBL = new ClaimAuditBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C1100050707000015");
        tVData.addElement(tLLCaseSchema);
        tClaimAuditBL.submitData(tVData, "PRINT");
        VData vdata = tClaimAuditBL.getResult();
    }
}
