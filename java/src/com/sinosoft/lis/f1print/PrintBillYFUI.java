package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;

public class PrintBillYFUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;

    public PrintBillYFUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

           /* if (!getInputData(cInputData))
            {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }*/

            PrintBillYFBL tPrintBillYFBL = new PrintBillYFBL();

            System.out.println("Start PLPsqs UI Submit ...");

            if (!tPrintBillYFBL.submitData(cInputData, cOperate))
            {
                if (tPrintBillYFBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPrintBillYFBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "FinChargeDayModeF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPrintBillYFBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PrintBillYFUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        vData.clear();
        vData.add(strBillNo);
        vData.add(strBankCode);
        vData.add(strMngCom);
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        //全局变量
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        strMngCom = (String) cInputData.get(2);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PrintBillYFUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        String tBillNo = "00000000000000000036";
        String tBankCode = "0102";
        tG.ManageCom = "8611";
        VData tVData = new VData();
        tVData.addElement(tBillNo);
        tVData.addElement(tBankCode);
        tVData.addElement(tG);
        PrintBillYFUI tPrintBillUI = new PrintBillYFUI();
        tPrintBillUI.submitData(tVData, "PRINT");
    }
}
