package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpPayColPrtUI
{
    public CErrors mErrors=new CErrors();       //错误处理类,需要错误处理的都放置该类
    private VData mResult=new VData();           //返回界面得数据
    private GlobalInput mGlobalInput=new GlobalInput();      //分别创建LLRegisterSchema
    private LLRegisterSchema mLLRegisterSchema=new LLRegisterSchema();   //和GlobalInput类的对象

    public GrpPayColPrtUI()
    {
    }

    private void buildError(String fun,String msg){
        CError cError=new CError();
        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = fun;
        cError.errorMessage = msg;
        this.mErrors.addOneError(cError);
    }
    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getInputData(VData cInputData){
       mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));  //从cInputData中分别取出GlobalInput
       mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.getObjectByObjectName("LLRegisterSchema", 0));//和LLRegisterSchema分别存入mGlobalInput和mLLRegisterSchema

       if (mLLRegisterSchema == null)
       {
           buildError("getInputData", "没有得到足够的信息！");
           return false;
       }

       return true;
    }

    private boolean dealData()
   {
       return true;
   }

   private boolean prepareOutputData(VData vData)
   {
       try
       {
           vData.clear();
           vData.add(mGlobalInput);
           vData.add(mLLRegisterSchema);
       }
       catch (Exception ex)
       {
           ex.printStackTrace();
           buildError("prepareOutputData", "发生异常");
           return false;
       }
       return true;
   }

   public boolean submitData(VData cInputData, String cOperate)
      {
          if (!cOperate.equals("PRINT"))
          {
              buildError("submitData", "不支持的操作字符串");
              return false;
          }

          // 得到外部传入的数据，将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }

          // 进行业务处理
          if (!dealData())
          {
              return false;
          }


          VData vData = new VData();

          if (!prepareOutputData(vData))
          {
              return false;
          }

          GrpPayColPrtBL tGrpPayColPrtBL = new GrpPayColPrtBL();
          System.out.println("Start PayAppPrt UI Submit ...");

          if (!tGrpPayColPrtBL.submitData(vData, cOperate))             //调用PayColPrtBL的submitData函数.
          {                                                           ////*********6)第六个被调的方法.
              if (tGrpPayColPrtBL.mErrors.needDealError())
              {
                  mErrors.copyAllErrors(tGrpPayColPrtBL.mErrors);
                  return false;
              }
              else
              {
                  buildError("submitData", "PayColPrtBL发生错误，但是没有提供详细的出错信息");
                  return false;
              }
          }
          else
          {
              mResult = tGrpPayColPrtBL.getResult();
              return true;
          }
      }


}
