package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAChannelWeekReportBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mOperate = "";

    private String mLevel = "";

    private String mRiskCode = "";

    private String mStartDate = "";

    private String mEndDate = "";

    private String mPayIntv = "";

    private String mPayYears = "";

    private String mPremType = "";

    private String mBranchType = "";

    private String mBranchType2 = "";
    private String firstCom="";
    private String secondCom="";
    private String thirdCom="";

    private MMap mMap = new MMap();

    public static void main(String[] args)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("Level","1"); 
        tTransferData.setNameAndValue("RiskCode",""); 
        tTransferData.setNameAndValue("StartDate","2007-01-01"); 
        tTransferData.setNameAndValue("EndDate","2007-02-01"); 
        tTransferData.setNameAndValue("PayIntv",""); 
        tTransferData.setNameAndValue("PayYears",""); 
        tTransferData.setNameAndValue("PremType","0"); 
        tTransferData.setNameAndValue("BranchType","3"); 
        tTransferData.setNameAndValue("BranchType2","01"); 
        
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(new GlobalInput());
        LAChannelWeekReportBL tLAChannelWeekReportBL=new LAChannelWeekReportBL();
        tLAChannelWeekReportBL.submitData(tVData,"PRINT");
    }

    public LAChannelWeekReportBL()
    {
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

            if (mGlobalInput == null || mTransferData == null)
            {
                CError tError = new CError();
                tError.moduleName = "LAChannelWeekReportBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLevel = (String) mTransferData.getValueByName("Level");
            this.mRiskCode = (String) mTransferData.getValueByName("RiskCode");
            this.mBranchType = (String) mTransferData
                    .getValueByName("BranchType");
            this.mBranchType2 = (String) mTransferData
                    .getValueByName("BranchType2");
            this.mStartDate = (String) mTransferData
                    .getValueByName("StartDate");
            this.mEndDate = (String) mTransferData.getValueByName("EndDate");
            this.mPayIntv = (String) mTransferData.getValueByName("PayIntv");
            this.mPayYears = (String) mTransferData.getValueByName("PayYears");
            this.mPremType = (String) mTransferData.getValueByName("PremType");

            return true;
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }
    }

    public boolean checkData()
    {
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealDate())
        {
            CError tError = new CError();
            tError.moduleName = "LAChannelWeekReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean dealDate()
    {
        XmlExport xmlexport = new XmlExport();
        ListTable tlistTable = new ListTable();
        TextTag texttag = new TextTag();
        tlistTable.setName("Get");
        String strArr[] = null;
        //查询保费在前三位的网点
        String[] tBankCom=getBankComByPrem();
        if(tBankCom!=null){
            firstCom=tBankCom[0];
            secondCom=tBankCom[1];
            thirdCom=tBankCom[2];
        }
        SSRS tRS = new ExeSQL().execSQL("select name from ldcom where comcode='"+firstCom+"'");
        if (tRS != null && tRS.MaxRow > 0){
            texttag.add("FirstCom", tRS.GetText(1, 1));
        }
        tRS = new ExeSQL().execSQL("select name from ldcom where comcode='"+secondCom+"'");
        if (tRS != null && tRS.MaxRow > 0){
            texttag.add("SecondCom", tRS.GetText(1, 1));
        }
        tRS = new ExeSQL().execSQL("select name from ldcom where comcode='"+thirdCom+"'");
        if (tRS != null && tRS.MaxRow > 0){
            texttag.add("ThirdCom", tRS.GetText(1, 1));
        }
        if (this.mLevel.equals("0"))
        {//按分公司
            xmlexport.createDocument("ChannelWeekReport.vts", "printer");
            String sql = "select comcode,name from ldcom where sign='1' and comcode in( select  distinct substr(comcode,1,4) from ldcom where comcode<>'86') order by comcode with ur";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if (tSSRS != null && tSSRS.MaxRow > 0)
            {
                String[] count=new String[35];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="周";
                //周
                System.out.println("count week......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    strArr = new String[35];
                    String tComCode = tSSRS.GetText(i, 1);
                    strArr[0] = tSSRS.GetText(i, 2);
                    strArr[1] = "周";
                    //工行
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "week");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "week");
                    strArr[4] = getComCount(tComCode, "PY001", "week");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "week");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "week");
                    strArr[7] = getComCount(tComCode, "PY005", "week");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "week");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "week");
                    strArr[10] = getComCount(tComCode, "PY002", "week");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "week");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "week");
                    strArr[13] = getComCount(tComCode, "PY004", "week");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "week");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "week");
                    strArr[16] = getComCount(tComCode, "PY009", "week");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "week");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "week");
                    strArr[19] = getComCount(tComCode, "PY015", "week");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "week");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "week");
                    strArr[22] = getComCount(tComCode, firstCom, "week");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "week");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "week");
                    strArr[25] = getComCount(tComCode, secondCom, "week");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "week");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "week");
                    strArr[28] = getComCount(tComCode, thirdCom, "week");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "week");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "week");
                    strArr[31] = getComCount(tComCode, "", "week");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));

                    tlistTable.add(strArr);
                }
                tlistTable.add(count);
                count=new String[35];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="月";
                //月
                System.out.println("count month......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    strArr = new String[35];
                    String tComCode = tSSRS.GetText(i, 1);
                    strArr[0] = tSSRS.GetText(i, 2);
                    strArr[1] = "月";
                    //工行 
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "month");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "month");
                    strArr[4] = getComCount(tComCode, "PY001", "month");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "month");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "month");
                    strArr[7] = getComCount(tComCode, "PY005", "month");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "month");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "month");
                    strArr[10] = getComCount(tComCode, "PY002", "month");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "month");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "month");
                    strArr[13] = getComCount(tComCode, "PY004", "month");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "month");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "month");
                    strArr[16] = getComCount(tComCode, "PY009", "month");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "month");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "month");
                    strArr[19] = getComCount(tComCode, "PY015", "month");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "month");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "month");
                    strArr[22] = getComCount(tComCode, firstCom, "month");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "month");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "month");
                    strArr[25] = getComCount(tComCode, secondCom, "month");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "month");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "month");
                    strArr[28] = getComCount(tComCode, thirdCom, "month");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "month");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "month");
                    strArr[31] = getComCount(tComCode, "", "month");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));
                    tlistTable.add(strArr);
                }
                tlistTable.add(count);
                count=new String[35];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="年";
                //年
                System.out.println("count year......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    strArr = new String[35];
                    String tComCode = tSSRS.GetText(i, 1);
                    strArr[0] = tSSRS.GetText(i, 2);
                    strArr[1] = "年";
                    //工行
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "year");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "year");
                    strArr[4] = getComCount(tComCode, "PY001", "year");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "year");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "year");
                    strArr[7] = getComCount(tComCode, "PY005", "year");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "year");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "year");
                    strArr[10] = getComCount(tComCode, "PY002", "year");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "year");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "year");
                    strArr[13] = getComCount(tComCode, "PY004", "year");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "year");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "year");
                    strArr[16] = getComCount(tComCode, "PY009", "year");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "year");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "year");
                    strArr[19] = getComCount(tComCode, "PY015", "year");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "year");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "year");
                    strArr[22] = getComCount(tComCode, firstCom, "year");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "year");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "year");
                    strArr[25] = getComCount(tComCode, secondCom, "year");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "year");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "year");
                    strArr[28] = getComCount(tComCode, thirdCom, "year");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "year");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "year");
                    strArr[31] = getComCount(tComCode, "", "year");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));
                    tlistTable.add(strArr);
                }
                tlistTable.add(count);
            }
        }
        else if (this.mLevel.equals("1"))
        {//按支公司
            xmlexport.createDocument("ChannelWeekReport1.vts", "printer");
            String sql = "select comcode,name from ldcom where sign='1' and comcode not in( select  distinct substr(comcode,1,4) from ldcom where comcode<>'86') and comcode<>'86' and comcode<>'86000000' order by comcode  with ur";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if (tSSRS != null && tSSRS.MaxRow > 0)
            {
                String[] count=new String[36];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="周";
                //周
                System.out.println("count week......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    
                    strArr = new String[36];
                    String tComCode = tSSRS.GetText(i, 1);
                    SSRS tRtS = new ExeSQL().execSQL("select name from ldcom where comcode='"+tComCode.substring(0,4)+"'");
                    if (tRtS != null && tRtS.MaxRow > 0)
                    {
                        strArr[0] =  tRtS.GetText(1, 1);
                    }
                    strArr[35] = tSSRS.GetText(i, 2);
                    strArr[1] = "周";
                    //工行
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "week");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "week");
                    strArr[4] = getComCount(tComCode, "PY001", "week");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "week");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "week");
                    strArr[7] = getComCount(tComCode, "PY005", "week");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "week");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "week");
                    strArr[10] = getComCount(tComCode, "PY002", "week");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "week");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "week");
                    strArr[13] = getComCount(tComCode, "PY004", "week");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "week");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "week");
                    strArr[16] = getComCount(tComCode, "PY009", "week");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "week");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "week");
                    strArr[19] = getComCount(tComCode, "PY015", "week");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "week");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "week");
                    strArr[22] = getComCount(tComCode, firstCom, "week");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "week");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "week");
                    strArr[25] = getComCount(tComCode, secondCom, "week");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "week");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "week");
                    strArr[28] = getComCount(tComCode, thirdCom, "week");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "week");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "week");
                    strArr[31] = getComCount(tComCode, "", "week");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));

                    tlistTable.add(strArr);
                }
                count[35]="";
                tlistTable.add(count);
                count=new String[36];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="月";
                //月
                System.out.println("count month......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    strArr = new String[36];
                    String tComCode = tSSRS.GetText(i, 1);
                    SSRS tRtS = new ExeSQL().execSQL("select name from ldcom where comcode='"+tComCode.substring(0,4)+"'");
                    if (tRtS != null && tRtS.MaxRow > 0)
                    {
                        strArr[0] =  tRtS.GetText(1, 1);
                    }
                    strArr[35] = tSSRS.GetText(i, 2);
                    strArr[1] = "月";
                    //工行 
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "month");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "month");
                    strArr[4] = getComCount(tComCode, "PY001", "month");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "month");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "month");
                    strArr[7] = getComCount(tComCode, "PY005", "month");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "month");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "month");
                    strArr[10] = getComCount(tComCode, "PY002", "month");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "month");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "month");
                    strArr[13] = getComCount(tComCode, "PY004", "month");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "month");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "month");
                    strArr[16] = getComCount(tComCode, "PY009", "month");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "month");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "month");
                    strArr[19] = getComCount(tComCode, "PY015", "month");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "month");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "month");
                    strArr[22] = getComCount(tComCode, firstCom, "month");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "month");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "month");
                    strArr[25] = getComCount(tComCode, secondCom, "month");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "month");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "month");
                    strArr[28] = getComCount(tComCode, thirdCom, "month");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "month");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "month");
                    strArr[31] = getComCount(tComCode, "", "month");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));
                    tlistTable.add(strArr);
                }
                count[35]="";
                tlistTable.add(count);
                count=new String[36];
                for(int i=0;i<count.length;i++){
                    count[i]="0";
                }
                count[0]="合计";
                count[1]="年";
                //年
                System.out.println("count year......");
                for (int i = 1; i <= tSSRS.MaxRow; i++)
                {
                    strArr = new String[36];
                    String tComCode = tSSRS.GetText(i, 1);
                    SSRS tRtS = new ExeSQL().execSQL("select name from ldcom where comcode='"+tComCode.substring(0,4)+"'");
                    if (tRtS != null && tRtS.MaxRow > 0)
                    {
                        strArr[0] =  tRtS.GetText(1, 1);
                    }
                    strArr[35] = tSSRS.GetText(i, 2);
                    strArr[1] = "年";
                    //工行
                    strArr[2] = getWanNengPrem(tComCode, "PY001", "year");
                    strArr[3] = getFeiWanNengPrem(tComCode, "PY001", "year");
                    strArr[4] = getComCount(tComCode, "PY001", "year");
                    count[2]=String.valueOf(Double.parseDouble(strArr[2])+Double.parseDouble(count[2]));
                    count[3]=String.valueOf(Double.parseDouble(strArr[3])+Double.parseDouble(count[3]));
                    count[4]=String.valueOf(Integer.parseInt(strArr[4])+Integer.parseInt(count[4]));
                    //农行
                    strArr[5] = getWanNengPrem(tComCode, "PY005", "year");
                    strArr[6] = getFeiWanNengPrem(tComCode, "PY005", "year");
                    strArr[7] = getComCount(tComCode, "PY005", "year");
                    count[5]=String.valueOf(Double.parseDouble(strArr[5])+Double.parseDouble(count[5]));
                    count[6]=String.valueOf(Double.parseDouble(strArr[6])+Double.parseDouble(count[6]));
                    count[7]=String.valueOf(Integer.parseInt(strArr[7])+Integer.parseInt(count[7]));
                    //中行
                    strArr[8] = getWanNengPrem(tComCode, "PY002", "year");
                    strArr[9] = getFeiWanNengPrem(tComCode, "PY002", "year");
                    strArr[10] = getComCount(tComCode, "PY002", "year");
                    count[8]=String.valueOf(Double.parseDouble(strArr[8])+Double.parseDouble(count[8]));
                    count[9]=String.valueOf(Double.parseDouble(strArr[9])+Double.parseDouble(count[9]));
                    count[10]=String.valueOf(Integer.parseInt(strArr[10])+Integer.parseInt(count[10]));
                    //建行
                    strArr[11] = getWanNengPrem(tComCode, "PY004", "year");
                    strArr[12] = getFeiWanNengPrem(tComCode, "PY004", "year");
                    strArr[13] = getComCount(tComCode, "PY004", "year");
                    count[11]=String.valueOf(Double.parseDouble(strArr[11])+Double.parseDouble(count[11]));
                    count[12]=String.valueOf(Double.parseDouble(strArr[12])+Double.parseDouble(count[12]));
                    count[13]=String.valueOf(Integer.parseInt(strArr[13])+Integer.parseInt(count[13]));
                    //交行
                    strArr[14] = getWanNengPrem(tComCode, "PY009", "year");
                    strArr[15] = getFeiWanNengPrem(tComCode, "PY009", "year");
                    strArr[16] = getComCount(tComCode, "PY009", "year");
                    count[14]=String.valueOf(Double.parseDouble(strArr[14])+Double.parseDouble(count[14]));
                    count[15]=String.valueOf(Double.parseDouble(strArr[15])+Double.parseDouble(count[15]));
                    count[16]=String.valueOf(Integer.parseInt(strArr[16])+Integer.parseInt(count[16]));
                    //邮储
                    strArr[17] = getWanNengPrem(tComCode, "PY015", "year");
                    strArr[18] = getFeiWanNengPrem(tComCode, "PY015", "year");
                    strArr[19] = getComCount(tComCode, "PY015", "year");
                    count[17]=String.valueOf(Double.parseDouble(strArr[17])+Double.parseDouble(count[17]));
                    count[18]=String.valueOf(Double.parseDouble(strArr[18])+Double.parseDouble(count[18]));
                    count[19]=String.valueOf(Integer.parseInt(strArr[19])+Integer.parseInt(count[19]));
                    //1
                    strArr[20] = getWanNengPrem(tComCode, firstCom, "year");
                    strArr[21] = getFeiWanNengPrem(tComCode, firstCom, "year");
                    strArr[22] = getComCount(tComCode, firstCom, "year");
                    count[20]=String.valueOf(Double.parseDouble(strArr[20])+Double.parseDouble(count[20]));
                    count[21]=String.valueOf(Double.parseDouble(strArr[21])+Double.parseDouble(count[21]));
                    count[22]=String.valueOf(Integer.parseInt(strArr[22])+Integer.parseInt(count[22]));
                    //2
                    strArr[23] = getWanNengPrem(tComCode, secondCom, "year");
                    strArr[24] = getFeiWanNengPrem(tComCode, secondCom, "year");
                    strArr[25] = getComCount(tComCode, secondCom, "year");
                    count[23]=String.valueOf(Double.parseDouble(strArr[23])+Double.parseDouble(count[23]));
                    count[24]=String.valueOf(Double.parseDouble(strArr[24])+Double.parseDouble(count[24]));
                    count[25]=String.valueOf(Integer.parseInt(strArr[25])+Integer.parseInt(count[25]));
                    //3
                    strArr[26] = getWanNengPrem(tComCode, thirdCom, "year");
                    strArr[27] = getFeiWanNengPrem(tComCode, thirdCom, "year");
                    strArr[28] = getComCount(tComCode, thirdCom, "year");
                    count[26]=String.valueOf(Double.parseDouble(strArr[26])+Double.parseDouble(count[26]));
                    count[27]=String.valueOf(Double.parseDouble(strArr[27])+Double.parseDouble(count[27]));
                    count[28]=String.valueOf(Integer.parseInt(strArr[28])+Integer.parseInt(count[28]));
                    //其他
                    strArr[29] = getWanNengPrem(tComCode, "", "year");
                    strArr[30] = getFeiWanNengPrem(tComCode, "", "year");
                    strArr[31] = getComCount(tComCode, "", "year");
                    count[29]=String.valueOf(Double.parseDouble(strArr[29])+Double.parseDouble(count[29]));
                    count[30]=String.valueOf(Double.parseDouble(strArr[30])+Double.parseDouble(count[30]));
                    count[31]=String.valueOf(Integer.parseInt(strArr[31])+Integer.parseInt(count[31]));
                    //合计
                    strArr[32] = String.valueOf(Double.parseDouble(strArr[2])
                            + Double.parseDouble(strArr[5])
                            + Double.parseDouble(strArr[8])
                            + Double.parseDouble(strArr[11])
                            + Double.parseDouble(strArr[14])
                            + Double.parseDouble(strArr[17])
                            + Double.parseDouble(strArr[20])
                            + Double.parseDouble(strArr[23])
                            + Double.parseDouble(strArr[26])
                            + Double.parseDouble(strArr[29]));
                    strArr[33] = String.valueOf(Double.parseDouble(strArr[3])
                            + Double.parseDouble(strArr[6])
                            + Double.parseDouble(strArr[9])
                            + Double.parseDouble(strArr[12])
                            + Double.parseDouble(strArr[15])
                            + Double.parseDouble(strArr[18])
                            + Double.parseDouble(strArr[21])
                            + Double.parseDouble(strArr[24])
                            + Double.parseDouble(strArr[27])
                            + Double.parseDouble(strArr[30]));
                    strArr[34] = String.valueOf(Integer.parseInt(strArr[4])
                            + Integer.parseInt(strArr[7])
                            + Integer.parseInt(strArr[10])
                            + Integer.parseInt(strArr[13])
                            + Integer.parseInt(strArr[16])
                            + Integer.parseInt(strArr[19])
                            + Integer.parseInt(strArr[22])
                            + Integer.parseInt(strArr[25])
                            + Integer.parseInt(strArr[28])
                            + Integer.parseInt(strArr[31]));
                    count[32]=String.valueOf(Double.parseDouble(strArr[32])+Double.parseDouble(count[32]));
                    count[33]=String.valueOf(Double.parseDouble(strArr[33])+Double.parseDouble(count[33]));
                    count[34]=String.valueOf(Integer.parseInt(strArr[34])+Integer.parseInt(count[34]));
                    tlistTable.add(strArr);
                }
                count[35]="";
                tlistTable.add(count);
            }
        }

        
        strArr = new String[36];
        for (int i = 0; i < 35; i++)
        {
            strArr[i] = "";
        }
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public String[] getBankComByPrem(){  
        String[] str=new String[3];
        str[0]="";
        str[1]="";
        str[2]="";
        if(mPremType!=null && mPremType.equals("0")){//有效保费
            str= getBankComByPrem1();   		
        }else if(mPremType!=null && mPremType.equals("1")){//承保保费
            str= getBankComByPrem3();
        }else if(mPremType!=null && mPremType.equals("2")){//撤保保费
            str= getBankComByPrem2();
        }
        return str;
    }
    //万能险保费
    public String getWanNengPrem(String comCode, String agentCom, String flag)
    {

        if (mPremType != null && mPremType.equals("0"))
        {//有效保费
            return getWanNengPremBy1(comCode, agentCom, flag);
        }
        else if (mPremType != null && mPremType.equals("1"))
        {//承保保费
            return getWanNengPremBy2(comCode, agentCom, flag);
        }
        else if (mPremType != null && mPremType.equals("2"))
        {//撤保保费
            return getWanNengPremBy3(comCode, agentCom, flag);
        }
        return "0";

    }

    //非万能险保费
    public String getFeiWanNengPrem(String comCode, String agentCom, String flag)
    {
        if (mPremType != null && mPremType.equals("0"))
        {//有效保费
            return getFeiWanNengPremBy1(comCode, agentCom, flag);
        }
        else if (mPremType != null && mPremType.equals("1"))
        {//承保保费
            return getFeiWanNengPremBy2(comCode, agentCom, flag);
        }
        else if (mPremType != null && mPremType.equals("2"))
        {//撤保保费
            return getFeiWanNengPremBy3(comCode, agentCom, flag);
        }
        return "0";
    }

    //网点数量
    public String getComCount(String comCode, String agentCom, String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        String tSql = "select count(1) from lacom where branchtype='3' and branchtype2='01' "
                + " and agentcom like '"
                + agentCom
                + "%' and endflag='Y' and managecom='"
                + comCode
                + "' "
                + " and enddate<='"
                + mEndDate
                + "' and enddate>='"
                + tStartDate + "' with ur ";
        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        return tResult;
    }

    //返回一周中的第一天
    public String getWeekDate(String tDate)
    {
        SSRS tSSRS = new ExeSQL().execSQL("select Date('" + tDate
                + "')-1 DAYS from dual");
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            String beforeDate = tSSRS.GetText(1, 1);
            tSSRS = new ExeSQL().execSQL("select dayofweek('" + beforeDate
                    + "')-1 from dual");
            if (tSSRS != null && tSSRS.MaxRow > 0)
            {
                String week = tSSRS.GetText(1, 1);
                if (week != null && week.equals("1"))
                {
                    return beforeDate;
                }
                else if (week != null && !week.equals("1"))
                {
                    return getWeekDate(beforeDate);
                }
            }
        }
        return null;
    }

    //万能险有效保费
    public String getWanNengPremBy1(String comCode, String agentCom, String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        //退保保费
        String tSQL = "select value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d "
                + " where b.polno=d.polno  and b.FeeOperationType in('CT','XT')"
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U')"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" )) "
                + Payyears1
                + " union all "
                + "select value(sum(getmoney),0) prem from LJAGetEndorse b,lbpol f "
                + " where b.polno=f.polno  and b.FeeOperationType in('CT','XT')"
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U')"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
                + Payyears2;
        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        try
        {
            tResult = String.valueOf(Double.parseDouble(getWanNengPremBy2(
                    comCode, agentCom, flag))
                    + Double.parseDouble(getWanNengPremBy3(comCode, agentCom,
                            flag)) + Double.parseDouble(tResult));
        }
        catch (Exception e)
        {
        }
        return tResult;
    }

    //万能险承保保费
    public String getWanNengPremBy2(String comCode, String agentCom, String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {

            if (mPayIntv.equals("0"))
                condition += "and b.payintv = 0";
            else if (mPayIntv.equals("1"))
                condition += " and b.payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;

            if (tpayyears == 0)
            {
                Payyears1 += " and e.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and e.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }

        String tSQL = "select value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LCPol e where b.payType='ZC'"
                + " and b.polno = e.polno  "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04') "
                + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04'))"
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U')"
                + condition
                + Payyears1
                + " union all "
                + "select value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LBPol f where b.payType='ZC'"
                + " and b.polno = f.polno  "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04') "
                + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04'))"
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U')"
                + condition + Payyears2;
        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        return tResult;
    }

    //万能险撤保保费
    public String getWanNengPremBy3(String comCode, String agentCom, String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;

            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        String tSQL = "select value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d"
                + " where d.polno=b.polno  and  b.FeeOperationType='WT' "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000' "
                + condition
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U') "
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" )) "
                + Payyears1
                + " union all "
                + "select value(sum(getmoney),0) prem from LJAGetEndorse b,lbpol f"
                + " where f.polno=b.polno  and  b.FeeOperationType='WT' "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000' "
                + condition
                + " and b.riskcode in (select riskcode from lmriskapp where kindcode='U') "
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
                + Payyears2;

        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        return tResult;
    }

    //非万能险有效保费
    public String getFeiWanNengPremBy1(String comCode, String agentCom,
            String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        //退保保费
        String tSQL = "select value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d"
                + " where b.polno=d.polno  and b.FeeOperationType in('CT','XT')"
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U')"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" )) "
                + Payyears1
                + " union all "
                + "select value(sum(getmoney),0) prem from LJAGetEndorse b,LBPol f"
                + " where b.polno = f.polno and b.FeeOperationType in('CT','XT')"
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U')"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" )) "
                + Payyears2;

        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        try
        {
            tResult = String.valueOf(Double.parseDouble(getWanNengPremBy2(
                    comCode, agentCom, flag))
                    + Double.parseDouble(getWanNengPremBy3(comCode, agentCom,
                            flag)) + Double.parseDouble(tResult));
        }
        catch (Exception e)
        {
        }
        return tResult;
    }

    //非万能险承保保费
    public String getFeiWanNengPremBy2(String comCode, String agentCom,
            String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }

        String tSQL = "select value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LCPol d where b.payType='ZC'"
                + " and b.polno = d.polno  "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+"  ) "
                + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04' "+PayIntvConditon+"  ))"
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U')"
                + condition
                + Payyears1
                + " union all "
                + " select value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LBPol f where b.payType='ZC'"
                + " and  b.polno=f.polno "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04'"+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04'"+PayIntvConditon+" ))"
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U')"
                + condition + Payyears2;

        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        return tResult;
    }

    //非万能险撤保保费
    public String getFeiWanNengPremBy3(String comCode, String agentCom,
            String flag)
    {
        String tResult = "0";
        String tStartDate = "";
        //设置符合条件的时间段
        if (flag.equals("week"))
        {
            tStartDate = this.mStartDate;
        }
        else if (flag.equals("month"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + temp[1] + "-" + "01";
        }
        else if (flag.equals("year"))
        {
            String[] temp = this.mEndDate.split("-");
            tStartDate = temp[0] + "-" + "01" + "-" + "01";
        }
        SSRS tRS = new ExeSQL().execSQL("select days('"+this.mStartDate+"')-days('"+tStartDate+"') from dual with  ur");
        if (tRS != null && tRS.MaxRow > 0)
        {
            String day = tRS.GetText(1, 1);
            if(day!=null && !day.toUpperCase().equals("NULL")){
                if(Integer.parseInt(day)>0){
                    tStartDate=this.mStartDate;
                }
            }
        }
        String condition = " and b.managecom='"+comCode+"' ";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        if(agentCom != null && !agentCom.equals("")){
            condition +="  and b.agentcom like '"+ agentCom+ "%' "; 
        }
        else{
            condition +=" and b.agentcom not like 'PY001%' "+
            " and b.agentcom not like 'PY005%' "+
            " and b.agentcom not like 'PY002%' "+
            " and b.agentcom not like 'PY004%' "+
            " and b.agentcom not like 'PY009%' "+
            " and b.agentcom not like 'PY015%' ";
            if(!this.firstCom.equals("")){
                condition +="  and b.agentcom not like '"+ this.firstCom+ "%' ";
            }
            if(!this.secondCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.secondCom+ "%' ";
            }
            if(!this.thirdCom.equals("")){
                condition +=" and b.agentcom not like '"+ this.thirdCom+ "%' ";
            }
                  
        }
        
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        String tSQL = "select value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d"
                + " where d.polno=b.polno  and  b.FeeOperationType='WT' "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000' "
                + condition
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U') "
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
                + Payyears1
                + " union all "
                + "select value(sum(getmoney),0) prem from LJAGetEndorse b,LBPol f"
                + " where  b.polno=f.polno and  b.FeeOperationType='WT' "
                + " and b.makedate >='"
                + tStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000' "
                + condition
                + " and b.riskcode not in (select riskcode from lmriskapp where kindcode='U') "
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
                + Payyears2;
        tSQL = "select sum(prem) from ( " + tSQL + " ) a with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            tResult = tSSRS.GetText(1, 1);
        }
        return tResult;
    }
    public String[] getBankComByPrem1(){
        String[] str=new String[3];
        str[0]="";
        str[1]="";
        str[2]="";
        String condition="";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        //撤保退保保费
        String tSQL1 = "select substr(b.agentcom,1,5) agentcom ,value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d "
                + " where b.polno=d.polno  and b.FeeOperationType in('CT','XT','WT')"
                + " and b.makedate >='"
                + this.mStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" )) "
                + Payyears1
                +" group by substr(b.agentcom,1,5)  "
                + " union all "
                + "select substr(b.agentcom,1,5) agentcom ,value(sum(getmoney),0) prem from LJAGetEndorse b,lbpol f "
                + " where b.polno=f.polno  and b.FeeOperationType in('CT','XT','WT')"
                + " and b.makedate >='"
                + this.mStartDate
                + "' and b.makedate<='"
                + this.mEndDate
                + "' and b.GrpPolNo = '00000000000000000000'"
                + condition
                + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
                + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
                + Payyears2
                +" group by substr(b.agentcom,1,5)  ";
 
//      承保保费
        String tSQL2 = "select substr(b.agentcom,1,5) agentcom,value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LCPol d where b.payType='ZC'"
            + " and b.polno = d.polno  "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + mEndDate
            + "' and b.GrpPolNo = '00000000000000000000'"
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+")  "
            + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04' "+PayIntvConditon+" ))"
            + condition
            + Payyears1
            +" group by substr(b.agentcom,1,5)  "
            + " union all "
            + " select substr(b.agentcom,1,5) agentcom,value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LBPol f where b.payType='ZC'"
            + " and  b.polno=f.polno "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + mEndDate
            + "' and b.GrpPolNo = '00000000000000000000'"
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+") "
            + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04' "+PayIntvConditon+"))"
            + condition + Payyears2
            +" group by substr(b.agentcom,1,5)  ";
        String tSQL="select agentcom,sum(prem) prem from ( "+tSQL1+" union all "+tSQL2+"  ) a  group by agentcom order by prem desc fetch first 3 rows only with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            for(int i=0;i<tSSRS.MaxRow;i++){
                str[i]=tSSRS.GetText(i+1, 1);
            }
        }
        return str;
    }
    public String[] getBankComByPrem2(){
        String[] str=new String[3];
        str[0]="";
        str[1]="";
        str[2]="";
        String condition="";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
        //撤保保费
        String tSQL = "select substr(b.agentcom,1,5) agentcom ,value(sum(getmoney),0) prem from LJAGetEndorse b,lcpol d"
            + " where d.polno=b.polno  and  b.FeeOperationType='WT' "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + this.mEndDate
            + "' and b.GrpPolNo = '00000000000000000000' "
            + condition
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
            + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
            + Payyears1
            +" group by substr(b.agentcom,1,5)  "
            + " union all "
            + "select substr(b.agentcom,1,5) agentcom ,value(sum(getmoney),0) prem from LJAGetEndorse b,LBPol f"
            + " where  b.polno=f.polno and  b.FeeOperationType='WT' "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + this.mEndDate
            + "' and b.GrpPolNo = '00000000000000000000' "
            + condition
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ) "
            + " or exists (select contno from lbcont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+" ))"
            + Payyears2
            +" group by substr(b.agentcom,1,5)  ";
        tSQL="select agentcom,sum(prem) prem from ( "+tSQL+" ) a  group by agentcom order by prem  fetch first 3 rows only with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            for(int i=0;i<tSSRS.MaxRow;i++){
                str[i]=tSSRS.GetText(i+1, 1);
            }
        }
        return str;
    }
    public String[] getBankComByPrem3(){
        String[] str=new String[3];
        str[0]="";
        str[1]="";
        str[2]="";
        String condition="";
        if (this.mRiskCode != null && !this.mRiskCode.equals(""))
        {
            condition += " and b.riskcode='" + mRiskCode + "' ";
        }
        String PayIntvConditon="";
        if (this.mPayIntv != null && !this.mPayIntv.equals(""))
        {
            if (mPayIntv.equals("0"))
                PayIntvConditon += " and payintv = 0";
            else if (mPayIntv.equals("1"))
                PayIntvConditon += " and payintv >= 1";
        }
        String Payyears1 = "";
        String Payyears2 = "";
        if (this.mPayYears != null && !this.mPayYears.equals(""))
        {
            int tpayyears = 0;
            if (mPayYears.equals("0"))
                tpayyears = 3;
            else if (mPayYears.equals("1"))
                tpayyears = 5;
            else if (mPayYears.equals("2"))
                tpayyears = 8;
            else if (mPayYears.equals("3"))
                tpayyears = 10;
            if (tpayyears == 0)
            {
                Payyears1 += " and d.payyears >= 0  ";
                Payyears2 += " and f.payyears >= 0  ";
            }
            else
            {
                Payyears1 += " and d.payyears =" + tpayyears;
                Payyears2 += " and f.payyears =" + tpayyears;
            }
        }
//      承保保费
        String tSQL = "select substr(b.agentcom,1,5) agentcom,value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LCPol d where b.payType='ZC'"
            + " and b.polno = d.polno  "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + mEndDate
            + "' and b.GrpPolNo = '00000000000000000000'"
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+")  "
            + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04' "+PayIntvConditon+" ))"
            + condition
            + Payyears1
            +" group by substr(b.agentcom,1,5)  "
            + " union all "
            + " select substr(b.agentcom,1,5) agentcom,value(sum(b.sumactupaymoney),0) prem from LJAPayPerson b ,LBPol f where b.payType='ZC'"
            + " and  b.polno=f.polno "
            + " and b.makedate >='"
            + this.mStartDate
            + "' and b.makedate<='"
            + mEndDate
            + "' and b.GrpPolNo = '00000000000000000000'"
            + " and ( exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04' "+PayIntvConditon+") "
            + " or exists (select contno from lbcont d where b.contno=d.contno and d.salechnl='04' "+PayIntvConditon+"))"
            + condition + Payyears2
            +" group by substr(b.agentcom,1,5)  ";
        tSQL="select agentcom,sum(prem) prem from ( "+tSQL+" ) a  group by agentcom order by prem desc fetch first 3 rows only with ur";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.MaxRow > 0)
        {
            for(int i=0;i<tSSRS.MaxRow;i++){
                str[i]=tSSRS.GetText(i+1, 1);
            }
        }
        return str;
    }

    public boolean prepareOutputData()
    {
        try
        {
            mInputData = new VData();
            mInputData.add(mMap);
        }
        catch (Exception e)
        {
            CError.buildErr(this, "提交数据失败！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
