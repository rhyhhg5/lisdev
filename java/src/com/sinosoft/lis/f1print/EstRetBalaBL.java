package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class EstRetBalaBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
//    private String sYear = ""; //年
//    private String sMonth = ""; // 月
//    private String sDay = ""; // 日
    private String mSql = null;
    private String Sqla = null;
    private String Sqlb = null;
    private String mOutXmlPath = null;

    private String AccountDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    SSRS SSRSa = new SSRS();
    SSRS SSRSb = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public EstRetBalaBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");
 
        ExeSQL tExeSQLl = new ExeSQL();
        Sqla = "select DATE(substr('"+AccountDate+"',1,5)||'01-01') from dual where 1=1";
        System.out.println(Sqla);
        String SSRSa = tExeSQLl.getOneValue(Sqla);
        System.out.println(SSRSa);
        
        ExeSQL tExeSQLll = new ExeSQL();
        Sqlb = "SELECT LAST_DAY('"+AccountDate+"') FROM dual where 1=1";
        System.out.println(Sqlb);
        String SSRSb = tExeSQLll.getOneValue(Sqlb);
        System.out.println(SSRSb);
        
 
            ExeSQL tExeSQL = new ExeSQL();
            mSql =  " Select char(rownumber() over()) as id, bb.* From (select distinct(select codename from ficodetrans where codetype = 'FXManageCom' and code = a.managecom) as 省级公司," 
            	   +"(select codename from ficodetrans where codetype = 'ManageCom'and code = c.managecom) 三级机构名称,(select ProjectNo from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目编码," 
            	   +"(select ProjectName from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目名称,"
            	   +"a.contno 保单号,(case c.CoInsuranceFlag when '1' then '是' else '否' end ) as 是否共保,c.grpname 投保单位,a.riskcode 险种,a.costcenter 成本中心, "
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','Y','"+AccountDate+"') 预估结余返还本年累计金额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','','"+AccountDate+"') 预估结余返还当月发生额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','Y','"+AccountDate+"') 实际结算结余返还本年累计金额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','','"+AccountDate+"') 实际结算结余返还当月发生额（元）"
            	   +" From fivoucherdatadetail a,fiabstandarddata b,lcgrpcont c "
	               +"where 1=1 and a.serialno = b.serialno and a.contno=c.grpcontno and b.feetype in ('BJ','YGBJ','YGBJ_F') and a.accountcode = '6031000000' and a.vouchertype = 'X4'"
	               +"and (db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','Y','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','Y','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','','"+AccountDate+"')<>0)"	   	 	
	               +"and a.accountdate between '"+SSRSa+"'and '"+SSRSb+"' and a.checkflag = '00' "
	               +" union "
	               +"select distinct (select codename from ficodetrans where codetype = 'FXManageCom' and code = a.managecom) as 省级公司," 
            	   +"(select codename from ficodetrans where codetype = 'ManageCom'and code = c.managecom) 三级机构名称,(select ProjectNo from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目编码," 
            	   +"(select ProjectName from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目名称,"
            	   +"a.contno 保单号,(case c.CoInsuranceFlag when '1' then '是' else '否' end ) as 是否共保,c.grpname 投保单位,a.riskcode 险种,a.costcenter 成本中心, "
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','Y','"+AccountDate+"') 预估结余返还本年累计金额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','','"+AccountDate+"') 预估结余返还当月发生额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','Y','"+AccountDate+"') 实际结算结余返还本年累计金额（元）,"
            	   +"db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','','"+AccountDate+"') 实际结算结余返还当月发生额（元）"
            	   +" From fivoucherdatadetail a,fiabstandarddata b,lbgrpcont c "
	               +"where 1=1 and a.serialno = b.serialno and a.contno=c.grpcontno and b.feetype in ('BJ','YGBJ','YGBJ_F') and a.accountcode = '6031000000' and a.vouchertype = 'X4'"
	               +"and (db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','Y','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'YGBJ','','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','Y','"+AccountDate+"')<>0 or db2inst1.LF_BJMONEY(a.contno,a.riskcode,'BJ','','"+AccountDate+"')<>0)"	   	 	
	               +"and a.accountdate between '"+SSRSa+"'and '"+SSRSb+"' and a.checkflag = '00' "
                   + ")as bb "; 
            System.out.println(mSql);
//            SSRS ttSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(tSSRS.getMaxRow()<=0)
            {
            	buiError("dealData","没有符合条件的信息!");
                return false;
            }
 
        return true;
    }
    
	private boolean createFile(){
		
//		 1、查看数据行数
	  	  int dataSize = this.tSSRS.getMaxRow();
	  	  if(this.tSSRS==null||this.tSSRS.getMaxRow()<1)
	  	  {
	  		  buiError("dealData","没有符合条件的信息!");
	  		  return false;
	  	  }
	        // 定义一个二维数组 
	  	   String[][] mToExcel = new String[dataSize + 6][30];
//	       mToExcel = new String[10000][30];
	        mToExcel[0][4] = "结余返还统计表";
//	        mToExcel[1][5] = EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月份，实际结余返还金额统计表";
	        mToExcel[1][6] = AccountDate+"结余返还统计表";
	        mToExcel[2][8] = "编制单位：" + getName();

	        mToExcel[3][0] = "序号";
	        mToExcel[3][1] = "省级公司";
	        mToExcel[3][2] = "三级机构名称";
	        mToExcel[3][3] = "项目编码";
	        mToExcel[3][4] = "项目名称";
	        mToExcel[3][5] = "保单号";
	        mToExcel[3][6] = "是否共保";
	        mToExcel[3][7] = "投保单位";
	        mToExcel[3][8] = "险种";
	        mToExcel[3][9] = "成本中心";
	        mToExcel[3][10] = "预估结余返还本年累计金额（元）";
	        mToExcel[3][11] = "预估结余返还当月发生额（元）";
	        mToExcel[3][12] = "实际结算结余返还本年累计金额（元）";
	        mToExcel[3][13] = "实际结算结余返还当月发生额（元）";
	        
	        int printNum = 3;
	        
	           int no = 1;
	            for (int row = 1; row <= tSSRS.getMaxRow(); row++)
	            {
	            	
	                for (int col = 1; col <= tSSRS.getMaxCol(); col++)
	                {       
	                        if (tSSRS.GetText(row, col).equals("null"))
	                        {
	                            mToExcel[row + printNum][col - 1] = "";
	                        }
	                        else
	                        {
	                            mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
	                        }
	                        }
	              
	                mToExcel[row + printNum][0] = no + "";
	                no++;}           

	        mToExcel[printNum + tSSRS.getMaxRow() + 2][0] = "制表";
	        mToExcel[printNum + tSSRS.getMaxRow() + 2][14] = "日期：" + mCurDate;
	            
		
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        AccountDate = (String) tf.getValueByName("AccountDate");      
        mMail = (String) tf.getValueByName("toMail");
        
        System.out.println("mMail-----"+mMail);

        if (AccountDate == null || mOutXmlPath == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    
    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "EstRetBalaBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
//        tTransferData.setNameAndValue("EndDate", "2012-05-01");
//        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}
