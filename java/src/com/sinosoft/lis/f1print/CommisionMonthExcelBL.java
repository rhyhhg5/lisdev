package com.sinosoft.lis.f1print;

/**
 * X8-佣金月结单
 * @date 2013-09-23
 * @author yan
 */
import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CommisionMonthExcelBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	String sWageNo = ""; // 月结单 格式：200807 yyyymm
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	private String tOutXmlPath = "";
	private String msql = "";

	public CommisionMonthExcelBL() {
	}

	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("进入 CommisionMonthExcelBL...X8");
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) { // 打印提数
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}


	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		String[] sDate = ((String) cInputData.get(0)).split("-"); // 获得WageNo
		sWageNo = sDate[0] + sDate[1];
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "CommisionMonthExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
 
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("WageNo", sWageNo);

        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
      //tServerPath = "E:/lisdev/ui/";
      //  tServerPath= "E:/DYL-workspace/Picch/ui/";
             
        int no = 6;     //初始化打印行数

        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[] {
                "ZJYJ",   // 直接佣金
        };

        for (int i = 0; i < getTypes.length; i++) {

            msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = tExeSQL.execSQL(msql);

            no=no+tSSRS.MaxRow+1;
            //合计金额
            msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",getTypes[i] + "Z");
            tSSRS = tExeSQL.execSQL(msql);
            System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
        }
        no=no+tSSRS.MaxRow+2;
       
        no=no+2;     
        
        String[] ZJTXSQLNode = new String[] {
				"TXZJYJ",   // 团险直接佣金
		};

		for (int i = 0; i < ZJTXSQLNode.length; i++) {

			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJTXSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			no=no+tSSRS.MaxRow+1;
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZJTXSQLNode[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
		}   
        no=no+tSSRS.MaxRow+2; 
        no=no+2; 
		String[] ZJHDDLSQLNode = new String[] {
				"HDZYDL",   // 互动直接佣金代理制
		};

		for (int i = 0; i < ZJHDDLSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJHDDLSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
		}
		    no=no+tSSRS.MaxRow+2; 
        no=no+2;
        String[] ZJHDYGSQLNode = new String[] {
				"HDZYYG",   // 互动直接佣金员工制
		};
		for (int i = 0; i < ZJHDYGSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJHDYGSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
		}
        no=no+tSSRS.MaxRow+2; 
        no=no+2;
         String[] ZJYDYGSQLNode = new String[] {
				"YDZYDL",   // 银代直接佣金代理制
		};

		for (int i = 0; i < ZJYDYGSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJYDYGSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
		}
		    no=no+tSSRS.MaxRow+2; 
        no=no+2;
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }
    
	/**
	 * 新的打印数据方法
	 * @return
	 */
	private boolean getPrintData() {

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		SSRS ttSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("WageNo", sWageNo);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//tServerPath = "E:/lisdev/ui/";
    //    tServerPath= "E:/DYL-workspace/Picch/ui/";
		
		String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		
		int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+25][10];
		mToExcel[1][0] = "佣金计提月结单";
		mToExcel[2][0] = "机构："+manageCom;
		mToExcel[2][2] = "月度："+sWageNo;
		mToExcel[4][2] = "直接佣金（个险）";
		mToExcel[5][0] = "险种代码";
		mToExcel[5][1] = "期缴(新单)";
		mToExcel[5][2] = "趸缴(新单)";
		mToExcel[5][3] = "续单";
		mToExcel[5][4] = "小计";
		
		int no = 6; 	//初始化打印行数

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] {
				"ZJYJ",   // 直接佣金
		};

		for (int i = 0; i < getTypes.length; i++) {

			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int row = 1; row <= tSSRS.MaxRow; row++){
				for(int col = 1; col <= tSSRS.MaxCol; col++){
					if(col == 1){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					if(col == 2){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					if(col == 3){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					if(col == 4){
						mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
					}
					mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
				}
			}
			no=no+tSSRS.MaxRow+1;
			//合计金额
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",getTypes[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "合计";
				mToExcel[no][1] = "0.00";
				mToExcel[no][2] = "0.00";
				mToExcel[no][3] = "0.00";
				mToExcel[no][4] = "0.00";
			}else{
				for(int row = 1; row <= tSSRS.MaxRow; row++){
					for(int col = 1; col <= tSSRS.MaxCol; col++){
						if(col != 1){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
					}
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "间接佣金（个险）";
		no=no+2;
		mToExcel[no-1][0] = "培训";
		mToExcel[no-1][1] = "奖励";
		mToExcel[no-1][2] = "其他";
		mToExcel[no-1][3] = "津贴";
		mToExcel[no-1][4] = "财务支持费用";
//		mToExcel[no-1][5] = "交叉销售业务支出";
		mToExcel[no-1][5] = "健代产";
		mToExcel[no-1][6] = "健代寿";
		mToExcel[no-1][7] = "其他";
		mToExcel[no-1][8] = "小计";
        // 间接佣金 提取 从xml文件中要提取的SQL的节点
		String[] JJYJSQLNode = new String[]{
				"JJYJJL",// 间接佣金-奖励
                "JJYJJT",// 间接佣金-津贴
				"JJYJCW",// 间接佣金-财务支持费用
				//"JJYJJX",//间接佣金-交叉销售业务支出
				"JJYJJDC",//间接佣金-交叉销售业务支出-健代产
				"JJYJJDS",//间接佣金-交叉销售业务支出-健代寿				
				"JJYJZ"  // 间接佣金-合计
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int i=0; i<JJYJSQLNode.length; i++){
			String sql  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",JJYJSQLNode[i]);
			tSSRS = tExeSQL.execSQL(sql);
			if(JJYJSQLNode[i].equals("JJYJJL")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][1]="0.00";
				}else{
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}	
			}else if(JJYJSQLNode[i].equals("JJYJJT")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][3] = "0.00";
				}else{
					mToExcel[no][3] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(JJYJSQLNode[i].equals("JJYJCW")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][4] = "0.00";
				}else{
					mToExcel[no][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(JJYJSQLNode[i].equals("JJYJJDC")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][5] = "0.00";
				}else{
					mToExcel[no][5] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(JJYJSQLNode[i].equals("JJYJJDS")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][6] = "0.00";
				}else{
					mToExcel[no][6] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(JJYJSQLNode[i].equals("JJYJZ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][8] = "0.00";
				}else{
					mToExcel[no][8] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "直接佣金（团险）";
		no=no+2;
		mToExcel[no-1][0] = "险种代码";
		mToExcel[no-1][1] = "期缴(新单)";
		mToExcel[no-1][2] = "趸缴(新单)";
		mToExcel[no-1][3] = "续单";
		mToExcel[no-1][4] = "小计";
		String[] ZJTXSQLNode = new String[] {
				"TXZJYJ",   // 团险直接佣金
		};

		for (int i = 0; i < ZJTXSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJTXSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
				for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
					if(tcol == 1){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 2){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 3){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 4){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
				}
			}
			no=no+tSSRS.MaxRow+1;
			//合计金额
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZJTXSQLNode[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "合计";
				mToExcel[no][1] = "0.00";
				mToExcel[no][2] = "0.00";
				mToExcel[no][3] = "0.00";
				mToExcel[no][4] = "0.00";
			}else{
				for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
					for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
						if(tcol != 1){
							mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
						}
						mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
					}
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "间接佣金（团险）";
		no=no+2;
		mToExcel[no-1][0] = "代理制津贴";
		mToExcel[no-1][1] = "代理制奖励";
		mToExcel[no-1][2] = "代理制交叉";
		mToExcel[no-1][3] = "员工制工资";
		mToExcel[no-1][4] = "员工制提奖";
		mToExcel[no-1][5] = "代理制佣金总计";
		mToExcel[no-1][6] = "员工制佣金总计";
        // 直接佣金 提取 从xml文件中要提取的SQL的节点
		String[] TXYJSQLNode = new String[]{
				"TXDLJT",// 团险代理制津贴
				"TXDLJL",// 团险代理制奖励
				"TXDLJC",// 团险代理制交叉
				"TXYGGZ",// 团险员工制工资
				"TXYGTJ",// 团险员工制提奖
				"TXDLZJ",// 团险代理制佣金总计
				"TXYGZJ"// 团险员工制佣金总计
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int K=0; K<TXYJSQLNode.length; K++){
			String sqlT  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",TXYJSQLNode[K]);
			System.out.println("sqlT"+sqlT);
			tSSRS = tExeSQL.execSQL(sqlT);
			System.out.println("第"+K+"个");
			if(TXYJSQLNode[K].equals("TXDLJT")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][0]="0.00";
				}else{
					mToExcel[no][0] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXDLJL")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][1] = "0.00";
				}else{
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXDLJC")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][2] = "0.00";
				}else{
					mToExcel[no][2] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXYGGZ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][3] = "0.00";
				}else{
					mToExcel[no][3] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXYGTJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][4] = "0.00";
				}else{
					mToExcel[no][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXDLZJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][5] = "0.00";
				}else{
					mToExcel[no][5] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(TXYJSQLNode[K].equals("TXYGZJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][6] = "0.00";
				}else{
					mToExcel[no][6] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "直接佣金代理制（互动）";
		no=no+2;
		mToExcel[no-1][0] = "险种代码";
		mToExcel[no-1][1] = "期缴(新单)";
		mToExcel[no-1][2] = "趸缴(新单)";
		mToExcel[no-1][3] = "续单";
		mToExcel[no-1][4] = "小计";
		String[] ZJHDDLSQLNode = new String[] {
				"HDZYDL",   // 互动直接佣金代理制
		};

		for (int i = 0; i < ZJHDDLSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJHDDLSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
				for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
					if(tcol == 1){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 2){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 3){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 4){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
				}
			}
			no=no+tSSRS.MaxRow;
			//合计金额
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZJHDDLSQLNode[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "合计";
				mToExcel[no][1] = "0.00";
				mToExcel[no][2] = "0.00";
				mToExcel[no][3] = "0.00";
				mToExcel[no][4] = "0.00";
			}else{
				for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
					for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
						if(tcol != 1){
							mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
						}
						mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
					}
				}
			}
		}
		no=no+tSSRS.MaxRow+2; 
		mToExcel[no][3] = "直接佣金员工制（互动）";
		no=no+2;
		mToExcel[no-1][0] = "险种代码";
		mToExcel[no-1][1] = "期缴(新单)";
		mToExcel[no-1][2] = "趸缴(新单)";
		mToExcel[no-1][3] = "续单";
		mToExcel[no-1][4] = "小计";
		String[] ZJHDYGSQLNode = new String[] {
				"HDZYYG",   // 互动直接佣金员工制
		};

		for (int i = 0; i < ZJHDYGSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJHDYGSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
				for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
					if(tcol == 1){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 2){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 3){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 4){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
				}
			}
			no=no+tSSRS.MaxRow;
			//合计金额
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZJHDYGSQLNode[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "合计";
				mToExcel[no][1] = "0.00";
				mToExcel[no][2] = "0.00";
				mToExcel[no][3] = "0.00";
				mToExcel[no][4] = "0.00";
			}else{
				for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
					for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
						if(tcol != 1){
							mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
						}
						mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
					}
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "间接佣金（互动）";
		no=no+2;
		mToExcel[no-1][0] = "代理制津贴";
		mToExcel[no-1][1] = "代理制奖励";
		mToExcel[no-1][2] = "代理制交叉";
		mToExcel[no-1][3] = "员工制工资";
		mToExcel[no-1][4] = "员工制提奖";
		mToExcel[no-1][5] = "代理制佣金总计";
		mToExcel[no-1][6] = "员工制佣金总计"; 
		String[] HDYJSQLNode = new String[]{
				"HDDLJT",// 互动代理制津贴
				"HDDLJL",// 互动代理制奖励
				"HDDLJC",// 互动代理制交叉
				"HDYGGZ",// 互动员工制工资
				"HDYGTJ",// 互动员工制提奖
				"HDDLZJ",// 互动代理制佣金总计
				"HDYGZJ"// 互动员工制佣金总计
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int K=0; K<HDYJSQLNode.length; K++){
			String sqlT  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",HDYJSQLNode[K]);
			System.out.println("sqlT"+sqlT);
			tSSRS = tExeSQL.execSQL(sqlT);
			System.out.println("第"+K+"个");
			if(HDYJSQLNode[K].equals("HDDLJT")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][0]="0.00";
				}else{
					mToExcel[no][0] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDDLJL")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][1] = "0.00";
				}else{
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDDLJC")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][2] = "0.00";
				}else{
					mToExcel[no][2] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDYGGZ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][3] = "0.00";
				}else{
					mToExcel[no][3] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDYGTJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][4] = "0.00";
				}else{
					mToExcel[no][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDDLZJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][5] = "0.00";
				}else{
					mToExcel[no][5] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(HDYJSQLNode[K].equals("HDYGZJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][6] = "0.00";
				}else{
					mToExcel[no][6] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "直接佣金（银代）";
		no=no+2;
		mToExcel[no-1][0] = "险种代码";
		mToExcel[no-1][1] = "期缴(新单)";
		mToExcel[no-1][2] = "趸缴(新单)";
		mToExcel[no-1][3] = "续单";
		mToExcel[no-1][4] = "小计";
		String[] ZJYDYGSQLNode = new String[] {
				"YDZYDL",   // 银代直接佣金代理制
		};

		for (int i = 0; i < ZJYDYGSQLNode.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", ZJYDYGSQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
				for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
					if(tcol == 1){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 2){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 3){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					if(tcol == 4){
						mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
					}
					mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
				}
			}
			no=no+tSSRS.MaxRow;
			//合计金额
			msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZJYDYGSQLNode[i] + "Z");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println("tSSRS.MaxRow---"+tSSRS.MaxRow);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "合计";
				mToExcel[no][1] = "0.00";
				mToExcel[no][2] = "0.00";
				mToExcel[no][3] = "0.00";
				mToExcel[no][4] = "0.00";
			}else{
				for(int trow = 1; trow <= tSSRS.MaxRow; trow++){
					for(int tcol = 1; tcol <= tSSRS.MaxCol; tcol++){
						if(tcol != 1){
							mToExcel[no+trow-1][tcol-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(trow, tcol)));
						}
						mToExcel[no+trow-1][tcol-1] = tSSRS.GetText(trow, tcol);
					}
				}
			}
		}
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "间接佣金（银代）";
		mToExcel[no+1][0] = "代理制津贴";
		mToExcel[no+1][1] = "代理制奖励";
		mToExcel[no+1][2] = "代理制交叉";
		mToExcel[no+1][3] = "员工制工资";
		mToExcel[no+1][4] = "员工制提奖";
		mToExcel[no+1][5] = "员工制福利";
		mToExcel[no+1][6] = "代理制佣金总计";
		mToExcel[no+1][7] = "员工制佣金总计";
		no=no+2;
        // 间接佣金 提取 从xml文件中要提取的SQL的节点
		String[] YDSQLNode = new String[]{
				"YDDLJT",// 银代代理制津贴
				"YDDLJL",// 银代代理制奖励
				"YDDLJC",// 银代代理制交叉
				"YDYGGZ",// 银代员工制工资
				"YDYGTJ",// 银代员工制提奖
				"YDYGFL",// 银代员工制福利
				"YDDLZJ",// 银代代理制佣金总计
				"YDYGZJ"// 银代员工制佣金总计
				
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int J=0; J<YDSQLNode.length; J++){
			String sqlL  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",YDSQLNode[J]);
			ttSSRS = tExeSQL.execSQL(sqlL);
			if(YDSQLNode[J].equals("YDDLJT")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][0]="0.00";
				}else{
					mToExcel[no][0] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}	
			}else if(YDSQLNode[J].equals("YDDLJL")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][1] = "0.00";
				}else{
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}
			}else if(YDSQLNode[J].equals("YDDLJC")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][2] = "0.00";
				}else{
					mToExcel[no][2] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}
			}else if(YDSQLNode[J].equals("YDYGGZ")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][3] = "0.00";
				}else{
					mToExcel[no][3] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}
			}else if(YDSQLNode[J].equals("YDYGTJ")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][4] = "0.00";
				}else{
					mToExcel[no][4] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}
				
			}else if(YDSQLNode[J].equals("YDYGFL")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][5] = "0.00";
				}else{
					mToExcel[no][5] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}
			}
			else if(YDSQLNode[J].equals("YDDLZJ")){
					if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
						mToExcel[no][6] = "0.00";
					}else{
						mToExcel[no][6] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
					}	
			}
			else if(YDSQLNode[J].equals("YDYGZJ")){
				if(ttSSRS.GetText(1, 1).equals("")||ttSSRS.GetText(1, 1)==null ||ttSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][7] = "0.00";
				}else{
					mToExcel[no][7] = new DecimalFormat("0.00").format(Double.valueOf(ttSSRS.GetText(1, 1)));
				}	
			}
		}
		
		//以下为个险佣金集中代收付部分
		no=no+tSSRS.MaxRow+2;
		mToExcel[no][3] = "税及税后佣金(个险)";
		no=no+3;
		mToExcel[no-1][0] = "增值税";
		mToExcel[no-1][1] = "城建税";
		mToExcel[no-1][2] = "教育费附加";
		mToExcel[no-1][3] = "地方教育费附加";
		mToExcel[no-1][4] = "其他附加";
		mToExcel[no-1][5] = "个人所得税";
		mToExcel[no-1][6] = "养老金计提";
		mToExcel[no-1][7] = "税后佣金";
		mToExcel[no-1][8] = "小计";
        // 间接佣金 提取 从xml文件中要提取的SQL的节点
		String[] GXYJSQLNode = new String[]{
				"GXYJZZ",// 个险佣金-增值税
                "GXYJCJ",// 个险佣金-城建税
				"GXYJFJ",// 个险佣金-教育费附加
				"GXYJDF",// 个险佣金-地方教育费附加
				"GXYJQF",// 个险佣金-其他附加
				"GXYJIT",// 个险佣金-个人所得税
				"GXYJYL",// 个险佣金-养老金计提
				"GXYJSY",// 个险佣金-税后佣金
				"GXYJZJ"  // 个险佣金-合计
				
		};
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int i=0; i<GXYJSQLNode.length; i++){
			String sql  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",GXYJSQLNode[i]);
			System.out.println("sql:"+sql);
			tSSRS = tExeSQL.execSQL(sql);
			if(GXYJSQLNode[i].equals("GXYJZZ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][0]="0.00";
				}else{
					mToExcel[no][0] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}	
			}else if(GXYJSQLNode[i].equals("GXYJCJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][1] = "0.00";
				}else{
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJFJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][2] = "0.00";
				}else{
					mToExcel[no][2] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJDF")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][3] = "0.00";
				}else{
					mToExcel[no][3] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJQF")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][4] = "0.00";
				}else{
					mToExcel[no][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJIT")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][5] = "0.00";
				}else{
					mToExcel[no][5] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJYL")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][6] = "0.00";
				}else{
					mToExcel[no][6] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJSY")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][7] = "0.00";
				}else{
					mToExcel[no][7] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}else if(GXYJSQLNode[i].equals("GXYJZJ")){
				if(tSSRS.GetText(1, 1).equals("")||tSSRS.GetText(1, 1)==null ||tSSRS.GetText(1, 1).equals("null")){
					mToExcel[no][8] = "0.00";
				}else{
					mToExcel[no][8] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}
			}						
		}
		try{
			System.out.println("X8--tOutXmlPath:"+tOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = {PubFun.getCurrentDate()};
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(tOutXmlPath);
			System.out.println("生成X8文件完成");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			buildError("getPrintData", e.toString());
		}
		return true;
	}
}
