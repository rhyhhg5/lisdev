package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLClaimESReportBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLUserQuantyReportBL {
    public LLUserQuantyReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mStartDate = "";

    private String mEndDate = "";

    private String mManageCom = "";

    private String mManageComName = "";

    private String mCaseType = "";

    private String mCaseTypeName = "";

    private String mOperator = "";

    private String[][] mShowDataList = null;

    private XmlExport mXmlExport = null;

    private String[] mDataList = null;

    private ListTable mListTable = new ListTable();

    private String currentDate = PubFun.getCurrentDate();

    private String mFileNameB = "";

    private String mMakeDate = "";

    private TransferData mTransferData = new TransferData();
    
	/** 统计类型 */
	private String mStatsType = "";


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        } else {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("tFileNameB", mFileNameB);
            tTransferData.setNameAndValue("tMakeDate", mMakeDate);
            tTransferData.setNameAndValue("tOperator", mOperator);
            LLPrintSave tLLPrintSave = new LLPrintSave();
            VData tVData = new VData();
            tVData.addElement(tTransferData);
            if (!tLLPrintSave.submitData(tVData, "")) {
                return false;
            }
        }

        return true;
    }

    private boolean getDataList() {
        int tConsultSum = 0;
        int tNoticeSum = 0;
        int tAppSum = 0;
        int tAppealSum = 0;
        int tErrSum = 0;
        int tTypeSum = 0;

        int tRegisteSum = 0;
        int tEndSum = 0;
        int tUWSum = 0;
        int tSpotSum = 0;
        int tBackSum = 0;
        int tOPSum = 0;
        int tDealSum = 0;

        SSRS tSSRS = new SSRS();
        String sql = "";
        
        if("3".equals(mStatsType) || "2".equals(mStatsType)){
        	sql = "select usercode,username from llsocialclaimuser where comcode like '"+mManageCom+"%' " +
        		" union all " +
        		" select usercode,username from llclaimuser where comcode like '"
                + mManageCom + "%' order by usercode "
        		+ "with ur";
        }else{
        	sql = "select usercode,username from llclaimuser where comcode like '"
                + mManageCom + "%' order by usercode with ur";
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println(sql);
        tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS.getMaxRow() < 1) {
            buildError("getDataList", "该机构下无理赔用户");
            return false;
        }

        String tCaseTypeSQL = "";

        if ("1".equals(mCaseType)) {
            tCaseTypeSQL =
                    " and not exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5') ";
        } else if ("2".equals(mCaseType)) {
            tCaseTypeSQL =
                    " and exists (select 1 from llregister where rgtno=a.rgtno and applyertype='5') ";
        }else{
        	tCaseTypeSQL = "";
        }

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String tUserCode = "";
            String Info[] = new String[15];
            tUserCode = tSSRS.GetText(i, 1);
            Info[0] = tUserCode;
            Info[1] = tSSRS.GetText(i, 2);
            if (tUserCode == null || "".equals(tUserCode)) {
                buildError("getDataList", "用户编码空");
                return false;
            }
            
            String mStatsTypeSql1 = "";//申请、申诉、纠错//审定、抽检//回退//处理量
            String mStatsTypeSql2 = "";//受理
            String mStatsTypeSql3 = "";//处理完毕1
            String mStatsTypeSql4 = "";//处理完毕2
            
            // 对统计类型进行判断
            if("1".equals(mStatsType)){//商业保险
            	mStatsTypeSql1 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=a.caseno fetch first 1 rows only)) = 'N' " ;
            	mStatsTypeSql2 = " and CHECKGRPCONT((select z.grpcontno from llcasepolicy z where z.caseno=a.caseno fetch first 1 rows only)) = 'N' ";
            	mStatsTypeSql3 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=d.caseno fetch first 1 rows only)) = 'N' " ;
            	mStatsTypeSql4 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=b.caseno fetch first 1 rows only)) = 'N' ";
            }else if("2".equals(mStatsType)){//社会保险
            	mStatsTypeSql1 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=a.caseno fetch first 1 rows only)) = 'Y' " ;
            	mStatsTypeSql2 = " and CHECKGRPCONT((select z.grpcontno from llcasepolicy z where z.caseno=a.caseno fetch first 1 rows only)) = 'Y' ";
            	mStatsTypeSql3 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=d.caseno fetch first 1 rows only)) = 'Y' " ;
            	mStatsTypeSql4 = " and CHECKGRPCONT((select z.grpcontno from llclaimdetail z where z.caseno=b.caseno fetch first 1 rows only)) = 'Y' ";
            }
            
            //咨询、通知
            Info[2] = "0";
            Info[3] = "0";
            if ("1".equals(mCaseType)) {
                String tMainAskSQL = "select "
                                     +
                                     " coalesce(sum((case when a.asktype = '0' then 1 else 0 end)),0),"
                                     +
                                     " coalesce(sum((case when a.asktype = '1' then 1 else 0 end)),0)"
                                     + " from llmainask a "
                                     + " where a.operator='" + tUserCode +
                                     "' and  a.modifydate between '" +
                                     mStartDate +
                                     "' and '" + mEndDate + "' with ur";
                System.out.println(tMainAskSQL);
                SSRS tMainAskSSRS = tExeSQL.execSQL(tMainAskSQL);

                if (tMainAskSSRS.getMaxRow() > 0) {
                    Info[2] = tMainAskSSRS.GetText(1, 1);
                    Info[3] = tMainAskSSRS.GetText(1, 2);
                    tConsultSum += Integer.parseInt(Info[2]);
                    tNoticeSum += Integer.parseInt(Info[3]);
                }
            }
            //申请、申诉、纠错
            String tRgtTypeSQL = "select "
                                 +
                                 " coalesce(sum((case when a.rgttype = '1' then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when a.rgttype = '4' then 1 else 0 end)),0),"
                                 +
                                 " coalesce(sum((case when a.rgttype = '5' then 1 else 0 end)),0)"
                                 + " from llcase a where "
                                 + " a.handler='" + tUserCode +
                                 "' and a.endcasedate between '" + mStartDate +
                                 "' and '" + mEndDate + "' " + tCaseTypeSQL + 
                                 mStatsTypeSql1 +
                                 " with ur";
            System.out.println(tRgtTypeSQL);
            SSRS tRgtTypeSSRS = tExeSQL.execSQL(tRgtTypeSQL);
            Info[4] = "0";
            Info[5] = "0";
            Info[6] = "0";

            if (tRgtTypeSSRS.getMaxRow() > 0) {
                Info[4] = tRgtTypeSSRS.GetText(1, 1);
                Info[5] = tRgtTypeSSRS.GetText(1, 2);
                Info[6] = tRgtTypeSSRS.GetText(1, 3);
                tAppSum += Integer.parseInt(Info[4]);
                tAppealSum += Integer.parseInt(Info[5]);
                tErrSum += Integer.parseInt(Info[6]);
            }

            //合计1
            int tSumCase1 = Integer.parseInt(Info[2]) + Integer.parseInt(Info[3]) +
                            Integer.parseInt(Info[4]) + Integer.parseInt(Info[5]) +
                            Integer.parseInt(Info[6]);
            Info[7] = String.valueOf(tSumCase1);
            tTypeSum += tSumCase1;

            //受理
            String tRegisteSQL = "select count(1) "
                                 + " from llcase a where a.rigister = '" +
                                 tUserCode + "' and a.rgtdate between '" +
                                 mStartDate +
                                 "' and '" + mEndDate + "' " + tCaseTypeSQL + 
                                 mStatsTypeSql2 +
                                 " with ur";
            System.out.println(tRegisteSQL);
            SSRS tRegisteSSRS = tExeSQL.execSQL(tRegisteSQL);
            Info[8] = "0";

            if (tRegisteSSRS.getMaxRow() > 0) {
                Info[8] = tRegisteSSRS.GetText(1, 1);
                tRegisteSum += Integer.parseInt(Info[8]);
            }

            //处理完毕
            String tEndSQL = "select count(1) from "
                             +
                             " (select d.caseno caseno,d.rgtno rgtno from llcase d where "
                             + " d.handler = '" + tUserCode
                             + "' and d.endcasedate between '" +
                             mStartDate +
                             "' and '" + mEndDate +"' "
                             +
                             mStatsTypeSql3+
                             " union select b.caseno caseno, b.rgtno rgtno from llcase b,"
                             + " llcaseoptime c where b.caseno = c.caseno "
                             +
                             mStatsTypeSql4+
                             " and b.rgtstate = '10' and b.rgtstate = c.rgtstate "
                             + " and b.handler = '" + tUserCode
                             + "' and c.startdate between '" + mStartDate +
                             "' and '" + mEndDate + "' ) as a where 1=1 " +
                             tCaseTypeSQL + " with ur ";
            System.out.println(tEndSQL);
            SSRS tEndSSRS = tExeSQL.execSQL(tEndSQL);
            Info[9] = "0";

            if (tEndSSRS.getMaxRow() > 0) {
                Info[9] = tEndSSRS.GetText(1, 1);
                tEndSum += Integer.parseInt(Info[9]);
            }

            //审定、抽检
            String tOpTimeSQL = "select "
                                +
                                " coalesce(sum((case when b.rgtstate = '06' then 1 else 0 end)),0),"
                                +
                                " coalesce(sum((case when b.rgtstate = '10' then 1 else 0 end)),0)"
                                +
                                " from llcase a,llcaseoptime b where a.caseno=b.caseno and "
                                + " b.operator='" + tUserCode +
                                "' and b.enddate between '" + mStartDate +
                                "' and '" + mEndDate + "' " + tCaseTypeSQL +
                                mStatsTypeSql1 +
                                " with ur";
            System.out.println(tOpTimeSQL);
            SSRS tOpTimeSSRS = tExeSQL.execSQL(tOpTimeSQL);
            Info[10] = "0";
            Info[11] = "0";

            if (tOpTimeSSRS.getMaxRow() > 0) {
                Info[10] = tOpTimeSSRS.GetText(1, 1);
                Info[11] = tOpTimeSSRS.GetText(1, 2);
                tUWSum += Integer.parseInt(Info[10]);
                tSpotSum += Integer.parseInt(Info[11]);
            }

            //回退
            String tBackSQL = "select count(1) from "
                              + " (select a.caseno,min(a.casebackno) "
                              + "  from llcaseback a where a.beforstate<>'02' "
                              + " and a.backtype is null "
                              + tCaseTypeSQL
                              + " and a.modifydate between '" + mStartDate +
                              "' and '" + mEndDate
                              + "' and a.ohandler='" + tUserCode + "' "+
                              mStatsTypeSql1 +
                              " group by a.caseno) as x with ur";
            System.out.println(tBackSQL);
            SSRS tBackSSRS = tExeSQL.execSQL(tBackSQL);
            Info[12] = "0";

            if (tOpTimeSSRS.getMaxRow() > 0) {
                Info[12] = tBackSSRS.GetText(1, 1);
                tBackSum += Integer.parseInt(Info[12]);
            }

            //处理量
            String tDealSQL = "select count(distinct a.caseno) "
                              + " from llcase a,llcaseoptime b where "
                              + " b.rgtstate in ('03','04','05','06','10') "
                              + tCaseTypeSQL
                              + " and a.caseno=b.caseno and b.operator = '"
                              + tUserCode +"' "
                              + mStatsTypeSql1 
                              + " and b.enddate between '" + mStartDate +
                              "' and '" + mEndDate + "' with ur";
            System.out.println(tDealSQL);
            SSRS tDealSSRS = tExeSQL.execSQL(tDealSQL);
            Info[14] = "0";

            if (tDealSSRS.getMaxRow() > 0) {
                Info[14] = tDealSSRS.GetText(1, 1);
                tDealSum += Integer.parseInt(Info[14]);
            }

            int tSumCase2 = Integer.parseInt(Info[8]) +
                            Integer.parseInt(Info[9]) +
                            Integer.parseInt(Info[10]) +
                            Integer.parseInt(Info[14]) +
                            Integer.parseInt(Info[11]);
            Info[13] = String.valueOf(tSumCase2);
            tOPSum += tSumCase2;

            if (tSumCase1 > 0 || tSumCase2 > 0 ||
                Integer.parseInt(Info[12]) > 0) {
                mListTable.add(Info);
            }
        }

        String[] tInfoSum = new String[15];
        tInfoSum[0] = "合计";
        tInfoSum[1] = "";
        tInfoSum[2] = String.valueOf(tConsultSum);
        tInfoSum[3] = String.valueOf(tNoticeSum);
        tInfoSum[4] = String.valueOf(tAppSum);
        tInfoSum[5] = String.valueOf(tAppealSum);
        tInfoSum[6] = String.valueOf(tErrSum);
        tInfoSum[7] = String.valueOf(tTypeSum);
        tInfoSum[8] = String.valueOf(tRegisteSum);
        tInfoSum[9] = String.valueOf(tEndSum);
        tInfoSum[10] = String.valueOf(tUWSum);
        tInfoSum[11] = String.valueOf(tSpotSum);
        tInfoSum[12] = String.valueOf(tBackSum);
        tInfoSum[13] = String.valueOf(tOPSum);
        tInfoSum[14] = String.valueOf(tDealSum);
        mListTable.add(tInfoSum);
        return true;
    }

    /**
     * 进行数据查询
     *
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LLUserQuantySta.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("CaseTypeName", mCaseTypeName);
        tTextTag.add("Operator", mOperator);
        tTextTag.add("MakeDate", currentDate);
        String tStatsName = "";
        if("1".equals(mStatsType)){
        	tStatsName = "商业保险";
        }else if("2".equals(mStatsType)){
        	tStatsName = "社会保险";
        }else{
        	tStatsName = "全部";
        }
        tTextTag.add("StatsName", tStatsName);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", "", "", "", "", "",
                         "", ""};
        if (!getDataList()) {
            return false;
        }

        mListTable.setName("CLAIM");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("E:\\Temp\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     *
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        // 全局变量
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        mManageComName = (String) pmInputData.get(3);
        mCaseType = (String) pmInputData.get(4);
        mStatsType = (String) pmInputData.get(5);
        System.out.println("mStatsType:"+mStatsType);

        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
                "TransferData", 0);
        mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mMakeDate = currentDate;
        mOperator = mGlobalInput.Operator;

        if (mStartDate == null || "".equals(mStartDate)) {
            buildError("getInputData", "统计起期不能为空");
            return false;
        }

        if (mEndDate == null || "".equals(mEndDate)) {
            buildError("getInputData", "统计止期不能为空");
            return false;
        }

        if (mManageCom == null || "".equals(mManageCom)) {
            buildError("getInputData", "统计机构不能为空");
            return false;
        }

        if ("1".equals(mCaseType)) {
            mCaseTypeName = "普通案件";
        } else if ("2".equals(mCaseType)) {
            mCaseTypeName = "批量处理案件";
        } else {
            mCaseTypeName = "全部";
        }

        return true;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLClaimerESReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {

        VData tVData = new VData();
        LLUserQuantyReportBL tClientRegisterBL = new LLUserQuantyReportBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "lptest";
        String fileNameB = "lptest_0.vts";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("tFileNameB", fileNameB);
        tVData.addElement("2009-1-1");
        tVData.addElement("2009-5-31");
        tVData.addElement("86");
        tVData.addElement("lptest");
        tVData.addElement(ReportPubFun.getMngName("86"));
        tVData.addElement("I");
        tVData.addElement("个险");
        tVData.addElement(tTransferData);

        tVData.add(mGlobalInput);
        tClientRegisterBL.submitData(tVData, "");
        tVData.clear();
        tVData = tClientRegisterBL.getResult();
    }
}
