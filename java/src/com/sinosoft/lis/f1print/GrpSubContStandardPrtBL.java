package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class GrpSubContStandardPrtBL implements PrintService{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    
  //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private LCContSchema mGrpSubContInfo = null;

    private LCInsuredSchema mGrpSCInsuInfo = null;

    private LCPolSet mGrpSCInsuPolInfo = null;

    private String mLoadFlag;

    private String mflag = null;

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    public GrpSubContStandardPrtBL(){}
	
    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
	public boolean submitData(VData cInputData, String cOperate) {
		
		mOperate = cOperate;
		
		try{
			
			if (!getInputData(cInputData)){
                return false;
            }
			
			mResult.clear();
			
			if (!getPrintData()){
                return false;
            }
			
			return true;
			
		}catch (Exception ex){
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
	}

	/**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData){
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null){
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if (!dealPrintManager()){
            return false;
        }

        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (tTransferData != null){
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }

        mflag = mOperate;

        return true;
    }
	
    private boolean getPrintData(){
        XmlExport xmlExport = new XmlExport();

        xmlExport.createDocument("GrpSubContPrint", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(xmlExport)){
            return false;
        }
        // ------------------------------

        // 处理团体分单基本信息。
        if (!dealGrpSubContBaseInfo(xmlExport)){
            return false;
        }
        // ------------------------------

        // 处理保障计划
        if (!getInsuredList(xmlExport)){
            return false;
        }

        // ------------------------------

        // 结束节点
        if (!setEndFlagNodeByName(xmlExport, "end")){
            return false;
        }
        // ------------------------------

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);

        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }
    
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();

        cError.moduleName = "GrpSubContStandardPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	
	public CErrors getErrors() {
		
		return null;
	}
	
	/**
    *
    * @return LOPRTManagerDB
    * @throws Exception
    */
   private boolean dealPrintManager(){
       String tContNo = mLOPRTManagerSchema.getStandbyFlag1();

       if (!loadGrpSubContInfo(tContNo)){
           buildError("dealPrintManager", "获取分单数据失败！");
           return false;
       }

       String tGrpContNo = mGrpSubContInfo.getGrpContNo();

       if (!loadGrpContInfo(tGrpContNo)){
           buildError("dealPrintManager", "获取团单数据失败！");
           return false;
       }

       String tInsuredNo = mLOPRTManagerSchema.getStandbyFlag2();
       if (!loadGrpSCInsuInfo(tContNo, tInsuredNo)){
           buildError("dealPrintManager", "获取被保人数据失败！");
           return false;
       }

       if (!loadGrpSubInsuPol(tContNo, tInsuredNo)){
           buildError("dealPrintManager", "获取被保人险种数据失败！");
           return false;
       }

       String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
       if (tStrPrtSeq == null || tStrPrtSeq.equals("")){
           buildError("dealPrintManager", "生成打印流水号失败。");
           return false;
       }

       mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

       mLOPRTManagerSchema.setOtherNoType("16");

       mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
       mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
       mLOPRTManagerSchema.setPrtType("0");
       mLOPRTManagerSchema.setStateFlag("1");

       mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
       mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

       return true;
   }

   /**
    * 获取分单数据。
    * @param cContNo
    * @return
    */
   private boolean loadGrpSubContInfo(String cContNo){
       LCContDB tLCContDB = new LCContDB();
       tLCContDB.setContNo(cContNo);
       if (!tLCContDB.getInfo()){
           return false;
       }
       mGrpSubContInfo = tLCContDB.getSchema();
       return true;
   }
   
   /**
    * 获取团单信息
    * @param cGrpContNo
    * @return
    */
   private boolean loadGrpContInfo(String cGrpContNo){
       LCGrpContDB tLCGrpContDB = new LCGrpContDB();
       tLCGrpContDB.setGrpContNo(cGrpContNo);
       if (!tLCGrpContDB.getInfo()){
           return false;
       }
       mGrpContInfo = tLCGrpContDB.getSchema();
       return true;
   }
   
   /**
    * 获取被保人数据。
    * @param cContNo
    * @return
    */
   private boolean loadGrpSCInsuInfo(String cContNo, String cInsuredNo){
       LCInsuredDB tInsuDB = new LCInsuredDB();
       tInsuDB.setContNo(cContNo);
       tInsuDB.setInsuredNo(cInsuredNo);
       if (!tInsuDB.getInfo()){
           return false;
       }
       mGrpSCInsuInfo = tInsuDB.getSchema();
       return true;
   }
   
   /**
    * 获取被保人险种信息。
    * @return
    */
   private boolean loadGrpSubInsuPol(String cContNo, String cInsuredNo){
       LCPolDB tLCPolDB = new LCPolDB();
       String tStrSql = " select * from LCPol lcp where lcp.ContNo = '" + mGrpSubContInfo.getContNo() + "' "
               + " and lcp.InsuredNo = '" + mGrpSCInsuInfo.getInsuredNo() + "' ";
       mGrpSCInsuPolInfo = tLCPolDB.executeQuery(tStrSql);
       if (mGrpSCInsuPolInfo == null || mGrpSCInsuPolInfo.size() == 0){
           return false;
       }
       return true;
   }
   
   /**
    * 产生PDF打印相关基本信息。
    * @param cTextTag
    * @return
    */
   private boolean dealPdfBaseInfo(XmlExport cXmlExport){
       
	   TextTag tTmpTextTag = new TextTag();

       tTmpTextTag.add("JetFormType", "GS005");

       String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
       String comcode = new ExeSQL().getOneValue(sqlusercom);
       if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000")){
           comcode = "86";
       }else if (comcode.length() >= 4){
           comcode = comcode.substring(0, 4);
       }else{
           buildError("dealGrpContInfo", "操作员机构查询出错!");
           return false;
       }
       String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode + "' with ur";
       String printcode = new ExeSQL().getOneValue(printcom);
       tTmpTextTag.add("ManageComLength4", printcode);

       tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));

       if (mflag.equals("batch")){
           tTmpTextTag.add("previewflag", "0");
       }else{
           tTmpTextTag.add("previewflag", "1");
       }

       cXmlExport.addTextTag(tTmpTextTag);

       return true;
   }
   
   /**
    * 处理团体保单层信息。
    * @param cTextTag
    * @return
    */
   private boolean dealGrpSubContBaseInfo(XmlExport cXmlExport){
       TextTag tTmpTextTag = new TextTag();

       tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
       tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
       tTmpTextTag.add("AppntName", mGrpContInfo.getGrpName());
       tTmpTextTag.add("CValidate", getDate(mGrpContInfo.getCValiDate()));
       tTmpTextTag.add("CInvalidate", getDate(mGrpContInfo.getCInValiDate()));
       tTmpTextTag.add("SignDate", mGrpSubContInfo.getSignDate());

       tTmpTextTag.add("ManageCom", mGrpContInfo.getManageCom());
       tTmpTextTag.add("ManageComName", chgGrpManageCom(mGrpContInfo.getManageCom()));
       tTmpTextTag.add("Remark", StrTool.cTrim(mGrpContInfo.getRemark()).equals("") ? "无" : mGrpContInfo.getRemark());

       tTmpTextTag.add("ContNo", mGrpSCInsuInfo.getContNo());
       tTmpTextTag.add("InsuredNo", mGrpSCInsuInfo.getInsuredNo());
       tTmpTextTag.add("InsuredName", mGrpSCInsuInfo.getName());
       if("0".equals(mGrpSCInsuInfo.getSex())){
    	   tTmpTextTag.add("InsuredSex", "男");
       }else if("1".equals(mGrpSCInsuInfo.getSex())){
    	   tTmpTextTag.add("InsuredSex", "女");
       }else{
    	   tTmpTextTag.add("InsuredSex", "不详");
       }
       tTmpTextTag.add("InsuredBirthday", mGrpSCInsuInfo.getBirthday());
       tTmpTextTag.add("InsuredIdType", getIDTypeName(mGrpSCInsuInfo.getIDType()));
       tTmpTextTag.add("InsuredIdNo", mGrpSCInsuInfo.getIDNo());
       tTmpTextTag.add("InsuredPhone", mGrpSCInsuInfo.getGrpInsuredPhone());   
       SSRS tSSRS = new ExeSQL().execSQL("select name,zipcode,servicepostaddress from ldcom where comcode='"+mGrpContInfo.getManageCom()+"' ");
       tTmpTextTag.add("PostalAddress",tSSRS.GetText(1, 3));
       tTmpTextTag.add("Zipcode", tSSRS.GetText(1, 2));
       tTmpTextTag.add("Today", getDate(PubFun.getCurrentDate()));

       cXmlExport.addTextTag(tTmpTextTag);

       return true;
   }
   
   private String getDate(String cDate){
       Date tDate = new FDate().getDate(cDate);
       if (tDate == null)
       {
           System.out.println("[" + cDate + "]日期转换失败");
           return cDate;
       }
       SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
       return df.format(tDate);
   }
   
   /**
    * 获取机构代码代码对应中文含义。
    * @param cPayIntv
    * @return
    */
   private String chgGrpManageCom(String cManageCom){
       String tStrSql = "select Name from LDCom where ComCode = '" + cManageCom + "'";
       return new ExeSQL().getOneValue(tStrSql);
   }
   
   /**
    * 获取证件号对应中文说明
    * @param cIDType
    * @return
    */
   private String getIDTypeName(String cIDType){
       String tStrSql = " select CodeName from LDCode " + " where CodeType = 'idtype' and Code = '" + cIDType + "' ";
       return new ExeSQL().getOneValue(tStrSql);
   }
   
   private boolean setEndFlagNodeByName(XmlExport cXmlExport, String cEndName)
   {
       String[] tEndFlag = new String[0];

       ListTable tLTEndNode = new ListTable();
       tLTEndNode.setName(cEndName);

       cXmlExport.addListTable(tLTEndNode, tEndFlag);

       return true;
   }
   
   private boolean getInsuredList(XmlExport cXmlExport)
   {
	   DecimalFormat df = new DecimalFormat("0.00%");
       String[] tPolListInfoTitle = new String[13];
       tPolListInfoTitle[0] = "ContPlanCode";
       tPolListInfoTitle[1] = "InsuredCount";
       tPolListInfoTitle[2] = "RiskCode";
       tPolListInfoTitle[3] = "RiskName";
       tPolListInfoTitle[4] = "DutyCode";
       tPolListInfoTitle[5] = "DutyName";
       tPolListInfoTitle[6] = "Amnt/Mult";
       tPolListInfoTitle[7] = "GetLimit";
       tPolListInfoTitle[8] = "GetRate";
       tPolListInfoTitle[9] = "Prem";
       tPolListInfoTitle[10] = "Mult";
       tPolListInfoTitle[11] = "Amnt";
       tPolListInfoTitle[12] = "DutyAmnt";

       ListTable tLTLCPol = new ListTable();
       tLTLCPol.setName("LCPol");

       //取险种相关责任
       StringBuffer tSQl1 = new StringBuffer(256);
       tSQl1
               .append(" select distinct ContPlanCode,(select case  when count(1)=0 then (select peoples2 from LCContPlan where grpcontno=z.grpcontno and ContPlanCode = z.ContPlanCode ) else count(1) end InsuredCount from LCInsured where GrpContNo = z.GrpContNo ");
       tSQl1
               .append(" and ContPlanCode = z.ContPlanCode and name not like'公共账户%' and name not like'无名单%' ),z.RiskCode,(select RiskName from LMRisk where RiskCode =z.riskcode), ");
       tSQl1.append(" (select trim(z.riskcode)||trim(outdutycode)  from LMduty where dutycode =z.dutycode), ");
       tSQl1.append(" (select outdutyname from LMduty where dutycode =z.dutycode), ");
       tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
       tSQl1
               .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
       tSQl1.append(" in ('Mult') and codetype='printflag' and code=z.RiskCode), ");
       tSQl1.append(" ( ");
       tSQl1.append(" select ");
       tSQl1
               .append(" varchar(replace(ltrim((replace(trim(char(decimal(lcd.Amnt,16,2))),'0',' '))),' ','0'), 50) || ldc.CodeName ");
       tSQl1.append(" from LCContPlanDutyParam lccpdp ");
       tSQl1
               .append(" inner join LCPol lcp on lcp.ContPlanCode = lccpdp.ContPlanCode and lcp.GrpContNo = lccpdp.GrpContNo ");
       tSQl1.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo and lcd.dutycode = lccpdp.dutycode ");
       tSQl1.append(" inner join LDCode ldc on ldc.CodeType = 'printflag' and ldc.Code = lcp.RiskCode ");
       tSQl1.append(" where 1 = 1 ");
       tSQl1.append(" and lccpdp.CalFactor = 'Amnt' ");
       tSQl1.append(" and lccpdp.DutyCode = z.DutyCode ");
       tSQl1.append(" and lcp.InsuredNo = '" + mGrpSCInsuInfo.getInsuredNo() + "' ");
       tSQl1.append(" and lcp.GrpContNo = z.GrpContNo ");
       tSQl1.append(" ),");
       tSQl1.append(" (select CalFactorValue ParamValue2 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
       tSQl1
               .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetLimit'), ");
       tSQl1.append(" (select CalFactorValue ParamValue3 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
       tSQl1
               .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetRate'), ");
       tSQl1.append(" (select n ");
       tSQl1.append(" from ");
       tSQl1.append(" (select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ");
       tSQl1.append(" from lccontplan a,lcpol b,lcinsured c ");
       tSQl1.append("  where ");
       tSQl1.append(" a.grpcontno=b.grpcontno  and b.insuredno=c.insuredno ");
       tSQl1.append(" and b.contno=c.contno ");
       tSQl1.append(" and a.contplancode=c.contplancode ");
       tSQl1.append(" and a.grpcontno=c.grpcontno ");
       tSQl1.append(" and b.grpcontno=c.grpcontno ");
       tSQl1.append(" and b.insuredname not like ('公共账户') ");
       tSQl1.append(" and c.insuredno = '" + mGrpSCInsuInfo.getInsuredNo() + "' ");
       tSQl1.append(" and b.appflag in('1','9') group by a.contplancode,b.RiskCode,a.grpcontno)  as X ");
       tSQl1.append(" where X.contplancode=z.contplancode ");
       tSQl1.append(" and X.RiskCode=z.riskcode  and X.grpcontno=z.grpcontno ) ");
       tSQl1.append("  ,dutycode, ");
       tSQl1.append(" ( ");
       tSQl1.append(" select ");
       tSQl1
              .append(" varchar(replace(ltrim((replace(trim(char(decimal(lcd.Amnt,16,2))),'0',' '))),' ','0'), 50) || ldc.CodeName ");
       tSQl1.append(" from LCContPlanDutyParam lccpdp ");
       tSQl1
       		  .append(" inner join LCPol lcp on lcp.ContPlanCode = lccpdp.ContPlanCode and lcp.GrpContNo = lccpdp.GrpContNo ");
       tSQl1.append(" inner join LCDuty lcd on lcd.PolNo = lcp.PolNo and lcd.dutycode = lccpdp.dutycode ");
       tSQl1.append(" inner join LDCode ldc on ldc.CodeType = 'printflag2' and ldc.Code = lcp.RiskCode ");
       tSQl1.append(" where 1 = 1 ");
       tSQl1.append(" and lccpdp.CalFactor = 'DutyAmnt' ");
       tSQl1.append(" and lccpdp.DutyCode = z.DutyCode ");
       tSQl1.append(" and lcp.InsuredNo = '" + mGrpSCInsuInfo.getInsuredNo() + "' ");
       tSQl1.append(" and lcp.GrpContNo = z.GrpContNo ");
       tSQl1.append(" ),");
       tSQl1.append(" b.GrpProposalNo ");
       tSQl1.append(" ,(Select Othersign from LDCODE  where codetype='printflag' and code = b.RiskCode) ");
       tSQl1.append(" from LCContPlanDutyParam z, LCGrpPol b ");
       tSQl1.append("  where z.GrpPolNo = b.GrpPolNo");
       tSQl1.append("  and z.GrpContNo = '" + mGrpContInfo.getGrpContNo() + "' and ContPlanCode != '00'");
       tSQl1.append("  and z.ContPlanCode = '" + mGrpSCInsuInfo.getContPlanCode() + "'");
       tSQl1.append(" and ContPlanCode != '11' order by ContPlanCode,b.GrpProposalNo,dutycode with ur ");
       System.out.println(tSQl1.toString());

       StringBuffer tsql = new StringBuffer();
       tsql.append(" select 'ALL',(select sum(z.peoples3)  from lccontplanrisk m,lccontplan z ");
       tsql
               .append(" where m.contplancode not in ('00','11')  and m.grpcontno=z.grpcontno and   m.contplancode=z.contplancode and ");
       tsql.append(" m.riskcode=a.riskcode and m.riskcode=c.riskcode and a.grpcontno=m.grpcontno ");
       tsql
               .append(" and a.grpcontno=z.grpcontno),a.riskcode,c.riskname,'',(case (Select Othersign from LDCODE  where codetype='printflag' and code = c.RiskCode) when '1' then '特需综合保障责任' else '团体帐户' end),'','','','',(select sum(prem) from lcpol where ");
       tsql.append(" lcpol.poltypeflag='2' and lcpol.insuredname like '公共账户' and lcpol.prtno=a.prtno ");
       tsql.append(" and lcpol.riskcode=a.riskcode) from lcgrppol a,lmrisk c where a.grpcontNo='"
               + mGrpContInfo.getGrpContNo() + "' ");
       tsql.append(" and  a.riskcode=c.riskcode and c.riskcode in(select riskcode from lmriskapp where ");
       tsql.append(" risktype9='4') order by a.riskcode ");
       System.out.println(tsql.toString());

       ExeSQL tExeSQL2 = new ExeSQL();
       SSRS tSSRS1 = tExeSQL2.execSQL(tSQl1.toString());
       SSRS tSSRS2 = tExeSQL2.execSQL(tsql.toString());
       if (tSSRS2.getMaxRow() > 0)
       {
           for (int Riskcount = 0; Riskcount < PubRiskcode.length; Riskcount++)
           {
               PubRiskcode[Riskcount] = "";
           }
       }

       int begincount = 0;
       String[] onePolInfo = null;

       for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
       {
           // 判断是否存在特殊打印需求。如170301，不打印责任。
           if ("1".equals(tSSRS1.GetText(j, 15)))
               continue;
           // --------------------------------------------

           if (j > 1 && tSSRS1.GetText(j, 1).equals(tSSRS1.GetText(j - 1, 1))
                   && tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j - 1, 3)))
           {
               begincount = j;
           }

           onePolInfo = new String[13];
           onePolInfo[0] = tSSRS1.GetText(j, 1);
           onePolInfo[1] = tSSRS1.GetText(j, 2);
           onePolInfo[2] = tSSRS1.GetText(j, 3);
           onePolInfo[3] = tSSRS1.GetText(j, 4);
           onePolInfo[4] = tSSRS1.GetText(j, 5);
           onePolInfo[5] = tSSRS1.GetText(j, 6);

           // 处理“保额/限额/档次”节点
           String tAmntOrMult = "";

           if (tSSRS1.GetText(j, 13).equals(""))
           {
               if (tSSRS1.GetText(j, 7).equals(""))
               {
                   tAmntOrMult = tSSRS1.GetText(j, 8);
               }
               else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 8).equals("")))
               {
                   tAmntOrMult = tSSRS1.GetText(j, 8).replace('D', 'X');
               }
               else
               {

                   tAmntOrMult = tSSRS1.GetText(j, 7);
               }
           }
           else if (!tSSRS1.GetText(j, 13).equals(""))
           {
               if (tSSRS1.GetText(j, 7).equals(""))
               {
                   tAmntOrMult = tSSRS1.GetText(j, 13);
               }
               else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 13).equals("")))
               {
                   tAmntOrMult = tSSRS1.GetText(j, 13).replace('D', 'B');
               }
               else
               {
                   tAmntOrMult = tSSRS1.GetText(j, 7);
               }
           }
           
        	   onePolInfo[6] = tAmntOrMult;

           if (tAmntOrMult != null && !tAmntOrMult.equals(""))
           {

               String tTmpAmntOrMult = tAmntOrMult.substring(0, tAmntOrMult.length() - 1);
               String tLastFlagOfAmnt = tAmntOrMult.substring(tAmntOrMult.length() - 1, tAmntOrMult.length());

               if ("X".equals(tLastFlagOfAmnt))
               {
                   onePolInfo[12] = tTmpAmntOrMult;
               }
               else if ("B".equals(tLastFlagOfAmnt))
               {
               	   onePolInfo[11] = tTmpAmntOrMult;
               }
               else if ("D".equals(tLastFlagOfAmnt))
               {
                   onePolInfo[10] = tTmpAmntOrMult;
               }
           }
           // ------------------------------------------

           onePolInfo[7] = tSSRS1.GetText(j, 9);

           if (!StrTool.cTrim(tSSRS1.GetText(j, 10)).equals(""))
           {
               onePolInfo[8] = format(Double.parseDouble(tSSRS1.GetText(j, 10))*100)+"%";
               if("100.00%".equals(onePolInfo[8])){
            	   onePolInfo[8]="100%";
               }
               
           }
           else
           {
//               onePolInfo[8] = tSSRS1.GetText(j, 10);
        	   onePolInfo[8] = "--";
           }
           if (j == begincount)
           {
               onePolInfo[9] = "";
           }
           else
           {
               String prem = setPrem(mGrpContInfo.getGrpContNo(), tSSRS1.GetText(j, 1), tSSRS1.GetText(j, 3));
               if (prem == null)
               {
                   return false;
               }

               onePolInfo[9] = prem;
           }
           if(tSSRS1.GetText(j, 3)=="162401"||"162401".equals(tSSRS1.GetText(j, 3))){
        	   String ttsql = "select Amnt "
        			   		+ "from lcduty where polno in (select polno from lcpol where grpcontno='"
        			   		+ mGrpContInfo.getGrpContNo() + "' "
        			   		+ "and InsuredNo = '"
        			   		+ mGrpSCInsuInfo.getInsuredNo() + "') "
        			   		+ "and dutycode='"
        			   		+ tSSRS1.GetText(j, 12) + "'";
        	   ExeSQL tExeSQL3 = new ExeSQL();
               SSRS tSSRS3 = tExeSQL3.execSQL(ttsql);
               if(tSSRS3.getMaxRow()>0){
            	   for(int k = 1;k <= tSSRS3.getMaxRow(); k++){
            			   onePolInfo[11] = tSSRS3.GetText(k, 1);
            	   }	   
               }
           }
           if("".equals(onePolInfo[7]) || onePolInfo[7]==null){
        	   onePolInfo[7]="--";
           }
           if("".equals(onePolInfo[10]) || onePolInfo[10]==null){
        	   onePolInfo[10]="--";
           }
           if("".equals(onePolInfo[11]) || onePolInfo[11]==null){
        	   onePolInfo[11]="--";
           }
           if("".equals(onePolInfo[12]) || onePolInfo[12]==null){
        	   onePolInfo[12]="--";
           }
           
           tLTLCPol.add(onePolInfo);
       }

       onePolInfo = null;
       //公共帐户显示,为了避免重复显示公共账户增加PubRiskcode数组缓存已经打印过公共账户的险种代码

       if (tSSRS2.getMaxRow() > 0)
       {

           for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
           {
               for (int tIndex = 1; tIndex <= tSSRS2.getMaxRow(); tIndex++)
               {
                   System.out.println("+++++");
                   for (int Riskcount1 = 0; Riskcount1 < PubRiskcode.length; Riskcount1++)
                   {
                       if (tSSRS2.GetText(tIndex, 3).equals(PubRiskcode[Riskcount1]))
                       {
                           PubRiskcodePrintFlag = "1";
                       }
                   }

                   onePolInfo = new String[13];

                   System.out.println("+++++");
                   if (tSSRS1.GetText(j, 3).equals(tSSRS2.GetText(tIndex, 3)) & PubRiskcodePrintFlag.equals("0"))
                   {
                       if (j < tSSRS1.getMaxRow() && !(tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j + 1, 3))))
                       {
                           onePolInfo[0] = tSSRS2.GetText(tIndex, 1);
                           onePolInfo[1] = tSSRS2.GetText(tIndex, 2);
                           onePolInfo[2] = tSSRS2.GetText(tIndex, 3);
                           onePolInfo[3] = tSSRS2.GetText(tIndex, 4);
                           onePolInfo[4] = tSSRS2.GetText(tIndex, 5);
                           onePolInfo[5] = tSSRS2.GetText(tIndex, 6);
                       	   onePolInfo[6] = tSSRS2.GetText(tIndex, 8);
                           onePolInfo[7] = tSSRS2.GetText(tIndex, 9);
//                           onePolInfo[8] = tSSRS2.GetText(tIndex, 10);
                           onePolInfo[8]=format(Double.parseDouble(tSSRS2.GetText(tIndex, 10))*100)+"%";

                           if (tSSRS2.GetText(tIndex, 11).equals(null) || tSSRS2.GetText(tIndex, 11).equals("null")
                                   || tSSRS2.GetText(tIndex, 11).equals(""))
                           {
                               onePolInfo[9] = "";
                           }
                           else
                           {
                               onePolInfo[9] = format(Double.parseDouble(tSSRS2.GetText(tIndex, 11)));
                           }
                           PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                           tLTLCPol.add(onePolInfo);

                       }
                       else if (j == tSSRS1.getMaxRow())
                       {
                           onePolInfo[0] = tSSRS2.GetText(tIndex, 1);
                           onePolInfo[1] = tSSRS2.GetText(tIndex, 2);
                           onePolInfo[2] = tSSRS2.GetText(tIndex, 3);
                           onePolInfo[3] = tSSRS2.GetText(tIndex, 4);
                           onePolInfo[4] = tSSRS2.GetText(tIndex, 5);
                           onePolInfo[5] = tSSRS2.GetText(tIndex, 6);
                           onePolInfo[6] = tSSRS2.GetText(tIndex, 8);
                           onePolInfo[7] = tSSRS2.GetText(tIndex, 9);
                           onePolInfo[8]=format(Double.parseDouble(tSSRS2.GetText(tIndex, 10))*100)+"%";
                                    

                           if (tSSRS2.GetText(tIndex, 11).equals(null) || tSSRS2.GetText(tIndex, 11).equals("null")
                                   || tSSRS2.GetText(tIndex, 11).equals(""))
                           {
                               onePolInfo[9] = "";
                           }
                           else
                           {
                               onePolInfo[9] = format(Double.parseDouble(tSSRS2.GetText(tIndex, 11)));
                           }

                           PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                           tLTLCPol.add(onePolInfo);
                       }
                   }
               }
           }
       }
    
      
       
       cXmlExport.addListTable(tLTLCPol, tPolListInfoTitle);

       // 结束节点
       if (!setEndFlagNodeByName(cXmlExport, "LCPolEnd"))
       {
           return false;
       }
       // ------------------------------

       return true;
   }
   
   /**
    * 格式化浮点型数据
    * @param dValue double
    * @return String
    */
   private static String format(double dValue){
       return new DecimalFormat("0.00").format(dValue);
   }
   
   private String setPrem(String cGrpContNo, String cContPlanCode, String cRiskCode)
   {
       LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
       tLMRiskAppDB.setRiskCode(cRiskCode);
       if (!tLMRiskAppDB.getInfo())
       {
           CError tError = new CError();
           tError.moduleName = "LCGrpContF1PBL";
           tError.functionName = "setPrem";
           tError.errorMessage = "没有查询到险种定义信息";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if ("0".equals(tLMRiskAppDB.getNotPrintPol()))
       {
           return "";
       }

       String premSumPart = "  and RiskCode = '" + cRiskCode + "' ";
       if ("1".equals(tLMRiskAppDB.getNotPrintPol()))
       {
           premSumPart = " and (X.RiskCode='" + cRiskCode + "' "
                   + "   or X.RiskCode in(select RiskCode from LDRiskParamPrint "
                   + "              where ParamType1 = 'sumprem' " + "                 and ParamValue1='" + cRiskCode
                   + "')) ";
       }

       //查询非绑定附险种的保费，包括绑定主险的附险保费
       ExeSQL tExeSQL = new ExeSQL();

       StringBuffer sql2 = new StringBuffer();
       sql2.append(" select nvl(sum(n), 0) from ").append("(").append(
               "    select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ").append(
               "    from lccontplan a,lcpol b,lcinsured c ").append(
               "    where  a.grpcontno=b.grpcontno  and b.insuredno=c.insuredno  and b.contno=c.contno ").append(
               "      and a.contplancode=c.contplancode  and a.grpcontno=c.grpcontno ").append(
               "      and b.grpcontno=c.grpcontno ").append("      and b.insuredname not like ('公共账户') ").append(
               "      and b.appflag in('1','9') ").append(
               " and c.insuredno = '" + mGrpSCInsuInfo.getInsuredNo() + "' ").append(
               "    group by a.contplancode,b.RiskCode,a.grpcontno ").append(" )  as X ").append(
               " where X.contplancode='" + cContPlanCode + "' ").append(premSumPart).append(
               "   and X.grpcontno='" + cGrpContNo + "' ");
       String prem = tExeSQL.getOneValue(sql2.toString());

       if (tExeSQL.mErrors.needDealError())
       {
           System.out.println(tExeSQL.mErrors.getErrContent());

           CError tError = new CError();
           tError.moduleName = "LCGrpContF1PBL";
           tError.functionName = "setPrem";
           tError.errorMessage = "查询邦定主险保费出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if ("".equals(prem) || "null".equals(prem))
       {
           CError tError = new CError();
           tError.moduleName = "LCGrpContF1PBL";
           tError.functionName = "setPrem";
           tError.errorMessage = "查询主险保费失败";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       return format(Double.parseDouble(prem));
   }
}
