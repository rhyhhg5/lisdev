package com.sinosoft.lis.f1print;

import java.io.File;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.vschema.LDSysVarSet;

public class FileQueue
{
    private static int nHead = 0;
    private static int nTotal = 0;

    static
    {
        //set nTotal
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        String strSql1 =
                "select * from ldsysvar where Sysvar='FileQueueLength'";
        LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(strSql1);
        LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
        String strLength = tLDSysVarSchema.getV(2);
        nTotal = Integer.parseInt(strLength);

        // check VTS file path
        String strSql2 = "select * from ldsysvar where Sysvar='VTSFilePath'";
        tLDSysVarSet = tLDSysVarDB.executeQuery(strSql2);
        tLDSysVarSchema = tLDSysVarSet.get(1);
        String strFilePath = tLDSysVarSchema.getV(2);

        String strSql3 = "select * from ldsysvar where Sysvar='VTSRealPath'";
        tLDSysVarSet = tLDSysVarDB.executeQuery(strSql3);
        tLDSysVarSchema = tLDSysVarSet.get(1);
        String strRealPath = tLDSysVarSchema.getV(2) + strFilePath;

        File fPathName = new File(strRealPath);
        if (fPathName.exists())
        {
            System.out.println("VTS path exists");
        }
        else
        { // Doesn't exist
            fPathName.mkdirs();
            System.out.println("Created " + fPathName);
        }
    }

    public FileQueue()
    {
    }

//    public static synchronized int getFileName()
//    {
//        int nTHead = nHead;
//        if (nHead >= (nTotal - 1))
//        {
//            nHead = 0;
//        }
//        else
//        {
//            nHead++;
//        }
//        return nTHead;
//    }
    
    /**
     * 防止formula one并发读写导致的宕机
     * @author filon51
     * @since 2009-05-06
     */
    public static synchronized long getFileName()
    {
        long nTHead = System.currentTimeMillis();
        return nTHead;
    }
}