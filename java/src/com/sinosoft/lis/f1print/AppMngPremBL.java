package com.sinosoft.lis.f1print;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AppMngPremBL {

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mType = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mOperator = "";

	private VData mInputData = new VData();

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";

	private CSVFileWrite mCSVFileWrite = null;

	private String mFilePath = "";

	private String mFileName = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mType = cOperate;
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryDataToCVS()) {
			return false;
		}

		return true;
	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();

		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;
		mManageCom = mGlobalInput.ManageCom;
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		System.out.println(mOperator);
		System.out.println(mManageCom);
		mStartDate = (String) mTransferData.getValueByName("StartDate");// 起始日期
		mEndDate = (String) mTransferData.getValueByName("EndDate");// 截止日期
		mFilePath = (String) mTransferData.getValueByName("realpath");
		System.out.println(mStartDate);
		System.out.println(mEndDate);

		return true;
	}

	private boolean queryDataToCVS() {


        mFilePath+="vtsfile/";
		System.out.println(mFilePath);
		// 本地测试
//		mFilePath = "F:\\vtsfile\\";

		String[][] tTitle = new String[4][];
		tTitle[1] = new String[] { "机构：" + mManageCom, "",
				"统计时间：" + mStartDate + "至" + mEndDate };
		tTitle[2] = new String[] { "制表人：" + mOperator,
				"制表时间：" + mPubFun.getCurrentDate() };
		if (mType.equals("0")) {
			mFileName = "Report1";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "各机构投保件数、保费", "" };
			tTitle[3] = new String[] { "管理机构", "投保件数", "保费" };
			String[] tContentType = { "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("1")) {
			mFileName = "Report2";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "各机构承保件数、保费", "" };
			tTitle[3] = new String[] { "管理机构", "承保件数", "保费" };
			String[] tContentType = { "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("2")) {
			mFileName = "Report3";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "", "", "",
					"分机构万能险期缴保费、基本保费、额外保费、追加保费保额、缴费年期", "", "", "", "" };
			tTitle[3] = new String[] { "管理机构", "印刷号", "保单号", "万能险保费", "基本保费",
					"额外保费", "追加保费", "保额", "缴费年期" };
			String[] tContentType = { "String", "String", "String", "Number",
					"Number", "Number", "Number", "Number", "String" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("3")) {
			mFileName = "Report4";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "问题件、体检件、契调件件数、及三个的回销件数", "", "" };
			tTitle[3] = new String[] { "管理机构", "统计类别", "总件数", "回销件数" };
			String[] tContentType = { "String", "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("4")) {
			mFileName = "Report5";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "各机构待收费件数、保费", "" };
			tTitle[3] = new String[] { "管理机构", "待收费件数", "保费" };
			String[] tContentType = { "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("5")) {
			mFileName = "Report6";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "各机构外包错误修改件数", "" };
			tTitle[3] = new String[] { "管理机构", "错误修改件数" };
			String[] tContentType = { "String", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("6")) {
			mFileName = "Report7";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "各机构新单撤消件数、保费", "" };
			tTitle[3] = new String[] { "管理机构", "新单撤消件数", "保费" };
			String[] tContentType = { "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("7")) {
			mFileName = "Report8";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "各机构新单删除件数、保费", "" };
			tTitle[3] = new String[] { "管理机构", "新单删除件数", "保费" };
			String[] tContentType = { "String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (mType.equals("8")) {
			mFileName = "Report9";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "各机构送人工核保件数", "" };
			tTitle[3] = new String[] { "管理机构", "送人工核保件数" };
			String[] tContentType = { "String", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}  
		
		if (mType.equals("9")) {
			mFileName = "Report10";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "","各机构正常承保件数、保费，延期承保件数、保费，拒保件数、保费，变更承保件数、保费", "","" };
			tTitle[3] = new String[] { "管理机构", "承保类型","件数","保费" };
			String[] tContentType = { "String","String", "Number","Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		} 
		
		if (mType.equals("10")) {
			mFileName = "Report11";
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "各机构体检阳性率总件数", "" };
			tTitle[3] = new String[] { "管理机构", "体检阳性率总件数" };
			String[] tContentType = { "String", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		} 

		if (!getDataListToCVS()) {
			return false;
		}

		mCSVFileWrite.closeFile();

		return true;
	}

	/*
	 * 统计案件结果
	 */
	private boolean getDataListToCVS() {
		ExeSQL tExeSQL = new ExeSQL();
		if (mType.equals("0")) {
			String sql1 = " select substr(lcc.managecom,1,4) ,count(1) ,sum(prem)"
					+ " from lccont lcc "
					+ " where 1 = 1 "
					+ " and lcc.inputdate >= '"
					+ mStartDate
					+ "'"
					+ " and lcc.inputdate <= '"
					+ mEndDate
					+ "'"
					+ " and lcc.salechnl in ('01','10') "
					+ " and lcc.conttype = '1' "
					+ " group by substr(lcc.ManageCom,1,4)" + " with ur";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[3];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[3];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}

		if (mType.equals("1")) {
			String sql1 = " select substr(lcc.managecom,1,4) ,count(1) ,sum(prem)"
					+ " from lccont lcc "
					+ " where 1 = 1 "
					+ " and lcc.inputdate >= '"
					+ mStartDate
					+ "'"
					+ " and lcc.inputdate <= '"
					+ mEndDate
					+ "'"
					+ " and lcc.salechnl in ('01','10') "
					+ " and lcc.conttype = '1' "
					+ " and lcc.appflag = '1' "
					+ " group by substr(lcc.ManageCom,1,4)" + " with ur";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[3];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[3];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}

		if (mType.equals("2")) {
			String sql1 = " select "
					+ " lcp.managecom 管理机构, "
					+ " lcp.prtno 印刷号, "
					+ " lcp.contno 保单号, "
					+ " sum(lcp.prem) 万能险保费, "
					+ " sum(double(case when amnt/20 < (case when prem < 6000 then prem else 6000 end) then amnt/20 else (case when prem < 6000 then prem else 6000 end) end)) 基本保费, "
					+ " sum(double(lcp.prem-(case when lcp.amnt/20 < (case when lcp.prem < 6000 then lcp.prem else 6000 end) then lcp.amnt/20 else (case when lcp.prem < 6000 then lcp.prem else 6000 end) end))) 额外保费, "
					+ " sum(lcp.Supplementaryprem) 追加保费, "
					+ " sum(lcp.amnt) 保额, "
					+ " lcp.payyears 缴费年期 "
					+ " from lcpol lcp  "
					+ " where 1 = 1  "
					+ " and lcp.appflag = '1' "
					+ " and lcp.stateflag = '1' "
					+ " and lcp.riskcode in ('331801','531201') "
					+ " and lcp.CValidate >= '"
					+ mStartDate
					+ "' "
					+ " and lcp.CValidate <= '"
					+ mEndDate
					+ "' "
					+ " group by lcp.managecom,lcp.prtno,lcp.contno,lcp.payyears "
					+ " with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[9];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					ListInfo[3] = tSSRS1.GetText(i, 4);
					ListInfo[4] = tSSRS1.GetText(i, 5);
					ListInfo[5] = tSSRS1.GetText(i, 6);
					ListInfo[6] = tSSRS1.GetText(i, 7);
					ListInfo[7] = tSSRS1.GetText(i, 8);
					ListInfo[8] = tSSRS1.GetText(i, 9);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[9];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";
				ListInfo[3] = "";
				ListInfo[4] = "";
				ListInfo[5] = "";
				ListInfo[6] = "";
				ListInfo[7] = "";
				ListInfo[8] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}

		if (mType.equals("3")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) managecom,'问题件' name,count(1) Number,count(edm.doccode) HXNumber "
					+ " from LOPRTManager lom "
					+ " inner join LCCont lcc on lcc.ProposalContno = lom.otherno "
					+ " left join ES_DOC_MAIN edm on edm.doccode = lom.prtseq "
					+ " where 1 = 1 "
					+ " and lom.OtherNoType = '02' "
					+ " and lom.Code = '85' "
					+ " and lom.MakeDate >= '"
					+ mStartDate
					+ "' "
					+ " and lom.MakeDate <= '"
					+ mEndDate
					+ "' "
					+ " and lcc.salechnl in ('01','10') "
					+ " group by substr(lcc.ManageCom,1,4) "
					+ " union all "
					+ " select substr(lcc.ManageCom,1,4) managecom,'体检件' name,count(1) Number,count(edm.doccode) HXNumber "
					+ " from LOPRTManager lom "
					+ " inner join LCCont lcc on lcc.ProposalContno = lom.otherno "
					+ " left join ES_DOC_MAIN edm on edm.doccode = lom.prtseq "
					+ " where 1 = 1 "
					+ " and lom.OtherNoType = '02' "
					+ " and lom.Code = '03' "
					+ " and lom.MakeDate >= '"
					+ mStartDate
					+ "' "
					+ " and lom.MakeDate <= '"
					+ mEndDate
					+ "' "
					+ " and lcc.salechnl in ('01','10') "
					+ " group by substr(lcc.ManageCom,1,4) "
					+ " union all "
					+ " select substr(lcc.ManageCom,1,4) managecom,'契调件' name,count(1) Number,count(lcrt.replydate) HXNumber "
					+ " from LOPRTManager lom "
					+ " inner join LCCont lcc on lcc.ProposalContno = lom.otherno "
					+ " left join LCRReport lcrt on lcrt.prtseq = lom.prtseq "
					+ " where 1 = 1 "
					+ " and lom.OtherNoType = '02' "
					+ " and lom.Code = '04' "
					+ " and lom.MakeDate >= '"
					+ mStartDate
					+ "' "
					+ " and lom.MakeDate <= '"
					+ mEndDate
					+ "' "
					+ " and lcc.salechnl in ('01','10') "
					+ " group by substr(lcc.ManageCom,1,4) "
					+ " order by managecom asc " + " with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[4];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					ListInfo[3] = tSSRS1.GetText(i, 4);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[4];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";
				ListInfo[3] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("4")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构,count(1) 件数,sum(prem) 保费 "
			+" from lccont lcc "
			+" inner join ljtempfee ljtf on ljtf.otherno = lcc.prtno "
			+" where 1 = 1 "
			+" and lcc.uwdate >= '"+mStartDate+"' " 
			+" and lcc.uwdate <= '"+mEndDate+"' "   
			+" and lcc.appflag = '0' "
			+" and lcc.uwflag in ('4','9')  "
			+" and lcc.salechnl in ('01','10') "
			+" and lcc.conttype = '1' "
			+" and ljtf.enteraccdate is null " 
			+" group by substr(lcc.ManageCom,1,4) " 
			+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[3];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[3];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}

		if (mType.equals("5")) {
			String sql1 = " select "
				+" substr(bpomds.ManageCom,1,4) ManageCom, "
				+" count(1) Number "  
				+" from BPOMISSIONDETAILSTATE bpomds "
				+" where 1 = 1 "
				+" and bpomds.Operateresult in ('0') "
				+" and bpomds.MakeDate >= '"+mStartDate+"' " 
				+" and bpomds.MakeDate <= '"+mEndDate+"' "   
				+" group by substr(bpomds.ManageCom,1,4) "
				+" with ur "; 
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[2];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[2];
				ListInfo[0] = "";
				ListInfo[1] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("6")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构,count(1) 撤消件数,sum(prem) 保费 "
				+" from lccont lcc "
				+" where 1 = 1 "
				+" and lcc.uwflag = 'a' "
				+" and lcc.uwdate >= '"+mStartDate+"' "    
				+" and lcc.uwdate <= '"+mEndDate+"' "     
				+" and lcc.salechnl in ('01','10') " 
				+" and lcc.conttype = '1' "
				+" group by substr(lcc.ManageCom,1,4) "
				+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[3];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[3];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("7")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构,count(1) 新单删除件数,sum(prem) 保费 " 
				+" from lobcont lcc "
				+" where 1 = 1 "
				+" and lcc.makedate >= '"+mStartDate+"' " 
				+" and lcc.makedate <= '"+mEndDate+"' "  
				+" and lcc.salechnl in ('01','10') " 
				+" and lcc.conttype = '1' "
				+" group by substr(lcc.ManageCom,1,4) "
				+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[3];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[3];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("8")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构,count(1) 件数 " 
				+" from lccont lcc "
				+" where 1 = 1 "
				+" and lcc.inputdate >= '"+mStartDate+"' " 
				+" and lcc.inputdate <= '"+mEndDate+"' "  
				+" and lcc.salechnl in ('01','10') "
				+" and lcc.conttype = '1' "
				+" and exists(select 1 from LCCUWMaster lwm where lwm.contno = lcc.contno and lwm.Autouwflag != '1') "
				+" group by substr(lcc.ManageCom,1,4) "
				+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[2];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[2];
				ListInfo[0] = "";
				ListInfo[1] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("9")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构, "
				+" case  when lcc.UWFlag = '1' then '谢绝承保' "
				+" 		when lcc.UWFlag = '4' then '变更承保' "
				+" 		when lcc.UWFlag = '8' then '延期承保' "
				+" 		when lcc.UWFlag = '9' then '正常承保' end 类型, " 
				+" count(1) 件数, sum(prem) 保费 "
				+" from lccont lcc "
				+" where 1 = 1 "
				+" and lcc.ContType = '1' " 
				+" and lcc.salechnl in ('01','10') "
				+" and lcc.inputdate >= '"+mStartDate+"' "  
				+" and lcc.inputdate <= '"+mEndDate+"' "   
				+" and lcc.UWFlag in ('1','4','8','9') " 
				+" group by substr(lcc.ManageCom,1,4), lcc.UWFlag " 
				+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[4];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					ListInfo[2] = tSSRS1.GetText(i, 3);
					ListInfo[3] = tSSRS1.GetText(i, 4);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[4];
				ListInfo[0] = "";
				ListInfo[1] = "";
				ListInfo[2] = "";
				ListInfo[3] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		if (mType.equals("10")) {
			String sql1 = " select substr(lcc.ManageCom,1,4) 管理机构,count(1) 体检阳性率总件数 " 
				+" from LOPRTManager lom "
				+" inner join LCCont lcc on lcc.ProposalContno = lom.otherno "
				+" where 1 = 1 "
				+" and lom.OtherNoType = '02' "
				+" and lom.Code = '03' "
				+" and lom.MakeDate >= '"+mStartDate+"' "  
				+" and lom.MakeDate <= '"+mEndDate+"' "    
				+" and lcc.salechnl in ('01','10') "
				+" and exists (select 1 from LCDiseaseResult lcdr where lcdr.ProposalContno = lom.otherno) "
				+" group by substr(lcc.ManageCom,1,4) "
				+" with ur ";
			System.out.println("SQL:" + sql1);
			SSRS tSSRS1 = tExeSQL.execSQL(sql1);
			if (tSSRS1.MaxRow > 0) {
				int num1 = tSSRS1.getMaxRow();
				String tContent[][] = new String[num1][];
				for (int i = 1; i <= num1; i++) {
					String ListInfo[] = new String[2];
					ListInfo[0] = tSSRS1.GetText(i, 1);
					ListInfo[1] = tSSRS1.GetText(i, 2);
					tContent[i - 1] = ListInfo;

				}
				mCSVFileWrite.addContent(tContent);

			} else {
				String tContent[][] = new String[1][];
				String ListInfo[] = new String[2];
				ListInfo[0] = "";
				ListInfo[1] = "";

				tContent[0] = ListInfo;
				mCSVFileWrite.addContent(tContent);

			}
		}
		
		
		
		
		if (!mCSVFileWrite.writeFile()) {
			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 得到xml的输入流
	 * 
	 * @return InputStream
	 */
	public InputStream getInputStream() {
		return mXmlExport.getInputStream();
	}

	public String getManagecomName(String com) {
		String sq = "select name from ldcom where comcode='" + com
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
		return tGrpTypeSSRS.GetText(1, 1);
	}

}
