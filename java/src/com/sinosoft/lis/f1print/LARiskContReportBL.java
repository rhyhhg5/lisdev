package com.sinosoft.lis.f1print;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class LARiskContReportBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mOutXmlPath = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mManageCom=null;
    private String mManageComName=null;
    private String mAgentGroup=null;
    private String mStartDate=null;
    private String mEndDate=null;

    public LARiskContReportBL()
    {
    }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }
    
    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {	
    	StringBuffer tSB=new StringBuffer();
    	StringBuffer tSB2=new StringBuffer();
        ExeSQL tExeSQL = new ExeSQL();
        double tTotalPrem=0;
        if(this.mAgentGroup!=null&&!this.mAgentGroup.equals("")){
        	tSB.append("select decimal(value(D+D1+D2,0),12,2) from ( select (select")
        		.append(" value(sum(b.prem),0) from lcpol b,lccont c,lmriskApp y ")
        		.append("where c.contno=b.contno and c.salechnl='01' and y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and y.risktype7='1') D,(select value(sum(b.prem),0)  from lbpol b,lbcont c,lmriskApp y where")
        		.append(" c.contno=b.contno and c.salechnl='01'  and y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and y.risktype7='1') D1,(select value(sum(b.prem),0)  from lbpol b,lccont c,lmriskApp y where")
        		.append(" c.contno=b.contno and c.salechnl='01'  and y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and y.risktype7='1') D2 from  dual) as x  with ur");
        	String tempResult=tExeSQL.getOneValue(tSB.toString());
        	if(tExeSQL.mErrors.needDealError() || tempResult==null || tempResult.equals(""))
            {
                System.out.println(tExeSQL.mErrors.getErrContent());

                CError tError = new CError();
                tError.moduleName = "LARiskContReportBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有查询到需要下载的数据";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        	tTotalPrem=Double.parseDouble(tempResult);
        	tSB.delete(0,tSB.length());
        	tSB.append("select B,rtrim(char(rownumber() over() + 0 )),d,e,f,g,h from (")
        		.append("select B,C+C1+C2 d,case when C+C1+C2=0  then 0 else ")
        		.append("decimal(round(decimal(D+D1+D2,14,2)/(C+C1+C2),2),14,2) end e,")
        		.append("decimal(round(decimal(D+D1+D2,14,2)/10000,6),14,6) f,")
        		.append("case when E+E1=0 then  0 else decimal(round(decimal(D+D1+D2,14,2)*2/(E+E1),2),14,2) end  g,")
        		.append("decimal(decimal(value(D+D1+D2,0),14,2)/")
        		.append(tTotalPrem)
        		.append(",12,6)*100 h from (select (select b.riskname  from lmrisk b where b.riskcode=y.riskcode) B,")
        		.append("(select count(distinct b.contno) from lcpol b,lccont c  where c.contno=b.contno and c.salechnl='01' and")
        		.append(" y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') C,")
        		.append("(select count(distinct b.contno) from lbpol b,lbcont c  where c.contno=b.contno and")
        		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') C1,")
        		.append("(select count(distinct b.contno) from lbpol b,lccont c  where c.contno=b.contno and")
        		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') C2,")
        		.append("(select value(sum(b.prem),0) from lcpol b,lccont c  where c.contno=b.contno and")
        		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') D,")
        		.append("(select value(sum(b.prem),0) from lbpol b,lbcont c  where c.contno=b.contno and")
        		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') D1,")
        		.append("(select value(sum(b.prem),0) from lbpol b,lccont c  where c.contno=b.contno and")
        		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
        		.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%') and ")
        		.append("c.makedate<='")
        		.append(this.mEndDate)
        		.append("' and c.makedate>='")
        		.append(this.mStartDate)
        		.append("' and c.managecom like '")
        		.append(this.mManageCom)
        		.append("%') D2,")
        		.append("(select count(b.agentcode) from laagent b where  (b.outworkdate>'")
        		.append(this.mStartDate)
        		.append("' or b.outworkdate is null) and b.employdate<='")
        		.append(this.mStartDate)
        		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and ")
        		.append("b.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%')) E,")
        		.append("(select count(b.agentcode) from laagent b where  (b.outworkdate>'")
        		.append(this.mEndDate)
        		.append("' or b.outworkdate is null) and b.employdate<='")
        		.append(this.mEndDate)
        		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and ")
        		.append("b.agentgroup in (select a.AgentGroup from labranchgroup a ")
        		.append("where a.managecom like '")
        		.append(this.mManageCom)
        		.append("%' and a.branchtype='1' and a.branchtype2='01'")
        		.append(" and a.branchseries like '%")
        		.append(this.mAgentGroup)
        		.append("%')) E1 ")
        		.append("from lmriskApp y where y.risktype7='1' ")
        		.append("order by D DESC) as x) as hh order by f desc with ur");
        	tSB2.append("select '合计' b,1,C+C1+C2 c,case when C+C1+C2=0  then 0 else ")
    			.append("decimal(round(decimal(D+D1+D2,14,2)/(C+C1+C2),2),14,2) end e,")
    			.append("decimal(round(decimal(D+D1+D2,14,2)/10000,6),14,6) f,case when E+E1=0 then  0 else ")
    			.append("decimal (round(decimal(D+D1+D2,14,2)*2/(E+E1),2),14,2) end g,100  from")
    			.append("( select ")
    			.append("(select count(distinct b.contno) from lcpol b,lccont c,lmriskApp y  where c.contno=b.contno and c.salechnl='01' and")
    			.append(" y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') C,")
    			.append("(select count(distinct b.contno) from lbpol b,lbcont c,lmriskApp y   where c.contno=b.contno and")
    			.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') C1,")
    			.append("(select count(distinct b.contno) from lbpol b,lccont c,lmriskApp y   where c.contno=b.contno and")
    			.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') C2,")
    			.append("(select value(sum(b.prem),0) from lcpol b,lccont c,lmriskApp y   where c.contno=b.contno and")
    			.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') D,")
    			.append("(select value(sum(b.prem),0) from lbpol b,lbcont c,lmriskApp y   where c.contno=b.contno and")
    			.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') D1,")
    			.append("(select value(sum(b.prem),0) from lbpol b,lccont c,lmriskApp y   where c.contno=b.contno and")
    			.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    			.append("c.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%') and ")
    			.append("c.makedate<='")
    			.append(this.mEndDate)
    			.append("' and c.makedate>='")
    			.append(this.mStartDate)
    			.append("' and c.managecom like '")
    			.append(this.mManageCom)
    			.append("%') D2,")
    			.append("(select count(b.agentcode)  from  laagent b where  (b.outworkdate>'")
    			.append(this.mStartDate)
    			.append("' or b.outworkdate is null) and b.employdate<='")
    			.append(this.mStartDate)
    			.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and ")
    			.append("b.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%')) E,")
    			.append("(select count(b.agentcode)  from  laagent b where  (b.outworkdate>'")
    			.append(this.mEndDate)
    			.append("' or b.outworkdate is null) and b.employdate<='")
    			.append(this.mEndDate)
    			.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and ")
    			.append("b.agentgroup in (select a.AgentGroup from labranchgroup a ")
    			.append("where a.managecom like '")
    			.append(this.mManageCom)
    			.append("%' and a.branchtype='1' and a.branchtype2='01'")
    			.append(" and a.branchseries like '%")
    			.append(this.mAgentGroup)
    			.append("%')) E1 from dual) as x with ur");
        }else{
        	tSB.append("select decimal(value(D+D1+D2,0),12,2) from ( select (select")
    		.append(" value(sum(b.prem),0) from lcpol b,lccont c,lmriskApp y ")
    		.append("where c.contno=b.contno and c.salechnl='01' and y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%' and y.risktype7='1') D,(select value(sum(b.prem),0)  from lbpol b,lbcont c,lmriskApp y where")
    		.append(" c.contno=b.contno and c.salechnl='01'  and y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%' and y.risktype7='1') D1,(select value(sum(b.prem),0)  from lbpol b,lccont c,lmriskApp y where")
    		.append(" c.contno=b.contno and c.salechnl='01'  and y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%' and y.risktype7='1') D2 from  dual) as x  with ur");
        	String tempResult=tExeSQL.getOneValue(tSB.toString());
        	if(tExeSQL.mErrors.needDealError() || tempResult==null || tempResult.equals(""))
            {
                System.out.println(tExeSQL.mErrors.getErrContent());

                CError tError = new CError();
                tError.moduleName = "LARiskContReportBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有查询到需要下载的数据";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        	tTotalPrem=Double.parseDouble(tempResult);
    	tSB.delete(0,tSB.length());
    	tSB.append("select B,rtrim(char(rownumber() over() + 0 )),d,e,f,g,h from (")
    		.append("select B,C+C1+C2 d,case when C+C1+C2=0  then 0 else ")
    		.append("decimal(round(decimal(D+D1+D2,14,2)/(C+C1+C2),2),14,2) end e,")
    		.append("decimal(round(decimal(D+D1+D2,14,2)/10000,6),14,6) f,")
    		.append("case when E+E1=0 then  0 else decimal(round(decimal(D+D1+D2,14,2)*2/(E+E1),2),14,2) end  g,")
    		.append("decimal(decimal(value(D+D1+D2,0),14,2)/")
    		.append(tTotalPrem)
    		.append(",12,6)*100 h from (select (select b.riskname  from lmrisk b where b.riskcode=y.riskcode) B,")
    		.append("(select count(distinct b.contno) from lcpol b,lccont c  where c.contno=b.contno and c.salechnl='01' and")
    		.append(" y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C,")
    		.append("(select count(distinct b.contno) from lbpol b,lbcont c  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C1,")
    		.append("(select count(distinct b.contno) from lbpol b,lccont c  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C2,")
    		.append("(select value(sum(b.prem),0) from lcpol b,lccont c  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D,")
    		.append("(select value(sum(b.prem),0) from lbpol b,lbcont c  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D1,")
    		.append("(select value(sum(b.prem),0) from lbpol b,lccont c  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D2,")
    		.append("(select count(b.agentcode) from laagent b where  (b.outworkdate>'")
    		.append(this.mStartDate)
    		.append("' or b.outworkdate is null) and b.employdate<='")
    		.append(this.mStartDate)
    		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    		.append(this.mManageCom)
    		.append("%') E,")
    		.append("(select count(b.agentcode) from laagent b where  (b.outworkdate>'")
    		.append(this.mEndDate)
    		.append("' or b.outworkdate is null) and b.employdate<='")
    		.append(this.mEndDate)
    		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    		.append(this.mManageCom)
    		.append("%') E1 ")
    		.append("from lmriskApp y where y.risktype7='1' ")
    		.append("order by D DESC) as x) as hh order by f desc with ur");
    		tSB2.append("select '合计' b,1,C+C1+C2 c,case when C+C1+C2=0  then 0 else ")
    		.append("decimal(round(decimal(D+D1+D2,14,2)/(C+C1+C2),2),14,2) end e,")
    		.append("decimal(round(decimal(D+D1+D2,14,2)/10000,6),14,6) f,case when E+E1=0 then  0 else ")
    		.append("decimal (round(decimal(D+D1+D2,14,2)*2/(E+E1),2),14,2) end g,100  from")
    		.append("( select ")
    		.append("(select count(distinct b.contno) from lcpol b,lccont c,lmriskApp y  where c.contno=b.contno and c.salechnl='01' and")
    		.append(" y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C,")
    		.append("(select count(distinct b.contno) from lbpol b,lbcont c,lmriskApp y  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C1,")
    		.append("(select count(distinct b.contno) from lbpol b,lccont c,lmriskApp y  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') C2,")
    		.append("(select value(sum(b.prem),0) from lcpol b,lccont c,lmriskApp y  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D,")
    		.append("(select value(sum(b.prem),0) from lbpol b,lbcont c,lmriskApp y  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D1,")
    		.append("(select value(sum(b.prem),0) from lbpol b,lccont c,lmriskApp y  where c.contno=b.contno and")
    		.append(" c.salechnl='01'  and  y.riskcode=b.riskcode and y.risktype7='1' and ")
    		.append("c.makedate<='")
    		.append(this.mEndDate)
    		.append("' and c.makedate>='")
    		.append(this.mStartDate)
    		.append("' and c.managecom like '")
    		.append(this.mManageCom)
    		.append("%') D2,")
    		.append("(select count(b.agentcode)  from  laagent b where  (b.outworkdate>'")
    		.append(this.mStartDate)
    		.append("' or b.outworkdate is null) and b.employdate<='")
    		.append(this.mStartDate)
    		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    		.append(this.mManageCom)
    		.append("%') E,")
    		.append("(select count(b.agentcode)  from  laagent b where  (b.outworkdate>'")
    		.append(this.mEndDate)
    		.append("' or b.outworkdate is null) and b.employdate<='")
    		.append(this.mEndDate)
    		.append("' and b.branchtype='1' and  b.branchtype2='01' and  b.managecom like '")
    		.append(this.mManageCom)
    		.append("%') E1 from dual) as x with ur");
        }
        SSRS tSSRS = tExeSQL.execSQL(tSB.toString());
        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LARiskContReportBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 6][20];
       
        mToExcel[0][0] = "中国人民健康保险股份有限公司";
        mToExcel[1][0] = this.mManageComName+"交单险种统计表（"
        				+this.mStartDate+"-"+this.mEndDate+"）";
        mToExcel[2][0] = "制表单位："+this.mManageComName;
        mToExcel[2][4] = "制表时间："+this.mCurrentDate+" "+this.mCurrentTime;
        mToExcel[3][0] = "险种名称";
        mToExcel[3][1] = "本期累计保费排名";
        mToExcel[3][2] = "交单件数（件）";
        mToExcel[3][3] = "件均保费（元）";
        mToExcel[3][4] = "本期累计保费（万元）";
        mToExcel[3][5] = "人均保费（元）";
        mToExcel[3][6] = "保费占比（%）";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row+3][col - 1] = tSSRS.GetText(row, col);
            }
        }
        SSRS tSSRS2 = tExeSQL.execSQL(tSB2.toString());
        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LARiskContReportBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询汇总数据时出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        for(int col = 1; col <= tSSRS2.getMaxCol(); col++)
        {
            mToExcel[tSSRS.getMaxRow()+4][col - 1] = tSSRS2.GetText(1, col);
        }
        mToExcel[tSSRS.getMaxRow()+5][0]="总经理：";
        mToExcel[tSSRS.getMaxRow()+5][2]="复核：";
        mToExcel[tSSRS.getMaxRow()+5][5]="制表：";
        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);
        
        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LARiskContReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        this.mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        this.mManageCom = (String) tf.getValueByName("ManageCom");
        this.mManageComName = (String) tf.getValueByName("ManageComName");
        this.mAgentGroup = (String) tf.getValueByName("AgentGroup");
        this.mStartDate = (String) tf.getValueByName("StartDate");
        this.mEndDate = (String) tf.getValueByName("EndDate");
       //mtype = (String) tf.getValueByName("Type");
        if(mOutXmlPath == null || mManageCom == null 
        		|| mManageComName == null  
        		|| mStartDate == null || mEndDate == null)
        {
            CError tError = new CError();
            tError.moduleName = "LARiskContReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        LARiskContReportBL LARiskContReportBL = new
            LARiskContReportBL();
    }
}
