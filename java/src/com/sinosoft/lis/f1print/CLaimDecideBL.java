package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LBInsuredSet;

public class CLaimDecideBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    //取得的延期承保原因
    private String mUWError = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mdirect;
    private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new
            LOBatchPRTManagerSchema();


    public CLaimDecideBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("ClaimDecideBL....");
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOBatchPRTManagerSchema.setSchema((LOBatchPRTManagerSchema) cInputData.
                                           getObjectByObjectName(
                "LOBatchPRTManagerSchema", 0));
        mdirect = "";
        mdirect += mLOBatchPRTManagerSchema.getStandbyFlag2();
        mLLCaseSchema.setCaseNo(mLOBatchPRTManagerSchema.getOtherNo());
        if (mLOBatchPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ComCode);
        tLDComDB.getInfo();
        String CaseNo = mLLCaseSchema.getCaseNo();
        //System.out.println("CaseNo="+CaseNo);
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(CaseNo);

        if (!tLLCaseDB.getInfo()) {
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            buildError("outputXML", "mLLCaseDB在取得打印队列中数据时发生错误");
            return false;

        }
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        System.out.println("受理日期" + mLLCaseSchema.getRgtDate());
        String RgtDate = getCDate(mLLCaseSchema.getRgtDate());
        System.out.println("中文受理日期" + RgtDate);
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            buildError("outputXML", "mLLRegisterDB在取得打印队列中数据时发生错误");
            return false;
        }
        String getmode = tLLRegisterDB.getGetMode() + "";
        String Statement = "";
        if (!getmode.equals("1") && !getmode.equals("2")) {
            Statement = "    本公司会在发出本通知书的10个工作日内，将上述数额保险金转帐至您指定的理赔帐户，请及时查收。";
        } else {
            Statement = "    在收到本通知后，请您及时携带身份证原件到本公司领取赔款。如需委托他人\n"
                        + "代领，请注意提供授权委托书、保险金申领人和代领人的身份证原件。\n";
        }
        LLClaimDB tLLClaimDB = new LLClaimDB();

        tLLClaimDB.setCaseNo(CaseNo);
        LLClaimSet tLLClaimSet = new LLClaimSet();
        tLLClaimSet.set(tLLClaimDB.query());
        if (tLLClaimSet.size() == 0) {
            buildError("tLLClaimSet", "没有得到足够的信息！");
            return false;
        }
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema = tLLClaimSet.get(1).getSchema();
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        // xmlexport.createDocuments("ClaimDecide.vts", mGlobalInput); //最好紧接着就初始化xml文档
        if (mdirect.equals("1")) {
            xmlexport.createDocument("ClaimDecide.vts", "print");
        } else {
            xmlexport.createDocuments("ClaimDecide.vts", mGlobalInput);
        }
//
//       LLBnfDB tLLBnfDB = new LLBnfDB();
//       System.out.print(CaseNo);
//       tLLBnfDB.setCaseNo(CaseNo);
//       LLBnfSet tLLBnfSet = new LLBnfSet();
//       tLLBnfSet.set(tLLBnfDB.query());
//
//       if(tLLBnfSet==null||tLLBnfSet.size() == 0)
//      {
//        buildError("tLLBnfSet", "没有得到足够的信息！");
//        return false;
//      }
//       LLBnfSchema tLLBnfSchema = new LLBnfSchema();
//       tLLBnfSchema = tLLBnfSet.get(1).getSchema();
//

        String tCaseGetMode = "";
//       tCaseGetMode = mLLCaseSchema.getCaseGetMode();
//       if(tCaseGetMode!=null&&!tCaseGetMode.equals(""))
//       {
//        //System.out.println("tCaseGetMode="+tCaseGetMode);
//      if(tCaseGetMode.equals("1")||tCaseGetMode.equals("2"))
//
//        xmlexport.addDisplayControl("displaybank");
//     else
//         xmlexport.addDisplayControl("displaymoney");
//       }
        //其它模版上单独不成块的信息

        //生成-年-月-日格式的日期


        //模版自上而下的元素


        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";

        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("SysDate", SysDate);
//        System.out.println(mLLCaseSchema.getCustomerSex());
////////////////////////////////////////////////////////////////////////////////////////
        //add by dongjf
        //解决llcase表中,有时没有存客户性别,导致程序报空指针异常.
        String tCustSex = ""; //客户性别
        String tCustNo = "";
        tCustNo = mLLCaseSchema.getCustomerNo();
        if (tCustNo.equals("")) {
            buildError("llcaseDB", "查询客户信息失败！");
            return false;
        }
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() < 1) {
            LBInsuredDB tLBInsuredDB = new LBInsuredDB();
            LBInsuredSet tLBInsuredSet = new LBInsuredSet();
            tLBInsuredDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
            tLBInsuredSet = tLBInsuredDB.query();
            if (tLBInsuredSet.size() < 1) {
                buildError("tLBInsuredDB", "查询客户性别失败！");
                return false;
            } else {
                LBInsuredSchema tLBInsuredSchema = new LBInsuredSchema();
                tLBInsuredSchema = tLBInsuredSet.get(1);
                tCustSex = tLBInsuredSchema.getSex();
            }
        } else {
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema = tLCInsuredSet.get(1);
            tCustSex = tLCInsuredSchema.getSex();
        }

        String tGrpNameSQL =
                "SELECT DISTINCT appntname FROM llclaimpolicy WHERE caseno='" +
                mLLCaseSchema.getCaseNo() +
                "'AND grpcontno<>'00000000000000000000' WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tGrpNameSQL);
        String tGrpName = "";
        if (tSSRS.getMaxRow() > 0) {
            tGrpName = tSSRS.GetText(1, 1);
        }
        ListTable tAgentTable = new ListTable();
        tAgentTable.setName("Agent");
        String[] Title = {"AgentName", "AgentCode", "AgentGroup"};
        String[] strCol;
        String tAgentSQL = "SELECT DISTINCT b.name,a.agentcode,c.name "
                           + " FROM llclaimdetail a,laagent b,labranchgroup c "
                           +
                           " WHERE a.agentcode=b.agentcode AND b.agentgroup=c.agentgroup "
                           + " AND a.caseno='" + mLLCaseSchema.getCaseNo() +
                           "' WITH UR";
        SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
        if (tAgentSSRS.getMaxRow() <= 0) {
            strCol = new String[3];
            strCol[0] = "";
            strCol[1] = "";
            strCol[2] = "";
            tAgentTable.add(strCol);
        }
        for (int j = 1; j <= tAgentSSRS.getMaxRow(); j++) {
            strCol = new String[3];
            strCol[0] = tAgentSSRS.GetText(j, 1);
            strCol[1] = tAgentSSRS.GetText(j, 2);
            strCol[2] = tAgentSSRS.GetText(j, 3);
            tAgentTable.add(strCol);
        }
        //////////////////////////////////////////////////////////////////////////////////////

       
            texttag.add("Name", mLLCaseSchema.getCustomerName() + "先生/女士"); //投保人名称
        System.out.println("先生/女士-----");
        texttag.add("CustomerName", mLLCaseSchema.getCustomerName()); //理赔号
        texttag.add("PostCode", tLLRegisterDB.getPostCode()); //理赔号
        texttag.add("PostalAddress", tLLRegisterDB.getRgtantAddress()); //理赔号
        //System.out.println("mLLCaseSchema.getInsuredNo()="+mLLCaseSchema.getInsuredNo());
        texttag.add("Money", tLLClaimSchema.getRealPay()); //体检日期
        texttag.add("RgtDate", RgtDate); //申请日期
        texttag.add("Statement", Statement);
        texttag.add("ClaimPhone", tLDComDB.getClaimReportPhone());
        texttag.add("Fax", tLDComDB.getFax());
        texttag.add("ClaimAddress", tLDComDB.getLetterServicePostAddress());
        texttag.add("ClaimZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ComName", tLDComDB.getLetterServiceName());
        texttag.add("XI_CaseNo", mLLCaseSchema.getCaseNo());
        texttag.add("XI_ContNo", "");
        texttag.add("XI_ManageCom", mLLCaseSchema.getMngCom());
        texttag.add("XI_CustomerNo", mLLCaseSchema.getCustomerNo());
        texttag.add("GrpName", tGrpName);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tEndListTable);
        xmlexport.addListTable(tAgentTable, Title);
//        xmlexport.outputDocumentToFile("d:\\", "claimdecide"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private String getCDate(String rgtdate) {
        String year = rgtdate.substring(0, 4);
        int dashid1 = rgtdate.indexOf("-") + 1;
        int dashid2 = rgtdate.indexOf("-", 6) + 1;
        String month = rgtdate.substring(dashid1, dashid2 - 1);
        String day = rgtdate.substring(dashid2);
        String ChineseDate = year + "年" + month + "月" + day + "日";
        return ChineseDate;
    }

    public static void main(String[] args) {

        CLaimDecideBL tCLaimDecideBL = new CLaimDecideBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("550000000000098");
        tVData.addElement(tLLCaseSchema);
        tCLaimDecideBL.submitData(tVData, "PRINT");
        VData vdata = tCLaimDecideBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }


}
