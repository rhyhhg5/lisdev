package com.sinosoft.lis.f1print;

/**
 * <p>Title: InsuredAllDownLoadBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class InsuredAllDownLoadBL {
	public InsuredAllDownLoadBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private String mGrpcontNo = "";

	private String moperator = "";

	private XmlExport mXmlExport = null;

	private String[] mDataList = null;

	private ListTable mListTable = new ListTable();

	private String currentDate = PubFun.getCurrentDate();

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		}
		return true;
	}

	public String getYear(String getstartdate, String Birthday) {
		String getYear = "";
		if (getstartdate != null && !getstartdate.equals("")) {
			getYear = "" + PubFun.calInterval(Birthday, getstartdate, "Y");
		}
		return getYear;
	}

	private boolean getDataList() {

		java.text.NumberFormat formater = java.text.DecimalFormat.getInstance();
		formater.setMaximumFractionDigits(2);
		formater.setMinimumFractionDigits(2);

		String sql = " select  rownumber() over() as no,a.Prtno,(select grpname from lcgrpcont where grpcontno=a.grpcontno)Grpname,a.name,"
				+ "(select codename from ldcode where codetype='relation' and code =a.RelationToMainInsured) Relation,(select codename from ldcode where codetype='sex' and code =a.sex) Sex,"
				+ " a.birthday,(select codename from ldcode where codetype='idtype' and code =a.IDType) IDType,a.IDNo,(select codename from ldcode where codetype='occupationtype' and code =a.OccupationType) Occupationtype,"
				+ "(Select BankName from LDBank where bankcode=a.BankCode),a.BankAccNo,a.AccName,a.JoinCompanyDate,(select Gradename from lcgrpposition where grpcontno =a.grpcontno  and gradecode=a.Position) Posotion,"
				+ "'getyear',(select getdutyname from lmdutygetalive where getdutycode='671201' and getdutykind=(select distinct Getdutykind from lcget where  grpcontno=a.grpcontno and insuredno=a.insuredno and Getdutykind is not null))GetDutyName,"
				+ "(select nvl(sum(l.prem),0) from lcprem l,lmdutypay m where l.polno in(select Polno from lcpol where contno=a.contno and insuredno=a.insuredno) and l.payplancode=m.payplancode and m.AccPayClass ='3') as Pubprem,"
				+ "(select nvl(sum(l.prem),0) from lcprem l,lmdutypay m where l.polno in(select Polno from lcpol where contno=a.contno and insuredno=a.insuredno) and l.payplancode=m.payplancode and m.AccPayClass ='4') as Grpprem,"
				+ "(select nvl(sum(l.prem),0) from lcprem l,lmdutypay m where l.polno in(select Polno from lcpol where contno=a.contno and insuredno=a.insuredno) and l.payplancode=m.payplancode and m.AccPayClass in('5','6')) as Selfprem,"
				+ "(select case when sum(round(b.prem*nvl(c.feevalue,0),2)) is null then 0 else sum(round(b.prem*nvl(c.feevalue,0),2)) end from lcprem b,lcgrpfee c where c.grpcontno=b.grpcontno and c.payplancode=b.payplancode and b.polno in (select polno from lcpol where contno=a.contno and insuredno=a.insuredno)),"
				+ "(Select BankName from LDBank where bankcode=(select BankCode from lccont where contno=a.contno))PreBankName,"
				+ "(Select  BankAccNo from lccont where contno=a.contno) PreBankAccNo,(Select  AccName from lccont where contno=a.contno) PreAccName ,a.insuredno , "
				+ "(select min(getstartdate) from lcget where grpcontno=a.grpcontno and InsuredNo=a.InsuredNo and GetDutyKind is not null), "
				+ "(select poltype from lccont where contno=a.contno and grpcontno=a.grpcontno) "
				+ " from lcinsured a "
				+ " where grpcontno='"
				+ mGrpcontNo
				+ "' order by contno";
		//String getYearString ="select "
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(sql);
		System.out.println(sql);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

			String Info[] = new String[24];
			System.out.println("tSSRS.GetText(i, 1)" + tSSRS.GetText(i, 1));
			if (tSSRS.GetText(i, 27).equals("2")) {
				Info[0] = String.valueOf(i);
				System.out.println("info[0]" + Info[0]);
				Info[1] = tSSRS.GetText(i, 2);
				Info[2] = tSSRS.GetText(i, 3);
				Info[3] = tSSRS.GetText(i, 4);
				Info[4] = tSSRS.GetText(i, 5);
				Info[5] = "";
				Info[6] = "";
				Info[7] = "";
				Info[8] = "";
				Info[9] = "";

				Info[10] = "";
				Info[11] = "";
				Info[12] = "";
				Info[13] = "";

				Info[14] = "";
				Info[15] = "";
				Info[16] = "";
				Info[17] = tSSRS.GetText(i, 18);
				Info[18] = tSSRS.GetText(i, 19);

				Info[19] = tSSRS.GetText(i, 20);
				Info[20] = formater.format(Double.parseDouble(tSSRS.GetText(i,
						21)));
				Info[21] = "";
				Info[22] = "";
				Info[23] = "";
			} else {

				Info[0] = String.valueOf(i);
				System.out.println("info[0]" + Info[0]);
				Info[1] = tSSRS.GetText(i, 2);
				Info[2] = tSSRS.GetText(i, 3);
				Info[3] = tSSRS.GetText(i, 4);
				Info[4] = tSSRS.GetText(i, 5);
				Info[5] = tSSRS.GetText(i, 6);
				Info[6] = tSSRS.GetText(i, 7);
				Info[7] = tSSRS.GetText(i, 8);
				Info[8] = tSSRS.GetText(i, 9);
				Info[9] = tSSRS.GetText(i, 10);

				Info[10] = tSSRS.GetText(i, 11);
				Info[11] = tSSRS.GetText(i, 12);
				Info[12] = tSSRS.GetText(i, 13);
				Info[13] = tSSRS.GetText(i, 14);

				Info[14] = tSSRS.GetText(i, 15);
				Info[15] = getYear(tSSRS.GetText(i, 26), tSSRS.GetText(i, 7));
				Info[16] = tSSRS.GetText(i, 17);
				Info[17] = tSSRS.GetText(i, 18);
				Info[18] = tSSRS.GetText(i, 19);

				Info[19] = tSSRS.GetText(i, 20);
				Info[20] = formater.format(Double.parseDouble(tSSRS.GetText(i,
						21)));
				Info[21] = tSSRS.GetText(i, 22);
				Info[22] = tSSRS.GetText(i, 23);
				Info[23] = tSSRS.GetText(i, 24);
			}

			/*	for (int t=1;t<=tSSRS.getMaxCol();i++) {
			 Info[i-1] = tSSRS.GetText(i, t);
			 } */
			mListTable.add(Info);
		}
		return true;
	}

	/**
	 * 进行数据查询
	 * @return boolean
	 */
	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		//设置模版名称
		mXmlExport.createDocument("InsuredList.vts", "printer");
		System.out.print("dayin252");
		String[] title = { "", "", "", "", "" };
		if (!getDataList()) {
			return false;
		}
		mListTable.setName("Insured");
		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();

		mResult.addElement(mXmlExport);

		return true;
	}

	/**
	 * 取得传入的数据
	 * @return boolean
	 */
	private boolean getInputData(VData pmInputData) {
		mGrpcontNo = (String) pmInputData.get(0);
		moperator = (String) pmInputData.get(1);

		return true;
	}

	/**
	 * 追加错误信息
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "InsuredAllDownLoadBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	private void jbInit() throws Exception {
	}

	public static void main(String[] args) {
	}
}
