package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LLExemptionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.vschema.LLExemptionSet;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.PubFun;

public class ClaimDecideNewBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    //取得的延期承保原因
    private String mUWError = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mdirect;
    private LOPRTManagerSchema mLOPRTManagerSchema = new
            LOPRTManagerSchema();
    private String mflag = null;
    /**豁免险判断时传入的案件号*/
    private String mOtherNo;
    /**豁免险标志*/
    private boolean mExempFlag = false;


    public ClaimDecideNewBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("ClaimDecideBL....");
//        if (!cOperate.equals("PRINT")) {
//            buildError("submitData", "不支持的操作字符串");
//            return false;
//        }
        mflag = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        //准备打印管理表数据
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                           getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        mdirect = "";
        mdirect += mLOPRTManagerSchema.getStandbyFlag2();
        mLLCaseSchema.setCaseNo(mLOPRTManagerSchema.getOtherNo());
        mOtherNo = mLOPRTManagerSchema.getOtherNo();
        if (mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ComCode);
        tLDComDB.getInfo();
        String CaseNo = mLLCaseSchema.getCaseNo();
        //System.out.println("CaseNo="+CaseNo);
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(CaseNo);

        if (!tLLCaseDB.getInfo()) {
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            buildError("outputXML", "mLLCaseDB在取得打印队列中数据时发生错误");
            return false;

        }
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        System.out.println("受理日期" + mLLCaseSchema.getRgtDate());
        String RgtDate = getCDate(mLLCaseSchema.getRgtDate());
        System.out.println("中文受理日期" + RgtDate);
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            buildError("outputXML", "mLLRegisterDB在取得打印队列中数据时发生错误");
            return false;
        }
        String getmode = tLLRegisterDB.getGetMode() + "";
        String Statement = "";
        LLClaimDB tLLClaimDB = new LLClaimDB();

        tLLClaimDB.setCaseNo(CaseNo);
        LLClaimSet tLLClaimSet = new LLClaimSet();
        tLLClaimSet.set(tLLClaimDB.query());
        if (tLLClaimSet.size() == 0) {
            buildError("tLLClaimSet", "没有得到足够的信息！");
            return false;
        }
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema = tLLClaimSet.get(1).getSchema();
        if (!getmode.equals("1") && !getmode.equals("2")&&tLLClaimSchema.getRealPay()>0) {
        	// #3251   理赔决定通知书错别字：帐户 调整为 账户   start
            Statement = "本公司会在发出本通知书的10个工作日内，将上述数额保险金转帐至您指定的理赔账户，请及时查收。";
            // #3251 理赔决定通知书错别字：帐户 调整为 账户  end
        } else if(tLLClaimSchema.getRealPay()>0){
            Statement = "在收到本通知后，请您及时携带身份证原件到本公司领取赔款。如需委托他人代领，请注意提供授权委托书、保险金申领人和代领人的身份证原件。";
        }
        
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        // xmlexport.createDocuments("ClaimDecide.vts", mGlobalInput); //最好紧接着就初始化xml文档
        if (mdirect.equals("1")) {
            xmlexport.createDocument("ClaimDecide.vts", "print");
        } else {
            xmlexport.createDocuments("ClaimDecide.vts", mGlobalInput);
        }
//
//       LLBnfDB tLLBnfDB = new LLBnfDB();
//       System.out.print(CaseNo);
//       tLLBnfDB.setCaseNo(CaseNo);
//       LLBnfSet tLLBnfSet = new LLBnfSet();
//       tLLBnfSet.set(tLLBnfDB.query());
//
//       if(tLLBnfSet==null||tLLBnfSet.size() == 0)
//      {
//        buildError("tLLBnfSet", "没有得到足够的信息！");
//        return false;
//      }
//       LLBnfSchema tLLBnfSchema = new LLBnfSchema();
//       tLLBnfSchema = tLLBnfSet.get(1).getSchema();
//

        String tCaseGetMode = "";
//       tCaseGetMode = mLLCaseSchema.getCaseGetMode();
//       if(tCaseGetMode!=null&&!tCaseGetMode.equals(""))
//       {
//        //System.out.println("tCaseGetMode="+tCaseGetMode);
//      if(tCaseGetMode.equals("1")||tCaseGetMode.equals("2"))
//
//        xmlexport.addDisplayControl("displaybank");
//     else
//         xmlexport.addDisplayControl("displaymoney");
//       }
        //其它模版上单独不成块的信息

        //生成-年-月-日格式的日期


        //模版自上而下的元素


        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";


        texttag.add("JetFormType", "lp000");//理赔决定通知书

        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if(mflag.equals("batch")){
            texttag.add("previewflag", "0");
        }else{
            texttag.add("previewflag", "1");
        }
        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("SysDate", SysDate);
//        System.out.println(mLLCaseSchema.getCustomerSex());
////////////////////////////////////////////////////////////////////////////////////////
        //add by dongjf
        //解决llcase表中,有时没有存客户性别,导致程序报空指针异常.
        String tCustSex = ""; //客户性别
        String tCustNo = "";
        tCustNo = mLLCaseSchema.getCustomerNo();
        if (tCustNo.equals("")) {
            buildError("llcaseDB", "查询客户信息失败！");
            return false;
        }
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() < 1) {
            LBInsuredDB tLBInsuredDB = new LBInsuredDB();
            LBInsuredSet tLBInsuredSet = new LBInsuredSet();
            tLBInsuredDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
            tLBInsuredSet = tLBInsuredDB.query();
            if (tLBInsuredSet.size() < 1) {
                buildError("tLBInsuredDB", "查询客户性别失败！");
                return false;
            } else {
                LBInsuredSchema tLBInsuredSchema = new LBInsuredSchema();
                tLBInsuredSchema = tLBInsuredSet.get(1);
                tCustSex = tLBInsuredSchema.getSex();
            }
        } else {
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            tLCInsuredSchema = tLCInsuredSet.get(1);
            tCustSex = tLCInsuredSchema.getSex();
        }

        String tGrpNameSQL =
                "SELECT DISTINCT appntname FROM llclaimpolicy WHERE caseno='" +
                mLLCaseSchema.getCaseNo() +
                "'AND grpcontno<>'00000000000000000000' WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tGrpNameSQL);
        String tGrpName = "";
        if (tSSRS.getMaxRow() > 0) {
            tGrpName = tSSRS.GetText(1, 1);
        }
        ListTable tAgentTable = new ListTable();
        tAgentTable.setName("Agent");
        String[] Title = {"AgentName", "AgentCode", "AgentGroup"};
        String[] strCol;
        String tAgentSQL = "SELECT DISTINCT b.name, getUniteCode(a.agentcode),c.name "
                           + " FROM llclaimdetail a,laagent b,labranchgroup c "
                           +
                           " WHERE a.agentcode=b.agentcode AND b.agentgroup=c.agentgroup "
                           + " AND a.caseno='" + mLLCaseSchema.getCaseNo() +
                           "' WITH UR";
        SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
        if (tAgentSSRS.getMaxRow() <= 0) {
            strCol = new String[3];
            strCol[0] = "";
            strCol[1] = "";
            strCol[2] = "";
            tAgentTable.add(strCol);
        }
        for (int j = 1; j <= tAgentSSRS.getMaxRow(); j++) {
            strCol = new String[3];
            strCol[0] = tAgentSSRS.GetText(j, 1);
            strCol[1] = tAgentSSRS.GetText(j, 2);
            strCol[2] = tAgentSSRS.GetText(j, 3);
            tAgentTable.add(strCol);
        }
        //////////////////////////////////////////////////////////////////////////////////////

        
        if("1".equals(tLLRegisterDB.getRgtClass()) && "1".equals(tLLRegisterDB.getTogetherFlag())) {
        	texttag.add("Name", mLLCaseSchema.getCustomerName()+ "先生/女士");//被保险人名称
            System.out.println(mLLCaseSchema.getCustomerName()+"------------");
            String addressSql = "select postaladdress from lcaddress where customerno = '" + mLLCaseSchema.getCustomerName()+"' with ur ";
            String address = new ExeSQL().getOneValue(addressSql);
            String zipcodeSql = "select zipcode from lcaddress where customerno = '" + mLLCaseSchema.getCustomerName()+"' with ur ";
            String zipcode = new ExeSQL().getOneValue(zipcodeSql);
            texttag.add("CustomerName", mLLCaseSchema.getCustomerName()); //被保险人姓名
            texttag.add("PostCode",zipcode); //被保险人邮编
            texttag.add("PostalAddress", address); //被保险人地址
        } else {
        	texttag.add("Name", tLLRegisterDB.getRgtantName() + "先生/女士");//申请人名称
            System.out.println(tLLRegisterDB.getRgtantName()+"------------");
            
            texttag.add("CustomerName", tLLRegisterDB.getRgtantName()); //申请人名称
            texttag.add("PostCode", tLLRegisterDB.getPostCode()); //申请人邮编
            texttag.add("PostalAddress", tLLRegisterDB.getRgtantAddress()); //申请人地址
        }
        
//      TODO：判断案件是否有豁免保费，这里只考虑个单案件
        String tExemptionDesc = "";
        String aSQL = "select a.contno,(select riskname from lmriskapp where riskcode=a.riskcode ),a.freestartdate,a.freeamnt,a.polno,a.freepried,b.givetype " +
        "from llexemption a ,llclaimdetail b where a.caseno='"+mOtherNo+"' and a.caseno=b.caseno and a.polno=b.polno with ur";
        SSRS aSSRS = new SSRS();
        ExeSQL aExeSQL = new ExeSQL();
        aSSRS = aExeSQL.execSQL(aSQL);
        if(aSSRS != null && aSSRS.getMaxRow() >0){//存在豁免的保单
        	tExemptionDesc = "经本公司审核决定，";
        	System.out.println("<-Go Into ExemptionDeal()->");
        	for(int i=1;i<=aSSRS.getMaxRow();i++){
        		String aRiskName = aSSRS.GetText(i, 2);
                String aContNo = aSSRS.GetText(i, 1);
                String aFreeStartDate = aSSRS.GetText(i, 3);//豁免开始时间
                aFreeStartDate = getCDate(aFreeStartDate);//转换为中文日期
                String aFreeAmnt = aSSRS.GetText(i, 4);//总共豁免金额
                aFreeAmnt = new DecimalFormat("0.00").format( Double.parseDouble(aFreeAmnt));//美化格式
                //String aPolNo = aSSRS.GetText(i, 5);
                //String aFreePried = aSSRS.GetText(i, 6);
                String aGiveType = aSSRS.GetText(i, 7);//给付结论："6-准予保费豁免","7-不予保费豁免"
                if("6".equals(aGiveType)){
                	tExemptionDesc += "同意"+aContNo+"号保单中《"+aRiskName+"》，自"+aFreeStartDate+"起，豁免保险费"+aFreeAmnt+"元。";
                	
                }else if("7".equals(aGiveType)){
                	tExemptionDesc = "不予豁免保费。";
                }
        	}
        	System.out.println("tExemptionDesc:"+tExemptionDesc);

        }       
        //System.out.println("mLLCaseSchema.getInsuredNo()="+mLLCaseSchema.getInsuredNo());
        texttag.add("Money", new DecimalFormat("0.00").format(tLLClaimSchema.getRealPay())); 
        texttag.add("RgtDate", RgtDate); //申请日期
        texttag.add("Statement", Statement);
        texttag.add("ClaimPhone", tLDComDB.getClaimReportPhone());
        texttag.add("Fax", tLDComDB.getFax());
        texttag.add("ClaimAddress", tLDComDB.getLetterServicePostAddress());
        texttag.add("ClaimZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ComName", tLDComDB.getLetterServiceName());
        texttag.add("XI_CaseNo", mLLCaseSchema.getCaseNo());
        texttag.add("XI_ContNo", "");
        texttag.add("XI_ManageCom", mLLCaseSchema.getMngCom());
        texttag.add("XI_CustomerNo", mLLCaseSchema.getCustomerNo());
        texttag.add("GrpName", tGrpName);
        texttag.add("ExemptionDesc", tExemptionDesc);
      //#30883的（理赔决定通知书修改）
        String bool=" select  a.caseno,a.contno,a.APPNTNO,a.APPNTNAME,a.INSUREDNO,a.INSUREDNAME, "+
        " sum(b.pay) "+
        " from LLClaimpolicy a ,LJAGetclaim b" +
        " where 1=1 and a.caseno='"+CaseNo+"' and a.caseno=b.otherno and b.othernotype='H' " +
        " and a.riskcode in (select riskcode from lmriskapp where risktype = 'L') "  +
        " group by a.caseno,a.contno,a.APPNTNO,a.APPNTNAME,a.INSUREDNO,a.INSUREDNAME"
        ;
        System.out.println("===============#30883的（理赔决定通知书修改）,校验分红SQL=============："+bool);
        SSRS messValue = tExeSQL.execSQL(bool);
        
        if(messValue!=null && messValue.getMaxRow()>0){ //校验是否是分红
        	for (int i = 1; i <= messValue.getMaxRow(); i++) {
        
        String CONTNO=messValue.GetText(i, 2);//保险合同号
        String APPNTNAME=messValue.GetText(i, 4);	//投保人
        String APPNTNO=messValue.GetText(i, 3);	//客户号
        String INSUREDNAME=messValue.GetText(i, 6);			//被保险人
        String INSUREDNO=messValue.GetText(i, 5);		//客户号
        String SUMPAY=messValue.GetText(i, 7);		//本次理赔给付保单红利累积生息账户中余额的本息合计
        
//        texttag.add("CONTNO"+i, CONTNO);
//        texttag.add("APPNTNAME"+i, APPNTNAME);
//        texttag.add("APPNTNO"+i, APPNTNO);
//        texttag.add("INSUREDNAME"+i, INSUREDNAME);
//        texttag.add("INSUREDNO"+i, INSUREDNO);
//        texttag.add("SUMPAY"+i, SUMPAY);
        
//        String MON="保单贷款本金值";		//保单贷款本金
//        String STDAY="贷款起息日值";		//贷款起息日
//        String GDENDAY="规定还款日值";		//规定还款日
//        String SJENDAY="实际还款申请日（结息日）值";		//实际还款申请日（结息日）yyyy年mm月dd日（取受理页面的申请日日期）
//        String INTEREST="保单贷款利息值";		//保单贷款利息
//        String KCSUMMON="本次理赔申请扣除应收取保单还款本金及利息累计合计值";		//本次理赔申请扣除应收取保单还款本金及利息累计合计
        String BonusMessage="   保险合同号"+CONTNO+"：投保人："+APPNTNAME+" 客户号："+APPNTNO+"，被保险人："+INSUREDNAME+" 客户号："+INSUREDNO+"，本次理赔给付保单红利累积生息账户中余额的本息合计："+SUMPAY+"元。";
        texttag.add("BonusMessage", BonusMessage);
//        保险合同号"+CONTNO+"：投保人："+APPNTNAME+" ，客户号："+APPNTNO+" 被保险人："+INSUREDNAME+" 客户号："+INSUREDNO+"，保单贷款本金："+MON+"元，贷款起息日：yyyy年mm月dd日"+STDAY+"，规定还款日：yyyy年mm月dd日"+GDENDAY+"，实际还款申请日（结息日）：yyyy年mm月dd日（取受理页面的申请日日期）"+SJENDAY+"，保单贷款利息："+INTEREST+"元，本次理赔申请扣除应收取保单还款本金及利息累计合计: "+KCSUMMON+"元。
        	}
        }        //#30883的（理赔决定通知书修改）
        
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
       
        xmlexport.addListTable(tEndListTable);
        xmlexport.addListTable(tAgentTable, Title);
        xmlexport.outputDocumentToFile("d:\\", "date"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private String getCDate(String rgtdate) {
        String year = rgtdate.substring(0, 4);
        int dashid1 = rgtdate.indexOf("-") + 1;
        int dashid2 = rgtdate.indexOf("-", 6) + 1;
        String month = rgtdate.substring(dashid1, dashid2 - 1);
        String day = rgtdate.substring(dashid2);
        String ChineseDate = year + "年" + month + "月" + day + "日";
        return ChineseDate;
    }

    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        if(mExempFlag){
        	String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(mLLCaseSchema.getCaseNo()); //存放前台传来的caseno
            tLOPRTManagerSchema.setOtherNoType("5");
            tLOPRTManagerSchema.setCode("lp017");
            tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
            tLOPRTManagerSchema.setAgentCode("");
            tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
            tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setStandbyFlag2(mLLCaseSchema.getCaseNo());
        }else{
        	String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(mLLCaseSchema.getCaseNo()); //存放前台传来的caseno
            tLOPRTManagerSchema.setOtherNoType("5");
            tLOPRTManagerSchema.setCode("lp000");
            tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
            tLOPRTManagerSchema.setAgentCode("");
            tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
            tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setStandbyFlag2(mLLCaseSchema.getCaseNo());
        }
        

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }


    public static void main(String[] args) {

        ClaimDecideNewBL tClaimDecideNewBL = new ClaimDecideNewBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C1100131211000002");
        tVData.addElement(tLLCaseSchema);
        tClaimDecideNewBL.submitData(tVData, "PRINT");
        VData vdata = tClaimDecideNewBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }


}
