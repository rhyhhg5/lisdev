package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.InterfaceTableSchema;

public class IntFaceTabDataBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String BatchNo = "";
    private String DepCode = "";
    private String VoucherType = "";
    private String ReadState = "";
    private String ChargeDate = "";
    private String SerialNo = "";
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	
       

    public IntFaceTabDataBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {

    	System.out.println("Start IntFaceTabDataBL Submit ...");
        try{
        if (!cOperate.equals("UPDATEE") &&!cOperate.equals("UPDATES")) {
        	buiError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }
        
//        置空数据
        if (cOperate.equals("UPDATEE"))
        {
            if (!emptyData()) {
             return false;
            }
        }   
//        屏蔽数据
        if (cOperate.equals("UPDATES"))
        {
            if (!ShieldData()) {
             return false;
                }

            }    
		if(!createFile()){
			return false;
		}        
            }           
            catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "XqqGrpCrsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean emptyData()
    {
        System.out.println("BL->置空数据"); 
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
        System.out.println("batchno : "+ BatchNo);    
        if (SerialNo != null && !SerialNo.equals("") && (VoucherType == null || VoucherType.equals("")) && (DepCode == null || DepCode.equals("")) && !ReadState.equals("4"))
        {
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and SerialNo = '" + SerialNo + "' and ReadState = '" + ReadState + "' ", "UPDATE");
        }
        
        else if (DepCode != null && !DepCode.equals("") && (VoucherType == null || VoucherType.equals("")) && (SerialNo == null || SerialNo.equals("")) && !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        
        else if (VoucherType != null && !VoucherType.equals("") && (SerialNo == null || SerialNo.equals("")) && (DepCode == null || DepCode.equals("")) && !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and VoucherType = '" + VoucherType + "' and ChargeDate = '"+ChargeDate+"' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        
        else if (SerialNo != null && !SerialNo.equals("")&& DepCode != null && !DepCode.equals("") && (VoucherType == null || VoucherType.equals(""))&& !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and SerialNo = '" + SerialNo + "' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        
        else if (SerialNo != null && !SerialNo.equals("")&& VoucherType != null && !VoucherType.equals("")&& (DepCode == null || DepCode.equals(""))&& !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and SerialNo = '" + SerialNo + "' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        
        else if (DepCode != null && !DepCode.equals("")&& VoucherType != null && !VoucherType.equals("")&& (SerialNo == null || SerialNo.equals("")) && !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        
        else if (DepCode != null && !DepCode.equals("")&& VoucherType != null && !VoucherType.equals("") && SerialNo != null && !SerialNo.equals("")&& !ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and ReadState = '" + ReadState + "' and SerialNo = '" + SerialNo + "'", "UPDATE");
        }
        
        else if (!ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = NULL,readdate = NULL,readtime = NULL,voucheryear = NULL,backvdate = NULL,backvtime = NULL,voucherid = NULL where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and ReadState = '" + ReadState + "'", "UPDATE");
        }
        tInputData.add(tmap);
        System.out.println(tmap);       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "批次号码为" + BatchNo + "屏蔽数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    
    public boolean ShieldData()
    {
        System.out.println("BL->屏蔽数据"); 
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
         if (SerialNo != null && !SerialNo.equals("") && (VoucherType == null || VoucherType.equals("")) && (DepCode == null || DepCode.equals("")) && ReadState.equals("4")) 
        {
        	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and SerialNo = '" + SerialNo + "' and ReadState is null", "UPDATE");
        }
         
         else if (DepCode != null && !DepCode.equals("") && (VoucherType == null || VoucherType.equals("")) && (SerialNo == null || SerialNo.equals("")) && ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and ReadState is null ", "UPDATE");
         }
         
         else if (VoucherType != null && !VoucherType.equals("") && (SerialNo == null || SerialNo.equals("")) && (DepCode == null || DepCode.equals("")) && ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and ReadState is null ", "UPDATE");
         }
         
         else if (SerialNo != null && !SerialNo.equals("") && DepCode != null && !DepCode.equals("") && (VoucherType == null || VoucherType.equals("")) && ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and SerialNo = '" + SerialNo + "' and ReadState is null ", "UPDATE");
         }
         
         else if (SerialNo != null && !SerialNo.equals("") && VoucherType != null && !VoucherType.equals("") && (DepCode == null || DepCode.equals("")) && ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and SerialNo = '" + SerialNo + "' and ReadState is null ", "UPDATE");
         }
         
         else if (VoucherType != null && !VoucherType.equals("") && DepCode != null && !DepCode.equals("") && (SerialNo == null || SerialNo.equals("")) && ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and ReadState is null ", "UPDATE");
         }
         
        else if (SerialNo != null && !SerialNo.equals("") && VoucherType != null && !VoucherType.equals("") && DepCode != null && !DepCode.equals("") && ReadState.equals("4")){
        	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and DepCode = '"+DepCode+"' and ChargeDate = '"+ChargeDate+"' and VoucherType = '" + VoucherType + "' and SerialNo = '" + SerialNo + "' and ReadState is null ", "UPDATE");
        }
         
        else if (ReadState.equals("4")){
         	tmap.put("update InterfaceTable set readstate = '3' where batchno = '"+BatchNo+"' and ChargeDate = '"+ChargeDate+"' and ReadState is null ", "UPDATE");
         }
         
        tInputData.add(tmap);
        System.out.println(tmap);       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "批次号码为" + BatchNo + "屏蔽数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        BatchNo = (String) tf.getValueByName("BatchNo");      
        DepCode = (String) tf.getValueByName("DepCode");
        VoucherType = (String) tf.getValueByName("VoucherType");
        ReadState = (String) tf.getValueByName("ReadState");
        ChargeDate = (String) tf.getValueByName("ChargeDate");
        SerialNo = (String) tf.getValueByName("SerialNo");

        if (BatchNo == null || ChargeDate == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "IntFaceTabDataBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}

