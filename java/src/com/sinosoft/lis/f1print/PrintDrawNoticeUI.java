package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 */
public class PrintDrawNoticeUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private String mOperate;
    public PrintDrawNoticeUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        PrintDrawNoticeBL tPrintDrawNoticeBL = new PrintDrawNoticeBL();

        System.out.println("---PrintDrawNoticeUI  BEGIN---");
        if (tPrintDrawNoticeBL.submitData(cInputData, mOperate) == false)
        {

            this.mErrors.copyAllErrors(tPrintDrawNoticeBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintDrawNoticeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        }
        else
        {
            mResult = tPrintDrawNoticeBL.getResult();
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        PrintDrawNoticeUI tPrintDrawNoticeUI = new PrintDrawNoticeUI();
        VData tVData = new VData();
        String GetNoticeNo = "";
        tVData.addElement(GetNoticeNo);
        tPrintDrawNoticeUI.submitData(tVData, "");
    }
}