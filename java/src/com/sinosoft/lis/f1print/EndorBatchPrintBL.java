package com.sinosoft.lis.f1print;

import java.io.*;
import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.schema.LPGrpEdorItemSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.bl.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import org.jdom.input.SAXBuilder;
import org.jdom.JDOMException;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EndorBatchPrintBL{
    private String mOperate="";
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorAppSchema mLPEdorAppSchema= new LPEdorAppSchema();
    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    public CErrors mErrors = new CErrors();
    private InputStream ins=null;
    private String ChlGrpflag="";

    public EndorBatchPrintBL() {
    }
    /**
        * 传输数据的公共方法
        * @param cInputData
        * @param cOperate
        * @return
        */
       public boolean submitData(VData cInputData, String cOperate) throws
            SQLException {
           if (!cOperate.equals("CONFIRM")
               && !cOperate.equals("REQUEST")
               && !cOperate.equals("PRINT")
               && !cOperate.equals("REQ"))
           {
               buildError("submitData", "不支持的操作字符串");
               return false;
           }

           mOperate = cOperate;

           // 得到外部传入的数据，将数据备份到本类中
           if (!getInputData(cInputData))
           {
               return false;
           }

           if (!dealData())
           {
               return false;
           }

           // 保存数据
           String tOperate = "";
           VData tVData = new VData();
           tVData.add(mLPEdorAppSchema);

           if (mOperate.equals("REQUEST"))
           {
               tOperate = "INSERT";
           }
           else if (mOperate.equals("CONFIRM"))
           {
               tOperate = "UPDATE";
           }
           else if (mOperate.equals("REQ"))
           {
               // 调用者通过getResult得到处理过的数据，在外部对得到的数据再进行处理
               mResult = tVData;
               return true;
           }
           else if (mOperate.equals("PRINT"))
           {
               // 直接返回
               return true;
           }


           return true;
       }

       public VData getResult()
       {
           return mResult;
       }
       public String setChlGrp(String strSaleChnl)
       {
           ChlGrpflag = strSaleChnl;
           return ChlGrpflag;
       }
       public static void main(String[] args) throws SQLException {
           GlobalInput tGlobalInput = new GlobalInput();

           tGlobalInput.ComCode = "86";
           tGlobalInput.ManageCom = "86";
           tGlobalInput.Operator = "001";

           //LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();

           //tLPEdorAppSchema.setEdorAcceptNo("20050527000001");
            LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
            tLPGrpEdorItemSchema.setEdorNo("20050810000004");
            tLPGrpEdorItemSchema.setEdorAcceptNo("20050810000004");
            tLPGrpEdorItemSchema.setEdorType("CT");
            tLPGrpEdorItemSchema.setGrpContNo("0000000104");

            VData vData = new VData();

           vData.add(tGlobalInput);
           vData.add(tLPGrpEdorItemSchema);
          // vData.add(tLPEdorAppSchema);
           EndorBatchPrintBL tEndorBatchPrintBL = new EndorBatchPrintBL();
           tEndorBatchPrintBL.setChlGrp("bq014");
           tEndorBatchPrintBL.submitData(vData, "PRINT");
           vData = tEndorBatchPrintBL.getResult();

           System.out.println(tEndorBatchPrintBL.mErrors.getFirstError());
           System.out.println("mResult:  "+vData);
       }

       /**
        * 根据前面的输入数据，进行BL逻辑处理
        * 如果在处理过程中出错，则返回false,否则返回true
        */
       private boolean dealData() throws SQLException {
           if (mOperate.equals("REQUEST") || mOperate.equals("REQ"))
           { // 打印申请
               // 处于未打印状态的通知书在打印队列中只能有一个
               // 条件：同一个单据类型，同一个其它号码，同一个其它号码类型
               LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
               tLPEdorAppDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
               tLPEdorAppDB.setOtherNo(mLPEdorAppSchema.getOtherNo());
               tLPEdorAppDB.setAccName(mLPEdorAppSchema.getAccName());
               tLPEdorAppDB.setEdorState(mLPEdorAppSchema.getEdorState());
               tLPEdorAppDB.setEdorAppDate(mLPEdorAppSchema.getEdorAppDate());

               LPEdorAppSet tLPEdorAppSet = tLPEdorAppDB.query();

               if (tLPEdorAppSet == null)
               {
                   buildError("dealData", "查询LPEdorApp表时出现错误");
                   return false;
               }

               if (tLPEdorAppSet.size() != 0)
               {
                   buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                   return false;
               }

               String strNoLimit = PubFun.getNoLimit(mGlobalInput.ComCode);

               mLPEdorAppSchema.setEdorAcceptNo(PubFun1.CreateMaxNo("PRTSEQNO",
                       strNoLimit));
               mLPEdorAppSchema.setManageCom(mGlobalInput.ComCode);
               //mLPEdorAppSchema.setReqOperator(mGlobalInput.Operator);
               //mLPEdorAppSchema.setStateFlag("0");
               mLPEdorAppSchema.setMakeDate(PubFun.getCurrentDate());
               mLPEdorAppSchema.setMakeTime(PubFun.getCurrentTime());

           }
           else if (mOperate.equals("CONFIRM"))
           { // 打印执行

              LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
               tLPEdorAppDB.setEdorAcceptNo(mLPEdorAppSchema.getEdorAcceptNo());
               if (!tLPEdorAppDB.getInfo())
               {
                   mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
                   return false;
               }

               // 查询打印队列的信息
               mLPEdorAppSchema = tLPEdorAppDB.getSchema();

             //  if (mLPEdorAppSchema.getStateFlag() == null)
             //  {
             //      buildError("dealData", "无效的打印状态");
             //      return false;
             //  }
             //  else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
             //  {
             //      buildError("dealData", "该打印请求不是在请求状态");
             //      return false;
             //  }

               // 调用打印服务
            try {
                if (!GetXMLDataStream(mLPEdorAppSchema)) {
                    return false;
                }
            } catch (JDOMException ex) {
            } catch (SQLException ex) {
            }

               // 打印后的处理
               //mLPEdorAppSchema.setStateFlag("1");
               mLPEdorAppSchema.setUWDate(PubFun.getCurrentDate());
               mLPEdorAppSchema.setUWTime(PubFun.getCurrentTime());

           }
           else if (mOperate.equals("PRINT"))
           { // 打印之后所执行的操作
               if (ChlGrpflag.equals("bq009"))
               {
                   LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
                   tLPEdorAppDB.setEdorAcceptNo(mLPEdorAppSchema.
                                                getEdorAcceptNo());
                   if (!tLPEdorAppDB.getInfo()) {
                       mErrors.copyAllErrors(tLPEdorAppDB.mErrors);
                       return false;
                   }

                   //查询打印队列的信息
                   mLPEdorAppSchema = tLPEdorAppDB.getSchema();

                   try {
                       if (!GetXMLDataStream(mLPEdorAppSchema)) {
                           return false;
                       }
                   } catch (JDOMException ex1) {
                   } catch (SQLException ex1) {
                   }

               } else if (ChlGrpflag.equals("bq014") )
           {
               LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
              tLPGrpEdorItemDB.setEdorNo(mLPGrpEdorItemSchema.getEdorNo());
              tLPGrpEdorItemDB.setEdorAcceptNo(mLPGrpEdorItemSchema.getEdorAcceptNo());
              tLPGrpEdorItemDB.setEdorType(mLPGrpEdorItemSchema.getEdorType() ) ;
              tLPGrpEdorItemDB.setGrpContNo(mLPGrpEdorItemSchema.getGrpContNo() ) ;
              if (!tLPGrpEdorItemDB.getInfo()) {
                  mErrors.copyAllErrors(tLPGrpEdorItemDB.mErrors);
                  return false;
              }

              //查询打印队列的信息
              mLPGrpEdorItemSchema = tLPGrpEdorItemDB.getSchema();

              try {
                  if (!GetXMLDataStream(mLPGrpEdorItemSchema)) {
                      return false;
                  }
              } catch (JDOMException ex1) {
              } catch (SQLException ex1) {
              }

          } else {
              buildError("dealData", "不支持的操作字符串");
              return false;
          }

           }

           return true;
       }

       /**
        * 从输入数据中得到所有对象
        * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData)
       {
           //全局变量
           mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                   "GlobalInput", 0));
           if (ChlGrpflag.equals("bq009"))
           {
               mLPEdorAppSchema.setSchema((LPEdorAppSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LPEdorAppSchema", 0));

               if (mGlobalInput == null || mLPEdorAppSchema == null) {
                   buildError("getInputData", "没有得到足够的信息！");
                   return false;
               }
           }
           if (ChlGrpflag.equals("bq014"))
           {
               mLPGrpEdorItemSchema.setSchema((LPGrpEdorItemSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LPGrpEdorItemSchema", 0));

               if (mGlobalInput == null || mLPGrpEdorItemSchema == null) {
                   buildError("getInputData", "没有得到足够的信息！");
                   return false;
               }
           }
           return true;
       }

       private void buildError(String szFunc, String szErrMsg)
       {
           CError cError = new CError();

           cError.moduleName = "PrintManagerBL";
           cError.functionName = szFunc;
           cError.errorMessage = szErrMsg;
           this.mErrors.addOneError(cError);
       }

       /**
        * 调用打印服务
        * @param aLOPRTManagerSchema
        * @return
        */
   private boolean GetXMLDataStream(LPGrpEdorItemSchema aLPGrpEdorItemSchema) throws
     SQLException, JDOMException {

    // 查找打印服务
    Connection conn = DBConnPool.getConnection();
    XmlExport xmlExport = new XmlExport();
    SAXBuilder saxBuilder = new SAXBuilder();


    Statement stmt = null;
    ResultSet rs = null;

    stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

       String strSQL = "SELECT * FROM lpedorprint WHERE ";
       strSQL += "EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() +
               "'";
       System.out.println("strSQL:  " + strSQL);
       rs = stmt.executeQuery(strSQL);

    COracleBlob tCOracleBlob = new COracleBlob();
    Blob tBlob = null;

    String tSQL = " and EdorNo = '" + aLPGrpEdorItemSchema.getEdorNo() + "'";
    tBlob = tCOracleBlob.SelectBlob("lpedorprint","edorinfo",tSQL,conn);
    ins=tBlob.getBinaryStream();

    org.jdom.Document document = saxBuilder.build(ins);
    xmlExport.setDocument(document);
    mResult.addElement(xmlExport);
    xmlExport.outputDocumentToFile("e:\\", "testZHJGrp");

    stmt.close();
    rs.close();
    conn.close();

    return true;
}

       private boolean GetXMLDataStream(LPEdorAppSchema aLPEdorAppSchema) throws
            SQLException, JDOMException {

           // 查找打印服务
           Connection conn = DBConnPool.getConnection();
           XmlExport xmlExport = new XmlExport();
           SAXBuilder saxBuilder = new SAXBuilder();


           Statement stmt = null;
           ResultSet rs = null;

           stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

              String strSQL = "SELECT * FROM LPEDORAPPPRINT WHERE ";
              strSQL += "EdorAcceptNo = '" + aLPEdorAppSchema.getEdorAcceptNo() +
                      "'";
              System.out.println("strSQL:  " + strSQL);
              rs = stmt.executeQuery(strSQL);

           COracleBlob tCOracleBlob = new COracleBlob();
           Blob tBlob = null;

           String tSQL = " and EdorAcceptNo = '" + aLPEdorAppSchema.getEdorAcceptNo() + "'";
           tBlob = tCOracleBlob.SelectBlob("LPEDORAPPPRINT","edorinfo",tSQL,conn);
           ins=tBlob.getBinaryStream();

           org.jdom.Document document = saxBuilder.build(ins);
           xmlExport.setDocument(document);
           mResult.addElement(xmlExport);
           xmlExport.outputDocumentToFile("e:\\", "testZHJ");

           stmt.close();
           rs.close();
           conn.close();

           return true;
    }
}
