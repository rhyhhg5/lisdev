package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLComCaseBL</p>
 * <p>Description: 机构团个险统计报表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLComCaseBL {
    public LLComCaseBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageComName = "";
    private String mContType = "";
    private String mCon = "";
    private String moperator = "";
    private long mStartDays = 0;
    private long mEndDays = 0;
    private String[][] mShowDataList = null;
    private XmlExport mXmlExport = null;
    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();
    private TransferData mTransferData = new TransferData();
	
	private String mFileNameB = "";
	
	private String mMakeDate = "";
	
	private String mOperator = "";
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }else{
			TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if(!tLLPrintSave.submitData(tVData,"")){
				return false;
			     }
		}

        return true;
    }


    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean getDataList() {
        double tPeoples = 0;
        double tCase = 0;
        double tPrem = 0;
        double tActuPay = 0;
        double tTemPrem = 0;
        double tCaseNum = 0;
        double tRealPay = 0;
        String tSQL = "";

        // 1、得到全部已开业的机构
        if(mManageCom.length() == 2){
        tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=4 AND comcode like '" +
               mManageCom + "%' ORDER BY comcode";
        //cbs00058484 修改，为避免重复，如果是用4位机构登陆查询，只需要带出该4位机构下的所有8位机构的数据即可。
        }else  if (mManageCom.length() == 4) {
        	tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=8 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
        System.out.println("YYYYYYYYYYYYYYY");
        }else{
        	 tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND comcode like '"
 				+ mManageCom + "%' ORDER BY comcode";
        }
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() <= 0) {
            buildError("queryData", "没有附和条件的机构信息！");
            return false;
        }

        // 2、查询需要表示的数据
        String tManageCom = "";
        String tComName = "";

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环机构进行统计
            String Info[] = new String[9];
            tManageCom = tSSRS.GetText(i, 1);
            tComName = tSSRS.GetText(i, 2);

            Info[0] = tManageCom;
            Info[1] = tComName;
            Info[2] = getPeoples(tManageCom);
            Info[3] = getCase(tManageCom);
            Info[4] = getPrem(tManageCom);
            Info[5] = getActuPay(tManageCom);
            Info[6] = getTemPrem(tManageCom);
            Info[7] = getCaseNum(tManageCom);
            Info[8] = getRealPay(tManageCom);

            tPeoples += Double.parseDouble(Info[2]);
            tCase += Double.parseDouble(Info[3]);
            tPrem += Double.parseDouble(Info[4]);
            tActuPay += Double.parseDouble(Info[5]);
            tTemPrem += Double.parseDouble(Info[6]);
            tCaseNum += Double.parseDouble(Info[7]);
            tRealPay += Double.parseDouble(Info[8]);

            mListTable.add(Info);
        }

        //合计所有公司所有险种不含特需
        String Info[] = new String[9];
        DecimalFormat tDF = new DecimalFormat("0.##");
        Info[0] = "合计";
        Info[1] = "总公司";
        Info[2] = tDF.format(tPeoples);
        Info[3] = tDF.format(tCase);
        Info[4] = tDF.format(tPrem);
        Info[5] = tDF.format(tActuPay);
        Info[6] = tDF.format(tTemPrem);
        Info[7] = tDF.format(tCaseNum);
        Info[8] = tDF.format(tRealPay);
        mListTable.add(Info);

        return true;
    }

    /**
     * 承保人数
     * @param cManageCom String
     * @return String
     */
    private String getPeoples(String cManageCom) {
        String tSql = null;
        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(peoples2),0) FROM lcgrpcont WHERE salechnl<>'04' AND appflag='1' AND "
                    + " managecom LIKE '" + cManageCom + "%' AND signdate>='"
                    + mStartDate + "'"
                    + " AND signdate<='" + mEndDate
                    + "' with ur";
        } else {
            tSql =
                    "SELECT COUNT(a.insuredno) FROM lcinsured a WHERE EXISTS(SELECT 1 FROM lccont WHERE salechnl<>'04' AND a.contno=contno"
                    + " AND conttype = '1' AND appflag='1' AND "
                    + " managecom LIKE '" + cManageCom + "%' AND signdate>='"
                    + mStartDate + "'"
                    + " AND signdate<='" + mEndDate
                    + "') with ur";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 承保件数
     * @param cManageCom String
     * @return String
     */
    private String getCase(String cManageCom) {
        String tSql = null;
        if (mContType.equals("G")) {
            tSql =
                    "SELECT COUNT(grpcontno) FROM lcgrpcont WHERE salechnl<>'04' AND appflag='1' AND "
                    + " managecom LIKE '" + cManageCom + "%' AND signdate>='"
                    + mStartDate + "'"
                    + " AND signdate<='" + mEndDate
                    + "' with ur";
        } else {
            tSql =
                    "SELECT COUNT(contno) FROM lccont WHERE salechnl<>'04' AND conttype = '1' AND appflag='1' AND "
                    + " managecom LIKE '" + cManageCom + "%' AND signdate>='"
                    + mStartDate + "'"
                    + " AND signdate<='" + mEndDate
                    + "' with ur";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 承保保费
     * @param cManageCom String
     * @return String
     */
    private String getPrem(String cManageCom) {
        String tSql = null;
        double tSumPrem = 0.0;
        String tPrem = "";
        ExeSQL tExeSQL = new ExeSQL();
        if (mContType.equals("G")) {
            tSql =
                    "SELECT sum(a.prem) FROM lcgrppol a,lcgrpcont b WHERE b.salechnl<>'04' AND a.grpcontno=b.grpcontno AND a.prem IS NOT NULL AND a.appflag='1' "
                    + " AND a.managecom LIKE '" + cManageCom +
                    "%' AND b.signdate>='"
                    + mStartDate + "'"
                    + " AND b.signdate<='" + mEndDate
                    + "' with ur";
        } else {
            tSql =
                    "SELECT sum(prem) FROM lcpol WHERE salechnl<>'04' AND conttype = '1' AND appflag='1'"
                    + " AND managecom LIKE '" + cManageCom +
                    "%' AND signdate>='"
                    + mStartDate + "'"
                    + " AND signdate<='" + mEndDate
                    + "' with ur";
        }

        tPrem = tExeSQL.getOneValue(tSql);
        if (tPrem.equals("null") || tPrem == null || tPrem.equals("")) {
            tSumPrem += 0;
        } else {
            tSumPrem += Double.parseDouble(tPrem);
        }

//        if (mContType.equals("G")) {
//            tSql =
//                    "SELECT sum(a.prem) FROM lbgrppol a,lbgrpcont b WHERE b.salechnl<>'04' AND a.grpcontno=b.grpcontno AND a.prem IS NOT NULL AND a.appflag='1' "
//                    + "AND a.managecom LIKE '" + cManageCom +
//                    "%' AND b.signdate>='"
//                    + mStartDate + "'"
//                    + " AND b.signdate<='" + mEndDate
//                    + "' with ur";
//        } else {
//            tSql =
//                    "SELECT sum(prem) FROM lbpol WHERE salechnl<>'04' AND conttype = '1' AND appflag='1'"
//                    + " AND managecom LIKE '" + cManageCom +
//                    "%' AND signdate>='"
//                    + mStartDate + "'"
//                    + " AND signdate<='" + mEndDate
//                    + "' with ur";
//        }
//
//        tPrem = tExeSQL.getOneValue(tSql);
//        if (tPrem.equals("null") || tPrem==null||tPrem.equals("")) {
//            tSumPrem += 0;
//        } else {
//            tSumPrem += Double.parseDouble(tPrem);
//        }
//
//        if (mContType.equals("G")) {
//            tSql =
//                    "SELECT sum(a.prem) FROM lbgrppol a,lcgrpcont b WHERE b.salechnl<>'04' AND a.grpcontno=b.grpcontno AND a.prem IS NOT NULL AND a.appflag='1'"
//                    + " AND a.managecom LIKE '" + cManageCom +
//                    "%' AND b.signdate>='"
//                    + mStartDate + "'"
//                    + " AND b.signdate<='" + mEndDate
//                    + "' with ur";
//
//            tPrem = tExeSQL.getOneValue(tSql);
//            if (tPrem.equals("null") || tPrem==null||tPrem.equals("")) {
//                tSumPrem += 0;
//            } else {
//                tSumPrem += Double.parseDouble(tPrem);
//            }
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        return tRturn;

    }

    /**
     * 实收保费
     * @param cManageCom String
     * @return String
     */
    private String getActuPay(String cManageCom) {
        String tSql = null;
        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(a.SumActuPayMoney),0) FROM ljapaygrp a WHERE a.endorsementno IS NULL "
                    + " AND EXISTS(SELECT 1 FROM lcgrpcont WHERE salechnl<>'04' AND grpcontno=a.grpcontno AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        } else {
            tSql =
                    "SELECT coalesce(SUM(a.SumActuPayMoney),0) FROM ljapayperson a,ljapay b WHERE a.payno=b.payno AND b.incometype NOT IN('3','10') "
                    + " AND EXISTS(SELECT 1 FROM lccont WHERE salechnl<>'04' AND contno=a.contno AND conttype='1' AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(a.getmoney),0) FROM ljagetendorse a WHERE "
                    +
                    "EXISTS(SELECT 1 FROM lcgrpcont WHERE salechnl<>'04' AND grpcontno=a.grpcontno AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        } else {
            tSql =
                    "SELECT coalesce(SUM(a.getmoney),0) FROM ljagetendorse a WHERE "
                    + " EXISTS(SELECT 1 FROM lccont WHERE salechnl<>'04' AND contno=a.contno AND conttype='1' AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);

        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }

        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(a.SumActuPayMoney),0) FROM ljapaygrp a WHERE a.endorsementno IS NULL "
                    + " AND EXISTS(SELECT 1 FROM lbgrpcont WHERE salechnl<>'04' AND grpcontno=a.grpcontno AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        } else {
            tSql =
                    "SELECT coalesce(SUM(a.SumActuPayMoney),0) FROM ljapayperson a,ljapay b WHERE a.payno=b.payno AND b.incometype NOT IN('3','10') "
                    + " AND EXISTS(SELECT 1 FROM lbcont WHERE salechnl<>'04' AND contno=a.contno AND conttype='1' AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);

        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }

        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(a.getmoney),0) FROM ljagetendorse a WHERE "
                    +
                    "EXISTS(SELECT 1 FROM lbgrpcont WHERE salechnl<>'04' AND grpcontno=a.grpcontno AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        } else {
            tSql =
                    "SELECT coalesce(SUM(a.getmoney),0) FROM ljagetendorse a WHERE "
                    + " EXISTS(SELECT 1 FROM lbcont WHERE salechnl<>'04' AND contno=a.contno AND conttype='1' AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom + "%' AND signdate>='" + mStartDate +
                    "' AND signdate<='" +
                    mEndDate + "') AND a.makedate<='" + mEndDate + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);

        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 经过保费（团险）
     * @param cManageCom String
     * @return String
     */
    private String getTemPremGroup(String cManageCom) {
        System.out.println("开始时间团险1。。。。" + PubFun.getCurrentTime());
        String tSql = null;
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPrem = 0.0;
        SSRS tSSRS = new SSRS();
//sum内不能有子查询，列函数（比如days（））所以下面的sql不能用sum，只能查询后外用sum
//注意t为虚拟表的名字，虚拟表需要给定名字外面才能用！
        tSql = "select sum(prem) from (SELECT  "
               + " case when days(a.cvalidate)>" + mStartDays
               + " then  "
               +
               " case when days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno)) < " +
               mEndDays
               + " then "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
               + " else  a.prem * 12 / a.payintv / 365* (days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
               + " end  "
               + " else "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (" + mEndDays +
               " - days(a.cvalidate) + 1)"
               + " else  a.prem * 12 / a.payintv / 365* (" + mEndDays +
               " - days(a.cvalidate) + 1)"
               + " end "
               + " end "
               + " else "
               +
               " case when days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno))<" +
               mEndDays
               + " then "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno)) - " +
               mStartDays + " + 1)"
               + " else  a.prem * 12 / a.payintv / 365* (days((SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno)) - " +
               mStartDays + " + 1)"
               + " end "
               + " else "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (" + mEndDays + " - " + mStartDays +
               " + 1)"
               + " else  a.prem * 12 / a.payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem "
               + " FROM lcgrppol a"
               +
                " WHERE a.salechnl<>'04' AND a.appflag='1' and a.managecom LIKE '"
               + cManageCom
               + "%' "
               + " and a.cvalidate <='"
               + mEndDate
               + "'"
               +
               " and (SELECT MAX(enddate) FROM lcpol WHERE a.grppolno=grppolno) >='"
               + mStartDate + "' ) t";

        tSSRS = tExeSQL.execSQL(tSql);
        //在tExeSQL.execSQL(tSql)执行后的结果，如果没有数据则置为"null"字符串，注意是被置为"null"字符串以示标志
        //所以在计算时要进行判断
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }
        System.out.println(tSumPrem);
        System.out.println("开始时间团险2。。。。" + PubFun.getCurrentTime());

        tSql = "select sum(prem) from (SELECT  "
               + " case when days(a.cvalidate)>" + mStartDays
               + " then  "
               +
               " case when days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno))<" +
               mEndDays
               + " then "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
               + " else  a.prem * 12 /  a.payintv / 365* (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
               + " end "
               + " else "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (" + mEndDays +
               " - days(a.cvalidate) + 1)"
               + " else  a.prem * 12 / a.payintv / 365* (" + mEndDays +
               " - days(a.cvalidate) + 1)"
               + " end "
               + " end "
               + " else "
               +
               " case when days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno))<" +
               mEndDays
               + " then "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - " +
               mStartDays + " + 1)"
               + " else  a.prem * 12 /  a.payintv / 365* (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - " +
               mStartDays + " + 1)"
               + " end"
               + " else "
               + " case when a.payintv<=0"
               + " then  a.prem / 365 * (" + mEndDays + " - " + mStartDays +
               " + 1)"
               + " else  a.prem * 12 /  a.payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem "

               + " FROM lbgrppol a"
               +
                " WHERE a.salechnl<>'04' AND a.appflag='1' and a.managecom LIKE '"
               + cManageCom
               + "%' "
               + " and a.cvalidate <='"
               + mEndDate
               + "'"
               +
               " and (SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno) >='"
               + mStartDate + "' ) t";

        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        System.out.println("结束时间团险。。。。" + PubFun.getCurrentTime());
        return tRturn;
    }

    /**
     * 经过保费(个险)
     * @param cManageCom String
     * @return String
     */

    private String getTemPremPerson(String cManageCom) {
        System.out.println("开始时间个险。。。。" + PubFun.getCurrentTime());
        String tSql = null;
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPrem = 0.0;
        SSRS tSSRS = new SSRS();

        tSql = "select sum(prem) from (SELECT  "
               + " case when days(cvalidate)>" + mStartDays
               + " then  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - days(cvalidate) + 1)"
               +
               " else  prem * 12 /  payintv / 365* (days(enddate) - days(cvalidate) + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - days(cvalidate) + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays +
               " - days(cvalidate) + 1)"
               + " end "
               + " end "
               + " else "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               +
               " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - " +
               mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (days(enddate) - " +
               mStartDays + " + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - " + mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem "

               + " FROM lcpol"
               +
                " WHERE salechnl<>'04' AND conttype='1' AND appflag='1' and managecom LIKE '"
               + cManageCom
               + "%' "
               + " and cvalidate <='"
               + mEndDate
               + "'"
               + " and enddate >='" + mStartDate + "' ) t";

        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }

        tSql = "select sum(prem) from (SELECT  "
               + " case when days(cvalidate)>" + mStartDays
               + " then  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - days(cvalidate) + 1)"
               +
               " else  prem * 12 /  payintv / 365* (days(enddate) - days(cvalidate) + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - days(cvalidate) + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays +
               " - days(cvalidate) + 1)"
               + " end "
               + " end "
               + " else  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               +
               " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - " +
               mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (days(enddate) - " +
               mStartDays + " + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - " + mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem"

               + " FROM lbpol"
               +
                " WHERE salechnl<>'04' AND conttype='1' AND appflag='1' and managecom LIKE '"
               + cManageCom
               + "%'"
               + " and cvalidate <='"
               + mEndDate
               + "'"
               + " and enddate >='" + mStartDate + "' ) t";

        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        System.out.println("结束时间个险。。。。" + PubFun.getCurrentTime());
        return tRturn;
    }

    /**
     * 经过保费
     * @param cManageCom String
     * @return String
     */
    //针对个险或团险调用相应方法来执行
    private String getTemPrem(String cManageCom) {
        if (mContType.equals("G")) {
            return this.getTemPremGroup(cManageCom);

        } else {
            return this.getTemPremPerson(cManageCom);
        }
    }

    /**
     * 理赔件数
     * @param cManageCom String
     * @return String
     */
    private String getCaseNum(String cManageCom) {
        String tSql = null;
        if (mContType.equals("G")) {
            tSql =
                    "SELECT COUNT(DISTINCT(a.caseno)) FROM llclaimdetail a WHERE "
                    +
                    " a.salechnl<>'04' AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='G')"
                    +
                    " AND EXISTS(SELECT 1 FROM llcase WHERE rgttype='1' AND caseno=a.caseno AND rgtstate in ('11','12') AND endcasedate>='"
                    + mStartDate + "' AND endcasedate<='"
                    + mEndDate + "' AND mngcom LIKE '" + cManageCom +
                    "%') with ur";
        } else {
            tSql =
                    "SELECT COUNT(DISTINCT(a.caseno)) FROM llclaimdetail a WHERE "
                    +
                    " a.salechnl<>'04' AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='I')"
                    +
                    " AND EXISTS(SELECT 1 FROM llcase WHERE rgttype='1' AND caseno=a.caseno AND rgtstate in ('11','12') AND endcasedate>='"
                    + mStartDate + "' AND endcasedate<='"
                    + mEndDate + "' AND mngcom LIKE '" + cManageCom +
                    "%') with ur";

        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 赔款支出
     * @param cManageCom String
     * @return String
     */
    private String getRealPay(String cManageCom) {
        String tSql = null;
        if (mContType.equals("G")) {
            tSql =
                    "SELECT coalesce(SUM(a.realpay),0) FROM llclaimdetail a WHERE "
                    +
                    " a.salechnl<>'04' AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='G')"
                    +
                    " AND EXISTS(SELECT 1 FROM llcase WHERE rgttype='1' AND caseno=a.caseno and rgtstate in('11','12') AND endcasedate>='"
                    + mStartDate + "' AND endcasedate<='"
                    + mEndDate + "' AND mngcom LIKE '" + cManageCom +
                    "%') with ur";
        } else {
            tSql =
                    "SELECT coalesce(SUM(a.realpay),0) FROM llclaimdetail a WHERE "
                    +
                    " a.salechnl<>'04' AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='I')"
                    +
                    " AND EXISTS(SELECT 1 FROM llcase WHERE rgttype='1' AND caseno=a.caseno and rgtstate in('11','12') AND endcasedate>='"
                    + mStartDate + "' AND endcasedate<='"
                    + mEndDate + "' AND mngcom LIKE '" + cManageCom +
                    "%') with ur";
        }

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

//    private String getSumPeoples() {
//        String tSql = null;
//        if (mContType.equals("G")) {
//            tSql =
//                    "SELECT coalesce(SUM(peoples2),0) FROM lcgrpcont WHERE appflag='1' AND "
//                    + " managecom LIKE '" + mManageCom +
//                    "%' AND signdate>='"
//                    + mStartDate + "'"
//                    + " AND signdate<='" + mEndDate
//                    + "' with ur";
//        } else {
//            tSql =
//                    "SELECT COUNT(a.insuredno) FROM lcinsured a WHERE EXISTS(SELECT 1 FROM lccont WHERE a.contno=contno"
//                    + " AND conttype = '1' AND appflag='1' AND "
//                    + " managecom LIKE '" + mManageCom + "%' AND signdate>='"
//                    + mStartDate + "'"
//                    + " AND signdate<='" + mEndDate
//                    + "') with ur";
//        }
//        ExeSQL tExeSQL = new ExeSQL();
//        String tCount1 = tExeSQL.getOneValue(tSql);
//
//        double tValue = 0;
//        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
//            tValue = 0;
//        } else {
//            tValue = Double.parseDouble(tCount1);
//        }
//        DecimalFormat tDF = new DecimalFormat("0.##");
//        String tRturn = tDF.format(tValue);
//        return tRturn;
//    }

//    private String getSumCase() {
//        String tSql = null;
//        if (mContType.equals("G")) {
//            tSql =
//                    "SELECT COUNT(grpcontno) FROM lcgrpcont WHERE appflag='1' AND "
//                    + " managecom LIKE '" + mManageCom +
//                    "%' AND signdate>='"
//                    + mStartDate + "'"
//                    + " AND signdate<='" + mEndDate
//                    + "' with ur";
//        } else {
//            tSql =
//                    "SELECT COUNT(contno) FROM lccont WHERE conttype = '1' AND appflag='1' AND "
//                    + " managecom LIKE '" + mManageCom +
//                    "%' AND signdate>='"
//                    + mStartDate + "'"
//                    + " AND signdate<='" + mEndDate
//                    + "' with ur";
//        }
//        ExeSQL tExeSQL = new ExeSQL();
//        String tCount1 = tExeSQL.getOneValue(tSql);
//
//        double tValue = 0;
//        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
//            tValue = 0;
//        } else {
//            tValue = Double.parseDouble(tCount1);
//        }
//        DecimalFormat tDF = new DecimalFormat("0.##");
//        String tRturn = tDF.format(tValue);
//        return tRturn;
//    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLComCaseReport.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("Con", mCon);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        mMakeDate = currentDate;
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", ""};

        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        moperator = (String) pmInputData.get(3);
        mManageComName = (String) pmInputData.get(4);
        mContType = (String) pmInputData.get(5);
        mCon = (String) pmInputData.get(6);
        mOperator = moperator;
        String tSql = "SELECT days('" + mStartDate + "'),days('" + mEndDate +
                      "') FROM dual";
        ExeSQL tExeSQLDate = new ExeSQL();
        SSRS tSSRSDate = new SSRS();
        tSSRSDate = tExeSQLDate.execSQL(tSql);
        mStartDays = Integer.parseInt(tSSRSDate.GetText(1, 1));
        mEndDays = Integer.parseInt(tSSRSDate.GetText(1, 2));
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
				"TransferData", 0);
		// 页面传入的数据 三个
		mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLComCaseBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
