/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 * 475要修改
 * 508要修改
 */
public class UWF1PBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    //    private String mPolNo = "";
    //String InsuredName="";
    //取得存放xml地址
    private String xmlUrl = "";

    //输入的查询sql语句
    private String mSql = "";

    //取得的延期承保原因
    //    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    // private String AddFeeInfoReason="";
    private String ReasonInfo1 = "您无需补交保险费！";

    private String lys_Flag = "0";

    private String lys_Flag_main = "0";

    //    private String lys_Flag_ab = "0";

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private static int sumCount = 10;

    public UWF1PBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));

        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        //根据印刷号查询打印队列中的纪录
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        String PrtNo = mLOPRTManagerSchema.getPrtSeq();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        int WH = 0;
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        boolean ChangePolFlag = false; //打印保险计划变更标记
        boolean payClientFee = false; //因保险计划变更，给客户退费标记
        boolean getClientFee = false; //因保险计划变更，客户需要补交费用标记
        double ChangeFee = 0; //因保险计划变更，客户需要补交费用或退费
        double SpecAddFeeSum = 0; //和保险计划变更一起的客户的加费
        String ChangeResult = "";
        String ChangePolReason = ""; //保险计划变更的原因

        boolean QuestionFlag = false; //打印问题件部分的判断标志
        boolean AddFeeFlag = false; //打印加费部分的判断标志
        boolean SpecFlag = false; //打印特别约定部分的判断标志
        boolean SpecFlag1 = false; //打印特别约定部分的判断标志
        double SumPrem = 0; //合计保费
        double oldSumPrem = 0; //合计加费

        LCContDB tLCContDB = new LCContDB();
        String strSQL = "SELECT * FROM LCCont WHERE ProposalContNo = '"
                + mLOPRTManagerSchema.getOtherNo() + "'";
        LCContSet tempLCContSet = tLCContDB.executeQuery(strSQL);

        if (tempLCContSet.size() == 0)
        {
            mErrors.copyAllErrors(tLCContDB.mErrors);
            buildError("outputXML", "在LCCont表中找不到相关信息");
            return false;
        }

        int m, i;
        tLCContDB.setSchema(tempLCContSet.get(1));

        //投保人地址和邮编
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContDB.getContNo());
        if (tLCAppntDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAppntDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if (tLCAddressDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAddressDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        this.mLCAddressSchema = tLCAddressDB.getSchema();
        mAgentCode = tLCContDB.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        String MainRiskName = new String();
        String Sql = new String();
        String tSql = new String();
        ListTable tRiskListTable = new ListTable();
        String[] RiskInfoTitle = new String[10];

        LCPolDB tLCPolDB = new LCPolDB();
        //        tLCPolDB.setContNo(tLCContDB.getContNo());
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* from lcpol a,lcinsured b where a.contno ='");
        sql.append(tLCContDB.getContNo());
        sql
                .append("' and a.contno=b.contno and a.insuredno=b.insuredno order by b.SequenceNo");
        LCPolSet tempLCPolSet = tLCPolDB.executeQuery(sql.toString());
        System.out.println("polno=" + tempLCPolSet.get(1).getPolNo());

        tRiskListTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名
        RiskInfoTitle[0] = "RiskName"; //险种名称
        RiskInfoTitle[1] = "Amnt"; //保险金额
        RiskInfoTitle[2] = "PayYears"; //缴费年期
        RiskInfoTitle[3] = "PayIntv"; //缴费方式（间隔）
        RiskInfoTitle[4] = "Prem"; //保费
        RiskInfoTitle[5] = "AddPrem"; //加费
        RiskInfoTitle[6] = "InsuredName"; //加费
        RiskInfoTitle[7] = "RiskCode"; //加费
        RiskInfoTitle[8] = "InsuYears"; //加费
        RiskInfoTitle[9] = "SeqNo"; //加费

        for (int nIndex = 0; nIndex < tempLCPolSet.size(); nIndex++)
        {
            //查询主险保单

            mLCPolSchema = tempLCPolSet.get(nIndex + 1).getSchema(); //保存险种投保单信息
            System.out.println("mLCPolSchema.getPolNo()="
                    + mLCPolSchema.getPolNo());

            //1-险种信息：
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
                return false;
            }
            MainRiskName = tLMRiskDB.getRiskName();
            System.out.println("MainRiskName=" + MainRiskName);
            SSRS tempSSRS = new SSRS();
            ExeSQL tempExeSQL = new ExeSQL();
            String sTemp = "";
            Double fTemp = new Double(mLCPolSchema.getAmnt());

            String[] RiskInfo = new String[10];
            RiskInfo[0] = MainRiskName; //险种名称
            RiskInfo[9] = nIndex + 1 + "";
            int iTemp = 0;
            RiskInfo[1] = mLCPolSchema.getMult() > 0 ? getNumberToCharacter(mLCPolSchema
                    .getMult())
                    + "档"
                    : fTemp.toString().substring(0,
                            fTemp.toString().lastIndexOf(".")); //保险金额
            if ((mLCPolSchema.getPayEndYear() == 1000 || mLCPolSchema
                    .getPayEndYear() == 106)
                    && mLCPolSchema.getPayEndYearFlag().toUpperCase().equals(
                            "A"))
            {
                RiskInfo[2] = "终生"; //交费年期
            }
            else
            {
                RiskInfo[2] = (new Integer(mLCPolSchema.getPayYears()))
                        .toString(); //交费年期
            }

            if (mLCPolSchema.getPayIntv() == -1)
            {
                sTemp = "不定期交费";
            }
            if (mLCPolSchema.getPayIntv() == 0)
            {
                sTemp = "趸交";
            }
            if (mLCPolSchema.getPayIntv() == 1)
            {
                sTemp = "月交";
            }
            if (mLCPolSchema.getPayIntv() == 3)
            {
                sTemp = "季交";
            }
            if (mLCPolSchema.getPayIntv() == 6)
            {
                sTemp = "半年交";
            }
            if (mLCPolSchema.getPayIntv() == 12)
            {
                sTemp = "年交";
            }
            RiskInfo[3] = sTemp; //交费方式
            RiskInfo[4] = new Double(mLCPolSchema.getStandPrem()).toString(); //保费

            // 取主险投保单加费信息
            tSql = "select sum(Prem) from LCPrem where PolNo='"
                    + mLCPolSchema.getPolNo()
                    + "' and PayPlanCode like '000000%'";
            System.out.println("Sql:" + tSql);
            tempSSRS = tempExeSQL.execSQL(tSql);
            if (tempSSRS.MaxCol > 0)
            {
                System.out.println("tempSSRS.GetText(1,1)="
                        + tempSSRS.GetText(1, 1));
                if (!(tempSSRS.GetText(1, 1).equals("0")
                        || tempSSRS.GetText(1, 1).trim().equals("") || tempSSRS
                        .GetText(1, 1).equals("null")))
                {
                    RiskInfo[5] = tempSSRS.GetText(1, 1); //累计加费
                    SpecAddFeeSum = SpecAddFeeSum
                            + Double.parseDouble(tempSSRS.GetText(1, 1)); //加费合计
                }
                else
                {
                    RiskInfo[5] = "0";
                }
            }
            RiskInfo[6] = mLCPolSchema.getInsuredName();
            RiskInfo[7] = mLCPolSchema.getRiskCode();
            RiskInfo[8] = mLCPolSchema.getInsuYear() + "";
            if (mLCPolSchema.getInsuYearFlag().equals("Y"))
            {
                RiskInfo[8] += "年";
            }
            else if (mLCPolSchema.getInsuYearFlag().equals("M"))
            {
                RiskInfo[8] += "月";
            }
            else if (mLCPolSchema.getInsuYearFlag().equals("D"))
            {
                RiskInfo[8] += "日";
            }
            else if (mLCPolSchema.getInsuYearFlag().equals("A"))
            {
                RiskInfo[8] = "至" + RiskInfo[8] + "岁";
            }

            if ((mLCPolSchema.getInsuYear() == 1000 || mLCPolSchema
                    .getInsuYear() == 106)
                    && mLCPolSchema.getInsuYearFlag().toUpperCase().equals("A"))
            {
                RiskInfo[8] = "终身";
            }
            tRiskListTable.add(RiskInfo); //加入主险信息
            SumPrem = SumPrem + mLCPolSchema.getStandPrem(); // 原保费合计

        }
        SpecAddFeeSum = Double
                .parseDouble(mDecimalFormat.format(SpecAddFeeSum)); //转换计算后的保费(规定的精度)
        SumPrem = Double.parseDouble(mDecimalFormat.format(SumPrem)); //转换计算后的保费(规定的精度)

        int SN = 0; //页面排序序号
        String[] QuestionTitle = new String[6];
        QuestionTitle[0] = "ID";
        QuestionTitle[1] = "RelationPeoples";
        QuestionTitle[2] = "ErrFieldName";
        QuestionTitle[3] = "ErrContent";
        QuestionTitle[4] = "Content";
        QuestionTitle[5] = " ";
        ListTable tQuestionListTable = new ListTable();
        //addition:如果有保险计划变更，就不显示问题件

        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(tLCContDB.getContNo());
        if (!tLCCUWMasterDB.getInfo())
        {
            mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            buildError("outputXML", "在取得LCUWMaster的数据时发生错误");
            //return false;
        }

        if (tLCCUWMasterDB.getChangePolFlag() != null)
        { //保险计划变更标记
            if (tLCCUWMasterDB.getChangePolFlag().equals("1")
                    || tLCCUWMasterDB.getChangePolFlag().equals("2"))
            {
                //查询暂交费纪录,判断是否补交或退费
                SN = 1;
                ChangePolFlag = true;
                QuestionFlag = false; //问题件不显示
                double SumTempfee = 0; //暂交费累积
                ChangePolReason = tLCCUWMasterDB.getChangePolReason(); //变更原因

            }
        }
        if (ChangePolFlag == false)
        {
            //2-问题信息
            String strQue[] = null;
            tQuestionListTable.setName("QUESTION"); //对应模版问题部分的行对象名
            String q_sql = "";
            q_sql = "select QuestionObj,ErrFieldName,ErrContent,IssueCont from LCIssuePol where BackObjType = '3'  and NeedPrint = 'Y' and prtseq='"
                    + mLOPRTManagerSchema.getPrtSeq()
                    + "' and contno  = '"
                    + mLOPRTManagerSchema.getOtherNo() + "'";
            System.out.println("问题件中所执行的sql是" + q_sql);
            ExeSQL q_exesql = new ExeSQL();
            SSRS q_ssrs = new SSRS();
            q_ssrs = q_exesql.execSQL(q_sql);
            if ((q_ssrs.GetText(1, 4).equals("0")
                    || q_ssrs.GetText(1, 4).trim().equals("") || q_ssrs
                    .GetText(1, 4).equals("null")))
            {
                System.out.println("没有问题件记录");
                QuestionFlag = false;
            }
            else
            {
                System.out.println("问题中有" + q_ssrs.getMaxRow() + "条记录！！！");
                QuestionFlag = true;
                WH = q_ssrs.getMaxRow();
                for (i = 1; i <= q_ssrs.getMaxRow(); i++)
                {
                    strQue = new String[6];
                    strQue[0] = (new Integer(i)).toString(); //序号
                    strQue[1] = q_ssrs.GetText(i, 1); //相关人员
                    strQue[2] = q_ssrs.GetText(i, 2); //需确认内容
                    strQue[3] = q_ssrs.GetText(i, 3); //原填写内容
                    strQue[4] = q_ssrs.GetText(i, 4); //确认原因
                    strQue[5] = " ";
                    tQuestionListTable.add(strQue);
                }
            }
            SN = q_ssrs.getMaxRow(); //页面排序序号
        }
        //3-加费信息
        //3-1取出加费原
        //上面补充的保险计划变更已经查询
        String AddFeeReason = tLCCUWMasterDB.getAddPremReason(); //得到加费原因,后用

        //3-2取出加费部分的数据
        System.out.println("对于主险的信息只有一条记录，而对应附加险的信息可以有多条记录，所以在附加险时要进行循环处理！！！");
        double SumAddFee = 0;
        double SumAddFeeInfo = 0; //lys最后的合计金额
        ListTable tAddFeeListTable = new ListTable();
        ListTable tAddFeeInfoListTable = new ListTable(); //加费明细信息
        String[] AddFee = new String[4];
        String AddFeeTitle[] = new String[4];

        String[] AddFeeInfo = new String[2]; //lys显示加费明细信息内容
        String AddFeeInfoTitle[] = new String[2]; //lys显示加费明细的信息内容

        for (int mIndex = 0; mIndex < tempLCPolSet.size(); mIndex++)
        {
            mLCPolSchema = tempLCPolSet.get(mIndex + 1).getSchema();
            // if(ChangePolFlag==false) //标明不是特殊类型的核保通知书,则显示加费
            {
                AddFeeTitle[0] = "RiskName"; //险种名称
                AddFeeTitle[1] = "Amnt"; //保险金额
                AddFeeTitle[2] = "OriPrem"; //原保险费
                AddFeeTitle[3] = "AddFee"; //加费

                AddFeeInfoTitle[0] = "RiskName"; //lys加费明细的险种名称
                AddFeeInfoTitle[1] = "Sum_bujiao"; //lys加费明细信息的补交金额

                tAddFeeListTable.setName("FEE"); //对应模版加费部分的行对象名
                tAddFeeInfoListTable.setName("FEEINFO"); //lys对应模板的加费明细的行对象名称

                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                //3-2-1 取主险投保单加费信息
                System.out.println("2003-04-25  hzm的主险的信息！！！");
                Sql = "select sum(StandPrem) from LCPrem where PolNo='"
                        + mLCPolSchema.getPolNo()
                        + "' and PayPlanCode like '000000%'";
                System.out.println("Sql:" + Sql);
                tSSRS = tExeSQL.execSQL(Sql);
                if (tSSRS.MaxCol > 0)
                {
                    if (!(tSSRS.GetText(1, 1).equals("0")
                            || tSSRS.GetText(1, 1).trim().equals("") || tSSRS
                            .GetText(1, 1).equals("null")))
                    {
                        AddFeeFlag = true; //加费标记为真
                        AddFee = new String[4];
                        AddFee[0] = MainRiskName; //险种名称
                        AddFee[1] = (new Double(mLCPolSchema.getAmnt()))
                                .toString(); //保额
                        AddFee[2] = (new Double(mLCPolSchema.getStandPrem()))
                                .toString(); //保费
                        AddFee[3] = tSSRS.GetText(1, 1); //累计加费
                        tAddFeeListTable.add(AddFee);
                        SumAddFee = SumAddFee
                                + Double.parseDouble(tSSRS.GetText(1, 1)); //加费合计
                        oldSumPrem = oldSumPrem + mLCPolSchema.getStandPrem(); //原保险费

                        //lys 添加的查询出该主险的暂收费的金额
                        System.out.println("2003-04-25 lys 暂收费的信息！！！");
                        System.out.println("lys您好，现在开始执行您的程序！！！");
                        String t_sql = "select sum(paymoney) from LJTempFee where  ( OtherNo = '"
                                + mLCPolSchema.getPolNo()
                                + "' or OtherNo = '"
                                + mLCPolSchema.getPrtNo()
                                + "') and RiskCode = '"
                                + mLCPolSchema.getRiskCode() + "'";
                        System.out.println("lys－您输入的t_sql语句的执行结果是" + t_sql);
                        SSRS t_ssrs = new SSRS();
                        t_ssrs = tExeSQL.execSQL(t_sql);
                        double temp_fee = 0;
                        System.out.print("t_ssrs.getMaxRow()="
                                + t_ssrs.getMaxRow());
                        if (t_ssrs.GetText(1, 1).equals("0")
                                || t_ssrs.GetText(1, 1).trim().equals("")
                                || t_ssrs.GetText(1, 1).equals("null"))
                        {
                            temp_fee = 0;
                        }
                        else
                        {
                            temp_fee = Double.parseDouble(t_ssrs.GetText(1, 1));
                            System.out.println("×××××××paymoney" + temp_fee);
                        }
                        double temp_fee_sum = Double.parseDouble(AddFee[2])
                                + Double.parseDouble(AddFee[3]) - temp_fee;
                        System.out.println("暂交费的值是" + temp_fee_sum);
                        if (temp_fee_sum > 0)
                        {
                            ReasonInfo1 = "您需要补交的保费金额如下";
                            lys_Flag = "1";
                            System.out.println("开始执行暂交费》0时的情况");
                            System.out.println("原保险费是" + AddFee[2]);
                            System.out.println("加费是" + AddFee[3]);
                            System.out.println("实际有保户交的费用是" + temp_fee_sum);

                            AddFeeInfo = new String[4]; //lys对加费的明细进行附值；
                            AddFeeInfo[0] = MainRiskName; //lys对加费的主险进行附值
                            AddFeeInfo[1] = (new Double(temp_fee_sum))
                                    .toString();

                            System.out.println("分保费是" + temp_fee_sum);
                            SumAddFeeInfo = SumAddFeeInfo + temp_fee_sum;
                            System.out.println("主险的合计是" + SumAddFeeInfo);
                            System.out.println("执行到开始判断合计的保费");
                            System.out.println("￥￥￥￥合计的保费是" + SumAddFeeInfo);
                            tAddFeeInfoListTable.add(AddFeeInfo);
                        }
                        if (temp_fee_sum <= 0)
                        {
                            lys_Flag_main = "1";
                            System.out.println("主险中没有要打印的数据");
                        }
                    }
                    System.out.println("2003-04-17--02-31");
                }
                //3-2-2 取附险投保单信息(利用前面查询得到的附险投保单集合)
                for (int n = 1; n <= mLCPolSet.size(); n++)
                {
                    System.out.println("2003-04-25 hzm 附加险的信息！！！");
                    LCPolSchema tLCPolSchema = mLCPolSet.get(n);
                    Sql = "select sum(StandPrem) from LCPrem where PolNo='"
                            + tLCPolSchema.getPolNo()
                            + "' and PayPlanCode like '000000%'";
                    tSSRS = tExeSQL.execSQL(Sql);
                    if (tSSRS.MaxCol > 0)
                    {
                        if (!(tSSRS.GetText(1, 1).equals("0") || tSSRS.GetText(
                                1, 1).trim().equals("")))
                        {
                            LMRiskDB tLMRiskDB = new LMRiskDB();
                            tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
                            if (!tLMRiskDB.getInfo())
                            {
                                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                                buildError("outputXML", "在取得LMRisk的数据时发生错误");
                                return false;
                            }
                            AddFeeFlag = true; //加费标记为真
                            AddFee = new String[4];
                            AddFee[0] = tLMRiskDB.getRiskName(); //险种名称
                            AddFee[1] = (new Double(tLCPolSchema.getAmnt()))
                                    .toString(); //保额
                            AddFee[2] = (new Double(tLCPolSchema.getStandPrem()))
                                    .toString(); //保费
                            AddFee[3] = tSSRS.GetText(1, 1); //累计加费
                            tAddFeeListTable.add(AddFee);

                            System.out.println("2003-04-25 lys 附加险的信息！！！");
                            String t_sql_f = "select sum(paymoney) from LJTempFee where ( OtherNo = '"
                                    + tLCPolSchema.getPolNo()
                                    + "' or OtherNo = '"
                                    + tLCPolSchema.getPrtNo()
                                    + "') and RiskCode = '"
                                    + tLCPolSchema.getRiskCode() + "'";

                            SSRS t_ssrs_f = new SSRS();
                            t_ssrs_f = tExeSQL.execSQL(t_sql_f);
                            double temp_fee_f = 0;
                            if (t_ssrs_f.getMaxRow() == 0)
                            {
                                System.out.println("lys附件险的暂交费的记录为空！！");
                                temp_fee_f = 0;
                            }
                            else
                            {
                                System.out.println("lys附件险的暂交费的金额是"
                                        + t_ssrs_f.GetText(1, 1));
                                temp_fee_f = Double.parseDouble(t_ssrs_f
                                        .GetText(1, 1));
                            }
                            double temp_fee_f_sum = 0;
                            temp_fee_f_sum = Double.parseDouble(AddFee[2])
                                    + Double.parseDouble(AddFee[3])
                                    - temp_fee_f;
                            System.out.println("lys与客户之间发生的现金交易额是"
                                    + temp_fee_f_sum);

                            if (temp_fee_f_sum > 0)
                            {

                                AddFeeInfo = new String[2]; //lys
                                AddFeeInfo[0] = tLMRiskDB.getRiskName(); //lys
                                AddFeeInfo[1] = (new Double(temp_fee_f_sum))
                                        .toString();
                                System.out
                                        .println("实际要保户交的费用是" + AddFeeInfo[1]);

                                tAddFeeInfoListTable.add(AddFeeInfo);
                                SumAddFee = SumAddFee
                                        + Double.parseDouble(tSSRS
                                                .GetText(1, 1)); //加费合计
                                SumAddFeeInfo = SumAddFeeInfo + temp_fee_f_sum;
                                oldSumPrem = oldSumPrem
                                        + tLCPolSchema.getStandPrem(); //原保险费
                                lys_Flag = "1";
                                lys_Flag_main = "0";
                                ReasonInfo1 = "您要补交的保险费用如下:";
                                System.out.println("2003-04-25-2");
                            }
                            if (temp_fee_f_sum <= 0)
                            {
                                lys_Flag = "0";
                                //                                lys_Flag_ab = "1";
                                System.out.println("附件险中没有要补交的数据");
                                lys_Flag_main = "您无需补交保险费！！！";
                                if (lys_Flag_main.equals("1"))
                                {
                                    System.out.println("您无需补交保险费");
                                    ReasonInfo1 = "您无需补交保险费！";
                                }
                            }
                        }
                    }
                }
            }
            SumAddFee = Double.parseDouble(mDecimalFormat.format(SumAddFee)); //转换计算后的保费(规定的精度)
            oldSumPrem = Double.parseDouble(mDecimalFormat.format(oldSumPrem)); //转换计算后的保费(规定的精度)
            SumAddFeeInfo = Double.parseDouble(mDecimalFormat
                    .format(SumAddFeeInfo));
            if (AddFeeFlag)
            {
                SN = SN + 1; //序号加1
            }
        }
        //其它模版上单独不成块的信息
        /** 动态行数，为了将显示等分 */
        ListTable tCountTable = new ListTable();
        String[] tCount = new String[1];
        tCountTable.setName("Blank");
        tCount[0] = "Blank";
        int count = sumCount - tempLCPolSet.size() - (WH * 2);
        if (count > 0)
        {
            for (i = 1; i <= count; i++)
            {
                String[] blank = { "" };
                tCountTable.add(blank);
            }
        }

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        if (ChangePolFlag)
        { //如果是保险计划变更
            xmlexport.createDocument("SpecUWNotice.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else
        {
            xmlexport.createDocument("UWNotice.vts", "printer"); //最好紧接着就初始化xml文档
        }
        if (tCountTable.size() > 0)
        {
            xmlexport.addListTable(tCountTable, tCount);
        }
        String[] SpecTitle = new String[3];
        SpecTitle[0] = "RiskName";
        SpecTitle[1] = "SpecInfo";
        SpecTitle[1] = "SpecReason";
        ListTable tSpecListTable = new ListTable();
        String strSpec[] = null;
        tSpecListTable.setName("SPECINFO"); //对应模版特别约定部分的行对象名

        //        boolean bSpecReason = false;

        for (int mIndex = 0; mIndex < tempLCPolSet.size(); mIndex++)
        {
            // 只取出不为空的特约内容
            String t_sql = "";

            t_sql = "SELECT SpecContent FROM LCSpec WHERE PolNo ='"
                    + tempLCPolSet.get(mIndex + 1).getPolNo() + "'"
                    + " AND TRIM(SpecContent) IS NOT NULL"
                    + " ORDER BY ModifyDate, ModifyTime DESC";
            SSRS yssrs = new SSRS();
            ExeSQL yExeSQL = new ExeSQL();
            yssrs = yExeSQL.execSQL(t_sql);

            if (yssrs.getMaxRow() == 0)
            {
                //                SpecFlag = false;
            }
            else
            {
                if (!((yssrs.GetText(1, 1).equals("")) || yssrs.GetText(1, 1) == null))
                {
                    SpecFlag1 = true;
                    //                    SpecFlag = true;
                    strSpec = new String[3];
                    LMRiskDB tLMRiskDB = new LMRiskDB();
                    tLMRiskDB.setRiskCode(tempLCPolSet.get(mIndex + 1)
                            .getRiskCode());
                    if (!tLMRiskDB.getInfo())
                    {
                        mErrors.copyAllErrors(tLMRiskDB.mErrors);
                        buildError("outputXML", "在取得LMRisk的数据时发生错误");
                        return false;
                    }
                    LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                    tLCUWMasterDB.setPolNo(tempLCPolSet.get(mIndex + 1)
                            .getPolNo());
                    LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
                    tLCUWMasterSet = tLCUWMasterDB.query();

                    strSpec[0] = tLMRiskDB.getRiskName();
                    strSpec[1] = "　　" + yssrs.GetText(1, 1);
                    strSpec[2] = tLCUWMasterSet.get(1).getSpecReason();
                    System.out.println(tLCUWMasterSet.get(1).getSpecReason());

                    tSpecListTable.add(strSpec);

                }

            }

            // 取特约原因
            //其它模版上单独不成块的信息
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
                .executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"
                        + tLCContDB.getAgentCode() + "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            buildError("getprintData", "没有查到该报单的代理人组别");
        }

        //模版自上而下的元素
        //texttag.add("LCCont.PolNo",tLCContDB.getPolNo());         //投保单号
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            return false;
        }
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getServicePhone());

        texttag.add("RiskName", MainRiskName); //主险名称
        texttag.add("PremSum", SumPrem); //合计保费
        if (ChangePolFlag == false)
        { //如果不是保险计划变更类型的核保
            texttag.add("AddFeeReason", AddFeeReason); //加费原因
            texttag.add("OldSumPrem", oldSumPrem);
            if (AddFeeFlag == true)
            {
                texttag.add("ReasonInfo", ReasonInfo1); //合计加费
            }
            texttag.add("FeeInfoSum", SumAddFeeInfo);
            texttag.add("AddFeeSum", SpecAddFeeSum); //加费
        }
        else
        {
            //保险计划变更后添加的元素
            texttag.add("ChangeResult", ChangeResult); //变更后加费或退费
            texttag.add("ChangePolReason", ChangePolReason); //变更原因
            texttag.add("SpecAddFeeSum", SpecAddFeeSum); //加费
            texttag.add("FeeInfoSum", SumAddFeeInfo);
        }

        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("Post", mLCAddressSchema.getZipCode()); //投保人邮政编码
        System.out.println("Post : " + mLCAddressSchema.getZipCode());
        texttag.add("Address", mLCAddressSchema.getPostalAddress()); //投保人地址
        System.out.println("Post : " + mLCAddressSchema.getPostalAddress());
        texttag.add("XI_AppntNo", tLCContDB.getAppntNo());
        texttag.add("AppntName", tLCContDB.getAppntName());
        if (tLCContDB.getPolApplyDate() != null)
        {
            texttag.add("ApplyDate", tLCContDB.getPolApplyDate().split("-")[0]
                    + "年" + tLCContDB.getPolApplyDate().split("-")[1] + "月"
                    + tLCContDB.getPolApplyDate().split("-")[2] + "日"); //保单申请日期

        }

        System.out.println(tLCContDB.getPolApplyDate());

        texttag.add("LCCont.AppntName", tLCContDB.getAppntName()); //投保人名称
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("AgentCode", mLAAgentSchema.getGroupAgentCode()); //代理人业务号
        //add by zhangxing 增加业务员组别
        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getBranchAttr());
        texttag.add("GlobalServicePhone", tLDComDB.getServicePhone());

        if (!StrTool.cTrim(mLAAgentSchema.getMobile()).equals(""))
        {
            texttag.add("AgentPhone", mLAAgentSchema.getMobile());
        }
        else if (!StrTool.cTrim(mLAAgentSchema.getPhone()).equals(""))
        {
            texttag.add("AgentPhone", mLAAgentSchema.getPhone());
        }
        else
        {
            texttag.add("AgentPhone", "          　");
        }
        texttag.add("LCCont.ManageCom", getComName(tLCContDB.getManageCom())); //营业机构
        texttag.add("Cavidate", tLCContDB.getCValiDate().split("-")[0] + "年"
                + tLCContDB.getCValiDate().split("-")[1] + "月"
                + tLCContDB.getCValiDate().split("-")[2] + "日"); //营业机构
        texttag.add("PrtNo", PrtNo);

        //流水号
        texttag.add("LCCont.PrtNo", tLCContDB.getPrtNo()); //印刷号
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                + tSrtTool.getDay() + "日";
        texttag.add("SysDate", SysDate);

        // 核保师代码 核保<--核保师代码
        texttag.add("LCCont.UWCode", tLCContDB.getUWOperator());
        // add by guoxiang at 2003-12-15:扫描，录入，复核，核保<--核保师代码
        //texttag.add("scanner", getScaner(tLCContDB.getPrtNo()));//扫描
        texttag.add("LCCont.Operator", tLCContDB.getOperator()); //录入
        texttag.add("LCCont.ApproveCode", tLCContDB.getApproveCode()); //复核
        texttag.add("Today", mLOPRTManagerSchema.getMakeDate().split("-")[0]
                + "年" + mLOPRTManagerSchema.getMakeDate().split("-")[1] + "月"
                + mLOPRTManagerSchema.getMakeDate().split("-")[2] + "日"); //复核
        texttag.add("ReBackDate", PubFun.calDate(
                mLOPRTManagerSchema.getMakeDate(), 5, "D", null).split("-")[0]
                + "年"
                + PubFun.calDate(mLOPRTManagerSchema.getMakeDate(), 5, "D",
                        null).split("-")[1]
                + "月"
                + PubFun.calDate(mLOPRTManagerSchema.getMakeDate(), 5, "D",
                        null).split("-")[2] + "日"); //复核 ;
        texttag.add("Title", tLCContDB.getAppntSex().equals("0") ? "先生" : "女士");

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + tLCContDB.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tRiskListTable, RiskInfoTitle); //保存险种信息及其标题栏

        if (ChangePolFlag == true)
        {
            xmlexport.addDisplayControl("displayChangePol"); //保险计划模版上的保险计划的部分的控标记
            if (AddFeeFlag == true)
            {
                xmlexport.addDisplayControl("displayfee"); //模版上的加费部分的控制标记
                xmlexport.addListTable(tAddFeeListTable, AddFeeTitle); //保存加费信息
                if (lys_Flag.equals("1"))
                {
                    xmlexport.addDisplayControl("displayfeeinfo");
                    xmlexport.addListTable(tAddFeeInfoListTable,
                            AddFeeInfoTitle);
                }
            }
        }
        else
        { //保存加费信息
            if (AddFeeFlag == true)
            {
                xmlexport.addDisplayControl("displayfee"); //模版上的加费部分的控制标记
                xmlexport.addListTable(tAddFeeListTable, AddFeeTitle); //保存加费信息
                if (lys_Flag.equals("1"))
                {
                    xmlexport.addDisplayControl("displayfeeinfo");
                    xmlexport.addListTable(tAddFeeInfoListTable,
                            AddFeeInfoTitle);
                }
            }
        }
        //保存问题信息
        if (QuestionFlag == true)
        {
            xmlexport.addDisplayControl("displayquestion"); //模版上的问题部分的控制标记
            xmlexport.addListTable(tQuestionListTable, QuestionTitle); //保存问题信息
        }

        //保存特别约定
        if (SpecFlag1 == true)
        {
            xmlexport.addDisplayControl("displayspec"); //模版上的特别约定部分的控制标记
            xmlexport.addListTable(tSpecListTable, SpecTitle); //保存特别约定信息
        }

        // 保存特约原因
        /*
         if( SpecFlag1 == true) {
         xmlexport.addDisplayControl("displayspecreason");  // 显示特约原因
         ListTable ltSpecReason = new ListTable();
         int nMaxCharsInOneLine = 50;  // The max number of chars that one line can contain
         int nSpecReasonLen = 0;
         String[] strArr = null;

         ltSpecReason.setName("SPECREASON");
         strSpecReason = "　　" + strSpecReason + "故本保单作如下特别约定：";
         nSpecReasonLen = strSpecReason.length();
         while( nSpecReasonLen > nMaxCharsInOneLine ) {
         strArr = new String[1];
         strArr[0] = strSpecReason.substring(0, nMaxCharsInOneLine);

         strSpecReason = strSpecReason.substring(nMaxCharsInOneLine);
         nSpecReasonLen -= nMaxCharsInOneLine;

         ltSpecReason.add(strArr);
         }

         if( nSpecReasonLen > 0 ) {
         strArr = new String[1];
         strArr[0] = strSpecReason;

         ltSpecReason.add(strArr);
         }

         strArr = new String[1];
         strArr[0] = "REASON";

         xmlexport.addListTable(ltSpecReason, strArr);
         }
         */
        //保存保险计划调整
        //保存其它信息
        xmlexport.outputDocumentToFile("d:\\", "uwresult"); //输出xml文档到文件

        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode String
     * @return String
     */
    private String getComName(String strComCode)
    {
        mSql = "select CodeName from LDcode where Code = '" + strComCode
                + "' and CodeType = 'station'";
        ExeSQL tExeSQL = new ExeSQL();
        return tExeSQL.getOneValue(mSql);
        //        LDCodeDB tLDCodeDB = new LDCodeDB();
        //        tLDCodeDB.setCode(strComCode);
        //        tLDCodeDB.setCodeType("station");
        //        if (!tLDCodeDB.getInfo())
        //        {
        //            mErrors.copyAllErrors(tLDCodeDB.mErrors);
        //            return "";
        //        }
        //        return tLDCodeDB.getCodeName();
    }

    private String getNumberToCharacter(double Num)
    {
        String cNum = String.valueOf((int) Num);
        if (cNum.equals("1"))
        {
            return "一";
        }
        if (cNum.equals("2"))
        {
            return "二";
        }
        if (cNum.equals("3"))
        {
            return "三";
        }
        if (cNum.equals("4"))
        {
            return "四";
        }
        if (cNum.equals("5"))
        {
            return "五";
        }
        if (cNum.equals("6"))
        {
            return "六";
        }
        if (cNum.equals("7"))
        {
            return "七";
        }
        if (cNum.equals("8"))
        {
            return "八";
        }
        return null;
    }
}
