package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LAClientDailyReportBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDay = "";
    private String mEndDay = "";
    private String mDailyType = "";
    private String mManageCom = "";
    private String mTitleName = "";
    private String[][] mShowDataList = null;

  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }

  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      return true;
  }

  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      // 根据统计类型作不同处理
      if(this.mDailyType.equals("01") || this.mDailyType.equals("02"))
      {// 根据管理机构进行数据统计
          mTitleName = "管理机构名称";
          if(!getDataListForManageCom())
          {
              return false;
          }
      }else if(this.mDailyType.equals("03"))
      {// 根据销售机构进行统计
          mTitleName = "销售机构名称";
          if(!getDataListForBranch())
          {
              return false;
          }
      }else if(this.mDailyType.equals("04"))
      {// 根据个人进行统计
          mTitleName = "销售人员姓名";
          if(!getDataListForAgent())
          {
              return false;
          }
      }

      return true;
  }

  /**
   * 按销售人员进行统计
   * @return boolean
   */
  private boolean getDataListForAgent()
  {
      String tSQL = "";

      // 1、检索全部已开业的销售机构下的在职人员
      tSQL  = "select a.* from laagent a,labranchgroup b";
      tSQL += " where b.branchtype = '2' and a.agentgroup = b.agentgroup";
      tSQL += "   and b.branchtype2 = '01' and b.managecom like '" + mManageCom + "%'";
      tSQL += "   and b.branchlevel = '21' and b.endflag = 'N'";
      tSQL += "   and b.managecom in (select comcode from ldcom";
      tSQL += "                      where Sign='1' and comcode not like '8600%')";
      tSQL += " order by agentcode";

      LAAgentDB tLAAgentDB = new LAAgentDB();
      LAAgentSet tLAAgentSet = new LAAgentSet();
      tLAAgentSet = tLAAgentDB.executeQuery(tSQL);
      if(tLAAgentSet.size()==0)
      {
          buildError("queryData", "没有符合条件的销售机构！");
          return false;
      }

      // 2、以销售人员为单位进行查询
      String[][] tShowDataList = new String[tLAAgentSet.size()][4];
      for(int i=1;i<=tLAAgentSet.size();i++)
      {// 进行循环统计
          LAAgentSchema tLAAgentSchema = new LAAgentSchema();
          tLAAgentSchema = tLAAgentSet.get(i);
          tShowDataList[i-1][0] = tLAAgentSchema.getName();

          // 查询需要表示的数据
          if(!queryOneDataList_3(tLAAgentSchema.getAgentCode(),tShowDataList[i-1]))
          {
              System.out.println("[" + tLAAgentSchema.getAgentCode() +
                                 "] 人员数据查询失败！");
              return false;
          }
          mShowDataList = tShowDataList;
      }

      return true;
  }

  /**
   * 按销售机构进行统计
   * @return boolean
   */
  private boolean getDataListForBranch()
  {
      String tSQL = "";

      // 1、检索全部已开业的销售机构
      tSQL  = "select * from labranchgroup where branchtype = '2'";
      tSQL += "   and branchtype2 = '01' and managecom like '" + mManageCom + "%'";
      tSQL += "   and branchlevel = '21' and endflag = 'N'";
      tSQL += "   and managecom in (select comcode from ldcom";
      tSQL += "                      where Sign='1' and comcode not like '8600%')";
      tSQL += " order by branchattr";
System.out.println(tSQL);
      LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
      LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
      tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
      if(tLABranchGroupSet.size()==0)
      {
          buildError("queryData", "没有符合条件的销售机构！");
          return false;
      }

      // 2、以销售机构为单位进行查询
      String[][] tShowDataList = new String[tLABranchGroupSet.size()][4];
      for(int i=1;i<=tLABranchGroupSet.size();i++)
      {// 进行循环统计
          LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
          tLABranchGroupSchema = tLABranchGroupSet.get(i);
          tShowDataList[i-1][0] = tLABranchGroupSchema.getName();

          // 查询需要表示的数据
          if(!queryOneDataList_2(tLABranchGroupSchema.getAgentGroup(),tShowDataList[i-1]))
          {
              System.out.println("[" + tLABranchGroupSchema.getAgentGroup() +
                                 "] 销售机构数据查询失败！");
              return false;
          }
      }
      mShowDataList = tShowDataList;

      return true;
  }

  /**
   * 按管理机构进行统计
   * @return boolean
   */
  private boolean getDataListForManageCom()
  {
      String tSQL = "";
      String tManageComLen = "";
      String tManageComName = "";

      if(this.mDailyType.equals("01"))
      {// 按分公司查询
          tManageComLen = "4";
          tManageComName = "分公司";
      }else if(this.mDailyType.equals("02"))
      {// 按支公司查询
          tManageComLen = "8";
          tManageComName = "支公司";
      }

      // 1、得到全部已开业的机构
      tSQL  = "select * from ldcom where Sign='1'";
      tSQL += "   and length(trim(comcode))="+tManageComLen;
      tSQL += "   and comcode <> '86000000'";
      tSQL += "   and comcode like '" + this.mGlobalInput.ManageCom + "%'";
      tSQL += " order by comcode";
      System.out.println(tSQL);
      LDComDB tLDComDB = new LDComDB();
      LDComSet tLDComSet = new LDComDBSet();
      tLDComSet = tLDComDB.executeQuery(tSQL);
      if(tLDComSet.size()==0)
      {
          buildError("queryData", "没有符合条件的机构！");
          return false;
      }

      // 2、查询需要表示的数据
      String[][] tShowDataList = new String[tLDComSet.size()][4];
      for(int i=1;i<=tLDComSet.size();i++)
      {// 循环机构进行统计
          LDComSchema tLDComSchema = new LDComSchema();
          tLDComSchema = tLDComSet.get(i);
          tShowDataList[i-1][0] = tLDComSchema.getName() + tManageComName;
          System.out.println(tLDComSchema.getComCode()+"/"+tShowDataList[i-1][0]);

          // 查询需要表示的数据
          if(!queryOneDataList_1(tLDComSchema.getComCode(),tShowDataList[i-1]))
          {
              System.out.println("["+tLDComSchema.getComCode()+"] 本机构数据查询失败！");
              return false;
          }
      }
      mShowDataList = tShowDataList;

      return true;
  }

  /**
   * 取得客户数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getAppCountForManagecom(String pmStartDate, String pmEndDate,
                                         String pmManageCom) {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "select count(appntno) from";
      tSQL += " (select distinct appntno from lcgrpcont a,LDGrp b";
      tSQL += "   where a.appntno = b.customerno";
      tSQL += "     and a.managecom like '" + pmManageCom + "%'";
      if(pmEndDate != null)
      {
          tSQL += "     and a.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and a.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and a.appflag = '1'";
      tSQL += "     and a.salechnl='02'";
      tSQL += " UNION";
      tSQL += "  select distinct appntno from lbgrpcont aa,LDGrp bb";
      tSQL += "   where aa.appntno = bb.customerno";
      tSQL += "     and aa.managecom like '" + pmManageCom + "%'";
      if(pmEndDate != null)
      {
          tSQL += "     and aa.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and aa.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and aa.salechnl='02'";
      tSQL += "     and aa.appflag = '1') as apptable";

      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAppCountForManagecom 出错！");
      }

      return tRtValue;
  }

  /**
   * 按销售机构取得客户数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getAppCountForBranch(String pmStartDate, String pmEndDate,
                                         String pmAgentGroup) {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "select count(appntno) from";
      tSQL += " (select distinct appntno from lcgrpcont a,LDGrp b";
      tSQL += "   where a.appntno = b.customerno";
      tSQL += "     and a.agentgroup = '" + pmAgentGroup + "'";
      if(pmEndDate != null)
      {
          tSQL += "     and a.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and a.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and a.appflag = '1'";
      tSQL += "     and a.salechnl='02'";
      tSQL += "UNION";
      tSQL += "  select distinct appntno from lbgrpcont aa,LDGrp bb";
      tSQL += "   where aa.appntno = bb.customerno";
      tSQL += "     and aa.agentgroup = '" + pmAgentGroup + "'";
      if(pmEndDate != null)
      {
          tSQL += "     and aa.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and aa.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and aa.salechnl='02'";
      tSQL += "     and aa.appflag = '1') as apptable";


      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAppCountForBranch 出错！");
      }

      return tRtValue;
  }

  /**
   * 按销售机构取得客户数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getAppCountForAgent(String pmStartDate, String pmEndDate,
                                         String pmAgentCode) {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "select count(appntno) from";
      tSQL += " (select distinct appntno from lcgrpcont a,LDGrp b";
      tSQL += "   where a.appntno = b.customerno";
      tSQL += "     and a.agentcode = '" + pmAgentCode + "'";
      if(pmEndDate != null)
      {
          tSQL += "     and a.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and a.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and a.appflag = '1'";
      tSQL += "     and a.salechnl='02'";
      tSQL += "UNION";
      tSQL += "  select distinct appntno from lbgrpcont aa,LDGrp bb";
      tSQL += "   where aa.appntno = bb.customerno";
      tSQL += "     and aa.agentcode = '" + pmAgentCode + "'";
      if(pmEndDate != null)
      {
          tSQL += "     and aa.signdate <= '" + pmEndDate + "'";
      }
      if(pmStartDate != null)
      {
          tSQL += "     and aa.signdate >= '" + pmStartDate + "'";
      }
      tSQL += "     and aa.salechnl='02'";
      tSQL += "     and aa.appflag = '1') as apptable";

      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAppCountForBranch 出错！");
      }

      return tRtValue;
  }

  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList_1(String pmComCode,String[] pmOneDataList)
  {
      try{
          // 0、机构名称
          // 1、当日客户数
          pmOneDataList[1] = getAppCountForManagecom(mEndDay,mEndDay,pmComCode);
          // 2、年度累计客户数
          pmOneDataList[2] = getAppCountForManagecom(mStartDay,mEndDay,pmComCode);
          // 3、历史累计客户数
          pmOneDataList[3] = getAppCountForManagecom(null,mEndDay,pmComCode);
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList_2(String pmAgentGroup,String[] pmOneDataList)
  {
      try{
          // 0、机构名称
          // 1、当日客户数
          pmOneDataList[1] = getAppCountForBranch(mEndDay,mEndDay,pmAgentGroup);
          // 2、年度累计客户数
          pmOneDataList[2] = getAppCountForBranch(mStartDay,mEndDay,pmAgentGroup);
          // 3、历史累计客户数
          pmOneDataList[3] = getAppCountForBranch(null,mEndDay,pmAgentGroup);
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList_3(String pmAgentCode,
                                     String[] pmOneDataList)
  {
      try {
          // 0、机构名称
          // 1、当日客户数
          pmOneDataList[1] = getAppCountForAgent(mEndDay, mEndDay,
                  pmAgentCode);
          // 2、年度累计客户数
          pmOneDataList[2] = getAppCountForAgent(mStartDay, mEndDay,
                  pmAgentCode);
          // 3、历史累计客户数
          pmOneDataList[3] = getAppCountForAgent(null, mEndDay, pmAgentCode);
      } catch (Exception ex) {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }


  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      String tSQL = "";
      String CurrentDate = PubFun.getCurrentDate();//得到当天日期
      //String strArr[] = null;

      try{
          // 1、查询数据
          if(!getDataList())
          {
              return false;
          }

//          mShowDataList = new String[3][4];
//          mShowDataList[0][0] = "A";
//          mShowDataList[0][1] = "A";
//          mShowDataList[0][2] = "A";
//          mShowDataList[0][3] = "A";
//          mShowDataList[1][0] = "B";
//          mShowDataList[1][1] = "B";
//          mShowDataList[1][2] = "B";
//          mShowDataList[1][3] = "B";

          // 2、设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Order");

          // 加入数据行
          for(int i=0;i<mShowDataList.length;i++)
          {
              tlistTable.add(mShowDataList[i]);
          }

          TextTag texttag = new TextTag();    //新建一个TextTag的实例
          texttag.add("StartYear", this.mEndDay.substring(0,4));    //输入统计时间
          texttag.add("StartMonth", this.mEndDay.substring(5,7));   //输入统计时间
          texttag.add("StartDay", this.mEndDay.substring(8,10));    //输入统计时间
          texttag.add("TitleName", mTitleName);                     //输入统计时间
          texttag.add("CurrentYear", CurrentDate.substring(0,4));   //输入制表时间
          texttag.add("CurrentMonth", CurrentDate.substring(5,7));  //输入制表时间
          texttag.add("CurrentDay", CurrentDate.substring(8,10));   //输入制表时间

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAClientDailyReport.vts", "printer"); //最好紧接着就初始化xml文档
          if (texttag.size() > 0)
              xmlexport.addTextTag(texttag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          mResult.addElement(xmlexport);
      }catch (Exception ex)
      {
          buildError("queryData", "LAClientDailyReportBL发生错误，准备数据时出错！");
          return false;
      }

      return true;
  }

    /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
              "GlobalInput", 0));
      mEndDay = (String) pmInputData.get(0);
      mStartDay = mEndDay.substring(0,4) + "-01-01";
      mDailyType = (String) pmInputData.get(1);
      mManageCom = mGlobalInput.ManageCom;
      System.out.println(mStartDay+" / "+mEndDay+" / "+mManageCom+" / "+mDailyType);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAClientDailyReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }

  /**
   * 执行SQL文查询结果
   * @param sql String
   * @return double
   */
  private double execQuery(String sql)
  {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

      }
    }

  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
}
