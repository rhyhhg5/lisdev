/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAgentSalaryTotalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计展业类型
    private String mBranchType = "";
    //统计渠道
    private String mBranchType2 = "";
    //文件暂存路径
    private String mfilePathAndName;
    //薪资月
    private String mWageNo;
    //查询年
    private String mYear;
   //查询月
    private String mMonth;
    //分公司名称
    private String mName;;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAAgentSalaryTotalBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("RBL start submitData");
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
        	System.out.println("RBL error PRINT");
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChnlPremReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
        	  System.out.println("RBL error getDataList");
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LABL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mBranchType = (String)cInputData.get(1);
     mBranchType2 = (String)cInputData.get(2);
     mYear =(String)cInputData.get(3) ;
     mMonth = (String)cInputData.get(4);
     mWageNo = mYear + mMonth;
     mName = (String)cInputData.get(5);
     mfilePathAndName=(String)cInputData.get(7);
     return true;
 }




    private boolean getDataList()
  {
      //查询人员
    	System.out.println("报表打印的有问题？没走到这个类里？");
      String manageSQL = "select agentgrade cc,'2' aa,agentcode b,branchattr bb,'姓名','代码','销售单位代码','综合拓展佣金','首年度佣金','续期佣金','续保佣金','孤儿单服务奖金','绩优达标奖','健代产佣金','健代寿佣金','续期服务奖',	  " +
      		"  '岗位津贴','季度奖','增员奖','晋升奖','','','','','培训津贴（筹）','团建季度补发津贴','团建年度补发津贴','推荐津贴','','其他加款', " +
      		" '考勤扣款','养老金','个人所得税','个人增值税','个人城建税','教育费附加税','地方教育附加税','其他附加税','其他扣款','上次佣金余额','实际收入' from lawage a where a.agentgrade >= 'A01' and a.agentgrade <= 'A11' " +
      		"  and a.branchtype = '1' and a.branchtype2 = '01' and indexcalno = '"+mWageNo+"'  and managecom like '"+mManageCom+"%' " +
      		" union " +
      		" select cc, '3' aa, b, BB, A, B, BB,varchar(W) ,varchar(C), varchar(D), varchar(BD),varchar(QQ),varchar(QE),varchar(VO),varchar(VVO),varchar(G), varchar(H), varchar(J), varchar(X), varchar(Z), '', " +
      		"  '', '','', varchar(AD), varchar(DA), varchar(DB), varchar(DC), '', varchar(O), varchar(P), varchar(Q), varchar(R), varchar(S), varchar(HH),varchar(JJ),varchar(KK),varchar(LL), varchar(T), " +
      		" varchar(U), varchar(V) " +
      		"  FROM (select agentgrade cc,(select b.name from laagent b where b.agentcode = a.agentcode) A,a.agentcode B,a.branchattr BB,a.F04 C,a.f05 D,a.f01 BD,a.f16 QQ,a.f18 QE,a.k16 VO,a.k17 VVO, " +
      		" a.F09 G,a.F02 H,a.F12 J,a.F08 X,a.F10 Z,a.F22 AD,a.F13 DA,a.F14 DB,a.F15 DC,a.F30 O,a.F13 P,a.K15 Q,a.K02 R,K01 S, a.F26 HH,a.F27 JJ,a.F28 KK,a.F29 LL, K12 T,LastMoney U,SumMoney V,(k07+k08+k09) W" +
      		" from lawage a  where a.agentgrade >= 'A01'   and a.agentgrade <= 'A11'   and a.branchtype = '1'   and a.branchtype2 = '01'   and indexcalno = '"+mWageNo+"'   and managecom like '"+mManageCom+"%') as X " +
      		" union " +
      		" select agentgrade cc, '0' aa, agentcode b, branchattr bb, '姓名', '代码', '销售单位代码', '综合拓展佣金','首年度佣金', '续期佣金', '续保佣金','孤儿单服务奖金','绩优达标奖','健代产佣金','健代寿佣金','续期服务奖', '岗位津贴', " +
      		"  '季度奖','增员奖','晋升奖','职务津贴','管理津贴','直接养成津贴','间接养成津贴','培训津贴（筹）','团建季度补发津贴','团建年度补发津贴', " +
      		" '推荐津贴','财补支持计划（筹）','其他加款','考勤扣款','养老金','个人所得税','个人增值税','个人城建税','教育费附加税','地方教育附加税','其他附加税','其他扣款','上次佣金余额','实际收入' " +
      		" from lawage a where a.agentgrade >= 'B01'  and a.branchtype = '1'  and a.branchtype2 = '01'  and indexcalno = '"+mWageNo+"'  and managecom like '"+mManageCom+"%' " +
			" union " +
			" select cc, '1' aa, b, BB, A, B, BB, varchar(W), varchar(C), varchar(D), varchar(BD),varchar(QQ),varchar(QE),varchar(VO),varchar(VVO),varchar(G), varchar(H), varchar(J), varchar(X), varchar(Z), varchar(L), " +
			"  varchar(N), varchar(XA), varchar(XB), varchar(AD), varchar(DA), varchar(DB), varchar(DC), varchar(AE), varchar(O), varchar(P), varchar(Q), " +
			" varchar(R),varchar(S), varchar(HH),varchar(JJ),varchar(KK),varchar(LL),varchar(T),varchar(U),varchar(V)  FROM (select agentgrade cc,(select b.name from laagent b where b.agentcode = a.agentcode) A, " +
			" a.agentcode B,a.branchattr BB,a.F04 C,a.f05 D,a.f01 BD,a.f16 QQ,a.f18 QE,a.k16 VO,a.K17 VVO,a.F09 G,a.F02 H,a.F12 J,a.F08 X,a.F10 Z,a.F11 L,a.F06 N,a.F19 XA,a.F20 XB,a.F22 AD, " +
			" a.F13 DA,a.F14 DB,a.F15 DC,a.F23 AE,a.F30 O,a.F13 P,a.K15 Q,a.K02 R,K01 S, a.F26 HH,a.F27 JJ,a.F28 KK,a.F29 LL,K12 T,LastMoney U,SumMoney V,(k07+k08+k09) W from lawage a  where a.branchtype = '1' " +
			" and a.branchtype2 = '01' and a.agentgrade >= 'B01' and indexcalno = '"+mWageNo+"' and managecom like '"+mManageCom+"%') as X order by bb, cc desc, b, aa " ;
        System.out.println(manageSQL);
//        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
//        tCreateCSVFile.initFile(this.mfilePathAndName);
//        WriteToExcel t = new WriteToExcel("");
//        t.createExcelFile();
//        String[][] tFirstDataList = new String[1][1];
//        String[][] tTheardDataList = new String[1][1];
//        String[][] tFourDataList = new String[1][36];
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(manageSQL);
        int linenum=tSSRS.MaxRow;
        String[][] tFourDataList = new String[linenum+5][48];
        tFourDataList[0][0]="中国人民健康保险股份有限公司"+mName+"分公司"+mYear+"年"+mMonth+"月营销业务人员工资条";
        WriteToExcel t = new WriteToExcel("");
        t.createExcelFile();
        tFourDataList[1][0]="编制部门:个人销售部 ";
//        tFourDataList[2][0]="姓     名 ";
//        tFourDataList[2][1]="代     码";
//        tFourDataList[2][2]="销售单位代码";
//        tFourDataList[2][3]="首年度佣金";
//        tFourDataList[2][4]="续年度佣金";
//        tFourDataList[2][5]="续保佣金";
//        tFourDataList[2][6]="续期服务奖";
//        tFourDataList[2][7]="岗位津贴";
//        tFourDataList[2][8]="季度奖";
//        tFourDataList[2][9]="增员奖";
//        tFourDataList[2][10]="晋升奖";
//        tFourDataList[2][11]="";
//        tFourDataList[2][12]="";
//        tFourDataList[2][13]="";
//        tFourDataList[2][14]="";
//        tFourDataList[2][15]="";
//        tFourDataList[2][16]="'培训津贴（筹）'";
//        tFourDataList[2][17]="团建季度补发津贴";
//        tFourDataList[2][18]="团建年度补发津贴";
//        tFourDataList[2][19]="推荐津贴";
//        tFourDataList[2][20]="";
//        tFourDataList[2][21]="其他加款";
//        tFourDataList[2][22]="考勤扣款";
//        tFourDataList[2][23]="养老金";
//        tFourDataList[2][24]="个人所得税";
//        tFourDataList[2][25]="增值税";
//		tFourDataList[2][26]="其他扣款";
//        tFourDataList[2][27]="上次佣金余额";
//        tFourDataList[2][28]="实际收入";
        
		
        
//        tCreateCSVFile.doWrite(tFirstDataList,0);
//        tCreateCSVFile.doWrite(tTheardDataList,0);
//        tCreateCSVFile.doWrite(tFourDataList,35);
//        SSRS tSSRS = new SSRS();
//        ExeSQL tExeSQL = new ExeSQL();
//        tSSRS = tExeSQL.execSQL(manageSQL);
        try{
        	
               
//                String[][] tShowDataList = new String[tSSRS.MaxRow][36];
//                int linenum=tSSRS.MaxRow;
                for(int i=1;i<=linenum;i++)
                {
                   if (!queryOneDataList(tSSRS,i,tFourDataList[i+2])){
                	  
                	   return false;
                   }
                }
                tFourDataList[linenum+4][0]="总经理：                       复核 ：                       制表：";
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
//                tCreateCSVFile.doWrite(tShowDataList,35);
                String[] sheetName ={PubFun.getCurrentDate()};
                t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
                t.setData(0, tFourDataList);
                t.write(this.mfilePathAndName);//获得文件读取路径
        }catch(Exception exc){
        	exc.printStackTrace();
        	return false;
        }
//        String[][] tlast1DataList = new String[1][1];
//        tlast1DataList[2][0]="总经理：                       复核 ：                       制表：";
//        tCreateCSVFile.doWrite(tlast1DataList,0);
//        tCreateCSVFile.doClose();
        return true;
 }

 /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 5);
          pmOneDataList[1] =tSSRS.GetText(i, 6)+"";
          pmOneDataList[2]=tSSRS.GetText(i, 7)+"";
          pmOneDataList[3]=tSSRS.GetText(i, 8)+"";
          pmOneDataList[4] =tSSRS.GetText(i, 9)+"";
          pmOneDataList[5] = tSSRS.GetText(i, 10)+"";
          pmOneDataList[6] = tSSRS.GetText(i, 11)+"";
          pmOneDataList[7] = tSSRS.GetText(i, 12)+"";
          pmOneDataList[8] = tSSRS.GetText(i, 13)+"";
          pmOneDataList[9] =tSSRS.GetText(i, 14)+"";
          pmOneDataList[10] = tSSRS.GetText(i, 15)+"";
          pmOneDataList[11]  = tSSRS.GetText(i, 16)+"";
          pmOneDataList[12]  = tSSRS.GetText(i, 17)+"";
          pmOneDataList[13]  = tSSRS.GetText(i, 18)+"";
          pmOneDataList[14]  = tSSRS.GetText(i, 19)+"";
          pmOneDataList[15]  =tSSRS.GetText(i, 20)+"";
          pmOneDataList[16]  = tSSRS.GetText(i, 21)+"";
          pmOneDataList[17]  = tSSRS.GetText(i, 22)+"";
          pmOneDataList[18]  =tSSRS.GetText(i, 23)+"";
          pmOneDataList[19]  =tSSRS.GetText(i, 24)+"";
          pmOneDataList[20]  = tSSRS.GetText(i, 25)+"";
          pmOneDataList[21]  =tSSRS.GetText(i, 26)+"";
          pmOneDataList[22]  =tSSRS.GetText(i, 27)+"";
          pmOneDataList[23]  =tSSRS.GetText(i, 28)+"";
          pmOneDataList[24]  =tSSRS.GetText(i, 29)+"";
          pmOneDataList[25]  =tSSRS.GetText(i, 30)+"";
          pmOneDataList[26]  =tSSRS.GetText(i, 31)+"";
          pmOneDataList[27]  =tSSRS.GetText(i, 32)+"";
          pmOneDataList[28]  =tSSRS.GetText(i, 33)+"";
          
          pmOneDataList[29]  =tSSRS.GetText(i, 34)+"";
          pmOneDataList[30]  =tSSRS.GetText(i, 35)+"";
          pmOneDataList[31]  =tSSRS.GetText(i, 36)+"";
          pmOneDataList[32]  =tSSRS.GetText(i, 37)+"";
          pmOneDataList[33]  =tSSRS.GetText(i, 38)+"";
          pmOneDataList[34]  =tSSRS.GetText(i, 39)+"";
          pmOneDataList[35]  =tSSRS.GetText(i, 40)+"";
          pmOneDataList[36]  =tSSRS.GetText(i, 41)+"";
          

         
       
          
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
 
 
    public VData getResult()
    {
        return mResult;
    }
}
