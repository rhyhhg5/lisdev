/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAwageSaleToTalListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计展业类型
    private String mBranchType = "";
    //统计渠道
    private String mBranchType2 = "";
    //文件暂存路径
    private String mfilePathAndName;
    //薪资月
    private String mWageNo;
    //查询年
    private String mYear;
   //查询月
    private String mMonth;
    //分公司名称
    private String mName;;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAAwageSaleToTalListBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("RBL start submitData");
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
        	System.out.println("RBL error PRINT");
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAwageSaleToTalListBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAwageSaleToTalListBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAwageSaleToTalListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
        	  System.out.println("RBL error getDataList");
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LAAwageSaleToTalListBL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mBranchType = (String)cInputData.get(1);
     mBranchType2 = (String)cInputData.get(2);
     mYear =(String)cInputData.get(3) ;
     mMonth = (String)cInputData.get(4);
     mWageNo = mYear + mMonth;
     mName = (String)cInputData.get(5);
     mfilePathAndName=(String)cInputData.get(7);
     return true;
 }




    private boolean getDataList()
  {
      //查询人员
    	System.out.println("报表打印的有问题？没走到这个类里？");
      String manageSQL = "select C,WW,D,E,F,G,DD,PP,QQ,TT,WS,H,I,K,L,M,N,O,P,R,S,T,U,V,W,X,Y,HH,JJ,KK,LL,Z,AA,AB	  " +
      		" from (select  (select b.name from labranchgroup b  " +
      		" where b.agentgroup = a.agentgroup) C,value(sum(a.k07+a.k08+a.k09),0) WW, value(sum(a.f04), 0) D, value(sum(a.f05), 0) E,value(sum(a.F07), 0) F,value(sum(a.F03), 0) G,value(sum(a.f01),0) DD,value(sum(a.f16),0) PP,value(sum(a.f18),0) QQ,value(sum(a.k16),0)TT,value(sum(a.k17),0) WS, " +
      		"  value(sum(a.F09), 0) H, value(sum(a.F02), 0) I, " +
      		" value(sum(a.F12), 0) K, value(sum(a.F08), 0) L,value(sum(a.F14), 0) M, value(sum(a.F10), 0) N,value(sum(a.F15), 0) O,  " +
      		" value(sum(a.F11), 0) P,value(sum(a.F06), 0) R,value(sum(a.F19), 0) S,value(sum(a.F20), 0) T, " +
      		" value(sum(a.F30), 0) U,value(sum(a.K03), 0) V,value(sum(a.K15), 0) W,value(sum(a.K02), 0) X,value(sum(a.K01), 0) Y,value(sum(a.F26), 0) HH,value(sum(a.F27), 0) JJ,value(sum(a.F28), 0) KK,value(sum(a.F29), 0) LL, " +
      		" value(sum(a.K12), 0) Z, value(sum(a.LastMoney), 0) AA, value(sum(a.SumMoney), 0) AB   from lawage a  where a.branchtype = '1' " +
      		" and a.branchtype2 = '01' and a.indexcalno = '"+mWageNo+"' and a.ManageCom like '"+mManageCom+"%'  group by a.agentgroup) as X " +
      		" union " +
      		"select  '合计' C,value(WW,0), value(D, 0), value(E, 0),value(F,0),value(G,0),value(DD,0),value(PP,0),value(QQ,0),value(TT,0),value(WS,0), value(H, 0), value(I, 0), " +
      		" value(K, 0), value(L, 0),value(M,0),value(N, 0),value(O,0),value(P, 0), value(R, 0), value(S, 0), value(T, 0)," +
      		" value(U, 0),  value(V, 0),  value(W, 0),  value(X, 0),  value(Y, 0),value(HH,0),value(JJ,0),value(KK,0),value(LL,0),value(Z, 0),  value(AA, 0),  value(AB, 0) " +
      		" from ( select value(sum(a.k07+k08+k09),0) WW,value(sum(a.f04), 0) D, value(sum(a.f05), 0) E,value(sum(a.f07), 0) F,value(sum(a.f03), 0) G,value(sum(a.f01),0) DD,value(sum(a.f16),0) PP,value(sum(a.f18),0) QQ,value(sum(a.K16),0) TT,value(sum(a.K17),0) WS,value(sum(a.F09), 0) H, " +
      		" value(sum(a.F02), 0) I, value(sum(a.F12), 0) K, value(sum(a.F08), 0) L,value(sum(a.F14), 0) M, value(sum(a.F10), 0) N, " +
      		" value(sum(a.F15), 0) O, value(sum(a.F11), 0) P,  value(sum(a.F06), 0) R, value(sum(a.F19), 0) S, value(sum(a.F20), 0) T, " +
      		"  value(sum(a.F30), 0) U,value(sum(a.K03), 0) V,value(sum(a.K15), 0) W,value(sum(a.K02), 0) X,value(sum(a.K01), 0) Y,value(sum(a.F26), 0) HH,value(sum(a.F27), 0) JJ,value(sum(a.F28), 0) KK,value(sum(a.F29), 0) LL ,value(sum(a.K12), 0) Z, " +
			" value(sum(a.LastMoney), 0) AA,value(sum(a.SumMoney), 0) AB  from lawage a where a.branchtype = '1' and a.branchtype2 = '01'and a.indexcalno = '"+mWageNo+"' " +
			" and a.ManageCom like '"+mManageCom+"%') as X  with ur " ;
			
        System.out.println(manageSQL);
//        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
//        tCreateCSVFile.initFile(this.mfilePathAndName);
//        WriteToExcel t = new WriteToExcel("");
//        t.createExcelFile();
//        String[][] tFirstDataList = new String[1][1];
//        String[][] tTheardDataList = new String[1][1];
//        String[][] tFourDataList = new String[1][36];
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(manageSQL);
        int linenum=tSSRS.MaxRow;
        String[][] tFourDataList = new String[linenum+5][48];
        tFourDataList[0][0]="中国人民健康保险股份有限公司"+mName+"分公司"+mYear+"年"+mMonth+"月营销业务人员佣金汇总表";
        WriteToExcel t = new WriteToExcel("");
        t.createExcelFile();
        tFourDataList[1][0]="编制部门:个人销售部 ";
        tFourDataList[2][0]="单位名称 ";
        tFourDataList[2][1]="综合拓展佣金";
        tFourDataList[2][2]="首年度佣金";               
        tFourDataList[2][3]="续期佣金";                
        tFourDataList[2][4]="新人成长津贴";              
        tFourDataList[2][5]="转正奖";                 
        tFourDataList[2][6]="续保佣金";                
        tFourDataList[2][7]="孤儿单服务奖金";             
        tFourDataList[2][8]="绩优达标奖";               
        tFourDataList[2][9]="健代产佣金";
        tFourDataList[2][10]="健代寿佣金";
        
        tFourDataList[2][11]="续期服务奖";               
        tFourDataList[2][12]="岗位津贴";               
        tFourDataList[2][13]="季度奖";                
        tFourDataList[2][14]="增员奖";                
        tFourDataList[2][15]="新人辅导奖";              
        tFourDataList[2][16]="晋升奖";                
        tFourDataList[2][17]="责任津贴";               
        tFourDataList[2][18]="职务津贴";               
        tFourDataList[2][19]="管理津贴";               
        tFourDataList[2][20]="直接养成津贴";             
        tFourDataList[2][21]="间接养成津贴";             
        tFourDataList[2][22]="加款";                 
        tFourDataList[2][23]="差勤扣款";               
        tFourDataList[2][24]="养老金计提";              
        tFourDataList[2][25]="个人所得税";              
        tFourDataList[2][26]="个人增值税";              
        tFourDataList[2][27]="城建税";                
        tFourDataList[2][28]="教育费附加税";             
        tFourDataList[2][29]="地方教育附加";             
        tFourDataList[2][30]="其他附加";               
        tFourDataList[2][31]="扣款";                 
        tFourDataList[2][32]="上次佣金余额";             
        tFourDataList[2][33]="实际收入";               

        
//        tCreateCSVFile.doWrite(tFirstDataList,0);
//        tCreateCSVFile.doWrite(tTheardDataList,0);
//        tCreateCSVFile.doWrite(tFourDataList,35);
//        SSRS tSSRS = new SSRS();
//        ExeSQL tExeSQL = new ExeSQL();
//        tSSRS = tExeSQL.execSQL(manageSQL);
        try{
        	
               
//                String[][] tShowDataList = new String[tSSRS.MaxRow][36];
//                int linenum=tSSRS.MaxRow;
                for(int i=1;i<=linenum;i++)
                {
                   if (!queryOneDataList(tSSRS,i,tFourDataList[i+2])){
                	  
                	   return false;
                   }
                }
                tFourDataList[linenum+4][0]="总经理：                       复核 ：                       制表：";
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
//                tCreateCSVFile.doWrite(tShowDataList,35);
                String[] sheetName ={PubFun.getCurrentDate()};
                t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
                t.setData(0, tFourDataList);
                t.write(this.mfilePathAndName);//获得文件读取路径
        }catch(Exception exc){
        	exc.printStackTrace();
        	return false;
        }
//        String[][] tlast1DataList = new String[1][1];
//        tlast1DataList[2][0]="总经理：                       复核 ：                       制表：";
//        tCreateCSVFile.doWrite(tlast1DataList,0);
//        tCreateCSVFile.doClose();
        return true;
 }

 /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 1);
          pmOneDataList[1] =tSSRS.GetText(i, 2)+"";
          pmOneDataList[2]=tSSRS.GetText(i, 3)+"";
          pmOneDataList[3]=tSSRS.GetText(i, 4)+"";
          pmOneDataList[4] =tSSRS.GetText(i, 5)+"";
          pmOneDataList[5] = tSSRS.GetText(i, 6)+"";
          pmOneDataList[6] = tSSRS.GetText(i, 7)+"";
          pmOneDataList[7] = tSSRS.GetText(i, 8)+"";
          pmOneDataList[8] = tSSRS.GetText(i, 9)+"";
          pmOneDataList[9] =tSSRS.GetText(i, 10)+"";
          pmOneDataList[10] = tSSRS.GetText(i, 11)+"";
          pmOneDataList[11]  = tSSRS.GetText(i, 12)+"";
          pmOneDataList[12]  = tSSRS.GetText(i, 13)+"";
          pmOneDataList[13]  = tSSRS.GetText(i, 14)+"";
          pmOneDataList[14]  = tSSRS.GetText(i, 15)+"";
          pmOneDataList[15]  =tSSRS.GetText(i, 16)+"";
          pmOneDataList[16]  = tSSRS.GetText(i, 17)+"";
          pmOneDataList[17]  = tSSRS.GetText(i, 18)+"";
          pmOneDataList[18]  =tSSRS.GetText(i, 19)+"";
          pmOneDataList[19]  =tSSRS.GetText(i, 20)+"";
          pmOneDataList[20]  = tSSRS.GetText(i, 21)+"";
          pmOneDataList[21]  =tSSRS.GetText(i, 22)+"";
          pmOneDataList[22]  =tSSRS.GetText(i, 23)+"";
          pmOneDataList[23]  =tSSRS.GetText(i, 24)+"";
          pmOneDataList[24]  =tSSRS.GetText(i, 25)+"";
          pmOneDataList[25]  =tSSRS.GetText(i, 26)+"";
          pmOneDataList[26]  =tSSRS.GetText(i, 27)+"";
          pmOneDataList[27]  =tSSRS.GetText(i, 28)+"";
          pmOneDataList[28]  =tSSRS.GetText(i, 29)+"";
          pmOneDataList[29]  =tSSRS.GetText(i, 30)+"";
          pmOneDataList[30]  =tSSRS.GetText(i, 31)+"";
          pmOneDataList[31]  =tSSRS.GetText(i, 32)+"";
          pmOneDataList[32]  =tSSRS.GetText(i, 33)+"";
          pmOneDataList[33]  =tSSRS.GetText(i, 34)+"";
       
          
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
 
 
    public VData getResult()
    {
        return mResult;
    }
}
