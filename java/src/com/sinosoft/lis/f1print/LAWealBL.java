package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAWealBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mAgentState = "";
    private String mAgentGroup ="";
    private String mAgentCode ="";
    private String mManageCom = "";
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAWealBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        LAWealBL tLAWealBL = new LAWealBL();
       // tLAWealBL.submitData(tVData, "");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
        this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
        this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
        this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getAgentNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 查询数据
    * @return boolean
   */
   private boolean getAgentNow() {

       ExeSQL tExeSQL = new ExeSQL();
       String tSql =
               "select b.groupagentcode,b.name,sum(a.k15),sum(a.k04),sum(a.k15+a.k04) "
               + " from lawage a,laagent b "
               + " where  a.agentcode=b.agentcode "
               + "  and  a.managecom like '" + mManageCom + "%' "
                + " and  a.indexcalno>='"+mStartDate+"' and a.indexcalno<='"+mEndDate+"'  "
               + " and a.branchtype='1' and a.branchtype2='01' ";
       if (mAgentCode != null && !mAgentCode.equals("")) {
           tSql += " and a.agentcode = '" + mAgentCode + "' ";
       }
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
           tSql += " and a.branchattr like '%" + mBranchAttr + "%' ";
       }
       tSql += " group by a.managecom,b.groupagentcode,b.name  ";
       tSql += " order by a.managecom,b.groupagentcode,b.name  ";

       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if (mSSRS1.getMaxRow() <= 0) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LAWealReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "" ,"","" };

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0) {
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
                String Info[] = new String[7];
                Info[0] = mSSRS1.GetText(i, 1);
                Info[1] = mSSRS1.GetText(i, 2);
                Info[2] = mSSRS1.GetText(i, 3);
                Info[3] = mSSRS1.GetText(i, 4);
                Info[4] = mSSRS1.GetText(i, 5);
                Info[5] = mStartDate;
                Info[6] = mEndDate;
                mListTable.add(Info);
            }
             mListTable.setName("Order");
        } else {
            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        return true;
    }

    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LAWealBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
