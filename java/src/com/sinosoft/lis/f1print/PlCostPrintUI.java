package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author hdl
 * @version 1.0
 * @date 2012-06-02
 * @function print notice bill User Interface Layer
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlCostPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private String strMngCom, strPlaceNo, strComLevel,strBeginDate,strEndDate;

	public PlCostPrintUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}

			if (!getInputData(cInputData)) {
				return false;
			}
			VData vData = new VData();

			if (!prepareOutputData(vData)) {
				return false;
			}

			PlCostPrintBL tPlCostPrintBL = new PlCostPrintBL();
			System.out.println("Start tPlApprovalPrintUI Submit ...");
			if (!tPlCostPrintBL.submitData(vData, cOperate)) {
				if (tPlCostPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlCostPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlCostPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlCostPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "PlCostPrintUI";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}

	private boolean prepareOutputData(VData vData) {
		vData.clear();
		vData.add(strPlaceNo);
		vData.add(strMngCom);
		vData.add(strComLevel);
		vData.add(strBeginDate);
		vData.add(strEndDate);
		return true;
	}

	private boolean getInputData(VData cInputData) {
		strPlaceNo = (String) cInputData.get(0);
		strMngCom = (String) cInputData.get(1);
		strComLevel = (String) cInputData.get(2);
		strBeginDate=(String) cInputData.get(3);
		strEndDate=(String) cInputData.get(4);
	
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlCostPrintBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}
}
