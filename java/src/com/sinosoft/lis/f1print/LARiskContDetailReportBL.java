package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LARiskContDetailReportBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
       private String mBranchAttr = "";
       private String mManageName = "";

       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
       private XmlExport mXmlExport = null;
       private PubFun mPubFun = new PubFun();
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidError("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidError("submitData", "数据不完整");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(mInputData)) {
             return false;
         }

         // 进行数据查询
      if (!dealdate()) {
          return false;
      }
      System.out.println("dayin");

      //进行数据打印
      if (!getPrintData()) {
          this.bulidError("getPrintData", "查询数据失败！");
          return false;
      }
    System.out.println("dayinchenggong1232121212121");

         return true;
     }

     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {

          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
              mTransferData = (TransferData) cInputData.getObjectByObjectName(
                      "TransferData", 0);
               //页面传入的数据 三个
               this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
               this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
               this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
               this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");

               System.out.println(mManageCom);

          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

   }

   /**
      * 获取打印所需要的数据
      * @param cFunction String
      * @param cErrorMsg String
      */
     private void bulidError(String cFunction, String cErrorMsg) {

         CError tCError = new CError();

         tCError.moduleName = "LARiskContDetailReportBL";
         tCError.functionName = cFunction;
         tCError.errorMessage = cErrorMsg;

         this.mErrors.addOneError(tCError);

    }


    /**
      * 业务处理方法
      * @return boolean
      */

     private boolean dealdate()
     {
        System.out.println("chuli1");
        //查询数据
        if (!getAgentNow())
        {
           return false;
        }

        return true;
   }

    /**
      * 查询数据
      * @return boolean
     */
     private boolean getAgentNow() {

         ExeSQL tExeSQL = new ExeSQL();
         String tSql =
                 "select  a.agentcode, "
                 + "(select name from laagent where agentcode=a.agentcode),"
                 +"a.proposalcontno,"
                 +"a.prem "
                 +"from lamakecont a  "
                 +" where   a.managecom like '" + mManageCom + "%'  "
                 +" and  a.inputdate>='"+mStartDate+"' and a.inputdate<='"+mEndDate+"'  ";

              if (mBranchAttr != null && !mBranchAttr.equals("")) {
          tSql += " and a.branchattr like '%" + mBranchAttr + "%' ";
                }
          tSql += " order by a.agentcode,a.makedate,a.proposalcontno ";

         mSSRS1 = tExeSQL.execSQL(tSql);
         System.out.println(tSql);
         if (tExeSQL.mErrors.needDealError()) {
             CError tCError = new CError();
             tCError.moduleName = "MakeXMLBL";
             tCError.functionName = "creatFile";
             tCError.errorMessage = "查询XML数据出错！";

             this.mErrors.addOneError(tCError);

             return false;

         }
         if (mSSRS1.getMaxRow() <= 0) {
             CError tCError = new CError();
             tCError.moduleName = "MakeXMLBL";
             tCError.functionName = "creatFile";
             tCError.errorMessage = "没有符合条件的信息！";

             this.mErrors.addOneError(tCError);

             return false;
         }

         return true;
     }

      /**
       *
       * @return boolean
       */
      private boolean getPrintData() {
          TextTag tTextTag = new TextTag();
          mXmlExport = new XmlExport();
          //设置模版名称
          mXmlExport.createDocument("LARiskContDetailReport2.vts", "printer");

          String tMakeDate = "";
          String tMakeTime = "";

          tMakeDate = mPubFun.getCurrentDate();
          tMakeTime = mPubFun.getCurrentTime();

          System.out.print("dayin252");
         if (!getManageName()) {
              return false;
          }
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
          tTextTag.add("tName", mManageName);
          tTextTag.add("StartDate",mStartDate);
          tTextTag.add("EndDate",mEndDate);

          System.out.println("1212121" + tMakeDate);
          if (tTextTag.size() < 1) {
              return false;
          }

          mXmlExport.addTextTag(tTextTag);

          String[] title = {"", "", "", "", "" ,"","" ,"",""};

          if (!getListTable()) {
              return false;
          }
          System.out.println("111");
          mXmlExport.addListTable(mListTable, title);
          System.out.println("121");
          mXmlExport.outputDocumentToFile("c:\\", "new1");
          this.mResult.clear();

          mResult.addElement(mXmlExport);

          return true;
      }

      /**
       * 查询列表显示数据
       * @return boolean
       */
      private boolean getListTable() {
          System.out.println("dayimboiap288");
          if (mSSRS1.getMaxRow() > 0) {
              for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
                  String Info[] = new String[4];
                  Info[0] = mSSRS1.GetText(i, 1);
                  Info[1] = mSSRS1.GetText(i, 2);
                  Info[2] = mSSRS1.GetText(i, 3);
                  Info[3] = mSSRS1.GetText(i, 4);

                  mListTable.add(Info);

                  System.out.println(Info[3]);
                  System.out.println("dayin305");
              }
               mListTable.setName("Order");
          } else {
              CError tCError = new CError();
              tCError.moduleName = "CreateXml";
              tCError.functionName = "creatFile";
              tCError.errorMessage = "没有符合条件的信息！";
              this.mErrors.addOneError(tCError);
              return false;
          }
          return true;
      }

      private boolean getManageName() {

          String sql = "select name from ldcom where comcode='" + mManageCom +
                       "'";

          SSRS tSSRS = new SSRS();

          ExeSQL tExeSQL = new ExeSQL();

          tSSRS = tExeSQL.execSQL(sql);

          if (tExeSQL.mErrors.needDealError()) {

              this.mErrors.addOneError("销售单位不存在！");

              return false;

          }

          if (mManageCom.equals("86")) {
              this.mManageName = "";
          } else {
              if(mManageCom.length()>4)
              {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
              else
              {this.mManageName = tSSRS.GetText(1, 1);}
          }

          return true;
      }


      /**
       * 获取打印所需要的数据
       * @param cFunction String
       * @param cErrorMsg String
       */


      /**
       *
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }
}
