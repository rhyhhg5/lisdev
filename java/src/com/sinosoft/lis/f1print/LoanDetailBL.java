package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LoanDetailBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  private String mManageCom=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public LoanDetailBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData)
    {
      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

      // 准备所有要打印的数据
      if( !getPrintData() ) {
    	  buildError("getPrintData", "下载失败");
    	  return null;
      }  

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  PAchieveRateBL tbl =new PAchieveRateBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-08");
      tTransferData.setNameAndValue("EndDate", "2010-10-08");
      tTransferData.setNameAndValue("ManageCom", "8644");
      tGlobalInput.ManageCom="8644";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"1");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }
 
  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
 
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mManageCom.equals("")||mManageCom==null) {
        buildError("getInputData", "没有得到足够的信息:管理机构不能为空！");
        return false;
      }
    
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      //管理机构、保单号、投保人姓名、投保人联系电话、投保人联系地址、贷款金额、贷款起息日期、应还款日期。
      String[][] tTitle = {{"机构代码","管理机构","保单号","投保人姓名","投保人联系电话",
			"投保人联系地址","贷款金额","贷款起息日期","应还款日期"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select b.managecom ," + //1.机构代码
      		"        (select name from ldcom where comcode = b.managecom) ," + //2.管理机构
      		"        b.contno ," + //3.保单号
      		"        b.appntname ," + //4.投保人姓名
      		"        (select mobile " +
      		"          from lcaddress " +
      		"          where customerno = b.appntno " +
      		"            and addressno = (select addressno " +
      		"                              from lcappnt  " +
      		"                            where contno = b.contno  " +
      		"                              and appntno = b.appntno)) ,  " + // 5.投保人联系电话
      		"      (select postaladdress           from lcaddress  " +
      		"        where customerno = b.appntno   " +
      		"         and addressno = (select addressno   " +
      		"                            from lcappnt     " +
      		"                         where contno = b.contno    " +
      		"                            and appntno = b.appntno)) ,   " + // 6.投保人联系地址
      		"     a.summoney ,  " + //7.贷款金额
      		"      a.loandate ,   " + // 8.贷款起息日期
      		"     a.payoffdate  " + // 9.应还款日期
      		"  from loloan a, lcpol b  " +
      		" where a.polno = b.polno   " +
      		" and a.PayOffFlag = '0' " +
      		" and b.managecom='"+mManageCom+"'" +
      		" and payoffdate <= current date - 1 month  with ur";
      
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  } 
}

