 
package com.sinosoft.lis.f1print;

/**
 * <p>Title: YBTydDayBL</p>
 * <p>Description:银保通异地预收保费日结提数 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : yanghao
 * @date:2008-07-21
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class YBTydDayBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
         private VData mResult = new VData();
         private String mDay[] = null; //获取时间
         private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
         private String sum1=null;
    public YBTydDayBL() {
    }
    
    public static void main(String[] args) {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT") ) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINT")) { //打印提数
            if (!getPrintData()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "YBTydDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 新的打印数据方法
     * @return
     */
    private boolean getPrintData(){
       SSRS tSSRS = new SSRS();
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
       // System.out.println("aaaaaaaaaaaaaaaaaaaaaa"+mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        tGetSQLFromXML.setParameters("SumMoney", sum1);
       String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
    //     String tServerPath= "E:/sinowork/picch/WebRoot/";
        ExeSQL tExeSQL = new ExeSQL();
        
        String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        TextTag texttag = new TextTag();       //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("YBTydDayCheck1.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate",   mDay[1]);
        texttag.add("ManageCom", manageCom);

        
        String[] detailArr = new String[]{"类型","渠道","保单号码","金额","对方代码"};
        String[] totalArr  = new String[]{"类型","总金额"};
        
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[]{
        		"YBTYDDS",    // 银保通代收日结单
        		};
        
        for(int i=0; i<getTypes.length;i++){
        	String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
        	ListTable tDetailListTable = getDetailListTable(msql, getTypes[i]);   //  明细ListTable
        	xmlexport.addListTable(tDetailListTable, detailArr);                  //  明细ListTable 放入xmlexport对象
            
        	// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
        	msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]+"Z");
        	ListTable tTotalListTabel =  getHZListTable(msql,getTypes[i]+"Z");   // 汇总ListTable
        	xmlexport.addListTable(tTotalListTabel, totalArr);                   // 汇总ListTable 放入xmlexport对象
        }
     //   System.out.println("sssssssssssssssssssss"+sum1);
        texttag.add("SumMoney", sum1);
        if (texttag.size() > 0){
            xmlexport.addTextTag(texttag);
        }
        mResult.clear();
        mResult.addElement(xmlexport);
    	return true;
    }
    
	/**
	 * 获得明细ListTable
	 * @param msql - 执行的SQL
	 * @param tName - Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName){
		SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName(tName);			
        for (int i = 1; i <= tSSRS.MaxRow; i++){
            strArr = new String[tSSRS.MaxCol]; // 列数
            for (int j = 1; j <= tSSRS.MaxCol; j++){
                if (j == 5){
                    strArr[j - 1] = tSSRS.GetText(i, j);
               //     strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
               //     strArr[j - 1] = strSum;
                    continue;
                }
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        return tlistTable;
	}
	
	
	/**
	 * 显示无金额的情况
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName){
		String[] result = new String[2];
	    if("YBTYDDSZ".equals(pName)){
			result[0] = "银保通代收日结单 合计";
		}
		result[1] = "0";
		return result;
	}
	
	/**
	 * 获得汇总ListTable
	 * @param msql - 执行的SQL
	 * @param tName - Table Name
	 * @return
	 */
	private ListTable getHZListTable(String msql,String tName){
		ListTable tHZlistTable = new ListTable();
		SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
		tHZlistTable.setName(tName);  
		// 设置Table名字
		String[] strArr=null;
		if(tSSRS.MaxRow > 0){
			double sum=0;
			for(int i=1;i<=tSSRS.MaxRow;i++){
		      strArr = new String[tSSRS.MaxCol];
	            for (int j = 1; j <= tSSRS.MaxCol; j++){
	            	strArr[j - 1] = tSSRS.GetText(i, j);
	            }
	            tHZlistTable.add(strArr);
	            System.out.println("tSSRS.GetText(i, tSSRS.MaxCol):" + tSSRS.GetText(i, tSSRS.MaxCol));
	            sum += Double.parseDouble((tSSRS.GetText(i, tSSRS.MaxCol)));
//	            System.out.println("aaaaaaaaaaaaaaaaaaaaaa"+sum);
	            sum1=String.valueOf(sum);
//	            System.out.println("wwwwwwwwwwwwwwwwwwwwwwwwwww"+sum1);
			}
        }
		else{
        	strArr = getChineseNameWithPY(tName);
        }
//       int sum=0;
//        for(int i=1;i<strArr.length;i=i+tSSRS.MaxCol){
//        	System.out.println(strArr[i]);
//        	 sum+=Integer.parseInt(strArr[i]);
//        	 
//        	 System.out.println("wwwwwwwwwwwwwwwwwwww"+Integer.parseInt(strArr[i]));
//        	 System.out.println("qqqqqqqqqqqqqq"+sum);
//        }
//        sum1=String.valueOf(sum);
//       System.out.println("dddddddddddddddddd"+sum1);
		return tHZlistTable;
	}  
    
    
 
}
