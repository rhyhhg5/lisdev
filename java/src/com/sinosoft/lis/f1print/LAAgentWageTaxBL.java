/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAgentWageTaxBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计展业类型
    private String mBranchType = "";
    //统计渠道
    private String mBranchType2 = "";
    //文件暂存路径
    private String mfilePathAndName;
    //薪资月
    private String mWageNo;
    //查询年
    private String mYear;
   //查询月
    private String mMonth;
    //分公司名称
    private String mName;;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAAgentWageTaxBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("RBL start submitData");
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
        	System.out.println("RBL error PRINT");
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentWageTaxBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentWageTaxBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAgentWageTaxBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
        	  System.out.println("LAAwageSaleToTalListBL error getDataList");
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LAAwageSaleToTalListBL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mBranchType = (String)cInputData.get(1);
     mBranchType2 = (String)cInputData.get(2);
     mYear =(String)cInputData.get(3) ;
     mMonth = (String)cInputData.get(4);
     mWageNo = mYear + mMonth;
     mName = (String)cInputData.get(5);
     mfilePathAndName=(String)cInputData.get(7);
     return true;
 }




    private boolean getDataList()
  {
      //查询人员
    	System.out.println("报表打印的有问题？没走到这个类里？");
      String manageSQL = "select  B, value(C, 0), value(D, 0),value(E, 0),value(F, 0),value(G, 0),value(H, 0), (value(C, 0) + value(D, 0)+ value(E, 0) + value(F, 0) + value(G, 0) +value(H, 0)) from (select 	 " +
      		" (select b.name from labranchgroup b  where b.agentgroup = a.agentgroup) B, sum(k02) C, sum(k01) D,sum(F26) E,sum(F27) F,sum(F28) G,sum(F29) H  " +
      		" from lawage a  where a.branchtype = '1' and a.branchtype2 = '01' and a.indexcalno = '"+mWageNo+"' and a.ManageCom like '"+mManageCom+"%'  " +
      		" group by a.agentgroup) as X " +
      		" union " +
      		" select  '合计' B, value(C, 0), value(D, 0),value(E, 0),value(F, 0),value(G, 0),value(H, 0), (value(C, 0) + value(D, 0) + value(E, 0) + value(F, 0) + value(G, 0) + value(H, 0))  " +
      		" from (select sum(k02) C, sum(k01) D,sum(F26) E,sum(F27) F,sum(F28) G,sum(F29) H from lawage a  where a.branchtype = '1' and a.branchtype2 = '01' and a.indexcalno = '"+mWageNo+"' " +
      		"  and a.ManageCom like '"+mManageCom+"%') as X with ur " ;
      		
        System.out.println(manageSQL);
//        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
//        tCreateCSVFile.initFile(this.mfilePathAndName);
//        WriteToExcel t = new WriteToExcel("");
//        t.createExcelFile();
//        String[][] tFirstDataList = new String[1][1];
//        String[][] tTheardDataList = new String[1][1];
//        String[][] tFourDataList = new String[1][36];
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(manageSQL);
        int linenum=tSSRS.MaxRow;
        String[][] tFourDataList = new String[linenum+5][10];
        tFourDataList[0][0]="中国人民健康保险股份有限公司"+mName+"分公司"+mYear+"年"+mMonth+"月扣税明细表";
        WriteToExcel t = new WriteToExcel("");
        t.createExcelFile();
        tFourDataList[1][0]="编制部门:个人销售部 ";
        tFourDataList[2][0]="单位 ";
        tFourDataList[2][1]="个人所得税";
        tFourDataList[2][2]="个人增值税";
        tFourDataList[2][3]="个人城建税";
        tFourDataList[2][4]="教育费附加税";
        tFourDataList[2][5]="地方教育附加税";
        tFourDataList[2][6]="其它附加税";
        tFourDataList[2][7]="税金合计";
        
        
//        tCreateCSVFile.doWrite(tFirstDataList,0);
//        tCreateCSVFile.doWrite(tTheardDataList,0);
//        tCreateCSVFile.doWrite(tFourDataList,35);
//        SSRS tSSRS = new SSRS();
//        ExeSQL tExeSQL = new ExeSQL();
//        tSSRS = tExeSQL.execSQL(manageSQL);
        try{
        	
               
//                String[][] tShowDataList = new String[tSSRS.MaxRow][36];
//                int linenum=tSSRS.MaxRow;
                for(int i=1;i<=linenum;i++)
                {
                   if (!queryOneDataList(tSSRS,i,tFourDataList[i+2])){
                	  
                	   return false;
                   }
                }
                tFourDataList[linenum+4][0]="总经理：                       复核 ：                       制表：";
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
//                tCreateCSVFile.doWrite(tShowDataList,35);
                String[] sheetName ={PubFun.getCurrentDate()};
                t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
                t.setData(0, tFourDataList);
                t.write(this.mfilePathAndName);//获得文件读取路径
        }catch(Exception exc){
        	exc.printStackTrace();
        	return false;
        }
//        String[][] tlast1DataList = new String[1][1];
//        tlast1DataList[2][0]="总经理：                       复核 ：                       制表：";
//        tCreateCSVFile.doWrite(tlast1DataList,0);
//        tCreateCSVFile.doClose();
        return true;
 }

 /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 1);
          pmOneDataList[1] =tSSRS.GetText(i, 2)+"";
          pmOneDataList[2]=tSSRS.GetText(i, 3)+"";
          pmOneDataList[3]=tSSRS.GetText(i, 4)+"";
          pmOneDataList[4] =tSSRS.GetText(i, 5)+"";
          pmOneDataList[5] = tSSRS.GetText(i, 6)+"";
          pmOneDataList[6] = tSSRS.GetText(i, 7)+"";
          pmOneDataList[7] = tSSRS.GetText(i, 8)+"";
                   
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
 
 
    public VData getResult()
    {
        return mResult;
    }
}
