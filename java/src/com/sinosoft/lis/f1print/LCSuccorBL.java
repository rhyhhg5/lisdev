package com.sinosoft.lis.f1print;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Hashtable;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class LCSuccorBL
{
    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    /** 传入数据 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 合同信息 */
    private LCContSchema mLCContSchema;

    /** 被保人信息 */
    private LCInsuredSchema mLCInsuredScheam;

    /** 投保人信息 */
    private LCAppntSchema mLCAppntSchema;

    /** 保单打印数据 */
    private XMLDatasets mXMLDatasets;

    /** 处理Xml配置文件 */
    private File mFile = null;

    /** 路径信息 */
    private TransferData mTransferData;

    private String mTemplatePath;

    private String mOutXmlPath;

    private LCPolSet tLCPolSet;

    public LCSuccorBL()
    {
    }

    /**
     * submitData
     *
     * @param vData VData
     * @param string String
     * @return boolean
     */
    public boolean submitData(VData vData, String string)
    {
        this.mInputData = vData;
        this.mOperate = string;
        /** 开始获取前台数据 */
        if (!getInputData())
        {
            return false;
        }
        /** 教研数据 */
        if (!checkData())
        {
            return false;
        }
        /** 生成节点 */
        if (!dealData())
        {
            return false;
        }
        /** 准备后台数据 */
        try
        {
            if (!perpareOutputData())
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        if (this.mInputData == null)
        {
            buildError("getInputData", "传入数据为null！");
            return false;
        }
        if (this.mOperate == null)
        {
            buildError("getInputData", "传入操作符为null！");
            return false;
        }
        mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mTemplatePath = (String) mTransferData
                .getValueByName("TemplatePath");
        this.mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(this.mLCContSchema.getContNo());
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() <= 0)
        {
            buildError("dealInsured", "查询险种信息失败！");
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (this.mLCContSchema == null)
        {
            buildError("checkData", "传入合同信息为null！");
            return false;
        }
        if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals(""))
        {
            buildError("checkData", "传入保单号码为null！");
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mLCContSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            buildError("checkData", "未查询到保单信息！");
            return false;
        }
        else
        {
            this.mLCContSchema.setSchema(tLCContDB.getSchema());
        }
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLCContSchema.getContNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() <= 0)
        {
            buildError("checkData", "未查询到被保人信息！");
            return false;
        }
        //        if (tLCInsuredSet.size() > 1) {
        //            buildError("checkData", "统一合同系的被保人信息不是唯一信息！");
        //            return false;
        //        }
        this.mLCInsuredScheam = tLCInsuredSet.get(1).getSchema();
        /** 一致性校验 */
        if (!this.mLCContSchema.getInsuredNo().equals(
                this.mLCInsuredScheam.getInsuredNo()))
        {
            buildError("checkData", "被保人数据与保单信息不一致！");
            return false;
        }
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(this.mLCContSchema.getContNo());
        if (!tLCAppntDB.getInfo())
        {
            buildError("checkData", "查询投保人信息失败！");
            return false;
        }
        this.mLCAppntSchema = tLCAppntDB.getSchema();
        /** 一致性校验 */
        if (!this.mLCAppntSchema.getAppntNo().equals(
                this.mLCContSchema.getAppntNo()))
        {
            buildError("checkData", "头保人信息与保单信息存储不一致！");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        /** Xml对象 */
        mXMLDatasets = new XMLDatasets();
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();
        if (!dealCont(tXMLDataset))
        {
            return false;
        }
        if (!getEnglishName(tXMLDataset))
        {
            return false;
        }
        if (!getAgent(tXMLDataset))
        {
            return false;
        }
        if (!dealInsured(tXMLDataset))
        {
            return false;
        }
        if (!dealAppnt(tXMLDataset))
        {
            return false;
        }
        if (!dealManageCom(tXMLDataset))
        {
            return false;
        }
        if (!dealBnf(tXMLDataset))
        {
            return false;
        }
        if (!dealNation(tXMLDataset))
        {
            return false;
        }

        try
        {
            if (!genInsuredCard(tXMLDataset))
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("dealData", "出错！" + ex.getMessage());
            return false;
        }
        try
        {
            //            for (int i = 1; i <= tLCPolSet.size(); i++) {
            if (!getRiskInfo(tXMLDataset, null))
            {
                return false;
            }
            //            }
        }
        catch (Exception ex)
        {
            buildError("dealData", "生成xml数据失败" + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * getAgent
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean getAgent(XMLDataset tXMLDataset)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            buildError("getAgent", "查询代理人信息失败！");
            return false;
        }
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AgentCode", tLAAgentDB
                .getAgentCode()));
        tXMLDataset.addDataObject(new XMLDataTag("LAAgent.Name", tLAAgentDB
                .getName()));
        return true;
    }

    /**
     * dealNation
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealNation(XMLDataset tXMLDataset)
    {
        String nationName = "";
        String strSql = "select EnglishName from lcnation where contno='"
                + this.mLCContSchema.getContNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(strSql);
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            nationName += ssrs.GetText(i, 1);
            if (i != ssrs.getMaxRow())
            {
                nationName += ",";
            }
        }
        tXMLDataset.addDataObject(new XMLDataTag("Nation", nationName));
        return true;
    }

    /**
     * getRiskInfo
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getRiskInfo(XMLDataset tXMLDataset, String tRiskCode)
            throws Exception
    {
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "RsikInfoCard.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_CONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            tXMLDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     *
     * @param tXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset tXmlDataset) throws Exception
    {
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "InsuredCard.xml";
        System.out.println("tTemplateFile" + tTemplateFile);
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_CONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            tXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * dealBnf
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealBnf(XMLDataset tXMLDataset)
    {
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setContNo(this.mLCContSchema.getContNo());
        tLCBnfDB.setInsuredNo(this.mLCContSchema.getInsuredNo());
        LCBnfSet tLCBnfSet = tLCBnfDB.query();
        if (tLCBnfSet != null && tLCBnfSet.size() > 0)
        {
            /** 受益人姓名 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Name", tLCBnfSet
                    .get(1).getName()));

            /** 受益人性别 */
            //String sex = tLCBnfSet.get(1).getSex().equals("0") ? "男" : "女";
            // 修正受益人性别不填所引起的报错。
            String sex = "";
            if (null != tLCBnfSet.get(1).getSex())
            {
                if ("0".equals(tLCBnfSet.get(1).getSex()))
                {
                    sex = "男";
                }
                else if ("1".equals(tLCBnfSet.get(1).getSex()))
                {
                    sex = "女";
                }
            }
            // ----------------

            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Sex", sex));
            /** 受益人证件号码 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.IDNo", tLCBnfSet
                    .get(1).getIDNo()));

        }
        else
        {
            /** 受益人姓名 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Name", ""));
            /** 受益人性别 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.Sex", ""));
            /** 受益人证件号码 */
            tXMLDataset.addDataObject(new XMLDataTag("LCBnf.IDNo", ""));
        }

        return true;
    }

    /**
     * dealManageCom
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealManageCom(XMLDataset tXMLDataset)
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(this.mLCContSchema.getManageCom());
        if (!tLDComDB.getInfo())
        {
            buildError("dealManageCom", "没有查旬到管理机构代码！");
            return false;
        }
        /** 管理机构名称 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.SignCom", tLDComDB
                .getLetterServiceName()));
        /** 服务电话 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.ServicePhone", tLDComDB
                .getPhone()));
        /** 公司地址 */
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.ServicePostAddress",
                tLDComDB.getServicePostAddress()));
        /** 公司地址 */
        tXMLDataset.addDataObject(new XMLDataTag("SignComEnName", tLDComDB
                .getEName()));
        /** 公司地址 */
        tXMLDataset.addDataObject(new XMLDataTag("EnPostAddress", tLDComDB
                .getEAddress()));
        return true;
    }

    /**
     * dealAppnt
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealAppnt(XMLDataset tXMLDataset)
    {
        /** 特别约定 */
        tXMLDataset.addDataObject(new XMLDataTag("LCAppnt.EnglishName",
                this.mLCAppntSchema.getEnglishName()));

        return true;
    }

    /**
     * dealInsured
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean dealInsured(XMLDataset tXMLDataset)
    {
        /** 被保人拼音 */
        tXMLDataset.addDataObject(new XMLDataTag("LCInsured.EnglishName",
                this.mLCInsuredScheam.getEnglishName()));
        /** 被保人投保年龄 */
        tXMLDataset.addDataObject(new XMLDataTag("LCPol.InsuredAge", tLCPolSet
                .get(1).getInsuredAppAge()));
        tXMLDataset.addDataObject(new XMLDataTag("LCInsured.OthIDNo",
                this.mLCInsuredScheam.getOthIDNo()));
        return true;
    }

    /**
     * dealCont
     *
     * @return boolean
     * @param tXMLDataset XMLDataset
     */
    private boolean dealCont(XMLDataset tXMLDataset)
    {
        /** 保单号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.ContNo",
                this.mLCContSchema.getContNo()));
        /** 印刷号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.PrtNo",
                this.mLCContSchema.getPrtNo()));
        /** 投保人号码 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AppntNo",
                this.mLCContSchema.getAppntNo()));
        /** 投保人 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.GrpName",
                this.mLCContSchema.getAppntName()));
        /** 签单日期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SignDate",
                this.mLCContSchema.getSignDate()));
        /** 生效时期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.CValiDate",
                this.mLCContSchema.getCValiDate()));
        /** 总保费 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SumPrem", String
                .valueOf(this.mLCContSchema.getSumPrem())));
        /** 特别约定 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.Remark", StrTool
                .cTrim(this.mLCContSchema.getRemark())));
        /** 失效时期 */
        String InValiDate = (new ExeSQL()).getOneValue("select date('"
                + mLCContSchema.getCInValiDate() + "') - 1 day from dual");
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.CInValiDate",
                InValiDate));

        /** 被保险人姓名 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.InsuredName",
                this.mLCContSchema.getInsuredName()));

        /** 被保人性别 */
        String sex = this.mLCContSchema.getInsuredSex().equals("0") ? "男" : "女";
        tXMLDataset.addDataObject(new XMLDataTag("LCcont.InsuredSex", sex));
        /** 特别约定 */
        tXMLDataset.addDataObject(new XMLDataTag("LCcont.Remark",
                this.mLCContSchema.getRemark()));
        /** 管理机构 */
        tXMLDataset.addDataObject(new XMLDataTag("XI_ManageCom", mLCContSchema
                .getManageCom()));
        /** 签单日期 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.SignDate",
                this.mLCContSchema.getSignDate()));
        /** 单次/多次 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.DegreeType",
                this.mLCContSchema.getDegreeType()));
        /** 单次/多次 */
        tXMLDataset.addDataObject(new XMLDataTag("LCCont.AppntName",
                this.mLCContSchema.getAppntName()));

        tXMLDataset.addDataObject(new XMLDataTag("LCCont.Operator",
                this.mLCContSchema.getOperator()));
        //保全补打 qulq 2007-11-28
        tXMLDataset.addDataObject(new XMLDataTag("LostTimes",
                this.mLCContSchema.getLostTimes()));
        tXMLDataset.addDataObject(new XMLDataTag("BQRePrintDate",
                com.sinosoft.lis.bq.CommonBL
                        .decodeDate(PubFun.getCurrentDate())));

        return true;
    }

    /**
     * perpareOutputData
     *
     * @return boolean
     * @throws Exception
     */
    private boolean perpareOutputData() throws Exception
    {
        String XmlFile = mOutXmlPath + "/printdata/data/brief/J03-"
                + this.mLCContSchema.getContNo() + ".xml";
        String XmlFile_Card = mOutXmlPath + "/printdata/data/brief/J04-"
                + this.mLCContSchema.getContNo() + ".xml";

        // 对不同的打印类别（保险卡，保单），生成相应的文件。
        // 只是针对境外救援而言。其它类型简易件不适用。
        String tPrtFlag = (String) mTransferData.getValueByName("prtFlag");

        if ("POL".equals(tPrtFlag))
        {
            creatXmlFile(XmlFile);
        }
        else if ("Notice".equals(tPrtFlag))
        {
            creatXmlFile(XmlFile_Card);
        }
        // ----------------------------

        if (this.mOperate.equals("REPRINT"))
        {
            if (!deleteFile(XmlFile))
            {
                return false;
            }
            if (!deleteFile(XmlFile_Card))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * deleteFile
     *
     * @param XmlFile String
     * @return boolean
     */
    private boolean deleteFile(String XmlFile)
    {
        String file = StrTool.replaceEx(XmlFile, ".xml", ".pdf");
        file = StrTool.replaceEx(file, "brief", "briefpdf");
        try
        {
            File tFile = new File(file);
            if (tFile.exists())
            {
                tFile.delete();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("deleteFile", "重打保单失败，原因是，ｐｄｆ文件存在但是删除原ｐｄｆ文件失败！");
            return false;
        }
        return true;
    }

    /**
     * creatXmlFile
     *
     * @param XmlFile String
     * @throws Exception
     * @return boolean
     */
    private boolean creatXmlFile(String XmlFile)
    {
        InputStream ins = mXMLDatasets.getInputStream();

        try
        {
            FileOutputStream fos = new FileOutputStream(XmlFile);
            int n = 0;
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1)
            {
                fos.write(c, 0, n);
            }
            fos.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("creatXmlFile", "生成Xml文件失败！");
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCSuccorBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args)
    {
        try
        {

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * getEnglishName
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean getEnglishName(XMLDataset tXMLDataset)
    {
        String sql = "select riskcode from lmriskapp where  RiskProp='I'  and RiskType8='1' order by RiskCode ";
        SSRS ssrs = (new ExeSQL()).execSQL(sql);
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            String tRiskCode = ssrs.GetText(i, 1);
            String sql_EnName = "select RiskEnName from LMRisk a,LCPol b where a.RiskCode=b.RiskCode and b.ContNo='"
                    + this.mLCContSchema.getContNo()
                    + "' and b.RiskCode='"
                    + tRiskCode + "'";
            String tEnName = (new ExeSQL()).getOneValue(sql_EnName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "EnName", tEnName));
            String sql_ZhName = "select RiskName from LMRisk a,LCPol b where a.RiskCode=b.RiskCode and b.ContNo='"
                    + this.mLCContSchema.getContNo()
                    + "' and b.RiskCode='"
                    + tRiskCode + "'";
            String tZhName = (new ExeSQL()).getOneValue(sql_ZhName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "ZhName", tZhName));
        }
        return true;
    }

}
