package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.io.File;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;
import com.sinosoft.utility.*;


public class GrpBodyCheckPrintBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的保单号码
    private String mContNo = "";
    //String InsuredName="";
    //输入的查询sql语句
    private String msql = "";
    //取得的延期承保原因
    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    private String mBatchSQL = "";
    private String mmPrtSeq = "";
    //客户端ip
    private String mIP = "";
    int ii = 0;

    //配置文件
    private String mConfigFile = "";

    private String mOperator; //操作员
    private String mManageCom;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSchema mmLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    public GrpBodyCheckPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData("printer"))
        {
            return false;
        }
        //打印单个得体检通知书

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        this.mIP = (String) cInputData.get(2);
        this.mConfigFile = (String) cInputData.get(3);

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    String printer = "";
    private boolean getPrintData(String tPrinter)
    {
        mIP = mIP + ".*"; //普通模板
        System.out.println("不通模板的地址是" + mIP);
        String pathfull = ConfigInfo.GetValuebyArea(mIP);
        System.out.print("pathfull=" + pathfull);
        int intIndex = 0;
        int intPosition = 0;
        String strRecords = "";
        String servername = "";
        String pathname = "";

        String printname = ""; //单据对应的打印机名
        if ((pathfull.length() > 0))
        {
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition, 1);
            servername = pathfull.substring(intPosition, intIndex);
            pathfull = pathfull.substring(intIndex + 1);
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition, 1);
            pathname = pathfull.substring(intPosition, intIndex) + "/";
            System.out.println("pathname:" + pathname);
            pathfull = pathfull.substring(intIndex + 1);
            printer = pathfull;
        }
        else
        {
            buildError("getPrintDataGrpPer", "IP解析错误！");
            return false;
        }
        System.out.println("printer====" + printer);
        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq(); //打印流水号

        System.out.println("PrtNo = " + PrtNo);
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        String OldPrtNo = mLOPRTManagerSchema.getOldPrtSeq(); //打印流水号

        System.out.println("oldprtno = " + OldPrtNo);
        boolean PEFlag = false; //打印体检件部分的判断标志
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        int m, i;
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();

        mAgentCode = mLCGrpContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        String Sql = "select Name from LCPENotice where GrpContNo='" +
                     mLCGrpContSchema.getGrpContNo() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(Sql);
        int SN = 0; //页面排序序号
        String GrpPETitle[] = new String[2];
        GrpPETitle[0] = "ID";
        GrpPETitle[1] = "name";
        ListTable tGrpPEListTable = new ListTable();

        tGrpPEListTable.setName("GRPPE");
        if (!(tSSRS.GetText(1, 1).equals("0") ||
              tSSRS.GetText(1, 1).trim().equals("") ||
              tSSRS.GetText(1, 1).equals("null")))
        {
            PEFlag = true;
            for (i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                String strGrpPE[] = new String[2];
                strGrpPE[0] = (new Integer(i)).toString(); //序号
                strGrpPE[1] = tSSRS.GetText(1, i); //序号对应的内容
                tGrpPEListTable.add(strGrpPE);

            }
            SN = tSSRS.getMaxRow(); //页面排序序号
        }

//其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpPENotice.vts", printer); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        //模版自上而下的元素
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("AppntName", mLCGrpContSchema.getGrpName()); //投保人名称
        texttag.add("GrpContNo", mLCGrpContSchema.getGrpContNo()); //投保单号
        texttag.add("Cvalidate", mLCGrpContSchema.getCValiDate()); //生效日期

        texttag.add("HealthName", tSSRS.GetText(1, 1)); //被保险人名称

        texttag.add("LAAgent.Name", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("LCCont.AgentCode", mLCGrpContSchema.getAgentCode()); //代理人业务号
        texttag.add("LCCont.ManageCom", mLCGrpContSchema.getManageCom()); //营业机构
        texttag.add("PrtNo", PrtNo); //刘水号
        texttag.add("LCCont.PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号
        texttag.add("LCCont.UWCode", mLCGrpContSchema.getOperator()); // 核保师代码

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        if (PEFlag == true)
        {
            xmlexport.addListTable(tGrpPEListTable, GrpPETitle); //保存体检信息
        }
        //保存保险计划调整
        //保存其它信息

        mResult.addElement(xmlexport);

        xmlexport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);

        File tFileXml = new File(pathname);
        System.out.println("Xml.pathname==" + pathname);

        if (!tFileXml.exists())
        {
            tFileXml.mkdirs();
        }

        Date date = new Date();
        String Xmlfilename = "GrpBodyCheck" +
                             String.valueOf(date.getTime());
        xmlexport.outputDocumentToFile(pathname, Xmlfilename);

        //打印个单体检通知书

        SSRS sSSRS = new SSRS();
        ExeSQL sExeSQL = new ExeSQL();
        String SQL = "select PrtSeq from LOPRTManager where StandbyFlag3='" +
                     mLCGrpContSchema.getGrpContNo() + "'"
                     + " and Code = '" + PrintManagerBL.CODE_PE + "'";
        sSSRS = sExeSQL.execSQL(SQL);

        if (sSSRS.getMaxRow() != 0)
        {
            for (int index = 1; index <= sSSRS.getMaxRow(); index++)
            {
                mmPrtSeq = sSSRS.GetText(index, 1);
                System.out.println("mmPrtSeq=" + mmPrtSeq);
                LOPRTManagerDB ttLOPRTManagerDB = new LOPRTManagerDB();
                ttLOPRTManagerDB.setPrtSeq(mmPrtSeq);
                if (ttLOPRTManagerDB.getInfo() == false)
                {
                    mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                    buildError("outputXML", "在取得打印队列中数据时发生错误");
                    return false;
                }
                mmLOPRTManagerSchema = ttLOPRTManagerDB.getSchema();

                boolean PPOlEFlag = false; //打印体检件部分的判断标志
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mmLOPRTManagerSchema.getOtherNo());

                if (!tLCContDB.getInfo())
                {
                    mErrors.copyAllErrors(tLCContDB.mErrors);
                    buildError("outputXML", "在取得LCCont的数据时发生错误");
                    return false;
                }
                mLCContSchema = tLCContDB.getSchema();

                mAgentCode = mLCContSchema.getAgentCode();
                LAAgentDB ttLAAgentDB = new LAAgentDB();
                ttLAAgentDB.setAgentCode(mAgentCode);
                if (!tLAAgentDB.getInfo())
                {
                    mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    buildError("outputXML", "在取得LAAgent的数据时发生错误");
                    return false;
                }
                mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

                //2-体检信息
                //2-1 查询体检主表
                LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
                LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                tLCPENoticeDB.setProposalContNo(mLCContSchema.getContNo());
                tLCPENoticeDB.setPrtSeq(mmPrtSeq);
                tLCPENoticeDB.setCustomerNo(mmLOPRTManagerSchema.
                                            getStandbyFlag1());

                if (!tLCPENoticeDB.getInfo())
                {
                    mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    buildError("outputXML", "在取得体检通知的数据时发生错误");
                    return false;
                }

                tLCPENoticeSchema.setSchema(tLCPENoticeDB.getSchema());
                String HealthName = tLCPENoticeSchema.getName(); //保存体检人名称

                String PEDate = tLCPENoticeSchema.getPEDate(); //保存体检日期
                String NeedLimosis = "";
                if (tLCPENoticeSchema.getPEBeforeCond().equals("Y")) //是否需要空腹
                {
                    NeedLimosis = "是";
                }
                else
                {
                    NeedLimosis = "否";
                }
                String PEAddress = tLCPENoticeSchema.getPEAddress(); //体检地点代码

                //体检人性别和年龄

                LDPersonDB tLDPersonDB = new LDPersonDB();

                tLDPersonDB.setCustomerNo(tLCPENoticeSchema.getCustomerNo());

                if (tLDPersonDB.getInfo() == false)
                {
                    mErrors.copyAllErrors(tLDPersonDB.mErrors);
                    buildError("outputXML", "在取得打印队列中数据时发生错误");
                    return false;
                }
                mLDPersonSchema = tLDPersonDB.getSchema();

                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("hospitalcode");
                tLDCodeDB.setCode(PEAddress);
                if (tLDCodeDB.getInfo())
                {
                    PEAddress = tLDCodeDB.getCodeName();
                }
                else
                {
                    tLDCodeDB.setCodeType("hospitalcodeuw");
                    tLDCodeDB.setCode(PEAddress);
                    if (tLDCodeDB.getInfo())
                    {
                        PEAddress = tLDCodeDB.getCodeName();
                    }
                    else
                    {
                        PEAddress = "该医院代码尚未建立，请确认！";
                    }
                }

                //2-1 查询体检子表
                String[] PETitle = new String[2];
                PETitle[0] = "ID";
                PETitle[1] = "CHECKITEM";
                ListTable tPEListTable = new ListTable();
                String strPE[] = null;
                tPEListTable.setName("CHECKITEM"); //对应模版体检部分的行对象名
                LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
                LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
                tLCPENoticeItemDB.setContNo(mLCContSchema.getContNo());
                tLCPENoticeItemDB.setPrtSeq(mmPrtSeq);
                tLCPENoticeItemDB.setFreePE("N");
                tLCPENoticeItemSet.set(tLCPENoticeItemDB.query());
                if (tLCPENoticeItemSet == null ||
                    tLCPENoticeItemSet.size() == 0)
                {
                    PEFlag = false;
                }
                else
                {
                    PEFlag = true;
                    LCPENoticeItemSchema tLCPENoticeItemSchema;
                    for (i = 1; i <= tLCPENoticeItemSet.size(); i++)
                    {
                        tLCPENoticeItemSchema = new LCPENoticeItemSchema();
                        tLCPENoticeItemSchema.setSchema(tLCPENoticeItemSet.get(
                                i));
                        strPE = new String[2];
                        strPE[0] = tLCPENoticeItemSchema.getPEItemCode(); //序号
                        strPE[1] = tLCPENoticeItemSchema.getPEItemName(); //序号对应的内容
                        tPEListTable.add(strPE);
                    }
                }

                //其它模版上单独不成块的信息


                xmlexport.createDocument("PENotice.vts", printer); //最好紧接着就初始化xml文档
                //生成-年-月-日格式的日期

                String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() +
                                 "月" + StrTool.getDay() + "日";
                PEDate = PEDate + "-";
                String CheckDate = StrTool.decodeStr(PEDate, "-", 1) + "年" +
                                   StrTool.decodeStr(PEDate, "-", 2) + "月" +
                                   StrTool.decodeStr(PEDate, "-", 3) + "日";

                //模版自上而下的元素

                texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
                texttag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
                texttag.add("Post", mLCAddressSchema.getZipCode()); //投保人邮政编码
                texttag.add("Address", mLCAddressSchema.getPostalAddress()); //投保人地址
                texttag.add("AppntName", mLCContSchema.getAppntName());

                texttag.add("LCCont.AppntName", mLCContSchema.getAppntName()); //投保人名称
                texttag.add("LCCont.ContNo", mLCContSchema.getContNo()); //投保单号
                texttag.add("CheckDate", CheckDate); //体检日期
                texttag.add("HealthName", HealthName); //被保险人名称
                texttag.add("NeedLimosis", NeedLimosis); //是否需要空腹
                texttag.add("Hospital", PEAddress); //体检地点
                texttag.add("LAAgent.Name", mLAAgentSchema.getName()); //代理人姓名
                texttag.add("LCCont.AgentCode", mLCContSchema.getAgentCode()); //代理人业务号
                texttag.add("Sex", getSexName(mLDPersonSchema.getSex())); //体检人性别
                texttag.add("Age",
                            PubFun.calInterval(mLDPersonSchema.getBirthday(),
                                               PubFun.getCurrentDate(), "Y")); //体检人年龄

                texttag.add("PrtNo", PrtNo); //刘水号
                texttag.add("LCCont.PrtNo", mLCContSchema.getPrtNo()); //印刷号
                texttag.add("LCCont.UWCode", tLCPENoticeDB.getOperator()); // 核保师代码
                texttag.add("SysDate", SysDate);
                if (texttag.size() > 0)
                {
                    xmlexport.addTextTag(texttag);
                }
                //保存体检信息
                if (PEFlag == true)
                {
                    xmlexport.addListTable(tPEListTable, PETitle); //保存体检信息
                }

                mResult.addElement(xmlexport);

                xmlexport = (XmlExport) mResult.getObjectByObjectName(
                        "XmlExport", 0);

                if (!tFileXml.exists())
                {
                    tFileXml.mkdirs();
                }

                ii = ii + 1;
                String ss = new Integer(ii).toString();

                String xmlfilename = "BodyCheckPrint" +
                                     String.valueOf(date.getTime()) + ss;
                xmlexport.outputDocumentToFile(pathname, xmlfilename);

            }

        }

        return true;
    }

    private String getSexName(String StrSex)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(StrSex);
        tLDCodeDB.setCodeType("sex");
        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

}
