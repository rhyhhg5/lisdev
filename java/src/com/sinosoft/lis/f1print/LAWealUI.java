package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAWealBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LAWealUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAWealUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAWealBL tLAWealBL = new LAWealBL();

        if (!tLAWealBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAWealBL.mErrors);

            return false;
        } else {
            this.mResult = tLAWealBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
