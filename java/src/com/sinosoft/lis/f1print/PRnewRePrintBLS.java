package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
//import com.sinosoft.utility.VData;
//import com.sinosoft.utility.JdbcUrl;
import java.sql.Connection;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


public class PRnewRePrintBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mResult = new VData();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public PRnewRePrintBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start PRnewRePrintBLS Submit...");

        tReturn = save(cInputData);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End PRnewRePrintBLS Submit...");

        return tReturn;
    }

    //保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PRnewRePrintBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

// 通知书(update)
            System.out.println("Start ...");

            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
            tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                      getObjectByObjectName(
                    "LOPRTManagerSchema", 0));
            if (!tLOPRTManagerDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PRnewRePrintBLS";
                tError.functionName = "insertData";
                tError.errorMessage = "补打数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PRnewRePrintBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }

        return tReturn;
    }

}

