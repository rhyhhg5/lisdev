package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
//import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LAAgentPremReportBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDay = "";
    private String mEndDay = "";
    private String mManageCom = "";
    private String mManageName= "";
    private String mBranchAttr = "";
    private String mBranchName = "";
    private String mAgentCode = "";
    private String mRiskCode = "";
    private String mOutXmlPath="";
    private ListTable mListTable = new ListTable();
    private SSRS mSSRS1= new SSRS() ;
    private SSRS mSSRS2= new SSRS() ;
    private SSRS mSSRS3= new SSRS() ;

    private String mSql_conditiong = "" ;
    private String  mType ;
  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }
      if (!dealdate())
      {
          return false;
      }

      // 进行数据查询
      if (!getPrintData()) {
          return false;
      }

      return true;
  }
  private boolean dealdate()
  {
    if(mType.equals("0"))
    {
        if (!getAgentInfo()) {
            return false;
        }
    }
    else
    {
        if (!getRiskInfo()) {
            return false;
        }
    }

     return true;
   }
  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      return true;
  }
  private String  getManageName()
  {
      String sql="select name from ldcom where comcode='"+mManageCom+"' ";
      ExeSQL tExeSQL= new ExeSQL() ;
      String name=tExeSQL.getOneValue(sql);
      return name;
  }
  private String  getBranchName()
  {
      String sql="select name from labranchgroup where branchattr='"+mBranchAttr+"' and branchtype='2' and branchtype2='01' ";
      ExeSQL tExeSQL= new ExeSQL() ;
      String name=tExeSQL.getOneValue(sql);
      return name;
  }
  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getAgentInfo()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL  = "select (select name from ldcom e  where e.comcode=a.managecom) "
              +" ,a.managecom "
              +" ,(select name from labranchgroup e  where e.agentgroup=a.agentgroup) "
              +" ,a.branchattr "
              +" ,(select name from laagent e where e.agentcode=a.agentcode)"
              +" ,getUniteCode(a.agentcode)"
              +" ,(select e.agentgrade from latree e where e.agentcode=a.agentcode)"
              +" ,a.transmoney"
              +" ,a.standprem "
              + "   from lacommision  a where 1=1 and a.agentcode in  "
              + "   (select agentcode from latree where branchtype='2'  and branchtype2='01')  " ;
      tSQL+=mSql_conditiong+" order by a.managecom,a.branchattr,a.agentcode";




      // 2、查询需要表示的数据
      ExeSQL tExeSQL = new ExeSQL();
      mSSRS1 = tExeSQL.execSQL(tSQL);
      if(mSSRS1.getMaxRow()<=0)
      {
          buildError("getDataList","没有符合条件的信息!");
          return false;
      }

       return true;
  }
  /**
   * 按险种统计
   * @return boolean
   */
  private boolean getRiskInfo()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL  = "select (select name from ldcom e  where e.comcode=a.managecom) "
              +" ,a.managecom "
              +" ,(select name from labranchgroup e  where e.agentgroup=a.agentgroup) "
              +" ,a.branchattr "
              +" ,(select name from laagent e where e.agentcode=a.agentcode)"
              +" ,getUniteCode(a.agentcode)"
              +" ,(select e.agentgrade from latree e where e.agentcode=a.agentcode)"
              +" ,riskcode"
              +" ,(select riskname from lmriskapp e where e.riskcode=a.riskcode) "
              +" ,a.transmoney"
              +" ,a.standprem "
              + "   from lacommision  a where 1=1  and a.agentcode in  "
              + "   (select agentcode from latree where branchtype='2'  and branchtype2='01')  " ;

      tSQL=tSQL+mSql_conditiong+" order by a.managecom,a.branchattr,a.agentcode,a.riskcode";



     System.out.println(tSQL);


      // 2、查询需要表示的数据
      ExeSQL tExeSQL = new ExeSQL();
      mSSRS1 = tExeSQL.execSQL(tSQL);
      if(mSSRS1.getMaxRow()<=0)
      {
          buildError("getDataList","没有符合条件的信息!");
          return false;
      }

       return true;
  }
  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean getPrintData() {
          if(mType.equals("0"))
          {
              if (!getListTable()) {
                  return false;
              }
          }
          else
          {
              if (!getRiskTable()) {
                  return false;
              }

          }

          return true;
    }



  /**
   * 对传入的数组进行求和处理
   * @param pmArrNum int
   * @return String
   */
  private String getAgentCount()
  {
      String tReturnValue = "";
      String tSQL = "select count(distinct agentcode) from lacommision a where 1=1 ";
      tSQL += mSql_conditiong;
      ExeSQL tExeSQL= new ExeSQL() ;
      tReturnValue=tExeSQL.getOneValue(tSQL);
      return tReturnValue;
  }

  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
      mStartDay = (String) pmInputData.get(0);
      mEndDay = (String) pmInputData.get(1);
      mManageCom = (String) pmInputData.get(2);
      mBranchAttr = (String) pmInputData.get(3)==null?"":(String) pmInputData.get(3);
      mAgentCode = (String) pmInputData.get(4)==null?"":(String) pmInputData.get(4);
      mRiskCode = (String) pmInputData.get(5)==null?"":(String) pmInputData.get(5);
      mType=(String) pmInputData.get(6);
      mOutXmlPath = (String)pmInputData.get(7);
      mManageName = getManageName();
      mBranchName = getBranchName();
      System.out.println(mStartDay+" / "+mEndDay);
      mSql_conditiong=" and a.tmakedate>='"+mStartDay+"' and a.tmakedate<='"+mEndDay+"' and a.managecom like '"+mManageCom+"%'";
      if(!mBranchAttr.equals(""))
      {
          mSql_conditiong+="  and a.branchattr='" + mBranchAttr + "'";
      }
      if(!mAgentCode.equals(""))
      {
          mSql_conditiong+="  and a.agentcode='" + mAgentCode + "'";
      }
      if(!mRiskCode.equals(""))
      {
          mSql_conditiong+=" and a.riskCode='"+mRiskCode+"' ";
      }
      mSql_conditiong+=" and a.branchtype='2' and a.branchtype2='01'";

      return true;
  }
  /**
   * 查询列表显示数据 按人员统计
   * @return boolean
   */
  private boolean getListTable()
  {

	  
	  System.out.println("---->>"+this.mSSRS1.getMaxRow());
	  // 1、查看数据行数
	  int dataSize = this.mSSRS1.getMaxRow();
	  if(this.mSSRS1==null||this.mSSRS1.getMaxRow()<1)
	  {
		  buildError("getDataList","没有符合条件的信息!");
		  return false;
	  }
      // 定义一个二维数组 
	  String[][] mToExcel = new String[dataSize + 4][10];
	  // 表标题
	  mToExcel[0][0] ="业务员保费查询表(按业务员统计)";
	  // 表头
	  mToExcel[1][0] ="序号";
	  mToExcel[1][1] ="所属机构";
	  mToExcel[1][2] ="机构代码";
	  mToExcel[1][3] ="所属团队";
	  mToExcel[1][4] ="团队代码";
	  mToExcel[1][5] ="业务员姓名";
	  mToExcel[1][6] ="业务员代码";
	  mToExcel[1][7] ="业务员职级";
	  mToExcel[1][8] ="实收保费";
	  mToExcel[1][9] ="标准保费";
	  
	  double tSumPrem=0; //实收保费 合计
	  
      double tSumStrandPrem=0;//标准保费 合计
	  //循环数据处理
	  for(int i = 1;i<=this.mSSRS1.getMaxRow();i++)
	  {
		  mToExcel[i+1][0]= String.valueOf(i);;//表示序号
		  for(int j=1;j<=this.mSSRS1.getMaxCol();j++)
		  {
			  // 查询出来的结果赋值
			  mToExcel[i+1][j]=this.mSSRS1.GetText(i,j);
			  // 增加统计信息 实收保费
			  if(j==8)
			  {
				  tSumPrem =Arith.add(Double.parseDouble(mSSRS1.GetText(i, j)),tSumPrem);
			  }
             //增加统计信息 标准保费
			  if(j==9)
			  {
				  tSumStrandPrem=Arith.add(Double.parseDouble(mSSRS1.GetText(i, j)),tSumStrandPrem);
			  }
				  
		  }
		  
	  }
	  // 增加一个汇总表头信息
	  mToExcel[dataSize+2][0] ="合计";
	  mToExcel[dataSize+2][1] ="所属机构";
	  mToExcel[dataSize+2][2] ="机构代码";
	  mToExcel[dataSize+2][3] ="所属团队";
	  mToExcel[dataSize+2][4] ="团队代码";
	  mToExcel[dataSize+2][5] ="人数合计";
	  mToExcel[dataSize+2][6] ="--";
	  mToExcel[dataSize+2][7] ="--";
	  mToExcel[dataSize+2][8] ="保费合计";
	  mToExcel[dataSize+2][9] ="保费合计";
	  
	  // 增加一个汇总数据信息
	  mToExcel[dataSize+3][0] ="--";
	  mToExcel[dataSize+3][1] = mManageName ;
	  mToExcel[dataSize+3][2] = mManageCom;
	  mToExcel[dataSize+3][3] = mBranchName==null?"--":mBranchName==""?"--":mBranchName ;
	  mToExcel[dataSize+3][4] = mBranchAttr==null?"--":mBranchAttr==""?"--":mBranchAttr ;
	  mToExcel[dataSize+3][5] = getAgentCount()  ;
	  mToExcel[dataSize+3][6] ="--";
	  mToExcel[dataSize+3][7] ="--";
	  DecimalFormat tDF = new DecimalFormat("0.##");
	  mToExcel[dataSize+3][8] = tDF.format(tSumPrem) ;
	  mToExcel[dataSize+3][9] = tDF.format(tSumStrandPrem)  ;
	  
	  try
      {
          WriteToExcel t = new WriteToExcel("");
          t.createExcelFile();
          String[] sheetName ={PubFun.getCurrentDate()};
          t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
          t.setData(0, mToExcel);//生成sheet数据 类型是二维数组类型
          t.write(mOutXmlPath);//获得文件读取路径
      }
      catch(Exception ex)
      {
          ex.toString();
          ex.printStackTrace();
      }

      return true;
    }
    /**
     * 查询列表显示数据 按险种统计
     * @return boolean
     */
    private boolean getRiskTable()
    {
    	 
  	  System.out.println("---->>"+this.mSSRS1.getMaxRow());
  	  // 1、查看数据行数
  	  int dataSize = this.mSSRS1.getMaxRow();
  	  if(this.mSSRS1==null||this.mSSRS1.getMaxRow()<1)
  	  {
  		  buildError("getDataList","没有符合条件的信息!");
  		  return false;
  	  }
        // 定义一个二维数组 
  	  String[][] mToExcel = new String[dataSize + 4][12];
  	  // 表标题
  	  mToExcel[0][0] ="业务员保费查询表(按业务员统计)";
  	  // 表头
  	  mToExcel[1][0] ="序号";
  	  mToExcel[1][1] ="所属机构";
  	  mToExcel[1][2] ="机构代码";
  	  mToExcel[1][3] ="所属团队";
  	  mToExcel[1][4] ="团队代码";
  	  mToExcel[1][5] ="业务员姓名";
  	  mToExcel[1][6] ="业务员代码";
  	  mToExcel[1][7] ="业务员职级";
  	  mToExcel[1][8] ="险种编码";
  	  mToExcel[1][9] ="险种名称";
  	  mToExcel[1][10] ="实收保费";
  	  mToExcel[1][11] ="标准保费";
  	  
  	  double tSumPrem=0; //实收保费 合计
  	  
        double tSumStrandPrem=0;//标准保费 合计
  	  //循环数据处理
  	  for(int i = 1;i<=this.mSSRS1.getMaxRow();i++)
  	  {
  		  mToExcel[i+1][0]= String.valueOf(i);;//表示序号
  		  for(int j=1;j<=this.mSSRS1.getMaxCol();j++)
  		  {
  			  // 查询出来的结果赋值
  			  mToExcel[i+1][j]=this.mSSRS1.GetText(i,j);
  			  // 增加统计信息 实收保费
  			  if(j==10)
  			  {
  				  tSumPrem =Arith.add(Double.parseDouble(mSSRS1.GetText(i, j)),tSumPrem);
  			  }
               //增加统计信息 标准保费
  			  if(j==11)
  			  {
  				  tSumStrandPrem=Arith.add(Double.parseDouble(mSSRS1.GetText(i, j)),tSumStrandPrem);
  			  }
  				  
  		  }
  		  
  	  }
  	  // 增加一个汇总表头信息
  	  mToExcel[dataSize+2][0] ="合计";
  	  mToExcel[dataSize+2][1] ="所属机构";
  	  mToExcel[dataSize+2][2] ="机构代码";
  	  mToExcel[dataSize+2][3] ="所属团队";
  	  mToExcel[dataSize+2][4] ="团队代码";
  	  mToExcel[dataSize+2][5] ="人数合计";
  	  mToExcel[dataSize+2][6] ="--";
  	  mToExcel[dataSize+2][7] ="--";
  	  mToExcel[dataSize+2][8] ="--";
 	  mToExcel[dataSize+2][9] ="--";
  	  mToExcel[dataSize+2][10] ="保费合计";
  	  mToExcel[dataSize+2][11] ="保费合计";
  	  
  	  // 增加一个汇总数据信息
  	  mToExcel[dataSize+3][0] ="--";
  	  mToExcel[dataSize+3][1] = mManageName ;
  	  mToExcel[dataSize+3][2] = mManageCom;
  	  mToExcel[dataSize+3][3] = mBranchName==null?"--":mBranchName==""?"--":mBranchName ;
  	  mToExcel[dataSize+3][4] = mBranchAttr==null?"--":mBranchAttr==""?"--":mBranchAttr ;
  	  mToExcel[dataSize+3][5] = getAgentCount()  ;
  	  mToExcel[dataSize+3][6] ="--";
  	  mToExcel[dataSize+3][7] ="--";
  	  mToExcel[dataSize+3][8] ="--";
 	  mToExcel[dataSize+3][9] ="--";
  	  DecimalFormat tDF = new DecimalFormat("0.##");
  	  mToExcel[dataSize+3][10] = tDF.format(tSumPrem) ;
  	  mToExcel[dataSize+3][11] = tDF.format(tSumStrandPrem)  ;
  	  
  	  try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
            t.setData(0, mToExcel);//生成sheet数据 类型是二维数组类型
            t.write(mOutXmlPath);//获得文件读取路径
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }
    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAAgentPremReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
  }

  /**
   * 取得返回处理过的结果
   * @return VData
   */

  public VData getResult()
  {
      return this.mResult;
  }
}
