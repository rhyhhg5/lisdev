package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDueFeePrintBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
    private LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    public GrpDueFeePrintBL(){}

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        System.out.println("GrpDueFeePrintBL begin");
        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT") )
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        if(cOperate.equals("INSERT") && needPrt==0)
        {
            if (!dealPrintMag()) {
                return false;
            }
        }
        System.out.println("GrpDueFeePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if(ttLOPRTManagerSchema == null)
        {//为空--VTS打印入口
            tLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
            tLJSPayBDB = (LJSPayBDB) cInputData.getObjectByObjectName("LJSPayBDB", 0);

            if (tLCGrpContSchema == null)
            {
                buildError("getInputData", "没有得到足够的信息！");
                return false;
            }
            return true;
        }
        else
        {//PDF入口
            //取得LJSPayBDB
            LJSPayBDB mLJSPayBDB = new LJSPayBDB();
            mLJSPayBDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
            if(!mLJSPayBDB.getInfo())
            {
                mErrors.addOneError("LJSPayBDB传入的数据不完整。");
                return false;
            }
            tLJSPayBDB = mLJSPayBDB.getDB();

            //取得tLCGrpContSchema
            LCGrpContDB mLCGrpContDB = new LCGrpContDB();
            mLCGrpContDB.setGrpContNo(mLJSPayBDB.getOtherNo());
            if(!mLCGrpContDB.getInfo())
            {
                buildError("getInputData", "LCGrpContDB没有得到足够的信息！");
                return false;
            }
            tLCGrpContSchema = mLCGrpContDB.getSchema();
            return true;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }


    private boolean getPrintData()
    {
        String GrpContNo = tLCGrpContSchema.getGrpContNo();
        System.out.println("GrpContNo=" + GrpContNo);
        String GetNoticeNo = "";
        String sql = "";
        String sqlone = "";
        String sqltwo = "";
        String sql3 = "";
        String s1 = "";

        sqlone = "select * from lcgrpcont where grpcontno='" + GrpContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sqlone);
        tLCGrpContSchema = tLCGrpContSet.get(1).getSchema();

        setFixedInfo();
        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(GrpContNo);

        if (!tLCGrpAppntDB.getInfo())
        {
            CError.buildErr(this,
                            "团单投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where grpcontno='" +
              GrpContNo + "') AND AddressNo = '" + tLCGrpAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        tLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).
                              getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        switch (Integer.parseInt(tLCGrpContSchema.getPayMode()))
        {
        case 1:
            showPaymode = "现金";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 2:
            showPaymode = "现金支票";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 3:
            showPaymode = "支票";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 4:
            showPaymode = "银行转账";
            xmlexport.createDocument("GrpDueFeeBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 11:
            showPaymode = "银行汇款";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        default:
            break;
        }
        ;

        //交费频次，交费方式设置
        textTag.add("PayMode", showPaymode);

        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        System.out.println("GrpZipCode=" + tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        String appntPhoneStr = "";
        if (tLCGrpAddressSchema.getPhone1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getPhone1();
        } else if (tLCGrpAddressSchema.getMobile1() != null)
        {
            appntPhoneStr = tLCGrpAddressSchema.getMobile1();
        }
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        textTag.add("AppntNo", tLCGrpContSchema.getAppntNo());
        textTag.add("GrpContNo", GrpContNo);
        System.out.println("GrpContNo=" + "" + GrpContNo);
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("GetNoticeNo", tLJSPayBDB.getGetNoticeNo());
        textTag.add("MakeDate", CommonBL.decodeDate(tLJSPayBDB.getMakeDate()));
        textTag.add("DeadLine", tLJSPayBDB.getPayDate());
        textTag.add("Prem", getPrem());
        textTag.add("AccGetBala", getAccGetBala()); //可抵扣帐户余额
        textTag.add("SumPrem",
                    CommonBL.bigDoubleToCommonString
                    (tLJSPayBDB.getSumDuePayMoney(), "0.00")); //本次应交费

        //应收日期
        FDate tD = new FDate();
        System.out.println("PayDate" + tLJSPayBDB.getPayDate());
        Date newBaseDate = tD.getDate(tLJSPayBDB.getPayDate());
        int PayInterval = tLCGrpContSchema.getPayIntv(); //用它算出新的交至日期
        int intDay = 0;
        if (showPaymode == "现金" || showPaymode == "现金支票")
        {
            intDay = -15;
        } else
        {
            intDay = -7;
        }

        strPayCanDate = tD.getString(newBaseDate);

        //Date PayToDate = PubFun.calDate(newBaseDate, -PayInterval, "M", null);
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        sqlone = "select * from ljspaygrpb where grpcontno='" + GrpContNo +
                 "' and GetNoticeNo='"
                 + tLJSPayBDB.getGetNoticeNo() + "'";
        LJSPayGrpBSet tLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(sqlone);
        tLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(sqlone);
        tLJSPayGrpBSchema = tLJSPayGrpBSet.get(1).getSchema();

        strPayToDate = tLJSPayGrpBSchema.getLastPayToDate();

        strPayDate = tD.getString(newBaseDate);
        textTag.add("PayDate", strPayDate + "、" + strPayToDate);
        textTag.add("PayCanDate", strPayCanDate);

        //业务员信息
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        textTag.add("Phone", tLaAgentDB.getPhone());

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSchema.getAgentGroup());
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", tLABranchGroupDB.getName());
        textTag.add("XI_ManageCom", tLCGrpContSchema.getManageCom());

        textTag.add("BarCode1", tLJSPayBDB.getGetNoticeNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("GetNoticeNo" + tLJSPayBDB.getGetNoticeNo());

        if (textTag.size() > 0)
        {
            xmlexport.addTextTag(textTag);
        }

        String[] title =
                {"保单号", "期交金额", "应交时间", "可抵扣的帐户余额", "实际应交金额",
                "交费方式", "转帐银行", "转帐帐号", "应收记录号"};
        xmlexport.addListTable(getListTable(), title);

        xmlexport.outputDocumentToFile("c:\\", "TaskPrint"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        if(mOperate.equals("INSERT"))
        {
            //放入打印列表
            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
            tLOPRTManagerDB.setCode("91");
            tLOPRTManagerSet = tLOPRTManagerDB.query();
            needPrt = tLOPRTManagerSet.size();

            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
                mLOPRTManagerSchema.setOtherNoType("01");
                mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INFORM);
                mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo()); //这里存放 （也为交费收据号）
            }
        }
        return true;
    }

    /**
     * 得到本次的期交保费
     * @return String
     */
    private String getPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select varchar(sum(sumDuePayMoney)) ")
                .append("from LJSPayGrpB ")
                .append("where getNoticeNo = '")
                .append(tLJSPayBDB.getGetNoticeNo())
                .append("'  and payType = 'ZC' ");
        String prem = new ExeSQL().getOneValue(sql.toString());
        if (prem.equals("") || prem.equals("null"))
        {
            return "";
        }
        return prem;
    }

    /**
     * 得到投保人帐户余额
     * @return String：余额
     */
    private String getAccGetBala()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select varchar(sum(abs(sumActuPayMoney))) ")
                .append("from LJSPayGrpB ")
                .append("where getNoticeNo = '")
                .append(tLJSPayBDB.getGetNoticeNo())
                .append("'  and payType = 'YEL' ");
        String accGetBala = new ExeSQL().getOneValue(sql.toString());
        if (accGetBala.equals("") || accGetBala.equals("null"))
        {
            return "0.00";
        }
        return accGetBala;
    }

    private void setFixedInfo()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCGrpContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();
//        String[] info = new String[8];
//        tLJSPayBSchema = tLJSPayBDB.getSchema();
//        info[0] = tLCGrpContSchema.getGrpContNo();
//        info[1] = Double.toString(tLCGrpContSchema.getPrem());
//        info[2] = strPayToDate;
//        info[3] = Double.toString(tLCGrpContSchema.getDif());
//        info[4] = Double.toString(tLJSPayBSchema.getSumDuePayMoney());
//        info[5] = showPaymode;
//        if (tLJSPayBSchema.getBankCode() == null ||
//            "null".equals(tLJSPayBSchema.getBankCode()) ||
//            "".equals(tLJSPayBSchema.getBankCode())) {
//            info[6] = "";
//        } else {
//
//            LDBankDB tLDBankDB = new LDBankDB();
//            tLDBankDB.setBankCode(tLJSPayBSchema.getBankCode());
//            tLDBankDB.setCanSendFlag("1");
//            if (!tLDBankDB.getInfo()) {
//                CError.buildErr(this,
//                                "银行代码表中缺少数据");
//
//            } else {
//                info[6] = tLDBankDB.getBankName();
//            }
//        }
//
//        if (tLJSPayBSchema.getBankAccNo() == null ||
//            "null".equals(tLJSPayBSchema.getBankAccNo()) ||
//            "".equals(tLJSPayBSchema.getBankAccNo())) {
//            info[7] = "";
//        } else {
//            info[7] = tLJSPayBSchema.getBankAccNo();
//        }
//        info[8] = tLJSPayBSchema.getGetNoticeNo();

        String peoples2;
        if (isWMD())
        {
            peoples2 = " (select peoples2Input from LCNoNamePremTrace "
                       + "where getNoticeNo = '"
                       + tLJSPayBDB.getGetNoticeNo()
                       + "'  and contPlanCode = a.contPlanCode) ";
        } else
        {
            peoples2 = " a.peoples2 ";
        }

        String period;
        String actuPayMoney;

        //待收费 + 待核销
        if (tLJSPayBDB.getDealState().equals("0")||tLJSPayBDB.getDealState().equals("4"))
        {
            period =
                    " (select char(min(lastPayToDate))|| '~' || char(min(curPayToDate) - 1 day ) "
                    + "from LJSPayPerson where getNoticeNo = '"
                    + tLJSPayBDB.getGetNoticeNo() + "') ";
            actuPayMoney =
                    " (select varchar(sum(sumActuPayMoney)) from LJSPayPerson b, LCPol c "
                    + "where b.polNo = c.polNo "
                    + "   and c.contPlanCode = a.contPlanCode "
                    + "   and c.grpContNo = a.grpContNo "
                    + "   and getNoticeNO = '"
                    + tLJSPayBDB.getGetNoticeNo() + "') ";
        }
        //核销
        else if (tLJSPayBDB.getDealState().equals("1"))
        {
            period =
                    " (select char(min(lastPayToDate))|| '~' || char(min(curPayToDate) - 1 day ) "
                    + "from LJAPayPerson where getNoticeNo = '"
                    + tLJSPayBDB.getGetNoticeNo() + "') ";
            actuPayMoney =
                    " (select varchar(sum(sumActuPayMoney)) from LJAPayPerson b, LCPol c "
                    + "where b.polNo = c.polNo "
                    + "   and c.contPlanCode = a.contPlanCode "
                    + "   and c.grpContNo = a.grpContNo "
                    + "   and getNoticeNO = '"
                    + tLJSPayBDB.getGetNoticeNo() + "') ";

        }
        //作废或撤销
        else
        {
            String sql = "  select max(payCount) "
                         + "from LJAPayPerson "
                         + "where grpContNo = '" + tLJSPayBDB.getOtherNo() +
                         "'";
            String mapPayCount = new ExeSQL().getOneValue(sql);

            period =
                    " (select char(min(lastPayToDate))|| '~' || char(min(curPayToDate) - 1 day ) "
                    + "from LJAPayPerson "
                    + "where grpContNo = '" + tLJSPayBDB.getOtherNo() + "' "
                    + "   and payCount = " + mapPayCount + ") ";

            if (isWMD())
            {
                actuPayMoney =
                        " (select premInput from LCNoNamePremTrace where getNoticeNo = '" +
                        tLJSPayBDB.getGetNoticeNo() +
                        "' and contPlanCode = a.contPlanCode ) ";
            } else
            {
                actuPayMoney = " (select varchar(sum(sumActuPayMoney)) "
                               + "from LJAPayPerson "
                               + "where grpContNo = '"
                               + tLJSPayBDB.getOtherNo() + "'"
                               + "   and payCount = " + mapPayCount + ") ";
            }
        }
        StringBuffer sql = new StringBuffer();
        sql.append("select a.contPlanCode, " + peoples2 + ", ")
                .append(
                        "  (select handlerDate from LCGrpCont where grpcontNO = a.grpContNo), ")
                .append(period + ", ")
                .append("  (select codeName from LDCode where codeType = 'payintv' and code = (select char(min(payIntv)) from LCPol 	where grpContNo = a.grpContNo and contPlanCode = a.contPlanCode)), ")
                .append(actuPayMoney + " ")
//            .append("  (select varchar(sum(prem)) from LCPol where grpContNo = a.grpContNo and contPlanCode = a.contPlanCode) ")
                .append("from LCContPlan a ")
                .append("where grpContNo = '")
                .append(tLJSPayBDB.getOtherNo())
                .append("'  and contPlanCode != '11' ");
        System.out.println(sql.toString());
        SSRS tSSRS = new ExeSQL().execSQL(sql.toString());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[7];
            info[0] = tSSRS.GetText(i, 1);
            info[1] = tSSRS.GetText(i, 2);
            info[2] = tSSRS.GetText(i, 3);
            info[3] = tSSRS.GetText(i, 4);
            info[4] = tSSRS.GetText(i, 5);
            info[5] = tSSRS.GetText(i, 6);
            //info[6] = tSSRS.GetText(i, 7);

            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 校验报单是否无名单
     * @return boolean
     */
    private boolean isWMD()
    {
        String sql = "select 1 from LCCont where grpContNo = '"
                     + tLJSPayBDB.getOtherNo()
                     + "'  and polType = '1' ";
        String temp = new ExeSQL().getOneValue(sql);
        if (temp.equals("") || temp.equals("null"))
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();

        GrpDueFeePrintBL p = new GrpDueFeePrintBL();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000001457");

        if(!tLJSPayBDB.getInfo())
        {
            p.mErrors.addOneError("LJSPayBDB传入的数据不完整。");
            return ;
        }

        LOPRTManagerDB ttLOPRTManagerDB = new LOPRTManagerDB();
        ttLOPRTManagerDB.setCode("91");
        ttLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
        String a = ttLOPRTManagerDB.query().get(1).getPrtSeq();
        ttLOPRTManagerDB.setPrtSeq(a);

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement( ttLOPRTManagerDB.getSchema());
        p.submitData(tVData, "PRINT");

    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT
     * @return
     */
    private boolean dealPrintMag()
    {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false)
        {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
