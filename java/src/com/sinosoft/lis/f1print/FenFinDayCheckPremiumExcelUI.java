package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FenFinDayCheckPremiumExcelUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    
    public FenFinDayCheckPremiumExcelUI()
    {
    }
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            FenFinDayCheckPremiumExcelBL tFenFinDayCheckPremiumExcelBL = new FenFinDayCheckPremiumExcelBL();
            System.out.println("Start FenFinDayCheckPremiumExcelBL Submit ...");

            if (!tFenFinDayCheckPremiumExcelBL.submitData(cInputData, cOperate))
            {
                if (tFenFinDayCheckPremiumExcelBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFenFinDayCheckPremiumExcelBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","FenFinDayCheckPremiumExcelBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "操作失败！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FenFinDayCheckPremiumExcelUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}