package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  hzy lys
 * @date:2003-06-04
 * @version 1.0
 */
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class TeXuDayCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private String mRiskName = "";
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public TeXuDayCheckBL()
    {
        mErrors=new CErrors();
        mResult=new VData();
        mDay=null;
        mRiskName="";
        mGlobalInput=new GlobalInput();

    }

    public static void main(String[] args)
    {
//        GlobalInput tG = new GlobalInput();
//        tG.Operator = "001";
//        tG.ManageCom = "86110000";
//        VData vData = new VData();
//        String[] tDay = new String[2];
//        tDay[0] = "2007-01-14";
//        tDay[1] = "2007-07-14";
//        vData.addElement(tDay);
//        vData.addElement(tG);
//
//        TeXuDayCheckBL tF = new TeXuDayCheckBL();
//        tF.submitData(vData, "PRINTGET");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINTGET")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        double SumMoney4 = 0;
        double SumMoney5 = 0;
        double SumMoney6 = 0;
        double SumMoney7 = 0;
        double SumMoney8 = 0;
        int SumNum=0;
//        String msql = "select b.contno, b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode), "
//    +"(select sum(InsuAccBala) from lcinsureacc where contno=b.contno and polno=b.polno and riskcode=b.riskcode) BJ, "
//    +"(select sum(getmoney) from LJAGetendorse where feeoperationtype ='MJ' and "
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode and Feefinatype='LX') LX, "
//    +" (select sum(fee) from lcinsureaccfee where contno=b.contno and polno=b.polno and riskcode=b.riskcode) GF,"
//    +" 0,(select sum(getmoney) from LJAGetendorse where feeoperationtype='MJ' and"
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode and Feefinatype='TF') DF"
//    +" from lcpol b where  b.grpcontno='00000000000000000000' and"
//    +" b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +" b.contno in (select contno from LJAGetendorse where feeoperationtype='MJ' and getConfirmDate>='"+mDay[0]+"' and getConfirmDate<='"+mDay[1]+"')"
//        
//    +" union all"
//
//    +" select b.contno, b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode),"
//    +" (select sum(InsuAccBala) from lcinsureacc where contno=b.contno and polno=b.polno and riskcode=b.riskcode) BJ,"
//    +" (select sum(getmoney) from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and"
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode and Feefinatype='LX') LX,"
//    +" (select sum(fee) from lcinsureaccfee where contno=b.contno and polno=b.polno and riskcode=b.riskcode) GF,"
//    +" (select sum(getmoney) from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and"
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode  and Feefinatype='TF') JZ,"
//    +"  0 from lbpol b where b.grpcontno='00000000000000000000' and"
//    +"   b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +" b.contno in (select contno from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and getConfirmDate>='"+mDay[0]+"' and getConfirmDate<='"+mDay[1]+"')"
//        
//    +" union all"
//
//    +" select b.grpcontno,b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode),"
//    +"  (select  sum(InsuAccBala) from lcinsureacc where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) BJ,"
//    +" (select  sum(getmoney) from LJAGetendorse where feeoperationtype ='MJ' and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode and Feefinatype='LX') LX,"
//    +"  (select  sum(fee) from lcinsureaccfee where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) GF,"
//    +"  0, (select  sum(getmoney) from LJAGetendorse where feeoperationtype='MJ' and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode and Feefinatype='TF') DF"
//    +" from lcgrppol b where b.grpcontno<>'00000000000000000000' and"
//    +"   b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +"  b.grpcontno in (select grpcontno from LJAGetendorse where feeoperationtype='MJ' and getConfirmDate>='"+mDay[0]+"' and getConfirmDate<='"+mDay[1]+"')"
//
//    +" union all"
//
//    +" select b.grpcontno, b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode),"
//    +"  (select  sum(InsuAccBala) from lcinsureacc where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) BJ,"
//    +" (select  sum(getmoney) from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode and Feefinatype='LX') LX,"
//    +" (select  sum(fee) from lcinsureaccfee where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) GF,"
//    +" (select  sum(getmoney) from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode  and Feefinatype='TF') JZ,"
//    +" 0 from lbgrppol b where b.grpcontno<>'00000000000000000000' and"
//    +"  b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +"  b.grpcontno in (select grpcontno from LJAGetendorse where feeoperationtype in ('WT','ZT','XT','CT') and getConfirmDate>='"+mDay[0]+"' and getConfirmDate<='"+mDay[1]+"')"
//    
//    +" union "
//    
//    +"select b.contno, b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode), "
//    +"(select sum(InsuAccBala) from lcinsureacc where contno=b.contno and polno=b.polno and riskcode=b.riskcode) BJ, "
//    +"(select sum(getmoney) from LJAGetendorse where feeoperationtype ='MJ' and "
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode and Feefinatype='LX') LX, "
//    +" (select sum(fee) from lcinsureaccfee where contno=b.contno and polno=b.polno and riskcode=b.riskcode) GF,"
//    +" 0,(select sum(getmoney) from LJAGetendorse where feeoperationtype='MJ' and"
//    +" contno=b.contno and polno=b.polno and riskcode=b.riskcode and Feefinatype='TF') DF"
//    +" from lcpol b where  b.grpcontno='00000000000000000000' and"
//    +" b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +" b.contno in (select incomeno from LJAPay where  confdate>='"+mDay[0]+"' and confdate<='"+mDay[1]+"')" 
//    
//    
//    +" union "
//
//    +" select b.grpcontno,b.riskcode,(select riskname from LMRiskApp where riskcode=b.riskcode),"
//    +"  (select  sum(InsuAccBala) from lcinsureacc where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) BJ,"
//    +" (select  sum(getmoney) from LJAGetendorse where feeoperationtype ='MJ' and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode and Feefinatype='LX') LX,"
//    +"  (select  sum(fee) from lcinsureaccfee where grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode) GF,"
//    +"  0, (select  sum(getmoney) from LJAGetendorse where feeoperationtype='MJ' and"
//    +" grpcontno=b.grpcontno and grppolno=b.grppolno and riskcode=b.riskcode and Feefinatype='TF') DF"
//    +" from lcgrppol b where b.grpcontno<>'00000000000000000000' and"
//    +"   b.riskcode in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode In ('170101','170301')) and "
//    +"  b.grpcontno in (select incomeno from LJAPay where  confdate>='"+mDay[0]+"' and confdate<='"+mDay[1]+"') with ur"; 

        GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "TeXu");
        
        
 
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName("TeXu");
        SumNum=tSSRS.MaxRow;
        for (int i = 1; i <= SumNum; i++)
        {
            strArr = new String[8];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
                if (j == 4 )
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney4 = SumMoney4 + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                    
                }
                else if(j == 5){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney5 = SumMoney5 + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                }
                else if(j == 6){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney6 = SumMoney6 + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                }
                else if(j == 7){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney7 = SumMoney7 + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                }
                else if(j == 8){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney8 = SumMoney8 + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                }
            }
            tlistTable.add(strArr);
        }
        strArr = new String[8];
        strArr[0]="contno";
        strArr[1]="riskcode";
        strArr[2]="riskname";
        strArr[3]="benjin";
        strArr[4]="lixi";
        strArr[5]="guanlifee";
        strArr[6]="jyzhifu";
        strArr[7]="dqfanhuan";

        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        SSRS nSSRS = new SSRS();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("TeXuDayCheck.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney4", SumMoney4);
        texttag.add("SumMoney5", SumMoney5);
        texttag.add("SumMoney6", SumMoney6);
        texttag.add("SumMoney7", SumMoney7);
        texttag.add("SumMoney8", SumMoney8);
        texttag.add("SumNum", SumNum);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("E:\\","TeXu");
        return true;
    }

    private void initCommon()
    {
        this.mRiskCode = "";
        this.mRiskType = "";
        this.mXsqd = "";
        this.mXzdl = "";
        this.mSxq = "";
        this.mDqj = "";
    }


}
