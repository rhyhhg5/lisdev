package com.sinosoft.lis.f1print;

import java.io.*;

import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class LCContPrintIntlPdfBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;  //保单打印路径
    private LCContSchema mLCContSchema = null;  //合同信息
    private VData mInputData = null;
    private XMLDatasets mXMLDatasets = null;

    private MMap map = new MMap(); //待提交的数据集合

    public LCContPrintIntlPdfBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitData(cInputData, operate) == null)
        {
            return false;
        }

        if(!outputXML())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * outPutXML
     *
     * @return boolean
     */
    private boolean outputXML()
    {
        mTransferData = (TransferData) mInputData
                        .getObjectByObjectName("TransferData", 0);
        String tOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");

        String tXmlFile = tOutXmlPath + "/printdata/data/brief/C001-"
                          + this.mLCContSchema.getContNo() + ".xml";
        System.out.println("\n\n\nXmlPath" + tXmlFile);

        InputStream ins = mXMLDatasets.getInputStream();

        try
        {
            FileOutputStream fos = new FileOutputStream(tXmlFile);

            int n = 0;
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while((n = ins.read(c)) != -1)
            {
                fos.write(c, 0, n);
            }
            fos.close();
        }
        catch(Exception ex)
        {
            System.out.println("\n\n\n报错了");
            ex.printStackTrace();
            return false;
        }


        return true;
    }

    /**
     * getSubmitData
     *
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：true提交成功, false提交失败
     */
    private MMap getSubmitData(VData cInputData, String operate)
    {
        mInputData = cInputData;

        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        if(mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入操作员信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if(mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入保单打印路径信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if(mLCContSchema == null || mLCContSchema.getContNo() == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入保单信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        LCContDB tLCContDB = mLCContSchema.getDB();
        if(!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有查询到合同信息" + tLCContDB.getContNo();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        /** Xml对象 */
        mXMLDatasets = new XMLDatasets();
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();
        mInputData.add(tXMLDataset);  //此处使用的是引用，LCContPrintPdfBL处理完后不需要显式获取

        if(!dealCommonInfo(tXMLDataset))
        {
            return false;
        }

        if(!dealInsuredInfo(tXMLDataset))
        {
            return false;
        }


//        InputStream ins = mXMLDatasets.getInputStream();
//        try
//        {
//            FileOutputStream fos = new FileOutputStream("D:\\test.xml");
//            int n = 0;
//            //采用缓冲池的方式写文件，针对I/O修改
//            byte[] c = new byte[4096];
//            while ((n = ins.read(c)) != -1)
//            {
//                fos.write(c, 0, n);
//            }
//            fos.close();
//        }
//        catch (Exception ex)
//        {
//            ex.printStackTrace();
//        }


        return true;
    }

    /**
     * dealInsuredInfo
     *生成被保人信息
     * @return boolean
     */
    private boolean dealInsuredInfo(XMLDataset tXMLDataset)
    {
        LCInsuredDB db = new LCInsuredDB();
        db.setContNo(mLCContSchema.getContNo());
        LCInsuredSet set = db.query();
        if(set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "dealInsuredInfo";
            tError.errorMessage = "没有查询到被保人信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        //为每个被保人生成保障信息
        for(int i = 1; i <= set.size(); i++)
        {
            Element elementDataset = new Element("InsuredList");

            //本信息
            XMLDataList tXMLDataList = getInsuredDataList(set.get(i));
            tXMLDataList.addDataTo(elementDataset);

            //保障信息
            XMLDataList tXMLDataList2 = getInsuredPolList(set.get(i));
            if(tXMLDataList2 == null)
            {
                return false;
            }
            tXMLDataList2.addDataTo(elementDataset);

            tXMLDataset.getElementNoClone().addContent(elementDataset);
        }

        return true;
    }

    /**
     * getInsuredPolList
     *
     * @param lCInsuredSchema LCInsuredSchema
     * @return XMLDataList
     */
    private XMLDataList getInsuredPolList(LCInsuredSchema insuredSchema)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCGet");
        tXMLDataList.addColHead("InsuredNo");
        tXMLDataList.addColHead("PolNo");
        tXMLDataList.addColHead("DutyCode");
        tXMLDataList.addColHead("GetDutyCode");
        tXMLDataList.addColHead("GetDutyName");
        tXMLDataList.addColHead("Amnt");
        tXMLDataList.addColHead("AmntFlag");
        tXMLDataList.addColHead("GetLimit");
        tXMLDataList.addColHead("GetLimitFlag");
        tXMLDataList.addColHead("SelfGetRate");
        tXMLDataList.buildColHead();

        //境外救援险种
        String sql = "select a.InsuredNo, a.PolNo, a.DutyCode, a.GetDutyCode, "
                     + "   b.GetDutyName, a.StandMoney, 'AmntFlag',a.GetLimit, "
                     + "   'GetLimitFlag', (1 - (select GetRate from LCDuty where PolNo = a.PolNo and DutyCode = a.DutyCode)) "
                     + "from LCGet a, LMDutyGet b "
                     + "where a.GetDutyCode = b.GetDutyCode "
                     + "   and b.InpFlag = 'G' "
                     + "   and a.ContNo = '"
                     + insuredSchema.getContNo() + "' "
                     + "   and a.InsuredNo = '"
                     + insuredSchema.getInsuredNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(!setOneXMLDataList(tXMLDataList, tSSRS))
        {
            return null;
        }

        //国际险种保障信息
        sql = "select b.InsuredNo, b.PolNo, a.DutyCode, '000000', "
              + "  (select DutyName from LMDuty where DutyCode = a.DutyCode), "
              + "  a.Amnt, 'AmntFlag',a.GetLimit,'GetLimitFlag',(1 - GetRate) "
              + "from LCDuty a, LCPol b "
              + "where a.PolNo = b.PolNo "
              + "   and a.ContNo = '" + insuredSchema.getContNo() + "' "
              + "   and b.InsuredNo = '" + insuredSchema.getInsuredNo() + "' ";
        System.out.println(sql);
        tSSRS = new ExeSQL().execSQL(sql);
        if(!setOneXMLDataList(tXMLDataList, tSSRS))
        {
            return null;
        }

        return tXMLDataList;

    }

    /**
     * 生成保障信息列表
     * @param <any> XMLDataListtXMLDataList
     * @return boolean
     */
    private boolean setOneXMLDataList(XMLDataList tXMLDataList, SSRS tSSRS)
    {
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            //往xml对象中添加现金价值信息
            tXMLDataList.setColValue("InsuredNo", tSSRS.GetText(i, 1));
            tXMLDataList.setColValue("PolNo", tSSRS.GetText(i, 2));
            tXMLDataList.setColValue("DutyCode", tSSRS.GetText(i, 3));
            tXMLDataList.setColValue("GetDutyCode", tSSRS.GetText(i, 4));
            tXMLDataList.setColValue("GetDutyName", tSSRS.GetText(i, 5));
            tXMLDataList.setColValue("Amnt", tSSRS.GetText(i, 6));

            String flag = getFlag(tSSRS.GetText(i, 6));
            if(flag == null)
            {
                return false;
            }
            tXMLDataList.setColValue("AmntFlag", flag);

            tXMLDataList.setColValue("GetLimit", tSSRS.GetText(i,8));

            flag = getFlag(tSSRS.GetText(i, 8));
            if(flag == null)
            {
                return false;
            }
            tXMLDataList.setColValue("GetLimitFlag", flag);

            tXMLDataList.setColValue("SelfGetRate", tSSRS.GetText(i, 10));
            tXMLDataList.insertRow(0);
        }

        return true;
    }

    /**
     * 判断传入的数字是否可显示为无限制标志1
     * @param sourceNumber String
     * @return String：若sourceNumber~99999999.00：0，否则0
     */
    private String getFlag(String sourceNumber)
    {
        try
        {
            double number = Double.parseDouble(sourceNumber);
            if(99999999.00 - number > 0.001)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LCContPrintIntlPdfBL";
            tError.functionName = "getFlag";
            tError.errorMessage = "请传入数字字符串";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
    }

    /**
     * 得到被保人的基本信息
     * @param schema LCInsuredSchema
     * @return XMLDataList
     */
    private XMLDataList getInsuredDataList(LCInsuredSchema insuredSchema)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCInsured");
        tXMLDataList.addColHead("InsuredNo");
        tXMLDataList.addColHead("Name");
        tXMLDataList.addColHead("Birthday");
        tXMLDataList.addColHead("Sex");
        tXMLDataList.addColHead("IDType");
        tXMLDataList.addColHead("IDNo");
        tXMLDataList.addColHead("EnglishName");
        tXMLDataList.addColHead("Prem");
        tXMLDataList.buildColHead();

        //往xml对象中添加现金价值信息
        tXMLDataList.setColValue("InsuredNo", insuredSchema.getInsuredNo());
        tXMLDataList.setColValue("Name", insuredSchema.getName());
        tXMLDataList.setColValue("Birthday", insuredSchema.getBirthday());
        tXMLDataList.setColValue("Sex", insuredSchema.getSex());
        tXMLDataList.setColValue("IDType", insuredSchema.getIDType());
        tXMLDataList.setColValue("IDNo", insuredSchema.getIDNo());
        tXMLDataList.setColValue("EnglishName", insuredSchema.getEnglishName());
        tXMLDataList.setColValue("Prem", getPrem(insuredSchema.getInsuredNo()));
        tXMLDataList.insertRow(0);

        return tXMLDataList;
    }

    /**
     * getPrem
     *得到被保人的保费
     * @param string String
     * @return String
     */
    private String getPrem(String cInsuredNo)
    {
        String sql = "select sum(Prem) "
                     + "from LCPol "
                     + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and InsuredNo = '" + cInsuredNo + "' ";
        String sumPrem = new ExeSQL().getOneValue(sql);

        return ("".equals(sumPrem) || sumPrem.equals("null") ? "0" : sumPrem);
    }

    /**
     * 生成投保人、管理机构等公共信息
     * @return boolean
     */
    private boolean dealCommonInfo(XMLDataset tXMLDataset)
    {
        LCContPrintPdfBL bl = new LCContPrintPdfBL();
        MMap tMMap = bl.getSubmitMap(mInputData, "PRINT");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        mLCContSchema = (LCContSchema) data
                        .getObjectByObjectName("LCContSchema", 0);

        return true;
    }


    public static void main(String[] args)
    {
        VData data = new VData();

        LCContSchema schema = new LCContSchema();
        schema.setContNo("13000513459");
        data.add(schema);

        GlobalInput gi = new GlobalInput();
        data.add(gi);

        TransferData tf = new TransferData();
        tf.setNameAndValue("TemplatePath", "D:/picch/uif1print/template/");
        tf.setNameAndValue("OutXmlPath", "D:/picch/ui/printdata/data");
        data.add(tf);

        LCContPrintIntlPdfBL bl = new LCContPrintIntlPdfBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
