package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * function :
 * @version 1.0
 * @date
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PrintFailListBL
{
    public  CErrors mErrors=new CErrors();
    private GlobalInput mGlobalInput;
    private String mSql;
    private XmlExport mXmlExport=new XmlExport();

    public PrintFailListBL()
    {
    }
    public XmlExport getXmlExport(VData data, String operate)
    {
        if(getInputData(data)==false)
        {
            mXmlExport=null;

        }
        else if(dealData()==false)
             {
                mXmlExport=null;
             }

        return mXmlExport;
    }
    public boolean dealData()
    {
        SSRS tSSRS =new ExeSQL().execSQL(mSql);
        if(tSSRS==null||tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到需要的清单数据");
            return false;
        }

        ListTable tlistTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[tSSRS.getMaxCol()+1];
            info[0]=""+i;
            for(int position=1; position <= tSSRS.getMaxCol(); position++)
            {
                info[position] = StrTool.cTrim(tSSRS.GetText(i, position));
            }

            tlistTable.add(info);
        }

        tlistTable.setName("failList");
        mXmlExport.createDocument("PrtFailList.vts", "printer");
        mXmlExport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);
        mXmlExport.outputDocumentToFile("d:\\","failList");
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mSql = (String)cInputData.get(0);
        System.out.print(mSql);
        if(mSql == null || mSql.equals(""))
        {
            mErrors.addOneError("请您在查询到清单后再执行打印程序。");
            return false;
        }
        mGlobalInput = (GlobalInput) cInputData.get(1);
        return true;
    }
    public static void main(String[] args)
   {
       PrintFailListBL test = new PrintFailListBL();
       test.mSql=args[0];
       test.dealData();
   }


}
