/*
 * <p>ClassName: LCGrpPolF1PBL </p>
 * <p>Description: LCGrpPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.pubfun.EasyScanQueryBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.COracleBlob;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataBlob;
import com.sinosoft.utility.XMLDataFormater;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

public class LCGrpPolF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mOperate = "";

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private String mszTemplatePath = null;
    private XMLDatasets mXMLDatasets = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vGrpPolNo = new Vector();

    public LCGrpPolF1PBL()
    {
    }

    /**
            传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");

                return false;
            }

            mOperate = cOperate;

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (mOperate.equals("PRINT"))
            {
                // 准备所有要打印的数据
                mXMLDatasets = new XMLDatasets();
                mXMLDatasets.createDocument();
                System.out.println(
                        "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                getPrintData();
            }

            if (mOperate.equals("REPRINT"))
            {
                getRePrintData();
            }

            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());

            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        if (mOperate.equals("PRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
            mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                    "LCGrpPolSet", 0));
            mszTemplatePath = (String) cInputData.getObjectByObjectName(
                    "String",
                    0);
        }

        if (mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
            mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                    "LCGrpPolSet", 0));
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void getPrintData() throws Exception
    {
        LCGrpPolSchema tLCGrpPolSchema = null;

        for (int nIndex = 0; nIndex < mLCGrpPolSet.size(); nIndex++)
        {
            tLCGrpPolSchema = mLCGrpPolSet.get(nIndex + 1);

            // 如果已经打印过这份保单
            if (m_vGrpPolNo.contains(tLCGrpPolSchema.getGrpPolNo()))
            {
                continue;
            }

            LCGrpPolSet tLCGrpPolSet = getRiskList(tLCGrpPolSchema.getGrpPolNo());

            // 校验所有保单是否符合打印条件
            // 记录下已经打印过的保单
            for (int n = 0; n < tLCGrpPolSet.size(); n++)
            {
                LCGrpPolSchema tempLCGrpPolSchema = tLCGrpPolSet.get(n + 1);
                String strGrpPolNo = tempLCGrpPolSchema.getGrpPolNo();

                if (!tempLCGrpPolSchema.getAppFlag().equals("1"))
                {
                    throw new Exception(strGrpPolNo + "号保单还没有签单");
                }
                /*Lis5.3 upgrade get
                                if (tempLCGrpPolSchema.getPrintCount() >= 1) {
                 throw new Exception(strGrpPolNo + "号保单已经打印过了");
                                }
                 */
                m_vGrpPolNo.add(strGrpPolNo);
            }

            // 如果是重打
            /*Lis5.3 upgrade get
                        if (tLCGrpPolSet.get(1).getPrintCount() == -1) {
                            getGrpPolDataSetEx(tLCGrpPolSet);
                        } else { // 如果是正常打印
                            getGrpPolDataSet(tLCGrpPolSet);
                        }
             */
        }

        LCGrpPolSet tempLCGrpPolSet = new LCGrpPolSet();

        for (int nIndex = 0; nIndex < m_vGrpPolNo.size(); nIndex++)
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo((String) m_vGrpPolNo.get(nIndex));

            if (!tLCGrpPolDB.getInfo())
            {
                throw new Exception("找不到要更新数据的团单数据");
            }
            /*Lis5.3 upgrade set
                         tLCGrpPolDB.setPrintCount(1);
             */
            tempLCGrpPolSet.add(tLCGrpPolDB);
        }

        // 准备要保存的数据
        mResult.add(tempLCGrpPolSet);
        mResult.add(mXMLDatasets);
        mResult.add(mGlobalInput);

        LCGrpPolF1PBLS tLCGrpPolF1PBLS = new LCGrpPolF1PBLS();
        tLCGrpPolF1PBLS.submitData(mResult, "PRINT");

        if (tLCGrpPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCGrpPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
        }

        mResult.clear();
        System.out.println("add inputstream to mResult");
        mResult.add(mXMLDatasets.getInputStream());
    }

    private void getScanPic(XMLDataset xmlDataset,
                            LCGrpPolSchema aLCGrpPolSchema) throws Exception
    {
        XMLDataList xmlDataList = new XMLDataList();

        xmlDataList.setDataObjectID("PicFile");

        xmlDataList.addColHead("FileUrl");
        xmlDataList.addColHead("PageIndex");
        xmlDataList.buildColHead();

        VData vData = new VData();
        vData.add(aLCGrpPolSchema.getPrtNo());

        EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();

        if (!tEasyScanQueryBL.submitData(vData, "QUERY||MAIN"))
        {
            System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
        }
        else
        {
            vData.clear();
            vData = tEasyScanQueryBL.getResult();

            String strFileName = "";

            for (int nIndex = 0; nIndex < vData.size(); nIndex++)
            {
                strFileName = (String) vData.get(nIndex);
                strFileName = strFileName.substring(0,
                        strFileName.lastIndexOf(".")) +
                              ".tif";

                xmlDataList.setColValue("FileUrl", strFileName);
                xmlDataList.setColValue("PageIndex", nIndex);
                xmlDataList.insertRow(0);
            }
        }

        xmlDataset.addDataObject(xmlDataList);
    }

    private void getRiskInfo(XMLDataset xmlDataset, LCGrpPolSet aLCGrpPolSet) throws
            Exception
    {
        // Get all LCGrpPols of the same prtno
        XMLDataList xmlDataList = new XMLDataList("RiskInfo");

        xmlDataList.addColHead("RiskCode");
        xmlDataList.addColHead("GrpPolNo");
        xmlDataList.addColHead("Peoples");
        xmlDataList.addColHead("InsuPeriod");
        xmlDataList.addColHead("Prem");
        xmlDataList.addColHead("PayIntv");
        xmlDataList.addColHead("Remark");
        xmlDataList.addColHead("PayMoney");
        xmlDataList.addColHead("ManageFeeRate");
        xmlDataList.addColHead("Remark1");
        xmlDataList.addColHead("GrpPrem");
        xmlDataList.addColHead("Prem1");
        xmlDataList.addColHead("PremSumChn");
        xmlDataList.addColHead("AcctPremSum");
        xmlDataList.addColHead("Template");
        xmlDataList.addColHead("SubRiskFlag");

        xmlDataList.buildColHead();

        double dPremSum = 0;
        double dPayMoneySum = 0;

        // 加入伤残给付和医院列表的信息
        String strInjuryGetFlag = "N";
        String strHospitalFlag = "N";
        String strRiskCode = "";
        String strTemp = "";

        LMRiskAppDB tLMRiskAppDB = null;

        for (int nIndex = 0; nIndex < aLCGrpPolSet.size(); nIndex++)
        {
            LCGrpPolSchema tLCGrpPolSchema = aLCGrpPolSet.get(nIndex + 1);

            strRiskCode = tLCGrpPolSchema.getRiskCode();

            tLMRiskAppDB = new LMRiskAppDB();

            tLMRiskAppDB.setRiskCode(strRiskCode);

            if (!tLMRiskAppDB.getInfo())
            {
                throw new Exception(tLMRiskAppDB.mErrors.getFirstError());
            }

            strTemp = tLMRiskAppDB.getNeedPrintGet();

            if ((strTemp != null) && strTemp.equals("1"))
            {
                strInjuryGetFlag = "Y";
            }

            strTemp = tLMRiskAppDB.getNeedPrintHospital();

            if ((strTemp != null) && strTemp.equals("1"))
            {
                strHospitalFlag = "Y";
            }

            xmlDataList.setColValue("RiskCode", tLCGrpPolSchema.getRiskCode());
            xmlDataList.setColValue("GrpPolNo", tLCGrpPolSchema.getGrpPolNo());
            xmlDataList.setColValue("Peoples", tLCGrpPolSchema.getPeoples2());
            xmlDataList.setColValue("InsuPeriod", genInsuPeriod(tLCGrpPolSchema));
            xmlDataList.setColValue("Prem", format(tLCGrpPolSchema.getPrem()));
            xmlDataList.setColValue("PayIntv", tLCGrpPolSchema.getPayIntv());
            xmlDataList.setColValue("Remark", tLCGrpPolSchema.getRemark());

            xmlDataList.setColValue("PayMoney",
                                    format(tLCGrpPolSchema.getPrem()));
            dPayMoneySum += tLCGrpPolSchema.getPrem();

            //      String strSQL = "SELECT SumActuPayMoney FROM LJAPay";
            //      strSQL += " WHERE IncomeNo = '" + tLCGrpPolSchema.getGrpPolNo() + "'";
            //      strSQL += " AND RiskCode = '" + tLCGrpPolSchema.getRiskCode() + "'";
            //      strSQL += " AND IncomeType = '1' ORDER BY MakeDate, MakeTime";
            //
            //      SSRS ssRs = new ExeSQL().execSQL(strSQL);
            //
            //      if( ssRs.ErrorFlag || ssRs.MaxRow == 0 ) {
            //        System.out.println("获取暂交费数据失败");
            //        xmlDataList.setColValue("PayMoney", "0");
            //        fPayMoneySum += 0;
            //      } else {
            //        xmlDataList.setColValue("PayMoney", ssRs.GetText(1, 1));
            //        fPayMoneySum += Double.parseDouble(ssRs.GetText(1, 1));
            //      }
            String sql1 = "select PolNo from lcpol " + "where GrpPolNo='" +
                          tLCGrpPolSchema.getGrpPolNo() + "' " +
                          " and RowNum <=1";

            ExeSQL aExeSQL = new ExeSQL();
            SSRS aSSRS = aExeSQL.execSQL(sql1);

            if (aSSRS == null)
            {
                throw new Exception("找不到这个团体保单下面的个人保单");
            }

            LCPremDB tLCPremDB = new LCPremDB();
            tLCPremDB.setPolNo(aSSRS.GetText(1, 1));

            LCPremSet tLCPremSet = new LCPremSet();
            tLCPremSet = tLCPremDB.query();

            double GrpPrem = 0;
            double IndiPrem = 0;
            String sql = "";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();

            if (tLCPremSet.size() == 2)
            {
                //集体交费累计和
                sql = "select sum(prem) from lcprem where polno in (select polno from lcpol where grppolno='" +
                      tLCGrpPolSchema.getGrpPolNo() +
                      "') and trim(payplancode) like '%101'";
                tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                GrpPrem = Double.parseDouble(tSSRS.GetText(1, 1));

                //个人交费累计和
                sql = "select sum(prem) from lcprem where polno in (select polno from lcpol where grppolno='" +
                      tLCGrpPolSchema.getGrpPolNo() +
                      "') and trim(payplancode) like '%102'";
                tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                IndiPrem = Double.parseDouble(tSSRS.GetText(1, 1));
            }
            else
            {
                GrpPrem = tLCGrpPolSchema.getPrem();
                IndiPrem = 0;
            }

            xmlDataList.setColValue("ManageFeeRate",
                                    tLCGrpPolSchema.getManageFeeRate());
            xmlDataList.setColValue("Remark1", tLCGrpPolSchema.getRemark());
            xmlDataList.setColValue("GrpPrem", format(GrpPrem));
            xmlDataList.setColValue("Prem1", format(IndiPrem));
            xmlDataList.setColValue("PremSumChn",
                                    PubFun.getChnMoney(tLCGrpPolSchema.getPrem()));
            xmlDataList.setColValue("AcctPremSum",
                                    format(tLCGrpPolSchema.getPrem() * (1 -
                    tLCGrpPolSchema.getManageFeeRate())));

            dPremSum += tLCGrpPolSchema.getPrem();

            LDCodeDB tLDCodeDB = new LDCodeDB();

            tLDCodeDB.setCodeType("printtemplate");
            tLDCodeDB.setCode(tLCGrpPolSchema.getRiskCode());

            if (!tLDCodeDB.getInfo())
            {
                throw new Exception("没有找到该险种对应的打印模板");
            }

            // 团单模板为OtherSign数据中的第二个字符
            xmlDataList.setColValue("Template",
                                    tLDCodeDB.getOtherSign().substring(1, 1));

            // 加入主附险标志
            xmlDataList.setColValue("SubRiskFlag", tLMRiskAppDB.getSubRiskFlag());
            xmlDataList.insertRow(0);
        }

        // end of "for(int nIndex = 0; nIndex < tLCGrpPolSet.size(); nIndex ++)"
        xmlDataset.addDataObject(xmlDataList);

        xmlDataset.addDataObject(new XMLDataTag("InjuryGetFlag",
                                                strInjuryGetFlag));
        xmlDataset.addDataObject(new XMLDataTag("HospitalFlag", strHospitalFlag));

        xmlDataset.addDataObject(new XMLDataTag("PremSum", format(dPremSum)));

        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.PayMoneySum",
                                                format(dPayMoneySum)));
        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.PayMoneySum1",
                                                PubFun.getChnMoney(dPayMoneySum)));

        String strSQL =
                "SELECT TempFeeNo, Operator FROM LJTempFee WHERE OtherNoType = '1' AND OtherNo = '" +
                aLCGrpPolSet.get(1).getGrpPolNo() + "'";

        SSRS ssRs = new ExeSQL().execSQL(strSQL);

        if (ssRs.ErrorFlag || (ssRs.MaxRow == 0))
        {
            throw new Exception("找不到该团体保单的暂交费数据");
        }

        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.TempFeeNo",
                                                ssRs.GetText(1, 1)));
        xmlDataset.addDataObject(new XMLDataTag("LJTempFee.Handler",
                                                ssRs.GetText(1, 2)));
    }

    // 正常打印的取数流程
    private void getGrpPolDataSet(LCGrpPolSet aLCGrpPolSet) throws Exception
    {
        XMLDataset xmlDataset = mXMLDatasets.createDataset();

        // 第一个元素是主险团体保单
        LCGrpPolSchema tMainLCGrpPolSchema = aLCGrpPolSet.get(1);

        LCGrpPolSchema tLCGrpPolSchema = null;

        // 获取打印的模板名
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();

        tLMRiskFormDB.setRiskCode(tMainLCGrpPolSchema.getRiskCode());
        tLMRiskFormDB.setFormType("PG");

        if (!tLMRiskFormDB.getInfo())
        {
            throw new Exception("没有该险种保单的打印模板信息，险种" +
                                tMainLCGrpPolSchema.getRiskCode());
        }

        xmlDataset.setTemplate(tLMRiskFormDB.getFormName());

        // 签单机构取管理机构的前四位
        /*Lis5.3 upgrade set
         tMainLCGrpPolSchema.setSignCom(tMainLCGrpPolSchema.getManageCom()
                                                          .substring(0, 4));
         */
        // 设置团单的缴至日期，取团单下个人的缴至日期的最大值
        // 设置团单的特约信息，如果没有特约内容，打印“此栏空白”
        /*Lis5.3 upgrade get
                 if ((tMainLCGrpPolSchema.getGrpSpec() == null) ||
                tMainLCGrpPolSchema.getGrpSpec().equals("")) {
            tMainLCGrpPolSchema.setGrpSpec("此栏空白");
                 }
         */

        // 代理人姓名信息
        LAAgentDB tLAAgentDB = new LAAgentDB();

        tLAAgentDB.setAgentCode(tMainLCGrpPolSchema.getAgentCode());
        tLAAgentDB.getInfo();
        xmlDataset.addDataObject(new XMLDataTag("LCGrpPol.AgentName",
                                                tLAAgentDB.getName()));

        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select InsuYear,InsuYearFlag from LCPol " +
                     " where GrpPolNo='" + tMainLCGrpPolSchema.getGrpPolNo() +
                     "' " +
                     " and RowNum <=1";
        SSRS tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS == null)
        {
            throw new Exception("没有找到该团单下的个单信息");
        }

        String InsuYear = tSSRS.GetText(1, 1);
        String InsuYearFlag = tSSRS.GetText(1, 2);

        xmlDataset.addDataObject(new XMLDataTag("LCGrpPol.InsuredYear",
                                                InsuYear));
        xmlDataset.addDataObject(new XMLDataTag("LCGrpPol.InsuredYearFlag",
                                                InsuYearFlag));

        LCPolSchema tLCPolSchema = null;
        LDPersonDB tLDPersonDB = null;

        sql = "select min(PaytoDate) from LCPol " + "where GrpPolNo='" +
              tMainLCGrpPolSchema.getGrpPolNo() + "' ";
        tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS == null)
        {
            throw new Exception("没有找到该团单下的个单信息");
        }

        String PaytoDate = tSSRS.GetText(1, 1);

        tMainLCGrpPolSchema.setPaytoDate(PaytoDate);
        xmlDataset.addSchema(tMainLCGrpPolSchema);

        // 产生被保人清单
        genInsuredList(xmlDataset, aLCGrpPolSet);

        // Get risk information
        getRiskInfo(xmlDataset, aLCGrpPolSet);
        getScanPic(xmlDataset, tMainLCGrpPolSchema);
    }

    /**
     * 产生保单生效、失效日期数据
     * @param strValidDate 保单生效日期
     * @param stEndDate 保单失效日期
     * @return
     */
    private String genInsuPeriod(LCGrpPolSchema aLCGrpPolSchema) throws
            Exception
    {
        // 将最大的失效日期减一天
        String strSQL =
                "SELECT TO_DATE(MAX(EndDate)) - 1 FROM LCPol WHERE GrpPolNo = '" +
                aLCGrpPolSchema.getGrpPolNo() + "'";

        System.out.println(strSQL);

        SSRS ssRs = new ExeSQL().execSQL(strSQL);

        String strEndDate = ssRs.GetText(1, 1);

        if ((null == strEndDate) || strEndDate.equals(""))
        {
            throw new Exception("该团单下的个单没有保单失效日期");
        }

        String strRet = "自" + aLCGrpPolSchema.getCValiDate().substring(0, 4) +
                        "年" + aLCGrpPolSchema.getCValiDate().substring(5, 7) +
                        "月" +
                        aLCGrpPolSchema.getCValiDate().substring(8, 10) +
                        "日零时起至" +
                        strEndDate.substring(0, 4) + "年" +
                        strEndDate.substring(5, 7) +
                        "月" + strEndDate.substring(8, 10) + "日二十四时止";

        return strRet;
    }

    // 获取被保人清单
    private void genInsuredList(XMLDataset xmlDataset, LCGrpPolSet aLCGrpPolSet) throws
            Exception
    {
        LCGrpPolSchema tLCGrpPolSchema = aLCGrpPolSet.get(1);
        InnerFormater innerFormater = null;
        String szTemplateFile = "";

        if (tLCGrpPolSchema != null)
        {
            innerFormater = new InnerFormater(tLCGrpPolSchema.getRiskCode());
        }

        for (int nIndex = 0; nIndex < aLCGrpPolSet.size(); nIndex++)
        {
            tLCGrpPolSchema = aLCGrpPolSet.get(nIndex + 1);
            szTemplateFile = mszTemplatePath + "DM" +
                             tLCGrpPolSchema.getRiskCode() + ".xml";

            if (new File(szTemplateFile).exists() == false)
            {
                throw new Exception("缺少配置文件：" + szTemplateFile);
            }

            Hashtable hashData = new Hashtable();

            hashData.put("_GRPPOLNO", tLCGrpPolSchema.getGrpPolNo());

            XMLDataMine xmlDataMine = new XMLDataMine(new FileInputStream(
                    szTemplateFile), hashData);

            xmlDataMine.setDataFormater(innerFormater);

            xmlDataset.addDataObject(xmlDataMine);
        }
    }

    /**
     * 取得主险，附加险列表
     * @param strGrpPolNo
     * @return LCGrpPolSet 第一个元素是主险，其余的元素是附加险的
     * @throws Exception
     */
    private LCGrpPolSet getRiskList(String strGrpPolNo) throws Exception
    {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

        // 查询个人保单表，从中得到主附险关系
        String sql = "select MainPolNo,PrtNo from lcpol " + "where GrpPolNo='" +
                     strGrpPolNo + "' " + " and RowNum <=1";
        System.out.println("*********************" + sql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS == null)
        {
            throw new Exception("找不到这个团体保单下面的个人保单");
        }

        String strPolNo = tSSRS.GetText(1, 1);
        String strPrtNo = tSSRS.GetText(1, 2);
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(strPolNo);

        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            throw new Exception("查询个人保单表时出错");
        }

        // 通过主险所在的个单找到主险所在的团单
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tLCPolDB.getGrpPolNo());

        if (!tLCGrpPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
            throw new Exception("查询团体保单表时出错");
        }

        // 将主险团体保单放到返回结果中
        tLCGrpPolSet.add(tLCGrpPolDB.getSchema());

        // 记录下主险团体保单的保单号
        String strMainGrpPolNo = tLCGrpPolDB.getGrpPolNo();

        // 查询个人保单表得到所有的附加险的个人保单
        tLCPolDB = new LCPolDB();

        String strSQL = "SELECT * FROM LCPol" +
                        " WHERE PolNo IN ( SELECT MAX(PolNo) FROM LCPol" +
                        " WHERE PrtNo = '" + strPrtNo +
                        "' GROUP BY RiskCode ) ";

        tLCPolSet = tLCPolDB.executeQuery(strSQL);

        if (tLCPolDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            throw new Exception("查询个人保单表时出错");
        }

        for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
        {
            tLCPolSchema = tLCPolSet.get(nIndex + 1);

            // 如果团体保单号等于主险团体保单号
            if (tLCPolSchema.getGrpPolNo().equals(strMainGrpPolNo))
            {
                continue;
            }

            tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo(tLCPolSchema.getGrpPolNo());

            if (!tLCGrpPolDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                throw new Exception("查询团体保单表时出错");
            }

            tLCGrpPolSet.add(tLCGrpPolDB.getSchema());
        }

        return tLCGrpPolSet;
    }

    // 获取补打保单的数据
    private void getRePrintData() throws Exception
    {
        String strCurDate = PubFun.getCurrentDate();
        String strCurTime = PubFun.getCurrentTime();

        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = null;

        // 利用LCGrpPolSet来传递数据，其中包含的数据并不是LCGrpPolSet的数据
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

        for (int nIndex = 0; nIndex < mLCGrpPolSet.size(); nIndex++)
        {
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpPolSchema.setSchema(mLCGrpPolSet.get(nIndex + 1));

            ssrs = exeSQL.execSQL(
                    "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '" +
                    tLCGrpPolSchema.getGrpPolNo() + "'");

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                throw new Exception("获取打印数据失败");
            }

            if (ssrs.MaxRow < 1)
            {
                throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
            }
            /*Lis5.3 upgrade set
                         tLCGrpPolSchema.setPrintCount(ssrs.GetText(1, 1));
             */
            tLCGrpPolSchema.setModifyDate(strCurDate);
            tLCGrpPolSchema.setModifyTime(strCurTime);

            tLCGrpPolSet.add(tLCGrpPolSchema);
        }

        mResult.clear();
        mResult.add(tLCGrpPolSet);

        LCGrpPolF1PBLS tLCGrpPolF1PBLS = new LCGrpPolF1PBLS();

        if (!tLCGrpPolF1PBLS.submitData(mResult, "REPRINT"))
        {
            if (tLCGrpPolF1PBLS.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLCGrpPolF1PBLS.mErrors);
            }

            throw new Exception("保存数据失败");
        }

        // 取打印数据
        Connection conn = null;

        try
        {
            DOMBuilder domBuilder = new DOMBuilder();
            Element rootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            String tSQL = "";
            java.sql.Blob tBlob = null;
            COracleBlob tCOracleBlob = new COracleBlob();

            for (int nIndex = 0; nIndex < tLCGrpPolSet.size(); nIndex++)
            {
                LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(nIndex + 1);

                tSQL = " and MainPolNo = '" + tLCGrpPolSchema.getGrpPolNo() +
                       "'";
                tBlob = tCOracleBlob.SelectBlob("LCPolPrint", "PolInfo", tSQL,
                                                conn);

                if (tBlob == null)
                {
                    throw new Exception("找不到打印数据");
                }

                //BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
                Element ele = domBuilder.build(tBlob.getBinaryStream())
                              .getRootElement();
                ele = new Element("DATASET").setMixedContent(ele.
                        getMixedContent());
                rootElement.addContent(ele);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            XMLOutputter xmlOutputter = new XMLOutputter("  ", true, "UTF-8");
            xmlOutputter.output(rootElement, baos);

            mResult.clear();
            mResult.add(new ByteArrayInputStream(baos.toByteArray()));

            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
    }

    // 重打保单的取数流程
    private void getGrpPolDataSetEx(LCGrpPolSet aLCGrpPolSet) throws Exception
    {
        XMLDataset xmlDataset = mXMLDatasets.createDataset();

        // 取打印数据
        Connection conn = null;

        try
        {
            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            LCGrpPolSchema tLCGrpPolSchema = aLCGrpPolSet.get(1);
            COracleBlob tCOracleBlob = new COracleBlob();
            String tSQL = "";
            java.sql.Blob tBlob = null;
            tSQL = " and MainPolNo = '" + tLCGrpPolSchema.getGrpPolNo() + "'";
            tBlob = tCOracleBlob.SelectBlob("LCPolPrint", "PolInfo", tSQL, conn);

            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            //BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            XMLDataBlob xmlDataBlob = new XMLDataBlob(tBlob.getBinaryStream());
            xmlDataset.addDataObject(xmlDataBlob);

            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
    }

    /**
     * 2003-07-21 Kevin
     * 格式化浮点型数据
     * @param dValue
     * @return
     */
    private String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    // 嵌套类 InnerComparator
    private class InnerComparator implements Comparator
    {
        private String m_strMainRiskCode = "";

        public InnerComparator(String strMainRiskCode)
        {
            m_strMainRiskCode = strMainRiskCode;
        }

        public int compare(Object o1, Object o2)
        {
            Element ele1 = (Element) o1;
            Element ele2 = (Element) o2;

            //      // 先比较客户号
            //      String str1 = ele1.getChildTextTrim("COL0");
            //      String str2 = ele2.getChildTextTrim("COL0");
            // 先比较投保单号
            String str1 = ele1.getChildTextTrim("COL16");
            String str2 = ele2.getChildTextTrim("COL16");

            if (str1 != null)
            {
                if (!str1.equals(str2))
                {
                    return str1.compareTo(str2);
                }
            }

            // 再比较主险/附加险
            str1 = ele1.getChildTextTrim("COL6");
            str2 = ele2.getChildTextTrim("COL6");

            if (!str1.equals(str2))
            {
                if (str1.equals(m_strMainRiskCode))
                {
                    return -1;
                }

                if (str2.equals(m_strMainRiskCode))
                {
                    return 1;
                }

                return str1.compareTo(str2);
            }

            return 0;
        }

        public boolean equals(Object o)
        {
            return false;
        }
    }


    // 嵌套类 InnerFormater
    private class InnerFormater implements XMLDataFormater
    {
        private String m_strMainRiskCode = "";

        public InnerFormater(String strMainRiskCode)
        {
            m_strMainRiskCode = strMainRiskCode;
        }

        public boolean format(Element element)
        {
            List list = element.getChild("LCPol").getChildren("ROW");
            Collections.sort(list, new InnerComparator(m_strMainRiskCode));

            // 给投保人列表加上序号
            Iterator it = list.iterator();
            int nIndex = 0;

            while (it.hasNext())
            {
                Element eleRow = (Element) it.next();
                eleRow.getChild("COL15").setText(String.valueOf(nIndex++));
            }

            element.getChild("LCPol").setChildren(list);

            return true;
        }
    }
}
