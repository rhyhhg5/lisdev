package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class EdorAppFeeF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //private VData mInputData = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private String GetNoticeNo;
    private String AppntNo;
    private String Agentcode;
    private String SumDuePayMoney;
    public EdorAppFeeF1PUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        EdorAppFeeF1PBL tEdorAppFeeF1PBL = new EdorAppFeeF1PBL();
        System.out.println("Start EdorFeeF1P UI Submit ...");

        if (!tEdorAppFeeF1PBL.submitData(cInputData, cOperate))
        {
            if (tEdorAppFeeF1PBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tEdorAppFeeF1PBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "EdorFeeF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tEdorAppFeeF1PBL.getResult();
            return true;
        }
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorNo("86110020030420000096");
        String GetNoticeNo = "86110020030310000067";
        String AppntNo = "0000000214";
        String SumDuePayMoney = "50.0";
        GlobalInput tG = new GlobalInput();
        VData tVData = new VData();
        tVData.addElement(tLPEdorMainSchema);
        tVData.addElement(GetNoticeNo);
        tVData.addElement(AppntNo);
        tVData.addElement(SumDuePayMoney);
        tVData.addElement(tG);
        EdorFeeF1PUI tEdorFeeF1PUI = new EdorFeeF1PUI();
        tEdorFeeF1PUI.submitData(tVData, "PRINT");
    }
}
