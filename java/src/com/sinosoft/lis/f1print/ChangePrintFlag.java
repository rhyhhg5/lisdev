package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class ChangePrintFlag
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    /** 暂交费表 */
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public ChangePrintFlag()
    {}

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        if (!UpdateLOPRTManager())
        {
            return false;
        }
        System.out.println("---UpdateLOPRTManager---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLOPRTManagerSet = ((LOPRTManagerSet) cInputData.getObjectByObjectName(
                "LOPRTManagerSet", 0));

        if (mLOPRTManagerSet == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangePrintFlag";
            tError.functionName = "getInputData";
            tError.errorMessage = "请输入查询条件!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 查询暂交费表信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean UpdateLOPRTManager()
    {
        String strSQL =
                "update LOPRTManager set StateFlag='1' where PrtSeq in (";
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        for (int i = 1; i <= mLOPRTManagerSet.size(); i++)
        {
            strSQL = strSQL + "'" + mLOPRTManagerSet.get(i).getPrtSeq() + "'";
            if (i != mLOPRTManagerSet.size())
            {
                strSQL = strSQL + ",";
            }
        }
        strSQL = strSQL + ")";
        System.out.println("sql:" + strSQL);
        ExeSQL tExeSQL = new ExeSQL();
        if (tExeSQL.execUpdateSQL(strSQL) == false)
        {
            CError tError = new CError();
            tError.moduleName = "ChangePrintFlag";
            tError.functionName = "UpdateLOPRTManager";
            tError.errorMessage = "更新数据库失败:" + tExeSQL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

}
