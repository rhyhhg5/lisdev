package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;


public class PlPayMoneyDetailPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	public PlPayMoneyDetailPrintUI() {
	}

	public boolean submitData(TransferData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			PlPayMoneyDetailPrintBL tPlPayMoneyDetailPrintBL = new PlPayMoneyDetailPrintBL();
			System.out.println("Start tPlApprovalPrintUI Submit ...");
			if (!tPlPayMoneyDetailPrintBL.submitData(cInputData, cOperate)) {
				if (tPlPayMoneyDetailPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlPayMoneyDetailPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlPayMoneyDetailPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlPayMoneyDetailPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			buildError("submitData", "执行错误，原因是："+e.toString());
			return false;
		}
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlPayMoneyDetailPrintUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}




}
