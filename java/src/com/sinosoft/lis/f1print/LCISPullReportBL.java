package com.sinosoft.lis.f1print;

/**
 * <p>Title: LCISPullReportBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LCISPullReportBL {
    public LCISPullReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";

    private String moperator = "";

    private String mOperatorName = "";

    private String mBatchNo = "";

    private String mAgentComName = "";

    private String mType = "";

    private String mTypeName = "";

    private String mAgentCom = "";

    private String mStartDate = "2008-11-1";

    private String mEndDate = "";

    private double mSumActuMoney = 0;

    private double mClaimMoney = 0;

    private String mCount = "";

    private String mMoneyType = "";

    private String[][] mShowDataList = null;

    private XmlExport mXmlExport = null;

    private String[] mDataList = null;

    private ListTable mListTable = new ListTable();

    private String currentDate = PubFun.getCurrentDate();
    DecimalFormat tDF = new DecimalFormat("0.00");

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

        return true;
    }


    private boolean getDataList() {

        String tSQL = null;

        tSQL = "select a.otherno,b.codename,a.grpcontno,c.grpname,a.riskcode,a.sumactumoney,a.rate,a.claimmoney,a.makedate "
               + " from lcispayget a,ldcode b,lcgrpcont c where a.batchno = '" +
               mBatchNo +
               "' and a.othernotype = b.code and a.grpcontno=c.grpcontno "
               + " and b.codetype = 'CISTYPE' union "
               + "select a.otherno,b.codename,a.grpcontno,c.grpname,a.riskcode,a.sumactumoney,a.rate,a.claimmoney,a.makedate "
               + " from lcispayget a,ldcode b,lbgrpcont c where a.batchno = '" +
               mBatchNo +
               "' and a.othernotype = b.code and a.grpcontno=c.grpcontno "
               + " and b.codetype = 'CISTYPE' with ur ";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        String tDateSQL = "";

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            String Info[] = new String[11];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            Info[4] = tSSRS.GetText(i, 5);
            Info[5] = tSSRS.GetText(i, 6);
            String tRateSQl =
                    "select 1-sum(b.rate) from LCIGrpCont a,LCCoInsuranceParam b "
                    + " where a.grpcontno=b.grpcontno and a.grpcontno='" +
                    Info[2] + "' fetch first 1 rows only with ur";
            String tRate = tExeSQL.getOneValue(tRateSQl);
            Info[6] = tRate;
            double tMoney = Arith.round(Double.parseDouble(Info[5]) *
                                        Double.parseDouble(tRate), 2);
            Info[7] = tDF.format(tMoney);
            Info[8] = tSSRS.GetText(i, 7);
            Info[9] = tDF.format(Double.parseDouble(tSSRS.GetText(i, 8)));
            mEndDate = tSSRS.GetText(i, 9);
            mSumActuMoney += Double.parseDouble(Info[5]);
            mClaimMoney += Double.parseDouble(Info[9]);

            if ("01".equals(mType)) {
                tDateSQL = "select signdate from lcgrpcont where prtno='" +
                           Info[0] +
                           "' union select signdate from lbgrpcont where prtno='" +
                           Info[0] + "' fetch first 1 rows only with ur";
            } else if ("02".equals(mType)) {
                tDateSQL = "select confdate from ljapay where payno = '" +
                           Info[0] + "' fetch first 1 rows only with ur";
            } else if ("03".equals(mType)) {
                tDateSQL =
                        "select confdate from lpedorapp where edoracceptno = '" +
                        Info[0] + "' fetch first 1 rows only with ur";
            } else if ("04".equals(mType)) {
                tDateSQL = "select makedate from ljagetclaim where otherno='" +
                           Info[0] + "' fetch first 1 rows only with ur";
            }
            Info[10] = tExeSQL.getOneValue(tDateSQL);

            mListTable.add(Info);
        }

        tSQL = "select name from lacom where agentcom='" + mAgentCom +
               "' with ur";
        mAgentComName = tExeSQL.getOneValue(tSQL);

        if (mClaimMoney > 0) {
            mMoneyType = "支付";
        } else {
            mMoneyType = "收取";
        }

        if ("01".equals(mType)) {
            mTypeName = "新单";
        } else if ("02".equals(mType)) {
            mTypeName = "续期";
        } else if ("03".equals(mType)) {
            mTypeName = "保全";
        } else if ("04".equals(mType)) {
            mTypeName = "理赔";
        } else if ("05".equals(mType)) {
            mTypeName = "理赔";
        } else if ("06".equals(mType)) {
            mTypeName = "理赔";
        }

        tSQL = "select username from lduser where usercode='" + moperator +
               "' with ur";
        mOperatorName = tExeSQL.getOneValue(tSQL);

        tSQL = "select count(distinct otherno) from lcispayget where batchno='" +
               mBatchNo + "' with ur";
        mCount = tExeSQL.getOneValue(tSQL);

        String tOtherNoType = mType;
        if ("LP".equals(mType)) {
            tOtherNoType = "C','F','5";
        }

        tSQL =
                "select max(a.makedate)+1 days from lcispayget a where a.agentcom='" +
                mAgentCom +
                "' and a.makedate<(select makedate from lcispayget where batchno='" +
                mBatchNo + "' fetch first 1 rows only ) and a.batchno<>'" + mBatchNo +
                "' and a.othernotype in ('" + tOtherNoType +
                "') fetch first 1 rows only with ur";
        mStartDate = tExeSQL.getOneValue(tSQL);
        if ("".equals(mStartDate) || "null".equals(mStartDate)) {
            mStartDate = "2008-11-1";
        }

        return true;
    }

    /**
     * 进行数据查询
     *
     * @return boolean
     */
    private boolean queryData() {

        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LCISPullReport.vts", "printer");
        System.out.print("dayin252");

        String[] title = {"", "", "", "", "", "", "", "", "", "", ""};
        if (!getDataList()) {
            return false;
        }

        tTextTag.add("AgentComName", mAgentComName);
        tTextTag.add("AgentCom", mAgentCom);
        tTextTag.add("BatchNo", mBatchNo);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("TypeName", mTypeName);
        tTextTag.add("SumActuMoney", tDF.format(Arith.round(mSumActuMoney, 2)));
        tTextTag.add("ClaimMoney",
                     tDF.format(Math.abs(Arith.round(mClaimMoney, 2))));
        tTextTag.add("MoneyCHN",
                     PubFun.getChnMoney(Math.abs(Arith.round(mClaimMoney, 2))));
        tTextTag.add("Operator", mOperatorName);
        tTextTag.add("MakeDate", currentDate);
        tTextTag.add("Count", mCount);
        tTextTag.add("MoneyType", mMoneyType);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     *
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        // 全局变量
        // mGlobalInput.setSchema((GlobalInput)
        // pmInputData.getObjectByObjectName(
        // "GlobalInput", 0));
        mManageCom = (String) pmInputData.get(0);
        moperator = (String) pmInputData.get(1);
        mBatchNo = (String) pmInputData.get(2);
        mType = (String) pmInputData.get(3);
        mAgentCom = (String) pmInputData.get(4);

        return true;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCISPullReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
