package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys
 * @version 1.0
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GrpRiskPreBudgetExcelUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GrpRiskPreBudgetExcelUI()
    {
    }
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            GrpRiskPreBudgetExcelBL tGrpRiskPreBudgetExcelBL = new GrpRiskPreBudgetExcelBL();
            System.out.println("Start GrpRiskPreReceiveExcelUI Submit ...");

            if (!tGrpRiskPreBudgetExcelBL.submitData(cInputData, cOperate))
            {
                if (tGrpRiskPreBudgetExcelBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tGrpRiskPreBudgetExcelBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","GrpRiskPreBudgetExcelBL，但是没有提供详细的出错信息");
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "操作失败！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "GrpRiskPreBudgetExcelUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}