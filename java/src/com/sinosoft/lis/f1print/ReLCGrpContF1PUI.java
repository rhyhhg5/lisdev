/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LCContRePrtLogSchema;

/*
 * <p>ClassName: LCGrpContF1PUI </p>
 * <p>Description: LCGrpContF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class ReLCGrpContF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCContRePrtLogSchema mLCContRePrtLogSchema = new LCContRePrtLogSchema();

    public ReLCGrpContF1PUI()
    {
    }

    /**
                  传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            ReLCGrpContF1PBL tReLCGrpContF1PBL = new ReLCGrpContF1PBL();
            System.out.println("Start ReLCGrpContF1P UI Submit ...");

            if (!tReLCGrpContF1PBL.submitData(vData, cOperate))
            {
                if (tReLCGrpContF1PBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tReLCGrpContF1PBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData",
                               "tReLCGrpContF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tReLCGrpContF1PBL.getResult();
                return true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    public static void main(String[] args)
    {
        ReLCGrpContF1PUI tReLCGrpContF1PUI = new ReLCGrpContF1PUI();
        LCGrpContSet tLCPolSet = new LCGrpContSet();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

        tLCGrpContSchema.setGrpContNo("240110000000028");
        tLCPolSet.add(tLCGrpContSchema);

        VData vData = new VData();

        vData.addElement(new GlobalInput());
        vData.addElement(tLCPolSet);

        if (!tReLCGrpContF1PUI.submitData(vData, "PRINT"))
        {
            System.out.println("fail to get print data");
        }
        else
        {
            vData = tReLCGrpContF1PUI.getResult();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCGrpContSet);
            vData.add(mLCContRePrtLogSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCGrpContSet.set((LCGrpContSet) cInputData.getObjectByObjectName(
                "LCGrpContSet",
                0));
        mLCContRePrtLogSchema.setSchema((LCContRePrtLogSchema) cInputData.
                                        getObjectByObjectName(
                                                "LCContRePrtLogSchema",
                                                0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
