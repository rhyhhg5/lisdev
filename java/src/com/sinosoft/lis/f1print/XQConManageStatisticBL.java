package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class XQConManageStatisticBL {
    public XQConManageStatisticBL() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

//    LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();

    LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */

    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            bulidErrors("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            bulidErrors("submitData", "没有符合条件的数据");
            return false;
        }

        return true;
    }

    /**
     * 错误信息
     * @param cFunction String
     * @param cErrorsMsg String
     */
    private void bulidErrors(String cFunction, String cErrorsMsg) {

        CError cError = new CError();

        cError.moduleName = "XQConManageStatisticBL";
        cError.functionName = cFunction;
        cError.errorMessage = cErrorsMsg;
        this.mErrors.addOneError(cError);

    }

    /**
     * 往后台传递参数
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
            mLJSPayBSchema.setSchema((LJSPayBSchema) cInputData.
                                     getObjectByObjectName("LJSPayBSchema", 0));
            mLJAPayPersonSchema.setSchema((LJAPayPersonSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LJAPayPersonSchema", 0));
            if (mLJSPayBSchema == null) {
                this.bulidErrors("getInputData", "缺少信息");
                return false;
            }

            if (mLJAPayPersonSchema == null) {
                this.bulidErrors("getInputData", "缺少信息");
                return false;
            }
        } catch (Exception e) {

            this.mErrors.addOneError("数据不完整！");

            return false;
        }
        return true;
    }

    /**
     * 需要打印的数据
     * @return boolean
     */
    private boolean getPrintData() {
        ListTable tRiskListTable = new ListTable();
        ListTable tInfoAllTable = new ListTable();
        String sManageCom = "";
        String sMangComBeiJing = "8611";
        String[] RiskInfoTitle = new String[14];
        sManageCom = mLJSPayBSchema.getManageCom();

        System.out.println("wwwwwwwwwwwwwwwwwwwww" + sManageCom);

        tInfoAllTable.setName("MAIN"); //对应模版投保信息部分的行对象名
        RiskInfoTitle[0] = "ManageCom"; //机构
        RiskInfoTitle[1] = "RiskName"; //险种名称
        RiskInfoTitle[2] = "ShouldPieceOne"; //13月应收件数
        RiskInfoTitle[3] = "ShouldExesOne"; //13月应收保费
        RiskInfoTitle[4] = "FactPieceOne"; //13月实收件数
        RiskInfoTitle[5] = "FactExesOne"; //13月实收保费
        RiskInfoTitle[6] = "PieceOne"; //13月继续率（件数）
        RiskInfoTitle[7] = "ExesOne"; //13月继续率（保费）
        RiskInfoTitle[8] = "ShouldPieceTwo"; //25月应收件数
        RiskInfoTitle[9] = "ShouldExesTwo"; //25月应收保费
        RiskInfoTitle[10] = "FactPieceTwo"; //25月实收件数
        RiskInfoTitle[11] = "FactExesTwo"; //25月实收保费
        RiskInfoTitle[12] = "PieceTwo"; //25月继续率（件数）
        RiskInfoTitle[13] = "ExesTwo"; //25月继续率（保费）

        /**全部信息*/
        tRiskListTable = this.inputData(tRiskListTable, sManageCom);

        /**北京信息*/
        tInfoAllTable = this.inputData(tRiskListTable, sMangComBeiJing);

        if (tInfoAllTable.size() < 1) {
            return false;
        }

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例

        xmlexport.createDocument("XQConYearReport.vts", "printer"); //最好紧接着就初始化xml文档

        xmlexport.addListTable(tInfoAllTable, RiskInfoTitle);

        xmlexport.outputDocumentToFile("c:\\", "Test11"); //输出xml文档到文件

        mResult.clear();

        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 查询数据
     * @param cSQL StringBuffer
     * @return SSRS
     */
    private SSRS exeSql(StringBuffer cSQL) {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(cSQL.toString());

        if (tExeSQL.mErrors.needDealError()) {
            CError tCError = new CError();

            tCError.moduleName = "XQConManageStatisticBL";
            tCError.functionName = "exeSql";
            tCError.errorMessage = "查询数据出现错误！";
            this.mErrors.addOneError(tCError);
            tSSRS = null;
        }
        return tSSRS;

    }

    /**
     * 将数据存放在ListTable中
     * @param cTable ListTable
     * @param cMangCom String
     * @return ListTable
     */
    private ListTable inputData(ListTable cTable, String cMangCom) {
        StringBuffer sqlOne = new StringBuffer(); //查询语句
        StringBuffer sqlTwo = new StringBuffer(); //查询语句
        StringBuffer sqlThree = new StringBuffer(); //查询语句
        StringBuffer sqlFour = new StringBuffer(); //查询语句

        cTable.setName("MAIN");

        int countShouldPieceOne = 0; //13月应收件数总计
        double countShouldExesOne = 0; //13月应收费用总计
        int countShouldPieceTwo = 0; //25月应收件数总计
        double countShouldExesTwo = 0; //25月应收费用总计
        int countFactPieceOne = 0; //13月实收件数总计
        double countFactExesOne = 0; //13月实收费用总计
        int countFactPieceTwo = 0; //25月实收件数总计
        double countFactExesTwo = 0; //25月实收费用总计

        SSRS tSSRSOne = new SSRS(); //查询结果
        SSRS tSSRSTwo = new SSRS(); //查询结果
        SSRS tSSRSThree = new SSRS(); //查询结果
        SSRS tSSRSFour = new SSRS(); //查询结果

        sqlOne.append("SELECT distinct(b.riskcode),a.managecom,");
        sqlOne.append("XQSTATISTIC(a.managecom,b.riskcode,'Y','M','C'),");
        sqlOne.append("XQSTATISTIC(a.managecom,b.riskcode,'Y','M','S')");
//        sqlOne.append("XQSTATISTIC(a.managecom,b.riskcode,'S','M','C'),");
//        sqlOne.append("XQSTATISTIC(a.managecom,b.riskcode,'S','M','S')");
        sqlOne.append(" FROM ljspayb a,ljspayperson b WHERE a.managecom like '" +
                      cMangCom + "%'");

        sqlThree.append("SELECT distinct(b.riskcode),a.managecom,");
        sqlThree.append("XQSTATISTIC(a.managecom,b.riskcode,'S','M','C'),");
        sqlThree.append("XQSTATISTIC(a.managecom,b.riskcode,'S','M','S')");
        sqlThree.append(
                " FROM ljspayb a,ljspayperson b WHERE a.managecom like '" +
                cMangCom + "%'");

        System.out.println(sqlOne.toString());

        if (sqlOne.toString().equals("")) {
            this.bulidErrors("getPrintData", "没有数据！");

        }
        /**查询13月应收数的结果*/
        tSSRSOne = this.exeSql(sqlOne);
        tSSRSThree = this.exeSql(sqlThree);

        if (tSSRSOne == null) {
            return cTable;
        }

        if (tSSRSThree == null) {
            return cTable;
        }

        sqlTwo.append("SELECT distinct(b.riskcode),a.managecom,");
        sqlTwo.append(" XQSTATISTIC(a.managecom,b.riskcode,'Y','L','C'),");
        sqlTwo.append("XQSTATISTIC(a.managecom,b.riskcode,'Y','L','S')");
//        sqlTwo.append("XQSTATISTIC(a.managecom,b.riskcode,'S','L','C'),");
//        sqlTwo.append("XQSTATISTIC(a.managecom,b.riskcode,'S','L','S')");
        sqlTwo.append(" FROM ljspayb a,ljspayperson b WHERE a.managecom like '" +
                      cMangCom + "%'");
        System.out.println(sqlTwo.toString());

        sqlFour.append("SELECT distinct(b.riskcode),a.managecom,");
//        sqlFour.append(" XQSTATISTIC(a.managecom,b.riskcode,'Y','L','C'),");
//        sqlFour.append("XQSTATISTIC(a.managecom,b.riskcode,'Y','L','S'),");
        sqlFour.append("XQSTATISTIC(a.managecom,b.riskcode,'S','L','C'),");
        sqlFour.append("XQSTATISTIC(a.managecom,b.riskcode,'S','L','S')");
        sqlFour.append(" FROM ljspayb a,ljspayperson b WHERE a.managecom like '" +
                       cMangCom + "%'");

        if (sqlTwo.toString().equals("")) {
            this.bulidErrors("getPrintData", "没有数据！");

        }
        /**查询13月实收数的结果*/
        tSSRSTwo = this.exeSql(sqlTwo);
        tSSRSFour = this.exeSql(sqlFour);
        if (tSSRSTwo == null) {
            return cTable;
        }

        if (tSSRSFour == null) {
            return cTable;
        }

        System.out.println("===============" + tSSRSOne.MaxRow);
        System.out.println("===============" + tSSRSTwo.MaxRow);

        for (int i = 1; i <= tSSRSOne.MaxRow; i++) {
            String[] Info = new String[14];
            System.out.println("888888888888888888888888888888888");
            System.out.println(i);
            Info[0] = tSSRSOne.GetText(i, 2); //机构
            System.out.println(Info[0]);
            Info[1] = tSSRSOne.GetText(i, 1); //险种
            System.out.println(Info[1]);

            Info[2] = tSSRSOne.GetText(i, 3); //13月应收件数
            Info[3] = tSSRSOne.GetText(i, 4); //13月应收保费
            Info[4] = tSSRSThree.GetText(i, 3); //13月实收件数
            Info[5] = tSSRSThree.GetText(i, 4); //13月实收报费

            double dValues;
            double dValue;
            double d;
            dValues = Double.parseDouble(tSSRSThree.GetText(i, 3));
            dValue = Double.parseDouble(tSSRSOne.GetText(i, 3));
            System.out.println("===========" + dValues);
            System.out.println("===========" + dValue);

            //13月实收率（件数）

            if (tSSRSOne.GetText(i, 3).equals("0")) {
                Info[6] = "0";
            } else {
                d = dValues / dValue;
                Info[6] = String.valueOf(d);
            }

            dValues = Double.parseDouble(tSSRSThree.GetText(i, 4));
            dValue = Double.parseDouble(tSSRSOne.GetText(i, 4));
            // 13月实收率（保费）
            if (tSSRSOne.GetText(i, 4).equals("0")) {
                Info[7] = "0";
            } else {
                d = dValues / dValue;
                Info[7] = String.valueOf(d);
            }

            Info[8] = tSSRSTwo.GetText(i, 3); //25月应收件数
            Info[9] = tSSRSTwo.GetText(i, 4); //25月应收保费
            Info[10] = tSSRSFour.GetText(i, 3); //25月实收件数
            Info[11] = tSSRSFour.GetText(i, 4); //25月实收报费

            dValues = Double.parseDouble(tSSRSFour.GetText(i, 3));
            dValue = Double.parseDouble(tSSRSTwo.GetText(i, 3));

            if (tSSRSTwo.GetText(i, 3).equals("0")) {
                Info[12] = "0";
            } else {
                d = dValues / dValue;
                Info[12] = String.valueOf(d);
            }

            dValues = Double.parseDouble(tSSRSFour.GetText(i, 4));
            dValue = Double.parseDouble(tSSRSTwo.GetText(i, 4));

            if (tSSRSTwo.GetText(i, 4).equals("0")) {
                Info[13] = "0";
            } else {
                d = dValues / dValue;
                Info[13] = String.valueOf(d);
            }

            cTable.add(Info);

            //总计
            countShouldPieceOne += Integer.parseInt(tSSRSOne.GetText(i, 3)); //13月应收件数总计
            countShouldExesOne += Double.parseDouble(tSSRSOne.GetText(i, 4)); //13月应收费用总计
            countShouldPieceTwo += Integer.parseInt(tSSRSTwo.GetText(i, 3)); //25月应收件数总计
            countShouldExesTwo += Double.parseDouble(tSSRSTwo.GetText(i, 4)); //25月应收费用总计
            countFactPieceOne += Integer.parseInt(tSSRSThree.GetText(i, 3)); //13月实收件数总计
            countFactExesOne += Double.parseDouble(tSSRSThree.GetText(i, 4)); //13月实收费用总计
            countFactPieceTwo += Integer.parseInt(tSSRSFour.GetText(i, 3)); //25月实收件数总计
            countFactExesTwo += Double.parseDouble(tSSRSFour.GetText(i, 4)); //25月实收费用总计

            if (i == tSSRSOne.MaxRow) {
                String[] InfoCount = new String[14];

                /**总计数据*/
                InfoCount[0] = "";
                InfoCount[1] = "小计";
                InfoCount[2] = String.valueOf(countShouldPieceOne);
                InfoCount[3] = String.valueOf(countShouldExesOne);
                InfoCount[4] = String.valueOf(countFactPieceOne);
                InfoCount[5] = String.valueOf(countFactExesOne);

                if (countShouldPieceOne == 0) {
                    InfoCount[6] = "0";
                } else {
                    InfoCount[6] = String.valueOf(countFactPieceOne /
                                                  countShouldPieceOne);
                }

                if (countShouldExesOne == 0) {
                    InfoCount[7] = "0";
                } else {
                    InfoCount[7] = String.valueOf(countFactExesOne /
                                                  countShouldExesOne);
                }

                InfoCount[8] = String.valueOf(countShouldPieceTwo);
                InfoCount[9] = String.valueOf(countShouldExesTwo);
                InfoCount[10] = String.valueOf(countFactPieceTwo);
                InfoCount[11] = String.valueOf(countFactExesTwo);

                if (countShouldPieceTwo == 0) {
                    InfoCount[12] = "0";
                } else {
                    InfoCount[12] = String.valueOf(countFactPieceTwo /
                            countShouldPieceTwo);
                }

                if (countShouldExesTwo == 0) {
                    InfoCount[13] = "0";
                } else {
                    InfoCount[13] = String.valueOf(countFactExesTwo /
                            countShouldExesTwo);
                }

                System.out.println("66666666666666666666666666666666");
                cTable.add(InfoCount);
            }

        }

        return cTable;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInp = new GlobalInput();

        VData mResul = new VData();

        LJSPayBSchema mLJSPayBSche = new LJSPayBSchema();

        LJAPayPersonSchema mLJAPayPersonSche = new LJAPayPersonSchema();

        mLJSPayBSche.setManageCom("86");

        mResul.add(mGlobalInp);
        mResul.add(mLJSPayBSche);
        mResul.add(mLJAPayPersonSche);

        XQConManageStatisticBL tXQConManageStatisticBL = new
                XQConManageStatisticBL();

        if (tXQConManageStatisticBL.submitData(mResul, "PRINT")) {
            System.out.println("11111");
        }

    }

}
