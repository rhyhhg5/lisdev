package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;

import java.math.BigDecimal;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LACustomNumBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mAgentGroup ="";
    private String mManageCom = "";
    private String mAgentCode = "";
    private String mGroupAgentCode = "";
    private String mBranchType = "";
    private String mBranchType2= "";
    private String sumManagecom = "";
    private String sumManageName = "";
    private String sumBranchattr = "";
    private String sumBranchName = "";
    private String sumAgentNum = "";
    private double sumCount = 0.0;
    private String sumCustomNum = ""; 
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LACustomNumBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";

        LACustomNumBL tLACustomNumBL = new LACustomNumBL();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
      try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
            this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
            this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
            this.mEndDate = (String)mTransferData.getValueByName("tEndDate");
            this.mAgentCode  = (String)mTransferData.getValueByName("tAgentCode");
            this.mGroupAgentCode  = (String)mTransferData.getValueByName("tGroupAgentCode");
            this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
            this.mBranchType = (String)mTransferData.getValueByName("tBranchType");
            this.mBranchType2 = (String)mTransferData.getValueByName("tBranchType2");
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {


      return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LACustomNum.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        System.out.println("121212121212121212121212121212" + tMakeDate);

   

        String[] title = {"", "", "", "", "" ,"","","",""};

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("sumManagecom", sumManagecom);
        tTextTag.add("sumManageName", sumManageName);
        tTextTag.add("sumBranchattr", sumBranchattr);
        tTextTag.add("sumBranchName", sumBranchName);
        tTextTag.add("sumAgentNum", sumAgentNum);
        tTextTag.add("sumCustomNum", sumCustomNum);     
        mXmlExport.addTextTag(tTextTag);
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();
        mResult.addElement(mXmlExport);
        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable()
    {
    	
    	int n = 1;
    	//根据管理机构查询   	
    	if (this.mBranchAttr==null||mBranchAttr.equals("")
    					&& (this.mAgentCode==null||mAgentCode.equals("")))
    	{//查询销售机构下的团队
    		String Sql = "";
    		if(this.mManageCom!=null&&!this.mManageCom.equals(""))   		
    			Sql= "select  * from ldcom where comcode like '"+mManageCom+"%' and length(trim(comcode))=8";
    		else
    			Sql = "select  * from ldcom where comcode like '86%' and length(trim(comcode))=8";
    		System.out.println(Sql);
    		LDComSet tLDComSet = new LDComSet();
    		LDComDB tLDComDB = new LDComDB();
    		tLDComSet = tLDComDB.executeQuery(Sql);
    		if(tLDComSet.size()<1)
    		{
    			CError.buildErr(this, "没有满足条件的机构！");
    			return false;
    		}
    		else//机构循环
    		{   			
    			for(int i=1;i<=tLDComSet.size();i++)
    			{
    				LDComSchema tLDComSchema = new LDComSchema();
    				tLDComSchema = tLDComSet.get(i);
    				String tSQL = "select * from labranchgroup where managecom='"+tLDComSchema.getComCode()+"' and branchtype='2'"
                     + " and branchtype2='01' and endflag ='N' order by agentgroup ";
    				System.out.println(tSQL);
    				LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
    				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    				tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
    				if(tLABranchGroupSet.size()>=1)//如果机构下没有团队就不用求
    				{
    					for(int j=1;j<=tLABranchGroupSet.size();j++)
    					{

    						String agentSQL ="select * from laagent where agentgroup='"+tLABranchGroupSet.get(j).getAgentGroup()+"'" +
    					 		" and branchtype='2' and branchtype2='01' order by agentcode"; 					 
    				     LAAgentSet tLAAgentSet = new LAAgentSet();
    				     LAAgentDB tLAAgentDB = new LAAgentDB();
    				     tLAAgentSet = tLAAgentDB.executeQuery(agentSQL);
    				     //循环求每一个人的客户数‘职级
    				     if(tLAAgentSet.size()>=1)
    				     {
    				    	
    			           for(int m=1;m<=tLAAgentSet.size();m++)
    			           {
    			        	 String tAgentGrade = "";
    				    	String GradeSQL = "select agentgrade from latree where agentcode='"+tLAAgentSet.get(m).getAgentCode()+"'";
    			            ExeSQL tExeSQL = new ExeSQL();
    			            tAgentGrade = tExeSQL.getOneValue(GradeSQL);
    				    	 String CustomSQL = "select " +
    				    	 		"(select count(distinct a.appntno) " +
    				    	 		"from   lacommision a where a.agentcode='"+tLAAgentSet.get(m).getAgentCode()+"'and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"'" +
    				    	 		"  and commdire='1' and a.branchtype='2' and a.branchtype2='01' " +
    				    	 		"and not  exists(select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno))" +
    				    	 		"+" +
    				    	 		"(select nvl(sum(a.k1),0) from   lacommision a where a.agentcode='"+tLAAgentSet.get(m).getAgentCode()+"'" +
    				    	 		" and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"' and commdire='1' and " +
    				    	 		"a.branchtype='2' and a.branchtype2='01' " +
    				    	 		"and exists (select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno)	) " +
    				    	 		"from dual";
    				    	 System.out.println(CustomSQL);
    				    	 ExeSQL ttExeSQL = new ExeSQL();
    				    	 String CustomNum = ttExeSQL.getOneValue(CustomSQL);
    				    	 //将每一条值放到临时数组中   				    	
    				    	 String ListInfo[] = new String[9];
     				    	 ListInfo[0] = String.valueOf(n);
    				    	 ListInfo[1] = tLDComSchema.getComCode();
    				    	 ListInfo[2] = tLDComSchema.getName();
    				    	 ListInfo[3] = tLABranchGroupSet.get(j).getBranchAttr();
    				    	 ListInfo[4] = tLABranchGroupSet.get(j).getName();
    				    	 ListInfo[5] = tLAAgentSet.get(m).getGroupAgentCode();
    				    	 ListInfo[6] = tLAAgentSet.get(m).getName();
    				    	 ListInfo[7] = tAgentGrade;
    				    	 ListInfo[8] = CustomNum;  
    				    	 mListTable.add(ListInfo);
    				    	 System.out.println(CustomNum);
    				    	 sumCount = sumCount + Double.parseDouble(CustomNum);
    				    	 n++;
    			           }
    				      }
    					}
    				}
    			}
    			
    		}
//    		处理合计行
    		sumManagecom = "";
    		sumManageName = "";
    	    sumBranchattr = "";
    	    sumBranchName = "";
    	    sumAgentNum  = String.valueOf(n-1);
    	    sumCustomNum = String.valueOf(sumCount);
    	}
    	//根据团队查询
    	else if(mBranchAttr!=null&&!this.mBranchAttr.equals("")&&
    			(this.mAgentCode==null||mAgentCode.equals("")))
    	{
    		//查询团队所在的机构
    		String manageSQL = "select comcode,name from ldcom where " +
    				" comcode = (select managecom from labranchgroup where agentgroup='"+this.mAgentGroup+"')";
    		ExeSQL kExeSQL = new ExeSQL();
    		SSRS kSSRS = new SSRS();
    		kSSRS = kExeSQL.execSQL(manageSQL);
    		if(kSSRS.getMaxRow()<=0)
    		{
    			CError.buildErr(this, "机构维护出错@");
    			return false;
    		}
    		//查询团队名称
    		String NameSQL = "select name from labranchgroup where agentgroup='"+this.mAgentGroup+"'";
    		String Name = kExeSQL.getOneValue(NameSQL);
    		//根据团队求人
    		String agentSQL ="select * from laagent where agentgroup='"+this.mAgentGroup+"'" +
	 		"  and branchtype='2' and branchtype2='01' order by agentcode"; 					 
		     LAAgentSet tLAAgentSet = new LAAgentSet();
		     LAAgentDB tLAAgentDB = new LAAgentDB();
		     tLAAgentSet = tLAAgentDB.executeQuery(agentSQL);
		     //循环求每一个人的客户数‘职级
		     for(int k=1;k<=tLAAgentSet.size();k++)
		     {
		    	 String tAgentGrade = "";
		     
			    	String Grade1SQL = "select agentgrade from latree where agentcode='"+tLAAgentSet.get(k).getAgentCode()+"'";
		            ExeSQL tExeSQL = new ExeSQL();
		            tAgentGrade = tExeSQL.getOneValue(Grade1SQL);
			    	 String CustomSQL = "select " +
			    	 		"(select count(distinct a.appntno) " +
			    	 		"from   lacommision a where a.agentcode='"+tLAAgentSet.get(k).getAgentCode()+"'and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"'" +
			    	 		"  and commdire='1' and a.branchtype='2' and a.branchtype2='01' " +
			    	 		"and not  exists(select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno))" +
			    	 		"+" +
			    	 		"(select nvl(sum(a.k1),0) from   lacommision a where a.agentcode='"+tLAAgentSet.get(k).getAgentCode()+"'" +
			    	 		" and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"' and commdire='1' and " +
			    	 		"a.branchtype='2' and a.branchtype2='01' " +
			    	 		"and exists (select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno)	) " +
			    	 		"from dual";
			    	 ExeSQL ttExeSQL = new ExeSQL();
			    	 String CustomNum = ttExeSQL.getOneValue(CustomSQL);
			    	 //将每一条值放到临时数组中   				    	
			    	 String ListInfo[] = new String[9];
			    	 ListInfo[0] = String.valueOf(n);
			    	 ListInfo[1] = kSSRS.GetText(1, 1);
			    	 ListInfo[2] = kSSRS.GetText(1, 2);
			    	 ListInfo[3] = this.mBranchAttr;
			    	 ListInfo[4] = Name;
			    	 ListInfo[5] = tLAAgentSet.get(k).getGroupAgentCode();
			    	 ListInfo[6] = tLAAgentSet.get(k).getName();
			    	 ListInfo[7] = tAgentGrade;
			    	 ListInfo[8] = CustomNum;  
			    	 sumCount = sumCount + Double.parseDouble(CustomNum);
			    	 mListTable.add(ListInfo);
			    	 sumManagecom = kSSRS.GetText(1, 1);
			    		sumManageName = kSSRS.GetText(1, 2);
			    	    sumBranchattr = this.mBranchAttr;
			    	    sumBranchName = Name;
			    	 n++;
		  
		     } 
//		   处理合计行    		
	    	    sumAgentNum = String.valueOf(n-1);
	    	    sumCustomNum = String.valueOf(sumCount);
      	}
        else if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
		 {
		     //查询业务员所在的机构、团队、职级
        	String allSQL = "select * from laagent where agentcode='"+this.mAgentCode+"' and branchtype='2' and branchtype2='01'";
        	LAAgentSet ttLAAgentSet = new LAAgentSet();
        	LAAgentDB ttLAAgentDB = new LAAgentDB();
        	ttLAAgentSet = ttLAAgentDB.executeQuery(allSQL);
        	if(ttLAAgentSet.size()<=0)
        	{
        		CError.buildErr(this, "查询业务员信息失败！");
        		return false;
        	}
        	String codeNameSQL = "select name from ldcom where comcode='"+ttLAAgentSet.get(1).getManageCom()+"'";
        	ExeSQL allExeSQL = new ExeSQL();
        	String CodeName = allExeSQL.getOneValue(codeNameSQL);
        	String agentgradeSQL = "select agentgrade from latree where agentcode='"+ttLAAgentSet.get(1).getAgentCode()+"'";
        	String AgentGrade = allExeSQL.getOneValue(agentgradeSQL);
        	String BranchattrSql = "select branchattr,name from labranchgroup where agentgroup='"+ttLAAgentSet.get(1).getAgentGroup()+"'";
        	SSRS allSSRS = new SSRS();
        	allSSRS = allExeSQL.execSQL(BranchattrSql);
        	if(allSSRS.getMaxRow()<=0)
        	{
        		CError.buildErr(this, "机构信息查询失败!");
        		return false;
        	}
        	//查询业务员的客户数
        	 String CustomSQL = "select " +
 	 		"(select count(distinct a.appntno) " +
 	 		"from   lacommision a where a.agentcode='"+this.mAgentCode+"'and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"'" +
 	 		"  and commdire='1' and a.branchtype='2' and a.branchtype2='01' " +
 	 		"and not  exists(select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno))" +
 	 		"+" +
 	 		"(select nvl(sum(a.k1),0) from   lacommision a where a.agentcode='"+this.mAgentCode+"'" +
 	 		" and a.tmakedate>='"+this.mStartDate+"' and a.tmakedate<='"+this.mEndDate+"' and commdire='1' and " +
 	 		"a.branchtype='2' and a.branchtype2='01' " +
 	 		"and exists (select *  from  lagrpcommisiondetail where grpcontno =a.grpcontno)	) " +
 	 		"from dual";
		 	 ExeSQL ttExeSQL = new ExeSQL();
		 	 String CustomNum = ttExeSQL.getOneValue(CustomSQL);
		 	 //将每一条值放到临时数组中   				    	
	    	 String ListInfo[] = new String[9];
	    	 ListInfo[0] = String.valueOf(n);
	    	 ListInfo[1] = ttLAAgentSet.get(1).getManageCom();
	    	 ListInfo[2] = CodeName;
	    	 ListInfo[3] = allSSRS.GetText(1, 1);
	    	 ListInfo[4] = allSSRS.GetText(1, 2);
	    	 ListInfo[5] = this.mGroupAgentCode;
	    	 ListInfo[6] = ttLAAgentSet.get(1).getName();
	    	 ListInfo[7] = AgentGrade;
	    	 ListInfo[8] = CustomNum;  
	    	 mListTable.add(ListInfo);
	    	 n++;
	    	 //求合计
	    		sumManagecom = ttLAAgentSet.get(1).getManageCom();
	    		sumManageName = CodeName;
	    	    sumBranchattr = allSSRS.GetText(1, 1);
	    	    sumBranchName = allSSRS.GetText(1, 2);
	    	    sumAgentNum = String.valueOf(n-1);
	    	    sumCustomNum = String.valueOf(CustomNum);
		 }     	
        mListTable.setName("NUM");

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LADimissionContBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
