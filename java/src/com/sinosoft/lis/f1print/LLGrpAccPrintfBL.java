package com.sinosoft.lis.f1print; 

import java.io.InputStream;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.RSWrapper;

public class LLGrpAccPrintfBL {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";
    
    private String mOperator = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mFileNameB = "";


    private String mSQL = "";

    private CSVFileWrite mCSVFileWrite = null;
    private String mFilePath = "";
    private String mFileName = "";
    
    public static void main(String[] args) {
    	LLCaseTraceQueryBL tLLCaseTraceQueryBL = new LLCaseTraceQueryBL();
    	TransferData tTransferData= new TransferData();
//    	传参
    	tTransferData.setNameAndValue("tManageCom","86940000");
    	tTransferData.setNameAndValue("tCaseNo","C9400140529000019");
    	tTransferData.setNameAndValue("tOperator","001");
    	tTransferData.setNameAndValue("tFileNameB","Test.cvs");

    	GlobalInput tG = new GlobalInput();
    	VData tVData = new VData();
    	tVData.addElement(tG);
    	tVData.addElement(tTransferData);
    	
    	tLLCaseTraceQueryBL.submitData(tVData, "PRINT");
	}
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("<-Go Into LLGrpAccPrintfBL->");

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("打印报表机构："
                           + this.mManageCom);
        
    
        // 进行数据查询
        if (!queryDataToCVS()){
            return false;
        } else {

        }

        System.out.println("结束查询");

        return true;
    }

    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLCasePolicy";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 获取前台传入的数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

    	System.out.println("<-Go Into getInputData()->");
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据
            mOperator = (String) mTransferData.getValueByName("tOperator");
            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
            mSQL = (String) mTransferData.getValueByName("LLAccSql");  
            System.out.println("lyc-figthing: " + mSQL);
            this.mManageCom = (String) mTransferData
                              .getValueByName("tManageCom");
            
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }
 
    private boolean queryDataToCVS()
    {
    	
      int datetype = -1;
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    	tLDSysVarDB.setSysVar("LPCSVREPORT");

    	if (!tLDSysVarDB.getInfo()) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	
    	mFilePath = tLDSysVarDB.getSysVarValue(); 
        //   	本地测试
//    	mFilePath="E:\\";
    	if (mFilePath == null || "".equals(mFilePath)) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	System.out.println("生成文件存放位置:"+mFilePath);
    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
    	String tDate = PubFun.getCurrentDate2();

    	mFileName = "LLGrpAccPrintfBL" + mGlobalInput.Operator + tDate + tTime;
    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
    
	    if (!getDataListToCVS(datetype))
	    {
	        return false;
	    }

        mCSVFileWrite.closeFile();

    	return true;
    }

    /**
     * 处理案件轨迹信息
     * @param datetype
     * @return
     */
    private boolean getDataListToCVS(int datetype)
    {
    	if (!getDataList()) {
            return false;
        }
    	
        return true;
    }
 
    /**
     * 传入案件号，生成该案件轨迹CVS
     * @param caseNo
     * @return
     */
    private boolean getDataList() {
    	
        //调用写出CVS文件的公共类
        String[][] tTitle = new String[3][];
    	tTitle[0] = new String[] { "","","","账户交易信息查询" };
    	tTitle[1] = new String[] {""};
    	tTitle[2] = new String[] { "序号","时间","业务类型","业务控制号","账户类型","收费金额","退费金额"};
    	String[] tContentType = {"String","String","String","String","String","String","String"};
    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
    		return false;
    	}
    	
    	 String sql = mSQL ;
		 System.out.println("lyc-fighting-SQL: " + sql);
		 	
		
		 ExeSQL tCaseTraceExeSQL = new ExeSQL();
		 SSRS tCaseTraceSSRS = new SSRS();
		 tCaseTraceSSRS = tCaseTraceExeSQL.execSQL(sql);   
		 try {
	         if (tCaseTraceSSRS != null || tCaseTraceSSRS.MaxRow > 0) {

	             String tContent[][] = new String[tCaseTraceSSRS.getMaxRow()][];
	             int j = 1;
	   
	             for (int i = 1; i <= tCaseTraceSSRS.getMaxRow(); i++) {
	                 String ListInfo[] = new String[7];
	                 
	                 for (int m = 0; m < ListInfo.length; m++) {
	                     ListInfo[m] = "";
	                 }	
	                 ListInfo[0] = String.valueOf(j++);  
	                 
	                 ListInfo[1] = tCaseTraceSSRS.GetText(i, 1);            
	                 
	                 ListInfo[2] = tCaseTraceSSRS.GetText(i, 2);
	               
	                 ListInfo[3] = tCaseTraceSSRS.GetText(i, 3);
	                
	                 ListInfo[4] = tCaseTraceSSRS.GetText(i, 4);
	            
	                 ListInfo[5] = tCaseTraceSSRS.GetText(i, 5);
	                
	                 ListInfo[6] = tCaseTraceSSRS.GetText(i, 6);

	                 tContent[i - 1] = ListInfo;
	             }	             
	             


	             mCSVFileWrite.addContent(tContent);
	             if (!mCSVFileWrite.writeFile()) {
	             	mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
	             	return false;
	             }
	         }else{
	         	String tContent[][] = new String[1][];
	         	String ListInfo[]={"","","","","","",""};	
				    ListInfo[0]="0";
				    ListInfo[1]="0";
				    ListInfo[2]="0";
				    ListInfo[3]="0";
				    ListInfo[4]="0";
				    ListInfo[5]="0";
				    ListInfo[6]="0";
				    
				    tContent[0]=ListInfo;
				    mCSVFileWrite.addContent(tContent);
				    if (!mCSVFileWrite.writeFile()) {
				    	mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
				    	return false;
				    }
	         }
	     
		 } catch (Exception ex) {
		
		 } finally {
			 
		 }
		 return true;
	}
	

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLGrpAccPrintfBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }

}
