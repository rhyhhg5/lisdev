package com.sinosoft.lis.f1print;

import java.sql.Connection;

import com.sinosoft.lis.db.LCPENoticeDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LCPENoticeSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class BodyCheckConfirmBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    public BodyCheckConfirmBLS()
    {
    }

    public static void main(String[] args)
    {
        BodyCheckConfirmBLS mBodyCheckConfirmBLS1 = new BodyCheckConfirmBLS();
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start BodyCheckConfirm BLS Submit...");

        tReturn = save(cInputData);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }

        System.out.println("End BodyCheckConfirm BLS Submit...");

        return tReturn;
    }

//保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BodyCheckConfirmBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

            //sxy-2003-09-22更新核保主表(更新体检标志update)
            System.out.println("Start 核保主表信息...");
            LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);
            LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
            tLCUWMasterSet = (LCUWMasterSet) mInputData.getObjectByObjectName(
                    "LCUWMasterSet", 0);
            if (tLCUWMasterSet != null)
            {
                for (int i = 1; i <= tLCUWMasterSet.size(); i++)
                {
                    tLCUWMasterSchema = tLCUWMasterSet.get(i);
                    if (tLCUWMasterSchema != null)
                    {
                        tLCUWMasterDB.setSchema(tLCUWMasterSchema);
                        if (!tLCUWMasterDB.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "BodyCheckConfirmBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "核保主表信息数据更新失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }
            }
            //sxy-2003-09-22更新核保主表(更新体检标志update)


            // 体检通知书(update)
            System.out.println("Start 体检通知书...");

            LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB(conn);
            LCPENoticeSchema tLCPENoticeSchema = (LCPENoticeSchema) mInputData.
                                                 getObjectByObjectName(
                    "LCPENoticeSchema", 0);
            //add by yt 2003-11-28,如果是补打的数据，则不用更新核保表
            if (tLCPENoticeSchema != null)
            {
                tLCPENoticeDB.setSchema(tLCPENoticeSchema);
                if (!tLCPENoticeDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "BodyCheckConfirmBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "体检通知书表数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            // 打印管理表
            System.out.println("Start 打印管理表...");
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
            tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                      getObjectByObjectName(
                    "LOPRTManagerSchema", 0));
            if (!tLOPRTManagerDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "BodyCheckConfirmBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "打印管理表数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BodyCheckConfirmBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }
        return tReturn;
    }
}