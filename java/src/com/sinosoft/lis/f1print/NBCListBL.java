package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author zy
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCAppntIndDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NBCListBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    public NBCListBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                "LCPolSchema", 0)); //销售渠道
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "NBCListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String SaleChnl = "";
        String tManagecom = "";
        SSRS tSSRS = new SSRS();
        if ((mLCPolSchema.getSaleChnl() != null) &&
            (!mLCPolSchema.getSaleChnl().equals("")))
        {
            SaleChnl = " and SaleChnl='" + mLCPolSchema.getSaleChnl() + "' ";
        }
        if ((mLCPolSchema.getManageCom() != null) &&
            (!mLCPolSchema.getManageCom().equals("")))
        {
            tManagecom = " and managecom like '" + mLCPolSchema.getManageCom() +
                         "%'";
            String sql = "select shortname from ldcom where comcode='" +
                         mLCPolSchema.getManageCom() + "'";
            ExeSQL mExeSQL = new ExeSQL();
            texttag.add("ManageCom", mExeSQL.getOneValue(sql));
        }
        else
        {
            tManagecom = " and managecom like '" + mGlobalInput.ManageCom +
                         "%'";
            String sql = "select shortname from ldcom where comcode='" +
                         mGlobalInput.ManageCom + "'";
            ExeSQL mExeSQL = new ExeSQL();
            texttag.add("ManageCom", mExeSQL.getOneValue(sql));
        }
        msql = "select RiskCode,SaleChnl,PolNo,AppntName,AgentCode,AppntNo,RiskVersion from LCPol where AppFlag='1' and SignDate>='" +
               mDay[0] + "' and SignDate<='" + mDay[1] +
               "' and GrpPolNo = '00000000000000000000' " + SaleChnl +
               tManagecom + " order by RiskCode";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("List");
        int n = 1;
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[14];
            strArr[0] = String.valueOf(n);
            n++;
            System.out.println(n);
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    LDCodeDB tLDCodeDB = new LDCodeDB();
                    tLDCodeDB.setCode(tSSRS.GetText(i, j));
                    tLDCodeDB.setCodeType("salechnl");
                    tLDCodeDB.getInfo();
                    strArr[j] = tLDCodeDB.getCodeName();
                }
                if (j == 3)
                {
                    strArr[j + 1] = tSSRS.GetText(i, j);
                    String usql = "select count(*) from lcspec where polno='" +
                                  tSSRS.GetText(i, j) +
                                  "' and SpecContent is not null";
                    ExeSQL uExeSQL = new ExeSQL();
                    String a = uExeSQL.getOneValue(usql);
                    if (a.equals("0"))
                    {
                        strArr[j + 10] = "";
                    }
                    else
                    {
                        strArr[j + 10] = "#";
                    }
                }
                if (j == 4)
                { //投保人
                    strArr[j + 1] = tSSRS.GetText(i, j);
                }
                if (j == 5)
                {
                    strArr[j + 7] = tSSRS.GetText(i, j);
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(tSSRS.GetText(i, j));
                    tLAAgentDB.getInfo();
                    strArr[j + 6] = tLAAgentDB.getName();
                }
                if (j == 6)
                {
                    LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
                    tLCAppntIndDB.setCustomerNo(tSSRS.GetText(i, j));
                    tLCAppntIndDB.setPolNo(tSSRS.GetText(i, 3));
                    tLCAppntIndDB.getInfo();
                    LDCodeDB tLDCodeDB = new LDCodeDB();
                    tLDCodeDB.setCodeType("sex");
                    tLDCodeDB.setCode(tLCAppntIndDB.getSex());
                    tLDCodeDB.getInfo();
                    strArr[j] = tLDCodeDB.getCodeName();
                    strArr[j + 1] = tLCAppntIndDB.getMobile();
                    strArr[j + 2] = tLCAppntIndDB.getPhone();
                    if (tLCAppntIndDB.getPostalAddress() != null)
                    {
                        strArr[j + 3] = tLCAppntIndDB.getPostalAddress();
                        strArr[j + 4] = tLCAppntIndDB.getZipCode();
                    }
                    else
                    {
                        strArr[j + 3] = tLCAppntIndDB.getHomeAddress();
                        strArr[j + 4] = tLCAppntIndDB.getHomeAddressCode();
                    }

                }
                if (j == 7)
                { //险种分类
                    LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                    tLMRiskAppDB.setRiskCode(tSSRS.GetText(i, j - 6));
                    tLMRiskAppDB.setRiskVer(tSSRS.GetText(i, j));
                    tLMRiskAppDB.getInfo();
                    LDCodeDB tLDCodeDB = new LDCodeDB();
                    tLDCodeDB.setCode(tLMRiskAppDB.getRiskType3());
                    tLDCodeDB.setCodeType("authorizeseries");
                    tLDCodeDB.getInfo();
                    strArr[j - 4] = tLDCodeDB.getCodeName();
                }
            }
            tlistTable.add(strArr);
        }
        strArr = new String[14];
        strArr[0] = "num";
        strArr[1] = "Riskcode";
        strArr[2] = "SaleChnl";
        strArr[3] = "RiskType";
        strArr[4] = "Polno";
        strArr[5] = "AppntName";
        strArr[6] = "Mobile";
        strArr[7] = "Phone";
        strArr[8] = "Address";
        strArr[9] = "Zip";
        strArr[10] = "Opeator";
        strArr[11] = "AggntNo";
        strArr[12] = "";
        strArr[13] = "";

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NBCList.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }
}