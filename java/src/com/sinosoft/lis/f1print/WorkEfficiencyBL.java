package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LDPersonSchema;

public class WorkEfficiencyBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();


    public WorkEfficiencyBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName(
                "OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String MakeDate = PubFun.getCurrentDate();

        /** 首先准备Sql */
        /** 由于sql太长，因此采取StingBuffer方式拼接Sql */
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;
        if (QYType.equals("1")) {
            if (Managecom.length() == 8) {
                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end,");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end,");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end ,");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end ,");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)  ");
                sql.append(" end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag<>'1'  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' ) and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' )) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.conttype='1' and a.CardFlag<>'1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append(" ) as X order by A with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end,");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end,");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end ,");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end ,");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)  ");
                sql.append(" end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag<>'1'  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append(" ) as X order by A with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第三个Sql语句~~~~~");

            } else {
                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end,");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end, ");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end,");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end, ");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)   ");
                sql.append(" end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag<>'1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) L, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "')*2 and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X order by A with ur ");
                /** 恐怖的Sql~~ 先将上面的Sql执行，将查询结果保存，等待最后填充报表处理 */
                /** 首先校验ssrs 是否返回null，如果返回null则不作后续操作。 */
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第三个Sql语句~~~~~");

                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end,");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end,");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end ,");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end ,");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)  ");
                sql.append(" end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag<>'1'  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  )) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.CardFlag<>'1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where  '" + Managecom +
                        "'='86' and Z.comcode='86000000' ");
                sql.append(" ) as X order by A with ur ");

                ssrs = tExeSQL.execSQL(sql.toString());
                /** To MengWT ： 下面这个方法一个比较优的方法，原理就是希望找到一个大容器来存储
                 * 查询的结果。查询的结果，每一条都将是一个数组，这样多个Sql执行完将会是一个数组集合
                 * 保存着全部Sql执行的结果，其效果将和使用Union的结果相同。同时还有一个优势，请 见程序282行 */
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end,");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end,");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end ,");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end ,");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)  ");
                sql.append(" end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag<>'1'  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  ) and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  )) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append(" ) as X order by A with ur ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = null;
                System.out.println("处理完成第四个Sql语句~~~~~");
            }

        } else if (QYType.equals("2")) {
            if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(" select A,B, ");
                sql.append(
                        "   case when char(E) is null and char(C) is null then '0' ");
                sql.append(
                        "       when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(
                        "       when char(C) is null then  varchar(E/(F+D)) ");
                sql.append("       else varchar((E+C)/(F+D)) end, ");
                sql.append(
                        "   case when char(L) is null and char(G) is null then '0' ");
                sql.append(
                        "       when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(
                        "       when char(G) is null then  varchar(L/(H+M)) ");
                sql.append("       else varchar((G+L)/(H+M)) end, ");
                sql.append("  case when N is null then '0' ");
                sql.append(
                        "       when  N is not null  then varchar(N/O)  end , ");
                sql.append("  case when P is null then '0' ");
                sql.append(
                        "       when  P is not null  then varchar(P/Q)  end, ");
                sql.append("  case when R is null then '0' ");
                sql.append(
                        "       when  R is not null  then varchar(R/S)  end ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                           "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append(
                        "   case when char(E) is null and char(C) is null then '0' ");
                sql.append(
                        "       when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(
                        "       when char(C) is null then  varchar(E/(F+D)) ");
                sql.append("       else varchar((E+C)/(F+D)) end, ");
                sql.append(
                        "   case when char(L) is null and char(G) is null then '0' ");
                sql.append(
                        "       when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(
                        "       when char(G) is null then  varchar(L/(H+M)) ");
                sql.append("       else varchar((G+L)/(H+M)) end, ");
                sql.append("  case when N is null then '0' ");
                sql.append(
                        "       when  N is not null  then varchar(N/O)  end , ");
                sql.append("  case when P is null then '0' ");
                sql.append(
                        "       when  P is not null  then varchar(P/Q)  end, ");
                sql.append("  case when R is null then '0' ");
                sql.append(
                        "       when  R is not null  then varchar(R/S)  end ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                           "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();

                System.out.println("处理完成第二个Sql语句~~~~~");

            } else {

                sql = new StringBuffer();
                sql.append(" select A,B, ");
                sql.append(
                        " case when char(E) is null and char(C) is null then '0' ");
                sql.append(" when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(" when char(C) is null then  varchar(E/(F+D)) ");
                sql.append(" else varchar((E+C)/(F+D)) end, ");
                sql.append(
                        " case when char(L) is null and char(G) is null then '0' ");
                sql.append(" when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(" when char(G) is null then  varchar(L/(H+M)) ");
                sql.append(" else varchar((G+L)/(H+M)) end, ");
                sql.append(" case when N is null then '0' ");
                sql.append(" when  N is not null  then varchar(N/O)  end , ");
                sql.append(" case when P is null then '0' ");
                sql.append(" when  P is not null  then varchar(P/Q)  end, ");
                sql.append(" case when R is null then '0' ");
                sql.append(" when  R is not null  then varchar(R/S)  end ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                           "')*2)=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "')*2 and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append(" ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();

                System.out.println("处理完成第一个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append(
                        "   case when char(E) is null and char(C) is null then '0' ");
                sql.append(
                        "       when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(
                        "       when char(C) is null then  varchar(E/(F+D)) ");
                sql.append("       else varchar((E+C)/(F+D)) end, ");
                sql.append(
                        "   case when char(L) is null and char(G) is null then '0' ");
                sql.append(
                        "       when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(
                        "       when char(G) is null then  varchar(L/(H+M)) ");
                sql.append("       else varchar((G+L)/(H+M)) end, ");
                sql.append("  case when N is null then '0' ");
                sql.append(
                        "       when  N is not null  then varchar(N/O)  end , ");
                sql.append("  case when P is null then '0' ");
                sql.append(
                        "       when  P is not null  then varchar(P/Q)  end, ");
                sql.append("  case when R is null then '0' ");
                sql.append(
                        "       when  R is not null  then varchar(R/S)  end ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                           "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(" from LDCom Z where     '" + Managecom +
                           "'='86' and Z.comcode='86000000' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成二个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append(
                        "   case when char(E) is null and char(C) is null then '0' ");
                sql.append(
                        "       when char(E) is null then  varchar(C/(F+D)) ");
                sql.append(
                        "       when char(C) is null then  varchar(E/(F+D)) ");
                sql.append("       else varchar((E+C)/(F+D)) end, ");
                sql.append(
                        "   case when char(L) is null and char(G) is null then '0' ");
                sql.append(
                        "       when char(L) is null then  varchar(G/(H+M)) ");
                sql.append(
                        "       when char(G) is null then  varchar(L/(H+M)) ");
                sql.append("       else varchar((G+L)/(H+M)) end, ");
                sql.append("  case when N is null then '0' ");
                sql.append(
                        "       when  N is not null  then varchar(N/O)  end , ");
                sql.append("  case when P is null then '0' ");
                sql.append(
                        "       when  P is not null  then varchar(P/Q)  end, ");
                sql.append("  case when R is null then '0' ");
                sql.append(
                        "       when  R is not null  then varchar(R/S)  end ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                           "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();

                System.out.println("处理完成第三个Sql语句~~~~~");
            }
        } else if (QYType.equals("3")) {
            if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(" select A,B, ");
                sql.append("  ctrim1((ctrim(C1)+ctrim(C)+ctrim(E1)+ctrim(E)),(ctrim(F)+ctrim(F1)+ctrim(D1)+ctrim(D))), ");
                sql.append("  ctrim1((ctrim(G1)+ctrim(G)+ctrim(L1)+ctrim(L)),(ctrim(H)+ctrim(H1)+ctrim(M1)+ctrim(M))), ");
                sql.append(
                        "  ctrim1((ctrim(N)+ctrim(N1)),(ctrim(O)+ctrim(O1))), ");
                sql.append(
                        "  ctrim1((ctrim(P)+ctrim(P1)),(ctrim(Q)+ctrim(Q1))), ");
                sql.append(
                        "  ctrim1((ctrim(R)+ctrim(R1)),(ctrim(S)+ctrim(S1))) ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                        "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C1, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D1, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1'  and substr( Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr( Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M1, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P1, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'  and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R1, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S1 ");
                sql.append(" ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append("  ctrim1((ctrim(C1)+ctrim(C)+ctrim(E1)+ctrim(E)),(ctrim(F)+ctrim(F1)+ctrim(D1)+ctrim(D))), ");
                sql.append("  ctrim1((ctrim(G1)+ctrim(G)+ctrim(L1)+ctrim(L)),(ctrim(H)+ctrim(H1)+ctrim(M1)+ctrim(M))), ");
                sql.append(
                        "  ctrim1((ctrim(N)+ctrim(N1)),(ctrim(O)+ctrim(O1))), ");
                sql.append(
                        "  ctrim1((ctrim(P)+ctrim(P1)),(ctrim(Q)+ctrim(Q1))), ");
                sql.append(
                        "  ctrim1((ctrim(R)+ctrim(R1)),(ctrim(S)+ctrim(S1))) ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                        "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C1, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D1, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1'  and substr( Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr( Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M1, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P1, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'  and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R1, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S1 ");
                sql.append(" ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else {
                sql.append(" select A,B, ");
                sql.append("  ctrim1((ctrim(C1)+ctrim(C)+ctrim(E1)+ctrim(E)),(ctrim(F)+ctrim(F1)+ctrim(D1)+ctrim(D))), ");
                sql.append("  ctrim1((ctrim(G1)+ctrim(G)+ctrim(L1)+ctrim(L)),(ctrim(H)+ctrim(H1)+ctrim(M1)+ctrim(M))), ");
                sql.append(
                        "  ctrim1((ctrim(N)+ctrim(N1)),(ctrim(O)+ctrim(O1))), ");
                sql.append(
                        "  ctrim1((ctrim(P)+ctrim(P1)),(ctrim(Q)+ctrim(Q1))), ");
                sql.append(
                        "  ctrim1((ctrim(R)+ctrim(R1)),(ctrim(S)+ctrim(S1))) ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                        "')*2)=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                        "')*2)=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  ) S, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) C1, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) D1, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1'  and substr( Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr( Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) F1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a. Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode ) G1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) H1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a. Managecom,1,length('" + Managecom +
                           "')*2)=Z.comcode) L1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M1, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) N1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                        "')*2)=Z.comcode and c.EnterAccDate is not null) O1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) P1, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) Q1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'  and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode ) R1, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "')*2)=Z.comcode  ) S1 ");
                sql.append(" ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "')*2 and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append("  ctrim1((ctrim(C1)+ctrim(C)+ctrim(E1)+ctrim(E)),(ctrim(F)+ctrim(F1)+ctrim(D1)+ctrim(D))), ");
                sql.append("  ctrim1((ctrim(G1)+ctrim(G)+ctrim(L1)+ctrim(L)),(ctrim(H)+ctrim(H1)+ctrim(M1)+ctrim(M))), ");
                sql.append(
                        "  ctrim1((ctrim(N)+ctrim(N1)),(ctrim(O)+ctrim(O1))), ");
                sql.append(
                        "  ctrim1((ctrim(P)+ctrim(P1)),(ctrim(Q)+ctrim(Q1))), ");
                sql.append(
                        "  ctrim1((ctrim(R)+ctrim(R1)),(ctrim(S)+ctrim(S1))) ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select Z.comcode A,Z.Name B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                        "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C1, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D1, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1'  and substr( Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr( Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M1, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P1, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'  and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R1, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S1 ");
                sql.append(" ");
                sql.append(" from LDCom Z where   '" + Managecom +
                           "'='86' and Z.comcode='86000000' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");
                sql.append(" select A,B, ");
                sql.append("  ctrim1((ctrim(C1)+ctrim(C)+ctrim(E1)+ctrim(E)),(ctrim(F)+ctrim(F1)+ctrim(D1)+ctrim(D))), ");
                sql.append("  ctrim1((ctrim(G1)+ctrim(G)+ctrim(L1)+ctrim(L)),(ctrim(H)+ctrim(H1)+ctrim(M1)+ctrim(M))), ");
                sql.append(
                        "  ctrim1((ctrim(N)+ctrim(N1)),(ctrim(O)+ctrim(O1))), ");
                sql.append(
                        "  ctrim1((ctrim(P)+ctrim(P1)),(ctrim(Q)+ctrim(Q1))), ");
                sql.append(
                        "  ctrim1((ctrim(R)+ctrim(R1)),(ctrim(S)+ctrim(S1))) ");
                sql.append(" from ");
                sql.append("  ( ");
                sql.append("  select '总计' A,'总计' B, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D, ");
                sql.append(" (select sum((case when (to_date(ReceiveDate)-to_date(PolApplyDate))<0 then 0 else (to_date(ReceiveDate)-to_date(PolApplyDate)) end )) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null  and substr(managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag is null and  prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1'   and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr(managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lcgrpcont a,ES_DOC_MAIN c where   c.DocCode=a.prtno and appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,ES_DOC_MAIN c where  c.DocCode=a.prtno  and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.ReceiveDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.ReceiveDate)) end)  from lcgrpcont a where a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  and a.prtno not in (select a.prtno from lcgrpcont a,ES_DOC_MAIN c where c.DocCode=a.prtno and  a.appflag='1' and  a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' ))  L, ");
                sql.append(" (select count(prtno) from lcgrpcont  where appflag='1'  and  signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag is null and substr(managecom,1,length('" +
                           Managecom +
                        "'))=Z.comcode and  prtno not in (select c.DocCode from ES_DOC_MAIN c)) M, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lcgrpcont a,LJTempFee c where   c.OtherNo=a.grpcontno   and a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where  c.OtherNo=a.grpcontno  and  a.appflag='1' and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a.managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.CardFlag is null and a.appflag='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a,LJTempFee c where c.OtherNo=a.grpcontno   and a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lcgrpcont a where  a.appflag='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and a.CustomGetPolDate is not null and a.CardFlag is null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R, ");
                sql.append(" (select count(a.prtno) from lcgrpcont a where  a.appflag='1'  and a.CardFlag is null and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'   and a.CustomGetPolDate is not null and substr(a.managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S, ");
                sql.append(" (select sum((case when (to_date(b.makedate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(b.makedate)-to_date(a.PolApplyDate)) end )) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and a.conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) C1, ");
                sql.append(" (select count(a.prtno) from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) D1, ");
                sql.append(" (select sum((case when (to_date(InputDate)-to_date(PolApplyDate))<0 then 0 else (to_date(InputDate)-to_date(PolApplyDate)) end )) from lccont  where appflag='1' and   conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1'  and substr( Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "')) E1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate + "' and CardFlag<>'1' and  prtno not in (select a.prtno from lccont a,ES_DOC_MAIN b where b.DocCode=a.prtno and a.appflag='1' and conttype='1'  and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "')and substr( Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) F1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(c.makedate))<0 then 0 else (to_date(a.uwdate)-to_date(c.makedate)) end)  from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and appflag='1' and conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and  substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode ) G1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) H1, ");
                sql.append(" (select sum(case when(to_date(a.uwdate)-to_date(a.InputDate))<0 then 0 else (to_date(a.uwdate)-to_date(a.InputDate)) end)  from lccont a,lccuwmaster b where b.contno=a.contno  and b.PassFlag not in ('5','z') and a.CardFlag<>'1' and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.prtno not in(select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "') and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode) L1, ");
                sql.append(" (select count(prtno) from lccont  where appflag='1' and conttype='1' and signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and CardFlag<>'1' and substr(Managecom,1,length('" +
                           Managecom + "'))=Z.comcode and  prtno not in (select a.prtno from lccont a,lccuwmaster b,ES_DOC_MAIN c where b.contno=a.contno and  c.DocCode=a.prtno and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  )) M1, ");
                sql.append(" (select sum(case when(to_date(c.EnterAccDate)-to_date(a.uwdate))<0 then 0 else  (to_date(c.EnterAccDate)-to_date(a.uwdate))end) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and a.CardFlag<>'1' and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "' and  c.EnterAccDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) N1, ");
                sql.append(" (select count(a.prtno) from lccont a,lccuwmaster b,LJTempFee c where b.contno=a.contno and  c.OtherNo=a.contno and c.OtherNoType='2' and b.PassFlag not in ('5','z') and a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "'  and substr(a. Managecom,1,length('" + Managecom +
                           "'))=Z.comcode and c.EnterAccDate is not null) O1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(c.EnterAccDate)) end)  from lccont a,LJTempFee c where c.OtherNo=a.contno and a.CardFlag<>'1' and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'  and c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) P1, ");
                sql.append(" (select count(a.prtno) from lccont a,LJTempFee c where c.OtherNo=a.contno and c.OtherNoType='2'  and a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "' and a.CardFlag<>'1' and  c.EnterAccDate is not null and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) Q1, ");
                sql.append(" (select sum(case when(to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate))<0 then 0 else (to_date(a.CustomGetPolDate)-to_date(a.PolApplyDate)) end ) from lccont a where  a.appflag='1' and a.CardFlag<>'1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate +
                        "'  and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode ) R1, ");
                sql.append(" (select count(a.prtno) from lccont a where  a.appflag='1' and a.conttype='1' and a.signdate between '" +
                           StartDate + "' and '" + EndDate + "'   and a.CardFlag<>'1' and a.CustomGetPolDate is not null and substr(a. Managecom,1,length('" +
                           Managecom + "'))=Z.comcode  ) S1 ");
                sql.append(" ");
                sql.append(
                        " from LDCom Z where   length(trim(Z.comcode))=length('" +
                        Managecom +
                        "') and sign='1' and substr(Z.comcode,1,length('" +
                        Managecom + "'))='" + Managecom + "' ");
                sql.append("  ) as X with ur ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
                            result.add(ssrs.getRowData(i));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第三个Sql语句~~~~~");

            }

        }
        /** 开始处理Xml */
        ListTable tListTable = new ListTable();
        tListTable.setName("WorkEfficencyReport");
        String[] title = {"A", "B", "C", "D", "E", "F", "G"};
        /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
         * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
         *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
         *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
         * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
         * 就是说 A - J 必须是一维的（一个数据）的数据
         * 不能出现A1--An是一个数据 B1--Bn是一个数组，
         * 这样会无法显示。
         *  */
        for (int i = 0; i < result.size(); i++) {
            tListTable.add((String[]) result.get(i));
        }

        XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
        xml.createDocument("WorkEfficiency.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                         "月" +
                         tSrtTool.getDay() + "日";

        //WorkEfficiencyBL模版元素
        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        MakeDate = MakeDate.replace('-', '月');
        MakeDate = MakeDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);
        texttag.add("QYType_ch", QYType_ch);
        texttag.add("MakeDate", MakeDate);
        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" +
                       OperatorManagecom +
                       "'";
        System.out.println("zhuzhu" + sql_M);
        tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("MngCom", temp_M[0][0]);

        if (texttag.size() > 0) {
            xml.addTextTag(texttag);
        }

        //保存信息
        xml.addListTable(tListTable, title);
        /** 添加完成将输出的结果保存到结果集中 */
        mResult.add(xml);
        mResult.clear();
        mResult.addElement(xml);

        return true;

    }

    public static void main(String[] args) {

        WorkEfficiencyBL tWorkEfficiencyBL = new WorkEfficiencyBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("OperatorManagecom", "86");
        mTransferData.setNameAndValue("Managecom", "86110000");
        mTransferData.setNameAndValue("StartDate", "2005-1-1");
        mTransferData.setNameAndValue("EndDate", "2005-11-1");
        mTransferData.setNameAndValue("QYType","3");
        tVData.add(mTransferData);
        tWorkEfficiencyBL.submitData(tVData, "PRINT");
        VData vData = tWorkEfficiencyBL.getResult();

    }


}
