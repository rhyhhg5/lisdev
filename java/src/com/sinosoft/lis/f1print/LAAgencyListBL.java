package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

class LAAgencyListBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mAgentcyCode= "";
       private String mAgencyName = "";
       private String mStartDate = "";
       private String mEndDate = "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private SSRS mSSRS1 = new SSRS();
//       private XmlExport mXmlExport = null;
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;
       private String  tMakeDate = mPubFun.getCurrentDate();
       private String tMakeTime = mPubFun.getCurrentTime();
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }
       // 得到外部传入的数据，将数据备份到本类中
       if (!getInputData(mInputData)) {
             return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }
         return true;
     }

     public VData getResult() {
         return mResult;
    }

     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAAgencyListBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

      private boolean getInputData(VData cInputData)
      {

          try {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));

              //页面传入的数据 五个
              this.mManageCom = (String) cInputData.get(1);
              this.mAgentcyCode = (String) cInputData.get(2);
              this.mAgencyName = (String) cInputData.get(3);
              this.mStartDate = (String) cInputData.get(4);
              this.mEndDate = (String) cInputData.get(5);
              System.out.println(mManageCom);
          }
           catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

      }

  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAAgencyListBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



  private boolean queryData()
  {
      try {
          // 查询数据
          if (!getDataList()) {
              return false;
          }
        if(mListTable==null||mListTable.equals(""))
        {
            CError.buildErr(this, "没有满足条件的值！");
            return false;
        }
          //新建一个TextTag的实例
          TextTag tTextTag = new TextTag();
          String tTitle[] ={"","","","","","",""};
          System.out.print("dayin252");
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
  //        tTextTag.add("tName", mManageName);

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAAgencyList.vts", "printer"); //初始化xml文档
          if (tTextTag.size() > 0)
              xmlexport.addTextTag(tTextTag); //添加动态文本标签
          mListTable.setName("Order");
          xmlexport.addListTable(mListTable,tTitle); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new1");
          this.mResult.clear();
          mResult.addElement(xmlexport);
          return true;

      } catch (Exception ex) {
          buildError("queryData", "LABL发生错误，准备数据时出错！");
          return false;
      }
  }
  private boolean getDataList()
   {
       String year = tMakeDate.substring(0,4);
       String tstartdate = year+"-01-01";
       String tSQL = "";
        String comSQL = "";

                 tSQL = "select a.managecom,getUniteCode(a.agentcode),"
                +
                "(select name from laagent where  a.agentcode=laagent.agentcode ),"
                + "a.grpcontno,a.riskcode,sum(transmoney),"
                +
                "(select b.peoples2 from lcgrppol b where b.grppolno=a.grppolno)"
                + " from lacommision a where branchtype='2' "
                +
                " and branchtype2='02' ";
         if(mManageCom!=null&&!mManageCom.equals(""))
         {
             tSQL += " and managecom like '"+mManageCom+"%'";
         }
         if (mStartDate == null || mStartDate.equals("")) {
             tSQL += " and tmakedate>='"+tstartdate+"'";
         } else {
             tSQL += " and tmakedate>='"+mStartDate+"'";
         }
         if (mEndDate == null || mEndDate.equals("")) {
             tSQL += " and tmakedate<='"+tMakeDate+"'";
         }
         else{
             tSQL += " and tmakedate<='"+mEndDate+"'";
         }
         if(mAgentcyCode != null&&!mAgentcyCode.equals(""))
         {
           tSQL+=" and a.Agentcode = getAgentCode('"+mAgentcyCode+"')";
         }
         tSQL+=" and a.agentcode in (select k.agentcode from laagent k where "
                    + "   branchtype='2' and branchtype2='02' ";
         if(mAgencyName!=null&&!mAgencyName.equals("")){
             tSQL+="  and  k.name='"+mAgencyName+"'"   ;
         }
         tSQL+=" ) group by managecom,agentcode,grpcontno,grppolno,riskcode order by managecom,agentcode";
         System.out.println(tSQL);
          ExeSQL ttExeSQL = new ExeSQL();
           SSRS ttSSRS = new SSRS();
           ttSSRS = ttExeSQL.execSQL(tSQL);
          for(int k=1;k<=ttSSRS.getMaxRow();k++)
          {
              String info[] = new String[7];
              info[0] = ttSSRS.GetText(k, 1);
              info[1] = ttSSRS.GetText(k, 2);
              info[2] = ttSSRS.GetText(k, 3);
              info[3] = ttSSRS.GetText(k, 4);
              info[4] = ttSSRS.GetText(k, 5);
              info[5] = ttSSRS.GetText(k, 6);
              info[6] = ttSSRS.GetText(k, 7);
              mListTable.add(info);
          }

     return true;
   }

}


