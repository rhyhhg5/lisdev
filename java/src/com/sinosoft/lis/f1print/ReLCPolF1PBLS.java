package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


public class ReLCPolF1PBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mInputData;
    private VData mResult = new VData();

    public ReLCPolF1PBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println("Start ReLCPolF1PBLS Submit...");

        tReturn = save(cInputData);

        System.out.println(tReturn);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ReLCPolF1PBLS Submit...");

        return tReturn;
    }

    //保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolF1PBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

            System.out.println("Start ....");
            LCPolDB tLCPolDB = new LCPolDB(conn);
            LCPolSet tLCPolSet = new LCPolSet();
            tLCPolSet.set((LCPolSet) mInputData.getObjectByObjectName(
                    "LCPolSet", 0)); //

            for (int i = 0; i < tLCPolSet.size(); i++)
            {
                tLCPolDB.setSchema(tLCPolSet.get(i + 1));

                if (!tLCPolDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ReLCPolF1PBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            } //end for ()
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReLCPolF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }

        return tReturn;
    }

}
