package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

public class LAAnnuityUI {

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public LAAnnuityUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAAnnuityBL tLAAnnuityBL = new LAAnnuityBL();

        if (!tLAAnnuityBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAAnnuityBL.mErrors);
            return false;
        } else {
            this.mResult.clear();
            this.mResult = tLAAnnuityBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }
}
