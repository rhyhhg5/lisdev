package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.adventnet.aclparser.ParseException;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.FIAboriginalGenDetailPSet;
import com.sinosoft.lis.vschema.FIAboriginalMainPSet;

public class DataBackUpBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    

    private String StateDate = "";
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();
	private PubSubmit mPubSubmit = new PubSubmit();
    	
	private String[][] mToExcel = null;
	
	
       

    public DataBackUpBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        	
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }       

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
public boolean dealData()
    {
//主表备份
	if (!dealMainData())
        {
            return false;
        } 
//明细表备份
	if (!dealDetailData())
        {
            return false;
        } 
//明细表手续费备份
	if (!dealDetailSXFData())
        {
            return false;
        }
	
    	System.out.println("主表明细表数据备份完成！");
    	
 return true;       
    	
    }
/**
 * dealMainData
 *主表备份 
 * @param cInputData VData：submitData中传入的VData对象
 * @return boolean：true提交成功, false提交失败
 */
private boolean dealMainData() {
	System.out.println("开始备份主表！");
	
// TODO Auto-generated method stub            
    String insertmain= 
		" select *  from FIAboriginalMain a where a.state = '10' and   exists "
		+" (select 1 from FIAboriginalMainAtt b where a.indexno=b.indexno and a.indexcode=b.indexcode and a.mserialno = b.mserialno  and gdcardflag<>'1')"
		+" and accountdate = '"+StateDate+"'"
		+" and not exists (select 1 from FIAboriginalMainP c where a.indexno=c.indexno and a.indexcode=c.indexcode and a.mserialno = c.mserialno)";
    
/*	String deletemain = "delete  from FIAboriginalMain a where a.state = '10' and   exists "
		+" (select 1 from FIAboriginalMainAtt b where a.indexno=b.indexno and a.indexcode=b.indexcode  and a.mserialno = b.mserialno and gdcardflag<>'1')"
		+" and exists (select 1 from FIAboriginalMainP c where a.indexno=c.indexno and a.indexcode=c.indexcode and a.mserialno = c.mserialno)"
		+" and accountdate = '"+StateDate+"'";*/

	RSWrapper rsWrapper1 = new RSWrapper();    
	FIAboriginalMainPSet tFIAboriginalMainPSet = new FIAboriginalMainPSet();
    if (!rsWrapper1.prepareData(tFIAboriginalMainPSet, insertmain))
    {
        mErrors.copyAllErrors(rsWrapper1.mErrors);
        System.out.println("批量准备主表数据失败"+rsWrapper1.mErrors.getFirstError());
        buildError("DataBackUpBL","dealMainData", "批量准备主表数据失败,提示信息为：" +rsWrapper1.mErrors.getFirstError());
        return false;
    }		
    do
    {
    	rsWrapper1.getData();
        if (tFIAboriginalMainPSet != null && tFIAboriginalMainPSet.size() > 0)
        {
        	System.out.println("开始往主表备份表插入主表数据！");
        	dealMainPData(tFIAboriginalMainPSet);
     	
        }                 
    }while(tFIAboriginalMainPSet != null && tFIAboriginalMainPSet.size() > 0);        
    tFIAboriginalMainPSet=null;
    rsWrapper1.close();
 /*   MMap tMap= new MMap();
	VData tInputData = new VData();            	
	tMap.put(deletemain, "DELETE");
	tInputData.add(tMap);
	PubSubmit mPubSubmitU = new PubSubmit();
    System.out.println("开始删除主表已经备份过得数据！");       		
    if (!mPubSubmitU.submitData(tInputData, ""))
    {
        this.mErrors.copyAllErrors(mPubSubmitU.mErrors);
        System.out.println("删除主表数据报错");
        buildError("DataBackUpBL","dealMainData", "删除主表数据报错,提示信息为：" +mPubSubmitU.mErrors.getFirstError());
        return false;
    }
    tInputData.clear();  
    tMap = new MMap();  */     
    System.out.println("主表备份完成！");	
		return true;
	}
/**
 * dealDetailData
 *明细表备份      
 * @param cInputData VData：submitData中传入的VData对象
 * @return boolean：true提交成功, false提交失败
 */

private boolean dealDetailData() {
	
// TODO Auto-generated method stub
	
	System.out.println("开始备份明细表！");     	
    String insertdetail = 
		" select *  from FIAboriginalGenDetail  a where a.state = '10' and  exists "
	   	+" (select 1 from FIAboriginalMainAtt b where a.indexno=b.indexno and a.indexcode=b.indexcode  and a.mserialno = b.mserialno and gdcardflag<>'1')"
	   	+" and accountdate = '"+StateDate+"'"
	   	+" and not exists (select 1 from FIAboriginalGenDetailP c where a.indexno=c.indexno and a.indexcode=c.indexcode and a.aserialno = c.aserialno)";  
    String deletedetail = "delete  from FIAboriginalGenDetail a where a.state = '10' and  exists "
    		+" (select 1 from FIAboriginalMainAtt b where a.indexno=b.indexno and a.indexcode=b.indexcode and a.mserialno = b.mserialno  and gdcardflag<>'1')"
    		+" and exists (select 1 from FIAboriginalGenDetailP c where a.indexno=c.indexno and a.indexcode=c.indexcode  and a.aserialno = c.aserialno)"
    		+" and accountdate = '"+StateDate+"'";

	RSWrapper rsWrapper2 = new RSWrapper();    
	FIAboriginalGenDetailPSet tFIAboriginalGenDetailPSet = new FIAboriginalGenDetailPSet();
    if (!rsWrapper2.prepareData(tFIAboriginalGenDetailPSet, insertdetail))
    {
        mErrors.copyAllErrors(rsWrapper2.mErrors);
        System.out.println("批量准备明细表数据失败"+rsWrapper2.mErrors.getFirstError());
        buildError("DataBackUpBL","dealDetailData", "批量准备明细表数据失败,提示信息为：" +rsWrapper2.mErrors.getFirstError());
        return false;
    }		
    do
    {
    	rsWrapper2.getData();
        if (tFIAboriginalGenDetailPSet != null && tFIAboriginalGenDetailPSet.size() > 0)
        {
        	System.out.println("开始往明细表备份表插入明细表数据！");
        	dealDetailPData(tFIAboriginalGenDetailPSet);

        }                 
    }while(tFIAboriginalGenDetailPSet != null && tFIAboriginalGenDetailPSet.size() > 0);        
    tFIAboriginalGenDetailPSet=null;
    rsWrapper2.close();
  /*  MMap tMap= new MMap();
	VData tInputData = new VData();            	
	tMap.put(deletedetail, "DELETE");
	tInputData.add(tMap);
	PubSubmit mPubSubmitU = new PubSubmit();
    System.out.println("开始删除明细表已经备份过得数据！");       		
    if (!mPubSubmitU.submitData(tInputData, ""))
    {
        this.mErrors.copyAllErrors(mPubSubmitU.mErrors);
        System.out.println("删除明细表数据报错");
        buildError("DataBackUpBL","dealDetailData", "删除明细表数据报错,提示信息为：" +mPubSubmitU.mErrors.getFirstError());
        return false;
    }
    tInputData.clear();  
    tMap = new MMap();  */          	
    System.out.println("明细表备份完成！");	
		return true;
	}


/**
 * dealDetailSXFData
 *明细表--手续费应付数据备份
 * @param cInputData VData：submitData中传入的VData对象
 * @return boolean：true提交成功, false提交失败
 */

 private boolean dealDetailSXFData() {
	 
// TODO Auto-generated method stub    
	 
	System.out.println("开始备份明细表手续费！"); 
    String insertdetailcharge = 
    		" select *  from FIAboriginalGenDetail  a where a.state = '10' and  exists "
    		+" (select 1 from FIAboriginalMainAtt b where a.mserialno=b.mserialno and a.indexcode=b.indexcode and gdcardflag<>'1' and b.businesscode = '09' and b.feetype = 'SX')"
    		+" and accountdate = '"+StateDate+"'"
    		+" and not exists (select 1 from FIAboriginalGenDetailP c where a.indexno=c.indexno and a.indexcode=c.indexcode)";   
    String deletedetailcharge = "delete  from FIAboriginalGenDetail a where a.state = '10' and   exists "
    		+" (select 1 from FIAboriginalMainAtt b where a.mserialno=b.mserialno and a.indexcode=b.indexcode and gdcardflag<>'1' and b.businesscode = '09' and b.feetype = 'SX')"
    		+" and exists (select 1 from FIAboriginalGenDetailP c where a.indexno=c.indexno and a.indexcode=c.indexcode)"
    		+" and accountdate = '"+StateDate+"'";
	RSWrapper rsWrapper3 = new RSWrapper();    
	FIAboriginalGenDetailPSet tFIAboriginalGenDetailPSet = new FIAboriginalGenDetailPSet();
    if (!rsWrapper3.prepareData(tFIAboriginalGenDetailPSet, insertdetailcharge))
    {
        mErrors.copyAllErrors(rsWrapper3.mErrors);
        System.out.println("批量准备明细表手续费数据失败"+rsWrapper3.mErrors.getFirstError());
        buildError("DataBackUpBL","dealDetailSXFData", "批量准备明细表手续费数据失败,提示信息为：" +rsWrapper3.mErrors.getFirstError());
        return false;
    }		
    do
    {
    	rsWrapper3.getData();
        if (tFIAboriginalGenDetailPSet != null && tFIAboriginalGenDetailPSet.size() > 0)
        {
        	System.out.println("开始往明细表备份表插入手续费数据！");
        	dealDetailPData(tFIAboriginalGenDetailPSet);
            MMap tMap= new MMap();
    		VData tInputData = new VData();            	
    		tMap.put(deletedetailcharge, "DELETE");
    		tInputData.add(tMap);
    		PubSubmit mPubSubmitU = new PubSubmit();
            System.out.println("开始删除明细表已经备份过得手续费数据！");       		
            if (!mPubSubmitU.submitData(tInputData, ""))
            {
                this.mErrors.copyAllErrors(mPubSubmitU.mErrors);
                System.out.println("删除明细表手续费数据报错");
                buildError("DataBackUpBL","dealDetailSXFData", "删除明细表手续费数据报错,提示信息为：" +mPubSubmitU.mErrors.getFirstError());
                return false;
            }
            tInputData.clear();  
            tMap = new MMap();            	
        }                 
    }while(tFIAboriginalGenDetailPSet != null && tFIAboriginalGenDetailPSet.size() > 0);        
    tFIAboriginalGenDetailPSet=null;
    rsWrapper3.close();
    System.out.println("明细表手续费备份完成！");	
		return true;
	}
 

/**
 * dealMainPData
 *插入主表备份表数据处理
 * @param cInputData VData：submitData中传入的VData对象
 * @return boolean：true提交成功, false提交失败
 */
	private boolean dealMainPData(FIAboriginalMainPSet tFIAboriginalMainPSet) {
		
		// TODO Auto-generated method stub
		if(tFIAboriginalMainPSet!=null && tFIAboriginalMainPSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();
			tMap.put(tFIAboriginalMainPSet, "INSERT");
			tInputData.add(tMap);
		    PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            buildError("DataBackUpBL","dealMainPData", "往主表备份表插入数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tFIAboriginalMainPSet=null;

		}
        return true;
	}
	/**
	 * dealDetailPData
	 *插入主表备份表数据处理
	 * @param cInputData VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	
private boolean dealDetailPData(FIAboriginalGenDetailPSet tFIAboriginalGenDetailPSet) {
	
		// TODO Auto-generated method stub
	
		if(tFIAboriginalGenDetailPSet!=null && tFIAboriginalGenDetailPSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();
			tMap.put(tFIAboriginalGenDetailPSet, "INSERT");
			tInputData.add(tMap);
		    PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            buildError("DataBackUpBL","dealDetailPData", "往明细备份表插入数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tFIAboriginalGenDetailPSet=null;

		}
	    return true;
	}
	
	public String[][] getMToExcel() {
		return mToExcel;
	}
	
	
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
	
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        StateDate = (String) tf.getValueByName("StateDate");      
        EndDate = (String) tf.getValueByName("EndDate");
        

        if (StateDate == null || EndDate == null)
        {
        	buildError("DataBackUpBL","getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}
