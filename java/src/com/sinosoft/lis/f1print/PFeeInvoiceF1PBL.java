package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PFeeInvoiceF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
    private String mOperate = "";
    private LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();

    public PFeeInvoiceF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        System.out.println("cOperate:" + cOperate);
        if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT") &&
            !cOperate.equals("REPRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        if (cOperate.equals("CONFIRM"))
        {
            mResult.clear();
            // 准备所有要打印的数据
            getPrintData();
        }
        else if (cOperate.equals("PRINT"))
        {
            System.out.print("update print");
            if (!saveData())
            {
                return false;
            }
        }
        else if (cOperate.equals("REPRINT"))
        {
            mResult.clear();
            // 准备所有要打印的数据
            getPrintData2();
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("PRINT") || mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLOPRTManager2Schema.setSchema((LOPRTManager2Schema) cInputData.
                                           getObjectByObjectName(
                    "LOPRTManager2Schema", 0));
        }

        if (mOperate.equals("CONFIRM"))
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLJAPaySchema.setSchema((LJAPaySchema) cInputData.
                                    getObjectByObjectName("LJAPaySchema", 0));
        }

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        int i;
        String PolNo = "";
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setSchema(mLJAPaySchema);
        if (!tLJAPayDB.getInfo())
        {
            mErrors.copyAllErrors(tLJAPayDB.mErrors);
            buildError("outputXML", "在取得LJAPay的数据时发生错误");
            return false;
        }
        String DSumMoney = "";
        double SumMoney = tLJAPayDB.getSumActuPayMoney();
        DSumMoney = PubFun.getChnMoney(SumMoney);

        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
        tLJAPayPersonDB.setPayType("ZC");
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        tLJAPayPersonSet.set(tLJAPayPersonDB.query());
        //int i=tLJAPayPersonSet.size();
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("FEE");
        LJAPayPersonSchema aLJAPayPersonSchema = null;
        for (i = 0; i < tLJAPayPersonSet.size(); i++)
        {

            aLJAPayPersonSchema = new LJAPayPersonSchema();
            aLJAPayPersonSchema.setSchema(tLJAPayPersonSet.get(i + 1));
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(aLJAPayPersonSchema.getPolNo());
            tLCPolDB.setMainPolNo(aLJAPayPersonSchema.getPolNo());
            if (tLCPolDB.getCount() == 1)
            {
                PolNo = tLCPolDB.getPolNo();
            }
            strArr = new String[3];
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(aLJAPayPersonSchema.getRiskCode());
            if (!tLMRiskDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                buildError("outputXML", "在取得LMRisk的数据时发生错误");
                return false;
            }
            strArr[0] = tLMRiskDB.getRiskName();
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("payintv");
            tLDCodeDB.setCode(String.valueOf(aLJAPayPersonSchema.getPayIntv()));
            if (!tLDCodeDB.getInfo())
            {
                mErrors.copyAllErrors(tLDCodeDB.mErrors);
                buildError("outputXML", "在取得LDCode的数据时发生错误");
                return false;
            }
            strArr[1] = tLDCodeDB.getCodeName();
            strArr[2] = String.valueOf(aLJAPayPersonSchema.getSumActuPayMoney());
            tlistTable.add(strArr);
        }
        strArr = new String[3];
        strArr[0] = "Risk";
        strArr[1] = "Mode";
        strArr[2] = "Money";

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(PolNo);
        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            buildError("outputXML", "在取得LCPol的数据时发生错误");
            return false;
        }

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCPolDB.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }

        if (controlPrt(tLJAPayDB.getSchema()) == false)
        {
            buildError("outputXML", "在准备打印数据时发生错误");
            return false;
        }

        String strPaytoDate = tLCPolDB.getPaytoDate();

        strPaytoDate = strPaytoDate.substring(0, 4) + "年"
                       + strPaytoDate.substring(5, 7) + "月"
                       + strPaytoDate.substring(8, 10) + "日";

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PFeeInvoice.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("PayNo", tLJAPayDB.getPayNo());
        texttag.add("XSumMoney", tLJAPayDB.getSumActuPayMoney());
        texttag.add("DSumMoney", DSumMoney);
        texttag.add("AppntName", tLCPolDB.getAppntName());
        texttag.add("PolNo", tLCPolDB.getPolNo());
        texttag.add("PaytoDate", strPaytoDate);
        //texttag.add("Handler",tLCPolDB.getHandler());
        texttag.add("Handler", mGlobalInput.Operator);
        texttag.add("AgentCode", tLCPolDB.getAgentCode());
        texttag.add("AgentName", tLAAgentDB.getName());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy  MM  dd");
        texttag.add("Today", sdf.format(new Date()));

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        mResult.addElement(mLOPRTManager2Schema);
        return true;

    }

    private boolean getPrintData2()
    {
        int i;
        String PolNo = "";
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setPayNo(mLOPRTManager2Schema.getOtherNo());
        if (!tLJAPayDB.getInfo())
        {
            mErrors.copyAllErrors(tLJAPayDB.mErrors);
            buildError("outputXML", "在取得LJAPay的数据时发生错误");
            return false;
        }
        mLJAPaySchema.setSchema(tLJAPayDB.getSchema());
        String DSumMoney = "";
        double SumMoney = tLJAPayDB.getSumActuPayMoney();
        DSumMoney = PubFun.getChnMoney(SumMoney);

        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
        tLJAPayPersonDB.setPayType("ZC");
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        tLJAPayPersonSet.set(tLJAPayPersonDB.query());
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("FEE");
        LJAPayPersonSchema aLJAPayPersonSchema = null;
        for (i = 0; i < tLJAPayPersonSet.size(); i++)
        {

            aLJAPayPersonSchema = new LJAPayPersonSchema();
            aLJAPayPersonSchema.setSchema(tLJAPayPersonSet.get(i + 1));
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(aLJAPayPersonSchema.getPolNo());
            tLCPolDB.setMainPolNo(aLJAPayPersonSchema.getPolNo());
            if (tLCPolDB.getCount() == 1)
            {
                PolNo = tLCPolDB.getPolNo();
            }
            strArr = new String[3];
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(aLJAPayPersonSchema.getRiskCode());
            if (!tLMRiskDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                buildError("outputXML", "在取得LMRisk的数据时发生错误");
                return false;
            }
            strArr[0] = tLMRiskDB.getRiskName();
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("payintv");
            tLDCodeDB.setCode(String.valueOf(aLJAPayPersonSchema.getPayIntv()));
            if (!tLDCodeDB.getInfo())
            {
                mErrors.copyAllErrors(tLDCodeDB.mErrors);
                buildError("outputXML", "在取得LDCode的数据时发生错误");
                return false;
            }
            strArr[1] = tLDCodeDB.getCodeName();
            strArr[2] = String.valueOf(aLJAPayPersonSchema.getSumActuPayMoney());
            tlistTable.add(strArr);
        }
        strArr = new String[3];
        strArr[0] = "Risk";
        strArr[1] = "Mode";
        strArr[2] = "Money";

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(PolNo);
        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            buildError("outputXML", "在取得LCPol的数据时发生错误");
            return false;
        }

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCPolDB.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }

        if (controlPrt(tLJAPayDB.getSchema()) == false)
        {
            buildError("outputXML", "在准备打印数据时发生错误");
            return false;
        }

        String strPaytoDate = tLCPolDB.getPaytoDate();

        strPaytoDate = strPaytoDate.substring(0, 4) + "年"
                       + strPaytoDate.substring(5, 7) + "月"
                       + strPaytoDate.substring(8, 10) + "日";

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PFeeInvoice.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("PayNo", tLJAPayDB.getPayNo());
        texttag.add("XSumMoney", tLJAPayDB.getSumActuPayMoney());
        texttag.add("DSumMoney", DSumMoney);
        texttag.add("AppntName", tLCPolDB.getAppntName());
        texttag.add("PolNo", tLCPolDB.getPolNo());
        texttag.add("PaytoDate", strPaytoDate);
        //texttag.add("Handler",tLCPolDB.getHandler());
        texttag.add("Handler", mGlobalInput.Operator);
        texttag.add("AgentCode", tLCPolDB.getAgentCode());
        texttag.add("AgentName", tLAAgentDB.getName());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy  MM  dd");
        texttag.add("Today", sdf.format(new Date()));

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        mResult.addElement(mLOPRTManager2Schema);
        return true;

    }

    public boolean saveData()
    {
        System.out.print("update print:" + mLOPRTManager2Schema.getPrtSeq());
        LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
        tLOPRTManager2DB.setPrtSeq(mLOPRTManager2Schema.getPrtSeq());
        if (tLOPRTManager2DB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "outputXML";
            tError.functionName = "getPrintData";
            tError.errorMessage = "打印数据准备失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLOPRTManager2Schema.setSchema(tLOPRTManager2DB.getSchema());
        mLOPRTManager2Schema.setStateFlag("1");
        tLOPRTManager2DB = new LOPRTManager2DB();
        tLOPRTManager2DB.setSchema(mLOPRTManager2Schema);
        tLOPRTManager2DB.update();
        System.out.print("update print");

        return true;
    }

    public boolean controlPrt(LJAPaySchema tLJAPaySchema)
    {
        //添加打印控制--预览时插入打印管理数据，打印时更新打印数据状态为已打
        LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
        LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
        tLOPRTManager2DB.setOtherNo(tLJAPaySchema.getPayNo());
        tLOPRTManager2DB.setOtherNoType("05"); //05 --- 交费收据号码
        tLOPRTManager2DB.setCode(PrintManagerBL.CODE_PINVOICE);
        tLOPRTManager2Set = tLOPRTManager2DB.query();
        if (tLOPRTManager2Set.size() == 0)
        {
            LOPRTManager2Schema tLOPRTManager2Schema = getPrintData(
                    tLJAPaySchema);
            if (tLOPRTManager2Schema == null)
            {
                CError tError = new CError();
                tError.moduleName = "outputXML";
                tError.functionName = "getPrintData";
                tError.errorMessage = "打印数据准备失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
//              tLOPRTManager2DB=new LOPRTManager2DB();
//              tLOPRTManager2DB.setSchema(tLOPRTManager2Schema);
//              if(tLOPRTManager2DB.insert()==false)
//              {
//                  this.mErrors.copyAllErrors(tLOPRTManager2DB.mErrors);
//                  CError tError = new CError();
//                  tError.moduleName = "outputXML";
//                  tError.functionName = "getPrintData";
//                  tError.errorMessage = "打印数据保存失败!";
//                  this.mErrors .addOneError(tError) ;
//                  return false;
//              }

            mLOPRTManager2Schema.setSchema(tLOPRTManager2Schema);
        }
        else
        {
            mLOPRTManager2Schema.setSchema(tLOPRTManager2Set.get(1));
        }

        return true;
    }


    public LOPRTManager2Schema getPrintData(LJAPaySchema tLJAPaySchema)
    {
        LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
        try
        {
            String tLimit = PubFun.getNoLimit(tLJAPaySchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
            tLOPRTManager2Schema.setOtherNo(tLJAPaySchema.getPayNo());
            tLOPRTManager2Schema.setOtherNoType("05");
            tLOPRTManager2Schema.setCode(PrintManagerBL.CODE_PINVOICE);
            tLOPRTManager2Schema.setManageCom(tLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setAgentCode(tLJAPaySchema.getAgentCode());
            tLOPRTManager2Schema.setReqCom(tLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setReqOperator(tLJAPaySchema.getOperator());
            tLOPRTManager2Schema.setPrtType("0");
            tLOPRTManager2Schema.setStateFlag("0");
            tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
        }
        catch (Exception ex)
        {
            return null;
        }
        return tLOPRTManager2Schema;
    }

    public static void main(String[] args)
    {
        PFeeInvoiceF1PBL PFeeInvoiceF1PBL1 = new PFeeInvoiceF1PBL();
    }
}