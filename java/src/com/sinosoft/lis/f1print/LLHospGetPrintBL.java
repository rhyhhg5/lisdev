package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLHospGetPrintBL</p>
 * <p>Description: 医保通给付凭证打印</p>
 * <p>Copyright: Copyright sinosoft (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class LLHospGetPrintBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private String mOperate;
    // 打印次数或打印方式（即时打印or批量打印）
    private String mdirect = "";
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    // 业务处理相关变量
    private GlobalInput mGlobalInput = new GlobalInput();

    // private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mHCNo = "";

    private String mflag = null;

    private int mPrintTimes = 0;

    public LLHospGetPrintBL() {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
//        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT")) {
//            buildError("submitData", "不支持的操作字符串");
//            return false;
//        }
        mOperate = cOperate;
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
//        if (!dealPrintMag("INSERT")) {
//            return false;
//        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        // 全局变量
        LOBatchPRTManagerSchema prtSchema = (LOBatchPRTManagerSchema)
                                            cInputData
                                            .getObjectByObjectName(
                "LOBatchPRTManagerSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema tLOPRTManagerSchema = (LOPRTManagerSchema)
                                                 cInputData.
                                                 getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if (tLOPRTManagerSchema.getOtherNo() != null) {
            mHCNo = tLOPRTManagerSchema.getOtherNo();
            mdirect += tLOPRTManagerSchema.getStandbyFlag3();
            mPrintTimes = (int) tLOPRTManagerSchema.getPrintTimes();
        } else if (prtSchema != null && prtSchema.getOtherNo() != null) {
            mHCNo = prtSchema.getOtherNo();
        } else {
            buildError("getInputData", "没有指定打印数据！");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean getPrintData() {

        TextTag tTextTag = new TextTag(); // 新建一个TextTag的实例
        // 根据印刷号查询打印队列中的纪录
        String tBankCode = "";
        String tBankName = "";
        String tBankAccNo = "";
        String tAccName = "";
        String tApplyer = "";
        String tAppLayerName = "";
        String tDrawer = "";
        double tSumPay = 0.0;
        String tMoney = "";
        String tCHNMoney = "";
        String tPayMode = "";
        String tPayModeName = "";
       
        //添加给付号码
        String tActuGetNo = "";
        
        if (mHCNo == null || "".equals(mHCNo) || "null".equals(mHCNo)) {
            buildError("getPrintData", "批次号不能为空！");
            return false;
        }

        LLHospGetDB tLLHospGetDB = new LLHospGetDB();
        tLLHospGetDB.setHCNo(mHCNo);
        if (!tLLHospGetDB.getInfo()) {
            buildError("getPrintData", "批次查询失败！");
            return false;
        }

        if (!"1".equals(tLLHospGetDB.getState())) {
            buildError("getPrintData", "批次还未确认！");
            return false;
        }

        tPayMode = tLLHospGetDB.getPayMode();

        if (tPayMode == null || "".equals(tPayMode)) {
            buildError("getPrintData", "给付方式为空！");
            return false;
        }

        tBankCode = tLLHospGetDB.getBankCode();
        if (tBankCode != null && !"".equals(tBankCode) &&
            !"null".equals(tBankCode)) {

            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(tBankCode);
            if (!tLDBankDB.getInfo()) {
                buildError("getPrintData", "银行查询失败！");
                return false;
            }
            tBankName = tLDBankDB.getBankName();
        }

        tBankAccNo = tLLHospGetDB.getBankAccNo();
        tAccName = tLLHospGetDB.getAccName();
        tApplyer = tLLHospGetDB.getApplyer();
        tDrawer = tLLHospGetDB.getDrawer();

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(tPayMode);
        tLDCodeDB.setCodeType("paymode");

        if (!tLDCodeDB.getInfo()) {
            buildError("getPrintData", "给付方式查询失败！");
            return false;
        }
        tPayModeName = tLDCodeDB.getCodeName();

        if (tApplyer == null || "".equals(tApplyer) ||
            "null".equals(tApplyer)) {
            buildError("getPrintData", "业务处理人为空！");
            return false;
        }
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tApplyer);
        if (!tLDUserDB.getInfo()) {
            buildError("getPrintData", "用户人查询失败！");
            return false;
        }

        tAppLayerName = tLDUserDB.getUserName();
        //从ljaget中获得actugetno
        LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = new LJAGetSet();
        tLJAGetDB.setOtherNo(mHCNo);
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSet = tLJAGetDB.query();
        if(tLJAGetSet == null || tLJAGetSet.size() <= 0){
            System.out.println("原有的医保通案件处理");
            LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
            LLHospCaseSet tLLHospCaseSet = new LLHospCaseSet();
            tLLHospCaseDB.setHCNo(mHCNo);
            tLLHospCaseSet = tLLHospCaseDB.query();
            String aCaseNo = tLLHospCaseSet.get(1).getCaseNo();
            
            LJAGetDB aLJAGetDB = new LJAGetDB();
            LJAGetSet aLJAGetSet = new LJAGetSet();
            aLJAGetDB.setOtherNo(aCaseNo);
            aLJAGetSet = aLJAGetDB.query();
            tActuGetNo = aLJAGetSet.get(1).getActuGetNo();
            
        }else{
            System.out.println("现在的医保通处理");
            tLJAGetSchema = tLJAGetSet.get(1).getSchema();
            if(tLJAGetSchema == null){
                buildError("getPrintData", "给付号码查询失败！");
                return false; 
            }
            tActuGetNo = tLJAGetSchema.getActuGetNo();
        }
        
        
        

        // ListTable
//        ListTable tCaseListTable = new ListTable();
//        tCaseListTable.setName("CASECOL");
          ListTable tEndListTable = new ListTable();
          tEndListTable.setName("End");
//
//        String[] tTitle = {"ACTUGETNO", "CUSTOMERNAME", "CUSTOMERNO",
//                          "IDNO", "MONEY", "REALPAY", "OTHERNO"};
//
//        String tCaseSQL =
//                "select b.actugetno,c.customername,c.customerno,c.idno,b.sumgetmoney,b.otherno "
//                + " from llhospcase a,ljaget b,llcase c "
//                + " where a.caseno=b.otherno and c.caseno=a.caseno "
//                + " and a.hcno='" + mHCNo
//                + "' and b.othernotype='5' and a.confirmstate='1' "
//                + " and (b.finstate!='FC' or b.finstate is null) ";
//        SSRS tCaseSSRS = new SSRS();
//        ExeSQL tExeSQL = new ExeSQL();
//        tCaseSSRS = tExeSQL.execSQL(tCaseSQL);
//
//        if (tCaseSSRS.getMaxRow() < 1) {
//            buildError("getPrintData", "批次下案件查询失败！");
//            return false;
//        }
//
//        String[] tStrCol;
//
//        for (int i = 1; i <= tCaseSSRS.getMaxRow(); i++) {
//            tStrCol = new String[7];
//            tStrCol[0] = tCaseSSRS.GetText(i, 1);
//            tStrCol[1] = tCaseSSRS.GetText(i, 2);
//            tStrCol[2] = tCaseSSRS.GetText(i, 3);
//            tStrCol[3] = tCaseSSRS.GetText(i, 4);
//
//            double tRealPay = Double.parseDouble(tCaseSSRS.GetText(i, 5));
//            tSumPay += tRealPay;
//            String tPay = new DecimalFormat("0.00").format(tRealPay);
//
//            tStrCol[4] = PubFun.getChnMoney(tRealPay);
//            tStrCol[5] = tPay;
//            tStrCol[6] = tCaseSSRS.GetText(i, 6);
//            tCaseListTable.add(tStrCol);
//        }
          
    //计算批次下案件的总计赔付金额    tSumPay  
          String tCaseSQL =
            "select b.sumgetmoney "
            + " from ljaget b "
            + " where b.otherno='" + mHCNo
            + "' and b.othernotype='5' "
            + " and (b.finstate!='FC' or b.finstate is null) ";
    SSRS tCaseSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tCaseSSRS = tExeSQL.execSQL(tCaseSQL);

    if (tCaseSSRS.getMaxRow() < 1) {
        System.out.println("原有医保通案件计算给付金额");
        String tYLCaseSql = "select lja.sumgetmoney from ljaget lja where lja.otherno in(select caseno from llhospcase where hcno='"+mHCNo+"') and lja.othernotype='5' and (lja.finstate != 'FC' or lja.finstate is null)";
        SSRS tYLCaseSSRS = new SSRS();
        ExeSQL tYLCaseExe = new ExeSQL();
        tYLCaseSSRS = tYLCaseExe.execSQL(tYLCaseSql);
        
        for(int i=1;i<=tYLCaseSSRS.getMaxRow();i++){
            double tRealPay = Double.parseDouble(tYLCaseSSRS.GetText(i, 1));
            tSumPay += tRealPay;
        }
    }else{
    
        for (int i = 1; i <= tCaseSSRS.getMaxRow(); i++) {
            double tRealPay = Double.parseDouble(tCaseSSRS.GetText(i, 1));
            tSumPay += tRealPay;
        }
    }
    
        tMoney = new DecimalFormat("0.00").format(Arith.round(tSumPay, 2));
        tCHNMoney = PubFun.getChnMoney(Arith.round(tSumPay, 2));

        // 生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                         + tSrtTool.getDay() + "日";

        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        tTextTag.add("JetFormType", "lp012");
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTextTag.add("ManageComLength4", printcode);
        tTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch")) {
            tTextTag.add("previewflag", "0");
        } else {
            tTextTag.add("previewflag", "1");
        }

        tTextTag.add("HCNo", mHCNo);
        tTextTag.add("Drawer", tDrawer);
        tTextTag.add("BankName", tBankName);
        tTextTag.add("BankAccNo", tBankAccNo);
        tTextTag.add("Applyer", tAppLayerName);
        //在打印页面上增加"给付号码"
        tTextTag.add("ActuGetNo", tActuGetNo);
        tTextTag.add("PrintTimes", mPrintTimes + 1);
        tTextTag.add("SysDate", SysDate);
        tTextTag.add("Operater", mGlobalInput.Operator);
        tTextTag.add("PayMode", tPayModeName);
        tTextTag.add("PayItem", "理赔");
        tTextTag.add("Money", tMoney);
        tTextTag.add("MoneyCHN", tCHNMoney);

        XmlExport tXmlExport = new XmlExport();
        tXmlExport.createDocument("LLHospGetPrint", "");

        if (tTextTag.size() > 0) {
            tXmlExport.addTextTag(tTextTag);
        }
//        tXmlExport.addListTable(tCaseListTable, tTitle); // 保存申请资料信息
        tXmlExport.addListTable(tEndListTable);
        tXmlExport.outputDocumentToFile("e:\\", "testHZM"); // 输出xml文档到文件

        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mHCNo);
        mLOPRTManagerSchema.setOtherNoType("12");
        mLOPRTManagerSchema.setCode("lp012");
        mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setAgentCode("");
        mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag3("1");

        mResult.clear();
        mResult.addElement(mLOPRTManagerSchema);
        mResult.addElement(tXmlExport);

        return true;
    }


    private boolean dealPrintMag(String cOperate) {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, cOperate);
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }


    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        PayColPrtBL tPayColPrtBL = new PayColPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0003";
        tVData.addElement(tGlobalInput);
        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("H5100131011000001");
        tLOBatchPRTManagerSchema.setStandbyFlag2("1");
        tVData.addElement(tLOBatchPRTManagerSchema);
        tPayColPrtBL.submitData(tVData, "PRINT");
    }

}
