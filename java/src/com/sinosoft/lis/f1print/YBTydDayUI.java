package com.sinosoft.lis.f1print;

/**
 * <p>Title: OtoFDayBalance</p>
 * <p>Description:财务提数对帐单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : yanghao
 * @date:2008-07-21
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class YBTydDayUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;
    public YBTydDayUI() {
    }
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("PRINT")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            VData vData = new VData();
            if (!prepareOutputData(vData)) {
                return false;
            }

            YBTydDayBL tYBTydDayBL = new YBTydDayBL();
            System.out.println("Start FinDayCheckBL Submit ...");
            if (!tYBTydDayBL.submitData(vData, cOperate)) {
                if (tYBTydDayBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tYBTydDayBL.mErrors);
                    return false;
                } else {
                    buildError("submitData",
                               "tOtoFDayBalanceBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            } else {
                mResult = tYBTydDayBL.getResult();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "tOtoFDayBalanceUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mDay);
            vData.add(mGlobalInput);

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /*
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "tYBTydDayUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        YBTydDayUI fin = new YBTydDayUI();
        String ttDay[] = new String[2];
        ttDay[0] = "2006-3-1";
        ttDay[1] = "2006-3-5";
        VData tVData = new VData();
        tVData.addElement(ttDay);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.Operator = "001";
        tVData.addElement(tG);
        fin.submitData(tVData, "PRINT");
    }
}
