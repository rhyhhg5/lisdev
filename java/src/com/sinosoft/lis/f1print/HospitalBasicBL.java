package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 医院基本情况统计
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author liuli
 * @version 1.0
 */
public class HospitalBasicBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LDHospitalSchema mLDHospitalSchema = new LDHospitalSchema();

    public HospitalBasicBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mLDHospitalSchema.setSchema((LDHospitalSchema) cInputData.
                                    getObjectByObjectName(
                                            "LDHospitalSchema", 0));
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "HospitalBasicBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        String MakeDate1 = mLDHospitalSchema.getMakeDate(); //入机日期,起始日期
        String MakeDate2 = mLDHospitalSchema.getModifyDate(); //入机日期,终止日期
        String AssociateDate = mLDHospitalSchema.getLastModiDate(); //借用最后修改日期传递合作时间
        String statflag = mLDHospitalSchema.getUrbanOption(); //标识打印方法"0"为分合作级别统计,"1"为基本情况统计
        String ManageCom = mLDHospitalSchema.getManageCom();
        if(ManageCom.equals("86000000")){
             ManageCom="86";
        }

        String makedate = ""; //医院的入系统时间
        if (MakeDate1 != null && MakeDate2 != null) {
            makedate = " and a.MakeDate between '" + MakeDate1 + "' and '" +
                       MakeDate2 + "'";
            texttag.add("MakeDate1", MakeDate1);
            texttag.add("MakeDate2", MakeDate2);
        } else if (MakeDate1 != null && MakeDate2 == null) {
            makedate = " and a.MakeDate < '" + MakeDate1 + "'";
            texttag.add("MakeDate2", MakeDate1);
        } else if ((MakeDate2 != null) && MakeDate1 == null) {
            makedate = " and a.MakeDate < '" + MakeDate2 + "'";
            texttag.add("MakeDate2", MakeDate2);
        }else{
            MakeDate2 = new PubFun().getCurrentDate();
            texttag.add("MakeDate2", MakeDate2);
        }

        if (AssociateDate == null ) {
            AssociateDate = new PubFun().getCurrentDate();
            texttag.add("AssociateDate", AssociateDate);
        } else {
            texttag.add("AssociateDate", AssociateDate);
        }

        String sqla = "select distinct substr(Comcode,1,4) as comcode from LDcom where 1 = 1 and Sign='1' and comcode like '" +
                      ManageCom + "%' order by comcode with ur";
        SSRS tSSRS = new ExeSQL().execSQL(sqla);
        if (tSSRS == null) {
            return false;
        }
        ListTable tListTable = new ListTable();
        tListTable.setName("table1");
        ListTable tListTable2 = new ListTable();
        tListTable2.setName("table2");

        for (int i = 0; i < tSSRS.getMaxRow(); i++) {

            String mcom = tSSRS.GetText(i + 1, 1); //获取机构号
            if (mcom.equals("86") || mcom.equals("8600")) {
                continue;
            }

            if (statflag.equals("0")) {
                for (int j = 0; j < 3; j++) {
                    String value[] = new String[20]; //存放一条记录
                    for (int h = 0; h < value.length; h++) {
                        value[h] = "0"; //初始化
                    }
                    String sqlname = "select name from LdCom  where ComCode='" +
                                     mcom + "' with ur";
                    SSRS nameSSRS = new ExeSQL().execSQL(sqlname);
                    if (nameSSRS != null) {
                        value[0] = nameSSRS.GetText(1, 1); //机构名称
                    }
                    switch (j) {
                    case 0:
                        value[1] = "推荐医院";
                        break;
                    case 1:
                        value[1] = "指定医院";
                        break;
                    case 2:
                        value[1] = "非指定医院";
                        break;

                    }

                    String sqlb1 =
                            " select communfixflag,count (*) from ldhospital a where exists ("
                            +
                            " select 1 from  ldcomhospital where ((enddate is null and '" +
                            AssociateDate +
                            "' > StartDate)  or (enddate is not null and '" +
                            AssociateDate +
                            "' between StartDate and EndDate)) "
                            +
                            " and a.HospitCode = HospitCode and Associatclass = '" +
                            value[1] + "') and a.Hospitaltype='1' and a.managecom like '" + mcom + "'"
                            + makedate
                            + " and communfixflag in ('0','1') "
                            + " group by communfixflag "
                            + " order by communfixflag with ur";
                    SSRS sb1 = new ExeSQL().execSQL(sqlb1);
                    if (sb1 != null) {
                        for (int m = 0; m < sb1.getMaxRow(); m++) {
                            int aa = Integer.parseInt(sb1.GetText(m + 1, 1));
                            switch (aa) {
                            case 1:
                                value[2] = sb1.GetText(m + 1, 2);
                                break;
                            case 0:
                                value[3] = sb1.GetText(m + 1, 2);
                                break;
                            }
                        }
                    }

                    String sqlb2 =
                            "select LevelCode,count (*) from ldhospital a where exists (" +
                            " select 1 from ldcomhospital where ((enddate is null and '" +
                            AssociateDate +
                            "' > StartDate) or (enddate is not null and '" +
                            AssociateDate +
                            "' between StartDate and EndDate)) " +
                            " and a.HospitCode = HospitCode and Associatclass = '" +
                            value[1] +
                            "') and managecom like '" + mcom + "'" + makedate +
                            " and a.Hospitaltype='1' and LevelCode in ('31','30','21','20','11','10','00') "
                            + " group by LevelCode order by levelcode"
                            + " with ur";
                    SSRS sb2 = new ExeSQL().execSQL(sqlb2);
                    if (sb2 != null) {
                        for (int m = 0; m < sb2.getMaxRow(); m++) {
                            int aa = Integer.parseInt(sb2.GetText(m + 1, 1));
                            switch (aa) {
                            case 31:
                                value[4] = sb2.GetText(m + 1, 2);
                                break;
                            case 30:
                                value[5] = sb2.GetText(m + 1, 2);
                                break;
                            case 21:
                                value[6] = sb2.GetText(m + 1, 2);
                                break;
                            case 20:
                                value[7] = sb2.GetText(m + 1, 2);
                                break;
                            case 11:
                                value[8] = sb2.GetText(m + 1, 2);
                                break;
                            case 10:
                                value[9] = sb2.GetText(m + 1, 2);
                                break;
                            case 00:
                                value[10] = sb2.GetText(m + 1, 2);
                                break;
                            }

                        }
                    }

                    String sqlc =
                            "select count (*) from ldhospital a where exists (" +
                            " select 1 from ldcomhospital where ((enddate is null and '" +
                            AssociateDate +
                            "' > StartDate) or (enddate is not null and '" +
                            AssociateDate +
                            "' between StartDate and EndDate)) " +
                            " and a.HospitCode = HospitCode and Associatclass = '" +
                            value[1] + "')"
                            + "and a.Hospitaltype='1' and managecom like '" + mcom + "'" + makedate +
                            " and LevelCode in ('31','30','21','20','11','10','00') "
                            + " with ur";

                    SSRS sc = new ExeSQL().execSQL(sqlc);
                    if (sc != null) {
                        value[11] = sc.GetText(1, 1);
                    }
                    tListTable.add(value);
                    //测试
                    for (int p = 0; p < value.length; p++) {
                        System.out.println(value[p]);
                    }

                }

            }
            if (statflag.equals("1")) {
                String[] value = new String[20];
                for (int h = 0; h < value.length; h++) {
                    value[h] = "0";
                }
                String sqlname = "select name from LdCom  where ComCode='" +
                                 mcom +
                                 "' with ur";
                SSRS nameSSRS = new ExeSQL().execSQL(sqlname);
                if (nameSSRS != null) {
                    value[0] = nameSSRS.GetText(1, 1); //机构名称
                }

                String sqlb =
                        "select b.Associatclass,count(b.serialno) from ldcomhospital b "
                        + " where ((enddate is null and '" + AssociateDate +
                        "'>= StartDate) or "
                        + " (enddate is not null and '" + AssociateDate +
                        "'  between StartDate and EndDate))"
                        + " and exists (select 1 from ldhospital a where a.hospitcode=b.hospitcode and  a.ManageCom='" +
                        mcom
                        + "' and a.Hospitaltype='1' " + makedate + " )"
                        + " group by b.Associatclass with ur";
                SSRS sb = new ExeSQL().execSQL(sqlb);
                if (sb != null) {
                    for (int m = 0; m < sb.getMaxRow(); m++) {
                        String cla = sb.GetText(m + 1, 1);
                        int aa = 0;
                        if (cla.equals("推荐医院")) {
                            aa = 1;
                        } else if (cla.equals("指定医院")) {
                            aa = 2;
                        } else if (cla.equals("非指定医院")) {
                            aa = 3;
                        }
                        switch (aa) {
                        case 1:
                            value[1] = sb.GetText(m + 1, 2);
                            break;
                        case 2:
                            value[2] = sb.GetText(m + 1, 2);
                            break;
                        case 3:
                            value[3] = sb.GetText(m + 1, 2);
                            break;

                        }
                    }
                }
                String sqlc =
                        "select a.communfixflag ,count(a.hospitcode) from ldhospital a "
                        + " where a.Hospitaltype='1' and a.ManageCom='" + mcom
                        + "'" + makedate +
                        " and a.communfixflag in ('0','1') group by a.communfixflag with ur";
                SSRS sc = new ExeSQL().execSQL(sqlc);
                if (sc != null) {
                    for (int m = 0; m < sc.getMaxRow(); m++) {
                        int aa = Integer.parseInt(sc.GetText(m + 1, 1));
                        switch (aa) {
                        case 1:
                            value[4] = sc.GetText(m + 1, 2);
                            break;
                        case 0:
                            value[5] = sc.GetText(m + 1, 2);
                            break;
                        }
                    }
                }

                String sqld =
                        "select a.levelcode,count(a.hospitcode) from ldhospital a "
                        + "where a.Hospitaltype='1' and a.ManageCom='" + mcom
                        + "'" + makedate
                        + " and a.levelcode in ('31','30','21','20','11','10','00') group by a.levelcode with ur";
                SSRS sd = new ExeSQL().execSQL(sqld);
                if (sd != null) {
                    for (int m = 0; m < sd.getMaxRow(); m++) {
                        int aa = Integer.parseInt(sd.GetText(m + 1, 1));
                        switch (aa) {
                        case 31:
                            value[6] = sd.GetText(m + 1, 2);
                            break;
                        case 30:
                            value[7] = sd.GetText(m + 1, 2);
                            break;
                        case 21:
                            value[8] = sd.GetText(m + 1, 2);
                            break;
                        case 20:
                            value[9] = sd.GetText(m + 1, 2);
                            break;
                        case 11:
                            value[10] = sd.GetText(m + 1, 2);
                            break;
                        case 10:
                            value[11] = sd.GetText(m + 1, 2);
                            break;
                        case 00:
                            value[12] = sd.GetText(m + 1, 2);
                            break;
                        }
                    }
                }

                String sqle = "select count(*) from ldhospital a "
                              + "where a.Hospitaltype='1' and  a.ManageCom='" + mcom
                              + "'" + makedate + " with ur";
                SSRS se = new ExeSQL().execSQL(sqle);
                if (se != null) {
                    value[13] = se.GetText(1, 1);
                }

                //测试
                for (int p = 0; p < value.length; p++) {
                    System.out.println(value[p]);
                }
                tListTable.add(value);
            }
        }

        //分合作级别的全系统统计
        int[] heji = new int[15];
        if (statflag.equals("0")) {

            for (int j = 0; j < 3; j++) {
                String whole[] = new String[20];
                for (int o = 0; o < whole.length; o++) {
                    whole[o] = "0";
                }

                whole[0] = "全系统";
                switch (j) {
                case 0:
                    whole[1] = "推荐医院";
                    break;
                case 1:
                    whole[1] = "指定医院";
                    break;
                case 2:
                    whole[1] = "非指定医院";
                    break;
                }

                String sqlaa =
                        " select communfixflag,count (*) from ldhospital a where exists ("
                        +
                        " select 1 from  ldcomhospital  where ((enddate is null and '" +
                        AssociateDate +
                        "' > StartDate) or (enddate is not null and '" +
                        AssociateDate +
                        "' between StartDate and EndDate)) "
                        +
                        " and a.HospitCode = HospitCode and Associatclass = '" +
                        whole[1] + "')" + " and a.Hospitaltype='1' and managecom like '86%'"
                        + makedate + " and communfixflag in ('0','1') "
                        + " group by communfixflag "
                        + " order by communfixflag with ur";
                SSRS sqaa = new ExeSQL().execSQL(sqlaa);
                if (sqaa != null) {
                    for (int m = 0; m < sqaa.getMaxRow(); m++) {
                        int aa = Integer.parseInt(sqaa.GetText(m + 1, 1));
                        switch (aa) {
                        case 1:
                            whole[2] = sqaa.GetText(m + 1, 2);
                            heji[2] = heji[2] + Integer.parseInt(whole[2]);
                            break;
                        case 0:
                            whole[3] = sqaa.GetText(m + 1, 2);
                            heji[3] = heji[3] + Integer.parseInt(whole[3]);
                            break;
                        }
                    }
                }
                String sqlbb =
                        "select LevelCode,count (*) from ldhospital a where exists (" +
                        " select 1 from ldcomhospital where ((enddate is null and '" +
                        AssociateDate +
                        "' > StartDate) or (enddate is not null and '" +
                        AssociateDate +
                        "' between StartDate and EndDate)) " +
                        " and a.HospitCode = HospitCode and Associatclass = '" +
                        whole[1] +
                        "')"
                        + " and managecom like '86%'" + makedate +
                        " and a.Hospitaltype='1' and LevelCode in ('31','30','21','20','11','10','00') "
                        + " group by LevelCode order by levelcode"
                        + " with ur";
                SSRS sqbb = new ExeSQL().execSQL(sqlbb);
                if (sqbb != null) {
                    for (int m = 0; m < sqbb.getMaxRow(); m++) {
                        int aa = Integer.parseInt(sqbb.GetText(m + 1, 1));
                        switch (aa) {
                        case 31:
                            whole[4] = sqbb.GetText(m + 1, 2);
                            heji[4] = heji[4] + Integer.parseInt(whole[4]);
                            break;
                        case 30:
                            whole[5] = sqbb.GetText(m + 1, 2);
                            heji[5] = heji[5] + Integer.parseInt(whole[5]);
                            break;
                        case 21:
                            whole[6] = sqbb.GetText(m + 1, 2);
                            heji[6] = heji[6] + Integer.parseInt(whole[6]);
                            break;
                        case 20:
                            whole[7] = sqbb.GetText(m + 1, 2);
                            heji[7] = heji[7] + Integer.parseInt(whole[7]);
                            break;
                        case 11:
                            whole[8] = sqbb.GetText(m + 1, 2);
                            heji[8] = heji[8] + Integer.parseInt(whole[8]);
                            break;
                        case 10:
                            whole[9] = sqbb.GetText(m + 1, 2);
                            heji[9] = heji[9] + Integer.parseInt(whole[9]);
                            break;
                        case 00:
                            whole[10] = sqbb.GetText(m + 1, 2);
                            heji[10] = heji[10] + Integer.parseInt(whole[10]);
                            break;
                        }
                    }
                }
                String sqlcc =
                        "select count (*) from ldhospital a where exists (" +
                        " select 1 from ldcomhospital where ((enddate is null and '" +
                        AssociateDate +
                        "' > StartDate) or (enddate is not null and '" +
                        AssociateDate +
                        "' between StartDate and EndDate)) " +
                        " and a.HospitCode = HospitCode and Associatclass = '" +
                        whole[1] + "')"
                        + " and a.Hospitaltype='1' and managecom like '86%'" + makedate
                        + " with ur";
                SSRS sqcc = new ExeSQL().execSQL(sqlcc);
                if (sqcc != null) {
                    whole[11] = sqcc.GetText(1, 1);
                    heji[11] = heji[11] + Integer.parseInt(whole[11]);
                }
                tListTable2.add(whole);
            }
        }
        //分合作级的合计
        for (int m = 0; m < heji.length; m++) {
            texttag.add("heji" + m, heji[m] + "");
        }

        //全系统统计的合计
        if (statflag.equals("1")) {
            String whole[] = new String[20];
            for (int o = 0; o < whole.length; o++) {
                whole[o] = "0";
            }

            String sqlb =
                    "select b.Associatclass,count(b.serialno) from ldcomhospital b "
                    + " where ((enddate is null and '" + AssociateDate +
                    "'>= StartDate) or "
                    + " (enddate is not null and '" + AssociateDate +
                    "'  between StartDate and EndDate))"
                    + " and b.hospitcode in (select a.hospitcode from ldhospital a where a.ManageCom like '86%' and a.Hospitaltype='1'"
                    + makedate + " )"
                    + " group by b.Associatclass with ur";
            SSRS sb = new ExeSQL().execSQL(sqlb);
            if (sb != null) {
                for (int m = 0; m < sb.getMaxRow(); m++) {
                    String cla = sb.GetText(m + 1, 1);
                    int aa = 0;
                    if (cla.equals("推荐医院")) {
                        aa = 1;
                    } else if (cla.equals("指定医院")) {
                        aa = 2;
                    } else if (cla.equals("非指定医院")) {
                        aa = 3;
                    }
                    switch (aa) {
                    case 1:
                        whole[1] = sb.GetText(m + 1, 2);
                        break;
                    case 2:
                        whole[2] = sb.GetText(m + 1, 2);
                        break;
                    case 3:
                        whole[3] = sb.GetText(m + 1, 2);
                        break;

                    }
                }
            }
            String sqlc =
                    "select a.communfixflag ,count(a.hospitcode) from ldhospital a "
                    + " where a.Hospitaltype='1' and a.ManageCom like '86%'" + makedate +
                    " and a.communfixflag in ('0','1') group by a.communfixflag with ur";
            SSRS sc = new ExeSQL().execSQL(sqlc);
            if (sc != null) {
                for (int m = 0; m < sc.getMaxRow(); m++) {
                    int aa = Integer.parseInt(sc.GetText(m + 1, 1));
                    switch (aa) {
                    case 1:
                        whole[4] = sc.GetText(m + 1, 2);
                        break;
                    case 0:
                        whole[5] = sc.GetText(m + 1, 2);
                        break;
                    }
                }
            }

            String sqld =
                    "select a.levelcode,count(a.hospitcode) from ldhospital a "
                    + "where a.ManageCom like '86%'" + makedate
                    + " and a.Hospitaltype='1' and a.levelcode in ('31','30','21','20','11','10','00') group by a.levelcode with ur";
            SSRS sd = new ExeSQL().execSQL(sqld);
            if (sd != null) {
                for (int m = 0; m < sd.getMaxRow(); m++) {
                    int aa = Integer.parseInt(sd.GetText(m + 1, 1));
                    switch (aa) {
                    case 31:
                        whole[6] = sd.GetText(m + 1, 2);
                        break;
                    case 30:
                        whole[7] = sd.GetText(m + 1, 2);
                        break;
                    case 21:
                        whole[8] = sd.GetText(m + 1, 2);
                        break;
                    case 20:
                        whole[9] = sd.GetText(m + 1, 2);
                        break;
                    case 11:
                        whole[10] = sd.GetText(m + 1, 2);
                        break;
                    case 10:
                        whole[11] = sd.GetText(m + 1, 2);
                        break;
                    case 00:
                        whole[12] = sd.GetText(m + 1, 2);
                        break;
                    }
                }
            }

            String sqle =
                    "select count(*) from ldhospital a "
                    + " where a.Hospitaltype='1' and a.ManageCom like '86%'"
                    + makedate + " with ur";
            SSRS se = new ExeSQL().execSQL(sqle);
            if (se != null) {
                whole[13] = se.GetText(1, 1);
            }

            //测试
            for (int p = 0; p < whole.length; p++) {
                System.out.println(whole[p]);
            }
            tListTable2.add(whole);
        }

        XmlExport xmlexport = new XmlExport();
        if (statflag.equals("1")) {
            xmlexport.createDocument("HostpitalBasic.vts", "printer"); //全系统
        } else if (statflag.equals("0")) {
            xmlexport.createDocument("HostpitalBasicAss.vts", "printer"); //分合作级别
        }
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tListTable, new String[20]);
        if(ManageCom.equals("86")||ManageCom.equals("86000000")){
            xmlexport.addDisplayControl("displayall");
            xmlexport.addListTable(tListTable2, new String[20]);
        }
        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public static void main(String[] args) {

        HospitalBasicBL tHospitalBasicBL = new HospitalBasicBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
//            LLConsultSchema tLLConsultSchema = new LLConsultSchema();
//            tLLConsultSchema.setConsultNo("Z0000050511000001");
//            tVData.addElement(tLLConsultSchema);
        tHospitalBasicBL.getPrintData();
        VData vdata = tHospitalBasicBL.getResult();

    }
}
