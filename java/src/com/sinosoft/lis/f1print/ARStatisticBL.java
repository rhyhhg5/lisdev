package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

public class ARStatisticBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public ARStatisticBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName(
                "OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String MakeDate = PubFun.getCurrentDate();
        System.out.println("zhuzhu" + QYType);
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;
        if (QYType.equals("1")) {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D3 ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("   '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D3 ");
                        sql.append(" from  lcpol where renewcount<=0 and conttype='1'");
                        sql.append(" ) as X order by A with ur");
 //                       System.out.println(sql.toString());
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select RiskCode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(distinct (insuredno)) from lcpol where renewcount<=0 and  prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                    sql.append(" (select count(distinct (insuredno)) from lbpol where renewcount<=0 and  prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                    sql.append(" (select count(distinct (insuredno)) from lbpol where renewcount<=0 and  prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C3, ");

                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D1 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D2 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D3 ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                    sql.append(" from ");
                    sql.append(" ( select  '总计' A,'总计' B, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C1, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C2, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C3, ");

                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D1 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D2 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D3 ");
                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
//                    System.out.println(sql.toString());
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M = tExeSQL.execSQL("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                    	String Mng = tSSRS_M.GetText(i, 1);
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D3 ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("   '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D3 ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");

                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D3 ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("   '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "')  ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) D3 ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第二个Sql语句~~~~~" + Mng);
                    }
                }

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D3 ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D3 ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();

            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = {"A", "B", "C", "D", "F"};
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++) {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("tbARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                             "月" +
                             tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" +
                           OperatorManagecom +
                           "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0) {
                xml.addTextTag(texttag);
            }
            xml.addListTable(tListTable, title);

            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);

        } else if (QYType.equals("2")) {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                        sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( select  '总计' A,'总计' B, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) G ,");
                        sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) H ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='a' ) C , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='a' ) D, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='1' ) E , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='1' ) F, ");
                    sql.append(" (select count(insuredno) from lcpol where  conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1')  and riskcode=Z.riskcode) G ,");
                    sql.append(" (select sum(prem) from lcpol where  conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1')  and riskcode=Z.riskcode) H ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append("  '总计' A,'总计' B, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='a' )C , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno  and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='a' ) D, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='1' ) E , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno  and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='1' ) F, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and appflag<>'1' and uwflag in('4','9') and conttype='1') ) G ,");
                    sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and appflag<>'1' and uwflag in('4','9') and conttype='1') ) H");
                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M = tExeSQL.execSQL("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                    	String Mng = tSSRS_M.GetText(i, 1);
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                        sql.append(" (select sum(prem) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( select  '总计' A,'总计' B, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) G ,");
                        sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) H ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                        sql = new StringBuffer();
                        sql.append(" select '总计',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                        sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( select  '总计' A,'总计' B, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') F ,");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) G ,");
                        sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng + "') ) H ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第二个Sql语句~~~~~" + Mng);
                    }
                }

            } else if (Managecom.length() == 8) {
                SSRS tSSRS_M;
                 String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                  Managecom +
                                                  "' order by ComCode");
                 sql = new StringBuffer();
                 sql.append(
                         " select (select showname from ldcom where sign='1' and comcode= '" +
                         Managecom +
                         "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                 sql.append(" from ");
                 sql.append(" ( ");
                 sql.append(" select ");
                 sql.append(
                         " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                 sql.append(
                         " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') C , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') D, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') E , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') F, ");
                 sql.append(" (select count(distinct insuredno) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                 sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                 sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                 sql.append(" ) as X ");
                 sql.append(" union ");
                 sql.append(
                         " select (select showname from ldcom where sign='1' and comcode= '" +
                         Managecom +
                         "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                 sql.append(" from ");
                 sql.append(" ( select  '总计' A,'总计' B, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') C , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') D, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') E , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') F, ");
                 sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') ) G ,");
                 sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') ) H ");
                 sql.append(" from  lcpol where conttype='1'");
                 sql.append(" ) as X order by A with ur");
                 ssrs = tExeSQL.execSQL(sql.toString());
                 if (ssrs != null) {
                     if (ssrs.getMaxRow() > 0) {
                         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                             result.add(ssrs.getRowData(m));
                         }
                     }
                 }
                 ssrs = null;
                 sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = {"A", "B", "C", "D", "F", "E", "H", "M", "N", };
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++) {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("ARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
//生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                             "月" +
                             tSrtTool.getDay() + "日";

//WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" +
                           OperatorManagecom +
                           "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0) {
                xml.addTextTag(texttag);
            }
            xml.addListTable(tListTable, title);

            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);

        } else {
            //承保
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and   contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");

                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("  '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E3 ");

                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select RiskCode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                    sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                    sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append("  '总计' A,'总计' B, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C1, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C2, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C3, ");
                    sql.append(" (select sum(prem) from lcpol where contno in(select contno from lccont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D1, ");
                    sql.append(" (select sum(prem) from lbpol where contno in(select contno from lccont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D2, ");
                    sql.append(" (select sum(prem) from lbpol where contno in(select contno from lbcont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D3, ");

                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E1, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E2, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E3 ");


                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M = tExeSQL.execSQL("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                    	String Mng = tSSRS_M.GetText(i, 1);
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("   '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                    StartDate + "' and '" + EndDate +
                                    "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                    Managecom + "')*2)='" + Mng +
                                    "' ) and appflag='1'  and conttype='1') E1, ");
                         sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                    StartDate + "' and '" + EndDate +
                                    "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                    Managecom + "')*2)='" + Mng +
                                    "' ) and appflag='1'  and conttype='1') E2, ");
                         sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                    StartDate + "' and '" + EndDate +
                                    "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                    Managecom + "')*2)='" + Mng +
                                    "' ) and appflag='1'  and conttype='1') E3 ");


                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");

                        sql = new StringBuffer();
                        sql.append(" select '总计',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select '总计',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("  '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E3 ");


                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第二个Sql语句~~~~~" + Mng);
                    }
                }

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");

                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E1, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E2, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E3 ");

                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = {"A", "B", "C", "D", "F", "E"};
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++) {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("chbARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                             "月" +
                             tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" +
                           OperatorManagecom +
                           "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0) {
                xml.addTextTag(texttag);
            }

            //保存信息
            xml.addListTable(tListTable, title);

            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);

        }

        return true;
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        ARStatisticBL tWorkEfficiencyBL = new ARStatisticBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("OperatorManagecom", "86");
        mTransferData.setNameAndValue("Managecom", "86");
        mTransferData.setNameAndValue("StartDate", "2005-1-1");
        mTransferData.setNameAndValue("EndDate", "2005-11-1");
        mTransferData.setNameAndValue("QYType", "1");
        tVData.add(mTransferData);
        tWorkEfficiencyBL.submitData(tVData, "PRINT");
        VData vData = tWorkEfficiencyBL.getResult();

    }
}
