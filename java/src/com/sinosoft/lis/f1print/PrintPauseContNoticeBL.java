package com.sinosoft.lis.f1print;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintPauseContNoticeBL {
	public CErrors mErrors = new CErrors();
	private String mContNo;
	private String mManageCom;
	private TransferData mTransferData = null;
	private GlobalInput mGlobalInput = null;
	private TextTag textTag = new TextTag();
	private XmlExport mXmlExport = new XmlExport();

	public PrintPauseContNoticeBL() {

	}

	public XmlExport getXmlExport(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			return null;
		}

		if (!dealData()) {
			return null;
		}

		return mXmlExport;
	}

	private boolean getInputData(VData data) {
		mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput",
				0);

		mTransferData = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);
		if (mGlobalInput == null || mGlobalInput.Operator == null
				|| mGlobalInput.Operator.equals("")) {
			mErrors.addOneError("传入的数据不完整。mGlobalInput");
			return false;
		}
		if (mTransferData == null) {
			mErrors.addOneError("传入的数据不完整mTransferData。");
			return false;
		}
		mContNo = (String) mTransferData.getValueByName("ContNo");
		
		mManageCom = (String) mTransferData.getValueByName("ManageCom");
		//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&& ManageCom ="+mManageCom);

		return true;
	}

	private boolean dealData() {
		if(!setCommonInfo())
        {
            return false;
        }
		mXmlExport.createDocument("PPauseContNotice.vts", "printer");
		mXmlExport.addTextTag(textTag);
		return true;
	}

	private boolean setCommonInfo() {
		LDComDB tLDComDB = new LDComDB();
		tLDComDB.setComCode(mManageCom);
		tLDComDB.getInfo();

		//System.out.println("Zipcode ： " + tLDComDB.getServicePostZipcode());
		
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Operator", mGlobalInput.Operator);
        textTag.add("ContNo",mContNo );
        
        String agentCode = getAgentCode();
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(agentCode);
        tLaAgentDB.getInfo();
        textTag.add("AgentName", StrTool.cTrim(tLaAgentDB.getName()));
        textTag.add("AgentCode", StrTool.cTrim(tLaAgentDB.getAgentCode()));
        
        String agentPhone = "";
        String temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null"))
        {
            agentPhone = tLaAgentDB.getPhone();
        } else
        {
            agentPhone = temPhone;
        }
        textTag.add("Phone", StrTool.cTrim(agentPhone));
        
        //获取并设置投保人信息
        String appntSql="select * from lccont where contno='"+mContNo+"'";
        LCContSchema tLCContSchema=new LCContSchema();
        LCContDB tLCContDB= new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContSet=tLCContDB.executeQuery(appntSql);
        tLCContSchema = tLCContSet.get(1);
        String tAppntNo=tLCContSchema.getAppntNo();
        textTag.add("AppntNo",tAppntNo);
        textTag.add("AppntName",tLCContSchema.getAppntName() );
        //System.out.println("&&&&&&&&&&&&&&&&&&&&&&&& tAppntNo ="+tAppntNo);
       
        LCAppntSchema tLCAppntSchema=new LCAppntSchema();
        String sql="select * from lcappnt where appntNo='"+tAppntNo+"'";
        LCAppntSet tLCAppntSet=new LCAppntSet();
        LCAppntDB tLCAppntDB=new LCAppntDB();
        tLCAppntSet =tLCAppntDB.executeQuery(sql);
        tLCAppntSchema =tLCAppntSet.get(1);
        String tAddressNo = tLCAppntSchema.getAddressNo();
        //System.out.println("&&&&&&&&&&&&&&&&&&&&&&&& tAddressNo ="+tAddressNo);
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        LCAddressSet tLCAddressSet=new LCAddressSet();
        LCAddressDB tLCAddressDB =new LCAddressDB();
        String tAddressSql="select * from LCAddress where CustomerNo='"+tAppntNo+"' and AddressNo='"+tAddressNo+"'" ;
        
        //System.out.println("tAddressSql : " + tAddressSql);
        
        tLCAddressSet=tLCAddressDB.executeQuery(tAddressSql);
        tLCAddressSchema =tLCAddressSet.get(1);
        
        //System.out.println("HomeZipCode : " + tLCAddressSchema.getHomeZipCode());
        //System.out.println("HomeAddress : " + tLCAddressSchema.getHomeAddress());
        
        
        textTag.add("ZipCode",tLCAddressSchema.getHomeZipCode() );
        textTag.add("Address",tLCAddressSchema.getHomeAddress() );
        textTag.add("Mobile",tLCAddressSchema.getMobile() );
        
        
        //查询失效开始日期
        SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	String LCContStateSql = "select startDate from LCContState where 1=1 "
            					 + " and StateType='Available' "
            					 + " and State='1'"
            					 + " and StateReason='02' "
            					 + " and GrpContNo='000000'"
            					 + " and polno='000000'"
            					 + " and contNo='" + mContNo + "'" ;
    	tSSRS = tExeSQL.execSQL(LCContStateSql);
    	if(tSSRS != null)
    	{
    		String EndDate= tSSRS.GetText(1, 1);
    		textTag.add("EndDate",EndDate);	
    	}
        
        
      //业务员机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLaAgentDB.getAgentGroup());
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", StrTool.cTrim(tLABranchGroupDB.getName()));
		return true;
	}
	private String getAgentCode(){
		String tAgentCode="";
		String tContNo = mContNo;
		String sql ="select AgentCode "
            + "from LCCont "
            + "where ContNo = '" + tContNo + "' "
            + "union "
            + "select AgentCode "
            + "from LBCont "
            + "where ContNo = '" + tContNo + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		tAgentCode = tExeSQL.getOneValue(sql);
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&& AgentCode ="+tAgentCode);
		if(tAgentCode!= "" && !tAgentCode.equals("null"))
        {
            return tAgentCode;
        }
		return tAgentCode;
	}
}
