package com.sinosoft.lis.f1print;

/**
 * <p>Title: FenClaimDayBalanceExcelBL</p>
 * <p>Description:X5理赔应付日结单 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author :  yan
 * @date:2013-09-16
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FenClaimDayBalanceExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; // 获取时间
	//取得文件路径
	private String tOutXmlPath="";
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

	public FenClaimDayBalanceExcelBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		FenClaimDayBalanceBL tC = new FenClaimDayBalanceBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath=(String)cInputData.get(2);
		
		System.out.println("打印日期："+mDay[0]+"至"+mDay[1]);
		
		if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FenClaimDayBalanceExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
     /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){
        int no=6;  //初始打印行数

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
 
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//        tServerPath= "E:/lisdev/ui/";      
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);

        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[] { 
                "LPSSYLJF", // 死伤医疗给付
                "LPPKZC", // 赔款支出
//              "LPPKZC-W", // 天津外包-赔款支出
                "LPTX", // 特需理赔
                "LPKCF", // 勘查费
                "XTXLPSSYLJF", // 新特需死伤医疗给付
                "XTXLPPKZC", // 新特需赔款支出
                "XTXLPTX", // 新特需理赔
                "WNLPSSYLJF", // 万能死伤医疗给付-非账户
                "WNLPPKZC",  // 万能死伤医疗给付-账户
                //"WNLPTX", // 万能理赔
                //"YFPK",    //预付赔款-给付确认
                "YFPK_SQ",  //预付赔款-申请
                "SBYDCKF"  //社保异地查勘费
        };

        for (int i = 0; i < getTypes.length; i++) {
        	System.out.println((getTypes[i])+"快点啦第一次");
            String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
            System.out.println((getTypes[i])+"快点啦第二次");
            tSSRS = tExeSQL.execSQL(msql);
            System.out.println((getTypes[i])+"快点啦第三次");
            int maxrow=tSSRS.getMaxRow();
            System.out.println((getTypes[i])+"快点啦，这个应该是没用的");

            no=no+maxrow+2;
        }
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }
    
	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath= "E:/lisdev/ui/";

		String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		
		int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+10][5];
		mToExcel[0][0] = "理赔应付日结单";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mDay[0]+"至"+mDay[1];
		mToExcel[2][0] = "统计机构：";
		mToExcel[2][1] = manageCom;
		mToExcel[5][0] = "类型";
		mToExcel[5][1] = "渠道";
		mToExcel[5][2] = "案件号码";
		mToExcel[5][3] = "保单号码";
		mToExcel[5][4] = "金额";
		
		int no=6;  //初始打印行数
		
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"LPSSYLJF", // 死伤医疗给付
				"LPPKZC", // 赔款支出
//		  "LPPKZC-W", // 天津外包-赔款支出
				"LPTX", // 特需理赔
				"LPKCF", // 勘查费
				"XTXLPSSYLJF", // 新特需死伤医疗给付
				"XTXLPPKZC", // 新特需赔款支出
				"XTXLPTX", // 新特需理赔
				"WNLPSSYLJF", // 万能死伤医疗给付-非账户
				"WNLPPKZC",  // 万能死伤医疗给付-账户
				//"WNLPTX", // 万能理赔
				//"YFPK",    //预付赔款-给付确认
				"YFPK_SQ",  //预付赔款-申请
				"SBYDCKF"  //社保异地查勘费
		};

		for (int i = 0; i < getTypes.length; i++) {
			String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
			System.out.println((getTypes[i])+"---------");
			tSSRS = tExeSQL.execSQL(msql);
			System.out.println((getTypes[i])+"---------");
			int maxrow=tSSRS.getMaxRow();
			double sumMoney=0.00;
			for (int row = 1; row <= maxrow; row++)
            {
                for (int col = 1; col <=5; col++)
                { 
                	if (col==5) {
                		mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						sumMoney=sumMoney+Double.parseDouble(tSSRS.GetText(row,col));
					}else {
						mToExcel[no+row-1][col - 1] = tSSRS.GetText(row, col);  
					}
                    }
                }
			mToExcel[no+maxrow][0] =getChineseNameWithPY(getTypes[i]);
			System.out.println(getChineseNameWithPY(getTypes[i])+"---------");
			mToExcel[no+maxrow][3] ="总计金额：";
			mToExcel[no+maxrow][4] =new DecimalFormat("0.00").format(Double.valueOf(sumMoney));
			no=no+maxrow+2;
		}
		//添加制表员和审核员
		mToExcel[no+1][0] = "制表员：";
		mToExcel[no+1][2] = "审核员：";
		try
        {
            System.out.println("X5--tOutXmlPath:"+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X5文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }
	return true;
}
	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String getChineseNameWithPY(String pName) {
		String result ="";
		if ("LPSSYLJF".equals(pName)) {
			result = "死伤医疗给付 合计";
		} else if ("LPPKZC".equals(pName)) {
			result = "赔款支出 合计";
//	} else if ("LPPKZC-W".equals(pName)) {
//		result = "天津外包-赔款支出 合计";
		} else if ("LPTX".equals(pName)) {
			result = "特需理赔 合计";
		} else if ("LPKCF".equals(pName)) {
			result = "勘查费 合计";
		}else if ("XTXLPSSYLJF".equals(pName)) {
			result = "新特需死伤医疗给付 合计";
		} else if ("XTXLPPKZC".equals(pName)) {
			result = "新特需赔款支出 合计";
		} else if ("XTXLPTX".equals(pName)) {
			result = "新特需理赔 合计";
		}else if ("WNLPSSYLJF".equals(pName)) {
			result = "万能理赔-非账户 合计";
		} else if ("WNLPPKZC".equals(pName)) {
			result = "万能理赔-账户 合计";
		} else if ("WNLPTX".equals(pName)) {
			result = "万能理赔 合计";
		} else if ("YFPK".equals(pName)) {
			result = "预付赔款给付确认 合计";
		}else if ("YFPK_SQ".equals(pName)) {
			result = "预付赔款申请 合计";
		}else if ("SBYDCKF".equals(pName)) {
			result = "社保异地查勘费 小计";
		}
		return result;
	}

}
