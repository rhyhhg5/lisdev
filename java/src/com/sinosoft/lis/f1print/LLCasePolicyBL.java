package com.sinosoft.lis.f1print;

import java.io.InputStream;

import utils.system;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.RSWrapper;

public class LLCasePolicyBL {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";

    private String mStartDate = "";

    private String mEndDate = "";
    
    private String mTZStartDate = "";

    private String mTZEndDate = "";
    
    private String mJFStartDate = "";

    private String mJFEndDate = "";

    private String mOperator = "";

    private String tCurrentDate = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private PubFun mPubFun = new PubFun();

    private ListTable mListTable = new ListTable();

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mManageComNam = "";

    private String mFileNameB = "";

    private String mMakeDate = "";

    private String mContType = "";

    private String mContNo = "";

    private String mRgtNo = "";
    
    private String mRiskCode = "";
    /** 统计类型 */
	private String mStatsType = "";

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                           + this.mManageCom);
        
        if(!checkInputData())
        {
        	this.bulidError("submitData", "数据校验不通过，请输入正确的结案、给付起止日期");
            return false;
        }
        // 进行数据查询
        if (!queryData()) {
            return false;
        } else {
//            TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("tFileNameB", mFileNameB);
//            tTransferData.setNameAndValue("tMakeDate", mMakeDate);
//            tTransferData.setNameAndValue("tOperator", mOperator);
//            LLPrintSave tLLPrintSave = new LLPrintSave();
//            VData tVData = new VData();
//            tVData.addElement(tTransferData);
//            if (!tLLPrintSave.submitData(tVData, "")) {
//                this.bulidErrorB("LLPrintSave-->submitData", "数据不完整");
//                return false;
//            }
        }

        System.out.println("dayinchenggong1232121212121");

        return true;
    }

    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLCasePolicy";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
     *
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        // tCurrentDate = mPubFun.getCurrentDate();
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据 三个
            mOperator = (String) mTransferData.getValueByName("tOperator");
            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
            mContType = (String) mTransferData.getValueByName("tContType");
            mContNo = (String) mTransferData.getValueByName("tContNo");
            mRgtNo = (String) mTransferData.getValueByName("RgtNo");
            mRiskCode = (String) mTransferData.getValueByName("RiskCode");
            System.out.println("mContType:" + mContType);
            this.mManageCom = (String) mTransferData
                              .getValueByName("tManageCom");

            System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                               + this.mManageCom);
            this.mStartDate = (String) mTransferData
                              .getValueByName("tStartDate");//结案起始日期
            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案截止日期
            
            this.mJFStartDate = (String) mTransferData.getValueByName("tJFStartDate");//给付起始日期
            this.mJFEndDate = (String) mTransferData.getValueByName("tJFEndDate");//给付截止日期
            
            this.mTZStartDate = (String) mTransferData.getValueByName("tTZStartDate");//通知起始日期
            this.mTZEndDate = (String) mTransferData.getValueByName("tTZEndDate");//通知截止日期
            this.mStatsType = (String) mTransferData.getValueByName("StatsType");// 统计类别
            
            
            
            if (mManageCom == null || mManageCom.equals("")) {
                this.mManageCom = "86";
            } 
            /*if (mStartDate == null || mStartDate.equals("")) {
                this.mStartDate = getYear(tCurrentDate) + "-01-01";
            }
            if (mEndDate == null || mEndDate.equals("")) {
                this.mEndDate = getYear(tCurrentDate) + "-12-31";
            }*/

            System.out
                    .println(
                            "XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
                            + mManageCom);
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }
    /**
     * 对传入的数据进行校验
     *
     * @return boolean
     */
    private boolean checkInputData() {
    	if (mStartDate == null || mStartDate.equals(""))
    	{
    		if(mEndDate!=null&&!mEndDate.equals(""))
    			return false;
    	}
    	else
    	{
    		if(mEndDate == null || mEndDate.equals(""))
    			return false;
    	}
    	if (mJFStartDate == null || mJFStartDate.equals(""))
    	{
    		if(mJFEndDate!=null&&!mJFEndDate.equals(""))
    			return false;
    	}
    	else
    	{
    		if(mJFEndDate == null || mJFEndDate.equals(""))
    			return false;
    	}
    	if (mTZStartDate == null || mTZStartDate.equals(""))
    	{
    		if(mTZEndDate!=null&&!mTZEndDate.equals(""))
    			return false;
    	}
    	else
    	{
    		if(mTZEndDate == null || mTZEndDate.equals(""))
    			return false;
    	}
    	
    	if((mStartDate == null || mStartDate.equals(""))&&(mJFStartDate == null || mJFStartDate.equals(""))&&(mTZStartDate == null || mTZStartDate.equals("")))
    		return false;
    	return true;
    }
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LLCasePolicy1.vts", "printer");

        String tMakeDate = "";
        tMakeDate = mPubFun.getCurrentDate();

        System.out.print("理赔明细报表");
        String sql = "select name from ldcom where comcode='" + mManageCom
                     + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String mManageComName = tExeSQL.getOneValue(sql);
        String tContType = "";
        if ("".equals(mContType)) {
            tContType = "";
        } else if ("I".equals(mContType)) {
            tContType = "个险";
        } else {
            tContType = "团险";
        }
      
         
        tTextTag.add("ManageCom", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("JFStartDate", mJFStartDate);
        tTextTag.add("TZStartDate", mTZStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("JFEndDate", mJFEndDate);
        tTextTag.add("TZEndDate", mTZEndDate);
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("operator", mOperator);
        tTextTag.add("ContType", tContType);
        tTextTag.add("ContNo", mContNo);
        tTextTag.add("RgtNo", mRgtNo);
        tTextTag.add("RgtNo", mRgtNo); //批次号
        
        mMakeDate = tMakeDate;
        System.out.println("1212121" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }
        mXmlExport.addTextTag(tTextTag);
        String[] title = {"","","","","",
        		"","","","","","","","","","",
        		"","","","","","","","","","",
        		"","","","","","","","","","",
        		"","","","","","","","","","",
        		"","","","","","","","","","",
        		"","","","","","","","","","",
        		"","","","","","","",""};
        mListTable.setName("FAULT");
        if (!getDataList()) {
            return false;
        }
        if (mListTable == null || mListTable.equals("")) {
            bulidError("queryData", "没有符合条件的信息!");
            return false;
        }

        System.out.println("ManageCom:"+mManageComName);
        System.out.println("StartDate:"+mStartDate);
        System.out.println("EndDate:"+mEndDate);
        System.out.println("Operator:"+mOperator);
        System.out.println("ContType:"+tContType);
        System.out.println("ContNo:"+mContNo);
        System.out.println("RgtNo:"+mRgtNo);
        //System.out.println("StatsName:"+tStatsName);
        
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();
        mResult.addElement(mXmlExport);

        return true;
    }

    private String getYear(String pmDate) {
        String mYear = "";
        String tSQL = "";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        mYear = tSSRS.GetText(1, 1);
        return mYear;
    }
    /**
     * 根据结案、给付起止日期参数有无情况来生成sql条件
     * @return boolean
     */
    private String getCondition() {
        String sqlCondition = "";
        if(mStartDate!=null&&!"".equals(mStartDate))//结案起始日期不为空意味着结案日期也不为空，因为否则校验不通过
        {
        	sqlCondition+=" and a.endcasedate between '" + mStartDate + "' and '" + mEndDate + "' ";
        }
        if(mJFStartDate!=null&&!"".equals(mJFStartDate))//结案起始日期不为空意味着结案日期也不为空，因为否则校验不通过
        {
        	sqlCondition+=" and exists (select 1 from ljagetclaim where otherno=a.caseno and othernotype='5' and feeoperationtype<>'FC' and "+
        	" confdate between '" + mJFStartDate + "' and '" + mJFEndDate + "') ";
        }
        if(mTZStartDate!=null&&!"".equals(mTZStartDate))//通知起始日期不为空意味着结案日期也不为空，因为否则校验不通过
        {
        	sqlCondition+=" and exists (select 1 from ljagetclaim where otherno=a.caseno and othernotype='5' and feeoperationtype<>'FC' and "+
        	" makedate between '" + mTZStartDate + "' and '" + mTZEndDate + "') ";
        }
        return sqlCondition;
    }
    /**
     * 查询案件信息
     * @return boolean
     */
    private boolean getDataList() {
    	
    	String tCountSQL = "select coalesce(count(a.caseno),0)"
    		             + " from llcase a,llclaimdetail b"
    		             + " where a.caseno=b.caseno "+getCondition()+" and a.mngcom like '" + mManageCom
//    		             + " and a.endcasedate between '" + mStartDate + "' and '" +
//    		             mEndDate
//    		             + "' and a.mngcom like '" + mManageCom
    		             + "%' "
    		             + getContType(mContType)
    		             + getRgtNo(mRgtNo)
    		             + getContNo(mContNo)
    		             + getRiskCode(mRiskCode)
    		             + getStatsType(mStatsType)
    		             + " with ur";
    	ExeSQL tExeSQL = new ExeSQL();
    	System.out.println(tCountSQL);
    	int tCount = Integer.parseInt(tExeSQL.getOneValue(tCountSQL));
    	System.out.println("tCount-------------------------------------"+tCount);
    	
    	if (tCount>30000) {
    		bulidError("getDataList", "统计数据量过大，导致系统异常，请缩短统计时间，建议统计区间为一个月！");
    		return false;
    	}
    	

    	
    	   String sql = "select "
               + " (select name from ldcom where comcode=a.mngcom), "
               + " (case when b.grpcontno='00000000000000000000' then b.contno else b.grpcontno end),"
               + " b.contno,a.rgtno,a.caseno, "
               + " codename('llrgtstate',a.rgtstate), "
               + " a.customerno,a.customername,a.custbirthday, "
               + " codename('sex',a.customersex), "
               + " codename('idtype',a.idtype), "
               + " a.idno,b.riskcode,sum(b.realpay), "
               + " codename('llclaimdecision',b.givetype), "
               + " a.rgtdate,a.rigister,a.endcasedate,a.handler,b.grpcontno,b.grppolno,b.polno "
               + " ,a.CustomerAge,(select getdutyname from lmdutygetclm where getdutycode=b.getdutycode and getdutykind=b.getdutykind), "
               + " (select min(m.accdate) from llsubreport m,llcaserela n where m.subrptno=n.subrptno and n.caseno=a.caseno) "
               + " ,case c.togetherflag when '3' then (case c.prepaidflag when '1' then '是' else '否' end) "
               + " when '4' then (case c.prepaidflag when '1' then '是' else '否' end) "
               + " when '1' then (case a.prepaidflag when '1' then '是' else '否' end) "
               + " when '3' then (case a.prepaidflag when '1' then '是' when  '0' then '否' else (case c.prepaidflag when '1' then '是' else '否' end)  end) "
               + " else (case a.prepaidflag when '1' then '是' else '否' end) "
               + " end , sum(b.DeclineAmnt) ,sum(b.TabFeeMoney) ,a.mngcom"
               + " from llcase a,llclaimdetail b,llclaim d ,llregister c"
               + " where a.caseno=b.caseno and a.caseno=d.caseno and a.rgtno=c.rgtno "+getCondition()+" and a.mngcom like '" + mManageCom
//               + " and a.endcasedate between '" + mStartDate + "' and '" +
//               mEndDate
//               + "' and a.mngcom like '" + mManageCom
               + "%' "
               + getContType(mContType)
               + getRiskCode(mRiskCode)
               + getRgtNo(mRgtNo)
               + getContNo(mContNo)
               + getStatsType(mStatsType)
               + " group by a.mngcom,b.grpcontno,b.contno,a.rgtno, "
               + " a.caseno,a.rgtstate,a.customerno,a.customername, "
               + " a.custbirthday,a.customersex,a.idtype,a.idno, "
               + " b.riskcode,b.givetype,a.rgtdate,a.rigister, "
               + " a.endcasedate,a.handler,b.grppolno,b.polno,a.CustomerAge,b.getdutycode,b.getdutykind,a.prepaidflag,c.prepaidflag,c.togetherflag with ur ";

        System.out.println("SQL:" + sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql)) {
            System.out.println("数据准备失败! ");
            return false;
        }

        try {
            SSRS tSSRS = new SSRS();
            do {
                tSSRS = rsWrapper.getSSRS();
                if (tSSRS != null || tSSRS.MaxRow > 0) {

                    String tCaseNo = "";
                    String tContNo = "";
                    String tCustomerNo = "";
                    String tRiskCode = "";
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        String ListInfo[] = new String[77];
                        for (int m = 0; m < ListInfo.length; m++) {
                            ListInfo[m] = "";
                        }
                        tCaseNo = tSSRS.GetText(i, 5);
                        
                        tCustomerNo = tSSRS.GetText(i, 7);
                        tContNo = tSSRS.GetText(i, 3);
                        tRiskCode = tSSRS.GetText(i, 13);
                        
                        //管理机构代码-------------------------------
                        ListInfo[75] = tSSRS.GetText(i, 29);
                        
                        //管理机构
                        ListInfo[0] = tSSRS.GetText(i, 1);
                        
                        //保单号码
                        ListInfo[3] = tSSRS.GetText(i, 2);
                        
                        //获取团体保单号
                        String grpcontno=tSSRS.GetText(i, 20);
                        //团体险种号
                        String grppolno= tSSRS.GetText(i, 21);
                        String tPolNo = tSSRS.GetText(i, 22);

                        String tSQL = null ;
                        //查询套餐编码
                        if ("00000000000000000000".equals(grpcontno)) {
                            tSQL ="select riskwrapcode from lcriskdutywrap where contno='" +
                            		tContNo +
                                    "' and riskcode='" +
                                    tRiskCode +
                                    "' fetch first 1 rows only";
                        } else{
                        	LCPolBL tLCPolBL = new LCPolBL();
                        	tLCPolBL.setPolNo(tPolNo);
                        	if (!tLCPolBL.getInfo()) {
                        		continue;
                        	}
                        	LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
                        	tSQL = "select DISTINCT riskwrapcode from lccontplanrisk where riskwrapflag='Y' and grpcontno='"
                        		 + tLCPolSchema.getGrpContNo()+"' and contplancode='"+tLCPolSchema.getContPlanCode()
                        		 + "' and riskcode = '"+tLCPolSchema.getRiskCode()+"' ";                       	                        	
                        }
                        
                        ExeSQL tExeriskwrapcode = new ExeSQL();
                    		String riskwrapcode = "";
                        if(!"".equals(tSQL)&&tSQL!=null){
                        	riskwrapcode = tExeriskwrapcode.getOneValue(tSQL);
                            System.out.println("套餐险种编码******************"+riskwrapcode);                   	
                        }
 
                        ListInfo[67] = riskwrapcode;
                        String tWrapName = "";
                        if(!"".equals(riskwrapcode)&&
                        		riskwrapcode!=null){
                        	String sqlWrapName = "select WrapName from ldwrap where riskwrapcode='"+ riskwrapcode +"' with ur ";
                            ExeSQL tExeWrapName= new ExeSQL();
                            tWrapName = tExeWrapName.getOneValue(sqlWrapName); 
                        }
                        
                        System.out.println("套餐名称******************"+tWrapName);
                        ListInfo[68] = tWrapName;
                        //批次号
                        ListInfo[4] = tSSRS.GetText(i, 4);
                        
                        String  pcString = tSSRS.GetText(i, 4);
                        String tStatsType = "";
                		String sqlSTypeName = "select case when CHECKGRPCONT((select rgtobjno from llregister where rgtno='"+pcString+"')) = 'N' then '商业保险' else '社会保险' end from dual";
                        		
                        ExeSQL tExeSTypeName= new ExeSQL();
                        tStatsType = tExeSTypeName.getOneValue(sqlSTypeName); 		
                         
                        //新增业务类型
                        ListInfo[76] = tStatsType;
                        
                        //理赔号
                        ListInfo[5] = tSSRS.GetText(i, 5);
                        //案件状态
                        ListInfo[6] = tSSRS.GetText(i, 6);
                        //客户号
                        ListInfo[7] = tSSRS.GetText(i, 7);
                        //客户姓名
                        ListInfo[8] = tSSRS.GetText(i, 8);
                        //出生年月
                        ListInfo[9] = tSSRS.GetText(i, 9);
                        //客户性别
                        ListInfo[10] = tSSRS.GetText(i, 10);
                        //证件类型
                        ListInfo[11] = tSSRS.GetText(i, 11);
                        //证件号码
                        ListInfo[12] = tSSRS.GetText(i, 12);

                        //理赔险种
                        ListInfo[17] = tSSRS.GetText(i, 13);
                        
                        //帐单金额
                        ListInfo[31] = tSSRS.GetText(i, 28);

                        //赔付金额
                        ListInfo[32] = tSSRS.GetText(i, 14);
                        //赔付结论
                        ListInfo[33] = tSSRS.GetText(i, 15);
                        //出险日期
                        ListInfo[34] = tSSRS.GetText(i, 25);

                        //受理日期
                        ListInfo[45] = tSSRS.GetText(i, 16);
                        //受理人员
                        ListInfo[46] = tSSRS.GetText(i, 17);
                        //结案日期
                        ListInfo[61] = tSSRS.GetText(i, 18);
                        //结案人员
                        ListInfo[62] = tSSRS.GetText(i, 19);
                        //年龄
                        ListInfo[71] = tSSRS.GetText(i, 23);
                        //给付责任
                        ListInfo[72] = tSSRS.GetText(i, 24);
                      //是否核销预付赔款
                        ListInfo[73] = tSSRS.GetText(i, 26);
                        //拒付金额
                        ListInfo[74] = tSSRS.GetText(i, 27);
                        

                        String tInsuredSQL = "select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), " +
                                             " a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "

                                             + " from lcpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "
                                             + " from lbpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "
                                             + " from lbpol a,lbinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' fetch first 1 rows only"
                                             + " with ur";
                        
                        System.out.println(tInsuredSQL);
                        SSRS tInsuredSSRS = tExeSQL.execSQL(tInsuredSQL);
                        if (tInsuredSSRS.getMaxRow() > 0) {
                            //投保单位
                            ListInfo[1] = tInsuredSSRS.GetText(1, 1);
                            //投保客户号
                            ListInfo[2] = tInsuredSSRS.GetText(1, 2);
                            //职业代码
                            ListInfo[13] = tInsuredSSRS.GetText(1, 3);
                            //职业名称
                            ListInfo[14] = tInsuredSSRS.GetText(1, 4);

                            //险种保额
                            ListInfo[18] = tInsuredSSRS.GetText(1, 5);
                            //保费
                            ListInfo[19] = tInsuredSSRS.GetText(1, 6);
                            //缴费频次
                            ListInfo[20] = tInsuredSSRS.GetText(1, 7);
                            //保单生效日
                            ListInfo[21] = tInsuredSSRS.GetText(1, 8);
                            //保单终止日
                            ListInfo[22] = tInsuredSSRS.GetText(1, 9);
                            //销售渠道
                            ListInfo[23] = tInsuredSSRS.GetText(1, 10);
                            //核保结论
                            ListInfo[24] = tInsuredSSRS.GetText(1, 11);
                            //业务员代码
                            ListInfo[25] = tInsuredSSRS.GetText(1, 12);
                            //业务员姓名
                            ListInfo[26] = tInsuredSSRS.GetText(1, 13);
                            //所属营业单位编码
                            ListInfo[27] = tInsuredSSRS.GetText(1, 14);
                            //所属营业部
                            ListInfo[28] = tInsuredSSRS.GetText(1, 15);
                            //中介公司代码
                            ListInfo[29] = tInsuredSSRS.GetText(1, 16);
                            //中介公司名称
                            ListInfo[30] = tInsuredSSRS.GetText(1, 17);
                            //职业类别代码
                            ListInfo[69] = tInsuredSSRS.GetText(1, 18);
                            //职业类别名称
                            ListInfo[70] = tInsuredSSRS.GetText(1, 19);
                        }
                        String tFeeSQL =
                                "select codename('insustat',a.insuredstat), "
                                + " codename('llfeetype',a.feetype),"
                                +
                                " sum(a.sumfee) OVER (PARTITION BY a.caseno),"
                                + " '',"
                                + " a.hospitalcode,a.hospitalname,"
                                + " codename('hospitalclass',a.hosgrade),"
                                + " a.hospstartdate,a.hospenddate,"
                                +
                                " sum(realhospdate) OVER (PARTITION BY a.caseno),"
                                + " (select codename('llhospiflag',associateclass) from ldhospital where hospitcode=a.hospitalcode fetch first 1 rows only)"
                                + " from llfeemain a,llcaserela b "
                                + " where a.caseno=b.caseno and a.caserelano=b.caserelano "
                                + " and b.caseno='" + tCaseNo
                                + "' order by a.makedate "
                                + " fetch first 1 rows only "
                                + " with ur";
                        System.out.println(tFeeSQL);

                        SSRS tFeeSSRS = tExeSQL.execSQL(tFeeSQL);

                        if (tFeeSSRS.getMaxRow() > 0) {
                            //人员类别
                            ListInfo[15] = tFeeSSRS.GetText(1, 1);
                            //治疗类别
                            ListInfo[16] = tFeeSSRS.GetText(1, 2);
                            
                            //就诊医院编码
                            ListInfo[35] = tFeeSSRS.GetText(1, 5);
                            //就诊医院
                            ListInfo[36] = tFeeSSRS.GetText(1, 6);
                            //医院等级
                            ListInfo[37] = tFeeSSRS.GetText(1, 7);
                            //入院日期
                            ListInfo[38] = tFeeSSRS.GetText(1, 8);
                            //出院日期
                            ListInfo[39] = tFeeSSRS.GetText(1, 9);
                            //治疗天数
                            ListInfo[40] = tFeeSSRS.GetText(1, 10);
                            //合作级别
                            ListInfo[66] = tFeeSSRS.GetText(1, 11);
                        }
                        String tCureSQL = "select a.diseasecode,a.diseasename"
                                          + " from llcasecure a,llcaserela b"
                                          + " where a.caserelano=b.caserelano"
                                          + " and a.caseno=b.caseno "
                                          + " and b.caseno='" + tCaseNo
                                          + "' order by a.makedate"
                                          + " fetch first 1 rows only "
                                          + " with ur";
                        System.out.println(tCureSQL);

                        SSRS tCureSSRS = tExeSQL.execSQL(tCureSQL);
                        if (tCureSSRS.getMaxRow() > 0) {
                            //疾病名称
                            ListInfo[41] = tCureSSRS.GetText(1, 2);
                            //ICD-10
                            ListInfo[42] = tCureSSRS.GetText(1, 1);
                        }
                        String tAccidentSQL = "select a.name,a.code"
                                              +
                                              " from llaccident a,llcaserela b"
                                              +
                                              " where a.caserelano=b.caserelano"
                                              + " and a.caseno=b.caseno "
                                              + " and b.caseno='" + tCaseNo
                                              + "' order by a.accidentno"
                                              + " fetch first 1 rows only "
                                              + " with ur";
                        System.out.println(tAccidentSQL);

                        SSRS tAccidentSSRS = tExeSQL.execSQL(tAccidentSQL);
                        if (tAccidentSSRS.getMaxRow() > 0) {
                            //意外信息
                            ListInfo[43] = tAccidentSSRS.GetText(1, 1);
                            //意外编码
                            ListInfo[44] = tAccidentSSRS.GetText(1, 2);
                        }
                        String tOPTimeSQL =
                                "select '01' rgtstate,a.modifydate makedate,"
                                +
                                " max(a.modifydate) OVER (PARTITION BY a.caseno) date,"
                                + " a.operator from llfeemain a "
                                + " where a.caseno='" + tCaseNo
                                + "' union all "
                                +
                                " select a.rgtstate rgtstate,a.enddate makedate,"
                                +
                                " max(a.enddate) OVER (PARTITION BY a.caseno,rgtstate) date,"
                                + " a.operator operator from llcaseoptime a "
                                + " where a.caseno='" + tCaseNo
                                + "' and rgtstate<>'01' "
                                + " order by rgtstate,makedate "
                                + " with ur";
                        System.out.println(tOPTimeSQL);

                        SSRS tOPTimeSSRS = tExeSQL.execSQL(tOPTimeSQL);
                        for (int j = 1; j <= tOPTimeSSRS.getMaxRow(); j++) {
                            String tRgtState = tOPTimeSSRS.GetText(j, 1);
                            String tMakeDate = tOPTimeSSRS.GetText(j, 3);
                            String tOperator = tOPTimeSSRS.GetText(j, 4);
                            if ("01".equals(tRgtState)) {
                                //录入日期
                                ListInfo[49] = tMakeDate;
                                //录入人员
                                ListInfo[50] = tOperator;
                            }
                            if ("02".equals(tRgtState)) {
                                //扫描日期
                                ListInfo[47] = tMakeDate;
                                //扫描人员
                                ListInfo[48] = tOperator;
                            }
                            if ("03".equals(tRgtState)) {
                                //检录日期
                                ListInfo[51] = tMakeDate;
                                //检录人员
                                ListInfo[52] = tOperator;
                            }
                            if ("04".equals(tRgtState)) {
                                //理算日期
                                ListInfo[53] = tMakeDate;
                                //理算人员
                                ListInfo[54] = tOperator;
                            }
                            if ("05".equals(tRgtState)) {
                                //审批日期
                                ListInfo[55] = tMakeDate;
                                //审批人员
                                ListInfo[56] = tOperator;
                            }
                            if ("06".equals(tRgtState)) {
                                //审定日期
                                ListInfo[57] = tMakeDate;
                                //审定人员
                                ListInfo[58] = tOperator;
                            }
                            if ("10".equals(tRgtState)) {
                                //抽检日期
                                ListInfo[59] = tMakeDate;
                                //抽检人员
                                ListInfo[60] = tOperator;
                            }
                        }

                        String tGetSQL =
                                "select OPConfirmDate,OPConfirmCode,confdate"
                                + " from ljagetclaim where otherno='"
                                + tCaseNo
                                + "' and othernotype='5' and feeoperationtype<>'FC'"
                                + " fetch first 1 rows only with ur";
                        System.out.println(tGetSQL);

                        SSRS tGetSSRS = tExeSQL.execSQL(tGetSQL);
                        if (tGetSSRS.getMaxRow() > 0) {
                            //通知日期
                            ListInfo[63] = tGetSSRS.GetText(1, 1);
                            //通知人员
                            ListInfo[64] = tGetSSRS.GetText(1, 2);
                            //给付日期
                            ListInfo[65] = tGetSSRS.GetText(1, 3);
                        }
                        mListTable.add(ListInfo);
                    }
                }
            } while (tSSRS != null && tSSRS.MaxRow > 0);
        } catch (Exception ex) {

        } finally {
            rsWrapper.close();
        }
        return true;
    }

    /**
     * 获取险种类型查询条件
     * @param aContType String
     * @return String
     */
    private String getContType(String aContType) {
        if (!"".equals(aContType)) {
            String tContTypeSQL =
                    " and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskprop='" +
                    aContType + "' ) ";
            return tContTypeSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取批次号查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRgtNo(String aRgtNo) {
        if (!"".equals(aRgtNo)) {
            String tRgtSQL = " and a.rgtno='" + aRgtNo + "'";
            return tRgtSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取保单号查询条件
     * @param aContNo String
     * @return String
     */
    private String getContNo(String aContNo) {
        if (!"".equals(aContNo)) {
            String tContSQL = " and (b.grpcontno='" + aContNo +
                              "' or b.contno='" + aContNo + "')";
            return tContSQL;
        } else {
            return "";
        }

    }
    
    /**
     * 获取险种查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRiskCode(String aRiskCode) {
        if (!"".equals(aRiskCode)) {
            String tRiskCodeSQL = " and b.riskcode='" + aRiskCode + "'";
            return tRiskCodeSQL;
        } else {
            return "";
        }
    }

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }
    
    /**
     * 获取统计类型的查询条件
     */
    private String getStatsType(String mStatsType) {
    	String mStatsTypeSql = "";
        // 对统计类型进行判断
        if("1".equals(mStatsType)){//商业保险
        	mStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=(select rgtno from llcase where caseno=a.caseno))) = 'N' ";
        }else if("2".equals(mStatsType)){//社会保险
         	mStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=(select rgtno from llcase where caseno=a.caseno))) = 'Y' ";
        }else{
        	mStatsTypeSql = "";
        }
        return mStatsTypeSql;
    }
}
