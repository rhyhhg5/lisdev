package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.certify.CertTakeBackBL;
import com.sinosoft.lis.certify.CertTakeBackBLS;
import com.sinosoft.lis.certify.CertTakeBackUI;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSGetDrawDB;

import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.finfee.FFInvoicePrtManagerBL;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetSchema;

import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vdb.LOPRTManager2DBSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;

import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class InstanceGetFeeBatchInvoice implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    public String mCom=new String();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private TransferData mTransferData = new TransferData();

    private LOPRTInvoiceManagerSchema mLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
    private String mStartNo="";

    public InstanceGetFeeBatchInvoice()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        System.out.println("cOperate:" + cOperate);
        if (mOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        System.out.println("getInputData:" + "start getInputData........");
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();
        if(!dealData()){
            return false;
        }

//      准备打印管理表数据
        if (!dealPrintMag()) {
         return false;
        }
        return true;
    }



    private boolean dealPrintMag() {
        try{
            LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
            String tLimit = PubFun.getNoLimit(mLJAGetSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
            tLOPRTManager2Schema.setOtherNo(mLJAGetSchema.getActuGetNo());
            tLOPRTManager2Schema.setOtherNoType("05");
            tLOPRTManager2Schema.setCode("37");
            tLOPRTManager2Schema.setManageCom(mLJAGetSchema.getManageCom());
            tLOPRTManager2Schema.setAgentCode(mLJAGetSchema.getAgentCode());
            tLOPRTManager2Schema.setReqCom(mLJAGetSchema.getManageCom());
            tLOPRTManager2Schema.setReqOperator(mLJAGetSchema.getOperator());
            tLOPRTManager2Schema.setExeOperator(this.mGlobalInput.Operator);
            tLOPRTManager2Schema.setPrtType("0");
            tLOPRTManager2Schema.setStateFlag("1");
            tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManager2Schema.setStandbyFlag4(mStartNo);
            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            tLOPRTManager2DB.setSchema(tLOPRTManager2Schema);
            tLOPRTManager2DB.insert();
            this.mResult.add(this.mLOPRTManagerSchema);
        }catch(Exception e){
            e.fillInStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String tContNo = "";
        String tEndorNo = "";
        String tAgentNo = "";
        String tAgentGroup = "";
        String tBranchattr = "";
        String tPayToDay = "";
        String AgentName = "";
        String AgentGroupName = "";
        String State = "";
        String Result = "";
        String tManageCom = "";

        String tHPerson=this.mLOPRTManagerSchema.getStandbyFlag2();
        String tCPerson=this.mLOPRTManagerSchema.getStandbyFlag3();
        String tRemark=this.mLOPRTManagerSchema.getStandbyFlag4();
        this.mLOPRTManagerSchema.setStandbyFlag2("");
        this.mLOPRTManagerSchema.setStandbyFlag3("");
        this.mLOPRTManagerSchema.setStandbyFlag4("");
        
        String tFPDM = this.mLOPRTManagerSchema.getOldPrtSeq();//临时存储发票代码
        this.mLOPRTManagerSchema.setOldPrtSeq(null);

        String tAppntName="";
        String tRiskName="";
        String tRiskName2="";
        String tAppntNo="";
        int count = 0;

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "";

        LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = new LJAGetSet();
        tLJAGetDB.setActuGetNo(this.mLOPRTManagerSchema.getOtherNo());
        tLJAGetSet=tLJAGetDB.query();
        if(tLJAGetSet==null || tLJAGetSet.size()<1){
            mErrors.copyAllErrors(tLJAGetDB.mErrors);
            buildError("outputXML", "在取得LJAGet的数据时发生错误");
            return false;
        }else{
            mLJAGetSchema=tLJAGetSet.get(1);
        }
        String DSumMoney = "";
        double sumMoney = 0.0;
        String strSQL = "select SumGetMoney from LJAGet where actugetno ='"
            + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("strSQL");
        String sMoney = tExeSQL.getOneValue(strSQL);

        if (StrTool.cTrim(sMoney).equals(""))
        {
            sumMoney = mLJAGetSchema.getSumGetMoney();
        }
        else
        {
            sumMoney = Double.parseDouble(sMoney);
        }
        DSumMoney = PubFun.getChnMoney(sumMoney);

        if (mLJAGetSchema.getOtherNoType().equals("5")||mLJAGetSchema.getOtherNoType().equals("C")||mLJAGetSchema.getOtherNoType().equals("F"))
        {
            tSql = "select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                + "when '530301' then (select riskwrapplanname from ldcode1 "
                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lccont c where c.ContNo = a.ContNo "
                + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
            tSql= tSql + "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
            + "when '530301' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
            + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
            + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
            tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
            + "when '530301' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lcgrpcont c where c.grpContNo = a.grpContNo "
            + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
            + "' and a.riskcode=b.riskcode  ";
            tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
            + "when '530301' then (select riskwrapplanname from ldcode1 "
            + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbgrpcont c where c.grpContNo = a.grpContNo "
            + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
            + "' and a.riskcode=b.riskcode  ";
          
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
            	   State = "赔款";
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                     }
            
          
                   LAAgentDB tLAAgentDB = new LAAgentDB();
                   LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

           
                   tContNo = tSSRS.GetText(1,5);
                   tAgentNo = tSSRS.GetText(1,6);
                   tLAAgentDB.setAgentCode(tAgentNo);
                   if (!tLAAgentDB.getInfo())
                   {
                	   mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                	   buildError("outputXML", "在取得LAAgent的数据时发生错误");
                	   return false;
                   }

                   tAgentGroup = tLAAgentDB.getAgentGroup();
                   tPayToDay =tSSRS.GetText(1,7);
                   tLABranchGroupDB.setAgentGroup(tAgentGroup);
                   if (!tLABranchGroupDB.getInfo())
                   {
                	   mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                	   buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                	   return false;
                   }
                   tBranchattr = tLABranchGroupDB.getBranchAttr();
                   AgentName = "业务员：" + tLAAgentDB.getName();
                   AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
                   tEndorNo = mLJAGetSchema.getOtherNo();
            }
        }


        if (mLJAGetSchema.getOtherNoType().equals("10"))
        {
            tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                   + "when '530301' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
                   + mLJAGetSchema.getActuGetNo()
                   + "' and a.contno=b.contno and b.riskcode=c.riskcode  order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                   State=tSSRS.GetText(1,6);
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }else{
            	 tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                     + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                     + "when '530301' then (select riskwrapplanname from ldcode1 "
                     + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lbpol b, lmriskapp c where ActuGetNo = '"
                     + mLJAGetSchema.getActuGetNo()
                     + "' and a.contno=b.contno and b.riskcode=c.riskcode  order by b.prem desc";
              tSSRS=tExeSQL.execSQL(tSql);
              if(tSSRS.getMaxRow()>0){
                     tAppntName=tSSRS.GetText(1,1);
                     tRiskName=tSSRS.GetText(1,2);
                     tAppntNo=tSSRS.GetText(1,4);
                     tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                     State=tSSRS.GetText(1,6);
                     if(tRiskName2!=""&&tRiskName2!=null){
                         tRiskName=tRiskName2;
                     }
               }
            	 
             }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(mLJAGetSchema.getActuGetNo());
            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(aLJAGetEndorseSchema.getContNo());
            if (tLCContDB.getInfo())
            {      
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContDB.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
            tContNo = tLCContDB.getContNo();
            tPayToDay = mLJAGetSchema.getConfDate();
            tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
            }else {
            	 LBContDB  tLBContDB = new LBContDB();
                 tLBContDB.setContNo(aLJAGetEndorseSchema.getContNo());
                 if (!tLBContDB.getInfo()){
                	System.out.println("保单"+aLJAGetEndorseSchema.getContNo()+"在lccont或lbcont中没有查询到数据");
                   mErrors.copyAllErrors(tLCContDB.mErrors);
                   buildError("outputXML", "在取得LCCont的数据时发生错误");
                   return false;
                 }else{
                	 LAAgentDB tLAAgentDB = new LAAgentDB();
                     tLAAgentDB.setAgentCode(tLBContDB.getAgentCode());
                     if (!tLAAgentDB.getInfo())
                     {
                         mErrors.copyAllErrors(tLAAgentDB.mErrors);
                         buildError("outputXML", "在取得LAAgent的数据时发生错误");
                         return false;
                     }
                     AgentName = "业务员：" + tLAAgentDB.getName();
                     LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                     tLABranchGroupDB.setAgentGroup(tLBContDB.getAgentGroup());
                     if (!tLABranchGroupDB.getInfo())
                     {
                         mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                         buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                         return false;
                     }
                     AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
                     tContNo = tLBContDB.getContNo();
                     tPayToDay = mLJAGetSchema.getConfDate();
                     tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
                 }
                 
            }
        }
        
        if (mLJAGetSchema.getOtherNoType().equals("3"))
        {
            tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '"
                   + mLJAGetSchema.getActuGetNo()
                   + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                   + " and b.riskcode=c.riskcode  order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                tAppntName=tSSRS.GetText(1,1);
                tRiskName=tSSRS.GetText(1,2);
                tAppntNo=tSSRS.GetText(1,4);
                tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                State=tSSRS.GetText(1,6);
                if(tRiskName2!=""&&tRiskName2!=null){
                    tRiskName=tRiskName2;
                }
          }else{
        	  tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lbgrppol b, lmriskapp c where ActuGetNo = '"
                  + mLJAGetSchema.getActuGetNo()
                  + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                  + " and b.riskcode=c.riskcode  order by b.prem desc";
        	  tSSRS=tExeSQL.execSQL(tSql);
           if(tSSRS.getMaxRow()>0){
                  tAppntName=tSSRS.GetText(1,1);
                  tRiskName=tSSRS.GetText(1,2);
                  tAppntNo=tSSRS.GetText(1,4);
                  tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                  State=tSSRS.GetText(1,6);
                  if(tRiskName2!=""&&tRiskName2!=null){
                      tRiskName=tRiskName2;
                  }
            } else {
                tSql = "Select b.Grpname, Riskname, b.Riskcode, b.Customerno, b.Grpcontno, (Select Edorname  From Lmedoritem Where Edorcode = a.Feeoperationtype Fetch First 1 Rows Only) From Ljagetendorse a, Lcgrppol b, Lmriskapp c Where Actugetno = '"
                     + mLJAGetSchema.getActuGetNo()
                     + "' And a.Grpcontno = b.Grpcontno And a.Grpcontno = (Select contno From lgwork Where workno = a.Endorsementno and statusno = '5' and typeno = '070015' Fetch First 1 Rows Only) " 
                     + "And b.Riskcode = c.Riskcode Order By b.Prem Desc";
              tSSRS=tExeSQL.execSQL(tSql);
             if(tSSRS.getMaxRow()>0){
                    tAppntName=tSSRS.GetText(1,1);
                    tRiskName=tSSRS.GetText(1,2);
                    tAppntNo=tSSRS.GetText(1,4);
                    tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                    State=tSSRS.GetText(1,6);
                    if(tRiskName2!=""&&tRiskName2!=null){
                        tRiskName=tRiskName2;
                    }
              } else {
                  tSql = "Select b.Grpname, Riskname, b.Riskcode, b.Customerno, b.Grpcontno, (Select Edorname  From Lmedoritem Where Edorcode = a.Feeoperationtype Fetch First 1 Rows Only) From Ljagetendorse a, LBgrppol b, Lmriskapp c Where Actugetno = '"
                      + mLJAGetSchema.getActuGetNo()
                      + "' And a.Grpcontno = b.Grpcontno And a.Grpcontno = (Select contno From lgwork Where workno = a.Endorsementno and statusno = '5' and typeno = '070015' Fetch First 1 Rows Only) " 
                      + "And b.Riskcode = c.Riskcode Order By b.Prem Desc";
                  tSSRS=tExeSQL.execSQL(tSql);
               if(tSSRS.getMaxRow()>0){
                      tAppntName=tSSRS.GetText(1,1);
                      tRiskName=tSSRS.GetText(1,2);
                      tAppntNo=tSSRS.GetText(1,4);
                      tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                      State=tSSRS.GetText(1,6);
                      if(tRiskName2!=""&&tRiskName2!=null){
                          tRiskName=tRiskName2;
                      }
                }
              }
            }
         	 
          }
         LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
         tLJAGetEndorseDB.setActuGetNo(mLJAGetSchema.getActuGetNo());
         LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
         LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

         LCGrpContDB tLCGrpContDB = new LCGrpContDB();
         tLCGrpContDB.setGrpContNo(aLJAGetEndorseSchema.getGrpContNo());
         if (tLCGrpContDB.getInfo())
         {      
         LAAgentDB tLAAgentDB = new LAAgentDB();
         tLAAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
         if (!tLAAgentDB.getInfo())
         {
             mErrors.copyAllErrors(tLAAgentDB.mErrors);
             buildError("outputXML", "在取得LAAgent的数据时发生错误");
             return false;
         }
         AgentName = "业务员：" + tLAAgentDB.getName();
         LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
         tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
         if (!tLABranchGroupDB.getInfo())
         {
             mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
             buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
             return false;
         }
         AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
         tContNo = tLCGrpContDB.getGrpContNo();
         tPayToDay = mLJAGetSchema.getConfDate();
         tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
         }else {
         	 LBGrpContDB  tLBGrpContDB = new LBGrpContDB();
         	tLBGrpContDB.setGrpContNo(aLJAGetEndorseSchema.getGrpContNo());
              if (!tLBGrpContDB.getInfo()){
             	System.out.println("保单"+aLJAGetEndorseSchema.getGrpContNo()+"在lcgrpcont或lbgrpcont中没有查询到数据");
                mErrors.copyAllErrors(tLBGrpContDB.mErrors);
                buildError("outputXML", "在取得LCCont的数据时发生错误");
                return false;
              }else{
             	 LAAgentDB tLAAgentDB = new LAAgentDB();
                  tLAAgentDB.setAgentCode(tLBGrpContDB.getAgentCode());
                  if (!tLAAgentDB.getInfo())
                  {
                      mErrors.copyAllErrors(tLAAgentDB.mErrors);
                      buildError("outputXML", "在取得LAAgent的数据时发生错误");
                      return false;
                  }
                  AgentName = "业务员：" + tLAAgentDB.getName();
                  LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                  tLABranchGroupDB.setAgentGroup(tLBGrpContDB.getAgentGroup());
                  if (!tLABranchGroupDB.getInfo())
                  {
                      mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                      buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                      return false;
                  }
                  AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
                  tContNo = tLBGrpContDB.getGrpContNo();
                  tPayToDay = mLJAGetSchema.getConfDate();
                  tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
              }
              
         }
     }
          
        if (mLJAGetSchema.getOtherNoType().equals("20"))
        {
            tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                   + "when '530301' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJSGetdraw a, lcpol b, lmriskapp c where a.getnoticeno = '"
                   + mLJAGetSchema.getActuGetNo()
                   + "' and a.contno=b.contno and b.riskcode=c.riskcode   order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                   State="满期给付";
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }else{
            	 tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                     + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                     + "when '530301' then (select riskwrapplanname from ldcode1 "
                     + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJSGetdraw a, lbpol b, lmriskapp c where a.getnoticeno = '"
                     + mLJAGetSchema.getActuGetNo()
                     + "' and a.contno=b.contno and b.riskcode=c.riskcode   order by b.prem desc";
              tSSRS=tExeSQL.execSQL(tSql);
              if(tSSRS.getMaxRow()>0){
                     tAppntName=tSSRS.GetText(1,1);
                     tRiskName=tSSRS.GetText(1,2);
                     tAppntNo=tSSRS.GetText(1,4);
                     tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                     State="满期给付";
                     if(tRiskName2!=""&&tRiskName2!=null){
                         tRiskName=tRiskName2;
                     }
               }
            	 
             }
            LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
            tLJSGetDrawDB.setGetNoticeNo(mLJAGetSchema.getActuGetNo());
            LJSGetDrawSet tLJSGetDrawSet = tLJSGetDrawDB.query();
            LJSGetDrawSchema aLJSGetDrawSchema = tLJSGetDrawSet.get(1);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(aLJSGetDrawSchema.getContNo());
            if (tLCContDB.getInfo())
            {      
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContDB.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
            tContNo = tLCContDB.getContNo();
            tPayToDay = mLJAGetSchema.getConfDate();
            tEndorNo = aLJSGetDrawSchema.getContNo();
            }else {
            	 LBContDB  tLBContDB = new LBContDB();
                 tLBContDB.setContNo(aLJSGetDrawSchema.getContNo());
                 if (!tLBContDB.getInfo()){
                	System.out.println("保单"+aLJSGetDrawSchema.getContNo()+"在lccont或lbcont中没有查询到数据");
                   mErrors.copyAllErrors(tLCContDB.mErrors);
                   buildError("outputXML", "在取得LCCont的数据时发生错误");
                   return false;
                 }else{
                	 LAAgentDB tLAAgentDB = new LAAgentDB();
                     tLAAgentDB.setAgentCode(tLBContDB.getAgentCode());
                     if (!tLAAgentDB.getInfo())
                     {
                         mErrors.copyAllErrors(tLAAgentDB.mErrors);
                         buildError("outputXML", "在取得LAAgent的数据时发生错误");
                         return false;
                     }
                     AgentName = "业务员：" + tLAAgentDB.getName();
                     LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                     tLABranchGroupDB.setAgentGroup(tLBContDB.getAgentGroup());
                     if (!tLABranchGroupDB.getInfo())
                     {
                         mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                         buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                         return false;
                     }
                     AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
                     tContNo = tLBContDB.getContNo();
                     tPayToDay = mLJAGetSchema.getConfDate();
                     tEndorNo = aLJSGetDrawSchema.getContNo();
                 }
                 
            }
        }
        
        if (mLJAGetSchema.getOtherNoType().equals("21"))
        {
            tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJSGetDraw a, lcgrppol b, lmriskapp c where a.getnoticeno = '"
                   + mLJAGetSchema.getActuGetNo()
                   + "' and a.grpcontno=b.grpcontno "
                   + " and b.riskcode=c.riskcode  order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                tAppntName=tSSRS.GetText(1,1);
                tRiskName=tSSRS.GetText(1,2);
                tAppntNo=tSSRS.GetText(1,4);
                tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                State="满期给付";
                if(tRiskName2!=""&&tRiskName2!=null){
                    tRiskName=tRiskName2;
                }
          }else{
        	  tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJSGetDraw a, lbgrppol b, lmriskapp c where a.getnoticeno = '"
                  + mLJAGetSchema.getActuGetNo()
                  + "' and a.grpcontno=b.grpcontno "
                  + " and b.riskcode=c.riskcode  order by b.prem desc";
        	  tSSRS=tExeSQL.execSQL(tSql);
           if(tSSRS.getMaxRow()>0){
                  tAppntName=tSSRS.GetText(1,1);
                  tRiskName=tSSRS.GetText(1,2);
                  tAppntNo=tSSRS.GetText(1,4);
                  tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                  State="满期给付";
                  if(tRiskName2!=""&&tRiskName2!=null){
                      tRiskName=tRiskName2;
                  }
            }
         	 
          }
            LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
         tLJSGetDrawDB.setGetNoticeNo(mLJAGetSchema.getActuGetNo());
         LJSGetDrawSet tLJSGetDrawSet = tLJSGetDrawDB.query();
         LJSGetDrawSchema aLJSGetDrawSchema = tLJSGetDrawSet.get(1);

         LCGrpContDB tLCGrpContDB = new LCGrpContDB();
         tLCGrpContDB.setGrpContNo(aLJSGetDrawSchema.getGrpContNo());
         if (tLCGrpContDB.getInfo())
         {      
         LAAgentDB tLAAgentDB = new LAAgentDB();
         tLAAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
         if (!tLAAgentDB.getInfo())
         {
             mErrors.copyAllErrors(tLAAgentDB.mErrors);
             buildError("outputXML", "在取得LAAgent的数据时发生错误");
             return false;
         }
         AgentName = "业务员：" + tLAAgentDB.getName();
         LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
         tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
         if (!tLABranchGroupDB.getInfo())
         {
             mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
             buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
             return false;
         }
         AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
         tContNo = tLCGrpContDB.getGrpContNo();
         tPayToDay = mLJAGetSchema.getConfDate();
         tEndorNo = aLJSGetDrawSchema.getGrpContNo();
         }else {
         	 LBGrpContDB  tLBGrpContDB = new LBGrpContDB();
         	tLBGrpContDB.setGrpContNo(aLJSGetDrawSchema.getGrpContNo());
              if (!tLBGrpContDB.getInfo()){
             	System.out.println("保单"+aLJSGetDrawSchema.getGrpContNo()+"在lcgrpcont或lbgrpcont中没有查询到数据");
                mErrors.copyAllErrors(tLBGrpContDB.mErrors);
                buildError("outputXML", "在取得LCCont的数据时发生错误");
                return false;
              }else{
             	 LAAgentDB tLAAgentDB = new LAAgentDB();
                  tLAAgentDB.setAgentCode(tLBGrpContDB.getAgentCode());
                  if (!tLAAgentDB.getInfo())
                  {
                      mErrors.copyAllErrors(tLAAgentDB.mErrors);
                      buildError("outputXML", "在取得LAAgent的数据时发生错误");
                      return false;
                  }
                  AgentName = "业务员：" + tLAAgentDB.getName();
                  LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                  tLABranchGroupDB.setAgentGroup(tLBGrpContDB.getAgentGroup());
                  if (!tLABranchGroupDB.getInfo())
                  {
                      mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                      buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                      return false;
                  }
                  AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
                  tContNo = tLBGrpContDB.getGrpContNo();
                  tPayToDay = mLJAGetSchema.getConfDate();
                  tEndorNo = aLJSGetDrawSchema.getGrpContNo();
              }
              
         }
     }
        
        
        
        
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLJAGetSchema.getManageCom());
        if (!tLDComDB.getInfo())
        {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            buildError("outputXML", "在取得LDCom的数据时发生错误");
            return false;
        }
       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        mCom=mLJAGetSchema.getManageCom();
        if(StrTool.cTrim(mCom).length()<4){
            buildError("outputXML", "管理机构不正确，不存在该类型");
            return false;
        }
        texttag.add("JetFormType", "finvoice");
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          this.mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", this.mGlobalInput.ClientIP.replaceAll("\\.","_"));
        texttag.add("previewflag", "1");

        if(mLJAGetSchema.getManageCom()!=null && mLJAGetSchema.getManageCom().length()>3){
            tManageCom = mLJAGetSchema.getManageCom().substring(0, 4);
        }
        texttag.add("CompanyAddress", tLDComDB.getLetterServicePostAddress());
        texttag.add("CompanyTel", tLDComDB.getServicePhone());
         if (tManageCom.equals("8621"))
        {
            xmlexport.createDocument("FeeInvoiceLNF.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
      
        else
        {
        	System.out.println("目前只开通辽宁付费打印发票，其他机构不能打印");
            CError cError = new CError();
            cError.moduleName = "InstanceGetFeeBatchInvoice";
            cError.functionName = "dealData";
            cError.errorMessage = "机构不是辽宁";
            this.mErrors.addOneError(cError);
            return false;
        }
       

        String tPostalAddress="";
        String tZipCode="";
        String tAttr2="";
        String ttSql = "select postaladdress,zipcode from lcaddress where customerno='"+tAppntNo+"' and addressno = (select addressno from lcappnt where appntno = customerno and contno = '"+tContNo+"') ";
        tSSRS = tExeSQL.execSQL(ttSql);
        if(tSSRS.getMaxRow()>0){
            if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                tPostalAddress=tSSRS.GetText(1,1);
            }
            if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                tZipCode=tSSRS.GetText(1,2);
            }
        }else{
        	ttSql = "select postaladdress,zipcode from lcaddress where customerno='"+tAppntNo+"' and addressno = (select addressno from lbappnt where appntno = customerno and contno = '"+tContNo+"') ";
            tSSRS = tExeSQL.execSQL(ttSql);
            if(tSSRS.getMaxRow()>0){
                if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                    tPostalAddress=tSSRS.GetText(1,1);
                }
                if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                    tZipCode=tSSRS.GetText(1,2);
                }
            }else{
        	ttSql = "select a.grpaddress,grpzipcode from LCGrpAddress a where a.customerno='"+tAppntNo+"' and a.addressno = (select b.addressno from lcgrpappnt b where b.customerno = a.customerno and b.grpcontno = '"+tContNo+"') ";
            tSSRS = tExeSQL.execSQL(ttSql);
            if(tSSRS.getMaxRow()>0){
                if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                    tPostalAddress=tSSRS.GetText(1,1);
                }
                if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                    tZipCode=tSSRS.GetText(1,2);
                }
            }else{
            ttSql = "select a.grpaddress,grpzipcode from LCGrpAddress a where a.customerno='"+tAppntNo+"' and a.addressno = (select b.addressno from lbgrpappnt b where b.customerno = a.customerno and b.grpcontno = '"+tContNo+"') ";
            tSSRS = tExeSQL.execSQL(ttSql);
            if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                tPostalAddress=tSSRS.GetText(1,1);
            }
            if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                tZipCode=tSSRS.GetText(1,2);
            }
            }
            }
        }
        /*
         * Modify by zhangbin End
         */

        if(!tZipCode.equals("")){
            tAttr2="邮编："+tZipCode+" ";
        }
        if(!tPostalAddress.equals("")){
            tAttr2="地址："+tPostalAddress;
        }
        texttag.add("Attr2", tAttr2);
        tSSRS.Clear();

        texttag.add("XSumMoney", "￥" + format(sumMoney));
        texttag.add("AppntNo", tAppntNo);
        texttag.add("AppntName", tAppntName);
        texttag.add("RiskName", tRiskName);
        texttag.add("ContNo", tContNo);
        texttag.add("PayToDay", tPayToDay);

        
        if (!StrTool.cTrim(tEndorNo).equals("") )
        {
            texttag.add("EndorNo", "业务号：" + tEndorNo);
        }
       
            texttag.add("Remark", "批注：" + tRemark);
            
           
        
        texttag.add("HPerson", tHPerson);
        texttag.add("CPerson", tCPerson);
        texttag.add("AgentCode", tAgentNo);
        texttag.add("AgentGroup", tBranchattr);
        texttag.add("State", State);
        texttag.add("AgentName", AgentName);
        texttag.add("AgentGroupName", AgentGroupName);
        texttag.add("ManageCom", mLJAGetSchema.getManageCom());
        texttag.add("DSumMoney", "人民币 " + DSumMoney);
 
        texttag.add("Com", tLDComDB.getName());
        texttag.add("TaxNo", tLDComDB.getTaxRegistryNo());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        texttag.add("Today", sdf.format(new Date()));

        //单证核销
        String tStandbyFlag1 = this.mLOPRTManagerSchema.getStandbyFlag1();
        int tNum = tStandbyFlag1.lastIndexOf(",");
        String tCertifyCode = tStandbyFlag1.substring(0,tNum);
        mLOPRTManagerSchema.setStandbyFlag1(tCertifyCode);
        mStartNo = tStandbyFlag1.substring(tNum+1);

        if ("00000000".equals(mStartNo)) {
            String tMinSql = "select min(StartNo) from LZCARD WHERE CertifyCode='"
                   + tCertifyCode + "' and ReceiveCom='B" + this.mGlobalInput.Operator
                   + "' and StateFlag in ('0','7') ";
            tSSRS = tExeSQL.execSQL(tMinSql);
            if(tSSRS.getMaxRow()>0){
                mStartNo=tSSRS.GetText(1,1);
            }
        }
        
        texttag.add("fphm",  mStartNo);//发票号码
        System.out.println("发票号码：" + mStartNo );
        
        String sql = "select CvaliDate, AppntIDNo from LCCont where ContNo='" + tContNo + "' union all select CvaliDate,(select OrganComCode from LDGrp where CustomerNo = a.AppntNo fetch first 1 row only) from LCGrpCont a where GrpContNo='"+tContNo+"' union all select CvaliDate, AppntIDNo from LBCont where ContNo='" + tContNo + "' union all select CvaliDate,(select OrganComCode from LDGrp where CustomerNo = a.AppntNo fetch first 1 row only) from LbGrpCont a where GrpContNo='"+tContNo+"' ";
        SSRS tjSSRS = tExeSQL.execSQL(sql);
        if(tjSSRS.getMaxRow()>0)
        {
        	texttag.add("validate",  tjSSRS.GetText(1,1));//生效日期
            texttag.add("fkfhm",  tjSSRS.GetText(1,2));//付款方纳税人识别号
        }
        
        texttag.add("fp_dm", tFPDM);//发票代码
        this.saveFPDM(tFPDM,mStartNo);
        
        System.out.println("生效日期：" +tjSSRS.GetText(1,1)+ "，付款方纳税人识别号：" +tjSSRS.GetText(1,2)+ "，发票代码：" +tFPDM);
        
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        mResult.clear();
        mResult.addElement(xmlexport);

        tSql = "select * from LZCARD WHERE CertifyCode='"
             + tCertifyCode
             + "' and ReceiveCom='B"
             + this.mGlobalInput.Operator
             + "' and StateFlag in ('0','7') and startno<='"
             + mStartNo + "' and endno>='"
             + mStartNo + "'";
        System.out.println(tSql);


        LZCardDB tLZCardDB=new LZCardDB();
//        tLZCardDB.setCertifyCode(tCertifyCode);
//        tLZCardDB.setStartNo(mStartNo);
//        tLZCardDB.setStateFlag("0");
        LZCardSet tLZCardSet = new LZCardSet();
        tLZCardSet = tLZCardDB.executeQuery(tSql);
        LZCardSchema schemaLZCard = new LZCardSchema();
       if(tCertifyCode==null || tCertifyCode.trim().equals("")
           || mStartNo==null || mStartNo.trim().equals("")
           ||tLZCardSet==null || tLZCardSet.size()<=0){
            System.out.println("单证号为空");
            CError cError = new CError();
            cError.moduleName = "InstanceFeeBatch";
            cError.functionName = "dealData";
            cError.errorMessage = "查询单证号为空";
            this.mErrors.addOneError(cError);
            //return false;
        }
        if(tLZCardSet!=null && tLZCardSet.size()>0){
            schemaLZCard.setCertifyCode(tCertifyCode);
            schemaLZCard.setSubCode("");
            schemaLZCard.setRiskCode("");
            schemaLZCard.setRiskVersion("");

            schemaLZCard.setStartNo(mStartNo);
            schemaLZCard.setEndNo(mStartNo);
            schemaLZCard.setStateFlag("5");

            schemaLZCard.setSendOutCom(tLZCardSet.get(1).getReceiveCom());
            schemaLZCard.setReceiveCom("00");

            schemaLZCard.setSumCount(0);
            schemaLZCard.setPrem("");
            schemaLZCard.setAmnt(1);
            schemaLZCard.setHandler(tLZCardSet.get(1).getHandler());
            schemaLZCard.setHandleDate(tLZCardSet.get(1).getHandleDate());
            schemaLZCard.setInvaliDate(tLZCardSet.get(1).getInvaliDate());

            schemaLZCard.setTakeBackNo("");
            schemaLZCard.setSaleChnl("");
            schemaLZCard.setOperateFlag("");
            schemaLZCard.setPayFlag("");
            schemaLZCard.setEnterAccFlag("");
            schemaLZCard.setReason("");
            schemaLZCard.setState("");
            schemaLZCard.setOperator("");
            schemaLZCard.setMakeDate("");
            schemaLZCard.setMakeTime("");
            schemaLZCard.setModifyDate("");
            schemaLZCard.setModifyTime("");

            LZCardSet UpdateLZCardSet=new LZCardSet();
            UpdateLZCardSet.add(schemaLZCard);

      //        准备传输数据 VData
            VData vData = new VData();

            vData.addElement(this.mGlobalInput);
            vData.addElement(UpdateLZCardSet);
System.out.println(UpdateLZCardSet.size());
            Hashtable hashParams = new Hashtable();
            hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
            vData.addElement(hashParams);
                // 数据传输

            System.out.println("start take back ............");
            CertTakeBackUI tCertTakeBackUI = new CertTakeBackUI();

            if (!tCertTakeBackUI.submitData(vData, "HEXIAO")) {
                 mErrors.copyAllErrors(CertifyFunc.mErrors);
                 //return false;
            }
        }

        try
        {
            if (mGlobalInput.ManageCom.length() >= 4
                    && ("8621".equals(mGlobalInput.ManageCom.substring(0, 4))
                            ||"8633".equals(mGlobalInput.ManageCom.substring(0, 4))
                            ||"8641".equals(mGlobalInput.ManageCom.substring(0, 4))
                            ||"8612".equals(mGlobalInput.ManageCom.substring(0, 4))))
            {
                mLOPRTInvoiceManagerSchema.setComCode(mGlobalInput.ManageCom);
                mLOPRTInvoiceManagerSchema.setInvoiceNo(mStartNo);
                mLOPRTInvoiceManagerSchema.setContNo(tContNo);
                mLOPRTInvoiceManagerSchema.setEndorNo(tEndorNo);
                mLOPRTInvoiceManagerSchema.setXSumMoney(sumMoney);
                mLOPRTInvoiceManagerSchema.setPayerName(tAppntName);
                //        mLOPRTInvoiceManagerSchema.setPayerAddr("辽宁");
                mLOPRTInvoiceManagerSchema.setOpdate(PubFun.getCurrentDate());

                VData tData = new VData();
                tData.add(mGlobalInput);
                tData.add(mLOPRTInvoiceManagerSchema);

                FFInvoicePrtManagerBL tFFInvoicePrtManagerBL = new FFInvoicePrtManagerBL();
                if (!tFFInvoicePrtManagerBL.submitData(tData, "INSERT"))
                {
                    System.out.println("Invoice print info save failed...");
                }
                else
                {
                    System.out.println("Invoice print info save successed...");
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Invoice print info save failed...");
            CError cError = new CError();
            cError.moduleName = "BriefFeeF1PBL";
            cError.functionName = "FFInvoicePrtManagerBL.submit";
            cError.errorMessage = "发票打印信息保存失败";
            this.mErrors.addOneError(cError);
            return false;
        }
        System.out.println("InstanceFeeBatchInvoice===="+xmlexport.outputString());
        
        //xmlexport.outputDocumentToFile("C:\\", "invoice");

        return true;
    }

	/**
     * 输入:险种编码
     * 输出：险种套餐的套餐名
     * 修改人：yanjing
     */
    private String getRiskName(String riskCode,ExeSQL tExeSQL,String tcontno){
        String riskName="";
        String riskSql="select riskwrapplanname from ldcode1 where codetype = 'bankcheckappendrisk' and code1 ='"+riskCode
        +"'  and code in(select riskwrapcode from lcriskwrap where grpcontno='"
        +tcontno+"' union select riskwrapcode from lcriskdutywrap where contno='"+tcontno+"' fetch first 1 rows only) with ur";
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL(riskSql);
        if(tSSRS.getMaxRow()>0){
            riskName=tSSRS.GetText(1, 1);
        }
        return riskName;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//      全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FeeInvoiceF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }
    
    //存储发票代码
    private void saveFPDM(String afpdm, String aStartNo) 
    {
    	if(afpdm!= null && !StrTool.cTrim(afpdm).equals("") && !StrTool.cTrim(afpdm).equals("null"))
        {
        	LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
            tLPEdorEspecialDataDB.setEdorNo(mLJAGetSchema.getActuGetNo());
            tLPEdorEspecialDataDB.setDetailType("fp_dm");
            tLPEdorEspecialDataDB.setEdorAcceptNo(mLJAGetSchema.getActuGetNo());
            tLPEdorEspecialDataDB.setEdorType("FP");
            tLPEdorEspecialDataDB.setEdorValue(afpdm);
            tLPEdorEspecialDataDB.setPolNo(aStartNo);
            tLPEdorEspecialDataDB.setState("1");
            tLPEdorEspecialDataDB.delete();
            tLPEdorEspecialDataDB.insert();
        }
	}

    public static void main(String[] args)
    {
        FeeInvoiceF1PBL FeeInvoiceF1PBL1 = new FeeInvoiceF1PBL();
        
    }
}
