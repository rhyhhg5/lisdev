package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class RiskClaimstatisticUI
{
    public RiskClaimstatisticUI()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;
    private String mDefineCode = "";
    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 进行业务处理
            if (!dealData())
            {
                return false;
            }
            RiskClaimstatisticBL tRiskClaimstatisticBL = new
                    RiskClaimstatisticBL();
            System.out.println("Start RiskClaimstatisticBL Submit ...");

            if (!tRiskClaimstatisticBL.submitData(cInputData, cOperate))
            {
                if (tRiskClaimstatisticBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tRiskClaimstatisticBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "RiskClaimstatisticBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tRiskClaimstatisticBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "RiskClaimstatisticUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDefineCode = (String) cInputData.get(0);
        if (mDefineCode == "")
        {
            buildError("DefineCode", "没有得到足够的信息！");
            return false;
        }
        mDay = (String[]) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "RiskClaimstatisticUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        RiskClaimstatisticUI RC = new RiskClaimstatisticUI();
        GlobalInput tG = new GlobalInput();
        String ttDay[] = new String[2];
        ttDay[0] = "2004-05-01";
        ttDay[1] = "2004-05-30";
        tG.ManageCom = "86";
        tG.Operator = "001";
        String code = "claim10";
        VData tVData = new VData();
        tVData.addElement(code);
        tVData.addElement(ttDay);
        tVData.addElement(tG);
        RC.submitData(tVData, "PRINTPAY");
    }
}