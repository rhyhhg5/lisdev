package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LFStandardFeePrintBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	// 取得的时间
	private String mDay[] = null;

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public LFStandardFeePrintBL() {}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		mResult.clear();

		// 准备所有要打印的数据
		if (!getPrintData()) {
			return false;
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PPrintBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintData() {
		
		StringBuffer sb = new StringBuffer("select (select codename from ldcode where code = salechnl and codetype = 'lcsalechnl'),");
		sb.append(" riskcode,riskname,(case risktype when '1' then '医疗保险' when '2' then '疾病保险' when '3' then '收入保障保险' when '4' then '长期护理保险' when '5' then '意外保险' when '6' then '医疗责任保险' else '其他' end),");
		sb.append(" sum(sumactupaymoney),(case char(payintv) when '0' then '趸缴' else  trim(char(payyears))|| '年' end)  as payintv,feerate,standardfee from (select b.salechnl as salechnl,");
		sb.append(" b.riskcode as riskcode,(select riskname from lmriskapp where riskcode = b.riskcode) as riskname,(select RiskType1 from lmriskapp where riskcode = b.riskcode) as RiskType,a.sumactupaymoney as sumactupaymoney,");
		sb.append(" b.payintv  as payintv,b.payyears as payyears,'' as FeeRate,'' as StandardFee from ljapayperson a,lcpol b where a.confdate between '");
		sb.append(mDay[0] + "' and '"+ mDay[1]+ "'");
		sb.append(" and a.polno = b.polno and b.conttype = '1' and a.managecom  = '"+mGlobalInput.ManageCom+"'");
		sb.append(" union all select b.salechnl as salechnl,b.riskcode as riskcode,(select riskname from lmriskapp where riskcode = b.riskcode) as riskname,(select RiskType1 from lmriskapp where riskcode = b.riskcode) as RiskType,abs(a.getmoney) as sumactupaymoney,b.payintv  as payintv,b.payyears as payyears,'' as FeeRate,'' as StandardFee ");
		sb.append(" from ljagetendorse a,lbpol b where a.makedate between '"+mDay[0]+"' and '"+mDay[1]+"' ");
		sb.append(" and a.polno = b.polno and b.conttype = '1' and a.feefinatype = 'TF' ");
		sb.append(" and a.managecom  = '"+mGlobalInput.ManageCom+"') as aa group by salechnl,riskcode,riskname,risktype,payintv,feerate,standardfee,payyears with ur");
		 
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sb.toString());
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("LFStandardFee.vts", "LFStandardFee"); // 最好紧接着就初始化xml文档
		ListTable tlistTable = new ListTable();
		tlistTable.setName("LFStandardFee");

		String[] strArr = null;
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[tSSRS.MaxCol];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				strArr[j - 1] = tSSRS.GetText(i, j);
			}
			tlistTable.add(strArr);
		}
		strArr = new String[8];
		strArr[0] = "主要销售渠道";
		strArr[1] = "产品代码";
		strArr[2] = "产品名称";
		strArr[3] = "险种类型";
		strArr[4] = "保费收入";
		strArr[5] = "缴费方式";
		strArr[6] = "折标比例";
		strArr[7] = "标准保费";
		xmlexport.addListTable(tlistTable, strArr);
		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

}
