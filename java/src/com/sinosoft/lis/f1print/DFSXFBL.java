package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class DFSXFBL {



		/** 错误处理类，每个需要错误处理的类中都放置该类 */
		public CErrors mErrors = new CErrors();

		private VData mResult = new VData();

//		private String mDay[] = null; // 获取时间

		private String mDay[] = null;

		private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

		public DFSXFBL() {
		}

		public static void main(String[] args) {
			GlobalInput tG = new GlobalInput();
			tG.Operator = "cwad1";
			tG.ManageCom = "86";
			VData vData = new VData();
			vData.addElement(tG);

			DFSXFBL DFSXFBL = new DFSXFBL();
			DFSXFBL.submitData(vData, "PRINT");

		}

		/**
		 * 传输数据的公共方法
		 */
		public boolean submitData(VData cInputData, String cOperate) {
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}
			mResult.clear();

			if (cOperate.equals("PRINT")) { // 打印提数
				if (!getPrintData()) {
					return false;
				}
			}
			return true;
		}

		/**
		 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		 */
		private boolean getInputData(VData cInputData) { // 打印付费
			// 全局变量
			mDay = (String[]) cInputData.get(0);
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0));
			if (mGlobalInput == null) {
				buildError("getInputData", "没有得到足够的信息！");
				return false;
			}
			return true;
		}

		public VData getResult() {
			return this.mResult;
		}

		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "DFSXFBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			this.mErrors.addOneError(cError);
		}

		/**
		 * 新的打印数据方法
		 *
		 * @return
		 */
		private boolean getPrintData() {
			SSRS tSSRS = new SSRS();

			GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
			tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
			tGetSQLFromXML.setParameters("DFDate", mDay[0]);
			tGetSQLFromXML.setParameters("EndDate", mDay[1]);
			tGetSQLFromXML.setParameters("RiskCode", "险种代码");
//			String tServerPath = "E:/sinowork/picch/WebRoot/";
			ExeSQL tExeSQL = new ExeSQL();

			String nsql = "select Name from LDCom where ComCode='"
					+ mGlobalInput.ManageCom + "'"; // 管理机构
			tSSRS = tExeSQL.execSQL(nsql);
			String manageCom = tSSRS.GetText(1, 1);
			TextTag texttag = new TextTag(); // 新建一个TextTag的实例
			XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
			xmlexport.createDocument("FinCommissionCharge.vts", "printer"); // 最好紧接着就初始化xml文档
			texttag.add("StartDate", mDay[0]);
			texttag.add("EndDate", mDay[1]);
			texttag.add("ManageCom", manageCom);
			texttag.add("RiskCode", "险种代码");
			if (texttag.size() > 0) {
                            xmlexport.addTextTag(texttag);
			}
			String[] detailArr = new String[] { "代理机构名称", "险种", "保费收入", "手续费比例", "代付手续费金额","手续费支出所属机构","应付机构"  };
			String[] totalArr = new String[1];
                        String tname = "SXFDSF";
                        String pname="SXFDFSUM";

			ListTable tTotalListTabel=getDetailListTable(detailArr,tname);
			xmlexport.addListTable(tTotalListTabel,detailArr);
			ListTable sTotalListTabel=getHZListTable(totalArr,pname);
			System.out.print(sTotalListTabel.get(0)[0]);
			xmlexport.addListTable(sTotalListTabel,totalArr);
			mResult.clear();
			mResult.addElement(xmlexport);
			return true;
		}
         
         // *获取 修改之后的机构代码
           
		private String getManagecom(String managecom){
			String tmanagecom=null;
			if(managecom.endsWith("0000"))
				  {
		return tmanagecom=managecom.substring(0, 4)	;
		}else{
				return tmanagecom=managecom;
			}
		}
		/**
		 * 获得明细ListTable
		 *
		 * @param msql -
		 *            执行的SQL
		 * @param tName -
		 *            Table Name
		 * @return
		 */
		private ListTable getDetailListTable(String[] detailArr,String tname) {
			SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
		
			String mmanagecom=getManagecom(mGlobalInput.ManageCom);
			System.out.println("zzzzzzzzzzzzzzzzzzzz"+mmanagecom);

			String msql="select "
				+"	d.name,"
				+"       a.RiskCode,"
				+"       a.Transmoney,"
				+"       a.chargerate,"
				+"       a.charge,"
				+"		 (select b.ManageCom from Ljfiget b, ljAGet c where a.Receiptno = b.otherno"
				+"   and b.ActuGetNo = c.ActugetNo and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"  and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"   and b.EnterAccDate <= '"+mDay[1]+"' fetch first 1 rows only )"
				+"        ManageCom,"
				+"		(select c.ManageCom from Ljfiget b, ljAGet c where a.Receiptno = b.otherno"
				+"   and b.ActuGetNo = c.ActugetNo and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"   and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"  and b.EnterAccDate <= '"+mDay[1]+"' fetch first 1 rows only )	"	
				+"       ManageCom "	
				+" from LAcharge a, lacom d "
				+" where   1 = 1 and "
				+" a.charge <> 0 and   a.AgentCom = d.AgentCom "
				+" and "
				+"	exists (select 1 from  Ljfiget b, ljAGet c where  "
				+"	a.Receiptno = b.otherno "
				+"   and b.ActuGetNo = c.ActugetNo "
				+"   and a.commisionsn=c.getnoticeno "
				+"   and c.othernoType in ('BC', 'AC') "
				+"   and (c.FinState != 'FC' or c.FinState is null) "
				+"   and (c.confDate is not null and c.EnterAccDate is not null) "
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null) "
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"  and b.EnterAccDate <= '"+mDay[1]+"' "
				+" ) ";
//			String msql="select d.name,a.RiskCode,a.Transmoney,a.chargerate, a.charge, b.ManageCom,c.ManageCom "
//			+"from LAcharge a , Ljfiget b ,ljAGet c ,lacom d "
//			+"where a.Receiptno=b.otherno and b.ActuGetNo=c.ActugetNo and a.AgentCom=d.AgentCom and 1=1 "
//			+"and c.othernoType in ('BC','AC') "
//			+"and (c.FinState!='FC' or c.FinState is null ) and a.charge <>0 "
//			+"and (c.confDate is not null and c.EnterAccDate is not null ) and b.policycom like '"+mmanagecom+"%'"
//			+"and (c.bankonthewayflag='0' or c.bankonthewayflag is null) "
//			+"and b.EnterAccDate>='"+mDay[0]+"' and b.EnterAccDate<='"+mDay[1]+"'";

			tSSRS = tExeSQL.execSQL(msql);
			ListTable tlistTable = new ListTable();
			tlistTable.setName(tname);
			String strSum = "";
			String strArr[] = null;
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				strArr = new String[9];
				for (int j = 1; j <= tSSRS.MaxCol; j++) {
					if (j == 3) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						continue;
					}
					if (j == 4) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						continue;
					}
					if (j == 5) {
						strArr[j - 1] = tSSRS.GetText(i, j);
						if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
						strSum = new DecimalFormat("0.00").format(Double
								.valueOf(strArr[j - 1]));
						strArr[j - 1] = strSum;}
						continue;
					}
					strArr[j - 1] = tSSRS.GetText(i, j);
				}
				tlistTable.add(strArr);
			}
			return tlistTable;
		}

		/**
		 * 获得汇总ListTable
		 *
		 * @param msql -
		 *            执行的SQL
		 * @param tName -
		 *            Table Name
		 * @return
		 */
		private ListTable getHZListTable(String[] totalArr,String pname) {
		    SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();

			String mmanagecom=getManagecom(mGlobalInput.ManageCom);
			String tsql="select db2inst1.nvl(sum(a.charge),0)  "
				+" from LAcharge a, lacom d"
				+" where   1 = 1 and"
				+" a.charge <> 0 and   a.AgentCom = d.AgentCom "
				+" and"
				+"	exists (select 1 from  Ljfiget b, ljAGet c where "
				+"	a.Receiptno = b.otherno"
				+"  and b.ActuGetNo = c.ActugetNo"
				+"  and a.commisionsn=c.getnoticeno "
				+"   and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"   and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%' "
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"'"
				+"   and b.EnterAccDate <= '"+mDay[1]+"')";
	
//				+"from LAcharge a , Ljfiget b ,ljAGet c "
//				+"where a.Receiptno=b.otherno and b.ActuGetNo=c.ActugetNo and 1=1 "
//				+"and c.othernoType in ('BC','AC')  "
//				+"and (c.FinState!='FC' or c.FinState is null ) and a.charge <> 0 "
//				+"and (c.confDate is not null and c.EnterAccDate is not null )  and b.policycom like '"+mmanagecom+"%' "
//				+"and (c.bankonthewayflag='0' or c.bankonthewayflag is null)"
//				+"and b.EnterAccDate>='"+mDay[0]+"' and b.EnterAccDate<='"+mDay[1]+"'";
			System.out.print(tsql);
//          测试
//			tSSRS = tExeSQL.execSQL(tsql);
//			String sumgetmoney= tSSRS.GetText(1, 1);

//			String msql="select db2inst1.nvl(sum(sumgetmoney),0) from ljaget where othernotype in ('BC','AC')" +
//			" and (FinState!='FC' or FinState is null ) "+
//			" and (confDate is not null and EnterAccDate is not null ) " +
//			" and (bankonthewayflag='0' or bankonthewayflag is null) " +
//			" and managecom like '" + mGlobalInput.ManageCom + "%' and makedate>='"+mDay[0]+"' and makedate<='"+mDay[1]+"'";
//			System.out.print(msql);
//			tSSRS = tExeSQL.execSQL(msql);
			tSSRS = tExeSQL.execSQL(tsql);
			ListTable tlistTable = new ListTable();
			tlistTable.setName(pname);
			String strSum = "";

			strSum = new DecimalFormat("0.00").format(Double
					.valueOf(tSSRS.GetText(1, 1)));
			totalArr[0] = strSum;
			System.out.print(totalArr);
			tlistTable.add(totalArr);
			return tlistTable;

	}

}
