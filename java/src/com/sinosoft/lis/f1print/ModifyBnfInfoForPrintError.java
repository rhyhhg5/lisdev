package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPolSpecRelaSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSubSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPolSpecRelaSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: LIS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Yangming
 * @version 6.0
 */
public class ModifyBnfInfoForPrintError {
	MMap map = new MMap();
	String mContNo;
	private LCPolSpecRelaSet mLCPolSpecRelaSet = new LCPolSpecRelaSet();
	private TransferData mTransferData;
	private VData mInputData;

	public ModifyBnfInfoForPrintError() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mInputData = (VData) cInputData.clone();
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealDate()) {
			return false;
		}
		return true;
	}

	private void getBnfInfo(LCPolSchema tLCPolSchema, LCSpecSet mLCSpecSet) {
		LCBnfDB tLCBnfDB = new LCBnfDB();
		tLCBnfDB.setPolNo(tLCPolSchema.getProposalNo());
		String strSql = "select * from lcbnf where polno in ('"
				+ tLCPolSchema.getPolNo() + "','"
				+ tLCPolSchema.getProposalNo() + "')";
		LCBnfSet tLCBnfSet = tLCBnfDB.executeQuery(strSql);
		/** 当受益人大于一人时添加注视 */
		int count = this.mLCPolSpecRelaSet.size();
		if (tLCBnfSet.size() > 0 && !"122002".equals(tLCPolSchema.getRiskCode())) {
			int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
			for (int i = 1; i <= tLCBnfSet.size(); i++) {
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));// 修改，解决注的顺序不连续问题
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i + count));
				tLCPolSpecRelaSchema.setSpecContent(getBnfContent(tLCBnfSet
						.get(i)));
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			}
		}
	}

	/**
	 * getBnfInfo
	 *
	 * @param tLCBnfSchema
	 *            LCBnfSchema
	 * @return String
	 */
	private String getBnfContent(LCBnfSchema tLCBnfSchema) {
		if (tLCBnfSchema != null) {
			if (tLCBnfSchema.getBnfType().equals("1")) {
				return "身故受益人：" + tLCBnfSchema.getName() + " 证件号码："
						+ StrTool.cTrim(tLCBnfSchema.getIDNo()) + " 受益顺位："
						+ tLCBnfSchema.getBnfGrade() + " 受益比例："
						+ tLCBnfSchema.getBnfLot() * 100 + "％";
			} else {
				return "生存受益人：" + tLCBnfSchema.getName() + " 证件号码："
						+ StrTool.cTrim(tLCBnfSchema.getIDNo()) + " 受益顺位："
						+ tLCBnfSchema.getBnfGrade() + " 受益比例："
						+ tLCBnfSchema.getBnfLot() * 100 + "％";
			}
		}
		return "";
	}

	private LCSpecSet getSpecInfo(LCPolSchema tLCPolSchema) {
		LCSpecDB mLCSpecDB = new LCSpecDB();
		mLCSpecDB.setProposalNo(tLCPolSchema.getProposalNo());
		LCSpecSet mLCSpecSet = mLCSpecDB.query();
		if (mLCSpecSet.size() > 0) {
			int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
			for (int i = 1; i <= mLCSpecSet.size(); i++) {
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(mLCSpecSet.get(i)
						.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));// 修改，解决注的顺序不连续问题
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i));
				tLCPolSpecRelaSchema.setSpecContent(mLCSpecSet.get(i)
						.getSpecContent());
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			}
		}
		return mLCSpecSet;
	}

	private void getAddFeeInfo(LCPolSchema tLCPolSchema, LCSpecSet mLCSpecSet) {
		String addFeeSql = "select * from lcprem where polno='"
				+ tLCPolSchema.getPolNo() + "' and payplancode like '000000%'";
		LCPremDB tLCPremDB = new LCPremDB();
		LCPremSet tLCPremSet = tLCPremDB.executeQuery(addFeeSql);
		if (tLCPremSet.size() > 0) {
			int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
			for (int i = 1; i <= tLCPremSet.size(); i++) {
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPremSet.get(i)
						.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));// 修改，解决注的顺序不连续问题
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i
						+ mLCSpecSet.size()));
				double addfee = tLCPremSet.get(i).getPrem();
				double addFeeRate = tLCPremSet.get(i).getRate();
				String StrarDate = tLCPremSet.get(i).getPayStartDate();
				String EndDate = tLCPremSet.get(i).getPayEndDate();
				String addFeeResult = "";
				if (addfee > 0 || addFeeRate > 0) {
					addFeeResult = "附加风险保费";
					addFeeResult += (addFeeRate > 0 ? String
							.valueOf(addFeeRate * 100) + "％" : String
							.valueOf(addfee)) + "; ";
					addFeeResult += "加费自" + StrarDate.split("-")[0] + "年"
							+ StrarDate.split("-")[1] + "月"
							+ StrarDate.split("-")[2] + "日" + "开始 ";
					if (!StrTool.cTrim(EndDate).equals("")) {
						addFeeResult += "至" + EndDate.split("-")[0] + "年"
								+ EndDate.split("-")[1] + "月"
								+ EndDate.split("-")[2] + "日" + "终止";
					}
				}
				tLCPolSpecRelaSchema.setSpecContent("附加保费"
						+ tLCPremSet.get(i).getPrem() + "元");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			}
		}
	}

	public static void main(String[] args) {
		ModifyBnfInfoForPrintError modifybnfinfoforprinterror = new ModifyBnfInfoForPrintError();
	}

	private boolean getInputData(VData cInputData) {
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mContNo = (String) mTransferData.getValueByName("ContNo");

		return true;
	}

	private boolean dealDate() {
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mContNo);
		LCPolSet tLCPolSet = tLCPolDB
				.executeQuery("select a.* from LCPol a,LCInsured b where a.ContNo=b.ContNo and a.InsuredNo=b.InsuredNo and a.ContNo ='"
						+ mContNo
						+ "' order by int(b.SequenceNo),int(a.RiskSeqNo) with ur ");
		String SQL1 = "delete from LCPolSpecRela where contno='" + mContNo
				+ "'";
		map.put(SQL1, "DELETE");
		for (int m = 1; m <= tLCPolSet.size(); m++) {
			LCPolSchema tLCPolSchema = tLCPolSet.get(m).getSchema();

			// 获取注的标号
			LCSpecSet mLCSpecSet = getSpecInfo(tLCPolSchema);
			getAddFeeInfo(tLCPolSchema, mLCSpecSet);
			getBnfInfo(tLCPolSchema, mLCSpecSet);
			getSpecRisk(tLCPolSchema);
		}
		System.out.println("完成保单号码：" + mContNo);
		/*
		 * for (int n = 1; n <= mLCPolSpecRelaSet.size(); n++) {
		 * System.out.println(mLCPolSpecRelaSet.get(n).encode().replace( '|',
		 * '\t')); }
		 */
		for (int k = 1; k <= mLCPolSpecRelaSet.size(); k++) {
			String tPolno = mLCPolSpecRelaSet.get(k).getPolNo();
			if (mLCPolSpecRelaSet.get(k).getSpecCode().contains("A")) {
				continue;
			}
			String tStrSql = " select 1 from lcpol "
					+ " where contno = '"
					+ mLCPolSpecRelaSet.get(k).getContNo()
					+ "' "
					+ " and polno = '"
					+ mLCPolSpecRelaSet.get(k).getPolNo()
					+ "' "
					+ " and RiskCode in (select Code from LDCode1 where CodeType = 'checkappendrisk' and RiskWrapPlanName is not null) "
					+ " with ur ";
			ExeSQL tExeSQL = new ExeSQL();
			String isFind = tExeSQL.getOneValue(tStrSql);

			if ("1".equals(isFind) && k < mLCPolSpecRelaSet.size()) {
				mLCPolSpecRelaSet.get(k).setSpecCode(
						mLCPolSpecRelaSet.get(k).getSpecCode() + "A");// 为避免不显示的特别约定，又显示出来，导致多个相同的注，将原注+字母A标记不同。
				for (int n = k + 1; n <= mLCPolSpecRelaSet.size(); n++) {
					if (tPolno.equals(mLCPolSpecRelaSet.get(n).getPolNo())) {
						mLCPolSpecRelaSet.get(n).setSpecCode(
								mLCPolSpecRelaSet.get(n).getSpecCode() + "A");// 为避免不显示的特别约定，又显示出来，导致多个相同的注，将原注+字母A标记不同。

					} else {
						String tSpecCode = mLCPolSpecRelaSet.get(n)
								.getSpecCode();

						mLCPolSpecRelaSet.get(n).setSpecCode(
								(Integer.parseInt(tSpecCode) - 1) + "");
					}

				}
			}
		}
		PubSubmit ps = new PubSubmit();
		VData tInput = new VData();

		map.put(mLCPolSpecRelaSet, "INSERT");
		tInput.add(map);
		if (!ps.submitData(tInput, null)) {
			CError.buildErr(this, "险种特约关联信息插入失败！");
			return false;
		}
		mLCPolSpecRelaSet = null;
		return true;
	}

	// BY GZH 20111109 特殊处理121501险种
	private void getSpecRisk(LCPolSchema tLCPolSchema) {
		int count = this.mLCPolSpecRelaSet.size();
		LCContDB tLCContBL = new LCContDB();
		tLCContBL.setContNo(mContNo);
		if (!tLCContBL.getInfo()) {
			CError.buildErr(this, "保单查询失败！");
			return;
		} else {
			LCContSchema tLCCont = new LCContSchema();
			tLCCont = tLCContBL.getSchema();
			if ("121501".equals(tLCPolSchema.getRiskCode())
					&& !"2".equals(tLCCont.getCardFlag())) {
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				String tSql = "select riskcode from lcpol where polno =(select lcp.mainpolno from lcpol lcp where lcp.polno = '"
						+ tLCPolSchema.getPolNo() + "' ) ";
				String tMainRiskCode = new ExeSQL().getOneValue(tSql);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema.setSpecContent("本险种的主险为" + tMainRiskCode
						+ "，主险合同效力终止，本附加险合同效力即行终止");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if ("332301".equals(tLCPolSchema.getRiskCode())
					&& !"2".equals(tLCCont.getCardFlag())) {

				String tSql = "select riskcode,insuredname from lcpol where polno =(select mainpolno from lcpol lcp where lcp.polno = '"
						+ tLCPolSchema.getPolNo()
						+ "' and lcp.riskcode = '332301' ) ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				if (tSSRS != null && tSSRS.MaxRow == 1) {
					int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
					tLCPolSpecRelaSchema.setSpecContent("本险种的主险为"
							+ tSSRS.GetText(1, 1) + "，主险合同效力终止，本附加险合同效力即行终止。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);

					tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(2 + count));
					tLCPolSpecRelaSchema
							.setSpecContent("《附加个人护理保险（万能型）》的投保人为主险"
									+ tSSRS.GetText(1, 1) + "的被保险人"
									+ tSSRS.GetText(1, 2) + "。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
				}

			} else if ("231401".equals(tLCPolSchema.getRiskCode())
					&& !"2".equals(tLCCont.getCardFlag())) {
				// modify by lxs 2016-12-09 附加豁免对应主险编码逻辑调整
				String tSql = "select riskcode,appntname from lcpol where prtno = '"
						+ tLCPolSchema.getPrtNo()
						+ "' and riskcode in (select code from ldcode1 where  codetype = 'checkexemptionrisk') ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema.setSpecContent("本险种的主险为"
						+ tSSRS.GetText(1, 1) + "，主险合同效力终止，本附加险合同效力即行终止。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if ("122001".equals(tLCPolSchema.getRiskCode())
					&& !"2".equals(tLCCont.getCardFlag())) {

				String tSql = "select riskcode,insuredname from lcpol where polno =(select mainpolno from lcpol lcp where lcp.polno = '"
						+ tLCPolSchema.getPolNo()
						+ "' and lcp.riskcode = '122001' ) ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				if (tSSRS != null && tSSRS.MaxRow == 1) {
					int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
					tLCPolSpecRelaSchema.setSpecContent("本险种的主险为"
							+ tSSRS.GetText(1, 1) + "，主险合同效力终止，本附加险合同效力即行终止。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
				}
			} else if ("260602".equals(tLCPolSchema.getRiskCode())
					&& "2".equals(tLCCont.getCardFlag())) {
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema
						.setSpecContent("本险种的承保责任为女性特定重大疾病保险金（基本部分）。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if ("334801".equals(tLCPolSchema.getRiskCode())) {

				String tSql = "select riskcode,insuredname from lcpol where polno =(select mainpolno from lcpol lcp where lcp.polno = '"
						+ tLCPolSchema.getPolNo()
						+ "' and lcp.riskcode = '334801' ) ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				if (tSSRS != null && tSSRS.MaxRow == 1) {
					int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
					tLCPolSpecRelaSchema.setSpecContent("本险种的主险为"
							+ tSSRS.GetText(1, 1) + "，主险合同效力终止，本附加险合同效力即行终止。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);

					tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(2 + count));
					tLCPolSpecRelaSchema
							.setSpecContent("《附加个人护理保险（万能型，B款）》的投保人为主险"
									+ tSSRS.GetText(1, 1) + "的被保险人"
									+ tSSRS.GetText(1, 2) + "。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
				}

			} else if ("320201".equals(tLCPolSchema.getRiskCode())
					&& "2".equals(tLCCont.getCardFlag())) {
				String tSql = "select case mult when 1 then '一类疫苗' else '一类、二类疫苗' end  from lcpol where contno = '"
						+ tLCPolSchema.getContNo()
						+ "' and polno='"
						+ tLCPolSchema.getPolNo() + "' ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema.setSpecContent("被保险人保险责任涉及疫苗为"
						+ tSSRS.GetText(1, 1) + "，护理保险金月领取标准及疾病身故保险金额详见保障计划表。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if ("122301".equals(tLCPolSchema.getRiskCode())
					|| "122302".equals(tLCPolSchema.getRiskCode())) {
				String tSql = "select getriskplan(riskcode,mult),amnt,case riskcode when '122302' then '仅包括基本责任。' else '包括基本责任及可选责任。' end  from lcpol where polno='"
						+ tLCPolSchema.getPolNo() + "'";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema.setSpecContent("被保险人"
						+ tLCPolSchema.getInsuredName() + "所投保的保障计划"
						+ tSSRS.GetText(1, 1) + "年度限额为人民币"
						+ tSSRS.GetText(1, 2) + "元，" + tSSRS.GetText(1, 3));
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if ("340501".equals(tLCPolSchema.getRiskCode())) {// modify
																		// by
																		// lxs
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema
						.setSpecContent("本保险合同中除健康一生个人护理保险（万能型）以外的保险产品，涉及被保险人未申请领取的健康维护保险金、老年护理保险金及老年关爱保险金等各项保险金，作为本保险合同健康一生个人护理保险（万能型）的保险费自动转入投保人的万能个人账户中。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			
			} else if ("122002".equals(tLCPolSchema.getRiskCode())
					&& !"2".equals(tLCCont.getCardFlag())) {

				String tSql = "select riskcode,insuredname from lcpol where polno =(select mainpolno from lcpol lcp where lcp.polno = '"
						+ tLCPolSchema.getPolNo()
						+ "' and lcp.riskcode = '122002' ) ";
				SSRS tSSRS = new ExeSQL().execSQL(tSql);
				if (tSSRS != null && tSSRS.MaxRow == 1) {
					int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
							.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String
							.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema
							.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
					tLCPolSpecRelaSchema.setSpecContent("本险种的主险为"
							+ tSSRS.GetText(1, 1) + "，主险合同效力终止，本附加险合同效力即行终止。");
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
				}
			} else if ("340602".equals(tLCPolSchema.getRiskCode())) {
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema
						.setSpecContent("本保险合同中除健康一生个人护理保险（万能型，A款）以外的保险产品，涉及投保人及被保险人未申请领取的符合本保险合同投保规定的各项保单利益，作为本保险合同健康一生个人护理保险（万能型，A款）的保险费自动转入投保人的万能个人账户中。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);

			} else if ("123701".equals(tLCPolSchema.getRiskCode())
					&& (1 == tLCPolSchema.getMult() || 2 == tLCPolSchema
							.getMult())) {
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema
						.setSpecContent("对被保险人<"
								+ tLCPolSchema.getInsuredName()
								+ ">“健康金福个人中端医疗保险（2017款）”，双方约定如下：（1）被保险人的医疗费用支付方式不包含社会医疗保险，按照被保险人无社保对应的保险费投保。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);

			} else if ("123701".equals(tLCPolSchema.getRiskCode())
					&& (3 == tLCPolSchema.getMult() || 4 == tLCPolSchema
							.getMult())) {
				int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
				tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
				tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
				tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
				tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
				tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
				tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
				tLCPolSpecRelaSchema
						.setSpecContent("对被保险人<"
								+ tLCPolSchema.getInsuredName()
								+ ">“健康金福个人中端医疗保险（2017款）”，双方约定如下：（1）被保险人的医疗费用支付方式包含社会医疗保险，按照被保险人有社保对应的保险费投保。");
				mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
			} else if("232701".equals(tLCPolSchema.getRiskCode())||"232901".equals(tLCPolSchema.getRiskCode())){
                String hRiskCodeName = "";
                int mi=0;
               LCPolDB cLCPoldb = new LCPolDB();
               cLCPoldb.setContNo(tLCPolSchema.getContNo());
               LCPolSet cLCPolSet = cLCPoldb.query();
                for(int m=1;m<=cLCPolSet.size();m++){
                	String exempSQL = "select 1 from ldcode1  where  codetype='checkexemptionrisk' and (code1='232901' or code1='232701') and code = '"+cLCPolSet.get(m).getRiskCode()+"'";
                	String appendSQL = "select 1 from ldcode1  where codetype='checkappendrisk' and code1 in (select code from ldcode1  where  codetype='checkexemptionrisk'  and (code1='232901' or code1='232701') ) and code = '"+cLCPolSet.get(m).getRiskCode()+"'";
                	SSRS exSSRS = new ExeSQL().execSQL(exempSQL);
                	SSRS apSSRS = new ExeSQL().execSQL(appendSQL);
                	if(exSSRS.MaxRow>0||apSSRS.MaxRow>0){
                		String sRiskCodeName = new ExeSQL().getOneValue("select riskname from lmriskapp where riskcode = '"+cLCPolSet.get(m).getRiskCode()+"'");
                 		if(mi!=0){
                 			hRiskCodeName = hRiskCodeName+ "、"+sRiskCodeName;
                 		}else{
                 			hRiskCodeName += sRiskCodeName;
                 		}
                 		mi++;
                	}
                	
                }
            	int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
            	LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
                tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
                tLCPolSpecRelaSchema.setSpecContent("本险种的被豁免合同为"+hRiskCodeName+"。");
            	mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            	
            }else if ("122601".equals(tLCPolSchema.getRiskCode())
					|| "122901".equals(tLCPolSchema.getRiskCode())) {
				try {
					LCContSubSet tLCContSubSet = new LCContSubSet();
					LCContSubDB tLCContSubDB = new LCContSubDB();
					tLCContSubDB.setPrtNo(tLCPolSchema.getPrtNo());
					tLCContSubSet = tLCContSubDB.query();
					String tTotaldiscountfactor = tLCContSubSet.get(1)
							.getTotaldiscountfactor();
					String tSupdiscountfactor = tLCContSubSet.get(1)
							.getSupdiscountfactor();
					if (1 > Double.parseDouble(tSupdiscountfactor)) {
						int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);

						LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
						tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
								.getGrpContNo());
						tLCPolSpecRelaSchema
								.setContNo(tLCPolSchema.getContNo());
						tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
						tLCPolSpecRelaSchema.setSpecCode(String
								.valueOf(tMaxSpecCode));
						tLCPolSpecRelaSchema.setOperator(tLCPolSchema
								.getOperator());
						PubFun.fillDefaultField(tLCPolSpecRelaSchema);
						tLCPolSpecRelaSchema.setSpecNo(String
								.valueOf(1 + count));
						tLCPolSpecRelaSchema.setSpecContent("保险费包含税优总折扣因子"
								+ tTotaldiscountfactor + "（其中，年龄折扣因子"
								+ tLCContSubSet.get(1).getAgediscountfactor()
								+ "，补充医疗保险折扣因子"
								+ tLCContSubSet.get(1).getSupdiscountfactor()
								+ "，团体投保人数折扣因子"
								+ tLCContSubSet.get(1).getGrpdiscountfactor()
								+ ")。");

						mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);

						LCPolSpecRelaSchema tLCPolSpecRelaSchema1 = new LCPolSpecRelaSchema();
						tLCPolSpecRelaSchema1.setGrpContNo(tLCPolSchema
								.getGrpContNo());
						tLCPolSpecRelaSchema1.setContNo(tLCPolSchema
								.getContNo());
						tLCPolSpecRelaSchema1.setPolNo(tLCPolSchema.getPolNo());
						tLCPolSpecRelaSchema1.setSpecCode(String
								.valueOf(tMaxSpecCode));
						tLCPolSpecRelaSchema1.setOperator(tLCPolSchema
								.getOperator());
						PubFun.fillDefaultField(tLCPolSpecRelaSchema1);
						tLCPolSpecRelaSchema1.setSpecNo(String
								.valueOf(3 + count));
						tLCPolSpecRelaSchema1.setSpecContent("被保险人"
								+ tLCPolSchema.getInsuredName()
								+ "已享受补充医疗保险费率折扣，理赔时需按照顺序进行申请："
								+ "基本医疗保险（或公费医疗）、补充医疗保险、税优健康保险。");

						mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema1);
						LCPolSpecRelaSchema tLCPolSpecRelaSchema2 = new LCPolSpecRelaSchema();
						tLCPolSpecRelaSchema2.setGrpContNo(tLCPolSchema
								.getGrpContNo());
						tLCPolSpecRelaSchema2.setContNo(tLCPolSchema
								.getContNo());
						tLCPolSpecRelaSchema2.setPolNo(tLCPolSchema.getPolNo());
						tLCPolSpecRelaSchema2.setSpecCode(String
								.valueOf(tMaxSpecCode));
						tLCPolSpecRelaSchema2.setOperator(tLCPolSchema
								.getOperator());
						PubFun.fillDefaultField(tLCPolSpecRelaSchema2);
						tLCPolSpecRelaSchema2.setSpecNo(String
								.valueOf(2 + count));
						tLCPolSpecRelaSchema2
								.setSpecContent("被保险人"
										+ tLCPolSchema.getInsuredName()
										+ "提出理赔申请时："
										+ "①若被保险人在其医保所属地以外的医疗机构就医，且其已从公费医疗或基本医疗保险获得费用补偿，则本公司承担的医疗费用范围为被保险人已发生的责任范围内的医疗费用的80%；"
										+ "②若被保险人已参加公费医疗或基本医疗保险，但未从公费医疗或基本医疗保险获得费用补偿,对于符合基本医疗保险基金支付范围内的费用，本公司承担的费用范围为被保险人已发生的上述基本医疗保险基金支付范围内医疗费用的50%；"
										+ "③对于本合同约定的当地基本医疗保险基金支付范围外的医疗必需且合理的进口材料，本公司承担的费用范围为该材料费用的30%。若该进口材料无法用类似国产普通型材料替代的，被保险人需向本公司申请，本公司将按照与国产普通型材料费用相同的方式给予赔付；"
										+ "④被保险人理赔时未从补充医疗保险获得费用补偿，则本公司承担的费用范围为被保险人已发生的责任范围内的医疗费用扣除基本医疗保险（或公费医疗）、其他途径已经补偿金额后的70%。包括基金支付范围内和基金支付范围外的医疗费用。"
										+ "⑤本公司计算本合同保障的医疗费用时，若既符合④，又符合①或②的，优先适用④；若既符合③，又符合①、②或④的，优先适用③。");

						mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema2);

					} else if (1 > Double.parseDouble(tTotaldiscountfactor)) {
						int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
						LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
						tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema
								.getGrpContNo());
						tLCPolSpecRelaSchema
								.setContNo(tLCPolSchema.getContNo());
						tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
						tLCPolSpecRelaSchema.setSpecCode(String
								.valueOf(tMaxSpecCode));
						tLCPolSpecRelaSchema.setOperator(tLCPolSchema
								.getOperator());
						PubFun.fillDefaultField(tLCPolSpecRelaSchema);
						tLCPolSpecRelaSchema.setSpecNo(String
								.valueOf(1 + count));
						tLCPolSpecRelaSchema.setSpecContent("保险费包含税优总折扣因子"
								+ tTotaldiscountfactor + "（其中，年龄折扣因子"
								+ tLCContSubSet.get(1).getAgediscountfactor()
								+ "，补充医疗保险折扣因子"
								+ tLCContSubSet.get(1).getSupdiscountfactor()
								+ "，团体投保人数折扣因子"
								+ tLCContSubSet.get(1).getGrpdiscountfactor()
								+ ")。");

						mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
					}
				} catch (Exception e) {
					System.out.println("折扣因子查询失败！");
					CError.buildErr(this, "折扣因子查询失败！");
					return;
				}
			} else if ("520901".equals(tLCPolSchema.getRiskCode())) {
				//wangyi
				String dutycodeSQL = "select dutycode from lcduty where polno = (select polno from lcpol where contno = '"
						+ tLCCont.getContNo() + "')";
				String dutycode = new ExeSQL().getOneValue(dutycodeSQL);
				String queryInsuredPerson = "select insuredname from lcpol where contno = '" + tLCCont.getContNo()
						+ "'";
				SSRS insuredPersonSSRS = new ExeSQL().execSQL(queryInsuredPerson);
				String insuredPersonStr = "";
				for (int k = 1; k <= insuredPersonSSRS.getMaxRow(); k++) {
					if (k == insuredPersonSSRS.getMaxRow()) {
						insuredPersonStr += insuredPersonSSRS.GetText(k, 1);
					} else {
						insuredPersonStr += insuredPersonSSRS.GetText(k, 1) + "、";
					}
				}
				String Content = "";
				if ("460001".equals(dutycode)) {
					Content += "被保险人保险责任涉及疫苗为一类疫苗，各项保险责任金额详见保障计划表。";
				} else if ("460002".equals(dutycode)) {
					Content += "被保险人保险责任涉及疫苗为一类、二类疫苗，各项保险责任金额详见保障计划表。";
				} else {
				}
				if(!"".equals(Content) && Content != null) {
					int tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tMaxSpecCode = getMaxSpecCode(tLCPolSchema);
					tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
					tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
					tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
					tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
					tLCPolSpecRelaSchema.setSpecCode(String.valueOf(tMaxSpecCode));
					tLCPolSpecRelaSchema.setOperator(tLCPolSchema.getOperator());
					tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
					tLCPolSpecRelaSchema.setSpecNo(String.valueOf(1 + count));
					tLCPolSpecRelaSchema.setSpecContent(Content);
					mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
				}
			}
		}

	}

	// by gzh 获取LCPolSpecRela最大speccode 20111209
	private int getMaxSpecCode(LCPolSchema tLCPolSchema) {
		int tMaxSpecCode = 0;
		boolean tFlag = true;
		if (mLCPolSpecRelaSet.size() == 0) {
			tMaxSpecCode = 1;
		} else {
			for (int maxspecno = 1; maxspecno <= mLCPolSpecRelaSet.size(); maxspecno++) {
				LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
				tLCPolSpecRelaSchema = mLCPolSpecRelaSet.get(maxspecno);
				if (tLCPolSpecRelaSchema.getPolNo().equals(
						tLCPolSchema.getPolNo())) {
					tMaxSpecCode = Integer.parseInt(tLCPolSpecRelaSchema
							.getSpecCode());
					tFlag = false;
					break;
				} else if (Integer.parseInt(tLCPolSpecRelaSchema.getSpecCode()) >= tMaxSpecCode) {
					tMaxSpecCode = Integer.parseInt(tLCPolSpecRelaSchema
							.getSpecCode());// 获取最大的SpecCode
				}
			}
			if (tFlag) {
				tMaxSpecCode = tMaxSpecCode + 1;// 最大SpecCode+1作为新的SpecCode
			}
		}
		return tMaxSpecCode;
	}
}
