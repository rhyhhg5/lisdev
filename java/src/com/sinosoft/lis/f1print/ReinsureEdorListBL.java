package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import java.io.File;

/**
 * <p>Title:保全计算结果明细</p>
 * <p>Description: 保全计算结果明细清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author huxl
 * @version 1.0
 * @date 2007-03-31
 */

public class ReinsureEdorListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    String StartDate = "";
    String EndDate = "";
    String ReRiskSort = "";
    String RecontCode = "";
    String ReRiskCode = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    public ReinsureEdorListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;

        // 准备所有要打印的数据
        if (!downloadEdorData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        ReRiskSort = (String) tTransferData.getValueByName("ReRiskSort");
        RecontCode = (String) tTransferData.getValueByName("RecontCode");
        ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean downloadEdorData() {
        try {
            SysPath = SysPath + "reinsure/";
            String Path = PubFun.getCurrentDate() + "-" + PubFun.getCurrentTime() +
                          ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel tWriteToExcel = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "ReEdorData" +
                           ((RecontCode == null || RecontCode.equals("")) ? "" :
                            "_" + RecontCode) +
                           ((ReRiskCode == null || ReRiskCode.equals("")) ? "" :
                            "_" + ReRiskCode) +
                           ".xls";
            tWriteToExcel.createExcelFile();
            String[] sheetName = {StartDate + "__" + EndDate};
            tWriteToExcel.addSheet(sheetName);

            if (ReRiskSort.equals("1")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += "and a.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += "and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,a.Amnt,c.GetMoney,a.Prem,e.RemainCount,e.ShouldReturnPrem," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',e.EdorBackFee,  " +
                              "d.ReProcFeeRate,d.SpeCemmRate,e.EdorProcFee " +
                              "from LRPol a,LCCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and d.PayCount= 1 and a.ContType = '1' and a.RiskCalSort = '1' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,a.Amnt,c.GetMoney,a.Prem,e.RemainCount,e.ShouldReturnPrem, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',e.EdorBackFee,  " +
                              "d.ReProcFeeRate,d.SpeCemmRate,e.EdorProcFee " +
                              "from LRPol a,LBCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and d.PayCount= 1 and a.ContType = '1' and a.RiskCalSort = '1' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                              "c.EdorValidate,c.GetConfDate,(select CodeName from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'), " +
                              "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex, " +
                              "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt,c.GetMoney,a.Prem,e.RemainCount,e.ShouldReturnPrem," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',e.EdorBackFee,  " +
                              "d.ReProcFeeRate,d.SpeCemmRate,e.EdorProcFee " +
                              "from LRPol a,LCGrpCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and d.PayCount= 1 and a.ContType = '2' and a.RiskCalSort = '1' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                              "c.EdorValidate,c.GetConfDate,(select CodeName from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'), " +
                              "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex, " +
                              "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,a.Amnt,c.GetMoney,a.Prem,e.RemainCount,e.ShouldReturnPrem," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',e.EdorBackFee,  " +
                              "d.ReProcFeeRate,d.SpeCemmRate,e.EdorProcFee " +
                              "from LRPol a,LBGrpCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and d.PayCount= 1 and a.ContType = '2' and a.RiskCalSort = '1' " +
                              sql + " with ur";
                String[][] strArr1Head = new String[1][29];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "失效日期";
                strArr1Head[0][7] = "保全结案日期";
                strArr1Head[0][8] = "缴费方式";

                strArr1Head[0][9] = "团体名称";
                strArr1Head[0][10] = "投保人客户号";
                strArr1Head[0][11] = "被保人客户号";
                strArr1Head[0][12] = "被保险人人数/姓名";
                strArr1Head[0][13] = "平均年龄/生日";
                strArr1Head[0][14] = "性别";
                strArr1Head[0][15] = "身份证号码";
                strArr1Head[0][16] = "职业类别";
                strArr1Head[0][17] = "限额/保额";
                strArr1Head[0][18] = "退保金额";
                strArr1Head[0][19] = "期交保费";
                strArr1Head[0][20] = "待缴期数";
                strArr1Head[0][21] = "应退保费合计";

                strArr1Head[0][22] = "分保类型";
                strArr1Head[0][23] = "成数分出比例";
                strArr1Head[0][24] = "成数分出保额";
                strArr1Head[0][25] = "成数退回保费";
                strArr1Head[0][26] = "分保手续费率";
                strArr1Head[0][27] = "特殊佣金费率";
                strArr1Head[0][28] = "分保手续费摊回";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("2")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,varchar(a.DiseaseAddFeeRate),c.GetMoney,a.Amnt,c.SumBackAmnt, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LCCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '1' and a.RiskCalSort = '2' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,varchar(a.DiseaseAddFeeRate),c.GetMoney,a.Amnt,c.SumBackAmnt, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LBCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '1' and a.RiskCalSort = '2' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                              "c.EdorValidate,c.GetConfDate,(select CodeName from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'), " +
                              "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                              "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,'',c.GetMoney,a.Amnt, " +
                              "c.SumBackAmnt, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LCGrpCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '2' and a.RiskCalSort = '2' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate), " +
                              "c.EdorValidate,c.GetConfDate,(select CodeName from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'), " +
                              "a.AppntName,a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex," +
                              "(select IdNo from LDPerson where CustomerNo = a.InsuredNo),a.OccupationType,'',c.GetMoney,a.Amnt, " +
                              "c.SumBackAmnt, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LBGrpCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.GrpContNo = b.GrpContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '2' and a.RiskCalSort = '2' " +
                              sql + " with ur";
                String[][] strArr1Head = new String[1][31];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "失效日期";
                strArr1Head[0][7] = "保全结案日期";
                strArr1Head[0][8] = "缴费方式";

                strArr1Head[0][9] = "团体名称";
                strArr1Head[0][10] = "投保人客户号";
                strArr1Head[0][11] = "被保人客户号";
                strArr1Head[0][12] = "被保险人人数/姓名";
                strArr1Head[0][13] = "平均年龄/生日";
                strArr1Head[0][14] = "性别";
                strArr1Head[0][15] = "身份证号码";
                strArr1Head[0][16] = "职业类别";
                strArr1Head[0][17] = "加费率";
                strArr1Head[0][18] = "退保金额"; //
                strArr1Head[0][19] = "原始保额";
                strArr1Head[0][20] = "本次退保保额";

                strArr1Head[0][21] = "分保类型";
                strArr1Head[0][22] = "未经过天数";
                strArr1Head[0][23] = "自留额";
                strArr1Head[0][24] = "溢额分出保额";
                strArr1Head[0][25] = "溢额分出比例";
                strArr1Head[0][26] = "净分保费率";
                strArr1Head[0][27] = "分保费退回";
                strArr1Head[0][28] = "分保手续费率";
                strArr1Head[0][29] = "选择折扣率";
                strArr1Head[0][30] = "分保手续费摊回";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("3")) {
                String sql = " and c.GetDataDate between '" + StartDate +
                             "' and '" +
                             EndDate + "' ";
                if (this.RecontCode != null && !this.RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (this.ReRiskCode != null && !this.ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "char(a.InsuYear)||a.InsuYearFlag,char(a.PayEndYear)||a.PayEndYearFlag,Year(a.GetDataDate - a.CValidate)+1 保单年度, " +
                              "(select max(CurPaytoDate) from LRPolDetail where polno = a.Polno), " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                              "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno), " +
                              "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt,c.SumBackAmnt,c.GetMoney, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LCCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '1' and a.RiskCalSort = '3' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',c.EdorValidate,c.GetConfDate, " +
                              "(select CodeName from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),  " +
                              "char(a.InsuYear)||a.InsuYearFlag,char(a.PayEndYear)||a.PayEndYearFlag,Year(a.GetDataDate - a.CValidate)+1 保单年度, " +
                              "(select max(CurPaytoDate) from LRPolDetail where polno = a.Polno), " +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo),  " +
                              "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                              "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno), " +
                              "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt,c.SumBackAmnt,c.GetMoney, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,c.NoPassDays, " +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate,d.CessPremRate,e.EdorBackFee,d.ReProcFeeRate,d.ChoiRebaFeeRate,e.EdorProcFee " +
                              "from LRPol a,LBCont b,LRPolEdor c,LRPolResult d,LRPolEdorResult e " +
                              "where a.ContNo = b.ContNo and a.PolNo = c.Polno and a.PolNo = d.Polno and a.polno = e.Polno " +
                              "and a.RecontCode = c.RecontCode and a.RecontCode = d.RecontCode and a.RecontCode = e.RecontCode  " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount " +
                              "and a.ContType = '1' and a.RiskCalSort = '3' " +
                              sql + " with ur";
                String[][] strArr1Head = new String[1][37];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "失效日期";
                strArr1Head[0][7] = "保全结案日期";
                strArr1Head[0][8] = "缴费方式";
                strArr1Head[0][9] = "保险期间";
                strArr1Head[0][10] = "缴费期间";
                strArr1Head[0][11] = "保单年度";
                strArr1Head[0][12] = "交至日";

                strArr1Head[0][13] = "团体名称";
                strArr1Head[0][14] = "投保人客户号";
                strArr1Head[0][15] = "被保人客户号";
                strArr1Head[0][16] = "被保险人人数/姓名";
                strArr1Head[0][17] = "平均年龄/生日";
                strArr1Head[0][18] = "性别";
                strArr1Head[0][19] = "身份证号码";
                strArr1Head[0][20] = "职业类别";
                strArr1Head[0][21] = "吸烟状态";
                strArr1Head[0][22] = "死亡加费率";
                strArr1Head[0][23] = "重疾加费率";
                strArr1Head[0][24] = "原始保额";
                strArr1Head[0][25] = "本次退保保额";
                strArr1Head[0][26] = "退保金额";

                strArr1Head[0][27] = "分保类型";
                strArr1Head[0][28] = "未经过天数";
                strArr1Head[0][29] = "自留额";
                strArr1Head[0][30] = "溢额分出保额";
                strArr1Head[0][31] = "溢额分出比例";
                strArr1Head[0][32] = "净分保费率";
                strArr1Head[0][33] = "分保费退回";
                strArr1Head[0][34] = "分保手续费率";
                strArr1Head[0][35] = "选择折扣率";
                strArr1Head[0][36] = "分保手续费摊回";

                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            }
            tWriteToExcel.write(SysPath);
            String NewPath = PubFun.getCurrentDate() + "-" +
                             PubFun.getCurrentTime() + ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = SysPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath!  : " + NewPath);

        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
