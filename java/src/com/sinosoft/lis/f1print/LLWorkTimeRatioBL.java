package com.sinosoft.lis.f1print;

/**
 * <p>Title: 承保和赔付汇总表</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author St.GN
 * @version 1.0
 */

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLWorkTimeRatioBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mFileNameB = "";
    private String moperator = "";
    private String mMakeDate = "";

    public LLWorkTimeRatioBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }else{
    		TransferData tTransferData= new TransferData();
    		tTransferData.setNameAndValue("tFileNameB",mFileNameB );
    		tTransferData.setNameAndValue("tMakeDate", mMakeDate);
    		tTransferData.setNameAndValue("tOperator", moperator);
    		LLPrintSave tLLPrintSave = new LLPrintSave();
    		VData tVData = new VData();
    		tVData.addElement(tTransferData);
    		if(!tLLPrintSave.submitData(tVData,"")){
    			return false;
    		     }
    	}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
        moperator = mGlobalInput.Operator;
        mMakeDate = PubFun.getCurrentDate();
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLWorkTimeRatioBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的参数
        String MngCom = (String) mTransferData.getValueByName("MngCom");

//        String MngName = (String) mTransferData.getValueByName("MngName");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        //人均处理信息
        ListTable PersondealTable = new ListTable();
        PersondealTable.setName("PERSONDEAL");
        String[] Title1 = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  " select distinct decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '01' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '02' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '03' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '04' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '05' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '06' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '11' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '07' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2), "
                         + "  decimal( (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '10' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')/decimal(num.a),5,2) "
                         + "  from llcaseoptime, (select count(usercode) a from llclaimuser where comcode = '"+MngCom+"' and stateflag = '1' and handleflag = '1') as num "
                         ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                PersondealTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //人均时效信息
        ListTable PersonratioTable = new ListTable();
        PersonratioTable.setName("PERSONRATIO");
        String[] Title2 = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =   " select distinct "
                           + " case when A is null or num is null or num = 0 then 0 else decimal(A/decimal(num*3600),12,2) end ,"
                           + " case when B is null or num is null or num = 0 then 0 else decimal(B/decimal(num*3600),12,2) end,"
                           + " case when C is null or num is null or num = 0 then 0 else decimal(C/decimal(num*3600),12,2) end,"
                           + " case when D is null or num is null or num = 0 then 0 else decimal(D/decimal(num*3600),12,2) end,"
                           + " case when E is null or num is null or num = 0 then 0 else decimal(E/decimal(num*3600),12,2) end,"
                           + " case when F is null or num is null or num = 0 then 0 else decimal(F/decimal(num*3600),12,2) end,"
                           + " case when G is null or num is null or num = 0 then 0 else decimal(G/decimal(num*3600),12,2) end,"
                           + " case when H is null or num is null or num = 0 then 0 else decimal(H/decimal(num*3600),12,2) end,"
                           + " case when I is null or num is null or num = 0 then 0 else decimal(I/decimal(num*3600),12,2) end "
                           + " from (	select distinct"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '01' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') A,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '02' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') B,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '03' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') C,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '04' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') D,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '05' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') E,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '06' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') F,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '11' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') G,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '07' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') H,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '10' and sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') I,"
                           + " (select count(usercode) a from llclaimuser where comcode = '"+MngCom+"' and stateflag = '1' and handleflag = '1') as num  from llcaseoptime ) as x"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                PersonratioTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //平均时效信息
        ListTable AvgTable = new ListTable();
        AvgTable.setName("AVERAGERATIO");
        String[] Title5 = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =   "select  distinct "
                           + " case when A is null or A1 is null or A1 = 0 then 0 else decimal(A/decimal(A1*3600),12,2) end,"
                           + " case when B is null or B1 is null or B1 = 0 then 0 else decimal(B/decimal(B1*3600),12,2) end,"
                           + " case when C is null or C1 is null or C1 = 0 then 0 else decimal(C/decimal(C1*3600),12,2) end,"
                           + " case when D is null or D1 is null or D1 = 0 then 0 else decimal(D/decimal(D1*3600),12,2) end,"
                           + " case when E is null or E1 is null or E1 = 0 then 0 else decimal(E/decimal(E1*3600),12,2) end,"
                           + " case when F is null or F1 is null or F1 = 0 then 0 else decimal(F/decimal(F1*3600),12,2) end,"
                           + " case when G is null or G1 is null or G1 = 0 then 0 else decimal(G/decimal(G1*3600),12,2) end,"
                           + " case when H is null or H1 is null or H1 = 0 then 0 else decimal(H/decimal(H1*3600),12,2) end,"
                           + " case when I is null or I1 is null or I1 = 0 then 0 else decimal(I/decimal(I1*3600),12,2) end "
                           + "from   (select "
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '01' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') A,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '02' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') B,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '03' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') C,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '04' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') D,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '05' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') E,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '06' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') F,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '11' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') G,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '07' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') H,"
                           + " (select sum(getseconds(a.optime)) from llcaseoptime a, llcase b where a.rgtstate = '10' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') I,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '01' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') A1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '02' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') B1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '03' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') C1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '04' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') D1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '05' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') E1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '06' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') F1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '11' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') G1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '07' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') H1,"
                           + " (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '10' and a.sequance = 1 and a.caseno = b.caseno and b.endcasedate is not null and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') I1 "
                           + "	from llcaseoptime	) as x"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                AvgTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //案件数量信息
        ListTable CasenumTable = new ListTable();
        CasenumTable.setName("CASENUM");
        String[] Title = {
                         "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                         "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  " select distinct"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '01' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '02' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '03' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '04' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '05' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '06' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '11' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '07' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          +" (select count(a.caseno) from llcaseoptime a, llcase b where a.rgtstate = '10' and a.sequance = 1 and a.managecom = '"+MngCom+"' and a.caseno = b.caseno and b.endcasedate is not null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"')"
                          + " from   llcaseoptime"
                          ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                CasenumTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //月度未完成信息
        ListTable NonCompleTable = new ListTable();
        NonCompleTable.setName("UNFINISH");
        String[] Title3 = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql = "select distinct "
                         + " case when A is null or A1 is null or A1 = 0 then 0 else decimal(A*100/decimal(A1),12,2) end,"
                         + " case when B is null or B1 is null or B1 = 0 then 0 else decimal(B*100/decimal(B1),12,2) end,"
                         + " case when C is null or C1 is null or C1 = 0 then 0 else decimal(C*100/decimal(C1),12,2) end,"
                         + " case when D is null or D1 is null or D1 = 0 then 0 else decimal(D*100/decimal(D1),12,2) end,"
                         + " case when E is null or E1 is null or E1 = 0 then 0 else decimal(E*100/decimal(E1),12,2) end,"
                         + " case when F is null or F1 is null or F1 = 0 then 0 else decimal(F*100/decimal(F1),12,2) end,"
                         + " case when G is null or G1 is null or G1 = 0 then 0 else decimal(G*100/decimal(G1),12,2) end,"
                         + " case when H is null or H1 is null or H1 = 0 then 0 else decimal(H*100/decimal(H1),12,2) end,"
                         + " case when I is null or I1 is null or I1 = 0 then 0 else decimal(I*100/decimal(I1),12,2) end "
                         + " from (select "
                         + " (select count(caseno) from llcase where rgtstate = '01' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') A,"
                         + " (select count(caseno) from llcase where rgtstate = '02' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') B,"
                         + " (select count(caseno) from llcase where rgtstate = '03' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') C,"
                         + " (select count(caseno) from llcase where rgtstate = '04' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') D,"
                         + " (select count(caseno) from llcase where rgtstate = '05' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') E,"
                         + " (select count(caseno) from llcase where rgtstate = '06' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') F,"
                         + " (select count(caseno) from llcase where rgtstate = '11' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') G,"
                         + " (select count(caseno) from llcase where rgtstate = '07' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') H,"
                         + " (select count(caseno) from llcase where rgtstate = '10' and endcasedate is null and rgtdate between '"+StartDate+"' and '"+EndDate+"') I,"
                         + " (select count(caseno) from llcase where rgtstate = '01' and rgtdate between '"+StartDate+"' and '"+EndDate+"') A1,"
                         + " (select count(caseno) from llcase where rgtstate = '02' and rgtdate between '"+StartDate+"' and '"+EndDate+"') B1,"
                         + " (select count(caseno) from llcase where rgtstate = '03' and rgtdate between '"+StartDate+"' and '"+EndDate+"') C1,"
                         + " (select count(caseno) from llcase where rgtstate = '04' and rgtdate between '"+StartDate+"' and '"+EndDate+"') D1,"
                         + " (select count(caseno) from llcase where rgtstate = '05' and rgtdate between '"+StartDate+"' and '"+EndDate+"') E1,"
                         + " (select count(caseno) from llcase where rgtstate = '06' and rgtdate between '"+StartDate+"' and '"+EndDate+"') F1,"
                         + " (select count(caseno) from llcase where rgtstate = '11' and rgtdate between '"+StartDate+"' and '"+EndDate+"') G1,"
                         + " (select count(caseno) from llcase where rgtstate = '07' and rgtdate between '"+StartDate+"' and '"+EndDate+"') H1,"
                         + " (select count(caseno) from llcase where rgtstate = '10' and rgtdate between '"+StartDate+"' and '"+EndDate+"') I1  from llcase ) as x"
                         ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                NonCompleTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //回退信息
        ListTable BackTable = new ListTable();
        BackTable.setName("BACKNUM");
        String[] Title4 = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  " select distinct"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '01' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '02' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '03' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '04' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '05' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '06' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '11' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '07' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"'),"
                          + " (select count(a.caseno) from llcaseoptime a, llcase b where a.caseno = b.caseno and b.endcasedate is not null and a.rgtstate = '10' and a.sequance >1 and a.managecom = '"+MngCom+"' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') "
                          + " from   llcaseoptime"
                          ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[9];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];

                BackTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLWorkTimeRatioBL";
            tError.functionName = "";
            tError.errorMessage = "信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLWorkTimeRatio.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";

        //LLWorkTimeRatioBL模版元素
        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);

        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" + MngCom +
                       "'";
        System.out.println(sql_M);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("MngCom", temp_M[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息

        xmlexport.addListTable(CasenumTable, Title);
        xmlexport.addListTable(PersondealTable, Title1);
        xmlexport.addListTable(PersonratioTable, Title2);
        xmlexport.addListTable(NonCompleTable, Title3);
        xmlexport.addListTable(BackTable, Title4);
        xmlexport.addListTable(AvgTable, Title5);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {

        LLWorkTimeRatioBL tLLWorkTimeRatioBL = new LLWorkTimeRatioBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("000000142");
        tVData.addElement(tLDPersonSchema);
        tLLWorkTimeRatioBL.submitData(tVData, "PRINT");
        VData vData = tLLWorkTimeRatioBL.getResult();

    }

    private void jbInit() throws Exception {
    }

}
