package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description:X4保全日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FenFinDayBQPremBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 取得的时间
	private String mDay[] = null;

	// 业务处理相关变量
	private String mRiskCode;

	private String mRiskType;

	private String mXsqd;

	private String mXzdl;

	private String mSxq;

	private String mDqj;

	private LMRiskAppSet mLMRiskAppSet;

	private String mRiskName = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public FenFinDayBQPremBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();
		/*
		 * 准备所有要打印的数据 if (cOperate.equals("PRINTGET")) //打印付费 { if(
		 * !getPrintDataGet() ) { return false; } }
		 */
		LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		this.mLMRiskAppSet = tLMRiskAppDB.query();
		if (cOperate.equals("PRINTPAY")) // 打印收费
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[2];
		tDay[0] = "2004-1-13";
		tDay[1] = "2004-1-13";
		vData.addElement(tDay);
		vData.addElement(tG);

		FenFinDayBQPremBL tF = new FenFinDayBQPremBL();
		tF.submitData(vData, "PRINTPAY");

	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		// 11-26
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinDayCheckBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 新的打印方法
	 * 
	 * @return
	 */
	private boolean getPrintDataPay() {
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);

		 String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		 tServerPath = "E:/sino/ui/";
		ExeSQL tExeSQL = new ExeSQL();

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("FenFinDayBQPrem.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] { "类型", "渠道", "批单号码", "保单号码", "金额" };
		String[] totalArr = new String[] { "类型", "总金额" };

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { "BQTF", // 保全退费
				"BQTBJ", // 退保金
				"BQYEI", // YEI
				"BQMJ", // 满期金
				"BQSSBF", // 实收保费
				"BQTJ", // 体检费
				"BQFX", // 保单复效利息
				"BQCJ", // 保户储金及投资款
				"BQTZLX", // 投资款业务利息
				"BQGL", // 账户管理费
				"BQGB", // 工本费
				"WNSSBF_BJCJ3",//万能实收保费-保户储金
				"WNSSBF_ZHGLF3",//万能实收保费-账户管理费
				"XTXSSBF_BHCJ3",//新特需实收保费-保户储金
				"XTXSSBF_ZHGLF3",//新特需实收保费-账户管理费
				"WNYJ_ZHLX3",//万能月结-账户利息
				"WNYJ_FXBF3",//万能月结-风险保费
				"WNYJ_BDGLF3",//万能月结-保单管理费
//				"WNBFLQ_ZHGLF3",//万能部分领取-账户管理费
//				"WNBQTB_BDGLF3",//万能保全退保-保单管理费
				"WNBQTB_FXKF3",//万能保全退保-风险扣费
				"WNBQTB_ZHLX3",//万能保全退保-账户利息
				"WNBQTB_ZHGLF3",//万能保全退保-账户管理费
//				"XTXBQXYTB_JYGLF3",//新特需保全协议退保-解约管理费				
				"BQCM",			//万能资料变更
				"BQDQJSJE",		//定期结算
//				"XTXBFLQ_ZHGLF3",//新特需部分领取-账户管理费
//				"XTXBQTB_BDGLF3",//新特需保全退保-保单管理费
//				"XTXBQTB_FXKF3",//新特需保全退保-风险扣费
//				"XTXBQTB_ZHLX3",//新特需保全退保-账户利息
//				"XTXBQTB_ZHGLF3"//新特需保全退保-账户管理费
				"WNBJ_BFLQ",       //万能——本金——部分领取
				"WNGLF_BFLQ",      //万能——利息——部分领取
				"XTXBJ_BFLQ",      //新特需——本金——部分领取
				"XTXGLF_BFLQ",     //新特需——本金——部分领取
				"WN_CXJJ",         //万能-持续奖金
				"WN_JYGLF",        //万能-解约管理费
				"BDZF",            //保单作废
				"YDTB_FC",		//约定缴费退保应收反冲
				"HL_XJ",           //分红险红利结算-现金领取：包括周年日领取和退保时临时结算领取
				"HL_LX"            //保单红利支出—累积生息：包括周年日进账户和退保时临时结算账户利息
		};

		for (int i = 0; i < getTypes.length; i++) {
			String msql = tGetSQLFromXML.getSql(tServerPath
					+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
			ListTable tDetailListTable = getDetailListTable(msql, getTypes[i]); // 明细ListTable
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象

			// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
			msql = tGetSQLFromXML
					.getSql(tServerPath
							+ "f1print/picctemplate/FenFeePrintSql.xml",
							getTypes[i] + "Z");
			ListTable tTotalListTabel = getHZListTable(msql, getTypes[i] + "Z"); // 汇总ListTable
			xmlexport.addListTable(tTotalListTabel, totalArr); // 汇总ListTable
																// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

	/**
	 * 获得明细ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("雪"+msql);
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strSum = "";
		String strArr[] = null;
		tlistTable.setName(tName);
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[5];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 5) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					System.out.println("金额******"+strArr[j - 1]);
					strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					continue;
				}
				strArr[j - 1] = tSSRS.GetText(i, j);
			}
			tlistTable.add(strArr);
		}
		return tlistTable;
	}

	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("BQTFZ".equals(pName)) {
			result[0] = "保全退费 小计";
		} else if ("BQTBJZ".equals(pName)) {
			result[0] = "退保金 小计";
		} else if ("BQYEIZ".equals(pName)) {
			result[0] = "YEI 小计";
		} else if ("BQMJZ".equals(pName)) {
			result[0] = "满期金 小计";
		} else if ("BQSSBFZ".equals(pName)) {
			result[0] = "实收保费 小计";
		} else if ("BQTJZ".equals(pName)) {
			result[0] = "体检费 小计";
		} else if ("BQFXZ".equals(pName)) {
			result[0] = "保单复效利息 小计";
		} else if ("BQCJZ".equals(pName)) {
			result[0] = "保户储金及投资款 小计";
		} else if ("BQTZLXZ".equals(pName)) {
			result[0] = "投资款业务利息 小计";
		} else if ("BQGLZ".equals(pName)) {
			result[0] = "账户管理费 小计";
		} else if ("BQGBZ".equals(pName)) {
			result[0] = "工本费 小计";
		}
		else if ("WNSSBF_BJCJ3Z".equals(pName)) {
			result[0] = "万能实收保费-保户储金 小计";
		} else if ("WNSSBF_ZHGLF3Z".equals(pName)) {
			result[0] = "能实收保费-账户管理费 小计";
		} else if ("XTXSSBF_BHCJ3Z".equals(pName)) {
			result[0] = "新特需实收保费-保户储金 小计";
		} else if ("XTXSSBF_ZHGLF3Z".equals(pName)) {
			result[0] = "新特需实收保费-账户管理费 小计";
		} else if ("WNYJ_ZHLX3Z".equals(pName)) {
			result[0] = "万能月结-账户利息 小计";
		} else if ("WNYJ_FXBF3Z".equals(pName)) {
			result[0] = "万能月结-风险保费 小计";
		} else if ("WNYJ_BDGLF3Z".equals(pName)) {
			result[0] = "万能月结-保单管理费 小计";
		} else if ("WNBFLQ_ZHGLF3Z".equals(pName)) {
			result[0] = "万能部分领取-账户管理费 小计";
		} else if ("WNBQTB_BDGLF3Z".equals(pName)) {
			result[0] = "万能保全退保-保单管理费 小计";
		} else if ("WNBQTB_FXKF3Z".equals(pName)) {
			result[0] = "万能保全退保-风险扣费 小计";
		} else if ("WNBQTB_ZHLX3Z".equals(pName)) {
			result[0] = "万能保全退保-账户利息 小计";
		} else if ("WNBQTB_ZHGLF3Z".equals(pName)) {
			result[0] = "万能保全退保-账户管理费 小计";
		} else if ("XTXBQXYTB_JYGLF3Z".equals(pName)) {
			result[0] = "新特需保全协议退保-解约管理费 小计";
		} else if ("BQCMJZ".equals(pName)) {
			result[0] = "万能客户资料变更 小计";
		} else if ("BQDQJSJEHJ".equals(pName)) {
			result[0] = "保全定期结算 小计";	
		}else if ("WNBJ_BFLQZ".equals(pName)) {
			result[0] = "万能保全部分领取-本金 小计";	
		}else if ("WNGLF_BFLQZ".equals(pName)) {
			result[0] = "万能保全部分领取-利息 小计";	
		}else if ("XTXBJ_BFLQZ".equals(pName)) {
			result[0] = "新特需保全部分领取-本金  小计";	
		}else if ("XTXGLF_BFLQZ".equals(pName)) {
			result[0] = "新特需保全部分领取-管理费 小计";	
		}else if ("WN_CXJJZ".equals(pName)) {
			result[0] = "万能-持续奖金 小计";	
		}else if ("WN_JYGLFZ".equals(pName)) {
			result[0] = "万能-解约管理费 小计";	
		}else if ("BDZFZ".equals(pName)) {
			result[0] = "保单作废 小计";	
		}else if ("YDTB_FCZ".equals(pName)) {
			result[0] = "约定缴费退保反冲 小计";
		}else if ("HL_XJZ".equals(pName)) {
			result[0] = "分红险红利结算-现金领取 小计";	
		}else if ("HL_LXZ".equals(pName)) {
			result[0] = "分红险红利结算-累积生息 小计";	
		}


		result[1] = "0";
		return result;
	}

	/**
	 * 获得汇总ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getHZListTable(String msql, String tName) {
		ListTable tHZlistTable = new ListTable();
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		tHZlistTable.setName(tName); // 设置Table名字
		String[] strArr = new String[2];
		if (tSSRS.MaxRow > 0) {
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				strArr[j - 1] = tSSRS.GetText(1, j);
			}
		} else {
			strArr = getChineseNameWithPY(tName);
		}
		tHZlistTable.add(strArr);
		return tHZlistTable;
	}

	/**
	 * 以前的打印方法
	 * @return
	 */
	private boolean getPrintDataPayOld() {
		SSRS tSSRS = new SSRS();
		double SumMoney = 0;

		// String msql = "select a.c,sum(a) from ("+
		// "select b.endorsementno c,(a.sumactupaymoney) a from LJAPay
		// a,ljagetendorse b where 1=1 "+
		// "and a.ConfDate>='"+mDay[0]+"' and a.ConfDate<='"+mDay[1]+"' and
		// a.ManageCom like '"+mGlobalInput.ManageCom+"%' "+
		// "and a.sumactupaymoney <> 0 "+
		// "and (b.riskcode not in (Select RiskCode From LMRiskApp Where
		// Risktype3 = '7' and riskcode not in ('170101','170301')) or
		// b.riskcode is null) "+
		// "and a.IncomeType in('10','3') and a.payno = b.actugetno and
		// b.FeeFinaType in ('BF','LX','TB','JK','HK','HD','GB','YHSL','TJ') " +
		// "union " +
		// "select b.endorsementno c,(a.sumgetmoney) a from LJAGet
		// a,ljagetendorse b where 1=1 " +
		// "and a.ConfDate>='"+mDay[0]+"' and a.ConfDate<='"+mDay[1]+"' and
		// a.ManageCom like '"+mGlobalInput.ManageCom+"%' "+
		// "and a.sumgetmoney <> 0 "+
		// "and (b.riskcode not in (Select RiskCode From LMRiskApp Where
		// Risktype3 = '7' and riskcode not in ('170101','170301')) or
		// b.riskcode is null) "+
		// "and a.Othernotype in ('10','3') and a.paymode not in ('B','J') and
		// a.actugetno = b.actugetno "+
		// "and b.FeeFinaType in ('BF','LX','HK','HD','GB','YHSL','TJ') "+
		// ") a group by a.c";
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		String tServerPath = (new ExeSQL())
				.getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		String msql = tGetSQLFromXML.getSql(tServerPath
				+ "f1print/picctemplate/FeePrintSql.xml", "BQPrem");

		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strArr[] = null;
		tlistTable.setName("RISK");
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[2];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 1) {
					strArr[j - 1] = tSSRS.GetText(i, j);
				}
				if (j == 2) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					String strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					System.out.println(Double.parseDouble(strArr[j - 1]));
					SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);

				}
			}
			tlistTable.add(strArr);
		}

		strArr = new String[2];
		strArr[0] = "ENDORNO";
		strArr[1] = "Money";

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'";
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);

		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("FinDayBQPrem.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		texttag.add("SumMoney", SumMoney);
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		xmlexport.addListTable(tlistTable, strArr);
		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

}
