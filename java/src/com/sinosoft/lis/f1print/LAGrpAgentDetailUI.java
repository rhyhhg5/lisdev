package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAGrpAgentDetailBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LAGrpAgentDetailUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAGrpAgentDetailUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAGrpAgentDetailBL tLAGrpAgentDetailBL = new LAGrpAgentDetailBL();

        if (!tLAGrpAgentDetailBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAGrpAgentDetailBL.mErrors);

            return false;
        } else {
            this.mResult = tLAGrpAgentDetailBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
