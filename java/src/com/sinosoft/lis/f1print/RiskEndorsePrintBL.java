package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class RiskEndorsePrintBL implements PrintService{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCContSchema tLCContSchema = new LCContSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
//    private LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
//    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    private LJAGetDB tLJAGetDB = new LJAGetDB();
    private LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    private TextTag mTextTag = null;
    private XmlExport mXmlExport = null;
    private Object tExeSQL;
    private ExeSQL mExeSQL = new ExeSQL();
    private String comcode = null;
    
    private String mflag = null;
    public RiskEndorsePrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("RiskEndorsePrintBL begin");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mflag = cOperate;
        mResult.clear();

       if (!getPrintData1()) {
          return false;
       }

        System.out.println("RiskEndorsePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError); 
    }
    
    private boolean getPrintData1(){
    	
    	mXmlExport = new XmlExport();
        mXmlExport.createDocument("RiskEndorsePrint.vts", "printer");
        mTextTag = new TextTag();
//        mXmlExport.createDocument("RiskEndorsePrint.vts", "printer");
        mTextTag.add("JetFormType", "RL000");
        if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

         mTextTag.add("ManageComLength4", printcode);
         mTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
         if(mflag.equals("batch")){
        	 mTextTag.add("previewflag", "0");
         }else{
        	 mTextTag.add("previewflag", "1");
         }
        mTextTag.add("Actugetno", tLJAGetDB.getActuGetNo());
        mTextTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        mTextTag.add("Contno",tLCContSchema.getContNo());
        mTextTag.add("Actugetno1", tLJAGetDB.getActuGetNo());
//        mTextTag.add("AppName", mExeSQL.getOneValue("select appntname from lcappnt where appntno='"+tLCContSchema.getAppntNo()+"'"));
        String strAppnt="select appntname from lcappnt where contno='"+tLCContSchema.getAppntNo()+"' "
        				+ " union "
        				+" select appntname from lbappnt where contno='"+tLCContSchema.getAppntNo()+"'";
        
        String appntName=mExeSQL.getOneValue(strAppnt);
        mTextTag.add("AppName",appntName);
        mTextTag.add("Money", tLJAGetDB.getSumGetMoney());
        mTextTag.add("Paymode", mExeSQL.getOneValue("select codename from ldcode where codetype='paymode' and code='"+tLJAGetDB.getPayMode()+"'"));
    	if(!"".equals(tLJAGetDB.getBankCode()) && tLJAGetDB.getBankCode()!=null){
    		mTextTag.add("Bank", mExeSQL.getOneValue("select bankname from ldbank where bankcode='"+tLJAGetDB.getBankCode()+"'"));
    	}else{
    		mTextTag.add("Bank", "-");
    	}
    	if(!"".equals(tLJAGetDB.getAccName()) && tLJAGetDB.getAccName()!=null){
    		mTextTag.add("AccName", tLJAGetDB.getAccName());
    	}else{
    		mTextTag.add("AccName", "-");
    	}
    	if(!"".equals(tLJAGetDB.getBankAccNo()) && tLJAGetDB.getBankAccNo()!=null){
    		mTextTag.add("AccNo", tLJAGetDB.getBankAccNo());
    	}else{
    		mTextTag.add("AccNo", "-");
    	}
    	
        
    	mXmlExport.addTextTag(mTextTag);
    	mXmlExport.outputDocumentToFile("C:/","RiskEndorsePrint13131");
        mResult.clear();
        mResult.addElement(mXmlExport);


       
            //放入打印列表
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJAGetDB.getActuGetNo());
            tLOPRTManagerDB.setCode("RL000");
            needPrt = tLOPRTManagerDB.query().size();
System.out.println("-------------------------the size is "+needPrt+"--------------------------");
            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit("86110000");
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
                mLOPRTManagerSchema.setOtherNoType("7");
//                mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INDIINFORM);
                mLOPRTManagerSchema.setCode("RL000");
                mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJAGetDB.getActuGetNo()); //这里存放 （也为交费收据号）
            }else{
            	mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
            }
               mResult.add(mLOPRTManagerSchema);

    	return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        comcode = mGlobalInput.ComCode;
//        xmlexport = (XmlExport)cInputData.getObjectByObjectName("XmlExport", 0);
//        textTag = (TextTag)cInputData.getObjectByObjectName("TextTag", 0);
        LOPRTManagerSchema ttLOPRTManagerSet = new LOPRTManagerSchema();
        ttLOPRTManagerSet = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setStandbyFlag2(ttLOPRTManagerSet.getStandbyFlag2());
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
        if(tLOPRTManagerSet!=null && tLOPRTManagerSet.size()>0){
        mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        }
        if(ttLOPRTManagerSet == null)
        {//为空--VTS打印入口
            System.out.println("---------为空--VTS打印入口！");
            tLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);
            tLJAGetDB = (LJAGetDB) cInputData.getObjectByObjectName("LJAGetDB", 0);
            if (tLCContSchema == null)
            {
                buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
                return false;
            }
            return true;
        }
        else
        {//PDF入口
            //取得LJSPayBDB
            System.out.println("---------不为空--PDF入口！");
            LJAGetDB mLJAGetDB = new LJAGetDB();
            mLJAGetDB.setActuGetNo(ttLOPRTManagerSet.getStandbyFlag2());
            if(!mLJAGetDB.getInfo())
            {
                mErrors.addOneError("PDF入口LJAGetDB传入的数据不完整。");
                return false;
            }
            tLJAGetDB = mLJAGetDB.getDB();

            //取得tLCContSchema
            LCContDB mLCContDB = new LCContDB();
            mLCContDB.setContNo(mLJAGetDB.getOtherNo());
            if(!mLCContDB.getInfo())
            {
            	LBContDB mLBContDB=new LBContDB();
            	mLBContDB.setContNo(mLJAGetDB.getOtherNo());
            	if(!mLBContDB.getInfo()){
            		buildError("getInputData", "获取保单信息失败！");
                    return false;
            	}
            	Reflections tReflections=new Reflections();
            	tReflections.transFields(tLCContSchema, mLBContDB.getSchema());
            }else{
            	tLCContSchema = mLCContDB.getSchema();
            }
            return true;
        }

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

//

    public static void main(String[] a)
    {
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setActuGetNo("37000816912");
        tLJAGetDB.getInfo();

        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLJAGetDB.getOtherNo());
        tLCContSchema.setContNo(tLJAGetDB.getOtherNo());
        LCContSet tLCContSet = new LCContSet();
        tLCContSet=tLCContDB.query();
        tLCContSchema = tLCContSet.get(1);

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tLCContSchema);
        tVData.addElement(tG);
        tVData.addElement(tLJAGetDB);

        RiskEndorsePrintBL bl = new RiskEndorsePrintBL();
        if(bl.submitData(tVData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
