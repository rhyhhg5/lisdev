package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.pubfun.*;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAAnnuityDetailBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mAnnuityYear = "";

    private String mAnnuityMonth = "";

    private String mManageCom = "";

    private XmlExport mXmlExport = null;

    private SSRS mSSRS = new SSRS();

    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAAnnuityDetailBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!getListData()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }

        this.mAnnuityYear = (String) mTransferData.getValueByName("AnnuityYear");
        this.mAnnuityMonth = (String) mTransferData.getValueByName(
                "AnnuityMonth");
        this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
        String calNo = "";
        calNo = mAnnuityYear + mAnnuityMonth;

        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$" + calNo);

        ExeSQL tExeSQL = new ExeSQL();

        sql.append(
                "SELECT a.agentcode,b.name,a.F04,a.F06,a.F11, a.K04 FROM lawage a,laagent b");
        sql.append(" WHERE a.agentcode=b.agentcode and a.indexcalno='" +
                   calNo + "' AND");
        sql.append(" a.managecom like '" + mManageCom +
                   "%'  and a.BranchType='1' and a.BranchType2='01'  order by a.agentcode");

        mSSRS = tExeSQL.execSQL(sql.toString());

        if (tExeSQL.mErrors.needDealError()) {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "查询XML数据出错！";

            this.mErrors.addOneError(tCError);

            return false;

        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LAAnnuityDetailReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }

        tTextTag.add("AnnuityYear", mAnnuityYear);
        tTextTag.add("AnnuityMonth", mAnnuityMonth);
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"营销员代码", "营销员姓名", "首年保费佣金", "绩效津贴", "职务津贴", "公司应付养老金"};

        if (!getListTable()) {
            return false;
        }

        mXmlExport.addListTable(mListTable, title);

        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS.getMaxRow() > 0) {
            double dFirst = 0;
            double dTwo = 0;
            double dThree = 0;
            double dFour = 0;
            System.out.println("232323232323232323232" + mSSRS.getMaxCol());
            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
                String Info[] = new String[6];

                for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                    Info[j - 1] = mSSRS.GetText(i, j);
                }

                mListTable.add(Info);
                dFirst = dFirst + Double.parseDouble(mSSRS.GetText(i, 3));
                dTwo = dTwo + Double.parseDouble(mSSRS.GetText(i, 4));
                dThree = dThree + Double.parseDouble(mSSRS.GetText(i, 5));
                dFour = dFour + Double.parseDouble(mSSRS.GetText(i, 6));

            }

            String InfoCount[] = new String[6];

            InfoCount[0] = "合计";
            InfoCount[1] = "";
            InfoCount[2] = String.valueOf(dFirst);
            InfoCount[3] = String.valueOf(dTwo);
            InfoCount[4] = String.valueOf(dThree);
            InfoCount[5] = String.valueOf(dFour);

            mListTable.add(InfoCount);

            mListTable.setName("ZT");

        } else {

            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的数据！";

            this.mErrors.addOneError(tCError);

            return false;

        }

        return true;

    }

    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LAAnnuityDetailBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }
}
