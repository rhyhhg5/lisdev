package com.sinosoft.lis.f1print;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLManageComControlBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mFileNameB = "";

	private String mMakeDate = "";

	private String mContType = "";
	
	private PubFun mPubFun = new PubFun();
	
	private String mStartDate="";
	
	private String mEndDate="";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				this.bulidErrorB("LLPrintSave-->submitData", "数据不完整");
				return false;
			}
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLUserControlBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			mOperator = (String)mTransferData.getValueByName("tOperator");
			mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
			mContType = (String) mTransferData.getValueByName("tContType");
			mStartDate = (String) mTransferData.getValueByName("tStartDate");
			mEndDate = (String) mTransferData.getValueByName("tEndDate");
			this.mManageCom = (String) mTransferData
					.getValueByName("tManageCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
			if (mManageCom == null || mManageCom.equals("")) {
				this.mManageCom = "86";
			}
			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLManageComControl.vts", "printer");

		String tMakeDate = mPubFun.getCurrentDate();
		System.out.print("dayin252");
		String sql = "select name from ldcom where comcode='" + mManageCom
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		String mManageComName = tExeSQL.getOneValue(sql);

		mMakeDate = tMakeDate;
		System.out.println("1212121" + tMakeDate);
		String[] title = { "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "",""};
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private String getYear(String pmDate) {
		String mYear = "";
		String tSQL = "";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		mYear = tSSRS.GetText(1, 1);
		return mYear;
	}

	private boolean getDataList() {

		SSRS tSSRS = new SSRS();
		String sql="";
		if(mManageCom.length()==2){
		 sql = "select comcode,name from ldcom where comcode like '"
				+ mManageCom + "%' and sign='1' and length(trim(comcode))=4 order by comcode with ur";
		}else if(mManageCom.length()==4){
			sql = "select comcode,name from ldcom where comcode like '"+mManageCom+"%' and sign='1'  and length(trim(comcode))>4 with ur";
		}else{
			sql = "select comcode,name from ldcom where comcode='"+mManageCom+"' and sign='1' with ur";
		}

		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql);
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);

		String comCode = "";
		long sumCaseNum[] = new long[16];
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String ListInfo[] = new String[18];
			comCode = tSSRS.GetText(i, 1);
			ListInfo[15] = comCode;
			ListInfo[0] = tSSRS.GetText(i, 2);
			System.out.println("1111111111111" + ListInfo[1]);
			ListInfo[1] = getRgtCaseNum(comCode);
			ListInfo[2] = getScanCaseNum(comCode);
			ListInfo[3] = getSerchCaseNum(comCode);
			ListInfo[4] = getCalculatCaseNum(comCode);
			ListInfo[5] = getCheckNum(comCode);
			ListInfo[6] = getCheckedNum(comCode);
			ListInfo[7] = getServeyNum(comCode);
			ListInfo[8] = getServeyedNum(comCode);
			ListInfo[10] = getSelectNum(comCode);
			ListInfo[9] = getEndCaseNum(comCode);
			// System.out.println( "MMMMMMMMMMMMMM"+getPayIntv(tPolNo));
			ListInfo[11] = getNotifyCaseNum(comCode);
			ListInfo[12] = getPayedCaseNum(comCode);
			ListInfo[13] = getCancleCaseNum(comCode);
			ListInfo[14] = getDelayCaseNum(comCode);
            ListInfo[17] = getLLUWNum(comCode);//理赔二核
			ListInfo[16] = Long.toString(Long.parseLong(ListInfo[1])+Long.parseLong(ListInfo[2])+Long.parseLong(ListInfo[3])
			               +Long.parseLong(ListInfo[4])+Long.parseLong(ListInfo[5])+Long.parseLong(ListInfo[6])
			               +Long.parseLong(ListInfo[7])+Long.parseLong(ListInfo[8])+Long.parseLong(ListInfo[10])
			               +Long.parseLong(ListInfo[14])+Long.parseLong(ListInfo[17]));
            
			sumCaseNum[0] = sumCaseNum[0]+Long.parseLong(ListInfo[1]);
			sumCaseNum[1] = sumCaseNum[1]+Long.parseLong(ListInfo[2]);
			sumCaseNum[2] = sumCaseNum[2]+Long.parseLong(ListInfo[3]);
			sumCaseNum[3] = sumCaseNum[3]+Long.parseLong(ListInfo[4]);
			sumCaseNum[4] = sumCaseNum[4]+Long.parseLong(ListInfo[5]);
			sumCaseNum[5] = sumCaseNum[5]+Long.parseLong(ListInfo[6]);
			sumCaseNum[6] = sumCaseNum[6]+Long.parseLong(ListInfo[7]);
			sumCaseNum[7] = sumCaseNum[7]+Long.parseLong(ListInfo[8]);
			sumCaseNum[8] = sumCaseNum[8]+Long.parseLong(ListInfo[9]);
			sumCaseNum[9] = sumCaseNum[9]+Long.parseLong(ListInfo[10]);
			sumCaseNum[10] = sumCaseNum[10]+Long.parseLong(ListInfo[11]);
			sumCaseNum[11] = sumCaseNum[11]+Long.parseLong(ListInfo[12]);
			sumCaseNum[12] = sumCaseNum[12]+Long.parseLong(ListInfo[13]);
			sumCaseNum[13] = sumCaseNum[13]+Long.parseLong(ListInfo[14]);
			sumCaseNum[14] = sumCaseNum[14]+Long.parseLong(ListInfo[16]);
            sumCaseNum[15] = sumCaseNum[15]+Long.parseLong(ListInfo[17]);
			mListTable.add(ListInfo);
		}
		TextTag tTextTag = new TextTag();
		for(int i=0;i<sumCaseNum.length;i++){
			tTextTag.add("RgtState"+(i+1),sumCaseNum[i] );
			mXmlExport.addTextTag(tTextTag);
		}

		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql);
		return true;
	}

	private String getRgtCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'  and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='01' and  exists  (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'  and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"'  and rgtstate='01' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5')  with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='01' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1')  with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"'  and  rgtstate='01' and exists  (select 1 from llregister where rgtno=o.rgtno and applyertype='2') with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
				+ comCode
				+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and  rgtstate='01' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}
	}

	private String getScanCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='02' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='02' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='5')  with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='02' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='1') with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='02' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='2') with ur";
		}else{
			sql = "select count(distinct caseno) from llcase  o where mngcom like'"
				+ comCode
				+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='02'   with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}
	}

	private String getSerchCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='03' and  exists ( select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='03' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='5')  with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='03' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase  o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='03' and exists (select 1 from llregister  where rgtno=o.rgtno and applyertype='2') with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
				+ comCode
				+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='03'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getCalculatCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase  o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='04' and exists ( select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase  o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='04' and  exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='04' and  exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"'  and rgtstate='04' and  exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}{
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
				+ comCode
				+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='04'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getCheckNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='05' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='05' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='05' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='05' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='05'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getCheckedNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='06' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null) ) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='06' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='06' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='06' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='06'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getServeyNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='07' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='07' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='07' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='07' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='07' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}
	
    //TODO:计算理赔二核案件数
    private String getLLUWNum(String comCode) {
        String sql = "";
        if ("1".equals(mContType)) {
            sql = "select count(distinct caseno) from llcase o where mngcom like'"
                    + comCode
                    + "%'   and rgtdate between '"
                    +mStartDate
                    +"' and '"
                    +mEndDate
                    +"' and rgtstate='16' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

        } else if ("2".equals(mContType)) {
            sql = "select count(distinct caseno) from llcase o where mngcom like'"
                    + comCode
                    + "%'   and rgtdate between '"
                    +mStartDate
                    +"' and '"
                    +mEndDate
                    +"' and rgtstate='16' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
        } else if ("3".equals(mContType)) {
            sql = "select count(distinct caseno) from llcase o where mngcom like'"
                    + comCode
                    + "%'   and rgtdate between '"
                    +mStartDate
                    +"' and '"
                    +mEndDate
                    +"' and rgtstate='16' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
        } else if("4".equals(mContType)){
            sql = "select count(distinct caseno) from llcase o where mngcom like'"
                    + comCode
                    + "%'   and rgtdate between '"
                    +mStartDate
                    +"' and '"
                    +mEndDate
                    +"' and rgtstate='16' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
        }else{
            sql = "select count(distinct caseno) from llcase o where mngcom like'"
                + comCode
                + "%'   and rgtdate between '"
                    +mStartDate
                    +"' and '"
                    +mEndDate
                    +"' and rgtstate='16' with ur";
    
        }
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        String result = tExeSQL.getOneValue(sql);
        if (result != null && !"".equals(result) && !"null".equals(result)) {
            return result;
        } else {
            return "0";
        }

    }
    
	private String getServeyedNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like '"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='08' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='08' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='08' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'  and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='08' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2') with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='08'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getSelectNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='10' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='10' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='10' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='10' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='10' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getEndCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='09' and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='09' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5' ) with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='09' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1' ) with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='09' and  exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2' ) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='09'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getCancleCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='14'  and  exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null)) with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode + "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='14'  and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5') with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode + "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='14'   and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1')  with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='14' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='2') with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='14' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getNotifyCaseNum(String comCode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='11' and exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null))  with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='11' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='5') with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='11' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1')  with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comCode
					+ "%'    and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='11' and exists (select 1 from llregister  where applyertype='2' and rgtno=o.rgtno) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comCode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='11'  with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	private String getPayedCaseNum(String comcode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='12'  and exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null))  with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='12' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='5') with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='12' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1') with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='12' and exists (select 1 from llregister  where  applyertype='2' and rgtno=o.rgtno) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comcode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='12' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}
	
	private String getDelayCaseNum(String comcode) {
		String sql = "";
		if ("1".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='13'  and exists (select 1 from llregister where rgtno=o.rgtno and (applyertype='0' or applyertype is null))  with ur";

		} else if ("2".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='13' and exists ( select 1 from llregister where rgtno=o.rgtno and applyertype='5') with ur";
		} else if ("3".equals(mContType)) {
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='13' and exists (select 1 from llregister where rgtno=o.rgtno and applyertype='1') with ur";
		} else if("4".equals(mContType)){
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
					+ comcode
					+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='13' and exists (select 1 from llregister  where  applyertype='2' and rgtno=o.rgtno) with ur";
		}else{
			sql = "select count(distinct caseno) from llcase o where mngcom like'"
				+ comcode
				+ "%'   and rgtdate between '"
					+mStartDate
					+"' and '"
					+mEndDate
					+"' and rgtstate='13' with ur";
	
		}
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			return result;
		} else {
			return "0";
		}

	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {

	}

}
