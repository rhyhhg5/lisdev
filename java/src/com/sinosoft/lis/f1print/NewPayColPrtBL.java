package com.sinosoft.lis.f1print;

/**
 * <p>Title: PayColPrtBL</p>
 * <p>Description: 团体赔付汇总表打印</p>
 * <p>Copyright: Copyright sinosoft (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class NewPayColPrtBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private String mOperate;
    // 打印次数或打印方式（即时打印or批量打印）
    private String mdirect = "";
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    // 业务处理相关变量
    private GlobalInput mGlobalInput = new GlobalInput();

    // private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mRgtNo = "";

    private String mflag = null;

    public NewPayColPrtBL() {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
//        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT")) {
//            buildError("submitData", "不支持的操作字符串");
//            return false;
//        }
        mOperate = cOperate;
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (mRgtNo.substring(1, 3).equals("12")) {
            if (!getTJPrintData()) {
                return false;
            }
        } else if (mRgtNo.substring(1, 3).equals("11")) {
            if (!getBJPrintData()) {
                return false;
            }
        } else {
            if (!getPrintData()) {
                return false;
            }
        }
        if (cOperate.equals("INSERT")) {
            if (!dealPrintMag("INSERT")) {
                return false;
            }
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        // 全局变量
        LOBatchPRTManagerSchema prtSchema = (LOBatchPRTManagerSchema)
                                            cInputData
                                            .getObjectByObjectName(
                "LOBatchPRTManagerSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema tLOPRTManagerSchema = (LOPRTManagerSchema)
                                                 cInputData.
                                                 getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if (tLOPRTManagerSchema.getOtherNo() != null) {
            mRgtNo = tLOPRTManagerSchema.getOtherNo();
            mdirect += tLOPRTManagerSchema.getStandbyFlag3();
        } else if (prtSchema != null && prtSchema.getOtherNo() != null) {
            mRgtNo = prtSchema.getOtherNo();
        } else {
            buildError("getInputData", "没有指定打印数据！");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean getTJPrintData() {
        //  if (mOperate.equals("INSERT")) {
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mRgtNo);
        mLOPRTManagerSchema.setOtherNoType("07");
        mLOPRTManagerSchema.setCode("lp011");
        mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setAgentCode("");
        mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(mRgtNo);
        mLOPRTManagerSchema.setStandbyFlag3("1");

        //return true;
        //}

        TextTag texttag = new TextTag(); // 新建一个TextTag的实例
        // 根据印刷号查询打印队列中的纪录
        String aGrpName = "";

        int count = 0;
        double realPaySum = 0;

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "查询团体批次信息失败！");
            return false;
        }
        aGrpName = tLLRegisterDB.getGrpName();

        // ListTable
        ListTable appColListTable = new ListTable();
        appColListTable.setName("TJPCOL");
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        String[] Title = {"NO", "COSTOMERNAME", "IDNo", "CASENO", "FeeType",
                         "TabFee", "StandPay", "RefuseAmnt", "RealPay",
                         "Remark",
                         "RefuseDetail"};

        LLCaseDB mLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        String tSql =
                "select * from llcase where rgtstate in ('09','11','12') "
                + " and rgtno='" + mRgtNo + "'";
        tLLCaseSet.set(mLLCaseDB.executeQuery(tSql));

        if (tLLCaseSet == null || tLLCaseSet.size() == 0) {
            CError.buildErr(this, "该团体批次下无已结案件，无法打印汇总明细！");
            return false;
        }
        count = tLLCaseSet.size();
        String[] strCol;
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        for (int i = 1; i <= count; i++) {
            tLLCaseSchema = tLLCaseSet.get(i);
            String aCaseNo = tLLCaseSchema.getCaseNo();
            String sql = "select coalesce(sum(tabfeemoney),0),coalesce(sum(claimmoney),0),coalesce(sum(refuseamnt),0),"
                         + "coalesce(sum(realpay),0),coalesce(sum(overamnt),0),caserelano from llclaimdetail "
                         + " where caseno='" + aCaseNo +
                         "' group by caserelano";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS fSSRS = new SSRS();
            fSSRS = tExeSQL.execSQL(sql);
            if (fSSRS.getMaxRow() <= 0) {
                strCol = new String[11];
                strCol[0] = Integer.toString(i);
                strCol[1] = tLLCaseSchema.getCustomerName();
                strCol[2] = tLLCaseSchema.getIDNo();
                strCol[3] = aCaseNo;
                strCol[4] = "";
                strCol[5] = "";
                strCol[6] = "";
                strCol[7] = "";
                strCol[8] = "";
                strCol[9] = "";
                strCol[10] = "";
                appColListTable.add(strCol);
                continue;
            }
            for (int j = 1; j <= fSSRS.getMaxRow(); j++) {
                realPaySum = realPaySum
                             + Double.parseDouble(fSSRS.GetText(j, 4));
                strCol = new String[11];
                strCol[0] = Integer.toString(i);
                strCol[1] = tLLCaseSchema.getCustomerName();
                ;
                strCol[2] = tLLCaseSchema.getIDNo();
                strCol[3] = aCaseNo;
                strCol[4] = "门诊";
                /*
                 * String feesql = "select distinct feetype from llfeemain where " + "
                 * caseno='" + aCaseNo + "' and caserelano='" + fSSRS.GetText(j,
                 * 6) + "'";
                 */
                String feesql = "select distinct(d.codename) from llfeemain f,ldcode d where f.feetype=d.code and d.codetype='llfeetype' and f.caseno='"
                                + aCaseNo
                                + "' and f.caserelano='"
                                + fSSRS.GetText(j, 6)
                                + "'";
                SSRS tfr = tExeSQL.execSQL(feesql);
                /*if (tfr.getMaxRow() > 0) {
                 if (tfr.GetText(1, 1).equals("2")) {
                  strCol[4] = "住院";
                 }
                     }*/
                if (tfr.getMaxRow() > 0) {
                    strCol[4] = tfr.GetText(1, 1);
                }
                strCol[5] = fSSRS.GetText(j, 1);
                strCol[6] = fSSRS.GetText(j, 2);
                strCol[7] = fSSRS.GetText(j, 3);
                feesql = "select coalesce(sum(a.fee),0),coalesce(sum(a.preamnt+a.refuseamnt+a.selfamnt),0) from "
                         +
                         "llcasereceipt a, llfeemain b where a.mainfeeno=b.mainfeeno"
                         + " and b.caseno='"
                         + aCaseNo
                         + "' and b.caserelano='"
                         + fSSRS.GetText(j, 6) + "'";
                tfr = tExeSQL.execSQL(feesql);
                if (tfr.getMaxRow() > 0) {
                    if (!tfr.GetText(1, 1).equals("0")
                        && !tfr.GetText(1, 1).equals("null")) {
                        strCol[5] = tfr.GetText(1, 1);
                    }
                    if (!tfr.GetText(1, 2).equals("0")
                        && !tfr.GetText(1, 2).equals("null")) {
                        strCol[7] = tfr.GetText(1, 2);
                    }
                }
                strCol[8] = fSSRS.GetText(j, 4);
                double toveramnt = 0;
                try {
                    toveramnt = Double.parseDouble(fSSRS.GetText(j, 5));
                } catch (Exception ex) {
                }
                strCol[9] = "";
                if (toveramnt > 0) {
                    strCol[9] = "足额赔付，溢额" + toveramnt + "元";
                }
                strCol[10] = getRefuseRemark(aCaseNo, fSSRS.GetText(j, 6));
                appColListTable.add(strCol);

            }
        }
        // 其它模版上单独不成块的信息

        String tAgentSQL =
                "SELECT getUniteCode(a.agentcode),b.name FROM lcgrpcont a,laagent b "
                + " WHERE a.agentcode=b.agentcode AND a.grpcontno='" +
                tLLRegisterDB.getRgtObjNo() + "' WITH UR";
        ExeSQL tAgentExeSQL = new ExeSQL();
        SSRS tAgentSSRS = tAgentExeSQL.execSQL(tAgentSQL);

        XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
        // xmlexport.createDocuments("PayColPrt.vts", mGlobalInput);
        // //最好紧接着就初始化xml文档
        if (mdirect.equals("1")) {
            xmlexport.createDocument("TJPayColPrt.vts", "print");
        } else {
            xmlexport.createDocuments("TJPayColPrt.vts", mGlobalInput);
        }
        // 生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                         + tSrtTool.getDay() + "日";

        // PayColPrtBL模版元素
        // if (countno > 0) {
        // appFeeSum = String.valueOf(SumappFee);
        // }
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag.add("JetFormType", "lp011");
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch")) {
            texttag.add("previewflag", "0");
        } else {
            texttag.add("previewflag", "1");
        }
        texttag.add("BarCode1", mRgtNo);
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("GrpName", aGrpName);
        texttag.add("GrpContNo", tLLRegisterDB.getRgtObjNo());
        texttag.add("RgtNo", mRgtNo);
        // texttag.add("count", count);
        texttag.add("RiskName", "守护专家社保补充团体医疗保险");
        realPaySum = Double.parseDouble(new DecimalFormat("0.00")
                                        .format(realPaySum));
        texttag.add("realPaySum", realPaySum);
        texttag.add("SysDate", SysDate);
        texttag.add("XI_ManageCom", tLLRegisterDB.getMngCom());
        texttag.add("XI_GrpAppntNo", tLLRegisterDB.getCustomerNo());
        texttag.add("AgentCode", tAgentSSRS.GetText(1, 1));
        texttag.add("AgentName", tAgentSSRS.GetText(1, 2));

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        // 保存信息
        xmlexport.addListTable(appColListTable, Title); // 保存申请资料信息
        xmlexport.addListTable(tEndListTable);
        xmlexport.outputDocumentToFile("e:\\", "testTJHZM"); // 输出xml文档到文件
        mResult.clear();
        mResult.addElement(mLOPRTManagerSchema);
        mResult.addElement(xmlexport);
        System.out.println(realPaySum);
        System.out.println("getTJPrintData---end");
        return true;
    }

    private boolean getPrintData() {
        // if (mOperate.equals("INSERT")) {
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mRgtNo);
        mLOPRTManagerSchema.setOtherNoType("07");
        mLOPRTManagerSchema.setCode("lp003");
        mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setAgentCode("");
        mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(mRgtNo);
        mLOPRTManagerSchema.setStandbyFlag3("1");

        //return true;
        // }

        TextTag texttag = new TextTag(); // 新建一个TextTag的实例
        // 根据印刷号查询打印队列中的纪录
        String aGrpName = "";
        String aCaseNo = "";
        String aCustomerName = "";
        String aCustomerNo = "";
        String appDate = "";
        String idNo = "";

        double realPay = 0;
        double SumappFee = 0;
        int count = 0;
        int countno = 0;
        String appFeeSum = "　　";
        double realPaySum = 0;

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            aGrpName = tLLRegisterDB.getCustomerNo();
        }
        aGrpName = tLLRegisterDB.getGrpName();

        LLCaseDB mLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        String tSql =
                "select * from llcase where rgtstate in ('09','11','12') and rgtno='"
                + mRgtNo + "' order by caseno";
        tLLCaseSet.set(mLLCaseDB.executeQuery(tSql));
        if (tLLCaseSet == null || tLLCaseSet.size() == 0) {
            CError.buildErr(this, "该批次下没有已结案件，无法打印汇总信息！");
            return false;
        }

        LLClaimDB tLLClaimDB;
        LLClaimSet tLLClaimSet;

        // ListTable
        ListTable appColListTable = new ListTable();
        appColListTable.setName("APPCOL");
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        String[] Title = {"NO", "COSTOMERNO", "APPDATE",
                         "APPFEE", "REALPAY", "CASENO","CUSTOMERID","COSTOMERNAME"};

        count = tLLCaseSet.size();

        String[] strCol;
        System.out.println(tLLCaseSet.size());
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        for (int i = 1; i <= tLLCaseSet.size(); i++) {
            String SappFee = "　　";
            tLLCaseSchema.setSchema(tLLCaseSet.get(i));
            aCaseNo = tLLCaseSchema.getCaseNo();
            aCustomerNo = tLLCaseSchema.getCustomerNo();
            aCustomerName = tLLCaseSchema.getCustomerName();
            appDate = tLLCaseSchema.getRgtDate();
            idNo = tLLCaseSchema.getIDNo();

            String sql12 = "select sum(sumfee) from llfeemain where caseno='"
                           + aCaseNo +
                           "' and  mainfeeno in ( select mainfeeno from "
                           +
                           " llfeemain where caserelano in (select caserelano from "
                           + " llclaimdetail where caseno ='" + aCaseNo +
                           "') )";

            ExeSQL tExeSQL = new ExeSQL();
            SSRS fSSRS = new SSRS();
            fSSRS = tExeSQL.execSQL(sql12);
            if (!(fSSRS.GetText(1, 1) == null || fSSRS.GetText(1, 1).equals("") ||
                  fSSRS
                  .GetText(1, 1).equals("null"))) {
                double feetemp = Double.parseDouble(fSSRS.GetText(1, 1));
                feetemp = Arith.round(feetemp, 2);
                SappFee = new DecimalFormat("0.00").format(feetemp);
                SumappFee += feetemp;
                countno = countno + 1;
            }

            System.out.println("个人申报金额：" + SappFee);

            // 计算实赔费用
            tLLClaimDB = new LLClaimDB();
            tLLClaimSet = new LLClaimSet();
            tLLClaimDB.setCaseNo(aCaseNo);
            tLLClaimDB.setRgtNo(mRgtNo);
            tLLClaimSet.set(tLLClaimDB.query());
            if (tLLClaimSet == null || tLLClaimSet.size() == 0) {
                break;
            } else {
                realPay = 0;
                realPay += tLLClaimSet.get(1).getRealPay();
            }
            realPaySum += realPay;
            strCol = new String[8];
            strCol[0] = Integer.toString(i);
            strCol[1] = aCustomerNo;
            strCol[2] = appDate;
            strCol[3] = "" + SappFee;
            strCol[4] = ""
                        + new DecimalFormat("0.00").format(realPay);
            strCol[5] = aCaseNo;
            strCol[6] = idNo;
            strCol[7] = aCustomerName;
            appColListTable.add(strCol);
        }

        // 其它模版上单独不成块的信息

        String tAgentSQL =
                "SELECT getUniteCode(a.agentcode),b.name FROM lcgrpcont a,laagent b "
                + " WHERE a.agentcode=b.agentcode AND a.grpcontno='" +
                tLLRegisterDB.getRgtObjNo() + "' WITH UR";
        ExeSQL tAgentExeSQL = new ExeSQL();
        SSRS tAgentSSRS = tAgentExeSQL.execSQL(tAgentSQL);

        XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
        if (mdirect.equals("1")) {
            xmlexport.createDocument("PayColPrt.vts", "print");
        } else {
            xmlexport.createDocuments("PayColPrt.vts", mGlobalInput);
        }
        // 生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                         + tSrtTool.getDay() + "日";

        // PayColPrtBL模版元素
        if (countno > 0) {
            SumappFee = Arith.round(SumappFee, 2);
            appFeeSum = new DecimalFormat("0.00").format(SumappFee);
        }
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag.add("JetFormType", "lp003");
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch")) {
            texttag.add("previewflag", "0");
        } else {
            texttag.add("previewflag", "1");
        }
        texttag.add("BarCode1", mRgtNo);
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("GrpName", aGrpName);
        texttag.add("RgtNo", mRgtNo);
        texttag.add("count", count);
        texttag.add("appFeeSum", appFeeSum);
        String tRealPaySum = new DecimalFormat("0.00").format(realPaySum);
        texttag.add("realPaySum", tRealPaySum);
        texttag.add("SysDate", SysDate);
        texttag.add("GrpContNo", tLLRegisterDB.getRgtObjNo());
        texttag.add("XI_ManageCom", tLLRegisterDB.getMngCom());
        texttag.add("XI_GrpAppntNo", tLLRegisterDB.getCustomerNo());
        texttag.add("AgentCode", tAgentSSRS.GetText(1, 1));
        texttag.add("AgentName", tAgentSSRS.GetText(1, 2));

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(appColListTable, Title); // 保存申请资料信息
        xmlexport.addListTable(tEndListTable);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); // 输出xml文档到文件
        mResult.clear();
        mResult.addElement(mLOPRTManagerSchema);
        mResult.addElement(xmlexport);

        return true;
    }

    private boolean getBJPrintData() {
        //if (mOperate.equals("INSERT")) {
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mRgtNo);
        mLOPRTManagerSchema.setOtherNoType("07");
        mLOPRTManagerSchema.setCode("lp003bj");
        mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setAgentCode("");
        mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(mRgtNo);
        mLOPRTManagerSchema.setStandbyFlag3("1");
        // return true;
        // }

        TextTag texttag = new TextTag(); // 新建一个TextTag的实例
        // 根据印刷号查询打印队列中的纪录
        String aGrpName = "";

        int count = 0;
        double TabFee = 0;
        double SupDoorFee = 0;
        double PlanFee = 0;
        double SupInHosFee = 0;
        double RetireAddFee = 0;
        double SelfPay1 = 0;
        double GetLimit = 0;
        double SelfPay2 = 0;
        double refuseamnt = 0;
        double SelfAmnt = 0;

        double Realpay = 0;

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "查询团体批次信息失败！");
            return false;
        }
        aGrpName = tLLRegisterDB.getGrpName();
        // ListTable
        ListTable appColListTable = new ListTable();
        appColListTable.setName("BJPCOL");
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        String[] Title = {"No", "InsuedStat", "COSTOMERNAME", "IDNo",
                         "CaseNo", "FeeType", "TabFee", "SupDoorFee", "PlanFee",
                         "SupInHosFee", "RetireAddFee", "SelfPay1", "GetLimit",
                         "SelfPay2", "SelfAmnt", "refuseamnt", "PayRate",
                         "Realpay"};

        LLCaseDB mLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        String tSql =
                "select * from llcase where rgtstate in ('09','11','12') "
                + " and rgtno='" + mRgtNo + "' order by caseno";
        tLLCaseSet.set(mLLCaseDB.executeQuery(tSql));

        if (tLLCaseSet == null || tLLCaseSet.size() == 0) {
            CError.buildErr(this, "该团体批次下无已结案件，无法打印汇总明细！");
            return false;
        }
        count = tLLCaseSet.size();
        String[] strCol;
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        for (int i = 1; i <= count; i++) {
            tLLCaseSchema = tLLCaseSet.get(i);
            String aCaseNo = tLLCaseSchema.getCaseNo();
            String sql = "select coalesce(sum(tabfeemoney),0),coalesce(sum(claimmoney),0),coalesce(sum(outdutyamnt),0),"
                         + "coalesce(sum(realpay),0) from llclaimdetail "
                         + " where caseno='" + aCaseNo + "' ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS fSSRS = new SSRS();
            fSSRS = tExeSQL.execSQL(sql);
            String IdNo = "";
            if (tLLCaseSchema.getIDNo() != null) {
                IdNo = tLLCaseSchema.getIDNo();
            }
            String tSql1 = "SELECT a.codename FROM ldcode a,llfeemain b WHERE a.codetype='insustat' AND a.code=b.insuredstat AND b.caseno='"
                           + tLLCaseSchema.getCaseNo() + "'";
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(tSql1);
            String tInsuredStat = "";
            if (tSSRS.getMaxRow() > 0) {
                tInsuredStat = tSSRS.GetText(1, 1);
            }
            strCol = new String[18];
            strCol[0] = Integer.toString(i);
            strCol[1] = tInsuredStat;
            strCol[2] = tLLCaseSchema.getCustomerName();
            strCol[3] = IdNo;
            strCol[4] = aCaseNo;

            if (fSSRS.getMaxRow() <= 0) {
                strCol[5] = "";
                strCol[6] = "";
                strCol[7] = "";
                strCol[8] = "";
                strCol[9] = "";
                strCol[10] = "";
                strCol[11] = "";
                strCol[12] = "";
                strCol[13] = "";
                strCol[14] = "";
                strCol[15] = "";
                strCol[16] = "";
                strCol[17] = "";
                appColListTable.add(strCol);
                continue;
            } else {
                strCol[5] = "门诊";
                String feesql = "select distinct feetype from llfeemain where "
                                + " caseno='" + aCaseNo
                                + "' and caserelano is not null";
                SSRS tfr = tExeSQL.execSQL(feesql);
                if (tfr.getMaxRow() > 0) {
                    if (tfr.GetText(1, 1).equals("2")) {
                        strCol[5] = "住院";
                    }
                }

                String securitySql = "select coalesce(sum(applyamnt),0),coalesce(sum(SupDoorFee),0),coalesce(sum(planfee),0),coalesce(sum(supinhosfee),0),coalesce(sum(RetireAddFee),0),"
                                     + "coalesce(sum(SelfPay1),0),coalesce(sum(Getlimit),0),coalesce(sum(SelfPay2),0),coalesce(sum(SelfAmnt),0) from llsecurityreceipt where "
                                     + " caseno='"
                                     + aCaseNo
                                     + "' and mainfeeno in ( select mainfeeno from llfeemain where caserelano is not null)";
                SSRS scf = tExeSQL.execSQL(securitySql);
                if (scf.getMaxRow() > 0) {
                    strCol[6] = scf.GetText(1, 1);
                    TabFee += Double.parseDouble(scf.GetText(1, 1));
                    if (strCol[6].equals("0") || strCol[6].equals("") ||
                        strCol[6].equals("null")) {
                        String tFeeMainSQL =
                                "SELECT coalesce(sum(sumfee),0) FROM llfeemain WHERE caseno='" +
                                aCaseNo + "' WITH UR";
                        ExeSQL tExeFeeSQL = new ExeSQL();
                        String tSumFee = "";
                        tSumFee = tExeFeeSQL.getOneValue(tFeeMainSQL);
                        strCol[6] = tSumFee;
                        TabFee += Double.parseDouble(tSumFee);
                    }
                    strCol[7] = scf.GetText(1, 2);
                    SupDoorFee += Double.parseDouble(scf.GetText(1, 2));
                    strCol[8] = scf.GetText(1, 3);
                    PlanFee += Double.parseDouble(scf.GetText(1, 3));
                    strCol[9] = scf.GetText(1, 4);
                    SupInHosFee += Double.parseDouble(scf.GetText(1, 4));
                    strCol[10] = scf.GetText(1, 5);
                    RetireAddFee += Double.parseDouble(scf.GetText(1, 5));
                    strCol[11] = scf.GetText(1, 6);
                    SelfPay1 += Double.parseDouble(scf.GetText(1, 6));
                    if (strCol[11].equals("0") || strCol[11].equals("") ||
                        strCol[11].equals("null")) {
                        String tSelfPay1SQL = "SELECT coalesce(sum(smalldoorpay+highdooramnt),0) FROM llsecurityreceipt WHERE caseno='" +
                                              aCaseNo + "' WITH UR";
                        ExeSQL tExeFeeSQL = new ExeSQL();
                        String tSelfPay1 = "";
                        tSelfPay1 = tExeFeeSQL.getOneValue(tSelfPay1SQL);
                        strCol[11] = tSelfPay1;
                        SelfPay1 += Double.parseDouble(tSelfPay1);
                    }
                    strCol[12] = scf.GetText(1, 7);
                    GetLimit += Double.parseDouble(scf.GetText(1, 7));
                    strCol[13] = scf.GetText(1, 8);
                    SelfPay2 += Double.parseDouble(scf.GetText(1, 8));
                    strCol[14] = scf.GetText(1, 9);
                    SelfAmnt += Double.parseDouble(scf.GetText(1, 9));
                }
                strCol[15] = fSSRS.GetText(1, 3);
                refuseamnt += Double.parseDouble(fSSRS.GetText(1, 3));
                String Ratesql =
                        "select distinct outdutyrate from llclaimdetail  where "
                        + " caseno='" + aCaseNo + "'";
                SSRS RaT = tExeSQL.execSQL(Ratesql);
                if (RaT.getMaxRow() > 0) {
                    strCol[16] = RaT.GetText(1, 1);
                }
                strCol[17] = fSSRS.GetText(1, 4);
                Realpay += Double.parseDouble(fSSRS.GetText(1, 4));
                appColListTable.add(strCol);
            }
        }
        // 其它模版上单独不成块的信息

        String tAgentSQL =
                "SELECT getUniteCode(a.agentcode),b.name FROM lcgrpcont a,laagent b "
                + " WHERE a.agentcode=b.agentcode AND a.grpcontno='" +
                tLLRegisterDB.getRgtObjNo() + "' WITH UR";
        ExeSQL tAgentExeSQL = new ExeSQL();
        SSRS tAgentSSRS = tAgentExeSQL.execSQL(tAgentSQL);

        XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
        // xmlexport.createDocuments("PayColPrt.vts", mGlobalInput);
        // //最好紧接着就初始化xml文档
        if (mdirect.equals("1")) {
            xmlexport.createDocument("BJPayColPrt.vts", "print");
        } else {
            xmlexport.createDocuments("BJPayColPrt.vts", mGlobalInput);
        }
        // 生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                         + tSrtTool.getDay() + "日";
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag.add("JetFormType", "lp003bj");
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch")) {
            texttag.add("previewflag", "0");
        } else {
            texttag.add("previewflag", "1");
        }
        texttag.add("BarCode1", mRgtNo);
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("GrpName", aGrpName);
        texttag.add("GrpContNo", tLLRegisterDB.getRgtObjNo());
        texttag.add("RgtNo", mRgtNo);
        texttag.add("count", count);
        texttag.add("RiskName", "守护专家社保补充团体医疗保险");
        texttag.add("TabFee", Double.parseDouble(new DecimalFormat("0.00")
                                                 .format(TabFee)));
        texttag.add("PlanFee", Double.parseDouble(new DecimalFormat("0.00")
                                                  .format(PlanFee)));
        texttag.add("SupInHosFee", Double.parseDouble(new DecimalFormat("0.00")
                .format(SupInHosFee)));
        texttag.add("SelfPay1", Double.parseDouble(new DecimalFormat("0.00")
                .format(SelfPay1)));
        texttag.add("GetLimit", Double.parseDouble(new DecimalFormat("0.00")
                .format(GetLimit)));
        texttag.add("SupDoorFee", Double.parseDouble(new DecimalFormat("0.00")
                .format(SupDoorFee)));
        texttag.add("SelfPay2", Double.parseDouble(new DecimalFormat("0.00")
                .format(SelfPay2)));
        texttag.add("SelfAmnt", Double.parseDouble(new DecimalFormat("0.00")
                .format(SelfAmnt)));
        texttag.add("refuseamnt", Double.parseDouble(new DecimalFormat("0.00")
                .format(refuseamnt)));
        texttag.add("RetireAddFee", Double
                    .parseDouble(new DecimalFormat("0.00").format(RetireAddFee)));
        texttag.add("Realpay", Double.parseDouble(new DecimalFormat("0.00")
                                                  .format(Realpay)));
        texttag.add("SysDate", SysDate);
        texttag.add("XI_ManageCom", tLLRegisterDB.getMngCom());
        texttag.add("XI_GrpAppntNo", tLLRegisterDB.getCustomerNo());
        texttag.add("AgentCode", tAgentSSRS.GetText(1, 1));
        texttag.add("AgentName", tAgentSSRS.GetText(1, 2));

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        // 保存信息
        xmlexport.addListTable(appColListTable, Title); // 保存申请资料信息
        xmlexport.addListTable(tEndListTable);
        xmlexport.outputDocumentToFile("e:\\", "testBJHZM"); // 输出xml文档到文件
        mResult.clear();
        mResult.addElement(mLOPRTManagerSchema);
        mResult.addElement(xmlexport);
        return true;
    }

    private String getRefuseRemark(String aCaseNo, String aCaseRelaNo) {
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        tLLFeeMainDB.setCaseNo(aCaseNo);
        tLLFeeMainDB.setCaseRelaNo(aCaseRelaNo);
        tLLFeeMainSet = tLLFeeMainDB.query();
        String drugdesc = "";
        LLCaseDrugDB tLLCaseDrugDB = new LLCaseDrugDB();
        LLCaseDrugSet tLLCaseDrugSet = new LLCaseDrugSet();
        for (int k = 1; k <= tLLFeeMainSet.size(); k++) {
            tLLCaseDrugDB.setCaseNo(aCaseNo);
            tLLCaseDrugDB.setMainFeeNo(tLLFeeMainSet.get(k).getMainFeeNo());
            tLLCaseDrugSet = tLLCaseDrugDB.query();
            if (tLLCaseDrugSet.size() > 0) {
                drugdesc += tLLFeeMainSet.get(k).getFeeDate() + "的"
                        + tLLFeeMainSet.get(k).getReceiptNo() + "号账单：\n ";
                for (int kk = 1; kk <= tLLCaseDrugSet.size(); kk++) {
                    LLCaseDrugSchema tLLCaseDrugSchema = new LLCaseDrugSchema();
                    tLLCaseDrugSchema = tLLCaseDrugSet.get(kk);
                    drugdesc += "    " + tLLCaseDrugSchema.getDrugName();
                    if (tLLCaseDrugSchema.getSelfPay2() >= 0.01) {
                        drugdesc += "  扣除 " + tLLCaseDrugSchema.getSelfPay2()
                                + "元 属于 部分自付 ";
                    }
                    if (tLLCaseDrugSchema.getSelfFee() >= 0.01) {
                        drugdesc += "  扣除 " + tLLCaseDrugSchema.getSelfFee()
                                + "元 属于 自费 ";
                    }
                    if (tLLCaseDrugSchema.getUnReasonableFee() >= 0.01) {
                        drugdesc += "  扣除 "
                                + tLLCaseDrugSchema.getUnReasonableFee()
                                + "元 属于 不合理费用,原因: "
                                + tLLCaseDrugSchema.getRemark();
                    }
                    drugdesc += "\n";
                }
            }
        }
        return drugdesc;
    }

    private boolean dealPrintMag(String cOperate) {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, cOperate);
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }


    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        PayColPrtBL tPayColPrtBL = new PayColPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        tVData.addElement(tGlobalInput);
        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("P1100070201000013");
        tLOBatchPRTManagerSchema.setStandbyFlag2("1");
        tVData.addElement(tLOBatchPRTManagerSchema);
        tPayColPrtBL.submitData(tVData, "PRINT");
    }

}
