package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class LCSuccorNewUI {
    public static String EEEEE = "1";
    private LCContSchema mLCContSchema;
    private TransferData mTransferData;
    private String mPrtFlag = "";
    public CErrors mErrors = new CErrors();
    /**传输到后台处理的map*/
    private MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    public LCSuccorNewUI() {
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args) {
//        Propertie p = new Propertie();
        System.setProperty("OS", "XP");
        System.out.println("System " + System.getProperty("application"));
        GlobalInput tG = new GlobalInput();
        tG.Operator = "pc";
        tG.ComCode = "86";
        tG.ManageCom = "86";
        tG.ClientIP = "10.252.130.144";
        LCSuccorNewUI tZhuF1PUI = new LCSuccorNewUI();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("014056212000001"); //合同单号
        tLCContSchema.setCardFlag("6"); //合同单号
        //        tLCContSchema.setGrpContNo("240110000000024"); //合同单号
        VData vData = new VData();
        vData.addElement(tG);
        vData.addElement(tLCContSchema);
        String szTemplatePath = "E:/lisdev/ui/f1print/template/";
        String sOutXmlPath = "E:/lisdev/ui/";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TemplatePath", szTemplatePath);
        tTransferData.setNameAndValue("OutXmlPath", sOutXmlPath);
        vData.add(tTransferData);
        if (true) {
            String a = "&fjfi";
        } else {
            String a;
        }
        vData.addElement("E:/lisdev/ui/f1print/template/");
        vData.addElement("E:/lisdev/ui/");
        if (!tZhuF1PUI.submitData(vData, "PRINT")) {
            System.out.println(tZhuF1PUI.mErrors.getFirstError());
            System.out.println("fail to get print data");
        }
    }

    /**
     * submitData
     *
     * @param vData VData
     * @param string String
     * @return boolean
     */
    public boolean submitData(VData vData, String string) {

        if (!getInputData(vData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData(vData, string)) {
            return false;
        }

        return true;
    }

    /**
     * dealData
     *
     * @param vData VData
     * @return boolean
     */
    private boolean dealData(VData vData, String string) {
        /** 境外救援的保单打印 */
        System.out.println("************");
        System.out.println(this.mLCContSchema.getCardFlag());
        System.out.println("************");

        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        String tCode = "";

        if ("1".equals(mLCContSchema.getCardFlag())) {
            if ("Notice".equals(mPrtFlag)) {
                tCode = "J04";
            } else {
                tCode = "J03";
            }
        } else if ("2".equals(mLCContSchema.getCardFlag())) {
            tCode = "J05";
            if (checkRisk(mLCContSchema.getContNo())) {
            	tCode = "J022";
            }else if (checkRiskJ(mLCContSchema.getContNo())) {
            	tCode = "J023";
            }
        } else if ("6".equals(mLCContSchema.getCardFlag())) {
            tCode = "J006";
            if (checkRiskC(mLCContSchema.getContNo())) {
            	tCode = "J024";
            }
        } else if ("5".equals(mLCContSchema.getCardFlag())) {
            tCode = "J010";
        }

        tLOPRTManagerSchema.setCode(tCode);

        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        tLOPRTManagerSet.add(tLOPRTManagerSchema);
        vData.add(tLOPRTManagerSet);

        if (this.mLCContSchema.getCardFlag().equals("1") ||
            this.mLCContSchema.getCardFlag().equals("2") ||
            this.mLCContSchema.getCardFlag().equals("5")) {
            PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new
                    PDFPrintBatchManagerBL();
            if (!tPDFPrintBatchManagerBL.submitData(vData, "only")) {
                this.mErrors.copyAllErrors(tPDFPrintBatchManagerBL.mErrors);
                return false;
            }
        }
        if (this.mLCContSchema.getCardFlag().equals("6")) {
//            PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new
//                    PDFPrintBatchManagerBL();
//            if (!tPDFPrintBatchManagerBL.submitData(vData, "only")) {
//                this.mErrors.copyAllErrors(tPDFPrintBatchManagerBL.mErrors);
//                return false;
//            }
        	VData tVData = new VData();
        	tVData.add(tLOPRTManagerSchema);
			tVData.add(mGlobalInput);
        	PDFPrintManagerBL tPDFPrintManagerBL = new PDFPrintManagerBL();
        	if (!tPDFPrintManagerBL.submitData(tVData, "only")) {
				mErrors.addOneError(tPDFPrintManagerBL.mErrors.getError(0));
				System.out.println("第一个错误："+mErrors.getFirstError());
				if (!mErrors.getError(0).functionName.equals("saveData")) {
					System.out.println("不需要处理的错误："+mErrors.getFirstError());
				} else {
					System.out.println("PDFPrintBatchManagerBL报非中断错："
							+ mErrors.getFirstError());
					return false;
				}
			}
        	
        	
        } else if ("1".equals(mLCContSchema.getIntlFlag())) {
            LCContPrintIntlPdfBL bl = new LCContPrintIntlPdfBL();
            if (!bl.submitData(vData, string)) {
                this.mErrors.copyAllErrors(bl.mErrors);
                return false;
            }
        }

        VData mInputData = new VData();
        LCContSchema sLCContSchema = new LCContSchema();        
        LCContDB mLCContDB = new LCContDB();
        mLCContDB.setContNo(mLCContSchema.getContNo());
        if (mLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCSuccorNewUI";
            tError.functionName = "dealData";
            tError.errorMessage = "请您确认：没查到相应的合同信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        sLCContSchema = mLCContDB.getSchema();
        
        if(sLCContSchema != null){
        	if(!"".equals(sLCContSchema.getContType()) && sLCContSchema.getContType().equals("1")){
        		map.put("update lccont set printcount = 1 where contno = '"+sLCContSchema.getContNo()+"'", "UPDATE");                
                mInputData.add(map);
                
                PubSubmit tPubSubmit = new PubSubmit();
                
                if (tPubSubmit.submitData(mInputData, "UPDATE") == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    return false;
                }
        	}
        }
        
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mLCContSchema == null) {
            buildError("checkData", "传入合同信息为null！");
            return false;
        }
        if (this.mLCContSchema.getContNo() == null ||
            this.mLCContSchema.getContNo().equals("")) {
            buildError("checkData", "传入合同号码为null！");
            return false;
        }
        if (this.mLCContSchema.getCardFlag() == null ||
            this.mLCContSchema.getCardFlag().equals("")) {
            buildError("checkData", "传入保单类型为null！");
            return false;
        }
        return true;
    }
    
    /**
	 * 判断是否使用个险模板--现仅有好日子产品使用--
	 * 
	 * @author 张成轩
	 * 
	 * @return false 不使 ture 使
	 */
	private boolean checkRisk(String contNo) {
		String checkSql = "select 1 from lcriskdutywrap where contno='"
				+ contNo
				+ "' "
				+ "And Exists (Select 1 From Ldcode1 Where Codetype = 'briefcontprint' And Code = 'J022' And Code1 = lcriskdutywrap.Riskwrapcode)";
		ExeSQL tExeSQL = new ExeSQL();
		String flag = tExeSQL.getOneValue(checkSql);
		if ("1".equals(flag)) {
			return true;
		}
		return false;
	}
	
	private boolean checkRiskJ(String contNo) {
		String checkSql = "select 1 from lcriskdutywrap where contno='"
				+ contNo
				+ "' "
				+ "And Exists (Select 1 From Ldcode1 Where Codetype = 'briefcontprint' And Code = 'J023' And Code1 = lcriskdutywrap.Riskwrapcode)";
		ExeSQL tExeSQL = new ExeSQL();
		String flag = tExeSQL.getOneValue(checkSql);
		if ("1".equals(flag)) {
			return true;
		}
		return false;
	}
	
	private boolean checkRiskC(String contNo) {
		String checkSql = "select 1 from lccont where contno='"
				+ contNo
				+ "' "
				+ "And Exists (Select 1 From Ldcode1 Where Codetype = 'bcprintcom' And Code = 'comcode' And Code1 = lccont.managecom)";
		ExeSQL tExeSQL = new ExeSQL();
		String flag = tExeSQL.getOneValue(checkSql);
		if ("1".equals(flag)) {
			return true;
		}
		return false;
	}

    /**
	 * getInputData
	 * 
	 * @return boolean
	 */
    private boolean getInputData(VData vData) {
        mLCContSchema = (LCContSchema) vData.getObjectByObjectName(
                "LCContSchema", 0);
        mTransferData = (TransferData) vData.getObjectByObjectName(
                "TransferData", 0);
        mPrtFlag = (String) mTransferData.getValueByName("prtFlag");
        mGlobalInput = (GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCSuccorUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
