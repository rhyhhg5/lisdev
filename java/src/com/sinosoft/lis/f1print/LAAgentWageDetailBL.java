/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAgentWageDetailBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计展业类型
    private String mBranchType = "";
    //统计渠道
    private String mBranchType2 = "";
    //文件暂存路径
    private String mfilePathAndName;
    //薪资月
    private String mWageNo;
    //查询年
    private String mYear;
   //查询月
    private String mMonth;
    //分公司名称
    private String mName;;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAAgentWageDetailBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("RBL start submitData");
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
        	System.out.println("RBL error PRINT");
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChnlPremReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
        	  System.out.println("RBL error getDataList");
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LABL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mBranchType = (String)cInputData.get(1);
     mBranchType2 = (String)cInputData.get(2);
     mYear =(String)cInputData.get(3) ;
     mMonth = (String)cInputData.get(4);
     mWageNo = mYear + mMonth;
     mName = (String)cInputData.get(5);
     mfilePathAndName=(String)cInputData.get(7);
     return true;
 }




    private boolean getDataList()
  {
      //查询人员
    	System.out.println("报表打印的有问题？没走到这个类里？");
      String manageSQL = "select B,C,AF,AH,AI,BA,D,E,BD,F,G,H,I,K,L,N,P,Q,TT,PP,R,S,T,AE,AG,DA,DB,DC,U,CB,V,SS,ShouldMoney,ShouldMoney,value(CBPTaxes,0),value(PCBPTaxes,0),value(ITBPTaxes,0),value(sumTaxex,0),W,X,Y,HH,JJ,KK,LL,Z,AA,AB,AC,AD,'0' A	  " +
      		" from (select (select b.name from laagent b where a.agentcode = b.agentcode) B,db2inst1.getunitecode(a.agentcode) C," +
      		" (select b.idno from laagent b where a.agentcode = b.agentcode) AF," +
      		" (select VALUE(NOWORKFLAG,'N')  from laagent b  where a.agentcode = b.agentcode) AH," +
      		" (select CHAR(ASTARTDATE) from LATREE b   where a.agentcode = b.agentcode) AI," +
      		" a.f04 D,a.f05 E,a.F01 BD,a.f07 F,a.f03 G,a.F09 H,a.f02 I,a.f12 K,a.f08 L,a.F10 N,a.F11 P,a.F18 Q,a.K16 TT,a.k17 PP," +
      		" a.F06 R,a.F19 S,a.F20 T,a.F22 AE,a.F23 AG,a.F13 DA,a.F14 DB,a.F15 DC,a.F30 U," +
      		" (select VALUE(sum(Money),0) from laRewardPunish where agentcode = a.agentcode  and date_FORMAT(DoneDate,'yyyymm') = a.indexcalno   and doneflag <> '4'  and aclass = '0' and branchtype = '1' AND branchtype2 = '01'   and doneflag = '7') CB," +
      		" a.K03 V,a.F16 SS,a.K15 W,a.K02 X,a.K01 Y,a.F26 HH,a.F27 JJ,a.F28 KK,a.F29 LL,a.K12 Z,a.LastMoney AA,a.SumMoney AB," +
      		" (select bank from LAAccounts where agentcode = a.agentcode  and state = '0') AC," +
      		" (select account from LAAccounts where agentcode = a.agentcode  and state = '0') AD," +
      		"a.ShouldMoney ShouldMoney,(select T7 from laindexinfo where agentcode = a.agentcode and indexcalno = a.indexcalno and managecom = a.managecom and indextype ='00') as CBPTaxes," +
      		"(select T70 from laindexinfo where agentcode = a.agentcode and indexcalno = a.indexcalno and managecom = a.managecom and indextype ='00') as PCBPTaxes," +
      		"(select T71 from laindexinfo where agentcode = a.agentcode and indexcalno = a.indexcalno and managecom = a.managecom and indextype ='00') as ITBPTaxes," +
      		"(select T8 from laindexinfo where agentcode = a.agentcode and indexcalno = a.indexcalno and managecom = a.managecom and indextype ='00') as sumTaxex ,(a.k07+a.k08+a.k09)BA from lawage a   where a.branchtype = '1'   and a.branchtype2 = '01' " +
      		" and a.indexcalno = '"+mWageNo+"' and a.ManageCom like '"+mManageCom+"%') as X " +
			" union " +
			" select '合计' B,'' C,'' AF,'' AH,'' AI,value(BA,0),value(D,0),value(E,0),value(BD,0),value(F,0),value(G,0),value(H,0),value(I,0)," +
			" value(K,0),value(L,0),value(N,0),value(P,0),value(Q,0),value(TT,0),value(PP,0),value(R,0),value(S,0),value(T,0),value(AE,0),value(AG,0),value(DA,0)," +
			" value(DB,0),value(DC,0),value(U,0),value(CB,0),value(V,0),value(SS,0),value(sumShouldMoney,0),value(sumShouldMoney,0),value(sumCBPTaxes,0),value(sumPCBPTaxes,0),value(sumITBPTaxes,0),value(sumAllTaxex,0),value(W,0),value(X,0),value(Y,0),value(HH,0),value(JJ,0),value(KK,0),value(LL,0),value(Z,0),value(AA,0),value(AB,0),'---' AC,'---' AD ,'1'  A  " +
			" from (select  sum(a.f04) D,sum(a.f05) E,sum(a.f01) BD,sum(a.f07) F,sum(a.f03) G,sum(a.F09) H,sum(a.f02) I,sum(a.f12) K,sum(a.f08) L,sum(a.F10) N," +
			" sum(a.F11) P,sum(a.F18) Q,sum(a.K16) TT,sum(a.K17) PP,sum(a.F06) R,sum(a.F19) S,sum(a.F20) T,sum(a.F22) AE,sum(a.F23) AG,sum(a.F13) DA,sum(a.F14) DB," +
			" sum(a.F15) DC,sum(a.F30) U,(select VALUE(sum(Money),0) from laRewardPunish where managecom like '"+mManageCom+"%' and date_FORMAT(DoneDate,'yyyymm') = '"+mWageNo+"'   and doneflag <> '4'  and aclass = '0' and branchtype = '1' AND branchtype2 = '01'   and doneflag = '7') CB," +
			" sum(K03) V,sum(a.F16) SS,sum(K15) W,sum(K02) X,sum(K01) Y,sum(F26) HH,sum(F27) JJ,sum(F28) KK,sum(f29) LL,sum(K12) Z,sum(LastMoney) AA,sum(SumMoney) AB," +
			"sum(a.ShouldMoney) as sumShouldMoney,(select sum(T7) from laindexinfo where  branchtype = '1'   and branchtype2 = '01' and indexcalno = '"+mWageNo+"' and managecom like '"+mManageCom+"%' and indextype ='00') as sumCBPTaxes," +
      		"(select sum(T70) from laindexinfo where  branchtype = '1'   and branchtype2 = '01' and indexcalno = '"+mWageNo+"' and managecom like '"+mManageCom+"%' and indextype ='00') as sumPCBPTaxes," +
      		"(select sum(T71) from laindexinfo where  branchtype = '1'   and branchtype2 = '01' and indexcalno = '"+mWageNo+"' and managecom like '"+mManageCom+"%' and indextype ='00') as sumITBPTaxes," +
      		"(select sum(T8) from laindexinfo where  branchtype = '1'   and branchtype2 = '01' and indexcalno = '"+mWageNo+"' and managecom like '"+mManageCom+"%' and indextype ='00') as sumAllTaxex,(sum(a.k07+a.k08+a.k09))as BA  from lawage a  " +
			" where a.branchtype = '1'   and a.branchtype2 = '01' and a.indexcalno = '"+mWageNo+"' and a.ManageCom like '"+mManageCom+"%' ) as X  order by A,C with ur   ";
        System.out.println(manageSQL);
//        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
//        tCreateCSVFile.initFile(this.mfilePathAndName);
//        WriteToExcel t = new WriteToExcel("");
//        t.createExcelFile();
//        String[][] tFirstDataList = new String[1][1];
//        String[][] tTheardDataList = new String[1][1];
//        String[][] tFourDataList = new String[1][36];
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(manageSQL);
        int linenum=tSSRS.MaxRow;
        String[][] tFourDataList = new String[linenum+5][50];
        tFourDataList[0][0]="中国人民健康保险股份有限公司"+mName+"分公司"+mYear+"年"+mMonth+"月营销业务人员工资明细表";
        WriteToExcel t = new WriteToExcel("");
        t.createExcelFile();
        tFourDataList[1][0]="编制部门:个人销售部 ";
        tFourDataList[2][0]="姓     名 ";
        tFourDataList[2][1]="代     码";
        tFourDataList[2][2]="身份证号";
        tFourDataList[2][3]="筹备标记";
        tFourDataList[2][4]="职级开始日期";
        tFourDataList[2][5]="综合拓展佣金";
        tFourDataList[2][6]="首年度佣金";    
        tFourDataList[2][7]="续期佣金";     
        tFourDataList[2][8]="续保佣金";     
        tFourDataList[2][9]="新人成长津贴";   
        tFourDataList[2][10]="转正奖";      
        tFourDataList[2][11]="续期服务奖";              
        tFourDataList[2][12]="岗位津贴";               
        tFourDataList[2][13]="季度奖";                
        tFourDataList[2][14]="增员奖";                
        tFourDataList[2][15]="晋升奖";                
        tFourDataList[2][16]="职务津贴";               
        tFourDataList[2][17]="绩优达标奖";              
        tFourDataList[2][18]="健代产佣金";
        tFourDataList[2][19]="健代寿佣金";
        
        tFourDataList[2][20]="管理津贴";               
        tFourDataList[2][21]="直接养成津贴";             
        tFourDataList[2][22]="间接养成津贴";             
        tFourDataList[2][23]="培训津贴";               
        tFourDataList[2][24]="财务支持费用";             
        tFourDataList[2][25]="季度团建补发津贴";           
        tFourDataList[2][26]="年度团建补发津贴";           
        tFourDataList[2][27]="推荐津贴";               
		tFourDataList[2][28]="加款(含财补加款)";          
        tFourDataList[2][29]="财补加款";               
        tFourDataList[2][30]="差勤扣款";               
        tFourDataList[2][31]="孤儿单服务奖金";            
        tFourDataList[2][32]="税前薪资";               
        tFourDataList[2][33]="应发合计";               
		tFourDataList[2][34]="总公司奖励应纳税项";          
		tFourDataList[2][35]="省公司奖励应纳税项";          
		tFourDataList[2][36]="中支奖励应纳税项";           
        tFourDataList[2][37]="应纳税所得额";             
        tFourDataList[2][38]="养老金";                
        tFourDataList[2][39]="个人所得税";              
        tFourDataList[2][40]="个人增值税";              
        tFourDataList[2][41]="个人城建税";              
        tFourDataList[2][42]="教育费附加税";             
        tFourDataList[2][43]="地方教育附加税";            
        tFourDataList[2][44]="其他附加税";              
        tFourDataList[2][45]="扣款";                 
        tFourDataList[2][46]="上次佣金余额";             
        tFourDataList[2][47]="实际收入";               
        tFourDataList[2][48]="开户银行名称";             
        tFourDataList[2][49]="开户账号";               

//        tCreateCSVFile.doWrite(tFirstDataList,0);
//        tCreateCSVFile.doWrite(tTheardDataList,0);
//        tCreateCSVFile.doWrite(tFourDataList,35);
//        SSRS tSSRS = new SSRS();
//        ExeSQL tExeSQL = new ExeSQL();
//        tSSRS = tExeSQL.execSQL(manageSQL);
        try{
        	
               
//                String[][] tShowDataList = new String[tSSRS.MaxRow][36];
//                int linenum=tSSRS.MaxRow;
                for(int i=1;i<=linenum;i++)
                {
                   if (!queryOneDataList(tSSRS,i,tFourDataList[i+2])){
                	  
                	   return false;
                   }
                }
                tFourDataList[linenum+4][0]="总经理：                       复核 ：                       制表：";
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
//                tCreateCSVFile.doWrite(tShowDataList,35);
                String[] sheetName ={PubFun.getCurrentDate()};
                t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
                t.setData(0, tFourDataList);
                t.write(this.mfilePathAndName);//获得文件读取路径
        }catch(Exception exc){
        	exc.printStackTrace();
        	return false;
        }
//        String[][] tlast1DataList = new String[1][1];
//        tlast1DataList[2][0]="总经理：                       复核 ：                       制表：";
//        tCreateCSVFile.doWrite(tlast1DataList,0);
//        tCreateCSVFile.doClose();
        return true;
 }

 /**
   * 查询填充表示数据
   * @return boolean
   */
  private boolean queryOneDataList(SSRS tSSRS,int i,
		  String[] pmOneDataList)
  {   String banktype="";
      try{
          
          pmOneDataList[0] =tSSRS.GetText(i, 1);
          pmOneDataList[1] =tSSRS.GetText(i, 2)+"";
          pmOneDataList[2]=tSSRS.GetText(i, 3)+"";
          pmOneDataList[3]=tSSRS.GetText(i, 4)+"";
          pmOneDataList[4] =tSSRS.GetText(i, 5)+"";
          pmOneDataList[5] = tSSRS.GetText(i, 6)+"";
          pmOneDataList[6] = tSSRS.GetText(i, 7)+"";
          pmOneDataList[7] = tSSRS.GetText(i, 8)+"";
          pmOneDataList[8] = tSSRS.GetText(i, 9)+"";
          pmOneDataList[9] =tSSRS.GetText(i, 10)+"";
          pmOneDataList[10] = tSSRS.GetText(i, 11)+"";
          pmOneDataList[11]  = tSSRS.GetText(i, 12)+"";
          pmOneDataList[12]  = tSSRS.GetText(i, 13)+"";
          pmOneDataList[13]  = tSSRS.GetText(i, 14)+"";
          pmOneDataList[14]  = tSSRS.GetText(i, 15)+"";
          pmOneDataList[15]  =tSSRS.GetText(i, 16)+"";
          pmOneDataList[16]  = tSSRS.GetText(i, 17)+"";
          pmOneDataList[17]  = tSSRS.GetText(i, 18)+"";
          pmOneDataList[18]  =tSSRS.GetText(i, 19)+"";
          pmOneDataList[19]  =tSSRS.GetText(i, 20)+"";
          pmOneDataList[20]  = tSSRS.GetText(i, 21)+"";
          pmOneDataList[21]  =tSSRS.GetText(i, 22)+"";
          pmOneDataList[22]  =tSSRS.GetText(i, 23)+"";
          pmOneDataList[23]  =tSSRS.GetText(i, 24)+"";
          pmOneDataList[24]  =tSSRS.GetText(i, 25)+"";
          pmOneDataList[25]  =tSSRS.GetText(i, 26)+"";
          pmOneDataList[26]  =tSSRS.GetText(i, 27)+"";
          pmOneDataList[27]  =tSSRS.GetText(i, 28)+"";
          pmOneDataList[28]  =tSSRS.GetText(i, 29)+"";
          pmOneDataList[29]  =tSSRS.GetText(i, 30)+"";
          pmOneDataList[30]  =tSSRS.GetText(i, 31)+"";
          pmOneDataList[31]  =tSSRS.GetText(i, 32)+"";
          pmOneDataList[32]  =tSSRS.GetText(i, 33)+"";
          pmOneDataList[33]  =tSSRS.GetText(i, 34)+"";
          pmOneDataList[34]  =tSSRS.GetText(i, 35)+"";
          pmOneDataList[35]  =tSSRS.GetText(i, 36)+"";
          pmOneDataList[36]  =tSSRS.GetText(i, 37)+"";
          pmOneDataList[37]  =tSSRS.GetText(i, 38)+"";
          pmOneDataList[38]  =tSSRS.GetText(i, 39)+"";
          pmOneDataList[39]  =tSSRS.GetText(i, 40)+"";
          pmOneDataList[40]  =tSSRS.GetText(i, 41)+"";
          pmOneDataList[41]  =tSSRS.GetText(i, 42)+"";
          pmOneDataList[42]  =tSSRS.GetText(i, 43)+"";
          pmOneDataList[43]  =tSSRS.GetText(i, 44)+"";
          pmOneDataList[44]  =tSSRS.GetText(i, 45)+"";
          pmOneDataList[45]  =tSSRS.GetText(i, 46)+"";
          pmOneDataList[46]  =tSSRS.GetText(i, 47)+"";
          pmOneDataList[47]  =tSSRS.GetText(i, 48)+"";
          pmOneDataList[48]  =tSSRS.GetText(i, 49)+"";
          pmOneDataList[49]  =tSSRS.GetText(i, 50)+"";
          
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }
 
 
    public VData getResult()
    {
        return mResult;
    }
}
