package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.ChangeCodetoName;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ConFeeF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public ConFeeF1PBL()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            System.out.println("cOperate:" + cOperate);
            if (!cOperate.equals("CONFIRM") &&
                !cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (cOperate.equals("CONFIRM"))
            {
                mResult.clear();
                // 准备所有要打印的数据
                getPrintData();
            }
            else if (cOperate.equals("PRINT"))
            {
                if (!saveData(cInputData))
                {
                    return false;
                }
            }
            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //mLJSPaySchema.setSchema((LJSPaySchema)cInputData.getObjectByObjectName("LJSPaySchema",0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ConFeeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        String flag = null;
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得LOPRTManager的数据时发生错误");
            return false;
        }

        LJSPaySet tLJSPaySet = new LJSPaySet();
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(tLOPRTManagerDB.getOtherNo());
        tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet == null)
        {
            mErrors.copyAllErrors(tLJSPayDB.mErrors);
            buildError("outputXML", "在取得LJSPay的数据时发生错误");
            return false;
        }
        LJSPaySchema tLJSPaySchema = tLJSPaySet.get(1);

        if (tLJSPaySchema.getOtherNoType().equals("1"))
        {
            flag = "GrpNo";
        }
        else
        if (tLJSPaySchema.getOtherNoType().equals("2"))
        {
            flag = "PerNo";
        }

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        texttag.add("GetNoticeNo", tLJSPaySchema.getGetNoticeNo());
        texttag.add("OtherNo", tLJSPaySchema.getOtherNo());
        //texttag.add("PayDate",tLJSPaySchema.getPayDate());
        texttag.add("SumDuePayMoney", tLJSPaySchema.getSumDuePayMoney());
        LAAgentDB tLAAgentDB=new LAAgentDB();
        tLAAgentDB.setAgentCode(tLJSPaySchema.getAgentCode());
        tLAAgentDB.getInfo();
        texttag.add("AgentCode", tLAAgentDB.getGroupAgentCode());
        texttag.add("AgentName",
                    ChangeCodetoName.getAgentName(tLJSPaySchema.getAgentCode()));

        if (flag.equals("GrpNo"))
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo(tLJSPaySchema.getOtherNo());
            if (!tLCGrpPolDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                buildError("outputXML", "在取得LCGrpPol的数据时发生错误");
                return false;
            }
            xmlexport.createDocument("GConFeeNotice.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("GrpName", tLCGrpPolDB.getGrpName());
            texttag.add("PayDate", tLCGrpPolDB.getPaytoDate());
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(tLCGrpPolDB.getManageCom());
            if (!tLDComDB.getInfo())
            {
                mErrors.copyAllErrors(tLDComDB.mErrors);
                buildError("outputXML", "在取得LDCom的数据时发生错误");
                return false;
            }
            texttag.add("ManageCom", tLDComDB.getName());
        }

        if (flag.equals("PerNo"))
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLJSPaySchema.getOtherNo());
            if (!tLCPolDB.getInfo())
            {
                mErrors.copyAllErrors(tLCPolDB.mErrors);
                buildError("outputXML", "在取得LCPol的数据时发生错误");
                return false;
            }
            xmlexport.createDocument("PConFeeNotice.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("AppntName", tLCPolDB.getAppntName());
            texttag.add("PayDate", tLCPolDB.getPaytoDate());
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(tLCPolDB.getManageCom());
            if (!tLDComDB.getInfo())
            {
                mErrors.copyAllErrors(tLDComDB.mErrors);
                buildError("outputXML", "在取得LDCom的数据时发生错误");
                return false;
            }
            texttag.add("ManageCom", tLDComDB.getName());
        }

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;

    }

    private boolean saveData(VData mInputData)
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName("LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);
        FirstPayF1PBLS tFirstPayF1PBLS = new FirstPayF1PBLS();
        tFirstPayF1PBLS.submitData(mResult, "PRINT");
        if (tFirstPayF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tFirstPayF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;

    }

    public static void main(String[] args)
    {
        ConFeeF1PBL conFeeF1PBL1 = new ConFeeF1PBL();
    }
}
