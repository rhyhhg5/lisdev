package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.operfee.IndiDueFeeListPrintBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class OmnipAnnalsTaxPrtListUI {

        /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public OmnipAnnalsTaxPrtListUI() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        OmnipAnnalsTaxPrtListBL tOmnipAnnalsTaxPrtListBL = new OmnipAnnalsTaxPrtListBL();

        if (!tOmnipAnnalsTaxPrtListBL.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(tOmnipAnnalsTaxPrtListBL.mErrors);
            return false;
        }
        mResult = tOmnipAnnalsTaxPrtListBL.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
