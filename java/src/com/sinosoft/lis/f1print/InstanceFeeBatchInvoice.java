package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.certify.CertTakeBackBL;
import com.sinosoft.lis.certify.CertTakeBackBLS;
import com.sinosoft.lis.certify.CertTakeBackUI;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.finfee.FFInvoicePrtManagerBL;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vdb.LOPRTManager2DBSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class InstanceFeeBatchInvoice implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    public String mCom=new String();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private TransferData mTransferData = new TransferData();

    private LOPRTInvoiceManagerSchema mLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
    private String mStartNo="";

    public InstanceFeeBatchInvoice()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        System.out.println("cOperate:" + cOperate);
        if (mOperate.equals("")) {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        System.out.println("getInputData:" + "start getInputData........");
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();
        if(!dealData()){
            return false;
        }

//      准备打印管理表数据
        if (!dealPrintMag()) {
         return false;
        }
        return true;
    }



    private boolean dealPrintMag() {
        try{
            LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
            String tLimit = PubFun.getNoLimit(mLJAPaySchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
            tLOPRTManager2Schema.setOtherNo(mLJAPaySchema.getPayNo());
            tLOPRTManager2Schema.setOtherNoType("05");
            tLOPRTManager2Schema.setCode(PrintManagerBL.CODE_INVOICE);
            tLOPRTManager2Schema.setManageCom(mLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setAgentCode(mLJAPaySchema.getAgentCode());
            tLOPRTManager2Schema.setReqCom(mLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setReqOperator(mLJAPaySchema.getOperator());
            tLOPRTManager2Schema.setExeOperator(this.mGlobalInput.Operator);
            tLOPRTManager2Schema.setPrtType("0");
            tLOPRTManager2Schema.setStateFlag("1");
            tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManager2Schema.setStandbyFlag4(mStartNo);
            tLOPRTManager2Schema.setPrintAmount(this.mLOPRTManagerSchema.getPrintAmount());
            tLOPRTManager2Schema.setPayer(this.mLOPRTManagerSchema.getPayer());
            tLOPRTManager2Schema.setStandbyFlag5(this.mLOPRTManagerSchema.getStandbyFlag5());
            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            tLOPRTManager2DB.setSchema(tLOPRTManager2Schema);
            tLOPRTManager2DB.insert();
            this.mResult.add(this.mLOPRTManagerSchema);
        }catch(Exception e){
            e.fillInStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String tContNo = "";
        String tEndorNo = "";
        String tAgentNo = "";
        String tAgentGroup = "";
        String tBranchattr = "";
        String tPayToDay = "";
        String AgentName = "";
        String AgentGroupName = "";
        String State = "";
        String Result = "";
        String tManageCom = "";
        String payCount = "";
        String mTaxCode = "";

        String tHPerson=this.mLOPRTManagerSchema.getStandbyFlag2();
        String tCPerson=this.mLOPRTManagerSchema.getStandbyFlag3();
        String tRemark=this.mLOPRTManagerSchema.getStandbyFlag4();
        this.mLOPRTManagerSchema.setStandbyFlag2("");
        this.mLOPRTManagerSchema.setStandbyFlag3("");
        this.mLOPRTManagerSchema.setStandbyFlag4("");
        
        String tFPDM = this.mLOPRTManagerSchema.getOldPrtSeq();//临时存储发票代码
        this.mLOPRTManagerSchema.setOldPrtSeq(null);

        String tAppntName="";
        String tRiskName="";
        String tRiskName2="";
        String tAppntNo="";
        int count = 0;

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "";

        LJAPayDB tLJAPayDB = new LJAPayDB();
        LJAPaySet tLJAPaySet = new LJAPaySet();
        
        if(this.mLOPRTManagerSchema.getOtherNo()==null || this.mLOPRTManagerSchema.getOtherNo().equals("")) {
          	mErrors.copyAllErrors(tLJAPayDB.mErrors);
            buildError("outputXML", "查询LJAPay的数据条件为空");
            return false;
        }
        tLJAPayDB.setPayNo(this.mLOPRTManagerSchema.getOtherNo());
        tLJAPaySet=tLJAPayDB.query();
        if(tLJAPaySet==null || tLJAPaySet.size()<1){
            mErrors.copyAllErrors(tLJAPayDB.mErrors);
            buildError("outputXML", "在取得LJAPay的数据时发生错误");
            return false;
        }else{
            mLJAPaySchema=tLJAPaySet.get(1);
        }
        String DSumMoney = "";
        double sumMoney = 0.0;
        String strSQL = "select SumActuPayMoney from LJAPay where Payno ='"
            + mLJAPaySchema.getPayNo() + "'";
        System.out.println("strSQL");
        String sMoney = tExeSQL.getOneValue(strSQL);

        if (StrTool.cTrim(sMoney).equals(""))
        {
            sumMoney = mLJAPaySchema.getSumActuPayMoney();
        }
        else
        {
            sumMoney = Double.parseDouble(sMoney);
        }
        if("5".equals(mLOPRTManagerSchema.getStandbyFlag5())){
        	sumMoney = mLOPRTManagerSchema.getPrintAmount();
        	
        }
        System.out.println("******金额变为："+sumMoney);
        DSumMoney = PubFun.getChnMoney(sumMoney);

        if (mLJAPaySchema.getIncomeType().equals("2"))
        {
            tSql = "select c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                + "when '530301' then (select riskwrapplanname from ldcode1 "
                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo from lcpol a,lmriskapp b,lccont c where c.ContNo = a.ContNo "
                + "and a.ContNo = '"+mLJAPaySchema.getIncomeNo()
                + "' and a.riskcode=b.riskcode and b.subriskflag='M' and a.stateflag='1' order by a.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,mLJAPaySchema.getIncomeNo());
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

            tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
            LJAPayPersonSchema aLJAPayPersonSchema = tLJAPayPersonSet.get(1);
            tContNo = aLJAPayPersonSchema.getContNo();
            tAgentNo = aLJAPayPersonSchema.getAgentCode();
            tLAAgentDB.setAgentCode(tAgentNo);
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }

            tAgentGroup = tLAAgentDB.getAgentGroup();
            tPayToDay = aLJAPayPersonSchema.getCurPayToDate();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            tBranchattr = tLABranchGroupDB.getBranchAttr();
            AgentName = "业务员：" + tLAAgentDB.getName();
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
            //个单发票，续期打印
            System.out.println("到个单续期打印");
           ExeSQL tExe2 = new ExeSQL();
            String tSql3="select max(int(a.paycount)) from ljapayperson a,ljapay b "
            +" where  a.contno=b.incomeno "
            +" and a.payno =b.payno "
            +"and b.incomeno='"+mLJAPaySchema.getIncomeNo()+"'"
            +"and b.payno='"+mLJAPaySchema.getPayNo()+"'"
            +"  with ur";
            String tPayCount =tExe2.getOneValue(tSql3);
            System.out.println("续期次数="+tPayCount);
            if(!StrTool.cTrim(tPayCount).equals("")){ 
                 	if(!tPayCount.equals("1")){
                 		State ="续期";
            		 count=Integer.parseInt(tPayCount);
            		 int  tPayCount2 =count;
            	    payCount ="第"+tPayCount2+"期续期";
            	    System.out.println(payCount);
            	}	
            }     
            /*
            if (!StrTool.cTrim(mLJAPaySchema.getGetNoticeNo()).equals(""))
            {
                Result = (new ExeSQL()
                        .getOneValue("select count(1) from ljapay a,ljspayb b where a.getnoticeno=b.getnoticeno and a.incometype='2' and a.getnoticeno='"
                                + mLJAPaySchema.getGetNoticeNo() + "'"));
                count = Integer.parseInt(Result);
                if(count > 0){
                    State = "续期";
                }
                ExeSQL tExe = new ExeSQL();
                String sumMonth = tExe.getOneValue("select (year(min(ap.curpaytodate)) - year(min(aps.lastpaytodate))) * 12 + month(min(ap.curpaytodate)) - month(min(aps.lastpaytodate))  from ljapayperson ap left join ljapayperson aps on aps.contno = '" + mLJAPaySchema.getIncomeNo() + "' where ap.payno= '" + mLJAPaySchema.getPayNo() + "'");
                String payintv = tExe.getOneValue("select distinct payintv from lcpol where contno='" + mLJAPaySchema.getIncomeNo() + "'");
                if(sumMonth == null || payintv == null || sumMonth.trim().equals("") || payintv.trim().equals("-1") || payintv.trim().equals("0")){
                    payCount = tExe.getOneValue("Select (Select Count (1) From Ljspayb Where Otherno = Ap.Incomeno And dealstate='1' And Makedate < (Select Makedate From Ljspayb Where Getnoticeno = Ap.Getnoticeno)) +  (Select Count (1) From Ljspayb Where Otherno = Ap.Incomeno And dealstate='1' And Makedate = (Select Makedate From Ljspayb Where Getnoticeno = Ap.Getnoticeno) and maketime <= (Select maketime From Ljspayb Where Getnoticeno = Ap.Getnoticeno)) From Ljapay Ap Where Ap.Payno = '" + mLJAPaySchema.getPayNo() + "'");
                    System.out.println("特殊续期数据获取期数----" + payCount);
                } else {
                    payCount = Math.round(Double.parseDouble(sumMonth) / Double.parseDouble(payintv) - 1) + "";
                    System.out.println("正常续期数据获取期数----" + payCount);
                }
                if(payCount == null || Integer.parseInt(payCount) <= 0){
                    System.out.println("获取续期期数失败----" + payCount);
                    payCount = "";
                } else {
                    payCount = "续第" + payCount + "期";
                }
            }*/
            
            String sqlC = "select distinct 1 ,lc.prtno from lcpol lc,lmriskapp lm where lm.riskcode=lc.riskcode and lm.TaxOptimal='Y' and lc.contno='"+mLJAPaySchema.getIncomeNo()+"' ";
            SSRS tSYSSRS = tExeSQL.execSQL(sqlC);
            if(tSYSSRS.getMaxRow()>0){
            	LCContSubDB tLCContSubDB = new LCContSubDB();
                tLCContSubDB.setPrtNo(tSYSSRS.GetText(1, 2));
                if(tLCContSubDB.getInfo()){
                	if(!"".equals(tLCContSubDB.getTaxCode())){
                		mTaxCode="税优识别码："+tLCContSubDB.getTaxCode();
                	}
                }   
            }           
        }

        if (mLJAPaySchema.getIncomeType().equals("1"))
        {
            tSql = "select  GrpName,RiskName,a.riskcode,a.CustomerNo from lcgrppol a,lmriskapp b where GrpContNo = '"
                + mLJAPaySchema.getIncomeNo()
                + "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,mLJAPaySchema.getIncomeNo());
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }

            LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.query();
            LJAPayGrpSchema aLJAPayGrpSchema = tLJAPayGrpSet.get(1);
            tContNo = aLJAPayGrpSchema.getGrpContNo();
            tPayToDay = aLJAPayGrpSchema.getCurPayToDate();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(aLJAPayGrpSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(aLJAPayGrpSchema.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            count = 0;
            Result = "";
            AgentName = "业务员：" + tLAAgentDB.getName();
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
          //团单续期缴费次数
            ExeSQL tExe2=new ExeSQL();
            String tSql4 ="select  max(int(a.paycount))  from  ljapaygrp a,ljapay b "
            +" where  a.grpcontno=b.incomeno "
            +" and a.payno=b.payno "
            +" and b.incomeno='"+mLJAPaySchema.getIncomeNo()+"' "
            +" and b.payno='"+mLJAPaySchema.getPayNo()+"' "
            +" with  ur";        
            String PayCount5 =tExe2.getOneValue(tSql4);
            System.out.println("续期的次数="+PayCount5);
            if(!StrTool.cTrim(PayCount5).equals("")){           	
            	if(!PayCount5.equals("1")){
            		   State="续期";
                      count =Integer.parseInt(PayCount5);
                      int count3=count;
                      payCount="第"+count3+"期续期";
                  }
            }
            /*  if (!StrTool.cTrim(aLJAPayGrpSchema.getGetNoticeNo()).equals(""))
            {
                Result = (new ExeSQL()
                        .getOneValue("select count(1) from ljapaygrp a,ljspaygrpb b where a.getnoticeno=b.getnoticeno and a.grppolno=b.grppolno and a.getnoticeno='"
                                + aLJAPayGrpSchema.getGetNoticeNo() + "'"));
                count = Integer.parseInt(Result);
                if(count > 0){
                    State = "续期";
                }
                ExeSQL tExe = new ExeSQL();
                String sumMonth = tExe.getOneValue("select (year(min(ap.curpaytodate)) - year(min(aps.lastpaytodate)))* 12 + month(min(ap.curpaytodate)) - month(min(aps.lastpaytodate))  from ljapayperson ap left join ljapayperson aps on aps.contno = '" + mLJAPaySchema.getIncomeNo() + "' where ap.payno= '" + mLJAPaySchema.getPayNo() + "'");
                String payintv = tExe.getOneValue("select distinct payintv from lcpol where contno='" + mLJAPaySchema.getIncomeNo() + "'");
                if(sumMonth == null || payintv == null || sumMonth.trim().equals("") || payintv.trim().equals("-1") || payintv.trim().equals("0")){
                    payCount = tExe.getOneValue("Select (Select Count (1) From Ljspayb Where Otherno = Ap.Incomeno And Makedate < (Select Makedate From Ljspayb Where Getnoticeno = Ap.Getnoticeno)) +  (Select Count (1) From Ljspayb Where Otherno = Ap.Incomeno And Makedate = (Select Makedate From Ljspayb Where Getnoticeno = Ap.Getnoticeno) and maketime <= (Select maketime From Ljspayb Where Getnoticeno = Ap.Getnoticeno)) From Ljapay Ap Where Ap.Payno = '" + mLJAPaySchema.getPayNo() + "'");
                    System.out.println("团特殊续期数据获取期数----" + payCount);
                } else {
                    payCount = Math.round(Double.parseDouble(sumMonth) / Double.parseDouble(payintv) - 1) + "";
                    System.out.println("团正常续期数据获取期数----" + payCount);
                }
                if(payCount == null || Integer.parseInt(payCount) <= 0){
                    System.out.println("团获取续期期数失败----" + payCount);
                    payCount = "";
                } else {
                    payCount = "续第" + payCount + "期";
                }
            }*/
        }

        if (mLJAPaySchema.getIncomeType().equals("10"))
        {
            tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                   + "when '530301' then (select riskwrapplanname from ldcode1 "
                   + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
                   + mLJAPaySchema.getPayNo()
                   + "'  and b.stateflag = '1' and a.contno=b.contno and b.riskcode=c.riskcode and c.subriskflag='M'  order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(mLJAPaySchema.getPayNo());
            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(aLJAGetEndorseSchema.getContNo());
            if (!tLCContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCContDB.mErrors);
                buildError("outputXML", "在取得LCCont的数据时发生错误");
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContDB.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
            tContNo = tLCContDB.getContNo();
            tPayToDay = tLCContDB.getPaytoDate();
            tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
        }

        if (mLJAPaySchema.getIncomeType().equals("3"))
        {
            tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '"
                   + mLJAPaySchema.getPayNo()
                   + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                   + " and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
             }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
             
            
            String GetEndorse = "select * from LJAGetEndorse where actugetno = '"+mLJAPaySchema.getPayNo()+"' and feefinatype not like 'YE%'";
           
            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(GetEndorse);
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);
            
            

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(aLJAGetEndorseSchema.getGrpContNo());
            if (!tLCGrpContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
                return false;
            }
            tContNo = tLCGrpContDB.getGrpContNo();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();

            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();

            LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.query();
            LJAPayGrpSchema aLJAPayGrpSchema = tLJAPayGrpSet.get(1);
            if (tLJAPayGrpSet.size() > 0)
            {
                tPayToDay = aLJAPayGrpSchema.getCurPayToDate();
            }
            else
            {
                tPayToDay = "";
            }
            tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
        }
        if (mLJAPaySchema.getIncomeType().equals("13"))
        {
            tSql = "Select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from ljagetendorse a,lcgrppol b,lmriskapp c "
                   +"where actugetno in ( Select Btactuno from ljaedorbaldetail "
                   +"where actuno in ( Select getnoticeno from LJAPay where payno='"
                   +mLJAPaySchema.getPayNo()+"' )) "
                   +"and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc ";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
            }

             LGWorkDB tLGWorkDB=new LGWorkDB();
            // tLJAGetEndorseDB.setActuGetNo(mLJAPaySchema.getPayNo());
            String endorseSQL = "select * from LGWork where workno in " +
            		"(select incomeno from ljapay where incomeno= "+
                    "'"+ mLJAPaySchema.getIncomeNo() + "' and incometype='13')";            
            LGWorkSet tLGWorkSet=tLGWorkDB.executeQuery(endorseSQL);
            LGWorkSchema tLGWorkSchema = tLGWorkSet.get(1);

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tLGWorkSchema.getContNo());

            if (!tLCGrpContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
                return false;
            }
            tContNo = tLCGrpContDB.getGrpContNo();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();

            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();

            LJSPayBDB tLJSPayBDB = new LJSPayBDB();
            tLJSPayBDB.setGetNoticeNo(mLJAPaySchema.getGetNoticeNo());
            LJSPayBSet tLJSPayBSet = tLJSPayBDB.query();
            LJSPayBSchema aLJSPayBSchema = tLJSPayBSet.get(1);
            tPayToDay = aLJSPayBSchema.getPayDate();
            tEndorNo = mLJAPaySchema.getIncomeNo();
        }
        if (mLJAPaySchema.getIncomeType().equals("15"))
        {
            tSql = "select distinct '',b.RiskName,b.riskcode,'' from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '"
                   + mLJAPaySchema.getIncomeNo()
                   + "' and a.riskcode=b.riskcode and b.subriskflag='M'  and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
                   + " order by  b.riskcode with ur";
            tSSRS=tExeSQL.execSQL(tSql);
            if(tSSRS.getMaxRow()>0){
                   tAppntName=tSSRS.GetText(1,1);
                   tRiskName=tSSRS.GetText(1,2);
                   tAppntNo=tSSRS.GetText(1,4);
                   tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,"");
                   if(tRiskName2!=""&&tRiskName2!=null){
                       tRiskName=tRiskName2;
                   }
            }

            String tAgentCode="";
            String ttContNo="";
            LJAPayPersonDB tLJAPayPersonDB=new LJAPayPersonDB();
            tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayPersonSet tLJAPayPersonSet=tLJAPayPersonDB.query();
            if(tLJAPayPersonSet!=null && tLJAPayPersonSet.size()>0){
                ttContNo=tLJAPayPersonSet.get(1).getContNo();
                tContNo = tLJAPayPersonSet.get(1).getContNo();
                tAgentCode=tLJAPayPersonSet.get(1).getAgentCode();
            }
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(ttContNo);
            if (!tLCContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCContDB.mErrors);
                buildError("outputXML", "在取得LCCont的数据时发生错误");
                return false;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContDB.getAgentGroup());
            if (tLABranchGroupDB.getInfo())
            {
                AgentGroupName = "营销服务部："+ tLABranchGroupDB.getBranchAttr();
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            AgentName = "业务员：" + tLAAgentDB.getName();
        }


        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLJAPaySchema.getManageCom());
        if (!tLDComDB.getInfo())
        {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            buildError("outputXML", "在取得LDCom的数据时发生错误");
            return false;
        }
        
        if("Y".equals(tLDComDB.getInteractFlag())){
        	LDComDB ttLDComDB = new LDComDB();
        	ttLDComDB.setComCode(tLDComDB.getComCode().substring(0, 4));
        	if (!ttLDComDB.getInfo())
            {
                buildError("outputXML", "在取得LDCom的数据时发生错误！");
                return false;
            }
        	tLDComDB.setName(ttLDComDB.getSchema().getName());
        }
        
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        mCom=mLJAPaySchema.getManageCom();
        if(StrTool.cTrim(mCom).length()<4){
            buildError("outputXML", "管理机构不正确，不存在该类型");
            return false;
        }
        texttag.add("JetFormType", "invoice");
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          this.mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", this.mGlobalInput.ClientIP.replaceAll("\\.","_"));
        texttag.add("previewflag", "1");

        if(mLJAPaySchema.getManageCom()!=null && mLJAPaySchema.getManageCom().length()>3){
            tManageCom = mLJAPaySchema.getManageCom().substring(0, 4);
        }
        texttag.add("CompanyAddress", tLDComDB.getLetterServicePostAddress());
        texttag.add("CompanyTel", tLDComDB.getServicePhone());
        if (tManageCom.equals("8637"))
        {
            xmlexport.createDocument("FeeInvoiceSD.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
        else if (tManageCom.equals("8632"))
        {
            xmlexport.createDocument("FeeInvoiceJS.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "78632112-0");
        }
        else if (tManageCom.equals("8633"))
        {
            xmlexport.createDocument("FeeInvoiceZJ.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
        else if (tManageCom.equals("8621"))
        {
            xmlexport.createDocument("FeeInvoiceLN.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
        else if (tManageCom.equals("8635"))
        {
            xmlexport.createDocument("FeeInvoiceFJ.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
        else if (tManageCom.equals("8644"))
        {
            xmlexport.createDocument("FeeInvoiceGD.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");

        }
        else
        {
            xmlexport.createDocument("FeeInvoice.vts", "printer"); //最好紧接着就初始化xml文档
            texttag.add("Attr1", "");
        }
        if(tManageCom.equals("8641")){

             //获取长无忧险的主险和附加险
            String sql="";
            if (mLJAPaySchema.getIncomeType().equals("1") || mLJAPaySchema.getIncomeType().equals("3") || mLJAPaySchema.getIncomeType().equals("13")){
                sql="select prem,riskcode,(select riskname from lmriskapp where riskcode=lcgrppol.riskcode) code from lcgrppol where grpcontno='"+tContNo+"' with ur";
            }else if(mLJAPaySchema.getIncomeType().equals("2") || mLJAPaySchema.getIncomeType().equals("10")){
                sql="select prem,riskcode,(select riskname from lmriskapp where riskcode=lcpol.riskcode) code from lcpol where contno='"+tContNo+"' with ur";
            }else if(mLJAPaySchema.getIncomeType().equals("15")){
                sql="select a.prem,,b.riskcode,(select riskname from lmriskapp where riskcode=lcpol.riskcode) code from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '"
                    + this.mLJAPaySchema.getIncomeNo()
                    + "' and a.riskcode=b.riskcode and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
                    + " order by  b.riskcode";
            }
            System.out.println("=="+sql+"==");
            ExeSQL tESQL = new ExeSQL();
            SSRS tRS = tESQL.execSQL(sql);
            int flag=0;
            String mainRisk1="";
            String subRisk1="";
            String RistNameStr="";
            if(tRS!=null && tRS.MaxRow>0){
               for(int index=1;index<=tRS.MaxRow;index++){
                   String tPrem=tRS.GetText(index, 1);
                   String tRiskCode=tRS.GetText(index, 2);
                   String ttRiskName=tRS.GetText(index, 3);
                   if(StrTool.cTrim(tRiskCode).equals("330501") || StrTool.cTrim(tRiskCode).equals("530301")){
                       flag=1;
                   }

                   if(StrTool.cTrim(tRiskCode).equals("330501")){
                        mainRisk1=ttRiskName+" "+tPrem+"元";
                   }

                   if(StrTool.cTrim(tRiskCode).equals("530301")){
                        subRisk1=ttRiskName+" "+tPrem+"元";
                   }
                   RistNameStr+=ttRiskName+" "+tPrem+"元;";
               }
            }
            if(flag==1){
                texttag.add("Flag", "2");
                if(tRiskName.substring(tRiskName.length()-1).equals("等")){
                    tRiskName=tRiskName.substring(0,tRiskName.length()-1);
                }
                texttag.add("RiskName1", mainRisk1);
                texttag.add("RiskName2", subRisk1);
                texttag.add("RiskName3", "");
                texttag.add("RiskName4", "");
            }else{
                texttag.add("Flag", "1");
                tRiskName=RistNameStr;
            }

            System.out.println(tRiskName);
        }

        /*
         * Modify by zhangbin Start
         */
        String tPostalAddress="";
        String tZipCode="";
        String tAttr2="";
        String ttSql = "select postaladdress,zipcode from lcaddress where customerno='"+tAppntNo+"' and addressno = (select addressno from lcappnt where appntno = customerno and contno = '"+tContNo+"') ";
        tSSRS = tExeSQL.execSQL(ttSql);
        if(tSSRS.getMaxRow()>0){
            if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                tPostalAddress=tSSRS.GetText(1,1);
            }
            if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                tZipCode=tSSRS.GetText(1,2);
            }
        }else{
            ttSql = "select a.grpaddress,grpzipcode from LCGrpAddress a where a.customerno='"+tAppntNo+"' and a.addressno = (select b.addressno from lcgrpappnt b where b.customerno = a.customerno and b.grpcontno = '"+tContNo+"') ";
            tSSRS = tExeSQL.execSQL(ttSql);
            if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("")){
                tPostalAddress=tSSRS.GetText(1,1);
            }
            if(tSSRS.GetText(1,2)!=null && !tSSRS.GetText(1,2).trim().equals("")){
                tZipCode=tSSRS.GetText(1,2);
            }
        }
        /*
         * Modify by zhangbin End
         */

        if(!tZipCode.equals("")){
            tAttr2="邮编："+tZipCode+" ";
        }
        if(!tPostalAddress.equals("")){
            tAttr2="地址："+tPostalAddress;
        }
        texttag.add("Attr2", tAttr2);
        tSSRS.Clear();
      if("5".equals(mLOPRTManagerSchema.getStandbyFlag5())){
    	tAppntName = mLOPRTManagerSchema.getPayer();
    	
    }
      System.out.println("******付款人变为："+tAppntName);
        texttag.add("XSumMoney", "￥" + format(sumMoney));
        texttag.add("AppntNo", tAppntNo);
        texttag.add("AppntName", tAppntName);
        texttag.add("RiskName", tRiskName);
        texttag.add("ContNo", tContNo);
        
        String tPrtNo = "";
        if(tContNo != null && !"".equals(tContNo)) {
        	String sql = "select prtno from lcgrpcont where grpcontno='" + tContNo + "' with ur";
        	tPrtNo = tExeSQL.getOneValue(sql);
        	if(tPrtNo == null || "".equals(tPrtNo)) {
        		sql = "select prtno from lccont where contno='" + tContNo + "' with ur";
            	tPrtNo = tExeSQL.getOneValue(sql);
        	}
        }
        texttag.add("PrtNo", tPrtNo);
        
        texttag.add("PayToDay", tPayToDay);

        
        if (!StrTool.cTrim(tEndorNo).equals("") && (tManageCom.equals("8637")))
        {
            texttag.add("EndorNo", "批单号：" + tEndorNo);
        }
        else if ((!StrTool.cTrim(tEndorNo).equals("") && (tManageCom
                .equals("8632"))))
        {
            texttag.add("EndorNo", "批单号：" + tEndorNo);
        }
        else
        {
            texttag.add("EndorNo", tEndorNo);
        }
        if (!StrTool.cTrim(tRemark).equals("") && (tManageCom.equals("8637")))
        {
            texttag.add("Remark", "批注：" + tRemark);
        }
        else if ((!StrTool.cTrim(tRemark).equals("") && (tManageCom
                .equals("8632"))))
        {
            texttag.add("Remark", "批注：" + tRemark);
        }else if(((tManageCom
                .equals("8611")))){
            String  maxCount = "1";
            String msql = "select max(paycount) from ljapayperson where contno='"+tContNo+"'";
            System.out.println(msql);
            tSSRS = tExeSQL.execSQL(msql);
            if(tSSRS.getMaxRow()>0){
                if(tSSRS.GetText(1,1)!=null && !tSSRS.GetText(1,1).trim().equals("") && !tSSRS.GetText(1,1).trim().equals("0")){
                    maxCount=tSSRS.GetText(1,1);
                }
            }
            texttag.add("Remark", "第"+maxCount+"次 "+tRemark);
        }else if(tManageCom.equals("8612")){
            String msql = "select cvalidate from lccont where contno='"+tContNo+"' union all select cvalidate from lcgrpcont where grpcontno='"+tContNo+"'  with ur";
            tSSRS = tExeSQL.execSQL(msql);
            if(tSSRS.getMaxRow()>0){
                if(!StrTool.cTrim(tSSRS.GetText(1,1)).equals("")){
                    FDate fDate = new FDate();
                    Date startDate = fDate.getDate(tSSRS.GetText(1,1));
                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat df = new SimpleDateFormat(pattern);
                    String tString = df.format(startDate);
                    tRemark+=" 生效日期 "+tString.substring(0,4)+"年"+tString.substring(5, 7)+"月"+tString.substring(8,10)+"日";
                }
            }
            texttag.add("Remark", "批注：" + tRemark);
            
            if(tFPDM==null || tFPDM.equals("null") || tFPDM.equals(""))
            {
            	System.out.println("天津打印发票，发票代码不能为空");
                CError cError = new CError();
                cError.moduleName = "InstanceFeeBatchInvoice";
                cError.functionName = "dealData";
                cError.errorMessage = "发票代码不能为空（天津）";
                this.mErrors.addOneError(cError);
                return false;
            }
        }
        else
        {
            texttag.add("Remark", tRemark);
        }
        texttag.add("HPerson", tHPerson);
        texttag.add("CPerson", tCPerson);
        
        //统一工号修改
        String SQL = "select getUniteCode("+tAgentNo+") from dual where 1=1 with ur";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(SQL);
	  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
	  		tAgentNo = aSSRS.GetText(1, 1);
	  	}
        texttag.add("AgentCode", tAgentNo);
        texttag.add("AgentGroup", tBranchattr);
        texttag.add("State", State);
        texttag.add("PayCount", payCount);
        texttag.add("AgentName", AgentName);
        texttag.add("AgentGroupName", AgentGroupName);
        texttag.add("ManageCom", mLJAPaySchema.getManageCom());    
        if (tManageCom.equals("8611") || tManageCom.equals("8612"))
        {
            texttag.add("DSumMoney", DSumMoney);
        }
        else
        {
            texttag.add("DSumMoney", "人民币 " + DSumMoney);
        }
        String ComName = tLDComDB.getName();
        String ComCode = tLDComDB.getComCode();
        if(ComCode.trim().length() >= 4 && ComCode.substring(0, 4).equals("8622")){
            ComName = "中国人民健康保险股份有限公司" + ComName;
            System.out.println("分公司名--" + ComName);
        }
        if(ComCode.trim().length() >= 4 && ComCode.substring(0, 4).equals("8633")){
            ComName = "中国人民健康保险股份有限公司" + ComName;
            System.out.println("分公司名--" + ComName);
        }
        if(ComCode.trim().length() >= 4 && ComCode.substring(0, 4).equals("8665")){
        	if(ComCode.trim().equals("86650000")){
        		ComName = "中国人民健康保险股份有限公司新疆分公司";
        	}else{
        		ComName = "中国人民健康保险股份有限公司" + ComName;
        	}            
            System.out.println("分公司名--" + ComName);
        }
        texttag.add("Com", ComName);
        texttag.add("TaxNo", tLDComDB.getTaxRegistryNo());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        texttag.add("Today", sdf.format(new Date()));
        
        //单证核销
        String tStandbyFlag1 = this.mLOPRTManagerSchema.getStandbyFlag1();
        int tNum = tStandbyFlag1.lastIndexOf(",");
        String tCertifyCode = tStandbyFlag1.substring(0,tNum);
        mLOPRTManagerSchema.setStandbyFlag1(tCertifyCode);
        mStartNo = tStandbyFlag1.substring(tNum+1);

        if ("00000000".equals(mStartNo)) {
            String tMinSql = "select min(StartNo) from LZCARD WHERE CertifyCode='"
                   + tCertifyCode + "' and ReceiveCom='B" + this.mGlobalInput.Operator
                   + "' and StateFlag in ('0','7') ";
            tSSRS = tExeSQL.execSQL(tMinSql);
            if(tSSRS.getMaxRow()>0){
                mStartNo=tSSRS.GetText(1,1);
            }
        }
        texttag.add("fphm",  mStartNo);//发票号码
        System.out.println("发票号码：" + mStartNo );
        
        String sql = "select CvaliDate, AppntIDNo from LCCont where ContNo='" + tContNo + "' union all select CvaliDate,(select OrganComCode from LDGrp where CustomerNo = a.AppntNo fetch first 1 row only) from LCGrpCont a where GrpContNo='"+tContNo+"' ";
        SSRS tjSSRS = tExeSQL.execSQL(sql);
        if(tjSSRS.getMaxRow()>0)
        {
        	texttag.add("validate",  tjSSRS.GetText(1,1));//生效日期
            texttag.add("fkfhm",  tjSSRS.GetText(1,2));//付款方纳税人识别号
        }
        
        texttag.add("fp_dm", tFPDM);//发票代码
        this.saveFPDM(tFPDM,mStartNo);
        
        System.out.println("生效日期：" +tjSSRS.GetText(1,1)+ "，付款方纳税人识别号：" +tjSSRS.GetText(1,2)+ "，发票代码：" +tFPDM);
        texttag.add("TaxCode", mTaxCode);
        
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        mResult.clear();
        mResult.addElement(xmlexport);

        tSql = "select * from LZCARD WHERE CertifyCode='"
             + tCertifyCode
             + "' and ReceiveCom='B"
             + this.mGlobalInput.Operator
             + "' and StateFlag in ('0','7') and startno<='"
             + mStartNo + "' and endno>='"
             + mStartNo + "'";
        System.out.println(tSql);


        LZCardDB tLZCardDB=new LZCardDB();
//        tLZCardDB.setCertifyCode(tCertifyCode);
//        tLZCardDB.setStartNo(mStartNo);
//        tLZCardDB.setStateFlag("0");
        LZCardSet tLZCardSet = new LZCardSet();
        tLZCardSet = tLZCardDB.executeQuery(tSql);
        LZCardSchema schemaLZCard = new LZCardSchema();
        if(tCertifyCode==null || tCertifyCode.trim().equals("")
           || mStartNo==null || mStartNo.trim().equals("")
           ||tLZCardSet==null || tLZCardSet.size()<=0){
            System.out.println("单证号为空");
            CError cError = new CError();
            cError.moduleName = "InstanceFeeBatch";
            cError.functionName = "dealData";
            cError.errorMessage = "查询单证号为空";
            this.mErrors.addOneError(cError);
            return false;
        }
        if(tLZCardSet!=null && tLZCardSet.size()>0){
            schemaLZCard.setCertifyCode(tCertifyCode);
            schemaLZCard.setSubCode("");
            schemaLZCard.setRiskCode("");
            schemaLZCard.setRiskVersion("");

            schemaLZCard.setStartNo(mStartNo);
            schemaLZCard.setEndNo(mStartNo);
            schemaLZCard.setStateFlag("5");

            schemaLZCard.setSendOutCom(tLZCardSet.get(1).getReceiveCom());
            schemaLZCard.setReceiveCom("00");

            schemaLZCard.setSumCount(0);
            schemaLZCard.setPrem("");
            schemaLZCard.setAmnt(1);
            schemaLZCard.setHandler(tLZCardSet.get(1).getHandler());
            schemaLZCard.setHandleDate(tLZCardSet.get(1).getHandleDate());
            schemaLZCard.setInvaliDate(tLZCardSet.get(1).getInvaliDate());

            schemaLZCard.setTakeBackNo("");
            schemaLZCard.setSaleChnl("");
            schemaLZCard.setOperateFlag("");
            schemaLZCard.setPayFlag("");
            schemaLZCard.setEnterAccFlag("");
            schemaLZCard.setReason("");
            schemaLZCard.setState("");
            schemaLZCard.setOperator("");
            schemaLZCard.setMakeDate("");
            schemaLZCard.setMakeTime("");
            schemaLZCard.setModifyDate("");
            schemaLZCard.setModifyTime("");

            LZCardSet UpdateLZCardSet=new LZCardSet();
            UpdateLZCardSet.add(schemaLZCard);

      //        准备传输数据 VData
            VData vData = new VData();

            vData.addElement(this.mGlobalInput);
            vData.addElement(UpdateLZCardSet);

            Hashtable hashParams = new Hashtable();
            hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
            vData.addElement(hashParams);
                // 数据传输

            System.out.println("start take back ............");
            CertTakeBackUI tCertTakeBackUI = new CertTakeBackUI();

            if (!tCertTakeBackUI.submitData(vData, "HEXIAO")) {
                 mErrors.copyAllErrors(CertifyFunc.mErrors);
                 return false;
            }
        }

        try
        {
                mLOPRTInvoiceManagerSchema.setComCode(mGlobalInput.ManageCom);
                mLOPRTInvoiceManagerSchema.setInvoiceNo(mStartNo);
                mLOPRTInvoiceManagerSchema.setContNo(tContNo);
                mLOPRTInvoiceManagerSchema.setEndorNo(tEndorNo);
                mLOPRTInvoiceManagerSchema.setXSumMoney(sumMoney);
                mLOPRTInvoiceManagerSchema.setPayerName(tAppntName);
                //        mLOPRTInvoiceManagerSchema.setPayerAddr("辽宁");
                mLOPRTInvoiceManagerSchema.setOpdate(PubFun.getCurrentDate());
                // 为大连准备
                mLOPRTInvoiceManagerSchema.setMemo(tCertifyCode);
                
                //大连分公司手工输入
				if (tFPDM != null && !"".equals(tFPDM.trim())) {
					mLOPRTInvoiceManagerSchema.setInvoiceCode(tFPDM);
				}
                
                VData tData = new VData();
                tData.add(mGlobalInput);
                tData.add(mLOPRTInvoiceManagerSchema);

                FFInvoicePrtManagerBL tFFInvoicePrtManagerBL = new FFInvoicePrtManagerBL();
                if (!tFFInvoicePrtManagerBL.submitData(tData, "INSERT"))
                {
                    System.out.println("Invoice print info save failed...");
                }
                else
                {
                    System.out.println("Invoice print info save successed...");
                }
        }
        catch (Exception ex)
        {
            System.out.println("Invoice print info save failed...");
            CError cError = new CError();
            cError.moduleName = "BriefFeeF1PBL";
            cError.functionName = "FFInvoicePrtManagerBL.submit";
            cError.errorMessage = "发票打印信息保存失败";
            this.mErrors.addOneError(cError);
            return false;
        }
        //xmlexport.outputDocumentToFile("D:\\test\\", "invoice");

        return true;
    }

	/**
     * 输入:险种编码
     * 输出：险种套餐的套餐名
     * 修改人：yanjing
     */
    private String getRiskName(String riskCode,ExeSQL tExeSQL,String tcontno){
        String riskName="";
        String riskSql="select riskwrapplanname from ldcode1 where codetype = 'bankcheckappendrisk' and code1 ='"+riskCode
        +"'  and code in(select riskwrapcode from lcriskwrap where grpcontno='"
        +tcontno+"' union select riskwrapcode from lcriskdutywrap where contno='"+tcontno+"' fetch first 1 rows only) "
        +" union "
        +" select riskwrapplanname from ldcode1 where codetype='checkappendrisk' and code1='"+riskCode+"'"
        +" with ur";
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL(riskSql);
        if(tSSRS.getMaxRow()>0){
            riskName=tSSRS.GetText(1, 1);
        }
        return riskName;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//      全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FeeInvoiceF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }
    
    //存储发票代码
    private void saveFPDM(String afpdm, String aStartNo) 
    {
    	if(afpdm!= null && !StrTool.cTrim(afpdm).equals("") && !StrTool.cTrim(afpdm).equals("null"))
        {
        	LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
            tLPEdorEspecialDataDB.setEdorNo(mLJAPaySchema.getPayNo());
            tLPEdorEspecialDataDB.setDetailType("fp_dm");
            tLPEdorEspecialDataDB.setEdorAcceptNo(mLJAPaySchema.getPayNo());
            tLPEdorEspecialDataDB.setEdorType("FP");
            tLPEdorEspecialDataDB.setEdorValue(afpdm);
            tLPEdorEspecialDataDB.setPolNo(aStartNo);
            tLPEdorEspecialDataDB.setState("1");
            tLPEdorEspecialDataDB.delete();
            tLPEdorEspecialDataDB.insert();
        }
	}

    public static void main(String[] args)
    {
    }
}
