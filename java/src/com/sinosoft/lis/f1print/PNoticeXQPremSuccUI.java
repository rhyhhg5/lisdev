package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2004-5-26
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PNoticeXQPremSuccUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strMainPolNo = "";
    private String strAppntName = "";
    private String strAccName = "";
    private String strDate = "";
    private String strBankAccNo = "";
    private String strStation = "";
    private String mOperate;

    public PNoticeXQPremSuccUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate = cOperate;
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            PNoticeXQPremSuccBL tPNoticeXQPremSuccBL = new PNoticeXQPremSuccBL();
            System.out.println("Start XQPremBankSuccUI Submit ...");
            if (!tPNoticeXQPremSuccBL.submitData(cInputData, cOperate))
            {
                if (tPNoticeXQPremSuccBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPNoticeXQPremSuccBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "PNoticeXQPremSuccBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPNoticeXQPremSuccBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PLPsqsUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinChargeDayModeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {

        String aMainPolNo = "86110020030210001110";
        String aAppntName = "李茹";
        String aAccName = "";
        String aDate = "";

        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";

        VData tVData = new VData();
        tVData.addElement(aMainPolNo);
        tVData.addElement(aAppntName);
        tVData.addElement(aAccName);
        tVData.addElement(aDate);

        PNoticeXQPremSuccUI tPNoticeXQPremSuccUI = new PNoticeXQPremSuccUI();
        if (tPNoticeXQPremSuccUI.submitData(tVData, "PRINT"))
        {
            System.out.println("执行完毕");
        }
    }
}