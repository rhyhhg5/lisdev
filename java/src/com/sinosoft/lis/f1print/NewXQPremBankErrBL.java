package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :续期保费银行转账失败清单
 * @version 1.0
 * @date 2004-4-27
 */

import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCAppntIndSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewXQPremBankErrBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();
    public PremBankPubFun mPremBankPubFun = new PremBankPubFun();

    private LYBankLogSet mLYBankLogSet = new LYBankLogSet();
    private LDBankSchema mLDBankSchema = new LDBankSchema();
    private LDCode1Schema mLDCode1Schema = new LDCode1Schema();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();

    //初始化全局变量，从前台承接数据
    private String strStartDate = ""; //开始日期
    private String strEndDate = ""; //结束日期
    private String strAgentState = ""; //业务员的状态(1为在职单，0为孤儿单)
    private String strPremType = ""; //首续期的标志
    private String strFlag = ""; //S or F(S为银行代收，F为银行代付)
    private String strComCode = ""; //系统登陆的机构(查询银行日志表)
    private String strStation = ""; //界面上录入的管理机构

    private String strBillNo = ""; //批次号码
    private String mBankName = ""; //银行名称
    private String mErrorReason = ""; //失败原因
    private String mChkSuccFlag = ""; //银行校验成功标志；
    private String mChkFailFlag = ""; //银行校验失败标志；
    private String mAgentGroup = "";
    private String mAgentState = "";
    private double mMoney = 0.00;
    private int mCount = 0;
    private VData mInputData;
    public NewXQPremBankErrBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        if (!cOperate.equals("PRINT"))
        {
            mPremBankPubFun.buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!CaseFunPub.checkDate(strStartDate, strEndDate))
        {
            mPremBankPubFun.buildError("submitData", "开始日期比结束日期晚,请从新录入");
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getPrintData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NewPrintXQBankErr.vts", "printer");

        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");
        //开始就查询LJSPayPerson，最后查询银行的信息

        String Main_sql =
                " select LJSPayPerson.PolNo,LJSPayPerson.GetNoticeNo,"
                + " LJSPayPerson.SumDuePayMoney,LJSPayPerson.LastPayToDate ,"
                +
                " LYReturnFromBankB.BankSuccFlag ,LYReturnFromBankB.BankCode, "
                + " LJSPayPerson.BankAccNo  "
                + " from LJSPayPerson ,LYReturnFromBankB where "
                + " LJSPayPerson.GetNoticeNo =  LYReturnFromBankB.PayCode "
                + " and LJSPayPerson.PayCount >'1' "
                + " and LJSPayPerson.ManageCom like '" + strStation + "%' "
                + " and  LYReturnFromBankB.serialno in ( "
                + " select serialno from LYBankLog where startDate >= '" +
                strStartDate + "' "
                + " and StartDate <='" + strEndDate + "' "
                + " and LogType = '" + strFlag +
                "' and ReturnDate is not null )";
        System.out.println("总的查询语句是" + Main_sql);
        ExeSQL M_exesql = new ExeSQL();
        SSRS M_ssrs = M_exesql.execSQL(Main_sql);
        System.out.println("count for M_ssrs is " + M_ssrs.getMaxRow());
        if (M_ssrs.getMaxRow() > 0)
        {
            for (int M_count = 1; M_count <= M_ssrs.getMaxRow(); M_count++)
            {
                String b_BankSuccFlag;
                //判断是成功还是失败
                if (M_ssrs.GetText(M_count, 5).trim() == null ||
                    M_ssrs.GetText(M_count, 5).trim().equals(""))
                {
                    b_BankSuccFlag = "1";
                }
                else
                {
                    b_BankSuccFlag = M_ssrs.GetText(M_count, 5).trim(); //银行成功标记
                }
                //需要进行确认
                String hq_flag_b = mPremBankPubFun.getBankSuccFlag(M_ssrs.
                        GetText(M_count, 6).trim());
                boolean jy_flag_b = mPremBankPubFun.verifyBankSuccFlag(
                        hq_flag_b, b_BankSuccFlag);

                System.out.println("校验后的成功标志是" + jy_flag_b);
                if (jy_flag_b)
                {
                    continue;
                }
                String S_sql = mPremBankPubFun.getPolType(M_ssrs.GetText(
                        M_count, 1), strAgentState);
                ExeSQL S_exesql = new ExeSQL();
                SSRS S_ssrs = S_exesql.execSQL(S_sql);
                System.out.println("S_ssrs" + S_ssrs.getMaxRow());
                // PolNo ,MainPolNo,AgentCode,AgentGroup,AppntNo
                if (S_ssrs.getMaxRow() <= 0)
                {
                    continue;
                }
                else
                {
                    //查询出其他的信息
                    String b_errorReason = mPremBankPubFun.getErrInfo(
                            "bankerror", M_ssrs.GetText(M_count, 6).trim(),
                            b_BankSuccFlag); //调用错误信息的方法
                    LDBankSchema tLDBankSchema = new LDBankSchema();
                    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                    tLAAgentSchema = mPremBankPubFun.getAgentInfo(S_ssrs.
                            GetText(1, 3)); //查询代理人姓名
                    mAgentGroup = mPremBankPubFun.getAgentGroup(S_ssrs.GetText(
                            1, 4)); //查询代理人组别
                    String strHZ_count = mPremBankPubFun.getLJSPay(M_ssrs.
                            GetText(M_count, 2)); //查询划账次数
                    LCAppntIndSchema pLCAppntIndSchema = new LCAppntIndSchema();
                    pLCAppntIndSchema = mPremBankPubFun.getAppntInfo(S_ssrs.
                            GetText(1, 5)); //查询投保人信息
                    tLDBankSchema.setSchema(mPremBankPubFun.getBankInfo(M_ssrs.
                            GetText(M_count, 6))); //调用得到银行信息的函数

                    //对其进行赋值
                    String[] cols = new String[13];
                    cols[0] = mAgentGroup; //业务员组别
                    cols[1] = tLAAgentSchema.getName(); //业务员姓名
                    cols[2] = pLCAppntIndSchema.getName(); //投保人姓名
                    cols[3] = S_ssrs.GetText(1, 2); //主险保单号码
                    cols[4] = tLDBankSchema.getBankName(); //开户行
                    cols[5] = M_ssrs.GetText(M_count, 7); //账号
                    cols[6] = M_ssrs.GetText(M_count, 4); //应缴日期
                    cols[7] = M_ssrs.GetText(M_count, 3); //应缴金额
                    cols[8] = strHZ_count; //划账次数
                    cols[9] = b_errorReason; //失败原因
                    cols[10] = tLAAgentSchema.getMobile(); //业务员电话
                    cols[11] = pLCAppntIndSchema.getPostalAddress(); //投保人地址
                    cols[12] = pLCAppntIndSchema.getPhone(); //投保人电话
                    alistTable.add(cols);
                    mMoney = mMoney + Double.parseDouble(cols[7]);
                    mCount = mCount + 1;
                }
            }
        }
        else
        {
            String MainB_sql =
                    " select LJSPayPerson.PolNo,LJSPayPerson.GetNoticeNo,"
                    +
                    " LJSPayPerson.SumDuePayMoney,LJSPayPerson.LastPayToDate ,"
                    +
                    " LYReturnFromBank.BankSuccFlag ,LYReturnFromBank.BankCode, "
                    + " LJSPayPerson.BankAccNo  "
                    + " from LJSPayPerson ,LYReturnFromBank where "
                    + " LJSPayPerson.GetNoticeNo =  LYReturnFromBank.PayCode "
                    +
                    " and LJSPayPerson.PayCount >'1' and LJSPayPerson.ManageCom like '" +
                    strStation + "%'"
                    + " and  LYReturnFromBank.serialno in ( "
                    + " select serialno from LYBankLog where startDate >= '" +
                    strStartDate + "' "
                    + " and StartDate <='" + strEndDate + "'  and LogType = '" +
                    strFlag + "' and ReturnDate is not null )";
            System.out.println("总的查询语句是" + MainB_sql);
            ExeSQL MB_exesql = new ExeSQL();
            SSRS MB_ssrs = MB_exesql.execSQL(MainB_sql);
            System.out.println("count for MB_ssrs is " + M_ssrs.getMaxRow());
            if (MB_ssrs.getMaxRow() > 0)
            {
                for (int MB_count = 1; MB_count <= MB_ssrs.getMaxRow();
                                    MB_count++)
                {
                    String b_BankSuccFlag;
                    //判断是成功还是失败
                    if (M_ssrs.GetText(MB_count, 5).trim() == null ||
                        M_ssrs.GetText(MB_count, 5).trim().equals(""))
                    {
                        b_BankSuccFlag = "1";
                    }
                    else
                    {
                        b_BankSuccFlag = M_ssrs.GetText(MB_count, 5).trim(); //银行成功标记
                    }
                    //需要进行确认
                    String hq_flag_b = mPremBankPubFun.getBankSuccFlag(MB_ssrs.
                            GetText(MB_count, 6).trim());
                    boolean jy_flag_b = mPremBankPubFun.verifyBankSuccFlag(
                            hq_flag_b, b_BankSuccFlag);

                    System.out.println("校验后的成功标志是" + jy_flag_b);
                    if (jy_flag_b)
                    {
                        continue;
                    }
                    String SB_sql = mPremBankPubFun.getPolType(M_ssrs.GetText(
                            MB_count, 1), strAgentState);
                    ExeSQL SB_exesql = new ExeSQL();
                    SSRS SB_ssrs = SB_exesql.execSQL(SB_sql);
                    System.out.println("SB_ssrs" + SB_ssrs.getMaxRow());
                    // PolNo ,MainPolNo,AgentCode,AgentGroup,AppntNo
                    if (SB_ssrs.getMaxRow() <= 0)
                    {
                        continue;
                    }
                    else
                    {
                        //查询出其他的信息
                        String b_errorReason = mPremBankPubFun.getErrInfo(
                                "bankerror", MB_ssrs.GetText(MB_count, 6).trim(),
                                b_BankSuccFlag); //调用错误信息的方法
                        LDBankSchema tLDBankSchema = new LDBankSchema();
                        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                        tLAAgentSchema = mPremBankPubFun.getAgentInfo(SB_ssrs.
                                GetText(1, 3)); //查询代理人姓名
                        mAgentGroup = mPremBankPubFun.getAgentGroup(SB_ssrs.
                                GetText(1, 4)); //查询代理人组别
                        String strHZ_count = mPremBankPubFun.getLJSPay(MB_ssrs.
                                GetText(MB_count, 2)); //查询划账次数
                        LCAppntIndSchema pLCAppntIndSchema = new
                                LCAppntIndSchema();
                        pLCAppntIndSchema = mPremBankPubFun.getAppntInfo(
                                SB_ssrs.GetText(1, 5)); //查询投保人信息
                        tLDBankSchema.setSchema(mPremBankPubFun.getBankInfo(
                                MB_ssrs.GetText(MB_count, 6))); //调用得到银行信息的函数

                        //对其进行赋值
                        String[] cols = new String[13];
                        cols[0] = mAgentGroup; //业务员组别
                        cols[1] = tLAAgentSchema.getName(); //业务员姓名
                        cols[2] = pLCAppntIndSchema.getName(); //投保人姓名
                        cols[3] = SB_ssrs.GetText(1, 2); //主险保单号码
                        cols[4] = tLDBankSchema.getBankName(); //开户行
                        cols[5] = MB_ssrs.GetText(MB_count, 7); //账号
                        cols[6] = MB_ssrs.GetText(MB_count, 4); //应缴日期
                        cols[7] = MB_ssrs.GetText(MB_count, 3); //应缴金额
                        cols[8] = strHZ_count; //划账次数
                        cols[9] = b_errorReason; //失败原因
                        cols[10] = tLAAgentSchema.getMobile();
                        cols[11] = pLCAppntIndSchema.getPostalAddress();
                        cols[12] = pLCAppntIndSchema.getPhone();

                        alistTable.add(cols);
                        mMoney = mMoney + Double.parseDouble(cols[7]);
                        mCount = mCount + 1;
                    }
                }
            }

            //在返回盘中查询
        }
        //全部放在最外层的循环
        if (mCount == 0)
        {
            mPremBankPubFun.buildError("submitData", "没有要进行打印的信息");
            return false;
        }
        System.out.println("开始执行最外部分的循环");
        String[] b_col = new String[13];
        xmlexport.addDisplayControl("displayinfo");
        xmlexport.addListTable(alistTable, b_col);
        texttag.add("AgentState", mAgentState);
        texttag.add("StartDate", strStartDate);
        texttag.add("EndDate", strEndDate);
        texttag.add("ComCode", strComCode);
        texttag.add("SumMoney", mMoney);
        texttag.add("SumCount", mCount);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("e:\\", "NewXQPremBankErrBL"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        strStartDate = (String) mInputData.get(0);
        strEndDate = (String) mInputData.get(1);
        strAgentState = (String) mInputData.get(2);
        strPremType = (String) mInputData.get(3); //首期还是续期
        strFlag = (String) mInputData.get(4); //F or S
        strComCode = (String) mInputData.get(5);
        strStation = (String) mInputData.get(6);
        System.out.println("strComCode" + strComCode);
        if (strAgentState.equals("1"))
        {
            mAgentState = "在职单";
        }
        else
        {
            mAgentState = "孤儿单";
        }

        strStartDate = strStartDate.trim();
        strEndDate = strEndDate.trim();
        strAgentState = strAgentState.trim();
        strPremType = strPremType.trim();
        strFlag = strFlag.trim();
        return true;
    }

    public static void main(String[] args)
    {
    }
}
