package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPEdorMainSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class EdorFeeF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //private VData mInputData = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();
    private String GetNoticeNo;
    private String AppntNo;
    private String Agentcode;
    private String SumDuePayMoney;
    public EdorFeeF1PUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        EdorFeeF1PBL tEdorFeeF1PBL = new EdorFeeF1PBL();
        System.out.println("Start EdorFeeF1P UI Submit ...");

        if (!tEdorFeeF1PBL.submitData(cInputData, cOperate))
        {
            if (tEdorFeeF1PBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tEdorFeeF1PBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "EdorFeeF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tEdorFeeF1PBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {

        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPEdorMainSchema.setSchema((LPEdorMainSchema) cInputData.
                                    getObjectByObjectName("LPEdorMainSchema", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
        tLPEdorMainSchema.setEdorNo("86110020030420000096");
        String GetNoticeNo = "86110020030310000067";
        String AppntNo = "0000000214";
        String SumDuePayMoney = "50.0";
        GlobalInput tG = new GlobalInput();
        VData tVData = new VData();
        tVData.addElement(tLPEdorMainSchema);
        tVData.addElement(GetNoticeNo);
        tVData.addElement(AppntNo);
        tVData.addElement(SumDuePayMoney);
        tVData.addElement(tG);
        EdorFeeF1PUI tEdorFeeF1PUI = new EdorFeeF1PUI();
        tEdorFeeF1PUI.submitData(tVData, "PRINT");
    }
}
