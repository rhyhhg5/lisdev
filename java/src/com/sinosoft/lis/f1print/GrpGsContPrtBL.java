package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpGsContPrtBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private String mflag = null;

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    public GrpGsContPrtBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            mResult.clear();

            // 准备所有要打印的数据
            if (!getPrintData())
            {
                return false;
            }

            // 处理保单打印信息
            MMap tTmpMap = null;
            String tPrtNo = mGrpContInfo.getPrtNo();
            tTmpMap = chgGrpContPrintInfo(tPrtNo);
            if (tTmpMap == null)
            {
                return false;
            }

            if (!submit(tTmpMap))
            {
                return false;
            }
            tTmpMap = null;
            // --------------------

            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    private String getDate(String cDate)
    {
        Date tDate = new FDate().getDate(cDate);
        if (tDate == null)
        {
            System.out.println("[" + cDate + "]日期转换失败");
            return cDate;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(tDate);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0));
        if (!dealPrintManager())
        {
            return false;
        }

        mflag = mOperate;

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CCSFirstPayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport();

        xmlExport.createDocument("GrpGsContPrint", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 处理团体分单基本信息。
        if (!dealGrpSubContBaseInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 处理保障计划
        if (!getInsuredList(xmlExport))
        {
            return false;
        }

        // ------------------------------

        // 结束节点
        if (!setEndFlagNodeByName(xmlExport, "end"))
        {
            return false;
        }
        // ------------------------------

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);

        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }

    /**
     * 获取团单信息
     * @param cGrpContNo
     * @return
     */
    private boolean loadGrpContInfo(String cGrpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(cGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            return false;
        }
        mGrpContInfo = tLCGrpContDB.getSchema();
        return true;
    }

    /**
     * 产生PDF打印相关基本信息。
     * @param cTextTag
     * @return
     */
    private boolean dealPdfBaseInfo(XmlExport cXmlExport)
    {
        TextTag tTmpTextTag = new TextTag();

        // 单证类型标志。
        tTmpTextTag.add("JetFormType", "GS002");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTmpTextTag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if (mflag.equals("batch"))
        {
            tTmpTextTag.add("previewflag", "0");
        }
        else
        {
            tTmpTextTag.add("previewflag", "1");
        }
        // -------------------------------

        cXmlExport.addTextTag(tTmpTextTag);

        return true;
    }

    /**
     * 处理团体保单层信息。
     * @param cTextTag
     * @return
     */
    private boolean dealGrpSubContBaseInfo(XmlExport cXmlExport)
    {
        TextTag tTmpTextTag = new TextTag();

        tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
        tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
        tTmpTextTag.add("GrpName", mGrpContInfo.getGrpName());
        tTmpTextTag.add("AppntNo", mGrpContInfo.getAppntNo());
        tTmpTextTag.add("CValidate", getDate(mGrpContInfo.getCValiDate()));
        tTmpTextTag.add("CInValidate", getDate(mGrpContInfo.getCInValiDate()));

        tTmpTextTag.add("ManageCom", mGrpContInfo.getManageCom());
        tTmpTextTag.add("ManageComName", chgGrpManageCom(mGrpContInfo.getManageCom()));
        tTmpTextTag.add("InsuredPeoples", mGrpContInfo.getPeoples2());

        // 被保人当期保费
        String tSumPrem = format(mGrpContInfo.getPrem());
        tTmpTextTag.add("SumPrem", tSumPrem);
        // --------------------

        tTmpTextTag.add("Remark", StrTool.cTrim(mGrpContInfo.getRemark()).equals("") ? "无" : mGrpContInfo.getRemark());

        tTmpTextTag.add("SignCom", getSignComName(mGrpContInfo.getSignCom()));
        tTmpTextTag.add("SignDate", mGrpContInfo.getSignDate());

        String tAgentCode = mGrpContInfo.getAgentCode();
        String tTmpSql = " select laa.Name, labg.BranchAttr, labg.Name " + " from LAAgent laa "
                + " inner join LABranchGroup labg on labg.AgentGroup = laa.AgentGroup " + " where laa.AgentCode = '"
                + tAgentCode + "' ";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tTmpSql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0)
        {
            buildError("dealGrpSubContBaseInfo", "未找到业务员相关信息。");
            return false;
        }
        String tAgentName = tSSRS.GetText(1, 1);
        String tBranchAttr = tSSRS.GetText(1, 2);
        String tBranchAttrName = tSSRS.GetText(1, 3);
        ExeSQL tExeSQL=new ExeSQL();
        tTmpTextTag.add("SaleChnl", mGrpContInfo.getSaleChnl());
        tTmpTextTag.add("AgentCom", mGrpContInfo.getAgentCom());
        tTmpTextTag.add("AgentCode", tExeSQL.getOneValue("select getUniteCode('"+tAgentCode+"') from dual")); //modify by zjd 业务员编码显示集团统一工号
        tTmpTextTag.add("AgentName", tAgentName);
        tTmpTextTag.add("AgentGroup", mGrpContInfo.getAgentGroup());
        tTmpTextTag.add("BranchAttr", tBranchAttr);
        tTmpTextTag.add("BranchAttrName", tBranchAttrName);

        tTmpTextTag.add("PayIntv", chgGrpPayIntv(String.valueOf(mGrpContInfo.getPayIntv())));
        tTmpTextTag.add("Operator", mGrpContInfo.getOperator());

        // 获取缴费期间，但该方式所取数据不准。
        //        String tPayPeriod = getPayPeriod();
        //        tTmpTextTag.add("PayPeriod", tPayPeriod);
        // ---------------------

        String tStrSql = "select Address, ZipCode, ServicePostAddress, ServicePostZipCode, ServicePhone, LetterServiceName, LetterServicePostAddress, LetterServicePostZipCode from ldcom where comcode = '"
                + mGrpContInfo.getManageCom() + "' ";
        tSSRS = new ExeSQL().execSQL(tStrSql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0)
        {
            buildError("dealGrpSubContBaseInfo", "未找到管理机构信息相关信息。");
            return false;
        }
        String tAddress = tSSRS.GetText(1, 1);
        String tZipCode = tSSRS.GetText(1, 2);
        String tServicePostAddress = tSSRS.GetText(1, 3);
        String tServicePostZipCode = tSSRS.GetText(1, 4);
        String tServicePhone = tSSRS.GetText(1, 5);
        String tLetterServiceName = tSSRS.GetText(1, 6);
        String tLetterServicePostAddress = tSSRS.GetText(1, 7);
        String tLetterServicePostZipCode = tSSRS.GetText(1, 8);
        tTmpTextTag.add("Address", tAddress);
        tTmpTextTag.add("ZipCode", tZipCode);
        tTmpTextTag.add("ServicePostAddress", tServicePostAddress);
        tTmpTextTag.add("ServicePostZipCode", tServicePostZipCode);
        tTmpTextTag.add("ServicePhone", tServicePhone);
        tTmpTextTag.add("LetterServiceName", tLetterServiceName);
        tTmpTextTag.add("LetterServicePostAddress", tLetterServicePostAddress);
        tTmpTextTag.add("LetterServicePostZipCode", tLetterServicePostZipCode);
        tTmpTextTag.add("WebUrl", "");

        tTmpTextTag.add("Today", getDate(PubFun.getCurrentDate()));

        cXmlExport.addTextTag(tTmpTextTag);

        return true;
    }

    private boolean getInsuredList(XmlExport cXmlExport)
    {
        TextTag tTmpTextTag = new TextTag();

        StringBuffer tSBql = new StringBuffer(256);
        tSBql.append("select ContPlanCode,ContPlanName from LCContPlan where GrpContNo = '");
        tSBql.append(this.mGrpContInfo.getGrpContNo());
        tSBql.append("' and ContPlanCode != '00' and ContPlanCode != '11'");
        tSBql.append(" order by ContPlanCode ");

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());

        StringBuffer tContPlanAabstractInfo = new StringBuffer();
        //循环获取计划信息
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            // 保障计划概述
            tContPlanAabstractInfo.append("计划 ");
            tContPlanAabstractInfo.append(tSSRS.GetText(i, 1));
            tContPlanAabstractInfo.append(".");
            tContPlanAabstractInfo.append(tSSRS.GetText(i, 2));
            tContPlanAabstractInfo.append(" ");
            // ----------------------------------
        }
        tTmpTextTag.add("ContPlanAabstractInfo", tContPlanAabstractInfo.toString());
        cXmlExport.addTextTag(tTmpTextTag);

        String[] tPolListInfoTitle = new String[13];
        tPolListInfoTitle[0] = "ContPlanCode";
        tPolListInfoTitle[1] = "InsuredCount";
        tPolListInfoTitle[2] = "RiskCode";
        tPolListInfoTitle[3] = "RiskName";
        tPolListInfoTitle[4] = "DutyCode";
        tPolListInfoTitle[5] = "DutyName";
        tPolListInfoTitle[6] = "Amnt/Mult";
        tPolListInfoTitle[7] = "GetLimit";
        tPolListInfoTitle[8] = "GetRate";
        tPolListInfoTitle[9] = "Prem";
        tPolListInfoTitle[10] = "Mult";
        tPolListInfoTitle[11] = "Amnt";
        tPolListInfoTitle[12] = "DutyAmnt";

        ListTable tLTLCPol = new ListTable();
        tLTLCPol.setName("LCPol");

        //取险种相关责任
        StringBuffer tSQl1 = new StringBuffer(256);
        tSQl1
                .append(" select distinct ContPlanCode,(select case  when count(1)=0 then (select peoples2 from LCContPlan where grpcontno=z.grpcontno and ContPlanCode = z.ContPlanCode ) else count(1) end InsuredCount from LCInsured where GrpContNo = z.GrpContNo ");
        tSQl1
                .append(" and ContPlanCode = z.ContPlanCode and name not like'公共账户%' and name not like'无名单%' ),z.RiskCode,(select RiskName from LMRisk where RiskCode =z.riskcode), ");
        tSQl1.append(" (select trim(z.riskcode)||trim(outdutycode)  from LMduty where dutycode =z.dutycode), ");
        tSQl1.append(" (select outdutyname from LMduty where dutycode =z.dutycode), ");
        tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
        tSQl1
                .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
        tSQl1.append(" in ('Mult') and codetype='printflag' and code=z.RiskCode), ");
        tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
        tSQl1
                .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
        tSQl1.append(" in ('Amnt') and codetype='printflag' and code=z.RiskCode), ");
        tSQl1.append(" (select CalFactorValue ParamValue2 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
        tSQl1
                .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetLimit'), ");
        tSQl1.append(" (select CalFactorValue ParamValue3 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
        tSQl1
                .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetRate'), ");
        tSQl1.append(" (select n ");
        tSQl1.append(" from ");
        tSQl1.append(" (select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ");
        tSQl1.append(" from lccontplan a,lcpol b ");
        tSQl1.append("  where ");
        tSQl1.append(" a.grpcontno=b.grpcontno ");
        tSQl1.append(" and b.insuredname not like ('公共账户') ");
        tSQl1.append(" and b.appflag in('1','9') group by a.contplancode,b.RiskCode,a.grpcontno)  as X ");
        tSQl1.append(" where X.contplancode=z.contplancode ");
        tSQl1.append(" and X.RiskCode=z.riskcode  and X.grpcontno=z.grpcontno ) ");
        tSQl1.append("  ,dutycode, ");
        tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
        tSQl1
                .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
        tSQl1.append(" in ('DutyAmnt') and codetype='printflag2' and code=z.RiskCode), ");
        tSQl1.append(" b.GrpProposalNo ");
        tSQl1.append(" ,(Select Othersign from LDCODE  where codetype='printflag' and code = b.RiskCode) ");
        tSQl1.append(" from LCContPlanDutyParam z, LCGrpPol b ");
        tSQl1.append("  where z.GrpPolNo = b.GrpPolNo");
        tSQl1.append("  and z.GrpContNo = '" + mGrpContInfo.getGrpContNo() + "' and ContPlanCode != '00'");
        tSQl1.append(" and ContPlanCode != '11' order by ContPlanCode,b.GrpProposalNo,dutycode with ur ");
        System.out.println(tSQl1.toString());

        StringBuffer tsql = new StringBuffer();
        tsql.append(" select 'ALL',(select sum(z.peoples3)  from lccontplanrisk m,lccontplan z ");
        tsql
                .append(" where m.contplancode not in ('00','11')  and m.grpcontno=z.grpcontno and   m.contplancode=z.contplancode and ");
        tsql.append(" m.riskcode=a.riskcode and m.riskcode=c.riskcode and a.grpcontno=m.grpcontno ");
        tsql
                .append(" and a.grpcontno=z.grpcontno),a.riskcode,c.riskname,'',(case (Select Othersign from LDCODE  where codetype='printflag' and code = c.RiskCode) when '1' then '特需综合保障责任' else '团体帐户' end),'','','','',(select sum(prem) from lcpol where ");
        tsql.append(" lcpol.poltypeflag='2' and lcpol.insuredname like '公共账户' and lcpol.prtno=a.prtno ");
        tsql.append(" and lcpol.riskcode=a.riskcode) from lcgrppol a,lmrisk c where a.grpcontNo='"
                + mGrpContInfo.getGrpContNo() + "' ");
        tsql.append(" and  a.riskcode=c.riskcode and c.riskcode in(select riskcode from lmriskapp where ");
        tsql.append(" risktype9='4') order by a.riskcode ");
        System.out.println(tsql.toString());

        ExeSQL tExeSQL2 = new ExeSQL();
        SSRS tSSRS1 = tExeSQL2.execSQL(tSQl1.toString());
        SSRS tSSRS2 = tExeSQL2.execSQL(tsql.toString());
        if (tSSRS2.getMaxRow() > 0)
        {
            for (int Riskcount = 0; Riskcount < PubRiskcode.length; Riskcount++)
            {
                PubRiskcode[Riskcount] = "";
            }
        }

        int begincount = 0;
        String[] onePolInfo = null;

        for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
        {
            // 判断是否存在特殊打印需求。如170301，不打印责任。
            if ("1".equals(tSSRS1.GetText(j, 15)))
                continue;
            // --------------------------------------------

            if (j > 1 && tSSRS1.GetText(j, 1).equals(tSSRS1.GetText(j - 1, 1))
                    && tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j - 1, 3)))
            {
                begincount = j;
            }

            onePolInfo = new String[13];
            onePolInfo[0] = tSSRS1.GetText(j, 1);
            onePolInfo[1] = tSSRS1.GetText(j, 2);
            onePolInfo[2] = tSSRS1.GetText(j, 3);
            onePolInfo[3] = tSSRS1.GetText(j, 4);
            onePolInfo[4] = tSSRS1.GetText(j, 5);
            onePolInfo[5] = tSSRS1.GetText(j, 6);

            // 处理“保额/限额/档次”节点
            String tAmntOrMult = "";

            if (tSSRS1.GetText(j, 13).equals(""))
            {
                if (tSSRS1.GetText(j, 7).equals(""))
                {
                    tAmntOrMult = tSSRS1.GetText(j, 8);
                }
                else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 8).equals("")))
                {
                    tAmntOrMult = tSSRS1.GetText(j, 8).replace('D', 'X');
                }
                else
                {

                    tAmntOrMult = tSSRS1.GetText(j, 7);
                }
            }
            else if (!tSSRS1.GetText(j, 13).equals(""))
            {
                if (tSSRS1.GetText(j, 7).equals(""))
                {
                    tAmntOrMult = tSSRS1.GetText(j, 13);
                }
                else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 13).equals("")))
                {
                    tAmntOrMult = tSSRS1.GetText(j, 13).replace('D', 'B');
                }
                else
                {
                    tAmntOrMult = tSSRS1.GetText(j, 7);
                }
            }

            onePolInfo[6] = tAmntOrMult;

            if (tAmntOrMult != null && !tAmntOrMult.equals(""))
            {

                String tTmpAmntOrMult = tAmntOrMult.substring(0, tAmntOrMult.length() - 1);
                String tLastFlagOfAmnt = tAmntOrMult.substring(tAmntOrMult.length() - 1, tAmntOrMult.length());

                if ("X".equals(tLastFlagOfAmnt))
                {
                    onePolInfo[12] = tTmpAmntOrMult;
                }
                else if ("B".equals(tLastFlagOfAmnt))
                {
                    onePolInfo[11] = tTmpAmntOrMult;
                }
                else if ("D".equals(tLastFlagOfAmnt))
                {
                    onePolInfo[10] = tTmpAmntOrMult;
                }
            }
            // ------------------------------------------

            onePolInfo[7] = tSSRS1.GetText(j, 9);

            if (!StrTool.cTrim(tSSRS1.GetText(j, 10)).equals(""))
            {
                onePolInfo[8] = format(Double.parseDouble(tSSRS1.GetText(j, 10)));
            }
            else
            {
                onePolInfo[8] = tSSRS1.GetText(j, 10);
            }
            if (j == begincount)
            {
                onePolInfo[9] = "";
            }
            else
            {
                String prem = setPrem(mGrpContInfo.getGrpContNo(), tSSRS1.GetText(j, 1), tSSRS1.GetText(j, 3));
                if (prem == null)
                {
                    return false;
                }

                onePolInfo[9] = prem;
            }
            for (int i = 6; i < onePolInfo.length; i++)
            {
                if (onePolInfo[i] == null || onePolInfo[i].equals(""))
                {
                    onePolInfo[i] = "--";
                }
            }
            tLTLCPol.add(onePolInfo);
        }

        onePolInfo = null;
        //公共帐户显示,为了避免重复显示公共账户增加PubRiskcode数组缓存已经打印过公共账户的险种代码

        if (tSSRS2.getMaxRow() > 0)
        {

            for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
            {
                for (int tIndex = 1; tIndex <= tSSRS2.getMaxRow(); tIndex++)
                {
                    System.out.println("+++++");
                    for (int Riskcount1 = 0; Riskcount1 < PubRiskcode.length; Riskcount1++)
                    {
                        if (tSSRS2.GetText(tIndex, 3).equals(PubRiskcode[Riskcount1]))
                        {
                            PubRiskcodePrintFlag = "1";
                        }
                    }

                    onePolInfo = new String[13];

                    System.out.println("+++++");
                    if (tSSRS1.GetText(j, 3).equals(tSSRS2.GetText(tIndex, 3)) & PubRiskcodePrintFlag.equals("0"))
                    {
                        if (j < tSSRS1.getMaxRow() && !(tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j + 1, 3))))
                        {
                            onePolInfo[0] = tSSRS2.GetText(tIndex, 1);
                            onePolInfo[1] = tSSRS2.GetText(tIndex, 2);
                            onePolInfo[2] = tSSRS2.GetText(tIndex, 3);
                            onePolInfo[3] = tSSRS2.GetText(tIndex, 4);
                            onePolInfo[4] = tSSRS2.GetText(tIndex, 5);
                            onePolInfo[5] = tSSRS2.GetText(tIndex, 6);
                            onePolInfo[6] = tSSRS2.GetText(tIndex, 8);
                            onePolInfo[7] = tSSRS2.GetText(tIndex, 9);
                            onePolInfo[8] = tSSRS2.GetText(tIndex, 10);

                            if (tSSRS2.GetText(tIndex, 11).equals(null) || tSSRS2.GetText(tIndex, 11).equals("null")
                                    || tSSRS2.GetText(tIndex, 11).equals(""))
                            {
                                onePolInfo[9] = "";
                            }
                            else
                            {
                                onePolInfo[9] = format(Double.parseDouble(tSSRS2.GetText(tIndex, 11)));
                            }
                            PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                            tLTLCPol.add(onePolInfo);

                        }
                        else if (j == tSSRS1.getMaxRow())
                        {
                            onePolInfo[0] = tSSRS2.GetText(tIndex, 1);
                            onePolInfo[1] = tSSRS2.GetText(tIndex, 2);
                            onePolInfo[2] = tSSRS2.GetText(tIndex, 3);
                            onePolInfo[3] = tSSRS2.GetText(tIndex, 4);
                            onePolInfo[4] = tSSRS2.GetText(tIndex, 5);
                            onePolInfo[5] = tSSRS2.GetText(tIndex, 6);
                            onePolInfo[6] = tSSRS2.GetText(tIndex, 8);
                            onePolInfo[7] = tSSRS2.GetText(tIndex, 9);
                            onePolInfo[8] = tSSRS2.GetText(tIndex, 10);

                            if (tSSRS2.GetText(tIndex, 11).equals(null) || tSSRS2.GetText(tIndex, 11).equals("null")
                                    || tSSRS2.GetText(tIndex, 11).equals(""))
                            {
                                onePolInfo[9] = "";
                            }
                            else
                            {
                                onePolInfo[9] = format(Double.parseDouble(tSSRS2.GetText(tIndex, 11)));
                            }

                            PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                            tLTLCPol.add(onePolInfo);
                        }
                    }
                }
            }
        }

        cXmlExport.addListTable(tLTLCPol, tPolListInfoTitle);

        // 结束节点
        if (!setEndFlagNodeByName(cXmlExport, "LCPolEnd"))
        {
            return false;
        }
        // ------------------------------

        return true;
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private boolean dealPrintManager()
    {
        String tGrpContNo = mLOPRTManagerSchema.getOtherNo();

        if (!loadGrpContInfo(tGrpContNo))
        {
            buildError("dealPrintManager", "获取团单数据失败！");
            return false;
        }

        String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
        if (tStrPrtSeq == null || tStrPrtSeq.equals(""))
        {
            buildError("dealPrintManager", "生成打印流水号失败。");
            return false;
        }

        mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

        mLOPRTManagerSchema.setOtherNoType("16");

        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");

        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        return true;
    }

    private boolean setEndFlagNodeByName(XmlExport cXmlExport, String cEndName)
    {
        String[] tEndFlag = new String[0];

        ListTable tLTEndNode = new ListTable();
        tLTEndNode.setName(cEndName);

        cXmlExport.addListTable(tLTEndNode, tEndFlag);

        return true;
    }

    private String setPrem(String cGrpContNo, String cContPlanCode, String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "没有查询到险种定义信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("0".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            return "";
        }

        String premSumPart = "  and RiskCode = '" + cRiskCode + "' ";
        if ("1".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            premSumPart = " and (X.RiskCode='" + cRiskCode + "' "
                    + "   or X.RiskCode in(select RiskCode from LDRiskParamPrint "
                    + "              where ParamType1 = 'sumprem' " + "                 and ParamValue1='" + cRiskCode
                    + "')) ";
        }

        //查询非绑定附险种的保费，包括绑定主险的附险保费
        ExeSQL tExeSQL = new ExeSQL();

        StringBuffer sql2 = new StringBuffer();
        sql2.append(" select nvl(sum(n), 0) from ").append("(").append(
                "    select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ").append(
                "    from lccontplan a,lcpol b ").append("    where  a.grpcontno=b.grpcontno ").append(
                "      and b.insuredname not like ('公共账户') ").append("      and b.appflag in('1','9') ").append(
                "    group by a.contplancode,b.RiskCode,a.grpcontno ").append(" )  as X ").append(
                " where X.contplancode='" + cContPlanCode + "' ").append(premSumPart).append(
                "   and X.grpcontno='" + cGrpContNo + "' ");
        String prem = tExeSQL.getOneValue(sql2.toString());

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询邦定主险保费出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("".equals(prem) || "null".equals(prem))
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询主险保费失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return format(Double.parseDouble(prem));
    }

    /**
     * 获取缴费频次代码对应中文含义。
     * @param cPayIntv
     * @return
     */
    private String chgGrpPayIntv(String cPayIntv)
    {
        String tStrSql = "select CodeName from LDCode where CodeType = 'payintv' " + " and Code = '" + cPayIntv + "'";
        return new ExeSQL().getOneValue(tStrSql);
    }

    /**
     * 获取机构代码代码对应中文含义。
     * @param cPayIntv
     * @return
     */
    private String chgGrpManageCom(String cManageCom)
    {
        String tStrSql = "select Name from LDCom where ComCode = '" + cManageCom.substring(0,4) + "'";
        return new ExeSQL().getOneValue(tStrSql);
    }

    private String getSignComName(String cManageCom)
    {
        String tStrSql = "select letterservicename from ldcom where comcode = '" + cManageCom + "'";
        return new ExeSQL().getOneValue(tStrSql);
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private MMap chgGrpContPrintInfo(String cPrtNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = "select * from LCGrpCont where PrtNo = '" + cPrtNo + "'";

        LCGrpContSchema tGrpContInfo = null;

        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() != 1)
        {
            buildError("addGetPolDate", "获取签单后保单信息失败。");
            return null;
        }
        tGrpContInfo = tGrpContInfoSet.get(1);

        String tGrpContNo = tGrpContInfo.getGrpContNo();

        String tCutDate = PubFun.getCurrentDate();
        String tCutTime = PubFun.getCurrentTime();

        int iDelayDays = 0;
        String tCustomGetPolDate = PubFun.calDate(tCutDate, iDelayDays, "D", null);

        StringBuffer tStrBSql = null;

        // 处理团单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCGrpCont set ");
        tStrBSql.append(" PrintCount = 1, ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "', ");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "', ");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        // 处理分单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCCont set ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "', ");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "', ");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        return tMMap;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }
        return true;
    }

    public CErrors getErrors()
    {
        return null;
    }
}
