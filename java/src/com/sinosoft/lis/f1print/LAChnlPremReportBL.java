/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAChnlPremReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计级别
    private String mBankType2 = "";
    //统计银代机构
    private String mAgentCom = "";
    //统计业务员
    private String mAgentCode = "";
    //统计保单状态
    private String mState = "";
    //统计保单回执回销状态
    private String mGetPolState="";
    //统计险种
    private String mRiskCode = "";
    //统计起期
    private String  mStartDate= "";
    //统计止期
    private String  mEndDate = "";
    //交费年限
    private String  mPayYears = "";
    private String  mPayYear = "";
    private String  mGetState = "";
    //是否为交叉销售
    private String  msaleFlag="";
    //投保人联系方式
    private String  mphoneNumber="";
    //投保人年龄
    private String  mage="";
    //是否自助终端出单
    private String  moutFlag="";
    //需要调用的模版
    private String mFlag = "";
    //转化交费方式
    private int mPayTime;
    //转换交费年限
    private int mpayyears;
    //文件暂存路径
    private String mfilePathAndName;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    private String mSQL=null;
    private ExeSQL mExeSQL=new ExeSQL();
    private SSRS mSSRS=new SSRS();
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public LAChnlPremReportBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate){
    	
        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("dealData:" + mOperate);
        System.out.println("开始处理数据，渠道保费明细报表");
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChnlPremReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
              return false;
          }
            return true;
           }catch(Exception ex)
            {
                buildError("queryData", "LABL发生错误，准备数据时出错！");
                return false;
            }
    }

   /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData){
     this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
     mManageCom = (String)cInputData.get(0);
     System.out.println("统计层级mManageCom:"+mManageCom);
     managecom=this.mGlobalInput.ManageCom;
     System.out.println("管理机构为:"+managecom);
     mBankType2 = (String)cInputData.get(1);
     mAgentCom = (String)cInputData.get(2);
     mRiskCode = (String)cInputData.get(3);
     mAgentCode = (String)cInputData.get(4);
     mState = (String)cInputData.get(5);
     mStartDate = (String)cInputData.get(6);
     mEndDate = (String)cInputData.get(7);
     mPayYears = (String)cInputData.get(8);//缴费方式
     mfilePathAndName=(String)cInputData.get(10);
     mGetPolState=(String)cInputData.get(11);
     mPayYear=(String)cInputData.get(12);
     mGetState=(String)cInputData.get(13);
     msaleFlag=(String)cInputData.get(14);
     mphoneNumber=(String)cInputData.get(15);
     mage=(String)cInputData.get(16);
     moutFlag=(String)cInputData.get(17);
     
     System.out.println("BL层接收的传入界面参数为"+mBankType2+"/"+mAgentCom+"/"+mRiskCode+"/"
    		 +mAgentCode+"/"+mState+"/"+mStartDate+"/"+mEndDate
    		 +"/"+mPayYears+"/"+mfilePathAndName+"/msaleFlag"+msaleFlag+"/"+mphoneNumber+"/"+mage+"/"+moutFlag);

    System.out.println("mpayyears="+mpayyears);
     return true;
 }




    private boolean getDataList()
    {
      //System.out.println("查询数据开始>>>>>>>>>>>");
      //新单用签单日期signdate，撤单用撤单日期tmakedate
         String manageSQL = "select * from lacommision where branchtype='3' and branchtype2='01' and managecom like '"+mManageCom+"%'";
          //如果统计级别不为空，加上AgentCom查询条件
         if(this.mBankType2!=null&&!this.mBankType2.equals(""))
          {
          manageSQL+=" and agentcom  in (select agentcom from lacom where banktype>='"+mBankType2+"' and managecom like '"+mManageCom+"%'   and  branchtype='3' and branchtype2='01')";
          }
          if(this.mAgentCom!=null&&!this.mAgentCom.equals(""))
          {
           manageSQL+=" and agentcom like '"+mAgentCom+"%'";
          }
          if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
          {
           manageSQL+=" and riskcode ='"+mRiskCode+"'";
          }
          if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
          {
           manageSQL+=" and agentcode ='"+mAgentCode+"'";
          }
          if(this.mState!=null&&!this.mState.equals(""))
          {
              if(mState.equals("0"))
              {
                manageSQL+=" and contno in (select distinct contno from  lccont where  appflag='1'  and stateflag='1' and salechnl in('04','13') and  managecom like '"+mManageCom+"%' )";
              }
              if(mState.equals("1"))
              {
                manageSQL+=" and  contno in (select distinct contno from lccont  where appflag='1'  and stateflag='3'  and salechnl  in('04','13') and  managecom like '"+mManageCom+"%'  union select distinct contno from lbcont  where  stateflag='3'  and salechnl in('04','13') and  managecom like '"+mManageCom+"%') ";
              }
          }
          if(this.mGetPolState!=null&&!this.mGetPolState.equals(""))
          {
              if(mGetPolState.equals("0"))
              {
                manageSQL+=" and customgetpoldate is not null ";
              }
              if(mGetPolState.equals("1"))
              {
                manageSQL+=" and customgetpoldate is null ";
              }
          }
          if(this.mGetState!=null&&!this.mGetState.equals(""))
          {
        	  if(this.mGetState.equals("0")){
        		  manageSQL+=" and payyear=0 and renewcount=0 "; 
        	  }else if(this.mGetState.equals("1")){
        		  manageSQL+=" and payyear>0 and renewcount=0 ";
        	  }else{
        		  manageSQL+=" and renewcount>0 ";
        	  }
          }
          if(this.mPayYear!=null&&!this.mPayYear.equals(""))
          {
        	  manageSQL+=" and ((payyear+1)="+Integer.parseInt(mPayYear)
        	  	+" or (renewcount+1)="+Integer.parseInt(mPayYear)+")";
          }
          if(this.mStartDate!=null&&!this.mStartDate.equals(""))
          {
           manageSQL+=" and signdate >='"+mStartDate+"'  and transtype='ZC' ";
          }
          if(this.mEndDate!=null&&!this.mEndDate.equals(""))
          {
           manageSQL+=" and signdate <='"+mEndDate+"' and transtype='ZC'  ";
          }
          if(this.mPayYears!=null&&!this.mPayYears.equals(""))
         {
        	  if(this.mPayYears.equals("0")){
        		  manageSQL+=" and payintv =0 ";
        	  }else{
        		  manageSQL+=" and payintv <>0 ";
        	  }
         }
          //是否为交叉销售判断条件
          if(this.msaleFlag!=null&&!this.msaleFlag.equals(""))
          {
        	  if(this.msaleFlag.equals("0"))
        	  {
        		 // manageSQL+=" and contno in (select contno from lccont where crs_salechnl is null )";
        		  manageSQL+=" and exists (select 1 from lccont where crs_salechnl is null and contno =lacommision.contno )";
        	  }
        	  if(this.msaleFlag.equals("1"))
        	  {
        		  //manageSQL+=" and contno in (select contno from lccont where crs_salechnl is not null )";
        		  manageSQL+=" and exists (select 1 from lccont where crs_salechnl is not null and contno =lacommision.contno )";
        	  } 	  
          }
          //投保人联系方式
          if(this.mphoneNumber!=null&&!this.mphoneNumber.equals(""))
          {
           manageSQL+=" and AppntNo='"+mphoneNumber+"'  ";
          }
          //投保人年龄
          if(this.mage!=null&&!this.mage.equals(""))
          {
           manageSQL+=" and exists (select 1 from lccont where timestampdiff (256, char(current date - timestamp(AppntBirthday)))='"+mage+"'  and contno =lacommision.contno )";
          }
          //是否自助终端出单
          if(this.moutFlag!=null&&!this.moutFlag.equals(""))
          {
        	  if(this.moutFlag.equals("0"))
        	  {
        		  manageSQL+=" and not exists (select 1 from lccont where CardFlag = 'd' and contno =lacommision.contno )";
        	  }
        	  if(this.moutFlag.equals("1"))
        	  {
        		  manageSQL+=" and exists (select 1 from lccont where CardFlag = 'd' and contno =lacommision.contno )";
        	  } 	  
          }
          
          
         manageSQL+="  union ";
         manageSQL += " select * from lacommision where branchtype='3' and branchtype2='01' and managecom like '"+mManageCom+"%' ";
         if(this.mBankType2!=null&&!this.mBankType2.equals(""))
         {
         manageSQL+=" and agentcom  in (select agentcom from lacom where banktype>='"+mBankType2+"'  and managecom like '"+mManageCom+"%'   and  branchtype='3' and branchtype2='01')";
         }
         if(this.mAgentCom!=null&&!this.mAgentCom.equals(""))
         {
          manageSQL+=" and agentcom like '"+mAgentCom+"%'";
         }
         if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
         {
          manageSQL+=" and riskcode ='"+mRiskCode+"'";
         }
         if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
         {
          manageSQL+=" and agentcode ='"+mAgentCode+"'";
         }
         if(this.mState!=null&&!this.mState.equals(""))
         {
             if(mState.equals("0"))
             {
               manageSQL+=" and contno in (select distinct contno from  lccont where  appflag='1'  and stateflag='1' and salechnl  in('04','13') and  managecom like '"+mManageCom+"%' )";
             }
             if(mState.equals("1"))
             {
               manageSQL+=" and  contno in (select distinct contno from lccont  where appflag='1'  and stateflag='3'  and salechnl  in('04','13') and  managecom like '"+mManageCom+"%'  union select distinct contno from lbcont  where  stateflag='3'  and salechnl in('04','13') and  managecom like '"+mManageCom+"%') ";
             }
         }
         if(this.mGetPolState!=null&&!this.mGetPolState.equals(""))
         {
             if(mGetPolState.equals("0"))
             {
               manageSQL+=" and customgetpoldate is not null ";
             }
             if(mGetPolState.equals("1"))
             {
               manageSQL+=" and customgetpoldate is null ";
             }
         }
         if(this.mGetState!=null&&!this.mGetState.equals(""))
         {
       	  if(this.mGetState.equals("0")){
       		  manageSQL+=" and payyear=0 and renewcount=0 "; 
       	  }else if(this.mGetState.equals("1")){
       		  manageSQL+=" and payyear>0 and renewcount=0 ";
       	  }else{
       		  manageSQL+=" and renewcount>0 ";
       	  }
         }
         if(this.mPayYear!=null&&!this.mPayYear.equals(""))
         {
       	  manageSQL+=" and ((payyear+1)="+Integer.parseInt(mPayYear)
       	  	+" or (renewcount+1)="+Integer.parseInt(mPayYear)+")";
         }
         if(this.mStartDate!=null&&!this.mStartDate.equals(""))
         {
          manageSQL+=" and tmakedate >='"+mStartDate+"'   and transtype='WT' ";
         }
         if(this.mEndDate!=null&&!this.mEndDate.equals(""))
         {
          manageSQL+=" and tmakedate <='"+mEndDate+"' and transtype='WT'  ";
         }
         if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
        	if(this.mPayYears.equals("0")){
       		  	manageSQL+=" and payintv =0 ";
       	  	}else{
       	  		manageSQL+=" and payintv <>0 ";
       	  	}
        }
         if(this.msaleFlag!=null&&!this.msaleFlag.equals(""))
        {
       	  if(this.msaleFlag.equals("0"))
       	  {
       		 // manageSQL+=" and contno in (select contno from lccont where crs_salechnl is null)";
       		manageSQL+=" and exists (select 1 from lccont where crs_salechnl is null and contno =lacommision.contno ) ";
       	  }
       	  if(this.msaleFlag.equals("1"))
       	  {
       		 // manageSQL+=" and contno in (select contno from lccont where crs_salechnl is not null )";
       		manageSQL+=" and exists (select 1 from lccont where crs_salechnl is not  null and contno =lacommision.contno ) ";
       	  } 	  
        }
         //投保人联系方式
         if(this.mphoneNumber!=null&&!this.mphoneNumber.equals(""))
         {
          manageSQL+=" and AppntNo='"+mphoneNumber+"'  ";
         }
         //投保人年龄
         if(this.mage!=null&&!this.mage.equals(""))
         {
          manageSQL+=" and exists (select 1 from lccont where timestampdiff(256, char(current date - timestamp(AppntBirthday)))='"+mage+"'  and contno =lacommision.contno ) ";
         }
         //是否自助终端出单
         if(this.moutFlag!=null&&!this.moutFlag.equals(""))
         {
	       	  if(this.moutFlag.equals("0"))
	       	  {
	       		  manageSQL+=" and not exists (select 1 from lccont where CardFlag = 'd' and contno =lacommision.contno ) ";
	       	  }
	       	  if(this.moutFlag.equals("1"))
	       	  {
	       		  manageSQL+=" and exists (select 1 from lccont where CardFlag = 'd' and contno =lacommision.contno ) ";
	       	  } 	  
         }
         
        manageSQL+=" order by contno with ur ";

        System.out.println(manageSQL);
        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
        tCreateCSVFile.initFile(this.mfilePathAndName); 
        
        String[][] tFirstDataList = new String[1][42];
       
        tFirstDataList[0][0]="编号";
        tFirstDataList[0][1]="机构名称";
        tFirstDataList[0][2]="渠道名称";
        tFirstDataList[0][3]="支行名称";
        tFirstDataList[0][4]="网点名称";
        tFirstDataList[0][5]="业务员代码";
        tFirstDataList[0][6]="业务员姓名";
        tFirstDataList[0][7]="业务员所属团队代码";
        tFirstDataList[0][8]="业务员所属团队名称";
        tFirstDataList[0][9]="生效日期";
        tFirstDataList[0][10]="投保日期";
        tFirstDataList[0][11]="签单日期";
        tFirstDataList[0][12]="保单号";
        tFirstDataList[0][13]="印刷号";
        tFirstDataList[0][14]="险种名称";
        tFirstDataList[0][15]="保费";
        tFirstDataList[0][16]="提奖比例";
        tFirstDataList[0][17]="提奖金额";
        tFirstDataList[0][18]="手续费比例";
        tFirstDataList[0][19]="手续费金额";
        tFirstDataList[0][20]="投保人姓名";
        tFirstDataList[0][21]="回执回销日期";
        tFirstDataList[0][22]="保费类型";
        tFirstDataList[0][23]="犹豫期撤保日期";
        tFirstDataList[0][24]="是否银代直销";
        tFirstDataList[0][25]="业管回执日期";
        tFirstDataList[0][26]="是否银保通出单";
        tFirstDataList[0][27]="续期收费时间";
        tFirstDataList[0][28]="收费性质";
        tFirstDataList[0][29]="保单年度";
        tFirstDataList[0][30]="缴费年限";
        tFirstDataList[0][31]="投保单扫描时间";
        tFirstDataList[0][32]="回执录入系统时间";
        tFirstDataList[0][33]="保险期间"; 
        tFirstDataList[0][34]="保险期间标志";
        tFirstDataList[0][35]="是否为交叉销售"; 
        tFirstDataList[0][36]="交叉销售渠道";
        tFirstDataList[0][37]="保单回访状态";
        tFirstDataList[0][38]="保单回访成功日期";
        tFirstDataList[0][39]="投保人联系方式";
        tFirstDataList[0][40]="投保人年龄";
        tFirstDataList[0][41]="是否自助终端出单";
        
        tCreateCSVFile.doWrite(tFirstDataList,41);  
        LACommisionSet tLACommisionSet = new LACommisionSet();
        /** 新增方法:
         * 游标查询方法 */
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLACommisionSet,manageSQL);
        try{
        	do {
                rswrapper.getData();
                String[][] tShowDataList = new String[tLACommisionSet.size()][42];
                //if()
                for(int i=1;i<=tLACommisionSet.size();i++)
                {
                   LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                   tLACommisionSchema = tLACommisionSet.get(i);
                   if(tLACommisionSchema.getTransState().equals("03")){
                	   tShowDataList[i-1][22]="追加保费";
                   }else if(tLACommisionSchema.getTransState().equals("00")){
                	   tShowDataList[i-1][22]="基本保费";
                   }else{
                	   tShowDataList[i-1][22]="";
                   }
                   if(tLACommisionSchema.getAgentCom()==null||tLACommisionSchema.getAgentCom().equals("")){
                     tShowDataList[i-1][24]="是";
                   }
                   else{
                      tShowDataList[i-1][24]="否";
                   }              
                 if (!queryOneDataList(i,tLACommisionSchema.getCommisionSN(),tShowDataList[i-1]))
                 {
                	 System.out.println("[" + tLACommisionSchema.getCommisionSN() +"] 数据查询失败~~~~（>_<)~~~~ ");
                	 return false;
                 }
                }
                System.out.println("...............bl java here mfilePathAndName"
             		   +mfilePathAndName);
                this.mTotalLine+=tLACommisionSet.size();
                this.mTotalPrem+=dealSum(15,tShowDataList);
                this.mTotalFYC+=dealSum(17,tShowDataList);
                this.mTotalCharge+=dealSum(19,tShowDataList);
                tCreateCSVFile.doWrite(tShowDataList,41);
        	}        	
        	while(tLACommisionSet.size() > 0);
          }
           catch(Exception exc){
        	exc.printStackTrace();
            rswrapper.close();
        	return false;
        }
           finally
           {
            rswrapper.close();
           }
           //添加一条合计行
           System.out.println("添加一条合计行");
        String[][] tLastDataList=new String[1][42]; 
        System.out.println("合计行的列数"+tLastDataList.length);
        if(!setAddRow(tLastDataList[0]))
        {
           buildError("queryData", "进行合计计算时出错！");
           return false;
        }
        System.out.println("1010101011010>>>");
        tCreateCSVFile.doWrite(tLastDataList,41);
        tCreateCSVFile.doClose();
      return true;   
 }
  private boolean queryOneDataList(int i,String pmCommisionSn,String[] pmOneDataList){
	  System.out.println("进入queryOneDataList()方法>>>>>>>>>");
	  mSQL="select (select name from ldcom where comcode=substr(a.managecom,1,4))," +
	  		"case when a.agentcom is not null then (select codename from ldcode where codetype='bankno' and code=substr(a.agentcom,1,5)) else '' end," +
	  		"value(case when a.agentcom is not null then (select name from lacom where banktype in ('01','02') and agentcom=a.agentcom " +
	  		"union select name from lacom where agentcom=(select upagentcom from lacom where banktype in ('03','04') and agentcom=a.agentcom))" +
	  		" else '' end,''),value(case when a.agentcom is not null " +
	  		"then (select name from lacom where banktype in ('03','04') and agentcom=a.agentcom) else '' end,'')," +
	  		"a.agentcode,c.name,d.branchattr,d.name,a.cvalidate," +
	  		"(select polapplydate from lccont where contno=a.contno union select polapplydate from lbcont where contno=a.contno),"+
	  		"a.signdate,a.contno,a.p14,(select riskname from lmriskapp where riskcode=a.riskcode),"+
	  		"a.transmoney,a.fycrate,a.fyc,value((select chargerate from lacharge where commisionsn=a.commisionsn),0),"+
	  		"value((select charge from lacharge where commisionsn=a.commisionsn),0),a.p11,a.customgetpoldate,"+
	  		"case when a.transstate='03' then '追加保费' else '基本保费' end,"+
	  		"(select edorvalidate from lpedoritem "+
	  		"where contno=a.contno and edortype='WT' fetch first 1 rows only),"+
	  		"case when a.agentcom is not null then '否' else '是' end,a.getpoldate," +
	  		"case when (select cardflag from lccont where contno=a.contno " +
	  		"union select cardflag from lccont where contno=a.contno)='9' then '是' else '否' end,a.tmakedate,"+
	  		"case when a.renewcount>0 then '续保' when a.renewcount=0 and a.payyear>0 then '续期' else '新单' end,"+
	  		"case when a.renewcount>0 then a.renewcount+1 else a.payyear+1 end, "+
	  		"case when a.payintv=0 then '趸缴' else char(a.payyears) end, "+
	  		"a.ScanDate,a.customgetpoldate,a.F4,a.F5, "+
	  		"case when (select crs_salechnl from Lccont where contno=a.contno) is not null then '是' else '否' end,"+
	  		"(select case crs_salechnl when '01' then '财代健' when '02' then '寿代健' else '' end from Lccont where contno=a.contno),"+
	  		"(select case Returnvisitflag when '1' then '健康件' when '4' then '书面回访件' else '' end from Returnvisittable where policyno=a.contno),"+
	  		"(select completetime from Returnvisittable where Policyno=a.contno), "+
	  		"a.AppntNo,"+
	  		"(select timestampdiff(256, char(current date - timestamp(AppntBirthday))) from lccont where contno =a.contno), "+
	  		"case when (select cardflag from lccont where contno=a.contno)='d' then '是' else '否' end "+
	  		"from lacommision a,laagent c,labranchgroup d " +
	  		"where a.agentcode=c.agentcode and c.agentgroup=d.agentgroup "
	  		+"and a.commisionsn='"+pmCommisionSn+"' with ur";
	  mSSRS=mExeSQL.execSQL(mSQL);
	  if(mSSRS.MaxRow>0){
		  pmOneDataList[0] = String.valueOf(i);
		  pmOneDataList[1] =mSSRS.GetText(1,1);
		  pmOneDataList[2] =mSSRS.GetText(1,2);
		  pmOneDataList[3] =mSSRS.GetText(1,3);
		  pmOneDataList[4] =mSSRS.GetText(1,4);
		  pmOneDataList[5] =mSSRS.GetText(1,5)+"	";
		  pmOneDataList[6] =mSSRS.GetText(1,6);
		  pmOneDataList[7] =mSSRS.GetText(1,7)+"	";
		  pmOneDataList[8] =mSSRS.GetText(1,8);
		  pmOneDataList[9] =mSSRS.GetText(1,9);
		  pmOneDataList[10] =mSSRS.GetText(1,10);
		  pmOneDataList[11] =mSSRS.GetText(1,11);
		  pmOneDataList[12] =mSSRS.GetText(1,12)+"	";
		  pmOneDataList[13] =mSSRS.GetText(1,13)+"	";
		  pmOneDataList[14] =mSSRS.GetText(1,14);
		  pmOneDataList[15] =mSSRS.GetText(1,15);
		  pmOneDataList[16] =mSSRS.GetText(1,16);
		  pmOneDataList[17] =mSSRS.GetText(1,17);
		  pmOneDataList[18] =mSSRS.GetText(1,18);
		  pmOneDataList[19] =mSSRS.GetText(1,19);
		  pmOneDataList[20] =mSSRS.GetText(1,20);
		  pmOneDataList[21] =mSSRS.GetText(1,21);
		  pmOneDataList[22] =mSSRS.GetText(1,22);
		  pmOneDataList[23] =mSSRS.GetText(1,23);
		  pmOneDataList[24] =mSSRS.GetText(1,24);
		  pmOneDataList[25] =mSSRS.GetText(1,25);
		  pmOneDataList[26] =mSSRS.GetText(1,26);
		  pmOneDataList[27] =mSSRS.GetText(1,27);
		  pmOneDataList[28] =mSSRS.GetText(1,28);
		  pmOneDataList[29] =mSSRS.GetText(1,29);
		  pmOneDataList[30] =mSSRS.GetText(1,30);
		  pmOneDataList[31] =mSSRS.GetText(1,31);
		  pmOneDataList[32] =mSSRS.GetText(1,32);	
		  pmOneDataList[33] =mSSRS.GetText(1,33);
		  pmOneDataList[34] =mSSRS.GetText(1,34);
		  pmOneDataList[35] =mSSRS.GetText(1,35);
		  pmOneDataList[36] =mSSRS.GetText(1,36);
		  pmOneDataList[37] =mSSRS.GetText(1,37);
		  pmOneDataList[38] =mSSRS.GetText(1,38);
		  pmOneDataList[39] =mSSRS.GetText(1,39);
		  pmOneDataList[40] =mSSRS.GetText(1,40);
		  pmOneDataList[41] =mSSRS.GetText(1,41);
		  
	  }else{
		  
		  return false;
	  }
	  return true;
  }
  /**
    * 查询管理机构名称
    * @param  String
    * @return String
    */
   private String getManageComName(String pmManageCom)
   {
       String tSQL = "";
       String tRtValue="";
       tSQL = "select name from ldcom where comcode='" + pmManageCom.substring(0,4) +"'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if (tExeSQL.mErrors.needDealError()) {
       this.mErrors.addOneError("管理机构不存在！");
             }
       tRtValue=tSSRS.GetText(1, 1);
       return tRtValue;
   }
    /**
   * 生效日期
   * @param  String
   * @return String
   
  private String getCvaliDate(String pmCommisionSn)
  {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select CValiDate from lacommision where CommisionSn='" + pmCommisionSn +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
       {
         tRtValue="";
       }
       else

      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
  }
*/
/**
     * 追加一条合计行
     * @return boolean
     */
    private boolean setAddRow(String[] pmOneDataList)
    {
        System.out.println("合计行的行数是："+this.mTotalLine);

        pmOneDataList[0] = "合  计";
        pmOneDataList[1] = getManageComName(mManageCom);
        pmOneDataList[2] = "";
        pmOneDataList[3] = "";
        pmOneDataList[4] = "";
        pmOneDataList[5] = "";
        pmOneDataList[6] = "";
        pmOneDataList[7] = "";
        pmOneDataList[8] = "";
        pmOneDataList[9] = "";
        pmOneDataList[10] = "";
        pmOneDataList[11] = "";
        pmOneDataList[12] = "";
        pmOneDataList[13] = "";
        pmOneDataList[14] ="";
        pmOneDataList[15] = this.mTotalPrem+"";
        pmOneDataList[16] = "";
        pmOneDataList[17] = this.mTotalFYC+"";
        pmOneDataList[18] = "";
        pmOneDataList[19] = this.mTotalCharge+"";
        pmOneDataList[20] = "";
        pmOneDataList[21] = "";
        pmOneDataList[22] = "";
        pmOneDataList[23] = "";
        pmOneDataList[24] = "";
        pmOneDataList[25] = "";
        pmOneDataList[26] = "";
        pmOneDataList[27] = "";
        pmOneDataList[28] = "";
        pmOneDataList[29] = "";
        pmOneDataList[30] = "";
        pmOneDataList[31] = "";
        pmOneDataList[32] = "";
        pmOneDataList[33] = "";
        pmOneDataList[34] = "";
        pmOneDataList[35] = "";
        pmOneDataList[36] = "";
        pmOneDataList[37] = "";
        pmOneDataList[38] = "";
        pmOneDataList[39] = "";
        pmOneDataList[40] = "";
        pmOneDataList[41] = "";
        return true;
    }

    /**
      * 对传入的数组进行求和处理
      * @param pmArrNum int
      * @return String
      */
     private double dealSum(int pmArrNum,String tDataList[][])
     {
         String tReturnValue = "";
         DecimalFormat tDF = new DecimalFormat("0.######");
         String tSQL = "select 0";

         for(int i=0;i<tDataList.length;i++)
         {
             tSQL += " + " + tDataList[i][pmArrNum];
         }

         tSQL += " + 0 from dual";

         tReturnValue = "" + tDF.format(execQuery(tSQL));

         return new Double(tReturnValue).doubleValue();
     }
    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)

    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

        }
      }
    public VData getResult()
    {
        return mResult;
    }
}
