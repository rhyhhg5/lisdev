package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.model.Workbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AppArchiveQueryListPrintBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private VData mInputData = new VData();

	private String mOperate = "";

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private String minArchiveNo;// 最小归档号
	private String maxArchiveNo;// 最大归档好

	private String mSQL = ""; // 查询语句
	private String mOutXmlPath = "";
	private String ManageCom = "";

	private SSRS mSSRS = new SSRS();
	// 需要打印的数据
	private String[][] mExcelData = null;

	public AppArchiveQueryListPrintBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return null;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}

		if (cOperate.equals("PRINT")) {
			// 准备所有要打印的数据
			if (!PgetPrintData()) {
				return null;
			}
		}
		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	public static void main(String[] args) {

		AppArchiveQueryListPrintBL tbl = new AppArchiveQueryListPrintBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2010-10-08");
		tTransferData.setNameAndValue("EndDate", "2010-10-08");
		tTransferData.setNameAndValue("ManageCom", "8644");
		tGlobalInput.ManageCom = "8644";
		tGlobalInput.Operator = "xp";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData, "PRINT");
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\contactcompare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));

			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
		} catch (Exception e) {
			this.mErrors.addOneError("数据不完整");
			return false;
		}
		mSQL = (String) mTransferData.getValueByName("SQL");
		minArchiveNo = (String) mTransferData.getValueByName("minArchiveNo");
		maxArchiveNo = (String) mTransferData.getValueByName("maxArchiveNo");
		mOutXmlPath = (String) mTransferData.getValueByName("mOutXmlPath");
		System.out.println("SQL : " + mSQL);
		this.ManageCom = mGlobalInput.ManageCom;

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "AppArchiveQueryListPrint";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean PgetPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);
		// mCreateExcelList.setData(0, 1, 3, "归档号清单",style.setFont(tf));
		// 设置表头
		// 获得EXCEL列信息
		String tSQL = mSQL;
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL);
		String[][] tTitle = {
				{ "", "", "", "", "","","","","","","","","" },
				{ "归档号清单", "", "", "", "","","","","","","","","" },
				{
						"机构："
								+ tExeSQL
										.getOneValue("select name from ldcom where comcode='"
												+ mGlobalInput.ManageCom + "' ")
								+ "     打印时间："
								+ PubFun.getCurrentDate()
								+ "     打印人："
								+ tExeSQL
										.getOneValue("select username from lduser where usercode='"
												+ mGlobalInput.Operator + "' ")
								+ "", "", "", "", "" ,"","","","","","","","" },
				{ "档案区间（" + minArchiveNo + "-" + maxArchiveNo + "）", "", "","","","","",
						"件数：" + tSSRS.MaxRow + "件", "" ,"","","",""},
				{ "档案号", "机构代码", "机构名称", "保单类型" ,"印刷号","保单号码","投保人","生效日期","终止日期","保单状态","渠道","业务员姓名","页数"} };

			//{ "档案号", "印刷号", "业务员姓名", "页数" ,"保单号码","保单类型","投保人","生效日期","保单状态","终止日期","机构代码","机构名称","渠道"} };
		
		
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5 ,6,7,8,9,10,11,12,13};
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5 ,6,7,8,9,10,11,12,13};

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}
		FileInputStream is = null;
		try {
			mCreateExcelList.write(mOutXmlPath);
			is = new FileInputStream(mOutXmlPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HSSFWorkbook wb = null;
		try {
			wb = new HSSFWorkbook(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HSSFCellStyle style = wb.createCellStyle();
		HSSFFont titleFont = wb.createFont();
		titleFont.setFontName("宋体");
		titleFont.setFontHeightInPoints((short) 18);// 设置字体大小
		titleFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);// 粗体显示
		// 标题样式
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
		style.setFont(titleFont);

		// 二级标题样式
		HSSFCellStyle Msgstyle = wb.createCellStyle();
		HSSFFont MsgFont = wb.createFont();
		MsgFont.setFontName("宋体");
		MsgFont.setFontHeightInPoints((short) 12);// 设置字体大小
		// 标题样式
		Msgstyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
		Msgstyle.setFont(MsgFont);
		// 正文样式
		HSSFCellStyle Contentstyle = wb.createCellStyle();
		HSSFFont ContentFont = wb.createFont();
		ContentFont.setFontName("宋体");
		ContentFont.setFontHeightInPoints((short) 10);// 设置字体大小
		Contentstyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		Contentstyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		Contentstyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		Contentstyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		Contentstyle.setFont(ContentFont);
		// 合并单元格
		HSSFSheet sheet = wb.getSheetAt(0); // 获取sheet
		HSSFRow trow = sheet.getRow(1);
		trow.setHeight((short) 500);
		for (int i = 0; i < tSSRS.MaxRow + 5; i++) {
			HSSFRow mrow = sheet.getRow(i);
			mrow.setHeight((short) 300);
			for (int j = 0; j < 13; j++) {
				HSSFCell firstCell = mrow.getCell((short) j);
				sheet.setColumnWidth((short) j, (short) 5000);
				firstCell.setCellStyle(Contentstyle);
			}
		}
		sheet.addMergedRegion(new Region(1, (short) 0, 1, (short) 12));
		sheet.addMergedRegion(new Region(2, (short) 0, 1, (short) 12));
		sheet.addMergedRegion(new Region(3, (short) 0, 3, (short) 6));
		sheet.addMergedRegion(new Region(3, (short) 7, 3, (short) 12));
		HSSFRow tRow0 = sheet.getRow((short) 1);
		tRow0.setHeight((short) 600);
		HSSFCell tRow0Cell = tRow0.getCell((short) 0);
		tRow0Cell.setCellStyle(style);

		HSSFRow tRow2 = sheet.getRow((short) 2);
		tRow2.setHeight((short) 600);
		HSSFCell tRow2Cell = tRow2.getCell((short) 1);
		tRow2Cell.setCellStyle(Msgstyle);

		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(mOutXmlPath);
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

}
