package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class EntrusManPrintBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;

    private String RiskCode = "";
    private String DepCodea = "";
    private String DepCodeb = "";
    private String StartDate = "";
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public EntrusManPrintBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");

            ExeSQL tExeSQL = new ExeSQL();
            
            mSql =  " Select char(rownumber() over()) as id, bb.* From (select 机构,险种, 保单号,业务日期,财务确认日期,成本中心,借贷标记,sum((case when accountcode in ('2611020101','2611020201') then money else 0 end)) as 保单金额,sum((case when accountcode = '6051010000' then money else 0 end)) as 管理费收入,投保单位,责任起期,责任止期,保全项目 from" 
            	   +"(select a.managecom as 机构,substr (a.riskcode,1,4) as 险种,a.contno as 保单号,a.accountcode as accountcode,a.accountdate as 财务确认日期,a.costcenter as 成本中心, (case a.finitemtype  when 'D' then '借' when 'C' then '贷' end) as 借贷标记," 
            	   +"(select SignDate from lcgrpcont where grpcontno = a.contno union select SignDate from lbgrpcont where grpcontno = a.contno) as 业务日期,"
            	   +"sum((case a.finitemtype when 'D' then nvl(-summoney,0) when 'C' then nvl(summoney,0) end)) as money,"
            	   +"(select grpname from lcgrpcont where grpcontno = a.contno union select grpname from lbgrpcont where grpcontno = a.contno fetch first row only) as 投保单位,"
            	   +"(select CValiDate from lcgrpcont where grpcontno = a.contno union select CValiDate from lbgrpcont where grpcontno = a.contno fetch first row only) as 责任起期,"
            	   +"(select CInValiDate from lcgrpcont where 1=1 and grpcontno = a.contno union select CInValiDate from lbgrpcont where 1=1 and grpcontno = a.contno fetch first row only ) as 责任止期,"
            	   +"(case substr(a.classtype,1,1)  when 'B' then(select edorname from lmedoritem where edorcode in (select FeeOperationType from LIAboriginalData where riskcode like '"+RiskCode+"%' and paydate between '"+StartDate+"' and '"+EndDate+"' and paydate = confdate and paydate = enteraccdate and serialno = a.STANDBYSTRING3 and grpcontno = a.contno))"
            	   +" else (select classtypename from db2inst1.LIClassTypeKeyDef where classtype in (select a.classtype from LIDataTransResult g where g.riskcode like '"+RiskCode+"%' and g.accountdate between '"+StartDate+"' and '"+EndDate+"' and g.accountcode in ('2611020101', '2611020201', '6051010000')and g.managecom between '"+DepCodea+"' and '"+DepCodeb+"'  and g.contno = a.contno )) end ) as 保全项目"
            	   +" from LIDataTransResult a where a.riskcode like '"+RiskCode+"%' and a.accountdate between '"+StartDate+"' and '"+EndDate+"' "
            	   +"and a.accountcode in ( '2611020101','2611020201','6051010000') and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"'"
	               +"group by a.managecom,a.riskcode,a.contno,a.accountdate,a.costcenter,a.finitemtype,a.accountcode ,a.STANDBYSTRING3,a.classtype) as aa "	   	 	
	               +"group by  机构,险种, 保单号,业务日期,财务确认日期,成本中心,借贷标记,投保单位,责任起期,责任止期,保全项目 "
	               +"having (sum(case when accountcode in ('2611020101','2611020201') then money else 0 end ) <> 0 or sum(case when accountcode = '6051010000' then money else 0 end ) <> 0)"
                   + " order by 保单号 )as bb ";
//            }
            System.out.println(mSql);
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(tSSRS.getMaxRow()<=0)
            {
            	buiError("dealData","没有符合条件的信息!");
                return false;
            }
 
        return true;
    }
    
	private boolean createFile(){
		
//		 1、查看数据行数
	  	  int dataSize = this.tSSRS.getMaxRow();
	  	  if(this.tSSRS==null||this.tSSRS.getMaxRow()<1)
	  	  {
	  		  buiError("dealData","没有符合条件的信息!");
	  		  return false;
	  	  }
	        // 定义一个二维数组 
	  	   String[][] mToExcel = new String[dataSize + 6][30];
//	       mToExcel = new String[10000][30];
	        mToExcel[0][4] = "委托管理业务清单";
//	        mToExcel[1][5] = EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月份，实际结余返还金额统计表";
	        mToExcel[1][6] = RiskCode+"险种的业务清单";
	        mToExcel[2][8] = "编制单位：" + getName();

	        mToExcel[3][0] = "序号";
	        mToExcel[3][1] = "机构";
	        mToExcel[3][2] = "险种";
	        mToExcel[3][3] = "保单号";
	        mToExcel[3][4] = "业务日期";
	        mToExcel[3][5] = "财务确认日期";
	        mToExcel[3][6] = "成本中心";
	        mToExcel[3][7] = "借贷标记";
	        mToExcel[3][8] = "保单金额";
	        mToExcel[3][9] = "管理费收入";
	        mToExcel[3][10] = "投保单位";
	        mToExcel[3][11] = "责任起期";
	        mToExcel[3][12] = "责任止期";
	        mToExcel[3][13] = "保全项目";

	        
	        int printNum = 3;
	        
	           int no = 1;
	            for (int row = 1; row <= tSSRS.getMaxRow(); row++)
	            {
	            	
	                for (int col = 1; col <= tSSRS.getMaxCol(); col++)
	                {       
	                        if (tSSRS.GetText(row, col).equals("null"))
	                        {
	                            mToExcel[row + printNum][col - 1] = "";
	                        }
	                        else
	                        {
	                            mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
	                        }
	                        }
	              
	                mToExcel[row + printNum][0] = no + "";
	                no++;}           

	        mToExcel[printNum + tSSRS.getMaxRow() + 2][0] = "制表";
	        mToExcel[printNum + tSSRS.getMaxRow() + 2][14] = "日期：" + mCurDate;
	            
		
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        RiskCode = (String) tf.getValueByName("RiskCode");     
        DepCodea = (String) tf.getValueByName("DepCodea");
        DepCodeb = (String) tf.getValueByName("DepCodeb");
        StartDate = (String) tf.getValueByName("StartDate");
        EndDate = (String) tf.getValueByName("EndDate");
        mMail = (String) tf.getValueByName("toMail");
        
        
        System.out.println("mMail-----"+mMail);

        if (RiskCode == null || mOutXmlPath == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    
    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "EntrusManPrintBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
//        tTransferData.setNameAndValue("EndDate", "2012-05-01");
//        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}
