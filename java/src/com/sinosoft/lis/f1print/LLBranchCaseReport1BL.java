package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLBranchCaseReportBL</p>
 * <p>Description: 机构理赔状况报表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */


import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class LLBranchCaseReport1BL{
    public LLBranchCaseReport1BL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */

    private String mStartDate = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageComName = "";
    private String moperator = "";
    private String mCaseType = "";
    private String mCaseTypeName = "";
    private String CaseTypeSql="";
    private long mStartDays = 0;
    private long mEndDays = 0;
    private XmlExport mXmlExport = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();
	private String mFileNameB = "";
	private String mMakeDate = "";
	private String mOperator = "";
	private TransferData mTransferData = new TransferData();
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }else{
			TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if(!tLLPrintSave.submitData(tVData,"")){
				System.out.println("LLPrintSave is fault!");
				return false;
			     }
		}

        return true;
    }


    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean getDataList() {
        double tRCase = 0;
        double tCCase = 0;
        double tRECase = 0;
        double tECase = 0;
        double tEPay = 0;
        double tETime = 0;
        double tSumETime = 0;
        double tREPay = 0;
        double tREPaymoney  = 0;
        String tSQL = "";
        DecimalFormat tDF = new DecimalFormat("0.##");
        DecimalFormat tDF1 = new DecimalFormat("0.#");

        // 1、得到全部已开业的机构
        if (mManageCom.length() == 2) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=4 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
        //cbs00058484 修改，为避免重复，如果是用4位机构登陆查询，只需要带出该4位机构下的所有8位机构的数据即可。
        } else if (mManageCom.length() == 4) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=8 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
            System.out.println("YYYYYYYYYYYYYYY");
        } else {
            tSQL =
                    "SELECT comcode,name FROM ldcom WHERE sign='1' AND comcode LIKE '" +
                    mManageCom + "%' ORDER BY comcode";
        }
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() <= 0) {
            buildError("queryData", "没有附和条件的机构信息！");
            return false;
        }

        // 2、查询需要表示的数据
        String tManageCom = "";
        String tComName = "";

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {// 循环机构进行统计
        	String Info[] = new String[10];
            tManageCom = tSSRS.GetText(i, 1);
            tComName = tSSRS.GetText(i, 2);

            Info[0] = tComName;    
            System.out.println("案件类型========="+mCaseType);
            if(mCaseType.equals("2")){
            	CaseTypeSql =" and  exists (select 1 from llregister where rgtno=b.rgtno and (applyertype='0' or applyertype is null))";
            }else if(mCaseType.equals("3")){
            	CaseTypeSql =" and exists (select 1 from llregister where rgtno=b.rgtno and applyertype='2')";
            }else if(mCaseType.equals("4")){
            	CaseTypeSql =" and exists (select 1 from llregister where rgtno=b.rgtno and applyertype='5')";
            }else if(mCaseType.equals("5")){
            	CaseTypeSql =" and exists (select 1 from llregister where rgtno=b.rgtno and (PrePaidFlag !='1' or PrePaidFlag is null)) and (b.PrePaidFlag != '1' or b.PrePaidFlag is null) ";
            }
            Info[1] = getRCase(tManageCom);
            Info[2] = getCCase(tManageCom);
            Info[3] = getRECase(tManageCom);
            Info[4] = getREPay(tManageCom);
            Info[5] = getECase(tManageCom);
            Info[6] = getEPay(tManageCom);
            tETime = getETime(tManageCom);
            Info[8] = getREPayMoney(tManageCom);
            if (Info[5].equals("0")) {
                Info[7] = "0";
            } else {
                Info[7] = tDF1.format(tETime / Double.parseDouble(Info[5]));
            }
            tRCase += Double.parseDouble(Info[1]);
            tCCase += Double.parseDouble(Info[2]);
            tRECase += Double.parseDouble(Info[3]);
            tREPay += Double.parseDouble(Info[4]);
            tECase += Double.parseDouble(Info[5]);
            tEPay += Double.parseDouble(Info[6]);
            tREPaymoney+=Double.parseDouble(Info[8]);
            tSumETime += tETime;
            mListTable.add(Info);
        }

        //合计所有公司
        String Info[] = new String[10];

        Info[0] = "合计";
        Info[1] = tDF.format(tRCase);
        Info[2] = tDF.format(tCCase);
        Info[3] = tDF.format(tRECase);
        Info[4] = tDF.format(tREPay);
        Info[5] = tDF.format(tECase);
        Info[6] = tDF.format(tEPay);
        Info[8] = tDF.format(tREPaymoney);
        if (Info[5].equals("0")) {
            Info[7] = "0";
        } else {
            Info[7] = tDF1.format(tSumETime / Double.parseDouble(Info[5]));
        }
        mListTable.add(Info);

        return true;
    }

    /**
     * 申请件数
     * @param cManageCom String
     * @return String
     */
    private String getRCase(String cManageCom) {
        String tSql = "SELECT COUNT(caseno) FROM llcase b WHERE mngcom LIKE '"
                      + cManageCom +
                      "%' AND rgtdate BETWEEN '" + mStartDate + "' AND '" +
                      mEndDate + "' "+CaseTypeSql+"  WITH UR ";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 撤销件数
     * @param cManageCom String
     * @return String
     */
    private String getCCase(String cManageCom) {
        String tSql =
                "SELECT COUNT(caseno) FROM llcase b WHERE rgtstate='14' AND mngcom LIKE '"
                + cManageCom +
                "%' AND cancledate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' "+CaseTypeSql+"  WITH UR ";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 申请已结件数
     * @param cManageCom String
     * @return String
     */
    private String getRECase(String cManageCom) {
        String tSql = "SELECT COUNT(caseno) FROM llcase b WHERE rgtstate IN ('09','11','12') AND mngcom LIKE '"
                      + cManageCom + "%' AND rgtdate BETWEEN '" + mStartDate +
                      "' AND '" + mEndDate + "' AND endcasedate <='" + mEndDate +
                      "' "+CaseTypeSql+"  WITH UR ";
        double tSumPrem = 0.0;
        String tPrem = "";
        ExeSQL tExeSQL = new ExeSQL();

        tPrem = tExeSQL.getOneValue(tSql);
        if (tPrem.equals("null") || tPrem == null || tPrem.equals("")) {
            tSumPrem = 0;
        } else {
            tSumPrem = Double.parseDouble(tPrem);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        return tRturn;

    }

    /**
     * 申请已结赔款
     * @param cManageCom String
     * @return String
     */
    private String getREPay(String cManageCom) {
        String tSql = "SELECT SUM(a.RealPay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12') AND b.mngcom LIKE '" +
                      cManageCom + "%' AND b.rgtdate BETWEEN '" + mStartDate +
                      "' AND '" + mEndDate + "' AND b.endcasedate <='" +
                      mEndDate + "' "+CaseTypeSql+"  WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案件数
     * @param cManageCom String
     * @return String
     */
    private String getECase(String cManageCom) {
        String tSql = "SELECT COUNT(caseno) FROM llcase b WHERE  rgtstate IN ('09','11','12') AND mngcom LIKE '" +
                      cManageCom + "%' AND endcasedate BETWEEN '" + mStartDate +
                      "' AND '" + mEndDate + "' "+CaseTypeSql+"  WITH UR  ";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案赔款
     * @param cManageCom String
     * @return String
     */

    private String getEPay(String cManageCom) {
        String tSql = "SELECT SUM(a.realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
                      cManageCom +
                      "%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
                      mEndDate + "' "+CaseTypeSql+" WITH UR ";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案时间
     * @param cManageCom String
     * @return Double
     */
    private double getETime(String cManageCom) {
        String tSql = "SELECT SUM(TO_DATE(endcasedate)-TO_DATE(rgtdate)+1) FROM llcase b WHERE rgtstate IN ('09','11','12') AND mngcom LIKE '" +
                      cManageCom +
                      "%' AND endcasedate BETWEEN '" + mStartDate + "' AND '" +
                      mEndDate + "' "+CaseTypeSql+"  WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }
    /**
     * 回销预付赔款
     * @param cManageCom String
     * @return String
     */
    private String getREPayMoney(String cManageCom) {
        String tSql = " select -sum(money) from llprepaidtrace a,llcase b where a.otherno = b.caseno and a.state = '1' and managecom LIKE '" +cManageCom + "%' and othernotype = 'C'  AND BalaDate  BETWEEN '" + mStartDate +
                      "' AND '" + mEndDate + "' "+CaseTypeSql+"  WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }
    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLBranchCaseReport2.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDateN", mStartDate);
        tTextTag.add("EndDateN", mEndDate);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        
        if(mCaseType.equals("1")){
        	mCaseTypeName ="全部案件";
        }else if(mCaseType.equals("2")){
        	mCaseTypeName ="常规案件";
        }else if(mCaseType.equals("3")){
        	mCaseTypeName ="批次受理案件";
        }else if(mCaseType.equals("4")){
        	mCaseTypeName ="批次处理案件";
        }else if(mCaseType.equals("5")){
        	mCaseTypeName ="不含回销预付赔款案件";
        }
        
        tTextTag.add("CaseTypeName", mCaseTypeName);

        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "",""};

        if (!getDataList()) {
            return false;
        }
        mListTable.setName("FAULT");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        moperator = (String) pmInputData.get(3);
        mManageComName = (String) pmInputData.get(4);
        mCaseType = (String) pmInputData.get(5);
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
				"TransferData", 0);
    	mOperator = moperator;
		mFileNameB = (String)mTransferData.getValueByName("tFileNameB");

        String tSql = "SELECT days('" + mStartDate + "'),days('" + mEndDate +
                      "') FROM dual";
        ExeSQL tExeSQLDate = new ExeSQL();
        SSRS tSSRSDate = new SSRS();
        tSSRSDate = tExeSQLDate.execSQL(tSql);
        mStartDays = Integer.parseInt(tSSRSDate.GetText(1, 1));
        mEndDays = Integer.parseInt(tSSRSDate.GetText(1, 2));
        mMakeDate = PubFun.getCurrentDate();
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLBranchCaseReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
