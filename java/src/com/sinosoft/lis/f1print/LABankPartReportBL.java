/*
 * <p>ClassName: LABankPartReportBL </p>
 * <p>Description: LABankPartReportBL 类文件 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2008-04-07 18:03:36
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LABankPartReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
     //管理机构
    private String managecom = "";
    //统计层级
    private String mLevel = "";
    //统计险种
    private String mRiskCode = "";
    //统计起期
    private String  mStartDate= "";
    //统计止期
    private String  mEndDate = "";
    //交费方式
    private String  mPayIntv = "";
    //交费年限
    private String  mPayYears = "";
    //需要调用的模版
    private String mFlag = "";
    //转化交费方式
    private int mPayTime;
    //转换交费年限
    private int mpayyears;
    //零时存放的数组

    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private  ListTable tListTable = new ListTable();
    private  ListTable mListTable = new ListTable();
    public LABankPartReportBL () {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("dealData:" + mOperate);
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankPartReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        tListTable = null;
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LABankPartReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("dealData:" + mOperate);
        mFlag = getflag(mLevel);
        ExeSQL tExeSQL = new ExeSQL();
        String manageSQL = "";
        int j=1;
        if(this.mLevel==null||this.mLevel.equals(""))
        {
            manageSQL = "select comcode,name from ldcom where comgrade='03' and sign='1' and comcode like '"+managecom+"%' order  by comcode";

        }
        else
        {
            manageSQL = "select comcode,name from ldcom where comcode like '"+this.mLevel+"%'  and  comgrade='03' and sign='1'";
        }
        System.out.println(manageSQL);
            SSRS managSSRS = new SSRS();
            managSSRS = tExeSQL.execSQL(manageSQL);
           // String data1[] = new String[98];
            String[] data1 = {
                    "0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0",
                    "0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0",
                    "0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0",
                    "0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0",
                    "0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
            for(int i = 1;i<=managSSRS.getMaxRow();i++)
            {//进行机构循环
                if(!getDataList(managSSRS.GetText(i, 1),managSSRS.GetText(i, 2),j,data1))
                {
                    CError tCError = new CError();
                    tCError.moduleName = "LABankPartReportBL";
                    tCError.functionName = "dealData";
                    tCError.errorMessage = "没有符合条件的信息！";
                    this.mErrors.addOneError(tCError);
                    return false;
                }
                j++;
            }


        if(tListTable==null||tListTable.equals(""))
        {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        //添加合计行
        mListTable.add(data1);
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LABank2.vts", "printer");
        tListTable.setName("LABank");
        mListTable.setName("Add");
        String[] title = {
                         "","","","","","","","","","","","","","","","","","","","",
                         "","","","","","","","","","","","","","","","","","","","",
                         "","","","","","","","","","","","","","","","","","","","",
                         "","","","","","","","","","","","","","","","","","","","",
                         "","","","","","","","","","","","","","","","","","",""};
        //放入一些基本值
        texttag.add("makedate", currentDate);
        texttag.add("maketime", currentTime);
        texttag.add("StartDate",mStartDate);
        texttag.add("EndDate",mEndDate);
        texttag.add("riskcode",mRiskCode);
        texttag.add("payIntv",mPayIntv);
        texttag.add("payyears",mPayYears);

        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
//       将数据放到LISTTABLE中
        txmlexport.addListTable(tListTable, title);
        txmlexport.addListTable(mListTable, title);
        txmlexport.outputDocumentToFile("c:\\", "lach");
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
    /**
     * @string
     * 模版调用标记
     */
    private String getflag(String level)
    {
        String tflag = "";
        if(level.equals("0"))
        {
            tflag = "1";
        }
        if(level.equals("1"))
        {
            tflag = "2";
        }
        if(level.equals("2"))
        {
            tflag = "3";
        }
        if(level.equals("3"))
        {
            tflag = "4";
        }
        return tflag;
    }
    /**
     * 获取模版
     */
    private boolean getStencil(String cflag)
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LABank2.vts","printer");
        tListTable.setName("LABank");
        String[] title = {"", "", "", "", "" ,"","","","","","","","","","","","","","",""
                         ,"","","",};
        //放入一些基本值
        texttag.add("makedate", currentDate);
        texttag.add("maketime", currentTime);
        texttag.add("company", getName());
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
//       将数据放到LISTTABLE中
        txmlexport.addListTable(tListTable, title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }

    /**
     *查询各列值
     *cCode-循环参数；Name--管理机构名字
     *农业银行-PY005；交通-PY009；中国银行-PY002；中国工商-PY001；中国建行-PY004，中国邮政-PY015
     */
    private boolean getDataList(String cCode,String Name, int j,String[] Add)
    {
        String mCode = cCode;
        try
        {
            DecimalFormat tDF = new DecimalFormat("0.######");
            String data[] = new String[99];
            data[0] = String.valueOf(j);
            data[1] = getPartName(mCode);
            data[2] = Name;
            //工商银行
            data[3] = getPrem(mCode,"PY001");
            data[4] = getWTPrem(mCode,"PY001");
            data[5] = getTBPrem(mCode,"PY001");
            data[6] = tDF.format(Double.parseDouble(data[3])+Double.parseDouble(data[4])+Double.parseDouble(data[5]));
            data[7] = String.valueOf(getNum(mCode,"PY001"));
            data[8] = String.valueOf(getWTNum(mCode,"PY001"));
            data[9] = String.valueOf(TBNum(mCode,"PY001"));
            data[10] = String.valueOf(Integer.parseInt(data[7])-Integer.parseInt(data[8])-Integer.parseInt(data[9]));
            data[11]= String.valueOf(AgentcomNum(mCode, "PY001"));
            data[12]= String.valueOf(YXAgentcomNum(mCode, "PY001"));
            //农行
            data[13] = getPrem(mCode,"PY005");
            data[14] = getWTPrem(mCode,"PY005");
            data[15] = getTBPrem(mCode,"PY005");
            data[16] = tDF.format(Double.parseDouble(data[13])+Double.parseDouble(data[14])+Double.parseDouble(data[15]));
            data[17] = String.valueOf(getNum(mCode,"PY005"));
            data[18] = String.valueOf(getWTNum(mCode,"PY005"));
            data[19] = String.valueOf(TBNum(mCode,"PY005"));
            data[20] = String.valueOf(Integer.parseInt(data[17])-Integer.parseInt(data[18])-Integer.parseInt(data[19]));
            data[21]= String.valueOf(AgentcomNum(mCode, "PY005"));
            data[22]= String.valueOf(YXAgentcomNum(mCode, "PY005"));
            //中国银行
            data[23]= getPrem(mCode,"PY002");
            data[24] = getWTPrem(mCode,"PY002");
            data[25] = getTBPrem(mCode,"PY002");
            data[26] = tDF.format(Double.parseDouble(data[23])+Double.parseDouble(data[24])+Double.parseDouble(data[25]));
            data[27] = String.valueOf(getNum(mCode,"PY002"));
            data[28] = String.valueOf(getWTNum(mCode,"PY002"));
            data[29] = String.valueOf(TBNum(mCode,"PY002"));
            data[30] = String.valueOf(Integer.parseInt(data[27])-Integer.parseInt(data[28])-Integer.parseInt(data[29]));
            data[31]= String.valueOf(AgentcomNum(mCode, "PY002"));
            data[32]= String.valueOf(YXAgentcomNum(mCode, "PY002"));
            //建行
            data[33]= getPrem(mCode,"PY004");
            data[34] = getWTPrem(mCode,"PY004");
            data[35] = getTBPrem(mCode,"PY004");
            data[36] = tDF.format(Double.parseDouble(data[33])+Double.parseDouble(data[34])+Double.parseDouble(data[35]));
            data[37] = String.valueOf(getNum(mCode,"PY004"));
            data[38] = String.valueOf(getWTNum(mCode,"PY004"));
            data[39] = String.valueOf(TBNum(mCode,"PY004"));
            data[40] = String.valueOf(Integer.parseInt(data[37])-Integer.parseInt(data[38])-Integer.parseInt(data[39]));
            data[41] = String.valueOf(AgentcomNum(mCode, "PY004"));
            data[42] = String.valueOf(YXAgentcomNum(mCode, "PY004"));
            //交行
            data[43]= getPrem(mCode,"PY009");
            data[44] = getWTPrem(mCode,"PY009");
            data[45] = getTBPrem(mCode,"PY009");
            data[46] = tDF.format(Double.parseDouble(data[43])+Double.parseDouble(data[44])+Double.parseDouble(data[45]));
            data[47] = String.valueOf(getNum(mCode,"PY009"));
            data[48] = String.valueOf(getWTNum(mCode,"PY009"));
            data[49] = String.valueOf(TBNum(mCode,"PY009"));
            data[50] = String.valueOf(Integer.parseInt(data[47])-Integer.parseInt(data[48])-Integer.parseInt(data[49]));
            data[51]= String.valueOf(AgentcomNum(mCode, "PY009"));
            data[52]= String.valueOf(YXAgentcomNum(mCode, "PY009"));
            //邮政
            data[53]= getPrem(mCode,"PY015");
            data[54] = getWTPrem(mCode,"PY015");
            data[55] = getTBPrem(mCode,"PY015");
            data[56] = tDF.format(Double.parseDouble(data[53])+Double.parseDouble(data[54])+Double.parseDouble(data[55]));
            data[57] = String.valueOf(getNum(mCode,"PY015"));
            data[58] = String.valueOf(getWTNum(mCode,"PY015"));
            data[59] = String.valueOf(TBNum(mCode,"PY015"));
            data[60] = String.valueOf(Integer.parseInt(data[57])-Integer.parseInt(data[58])-Integer.parseInt(data[59]));
            data[61]= String.valueOf(AgentcomNum(mCode, "PY015"));
            data[62]= String.valueOf(YXAgentcomNum(mCode, "PY015"));
            //其他
            data[63] = getPrem(mCode,"1");
            data[64] = getWTPrem(mCode,"1");
            data[65] = getTBPrem(mCode,"1");
            data[66] = tDF.format(Double.parseDouble(data[63])+Double.parseDouble(data[64])+Double.parseDouble(data[65]));
            data[67] = String.valueOf(getNum(mCode,"1"));
            data[68] = String.valueOf(getWTNum(mCode,"1"));
            data[69] = String.valueOf(TBNum(mCode,"1"));
            data[70] = String.valueOf(Integer.parseInt(data[67])-Integer.parseInt(data[68])-Integer.parseInt(data[69]));
            data[71] = String.valueOf(AgentcomNum(mCode, "1"));
            data[72] = String.valueOf(YXAgentcomNum(mCode, "1"));
            //横向总计
            data[73]= tDF.format(Double.parseDouble(data[3])+Double.parseDouble(data[13])+Double.parseDouble(data[23])+Double.parseDouble(data[33])+
                    Double.parseDouble(data[43])+Double.parseDouble(data[53])+Double.parseDouble(data[63]));
            data[74] =  tDF.format(Double.parseDouble(data[4])+Double.parseDouble(data[14])+Double.parseDouble(data[24])+Double.parseDouble(data[34])+
                   Double.parseDouble(data[44])+Double.parseDouble(data[54])+Double.parseDouble(data[64]));
            data[75] =  tDF.format(Double.parseDouble(data[5])+Double.parseDouble(data[15])+Double.parseDouble(data[25])+Double.parseDouble(data[35])+
                   Double.parseDouble(data[45])+Double.parseDouble(data[55])+Double.parseDouble(data[65]));
            data[76] = tDF.format(Double.parseDouble(data[73])+Double.parseDouble(data[74])+Double.parseDouble(data[75]));
            data[77] = String.valueOf(Integer.parseInt(data[7])+Integer.parseInt(data[17])+Integer.parseInt(data[27])+Integer.parseInt(data[37])+Integer.parseInt(data[47])+
                    Integer.parseInt(data[57])+Integer.parseInt(data[67]));
            data[78] = String.valueOf(Integer.parseInt(data[8])+Integer.parseInt(data[18])+Integer.parseInt(data[28])+Integer.parseInt(data[38])+Integer.parseInt(data[48])+
                    Integer.parseInt(data[58])+Integer.parseInt(data[68]));
            data[79] = String.valueOf(Integer.parseInt(data[9])+Integer.parseInt(data[19])+Integer.parseInt(data[29])+Integer.parseInt(data[39])+Integer.parseInt(data[49])+
                    Integer.parseInt(data[59])+Integer.parseInt(data[69]));
            data[80] = String.valueOf(Integer.parseInt(data[77])-Integer.parseInt(data[78])-Integer.parseInt(data[79]));
            data[81]=String.valueOf(Integer.parseInt(data[11])+Integer.parseInt(data[21])+Integer.parseInt(data[31])+Integer.parseInt(data[41])+Integer.parseInt(data[51])+
                    Integer.parseInt(data[61])+Integer.parseInt(data[71]));
            data[82]= String.valueOf(Integer.parseInt(data[12])+Integer.parseInt(data[22])+Integer.parseInt(data[32])+Integer.parseInt(data[42])+Integer.parseInt(data[52])+
                   Integer.parseInt(data[62])+Integer.parseInt(data[72]));
            //月度累计
           data[83]= getSumPrem(mCode,mEndDate,"1");
           data[84] = getWTSumPrem(mCode,mEndDate,"1");
           data[85] = getTBSumPrem(mCode,mEndDate,"1");
           data[86] = tDF.format(Double.parseDouble(data[83])+Double.parseDouble(data[84])+Double.parseDouble(data[85]));
           data[87] = String.valueOf(getSumNum(mCode,mEndDate,"1"));
           data[88] = String.valueOf(getWTSumNum(mCode,mEndDate,"1"));
           data[89] = String.valueOf(TBSumNum(mCode,mEndDate,"1"));
           data[90] = String.valueOf(Integer.parseInt(data[87])-Integer.parseInt(data[88])-Integer.parseInt(data[89]));
           //年度累计
           data[91]= getSumPrem(mCode,mEndDate,"2");
           data[92]= getWTSumPrem(mCode,mEndDate,"2");
           data[93]= getTBSumPrem(mCode,mEndDate,"2");
           data[94] = tDF.format(Double.parseDouble(data[91])+Double.parseDouble(data[92])+Double.parseDouble(data[93]));
           data[95] = String.valueOf(getSumNum(mCode,mEndDate,"2"));
           data[96] = String.valueOf(getWTSumNum(mCode,mEndDate,"2"));
           data[97] = String.valueOf(TBSumNum(mCode,mEndDate,"2"));
           data[98] = String.valueOf(Integer.parseInt(data[95])-Integer.parseInt(data[96])-Integer.parseInt(data[97]));

            //纵向合计
            Add[0] = String.valueOf(j+1);
            Add[1] = "合  计";
            Add[2] = "";
            Add[3] = tDF.format(Double.parseDouble(Add[3])+Double.parseDouble(data[3]));
            Add[4] = tDF.format(Double.parseDouble(Add[4])+Double.parseDouble(data[4]));
            Add[5] = tDF.format(Double.parseDouble(Add[5])+Double.parseDouble(data[5]));
            Add[6] = tDF.format(Double.parseDouble(Add[6])+Double.parseDouble(data[6]));
            Add[7] = String.valueOf(Integer.parseInt(Add[7])+Integer.parseInt(data[7]));
            Add[8] = String.valueOf(Integer.parseInt(Add[8])+Integer.parseInt(data[8]));
            Add[9] = String.valueOf(Integer.parseInt(Add[9])+Integer.parseInt(data[9]));
            Add[10] = String.valueOf(Integer.parseInt(Add[10])+Integer.parseInt(data[10]));
            Add[11] = String.valueOf(Integer.parseInt(Add[11])+Integer.parseInt(data[11]));
            Add[12] = String.valueOf(Integer.parseInt(Add[12])+Integer.parseInt(data[12]));
            Add[13] = tDF.format(Double.parseDouble(Add[13])+Double.parseDouble(data[13]));
            Add[14] = tDF.format(Double.parseDouble(Add[14])+Double.parseDouble(data[14]));
            Add[15] = tDF.format(Double.parseDouble(Add[15])+Double.parseDouble(data[15]));
            Add[16] = tDF.format(Double.parseDouble(Add[16])+Double.parseDouble(data[16]));
            Add[17] = String.valueOf(Integer.parseInt(Add[17])+Integer.parseInt(data[17]));
            Add[18] = String.valueOf(Integer.parseInt(Add[18])+Integer.parseInt(data[18]));
            Add[19] = String.valueOf(Integer.parseInt(Add[19])+Integer.parseInt(data[19]));
            Add[20] = String.valueOf(Integer.parseInt(Add[20])+Integer.parseInt(data[20]));
            Add[21] = String.valueOf(Integer.parseInt(Add[21])+Integer.parseInt(data[21]));
            Add[22] = String.valueOf(Integer.parseInt(Add[22])+Integer.parseInt(data[22]));
            Add[23] = tDF.format(Double.parseDouble(Add[23])+Double.parseDouble(data[23]));
            Add[24] = tDF.format(Double.parseDouble(Add[24])+Double.parseDouble(data[24]));
            Add[25] = tDF.format(Double.parseDouble(Add[25])+Double.parseDouble(data[25]));
            Add[26] = tDF.format(Double.parseDouble(Add[26])+Double.parseDouble(data[26]));
            Add[27] = String.valueOf(Integer.parseInt(Add[27])+Integer.parseInt(data[27]));
            Add[28] = String.valueOf(Integer.parseInt(Add[28])+Integer.parseInt(data[28]));
            Add[29] = String.valueOf(Integer.parseInt(Add[29])+Integer.parseInt(data[29]));
            Add[30] = String.valueOf(Integer.parseInt(Add[30])+Integer.parseInt(data[30]));
            Add[31] = String.valueOf(Integer.parseInt(Add[31])+Integer.parseInt(data[31]));
            Add[32] = String.valueOf(Integer.parseInt(Add[32])+Integer.parseInt(data[32]));
            Add[33] = tDF.format(Double.parseDouble(Add[33])+Double.parseDouble(data[33]));
            Add[34] = tDF.format(Double.parseDouble(Add[34])+Double.parseDouble(data[34]));
            Add[35] = tDF.format(Double.parseDouble(Add[35])+Double.parseDouble(data[35]));
            Add[36] = tDF.format(Double.parseDouble(Add[36])+Double.parseDouble(data[36]));
            Add[37] = String.valueOf(Integer.parseInt(Add[37])+Integer.parseInt(data[37]));
            Add[38] = String.valueOf(Integer.parseInt(Add[38])+Integer.parseInt(data[38]));
            Add[39] = String.valueOf(Integer.parseInt(Add[39])+Integer.parseInt(data[39]));
            Add[40] = String.valueOf(Integer.parseInt(Add[40])+Integer.parseInt(data[40]));
            Add[41] = String.valueOf(Integer.parseInt(Add[41])+Integer.parseInt(data[41]));
            Add[42] = String.valueOf(Integer.parseInt(Add[42])+Integer.parseInt(data[42]));
            Add[43] = tDF.format(Double.parseDouble(Add[43])+Double.parseDouble(data[43]));
            Add[44] = tDF.format(Double.parseDouble(Add[44])+Double.parseDouble(data[44]));
            Add[45] = tDF.format(Double.parseDouble(Add[45])+Double.parseDouble(data[45]));
            Add[46] = tDF.format(Double.parseDouble(Add[46])+Double.parseDouble(data[46]));
            Add[47] = String.valueOf(Integer.parseInt(Add[47])+Integer.parseInt(data[47]));
            Add[48] = String.valueOf(Integer.parseInt(Add[48])+Integer.parseInt(data[48]));
            Add[49] = String.valueOf(Integer.parseInt(Add[49])+Integer.parseInt(data[49]));
            Add[50] = String.valueOf(Integer.parseInt(Add[50])+Integer.parseInt(data[50]));
            Add[51] = String.valueOf(Integer.parseInt(Add[51])+Integer.parseInt(data[51]));
            Add[52] = String.valueOf(Integer.parseInt(Add[52])+Integer.parseInt(data[52]));
            Add[53] = tDF.format(Double.parseDouble(Add[53])+Double.parseDouble(data[53]));
            Add[54] = tDF.format(Double.parseDouble(Add[54])+Double.parseDouble(data[54]));
            Add[55] = tDF.format(Double.parseDouble(Add[55])+Double.parseDouble(data[55]));
            Add[56] = tDF.format(Double.parseDouble(Add[56])+Double.parseDouble(data[56]));
            Add[57] = String.valueOf(Integer.parseInt(Add[57])+Integer.parseInt(data[57]));
            Add[58] = String.valueOf(Integer.parseInt(Add[58])+Integer.parseInt(data[58]));
            Add[59] = String.valueOf(Integer.parseInt(Add[59])+Integer.parseInt(data[59]));
            Add[60] = String.valueOf(Integer.parseInt(Add[60])+Integer.parseInt(data[60]));
            Add[61] = String.valueOf(Integer.parseInt(Add[61])+Integer.parseInt(data[61]));
            Add[62] = String.valueOf(Integer.parseInt(Add[62])+Integer.parseInt(data[62]));
            Add[63] = tDF.format(Double.parseDouble(Add[63])+Double.parseDouble(data[63]));
            Add[64] = tDF.format(Double.parseDouble(Add[64])+Double.parseDouble(data[64]));
            Add[65] = tDF.format(Double.parseDouble(Add[65])+Double.parseDouble(data[65]));
            Add[66] = tDF.format(Double.parseDouble(Add[66])+Double.parseDouble(data[66]));
            Add[67] = String.valueOf(Integer.parseInt(Add[67])+Integer.parseInt(data[67]));
            Add[68] = String.valueOf(Integer.parseInt(Add[68])+Integer.parseInt(data[68]));
            Add[69] = String.valueOf(Integer.parseInt(Add[69])+Integer.parseInt(data[69]));
            Add[70] = String.valueOf(Integer.parseInt(Add[70])+Integer.parseInt(data[70]));
            Add[71] = String.valueOf(Integer.parseInt(Add[71])+Integer.parseInt(data[71]));
            Add[72] = String.valueOf(Integer.parseInt(Add[72])+Integer.parseInt(data[72]));
            Add[73] = tDF.format(Double.parseDouble(Add[73])+Double.parseDouble(data[73]));
            Add[74] = tDF.format(Double.parseDouble(Add[74])+Double.parseDouble(data[74]));
            Add[75] = tDF.format(Double.parseDouble(Add[75])+Double.parseDouble(data[75]));
            Add[76] = tDF.format(Double.parseDouble(Add[76])+Double.parseDouble(data[76]));
            Add[77] = String.valueOf(Integer.parseInt(Add[77])+Integer.parseInt(data[77]));
            Add[78] = String.valueOf(Integer.parseInt(Add[78])+Integer.parseInt(data[78]));
            Add[79] = String.valueOf(Integer.parseInt(Add[79])+Integer.parseInt(data[79]));
            Add[80] = String.valueOf(Integer.parseInt(Add[80])+Integer.parseInt(data[80]));
            Add[81] = String.valueOf(Integer.parseInt(Add[81])+Integer.parseInt(data[81]));
            Add[82] = String.valueOf(Integer.parseInt(Add[82])+Integer.parseInt(data[82]));
            Add[83] = tDF.format(Double.parseDouble(Add[83])+Double.parseDouble(data[83]));
            Add[84] = tDF.format(Double.parseDouble(Add[84])+Double.parseDouble(data[84]));
            Add[85] = tDF.format(Double.parseDouble(Add[85])+Double.parseDouble(data[85]));
            Add[86] = tDF.format(Double.parseDouble(Add[86])+Double.parseDouble(data[86]));
            Add[87] = String.valueOf(Integer.parseInt(Add[87])+Integer.parseInt(data[87]));
            Add[88] = String.valueOf(Integer.parseInt(Add[88])+Integer.parseInt(data[88]));
            Add[89] = String.valueOf(Integer.parseInt(Add[89])+Integer.parseInt(data[89]));
            Add[90] = String.valueOf(Integer.parseInt(Add[90])+Integer.parseInt(data[90]));
            Add[91] = tDF.format(Double.parseDouble(Add[91])+Double.parseDouble(data[91]));
             Add[92] = tDF.format(Double.parseDouble(Add[92])+Double.parseDouble(data[92]));
             Add[93] = tDF.format(Double.parseDouble(Add[93])+Double.parseDouble(data[93]));
             Add[94] = tDF.format(Double.parseDouble(Add[94])+Double.parseDouble(data[94]));
             Add[95] = String.valueOf(Integer.parseInt(Add[95])+Integer.parseInt(data[95]));
             Add[96] = String.valueOf(Integer.parseInt(Add[96])+Integer.parseInt(data[96]));
             Add[97] = String.valueOf(Integer.parseInt(Add[97])+Integer.parseInt(data[97]));
             Add[98] = String.valueOf(Integer.parseInt(Add[98])+Integer.parseInt(data[98]));




            tListTable.add(data);
        }
        catch(Exception ex)
        {
            CError.buildErr("getDataList", "准备数据时出错！");
            System.out.println(ex.toString());
            return false;
        }
        return true;
    }

    private String getPartName(String mCode) {
         String tName="";
         String  tCode=mCode.substring(0,4);
         String sql = "select name from ldcom where comcode='" + tCode + "'";
         SSRS tSSRS = new SSRS();
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         try{
            tName=tSSRS.GetText(1, 1);
         }catch(Exception ex)
         {
             System.out.println("getPartName 出错！");
         }
          return tName;
   }

    /**
     * 承保保费查询
     * agentcom-渠道
     */
    private String getPrem(String mCode,String agentcom)
    {
        String Prem = "";
        String tSQL ="";
        DecimalFormat tDF = new DecimalFormat("0.######");
        if(agentcom.equals("1"))
        {
            tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b where b.payType='ZC'" +

            " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" +
                    " and b.agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
                    " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
                    " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
        }
        else
         tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b  " +
                "where b.payType='ZC'" +
                "  and b.agentcom like '"+agentcom+"%'" +
                " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" ;


        tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ) " ;
        tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ))";
        if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
        {
            tSQL += " and b.riskcode = '"+mRiskCode+"'";
        }

            tSQL += " and b.managecom like '"+mCode+"%'";


        try{
            Prem = tDF.format(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getPrem 出错！");
        }

        return Prem;
    }

    /**
    * 月年累计年度累计
    * mDate-结束日期
    */
   private String getSumPrem(String mCode,String mDate,String mType)
   {
         System.out.println("mDate:"+mDate);
       String Prem = "";
       String tSQL ="";
       String tSQL1 ="";
       String mDate1="";
       String mDate2="";
       if(mType.equals("1"))
       {
           mDate2 = mDate;
           mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
           System.out.println("mDate:"+mDate);
           tSQL1 = "select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                   mDate + "";

           SSRS tSSRS = new SSRS();
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(tSQL1);
           mDate1 = tSSRS.GetText(1, 1);
       }
       else  if (mType.equals("2"))
       {
            mDate2=mDate;
            mDate = mDate.substring(0, 4);
            mDate1=mDate+"-01-01";
      }
       DecimalFormat tDF = new DecimalFormat("0.######");
       tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b where b.payType='ZC'" +

           " and b.makedate >='"+mDate1+"' and b.makedate<='"+mDate2+"' and b.GrpPolNo = '00000000000000000000'" ;
       tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ) " ;
       tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ))";
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
           tSQL += " and b.riskcode = '"+mRiskCode+"'";
       }

           tSQL += " and b.managecom like '"+mCode+"%'";


       try{
           Prem = tDF.format(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getSumPrem 出错！");
       }

       return Prem;
   }

    /**
     * 撤保保费
     */
    private String getWTPrem(String mCode,String agentcom)
    {
        String WTPrem = "";
        String tSQL ="";
        DecimalFormat tDF = new DecimalFormat("0.######");
        if(agentcom.equals("1"))
        {
            tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
                    "where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
            " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" +

                    " and b.agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
                    " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
                    " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
        }
        else
         tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
                " where   b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
                " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000' " +
                        " and b.agentcom like '"+agentcom+"%'" ;

        tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ) " ;
        tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ))";

      if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
         {
             tSQL += " and b.riskcode = '"+mRiskCode+"'";
         }


             tSQL += " and b.managecom like '"+mCode+"%'";


        try{
            WTPrem = tDF.format(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getWTPrem 出错！");
        }

        return WTPrem;
    }
    /**
    * 年月累计撤保保费
    */
   private String getWTSumPrem(String mCode,String mDate,String mType)
   {
       String WTPrem = "";
       String tSQL ="";
       DecimalFormat tDF = new DecimalFormat("0.######");
      String tSQL1 ="";
      String mDate1="";
      String mDate2="";

      if(mType.equals("1"))
      {
          mDate2 = mDate;
          mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
          tSQL1 = "select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                  mDate + "";

          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(tSQL1);
          mDate1 = tSSRS.GetText(1, 1);

      }
      else  if (mType.equals("2"))
      {
          mDate2=mDate;
           mDate = mDate.substring(0, 4);
           mDate1=mDate+"-01-01";

      }
     tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
        "where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
       " and b.makedate >='"+mDate1+"' and b.makedate<='"+mDate2+"' and b.GrpPolNo = '00000000000000000000'" ;
     tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ) " ;
       tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ))";

     if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
        {
            tSQL += " and b.riskcode = '"+mRiskCode+"'";
        }
         tSQL += " and b.managecom like '"+mCode+"%'";
     try{
           WTPrem = tDF.format(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getWTSumPrem 出错！");
       }

       return WTPrem;
   }

    /**
     * 承保件数
     */
    private int getNum(String mCode,String agentcom)
    {
        int CBNum = 0;
        String tSQL ="";
        if(agentcom.equals("1"))
        {
            tSQL = "select count(distinct b.contno) from LJAPayPerson b where   b.payType = 'ZC' " +
            "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " +

            " and b.agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
            " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
            " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
        }
        else
        tSQL = "select count(distinct b.contno) from LJAPayPerson b " +
                "  where  b.payType = 'ZC' " +
                "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000'" +
                        " and b.agentcom like '"+agentcom+"%' " ;
        tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ) " ;
        tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            if(mPayIntv.equals("0"))
                tSQL += "and c.payintv = "+mPayTime+"" ;
            else
                tSQL += " and c.payintv <> "+mPayTime+"";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ))";
        if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
        {
            tSQL += " and b.riskcode = '"+mRiskCode+"'";
        }

            tSQL += " and b.managecom like '"+mCode+"%'";


       try{
           CBNum = (int) execQuery(tSQL);
       }catch(Exception ex)
       {
           System.out.println("getNum 出错！");
       }
        return CBNum;
    }


    /**
    * 年月承保件数
    */
   private int getSumNum(String mCode,String mDate,String mType)
   {
       int CBNum = 0;
       String tSQL ="";
       String tSQL1 ="";
      String mDate1="";
      String mDate2="";
      if(mType.equals("1"))
       {
           mDate2 = mDate;
           mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
           tSQL1 = "select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                   mDate + "";

           SSRS tSSRS = new SSRS();
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(tSQL1);
           mDate1 = tSSRS.GetText(1, 1);

       }
       else  if (mType.equals("2"))
       {
           mDate2=mDate;
            mDate = mDate.substring(0, 4);
            mDate1=mDate+"-01-01";

        }
     tSQL = "select count(distinct b.contno) from LJAPayPerson b where   b.payType = 'ZC' " +
      "and  b.MakeDate>='"+mDate1+"' and b.makedate<='"+mDate2+"'  and b.GrpPolNo = '00000000000000000000' ";
      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ) " ;
       tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           if(mPayIntv.equals("0"))
               tSQL += "and c.payintv = "+mPayTime+"" ;
           else
               tSQL += " and c.payintv <> "+mPayTime+"";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ))";
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
           tSQL += " and b.riskcode = '"+mRiskCode+"'";
       }

           tSQL += " and b.managecom like '"+mCode+"%'";
     try{
          CBNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getSumNum 出错！");
      }
       return CBNum;
   }

    /**
     * 撤保件数
     */
  private   int getWTNum(String mCode,String agentcom)
  {
      int WTNum = 0;
      String tSQL ="";
      if(agentcom.equals("1"))
      {
          tSQL = "select count(distinct b.contno) from LJAGetEndorse b  " +
                "where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF'" +
          " and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " +

          " and b.agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
          " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
          " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
      }
      else
      tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
            " where b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
              " and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " +
                    "and b.agentcom like '"+agentcom+"%'" ;
      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";
      if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
      {
          tSQL += " and b.riskcode = '"+mRiskCode+"'";
      }


          tSQL += " and b.managecom like '"+mCode+"%'";


     try{
         WTNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getWTNum 出错！");
     }
      return WTNum;
  }
  /**
    *年月 撤保件数
    */
 private   int getWTSumNum(String mCode,String mDate,String mType)
 {
     int WTNum = 0;
     String tSQL ="";
     String tSQL1 ="";
     String mDate1="";
     String mDate2="";
     if(mType.equals("1"))
       {
           mDate2 = mDate;
           mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
           tSQL1 ="select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                   mDate + "";

           SSRS tSSRS = new SSRS();
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(tSQL1);
           mDate1 = tSSRS.GetText(1, 1);
       }
       else  if (mType.equals("2"))
       {
           mDate2=mDate;
             mDate = mDate.substring(0, 4);
             mDate1=mDate+"-01-01";

       }
      tSQL = "select count(distinct b.contno) from LJAGetEndorse b  " +
           "where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF'" +
         " and  b.MakeDate>='"+mDate1+"' and b.makedate<='"+mDate2+"'  and b.GrpPolNo = '00000000000000000000' " ;
    tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
     if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
     {
         if(mPayIntv.equals("0"))
             tSQL += "and c.payintv = "+mPayTime+"" ;
         else
             tSQL += " and c.payintv <> "+mPayTime+"";
     }
     if(this.mPayYears!=null&&!this.mPayYears.equals(""))
     {
         if(this.mPayYears.equals("4"))
             tSQL += " and c.payyears > "+this.mpayyears+"";
            else
                tSQL += " and c.payyears = "+this.mpayyears+"";
     }
     tSQL += " ) " ;
     tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
     if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
     {
         if(mPayIntv.equals("0"))
             tSQL += "and c.payintv = "+mPayTime+"" ;
         else
             tSQL += " and c.payintv <> "+mPayTime+"";
     }
     if(this.mPayYears!=null&&!this.mPayYears.equals(""))
     {
         if(this.mPayYears.equals("4"))
             tSQL += " and c.payyears > "+this.mpayyears+"";
            else
                tSQL += " and c.payyears = "+this.mpayyears+"";
     }
     tSQL += " ))";
     if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
     {
         tSQL += " and b.riskcode = '"+mRiskCode+"'";
     }


         tSQL += " and b.managecom like '"+mCode+"%'";


    try{
        WTNum = (int) execQuery(tSQL);
    }catch(Exception ex)
    {
        System.out.println("getWTSumNum 出错！");
    }
     return WTNum;
 }

  /**
   * 退保保费
   */
  private String getTBPrem(String mCode,String agentcom)
  {
      String TBPrem = "";
      String tSQL ="";
      DecimalFormat tDF = new DecimalFormat("0.######");
      if(agentcom.equals("1"))
      {
          tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
                " where b.FeeOperationType in('CT','XT') AND b.FEEFINATYPE='TF'" +
          " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" +

                  " and b.agentcom not in(select agentcom from lacom where agentcom like 'PY001%' or " +
                  " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
                  " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
      }
      else
       tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
            " where b.FeeOperationType in('CT','XT') AND b.FEEFINATYPE='TF' " +
              " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" +
                    " and b.agentcom like '"+agentcom+"%'" ;
      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
           tSQL += " and b.riskcode = '"+mRiskCode+"'";
       }

           tSQL += " and b.managecom like '"+mCode+"%'";


      try{
          TBPrem = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getTBPrem 出错！");
      }

      return TBPrem;
  }
  /**
   * 年月 退保保费
   */
  private String getTBSumPrem(String mCode,String mDate,String mType)
  {
      String TBPrem = "";
      String tSQL ="";
      String tSQL1 ="";
      String mDate1="";
      String mDate2="";
      if(mType.equals("1"))
      {
          mDate2 =mDate;
          mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
          tSQL1 = "select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                  mDate + "";

          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(tSQL1);
          mDate1 = tSSRS.GetText(1, 1);

      }
      else  if (mType.equals("2"))
      {
          mDate2=mDate;
            mDate = mDate.substring(0, 4);
            mDate1=mDate+"-01-01";

      }
     DecimalFormat tDF = new DecimalFormat("0.######");
      tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
                " where b.FeeOperationType in('CT','XT') AND b.FEEFINATYPE='TF'" +
          " and b.makedate >='"+mDate1+"' and b.makedate<='"+mDate2+"' and b.GrpPolNo = '00000000000000000000'";
     tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
           tSQL += " and b.riskcode = '"+mRiskCode+"'";
       }

           tSQL += " and b.managecom like '"+mCode+"%'";


      try{
          TBPrem = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getTBSumPrem 出错！");
      }

      return TBPrem;
  }

  /**
   * 退保件数
   */
  private int TBNum(String mCode,String agentcom)
  {
      int TBNum = 0;
      String tSQL ="";
      if(agentcom.equals("1"))
      {
          tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
                "  where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
          "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " +

          " and b.agentcom not in (select agentcom from lacom  where agentcom like 'PY001%' or " +
          " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
          " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
      }
      else
      {
      tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
            "  where b.FeeOperationType='WT' AND b.FEEFINATYPE='TF'  " +
              " and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " +
                    " and b.agentcom like '"+agentcom+"%'" ;

      }
      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          if(mPayIntv.equals("0"))
              tSQL += "and c.payintv = "+mPayTime+"" ;
          else
              tSQL += " and c.payintv <> "+mPayTime+"";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";
      if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
      {
          tSQL += " and b.riskcode = '"+mRiskCode+"'";
      }

          tSQL += " and b.managecom like '"+mCode+"%'";


     try{
         TBNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("TBNum 出错！");
     }
      return TBNum;
  }

  /**
  * 年月退保件数
  */
 private int TBSumNum(String mCode,String mDate,String mType)
 {
     int TBNum = 0;
     String tSQL ="";
     String tSQL1 ="";
    String mDate1="";
    String mDate2="";
    if(mType.equals("1"))
      {
          mDate2 = mDate;
          mDate = mDate.substring(0, 4) + mDate.substring(5, 7);
          tSQL1 =
                  "select startdate,enddate from lastatsegment where stattype='1' and yearmonth =" +
                  mDate + "";

          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(tSQL1);
          mDate1 = tSSRS.GetText(1, 1);

      }
      else  if (mType.equals("2"))
      {
          mDate2=mDate;
            mDate = mDate.substring(0, 4);
            mDate1=mDate+"-01-01";


      }



         tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
               "  where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
         "and  b.MakeDate>='"+mDate1+"' and b.makedate<='"+mDate2+"'  and b.GrpPolNo = '00000000000000000000' ";

     tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;
     if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
     {
         if(mPayIntv.equals("0"))
             tSQL += "and c.payintv = "+mPayTime+"" ;
         else
             tSQL += " and c.payintv <> "+mPayTime+"";
     }
     if(this.mPayYears!=null&&!this.mPayYears.equals(""))
     {
         if(this.mPayYears.equals("4"))
             tSQL += " and c.payyears > "+this.mpayyears+"";
            else
                tSQL += " and c.payyears = "+this.mpayyears+"";
     }
     tSQL += " ) " ;
     tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
     if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
     {
         if(mPayIntv.equals("0"))
             tSQL += "and c.payintv = "+mPayTime+"" ;
         else
             tSQL += " and c.payintv <> "+mPayTime+"";
     }
     if(this.mPayYears!=null&&!this.mPayYears.equals(""))
     {
         if(this.mPayYears.equals("4"))
             tSQL += " and c.payyears > "+this.mpayyears+"";
            else
                tSQL += " and c.payyears = "+this.mpayyears+"";
     }
     tSQL += " ))";
     if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
     {
         tSQL += " and b.riskcode = '"+mRiskCode+"'";
     }

         tSQL += " and b.managecom like '"+mCode+"%'";


    try{
        TBNum = (int) execQuery(tSQL);
    }catch(Exception ex)
    {
        System.out.println("TBNum 出错！");
    }
     return TBNum;
 }

  /**
   * 网点数量
   */
  private int AgentcomNum(String mCode,String agentcom)
  {
      int AgentcomNum = 0;
      String tSQL ="";
      if(agentcom.equals("1"))
      {
          tSQL = "select count(b.agentcom) from lacom b where b.branchtype='3' and b.branchtype2='01' " +
          		" and (enddate is null or enddate>'"+this.mEndDate+"') and " +
          " agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
          " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
          " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
      }
      else
      tSQL = "select count(b.agentcom) from lacom b where b.branchtype='3' and b.branchtype2='01'" +
      		" and (enddate is null or enddate>'"+this.mEndDate+"') and b.agentcom like '"+agentcom+"%'";

          tSQL += " and b.managecom like '"+mCode+"%' and makedate<='"+this.mEndDate+"'";


     try{
         AgentcomNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("AgentcomNum 出错！");
     }
      return AgentcomNum;
  }
  /**
   * 有效网点数量
   */
  private int YXAgentcomNum(String mCode,String agentcom)
  {
      int YXAgentcomNum = 0;
      String tSQL ="";
      if(agentcom.equals("1"))
      {
          tSQL = "select count(distinct(agentcom)) from LJAPayPerson b " +
          " where b.grpcontno='00000000000000000000' and  b.makedate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'" +
                  " and (exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04')" +
                  " or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))" +
                  " and b.agentcom not in(select agentcom from lacom  where agentcom like 'PY001%' or " +
                  " agentcom like 'PY005%' or agentcom like 'PY002%' or " +
                  " agentcom like 'PY004%' or agentcom like 'PY009%' or agentcom like 'PY015%' )";
      }
      else
      {
      tSQL = "select count(distinct(agentcom)) from LJAPayPerson b " +
            "where b.grpcontno='00000000000000000000' and  b.makedate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'" +
                    " and b.agentcom like '"+agentcom+"%'" +
                    " and (exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04')" +
                    " or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))";
      }
          tSQL += " and b.managecom like '"+mCode+"%'";


     try{
         YXAgentcomNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("YXAgentcomNum 出错！");
     }
      return YXAgentcomNum;
  }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mLevel = (String)cInputData.get(0);
        System.out.println("统计层级mLevel:"+mLevel);
        managecom=this.mGlobalInput.ManageCom;
        System.out.println("统计层级managecom:"+managecom);
        mRiskCode = (String)cInputData.get(1);
        mStartDate = (String)cInputData.get(2);
        mEndDate = (String)cInputData.get(3);
        mPayIntv = (String)cInputData.get(4);
        mPayYears = (String)cInputData.get(5);
       if(mPayYears!=null&&!mPayYears.equals(""))
       {
        if(mPayYears.equals("0"))
            mpayyears = 3;
        else if(mPayYears.equals("1"))
            mpayyears = 5;
        else if(mPayYears.equals("2"))
            mpayyears = 8;
        else if(mPayYears.equals("3"))
            mpayyears = 10;
        else
            mpayyears = 10;
       }
       System.out.println("mPayTime="+mPayTime);
       System.out.println("mpayyears="+mpayyears);
        return true;
    }
    /**
     * 得到登录机构的名称
     */
    private String getName()
    {
        String name = "";
        String SQL = "select name from ldcom where comcode = '"+this.mGlobalInput.ManageCom+"'";
        ExeSQL tExeSQL = new ExeSQL();
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LAChannelReport";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败--->getName()";
            this.mErrors.addOneError(tError);
            return "";
        }
        name = tExeSQL.getOneValue(SQL);
        return name;
    }
    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

        }
      }
    public VData getResult()
    {
        return mResult;
    }
}
