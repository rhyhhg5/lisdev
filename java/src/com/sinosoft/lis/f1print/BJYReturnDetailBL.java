package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.adventnet.aclparser.ParseException;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BJYReturnDetailBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;
    private String mOutXmlPath = null;

    private String StartDate = "";
    
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public BJYReturnDetailBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");
 
            ExeSQL tExeSQL = new ExeSQL();
            mSql ="Select char(rownumber() over()) as id, bb.* From (select distinct(select codename from ficodetrans where codetype = 'ManageCom'and code = c.managecom) 三级机构名称," 
         	   +"(select ProjectName from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目名称," 
         	   +"a.contno 保单号,a.riskcode 险种,a.costcenter 成本中心,a.summoney as 金额,substr(a.accountdate,1,7) as 入账月份,"
         	   +"(select voucherid from interfacetable where readstate = '2'and ImportType = a.classtype and  depcode = a.managecom and batchno = a.batchno and accountcode= a.accountcode and  chargedate = a.accountdate and vouchertype = 'X4' fetch first 1 rows only )as SAP流转凭证号,"
         	   +"(case c.CoInsuranceFlag when '1' then '是' else '否' end ) as 是否共保"           
         	   +" From fivoucherdatadetail a,fiabstandarddata b,lcgrpcont c "
	           +"where 1=1 and a.serialno = b.serialno and a.contno=c.grpcontno and b.feetype = 'BJ' and a.accountcode in ('6031000000','6031000101','6031000301') and a.vouchertype = 'X4'"
	           +" and a.summoney <> 0 and a.accountdate between '"+StartDate+"'and '"+EndDate+"' and a.checkflag = '00' " 	
	           +" union all"
	           +" select distinct(select codename from ficodetrans where codetype = 'ManageCom'and code = c.managecom) 三级机构名称," 
         	   +"(select ProjectName from db2inst1.lcgrpcontsub where prtno=c.prtno and ProjectNo is not null and ProjectNo <> '') 项目名称," 
         	   +"a.contno 保单号,a.riskcode 险种,a.costcenter 成本中心,a.summoney as 金额,substr(a.accountdate,1,7) as 入账月份,"
         	   +"(select voucherid from interfacetable where readstate = '2'and ImportType = a.classtype and  depcode = a.managecom and batchno = a.batchno and accountcode= a.accountcode and  chargedate = a.accountdate and vouchertype = 'X4' fetch first 1 rows only )as SAP流转凭证号,"
         	   +"(case c.CoInsuranceFlag when '1' then '是' else '否' end ) as 是否共保"           
         	   +" From fivoucherdatadetail a,fiabstandarddata b,lbgrpcont c "
	           +"where 1=1 and a.serialno = b.serialno and a.contno=c.grpcontno and b.feetype = 'BJ' and a.accountcode in ('6031000000','6031000101','6031000301') and a.vouchertype = 'X4'"
	           +" and a.summoney <> 0 and a.accountdate between '"+StartDate+"'and '"+EndDate+"' and a.checkflag = '00' " 
               + ")as bb ";  
            System.out.println(mSql);
//            SSRS ttSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(tSSRS.getMaxRow()<=0)
            {
            	buiError("dealData","没有符合条件的信息!");
                return false;
            }
            
            
        return true;
    }
    
   
    
	private boolean createFile(){
		
//		 1、查看数据行数
	  	  int dataSize = this.tSSRS.getMaxRow();
	  	  if(this.tSSRS==null||this.tSSRS.getMaxRow()<1)
	  	  {
	  		  buiError("dealData","没有符合条件的信息!");
	  		  return false;
	  	  }
	        // 定义一个二维数组 
	  	   String[][] mToExcel = new String[dataSize + 6][30];
//	       mToExcel = new String[10000][30];
	        mToExcel[0][4] = "实际结余返还统计明细表";
	        mToExcel[1][6] = StartDate.substring(0, EndDate.indexOf('-'))+ "年"+ StartDate.substring(StartDate.indexOf('-') + 1, StartDate.lastIndexOf('-')) + "月至"+EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月";
//	        mToExcel[1][6] = AccountDate+"结余返还统计表";
	        mToExcel[2][8] = "编制单位：" + getName();

	        mToExcel[3][0] = "序号";
	        mToExcel[3][1] = "三级机构名称";
	        mToExcel[3][2] = "项目名称";
	        mToExcel[3][3] = "保单号";
	        mToExcel[3][4] = "险种";
	        mToExcel[3][5] = "成本中心";
	        mToExcel[3][6] = "金额";
	        mToExcel[3][7] = "入账月份";
	        mToExcel[3][8] = "SAP流转凭证号";
	        mToExcel[3][9] = "是否共保";
      
      int printNum = 3;
      int no = 1;
      for (int row = 1; row <= tSSRS.getMaxRow(); row++)
      {
          for (int col = 1; col <= tSSRS.getMaxCol(); col++)
          {                   
                  if (tSSRS.GetText(row, col).equals("null"))
                  {
                      mToExcel[row + printNum][col - 1] = "";
                  }
                  else
                  {
                      mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
                  }
              }
        
          mToExcel[row + printNum][0] = no + "";
          no++;}           

  mToExcel[printNum + tSSRS.getMaxRow() + 2][0] = "制表";
  mToExcel[printNum + tSSRS.getMaxRow() + 2][14] = "日期：" + mCurDate;
		
  
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        StartDate = (String) tf.getValueByName("StartDate");      
        EndDate = (String) tf.getValueByName("EndDate");
        mMail = (String) tf.getValueByName("toMail");
        
        System.out.println("mMail-----"+mMail);

        if (EndDate == null || mOutXmlPath == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    
    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "BJYReturnDetailBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}

