package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author GN
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LHScanInfoPrtBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据容器 */
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public LHScanInfoPrtBL()
    {
        try { jbInit(); }
        catch (Exception ex) { ex.printStackTrace(); }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
        if (mTransferData == null)
        {
            buildError("getInputData", "没有得到足够的信息！!-");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LHScanInfoPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String SerialNo = (String) mTransferData.getValueByName("SerialNo");
        String CustomerNo = (String) mTransferData.getValueByName("CustomerNo");
        String CustomerName = (String) mTransferData.getValueByName("CustomerName");

        //其它模版上单独不成块的信息
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LHScanInfo.vts", "printer"); //最好紧接着就初始化xml文档

        //LHScanInfoPrtBL模版元素
        texttag.add("CustomerNo", CustomerNo);
        texttag.add("CustomerName", CustomerName);
        texttag.add("BarCode1", SerialNo);
        texttag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {}
    private void jbInit() throws Exception {}
}
