package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-04-04
 * @function print InputEfficiency or print CheckEfficiency User Interface Layer
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PEfficiencyUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strStartDate;
    private String strEndDate;
    private String strOperateFlag;

    public PEfficiencyUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            if (!getInputData(cInputData))
            {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            PEfficiencyBL tPEfficiencyBL = new PEfficiencyBL();
            System.out.println("Start PEfficiency UI Submit ...");
            if (!tPEfficiencyBL.submitData(vData, cOperate))
            {
                if (tPEfficiencyBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPEfficiencyBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "PEfficiencyBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPEfficiencyBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PEfficiencyUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    private boolean prepareOutputData(VData vData)
    {
        vData.clear();
        vData.add(strStartDate);
        vData.add(strEndDate);
        vData.add(strOperateFlag);
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        strStartDate = (String) cInputData.get(0);
        strEndDate = (String) cInputData.get(1);
        strOperateFlag = (String) cInputData.get(2);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinChargeDayModeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
    }
}