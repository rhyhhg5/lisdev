/*
 * <p>ClassName: LAChnlSumReportBL </p>
 * <p>Description: LAChnlSumReportBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * <p>author: xx </p>
 * @CreateDate：2008-04-17
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.agentprint.LISComparator;
import java.util.Arrays;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.pubfun.FDate;

public class LAChnlSumReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  manageCom= "";
    //统计管理机构
    private String  mManageCom= "";
    //统计三级管理机构
    private String  mManageCom3= "";
    //最终统计管理机构
    private String  mManageCom4= "";
    //统计起期
    private String  mStartDate= "";
    //统计止期
    private String  mEndDate = "";

    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
   public LAChnlSumReportBL() {
    }

   public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
   public boolean submitData(VData cInputData, String cOperate) {
     //将操作数据拷贝到本类中
     if (!cOperate.equals("PRINT"))
     {
         buildError("submitData", "不支持的操作字符串");
         return false;
     }
     System.out.println("dealData:" + mOperate);

     //得到外部传入的数据,将数据备份到本类中
     if (!getInputData(cInputData)) {
         return false;
     }
     System.out.println("dealData:" + mOperate);

     System.out.println("开始处理数据，渠道综合报表");
     //进行业务处理
     if (!dealData()) {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LAChnlSumReportBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据处理失败LAChnlSumReportBL-->dealData!";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
    }

   private void buildError(String szFunc, String szErrMsg)
    {
      CError cError = new CError();
      cError.moduleName = "LAChnlSumReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
    }
   /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
   private boolean dealData() {
     try{
                // 查询数据
        if(!getDataList())
        {
            return false;
        }
        System.out.println(mShowDataList.length);
        System.out.println(mShowDataList[0].length);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Order");
        this.mDataList = new String[mShowDataList[0].length];
 /**
         // 2、进行排序  进行保费排序
          LISComparator tLISComparator = new LISComparator();
          tLISComparator.setNum(1);
           Arrays.sort(mShowDataList,tLISComparator);
              for(int j=1;j<mShowDataList.length;j++)
              {// 追加序号
               if((mShowDataList[j][1]).equals(mShowDataList[j-1][1]))
                  {
                      mShowDataList[j][2] = mShowDataList[j-1][2];
                  }
                  else
                      mShowDataList[j][2] = "" + (j + 1);
              }
*/
        for(int i=0;i<mShowDataList.length;i++){
          tlistTable.add(mShowDataList[i]);
          }

              // 3、追加 合计 行
        if(!setAddRow(mShowDataList.length + 1))
          {
          buildError("queryData", "进行合计计算时出错！");
          return false;
          }
        else  { tlistTable.add(mDataList);  }


         //新建一个TextTag的实例
        TextTag  tTextTag = new TextTag();

        System.out.print("dayin175");
        tTextTag.add("MakeDate", currentDate);
        tTextTag.add("MakeTime", currentTime);
        tTextTag.add("StartDate",mStartDate);
        tTextTag.add("EndDate",mEndDate);


        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LAChnlSumReport.vts", "printer"); //初始化xml文档
        if (tTextTag.size() > 0)
        xmlexport.addTextTag(tTextTag);     //添加动态文本标签
        xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
        xmlexport.outputDocumentToFile("c:\\", "new1");
        //this.mResult.clear();
        mResult.addElement(xmlexport);
        System.out.print("22222222222222222");
        return true;
        }catch(Exception ex)
        {
               buildError("queryData", "LAChnlSumReportBL发生错误，准备数据时出错！");
               return false;
      }
    }


   /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     manageCom=this.mGlobalInput.ManageCom;
     System.out.println("登陆机构为:"+manageCom);
     mManageCom = (String)cInputData.get(0);
     System.out.println("二级管理机构mManageCom:"+mManageCom);
     mManageCom3 = (String)cInputData.get(1);
     System.out.println("三级管理机构为:"+mManageCom3);
     mStartDate = (String)cInputData.get(2);
     mEndDate = (String)cInputData.get(3);
     System.out.println("统计区间为："+mStartDate+"/"+mEndDate);
     if(mManageCom.equals("")&&mManageCom3.equals(""))
     {
      mManageCom4="86";
     }
     if(!mManageCom.equals("")&&mManageCom3.equals(""))
     {
      mManageCom4=mManageCom;
     }
     if(!mManageCom3.equals(""))
     {
      mManageCom4=mManageCom3;
     }

     System.out.println("统计管理机构为："+mManageCom4);
     return true;
   }




   private boolean getDataList()
   {
      //新单用签单日期signdate，撤单用撤单日期tmakedate
      String  agentcomSQL = "select * from ldcode where codetype='bankno'  order by code ";
      System.out.println("查询渠道语句："+agentcomSQL);
      LDCodeDB tLDCodeDB = new LDCodeDB();
      LDCodeSet tLDCodeSet = new LDCodeSet();
      tLDCodeSet = tLDCodeDB.executeQuery(agentcomSQL);
      if(tLDCodeSet.size()==0)
      {
           buildError("queryData", "没有符合条件的数据！");
           return false;
      }
///////////////////////////////////////////////////需要变换的数字
      String[][] tShowDataList = new String[tLDCodeSet.size()][54];

      for(int i=1;i<=tLDCodeSet.size();i++)
      {//进行渠道循环
        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        tLDCodeSchema = tLDCodeSet.get(i);
        //tShowDataList[i-1][0] = tLACommisionSchema.getCommisionSN();
        //System.out.println(tLACommisionSchema.getCommisionSN()+"/"+tShowDataList[i-1][0]);

        //getDataList(managSSRS.GetText(i, 1),
        //            managSSRS.GetText(i, 2),managSSRS.GetText(i, 1),"分公司",j,Add);
        if (!queryOneDataList(tLDCodeSchema.getCodeName(),
                              tLDCodeSchema.getCode(),
                              mManageCom4,
                              i,
                              tShowDataList[i - 1]))
        {
           System.out.println("[" + tLDCodeSchema.getCodeName() +"] 数据查询失败！");
           return false;
        }
      }
      mShowDataList = tShowDataList;

      return true;
   }

   /**
   * 查询填充表示数据
   * @return boolean
   */
   private boolean queryOneDataList(String pmCodeName,String pmCode,String pmManageCom,int i,String[] pmOneDataList)
   {
      String sumSum1="";
      String sumSum2="";
      String sumSum3="";
      String sumSum4="";
      String sumSum5="";
      String sumSum6="";
      String AgentSumNum="";
      String AgentGetNum="";

      try{
           DecimalFormat tDF = new DecimalFormat("0.######");
          //1渠道
          pmOneDataList[0] = pmCodeName;
          //2、承保保费
          //非万能产品
          //周累计
          pmOneDataList[1] =getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"1","1");
          //月累计
          pmOneDataList[2] = getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"2","1");
          //年累计
          pmOneDataList[3] = getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"3","1");
          //万能产品
          //周累计
          pmOneDataList[4] =getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"1","2");
          //月累计
          pmOneDataList[5] = getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"2","2");
          //年累计
          pmOneDataList[6] =getGetPrem(pmCode,pmManageCom,mStartDate,mEndDate,"3","2");
          //3、承保件数
          //非万能产品
          //周累计
          pmOneDataList[7] =String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"1","1"));
          //月累计
          pmOneDataList[8] = String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"2","1"));
          //年累计
          pmOneDataList[9] = String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"3","1"));
          //万能产品
          //周累计
          pmOneDataList[10] =String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"1","2"));
          //月累计
          pmOneDataList[11] = String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"2","2"));
          //年累计
          pmOneDataList[12] =String.valueOf(getGetNum(pmCode,pmManageCom,mStartDate,mEndDate,"3","2"));
           //4、撤保保费
          //非万能产品
          //周累计
          pmOneDataList[13] =getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"1","1");
          //月累计
          pmOneDataList[14] = getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"2","1");
          //年累计
          pmOneDataList[15] =getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"3","1");
          //万能产品
          //周累计
          pmOneDataList[16] =getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"1","2");
          //月累计
          pmOneDataList[17] = getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"2","2");
          //年累计
          pmOneDataList[18] = getWTPrem(pmCode,pmManageCom,mStartDate,mEndDate,"3","2");
          //5、撤保件数
          //非万能产品
          //周累计
          pmOneDataList[19] =String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"1","1"));
          //月累计
          pmOneDataList[20] = String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"2","1"));
          //年累计
          pmOneDataList[21] = String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"3","1"));
          //万能产品
          //周累计
          pmOneDataList[22] =String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"1","2"));
          //月累计
          pmOneDataList[23] = String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"2","2"));
          //年累计
          pmOneDataList[24] = String.valueOf(getWTNum(pmCode,pmManageCom,mStartDate,mEndDate,"3","2"));
           //6、有效保费
          //非万能产品
          //周累计
          pmOneDataList[25] =tDF.format(Double.parseDouble(pmOneDataList[1])+Double.parseDouble(pmOneDataList[13]));
          //月累计
          pmOneDataList[26] = tDF.format(Double.parseDouble(pmOneDataList[2])+Double.parseDouble(pmOneDataList[14]));
          //年累计
          pmOneDataList[27] = tDF.format(Double.parseDouble(pmOneDataList[3])+Double.parseDouble(pmOneDataList[15]));
          //万能产品
          //周累计
          pmOneDataList[28] =tDF.format(Double.parseDouble(pmOneDataList[4])+Double.parseDouble(pmOneDataList[16]));
          //月累计
          pmOneDataList[29] = tDF.format(Double.parseDouble(pmOneDataList[5])+Double.parseDouble(pmOneDataList[17]));
          //年累计
          pmOneDataList[30] =tDF.format(Double.parseDouble(pmOneDataList[6])+Double.parseDouble(pmOneDataList[18]));
          //7、有效件数
          //非万能产品
          //周累计
          pmOneDataList[31] =String.valueOf(Integer.parseInt(pmOneDataList[7])-Integer.parseInt(pmOneDataList[19]));
          //月累计
          pmOneDataList[32] = String.valueOf(Integer.parseInt(pmOneDataList[8])-Integer.parseInt(pmOneDataList[20]));
          //年累计
          pmOneDataList[33] =String.valueOf(Integer.parseInt(pmOneDataList[9])-Integer.parseInt(pmOneDataList[21]));
          //万能产品
          //周累计
          pmOneDataList[34] =String.valueOf(Integer.parseInt(pmOneDataList[10])-Integer.parseInt(pmOneDataList[22]));
          //月累计
          pmOneDataList[35] = String.valueOf(Integer.parseInt(pmOneDataList[11])-Integer.parseInt(pmOneDataList[23]));
          //年累计
          pmOneDataList[36] =String.valueOf(Integer.parseInt(pmOneDataList[12])-Integer.parseInt(pmOneDataList[24]));
          //8、撤保率
          //撤保率=撤费率和撤件率的平均数
          //撤费率=撤保保费/承保保费    撤件率=撤保件数/承保件数
          //非万能产品
          //周累计
          pmOneDataList[37] =getAvg(getAct(pmOneDataList[13],pmOneDataList[1]),getAct(pmOneDataList[19],pmOneDataList[7]));
          //月累计
          pmOneDataList[38] =getAvg(getAct(pmOneDataList[14],pmOneDataList[2]),getAct(pmOneDataList[20],pmOneDataList[8]));
          //年累计
          pmOneDataList[39] =getAvg(getAct(pmOneDataList[15],pmOneDataList[3]),getAct(pmOneDataList[21],pmOneDataList[9]));
          //万能产品
          //周累计
          pmOneDataList[40] =getAvg(getAct(pmOneDataList[16],pmOneDataList[4]),getAct(pmOneDataList[22],pmOneDataList[10]));
          //月累计
          pmOneDataList[41] =getAvg(getAct(pmOneDataList[17],pmOneDataList[5]),getAct(pmOneDataList[23],pmOneDataList[11]));
          //年累计
          pmOneDataList[42] =getAvg(getAct(pmOneDataList[18],pmOneDataList[6]),getAct(pmOneDataList[24],pmOneDataList[12]));
           //9、渠道占比= 渠道保费/渠道累计保费
          //非万能产品
          //周累计
          sumSum1=getSumPrem(pmManageCom,mStartDate,mEndDate,"1","1");
          sumSum2=getSumWTSum(pmManageCom,mStartDate,mEndDate,"1","1");
          sumSum3=tDF.format(Double.parseDouble(sumSum1)+Double.parseDouble(sumSum2));
          System.out.println("pmOneDataList[25]:"+pmOneDataList[25]);
          System.out.println("sumSum3:"+sumSum3);
          pmOneDataList[43] =getAct(pmOneDataList[25],sumSum3);
          System.out.println("zhanbi:"+pmOneDataList[43]);
          //月累计

          sumSum4=getSumPrem(pmManageCom,mStartDate,mEndDate,"2","1");
          sumSum5=getSumWTSum(pmManageCom,mStartDate,mEndDate,"2","1");
          sumSum6=tDF.format(Double.parseDouble(sumSum4)+Double.parseDouble(sumSum5));
          pmOneDataList[44] =getAct(pmOneDataList[26],sumSum6);

          //年累计
          sumSum1=getSumPrem(pmManageCom,mStartDate,mEndDate,"3","1");
          sumSum2=getSumWTSum(pmManageCom,mStartDate,mEndDate,"3","1");
          sumSum3=tDF.format(Double.parseDouble(sumSum1)+Double.parseDouble(sumSum2));
          pmOneDataList[45] =getAct(pmOneDataList[27],sumSum3);

          //万能产品
          //周累计
          sumSum4=getSumPrem(pmManageCom,mStartDate,mEndDate,"1","2");
          sumSum5=getSumWTSum(pmManageCom,mStartDate,mEndDate,"1","2");
          sumSum6=tDF.format(Double.parseDouble(sumSum4)+Double.parseDouble(sumSum5));
          pmOneDataList[46] =getAct(pmOneDataList[28],sumSum6);

          //月累计
          sumSum1=getSumPrem(pmManageCom,mStartDate,mEndDate,"2","2");
          sumSum2=getSumWTSum(pmManageCom,mStartDate,mEndDate,"2","2");
          sumSum3=tDF.format(Double.parseDouble(sumSum1)+Double.parseDouble(sumSum2));
          pmOneDataList[47] =getAct(pmOneDataList[29],sumSum3);

          //年累计
          sumSum4=getSumPrem(pmManageCom,mStartDate,mEndDate,"3","2");
          sumSum5=getSumWTSum(pmManageCom,mStartDate,mEndDate,"3","2");
          sumSum6=tDF.format(Double.parseDouble(sumSum4)+Double.parseDouble(sumSum5));
          pmOneDataList[48] =getAct(pmOneDataList[30],sumSum6);

            //10、网点信息
         //网点数
          pmOneDataList[49] = String.valueOf(AgentcomNum(pmCode,pmManageCom,mEndDate));
          //网点占有率=一个渠道网点数/全部渠道网点数
          AgentSumNum= String.valueOf(AgentcomSumNum(pmManageCom,mEndDate));
          pmOneDataList[50] = getAct(pmOneDataList[49],AgentSumNum);
          //有效网点数=出单的网点数
          pmOneDataList[51] = String.valueOf(YXAgentcomNum(pmCode,pmManageCom,mStartDate,mEndDate));
          //网点活动率=有效网点数/一个渠道渠道网点数
          pmOneDataList[52] =getAct(pmOneDataList[51],pmOneDataList[49]);
          //网均保费=有效保费/有效网点数
          AgentGetNum=YXAgentcomSum(pmCode,pmManageCom,mStartDate,mEndDate);
          pmOneDataList[53] =getDiv(AgentGetNum,pmOneDataList[51]);

          System.out.println( pmOneDataList[0]+"个数据"+pmCodeName);

      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
   }


    /**
    * 保费
    * @param  String
    * @return String
    */
   private String getTransMoney(String pmCommisionSn)
   {
     String tSQL = "";
     String tRtValue="";
     tSQL = "select TransMoney from lacommision where CommisionSn='" + pmCommisionSn +"'";
     SSRS tSSRS = new SSRS();
     ExeSQL tExeSQL = new ExeSQL();
     tSSRS = tExeSQL.execSQL(tSQL);
     if(tSSRS.getMaxRow()==0)
      {
        tRtValue="0";
      }
      else

     tRtValue=tSSRS.GetText(1, 1);
     return tRtValue;
   }


    /**
     * 追加一条合计行
     * @return boolean
     */
    private boolean setAddRow(int pmRow)
    {
        System.out.println("合计行的行数是："+pmRow);

        mDataList[0] = "合  计";
        mDataList[1] = dealSum(1);
        mDataList[2] = dealSum(2);
        mDataList[3] = dealSum(3);
        mDataList[4] = dealSum(4);
        mDataList[5] = dealSum(5);
        mDataList[6] = dealSum(6);
        mDataList[7] = dealSum(7);
        mDataList[8] = dealSum(8);
        mDataList[9] = dealSum(9);
        mDataList[10] = dealSum(10);
        mDataList[11] = dealSum(11);
        mDataList[12] = dealSum(12);
        mDataList[13] = dealSum(13);
        mDataList[14] = dealSum(14);
        mDataList[15] = dealSum(15);
        mDataList[16] = dealSum(16);
        mDataList[17] = dealSum(17);
        mDataList[18] = dealSum(18);
        mDataList[19] = dealSum(19);
        mDataList[20] = dealSum(20);
        mDataList[21] = dealSum(21);
        mDataList[22] = dealSum(22);
        mDataList[23] = dealSum(23);
        mDataList[24] = dealSum(24);
        mDataList[25] = dealSum(25);
        mDataList[26] = dealSum(26);
        mDataList[27] = dealSum(27);
        mDataList[28] = dealSum(28);
        mDataList[29] = dealSum(29);
        mDataList[30] = dealSum(30);
        mDataList[31] = dealSum(31);
        mDataList[32] = dealSum(32);
        mDataList[33] = dealSum(33);
        mDataList[34] = dealSum(34);
        mDataList[35] = dealSum(35);
        mDataList[36] = dealSum(36);
        mDataList[37] = dealSum(37);
        mDataList[38] = dealSum(38);
        mDataList[39] = dealSum(39);
        mDataList[40] = dealSum(40);
        mDataList[41] = dealSum(41);
        mDataList[42] = dealSum(42);
        mDataList[43] = dealSum(43);
        mDataList[44] = dealSum(44);
        mDataList[45] = dealSum(45);
        mDataList[46] = dealSum(46);
        mDataList[47] = dealSum(47);
        mDataList[48] = dealSum(48);
        mDataList[49] = dealSum(49);
        mDataList[50] = "100";
        mDataList[51] = dealSum(51);
        mDataList[52] = dealSum(52);
        mDataList[53] = dealSum(53);

        return true;
    }

    /**
      * 对传入的数组进行求和处理
      * @param pmArrNum int
      * @return String
      */
    private String dealSum(int pmArrNum)
    {
      String tReturnValue = "";
      DecimalFormat tDF = new DecimalFormat("0.######");
      String tSQL = "select 0";

      for(int i=0;i<this.mShowDataList.length;i++)
      {
          tSQL += " + " + this.mShowDataList[i][pmArrNum];
      }

      tSQL += " + 0 from dual";

      tReturnValue = "" + tDF.format(execQuery(tSQL));

      return tReturnValue;
    }


    /**
     * 承保保费查询
     * agentcom-渠道
     * startdate-起始时间
     * enddate-终止时间
     * mflag-统计标记 1-周累计 2-月累计 3-年累计
     * mType-统计类型 1-非万能 2-万能
     */
    private String getGetPrem(String pmAgentCom,String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
      String Prem = "";
      String tSQL ="";
      DecimalFormat tDF = new DecimalFormat("0.######");
      //周累计=统计起期至统计止期
      if(pmFlag.equals("1"))
      {
          StartDate=StartDate;
          /**
          String tempt = "";
          String mSQL = "select date('" + EndDate + "')-7 day from dual";
          SSRS tSSRS = new SSRS();
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(mSQL);
          tempt = tSSRS.GetText(1, 1);
           FDate fd = new FDate();
          if (fd.getDate(StartDate).compareTo(fd.getDate(tempt)) < 0) {
              StartDate = tempt;
           }*/


      }
      //月累计
      else if (pmFlag.equals("2"))
      {
         StartDate=EndDate.substring(0,7)+"-01";
              /**
         String tempt =StartDate.substring(0,7);
         String tempt1 =EndDate.substring(0,7);
         if(!tempt.equals(tempt1))
         {
             StartDate=EndDate.substring(0,7)+"-01";
         }
         */

       }
        //年累计
      else if (pmFlag.equals("3"))
      {
         StartDate=EndDate.substring(0,4)+"-01-01";
       //String tempt =StartDate.substring(0,4);
       //  String tempt1 =EndDate.substring(0,4);
       // if(!tempt.equals(tempt1))
       // {
       //    StartDate=EndDate.substring(0,4)+"-01-01";
       //}
       // else
       //  StartDate=EndDate.substring(0,4)+"-01-01";
      }

       tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b where b.payType='ZC'"
              + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
              +"' and b.GrpPolNo = '00000000000000000000' "
              +"  and b.managecom like '"+pmManageCom+"%' "
              +" and b.agentcom like '"+pmAgentCom+"%' "
              + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
              + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
      if(pmType.equals("1"))
      {
           tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
      }
      else if (pmType.equals("2"))
      {
          tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
      }

      try{
          Prem = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getGetPrem 出错！");
      }

      return Prem;
    }


    /**
        * 承保件数查询
        * agentcom-渠道
        * startdate-起始时间
        * enddate-终止时间
        * mflag-统计标记 1-周累计 2-月累计 3-年累计
        * mType-统计类型 1-非万能 2-万能
        */
    private  int getGetNum(String pmAgentCom,String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
      int CBNum = 0;
      String tSQL ="";

      //周累计
      if(pmFlag.equals("1"))
      {
          StartDate=StartDate;
          }
      //月累计
      else if (pmFlag.equals("2"))
      {
         StartDate=EndDate.substring(0,7)+"-01";
      }
       //年累计
       else if (pmFlag.equals("3"))
      {
          StartDate=EndDate.substring(0,4)+"-01-01";
      }

       tSQL = "select count(distinct b.contno)  from LJAPayPerson b where b.payType='ZC'"
              + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
              +"' and b.GrpPolNo = '00000000000000000000' "
              +" and b.managecom like '"+pmManageCom+"%' "
              +" and b.agentcom like '"+pmAgentCom+"%' "
              + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
              + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
      if(pmType.equals("1"))
      {
           tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
      }
      else if (pmType.equals("2"))
      {
          tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
      }

      try{
          CBNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getGetNum 出错！");
      }

      return CBNum;
    }

    /**
        * 撤保保费查询
        * agentcom-渠道
        * startdate-起始时间
        * enddate-终止时间
        * mflag-统计标记 1-周累计 2-月累计 3-年累计
        * mType-统计类型 1-非万能 2-万能
        */
    private String getWTPrem(String pmAgentCom,String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
      String Prem = "";
      String tSQL ="";
      DecimalFormat tDF = new DecimalFormat("0.######");
      //周累计
       if(pmFlag.equals("1"))
       {
           StartDate=StartDate;
           }
       //月累计
       else if (pmFlag.equals("2"))
       {
          StartDate=EndDate.substring(0,7)+"-01";
       }
        //年累计
        else if (pmFlag.equals("3"))
       {
           StartDate=EndDate.substring(0,4)+"-01-01";
       }


       tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b "
              +" where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' "
              + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
              +"' and b.GrpPolNo = '00000000000000000000' "
              +" and b.managecom like '"+pmManageCom+"%' "
              +" and b.agentcom like '"+pmAgentCom+"%' "
              + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
              + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
       if(pmType.equals("1"))
       {
            tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
       }
       else if (pmType.equals("2"))
       {
           tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
       }

       try{
           Prem = tDF.format(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getWTPrem 出错！");
       }

       return Prem;
    }

    /**
              * 撤保件数查询
              * agentcom-渠道
              * startdate-起始时间
              * enddate-终止时间
              * mflag-统计标记 1-周累计 2-月累计 3-年累计
              * mType-统计类型 1-非万能 2-万能
       */
    private  int getWTNum(String pmAgentCom,String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
       int WTNum = 0;
       String tSQL ="";

       //周累计
      if(pmFlag.equals("1"))
      {
          StartDate=StartDate;
          }
      //月累计
      else if (pmFlag.equals("2"))
      {
         StartDate=EndDate.substring(0,7)+"-01";
      }
       //年累计
       else if (pmFlag.equals("3"))
      {
          StartDate=EndDate.substring(0,4)+"-01-01";
      }


       tSQL = "select count(distinct b.contno) from LJAGetEndorse b  "
              +"  where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' "
              + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
              +"' and b.GrpPolNo = '00000000000000000000' "
              +" and b.managecom like '"+pmManageCom+"%' "
              +" and b.agentcom like '"+pmAgentCom+"%' "
              + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
              + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
       if(pmType.equals("1"))
       {
            tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
       }
       else if (pmType.equals("2"))
       {
           tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
       }

       try{
           WTNum = (int) execQuery(tSQL);
       }catch(Exception ex)
       {
           System.out.println("getWTNum 出错！");
       }

       return WTNum;
    }

    /**
    * 计算平均数
    * @param pmParaA String
    * @param pmParaB String
    * @return String
        */
    private String getAvg(String pmParaA,String pmParaB)
    {
       String tSQL = "";
       String tRtValue = "";
       DecimalFormat tDF = new DecimalFormat("0.##");


       tSQL = "select DECIMAL((DECIMAL(" + pmParaA + ",12,2) + DECIMAL(" +
              pmParaB + ",12,2))/2 ,12,2) from dual";

       try{
           tRtValue = "" + tDF.format(execQuery(tSQL));

       }catch(Exception ex)
       {
           System.out.println("getEqual 出错！");
       }

       return tRtValue;
    }

    /**
    * 计算比率
    * @param pmParaA String
    * @param pmParaB String
    * @return String
       */
    private String getAct(String pmParaA,String pmParaB)
    {
       String tSQL = "";
       String tRtValue = "";
       DecimalFormat tDF = new DecimalFormat("0.##");

       if("0".equals(pmParaB))
       {
           return "0";
       }
       pmParaA=String.valueOf(Math.abs(Float.parseFloat(pmParaA)));
       tSQL = "select DECIMAL(DECIMAL(" + pmParaA + "* 100,12,2) / DECIMAL(" +
              pmParaB + ",12,2) ,12,2) from dual";

       try{
           tRtValue = "" + tDF.format(execQuery(tSQL));

       }catch(Exception ex)
       {
           System.out.println("getAct 出错！");
       }

       return tRtValue;
    }

    /**
    * 计算比值
    * @param pmParaA String
    * @param pmParaB String
    * @return String
    */
    private String getDiv(String pmParaA,String pmParaB)
    {
       String tSQL = "";
       String tRtValue = "";
       DecimalFormat tDF = new DecimalFormat("0.##");

       if("0".equals(pmParaB))
       {
           return "0";
       }

       tSQL = "select DECIMAL(DECIMAL(" + pmParaA + ",12,2) / DECIMAL(" +
              pmParaB + ",12,2) ,12,2) from dual";

       try{
           tRtValue = "" + tDF.format(execQuery(tSQL));

       }catch(Exception ex)
       {
           System.out.println("getDiv 出错！");
       }

       return tRtValue;
    }



     /**
     * 承保保费查询
     * agentcom-渠道
     * startdate-起始时间
     * enddate-终止时间
     * mflag-统计标记 1-周累计 2-月累计 3-年累计
     * mType-统计类型 1-非万能 2-万能
     */
    private String getSumPrem(String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
       String Prem = "";
       String tSQL ="";
       DecimalFormat tDF = new DecimalFormat("0.######");
       //周累计
     if(pmFlag.equals("1"))
     {
         StartDate=StartDate;
         }
     //月累计
     else if (pmFlag.equals("2"))
     {
        StartDate=EndDate.substring(0,7)+"-01";
     }
      //年累计
      else if (pmFlag.equals("3"))
     {
         StartDate=EndDate.substring(0,4)+"-01-01";
     }


       tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b where 1=1 "
              + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
              +"' and b.GrpPolNo = '00000000000000000000' "
              +" and b.managecom like '"+pmManageCom+"%' "
              + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
              + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
       if(pmType.equals("1"))
       {
            tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
       }
       else if (pmType.equals("2"))
       {
           tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
       }

       try{
           Prem =""+ tDF.format(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getGetPrem 出错！");
       }

       return Prem;
    }


    /**
        * 全部件数查询
        * agentcom-渠道
        * startdate-起始时间
        * enddate-终止时间
        * mflag-统计标记 1-周累计 2-月累计 3-年累计
        * mType-统计类型 1-非万能 2-万能
        */
    private  String getSumWTSum(String pmManageCom,String StartDate,String EndDate,String pmFlag,String pmType)
    {
        String Prem = "";
        String tSQL ="";
        DecimalFormat tDF = new DecimalFormat("0.######");
        //周累计
      if(pmFlag.equals("1"))
      {
          StartDate=StartDate;
          }
      //月累计
      else if (pmFlag.equals("2"))
      {
         StartDate=EndDate.substring(0,7)+"-01";
      }
       //年累计
       else if (pmFlag.equals("3"))
      {
          StartDate=EndDate.substring(0,4)+"-01-01";
      }


         tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b "
                +" where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' "
                + " and b.makedate >='"+StartDate+"' and b.makedate<='"+EndDate
                +"' and b.GrpPolNo = '00000000000000000000' "
                +" and b.managecom like '"+pmManageCom+"%' "
                + " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04')"
                + " or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'))" ;
         if(pmType.equals("1"))
         {
              tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode<>'U'  )";
         }
         else if (pmType.equals("2"))
         {
             tSQL += " and b.riskcode  in (select riskcode from LMRISKAPP where kindcode='U'  )";
         }

         try{
             Prem =""+ tDF.format(execQuery(tSQL));
         }catch(Exception ex)
         {
             System.out.println("getWTPrem 出错！");
         }

         return Prem;

    }

    /**
         * 网点数量
         * 全部开业的一个渠道的网点数
       */
    private int AgentcomNum(String pmAgentCom,String pmManageCom,String EndDate)
    {
      int AgentcomNum = 0;
      String tSQL ="";
      tSQL = "select count(b.agentcom) from lacom b where b.branchtype='3' and b.branchtype2='01' "
             + " and (enddate is null or enddate>'"+EndDate+"')"
             +"  and b.managecom like '"+pmManageCom+"%'"
             +"  and b.agentcom like '"+pmAgentCom+"%'"
             +"  and b.makedate<='"+EndDate+"'";

      try{
        AgentcomNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("AgentcomNum 出错！");
      }
      return AgentcomNum;
    }
    /**
        * 网点数量
        * 全部开业的所有渠道的网点数
        */
    private int AgentcomSumNum(String pmManageCom,String EndDate)
    {
       int AgentcomSumNum = 0;
       String tSQL ="";
       tSQL = "select count(b.agentcom) from lacom b where b.branchtype='3' and b.branchtype2='01' "
              + " and (enddate is null or enddate>'"+EndDate+"')"
              +"  and b.makedate<='"+EndDate+"'"
              +"  and b.managecom like '"+pmManageCom+"%'";

       try{
       AgentcomSumNum = (int) execQuery(tSQL);
       }catch(Exception ex)
       {
           System.out.println("AgentcomSumNum 出错！");
       }
       return AgentcomSumNum;
    }


    /**
   * 有效网点数量
   * 起止日期到终止日期内的出单网点数
       */
    private int YXAgentcomNum(String pmAgentCom,String pmManageCom,String StartDate,String EndDate)
    {
      int YXAgentcomNum = 0;
      String tSQL ="";
      tSQL = "select count(distinct(agentcom)) from LJAPayPerson b " +
            "where b.grpcontno='00000000000000000000' and  b.makedate>='"+StartDate+"' "
            +" and b.makedate<='"+EndDate+"'"
            +" and b.managecom like '"+pmManageCom+"%'"
            +" and b.agentcom like '"+pmAgentCom+"%'"
            +" and (exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04')"
            +" or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))";


      try{
          YXAgentcomNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("YXAgentcomNum 出错！");
      }
       return YXAgentcomNum;
    }
   /**
   * 有效保费
   * 起止日期到终止日期内的出单保费
    */
    private String YXAgentcomSum(String pmAgentCom,String pmManageCom,String StartDate,String EndDate)
    {
      String Prem = "";
      String tSQL ="";
      DecimalFormat tDF = new DecimalFormat("0.######");


      tSQL = "select value(sum(b.sumactupaymoney),0) from LJAPayPerson b " +
            "where b.grpcontno='00000000000000000000' and  b.makedate>='"+StartDate+"' "
            +" and b.makedate<='"+EndDate+"'"
            +" and b.managecom like '"+pmManageCom+"%'"
            +" and b.agentcom like '"+pmAgentCom+"%'"
            +" and (exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04')"
            +"  or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))";


      try{
          Prem = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("YXAgentcomSum 出错！");
      }
      return Prem;
    }

    /**
     * 得到登录机构的名称
     */
    private String getName()
    {
       String name = "";
       String SQL = "select name from ldcom where comcode = '"+this.mGlobalInput.ManageCom+"'";
       ExeSQL tExeSQL = new ExeSQL();
       if(tExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "LAChnlSumReportBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据处理失败--->getName()";
           this.mErrors.addOneError(tError);
           return "";
       }
       name = tExeSQL.getOneValue(SQL);
       return name;
    }
    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)

    {
       Connection conn;
       conn = null;
       conn = DBConnPool.getConnection();

       System.out.println(sql);

       PreparedStatement st = null;
       ResultSet rs = null;
       try {
           if (conn == null)return 0.00;
           st = conn.prepareStatement(sql);
           if (st == null)return 0.00;
           rs = st.executeQuery();
           if (rs.next()) {
               return rs.getDouble(1);
           }
           return 0.00;
       } catch (Exception ex) {
           ex.printStackTrace();
           return -1;
       } finally {
           try {
              if (!conn.isClosed()) {
                  conn.close();
              }
              try {
                  st.close();
                  rs.close();
              } catch (Exception ex2) {
                  ex2.printStackTrace();
              }
              st = null;
              rs = null;
              conn = null;
            } catch (Exception e) {}

       }
      }
    public VData getResult()
    {
        return mResult;
    }
}
