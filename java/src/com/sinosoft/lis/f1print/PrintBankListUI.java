package com.sinosoft.lis.f1print;

/**
 * <p>Title:PrintBankListUI </p>
 *
 * <p>Description: 银行列表打印</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author : zhangjun
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;

public class PrintBankListUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    public PrintBankListUI()
    {
    }
    /**
       传输数据的公共方法
       */
      public boolean submitData(VData cInputData, String cOperate)
      {
          this.mOperate = cOperate;
          try
          {
              if (!cOperate.equals("PRINT"))
              {
                  buildError("submitData", "不支持的操作字符串");
                  return false;
              }
              PrintBankListBL tPrintBankListBL = new PrintBankListBL();
              System.out.println("Start PrintBillUI Submit ...");
              if (!tPrintBankListBL.submitData(cInputData, cOperate))
              {
                  if (tPrintBankListBL.mErrors.needDealError())
                  {
                      mErrors.copyAllErrors(tPrintBankListBL.mErrors);
                      return false;
                  }
                  else
                  {
                      buildError("submitData",
                                 "tPrintBankListBL发生错误，但是没有提供详细的出错信息");
                      return false;
                  }
              }
              else
              {
                  mResult = tPrintBankListBL.getResult();
                  return true;
              }
          }
          catch (Exception e)
          {
              e.printStackTrace();
              CError cError = new CError();
              cError.moduleName = "PLPsqsUI";
              cError.functionName = "submit";
              cError.errorMessage = e.toString();
              mErrors.addOneError(cError);
              return false;
          }
      }

      /**
       * 准备往后层输出所需要的数据
       * 输出：如果准备数据时发生错误则返回false,否则返回true
       */

      public VData getResult()
      {
          return this.mResult;
      }

      private void buildError(String szFunc, String szErrMsg)
      {
          CError cError = new CError();

          cError.moduleName = "FinChargeDayModeF1PUI";
          cError.functionName = szFunc;
          cError.errorMessage = szErrMsg;
          this.mErrors.addOneError(cError);
      }

      public static void main(String[] args)
      {
          GlobalInput mG = new GlobalInput();
          mG.Operator = "001";
          mG.ManageCom = "86110000";
          String tCanSendBank = "1";

          VData tVData = new VData();
          tVData.addElement(tCanSendBank);
          tVData.addElement(mG);

          PrintBankListUI tPrintBankListUI = new PrintBankListUI();
          tPrintBankListUI.submitData(tVData, "PRINT");
      }


}
