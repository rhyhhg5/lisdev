package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author qulq
 * @version 1.0
 */
public class ExpirBenefitPrintBL implements PrintService{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCContSchema tLCContSchema = new LCContSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private Object tExeSQL;
    public ExpirBenefitPrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("ExpirBenefitPrintBL begin");
        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();
        if (!getListData()) {
            return false;
        }

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        if(cOperate.equals("INSERT") && needPrt==0)
        {
            if (!dealPrintMag())
            {
                return false;
            }
        }

        System.out.println("ExpirBenefitPrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        LJSGetDB tLJSGetDB = new LJSGetDB();
        if(ttLOPRTManagerSchema == null)
        {
           tLJSGetDB = (LJSGetDB) cInputData.getObjectByObjectName("LJSGetDB", 0);
        }
        else
        {
            tLJSGetDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
        }
        if (tLJSGetDB == null)
        {
           buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
           return false;
        }
        tLJSGetSchema = tLJSGetDB.query().get(1);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawDB.setGetNoticeNo(tLJSGetSchema.getGetNoticeNo());
        LJSGetDrawSchema tLJSGetDrawSchema =tLJSGetDrawDB.query().get(1);
        String ContNo = tLJSGetDrawSchema.getContNo();
        System.out.println("ContNo=" + ContNo);
        String GetNoticeNo = tLJSGetSchema.getGetNoticeNo();
        String sql = "";
        String sqlone = "";
        String sqltwo = "";
        String sql3 = "";
        String s1 = "";

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(ContNo);

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='" +
              ContNo + "') AND AddressNo = '" + tLCAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        tLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).
                           getSchema();

        sqlone = "select * from lccont where contno='" + ContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sqlone);
        tLCContSchema = tLCContSet.get(1).getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ExpirBenefitPrint.vts", "printer");
        switch (Integer.parseInt(tLJSGetSchema.getPayMode())) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
        case 7:
        case 9:
            xmlexport.addDisplayControl("displaycash");
            break;
        case 4:
            xmlexport.addDisplayControl("displaybank");
            break;
        }
        ;
        setFixedInfo();
        textTag.add("GrpZipCode", tLCAddressSchema.getZipCode());
        System.out.println("GrpZipCode=" + tLCAddressSchema.getZipCode());
        textTag.add("GrpAddress", tLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if(tLCAddressSchema.getPhone() != null&&!tLCAddressSchema.getPhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getPhone() + "、";
        }
        if (tLCAddressSchema.getHomePhone() != null&&!tLCAddressSchema.getHomePhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getHomePhone() + "、";
        }
        if(tLCAddressSchema.getCompanyPhone() != null&&!tLCAddressSchema.getCompanyPhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getCompanyPhone() + "、";
        }
        if (tLCAddressSchema.getMobile() != null&&!tLCAddressSchema.getMobile().equals("")){
            appntPhoneStr += tLCAddressSchema.getMobile() + "、";
        }
        appntPhoneStr = appntPhoneStr.substring(0,appntPhoneStr.length()-1);
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCContSchema.getAppntName());
        textTag.add("AppntNo", tLCContSchema.getAppntNo());
//        textTag.add("GetDate", CommonBL.decodeDate(PubFun.calDate(tLCContSchema.getCInValiDate(),15,"D",null)));
//        System.out.println(CommonBL.decodeDate(PubFun.calDate(tLCContSchema.getCInValiDate(),15,"D",null)));
        textTag.add("ContNo", ContNo);
        System.out.println("ContNo=" + "" + ContNo);
        textTag.add("AppntName", tLCContSchema.getAppntName());
        textTag.add("GetNoticeNo", tLJSGetSchema.getGetNoticeNo());
        textTag.add("MakeDate", tLJSGetSchema.getMakeDate());
        textTag.add("bankAccNo", tLJSGetSchema.getBankAccNo());
        textTag.add("AccName", tLJSGetSchema.getAccName());
        textTag.add("shouldDate",tLJSGetSchema.getGetDate());
        textTag.add("bankAccNo", tLJSGetSchema.getBankAccNo());
        textTag.add("Drawer", tLCContSchema.getAppntName());
        textTag.add("sumgetMoney",tLJSGetSchema.getSumGetMoney());
        textTag.add("DrawerID",tLJSGetSchema.getDrawerID());
        textTag.add("PayToDate", CommonBL.decodeDate(PubFun.calDate(tLCContSchema.getCInValiDate(),-1,"D",null)));//转换格式，显示为24时
        textTag.add("CreatDate", tLJSGetSchema.getMakeDate());
        String strSql = "select max(PayToDate) from lcpol where riskcode = '320106' and ContNo = '" + tLCContSchema.getContNo() + "' with ur";
        String payDate = new ExeSQL().getOneValue(strSql);
        textTag.add("PayDate",CommonBL.decodeDate(PubFun.calDate(payDate,15,"D",null)));

        ExeSQL tExeSQL = new ExeSQL();


        String banksql =" select bankname From ldbank where bankcode in(select bankcode from lccont where contno ='"+tLCContSchema.getContNo()+"')";

        String sqlMoney =" select getmoney from ljsgetdraw a where dutycode !='000000' "
                        +" and getnoticeno in (select getnoticeno from ljsget where othernotype ='20' "
                        +" and otherno ='"+tLJSGetSchema.getOtherNo()+"')"
                        ;
        textTag.add("BankName",tExeSQL.getOneValue(banksql));
        textTag.add("SumMoney",Double.parseDouble(tExeSQL.getOneValue(sqlMoney)));
        String sexCode = tLCAppntDB.getAppntSex();
        String sex;
        if(sexCode == null)
        {
            sex = "先生/女士";
        }
        else if(sexCode.equals("0"))
        {
            sex = "先生";
        }
        else if(sexCode.equals("1"))
        {
            sex = "女士";
        }
        else
        {
            sex = "先生/女士";
        }
        textTag.add("LinkManSex", sex);

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
			+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
			+" (select agentgroup from laagent where agentcode ='"
                        + tLaAgentDB.getAgentCode()+"'))"
			;
	LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        textTag.add("BarCode1", tLJSGetSchema.getOtherNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        //缴费方式
        String payMode = "";
        if("4".equals(tLCContSchema.getPayMode()))
        {
            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(tLCContSchema.getBankCode());
            tLDBankDB.getInfo();

            payMode = "银行转帐，开户银行："+tLDBankDB.getBankName()+"，帐户名："+tLCContSchema.getAccName()+"，帐号"+tLCContSchema.getBankAccNo();
        }else
        {
            payMode = "现金";
        }
        textTag.add("PayMode",payMode);
        
        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        String[] title = {"满期险种","险种名称", "满期金额"};
        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\","indiDueFeehuxl");
        mResult.clear();
        mResult.addElement(xmlexport);


        if(mOperate.equals("INSERT"))
        {
            //放入打印列表
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo());
            tLOPRTManagerDB.setCode("bq001");
            needPrt = tLOPRTManagerDB.query().size();
            System.out.println("---------the size is "+needPrt+"--------");
            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
                mLOPRTManagerSchema.setOtherNoType("00");
                mLOPRTManagerSchema.setCode("bq001");
                mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
                mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
            }
        }
        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
//        sql.append(" select (select riskname from lmriskapp where riskcode = a.riskcode),getmoney ")
//            .append(" from ljsgetdraw a where dutycode !='000000' and getnoticeno in (")
          sql.append(" select lc.riskcode,(select riskname from lmrisk where riskcode=lc.riskcode),prem  ")
             .append(" from ljsgetdraw a ,lcpol lc where dutycode !='000000' and a.contno = lc.contno and getnoticeno in ( ")
            .append("	select getnoticeno from ljsget where othernotype ='20' and otherno ='"+tLJSGetSchema.getOtherNo()+"')")
            .append(" and lc.riskcode in ('320106','120706') ")
            .append(" union ")
            .append(" select (select codename from LDCODE where code = a.riskcode and codetype='RiskGetAlive'),(select riskname from lmrisk where riskcode=lc.riskcode),getmoney ")
            .append(" from ljsgetdraw a, lcpol lc where dutycode = '000000' and a.contno = lc.contno and getnoticeno in ( ")
            .append("	select getnoticeno from ljsget where othernotype ='20' and otherno ='"+tLJSGetSchema.getOtherNo()+"')")
            .append(" and lc.riskcode in ('320106','120706') with ur")
            ;

        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(sql.toString());
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public static void main(String[] a)
    {
        /*LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000001965");
        tLJSPayBDB.getInfo();

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo(tLJSPayBDB.getOtherNo());

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tLCContSchema);
        tVData.addElement(tG);
        tVData.addElement(tLJSPayBDB);

        ExpirBenefitPrintBL bl = new ExpirBenefitPrintBL();
        if(bl.submitData(tVData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }*/
    	MMap tMap = new MMap();
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(tVData, "") ;
        
    	
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
