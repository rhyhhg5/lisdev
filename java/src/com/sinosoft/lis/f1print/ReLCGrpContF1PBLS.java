/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
public class ReLCGrpContF1PBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mInputData;
    private VData mResult = new VData();

    public ReLCGrpContF1PBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println("Start ReLCGrpContF1PBLS Submit...");

        tReturn = save(cInputData);

        System.out.println(tReturn);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ReLCGrpContF1PBLS Submit...");

        return tReturn;
    }

    //保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

            System.out.println("Start ....");
            LCGrpContDB tLCGrpContDB = new LCGrpContDB(conn);
            LCGrpContSet tLCGrpContSet = new LCGrpContSet();
            tLCGrpContSet.set((LCGrpContSet) mInputData.getObjectByObjectName(
                    "LCGrpContSet", 0)); //

            for (int i = 0; i < tLCGrpContSet.size(); i++)
            {
                tLCGrpContDB.setSchema(tLCGrpContSet.get(i + 1));

                if (!tLCGrpContDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ReLCGrpContF1PBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            } //end for ()
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReLCGrpContF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }

        return tReturn;
    }

}
