/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.CreateCSVFile;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class BFJHExportDataBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    private String mSQL="";
    //统计管理机构
    private String  mManageCom= "";
    //统计级别
    private String mBankType2 = "";
    //统计银代机构
    private String mAgentCom = "";
    //统计业务员
    private String mAgentCode = "";
    //统计保单状态
    private String mState = "";
    //统计保单回执回销状态
    private String mGetPolState="";
    //统计险种
    private String mRiskCode = "";
    //统计起期
    private String  mStartDate= "";
    //统计止期
    private String  mEndDate = "";
    //交费年限
    private String  mPayYears = "";
    private String  mPayYear = "";
    private String  mGetState = "";
    //需要调用的模版
    private String mFlag = "";
    //转化交费方式
    private int mPayTime;
    //转换交费年限
    private int mpayyears;
    //文件暂存路径
    private String mfilePathAndName;
    //记录总行数
    private int mTotalLine=0;
    //记录总保费
    private double mTotalPrem=0;
    //记录总提奖金额
    private double mTotalFYC=0;
    //记录总手续费
    private double mTotalCharge=0;
    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String[] mDataList = null;
    private String[][] mShowDataList = null;
    public BFJHExportDataBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("dealData:" + mOperate);
        System.out.println("开始处理数据，渠道保费明细报表");
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BFJHExportDataBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        try{
                // 查询数据
          if(!getDataList())
          {
              return false;
          }
           System.out.println(mShowDataList.length);
           System.out.println(mShowDataList[0].length);
//            System.out.print("22222222222222222");
            return true;


            }catch(Exception ex)
            {
                buildError("queryData", "LABL发生错误，准备数据时出错！");
                return false;
            }


    }


    /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {

     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
     mfilePathAndName=(String)cInputData.get(1);
     mSQL=(String)cInputData.get(2);
      return true;
 }




    private boolean getDataList()
  {
      //新单用签单日期signdate，撤单用撤单日期tmakedate
      
        CreateCSVFile tCreateCSVFile=new CreateCSVFile();
        tCreateCSVFile.initFile(this.mfilePathAndName);
        LACommisionSet tLACommisionSet = new LACommisionSet();
        String tempInt=null;
        int tempNum=0;
        /** 新增方法:
         * 游标查询方法 */
        RSWrapper rswrapper = new RSWrapper();
        rswrapper.prepareData(tLACommisionSet, mSQL);
        try{
        	do {
        		SSRS tSSRS=rswrapper.getSSRS();
                String[] tShowDataList = new String[tSSRS.MaxRow];
                String tempRow="";
                System.out.println("here2:"+tSSRS.MaxRow);
                System.out.println("here3:"+tSSRS.GetText(1,1));
                for(int i=1;i<=tSSRS.MaxRow;i++){
                	tempRow=tSSRS.GetText(i,1);
                	for(int j=2;j<=tSSRS.MaxCol;j++){
                		if(j==16 || j==17 || j==20 || j==21){
                			tempInt=tSSRS.GetText(i,j);
                			tempNum = tempInt.lastIndexOf(".");
                			if(tempNum==-1){
                				tempInt+=".00";
                			}else if(tempInt.length()==tempNum+2){
                				tempInt+="0";
                			}
                			tempRow+="|"+tempInt;
                		}else{
                			tempRow+="|"+tSSRS.GetText(i,j);
                		}
                	}
                	tempRow+="|";
                	tShowDataList[i-1]=tempRow;
                }
                tCreateCSVFile.doWrite(tShowDataList);
        	}while(tLACommisionSet.size() > 0);
        }catch(Exception exc){
        	exc.printStackTrace();
            rswrapper.close();
        	return false;
        } finally
        {
            rswrapper.close();
         }

        tCreateCSVFile.doClose();
      return true;
 }

    public VData getResult()
    {
        return mResult;
    }
}
