package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Hezy Lys
 * @version 1.0
 * @date:2003-05-28
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinDayCheckBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //取得的时间
    private String mOpt = ""; //接收收费的类型：暂收OR预收
    private String mChargeType = ""; //将收费类型转换成汉字的显示
    private GlobalInput mGlobalInput = new GlobalInput(); // 全局数据
    public FinDayCheckBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        /**************************************************************************
         * date:2003-05-28
         * author:lys
         * function:判断收费的标志，若是Pay则是“暂收”、若是YSPay则是“预收”
         */
        if (mOpt.equals("Pay"))
        {
            mChargeType = "暂收";
            System.out.println("收费类型是" + mChargeType);
        }
        if (mOpt.equals("YSPay"))
        {
            mChargeType = "预收";
            System.out.println("收费类型是" + mChargeType);
        }
        if (mOpt.equals("Card"))
        {
            mChargeType = "卡单";
            System.out.println("收费类型是" + mChargeType);
        }
        // 准备所有要打印的数据
        if (cOperate.equals("PRINTGET")) //打印付费
        {
            if (!getPrintDataGet())
            {
                return false;
            }
        }

        if (cOperate.equals("PRINTPAY")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args)
    {

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        mDay = (String[]) cInputData.get(0);
        mOpt = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println("2003-05-28  BL ");
        System.out.println("接收的收费标志是" + mOpt);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        SSRS nSSRS = new SSRS();
        double SumMoney = 0;
        long SumNumber = 0;
        String strArr[] = null;
        XmlExport xmlexport = new XmlExport();

        if (mOpt.equals("Pay"))
        {
            System.out.println("执行的是暂收费操作！！！");
            xmlexport.createDocument("FinDayCheckPay.vts", "printer");
//        for (int k = 1; k <= 2; k++)
//        {
//            ListTable tlistTable = new ListTable();
//            ExeSQL tExeSQL = new ExeSQL();
//
//            String msql =
//                        "select RiskCode,sum(PayMoney),sum(1) from LJTempFee where Confmakedate>='" +
//                        mDay[0] + "' and Confmakedate<='" + mDay[1] +
//                        "' and ManageCom like'" + mGlobalInput.ManageCom +
//                        "%' and TempFeeType in('1','5') and otherno not in " +
//                       "(select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) and uwflag  in  ('9','4','6')"+
//                       "and (paytodate - Cvalidate) <= 366) and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7')";
//
//            String tGrpSql = "  Group By RiskCode ";
//            String tRisk = "";
//            String tRiskSql = "";
//            if (k == 1)
//            {
//                //查询出险种是健康险的险种编码
//                tRiskSql =
//                        " and riskcode in(select riskcode from LMRiskApp where RiskType in ('H','L','O')) ";
//                msql = msql + tRiskSql;
//                msql = msql + tGrpSql;
//                tRisk = mChargeType + "保费\\健康险";
//                tlistTable.setName("RISK1");
//            }
//            if (k == 2)
//            {
//                //查询出险种是意外险的险种代码
//                tRiskSql =
//                        " and riskcode in(select riskcode from LMRiskApp where RiskType in ('A','R')) ";
//                msql = msql + tRiskSql;
//                msql = msql + tGrpSql;
//                tRisk = mChargeType + "保费\\意外险";
//                tlistTable.setName("RISK2");
//            }
//
//            tSSRS = tExeSQL.execSQL(msql);
//            double tSum = 0;
//            int tNum = 0;
//            for (int i = 1; i <= tSSRS.MaxRow; i++)
//            {
//                tSum = tSum + Double.valueOf(tSSRS.GetText(i, 2)).doubleValue();
//                tNum = tNum + Integer.valueOf(tSSRS.GetText(i, 3)).intValue();
//            }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
//            for (int i = 1; i <= tSSRS.MaxRow; i++)
//            {
//                strArr = new String[3];
//                for (int j = 1; j <= tSSRS.MaxCol; j++)
//                {
//                    if (j == 1)
//                    {
//                        strArr[j - 1] = tSSRS.GetText(i, j);
//                        String tRiskcode = tSSRS.GetText(i, j);
//                        LMRiskDB tLMRiskDB = new LMRiskDB();
//                        tLMRiskDB.setRiskCode(tRiskcode);
//                        if (tLMRiskDB.getInfo())
//                        {
//                            strArr[j -
//                                    1] = tRisk + "\\" + tLMRiskDB.getRiskName();
//                        }
//                    }
//                    if (j == 2)
//                    {
//                        strArr[j - 1] = tSSRS.GetText(i, j);
//                        String strSum = new DecimalFormat("0.00").format(Double.
//                                valueOf(strArr[j - 1]));
//                        strArr[j - 1] = strSum;
//                        SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);
//                    }
//                    if (j == 3)
//                    {
//                        strArr[j - 1] = tSSRS.GetText(i, j);
//                        SumNumber = SumNumber +
//                                    Long.valueOf(strArr[j - 1]).longValue();
//                    }
//                }
//                tlistTable.add(strArr);
//            }
//            strArr = new String[3];
//            strArr[0] = "Risk";
//            strArr[1] = "Money";
//            strArr[2] = "Mult";
//            xmlexport.addListTable(tlistTable, strArr);
//        }

            ListTable tlistTable = new ListTable();
            ExeSQL tExeSQL = new ExeSQL();
//            String msql =
//                        "select '暂收' || case a.paymode when '4' then ' 银行转帐' else '' end, a.prtno A,a.riskcode,b.paymoney,a.paymode from lcgrppol a,ljtempfee b where"+
//                        " a.riskcode=b.riskcode and ((a.grpcontno=b.otherno) or (a.prtno=b.otherno)) and b.Confmakedate>='" +
//                        mDay[0] + "' and b.Confmakedate<='" + mDay[1] +
//                        "' and b.ManageCom like'" + mGlobalInput.ManageCom +
//                        "%' and b.TempFeeType in('1','5','16') and b.otherno not in " +
//                       "(select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) and uwflag  in  ('9','4','6')"+
//                       "and (paytodate - Cvalidate) <= 1000) and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7'  and riskcode not in ('170101','170301')) union "+
//                       "select '暂收' || case a.paymode when '4' then ' 银行转帐' else '' end,a.prtno A,a.riskcode,b.paymoney,a.paymode from lcpol a,ljtempfee b where"+
//                       " a.riskcode=b.riskcode and ((a.contno=b.otherno) or (a.prtno=b.otherno)) and b.Confmakedate>='" +
//                       mDay[0] + "' and b.Confmakedate<='" + mDay[1] +
//                       "' and b.ManageCom like'" + mGlobalInput.ManageCom +
//                       "%' and b.TempFeeType in('1','5','16') and b.otherno not in " +
//                      "(select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) and uwflag  in  ('9','4','6')"+
//                      "and (paytodate - Cvalidate) <= 1000) and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7'  and riskcode not in ('170101','170301')) union "+
//                      "select '暂收' || case a.paymode when '4' then ' 银行转帐' else '' end, a.prtno A,a.riskcode,b.paymoney,a.paymode  from lbgrppol a,ljtempfee b where"+
//                     " a.riskcode=b.riskcode and ((a.grpcontno=b.otherno) or (a.prtno=b.otherno)) and b.Confmakedate>='" +
//                     mDay[0] + "' and b.Confmakedate<='" + mDay[1] +
//                     "' and b.ManageCom like'" + mGlobalInput.ManageCom +
//                     "%' and b.TempFeeType in('1','5','16') and b.otherno not in " +
//                    "(select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) and uwflag  in  ('9','4','6')"+
//                    "and (paytodate - Cvalidate) <= 1000) and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) union "+
//                    "select '暂收' || case a.paymode when '4' then ' 银行转帐' else '' end,a.prtno A,a.riskcode,b.paymoney,a.paymode from lbpol a,ljtempfee b where"+
//                    " a.riskcode=b.riskcode and ((a.contno=b.otherno) or (a.prtno=b.otherno)) and b.Confmakedate>='" +
//                    mDay[0] + "' and b.Confmakedate<='" + mDay[1] +
//                    "' and b.ManageCom like'" + mGlobalInput.ManageCom +
//                    "%' and b.TempFeeType in('1','5','16') and b.otherno not in " +
//                   "(select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) and uwflag  in  ('9','4','6')"+
//                   "and (paytodate - Cvalidate) <= 1000) and a.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) ORDER BY A";
            GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
            tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
            tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
            tGetSQLFromXML.setParameters("EndDate", mDay[1]);
            String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
            String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "TempFee");
            tSSRS = tExeSQL.execSQL(msql);
            tlistTable.setName("RISK");
            double tSum = 0;
            int tNum=0;
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                tSum = tSum + Double.valueOf(tSSRS.GetText(i, 4)).doubleValue();
                if(tSSRS.GetText(i, 5)!=null && tSSRS.GetText(i, 5).equals("4")){
                    tNum++;
                }
            }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                strArr = new String[4];
                for (int j = 1; j <= tSSRS.MaxCol; j++)
                {
                    if (j == 1)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    }
                    if (j == 2)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    }
                    if (j == 3)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    }
                    if (j == 4)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    }
                }
                tlistTable.add(strArr);
            }
            strArr = new String[4];
            strArr[0] = "RISK";
            strArr[1] = "PrtNo";
            strArr[2] = "Mult";
            strArr[3] = "Money";
            xmlexport.addListTable(tlistTable, strArr);


//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
        String nsql = "select Name from LDCom where ComCode='" +
                      mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", tSum);
        texttag.add("SumNum", tNum);
//        texttag.add("SumNumber", SumNumber);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","FinDayCheckBL.xml");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        }
        else  if (mOpt.equals("YSPay"))
        {
           System.out.println("执行的是预收操作！！！");
           xmlexport.createDocument("FinDayCheckPayYS.vts", "printer");
           ListTable tlistTable = new ListTable();
           ExeSQL tExeSQL = new ExeSQL();
//           String msql ="select a.otherno,b.paymoney from LJTempFee a,ljtempfeeclass b,ljagetendorse c  "+
//           "where 1=1 and a.Confmakedate>='"+mDay[0]+"' and a.Confmakedate<='"+mDay[1]+"'"+
//           "and a.ManageCom like '"+mGlobalInput.ManageCom+"%' and a.TempFeeType in ('4','7') and b.tempfeeno = a.tempfeeno "+
//           "and b.Paymode not in ('B','J') and a.tempfeeno not in (Select GetNoticeNo From LJAPay Where IncomeType = '13' )" +
//           "and a.otherno = c.otherno and (c.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) or c.RiskCode is null)";
           GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
           tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
           tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
           tGetSQLFromXML.setParameters("EndDate", mDay[1]);
           String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
           String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "JSPay");
           tSSRS = tExeSQL.execSQL(msql);
           tlistTable.setName("RISK");
           double tSum = 0;
           for (int i = 1; i <= tSSRS.MaxRow; i++)
           {
               tSum = tSum + Double.valueOf(tSSRS.GetText(i, 2)).doubleValue();
           }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
           for (int i = 1; i <= tSSRS.MaxRow; i++)
           {

               strArr = new String[2];
               for (int j = 1; j <= tSSRS.MaxCol; j++)
               {
                   if (j == 1)
                   {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                   }
                   if (j == 2)
                   {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                   System.out.println(strArr[j - 1] );
                   }
               }
               tlistTable.add(strArr);
           }
           strArr = new String[2];
           strArr[0] = "RISK";
           strArr[1] = "Money";
           xmlexport.addListTable(tlistTable, strArr);


//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
       String nsql = "select Name from LDCom where ComCode='" +
                     mGlobalInput.ManageCom + "'";
       ExeSQL nExeSQL = new ExeSQL();
       nSSRS = nExeSQL.execSQL(nsql);
       String manageCom = nSSRS.GetText(1, 1);

       TextTag texttag = new TextTag(); //新建一个TextTag的实例
       texttag.add("StartDate", mDay[0]);
       texttag.add("EndDate", mDay[1]);
       texttag.add("ManageCom", manageCom);
       texttag.add("SumMoney", tSum);
       if (texttag.size() > 0)
       {
           xmlexport.addTextTag(texttag);
       }
        mResult.clear();
        mResult.addElement(xmlexport);
        }
        else if (mOpt.equals("YSToPrem"))
        {
           System.out.println("执行的是应收保费转保费操作！！！");
           xmlexport.createDocument("FinDayYSToPrem.vts", "printer");
           ListTable tlistTable = new ListTable();
           ListTable mlistTable = new ListTable();
           ExeSQL tExeSQL = new ExeSQL();
//           String msql ="select '续保', b.contno,sum(a.paymoney) from (select distinct otherno a,paymoney from LJTempFee where Confdate>='"+mDay[0]+"' and Confdate<='"+mDay[1]+"' "+
//                        "and ManageCom like '"+mGlobalInput.ManageCom+"%' and TempFeeType = '2' and tempfeeno in  ( "+
//                        "select distinct getnoticeno from ljspaygrpb  where DealState ='1' and paycount >=1 "+
//                        "union "+
//                        "select distinct getnoticeno from ljspaypersonb  where DealState ='1'  and paycount >=1) "+
//                        "and otherno not in (select distinct grpcontno from lcgrppol where 1=1 and "+
//                        "payintv in (1,3,6) and uwflag  in  ('9','4','6') and "+
//                        "(payenddate - Cvalidate) <= 10000 and Cvalidate >= '"+mDay[0]+"' and Cvalidate <= '"+mDay[0]+"') "+
//                        " and RiskCode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) "+
//                        "and otherno in (select distinct polno from lcrnewstatelog)) as a ,lcrnewstatelog b where "+
//                        "b.polno = a.a group by b.contno";
           GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
           tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
           tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
           tGetSQLFromXML.setParameters("EndDate", mDay[1]);
           String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
           String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "YSToPrem1");
           
           tSSRS = tExeSQL.execSQL(msql);
           tlistTable.setName("XB");
           double tSum = 0;
           for (int i = 1; i <= tSSRS.MaxRow; i++)
           {
               tSum = tSum + Double.valueOf(tSSRS.GetText(i, 3)).doubleValue();
           }
           for (int i = 1; i <= tSSRS.MaxRow; i++)
           {

               strArr = new String[3];
               for (int j = 1; j <= tSSRS.MaxCol; j++)
               {
                   if (j == 1)
                   {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                   }
                   if (j == 2)
                   {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                   }
                   if (j == 3)
                   {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                   }
               }
               tlistTable.add(strArr);
           }
           strArr = new String[3];
           strArr[0] = "XUBAO";
           strArr[1] = "ContNo";
           strArr[2] = "Money";
           xmlexport.addListTable(tlistTable, strArr);

//           msql ="select '续期', otherno ,sum(paymoney) from LJTempFee where Confdate>='"+mDay[0]+"' and Confdate<='"+mDay[1]+"' "+
//                 "and ManageCom like '"+mGlobalInput.ManageCom+"%' and TempFeeType = '2' and tempfeeno in "+
//                 "(select distinct getnoticeno from ljspaygrpb  where DealState ='1' and paycount >=1 union select"+
//                 " distinct getnoticeno from ljspaypersonb  where DealState ='1'  and paycount >=1) "+
//                 "and otherno not in (select distinct grpcontno from lcgrppol where 1=1 and "+
//                 "payintv in (1,3,6) and uwflag  in  ('9','4','6') and "+
//                 "(payenddate - Cvalidate) <= 10000 and Cvalidate >= '"+mDay[0]+"' and "+
//                 "Cvalidate <= '"+mDay[1]+"') "+
//                 " and RiskCode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) "+
//                 " and otherno not in (select distinct polno from lcrnewstatelog) group by '续期',otherno";
           msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "YSToPrem2");
           
           System.out.println(msql);
           tSSRS = tExeSQL.execSQL(msql);
           System.out.println(tSSRS.MaxRow);
           mlistTable.setName("XQ");
           double mSum = 0;
           String strmArr[] = null;
             for (int i = 1; i <= tSSRS.MaxRow; i++)
               {
                mSum = mSum + Double.valueOf(tSSRS.GetText(i, 3)).doubleValue();
                 }
                   for (int i = 1; i <= tSSRS.MaxRow; i++)
                      {
                          strmArr = new String[3];
                          for (int j = 1; j <= tSSRS.MaxCol; j++)
                          {
                              if (j == 1)
                              {
                               strmArr[j - 1] = tSSRS.GetText(i, j);
                              }
                              if (j == 2)
                              {
                               strmArr[j - 1] = tSSRS.GetText(i, j);
                              }
                              if (j == 3)
                              {
                               strmArr[j - 1] = tSSRS.GetText(i, j);
                              }
                          System.out.println(strmArr[j - 1]);
                          }
                          mlistTable.add(strmArr);
                      }
                   strmArr = new String[3];
                   strmArr[0] = "XUQI";
                   strmArr[1] = "ContNo";
                   strmArr[2] = "Money";
                   xmlexport.addListTable(mlistTable, strmArr);
        SumMoney = tSum + mSum;
       String nsql = "select Name from LDCom where ComCode='" +
                     mGlobalInput.ManageCom + "'";
       ExeSQL nExeSQL = new ExeSQL();
       nSSRS = nExeSQL.execSQL(nsql);
       String manageCom = nSSRS.GetText(1, 1);

       TextTag texttag = new TextTag(); //新建一个TextTag的实例
       texttag.add("StartDate", mDay[0]);
       texttag.add("EndDate", mDay[1]);
       texttag.add("ManageCom", manageCom);
       texttag.add("SumMoney", SumMoney);
       if (texttag.size() > 0)
       {
           xmlexport.addTextTag(texttag);
       }
        mResult.clear();
        mResult.addElement(xmlexport);
        }
        else  if (mOpt.equals("YSPrem"))
         {
            System.out.println("执行的是应收保费日结操作！！！");
            xmlexport.createDocument("FinDayCheckYSPrem.vts", "printer");
            ListTable tlistTable = new ListTable();
            ExeSQL tExeSQL = new ExeSQL();
//            String msql ="select getnoticeno,sum(sumactupaymoney)  from LJSPayGrpb where "+
//            "Modifydate >= '"+mDay[0]+"' and Modifydate <='"+mDay[1]+"' and ManageCom like '"+mGlobalInput.ManageCom+"' "+
//            "and DealState ='1' and grpcontno not in (select distinct grpcontno from lcgrppol where 1=1 and "+
//            "payintv in (1,3,6) and uwflag  in  ('9','4','6') "+
//            "and (payenddate - Cvalidate) <= 10000 and Cvalidate>='"+mDay[0]+"' and Cvalidate<='"+mDay[1]+"') "+
//            "and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and paytype <> 'YEL' and "+
//            "riskcode not in ('170101','170301'))  group by getnoticeno "+
//            "union all " +
//            "select getnoticeno,sum(sumactupaymoney) from LJSPayPersonB where "+
//            "Modifydate >= '"+mDay[0]+"' and Modifydate <='"+mDay[1]+"' "+
//            "and paytype <> 'YEL' and  ManageCom like '"+mGlobalInput.ManageCom+"%' and DealState ='1' group by getnoticeno";
//            
            GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
            tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
            tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
            tGetSQLFromXML.setParameters("EndDate", mDay[1]);
            String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
            String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "YSPrem");
            
             tSSRS = tExeSQL.execSQL(msql);
            tlistTable.setName("XQ");
            double tSum = 0;
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                tSum = tSum + Double.valueOf(tSSRS.GetText(i, 2)).doubleValue();
            }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {

                strArr = new String[2];
                for (int j = 1; j <= tSSRS.MaxCol; j++)
                {
                    if (j == 1)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    }
                    if (j == 2)
                    {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    System.out.println(strArr[j - 1] );
                    }
                }
                tlistTable.add(strArr);
            }
            strArr = new String[2];
            strArr[0] = "GetNoticeNo";
            strArr[1] = "Money";
            xmlexport.addListTable(tlistTable, strArr);


//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
        String nsql = "select Name from LDCom where ComCode='" +
                      mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", tSum);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
         mResult.clear();
         mResult.addElement(xmlexport);
         }
         else  if (mOpt.equals("PayIntvPay"))
          {
             System.out.println("执行的是一年期期交收费日结操作！！！");
             xmlexport.createDocument("FinDayCheckPayIntvPay.vts", "printer");
             ListTable tlistTable = new ListTable();
             ExeSQL tExeSQL = new ExeSQL();
//             String msql ="select a.a,b.e,a.c from (select TempFeeNo a,otherno b,sum(PayMoney) c from LJTempFee where Confmakedate>='"+mDay[0]+"' and Confmakedate<='"+mDay[1]+"' "+
//                          "and ManageCom like '"+mGlobalInput.ManageCom+"%' and TempFeeType in('1')  and otherno in (select distinct grpcontno from lcgrppol where 1=1 "+
//                          "and payintv in (1,3,6) and uwflag  in  ('9','4','6') and (payenddate - Cvalidate) <= 10000) "+
//                          "and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) "+
//                          "group by TempFeeNo,payintv,otherno) as a ,(select distinct grpcontno d,payintv e from lcgrppol where 1=1 "+
//                          "and payintv in (1,3,6) and uwflag  in  ('9','4','6') and (payenddate - Cvalidate) <= 10000) as b where a.b = b.d";
             GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
             tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
             tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
             tGetSQLFromXML.setParameters("EndDate", mDay[1]);
             String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
             String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "PayIntvPay");
             
             tSSRS = tExeSQL.execSQL(msql);
             tlistTable.setName("PREM");
             double tSum = 0;
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {
                 tSum = tSum + Double.valueOf(tSSRS.GetText(i, 3)).doubleValue();
             }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {

                 strArr = new String[3];
                 for (int j = 1; j <= tSSRS.MaxCol; j++)
                 {
                     if (j == 1)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                     if (j == 2)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                     if (j == 3)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                 }
                 tlistTable.add(strArr);
             }
             strArr = new String[3];
             strArr[0] = "TempfeeNo";
             strArr[1] = "PayIntv";
             strArr[2] = "Money";
             xmlexport.addListTable(tlistTable, strArr);


//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
         String nsql = "select Name from LDCom where ComCode='" +
                       mGlobalInput.ManageCom + "'";
         ExeSQL nExeSQL = new ExeSQL();
         nSSRS = nExeSQL.execSQL(nsql);
         String manageCom = nSSRS.GetText(1, 1);

         TextTag texttag = new TextTag(); //新建一个TextTag的实例
         texttag.add("StartDate", mDay[0]);
         texttag.add("EndDate", mDay[1]);
         texttag.add("ManageCom", manageCom);
         texttag.add("SumMoney", tSum);
         if (texttag.size() > 0)
         {
             xmlexport.addTextTag(texttag);
         }
          mResult.clear();
          mResult.addElement(xmlexport);
         }
         else  if (mOpt.equals("PayIntvPrem"))
          {
             System.out.println("执行的是一年期期交保费日结操作！！！");
             xmlexport.createDocument("FinDayCheckPayIntvPrem.vts", "printer");
             ListTable tlistTable = new ListTable();
             ExeSQL tExeSQL = new ExeSQL();
//             String msql ="select a.a,b.d,a.b from (select grpcontno a,sum(sumduepaymoney) b from LJAPayGrp where 1=1 and ConfDate>='"+mDay[0]+"' and ConfDate<='"+mDay[1]+"' "+
//                          "and ManageCom like '"+mGlobalInput.ManageCom+"%' "+
//                          "and payno in(select payno from ljapay where incometype in('1') and paycount <=1 and endorsementno is null "+
//                          "and grppolno not in (select grppolno from LCRnewStateLog) "+
//                          "and grpcontno in (select distinct grpcontno from lcgrppol where 1=1 and payintv in (1,3,6) "+
//                          "and uwflag  in  ('9','4','6') and (payenddate - Cvalidate) <= 10000)) "+
//                          "and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) "+
//                          "group by grpcontno) as a ,(select distinct grpcontno c,payintv d from lcgrppol where 1=1 and payintv in (1,3,6) "+
//                          "and uwflag  in  ('9','4','6') and (payenddate - Cvalidate) <= 10000) as b where a.a=b.c";
//             
             GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
             tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
             tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
             tGetSQLFromXML.setParameters("EndDate", mDay[1]);
             String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
             String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "PayIntvPrem");
             
             tSSRS = tExeSQL.execSQL(msql);
             tlistTable.setName("PREM");
             double tSum = 0;
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {
                 tSum = tSum + Double.valueOf(tSSRS.GetText(i, 3)).doubleValue();
             }
//            strArr = new String[3];
//            strArr[0] = tRisk;
//            strArr[1] = new DecimalFormat("0.00").format(new Double(tSum));
//            strArr[2] = String.valueOf(tNum);
//            tlistTable.add(strArr);
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {

                 strArr = new String[3];
                 for (int j = 1; j <= tSSRS.MaxCol; j++)
                 {
                     if (j == 1)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                     if (j == 2)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                     if (j == 3)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                 }
                 tlistTable.add(strArr);
             }
             strArr = new String[3];
             strArr[0] = "TempfeeNo";
             strArr[1] = "PayIntv";
             strArr[2] = "Money";
             xmlexport.addListTable(tlistTable, strArr);


//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
         String nsql = "select Name from LDCom where ComCode='" +
                       mGlobalInput.ManageCom + "'";
         ExeSQL nExeSQL = new ExeSQL();
         nSSRS = nExeSQL.execSQL(nsql);
         String manageCom = nSSRS.GetText(1, 1);

         TextTag texttag = new TextTag(); //新建一个TextTag的实例
         texttag.add("StartDate", mDay[0]);
         texttag.add("EndDate", mDay[1]);
         texttag.add("ManageCom", manageCom);
         texttag.add("SumMoney", tSum);
         if (texttag.size() > 0)
         {
             xmlexport.addTextTag(texttag);
         }
          mResult.clear();
          mResult.addElement(xmlexport);
         }
        else {
            System.out.println("执行的是卡单暂收操作！！！");
             xmlexport.createDocument("FinDayCheckPayCard.vts", "printer");
             ListTable tlistTable = new ListTable();
             ExeSQL tExeSQL = new ExeSQL();
//             String msql ="Select otherno,sum(paymoney) from LJTempFee where TempFeeType in ('6','10') "+
//             "and Confmakedate>='"+mDay[0]+"' and Confmakedate <='"+mDay[1]+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' "+
//             "group by  otherno "; 
             GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
             tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
             tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
             tGetSQLFromXML.setParameters("EndDate", mDay[1]);
             String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
             String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "Card");
             
             tSSRS = tExeSQL.execSQL(msql);
             tlistTable.setName("RISK");
             double tSum = 0;
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {
                 tSum = tSum + Double.valueOf(tSSRS.GetText(i, 2)).doubleValue();
             }
             for (int i = 1; i <= tSSRS.MaxRow; i++)
             {

                 strArr = new String[2];
                 for (int j = 1; j <= tSSRS.MaxCol; j++)
                 {
                     if (j == 1)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     }
                     if (j == 2)
                     {
                      strArr[j - 1] = tSSRS.GetText(i, j);
                     System.out.println(strArr[j - 1] );
                     }
                 }
                 tlistTable.add(strArr);
             }
             strArr = new String[2];
             strArr[0] = "RISK";
             strArr[1] = "Money";
             xmlexport.addListTable(tlistTable, strArr);

         String nsql = "select Name from LDCom where ComCode='" +
                       mGlobalInput.ManageCom + "'";
         ExeSQL nExeSQL = new ExeSQL();
         nSSRS = nExeSQL.execSQL(nsql);
         String manageCom = nSSRS.GetText(1, 1);

         TextTag texttag = new TextTag(); //新建一个TextTag的实例
         texttag.add("StartDate", mDay[0]);
         texttag.add("EndDate", mDay[1]);
         texttag.add("ManageCom", manageCom);
         texttag.add("SumMoney", tSum);
//        texttag.add("SumNumber", SumNumber);
         if (texttag.size() > 0)
         {
             xmlexport.addTextTag(texttag);
         }
          mResult.clear();
          mResult.addElement(xmlexport);
        }
        return true;
    }

    /****************************************************************************
     * 准备付费的打印数据
     * date：2003-05-28
     * author：lys
     * table：实付总表（LJAGet）
     **/
    private boolean getPrintDataGet()
    {
        /**************************************************************************
         * 变量说明
         * CS：次数 F : 分红  T: 投连 C：传统
         * JE:金额
         ***/
        //总合计
        int CS = 0;
        double JE = 0;

        //年金
        int NCS = 0;
        int NFCS = 0;
        int NTCS = 0;
        int NCCS = 0;

        double NJE = 0;
        double NFJE = 0;
        double NTJE = 0;
        double NCJE = 0;

        //满期
        int MCS = 0;
        int MFCS = 0;
        int MTCS = 0;
        int MCCS = 0;

        double MJE = 0;
        double MFJE = 0;
        double MTJE = 0;
        double MCJE = 0;

        //理赔
        int LCS = 0;
        int LFCS = 0;
        int LTCS = 0;
        int LCCS = 0;

        double LJE = 0;
        double LFJE = 0;
        double LTJE = 0;
        double LCJE = 0;

        //退保金---犹豫期撤单
        int TCS = 0;
        int TFCS = 0;
        int TTCS = 0;
        int TCCS = 0;

        double TJE = 0;
        double TFJE = 0;
        double TTJE = 0;
        double TCJE = 0;

        //退保金---退保金
        double T2JE = 0;
        double T2FJE = 0; //分红金额
        double T2TJE = 0; //投连金额
        double T2CJE = 0; //传统金额

        //批改补退费的其他项目
        // double PQJE = 0;
        // double PQFJE = 0;//分红金额
        // double PQTJE = 0;//投连金额
        // double PQCJE = 0;//传统金额

        //借款的相关金额
        double JKJE = 0; //借款的总金额
        double JKFJE = 0; //借款的分红金额
        double JKTJE = 0; //借款的投连金额
        double JKCJE = 0; //借款的传统金额

        //退费的相关金额
        double TuiFJE = 0; //退费的总金额
        double TuiFFJE = 0; //退费的分红金额
        double TuiFTJE = 0; //退费的投连金额
        double TuiFCJE = 0; //退费的传统金额

        //余额的相关金额
        double EYJE = 0; //余额的总金额
        double EYFJE = 0; //余额的分红金额
        double EYTJE = 0; //余额的投连金额
        double EYCJE = 0; //余额的传统金额

        //还款的相关金额
        double HKJE = 0; //还款的总金额
        double HKFJE = 0; //还款的分红金额
        double HKTJE = 0; //还款的投连金额
        double HKCJE = 0; //还款的传统金额

        //利息的相关金额
        double LXJE = 0; //利息的总金额
        double LXFJE = 0; //利息的分红金额
        double LXTJE = 0; //利息的投连金额
        double LXCJE = 0; //利息的传统金额

        //红利
        int HCS = 0;
        int HFCS = 0;
        int HTCS = 0;
        int HCCS = 0;

        double HJE = 0;
        double HFJE = 0;
        double HTJE = 0;
        double HCJE = 0;

        //暂收保费
        int ZCS = 0;
        int ZFCS = 0;
        int ZTCS = 0;
        int ZCCS = 0;

        double ZJE = 0;
        double ZFJE = 0;
        double ZTJE = 0;
        double ZCJE = 0;

        //其他
        int QCS = 0;
        int QFCS = 0;
        int QTCS = 0;
        int QCCS = 0;

        double QJE = 0;
        double QFJE = 0;
        double QTJE = 0;
        double QCJE = 0;

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FinDayCheckGet.vts", "printer"); //最好紧接着就初始化xml文档
        /***************************************************************************
         * 在实付总表中查询出该时间段内的实付号码和其他号码类型.
         * 根据号码类型来判断所要查的表
         * 0 ---表示生存领取对应的合同号
         * 1 ---表示生存领取对应的集体保单号
         * 2 ---表示生存领取对应的个人保单号
         * 3 ---表示批改号
         * 4 ---暂交费退费,对应暂交费退费表的给付通知书号
         * 5 ---表示赔付应收表中的给付通知书号（就是赔案号）
         * 6 ---表示其他退费的给付通知书号码(对应号码添个人保单号）
         * 7 ---表示红利对应的个人保单号。
         * 8 ---表示其他退费的给付通知书号码(对应号码填团体保单号）
         **/

        String ActuGetNo_sql =
                "Select ActuGetNo,OtherNoType From LJAGet Where ConfDate >= '"
                + mDay[0] + "' and ConfDate <= '" + mDay[1] +
                "' and ManageCom like'" + mGlobalInput.ManageCom + "%'";
        System.out.println("查询出实付总表中的实付号码和类型的sql语句是");
        System.out.println(ActuGetNo_sql);
        ExeSQL t_exesql = new ExeSQL();
        SSRS t_ssrs = t_exesql.execSQL(ActuGetNo_sql);
        System.out.println("查询的总个数是" + t_ssrs.getMaxRow());
        if (t_ssrs.getMaxRow() == 0)
        {
            System.out.println("查询的结果是0");
            CError tError = new CError();
            tError.moduleName = "ShowBillBL";
            tError.functionName = "getDutyGetClmInfo";
            tError.errorMessage = "没有要打印的数据！！！";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("实付表中的实付号码条数是" + t_ssrs.getMaxRow());
        for (int i = 1; i <= t_ssrs.getMaxRow(); i++)
        {
            /***********************************************************************
             * 对于三级科目中的信息（分红（2）、投连（3）、传统（1,4,5））
             * 年金和满期的情况是当OtherNoType in（0，1,2）
             * FenHong_sql :分红类型的查询
             * TouLian_sql:投连类型的查询
             * ChuanTong_sql:传统类型的查询
             * 年金的情况当OtherNoType in(0,1,2)时是生存领取表的情况
             * 根据LJAGetDraw（生存领取）表中的GetDutyCode（给付责任编码）到LMDutyGet
             * （给付责任表）中查询出GetType1（给付分类）--（0：满期、其余是年金）
             */
            System.out.println("查询出的号码是" + t_ssrs.GetText(i, 1));
            System.out.println("查询出的类型是" + t_ssrs.GetText(i, 2));
            //分红查询的sql语句
            String FenHong_sql =
                    " and RiskCode in ( Select RiskCode From LMRiskApp"
                    + " Where RiskType3 = '2')";
            //投连查询的sql语句
            String TouLian_sql =
                    " and RiskCode in ( Select RiskCode From LMRiskApp "
                    + " Where RiskType3 = '3')";
            //传统查询的sql语句
            String ChuanTong_sql =
                    " and RiskCode in ( Select RiskCode From LMRiskApp"
                    + " Where RiskType3 in ('1','4','5'))";
            System.out.println("￥￥分红" + FenHong_sql);
            System.out.println("投连" + TouLian_sql);
            System.out.println("传统" + ChuanTong_sql);

            if ((t_ssrs.GetText(i, 2).equals("0")) ||
                (t_ssrs.GetText(i, 2).equals("1"))
                || (t_ssrs.GetText(i, 2).equals("2")))
            {
                //先判断是年金还是满期GetType1=0是满期；GetType1<>0是年金
                System.out.println("判断是年金还是满期");
                String tType =
                        " Select GetType1 From LMDutyGet Where GetDutyCode = "
                        +
                        "( Select GetDutyCode From LJAGetDraw Where ActuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' )";
                ExeSQL tType_exesql = new ExeSQL();
                SSRS tType_ssrs = tType_exesql.execSQL(tType);
                if (tType_ssrs.getMaxRow() != 0)
                {
                    System.out.println("年金、满期的判断标志是" + tType_ssrs.GetText(1, 1));
                    if (tType_ssrs.GetText(1, 1).equals("0"))
                    {
                        //满期分红的查询
                        String ManQiFH_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 = '0') "
                                + FenHong_sql;
                        System.out.println("￥￥满期分红的查询" + ManQiFH_sql);
                        SSRS ManQiFH_ssrs = t_exesql.execSQL(ManQiFH_sql);
                        MFJE = MFJE +
                               Double.parseDouble(ManQiFH_ssrs.GetText(1, 1));

                        //满期投连的查询
                        String ManQiTL_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 = '0') "
                                + TouLian_sql;

                        System.out.println("￥￥满期投连的查询" + ManQiTL_sql);
                        SSRS ManQiTL_ssrs = t_exesql.execSQL(ManQiTL_sql);
                        MTJE = MTJE +
                               Double.parseDouble(ManQiTL_ssrs.GetText(1, 1));

                        //满期传统的查询
                        String ManQiCT_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End  From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 = '0') "
                                + ChuanTong_sql;

                        System.out.println("￥￥满期传统的查询" + ManQiCT_sql);
                        SSRS ManQiCT_ssrs = t_exesql.execSQL(ManQiCT_sql);
                        MCJE = MCJE +
                               Double.parseDouble(ManQiCT_ssrs.GetText(1, 1));
                    }
                    else
                    {
                        //年金分红信息的查询
                        String NianJinFH_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 <> '0')"
                                + FenHong_sql;
                        System.out.println("对年金分红的查询");
                        SSRS NianJinFH_ssrs = t_exesql.execSQL(NianJinFH_sql);
                        NFJE = NFJE +
                               Double.parseDouble(NianJinFH_ssrs.GetText(1, 1));

                        //年金投连信息的查询
                        String NianJinTL_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 <> '0')"
                                + TouLian_sql;
                        System.out.println("年金投连的查询" + NianJinTL_sql);
                        SSRS NianJinTL_ssrs = t_exesql.execSQL(NianJinTL_sql);
                        NTJE = NTJE +
                               Double.parseDouble(NianJinTL_ssrs.GetText(1, 1));

                        //年金传统信息的查询
                        String NianJinCT_sql =
                                "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetDraw Where ActuGetNo = '"
                                + t_ssrs.GetText(i, 1) +
                                "' and  GetDutyCode in "
                                +
                                "(Select GetDutyCode From LMDutyGet Where GetType1 <> '0')"
                                + ChuanTong_sql;
                        System.out.println("对年金传统的查询" + NianJinCT_sql);
                        SSRS NianJinCT_ssrs = t_exesql.execSQL(NianJinCT_sql);
                        NCJE = NCJE +
                               Double.parseDouble(NianJinCT_ssrs.GetText(1, 1));
                    }
                }
            }
            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * 对于理赔的情况：当OtherNotype＝＝5时，表示理赔
             * 在LJAGetClaim（赔付实付表）中进行数据的查询
             *
             */

            if (t_ssrs.GetText(i, 2).equals("5"))
            {
                //理赔分红
                String LiPeiFH_sql =
                        "Select  Case when Sum(Pay) Is Null Then 0 Else Sum(Pay) End From LJAGetClaim "
                        + "Where ActuGetNo = '" + t_ssrs.GetText(i, 1) + "'"
                        + FenHong_sql;
                System.out.println("理赔分红" + LiPeiFH_sql);
                SSRS LiPeiFH_ssrs = t_exesql.execSQL(LiPeiFH_sql);
                LFJE = LFJE + Double.parseDouble(LiPeiFH_ssrs.GetText(1, 1));

                //理赔投连
                String LiPeiTL_sql = "Select Case when Sum(Pay) Is Null Then 0 Else Sum(Pay) End From LJAGetClaim "
                                     + "Where ActuGetNo = '" +
                                     t_ssrs.GetText(i, 1) + "'"
                                     + TouLian_sql;
                System.out.println("理赔投连" + LiPeiTL_sql);
                SSRS LiPeiTL_ssrs = t_exesql.execSQL(LiPeiTL_sql);
                LTJE = LTJE + Double.parseDouble(LiPeiTL_ssrs.GetText(1, 1));

                //理赔传统
                String LiPeiCT_sql =
                        "Select Case when Sum(Pay) Is Null Then 0 Else Sum(Pay) End From LJAGetClaim "
                        + "Where ActuGetNo = '" + t_ssrs.GetText(i, 1) + "'"
                        + ChuanTong_sql;
                System.out.println("理赔传统" + LiPeiCT_sql);
                SSRS LiPeiCT_ssrs = t_exesql.execSQL(LiPeiCT_sql);
                LCJE = LCJE + Double.parseDouble(LiPeiCT_ssrs.GetText(1, 1));
            }

            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * function :查询退保金情况---犹豫期撤单的操作 对LJAGetEndorse（批改补退费表）进行查询信息。
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType='WT'
             ******/
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //犹豫期撤单分红
                String TuiBaoJinFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeOperationType='WT' "
                        + FenHong_sql;
                System.out.println("退保金分红情况" + TuiBaoJinFH_sql);
                SSRS TuiBaoJinFH_ssrs = t_exesql.execSQL(TuiBaoJinFH_sql);
                TFJE = TFJE + Double.parseDouble(TuiBaoJinFH_ssrs.GetText(1, 1));
                System.out.println("当i= " + i + "时，退保金－分红-次数的合计是" + TFCS);

                //犹豫期撤单投连
                String TuiBaoJinTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeOperationType='WT'"
                        + TouLian_sql;
                System.out.println("退保金投连" + TuiBaoJinTL_sql);
                SSRS TuiBaoJinTL_ssrs = t_exesql.execSQL(TuiBaoJinTL_sql);
                TTJE = TTJE + Double.parseDouble(TuiBaoJinTL_ssrs.GetText(1, 1));
                System.out.println("当i= " + i + "时，退保金－投连-次数的合计是" + TTCS);

                //犹豫期撤单传统
                String TuiBaoJinCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeOperationType='WT'"
                        + ChuanTong_sql;
                System.out.println("退保金传统" + TuiBaoJinCT_sql);
                SSRS TuiBaoJinCT_ssrs = t_exesql.execSQL(TuiBaoJinCT_sql);
                TCJE = TCJE + Double.parseDouble(TuiBaoJinCT_ssrs.GetText(1, 1));
            }

            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * function :查询退保金情况---退保金的操作 对LJAGetEndorse（批改补退费表）进行查询信息。
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='TB'
             ******/
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //退保金分红
                String TuiBaoJin2FH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType='TB' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("退保金分红情况" + TuiBaoJin2FH_sql);
                SSRS TuiBaoJin2FH_ssrs = t_exesql.execSQL(TuiBaoJin2FH_sql);
                T2FJE = T2FJE +
                        Double.parseDouble(TuiBaoJin2FH_ssrs.GetText(1, 1));

                //退保金投连
                String TuiBaoJin2TL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType='TB' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS TuiBaoJin2TL_ssrs = t_exesql.execSQL(TuiBaoJin2TL_sql);
                T2TJE = T2TJE +
                        Double.parseDouble(TuiBaoJin2TL_ssrs.GetText(1, 1));

                //退保金传统
                String TuiBaoJin2CT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType='TB' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS TuiBaoJin2CT_ssrs = t_exesql.execSQL(TuiBaoJin2CT_sql);
                T2CJE = T2CJE +
                        Double.parseDouble(TuiBaoJin2CT_ssrs.GetText(1, 1));
            }

            /*************************************************************************
             * Date     :2003-12-15
             * Author   :LiuYansong
             * Function :查询是余额的情况
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='EY'
             */
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //余额分红
                String YuEFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'EY' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("余额分红金额是" + YuEFH_sql);
                SSRS YuEFH_ssrs = t_exesql.execSQL(YuEFH_sql);
                EYFJE = EYFJE + Double.parseDouble(YuEFH_ssrs.GetText(1, 1));

                //余额投连
                String YuETL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'EY' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS YuETL_ssrs = t_exesql.execSQL(YuETL_sql);
                EYTJE = EYTJE + Double.parseDouble(YuETL_ssrs.GetText(1, 1));

                //余额传统
                String YuECT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'EY' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS YuECT_ssrs = t_exesql.execSQL(YuECT_sql);
                EYCJE = EYCJE + Double.parseDouble(YuECT_ssrs.GetText(1, 1));
            }

            /***********************************************************************
             * date:2003-12-12
             * 项目：批改补退费的其他项目。与后面的其他项目进行合计
             * author:lys
             * function :查询退保金情况---退保金的操作 对LJAGetEndorse（批改补退费表）进行查询信息。
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='TF'
             */
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //退费分红
                String TuiFeiFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'TF' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("退费分红金额是" + TuiFeiFH_sql);
                SSRS TuiFeiFH_ssrs = t_exesql.execSQL(TuiFeiFH_sql);
                TuiFFJE = TuiFFJE +
                          Double.parseDouble(TuiFeiFH_ssrs.GetText(1, 1));

                //退费投连
                String TuiFeiTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'TF' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS TuiFeiTL_ssrs = t_exesql.execSQL(TuiFeiTL_sql);
                TuiFTJE = TuiFTJE +
                          Double.parseDouble(TuiFeiTL_ssrs.GetText(1, 1));

                //退费传统
                String TuiFeiCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'TF' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS TuiFeiCT_ssrs = t_exesql.execSQL(TuiFeiCT_sql);
                TuiFCJE = TuiFCJE +
                          Double.parseDouble(TuiFeiCT_ssrs.GetText(1, 1));
            }
            /*************************************************************************
             * Date     :2003-12-15
             * Author   :LiuYansong
             * Function :应付业务支持的"付款"类型的操作
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='JK'
             *
             */
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //借款分红
                String JieKuanFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'JK' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("借款分红金额是" + JieKuanFH_sql);
                SSRS JieKuanFH_ssrs = t_exesql.execSQL(JieKuanFH_sql);
                JKFJE = JKFJE + Double.parseDouble(JieKuanFH_ssrs.GetText(1, 1));

                //借款投连
                String JieKuanTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'JK' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS JieKuanTL_ssrs = t_exesql.execSQL(JieKuanTL_sql);
                JKTJE = JKTJE + Double.parseDouble(JieKuanTL_ssrs.GetText(1, 1));
                System.out.println("2003-12-15借款投连金额是" + JKTJE);

                //借款传统
                String JieKuanCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'JK' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS JieKuanCT_ssrs = t_exesql.execSQL(JieKuanCT_sql);
                JKCJE = JKCJE + Double.parseDouble(JieKuanCT_ssrs.GetText(1, 1));
                System.out.println("2003-12-15借款传统是" + JKCJE);
            }
            /*************************************************************************
             * CreateDate : 2003-12-19
             * Author     : LiuYansong
             * Function   : 处理还款的相关金额
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='HK'
             */
            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //还款分红
                String HuanKuanFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'HK' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("还款分红金额是" + HuanKuanFH_sql);
                SSRS HuanKuanFH_ssrs = t_exesql.execSQL(HuanKuanFH_sql);
                HKFJE = HKFJE + Double.parseDouble(HuanKuanFH_ssrs.GetText(1, 1));

                //还款投连
                String HuanKuanTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'HK' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS HuanKuanTL_ssrs = t_exesql.execSQL(HuanKuanTL_sql);
                HKTJE = HKTJE + Double.parseDouble(HuanKuanTL_ssrs.GetText(1, 1));
                System.out.println("2003-12-19还款投连金额是" + HKTJE);

                //还款传统
                String HuanKuanCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'HK' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS HuanKuanCT_ssrs = t_exesql.execSQL(HuanKuanCT_sql);
                HKCJE = HKCJE + Double.parseDouble(HuanKuanCT_ssrs.GetText(1, 1));
                System.out.println("2003-12-19还款传统是" + HKCJE);
            }

            /*************************************************************************
             * CreateDate : 2003-12-19
             * Author     : LiuYansong
             * Function   : 处理利息的相关金额
             * OtherTypeNo ==3时是退保金情况
             * FeeOperationType!='WT'
             * FeeFinaType='LX'
             */

            if (t_ssrs.GetText(i, 2).equals("3"))
            {
                //利息分红
                String LiXiFH_sql =
                        " Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'LX' and FeeOperationType <>'WT' "
                        + FenHong_sql;
                System.out.println("利息分红金额是" + LiXiFH_sql);
                SSRS LiXiFH_ssrs = t_exesql.execSQL(LiXiFH_sql);
                LXFJE = LXFJE + Double.parseDouble(LiXiFH_ssrs.GetText(1, 1));

                //利息投连
                String LiXiTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'LX' and FeeOperationType <>'WT' "
                        + TouLian_sql;
                SSRS LiXiTL_ssrs = t_exesql.execSQL(LiXiTL_sql);
                LXTJE = LXTJE + Double.parseDouble(LiXiTL_ssrs.GetText(1, 1));
                System.out.println("2003-12-19利息投连金额是" + LXTJE);

                //利息传统
                String LiXiCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetEndorse "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) +
                        "' and FeeFinaType = 'LX' and FeeOperationType <>'WT' "
                        + ChuanTong_sql;
                SSRS LiXiCT_ssrs = t_exesql.execSQL(LiXiCT_sql);
                LXCJE = LXCJE + Double.parseDouble(LiXiCT_ssrs.GetText(1, 1));
                System.out.println("2003-12-19利息传统是" + LXCJE);
            }

            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * function :处理红利情况。查询LJABonusGet（红利给付实付表）
             * OtherNoType = 7
             * LJABonusGet表中的OtherNO 对应于LCPol表中的PolNo
             *
             */
            if (t_ssrs.GetText(i, 2).equals("7"))
            {
                //红利/分红/总金额
                String HongLiFHJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJABonusGet Where actuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 ='2'))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 = '2')))";
                System.out.println("红利/分红/总金额" + HongLiFHJE_sql);
                SSRS HongLIFHJE_ssrs = t_exesql.execSQL(HongLiFHJE_sql);
                HFJE = HFJE + Double.parseDouble(HongLIFHJE_ssrs.GetText(1, 1));

                //红利/投连/总金额
                String HongLiTLJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJABonusGet Where actuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 ='3'))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 = '3')))";
                System.out.println("红利/投连/总金额" + HongLiTLJE_sql);
                SSRS HongLiTLJE_ssrs = t_exesql.execSQL(HongLiTLJE_sql);
                HTJE = HTJE + Double.parseDouble(HongLiTLJE_ssrs.GetText(1, 1));

                //红利/传统/总金额
                String HongLiCTJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJABonusGet Where actuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5')))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5'))))";
                System.out.println("红利/传统/总金额" + HongLiCTJE_sql);
                SSRS HongLiCTJE_ssrs = t_exesql.execSQL(HongLiCTJE_sql);
                HCJE = HCJE + Double.parseDouble(HongLiCTJE_ssrs.GetText(1, 1));
            }
            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * function 处理暂收退费情况.
             * OtherNoType = 4 ,LJAGetTempFee(赞交费退费实付表)
             **/
            if (t_ssrs.GetText(i, 2).equals("4"))
            {
                //暂收退费分红
                String ZanShouFH_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetTempFee "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) + "'"
                        + FenHong_sql;
                System.out.println("暂收退费/分红 " + ZanShouFH_sql);
                SSRS ZanShouFH_ssrs = t_exesql.execSQL(ZanShouFH_sql);
                ZFJE = ZFJE + Double.parseDouble(ZanShouFH_ssrs.GetText(1, 1));

                //暂收退费投连
                String ZanShouTL_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetTempFee "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) + "'"
                        + TouLian_sql;
                System.out.println("暂收退费/投连" + ZanShouTL_sql);
                SSRS ZanShouTL_ssrs = t_exesql.execSQL(ZanShouTL_sql);
                ZTJE = ZTJE + Double.parseDouble(ZanShouTL_ssrs.GetText(1, 1));
                System.out.println("当i = " + i + "时，暂收退费－投连－次数合计是" + ZTCS);

                //暂收退费传统
                String ZanShouCT_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End  From LJAGetTempFee "
                        + " Where ActuGetNo = '" + t_ssrs.GetText(i, 1) + "'"
                        + ChuanTong_sql;
                System.out.println("暂收退费/传统" + ZanShouCT_sql);
                SSRS ZanShouCT_ssrs = t_exesql.execSQL(ZanShouCT_sql);
                ZCJE = ZCJE + Double.parseDouble(ZanShouCT_ssrs.GetText(1, 1));
                System.out.println("当i = " + i + "时，暂收退费－传统－次数合计是" + ZCCS);
            }
            /***********************************************************************
             * date:2003-05-29
             * author:lys
             * function :处理其他信息。LJAGetOther（其他退费实付表）
             * OtherNoType = 6 OR 8
             * 查询RiskCode 时 OtherNO = LCPol表中的PolNo
             *
             **/
            if ((t_ssrs.GetText(i, 2).equals("6")) ||
                (t_ssrs.GetText(i, 2).equals("8")))
            {
                //其他/分红/总金额
                String QiTaFHJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetOther Where ActuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 ='2'))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 = '2'))"
                        + " or OtherNo in (Select GrpPolNo From LCGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 = '2'))"
                        + " or OtherNo in (Select GrpPolNo From LBGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 = '2')))";
                System.out.println("其他/分红/总金额" + QiTaFHJE_sql);
                SSRS QiTaFHJE_ssrs = t_exesql.execSQL(QiTaFHJE_sql);
                QFJE = QFJE + Double.parseDouble(QiTaFHJE_ssrs.GetText(1, 1));

                //其他/投连/总金额
                String QiTaTLJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetOther Where ActuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 ='3'))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 = '3'))"
                        + " or OtherNo in (Select GrpPolNo From LCGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 = '3'))"
                        + " or OtherNo in (Select GrpPolNo From LBGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 = '3') ))";
                System.out.println("其他/投连/总金额" + QiTaTLJE_sql);
                SSRS QiTaTLJE_ssrs = t_exesql.execSQL(QiTaTLJE_sql);
                QTJE = QTJE + Double.parseDouble(QiTaTLJE_ssrs.GetText(1, 1));

                //其他/传统/总金额
                String QiTaCTJE_sql =
                        "Select Case when Sum(GetMoney) Is Null Then 0 Else Sum(GetMoney) End From LJAGetOther Where actuGetNo = '"
                        + t_ssrs.GetText(i, 1) + "' and ( OtherNo in "
                        + " (( Select PolNo From LCPol Where RiskCode in "
                        +
                        " (Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5')))) "
                        +
                        " or OtherNo in (Select PolNo From LBPol Where RiskCode in "
                        +
                        "(Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5')))"
                        + " or OtherNo in (Select GrpPolNo From LCGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5')))"
                        + " or OtherNo in (Select GrpPolNo From LBGrpPol Where RiskCode in (Select RiskCode From LMRiskApp Where RiskType3 in ('1','4','5'))))";
                System.out.println("其他/传统/总金额" + QiTaCTJE_sql);
                SSRS QiTaCTJE_ssrs = t_exesql.execSQL(QiTaCTJE_sql);
                QCJE = QCJE + Double.parseDouble(QiTaCTJE_ssrs.GetText(1, 1));
            }
        }
        /**************************************************************************
         * 对vts上的变量进行附值
         * CS :次数
         * JE :金额
         **/

        NCS = NFCS + NTCS + NCCS; //年金次数
        NJE = NFJE + NTJE + NCJE;

        MCS = MFCS + MTCS + MCCS; //满期次数
        MJE = MFJE + MTJE + MCJE;

        LCS = LFCS + LTCS + LCCS; //理赔次数
        LJE = LFJE + LTJE + LCJE;

        TCS = TFCS + TTCS + TCCS; //退保金---犹豫期撤单次数
        TJE = TFJE + TTJE + TCJE;

        T2JE = T2FJE + T2TJE + T2CJE; //退保金---退保金

        HCS = HFCS + HTCS + HCCS; //红利次数
        HJE = HFJE + HTJE + HCJE;

        ZCS = ZFCS + ZTCS + ZCCS; //暂收退费次数
        ZJE = ZFJE + ZTJE + ZCJE;

        QCS = QFCS + QTCS + QCCS; //其他次数

        QJE = QFJE + QTJE + QCJE;
        //退费金额
        TuiFJE = TuiFFJE + TuiFTJE + TuiFCJE;
        //借款金额
        JKJE = JKFJE + JKTJE + JKCJE;
        //余额金额
        EYJE = EYFJE + EYTJE + EYCJE;
        //还款金额
        HKJE = HKFJE + HKTJE + HKCJE;
        //利息金额
        LXJE = LXFJE + LXTJE + LXCJE;
        //总金额
        JE = NJE + MJE + LJE + TJE + HJE + ZJE + QJE + T2JE + TuiFJE + EYJE +
             JKJE + HKJE + LXJE;

        //对vts进行附值
        TextTag mtexttag = new TextTag();
        mtexttag.add("StartDate", mDay[0]); //开始日期
        mtexttag.add("EndDate", mDay[1]); //结束日期

        mtexttag.add("JE", new DecimalFormat("0.00").format(JE));

        mtexttag.add("NJE", new DecimalFormat("0.00").format(NJE));
        mtexttag.add("NFJE", new DecimalFormat("0.00").format(NFJE));
        mtexttag.add("NTJE", new DecimalFormat("0.00").format(NTJE));
        mtexttag.add("NCJE", new DecimalFormat("0.00").format(NCJE));

        mtexttag.add("MJE", new DecimalFormat("0.00").format(MJE));
        mtexttag.add("MFJE", new DecimalFormat("0.00").format(MFJE));
        mtexttag.add("MTJE", new DecimalFormat("0.00").format(MTJE));
        mtexttag.add("MCJE", new DecimalFormat("0.00").format(MCJE));

        mtexttag.add("LJE", new DecimalFormat("0.00").format(LJE));
        mtexttag.add("LFJE", new DecimalFormat("0.00").format(LFJE));
        mtexttag.add("LTJE", new DecimalFormat("0.00").format(LTJE));
        mtexttag.add("LCJE", new DecimalFormat("0.00").format(LCJE));

        mtexttag.add("TJE", new DecimalFormat("0.00").format(TJE)); //犹豫期撤单
        mtexttag.add("TFJE", new DecimalFormat("0.00").format(TFJE));
        mtexttag.add("TTJE", new DecimalFormat("0.00").format(TTJE));
        mtexttag.add("TCJE", new DecimalFormat("0.00").format(TCJE));

        mtexttag.add("T2JE", new DecimalFormat("0.00").format(T2JE)); //退保金
        mtexttag.add("T2FJE", new DecimalFormat("0.00").format(T2FJE));
        mtexttag.add("T2TJE", new DecimalFormat("0.00").format(T2TJE));
        mtexttag.add("T2CJE", new DecimalFormat("0.00").format(T2CJE));

        mtexttag.add("HJE", new DecimalFormat("0.00").format(HJE));
        mtexttag.add("HFJE", new DecimalFormat("0.00").format(HFJE));
        mtexttag.add("HTJE", new DecimalFormat("0.00").format(HTJE));
        mtexttag.add("HCJE", new DecimalFormat("0.00").format(HCJE));

        mtexttag.add("ZJE", new DecimalFormat("0.00").format(ZJE));
        mtexttag.add("ZFJE", new DecimalFormat("0.00").format(ZFJE));
        mtexttag.add("ZTJE", new DecimalFormat("0.00").format(ZTJE));
        mtexttag.add("ZCJE", new DecimalFormat("0.00").format(ZCJE));

        mtexttag.add("QJE", new DecimalFormat("0.00").format(QJE));
        mtexttag.add("QFJE", new DecimalFormat("0.00").format(QFJE));
        mtexttag.add("QTJE", new DecimalFormat("0.00").format(QTJE));
        mtexttag.add("QCJE", new DecimalFormat("0.00").format(QCJE));

        //增加退费的金额
        mtexttag.add("TuiFJE", new DecimalFormat("0.00").format(TuiFJE));
        mtexttag.add("TuiFFJE", new DecimalFormat("0.00").format(TuiFFJE));
        mtexttag.add("TuiFTJE", new DecimalFormat("0.00").format(TuiFTJE));
        mtexttag.add("TuiFCJE", new DecimalFormat("0.00").format(TuiFCJE));

        //增加余额的金额
        mtexttag.add("EYJE", new DecimalFormat("0.00").format(EYJE));
        mtexttag.add("EYFJE", new DecimalFormat("0.00").format(EYFJE));
        mtexttag.add("EYTJE", new DecimalFormat("0.00").format(EYTJE));
        mtexttag.add("EYCJE", new DecimalFormat("0.00").format(EYCJE));

        //增加借款的金额
        mtexttag.add("JKJE", new DecimalFormat("0.00").format(JKJE));
        mtexttag.add("JKFJE", new DecimalFormat("0.00").format(JKFJE));
        mtexttag.add("JKTJE", new DecimalFormat("0.00").format(JKTJE));
        mtexttag.add("JKCJE", new DecimalFormat("0.00").format(JKCJE));

        //增加还款的金额
        mtexttag.add("HKJE", new DecimalFormat("0.00").format(HKJE));
        mtexttag.add("HKFJE", new DecimalFormat("0.00").format(HKFJE));
        mtexttag.add("HKTJE", new DecimalFormat("0.00").format(HKTJE));
        mtexttag.add("HKCJE", new DecimalFormat("0.00").format(HKCJE));

        //增加利息的金额
        mtexttag.add("LXJE", new DecimalFormat("0.00").format(LXJE));
        mtexttag.add("LXFJE", new DecimalFormat("0.00").format(LXFJE));
        mtexttag.add("LXTJE", new DecimalFormat("0.00").format(LXTJE));
        mtexttag.add("LXCJE", new DecimalFormat("0.00").format(LXCJE));

        if (mtexttag.size() > 0)
        {
            xmlexport.addTextTag(mtexttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","财务日结");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }
}
