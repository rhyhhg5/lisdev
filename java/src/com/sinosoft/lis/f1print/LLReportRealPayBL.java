package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */


import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLReportRealPayBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mOperate = "";

    private String Operate = "";

    private String StartDate;

    private String EndDate;

    private String ManageCom;

    private String AmntSpan;
    
    private  SSRS tsumamnt ,PREMS;
    

    public LLReportRealPayBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));

        if (mTransferData == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        ManageCom = (String) mTransferData.getValueByName("ManageCom");
        AmntSpan = (String) mTransferData.getValueByName("AmntSpan");
        Operate = mGlobalInput.Operator;
        if(!queryData()){
        	return false ;
        }else{
         	printCaseReport();
         	 return true;
        }
        //    if(mOperate.equals("RESULT"))
        //        printRsuReport();
       
    }
    
    //控制数据不为空，以免在JSP报空指异常
    
    private boolean queryData(){
    	
    	 ExeSQL esql = new ExeSQL();
    	 
         String exeSqlx1 = " select max(a.realpay) from llclaim a,llcase b where b.endcasedate between '"
                 + StartDate
                 + "' and '"
                 + EndDate
                 + " ' and b.rgttype='1' "
                 + " and b.rgtstate in ('09','11','12') "
                 + " and b.mngcom like '"
                 + ManageCom + "%' and a.caseno=b.caseno with ur";
         System.out.println("Sql1  "+ exeSqlx1);
         tsumamnt = esql.execSQL(exeSqlx1);
        
         
         System.out.println("==tsumamnt.getMaxRow()===  "+tsumamnt.getMaxRow()+" tsumamnt.GetText(1, 1).toString().equals(null)>>>"+tsumamnt.GetText(1, 1).toString().equals("null")+"<<<");
         if(tsumamnt.getMaxRow()==0||tsumamnt.GetText(1, 1).trim().equals("null")){
        	 
         	 System.out.println("  tsumamnt 没有查询到相关数据！");
        	 buildError("queryData", "没有查询到相关数据！");
        	 
        	 return false ;
        	
         }
         
         
         String exeSql2 = "SELECT COUNT(distinct A.CASENO),coalesce(SUM(A.REALPAY),0) FROM LLCLAIM A,LLCASE A1 WHERE A1.endcasedate between '"
             + StartDate
             + "' and '"
             + EndDate
             + "' and  A1.RGTTYPE='1' AND A1.rgtstate in('09','11','12') "
             + " AND A1.MNGCOM like '"
             + ManageCom + "%' and A1.CASENO=A.CASENO ";
         System.out.println(" Sql2  " +exeSql2);
         PREMS = esql.execSQL(exeSql2);
         System.out.println("==========PREMS.getMaxRow()==========  "+PREMS.getMaxRow());
         if(null!=PREMS && PREMS.getMaxRow() ==0){
        	 System.out.println("  PREMS 没有查询到相关数据！");
        	 buildError("queryData", "没有查询到相关数据！");
        	 
        	 return false ;
         }
     
    	return true ;
    }
    
    private void printCaseReport()
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        System.out.println("总件数，总金额查询开始  "+PubFun.getCurrentTime());
        String[] strCol;
        ListTable appColListTable = new ListTable();
        appColListTable.setName("Report");
        String[] Title = { "AmntSpan", "TCaseNo", "TRealPay", "TCaseRate",
                "TPayRate", "TCM1", "TMM1", "TCM2", "TMM2", "TCM3", "TMM3",
                "TCM4", "TMM4", "TMM5", "TMM5" };
        int span = Integer.parseInt(AmntSpan);
        int amnt = span;
        int tempamnt = 0;
        double[] tempValue = new double[10];
        
       /* ExeSQL esql = new ExeSQL();
        String exeSqlx1 = " select max(a.realpay) from llclaim a,llcase b where b.endcasedate between '"
                + StartDate
                + "' and '"
                + EndDate
                + " ' and b.rgttype='1' "
                + " and b.rgtstate in ('09','11','12') "
                + " and b.mngcom like '"
                + ManageCom + "%' and a.caseno=b.caseno with ur";

        SSRS tsumamnt = esql.execSQL(exeSqlx1);*/
        System.out.println("tsumamnt.GetText(1, 1)=== "+tsumamnt.GetText(1, 1));
        double sumamnt = Double.parseDouble(tsumamnt.GetText(1, 1));

       /* String exeSql1 = "SELECT COUNT(distinct A.CASENO),coalesce(SUM(A.REALPAY),0) FROM LLCLAIM A,LLCASE A1 WHERE A1.endcasedate between '"
                + StartDate
                + "' and '"
                + EndDate
                + "' and  A1.RGTTYPE='1' AND A1.rgtstate in('09','11','12') "
                + " AND A1.MNGCOM like '"
                + ManageCom + "%' and A1.CASENO=A.CASENO ";

        SSRS PREMS = esql.execSQL(exeSql1);*/
        String[][] tempdata = null;

        
        if (PREMS != null && PREMS.getMaxRow() > 0)
            tempdata = PREMS.getAllData();

        String tsumcount = tempdata[0][0];


        double sumcont0 = Double.parseDouble(tsumcount); //总件数
        double sumprem0 = Double.parseDouble(tempdata[0][1]); //总金额
        System.out.println("总件数，总金额查询结束 "+PubFun.getCurrentTime());
       
        while (tempamnt <= sumamnt)
        {
            System.out.println("这是从"+tempamnt+" 到"+amnt+" 开始 "+PubFun.getCurrentTime());
            ExeSQL exesql = new ExeSQL();
            String sql0 = "SELECT COUNT(distinct A.CASENO),coalesce(SUM(A.REALPAY),0) FROM LLCLAIM A,LLCASE A1 "
                    + " WHERE A1.MNGCOM like '"+ManageCom+"%' and A1.endcasedate between '"+StartDate+ "' and '" + EndDate +"'  AND A1.RGTTYPE='1' AND A1.rgtstate in ('09','11','12') AND A.REALPAY>="
                    + tempamnt
                    + " AND A.REALPAY<"
                    + amnt
                    + "  and A1.CASENO=A.CASENO ";

            String sqlbase0 = "SELECT COUNT(distinct A.CASENO),coalesce(SUM(A.REALPAY),0) FROM LLCLAIM A,LLCASE A1 WHERE A1.MNGCOM like '"+ManageCom+"%' and A1.endcasedate between '"+ StartDate
                    + "' and '"+EndDate+"'  and A1.RGTTYPE='1' AND A1.rgtstate in ('09','11','12') AND A.REALPAY>="
                    + tempamnt
                    + " AND A.REALPAY<"
                    + amnt
                    + " and A1.CASENO=A.CASENO ";
            String sql1 = sqlbase0 + " AND A.GIVETYPE='1' WITH UR ";
            String sql2 = sqlbase0 + " AND A.GIVETYPE='2' WITH UR ";
            String sql3 = "select count(H),coalesce(sum(G),0) from (select H,coalesce(sum(F),0) G from ( "
                    + "select A1.CASENO H,case when declineamnt =0 then outdutyamnt else declineamnt end  F "
                    + "FROM LLCASE A1,LLCLAIMDETAIL A2,llclaim A3 "
                    + "WHERE A1.MNGCOM like '"+ManageCom+"%' and A1.endcasedate between '"+StartDate+ "' and '"+EndDate+"' AND A1.RGTTYPE='1' AND A1.rgtstate in ('09','11','12') "
                    + " and A3.GIVETYPE='3' and A1.CASENO=A3.CASENO and A1.CASENO=A2.CASENO) as x group by H ) "
                    + " as x where  G >=" + tempamnt + " and G<" + amnt;
            String sql4 = sqlbase0 + " AND A.GIVETYPE='4' WITH UR ";
            String sql5 = sqlbase0 + " AND A.GIVETYPE='5' WITH UR ";
            SSRS COUNT0 = exesql.execSQL(sql0);
            SSRS COUNT1 = exesql.execSQL(sql1);
            SSRS COUNT2 = exesql.execSQL(sql2);
            SSRS COUNT3 = exesql.execSQL(sql3);
            SSRS COUNT4 = exesql.execSQL(sql4);
            SSRS COUNT5 = exesql.execSQL(sql5);
            String[][] temdata0 = null;
            String[][] temdata1 = null;
            String[][] temdata2 = null;
            String[][] temdata3 = null;
            String[][] temdata4 = null;
            String[][] temdata5 = null;
            //        String[][] temdata6 = null;
            if (COUNT0 != null && COUNT0.getMaxRow() > 0)
            {
                temdata0 = COUNT0.getAllData(); //金额段内 总计
            }
            if (COUNT1 != null && COUNT1.getMaxRow() > 0)
            {
                temdata1 = COUNT1.getAllData(); //金额段内 正常给付
            }
            if (COUNT2 != null && COUNT2.getMaxRow() > 0)
            {
                temdata2 = COUNT2.getAllData(); //金额段内 部分给付
            }
            if (COUNT3 != null && COUNT3.getMaxRow() > 0)
            {
                temdata3 = COUNT3.getAllData(); //金额段内 全额拒付
            }
            if (COUNT4 != null && COUNT4.getMaxRow() > 0)
            {
                temdata4 = COUNT4.getAllData(); //金额段内 协议给付
            }
            if (COUNT5 != null && COUNT5.getMaxRow() > 0)
            {
                temdata5 = COUNT5.getAllData(); //金额段内 通融给付
            }

            strCol = new String[15];
            strCol[0] = amnt + "元";
            strCol[1] = temdata0[0][0];
            strCol[2] = temdata0[0][1];
            if (sumcont0 == 0)
                strCol[3] = "0";
            else
            {
                double claimrate = Double.parseDouble(strCol[1]) / sumcont0
                        * 100;
                claimrate = Arith.round(claimrate, 2);
                strCol[3] = claimrate + "";
            }

            if (sumprem0 < 0.001)
                strCol[4] = "0";
            else
            {
                double claimrate = Double.parseDouble(strCol[2]) / sumprem0
                        * 100;
                claimrate = Arith.round(claimrate, 2);
                strCol[4] = claimrate + "";
            }
            strCol[5] = temdata1[0][0];
            strCol[6] = temdata1[0][1];
            strCol[7] = temdata2[0][0];
            strCol[8] = temdata2[0][1];
            strCol[9] = temdata3[0][0];
            strCol[10] = temdata3[0][1];
            strCol[11] = temdata4[0][0];
            strCol[12] = temdata4[0][1];
            strCol[13] = temdata5[0][0];
            strCol[14] = temdata5[0][1];
            appColListTable.add(strCol);
            System.out.println("这是从"+tempamnt+" 到"+amnt+" 结束 "+PubFun.getCurrentTime());
            tempamnt = amnt;
            amnt += span;
            tempValue[0]+=Double.parseDouble(strCol[5]);
            tempValue[1]+=Double.parseDouble(strCol[6]);
            tempValue[2]+=Double.parseDouble(strCol[7]);
            tempValue[3]+=Double.parseDouble(strCol[8]);
            tempValue[4]+=Double.parseDouble(strCol[9]);
            tempValue[5]+=Double.parseDouble(strCol[10]);
            tempValue[6]+=Double.parseDouble(strCol[11]);
            tempValue[7]+=Double.parseDouble(strCol[12]);
            tempValue[8]+=Double.parseDouble(strCol[13]);
            tempValue[9]+=Double.parseDouble(strCol[14]);
           
            
        }
//        ExeSQL exesql = new ExeSQL();
//        System.out.println("最后合计 "+" 开始  "+PubFun.getCurrentTime());
//        String sqlbase0 = "SELECT COUNT(distinct A.CASENO),coalesce(SUM(A.REALPAY),0) FROM LLCLAIM A,LLCASE A1 WHERE "
//                + " A1.MNGCOM like '"+ManageCom+"%' and A1.endcasedate between ' "+StartDate+"' and '"+EndDate+"' AND A1.RGTTYPE='1' AND A1.rgtstate in ('09','11','12') "
//                + " and A1.CASENO=A.CASENO";
//        String sql1 = sqlbase0 + " AND A.GIVETYPE='1' ";
//        String sql2 = sqlbase0 + " AND A.GIVETYPE='2' ";
//
//        String sql3 = "select count(distinct H),coalesce(sum(F),0) from ( "
//                + "select A1.CASENO H,case when declineamnt =0 then outdutyamnt else declineamnt end  F "
//                + "FROM LLCASE A1,LLCLAIMDETAIL A2,llclaim A3 "
//                + "WHERE A1.MNGCOM like '"+ManageCom+"%' and A1.endcasedate between '"+StartDate+"' and '"+EndDate+"'  AND A1.RGTTYPE='1' AND A1.rgtstate in ('09','11','12') "
//                + " and A3.GIVETYPE='3' and A1.CASENO=A2.CASENO AND A1.CASENO=A3.CASENO ) as x ";
//
//        String sql4 = sqlbase0 + " AND A.GIVETYPE='4' ";
//        String sql5 = sqlbase0 + " AND A.GIVETYPE='5' ";
//
//        SSRS COUNT1 = exesql.execSQL(sql1);
//        SSRS COUNT2 = exesql.execSQL(sql2);
//        SSRS COUNT3 = exesql.execSQL(sql3);
//        SSRS COUNT4 = exesql.execSQL(sql4);
//        SSRS COUNT5 = exesql.execSQL(sql5);
//
//        String[][] temdata1 = null;
//        String[][] temdata2 = null;
//        String[][] temdata3 = null;
//        String[][] temdata4 = null;
//        String[][] temdata5 = null;
//
//        if (COUNT1 != null && COUNT1.getMaxRow() > 0)
//        {
//            temdata1 = COUNT1.getAllData();
//        }
//        if (COUNT2 != null && COUNT2.getMaxRow() > 0)
//        {
//            temdata2 = COUNT2.getAllData();
//        }
//        if (COUNT3 != null && COUNT3.getMaxRow() > 0)
//        {
//            temdata3 = COUNT3.getAllData();
//        }
//        if (COUNT4 != null && COUNT4.getMaxRow() > 0)
//        {
//            temdata4 = COUNT4.getAllData();
//        }
//        if (COUNT5 != null && COUNT5.getMaxRow() > 0)
//        {
//            temdata5 = COUNT5.getAllData();
//        }

        String TCaseNo = String.valueOf(sumcont0);
        String TRealPay = String.valueOf(sumprem0);

        String TCaseRate = "";
        if (sumcont0 == 0)
            TCaseRate = "0";
        else
        {
            TCaseRate = "100";
        }
        String TPayRate = "";
        if (sumprem0 < 0.001)
            TPayRate = "0";
        else
        {
            TPayRate = "100";
        }
        int tTCM1=0;
        double tTCM2=0.0;
        int tTCM3=0;
        double tTCM4=0.0;
        int tTCM5=0;
        double tTCM6=0.0;
        int tTCM7=0;
        double tTCM8=0.0;
        int tTCM9=0;
        double tTCM10=0.0;
        
        tTCM1+=(int)tempValue[0];
        tTCM2+=tempValue[1];
        tTCM3+=(int)tempValue[2];
        tTCM4+=tempValue[3];
            
        tTCM5+=(int)tempValue[4];
        tTCM6+=tempValue[5];
        tTCM7+=(int)tempValue[6];
        tTCM8+=tempValue[7];
        tTCM9+=(int)tempValue[8];
        tTCM10+=tempValue[9];

        String TCM1 = String.valueOf(tTCM1);
        String TMM1 = String.valueOf(tTCM2);
        String TCM2 = String.valueOf(tTCM3);
        String TMM2 = String.valueOf(tTCM4);
        String TCM3 = String.valueOf(tTCM5);
        String TMM3 = String.valueOf(tTCM6);
        String TCM4 = String.valueOf(tTCM7);
        String TMM4 = String.valueOf(tTCM8);
        String TCM5 = String.valueOf(tTCM9);
        String TMM5 = String.valueOf(tTCM10);
        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLRealPayReport.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月"
                + tSrtTool.getDay() + "日";
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(ManageCom);
        String ComName = "";
        if (tLDComDB.getInfo())
            ComName = tLDComDB.getName();
        int pos1 = StartDate.indexOf("-");
        int pos2 = StartDate.lastIndexOf("-");
        String MStartDate = StartDate.substring(0, pos1) + "年"
                + StartDate.substring(pos1 + 1, pos2) + "月"
                + StartDate.substring(pos2 + 1) + "日";
        pos1 = EndDate.indexOf("-");
        pos2 = EndDate.lastIndexOf("-");
        String MEndDate = EndDate.substring(0, pos1) + "年"
                + EndDate.substring(pos1 + 1, pos2) + "月"
                + EndDate.substring(pos2 + 1) + "日";
        //    String StaDate = Year+"年"+Month+"月";
        //PayColPrtBL模版元素
        texttag.add("SysDate", SysDate);
        texttag.add("TCaseNo", TCaseNo);
        texttag.add("TRealPay", TRealPay);
        texttag.add("TCaseRate", TCaseRate);
        texttag.add("TPayRate", TPayRate);
        texttag.add("TCM1", TCM1);
        texttag.add("TMM1", TMM1);
        texttag.add("TCM2", TCM2);
        texttag.add("TMM2", TMM2);
        texttag.add("TCM3", TCM3);
        texttag.add("TMM3", TMM3);
        texttag.add("TCM4", TCM4);
        texttag.add("TMM4", TMM4);
        texttag.add("TCM5", TCM5);
        texttag.add("TMM5", TMM5);
        texttag.add("ManageCom", ComName);
        texttag.add("StartDate", MStartDate);
        texttag.add("EndDate", MEndDate);
        texttag.add("Operate", Operate);
        //    texttag.add("StaDate",StaDate);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
        //    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println("最后合计 "+" 结束  "+PubFun.getCurrentTime());
    }

    public static void main(String[] args)
    {
        TransferData PrintElement = new TransferData();
        PrintElement.setNameAndValue("StartDate", "2006-6-1");
        PrintElement.setNameAndValue("EndDate", "2006-7-31");
        PrintElement.setNameAndValue("ManageCom", "86");
        PrintElement.setNameAndValue("AmntSpan", "1000");

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cm0001";
        VData tVData = new VData();
        tVData.addElement(PrintElement);
        tVData.addElement(tG);

        LLReportRealPayUI tLLReportRealPayUI = new LLReportRealPayUI();
        tLLReportRealPayUI.submitData(tVData, "PRINT");
    }
}
