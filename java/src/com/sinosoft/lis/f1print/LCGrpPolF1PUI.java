/*
 * <p>ClassName: LCGrpPolF1PUI </p>
 * <p>Description: LCGrpPolF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.f1print;

import java.io.FileOutputStream;
import java.io.InputStream;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LCGrpPolF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private String mszTemplatePath = null;
    private String mOperate = "";

    public LCGrpPolF1PUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            mOperate = cOperate;

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            LCGrpPolF1PBL tLCGrpPolF1PBL = new LCGrpPolF1PBL();
            System.out.println("Start LCGrpPolF1P UI Submit ...");

            if (!tLCGrpPolF1PBL.submitData(vData, cOperate))
            {
                if (tLCGrpPolF1PBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLCGrpPolF1PBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData", "tLCGrpPolF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tLCGrpPolF1PBL.getResult();
                return true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    public static void main(String[] args)
    {
        LCGrpPolF1PUI tLCGrpPolF1PUI = new LCGrpPolF1PUI();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

        tLCGrpPolSchema.setGrpPolNo("86130020040220000002");
        tLCGrpPolSet.add(tLCGrpPolSchema);

//    tLCGrpPolSchema = new LCGrpPolSchema();
//    tLCGrpPolSchema.setGrpPolNo("86110020030220000082");
//    tLCGrpPolSet.add(tLCGrpPolSchema);

        VData vData = new VData();

        vData.addElement(new GlobalInput());
        vData.addElement(tLCGrpPolSet);
        vData.addElement("E:\\work\\ui\\f1print\\template\\");

        if (!tLCGrpPolF1PUI.submitData(vData, "PRINT"))
        {
            System.out.println("fail to get print data");
        }
        else
        {
            vData = tLCGrpPolF1PUI.getResult();
            try
            {
                InputStream ins = (InputStream) vData.get(0);
                FileOutputStream fos = new FileOutputStream(
                        "E:\\work\\ui\\f1print\\template\\LCGrpPolData11.xml");
                int n = 0;

                while ((n = ins.read()) != -1)
                {
                    fos.write(n);
                }

                fos.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCGrpPolSet);
            vData.add(mszTemplatePath);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        if (mOperate.equals("PRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                    "LCGrpPolSet", 0));
            mszTemplatePath = (String) cInputData.getObjectByObjectName(
                    "String", 0);
        }

        if (mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                    "LCGrpPolSet", 0));
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpPolF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}