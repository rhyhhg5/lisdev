package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

public class GrpARStatisticBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    public GrpARStatisticBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
        if (mTransferData == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName("OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String MakeDate = PubFun.getCurrentDate();
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;
        if (QYType.equals("4"))
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C1, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C2, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F3");

                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C1, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C2, ");

                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F3");
                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        System.out.println("sql" + sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and   makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and    makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and   makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F3");
                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");

                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F3");
                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    System.out.println("sql" + sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                String Mng = tExeSQL
                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                                + Managecom + "' order by ComCode");

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F3");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C3, ");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F3");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where  edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%'  and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  ) F3");
                sql.append(" from ldcom Z where Z.comcode ='86' ");
                sql.append(" ) as X  order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                System.out.println("处理完成第二个Sql语句~~~~~");
            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F3");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F3");
                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = { "A", "B", "C", "D", "E", "F", "G" };
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++)
            {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("tbGrpARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" + tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" + OperatorManagecom + "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0)
            {
                xml.addTextTag(texttag);
            }

            //保存信息
            xml.addListTable(tListTable, title);
            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);
        }
        else if (QYType.equals("5"))
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                        sql
                                .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' )) L, ");
                        sql
                                .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') N ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  and a.passflag='a' ) G, ");
                        sql
                                .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  and a.passflag='a' ) H, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')) L, ");
                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') N ");
                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "'   and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                    sql
                            .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "'   and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "' )) L, ");
                    sql
                            .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') M, ");
                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') N ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'   and a.passflag='a' ) G, ");
                    sql
                            .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'   and a.passflag='a' ) H, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "' )) L, ");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') M, ");
                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') N ");
                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                String Mng = tExeSQL
                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                                + Managecom + "' order by ComCode");

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = { "A", "B", "C", "D", "E", "F", "G", "H" };
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++)
            {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("GrpARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" + tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" + OperatorManagecom + "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0)
            {
                xml.addTextTag(texttag);
            }

            //保存信息
            xml.addListTable(tListTable, title);
            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);

        }
        else
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C1, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C2, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno)) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G3");

                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C1, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C2, ");

                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) F3,");

                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) G1,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) G2,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) G3");

                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and   signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and    signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and   signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F3,");

                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G1,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G2,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G3");

                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");

                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F3,");

                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G1,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G2,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G3");

                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                //                String Mng = tExeSQL
                //                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                //                                + Managecom + "' order by ComCode");

                String Mng = Managecom;

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C3, ");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='86' ");
                sql.append(" ) as X  order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                System.out.println("处理完成第二个Sql语句~~~~~");
            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and   conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F3,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }
            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = { "A", "B", "C", "D", "E", "F", "G", "M" };
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++)
            {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("chbGrpARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" + tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" + OperatorManagecom + "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0)
            {
                xml.addTextTag(texttag);
            }

            //保存信息
            xml.addListTable(tListTable, title);
            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);
        }
        /*        if (Managecom.length() == 2) {
         SSRS tSSRS_M;
         String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
         System.out.println("zhuzhu" + sql_M);
         tExeSQL = new ExeSQL();
         tSSRS_M = tExeSQL.execSQL(sql_M);
         String temp_M[][] = tSSRS_M.getAllData();
         String Mng;
         if (tSSRS_M.getMaxRow() > 0) {
         for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
         Mng = temp_M[i - 1][0];
         String Mng1;
         if (Mng.equals("86000000")) {
         Mng1 = Mng;
         } else {
         Mng1 = Mng.substring(0, 4);
         }
         sql = new StringBuffer();
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Mng1 +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) A, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and a.riskcode=Z.riskcode ) C, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and a.riskcode=Z.riskcode ) L, ");
         sql.append(" (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where  riskcode=Z.riskcode) and makedate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"') H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and  substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"') E, ");
         sql.append(" (select count(1) from lbgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"') P, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"') Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode=Z.riskcode) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode=Z.riskcode) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode=Z.riskcode) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*4)='"+ Mng +"' and riskcode=Z.riskcode) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) J ");
         sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X ");
         sql.append(" union ");
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Mng1 +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" '总计' A, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)=Z.comcode  ) C, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)=Z.comcode   ) L, ");
         sql.append(" (select count(a.grpcontno)  from lcgrppol a,lcgrpcont b where  a.prtno=b.prtno and  b.makedate between '"+StartDate+"' and '"+EndDate+"'  and substr(b.managecom,1,length('"+Managecom +"')*4)=Z.comcode ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode   and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode   and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lcpol b where b.prtno=a.prtno and a.appflag='1' and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*4)=Z.comcode ) E, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lbpol b where b.prtno=a.prtno and a.appflag='1'  and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*4)=Z.comcode ) P, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lbgrpcont a,lbpol b where b.prtno=a.prtno and a.appflag='1'  and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*4)=Z.comcode ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode  ) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode  ) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode  ) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*4)=Z.comcode  ) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)=Z.comcode  and a.passflag='a' ) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*4)=Z.comcode  and a.passflag='a' ) J ");
         sql.append(" from ldcom Z where Z.comcode ='"+ Mng +"' ");
         sql.append(" ) as X  order by A with ur");

         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         sql = new StringBuffer();
         System.out.println("处理完成第一个Sql语句~~~~~");
         }
         sql = new StringBuffer();
         sql.append(" select '全国',A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) A, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"'  and a.riskcode=Z.riskcode ) C, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"'  and a.riskcode=Z.riskcode ) L, ");
         sql.append(" (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where  riskcode=Z.riskcode) and makedate between '"+StartDate+"' and '"+EndDate+"' ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"')  and riskcode =Z.riskcode and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1')  and riskcode =Z.riskcode and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' ) E, ");
         sql.append(" (select count(1) from lbgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' ) P, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1')  and riskcode=Z.riskcode) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1')  and riskcode=Z.riskcode) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1')  and riskcode=Z.riskcode) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' )  and riskcode=Z.riskcode) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"'   and a.passflag='a' and c.riskcode=Z.riskcode) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"'   and a.passflag='a' and c.riskcode=Z.riskcode) J ");
         sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X ");
         sql.append(" union ");
         sql.append(" select '全国',A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" '总计' A, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode  ) C, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode   ) L, ");
         sql.append(" (select count(a.grpcontno)  from lcgrppol a,lcgrpcont b where  a.grpcontno=b.grpcontno and  b.makedate between '"+StartDate+"' and '"+EndDate+"'  and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode   and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode   and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lcpol b where a.prtno=b.prtno and a.appflag='1' and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) E, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lbgrpcont a,lbpol b where a.prtno=b.prtno and a.appflag='1'  and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) P, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lbpol b where a.prtno=b.prtno and a.appflag='1'  and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode  and a.passflag='a' ) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode  and a.passflag='a' ) J ");
         sql.append(" from ldcom Z where Z.comcode ='86' ");
         sql.append(" ) as X  order by A with ur");
         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         System.out.println("处理完成第二个Sql语句~~~~~");
         }

         } else if (Managecom.length() == 4) {
         SSRS tSSRS_M;
         String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
         Managecom +
         "' order by ComCode");
         sql = new StringBuffer();
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Mng +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) A, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and a.riskcode=Z.riskcode ) C, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and a.riskcode=Z.riskcode ) L, ");
         sql.append(" (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where  riskcode=Z.riskcode) and makedate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and  substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') E, ");
         sql.append(" (select count(1) from lbgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') P, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) J ");
         sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X ");
         sql.append(" union ");
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Mng +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" '总计' A, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) C, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode   ) L, ");
         sql.append(" (select count(a.grpcontno)  from lcgrppol a,lcgrpcont b where  a.grpcontno=b.grpcontno and  b.makedate between '"+StartDate+"' and '"+EndDate+"'  and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode   and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode   and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lcpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) E, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) P, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lbgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' ) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' ) J ");
         sql.append(" from ldcom Z where Z.comcode ='"+ Mng +"' ");
         sql.append(" ) as X  order by A  with ur");
         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         sql = new StringBuffer();
         System.out.println("处理完成第一个Sql语句~~~~~");

         sql = new StringBuffer();
         sql = new StringBuffer();
         sql.append(" select '总计',A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) A, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and a.riskcode=Z.riskcode ) C, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and a.riskcode=Z.riskcode ) L, ");
         sql.append(" (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where  riskcode=Z.riskcode) and makedate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode =Z.riskcode and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and  substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') E, ");
         sql.append(" (select count(1) from lbgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') P, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"') Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*2)='"+ Mng +"' and riskcode=Z.riskcode) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' and c.riskcode=Z.riskcode) J ");
         sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X ");
         sql.append(" union ");
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Mng +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" '总计' A, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) C, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode   ) L, ");
         sql.append(" (select count(a.grpcontno)  from lcgrppol a,lcgrpcont b where  a.grpcontno=b.grpcontno and  b.makedate between '"+StartDate+"' and '"+EndDate+"'  and substr(b.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode   and PolTypeFlag <>'2') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode   and PolTypeFlag <>'2') D, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lcpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) E, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) P, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lbgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"')*2)=Z.comcode ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"')*2)=Z.comcode  ) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' ) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"')*2)='"+ Mng +"'  and a.passflag='a' ) J ");
         sql.append(" from ldcom Z where Z.comcode ='"+ Mng +"' ");
         sql.append(" ) as X  order by A  with ur");
         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         sql = new StringBuffer();
         System.out.println("处理完成第二个Sql语句~~~~~");
         } else if (Managecom.length() == 8) {
         sql = new StringBuffer();
         SSRS tSSRS_M;
         sql = new StringBuffer();
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Managecom +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) A, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and a.riskcode=Z.riskcode ) C, ");
         sql.append(" (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and a.riskcode=Z.riskcode ) L, ");
         sql.append(" (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where  riskcode=Z.riskcode) and makedate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"') H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode =Z.riskcode and PolTypeFlag <>'') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode =Z.riskcode and PolTypeFlag <>'') D, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and  substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"') E, ");
         sql.append(" (select count(1) from lbgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"') P, ");
         sql.append(" (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lbpol where  riskcode=Z.riskcode and conttype='2' and appflag='1') and signdate between '"+StartDate+"' and '"+EndDate+"' and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"') Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode=Z.riskcode) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode=Z.riskcode) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode=Z.riskcode) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"'))='"+ Managecom +"' and riskcode=Z.riskcode) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"'  and a.passflag='a' and c.riskcode=Z.riskcode) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"'  and a.passflag='a' and c.riskcode=Z.riskcode) J ");
         sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X ");
         sql.append(" union ");
         sql.append(" select (select showname from ldcom where sign='1' and comcode= '"+ Managecom +"'),A, ");
         sql.append(" char(H),char(L),char(G),case when varchar(M) is null then '0' else varchar(M) end,char(R),case when varchar(J) is null then '0' else varchar(J) end,char(E),char(C),char(D), ");
         sql.append(" case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end, ");
         sql.append(" varchar(to_zero(E)+to_zero(P)+to_zero(Q)),varchar(to_zero(F)+to_zero(T)+to_zero(S))");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append(" select ");
         sql.append(" '总计' A, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode  ) C, ");
         sql.append(" (select count(distinct (CustomerNo||riskcode)) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode   ) L, ");
         sql.append(" (select count(a.grpcontno)  from lcgrppol a,lcgrpcont b where  a.grpcontno=b.grpcontno and  b.makedate between '"+StartDate+"' and '"+EndDate+"'  and substr(b.managecom,1,length('"+Managecom +"'))=Z.comcode ) H, ");
         sql.append(" (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode   and PolTypeFlag <>'') G, ");
         sql.append(" (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"' and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode   and PolTypeFlag <>'') D, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lcpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) E, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lcgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) P, ");
         sql.append(" (select count(distinct (b.prtno||b.riskcode)) from lbgrpcont a,lbpol b where a.appflag='1' and a.prtno=b.prtno and  a.signdate between '"+StartDate+"' and '"+EndDate+"'  and substr(a.managecom,1,length('"+Managecom +"'))=Z.comcode ) Q, ");
         sql.append(" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) F, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) T, ");
         sql.append(" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  signdate between '"+StartDate+"' and '"+EndDate+"'  and appflag='1') and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) S, ");
         sql.append(" (select sum(prem) from lcpol where conttype='' and grpcontno in (select grpcontno from lcgrpcont where  makedate between '"+StartDate+"' and '"+EndDate+"' ) and substr(managecom,1,length('"+Managecom +"'))=Z.comcode  ) M ,");
         sql.append(" (select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"'  and a.passflag='a' ) R,");
         sql.append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+StartDate+"' and '"+EndDate+"' and substr(b.managecom,1,length('"+Managecom +"'))='"+ Managecom +"'  and a.passflag='a' ) J ");
         sql.append(" from ldcom Z where Z.comcode ='"+ Managecom +"' ");
         sql.append(" ) as X  order by A  with ur");


         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         sql = new StringBuffer();
         System.out.println("处理完成第一个Sql语句~~~~~");
         sql = new StringBuffer();
         /*            sql.append(
         " select '总计',A,");
         sql.append(" char(H),char(L),char(G),char(E),char(C),char(D),");
         sql.append(
         " case when varchar(F) is null then '0' else varchar(F) end, ");
         sql.append(" case when char(E)<>'0' then varchar(F/E) when varchar(F/E) is null then '0' else varchar(F/E)  end ");
         sql.append(" from ");
         sql.append(" ( ");
         sql.append("  select ");
         sql.append("  Z.riskcode A, ");
         sql.append("  (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where b.appflag='1' and a.prtno=b.prtno and b.signdate between '" +
         StartDate + "' and '" + EndDate +
         "' and substr(b.managecom,1,length('" +
         Managecom + "'))='" + Managecom +
         "' and a.riskcode=Z.riskcode ) C, ");
         sql.append("  (select count(1) from lcgrpcont where  grpcontno in (select grpcontno from lcgrppol where makedate between '" +
         StartDate + "' and '" + EndDate +
         "' and riskcode=Z.riskcode) and substr(managecom,1,length('" +
         Managecom + "'))='" + Managecom + "') H, ");
         sql.append("  (select count(distinct CustomerNo) from lcgrppol a,lcgrpcont b where a.prtno=b.prtno and b.makedate between '" +
         StartDate + "' and '" + EndDate +
         "' and substr(b.managecom,1,length('" +
         Managecom + "'))='" + Managecom +
         "' and a.riskcode=Z.riskcode ) L, ");
         sql.append("  (select count(1) from lcpol where  grpcontno in (select grpcontno from lcgrpcont where  makedate between '" +
         StartDate + "' and '" + EndDate +
         "') and substr(managecom,1,length('" +
         Managecom + "'))='" + Managecom +
         "' and riskcode =Z.riskcode and PolTypeFlag <>'2') G, ");

         sql.append("  (select count(1) from lcpol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '" +
         StartDate + "' and '" + EndDate +
         "' and appflag='1') and substr(managecom,1,length('" +
         Managecom + "'))='" + Managecom +
         "' and riskcode =Z.riskcode and PolTypeFlag <>'2') D, ");
         sql.append("  (select count(1) from lcgrpcont where appflag='1' and grpcontno in (select grpcontno from lcgrppol where signdate between '" +
         StartDate + "' and '" + EndDate +
         "' and riskcode=Z.riskcode) and substr(managecom,1,length('" +
         Managecom + "'))='" + Managecom + "') E, ");
         sql.append("  (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  signdate between '" +
         StartDate + "' and '" + EndDate +
         "'  and appflag='1') and substr(managecom,1,length('" +
         Managecom + "'))='" + Managecom +
         "' and riskcode=Z.riskcode) F ");
         sql.append("  from lmriskapp Z where Z.RiskProp='G' ");
         sql.append(" ) as X order by A");

         ssrs = tExeSQL.execSQL(sql.toString());
         if (ssrs != null) {
         if (ssrs.getMaxRow() > 0) {
         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
         result.add(ssrs.getRowData(m));
         }
         }
         }
         ssrs = null;
         sql = new StringBuffer();
         System.out.println("处理完成第二个Sql语句~~~~~");

         }

         ListTable tListTable = new ListTable();
         tListTable.setName("WorkEfficencyReport");
         String[] title = {"A", "B", "C", "D", "E", "F", "G", "L", "M","H","N","O","P","Q","R"};
         /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
         * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
         *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
         *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
         * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
         * 就是说 A - J 必须是一维的（一个数据）的数据
         * 不能出现A1--An是一个数据 B1--Bn是一个数组，
         * 这样会无法显示。
         *  */
        /*        for (int i = 0; i < result.size(); i++) {
         tListTable.add((String[]) result.get(i));
         }

         XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
         xml.createDocument("GrpARStatistic.vts", "printer"); //最好紧接着就初始化xml文档
         //生成-年-月-日格式的日期
         StrTool tSrtTool = new StrTool();
         String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
         "月" +
         tSrtTool.getDay() + "日";

         //WorkEfficiencyBL模版元素
         StartDate = StartDate.replace('-', '月');
         StartDate = StartDate.replaceFirst("月", "年") + "日";
         EndDate = EndDate.replace('-', '月');
         EndDate = EndDate.replaceFirst("月", "年") + "日";
         MakeDate = MakeDate.replace('-', '月');
         MakeDate = MakeDate.replaceFirst("月", "年") + "日";
         texttag.add("StartDate", StartDate);
         texttag.add("EndDate", EndDate);
         texttag.add("QYType_ch", QYType_ch);
         texttag.add("MakeDate", MakeDate);
         SSRS tSSRS_M = new SSRS();
         String sql_M = "select Name from ldcom where comcode = '" +
         OperatorManagecom +
         "'";
         System.out.println("zhuzhu" + sql_M);
         tExeSQL = new ExeSQL();
         tSSRS_M = tExeSQL.execSQL(sql_M);
         String temp_M[][] = tSSRS_M.getAllData();
         texttag.add("MngCom", temp_M[0][0]);

         if (texttag.size() > 0) {
         xml.addTextTag(texttag);
         }

         //保存信息
         xml.addListTable(tListTable, title);
         /** 添加完成将输出的结果保存到结果集中 */
        /*        mResult.add(xml);
         mResult.clear();
         mResult.addElement(xml);
         */
        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GrpARStatisticBL tWorkEfficiencyBL = new GrpARStatisticBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("OperatorManagecom", "86");
        mTransferData.setNameAndValue("Managecom", "86");
        mTransferData.setNameAndValue("StartDate", "2005-1-1");
        mTransferData.setNameAndValue("EndDate", "2005-11-1");
        mTransferData.setNameAndValue("QYType", "1");
        tVData.add(mTransferData);
        tWorkEfficiencyBL.submitData(tVData, "PRINT");
        VData vData = tWorkEfficiencyBL.getResult();

    }
}
