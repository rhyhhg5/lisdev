package com.sinosoft.lis.f1print;

/**
 * <p>Title: FenClaimDayBalanceExcelBL</p>
 * <p>Description:保监会大病简表 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author :  y
 * @date:2013-11-11
 * @version 1.0
 */
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class TContPreExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; // 获取时间
	//取得文件路径
	private String tOutXmlPath="";
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

	public TContPreExcelBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		TContPreExcelBL tC = new TContPreExcelBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		System.out.println("start TContPreExcelBL...TContPre");
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath=(String)cInputData.get(2);
		
		System.out.println("打印日期："+mDay[0]+"至"+mDay[1]);
		
		if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TContPreExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		String nSql = "SELECT DISTINCT CODE FROM FICODETRANS WHERE CODETYPE='ContNo'";
		tSSRS = tExeSQL.execSQL(nSql);
		if(tSSRS.getMaxRow() == 0){
			buildError("ContPreExcelBL", "没有符合打印条件的保单数据");
		}
		String ContNo = "";
		for(int c = 1;c <= tSSRS.getMaxRow(); c++){
			if(c == 1){
				ContNo = "'"+tSSRS.GetText(c, 1)+"'";
			}else{
				ContNo = ContNo+",'"+tSSRS.GetText(c, 1)+"'";
			}
		}
		System.out.println("大病保单为："+ContNo);
		
		String[][] mToExcel = new String[10000][12];
		mToExcel[0][0] = "保险公司大病保险统计简表";
		mToExcel[1][0] = "报送单位：";
		mToExcel[1][1] = "_________";
		mToExcel[1][2] = "保险公司";
		mToExcel[2][0] = "报表时段：";
		mToExcel[2][1] = "_________";
		mToExcel[2][2] = "    年";
		mToExcel[2][3] = "_________";
		mToExcel[2][4] = "季";
		mToExcel[4][1] = "项目数(个)";
		mToExcel[4][2] = "省级统筹";
		mToExcel[4][3] = "地市级统筹";
		mToExcel[4][4] = "县区级统筹";
		mToExcel[4][5] = "期末有效承保人数(万人)";
		mToExcel[4][6] = "原保险保费收入(万元)";
		mToExcel[4][7] = "应收保费(万元)";
		mToExcel[4][8] = "赔付支出(万元)";
		mToExcel[4][9] = "赔付人次(万人)";
		mToExcel[4][10] = "赔付人数(万人)";
		mToExcel[4][11] = "备注";
		mToExcel[5][0] = "纵栏序号";
		mToExcel[5][1] = "1＝2+3+4";
		mToExcel[5][2] = "     2";
		mToExcel[5][3] = "     3";
		mToExcel[5][4] = "     4";
		mToExcel[5][5] = "     5";
		mToExcel[5][6] = "     6";
		mToExcel[5][7] = "     7";
		mToExcel[5][8] = "     8";
		mToExcel[5][9] = "     9";
		mToExcel[5][10] = "   10";
		mToExcel[5][11] = "   11";
		
		int no=6;  //初始打印行数
		
		String sql = "select manage,"
			+"NVL(sum(case accountcode when '6031000000' then summon else 0 end),0),"
			+"NVL(sum(case accountcode when '1122000000' then summon else 0 end),0),"
			+"NVL(sum(case accountcode when '6031000000' then 0 when '1122000000' then 0 else -summon end),0) from("
			+"select (select codename from ficodetrans where codetype='FXManageCom' and code=a.managecom fetch first row only) as manage,"
			+"accountcode as accountcode,"
			+"sum(case finitemtype when 'C' then summoney else -summoney end) as summon "
			+"from fivoucherdatadetail a where 1=1 "
			+"and a.accountdate between '"+mDay[0]+"' and '"+mDay[1]+"' " 
			+"and a.contno in ("+ContNo+") "
			+"and (a.accountcode ='6031000000' or a.accountcode = '1122000000' or a.accountcode like '651101%') "
			+"and a.riskcode in('161201','161401','161101') "
			+"group by managecom,accountcode order by managecom )as aa group by manage";
		tSSRS = tExeSQL.execSQL(sql);
		int maxrow=tSSRS.getMaxRow();
		String managecom = "";
		for (int row = 1; row <= maxrow; row++)
        {
            for (int col = 1; col <=4; col++)
            { 
            	if (col==1) {
            		managecom = tSSRS.GetText(row, col); ;
            		mToExcel[no+row-1][0] = managecom.substring(4, managecom.length()-3); 
            		
            	}
            	if (col==2){
            		String bf = tSSRS.GetText(row, col);
            		if(bf==null){
            			mToExcel[no+row-1][6]="0.00";
            		}else{
            			mToExcel[no+row-1][6]= divide(bf, "10000", 2);
            		}
				}
            	if (col==3){
            		String ys = tSSRS.GetText(row, col);
            		if(ys==null){
            			mToExcel[no+row-1][7]="0.00";
            		}else{
            			mToExcel[no+row-1][7]=divide(ys, "10000", 2);
            		}
            	}
            	if (col==4){
            		String lp = tSSRS.GetText(row, col);
            		if(lp==null){
            			mToExcel[no+row-1][8]="0.00";
            		}else{
            			mToExcel[no+row-1][8]=divide(lp, "10000", 2);
            		}
            	}
            }
//            System.out.println(""+managecom.substring(4, managecom.length()-3));
        }
		try
        {
            System.out.println("TContPre--tOutXmlPath:"+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成TContPre文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }
	return true;
}

	public double divide(double v1,double v2,int scale, int round_mode){
		if(scale < 0){
		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.divide(b2, scale, round_mode).doubleValue();
	}


	public String divide(String s1,String s2, int scale){
		String temp = "";
		if(s1.equals("")|| s1=="null"){
			temp = "0.00";
		}else{
			double v1=Double.parseDouble(s1);
			double v2=Double.parseDouble(s2);
			double v= divide(v1, v2, scale, BigDecimal.ROUND_HALF_EVEN);
			temp=String.valueOf(v);
			String c=temp.replace(".", ",");
			String[] arr=c.split(",");
			if("0".equals(arr[1])){
				temp+="0";
			}
			if(arr[1].length()==1 && !arr[1].equals("0")){
				temp+="0";
			}
		}
		return temp;
	}
}
