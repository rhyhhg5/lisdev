package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLCustomerMessageBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;


public class LLCustomerMessageBL {
    public LLCustomerMessageBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private String mManageCom = "";
    private String mManageComName = "";
    private String moperator = "";
    private String mRgtState = "";
    private String mRgtDateS = "";
    private String mRgtDateE = "";
    private XmlExport mXmlExport = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }else {
        	String fileNameB = moperator + "_" + FileQueue.getFileName()+".vts";
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("tFileNameB", fileNameB);
            tTransferData.setNameAndValue("tMakeDate", currentDate);
            tTransferData.setNameAndValue("tOperator", moperator);
            LLPrintSave tLLPrintSave = new LLPrintSave();
            VData tVData = new VData();
            tVData.addElement(tTransferData);
            if (!tLLPrintSave.submitData(tVData, "")) {
                return false;
            }
        }
        return true;
    }


    private boolean getDataList() {

        if (mRgtState.equals("") || mRgtState == "") {
            CError.buildErr(this, "未获得要打印的数据！");
            return false;
        }
        if (mManageCom.equals("") || mManageCom == "") {
            CError.buildErr(this, "选择要打印的公司！");
            return false;
        }

//        if (mRgtState.equals("1")) {
//            getCaseAccept();
//        }
        if (mRgtState.equals("1")) {
            getCaseDeal();
        }
        if (mRgtState.equals("2")) {
            getCaseEnd();
        }
        if (mRgtState.equals("3")) {
            getGrpCaseEnd();
        }
        return true;
    }

//    private boolean getCaseAccept() {
//        String sql =
//                "SELECT DISTINCT r.mngcom,(SELECT name FROM ldcom WHERE comcode=r.mngcom),c.caseno,r.RgtantMobile,r.RgtantName FROM llcase c,llcaseoptime o,llregister r "
//                + "WHERE c.rgttype='1' AND c.caseno=o.caseno AND o.startdate=c.rgtdate AND c.rgtdate BETWEEN '" +
//                mRgtDateS + "' AND '" + mRgtDateE + "' AND o.rgtstate='01'"
//                + " AND EXISTS(SELECT 1 FROM lcpol p,lmriskapp ap WHERE p.riskcode=ap.riskcode AND ap.riskprop='I' AND c.customerno=p.insuredno AND p.conttype='1')"
//                + " AND c.mngcom LIKE '" + mManageCom + "%' AND c.caseno=r.rgtno AND r.rgtobjno=r.customerno AND r.rgtantmobile IS NOT NULL with ur";
//        ExeSQL tExeSQL = new ExeSQL();
//        SSRS tSSRS = new SSRS();
//        tSSRS = tExeSQL.execSQL(sql);
//        System.out.println(sql);
//        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            String Info[] = new String[5];
//            Info[0] = tSSRS.GetText(i, 1);
//            Info[1] = tSSRS.GetText(i, 2);
//            Info[2] = tSSRS.GetText(i, 3);
//            Info[3] = tSSRS.GetText(i, 4);
//            Info[4] = "理赔温馨提示：尊敬的" + tSSRS.GetText(i, 5) +
//                      "女士/先生，您的理赔申请已受理，请等候理赔结案通知。";
//            mListTable.add(Info);
//        }
//        return true;
//    }

    private boolean getCaseDeal() {
        String sql =
                "SELECT r.mngcom,(SELECT name FROM ldcom WHERE comcode=r.mngcom),c.caseno,r.RgtantMobile,r.RgtantName FROM llcase c,llregister r "
                + "WHERE c.caseno=r.rgtno AND r.rgtobjno=r.customerno AND c.rgtstate NOT IN ('07','11','12','13','14')"
                + " AND c.rgttype='1' AND r.rgtantmobile IS NOT NULL "
                + " AND EXISTS(SELECT 1 FROM lcpol p,lmriskapp ap WHERE p.riskcode=ap.riskcode AND ap.riskprop='I' AND c.customerno=p.insuredno AND p.conttype='1') "
                + " AND c.mngcom LIKE '" + mManageCom +
                "%' AND days('" + mRgtDateE + "')-days(c.rgtdate)=13 with ur";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String Info[] = new String[5];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            Info[4] = tSSRS.GetText(i, 5) +
                      "女士/先生，您的理赔申请正在处理中，处理完毕后将及时通知您。【人保健康】";
            mListTable.add(Info);
        }

        return true;
    }

    private boolean getCaseEnd() {
        String sql = "SELECT r.mngcom,(SELECT name FROM ldcom WHERE comcode=r.mngcom),c.caseno,r.RgtantMobile,r.RgtantName,g.paymode,g.drawer,g.bankaccno,g.sumgetmoney FROM llcase c,llregister r,ljaget g,llclaim cl "
                     + "WHERE c.caseno=r.rgtno AND r.rgtobjno=r.customerno AND c.caseno=cl.caseno AND c.rgtstate IN ('11','12') AND c.caseno=g.otherno AND r.rgtantmobile IS NOT NULL"
                     + " AND EXISTS(SELECT 1 FROM lcpol p,llclaimdetail clt,lmriskapp ap WHERE clt.polno=p.polno AND clt.caseno=c.caseno " 
                     + " AND p.conttype='1' " 
                     + " AND ap.riskcode=p.riskcode AND ap.riskprop='I' ) "
                     +" AND c.rgttype='1' AND c.caseno=cl.caseno AND cl.givetype in ('1','2') AND g.paymode IN ('1','3','4','11') AND c.mngcom LIKE '" +
                     mManageCom + "%' AND g.makedate BETWEEN '" + mRgtDateS +
                     "' AND '" + mRgtDateE + "'  " 
                  	 + " union all "
                  	 +"SELECT r.mngcom," +
                  	 		"(SELECT name FROM ldcom WHERE comcode=r.mngcom)," +
                  	 		"c.caseno," +
                  	 		"r.RgtantMobile," +
                  	 		"r.RgtantName," +
                  	 		"g.paymode," +
                  	 		"g.drawer," +
                  	 		"g.bankaccno," +
                  	 		"g.sumgetmoney " 
                  	 +"FROM llcase c,llregister r,ljaget g,llclaim cl "
                  	+ " WHERE c.rgtno = r.rgtno "
                	+ " AND c.caseno = cl.caseno "
                	+ " AND  ((g.paymode='1' and c.rgtstate in('11','12') and g.makedate BETWEEN '" + mRgtDateS +  "' AND '" + mRgtDateE + "'  ) or(g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate BETWEEN '" + mRgtDateS + "' AND '" + mRgtDateE + "'  )) "
                	+ " AND c.caseno = g.otherno "
                	+ " AND r.RgtantMobile IS NOT NULL "
                	+ " AND EXISTS "
                	+ "  (SELECT 1 "
                	+ " FROM llclaimdetail clt, lcgrpcont cp "
                	+ " WHERE clt.grpcontno = cp.grpcontno and clt.caseno=c.caseno "
                	+ " AND cp.cardflag in ('2','3')) "
                	+ " AND NOT EXISTS (SELECT 1 "
                	+ " FROM llclaimdetail clt, lmriskapp ap "
                	+ " WHERE clt.caseno = c.caseno "
                	+ " AND ap.riskcode = clt.riskcode "
                	+ " AND ap.riskprop = 'I' "
                	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
                	+ " AND c.rgttype = '1' "
                	+ " AND cl.givetype in ('1', '2') "
                	+ " and g.paymode in ('1','3', '4', '11')  "
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "     	
                     +"with ur";

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String Info[] = new String[5];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            String tPayMode = tSSRS.GetText(i, 6);
            String tBankAccNo = tSSRS.GetText(i, 8);
            String tPrem = tSSRS.GetText(i, 9);
            String tSexContent = "女士/先生";
            
            if("1".equals(tPayMode)){
            	Info[4] = tSSRS.GetText(i, 5) + tSexContent +
            		",您的理赔申请已处理完毕，请及时受领保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tPrem)),"0.00")+"元。【人保健康】";
            }else if("4".equals(tPayMode) || "11".equals(tPayMode)
         		   || "3".equals(tPayMode)){
            	
            	if (tBankAccNo == null || "".equals(tBankAccNo) ||
                        "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                        continue;
                    }
            	
            	Info[4] = tSSRS.GetText(i, 5) + tSexContent +
                ",您的理赔申请已处理完毕，请查收尾号为" +
                tSSRS.GetText(i, 8).
                substring(tSSRS.GetText(i, 8).length() - 4,
                          tSSRS.GetText(i, 8).length()) +
                "的账户收到的保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tPrem)),"0.00")+"元。【人保健康】";
            }
            mListTable.add(Info);
        }
        //dealHLMSG();
        
        return true;
    }

    /**
     * TODO:#1617 分红险理赔案件处理调整,给付通知后短信通知
     *
     */
    private void dealHLMSG() {
    	//案件给付确认当天下午    	
    	String tSQL =  "";
    	
    	tSQL = "SELECT a.mngcom, " +//案件受理机构
    			"d.paymode, " +//付费方式
    			"a.caseno, " +//案件号
    			"(select mobile from lcappnt lc, lcaddress lca where lc.appntno = lca.customerno and lc.addressno = lca.addressno and lc.appntno = c.appntno and lc.contno = c.contno union select mobile from lbappnt lc, lcaddress lca where lc.appntno = lca.customerno and lc.addressno = lca.addressno and lc.appntno = c.appntno and lc.contno = c.contno), " +//投保人手机
    			"c.appntname, " +//投保人姓名
    			"d.bankaccno, " +//投保人银行账号
    			"(select appntsex from lcappnt where appntno = c.appntno and contno = c.contno union select appntsex from lbappnt where appntno = c.appntno and contno = c.contno), " +//投保人性别
    			"sum(b.pay), " +//红利金额
    			"d.makedate, " +//理赔合同终止日期
    			"c.contno, " +//保单号
    			"(SELECT name FROM ldcom WHERE comcode=a.mngcom) " +
    			"from llcase a, ljagetclaim b, lccont c, ljaget d " +
    			"where 1 = 1 and a.caseno=b.otherno " + 
    			"and b.contno=c.contno " + 
    			"and b.actugetno=d.actugetno and a.caseno = d.otherno " +
    			"and ((d.paymode = '1' and a.rgtstate in ('11', '12') and d.makedate BETWEEN '" + mRgtDateS +  "' AND '" + mRgtDateE + "' ) or (d.paymode = '4' and a.rgtstate = '12' and d.confdate BETWEEN '" + mRgtDateS + "' AND '" + mRgtDateE + "')) " +
    			//为个险，且险种为分红险 全额拒付、豁免保费等不发送
    			"and exists (select 1 from llclaimdetail clt, lmriskapp ap where clt.caseno = a.caseno and ap.riskcode = clt.riskcode and ap.risktype4 = '2' and clt.givetype not in ('3', '6', '7')) " +
    			"and a.rgttype = '1' " +//申请类
    			"and d.paymode in ('1', '4') " +//现金、银行转账
    			"and d.othernotype = 'H' " +
    			"and d.sumgetmoney > 0 " +
    			"and a.mngcom LIKE '" +mManageCom+ "%' " +
    			"and not exists (select 1 from ldcode1 where codetype = 'LPMsg' and code = Substr(a.mngcom, 1, 6)) " +
    			"group by a.mngcom,d.paymode,a.caseno,c.contno,c.appntno,c.appntname,d.bankaccno,d.makedate " +
    			"union " +
    			"SELECT a.mngcom, " +//案件受理机构
    			"d.paymode, " +//付费方式
    			"a.caseno, " +//案件号
    			"(select mobile from lcappnt lc, lcaddress lca where lc.appntno = lca.customerno and lc.addressno = lca.addressno and lc.appntno = c.appntno and lc.contno = c.contno union select mobile from lbappnt lc, lcaddress lca where lc.appntno = lca.customerno and lc.addressno = lca.addressno and lc.appntno = c.appntno and lc.contno = c.contno), " +//投保人手机
    			"c.appntname, " +//投保人姓名
    			"d.bankaccno, " +//投保人银行账号
    			"(select appntsex from lcappnt where appntno = c.appntno and contno = c.contno union select appntsex from lbappnt where appntno = c.appntno and contno = c.contno), " +//投保人性别
    			"sum(b.pay), " +//红利金额
    			"d.makedate, " +//理赔合同终止日期
    			"c.contno, " +//保单号
    			"(SELECT name FROM ldcom WHERE comcode=a.mngcom) " +
    			"from llcase a, ljagetclaim b, lbcont c, ljaget d " +
    			"where 1 = 1 and a.caseno=b.otherno " + 
    			"and b.contno=c.contno " + 
    			"and b.actugetno=d.actugetno and a.caseno = d.otherno " +
    			"and ((d.paymode = '1' and a.rgtstate in ('11', '12') and d.makedate BETWEEN '" + mRgtDateS +  "' AND '" + mRgtDateE + "' ) or (d.paymode = '4' and a.rgtstate = '12' and d.confdate BETWEEN '" + mRgtDateS + "' AND '" + mRgtDateE + "')) " +
    			//为个险，且险种为分红险 全额拒付、豁免保费等不发送
    			"and exists (select 1 from llclaimdetail clt, lmriskapp ap where clt.caseno = a.caseno and ap.riskcode = clt.riskcode and ap.risktype4 = '2' and clt.givetype not in ('3', '6', '7')) " +
    			"and a.rgttype = '1' " +//申请类
    			"and d.paymode in ('1', '4') " +//现金、银行转账
    			"and d.othernotype = 'H' " +
    			"and d.sumgetmoney > 0 " +
    			"and a.mngcom LIKE '" +mManageCom+ "%' " +
    			"and not exists (select 1 from ldcode1 where codetype = 'LPMsg' and code = Substr(a.mngcom, 1, 6)) " +
    			"group by a.mngcom,d.paymode,a.caseno,c.contno,c.appntno,c.appntname,d.bankaccno,d.makedate " +
    			"with ur";
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
        	String Info[] = new String[5];
        	
            String tManageCom = tMsgSSRS.GetText(i, 1);
            String tPayMode = tMsgSSRS.GetText(i, 2);
            String tCaseNo = tMsgSSRS.GetText(i, 3);
            String tMobile = tMsgSSRS.GetText(i, 4);
            //tMobile ="13141205229";
            String tCustomerName = tMsgSSRS.GetText(i, 5);
            String tBankAccNo = tMsgSSRS.GetText(i, 6);
            String tSex = tMsgSSRS.GetText(i, 7);
            String tPay = tMsgSSRS.GetText(i, 8);
            tPay = CommonBL.bigDoubleToCommonString((Double.parseDouble(tPay)),"0.00");
            String tMakedate = tMsgSSRS.GetText(i, 9);
            String tContNo = tMsgSSRS.GetText(i, 10);
            String tManageComName = tMsgSSRS.GetText(i, 11);
            
            Info[0] = tManageCom;
            Info[1] = tManageComName;
            Info[2] = tCaseNo;
            Info[3] = tMobile;

            if (tManageCom == null || "".equals(tManageCom) ||
                "null".equals(tManageCom)) {
                continue;
            }
            if (tPayMode == null || "".equals(tPayMode) ||
                "null".equals(tPayMode)) {
                continue;
            }
            if (tMobile == null || "".equals(tMobile) ||
                "null".equals(tMobile)) {
                continue;
            }
            if (tCustomerName == null || "".equals(tCustomerName) ||
                "null".equals(tCustomerName)) {
                continue;
            }
            if (tMakedate == null || "".equals(tMakedate) ||
                "null".equals(tMakedate)) {
                continue;
            }           
            if (tPay == null || "".equals(tPay) ||
                    "null".equals(tPay)) {
                    continue;
            }
            
            String tMSG = "";
            //用户性别的判断
            String tSexContent = "女士/先生";
            if ("0".equals(tSex)) {
            	tSexContent = "先生";
            } else if ("1".equals(tSex)) {
            	tSexContent = "女士";
            }
 
            //判断短信发送类型
            if ("1".equals(tPayMode)) {
            	//红利现金专属短信模板
            	tMSG = "尊敬的"+tCustomerName+tSexContent+"，您好！您的保单号为"+tContNo+"的保单已于"+tMakedate+"因理赔完毕效力终止，请您到公司柜面办理红利领取手续。祝您健康。客服电话：95591。";
            } else if ("4".equals(tPayMode)) {
            	//红利专属短信模板，领取方式为银行转账
                if (tBankAccNo == null || "".equals(tBankAccNo) ||
                    "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                    continue;
                }
                String aNeedBankAccNo = "";
                //银行账户前4位
                aNeedBankAccNo = tBankAccNo.substring(0,
                        4);
                aNeedBankAccNo += "***";
                //银行账户后4位
                aNeedBankAccNo += tBankAccNo.substring(tBankAccNo.length() - 4,
                        tBankAccNo.length());
                
                tMSG = "尊敬的"+tCustomerName+tSexContent+"，您好！您的保单号为"+tContNo+"的保单已于"+tMakedate+"因理赔完毕效力终止，该保单的累积红利"+tPay+"已经自动转至您投保人银行账户，银行账户"+aNeedBankAccNo+"。请注意查收。祝您健康。客服电话：95591";

            }
            System.out.println("tMSG:"+tMSG);
            Info[4] = tMSG;
            mListTable.add(Info);
            
            System.out.println("发送短信：" + tPayMode + mManageCom + tCaseNo +
                    tMobile + tCustomerName);
        }   

	}

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean getGrpCaseEnd() {
        String sql = "SELECT r.mngcom,"
                     + " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                     + " c.caseno,c.mobilephone,c.customername,g.bankaccno,c.customersex, "
                 	 +" g.paymode, "
                 	+" g.sumgetmoney "
                     + " FROM llcase c,llregister r,ljaget g,llclaim cl "
                     + " WHERE c.rgtno = r.rgtno "
                 	+ " AND c.caseno = cl.caseno "
                 	+ " AND c.rgtstate = '12' "//结案
                 	+ " AND c.caseno = g.otherno "
                 	+ " AND c.mobilephone IS NOT NULL "//导入被保人手机
                 	+ " AND EXISTS "
                 	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap "
                 	//#2259 （需求编号：2014-218）团体保单理赔案件短信发送功能调整
                 	//+ ", LLAppClaimReason cr "//团体
                 	+ " WHERE clt.caseno = c.caseno "
                 	//+ " and cr.rgtno = clt.grpcontno "
                 	//+ " and cr.reasoncode = '97' "//在页面配置过的团单，不含卡折
                 	//+ " and cr.reasontype = '9' "
                 	+ " AND ap.riskcode = clt.riskcode "
                 	+ " AND ap.riskprop = 'G' "
                 	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "//不发送卡折
                 	+ " AND r.mngcom not like '8644%' and r.mngcom not like '8632%' "
                 	+ " AND NOT EXISTS (SELECT 1 "//不包含个险
                 	+ " FROM llclaimdetail clt, lmriskapp ap "
                 	+ " WHERE clt.caseno = c.caseno "
                 	+ " AND ap.riskcode = clt.riskcode "
                 	+ " AND ap.riskprop = 'I') "
                 	+ " AND c.rgttype = '1' "//申请类
                 	+ " AND cl.givetype in ('1', '2') "
                 	+ " and g.paymode in ('3', '4', '11')  "
                 	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
                     + "AND c.mngcom LIKE '" +mManageCom+ "%' AND g.makedate BETWEEN '"
                     + mRgtDateS + "' AND '" + mRgtDateE + "'  " 
                 	+ " union all "
                	+ " SELECT r.mngcom, "
                	+ " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                	+ " c.caseno, "
                	+ " ins.grpinsuredphone, "
                	+ " ins.name,  "
                	+ " g.bankaccno, "
                	+ " c.customersex, "
                	+" g.paymode, "
                	+" g.sumgetmoney "
                	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
                	+" WHERE c.rgtno = r.rgtno "
                	+" AND c.caseno = cl.caseno "
                	+" AND c.rgtstate = '12' "
                	+" AND c.caseno = g.otherno "
                	+" AND c.mobilephone IS NULL "
                	+ " AND EXISTS "//配置过的团体，不含卡折
                	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap "
                	//#2259 （需求编号：2014-218）团体保单理赔案件短信发送功能调整
                	//+ ", LLAppClaimReason cr "
                	+ " WHERE clt.caseno = c.caseno "
                	//+ " and cr.rgtno = clt.grpcontno "
                	//+ " and cr.reasoncode = '97' "
                	//+ " and cr.reasontype = '9' "
                	+ " AND ap.riskcode = clt.riskcode "
                	+ " AND ap.riskprop = 'G' "
                	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "
                	+ " AND r.mngcom not like '8644%' and r.mngcom not like '8632%' "
                	+" and c.customerno = ins.insuredno "
                	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
                	+" and ins.grpinsuredphone is not null "
                	+" and g.sumgetmoney>0 "
                	+" AND NOT EXISTS (SELECT 1 " //不含个单
                	+" FROM llclaimdetail clt, lmriskapp ap " 
                	+" WHERE clt.caseno = c.caseno "
                	+" AND ap.riskcode = clt.riskcode " 
                	+" AND ap.riskprop = 'I') "
                	+" AND c.rgttype = '1' " 
                	+" AND cl.givetype in ('1', '2') " 
                	+" and g.paymode in ('3', '4', '11') " 
                	+" and r.togetherflag='1' "  //个人给付
                	+" and r.applyertype in ('2','5') "
                	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+" AND g.confdate BETWEEN '"+ mRgtDateS+ "' AND '" + mRgtDateE + "' " 
                	+ " union all "
//                	2014-01-13添加，#1631 江苏分公司理赔短信发送功能
                	+ " SELECT r.mngcom, "
                	+ " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                	+ " c.caseno, "
                	+ " c.MobilePhone, "
                	+ " c.CustomerName,  "
                	+ " g.bankaccno, "
                	+ " '', "
                	 +" g.paymode, "
                 	+" g.sumgetmoney "
                	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
                	+ " WHERE c.rgtno = r.rgtno "
                	+ " AND c.caseno = cl.caseno "
                	+ " AND (g.paymode='1' and c.rgtstate in('11','12') AND g.makedate BETWEEN '"+ mRgtDateS+ "' AND '" + mRgtDateE + "')  "
                	+ " AND c.caseno = g.otherno "
                	+ " AND c.MobilePhone IS NOT NULL "
                	+" AND ( r.mngcom like '8632%') "
                	+ " AND NOT EXISTS (SELECT 1 "
                	+ " FROM llclaimdetail clt, lmriskapp ap "
                	+ " WHERE clt.caseno = c.caseno "
                	+ " AND ap.riskcode = clt.riskcode "
                	+ " AND ap.riskprop = 'I' "
                	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
                	+ " AND c.rgttype = '1' "
                	+ " AND cl.givetype in ('1', '2') "
                	+ " and g.paymode in ('1') "
                	+" and r.togetherflag='1' "  //个人给付
                	+ " and g.sumgetmoney > 0 "    	
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+ " union all "
                	//2014-01-13添加，导入时未导入手机号
                	+ " SELECT r.mngcom, "
                	+ " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                	+" c.caseno, "
                	+" ins.grpinsuredphone, "
                	+" c.CustomerName, "
                	+" g.bankaccno, "
                	+" '', "
                	 +" g.paymode, "
                 	+" g.sumgetmoney "
                	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
                	+" WHERE c.rgtno = r.rgtno "
                	+" AND c.caseno = cl.caseno "
                	+" AND c.rgtstate = '11' "
                	+" AND c.caseno = g.otherno "
                	+" AND c.mobilephone IS NULL "
                	+" AND ( r.mngcom like '8632%') "
                	+ " AND NOT EXISTS (SELECT 1 "
                	+ " FROM llclaimdetail clt, lmriskapp ap "
                	+ " WHERE clt.caseno = c.caseno "
                	+ " AND ap.riskcode = clt.riskcode "
                	+ " AND ap.riskprop = 'I' "
                	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
                	+" and c.customerno = ins.insuredno "
                	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
                	+" and ins.grpinsuredphone is not null "
                	+" and g.sumgetmoney>0 "
                	+" AND c.rgttype = '1' " 
                	+" AND cl.givetype in ('1', '2') " 
                	+ " and g.paymode in ('1') "
                	+" and r.togetherflag='1' "  
                	+" AND g.makedate BETWEEN '"+ mRgtDateS+ "' AND '" + mRgtDateE + "'  "
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+ " union all "
                	+ " SELECT r.mngcom, "
                	+ " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                	+ " c.caseno, "
                	+ " c.MobilePhone, "
                	+ " c.customername,  "
                	+ " g.bankaccno, "
                	+ " c.customersex, "
                	 +" g.paymode, "
                 	+" g.sumgetmoney "
                	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
                	+ " WHERE c.rgtno = r.rgtno "
                	+ " AND c.caseno = cl.caseno "
                	+ " AND c.rgtstate = '12' "
                	+ " AND c.caseno = g.otherno "
                	//modify by houyd 2014-02-08
                	+ " AND ( r.mngcom like '8644%' or r.mngcom like '8632%') "
                	+ " AND c.mobilephone IS NOT NULL "
                	+ " AND NOT EXISTS (SELECT 1 "
                	+ " FROM llclaimdetail clt, lmriskapp ap "
                	+ " WHERE clt.caseno = c.caseno "
                	+ " AND ap.riskcode = clt.riskcode "
                	+ " AND ap.riskprop = 'I') "
                	+ " AND c.rgttype = '1' "
                	+ " AND cl.givetype in ('1', '2') "
                	+ " and g.paymode in ('3', '4', '11')  "
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+ " AND g.confdate BETWEEN '"+ mRgtDateS+ "' AND '" + mRgtDateE + "' "
                	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
                	+ " union all"
                	+ " SELECT r.mngcom, "
                	+ " (SELECT name FROM ldcom WHERE comcode=r.mngcom),"
                	+ " c.caseno, "
                	+ " ins.grpinsuredphone, "
                	+ " ins.name,  "
                	+ " g.bankaccno, "
                	+ " c.customersex, "
                	 +" g.paymode, "
                 	+" g.sumgetmoney "
                	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
                	+" WHERE c.rgtno = r.rgtno "
                	+" AND c.caseno = cl.caseno "
                	+" AND c.rgtstate = '12' "
                	+" AND c.caseno = g.otherno "
                	+" AND c.mobilephone IS NULL "
                	//modify by houyd 2014-02-08
                	+ " AND ( r.mngcom like '8644%' or r.mngcom like '8632%') "
                	+" and c.customerno = ins.insuredno "
                	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
                	+" and ins.grpinsuredphone is not null "
                	+" and g.sumgetmoney>0 "
                	+" AND NOT EXISTS (SELECT 1 " 
                	+" FROM llclaimdetail clt, lmriskapp ap " 
                	+" WHERE clt.caseno = c.caseno "
                	+" AND ap.riskcode = clt.riskcode " 
                	+" AND ap.riskprop = 'I') "
                	+" AND c.rgttype = '1' " 
                	+" AND cl.givetype in ('1', '2') " 
                	+" and g.paymode in ('3', '4', '11') " 
                	+" and r.togetherflag='1' "  
                	+" and r.applyertype in ('2','5') "
                	+ " AND c.mngcom LIKE '" +mManageCom+ "%' "
                	+" AND g.confdate BETWEEN '"+ mRgtDateS+ "' AND '" + mRgtDateE + "' "
                	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
                	+" with ur";
        
        

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String Info[] = new String[5];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            String tBankAccNo = tSSRS.GetText(i, 6);
            String tPayMode = tSSRS.GetText(i, 8);
            String tPrem = tSSRS.GetText(i, 9);
            
            String tSexContent = "女士/先生";
            if ("0".equals(tSSRS.GetText(i, 7))) {
            	tSexContent = "先生";
            } else if ("1".equals(tSSRS.GetText(i, 7))) {
            	tSexContent = "女士";
            }
            
            if("1".equals(tPayMode)){
            	Info[4] = tSSRS.GetText(i, 5) + tSexContent +
            		",您的理赔申请已处理完毕，请及时受领保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tPrem)),"0.00")+"元。【人保健康】";
            }else if("4".equals(tPayMode) || "11".equals(tPayMode)
         		   || "3".equals(tPayMode)){
            	
            	if (tBankAccNo == null || "".equals(tBankAccNo) ||
                        "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                        continue;
                    }
            	
            	Info[4] = tSSRS.GetText(i, 5) + tSexContent +
                ",您的理赔申请已处理完毕，请查收尾号为" +
                tSSRS.GetText(i, 6).
                substring(tSSRS.GetText(i, 6).length() - 4,
                          tSSRS.GetText(i, 6).length()) +
                "的账户收到的保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tPrem)),"0.00")+"元。【人保健康】";
            }
            
            mListTable.add(Info);
        }
        return true;
    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLCustomerMessage.vts", "printer");
        System.out.print("XML模板生成开始");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        tTextTag.add("StartDate", mRgtDateS);
        tTextTag.add("EndDate", mRgtDateE);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", ""};
        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("E:\\Temp\\", "LLCustomerMessageBL");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        mManageCom = (String) pmInputData.get(0);
        moperator = (String) pmInputData.get(1);
        mManageComName = (String) pmInputData.get(2);
        mRgtState = (String) pmInputData.get(3);
        System.out.println(mRgtState);
        mRgtDateS = (String) pmInputData.get(4);
        System.out.println(mRgtDateS);
        mRgtDateE = (String) pmInputData.get(5);
        System.out.println(mRgtDateE);
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
//    private void buildError(String szFunc, String szErrMsg) {
//        CError cError = new CError();
//        cError.moduleName = "LLCustomerMessageBL";
//        cError.functionName = szFunc;
//        cError.errorMessage = szErrMsg;
//        System.out.println(szFunc + "--" + szErrMsg);
//        this.mErrors.addOneError(cError);
//    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
