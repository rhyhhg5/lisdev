package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author guoly
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class HealthContCheckRptBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    private TransferData mTransferData = new TransferData();
    private LHCustomInHospitalSchema mLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
    public HealthContCheckRptBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLDPersonSchema.setSchema((LDPersonSchema)
                                       cInputData.getObjectByObjectName(
                                               "LDPersonSchema", 0));
        System.out.println(mLDPersonSchema.getCustomerNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mLDPersonSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLDPersonSchema.getCustomerNo() == null) {
            buildError("getInputData", "没有得到足够的信息:客户号不能为空！");
            return false;
        }
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "HealthContCheckRptBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        //String MStartDate = (String) mTransferData.getValueByName("Begin");
        //String MEndDate = (String) mTransferData.getValueByName("End");
        String InHospitNo = (String) mTransferData.getValueByName("InHospitNo");
        //得到客户基本信息
        String aCustomerNo = "";
        String aCustomerName = "";
        String aSex = "";
        String aBirthDay = "";
        String aGrpName = "";

        //客户基本健康信息
        String aHospitName = "";//体检机构名称
        String aHospitMode = "";// 体检方式
        String aHospitDate = "";//体检日期

        aCustomerNo = mLDPersonSchema.getCustomerNo();
        LDPersonDB mLDPersonDB = new LDPersonDB();
        mLDPersonDB.setCustomerNo(aCustomerNo);

        if (!mLDPersonDB.getInfo()) {
            System.out.println("--------DB_GetInfo_Fail--------");
        }
        aCustomerNo = mLDPersonDB.getCustomerNo();
        aCustomerName = mLDPersonDB.getName();
        aSex = mLDPersonDB.getSex();
        aBirthDay = mLDPersonDB.getBirthday();

        //基本健康信息
        try {

                String sqlHospitName =" select  distinct ( select distinct  HospitName from LDHospital where HospitCode=a.HospitCode)"
                       +" from  LHCustomInHospital a ,LHCustomTest b  "
                       +"  where 1=1 and inhospitmode in ('31','32')  and a.InHospitNo=b.InHospitNo "
                       +"  and    a.customerno = b.customerno and  "
                       +"  a.customerno = '" + aCustomerNo + "' and  "
                       +" a.Inhospitno='"+ InHospitNo +"'"
                       ;
                aHospitName =  new ExeSQL().getOneValue(sqlHospitName);//体检机构名称

                String sqlHospitMode =" select  distinct ( case a.InHospitMode when  '31' then '核保体检' when '32' then  '体检服务'  else '无' end )"
                       +" from  LHCustomInHospital a ,LHCustomTest b  "
                       +"  where 1=1 and inhospitmode in ('31','32')  and a.InHospitNo=b.InHospitNo "
                       +"  and    a.customerno = b.customerno and  "
                       +"  a.customerno = '" + aCustomerNo + "' and  "
                       +" a.Inhospitno='"+ InHospitNo +"'"
                       ;
                aHospitMode =  new ExeSQL().getOneValue(sqlHospitMode);//体检方式

                String sqlHospitDate =" select  distinct InHospitDate "
                       +" from  LHCustomInHospital a ,LHCustomTest b  "
                       +"  where 1=1 and inhospitmode in ('31','32')  and a.InHospitNo=b.InHospitNo "
                       +"  and    a.customerno = b.customerno and  "
                       +"  a.customerno = '" + aCustomerNo + "' and  "
                       +" a.Inhospitno='"+ InHospitNo +"'"
                       ;
                aHospitDate =  new ExeSQL().getOneValue(sqlHospitDate);//体检日期

       } catch (Exception e) {
           CError tError = new CError();
            tError.moduleName = "HealthContCheckRptBL";
           tError.functionName = "";
           tError.errorMessage = "客户基本信息查询失败";
     }

        //一 般 情 况
        ListTable TestComListTable = new ListTable();
        TestComListTable.setName("TESTCOM");
        String[] Title_D = { "MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
        try {
            SSRS tSSRS_D = new SSRS();
            String sql = " select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                         + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                         +"  b.isnormal,  "
                         +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                         +" from LHCustomInHospital a ,LHCustomTest b  "
                         +" where 1=1 and inhospitmode in ('31','32') "
                         +" and  a.customerno = '" +aCustomerNo+ "'"
                         +" and a.InHospitNo=b.InHospitNo "
                         +" and  a.customerno = b.customerno  "
                         +" and b.MedicaItemCode in ('110500009','110500010','110500011','110500012','110500013','110500014','110500015','110500016','110500017')"
                         +" and a.Inhospitno='"+ InHospitNo +"'"
                         ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_D = tExeSQL.execSQL(sql);
            int count_D = tSSRS_D.getMaxRow();
            for (int i = 0; i < count_D; i++) {
                String temp_D[][] = tSSRS_D.getAllData();
                String[] strCol;
                strCol = new String[4];
                strCol[0] = temp_D[i][0];
                strCol[1] = temp_D[i][1];
                strCol[2] = temp_D[i][2];
                strCol[3] = temp_D[i][3];
                TestComListTable.add(strCol);
            }
         } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthContCheckRptBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "一般情况信息查询失败";
            this.mErrors.addOneError(tError);
            mLHCustomInHospitalSchema.setInHospitNo("");
         }

        //各 科 物 理 检 查
        ListTable TestPhyListTable = new ListTable();
        TestPhyListTable.setName("TEST");
        String[] Title_T = { "MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
        try {
            SSRS tSSRS_T = new SSRS();
            String sql_T = " select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                        + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                        +"  b.isnormal,  "
                        +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                        +" from LHCustomInHospital a ,LHCustomTest b  "
                        +" where 1=1 and inhospitmode in ('31','32') "
                        +" and  a.customerno = '" + aCustomerNo + "'"
                        +" and a.InHospitNo=b.InHospitNo "
                        +" and  a.customerno = b.customerno  "
                        +" and b.MedicaItemCode in ('110500001','110500002','110500003','110500004','110500005','110500006','110500007','110500008')"
                        +" and a.Inhospitno='"+ InHospitNo +"'"
                        ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_T = tExeSQL.execSQL(sql_T);
            int count_T = tSSRS_T.getMaxRow();
            for (int i = 0; i < count_T; i++) {
                String temp_T[][] = tSSRS_T.getAllData();
                String[] strCol;
                strCol = new String[4];
                strCol[0] = temp_T[i][0];
                strCol[1] = temp_T[i][1];
                strCol[2] = temp_T[i][2];
                strCol[3] = temp_T[i][3];
                TestPhyListTable.add(strCol);
            }
          } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthContCheckRptBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "各科物理检查信息查询失败";
            this.mErrors.addOneError(tError);
         }
        //实验室检验血常规-项目名称
        ListTable BloodListTable = new ListTable();
        BloodListTable.setName("BLOOD");
        String[] Title_O = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};

        try {
            SSRS tSSRS_O = new SSRS();
            String sql_O = "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                      + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                      +"  b.isnormal,  "
                      +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                      +" from LHCustomInHospital a ,LHCustomTest b  "
                      +" where 1=1 and inhospitmode in ('31','32') "
                      +" and  a.customerno = '" + aCustomerNo + "'"
                      +" and a.InHospitNo=b.InHospitNo "
                      +" and  a.customerno = b.customerno  "
                      +" and b.MedicaItemCode in  ('260000002','260000004','250101001','250101002','250101009','250101014','250101003','250101010','250101027','250101004','250101024','250101025')"
                      +" and a.Inhospitno='"+ InHospitNo +"'"
                      ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_O = tExeSQL.execSQL(sql_O);
            int count_O = tSSRS_O.getMaxRow();
            for (int i = 0; i < count_O; i++) {
                String temp_O[][] = tSSRS_O.getAllData();
                String[] strCol;
                strCol = new String[4];
                strCol[0] = temp_O[i][0];
                strCol[1] = temp_O[i][1];
                strCol[2] = temp_O[i][2];
                strCol[3] = temp_O[i][3];
                BloodListTable.add(strCol);
            }
          } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthContCheckRptBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "验室检验血常规-项目名称查询失败";
            this.mErrors.addOneError(tError);
         }

        //尿常规-项目名称
        ListTable StableListTable = new ListTable();
        StableListTable.setName("STABLE");
        String[] Title_F = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
        try {
           SSRS tSSRS_F = new SSRS();
            String sql_F = "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                     + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                     +"  b.isnormal,  "
                     +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                     +" from LHCustomInHospital a ,LHCustomTest b  "
                     +" where 1=1 and inhospitmode in ('31','32') "
                     +" and  a.customerno = '" + aCustomerNo + "'"
                     +" and a.InHospitNo=b.InHospitNo "
                     +" and  a.customerno = b.customerno  "
                     +" and b.MedicaItemCode in  ('250102001','250102002','250102003','250102005','250102010','250102012','250102036','250102037','250102038','250102039')"
                     +" and a.Inhospitno='"+ InHospitNo +"'"
                     ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_F = tExeSQL.execSQL(sql_F);
            int count_F = tSSRS_F.getMaxRow();
            for (int i = 0; i < count_F; i++) {
                String temp_F[][] = tSSRS_F.getAllData();
                String[] strCol;
                strCol = new String[4];
                strCol[0] = temp_F[i][0];
                strCol[1] = temp_F[i][1];
                strCol[2] = temp_F[i][2];
                strCol[3] = temp_F[i][3];
                StableListTable.add(strCol);
            }
          } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthContCheckRptBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "尿常规-项目名称查询失败";
            this.mErrors.addOneError(tError);
         }

      //便常规-项目名称
      ListTable SoilListTable = new ListTable();
      SoilListTable.setName("SOIL");
      String[] Title_S = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
      try {
            SSRS tSSRS_S = new SSRS();
            String sql_S = "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                   + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                   +"  b.isnormal,  "
                   +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                   +" from LHCustomInHospital a ,LHCustomTest b  "
                   +" where 1=1 and inhospitmode in ('31','32') "
                   +" and  a.customerno = '" + aCustomerNo + "'"
                   +" and a.InHospitNo=b.InHospitNo "
                   +" and  a.customerno = b.customerno  "
                   +" and b.MedicaItemCode in  ('250103001','250103002','250103005','250103006')"
                   +" and a.Inhospitno='"+ InHospitNo +"'"
                   ;
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS_S = tExeSQL.execSQL(sql_S);
         int count_F = tSSRS_S.getMaxRow();
         for (int i = 0; i < count_F; i++) {
             String temp_F[][] = tSSRS_S.getAllData();
             String[] strCol;
             strCol = new String[4];
             strCol[0] = temp_F[i][0];
             strCol[1] = temp_F[i][1];
             strCol[2] = temp_F[i][2];
             strCol[3] = temp_F[i][3];
             SoilListTable.add(strCol);
         }

     } catch (Exception e) {
         CError tError = new CError();
         tError.moduleName = "HealthContCheckRptBL";
         tError.functionName = "createAddressNo";
         tError.errorMessage = "便常规-项目名称查询失败";
         this.mErrors.addOneError(tError);
     }

     //血糖-项目名称
     ListTable SugerListTable = new ListTable();
     SugerListTable.setName("SUGER");
     String[] Title_G = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
     try {
           SSRS tSSRS_G = new SSRS();
           String sql_G = "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                  + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                  +"  b.isnormal,  "
                  +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                  +" from LHCustomInHospital a ,LHCustomTest b  "
                  +" where 1=1 and inhospitmode in ('31','32') "
                  +" and  a.customerno = '" + aCustomerNo + "'"
                  +" and a.InHospitNo=b.InHospitNo "
                  +" and  a.customerno = b.customerno  "
                  +" and b.MedicaItemCode in ('250302001','250302003','250302010','250310039','250310041')"
                  +" and a.Inhospitno='"+ InHospitNo +"'"
                  ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_G = tExeSQL.execSQL(sql_G);
        int count_F = tSSRS_G.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_G.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            SugerListTable.add(strCol);
        }
    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "血糖-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }
    //血脂-项目名称
    ListTable SFatListTable = new ListTable();
    SFatListTable.setName("SFAT");
    String[] Title_A = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
    try {
      SSRS tSSRS_A = new SSRS();
      String sql_A = "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
             + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
             +"  b.isnormal,  "
             +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
             +" from LHCustomInHospital a ,LHCustomTest b  "
             +" where 1=1 and inhospitmode in ('31','32') "
             +" and  a.customerno = '" + aCustomerNo + "'"
             +" and a.InHospitNo=b.InHospitNo "
             +" and  a.customerno = b.customerno  "
             +" and b.MedicaItemCode in  ('250303001','250303002','250303004','250303005','250303018') "
             +" and a.Inhospitno='"+ InHospitNo +"'"
             ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_A = tExeSQL.execSQL(sql_A);
        int count_F = tSSRS_A.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_A.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            SFatListTable.add(strCol);
        }
    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "血脂-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }

    //肝功能-项目名称
    ListTable LiverListTable = new ListTable();
    LiverListTable.setName("LIVER");
    String[] Title_L = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
    try {
        SSRS tSSRS_L = new SSRS();
        String sql_L = "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + "  b.isnormal,  "
                       + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                       + " from LHCustomInHospital a ,LHCustomTest b  "
                       + " where 1=1 and inhospitmode in ('31','32') "
                       + " and  a.customerno = '" + aCustomerNo + "'"
                       + " and a.InHospitNo=b.InHospitNo "
                       + " and  a.customerno = b.customerno  "
                       + " and b.MedicaItemCode in   ('250305001','250305002','250305003','250301001','250301002','250301003','250301020','250305007','250305008','250305009','250305026','250305011','250305005') "
                       +" and a.Inhospitno='"+ InHospitNo +"'"
                       ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_L = tExeSQL.execSQL(sql_L);
        int count_F = tSSRS_L.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_L.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            LiverListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "肝功能-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }

    //肾功能-项目名称
    ListTable KidneyListTable = new ListTable();
    KidneyListTable.setName("KIDNEY");
    String[] Title_K = {"MEDICAITEMNAME","RESTRESULT","STANDARDMEASUREUNIT", "ISNORMAL", "NOMALVALUE"};
    try {
      SSRS tSSRS_K = new SSRS();
      String sql_K = "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
             + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
             +"  b.isnormal,  "
             +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
             +" from LHCustomInHospital a ,LHCustomTest b  "
             +" where 1=1 and inhospitmode in ('31','32') "
             +" and  a.customerno = '" + aCustomerNo + "'"
             +" and a.InHospitNo=b.InHospitNo "
             +" and  a.customerno = b.customerno  "
             +" and b.MedicaItemCode in   ('250307001','250307002','250307005')"
             +" and a.Inhospitno='"+ InHospitNo +"'"
             ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_K = tExeSQL.execSQL(sql_K);
        int count_F = tSSRS_K.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_K.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            KidneyListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "肾功能-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }

    //无机元素测定-项目名称
    ListTable AbioListTable = new ListTable();
    AbioListTable.setName("ABIO");
    String[] Title_B = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
    try {
        SSRS tSSRS_B = new SSRS();
        String sql_B= "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + "  b.isnormal,  "
                       + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                       + " from LHCustomInHospital a ,LHCustomTest b  "
                       + " where 1=1 and inhospitmode in ('31','32') "
                       + " and  a.customerno = '" + aCustomerNo + "'"
                       + " and a.InHospitNo=b.InHospitNo "
                       + " and  a.customerno = b.customerno  "
                       + " and b.MedicaItemCode in ('250304001','250304002','250304003','250304004','250304005','250304006','250304007','250304008','250304014')"
                       +" and a.Inhospitno='"+ InHospitNo +"'"
                       ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_B = tExeSQL.execSQL(sql_B);
        int count_F = tSSRS_B.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_B.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            AbioListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "无机元素测定-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }

    //心肌疾病-项目名称
       ListTable CardiacListTable = new ListTable();
       CardiacListTable.setName("CARDIAC");
       String[] Title_C = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
       try {
           SSRS tSSRS_C = new SSRS();
           String sql_C= "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                          + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                          + "  b.isnormal,  "
                          + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                          + " from LHCustomInHospital a ,LHCustomTest b  "
                          + " where 1=1 and inhospitmode in ('31','32') "
                          + " and  a.customerno = '" + aCustomerNo + "'"
                          + " and a.InHospitNo=b.InHospitNo "
                          + " and  a.customerno = b.customerno  "
                          + " and b.MedicaItemCode in  ('250306001','250306005','250306007','250306008','250306009','250306010','250306011')"
                          +" and a.Inhospitno='"+ InHospitNo +"'"
                          ;
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS_C = tExeSQL.execSQL(sql_C);
           int count_F = tSSRS_C.getMaxRow();
           for (int i = 0; i < count_F; i++) {
               String temp_F[][] = tSSRS_C.getAllData();
               String[] strCol;
               strCol = new String[4];
               strCol[0] = temp_F[i][0];
               strCol[1] = temp_F[i][1];
               strCol[2] = temp_F[i][2];
               strCol[3] = temp_F[i][3];
               CardiacListTable.add(strCol);
           }

       } catch (Exception e) {
           CError tError = new CError();
           tError.moduleName = "HealthContCheckRptBL";
           tError.functionName = "createAddressNo";
           tError.errorMessage = "心肌疾病-项目名称查询失败";
           this.mErrors.addOneError(tError);
    }
    //感染免疫学-项目名称
       ListTable ImmunityListTable = new ListTable();
       ImmunityListTable.setName("IMMUNITY");
       String[] Title_M = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
       try {
           SSRS tSSRS_M = new SSRS();
           String sql_M= "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                          + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                          + "  b.isnormal,  "
                          + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                          + " from LHCustomInHospital a ,LHCustomTest b  "
                          + " where 1=1 and inhospitmode in ('31','32') "
                          + " and  a.customerno = '" + aCustomerNo + "'"
                          + " and a.InHospitNo=b.InHospitNo "
                          + " and  a.customerno = b.customerno  "
                          + " and b.MedicaItemCode in ('250403004','250403005','250403006','250403007','250403008','250403009','250403013','250403014','250403019','250403015','250403016','250403017','250403018')"
                          +" and a.Inhospitno='"+ InHospitNo +"'"
                          ;
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS_M = tExeSQL.execSQL(sql_M);
           int count_F = tSSRS_M.getMaxRow();
           for (int i = 0; i < count_F; i++) {
               String temp_F[][] = tSSRS_M.getAllData();
               String[] strCol;
               strCol = new String[4];
               strCol[0] = temp_F[i][0];
               strCol[1] = temp_F[i][1];
               strCol[2] = temp_F[i][2];
               strCol[3] = temp_F[i][3];
               ImmunityListTable.add(strCol);
           }
       } catch (Exception e) {
           CError tError = new CError();
           tError.moduleName = "HealthContCheckRptBL";
           tError.functionName = "createAddressNo";
           tError.errorMessage = "感染免疫学-项目名称查询失败";
           this.mErrors.addOneError(tError);
    }

    //肿瘤标记物-项目名称
   ListTable KnubListTable = new ListTable();
   KnubListTable.setName("KNUB");
   String[] Title_U = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
   try {
       SSRS tSSRS_U = new SSRS();
       String sql_U= "select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                      + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                      + "  b.isnormal,  "
                      + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                      + " from LHCustomInHospital a ,LHCustomTest b  "
                      + " where 1=1 and inhospitmode in ('31','32') "
                      + " and  a.customerno = '" + aCustomerNo + "'"
                      + " and a.InHospitNo=b.InHospitNo "
                      + " and  a.customerno = b.customerno  "
                      + " and b.MedicaItemCode in ('250404001','250404002','250404005','250404010','250404011','250404013','250404021','250404022','250404009','250404023')"
                      +" and a.Inhospitno='"+ InHospitNo +"'"
                      ;
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS_U = tExeSQL.execSQL(sql_U);
       int count_F = tSSRS_U.getMaxRow();
       for (int i = 0; i < count_F; i++) {
           String temp_F[][] = tSSRS_U.getAllData();
           String[] strCol;
           strCol = new String[4];
           strCol[0] = temp_F[i][0];
           strCol[1] = temp_F[i][1];
           strCol[2] = temp_F[i][2];
           strCol[3] = temp_F[i][3];
           KnubListTable.add(strCol);
       }

     } catch (Exception e) {
       CError tError = new CError();
       tError.moduleName = "HealthContCheckRptBL";
       tError.functionName = "createAddressNo";
       tError.errorMessage = "肿瘤标记物-项目名称查询失败";
       this.mErrors.addOneError(tError);
    }

    //血流变检查-项目名称
    ListTable BstreamListTable = new ListTable();
    BstreamListTable.setName("BSTREAM");
    String[] Title_E = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
    try {
        SSRS tSSRS_E = new SSRS();
        String sql_E = "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + "  b.isnormal,  "
                       + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                       + " from LHCustomInHospital a ,LHCustomTest b  "
                       + " where 1=1 and inhospitmode in ('31','32') "
                       + " and  a.customerno = '" + aCustomerNo + "'"
                       + " and a.InHospitNo=b.InHospitNo "
                       + " and  a.customerno = b.customerno  "
                       + " and b.MedicaItemCode in ('250203070','250203071','250203072','250203078','250203079','250203080','250203081','250203082','250203083','250203084','250203085','250203086','250203087','250203088','250203092','250203093','250203094','250203096','250203095','250203098','250101008')"
                       +" and a.Inhospitno='"+ InHospitNo +"'"
                       ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_E = tExeSQL.execSQL(sql_E);
        int count_F = tSSRS_E.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_E.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            BstreamListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "血流变检查-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }

    //其他检查-项目名称
    ListTable OtherListTable = new ListTable();
    OtherListTable.setName("OTHER");
    String[] Title_R = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
    try {
        SSRS tSSRS_R = new SSRS();
        String sql_R = "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + "  b.isnormal,  "
                       + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                       + " from LHCustomInHospital a ,LHCustomTest b  "
                       + " where 1=1 and inhospitmode in ('31','32') "
                       + " and  a.customerno = '" + aCustomerNo + "'"
                       + " and a.InHospitNo=b.InHospitNo "
                       + " and  a.customerno = b.customerno  "
                       + " and b.MedicaItemCode not in  ('110500009','110500010','110500011','110500012','110500013','110500014','110500015','110500016','110500017',"
                       +" '110500001','110500002','110500003','110500004','110500005','110500006','110500007','110500008', "
                       +" '260000002','260000004','250101001','250101002','250101009','250101014','250101003','250101010','250101027','250101004','250101024','250101025', "
                       +" '250102001','250102002','250102003','250102005','250102010','250102012','250102036','250102037','250102038','250102039', "
                       +" '250103001','250103002','250103005','250103006', "
                       +" '250302001','250302003','250302010','250310039','250310041', "
                       +" '250303001','250303002','250303004','250303005','250303018', "
                       +" '250305001','250305002','250305003','250301001','250301002','250301003','250301020','250305007','250305008','250305009','250305026','250305011','250305005', "
                       +" '250307001','250307002','250307005', "
                       +" '250304001','250304002','250304003','250304004','250304005','250304006','250304007','250304008','250304014', "
                       +" '250306001','250306005','250306007','250306008','250306009','250306010','250306011', "
                       +" '250403004','250403005','250403006','250403007','250403008','250403009','250403013','250403014','250403019','250403015','250403016','250403017','250403018',"
                       +" '250404001','250404002','250404005','250404010','250404011','250404013','250404021','250404022','250404009','250404023', "
                       +" '250203070','250203071','250203072','250203078','250203079','250203080','250203081','250203082','250203083','250203084','250203085','250203086','250203087','250203088','250203092','250203093','250203094','250203096','250203095','250203098','250101008', "
                       +" '210101001','210102001','210102002','210102013','220201010','220201011','210500002','220301001','220301003','220301004','220600004','310701001','310701003','220400001') "
                       +" and a.Inhospitno='"+ InHospitNo +"'"
                       ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_R = tExeSQL.execSQL(sql_R);
        int count_F = tSSRS_R.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_R.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            OtherListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "其他检查-项目名称查询失败";
        this.mErrors.addOneError(tError);
    }


    //影像学/心电图检查-项目名称
    ListTable ShadowListTable = new ListTable();
    ShadowListTable.setName("SHADOW");
    String[] Title_W = {"MEDICAITEMNAME", "RESTRESULT", "STANDARDMEASUREUNIT","ISNORMAL", "NOMALVALUE"};
    try {
        SSRS tSSRS_W = new SSRS();
        String sql_W = "select  (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + " b.TestResult ||' '||(select distinct value(c.standardmeasureunit,'') from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                       + "  b.isnormal,  "
                       + " (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode )"
                       + " from LHCustomInHospital a ,LHCustomTest b  "
                       + " where 1=1 and inhospitmode in ('31','32') "
                       + " and  a.customerno = '" + aCustomerNo + "'"
                       + " and a.InHospitNo=b.InHospitNo "
                       + " and  a.customerno = b.customerno  "
                       +" and b.MedicaItemCode in  ('210101001','210102001','210102002','210102013','220201010','220201011','210500002','220301001','220301003','220301004','220600004','310701001','310701003','220400001')"
                       +" and a.Inhospitno='"+ InHospitNo +"'"
                       ;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_W = tExeSQL.execSQL(sql_W);
        int count_F = tSSRS_W.getMaxRow();
        for (int i = 0; i < count_F; i++) {
            String temp_F[][] = tSSRS_W.getAllData();
            String[] strCol;
            strCol = new String[4];
            strCol[0] = temp_F[i][0];
            strCol[1] = temp_F[i][1];
            strCol[2] = temp_F[i][2];
            strCol[3] = temp_F[i][3];
            ShadowListTable.add(strCol);
        }

    } catch (Exception e) {
        CError tError = new CError();
        tError.moduleName = "HealthContCheckRptBL";
        tError.functionName = "createAddressNo";
        tError.errorMessage = "影像学/心电图检查-项目名称失败";
        this.mErrors.addOneError(tError);
    }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("HealthContCheckRpt.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" + tSrtTool.getDay() + "日";

        //HealthArchiveBL模版元素
        //MStartDate=MStartDate.replaceFirst("-","年");
       // MStartDate=MStartDate.replaceFirst("-","月");
       // MEndDate=MEndDate.replaceFirst("-","年");
       // MEndDate=MEndDate.replaceFirst("-","月");
       //texttag.add("MStartDate", MStartDate);
       //texttag.add("MEndDate", MEndDate);
        texttag.add("CustomerName", aCustomerName);
        texttag.add("CustomerNo", aCustomerNo);
        if (aSex.equals("0")) {
            aSex = "男";
        } else {
            aSex = "女";
        }
        texttag.add("Sex", aSex);
        String Age = this.getAge(aBirthDay);
        texttag.add("BirthDay", aBirthDay);
        texttag.add("WorkPlace", aGrpName);
        texttag.add("HospitName",aHospitName);
        texttag.add("HospitMode",aHospitMode);
        texttag.add("HospitDate",aHospitDate);

        SSRS tTask = null;
        String sql1="select name  from ldcom where comcode='"+ mGlobalInput.ManageCom +"'";
        ExeSQL tExeSQL= new ExeSQL();
        tTask=tExeSQL.execSQL(sql1);
        String[][] msgInfo = tTask.getAllData();
        texttag.add("Corp",msgInfo[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息
        xmlexport.addListTable(TestComListTable, Title_D);
        xmlexport.addListTable(TestPhyListTable, Title_T);
        xmlexport.addListTable(BloodListTable, Title_O);
        xmlexport.addListTable(StableListTable, Title_F);
        xmlexport.addListTable(SoilListTable, Title_S);
        xmlexport.addListTable(SugerListTable, Title_G);
        xmlexport.addListTable(SFatListTable, Title_A);
        xmlexport.addListTable(LiverListTable, Title_L);
        xmlexport.addListTable(KidneyListTable, Title_K);
        xmlexport.addListTable(AbioListTable, Title_B);
        xmlexport.addListTable(CardiacListTable, Title_C);
        xmlexport.addListTable(ImmunityListTable, Title_M);
        xmlexport.addListTable(KnubListTable, Title_U);
        xmlexport.addListTable(BstreamListTable, Title_E);
        xmlexport.addListTable(OtherListTable, Title_R);
        xmlexport.addListTable(ShadowListTable, Title_W);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }
    private String getAge(String BirthDay) {
        Integer ii = new Integer("0");
        Integer ii2 = new Integer("0");
        int i = (ii2.parseInt(PubFun.getCurrentDate().substring(0, 4)) -
                 ii.parseInt(BirthDay.substring(0, 4)));

        if (ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
            ii.parseInt(BirthDay.substring(5, 7)) > 0)
        {
            BirthDay = i + "";
        }
        else
        {
            if ((ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
                 ii.parseInt(BirthDay.substring(5, 7))) < 0)
            {
                BirthDay = (i - 1) + "";
            }
            else
            {
                if ((ii2.parseInt(PubFun.getCurrentDate().substring(8, 10)) -
                     ii.parseInt(BirthDay.substring(8, 10))) < 0)
                {
                    BirthDay = (i - 1) + "";
                }
                else
                {
                    BirthDay = i + "";
                }
            }
        }
        return BirthDay;
    }

    public static void main(String[] args) {

        HealthContCheckRptBL tHealthContCheckRptBL = new HealthContCheckRptBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("000934152");
        tVData.addElement(tLDPersonSchema);
        TransferData StartEnd = new TransferData();
        StartEnd.setNameAndValue("InHospitNo","1");
        tVData.addElement(StartEnd);
        tHealthContCheckRptBL.submitData(tVData, "PRINT");
        VData vdata = tHealthContCheckRptBL.getResult();
    }

    private void jbInit() throws Exception {
    }

}
