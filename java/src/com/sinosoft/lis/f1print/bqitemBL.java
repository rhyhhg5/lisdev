package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.vdb.LGWorkDBSet;
import com.sinosoft.utility.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.db.LPBnfDB;

/**
 * <p>Title: 保全统计 </p>
 * <p>Description: 保单工作状况统计月报表 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author yanchao
 * @modify qulq 2007-1-25 17:40
 * @version 1.0
 * @date 2005-11.18
 */

public class bqitemBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ListTable tlistTable;
    private ListTable klistTable;
    private VData mResult = new VData();

    String StartDate = "";
    String EndDate = "";
    String organcode = "";
    String managecom = "";
    public bqitemBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        organcode = (String) tTransferData.getValueByName("organcode");
        System.out.println(StartDate);
        System.out.println(EndDate);
        System.out.println(organcode);

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("bqitem.vts", "printer"); //最好紧接着就初始化xml文档

        SSRS tSSRS = new SSRS();
        SSRS lSSRS = new SSRS();
        SSRS kSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        if (organcode.length() > 4 && organcode.substring(4).equals("0000")) {
            managecom = " and b.managecom ='" + organcode + "'";
        } else {
            managecom = " and b.managecom  like '" + organcode + "%'";
        }

        String sql = "select name from ldcom " +
                     "where comcode='" + organcode + "'";
        String comname = tExeSQL.getOneValue(sql);

        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);
        texttag.add("comname", comname);

        /*
                String sql1 = "select b.managecom from lpedoritem a,lccont b" +
                              " where  edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
                              "and a.contno=b.contno and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                              managecom +
                              " group by b.managecom "+
                              "union "+
                              "select b.managecom from lpedoritem a,lbcont b" +
                             " where  edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
                             "and a.contno=b.contno and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                              managecom +
                             " group by b.managecom with ur";
         */
        String sql1 = " select b.managecom from lpedoritem a,lccont b"
                      + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                      + " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +StartDate + "' and '" + EndDate + "')"
                      + " and a.contno=b.contno "
                      + managecom
                      + " group by b.managecom "
                      + "union "
                      + " select b.managecom from lpedoritem a,lbcont b"
                      + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                      + " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                      StartDate + "' and '" + EndDate + "')"
                      + " and a.contno=b.contno "
                      + managecom
                      + " group by b.managecom with ur"
                      ;
        tSSRS = tExeSQL.execSQL(sql1);
        System.out.println(sql1);

        System.out.println("row=" + tSSRS.getMaxRow());

        //个单项目统计

        tlistTable = new ListTable();
        tlistTable.setName("ORGANP");

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] strArr = new String[15];

            //and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=p.contno)
            /*            String sql2 =
                                "select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a " +
                               " where edortype='WT'  and edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql2 = " select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                          + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                          + " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +StartDate + "' and '" + EndDate + "')"
                          + " and a.edortype='WT' and  (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno ) ='" +
                          tSSRS.GetText(i, 1) + "'  "
                          + " with ur"
                          ;
            lSSRS = tExeSQL.execSQL(sql2);
            strArr[0] = tSSRS.GetText(i, 1);
            String sql3 = "select name from ldcom " +
                          "where comcode='" + tSSRS.GetText(i, 1) + "'";
            strArr[1] = tExeSQL.getOneValue(sql3);
            strArr[2] = lSSRS.GetText(1, 1);
            strArr[3] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    lSSRS.GetText(1, 2)), "0.00"));
            /*
                        String sql4=

                                "select  count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a " +
                               " where  edortype='CT'  and edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql4 =
                    "select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                    + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                    +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                    StartDate + "' and '" + EndDate + "')"
                    + " and a.edortype='CT' and  (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno) ='" +
                    tSSRS.GetText(i, 1) + "'  "
                    + " with ur"
                    ;
            lSSRS = tExeSQL.execSQL(sql4);
            strArr[4] = lSSRS.GetText(1, 1);
            strArr[5] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    lSSRS.GetText(1, 2)), "0.00"));
            /*
                        String sql5=

             "select  count(edoracceptno) m from lpedoritem a" +
                                " where   edortype='AD'  and edorstate='0' " +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                                " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql5 = "select  count(edoracceptno) m from lpedoritem a"
                          + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                          +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                          StartDate + "' and '" + EndDate + "')"
                          + " and a.edortype='AD' and  (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno ) ='" +
                          tSSRS.GetText(i, 1) + "'  "
                          + " with ur"
                          ;

            lSSRS = tExeSQL.execSQL(sql5);
            strArr[6] = lSSRS.GetText(1, 1);
            /*
                        String sql6=

             "select  count(edoracceptno) m from lpedoritem a " +
                               " where  edortype not in ('WT','CT','AD') and  edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem) " +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                                " with ur";
             */
            String sql6 = " select  count(edoracceptno) m from lpedoritem a "
                          + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                          +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                          StartDate + "' and '" + EndDate + "')"
                          + " and a.edortype  not in ('WT','CT','AD')  and  (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno ) ='" +
                          tSSRS.GetText(i, 1) + "'  "
                          + " with ur"
                          ;
            lSSRS = tExeSQL.execSQL(sql6);
            strArr[7] = lSSRS.GetText(1, 1);
            /*
                       String sql7=

             "select  count(edoracceptno) m from lpedoritem a " +
                               " where edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem) " +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql7 = "select  count(edoracceptno) m from lpedoritem a "
                          + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                          +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                          StartDate + "' and '" + EndDate + "')"
                          + " and (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno ) ='" +
                          tSSRS.GetText(i, 1) + "'  "
                          + " with ur"
                          ;

            lSSRS = tExeSQL.execSQL(sql7);

            strArr[8] = lSSRS.GetText(1, 1);
            /*
                       String sql8=

             "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a " +
                               " where    edorstate='0'  and a.getmoney>0  and edoracceptno not in (select edoracceptno from lpgrpedoritem) " +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql8 =
                    "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                    + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                    +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                    StartDate + "' and '" + EndDate + "')"
                    + " and a.getmoney>0 and (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont e where  e.contno=a.contno ) ='" +
                    tSSRS.GetText(i, 1) + "'  "
                    + " with ur"
                    ;

            lSSRS = tExeSQL.execSQL(sql8);
            strArr[9] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    lSSRS.GetText(1, 1)), "0.00"));
            /*
                       String sql16=

             "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a " +
                               " where    edorstate='0'  and a.getmoney<0  and edoracceptno not in (select edoracceptno from lpgrpedoritem) " +
             " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               " and  (select managecom from lbcont  c where  c.contno=a.contno union select managecom from lccont  c where  c.contno=a.contno) ='" + tSSRS.GetText(i, 1) + "'  "+
                               " with ur";
             */
            String sql16 =
                    "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                    + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                    +
                    " c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                    StartDate + "' and '" + EndDate + "')"
                    + " and a.getmoney<0 and (select managecom from lbcont  d where  d.contno=a.contno union select managecom from lccont  e where  e.contno=a.contno ) ='" +
                    tSSRS.GetText(i, 1) + "'  "
                    + " with ur"
                    ;

            lSSRS = tExeSQL.execSQL(sql16);
            strArr[10] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    lSSRS.GetText(1, 1)), "0.00"));

            tlistTable.add(strArr);
        }

        String[] strArrHead = new String[15];
        strArrHead[0] = "序号";
        strArrHead[1] = "管理机构";
        strArrHead[2] = "犹豫期退保件数";
        strArrHead[3] = "犹豫期退保金额";
        strArrHead[4] = "退保件数";
        strArrHead[5] = "退保金额";
        strArrHead[6] = "保障保费调整";
        strArrHead[7] = "续保变更";
        strArrHead[8] = "保单注销";
        strArrHead[9] = "复效";
        strArrHead[10] = "联系方式变更";
        strArrHead[11] = "其它";
        strArrHead[12] = "件数合计";
        xmlexport.addListTable(tlistTable, strArrHead);
        /*
                String sql9 =
                        "select sum(m),round(sum(n),2) from (" +
                        " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lccont b " +
                        " where edortype='WT'  and edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                        managecom + " and a.contno=b.contno "+
                        " union  all "+
                        " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lbcont b " +
                        " where edortype='WT'  and edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                        managecom + "   and a.contno=b.contno ) k with ur";
         */
        String sql9 =
                " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                + " where edortype='WT' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                managecom
                + " union "
                + " select 1 from lccont b where  b.contno=a.contno " +
                managecom
                + ")with ur"
                ;

        tSSRS = tExeSQL.execSQL(sql9);
        System.out.println(sql9);
        String WTPCount = tSSRS.GetText(1, 1);
        String WTPMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                tSSRS.GetText(1, 2)), "0.00"));
        texttag.add("WTPCount", WTPCount);
        texttag.add("WTPMoney", WTPMoney);
        /*
                String sql10 =
                        "select sum(m),round(sum(n),2) from (" +
                        " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lccont b " +
                        " where edortype='CT'  and edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                        managecom + " and a.contno=b.contno " +
                        " union all "+
                        " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lbcont b " +
                        " where edortype='CT'  and edorstate='0'and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                        managecom + " and a.contno=b.contno ) k with ur ";
         */
        String sql10 =
                " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                + " where edortype='CT' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                managecom
                + " union "
                + " select 1 from lccont b where b.contno=a.contno " +
                managecom
                + ")with ur"
                ;
        lSSRS = tExeSQL.execSQL(sql10);
        System.out.println(sql10);
        String CTPCount = lSSRS.GetText(1, 1);
        String CTPMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                lSSRS.GetText(1, 2)), "0.00"));
        texttag.add("CTPCount", CTPCount);
        texttag.add("CTPMoney", CTPMoney);
        /*
                String sql11 =
                       "select sum(m) from (" +
         "select  count(edoracceptno) m from lpedoritem a,lccont b " +
                        " where   edortype='AD'  and edorstate='0' " +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + " and a.contno=b.contno"+
                        " union all "+
         " select  count(edoracceptno) m from lpedoritem a,lbcont b " +
                        " where   edortype='AD'  and edorstate='0' " +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + "    and a.contno=b.contno ) k with ur";
         */
        String sql11 = " select count(edoracceptno) m from lpedoritem a "
                       + " where edortype='AD' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                       +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                       StartDate + "' and '" + EndDate + "')"
                       +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                       managecom
                       + " union "
                       +
                       " select 1 from lccont b where  b.contno=a.contno " +
                       managecom
                       + ")with ur"
                       ;
        lSSRS = tExeSQL.execSQL(sql11);
        String ADPCount = lSSRS.GetText(1, 1);
        texttag.add("ADPCount", ADPCount);
        /*
               String sql12 =
                       "select sum(m) from (" +
         "select  count(edoracceptno) m from lpedoritem a,lccont b " +
                        " where   edortype not in ('WT','CT','AD')  and edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + "  and a.contno=b.contno"+
                        " union  all "+
         "select  count(edoracceptno) m from lpedoritem a,lbcont b " +
                        " where   edortype not in ('WT','CT','AD')  and edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                         managecom + "  and a.contno=b.contno ) k with ur";
         */
        String sql12 =
                " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                + " where a.edortype not in ('WT','CT','AD') and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                managecom
                + " union "
                + " select 1 from lccont b where  b.contno=a.contno " +
                managecom
                + ")with ur"
                ;
        lSSRS = tExeSQL.execSQL(sql12);
        String OPCount = lSSRS.GetText(1, 1);
        texttag.add("OPCount", OPCount);
        /*
               String sql13 =
                       "select sum(m) from (" +
         "select  count(edoracceptno) m from lpedoritem a,lccont b" +
                       " where  edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                       managecom + "  and a.contno=b.contno "+
                       " union all "+
         "select  count(edoracceptno) m from lpedoritem a,lbcont b" +
                       " where  edorstate='0' and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + "  and a.contno=b.contno ) k with ur";
         */
        String sql13 =
                " select count(edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpedoritem a "
                + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno  " +
                managecom
                + " union "
                + " select 1 from lccont b where  b.contno=a.contno " +
                managecom
                + ")with ur"
                ;
        lSSRS = tExeSQL.execSQL(sql13);
        String AllPCount = lSSRS.GetText(1, 1);
        texttag.add("AllPCount", AllPCount);
        /*
               String sql14 =
                       "select round(sum(n),2) from (" +
         "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lccont b" +
                       " where  edorstate='0'  and a.getmoney>0  and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + " and a.contno=b.contno"+
                       " union all "+
         "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lbcont b" +
                       " where  edorstate='0'  and a.getmoney>0  and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                       managecom + " and a.contno=b.contno ) k with ur";
         */
        String sql14 =
                " select round(nvl(abs(sum(a.getmoney)),0),2) n from lpedoritem a "
                + " where a.getmoney>0 and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                managecom
                + " union "
                + " select 1 from lccont b where  b.contno=a.contno " +
                managecom
                + ")with ur"
                ;
        lSSRS = tExeSQL.execSQL(sql14);
        String AllGetPMoney = String.valueOf(PubFun.setPrecision(Double.
                parseDouble(lSSRS.GetText(1, 1)), "0.00"));
        texttag.add("AllGetPMoney", AllGetPMoney);
        /*
               String sql15 =
                       "select round(sum(n),2) from (" +
         "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lccont b" +
                       " where  edorstate='0'  and a.getmoney<0  and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + " and a.contno=b.contno"+
                       " union all "+
         "select  nvl(abs(sum(a.getmoney)),0) n from lpedoritem a,lbcont b" +
                       " where  edorstate='0'  and a.getmoney<0  and edoracceptno not in (select edoracceptno from lpgrpedoritem)" +
         " and a.makedate between '" + StartDate + "' and '" + EndDate + "'" +
                        managecom + "   and a.contno=b.contno ) k with ur";
         */
        String sql15 =
                " select round(nvl(abs(sum(a.getmoney)),0),2) n from lpedoritem a "
                + " where a.getmoney<0 and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.othernotype ='1' and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbcont b where b.contno=a.contno " +
                managecom
                + " union "
                + " select 1 from lccont b where  b.contno=a.contno " +
                managecom
                + ")with ur"
                ;
        lSSRS = tExeSQL.execSQL(sql15);
        String AllPayPMoney = String.valueOf(PubFun.setPrecision(Double.
                parseDouble(lSSRS.GetText(1, 1)), "0.00"));
        texttag.add("AllPayPMoney", AllPayPMoney);

        //团单项目统计


        klistTable = new ListTable();
        klistTable.setName("ORGANG");
        /*
                String sql20 ="select b.managecom from lpgrpedoritem a,lcgrpcont b,lpedorapp c " +
                              " where  c.edorstate='0' and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               managecom +
                              " group by b.managecom " +
                              "union "+
         "select b.managecom from lpgrpedoritem a,lbgrpcont b,lpedorapp c " +
                              " where  c.edorstate='0' and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
         " and a.makedate between '"+StartDate+"' and '"+EndDate+"'"+
                               managecom +
                              " group by b.managecom with ur" ;
         */
        String sql20 = " select b.managecom from lpgrpedoritem a,lcgrpcont b"
                       + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                       + "	c.edorstate = '0' and c.ModifyDate between '" +
                       StartDate + "' and '" + EndDate + "')"
                       + " and a.grpcontno=b.grpcontno "
                       + managecom
                       + " group by b.managecom "
                       + "union "
                       + " select b.managecom from lpgrpedoritem a,lbgrpcont b"
                       + " where exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno and "
                       + " c.edorstate = '0' and c.ModifyDate between '" +
                       StartDate + "' and '" + EndDate + "')"
                       + " and a.grpcontno=b.grpcontno "
                       + managecom
                       + " group by b.managecom with ur"
                       ;
        tSSRS = tExeSQL.execSQL(sql20);
        System.out.println(sql20);

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] strArr1 = new String[15];

            String sql21 =

                    "select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lpedorapp c " +
                    " where   edortype='WT'  and c.edorstate='0'  and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";
            kSSRS = tExeSQL.execSQL(sql21);
            strArr1[0] = tSSRS.GetText(i, 1);
            String sql22 = "select name from ldcom " +
                           "where comcode='" + tSSRS.GetText(i, 1) + "'";
            strArr1[1] = tExeSQL.getOneValue(sql22);
            strArr1[2] = kSSRS.GetText(1, 1);
            strArr1[3] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 2)), "0.00"));
            String sql23 =

                    "select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lpedorapp c " +
                    " where   edortype='CT'  and   c.edorstate='0' and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "' " +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql23);
            strArr1[4] = kSSRS.GetText(1, 1);
            strArr1[5] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 2)), "0.00"));
            String sql24 =

                    "select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lpedorapp b " +
                    " where   a.edortype='NI'  and b.edorstate='0' and a.edoracceptno = b.edoracceptno " +
                    " and b.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql24);
            strArr1[6] = kSSRS.GetText(1, 1);
            strArr1[7] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 2)), "0.00"));
            String sql25 =

                    "select  count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lpedorapp c " +
                    " where   edortype='ZT'  and c.edorstate='0'   and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql25);
            strArr1[8] = kSSRS.GetText(1, 1);
            strArr1[9] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 2)), "0.00"));
            String sql26 =

                    " select count(a.edoracceptno) m from lpgrpedoritem  a , lpedorapp c" +
                    " where  edortype not in ('WT','CT','NI','ZT')  and c.edorstate='0' and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql26);
            strArr1[10] = kSSRS.GetText(1, 1);
            String sql27 =

                    " select count(a.edoracceptno) m from lpgrpedoritem  a,lpedorapp c " +
                    " where c.edorstate='0'   and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql27);
            strArr1[11] = kSSRS.GetText(1, 1);
            String sql28 =

                    "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem a,lpedorapp c " +
                    " where a.getmoney>0   and c.edorstate='0'   and  a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql28);
            strArr1[12] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 1)), "0.00"));
            String sql29 =

                    "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lpedorapp c " +
                    " where  a.getmoney<0   and c.edorstate='0'  and a.edoracceptno=c.edoracceptno " +
                    " and c.modifydate between '" + StartDate + "' and '" +
                    EndDate + "'" +
                    " and  (select managecom from lbgrpcont  g where  g.grpcontno=a.grpcontno union select managecom from lcgrpcont  g where  g.grpcontno=a.grpcontno) ='" +
                    tSSRS.GetText(i, 1) + "'  " +
                    " with ur";

            kSSRS = tExeSQL.execSQL(sql29);
            strArr1[13] = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                    kSSRS.GetText(1, 1)), "0.00"));

            klistTable.add(strArr1);
        }

        String[] strArr1Head = new String[15];
        strArr1Head[0] = "序号";
        strArr1Head[1] = "管理机构";
        strArr1Head[2] = "犹豫期退保件数";
        strArr1Head[3] = "犹豫期退保金额";
        strArr1Head[4] = "受益人性别";
        strArr1Head[5] = "证件类型";
        strArr1Head[6] = "证件号码";
        strArr1Head[7] = "与被保人关系";
        strArr1Head[8] = "受益顺序";
        strArr1Head[9] = "受益份额";
        xmlexport.addListTable(klistTable, strArr1Head);
/*
        String sql39 =
                "select sum(m),round(sum(n),2) from (" +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where edortype='WT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b,lpedorapp c " +
                " where edortype='WT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
         String sql39 =" select count(edoracceptno) m,round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where edortype='WT' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        tSSRS = tExeSQL.execSQL(sql39);
        System.out.println(sql39);
        String WTGCount = tSSRS.GetText(1, 1);
        String WTGMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                tSSRS.GetText(1, 2)), "0.00"));
        texttag.add("WTGCount", WTGCount);
        texttag.add("WTGMoney", WTGMoney);
/*
        String sql40 =
                "select sum(m),round(sum(n),2) from (" +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where edortype='CT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "' " +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b,lpedorapp c " +
                " where edortype='CT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql40 =" select count(edoracceptno) m,round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where edortype='CT' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql40);
        System.out.println(sql40);
        String CTGCount = kSSRS.GetText(1, 1);
        String CTGMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                kSSRS.GetText(1, 2)), "0.00"));
        texttag.add("CTGCount", CTGCount);
        texttag.add("CTGMoney", CTGMoney);
/*
        String sql41 =
                "select sum(m),round(sum(n),2) from (" +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b ,lpedorapp c " +
                " where a.edortype='NI'  and c.edorstate='0' and a.edoracceptno = c.edoracceptno and a.grpcontno=b.grpcontno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b ,lpedorapp c" +
                " where a.edortype='NI'  and c.edorstate='0' and a.edoracceptno = c.edoracceptno and a.grpcontno=b.grpcontno" +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql41 =" select count(edoracceptno) m,round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where edortype='NI' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql41);
        System.out.println(sql41);
        String NIGCount = kSSRS.GetText(1, 1);
        String NIGMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                kSSRS.GetText(1, 2)), "0.00"));
        texttag.add("NIGCount", NIGCount);
        texttag.add("NIGMoney", NIGMoney);
/*
        String sql42 =
                "select sum(m),round(sum(n),2) from (" +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where edortype='ZT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m,nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b,lpedorapp c " +
                " where edortype='ZT'  and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql42 = " select count(edoracceptno) m,round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where edortype='ZT' and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql42);
        System.out.println(sql42);
        String ZTGCount = kSSRS.GetText(1, 1);
        String ZTGMoney = String.valueOf(PubFun.setPrecision(Double.parseDouble(
                kSSRS.GetText(1, 2)), "0.00"));
        texttag.add("ZTGCount", ZTGCount);
        texttag.add("ZTGMoney", ZTGMoney);
/*
        String sql43 =
                "select sum(m) from (" +
                " select count(a.edoracceptno) m from lpgrpedoritem  a,lcgrpcont b,lpedorapp c  " +
                " where  edortype not in ('WT','CT','NI','ZT')  and c.edorstate='0' and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'  and a.grpcontno=b.grpcontno " +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m from lpgrpedoritem  a,lbgrpcont b ,lpedorapp c " +
                " where  edortype not in ('WT','CT','NI','ZT')  and c.edorstate='0' and a.edoracceptno=c.edoracceptno  " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'  and a.grpcontno=b.grpcontno" +
                managecom + ") k with ur";
*/
				String sql43 =" select count(edoracceptno) m from lpGRPedoritem a "
                + " where edortype not in ('WT','CT','NI','ZT') and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql43);
        String OGCount = kSSRS.GetText(1, 1);
        texttag.add("OGCount", OGCount);
/*
        String sql44 =
                "select sum(m) from (" +
                " select count(a.edoracceptno) m from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where  c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno  " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom +
                " union all " +
                " select count(a.edoracceptno) m from lpgrpedoritem a,lbgrpcont b,lpedorapp c " +
                " where  c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno  " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql44 =" select count(edoracceptno) m from lpGRPedoritem a "
                + " where  exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql44);
        String AllGCount = kSSRS.GetText(1, 1);
        texttag.add("AllGCount", AllGCount);
/*
        String sql45 =
                "select round(sum(n),2) from (" +
                "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where  a.getmoney>0   and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom +
                " union all " +
                "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b,lpedorapp c " +
                " where  a.getmoney>0   and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql45 =" select round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where  a.getmoney>0 and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql45);
        String AllGetGMoney = String.valueOf(PubFun.setPrecision(Double.
                parseDouble(kSSRS.GetText(1, 1)), "0.00"));
        texttag.add("AllGetGMoney", AllGetGMoney);
/*
        String sql46 =
                "select round(sum(n),2) from (" +
                "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lcgrpcont b,lpedorapp c " +
                " where  a.getmoney<0   and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "' " +
                managecom +
                " union all " +
                "select nvl(abs(sum(a.getmoney)),0) n from lpgrpedoritem  a,lbgrpcont b,lpedorapp c " +
                " where  a.getmoney<0   and c.edorstate='0'  and a.grpcontno=b.grpcontno and a.edoracceptno=c.edoracceptno " +
                " and c.modifydate between '" + StartDate + "' and '" + EndDate +
                "'" +
                managecom + ") k with ur";
*/
				String sql46 =" select round(nvl(abs(sum(a.getmoney)),0),2) n from lpGRPedoritem a "
                + " where  a.getmoney<0 and exists (select 1 from lpedorapp c where c.edoracceptno = a.edoracceptno "
                +
                " and c.edorstate = '0' and c.ModifyDate between '" +
                StartDate + "' and '" + EndDate + "')"
                +
                " and exists (select 1 from lbGRPcont b where b.GRPcontno=a.GRPcontno " +
                managecom
                + " union "
                + " select 1 from lcGRPcont b where  b.GRPcontno=a.GRPcontno " +
                managecom
                + ")with ur"
                ;
        kSSRS = tExeSQL.execSQL(sql46);
        String AllPayGMoney = String.valueOf(PubFun.setPrecision(Double.
                parseDouble(kSSRS.GetText(1, 1)), "0.00"));
        texttag.add("AllPayGMoney", AllPayGMoney);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        xmlexport.outputDocumentToFile("d:\\", "TaskPrint"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

    }

}
