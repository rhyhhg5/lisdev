package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProblemAmountListPrintBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public ProblemAmountListPrintBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
      if( !cOperate.equals("PRINT")) {
        buildError("submitData", "不支持的操作字符串");
        return null;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

      if(cOperate.equals("PRINT")){
      // 准备所有要打印的数据
      if( !PgetPrintData() ) {
        return null;
      }}
      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  ProblemAmountListPrintBL tbl =new ProblemAmountListPrintBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-08");
      tTransferData.setNameAndValue("EndDate", "2010-10-08");
      tTransferData.setNameAndValue("ManageCom", "8644");
      tGlobalInput.ManageCom="8644";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"PRINT");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
    mStartDate = (String) mTransferData.getValueByName("StartDate");
    mEndDate = (String) mTransferData.getValueByName("EndDate");
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null ) {
      buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean PgetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"印刷号", "保单号", "管理机构", "销售渠道", "业务员代码",
                           "投保人", "投保人性别", "投保人生日","被保人","被保人性别","被保人生日", "保费","保额 ","生效日期","保单状态","扫描回销状态"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }


      //获得EXCEL列信息      
      String tSQL="select lc.prtno,lc.contno,lc.managecom,lc.salechnl,lc.agentcode ,lc.appntname,db2inst1.codename('sex',lc.appntsex),lc.appntbirthday,"
      		+ " lc.insuredname,db2inst1.codename('sex',lc.insuredsex),lc.insuredbirthday,lc.prem,lc.amnt,lc.cvalidate,db2inst1.codename('stateflag', lc.stateflag),"
      		+ " case when (select 1 from es_doc_main where doccode = lc.prtno and subtype ='TB32' and state = '06')= 1 then '已出单已扫描已回销' "
      		+ " when (select 1 from es_doc_main where doccode = lc.prtno and subtype ='TB32' )= 1 then '已出单已扫描未回销' else   '已出单未扫描' end "
      		+ " from lccont lc where lc.conttype = 1  and lc.prem > 200000 and  lc.cardflag in('9','a','c','d','e','f') "
      		+ " and not exists (select 1 from lcrnewstatelog n where n.contno =lc.contno and state='6') "
      		+ " and not exists (select 1 from lcrnewstatelog n where n.newcontno =lc.contno and state='6')"
      		+" and lc.managecom like '"+mManageCom+"%' and lc.PolApplyDate between '"+mStartDate+"' and '"+mEndDate+"' " 
      		+ " union all "
      		+ " select lc.prtno,lc.contno,lc.managecom,lc.salechnl,lc.agentcode,lc.appntname,db2inst1.codename('sex',lc.appntsex),"
      		+ " lc.appntbirthday,lc.insuredname,db2inst1.codename('sex',lc.insuredsex),lc.insuredbirthday,lc.prem,lc.amnt,"
      		+ " lc.cvalidate,db2inst1.codename('stateflag', lc.stateflag), "
      		+ " case when (select 1 from es_doc_main where doccode = lc.prtno and subtype ='TB32' and state = '06')= 1 then '已出单已扫描已回销' "
      		+ " when (select 1 from es_doc_main where doccode = lc.prtno and subtype ='TB32' )= 1 then '已出单已扫描未回销' else   '已出单未扫描' end "
      		+ " from lbcont lc where lc.conttype = 1  and lc.prem > 200000 and  lc.cardflag in('9','a','c','d','e','f') "
      		+ " and not exists (select 1 from lcrnewstatelog n where n.contno =lc.contno and state='6') and "
      		+ " not exists (select 1 from lcrnewstatelog n where n.newcontno =lc.contno and state='6') and stateflag = '3' "
      		+ " and lc.managecom like '"+mManageCom+"%' and lc.PolApplyDate between '"+mStartDate+"' and '"+mEndDate+"'"
      		;
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }
  
}

