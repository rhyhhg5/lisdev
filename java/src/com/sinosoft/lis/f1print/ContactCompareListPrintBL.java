package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ContactCompareListPrintBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom=""; 
  private String mContType=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public ContactCompareListPrintBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
      if( !cOperate.equals("PRINT")&&!cOperate.equals("GRPPRINT") ) {
        buildError("submitData", "不支持的操作字符串");
        return null;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

      if(cOperate.equals("PRINT")){
      // 准备所有要打印的数据
      if( !PgetPrintData() ) {
        return null;
      }}
      if(cOperate.equals("GRPPRINT")){
          // 准备所有要打印的数据
          if( !GgetPrintData() ) {
            return null;
          }}

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  ContactCompareListPrintBL tbl =new ContactCompareListPrintBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-10-08");
      tTransferData.setNameAndValue("EndDate", "2010-10-08");
      tTransferData.setNameAndValue("ManageCom", "8644");
      tGlobalInput.ManageCom="8644";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"PRINT");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
    mStartDate = (String) mTransferData.getValueByName("StartDate");
    mEndDate = (String) mTransferData.getValueByName("EndDate");
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    mContType = (String) mTransferData.getValueByName("ContType");
    if( mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null ) {
      buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean PgetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "机构名称", "销售渠道", "网点", "业务员姓名",
                           "业务员代码", "保单号", "投保单号","归档号", "投保人","保费 ","投保人联系电话","投保人手机","业务员电话","问题说明"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      String wherePar=null;
      if(mContType.equals("1"))
      {
    	  wherePar=" and a.SaleChnl not in ('04','13') ";
      }
      else if(mContType.equals("3"))
      {
    	  wherePar=" and a.SaleChnl  in ('04','13') ";
      }
      //获得EXCEL列信息      
      String tSQL="select a.managecom ,(select name from ldcom where comcode=a.managecom )," +  //0 1
      		"(select codealias from ldcode1 where codetype ='salechnl' and code=a.salechnl)," + //2
      		"(select name from lacom where agentcom = a.agentcom)," +                           //3
      		"(select b.Name from laagent b where b.AgentCode=a.AgentCode)," +                   //4
      		"getUniteCode(a.AgentCode),a.contno,a.prtno," +   //5 6 7
      		"(select archiveno from es_doc_main where doccode=a.prtno fetch first 1 rows only)," +  //8
      		"a.appntname,a.prem," +   //9 10 
      		"(select c.Phone from lcaddress c where c.CustomerNo=a.AppntNo  and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno) fetch first 1 row only)," + //11
      		"(select c.Mobile from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno) fetch first 1 row only)," + //12
      		"(select phone from laagent where agentcode=a.agentcode),'',a.appntno," + //13 14 15
      		"(select idno from laagent where agentcode=a.agentcode)," + //16
      		" a.appntidno" + //17
      		"  from lccont a where conttype='1' and appflag='1' and stateflag='1' " +
      		" and managecom like '"+mManageCom+"%' and signdate between '"+mStartDate+"' and '"+mEndDate+"' " +
      		wherePar +
      		//" and contno='002871331000001'"+
      		" with ur ";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      

      ArrayList tArrayList = new ArrayList();
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
		
      for (int j = 0; j < mExcelData.length;j++) {
     	  LCContactSchema tLCContactSchema=new LCContactSchema();
     	  LCContactDB tLCContactDB=new LCContactDB();
     	  LCContactSet tLCContactSet=new LCContactSet();

     	  if(mExcelData[j][4].equals("")||mExcelData[j][4]==null){
     		  System.out.println("保单号"+mExcelData[j][6]+"无业务员姓名信息");
     	  }else{
     		  LCContactSet aLCContactSet=new LCContactSet();
     		//判断是否为同一人的依据由身份证号改为姓名，若姓名相同则视为同一人，直接过滤掉不判断电话号码；
            //在对比电话号码是否相同时，业务员之间不再进行对比，即不同的业务员允许有相同的电话号码，这种情况直接过滤掉不体现在清单中。 
     		//contacttype in ('03')  业务员
     		  //投保人的电话和业务员的姓名相同，电话不同的查出来
//     		  System.out.println("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and conttype='1' and contacttype in ('01') and  contactname <> trim("+mExcelData[j][4]+")  and (phone='"+mExcelData[j][13]+"' or mobile='"+mExcelData[j][13]+"') with ur");
     		 if(mExcelData[j][13].equals("")||mExcelData[j][13]==null)
     		 {
        		  System.out.println("保单号"+mExcelData[j][6]+"无业务员电话信息");
        	 }
     		 else
     		 {
        		  aLCContactSet=tLCContactDB.executeQuery("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and conttype='1' and contacttype in ('01') and  contactname <> trim('"+mExcelData[j][4]+"')  and (phone='"+mExcelData[j][13]+"' or mobile='"+mExcelData[j][13]+"') with ur");
         		  tLCContactSet.add(aLCContactSet);
     		  
        	  }
     		 
     	  }
     	  if(mExcelData[j][9].equals("")||mExcelData[j][9]==null){
     		  System.out.println("保单号"+mExcelData[j][6]+"无投保人姓名信息");
     	  }else{
     		  LCContactSet bLCContactSet=new LCContactSet();
            //投保人的电话和投保人的姓名相同，电话不同的查出来
     		 if((mExcelData[j][12].equals("")||mExcelData[j][12]==null)&&(mExcelData[j][11].equals("")||mExcelData[j][11]==null)){
        		  System.out.println("保单号"+mExcelData[j][6]+"无投保人手机或联系电话信息");
        	  }else{
//         		  System.out.println("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and conttype='1' and contacttype in ('01')  contactname <> trim ("+mExcelData[j][9]+") and ((phone='"+mExcelData[j][12]+"' or mobile='"+mExcelData[j][12]+"') or (phone='"+mExcelData[j][11]+"' or mobile='"+mExcelData[j][11]+"')) with ur");
         		  bLCContactSet=tLCContactDB.executeQuery("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and conttype='1' and contacttype in ('01') and  contactname <> trim ('"+mExcelData[j][9]+"') and ((phone='"+mExcelData[j][12]+"' or mobile='"+mExcelData[j][12]+"') or (phone='"+mExcelData[j][11]+"' or mobile='"+mExcelData[j][11]+"')) with ur");	  
         		  tLCContactSet.add(bLCContactSet);

        	  }

     	  }
     	 
     	  
     	  if(tLCContactSet.size()>0) {
      		tLCContactSchema=tLCContactSet.get(1);
      		String tContactType="";
      		if(tLCContactSchema.getContactType().equals("01"))
      		{
      			tContactType="投保人";
      		}
      		else
      		{
      			tContactType="业务员";
      	    }
      		 mExcelData[j][14]+="与"+tLCContactSchema.getContNo()+"保险合同的"+tContactType+tLCContactSchema.getContactName()+"的联系电话一致。";
  		}
    	  if(!mExcelData[j][14].equals("") && !mExcelData[j][14].equals("null") && mExcelData[j][14] != null)
    	  {
    		  tArrayList.add(mExcelData[j]);
    	  }

      }


      String[][] tExcelData=new String[tArrayList.size()][]; 
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }
      if(mCreateExcelList.setData(tExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }

  private boolean GgetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "机构名称", "销售渠道", "网点 ", "业务员姓名",
                           "业务员代码", "保单号", "投保单号","归档号", "投保人","保费 ","投保人联系电话","投保人手机","业务员电话","问题说明"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select a.managecom ,(select name from ldcom where comcode=a.managecom ) ," +
      		"(select codealias from ldcode1 where codetype ='salechnl' and code=a.salechnl)," +
      		"(select name from lacom where agentcom = a.agentcom) ," +
      		"(select b.Name from laagent b where b.AgentCode=a.AgentCode) ,getUniteCode(a.AgentCode) ,a.grpcontno," +
      		"a.prtno,(select archiveno from es_doc_main where doccode=a.prtno fetch first 1 rows only)," +
      		"a.grpname ,a.prem ," +
      		"(select c.phone1 from lcgrpaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcgrpappnt where grpcontno=a.grpcontno) fetch first 1 row only) ," +
      		"(select c.mobile1 from lcgrpaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcgrpappnt where grpcontno=a.grpcontno) fetch first 1 row only)," +
      		"(select phone from laagent where agentcode=a.agentcode),'',a.appntno  from lcgrpcont a where appflag='1' and stateflag='1' " +
      		" and managecom like '"+mManageCom+"%' and signdate between '"+mStartDate+"' and '"+mEndDate+"' with ur ";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
          
      ArrayList tArrayList = new ArrayList();
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      for (int j = 0; j < mExcelData.length;j++) {
    	  LCContactSchema tLCContactSchema=new LCContactSchema();
    	  LCContactDB tLCContactDB=new LCContactDB();
    	  LCContactSet tLCContactSet=new LCContactSet();
    	  if(mExcelData[j][11].equals("")||mExcelData[j][11]==null){
    		  System.out.println("保单号"+mExcelData[j][6]+"无投保人联系电话信息");
    	  }else{
    		  LCContactSet aLCContactSet=new LCContactSet();
    		  aLCContactSet=tLCContactDB.executeQuery("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and contactno<>'"+mExcelData[j][5]+"' and contactno<>'"+mExcelData[j][15]+"'   and conttype='2' and contacttype in ('04') and (phone='"+mExcelData[j][11]+"' or mobile='"+mExcelData[j][11]+"') with ur");
    		  tLCContactSet.add(aLCContactSet);
    	  }
    	  if(mExcelData[j][12].equals("")||mExcelData[j][12]==null){
    		  System.out.println("保单号"+mExcelData[j][6]+"无投保人手机信息");
    	  }else{
    		  LCContactSet bLCContactSet=new LCContactSet();
    		  bLCContactSet=tLCContactDB.executeQuery("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and contactno<>'"+mExcelData[j][5]+"' and contactno<>'"+mExcelData[j][15]+"'   and conttype='2' and contacttype in ('04') and (phone='"+mExcelData[j][12]+"' or mobile='"+mExcelData[j][12]+"') with ur");
    		  tLCContactSet.add(bLCContactSet);
    	  }
    	  if(mExcelData[j][13].equals("")||mExcelData[j][13]==null){
    		  System.out.println("保单号"+mExcelData[j][6]+"无业务员电话信息");
    	  }else{
    		  LCContactSet cLCContactSet=new LCContactSet();
    		  cLCContactSet=tLCContactDB.executeQuery("select * from lccontact where contno<>'"+mExcelData[j][6]+"' and contactno<>'"+mExcelData[j][5]+"' and contactno<>'"+mExcelData[j][15]+"'   and conttype='2' and contacttype in ('04') and (phone='"+mExcelData[j][13]+"' or mobile='"+mExcelData[j][13]+"') with ur");
    		  tLCContactSet.add(cLCContactSet);    		  
    	  }

    	  if( tLCContactSet.size()>0) {
      		tLCContactSchema=tLCContactSet.get(1);
      		String tContactType="";
      		if(tLCContactSchema.getContactType().equals("04"))
      		{tContactType="投保联系人";}
      		else
      		{tContactType="业务员";}
      		 mExcelData[j][14]+="与"+tLCContactSchema.getContNo()+"保险合同的"+tContactType+tLCContactSchema.getContactName()+"的联系电话一致。";
  		}
//     	  by gzh 增加可疑清单碰对改造
     	  String tSQL1 = "select lgad.LinkMan1,lgad.Phone1 from LCGrpAddress lgad inner join LCGrpAppnt lgap on lgad.customerno = lgap.customerno and lgad.addressno = lgap.addressno where lgap.grpcontno = '"+mExcelData[j][6]+"' ";
     	  SSRS tSSRS1 = new ExeSQL().execSQL(tSQL1);
     	  if(tSSRS1 != null && tSSRS1.MaxRow == 1){
     		  String tLinkMan1 = tSSRS1.GetText(1, 1);
     		  String tPhone1 = tSSRS1.GetText(1, 2);
     		  String tSQL2 = "select 1 from laagent where name = '"+tLinkMan1+"' and managecom like '8644%' ";
     		  String tLFlag = new ExeSQL().getOneValue(tSQL2);
     		  if("1".equals(tLFlag)){
     			 mExcelData[j][14]+="联系人姓名与业务员"+tLinkMan1+"的姓名一致。";
     		  }
     		  tSQL2 = "select name from laagent where  phone = '"+tPhone1+"' or mobile = '"+tPhone1+"' and managecom like '8644%' ";
    		  String tName = new ExeSQL().getOneValue(tSQL2);
    		  if(!"".equals(tName)){
    			 mExcelData[j][14]+="联系电话与业务员"+tName+"的电话一致。";
    		  }
    		  tSQL2 = "select 1 from laagenttemp where name = '"+tLinkMan1+"' and managecom like '8644%' ";
     		  tLFlag = new ExeSQL().getOneValue(tSQL2);
     		  if("1".equals(tLFlag)){
     			 mExcelData[j][14]+="联系人姓名与中介机构业务员"+tLinkMan1+"的姓名一致。";
     		  }
     		  tSQL2 = "select name from laagenttemp where  phone = '"+tPhone1+"' or mobile = '"+tPhone1+"' and managecom like '8644%' ";
     		 tName = new ExeSQL().getOneValue(tSQL2);
    		  if(!"".equals(tName)){
    			 mExcelData[j][14]+="联系电话与中介机构业务员"+tName+"的电话一致。";
    		  }
     	  }
     	  
    	  if(!mExcelData[j][14].equals("") && !mExcelData[j][14].equals("null") && mExcelData[j][14] != null)
    	  {
    		  tArrayList.add(mExcelData[j]);
    	  }
      }
      String[][] tExcelData=new String[tArrayList.size()][];  
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }

      if(mCreateExcelList.setData(tExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }
  
  /**
   * 得到通过传入参数得到相关信息
   * @param strComCode
   * @return
   * @throws Exception
   */
  private boolean getExcelData()
  {

	  String tSQL="select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'" +
	  		" and managecom like '' and signdate between '' and '' with ur ";
	  ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
          CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
          mErrors.addOneError(tError);
          return false;
      }
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
    	  return false;
      }
      return true;
  }
  
  
}

