package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAReContinueBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mFlag = null;
    private String mOutXmlPath = null;
    //private String mtype = null;


    SSRS tSSRS=new SSRS();
    public LAReContinueBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAReContinueBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        if(mFlag.equals("Cont")){
        	mToExcel[0][0] = "按保单查询下载";
        	mToExcel[1][0] = "保单号码";
            mToExcel[1][1] = "投保人姓名";
            mToExcel[1][2] = "生效日期";
            mToExcel[1][3] = "签单代理人编码";        
            mToExcel[1][4] = "签单代理人姓名";
            mToExcel[1][5] = "签单代理人离职日期";
            mToExcel[1][6] = "管理机构";
            mToExcel[1][7] = "销售单位代码";
            mToExcel[1][8] = "投保人手机";
            mToExcel[1][9] = "联系地址";
            mToExcel[1][10] = "联系电话";
            mToExcel[1][11] = "投保人性别";
            mToExcel[1][12] = "整单保费";
            mToExcel[1][13] = "保单状态";
            mToExcel[1][14] = "保单属性";
            mToExcel[1][15] = "缴费年期";
            
        }
        else {
        mToExcel[0][0] = "按险种查询下载";	
        mToExcel[1][0] = "保单号码";
        mToExcel[1][1] = "生效日期";
        mToExcel[1][2] = "签单代理人编码";
        mToExcel[1][3] = "签单代理人姓名";
        mToExcel[1][4] = "签单代理人离职日期"; 
        mToExcel[1][5] = "管理机构";
        mToExcel[1][6] = "销售单位代码";
        mToExcel[1][7] = "投保人姓名";
        mToExcel[1][8] = "投保人手机";
        mToExcel[1][9] = "联系地址";
        mToExcel[1][10] = "联系电话";
        mToExcel[1][11] = "被保人姓名";
        mToExcel[1][12] = "险种编码";
        mToExcel[1][13] = "险种名称";
        mToExcel[1][14] = "险种保费";
        mToExcel[1][15] = "保单状态";
        mToExcel[1][16] = "保单属性";
        mToExcel[1][17] = "缴费年期";
          
        }
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAReContinueBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mFlag = (String) tf.getValueByName("Flag");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAReContinueBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

  

    /**
    * 执行SQL文查询结果
    * @param sql String
    * @return double
    */
   private double execQuery(String sql)

   {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
     }

    public static void main(String[] args)
    {
        LAReContinueBL LAReContinueBL = new
            LAReContinueBL();
    System.out.println("11111111");
    }
}
