package com.sinosoft.lis.f1print;

import java.io.File;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMDutyGetSet;
import com.sinosoft.utility.*;

public class GrpPersonBL
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //取得的时间

//  private String mOpt = "";           //接收收费的类型：暂收OR预收
    private String mChargeType = ""; //将收费类型转换成汉字的显示
    private GlobalInput mGlobalInput = new GlobalInput(); // 全局数据
    private String mIP = "";
    private String mConfigFile = "";

    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

    public GrpPersonBL()
    {
    }

    public static void main(String[] args)
    {
        GrpPersonBL grpPersonBL1 = new GrpPersonBL();
        GlobalInput tGI = new GlobalInput();
        String tIP = "10.0.16.7";
        System.out.println(tIP);
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema.setGrpPolNo("86110020040220000321");
        VData tVData = new VData();
        tVData.addElement(tLCGrpPolSchema);
        tVData.addElement(tGI);
        tVData.addElement(tIP);
        tVData.addElement("configfile");
        grpPersonBL1.submitData(tVData, "PRTGRPPER");
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRTGRPPER"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        /**************************************************************************
         * date:2003-05-28
         * author:lys
         * function:判断收费的标志，若是Pay则是“暂收”、若是YSPay则是“预收”
         */
        // 准备所有要打印的数据
        if (cOperate.equals("PRTGRPPER"))
        { //打印付费
            if (!getPrintDataGrpPer())
            {
                return false;
            }
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpPersonBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData)
    {
        mLCGrpPolSchema.setSchema((LCGrpPolSchema) cInputData.
                                  getObjectByObjectName("LCGrpPolSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mIP = (String) cInputData.getObjectByObjectName("String", 0);
        this.mConfigFile = (String) cInputData.get(3);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        System.out.println("BL.IP===" + this.mIP);
        return true;
    }

    private boolean getPrintDataGrpPer()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());
        if (!tLCGrpPolDB.getInfo())
        {
            buildError("getPrintDataGrpPer", "无此集体保单号！");
        }
        mLCGrpPolSchema.setSchema(tLCGrpPolDB.getSchema());
        String FileVts = "GrpPer" + this.mLCGrpPolSchema.getRiskCode();
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        String tSql = "select * from lcpol where grppolno='" +
                      this.mLCGrpPolSchema.getGrpPolNo() +
                      "' order by proposalno";
        tLCPolSet = tLCPolDB.executeQuery(tSql);
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(this.mLCGrpPolSchema.getRiskCode());
        tLMRiskDB.getInfo();
        LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
        LMDutyGetSet tLMDutyGetSet = tLMDutyGetDB.query();
        String RiskName = tLMRiskDB.getRiskName();
        mIP = mIP + ".0"; //普通模板
        System.out.println("mIP===" + mIP);
//        ConfigInfo tConfigInfo = new ConfigInfo(this.mConfigFile);
        System.out.println("class.configfile:" + ConfigInfo.GetConfigPath());
        String pathfull = ConfigInfo.GetValuebyArea(mIP);
        int intIndex = 0;
        int intPosition = 0;
        String strRecords = "";
        String servername = "";
        String pathname = "";
        String printer = "";
        String printname = ""; //单据对应的打印机名
        if ((pathfull.length() > 0))
        {
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition,
                                      1);
            servername = pathfull.substring(intPosition, intIndex);
            pathfull = pathfull.substring(intIndex + 1);
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition,
                                      1);
            pathname = pathfull.substring(intPosition, intIndex);
            System.out.println("pathname:" + pathname);
//      UserLog.println("pathname:"+pathname);
            pathfull = pathfull.substring(intIndex + 1);
            printer = pathfull;
//      UserLog.println("printername:"+printer);
        }
        else
        {
            buildError("getPrintDataGrpPer", "IP解析错误！");
            return false;
        }
        System.out.println("printer====" + printer);
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            /*Lis5.3 upgrade set
                   tLCInsuredDB.setCustomerNo(tLCPolSchema.getInsuredNo());
                   tLCInsuredDB.setPolNo(tLCPolSchema.getPolNo());
             */
            String InsuredId = "";
            if (tLCInsuredDB.getInfo())
            {
                /*Lis5.3 upgrade get
                         if (tLCInsuredDB.getIDType().equals("0"))
                         {
                  InsuredId=tLCInsuredDB.getIDNo();
                         }
                 */
            }

            LCGetDB tLCGetDB = new LCGetDB();
            tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
            LCGetSet tLCGetSet = tLCGetDB.query();

            //得到养老金信息(最早)
            String YanglaoGetStartDate = "";
            String YanglaoGetMode = "";
            String YanglaoGetModeName = "";
            String tGetDutyCode = "";
            LCGetSet tYLLCGetSet = new LCGetSet();
            //加载保单所有的养老金
            for (int LCGetCount = 1; LCGetCount <= tLCGetSet.size(); LCGetCount++)
            {
                LCGetSchema tLCGetSchema = tLCGetSet.get(LCGetCount);
                for (int j = 1; j <= tLMDutyGetSet.size(); j++)
                {
                    LMDutyGetSchema tLMDutyGetSchema = tLMDutyGetSet.get(j);
                    if (tLMDutyGetSchema.getGetType2() == null)
                    {
                        continue;
                    }
                    if (!tLMDutyGetSchema.getGetType2().equals("1")) //非养老金
                    {
                        continue;
                    }
                    if (tLMDutyGetSchema.getGetDutyCode().equals(tLCGetSchema.
                            getGetDutyCode()))
                    {
                        tYLLCGetSet.add(tLCGetSchema);
                    }
                }
            }
            //选取最早领取的给付金
            int StartAge = 10000;
            for (int j = 1; j <= tYLLCGetSet.size(); j++)
            {
                LCGetSchema tLCGetSchema = tYLLCGetSet.get(j);
                //养老金的起领年龄
                int ThisStartAge = this.CalAge(tLCPolSchema.getInsuredBirthday(),
                                               tLCGetSchema.getGetStartDate());
                if (StartAge > ThisStartAge)
                {
                    //养老金起领日期
                    YanglaoGetStartDate = tLCGetSchema.getGetStartDate();
                    //养老的领取方式
                    YanglaoGetMode = tLCGetSchema.getGetDutyKind();
                    tGetDutyCode = tLCGetSchema.getGetDutyCode();
                    StartAge = ThisStartAge;
                }
            }
            if (YanglaoGetMode != null && YanglaoGetMode.length() != 0)
            {
                LMDutyGetAliveDB tLMDutyGetAliveDB = new LMDutyGetAliveDB();
                tLMDutyGetAliveDB.setGetDutyCode(tGetDutyCode);
                tLMDutyGetAliveDB.setGetDutyKind(YanglaoGetMode);
                if (tLMDutyGetAliveDB.getInfo())
                {
                    YanglaoGetModeName = tLMDutyGetAliveDB.getGetDutyName();
                }
            }

            //身故收益人
            String DeathBnfName = "";
            LCBnfDB tLCBnfDB = new LCBnfDB();
            tLCBnfDB.setPolNo(tLCPolSchema.getPolNo());
            tLCBnfDB.setBnfType("1"); //死亡收益人
            LCBnfSet tLCBnfSet = tLCBnfDB.query();
            for (int k = 1; k <= tLCBnfSet.size(); k++)
            {
                LCBnfSchema tLCBnfSchema = tLCBnfSet.get(k);
                if (k > 1)
                {
                    DeathBnfName = DeathBnfName + "/";
                }
                DeathBnfName = DeathBnfName + tLCBnfSchema.getName();
            }
            if (DeathBnfName.equals("") || DeathBnfName.length() == 0)
            {
                DeathBnfName = "法定";
            }
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(tLCPolSchema.getManageCom());
            tLDComDB.getInfo();
            String ManageComName = tLDComDB.getName();
            String tPhone = tLDComDB.getPhone();

            //生效日期
            String tCValidDate = Display_Year(tLCPolSchema.getCValiDate());
            System.out.println("生效日期：" + tCValidDate);
            //终止日期
            String tInsuredEndDate = this.Display_Year(PubFun.calDate(
                    tLCPolSchema.
                    getEndDate(), -1, "D", ""));
            System.out.println("终止日期：" + tInsuredEndDate);

            //个人账户金额
            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
            LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
            tLCInsureAccSet = tLCInsureAccDB.query();
            double tAccMoney = 0;
            for (int j = 1; j <= tLCInsureAccSet.size(); j++)
            {
                LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
                tLCInsureAccSchema = tLCInsureAccSet.get(j);
                if (tLCInsureAccSchema.getAccType().equals("003"))
                {
                    tAccMoney = tAccMoney + tLCInsureAccSchema.getInsuAccBala();
                }
            }

            xmlexport.createDocument(FileVts + ".vts", printer);
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
//    集体保单号
            texttag.add("GrpPolNo", this.mLCGrpPolSchema.getGrpPolNo());
//    集体名称
            texttag.add("GrpName", this.mLCGrpPolSchema.getGrpName());
            /*Lis5.3 upgrade get
//    单位地址
             texttag.add("GrpAddress", this.mLCGrpPolSchema.getGrpAddress());
//    单位邮编
             texttag.add("GrpZipCode", this.mLCGrpPolSchema.getGrpZipCode());
             */
//    集体保单印刷号
            texttag.add("GrpPrtNo", this.mLCGrpPolSchema.getPrtNo());
//    险种名称
            texttag.add("RiskName", RiskName);
//    集体投保单号
            texttag.add("GrpProposalNo", this.mLCGrpPolSchema.getGrpProposalNo());
//    个人保单号
            texttag.add("PolNo", tLCPolSchema.getPolNo());
//    被保人姓名
            texttag.add("InsuredName", tLCPolSchema.getInsuredName());
//    被保人投保年龄
            texttag.add("InsuredAppAge", tLCPolSchema.getInsuredAppAge());
//    被保人身份证号码
            texttag.add("InsuredIdlNo", InsuredId);
//    个单保费
            texttag.add("Prem", tLCPolSchema.getPrem());
//    养老金起领日期
            texttag.add("YLGetStartDate", YanglaoGetStartDate);
//    养老金领取方式
            texttag.add("YLGetMode", YanglaoGetModeName);
//    死亡受益人
            texttag.add("DeathBnfName", DeathBnfName);
//    备注
            texttag.add("Note", tLCPolSchema.getRemark());
//    保单管理机构
            texttag.add("ManageCom", ManageComName);
//    管理机构电话
            texttag.add("Phone", tPhone);
//    保单生效日期
            texttag.add("CValidDate", tCValidDate);
//    保险终止日期
            texttag.add("InsuredEndDate", tInsuredEndDate);
//    银行账号
            /*Lis5.3 upgrade get
                   texttag.add("BankAccNo", tLCPolSchema.getBankAccNo());
             */
//    保险金额
            texttag.add("Amnt", tLCPolSchema.getAmnt());
//    管理费比例
            texttag.add("ManageFeeRate", tLCPolSchema.getManageFeeRate());
//    个人账户余额
            texttag.add("InsuAccBala", tAccMoney);
//　　 临时增加退保金额1
            texttag.add("Prem23", tLCPolSchema.getPrem() * 1.06);
//    临时增加退保金额2
            texttag.add("Prem34", tLCPolSchema.getPrem() * 1.095);
//    临时增加退保金额3
            texttag.add("TB1", tLCPolSchema.getPrem() * 1.1);
//    临时增加退保金额4
            texttag.add("TB2", tLCPolSchema.getPrem() * 1.06);
//    临时增加退保金额5
            texttag.add("TB3", tLCPolSchema.getPrem() * 0.96);
//    临时增加退保金额6
            texttag.add("TB4", tLCPolSchema.getPrem() * 0.95);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }

            //写文件
//      String dirSys=System.getProperty("user.dir");
//      String dirXml = dirSys + File.separator + "print"+File.separator;


//      dirXml = dirXml + this.mGlobalInput.ManageCom+File.separator+servername+File.separator;
            File tFileXml = new File(pathname);
            System.out.println("Xml.pathname==" + pathname);
            if (!tFileXml.exists())
            {
                tFileXml.mkdirs();
            }
            String Xmlfilename = getFileName();
            xmlexport.outputDocumentToFile(pathname, Xmlfilename);
        }
        return true;
    }

    private int CalAge(String sDate, String eDate)
    {
        String tSql = "select get_age('" + sDate + "','" + eDate +
                      "') from dual";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = exesql.execSQL(tSql);
        int age = Integer.parseInt(ssrs.GetText(1, 1));
        return age;
    }

    private String Display_Year(String date)
    {
        if (!((date == null) || date.equals("")))
        {
            String t = "";
            String t1 = date.substring(0, 4);
            String t2;
            if (date.substring(5, 6).equals("0"))
            {
                t2 = date.substring(6, 7);
            }
            else
            {
                t2 = date.substring(5, 7);
            }
            String t3;
            if (date.substring(8, 9).equals("0"))
            {
                t3 = date.substring(9, 10);
            }
            else
            {
                t3 = date.substring(8, 10);
            }
            t = t1 + "年" + t2 + "月" + t3 + "日";
            System.out.println("转换后的日期是" + t);
            return t;
        }
        else
        {
            return "";
        }
    }

    private synchronized String getFileName()
    {
//    String tCt=PubFun.getCurrentTime();
//    tCt=tCt.replace(':','-');
//    String tTimeString=PubFun.getCurrentDate()+"-"+tCt;
//    String Xmlfilename="Prt"+tTimeString;
        Date date = new Date();
        String Xmlfilename = "Prt" + String.valueOf(date.getTime());

        return Xmlfilename;
    }
}
