/*
 * <p>ClassName: LCISPullReportUI </p>
 * <p>Description: LCISPullReportUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-08-22
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class NewLCISPullReportUI {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	public NewLCISPullReportUI() {
		System.out.println("ContAnalysisUI");
	}

	public boolean submitData(VData cInputData, String cOperator) {
		NewLCISPullReportBL bl = new NewLCISPullReportBL();
		if (!bl.submitData(cInputData, cOperator)) {
			mErrors.copyAllErrors(bl.mErrors);
			return false;
		}

		return true;
	}

}
