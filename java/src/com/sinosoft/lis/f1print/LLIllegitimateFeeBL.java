package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLIllegitimateFeeBL</p>
 * <p>Description: 不合理费用</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;
import com.sinosoft.lis.llcase.LLPrintSave;

public class LLIllegitimateFeeBL {
    public LLIllegitimateFeeBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageComName = "";
    private String mOperator = "";
    private String mContType = "";
    private TransferData mTransferData = new TransferData();

    private String[][] mShowDataList = null;
    private XmlExport mXmlExport = null;
    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();
    private String mFileNameB = "";


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        } else {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("tFileNameB", mFileNameB);
            tTransferData.setNameAndValue("tMakeDate", currentDate);
            tTransferData.setNameAndValue("tOperator", mOperator);
            LLPrintSave tLLPrintSave = new LLPrintSave();
            VData tVData = new VData();
            tVData.addElement(tTransferData);
            if (!tLLPrintSave.submitData(tVData, "")) {
                this.buildError("LLPrintSave-->submitData", "数据不完整");
                return false;
            }
        }

        return true;
    }


    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean getDataList() {
        String tSQL = "";
        DecimalFormat tDF = new DecimalFormat("0.##");
        DecimalFormat tDF1 = new DecimalFormat("0");

        String tPartSQL = "";

        if ("1".equals(mContType)) {
            tPartSQL = " and  exists (select 1 from llregister where rgtno=b.rgtno and (applyertype='0' or applyertype is null)) ";
        } else if ("2".equals(mContType)) {
            tPartSQL = " and exists (select 1 from llregister where rgtno=b.rgtno and applyertype='5') ";
        } else if ("3".equals(mContType)) {
            tPartSQL = " and exists (select 1 from llregister where rgtno=b.rgtno and applyertype='1') ";
        } else if ("4".equals(mContType)) {
            tPartSQL = " and exists (select 1 from llregister where rgtno=b.rgtno and applyertype='2') ";
        }

        // 1、得到全部已开业的机构
        if (mManageCom.length() == 2) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=4 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
        } else if (mManageCom.length() == 4) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))>4 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
        } else {
            tSQL =
                    "SELECT comcode,name FROM ldcom WHERE sign='1' AND comcode LIKE '" +
                    mManageCom + "%' ORDER BY comcode";
        }
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() <= 0) {
            buildError("queryData", "没有附和条件的机构信息！");
            return false;
        }

        // 2、查询需要表示的数据
        String tManageCom = "";
        String tComName = "";

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环机构进行统计

            String Info[] = new String[11];
            tManageCom = tSSRS.GetText(i, 1);
            tComName = tSSRS.GetText(i, 2);

            Info[0] = tComName;

            //不合理费用
            String tFee1SQL =
                    "select count(distinct a.caseno),coalesce(sum(a.refuseamnt),0) "
                    + " from llfeemain a,llcase b where a.caseno=b.caseno "
                    + " and b.rgttype='1' and a.feeatti='0' "
                    + " and b.rgtdate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and a.refuseamnt>0 and b.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("不合理费用：" + tFee1SQL);
            SSRS tFee1SSRS = tExeSQL.execSQL(tFee1SQL);
            Info[1] = tFee1SSRS.GetText(1, 1);
            Info[6] = tFee1SSRS.GetText(1, 2);

            //自费部分
            double tSelfAmnt = 0;
            double tSelfCase = 0;
            String tFee21SQL =
                    "select count(distinct a.caseno),coalesce(sum(a.selfamnt),0) "
                    + " from llfeemain a,llcase b where a.caseno=b.caseno "
                    + " and b.rgttype='1' and a.feeatti='0' "
                    + " and b.rgtdate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and a.selfamnt>0 and b.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("自费费用1：" + tFee21SQL);
            SSRS tFee21SSRS = tExeSQL.execSQL(tFee21SQL);

            tSelfCase += Double.parseDouble(tFee21SSRS.GetText(1, 1));
            tSelfAmnt += Double.parseDouble(tFee21SSRS.GetText(1, 2));

            String tFee22SQL =
                    "select count(distinct a.caseno),coalesce(sum(c.selfamnt),0) "
                    + " from llfeemain a,llcase b,llsecurityreceipt c where a.caseno=b.caseno "
                    + " and a.mainfeeno=c.mainfeeno and b.caseno=c.caseno"
                    + " and b.rgttype='1' and a.feeatti in ('1','2','4') "
                    + " and b.rgtdate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and c.selfamnt>0 and b.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("自费费用2：" + tFee22SQL);
            SSRS tFee22SSRS = tExeSQL.execSQL(tFee22SQL);

            tSelfCase += Double.parseDouble(tFee22SSRS.GetText(1, 1));
            tSelfAmnt += Double.parseDouble(tFee22SSRS.GetText(1, 2));

            Info[2] = tDF1.format(tSelfCase);
            Info[7] = tDF.format(tSelfAmnt);

            //部分自付
            double tPreaCase = 0;
            double tPreaAmnt = 0;
            String tFee31SQL =
                    "select count(distinct a.caseno),coalesce(sum(a.preamnt),0) "
                    + " from llfeemain a,llcase b where a.caseno=b.caseno "
                    + " and b.rgttype='1' and a.feeatti='0' "
                    + " and b.rgtdate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and a.preamnt>0 and b.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("部分自付费用1：" + tFee31SQL);
            SSRS tFee31SSRS = tExeSQL.execSQL(tFee31SQL);
            tPreaCase += Double.parseDouble(tFee31SSRS.GetText(1, 1));
            tPreaAmnt += Double.parseDouble(tFee31SSRS.GetText(1, 2));

            String tFee32SQL =
                    "select count(distinct a.caseno),coalesce(sum(c.selfpay2),0) "
                    + " from llfeemain a,llcase b,llsecurityreceipt c where a.caseno=b.caseno "
                    + " and a.mainfeeno=c.mainfeeno and c.caseno=b.caseno"
                    + " and b.rgttype='1' and a.feeatti in ('1','2','4') "
                    + " and b.rgtdate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and c.selfpay2>0 and b.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("部分自付费用1：" + tFee32SQL);
            SSRS tFee32SSRS = tExeSQL.execSQL(tFee32SQL);
            tPreaCase += Double.parseDouble(tFee32SSRS.GetText(1, 1));
            tPreaAmnt += Double.parseDouble(tFee32SSRS.GetText(1, 2));

            Info[3] = tDF1.format(tPreaCase);
            Info[8] = tDF.format(tPreaAmnt);

            //部分拒付
            String tGive2SQL =
                    "select count(distinct a.caseno),coalesce(sum(case when c.declineamnt =0 then c.outdutyamnt else c.declineamnt end),0) "
                    + " from llcase a,llclaim b,llclaimdetail c "
                    + " where a.caseno=b.caseno and a.caseno=c.caseno "
                    +
                    " and a.rgttype='1' and b.givetype='2' and a.rgtstate in ('09','11','12') "
                    + " and a.endcasedate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and a.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("部分给付：" + tGive2SQL);
            SSRS tGive2SSRS = tExeSQL.execSQL(tGive2SQL);

            Info[4] = tGive2SSRS.GetText(1, 1);
            Info[9] = tGive2SSRS.GetText(1, 2);

            //全额拒付
            String tGive3SQL =
                    "select count(distinct a.caseno),coalesce(sum(case when c.declineamnt =0 then c.outdutyamnt else c.declineamnt end),0) "
                    + " from llcase a,llclaim b,llclaimdetail c "
                    + " where a.caseno=b.caseno and a.caseno=c.caseno "
                    +
                    " and a.rgttype='1' and b.givetype='3' and a.rgtstate in ('09','11','12') "
                    + " and a.endcasedate between '" + mStartDate + "' and '" +
                    mEndDate + "' "
                    + " and a.mngcom like '" + tManageCom +
                    "%' " + tPartSQL + " with ur";

            System.out.println("全额拒付：" + tGive3SQL);
            SSRS tGive3SSRS = tExeSQL.execSQL(tGive3SQL);
            Info[5] = tGive3SSRS.GetText(1, 1);
            Info[10] = tGive3SSRS.GetText(1, 2);

            mListTable.add(Info);
        }

        //合计所有公司
//        String Info[] = new String[11];
//
//        Info[0] = "合计";
//        mListTable.add(Info);

        return true;
    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLIllegitimateFeeReport.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("Operator", mOperator);
        tTextTag.add("MakeDate", currentDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", "", "", ""};

        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData
                               .getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
                "TransferData", 0);
        // 页面传入的数据 三个
        mOperator = (String) mTransferData.getValueByName("tOperator");
        mContType = (String) mTransferData.getValueByName("tContType");
        mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
        this.mManageCom = (String) mTransferData
                          .getValueByName("tManageCom");
        mManageComName = ReportPubFun.getMngName(mManageCom);
        this.mStartDate = (String) mTransferData
                          .getValueByName("tStartDate");
        this.mEndDate = (String) mTransferData.getValueByName("tEndDate");

        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLBranchCaseReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
