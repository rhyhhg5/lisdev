package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft </p>
 * @author liuli
 * @version 1.0
 */
import java.io.*;
import java.net.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.ftp.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PDFPrintManagerBL {

    private GlobalInput mGlobalInput = new GlobalInput();
    /**打印管理表信息*/
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /**结果变量*/
    private VData mResult = new VData();
    /**生成的xml文件存放路径*/
    private String mStrUrl = null;
    /**生成的xml文件名称*/
    private String mFileName = null;

    private String mflag = null;
    
    private String mCode = "";
    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param flag "only"为单次打印标记 "batch"为批量打印标记
     * 08/11/17 修改：单打改为类似于批打，生成清单文件和数据文件，上传目的地也和批打一致。
     * @return
     */
    public boolean submitData(VData cInputData, String flag) {
    	
    	System.out.println("---PDFPrintManagerBL---start---");
    	
    	
        if (!flag.equals("only")&& !flag.equals("batch")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
         mflag = flag;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        
        System.out.println("---dealdata---start---");
        
        if (!dealData()) {
            return false;
        }
        //上传文件
        if(!sendZip(mStrUrl+mFileName+".xml",flag)){
            return false;
        }else{
            //上传成功后删除
            File cFile = new File(mStrUrl+mFileName+".xml");
            cFile.delete();//删除上传完的文件
        }
        // 保存数据
        if(!saveData(flag)){
            return false;
        }
        return true;
     }

    /**
     * saveData
     * 获取服务类生成的打印管理表数据。
     * 判断打印管理表该条打印信息是否存在。已存在，则修改打印次数;不存在，则插入新数据
     * @return boolean
     */
     private boolean saveData(String flag) {
        String toperate = null;
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema = (LOPRTManagerSchema) mResult.getObjectByObjectName(
                    "LOPRTManagerSchema", 0);
        //打印状态标记：如没有值（NULL）或为未打印状态（0），置为已打印（1）
        String stateFlag = tLOPRTManagerSchema.getStateFlag();

        LOPRTManagerDB tLOPRTManagerDB = new  LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(tLOPRTManagerSchema.getOtherNo());
        tLOPRTManagerDB.setOtherNoType(tLOPRTManagerSchema.getOtherNoType());
        tLOPRTManagerDB.setCode(tLOPRTManagerSchema.getCode());
        tLOPRTManagerDB.setOldPrtSeq(tLOPRTManagerSchema.getOldPrtSeq());
        tLOPRTManagerDB.setStandbyFlag1(tLOPRTManagerSchema.getStandbyFlag1());
        tLOPRTManagerDB.setStandbyFlag2(tLOPRTManagerSchema.getStandbyFlag2());
        tLOPRTManagerDB.setStandbyFlag3(tLOPRTManagerSchema.getStandbyFlag3());
        tLOPRTManagerDB.setStandbyFlag4(tLOPRTManagerSchema.getStandbyFlag4());

        StringBuffer condition = new StringBuffer();
        condition.append(" 1=1 ");
        if(tLOPRTManagerSchema.getOtherNo()!=null && !"null".equals(tLOPRTManagerSchema.getOtherNo()) && !"".equals(tLOPRTManagerSchema.getOtherNo())){
        	condition.append(" and otherno = '"+tLOPRTManagerSchema.getOtherNo()+"'");
        }
        if(tLOPRTManagerSchema.getOtherNoType()!=null && !"null".equals(tLOPRTManagerSchema.getOtherNoType()) && !"".equals(tLOPRTManagerSchema.getOtherNoType())){
        	condition.append(" and othernotype = '"+tLOPRTManagerSchema.getOtherNoType()+"'");
        }
        if(tLOPRTManagerSchema.getCode()!=null && !"null".equals(tLOPRTManagerSchema.getCode()) && !"".equals(tLOPRTManagerSchema.getCode())){
        	condition.append(" and code = '"+tLOPRTManagerSchema.getCode()+"'");
        }
        if(tLOPRTManagerSchema.getOldPrtSeq()!=null && !"null".equals(tLOPRTManagerSchema.getOldPrtSeq()) && !"".equals(tLOPRTManagerSchema.getOldPrtSeq())){
        	condition.append(" and oldprtseq = '"+tLOPRTManagerSchema.getOldPrtSeq()+"'");
        }
        if(tLOPRTManagerSchema.getStandbyFlag1()!=null && !"null".equals(tLOPRTManagerSchema.getStandbyFlag1()) && !"".equals(tLOPRTManagerSchema.getStandbyFlag1())){
        	condition.append(" and standbyflag1 = '"+tLOPRTManagerSchema.getStandbyFlag1()+"'");
        }
        if(tLOPRTManagerSchema.getStandbyFlag2()!=null && !"null".equals(tLOPRTManagerSchema.getStandbyFlag2()) && !"".equals(tLOPRTManagerSchema.getStandbyFlag2())){
        	condition.append(" and standbyflag2 = '"+tLOPRTManagerSchema.getStandbyFlag2()+"'");
        }
        if(tLOPRTManagerSchema.getStandbyFlag3()!=null && !"null".equals(tLOPRTManagerSchema.getStandbyFlag3()) && !"".equals(tLOPRTManagerSchema.getStandbyFlag3())){
        	condition.append(" and standbyflag3 = '"+tLOPRTManagerSchema.getStandbyFlag3()+"'");
        }
        if(tLOPRTManagerSchema.getStandbyFlag4()!=null && !"null".equals(tLOPRTManagerSchema.getStandbyFlag4()) && !"".equals(tLOPRTManagerSchema.getStandbyFlag4())){
        	condition.append(" and standbyflag4 = '"+tLOPRTManagerSchema.getStandbyFlag4()+"'");
        }
        String SQL = "select * from LOPRTManager where " +condition.toString();
        System.out.println(SQL);
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        tLOPRTManagerSet = tLOPRTManagerDB.executeQuery(SQL);
        if(tLOPRTManagerSet.size()==0){
            if(stateFlag == null || stateFlag.equals("0"))
                tLOPRTManagerSchema.setStateFlag("1");            
            tLOPRTManagerSchema.setPrintTimes(1);
            toperate = "INSERT";
        }else if(tLOPRTManagerSet.size()==1){
            tLOPRTManagerSchema.setPrtSeq(tLOPRTManagerSet.get(1).getPrtSeq());
            if(stateFlag == null || stateFlag.equals("0"))
                tLOPRTManagerSchema.setStateFlag("1");
            tLOPRTManagerSchema.setPrintTimes((tLOPRTManagerSet.get(1).getPrintTimes())+1);
            tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
            toperate = "UPDATE";
        }else{
            tLOPRTManagerSchema.setPrtSeq(tLOPRTManagerSet.get(1).getPrtSeq());
            if(stateFlag == null || stateFlag.equals("0"))
                tLOPRTManagerSchema.setStateFlag("1");
            tLOPRTManagerSchema.setPrintTimes((tLOPRTManagerSet.get(1).getPrintTimes())+1);
            tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
            toperate = "UPDATE";
            buildError("saveData","打印管理表信息出错，请维护！");
        }
        VData tVData = new VData();
        tVData.add(tLOPRTManagerSchema);
        PrintManagerBLS tPrintManagerBLS = new PrintManagerBLS();
        if (!tPrintManagerBLS.submitData(tVData, toperate)) {
            mErrors.copyAllErrors(tPrintManagerBLS.mErrors);
            buildError("saveData","更新打印管理表信息出错!正常打印！");
            return false;
        }
        return true;
     }

        /**
         * 向指定地址上传文件
         * @param cZipFilePath String
         * @return boolean
         */
        public boolean sendZip(String cZipFilePath,String flag)
        {
           //登入前置机
            String ip = "";
            String user = "";
            String pwd = "";
            int port = 21;//默认端口为21

            String sql = "select code,codename from LDCODE where codetype='printserver'";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if(tSSRS != null){
                for(int m=1;m<=tSSRS.getMaxRow();m++){
                    if(tSSRS.GetText(m,1).equals("IP")){
                        ip = tSSRS.GetText(m,2);
                    }else if(tSSRS.GetText(m,1).equals("UserName")){
                        user = tSSRS.GetText(m,2);
                    }else if(tSSRS.GetText(m,1).equals("Password")){
                        pwd = tSSRS.GetText(m,2);
                    }else if(tSSRS.GetText(m,1).equals("Port")){
                        port = Integer.parseInt(tSSRS.GetText(m,2));
                    }
                }
            }
            FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
            try
            {
                if (!tFTPTool.loginFTP())
                {
                    System.out.println(tFTPTool.getErrContent(1));
                    return false;
                }
                else
                {
//                    if(flag.equals("batch")){
                        //获取目的地指定文件夹名称
                        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrlFile'";//生成文件的存放路径
                        String fileStrUrl = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
                        if(fileStrUrl == null || fileStrUrl.equals("")){
                            buildError("sendZip","上传目的地指定文件夹为空，请指定文件夹！");
                            return false;
                        }
                        if("J024".equals(mCode)){
                        	fileStrUrl=".";
                        }
                        if(!tFTPTool.upload(fileStrUrl,cZipFilePath)){
                        CError tError = new CError();
                        tError.errorMessage = tFTPTool
                                 .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                        buildError("sendZip",tError.errorMessage);
                        System.out.println("上载文件失败!");
                        return false;
                       }
//                    }
//                    else if(flag.equals("only")){
//                        if (!tFTPTool.upload(cZipFilePath)) {
//                            CError tError = new CError();
//                            tError.errorMessage = tFTPTool
//                                                  .getErrContent(
//                                    FTPReplyCodeName.LANGUSGE_CHINESE);
//                            buildError("sendZip", tError.errorMessage);
//                            System.out.println("上载文件失败!");
//                            return false;
//                        }
//                    }
                    tFTPTool.logoutFTP();
                }
            }
            catch (SocketException ex)
            {
                ex.printStackTrace();
                CError tError = new CError();
                tError.errorMessage = tFTPTool
                         .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                 buildError("sendZip",tError.errorMessage);
                System.out.println("上载文件失败，可能是网络异常!");
                return false;
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                CError tError = new CError();
                tError.errorMessage = tFTPTool
                        .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                buildError("sendZip",tError.errorMessage);
                System.out.println("上载文件失败，可能是无法写入文件");
                return false;
            }

            return true;
    }
    /**
     * dealData
     * 生成打印数据
     * @return boolean
     */
    private boolean dealData() {

        if(!getFileUrlName()){
            return false;
        }

        //生成对应的单证打印XML数据
        if(!callPrintService(mLOPRTManagerSchema)){
            return false;
        }
        return true;
    }

    /**
     * getInputData
     * 从前台获取打印所需要数据，用户信息和单证信息
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                 getObjectByObjectName("LOPRTManagerSchema", 0));

        if (mGlobalInput == null || mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mCode = mLOPRTManagerSchema.getCode();
       if(mCode == null || mCode.equals("")){
           buildError("getInputData","请输入单证类型！");
           return false;
       }
      return true;
    }

    /**
     * getFileUrlName
     * 获取生成文件的存放地址，生成文件名字（当前日期＋流水号）
     * @return boolean
     */
    private boolean getFileUrlName() {
        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
        mStrUrl = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
//        mStrUrl="F:\\ZQT\\";
        System.out.println("生成文件的存放路径   "+mStrUrl);//调试用－－－－－
        if(mStrUrl == null ||mStrUrl.equals("")){
            buildError("getFileUrlName","获取文件存放路径出错");
            return false;
        }

        //生成 当前日期＋6位流水号的文件名
        String MaxNo = new SysMaxNoPicch().CreateMaxNo("PDFPRINT",PubFun.getCurrentDate2(),6);
        mFileName = PubFun.getCurrentDate2()+MaxNo;
        System.out.println("生成文件名："+mFileName);
        if(mFileName == null || mFileName.equals("")){
            buildError("getFileUrlName","生成文件名出错");
            return false;
        }
        return true;
    }

    /**
     * 根据Code类型调用对应的类，生成打印数据文件
     * @param aLOPRTManagerSchema
     * @return
     */
    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {
        // 查找对应的打印服务类
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";

        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);
        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务类
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);
        
        System.out.println("---打印编码---" + tLDCodeSchema.getCode());
        System.out.println("---打印处理类---" + tLDCodeSchema.getCodeAlias());
        
        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();
            VData vData = new VData();
            vData.add(mGlobalInput);
            vData.add(aLOPRTManagerSchema);         
            if (!ps.submitData(vData, mflag)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }
            mResult = ps.getResult();
            XmlExport txmlExport = (XmlExport) mResult.getObjectByObjectName(
                    "XmlExport", 0);
            System.out.println("txmlExport"+txmlExport);
            if (txmlExport == null) {
                System.out.println("没有所需的打印数据文件");
                XMLDatasets tXMLDatasets = (XMLDatasets) mResult.getObjectByObjectName("XMLDatasets",0);
                tXMLDatasets.outputCode(mStrUrl+mFileName+".xml","GB2312");
                System.out.println(mStrUrl+mFileName+".xml");
            } else {
//            	String mStrUrl = "E:\\";
                txmlExport.outputDocumentToFile(mStrUrl, mFileName);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            mErrors.addOneError("调用打印服务失败");
            buildError("callPrintService", ex.toString());
            return false;
        }
        return true;
    }

    //批打调用时要获取的本次单打的文件名
    public String getFileName(){
        String filename = mFileName+".xml";
        return filename;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PDFPrintManagerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main (String args[]){
        PDFPrintManagerBL xx = new PDFPrintManagerBL();
        xx.sendZip("E:/workspace/ui/printdata/data/brief/20080602000003.txt","batch");
        xx.sendZip("E:/workspace/ui/printdata/data/brief/20080602000002.txt","only");
    	
    	
    	
    }

}
