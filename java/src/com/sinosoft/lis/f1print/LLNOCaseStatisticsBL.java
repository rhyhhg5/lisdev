package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLNOCaseStatisticsBL
{
	 public LLNOCaseStatisticsBL() 
{
	        try {
	            jbInit();
	        } 
	        catch (Exception ex) 
	        {
	            ex.printStackTrace();
	        } 
}

public CErrors mErrors = new CErrors();
private VData mResult = new VData();
private GlobalInput mGlobalInput = new GlobalInput();
private String mManageCom; //机构号码
private String mRgtState;//案件状态
private String mUserCode;//处理人
private String mUpUserCode;  //处理人上级
private String mPNo;  //批次号
private String mCaseNo;  //理赔号
private String mZJNo;  //证件号码
private String mStartDateNo;  //时效天数起期
private String mEndDateNo;    //时效天数止期

private DecimalFormat mDF = new DecimalFormat("0.00");
private XmlExport mXmlExport = null;
private ListTable mListTable = new ListTable();
private String    mCurrentDate = PubFun.getCurrentDate();
private String    mFileNameB="";
private TransferData mTransferData = new TransferData();
private CSVFileWrite mCSVFileWrite = null;
private String mFilePath = "";
private String mFileName = "";
//传输数据公共方法
 public boolean submitData(VData cInputData, String cOperate) 
{
    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) 
    {
        return false;
    }

// 进行数据查询
if (!queryDataToCVS())
{
    return false;
}
        return true;
}
 
/*
 * 统计案件结果
 */
private boolean getDataList(int datetype)
{

  if (mManageCom == null || mManageCom.equals("")) {
	  buildError("getDataList", "机构编码缺失！");
	  return false;
  } 
  try{
	 ExeSQL ttExeSQL = new ExeSQL();
	 System.out.println("mUserCode:"+mRgtState+mUserCode+mUpUserCode+mPNo+mCaseNo+mZJNo+mEndDateNo);
	 String YSQL="select a.mngcom ,a.rgtno ,a.caseno ,days(current date)-days(a.rgtdate) , a.rgtstate , a.handler ,b.username ,b.upusercode ,(select username from llclaimuser where usercode=b.upusercode) "
		         +" from llcase a,llclaimuser b where a.handler=b.usercode and a.rgtstate not in ('09','11','12') ";
	      if(mManageCom!=null&&!"".equals(mManageCom))
	    	  YSQL=YSQL+"and a.mngcom like '"+mManageCom+"%' ";
	      if(mRgtState!=null&&!"".equals(mRgtState))
	    	  YSQL=YSQL+"and a.rgtstate = '"+mRgtState+"' ";
	      if(mUserCode!=null&&!"".equals(mUserCode))
	    	  YSQL=YSQL+"and a.handler = '"+mUserCode+"' ";
	      if(mUpUserCode!=null&&!"".equals(mUpUserCode))
	    	  YSQL=YSQL+"and b.upusercode = '"+mUpUserCode+"' ";
	      if(mPNo!=null&&!"".equals(mPNo))
	    	  YSQL=YSQL+"and a.rgtno = '"+mPNo+"' ";
	      if(mCaseNo!=null&&!"".equals(mCaseNo))
	    	  YSQL=YSQL+"and a.caseno = '"+mCaseNo+"' ";
	      if(mZJNo!=null&&!"".equals(mZJNo))
	    	  YSQL=YSQL+"and a.IDNo = '"+mZJNo+"' ";
	      if(mEndDateNo!=null&&!"".equals(mEndDateNo))
	    	  YSQL=YSQL+"and (days(current date)-days(a.rgtdate)) between int('"+mStartDateNo+"') and int('"+mEndDateNo+"') ";
	      
	      YSQL=YSQL+" with ur";      

  
    	
    	        System.out.println(YSQL);
                SSRS ttSSRS1 =ttExeSQL.execSQL(YSQL);
 
//	            //当查询结果不为零时
                if (ttSSRS1.getMaxRow()>0)
                {
                for(int i=1;i<=ttSSRS1.getMaxRow();i++)
	            {

                 String ListInfo[]={"","","","","","","","",""};	
                 ListInfo[0]=ttSSRS1.GetText(i, 1);
	             ListInfo[1]=ttSSRS1.GetText(i, 2);
	             ListInfo[2]=ttSSRS1.GetText(i, 3);
	             ListInfo[3]=ttSSRS1.GetText(i, 4);
	             ListInfo[4]=ttSSRS1.GetText(i, 5);
	             ListInfo[5]=ttSSRS1.GetText(i, 6);
	             ListInfo[6]=ttSSRS1.GetText(i, 7);
	             ListInfo[7]=ttSSRS1.GetText(i, 8);
	             ListInfo[8]=ttSSRS1.GetText(i, 9);
	             mListTable.add(ListInfo);
	            }
            
                 }else{
                String ListInfo[]={"","","","","","","","",""};	
		        ListInfo[3]="0";
		        ListInfo[4]="0";
		        ListInfo[1]="0";
		        ListInfo[2]="0";
		        ListInfo[5]="0";
		        ListInfo[6]="0";
		        ListInfo[7]="0";
		        ListInfo[8]="0";
		        ListInfo[0]="0";
		        
		
	    }
	 
     
   }
              catch (Exception ex) {
	          ex.printStackTrace();
    }   
      
     return true;
    
  }
 
private boolean queryData()
{
  
  int datetype = -1;
  TextTag tTextTag = new TextTag();
  mXmlExport = new XmlExport();
  //设置模板名称
  mXmlExport.createDocument("LLNOCaseStatistics.vts","printer");
  System.out.println("获取基本信息：");
  
  String Sql0="select Name from ldcom where comcode='"+mManageCom+"'";
  ExeSQL ttExeSQL = new ExeSQL();
  SSRS ttSSRS3 =ttExeSQL.execSQL(Sql0);

  
  
  tTextTag.add("ComName",ttSSRS3.GetText(1,1));
  tTextTag.add("RgtState",mRgtState);
  tTextTag.add("UserCode",mUserCode);
  tTextTag.add("UpUserCode", mUpUserCode);
  tTextTag.add("PNo", mPNo);
  tTextTag.add("CaseNo", mCaseNo);
  tTextTag.add("ZJNo", mZJNo);
  tTextTag.add("StartDateNo", mStartDateNo);
  tTextTag.add("EndDateNo", mEndDateNo);
  tTextTag.add("Operator", mGlobalInput.Operator);
  tTextTag.add("MakeDate", mCurrentDate);
 
	
	
  mXmlExport.addTextTag(tTextTag);
  String[] tTitle ={ "COL1", "COL2", "COL3", "COL4", "COL5"};
  if (!getDataList(datetype))
  {
    return false;
  }
  mListTable.setName("ENDOR");
  mXmlExport.addListTable(mListTable,tTitle);
  mXmlExport.outputDocumentToFile("D:\\", "test7");
  this.mResult.clear();
  mResult.addElement(mXmlExport);
  return true;

}
private boolean queryDataToCVS()
{
  
  int datetype = -1;
  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	tLDSysVarDB.setSysVar("LPCSVREPORT");

	if (!tLDSysVarDB.getInfo()) {
		buildError("queryData", "查询文件路径失败");
		return false;
	}

	mFilePath = tLDSysVarDB.getSysVarValue(); 
	if (mFilePath == null || "".equals(mFilePath)) {
		buildError("queryData", "查询文件路径失败");
		return false;
	}
	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
	String tDate = PubFun.getCurrentDate2();

	mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
	System.out.println(mFileName);
	String[][] tTitle = new String[2][];
	tTitle[0] = new String[] { "未结案案件案件时效报表" };
	tTitle[1] = new String[] { "机构代码","机构名称", "批次号", "理赔号", "时效", "案件状态",
			"处理人代码", "处理人姓名", "处理人上级代码", "处理人上级姓名"};
	String[] tContentType = {"String","String","String","String","Number","String",
			"String","String","String","String"};
	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
		return false;
	}
  if (!getDataListToCVS(datetype))
  {
    return false;
  }
  mCSVFileWrite.closeFile();

	return true;
}
/*
 * 统计案件结果
 */
private boolean getDataListToCVS(int datetype)
{

  if (mManageCom == null || mManageCom.equals("")) {
	  buildError("getDataList", "机构编码缺失！");
	  return false;
  } 
  try{
	 ExeSQL ttExeSQL = new ExeSQL();
	 System.out.println("mUserCode:"+mRgtState+mUserCode+mUpUserCode+mPNo+mCaseNo+mZJNo+mEndDateNo);
	 String YSQL="select a.mngcom ,(select name from ldcom where comcode=a.mngcom),a.rgtno ,a.caseno ,days(current date)-days(a.rgtdate) , (select codename from ldcode where  codetype='llrgtstate' and code=a.rgtstate) , a.handler ,b.username ,b.upusercode ,(select username from llclaimuser where usercode=b.upusercode) "
		         +" from llcase a,llclaimuser b where a.handler=b.usercode and a.rgtstate not in ('09','11','12') ";
	      if(mManageCom!=null&&!"".equals(mManageCom))
	    	  YSQL=YSQL+"and a.mngcom like '"+mManageCom+"%' ";
	      if(mRgtState!=null&&!"".equals(mRgtState))
	    	  YSQL=YSQL+"and a.rgtstate = '"+mRgtState+"' ";
	      if(mUserCode!=null&&!"".equals(mUserCode))
	    	  YSQL=YSQL+"and a.handler = '"+mUserCode+"' ";
	      if(mUpUserCode!=null&&!"".equals(mUpUserCode))
	    	  YSQL=YSQL+"and b.upusercode = '"+mUpUserCode+"' ";
	      if(mPNo!=null&&!"".equals(mPNo))
	    	  YSQL=YSQL+"and a.rgtno = '"+mPNo+"' ";
	      if(mCaseNo!=null&&!"".equals(mCaseNo))
	    	  YSQL=YSQL+"and a.caseno = '"+mCaseNo+"' ";
	      if(mZJNo!=null&&!"".equals(mZJNo))
	    	  YSQL=YSQL+"and a.IDNo = '"+mZJNo+"' ";
	      if(mEndDateNo!=null&&!"".equals(mEndDateNo))
	    	  YSQL=YSQL+"and (days(current date)-days(a.rgtdate)) between int('"+mStartDateNo+"') and int('"+mEndDateNo+"') ";
	      
	      YSQL=YSQL+" with ur";      

  
    	
    	        System.out.println(YSQL);
                SSRS ttSSRS1 =ttExeSQL.execSQL(YSQL);
 
//	            //当查询结果不为零时
                if (ttSSRS1.getMaxRow()>0)
                {
                String tContent[][] = new String[ttSSRS1.getMaxRow()][];
	                for(int i=1;i<=ttSSRS1.getMaxRow();i++)
		            {
	
	                 String ListInfo[]={"","","","","","","","","",""};	
	                 ListInfo[0]=ttSSRS1.GetText(i, 1);
		             ListInfo[1]=ttSSRS1.GetText(i, 2);
		             ListInfo[2]=ttSSRS1.GetText(i, 3);
		             ListInfo[3]=ttSSRS1.GetText(i, 4);
		             ListInfo[4]=ttSSRS1.GetText(i, 5);
		             ListInfo[5]=ttSSRS1.GetText(i, 6);
		             ListInfo[6]=ttSSRS1.GetText(i, 7);
		             ListInfo[7]=ttSSRS1.GetText(i, 8);
		             ListInfo[8]=ttSSRS1.GetText(i, 9);
		             ListInfo[9]=ttSSRS1.GetText(i, 10);
		            // mListTable.add(ListInfo);
		             tContent[i - 1] = ListInfo;
		            }
	                mCSVFileWrite.addContent(tContent);
	                if (!mCSVFileWrite.writeFile()) {
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
						return false;
					}
                }else{
                	String tContent[][] = new String[1][];
                    String ListInfo[]={"","","","","","","","","",""};	
    		        ListInfo[3]="";
    		        ListInfo[4]="";
    		        ListInfo[1]="";
    		        ListInfo[2]="";
    		        ListInfo[5]="";
    		        ListInfo[6]="";
    		        ListInfo[7]="";
    		        ListInfo[8]="";
    		        ListInfo[0]="";
    		        ListInfo[9]="";
    		        tContent[0]=ListInfo;
    		        mCSVFileWrite.addContent(tContent);
                    if (!mCSVFileWrite.writeFile()) {
    					mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
    					return false;
    				}
                  }
     
   }
              catch (Exception ex) {
	          ex.printStackTrace();
	      	mCSVFileWrite.closeFile();
	      	return false;
    }   
      
     return true;
    
  }
//取得传入的数据
private boolean getInputData(VData mInputData)
{
 mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
 mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
 mManageCom =(String)mTransferData.getValueByName("ManageCom");
 mRgtState  =(String)mTransferData.getValueByName("RgtState");
 mUserCode=(String)mTransferData.getValueByName("UserCode");
 mUpUserCode=(String)mTransferData.getValueByName("UpUserCode");
 mPNo  =(String)mTransferData.getValueByName("PNo");
 mCaseNo  =(String)mTransferData.getValueByName("CaseNo");
 mZJNo  =(String)mTransferData.getValueByName("ZJNo");
 mStartDateNo  =(String)mTransferData.getValueByName("StartDateNo");
 mEndDateNo   =(String) mTransferData.getValueByName("EndDateNo");
 mFileNameB =(String) mTransferData.getValueByName("tFileNameB");
 return true;
}

 /**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLNOCaseStatisticsBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}

	public static void main(String[] args) {
		
		

	}

	private void jbInit() throws Exception {
	}
 
}
