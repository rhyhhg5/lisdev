package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

public class LAJXCommisionWageBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mStartDate = "";

//取得的机构外部代码
    private String mBranchAttr = "";

//取得的代理人离职日期
    private String mEndDate = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mStartDate = (String)cInputData.get(2);
    	mEndDate = (String)cInputData.get(3);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAJXCommision1.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAJX");
        ListTable mListTable = new ListTable();
        mListTable.setName("SUM"); //合计行
        String[] title = {"", "", "", "", "", "", "", "", ""};
        //	String tAgentGroup = "";
        String SQL = "";
        double firstPremSum = 0.0;
        double nextPremSum = 0.0;
        double xubaoPremSum = 0.0;
        double firstFycSum = 0.0;
        double nextFycSum = 0.0;
        double xubaoFycSum = 0.0;
        int customNumSum = 0;
    //       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
    //       {
    //    	  SQL = "select * FROM labranchgroup where branchattr like '"+this.mBranchAttr+"%' and managecom like '"+this.mManageCom+"%' and branchtype='1' and branchtype2='01'";
    //       }
    //       else
    //    	  SQL = "select * from labranchgroup where managecom like '"+this.mManageCom+"%' and branchtype='1' and branchtype2='01'";
    //      System.out.println(SQL);
    //       LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
    //       LABranchGroupDB  tLABranchGroupDB = new LABranchGroupDB();
    //       tLABranchGroupSet = tLABranchGroupDB.executeQuery(SQL);
//        ExeSQL tExeSQL = new ExeSQL();
//        //首年度保费
//        String firstPremSQL = "select value(sum(transmoney),0) from lacommision where payyear=0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" +
//                              mStartDate + "' and tmakedate<='" + this.mEndDate +
//                              "' and branchtype='1' and branchtype2='03'";
//        String firstPrem = tExeSQL.getOneValue(firstPremSQL);
//        //续年保费
//        String nextPremSQL = "select value(sum(transmoney),0) from lacommision where payyear>0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" +
//                             mStartDate + "' and tmakedate<='" + this.mEndDate +
//                             "'  and branchtype='1' and branchtype2='03'";
//        String nextPrem = tExeSQL.getOneValue(nextPremSQL);
//        //续保保费
//        String xubaoPremSQL = "select value(sum(transmoney),0) from lacommision where renewcount>0 and agentcode=a.agentcode  and tmakedate>='" +
//                              mStartDate + "' and tmakedate<='" + this.mEndDate +
//                              "' and branchtype='1' and branchtype2='03'";
//        String xubaoPrem = tExeSQL.getOneValue(xubaoPremSQL);
//        //客户数
//        String customNumSQL = "select count(distinct(appntno)) from lacommision where paycount=0 and renewcount=0 and agentcode=a.agentcode and tmakedate>='" +
//                              mStartDate + "' and tmakedate<='" + this.mEndDate +
//                              "' and branchtype='1' and branchtype2='03'";
//        String customNum = tExeSQL.getOneValue(customNumSQL);
//        //首年度佣金
//        String firstFycSQL = "select value(sum(fyc),0) from lacommision where payyear=0 and renewcount=0 and agentcode=a.agentcode and tmakedate>='" +
//                             mStartDate + "' and tmakedate<='" + this.mEndDate +
//                             "' and branchtype='1' and branchtype2='03'";
//        String firstFyc = tExeSQL.getOneValue(firstFycSQL);
//        //续年度佣金
//        String nextFycSQL = "select value(sum(fyc),0) from lacommision where payyear>0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" +
//                            mStartDate + "' and tmakedate<='" + this.mEndDate +
//                            "' and branchtype='1' and branchtype2='03'";
//        String nextFyc = tExeSQL.getOneValue(nextFycSQL);
//        //续保佣金
//        String xubaoFycSQL = "select value(sum(fyc),0) from lacommision where  renewcount>0 and agentcode=a.agentcode  and tmakedate>='" +
//                             mStartDate + "' and tmakedate<='" + this.mEndDate +
//                             "' and branchtype='1' and branchtype2='03'";

        String tsql="select a,b,c,d,e,f,g,h,i from (select a.agentcode a,a.name b,"
                    +" (select value(sum(transmoney),0) from lacommision where payyear=0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" +mStartDate + "' and tmakedate<='" + this.mEndDate +"' and branchtype='1' and branchtype2='03' ) c,"
                    +" (select value(sum(transmoney),0) from lacommision where payyear>0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" +mStartDate + "' and tmakedate<='" + this.mEndDate +"'  and branchtype='1' and branchtype2='03') d,"
                    +" (select value(sum(transmoney),0) from lacommision where renewcount>0 and agentcode=a.agentcode  and tmakedate>='" +mStartDate + "' and tmakedate<='" + this.mEndDate +"' and branchtype='1' and branchtype2='03') e ,"
                    +" (select value(sum(fyc),0) from lacommision where payyear=0 and renewcount=0 and agentcode=a.agentcode and tmakedate>='" +mStartDate + "' and tmakedate<='" + this.mEndDate +"' and branchtype='1' and branchtype2='03') f,"
                    +" (select value(sum(fyc),0) from lacommision where payyear>0 and renewcount=0 and agentcode=a.agentcode  and tmakedate>='" + mStartDate + "' and tmakedate<='" + this.mEndDate + "' and branchtype='1' and branchtype2='03') g,"
                    +" (select value(sum(fyc),0) from lacommision where  renewcount>0 and agentcode=a.agentcode  and tmakedate>='" + mStartDate + "' and tmakedate<='" + this.mEndDate + "' and branchtype='1' and branchtype2='03') h,"
                    +" (select count(distinct(appntno)) from lacommision where paycount=0 and renewcount=0 and agentcode=a.agentcode and tmakedate>='" + mStartDate + "' and tmakedate<='" + this.mEndDate + "' and branchtype='1' and branchtype2='03' ) i "
                    + "   from laagent a where branchtype='1' and branchtype2='01' and managecom like '"+this.mManageCom+"%'";
        if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
        {
            tsql+=" and a.agentgroup in (select agentgroup from labranchgroup branchattr like '"+this.mBranchAttr+"%' and managecom like '"+this.mManageCom+"%' and branchtype='1' and branchtype2='01' )";

        }
        tsql+=" and employdate <='"+ this.mEndDate +"' and (outworkdate>='"+mStartDate+"' or outworkdate is null ) ";
        tsql+=") as x order by a ";
        SSRS tSSRS= new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS=tExeSQL.execSQL(tsql);
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LAJXCommisionWageBL";
            tError.functionName = "getPrintData";
            tError.errorMessage = "查询出错!";
            this.mErrors.addOneError(tError);
        }
        for(int i=1;i<=tSSRS.getMaxRow();i++)
        {




            String info[] = new String[9];
            info[0] = tSSRS.GetText(i, 1);
            info[1] = tSSRS.GetText(i, 2);
            info[2] = tSSRS.GetText(i, 3);
            info[3] = tSSRS.GetText(i, 4);
            info[4] = tSSRS.GetText(i, 5);
            info[5] = tSSRS.GetText(i, 6);
            info[6] = tSSRS.GetText(i, 7);
            info[7] = tSSRS.GetText(i, 8);
            info[8] = tSSRS.GetText(i, 9);

            tListTable.add(info);
            firstPremSum += Double.parseDouble(tSSRS.GetText(i, 3));
            nextPremSum += Double.parseDouble(tSSRS.GetText(i, 4));
            xubaoPremSum += Double.parseDouble(tSSRS.GetText(i, 5));
            firstFycSum += Double.parseDouble(tSSRS.GetText(i, 6));
            nextFycSum += Double.parseDouble(tSSRS.GetText(i, 7));
            xubaoFycSum += Double.parseDouble(tSSRS.GetText(i, 8));
            customNumSum += Integer.parseInt(tSSRS.GetText(i, 9));
        }

        //求合计
        String sum[] = new String[9];
        sum[0] = "合  计";
        sum[1] = "";
        sum[2] = String.valueOf(firstPremSum);
        sum[3] = String.valueOf(nextPremSum);
        sum[4] = String.valueOf(xubaoPremSum);
        sum[5] = String.valueOf(firstFycSum);
        sum[6] = String.valueOf(nextFycSum);
        sum[7] = String.valueOf(xubaoFycSum);
        sum[8] = String.valueOf(customNumSum);
        mListTable.add(sum);

        if (tListTable.size() <= 0) {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有需要统计的承保信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
        texttag.add("CurrentDate", mdate); //日期
        texttag.add("CurrentTime", mtime);
        texttag.add("tStartDate", this.mStartDate);
        texttag.add("tEndDate", this.mEndDate);
        texttag.add("tName", getComName(this.mManageCom));
        //  texttag.add("tManageComName", this.mGlobalInput.ManageCom);
        if (texttag.size() > 0) {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable, title);
        txmlexport.addListTable(mListTable, title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }


    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
