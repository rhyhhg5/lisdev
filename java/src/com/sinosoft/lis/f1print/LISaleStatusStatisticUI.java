
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.*;


public class LISaleStatusStatisticUI {
	public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();


    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
         System.out.println("String PrintHFProexecReportBL...");
         LISaleStatusStatisticBL tLISaleStatusStatisticBL = new LISaleStatusStatisticBL();
        if (!tLISaleStatusStatisticBL.submitData(mInputData, mOperate))
        {
            if (tLISaleStatusStatisticBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLISaleStatusStatisticBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LISaleStatusStatisticUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLISaleStatusStatisticBL.getResult();
            return true;
        }
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LISaleStatusStatisticUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult() {
        return this.mResult;
    }
}
