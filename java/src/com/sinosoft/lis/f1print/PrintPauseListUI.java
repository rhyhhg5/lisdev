package com.sinosoft.lis.f1print;
import com.sinosoft.utility.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrintPauseListUI {
    public CErrors mErrors = new CErrors();
    private XmlExport tXmlExport;
    public PrintPauseListUI() {
    }
    /**
     * 将数据提交给后台程序处理
     * @param data VData
     * @param operate String   可以为空
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData data, String operate)
    {

            PrintPauseListBL tPrintPauseListBL = new PrintPauseListBL();
            tXmlExport=tPrintPauseListBL.getXmlExport(data, operate);
            mErrors.copyAllErrors(tPrintPauseListBL.mErrors);
            return tXmlExport;
    }
    public void main(String[] arg)
    {

    }

}
