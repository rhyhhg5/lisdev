package com.sinosoft.lis.f1print;

import java.io.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保问题件通知书</p>
 * <p>Description: 保全人工核保问题件通知书</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWF1PBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private MMap mMap = new MMap();

    private String mEdorNo = null;

    private String mContNo = null;

    private String mPrtSeq = null;

    private String mAppntNo = null;

    private String mAppntName = null;

    private String mAppntSex = null;

    private String mManageCom = null;

    private String mManageName = null;

    private String mManageAddress = null;

    private String mManageZipCode = null;

    private String mManageFax = null;

    private String mManagePhone = null;

    private String mPost = null;

    private String mAddress = null;

    private String mAgentCode = null;

    private String mAgentName = null;

    private String mAgentPhone = null;

    private String mAgentGroup = null;

    private String mBranchAttr = null;

    private String mSendDate = null;

    private String mPEName = null;

    private String mPEPhone = null;

    private String mSysDate = CommonBL.decodeDate(PubFun.getCurrentDate());

    private XmlExport xml = new XmlExport();

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param prtNo String
     */
    public PEdorUWF1PBL(GlobalInput gi, String prtSeq)
    {
        this.mGlobalInput = gi;
        this.mPrtSeq = prtSeq;
    }

    /**
     * 提交业务数据
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    private boolean getInputData()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mPrtSeq);
        if (!tLOPRTManagerDB.getInfo())
        {
            mErrors.addOneError("未找到通知书打印信息！");
            return false;
        }
        this.mEdorNo = tLOPRTManagerDB.getOtherNo();
        LPIssuePolDB tLPIssuePolDB = new LPIssuePolDB();
        tLPIssuePolDB.setEdorNo(mEdorNo);
        LPIssuePolSet tLPIssuePolSet = tLPIssuePolDB.query();
        if (tLPIssuePolSet.size() == 0)
        {
            mErrors.addOneError("未找到问题件录入信息！");
            return false;
        }
        this.mContNo = tLPIssuePolSet.get(1).getContNo();
        System.out.println("mContNo:" + mContNo);
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!getPrintData())
        {
            return false;
        }
        if (!createXML())
        {
            return false;
        }
        if (!setPrintFlag())
        {
            return false;
        }
        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到打印数据
     * @return boolean
     */
    private boolean getPrintData()
    {
        if(!getAppntData())
        {
            return false;
        }
        if (!getAddressData())
        {
            return false;
        }
        if (!getManageComAddress())
        {
            return false;
        }
        if (!getAgentData())
        {
            return false;
        }
        return true;
    }

    private boolean getAppntData()
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mContNo);
        if (!tLCAppntDB.getInfo())
        {
            mErrors.addOneError("未找到投保人信息!");
            return false;
        }
        this.mAppntNo = tLCAppntDB.getAppntNo();
        this.mAppntName = tLCAppntDB.getAppntName();
        this.mAppntSex = tLCAppntDB.getAppntSex();
        this.mManageCom = tLCAppntDB.getManageCom();
        return true;
    }

    /**
     * 得到投保人联系地址信息
     * @return boolean
     */
    private boolean getAddressData()
    {
        LCAddressSchema tLCAddressSchema = CommonBL.getLCAddress(mContNo);
        if (tLCAddressSchema == null)
        {
            mErrors.addOneError("未找到投保人联系地址信息!");
            return false;
        }
        this.mPost = tLCAddressSchema.getZipCode();
        this.mAddress = tLCAddressSchema.getPostalAddress();
        return true;
    }

    /**
     * 得到管理机构信息
     * @return boolean
     */
    private boolean getManageComAddress()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mManageCom);
        if (!tLDComDB.getInfo())
        {
            mErrors.addOneError("未找到管理机构信息!");
            return false;
        }
        this.mManageName = tLDComDB.getLetterServiceName();
        this.mManageAddress = tLDComDB.getServicePostAddress();
        this.mManageZipCode = tLDComDB.getServicePostZipcode();
        this.mManageFax = tLDComDB.getFax();
        this.mManagePhone = tLDComDB.getServicePhone();
        return true;
    }

    /**
     * 得到代理人信息
     * @return boolean
     */
    private boolean getAgentData()
    {
        LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
        if (tLCContSchema == null)
        {
            mErrors.addOneError("未找到合同信息！");
            return false;
        }
        this.mAgentCode = tLCContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.addOneError("未找到代理人信息！");
            return false;
        }
        this.mAgentName = tLAAgentDB.getName();
        this.mAgentPhone = tLAAgentDB.getPhone();
        this.mAgentGroup = tLAAgentDB.getAgentGroup();
        if (mAgentPhone == null)
        {
            this.mAgentPhone = tLAAgentDB.getMobile();
        }

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(mAgentGroup);
        if (tLABranchGroupDB.getInfo())
        {
            this.mBranchAttr = tLABranchGroupDB.getBranchAttr();
        }
        return true;
    }


    /**
     * 根据清单数据生成XML
     * @param xmlExport XmlExport
     */
    private boolean createXML()
    {
        xml.createDocument("PEdorUWNotice.vts", "printer");
        addPolList();
        addQuestList();
        addTags();
        return true;
    }

    /**
     * 添加打印文本数据
     */
    private void addTags()
    {
        TextTag tag = new TextTag();
        tag.add("EdorNo", StrTool.cTrim(mEdorNo));
        tag.add("AppntNo", mAppntNo);
        tag.add("AppntName", StrTool.cTrim(mAppntName));
        tag.add("AppntSex", CommonBL.decodeSex(mAppntSex));
        tag.add("ManageCom", StrTool.cTrim(mManageName));
        tag.add("ManageAddress", StrTool.cTrim(mManageAddress));
        tag.add("ManageZipCode", StrTool.cTrim(mManageZipCode));
        tag.add("ManageFax", StrTool.cTrim(mManageFax));
        tag.add("ManagePhone", StrTool.cTrim(mManagePhone));
        tag.add("BarCode1", StrTool.cTrim(mPrtSeq));
        tag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        tag.add("Post", StrTool.cTrim(mPost)); //投保人邮政编码
        tag.add("Address", StrTool.cTrim(mAddress)); //投保人地址
        tag.add("ContNo", StrTool.cTrim(mContNo));
        tag.add("AgentPhone", StrTool.cTrim(mAgentPhone));
        tag.add("AgentName", StrTool.cTrim(mAgentName));
		//2014-11-2    杨阳
		//业务员号友显示LAagent表中GroupAgentcode字段
		String groupAgentCode = new ExeSQL().getOneValue("select getUniteCode("+mAgentCode+") from dual with ur");
        tag.add("AgentCode", StrTool.cTrim(groupAgentCode));
        tag.add("BranchAttr", StrTool.cTrim(mBranchAttr));
        System.out.println("mSendDate" + mSendDate);
        String rebackDate;
        if ((mSendDate != null) && (!mSendDate.equals("")))
        {
            rebackDate = CommonBL.decodeDate(FDate.toString(CommonBL.changeDate(mSendDate, 5)));
        }
        else
        {
            rebackDate = "";
        }
        tag.add("ReBackDate", rebackDate); //截止日期
//        tag.add("PEName", StrTool.cTrim(mPEName));
//        tag.add("PEPhone", StrTool.cTrim(mPEPhone));
        tag.add("SysDate", StrTool.cTrim(mSysDate));
        xml.addTextTag(tag);
    }

    /**
     * 显示险种信息
     */
    private void addPolList()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        ListTable polListTable = new ListTable();
        polListTable.setName("RiskInfo");
        for (int i= 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            String[] detail = new String[6];
            detail[0] = String.valueOf(i);
            detail[1] = StrTool.cTrim(tLCPolSchema.getInsuredName());
            detail[2] = CommonBL.getRiskName(tLCPolSchema.getRiskCode());
            detail[3] = StrTool.cTrim(tLCPolSchema.getRiskCode());
            detail[4] = tLCPolSchema.getInsuYear() + "";
            if (tLCPolSchema.getInsuYearFlag().equals("Y"))
            {
                detail[4] += "年";
            }
            else if (tLCPolSchema.getInsuYearFlag().equals("M"))
            {
                detail[4] += "月";
            }
            else if (tLCPolSchema.getInsuYearFlag().equals("D"))
            {
                detail[4] += "日";
            }
            else if (tLCPolSchema.getInsuYearFlag().equals("A"))
            {
                detail[4] = "至" + detail[4] + "岁";
            }
            if (tLCPolSchema.getInsuYear() == 1000 &&
                    tLCPolSchema.getInsuYearFlag().toUpperCase().equals("A"))
            {
                detail[4] = "终身";
            }
            if (tLCPolSchema.getMult() > 0)
            {
                detail[5] = String.valueOf(tLCPolSchema.getMult());
            }
            else
            {
                detail[5] = String.valueOf(tLCPolSchema.getAmnt());
            }
            polListTable.add(detail);
        }
        xml.addListTable(polListTable, new String[6]);
    }

    /**
     * 显示问题件
     */
    private void addQuestList()
    {
        LPIssuePolDB tLPIssuePolDB = new LPIssuePolDB();
        tLPIssuePolDB.setEdorNo(mEdorNo);
        tLPIssuePolDB.setPrtSeq(mPrtSeq);
        LPIssuePolSet tLPIssuePolSet = tLPIssuePolDB.query();
        ListTable questListTable = new ListTable();
        questListTable.setName("QUESTION");
        for (int i= 1; i <= tLPIssuePolSet.size(); i++)
        {
            LPIssuePolSchema tLPIssuePolSchema = tLPIssuePolSet.get(i);
            String[] detail = new String[6];
            detail[0] = String.valueOf(i);
            detail[1] = StrTool.cTrim(tLPIssuePolSchema.getQuestionObj());
            detail[2] = StrTool.cTrim(tLPIssuePolSchema.getErrFieldName());
            detail[3] = StrTool.cTrim(tLPIssuePolSchema.getErrContent());
            detail[4] = StrTool.cTrim(tLPIssuePolSchema.getIssueCont());
            detail[5] = "";
            questListTable.add(detail);
            this.mSendDate = tLPIssuePolSchema.getModifyDate();
        }
        xml.addListTable(questListTable, new String[6]);
    }

    /**
     * 设置打印标志
     * @return boolean
     */
    private boolean setPrintFlag()
    {
        String sql = "update LOPRTManager " +
                "set StateFlag = '" + BQ.PRINTFLAG_Y + "' " +
                "where PrtSeq = '" + mPrtSeq + "'";
        mMap.put(sql, "UPDATE");
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
