/*
 * <p>ClassName: LAMarketUI  </p>
 * <p>Description: LAMarketUI 类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-11-13
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAMarketUI {
    public LAMarketUI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

   public CErrors mErrors = new CErrors();
   private VData mInputData = new VData();
   /** 数据操作字符串 */
   private String mOperate;
   private VData mResult = new VData();


   public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
        System.out.println("String LAMarketBL ...");
        LAMarketBL  tLAMarketBL = new LAMarketBL();
        if (!tLAMarketBL.submitData(mInputData, mOperate))
        {
            if (tLAMarketBL .mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLAMarketBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LAMarketUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLAMarketBL.getResult();
            return true;
        }
    }

    /**
  * 追加错误信息
  * @param szFunc String
  * @param szErrMsg String
  */
 private void buildError(String szFunc, String szErrMsg)
 {
     CError cError = new CError();

     cError.moduleName = "tLAMarketUI";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
 }


 /**
  * 取得返回处理过的结果
  * @return VData
  */
 public VData getResult() {
     return this.mResult;
 }

    private void jbInit() throws Exception {
    }


}
