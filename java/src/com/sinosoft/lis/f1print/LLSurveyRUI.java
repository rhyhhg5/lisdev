package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLSurveyRUI {
	public CErrors mErrors = new CErrors();

	private String mOperate;

	private VData mInputData = new VData();

	//private LLSurveyRBL mLLSurveyRBL = new LLSurveyRBL();

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		mInputData = (VData) cInputData.clone();
		System.out.println("String LLSurveyBL begin...");
		LLSurveyRBL mLLSurveyRBL = new LLSurveyRBL();

		if (!mLLSurveyRBL.submitData(mInputData, mOperate)) {
			if (mLLSurveyRBL.mErrors.needDealError()) {
				mErrors.copyAllErrors(mLLSurveyRBL.mErrors);
				return false;
			} else {
				buildError("submitData", "LLSurveyUI发生错误，但是没有提供详细的出错信息");
				return false;
			}
		}

		else {
			mInputData = mLLSurveyRBL.getResult();
			return true;
		}

	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LLSurveyUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mInputData;
	}

}
