package com.sinosoft.lis.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Hashtable;

import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCNationDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataObject;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCGrpSuccorNewBL2 implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LDComSchema mLDComSchema = new LDComSchema();

    private String mTemplatePath = "";

    private String mOutXmlPath = null;

    private XMLDatasets mXMLDatasets = null;

    private String[][] mPicFile = null;

    private TransferData mTransferData;

    private MMap Map = new MMap();

    //  private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    //    private Vector m_vGrpPolNo = new Vector();
    public LCGrpSuccorNewBL2() {
    }

    /**
     * 主程序
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            //判定打印类型
//            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
//            {
//                buildError("submitData", "不支持的操作字符串");
//                return false;
//            }
            //全局变量赋值
            mOperate = cOperate;
            //得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }
            //正常打印处理
//            if (mOperate.equals("PRINT")) {
            //新建一个xml对象
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            //数据准备
            if (!getPrintData()) {
                return false;
            }

            if (!dealPrintMag()) {
                return false;
            }

//            }
            //重打处理
            if (mOperate.equals("REPRINT")) {
                if (!getRePrintData()) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //正常打印
        mGlobalInput.setSchema((GlobalInput) cInputData
                               .getObjectByObjectName("GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        //重打模式
        if (mOperate.equals("REPRINT")) {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData
                                       .getObjectByObjectName("LCGrpContSchema",
                    0));
        }

        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='ServerRoot'";//生成文件的存放路径
        mTemplatePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
        //mTemplatePath = "E:\\hmyc\\Projects\\PICCH\\ui\\";
        System.out.println("文件的存放路径   "+mTemplatePath);//调试用－－－－－
        if(mTemplatePath == null ||mTemplatePath.equals("")){
            buildError("getFileUrlName","获取文件存放路径出错");
            return false;
        }
        mTemplatePath += "f1print/template/";

        return true;
    }

    /**
     * 返回信息
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception {
        //团单Schema
//        LCGrpContSchema tLCGrpContSchema = null;
        //原则上一次只打印一个团单，当然可以一次多打
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCGrpContDB.getInfo()) {
            System.out
                    .println("getInfo" + tLCGrpContDB.mErrors.getErrContent());
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();

        //取得该团单下的所有投保险种信息
        LCGrpPolSet tLCGrpPolSet = getRiskList(mLCGrpContSchema.getGrpContNo());

        //打印5也内容
        //正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(0,"")) {//正本
            return false;
        }
        if (!getGrpPolDataSet(1,"")) {//正本清单
            return false;
        }
        if (!getGrpPolDataSet(2,"")) {//副本
            return false;
        }
        if (!getGrpPolDataSet(3,"")) {//副本清单
            return false;
        }
        String tinsuredlistsql=" select insuredno from lcinsured where grpcontno='"+mLCGrpContSchema.getGrpContNo()+"'  ";
        SSRS mresult=new ExeSQL().execSQL(tinsuredlistsql);
        if(mresult!=null && mresult.MaxRow>0){
        	for(int i=1;i<=mresult.MaxRow;i++){
        		if (!getGrpPolDataSet(4,mresult.GetText(i, 1))) {//被保人清单
                    return false;
                }
        	}
        	
        }
        //将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        //    mResult.add(tLCGrpPolSet);//其实没必要更新这个表
        this.mLCGrpContSchema.setPrintCount(1);
//        mResult.add(this.mLCGrpContSchema); //只更新这个表就好拉
        mResult.add(mXMLDatasets); //xml数据流
        mResult.add(mGlobalInput); //全局变量
        XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

        System.out.println("生成的报文："+outputter.outputString(mXMLDatasets.getDocument()));
        //更新保单打印次数
        Map.put("update lcgrpcont set printcount=1  where  prtno='"
                + this.mLCGrpContSchema.getPrtNo() + "' and printcount<>1",
                "UPDATE");
        System.out.println("update lcgrpcont set printcount=1  where  prtno='"
                           + this.mLCGrpContSchema.getPrtNo() +
                           "' and printcount<>1");
//        mResult.clear();
        VData tVData = new VData();
        tVData.add(Map);
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(tVData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCGrpSuccorBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("ImpartToICDBL end");

        return true;
    }
    /**
     * 正常打印流程
     * @param cLCGrpPolSet LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(int printtype,String minsuredid ) throws Exception {
        //xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        //团个单标志，1个单，2团单
        tXMLDataset.addDataObject(new XMLDataTag("JetFormType",
                                                 mLOPRTManagerSchema.getCode()));
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", printcode));
        tXMLDataset.addDataObject(new XMLDataTag("userIP",
                                                 mGlobalInput.ClientIP.
                                                 replaceAll("\\.", "_")));
        if ("batch".equals(mOperate)) {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "0"));
        } else {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "1"));
        }

        tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));

        //获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo()) {
            //只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError()) {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //默认填入空
            tXMLDataset.setTemplate("");
        } else {
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpContNo",
                                                 this.mLCGrpContSchema.
                                             getGrpContNo()));
        //正本和副本需要
        if(printtype==0 || printtype==2 || printtype==4){
        	tXMLDataset.addDataObject(new XMLDataTag("XI_ManageCom",
                    mLCGrpContSchema.getManageCom()));
        }
        
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.PrtNo",
                                                 this.mLCGrpContSchema.getPrtNo()));
      //正本和副本需要
        if(printtype==0 || printtype==2 || printtype==4){
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AppntNo",
                                                 this.mLCGrpContSchema.
                                                 getAppntNo()));
        }
      //正本和副本需要、被保人清单
        if(printtype==0 || printtype==2 || printtype==4){
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Peoples2",
                                                 this.mLCGrpContSchema.
                                                 getPeoples2()));
        }
      //正本和副本需要
        if(printtype==0 || printtype==2 || printtype==4){
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpName",
                                                 this.mLCGrpContSchema.
                                                 getGrpName()));
        }
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SignDate", PubFun
                                                 .getCurrentDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CValiDate",
                                                 this.mLCGrpContSchema.
                                                 getCValiDate()));
      //正本和副本需要
        if(printtype==0 || printtype==2 || printtype==4){
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SumPrem", String
                                                 .valueOf(this.mLCGrpContSchema.
                getSumPrem())));
        }
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Remark", StrTool
                                                 .cTrim(this.mLCGrpContSchema.
                getRemark())));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CInValiDate",
                                                 this.mLCGrpContSchema.
                                                 getCInValiDate()));
      //正本和副本需要 被保人清单
        if(printtype==0 || printtype==2 ||printtype==4){
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.DegreeType",
                                                 this.mLCGrpContSchema.
                                                 getDegreeType()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Operator",
                                                 this.mLCGrpContSchema.
                                                 getOperator()));
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCGrpContSchema.getAgentCode());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo()) {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AgentCode",
                                                 this.mLAAgentSchema.
                                                 getGroupAgentCode()));
        //增加被保人信息
        if(printtype==4){
        	LCInsuredSchema tLCInsuredSchema=new LCInsuredSchema();
        	LCInsuredDB tLCInsuredDB=new LCInsuredDB();
        	tLCInsuredDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        	tLCInsuredDB.setInsuredNo(minsuredid);
        	tLCInsuredSchema=tLCInsuredDB.query().get(1);
        	tXMLDataset.addDataObject(new XMLDataTag("InsuredName",
        			tLCInsuredSchema.getName()));
        	tXMLDataset.addDataObject(new XMLDataTag("EnglishName",
        			tLCInsuredSchema.getEnglishName()));
        	tXMLDataset.addDataObject(new XMLDataTag("InsuredSex",
        			new ExeSQL().getOneValue("select CodeName from LDCode where CodeType = 'sex' and Code = '"+tLCInsuredSchema.getSex()+"'")));
        	tXMLDataset.addDataObject(new XMLDataTag("InsuredAge",
        			new ExeSQL().getOneValue("select insuredappage from LCPol lcp where lcp.ContNo = '"+tLCInsuredSchema.getContNo()+"' and lcp.InsuredNo = '"+tLCInsuredSchema.getInsuredNo()+"' ")));
        	tXMLDataset.addDataObject(new XMLDataTag("InsuredPassportNo",
        			tLCInsuredSchema.getOthIDNo()));
        }
        if(printtype==0 || printtype==2){
        	//保全补打 qulq 2007-11-28
            tXMLDataset.addDataObject(new XMLDataTag("LostTimes",
                                                     this.mLCGrpContSchema.
                                                     getLostTimes()));
        }
        }
        //除被保人清单
        if(printtype != 4 ){
        tXMLDataset.addDataObject(new XMLDataTag("BQRePrintDate",
                                                 com.sinosoft.lis.bq.CommonBL.
                                                 decodeDate(PubFun.
                getCurrentDate())
                                  ));
        }
      //正本和副本需要
        if(printtype==0 || printtype==2 ||printtype==4){
        //tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Uwoperator", this.mLCGrpContSchema.getUWOperator()));
        //如果是卡单型险种需要查询抵达国家
        if (!StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("")) {
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++) {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size()) {
                    Nation += ",";
                }
            }
            tXMLDataset.addDataObject(new XMLDataTag("Nation", Nation));
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLDGrpDB.getInfo();
            tXMLDataset.addDataObject(new XMLDataTag("GrpEnglithName", tLDGrpDB
                    .getGrpEnglishName()));
            /** 增加签单机构 */
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
            if (!tLDComDB.getInfo()) {
                buildError("getGrpPolDataSet", "查询失败！");
                return false;
            }
            tXMLDataset.addDataObject(new XMLDataTag("SignCom", tLDComDB
                    .getLetterServiceName()));
            tXMLDataset.addDataObject(new XMLDataTag("SignComEnName", tLDComDB
                    .getEName()));
            tXMLDataset.addDataObject(new XMLDataTag("EnPostAddress", tLDComDB
                    .getEAddress()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePhone", tLDComDB
                    .getPhone()));
            //        tXMLDataset.addDataObject(new XMLDataTag("ServicePhone",
            //                                                 tLDComDB.getServicePhone()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePostAddress",
                    tLDComDB.getServicePostAddress()));
        }
        }
        //查询投保人信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        //根据合同号查询
        tLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAppntDB.getInfo()) {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCGrpAppntSchema = tLCGrpAppntDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCGrpAppntSchema);

        //查询投保人的地址信息
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        //根据投保人编码和地址编码查询
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAddressDB.getInfo()) {
            buildError("getInfo", "查询个单投保人地址信息失败！");
            return false;
        }
        this.mLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCGrpAddressSchema);

        //查询管理机构信息
        LDComDB tLDComDB = new LDComDB();
        //根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDComDB.getInfo()) {
            buildError("getInfo", "查询管理机构信息失败！");
            return false;
        }
        this.mLDComSchema = tLDComDB.getSchema();
        if("Y".equals(mLDComSchema.getInteractFlag())){
        	tLDComDB.setComCode(mGlobalInput.ManageCom.substring(0, 4));
        	if (!tLDComDB.getInfo())
            {
                buildError("getInfo", "查询管理机构信息失败！");
                return false;
            }
        	mLDComSchema.setName(tLDComDB.getSchema().getName());
        }
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLDComSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.Name",
                                                 this.mLDComSchema.getName()));
        
        tXMLDataset.addDataObject(new XMLDataTag("LAAgent.Name", mLAAgentSchema
                .getName()));
      //正本和副本需要
        if(printtype==0 || printtype==2 || printtype==4){
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLAAgentSchema);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.mLCGrpContSchema.getAgentGroup());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLABranchGroupDB.getInfo()) {
            buildError("getInfo", "查询销售机构信息失败！");
            return false;
        }
        this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLABranchGroupSchema);
        
        //by gzh 20120522 增加对中介机构及业务员的处理
//      by gzh 20120521
        //增加中介机构及中介机构销售人员信息
        String tAgentCom = this.mLCGrpContSchema.getAgentCom();
        String tAgentComName = "";
        if(tAgentCom != null && !"".equals(tAgentCom)){
        	LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(tAgentCom);
//          如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAComDB.getInfo())
            {
            	buildError("getInfo", "查询中介机构信息失败！");
                return false;
            }
            this.mLAComSchema = tLAComDB.getSchema();
            tAgentComName = this.mLAComSchema.getName();
        }
        if(printtype!=4){
        	
       
//      将查询出的schema信息放入到xml对象中
//        tXMLDataset.addSchema(this.mLAComSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LACom.AgentCom", tAgentCom));
        tXMLDataset.addDataObject(new XMLDataTag("LACom.Name", tAgentComName));
        
        String tAgentSaleCode = this.mLCGrpContSchema.getAgentSaleCode();
        String tAgentSaleName = "";
        if(tAgentSaleCode != null && !"".equals(tAgentSaleCode)){
        	LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
            tLAAgenttempDB.setAgentCode(tAgentSaleCode);
            // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAAgenttempDB.getInfo())
            {
                buildError("getInfo", "查询中介机构代理业务员信息失败！");
                return false;
            }
            this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
            tAgentSaleName = this.mLAAgenttempSchema.getName();
        }
        // 将查询出的schema信息放入到xml对象中
//        tXMLDataset.addSchema(this.mLAAgenttempSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LAAgenttemp.AgentCode", tAgentSaleCode));
        tXMLDataset.addDataObject(new XMLDataTag("LAAgenttemp.Name", tAgentSaleName));
        // by gzh end 20120521
        }
        
        }
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql =
                "select cvalidate,cinvalidate from LCGrpCont where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("BeginDate", tSSRS.GetText(1,
                1)));
        tXMLDataset
                .addDataObject(new XMLDataTag("EndDate", tSSRS.GetText(1, 2)));

        //操作人员编码和操作人员姓名
        tSql = "select UserCode,UserName from LDUser where UserCode in (Select Operator from LJTempFee where OtherNo = '"
               + this.mLCGrpContSchema.getGrpContNo()
               + "' and OtherNoType = '7')";
        tSSRS = tExeSQL.execSQL(tSql);
        tXMLDataset.addDataObject(new XMLDataTag("OperatorCode",
                                                 mGlobalInput.Operator));
        String tempSql = "select UserName from lduser where usercode='"
                         + mGlobalInput.Operator + "'";
        tSSRS = tExeSQL.execSQL(tempSql);
        tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS
                                                 .GetText(1, 1)));

        //        }
        //产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genInsuredList(tXMLDataset, printtype)) {
            return false;
        }
        //清单正本和清单副本使用
        if(printtype==1 || printtype==3){
        	//产生团单下被保人清单
            if (!genInsuredDetail(tXMLDataset)) {
                return false;
            }
        }
      //被保人清单
        if(printtype==4){
        	String tinsuredinfo=" Select Rownumber() Over() As Insuredid,Insuredstat State,(Select Case When Name Is Null Then (Select Insuredname From Lccont Where Contno = b.Contno) Else Name End  From Lcinsured b  Where b.Contno = Lcinsured.Contno  And Relationtomaininsured = '00') Maininsuredname,       Name Insuredname,       (Select Codename        From Ldcode        Where Codetype = 'relation'        And Code = Relationtomaininsured) As Relationname,       (Select Codename        From Ldcode        Where Codetype = 'sex'        And Code = Sex) As Sexname,       Birthday,       (Case Idtype         When '0' Then          '身份证'         When '1' Then          '护照'    when 'a' then '港澳居民居住证' when 'b' then '台湾居民居住证'     Else          '其它'       End) As Idname,       Idno,       Englishname,       (Select Insuredappage        From Lcpol Lcp        Where Lcp.Contno = Lcinsured.Contno        And Lcp.Insuredno = Lcinsured.Insuredno Fetch First Row Only) Insuredappage From Lcinsured Where Grpcontno = '"+mLCGrpContSchema.getGrpContNo()+"'And Insuredno = '"+minsuredid+"'And Name Not In ('公共账户', '无名单')Order By Integer(Diskimportno) With Ur";
        	SSRS tinsinfo=tExeSQL.execSQL(tinsuredinfo);
        	//产生团单下被保人清单
            if (tinsinfo!=null && tinsinfo.MaxRow>0) {
            	Element tindetail=new Element("LCInsuredDetail");
            	for(int i=0;i<tinsinfo.MaxNumber;i++){
            		Element tCols=new Element("COL0"+i);
    				tCols.setText(tinsinfo.GetText(1,i+1));
    				tindetail.addContent(tCols);	
            	}
            	tXMLDataset.getElementNoClone().addContent(tindetail);
            }
        }
        //获取EndInfo
        if (!genEndInfo(tXMLDataset,printtype)) {
            return false;
        }
        //标记
        Element tFlagSub=new Element("FlagSub");
        Element tPageFlag=new Element("PageFlag");
        tPageFlag.setText(String.valueOf(printtype));
        tFlagSub.addContent(tPageFlag);
        tXMLDataset.getElementNoClone().addContent(tFlagSub);
        return true;
    }
    
    private boolean genEndInfo(XMLDataset cXmlDataset,int printtype){
    	/** 增加签单机构 */
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
        if (!tLDComDB.getInfo()) {
            buildError("getGrpPolDataSet", "查询失败！");
            return false;
        }
    	Element tendinfo=new Element("EndInfo");
        Element tSignCom=new Element("SignCom");
        tSignCom.setText(tLDComDB
                .getLetterServiceName());
        tendinfo.addContent(tSignCom);
        Element tSignComEnName=new Element("SignComEnName");
        tSignComEnName.setText(tLDComDB
                .getEName());
        tendinfo.addContent(tSignComEnName);
        Element tSignDate=new Element("LCGrpCont.SignDate");
        tSignDate.setText(mLCGrpContSchema.getSignDate());
        tendinfo.addContent(tSignDate);
        Element tLAAgentName=new Element("LAAgent.Name");
        tLAAgentName.setText(mLAAgentSchema.getName());
        tendinfo.addContent(tLAAgentName);
        Element tOperator=new Element("LCGrpCont.Operator");
        tOperator.setText(mLCGrpContSchema.getOperator());
        tendinfo.addContent(tOperator);
        Element tLAComName=new Element("LACom.Name");
        tLAComName.setText(mLAComSchema.getName());
        tendinfo.addContent(tLAComName);
        Element tLAAgenttempName=new Element("LAAgenttemp.Name");
        tLAAgenttempName.setText(mLAAgenttempSchema.getName());
        tendinfo.addContent(tLAAgenttempName);
        Element tServicePostAddress=new Element("ServicePostAddress");
        tServicePostAddress.setText(tLDComDB.getServicePostAddress());
        tendinfo.addContent(tServicePostAddress);
        Element tServicePhone=new Element("ServicePhone");
        tServicePhone.setText(tLDComDB
                .getPhone());
        tendinfo.addContent(tServicePhone);
        String tempSql = "select UserName from lduser where usercode='"
                + mGlobalInput.Operator + "'";
       SSRS tSSRS = new ExeSQL().execSQL(tempSql);
        if(printtype==0 || printtype==2){
        	Element tSumPrem=new Element("LCGrpCont.SumPrem");
            tSumPrem.setText(String.valueOf(mLCGrpContSchema.getSumPrem()));
            tendinfo.addContent(tSumPrem);
            Element tRemark=new Element("LCGrpCont.Remark");
            tRemark.setText(mLCGrpContSchema.getRemark());
            tendinfo.addContent(tRemark);
            Element ttOperator=new Element("Operator");
            ttOperator.setText(tSSRS.GetText(1, 1));
            tendinfo.addContent(ttOperator);
        }
        if(printtype==1 || printtype==3){
        	Element ttOperator=new Element("Operator");
            ttOperator.setText(tSSRS.GetText(1, 1));
            tendinfo.addContent(ttOperator);
        	Element tPeoples2=new Element("LCGrpCont.Peoples2");
        	tPeoples2.setText(String.valueOf(mLCGrpContSchema.getPeoples2()));
            tendinfo.addContent(tPeoples2);
            Element tRemark=new Element("LCGrpCont.Remark");
            tRemark.setText(mLCGrpContSchema.getRemark());
            tendinfo.addContent(tRemark);
            
        }
        if(printtype==4){
        	Element tSumPrem=new Element("LCGrpCont.SumPrem");
            tSumPrem.setText(String.valueOf(mLCGrpContSchema.getSumPrem()));
            tendinfo.addContent(tSumPrem);
            Element tRemark=new Element("LCGrpCont.Remark");
            tRemark.setText(mLCGrpContSchema.getRemark());
            tendinfo.addContent(tRemark);
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++) {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size()) {
                    Nation += ",";
                }
            }
            Element tNation=new Element("Nation");
            tNation.setText(Nation);
            tendinfo.addContent(tNation);
            Element ttOperator=new Element("Operator");
            ttOperator.setText(tSSRS.GetText(1, 1));
            tendinfo.addContent(ttOperator);
            
        }
        cXmlDataset.getElementNoClone().addContent(tendinfo);
    	return true;
    }

    /**
     * 根据团单合同号，查询明细信息
     * 责任信息、特约信息、个人险种明细、险种条款信息等
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredList(XMLDataset cXmlDataset,int printtype) throws Exception {
    	ExeSQL tExeSQL = new ExeSQL();
    	String tpolsql="";
    	if(printtype==1 || printtype==3){
    		 tpolsql=" select riskcode ,(select riskname from lmriskapp lm where lm.riskcode=lgp.riskcode ),Prem from lcgrppol lgp where grpcontno='"+mLCGrpContSchema.getGrpContNo()+"'  order by riskcode desc ";
    	}else{
    		 tpolsql=" select riskcode ,(select riskname from lmriskapp lm where lm.riskcode=lgp.riskcode ),Prem from lcgrppol lgp where grpcontno='"+mLCGrpContSchema.getGrpContNo()+"' ";
    	}
    	
    	SSRS tresult=tExeSQL.execSQL(tpolsql);
    	if(tresult!=null && tresult.MaxRow >0){
    		Element tlcpol =new Element("LCPol");
    		for(int i=1;i<=tresult.MaxRow;i++){
    			Element trow=new Element("ROW");
    			Element triskcode=new Element("RiskCode");
    			triskcode.setText(tresult.GetText(i, 1));
    			trow.addContent(triskcode);
    			Element triskname=new Element("RiskName");
    			triskname.setText(tresult.GetText(i, 2));
    			trow.addContent(triskname);
    			Element tsumprem=new Element("SumPrem");
    			tsumprem.setText(tresult.GetText(i, 3));
    			trow.addContent(tsumprem);
    			String tlcdutysql="";
    			if("1502".equals(tresult.GetText(i, 1))){
    				tlcdutysql="select to_zero(A) A,to_zero(B) B,to_zero(C) C,to_zero(D) D,to_zero(E) E,to_zero(F) F,to_zero(G) G,to_zero(H) H, to_zero(I) I from ( select (select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode in ('613215')) A,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613209') B,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613217') C,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613204') D,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613213') E,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613214') F,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613216') G,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613205') H, (select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='613203') I from LCGrpCont Z where Z.GrpContNO = '"+mLCGrpContSchema.getGrpContNo()+"' ) as x ";
    			}else if("551201".equals(tresult.GetText(i, 1))){
    				tlcdutysql="select to_zero(max(amnt)) as Amnt1 from LCPol where grpcontno='"+mLCGrpContSchema.getGrpContNo()+"' and riskcode='551201'" ;
    			}
    			else{
    				tlcdutysql="select to_zero(A) A,to_zero(B) B,to_zero(C) C,to_zero(D) D,to_zero(E) E,to_zero(F) F,to_zero(G) G,to_zero(H) H, to_zero(I) I from ( select (select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode in ('611201')) A,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611202') B,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611203') C,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611204') D,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611205') E,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611206') F,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611207') G,(select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611208') H, (select avg(standmoney) from lcget where GrpContNo=Z.GrpContNo and getdutycode='611209') I from LCGrpCont Z where Z.GrpContNO = '"+mLCGrpContSchema.getGrpContNo()+"' ) as x ";
    			}
    			SSRS tdutyresult=tExeSQL.execSQL(tlcdutysql);
    			if(tdutyresult!=null && tdutyresult.MaxRow > 0){
    				Element tlcduty=new Element("LCDuty");
    				for(int j=0 ;j<tdutyresult.MaxNumber;j++){
        				Element tCols=new Element("COL0"+j);
        				tCols.setText(tdutyresult.GetText(1,j+1));
        				tlcduty.addContent(tCols);	
    				}
    				trow.addContent(tlcduty);
    			}else{
    				buildError("genInsuredList", "给付责任信息查询失败！");
    	            return false;
    			}
    			tlcpol.addContent(trow);
    			//tlcpol.addChild(trow);
    		}
	    	cXmlDataset.getElementNoClone().addContent(tlcpol);
	    	//cXmlDataset.getElementNoClone().addChild(tlcpol);
	    	XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");
	    	System.out.println("生成的险种报文："+outputter.outputString(tlcpol));
	        System.out.println("生成的报文："+outputter.outputString(cXmlDataset.getElement()));
    	}else{
    		buildError("genInsuredList", "险种信息查询失败！");
            return false;
    	}
    	
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsured2.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredDetail", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 取得团单下的全部LCGrpPol表数据
     * @param cGrpContNo String
     * @return LCGrpPolSet
     * @throws Exception
     */
    private static LCGrpPolSet getRiskList(String cGrpContNo) throws Exception {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();

        tLCGrpPolDB.setGrpContNo(cGrpContNo);
        //由于LCGrpCont和LCGrpPol为一对多的关系，所以采用query方法
        tLCGrpPolSet = tLCGrpPolDB.query();

        return tLCGrpPolSet;
    }

    /**
     * 获取补打保单的数据
     * @return boolean
     * @throws Exception
     */
    private boolean getRePrintData() throws Exception {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        //查询LCPolPrint表，获取要补打的保单数据
        String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '"
                      + mLCGrpContSchema.getGrpContNo() + "'";
        System.out.println(tSql);
        tSSRS = tExeSQL.execSQL(tSql);

        if (tExeSQL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tExeSQL.mErrors);
            throw new Exception("获取打印数据失败");
        }
        if (tSSRS.MaxRow < 1) {
            throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
        }

        mResult.clear();
        mResult.add(mLCGrpContSchema);

        // 取打印数据
        Connection conn = null;

        try {
            DOMBuilder tDOMBuilder = new DOMBuilder();
            Element tRootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null) {
                throw new Exception("连接数据库失败！");
            }

            //            String tSql = "";
            java.sql.Blob tBlob = null;
            //            CDB2Blob tCDB2Blob = new CDB2Blob();

            tSql = " and MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);

            if (tBlob == null) {
                throw new Exception("找不到打印数据");
            }

            //BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            Element tElement = tDOMBuilder.build(tBlob.getBinaryStream())
                               .getRootElement();
            tElement = new Element("DATASET").setMixedContent(tElement
                    .getMixedContent());
            tRootElement.addContent(tElement);

            ByteArrayOutputStream tByteArrayOutputStream = new
                    ByteArrayOutputStream();
            XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "GB2312");
            tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

            //            mResult.clear();
            //            mResult.add(new ByteArrayInputStream(tByteArrayOutputStream.toByteArray()));
            ByteArrayInputStream tByteArrayInputStream = new
                    ByteArrayInputStream(
                            tByteArrayOutputStream.toByteArray());

            //仅仅重新生成xml打印数据，影印件信息暂时不重新获取
            String FilePath = mOutXmlPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists()) {
                mFile.mkdir();
            }
            //根据合同号生成打印数据存放文件夹
            FilePath = mOutXmlPath + "/printdata" + "/"
                       + mLCGrpContSchema.getGrpContNo();
            mFile = new File(FilePath);
            if (!mFile.exists()) {
                mFile.mkdir();
            }

            //根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mLCGrpContSchema.getGrpContNo()
                             + ".xml";
            FileOutputStream fos = new FileOutputStream(XmlFile);
            //此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            while ((n = tByteArrayInputStream.read()) != -1) {
                fos.write(n);
            }
            fos.close();

            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                // do nothing
            }

            throw ex;
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue) {
        return new DecimalFormat("0.00").format(dValue);
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLOPRTManagerSchema.setOtherNoType("02");
        mLOPRTManagerSchema.setCode(mLOPRTManagerSchema.getCode());
        mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

	public CErrors getErrors() {
		// TODO Auto-generated method stub
		
		return mErrors;
	}


}
