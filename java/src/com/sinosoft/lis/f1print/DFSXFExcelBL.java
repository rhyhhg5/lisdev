package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class DFSXFExcelBL {

		/** 错误处理类，每个需要错误处理的类中都放置该类 */
		public CErrors mErrors = new CErrors();
		private String mDay[] = null; // 获取时间
		private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
		private String tOutXmlPath = "";
		private String msql = "";

		public DFSXFExcelBL() {
		}

		public static void main(String[] args) {
			GlobalInput tG = new GlobalInput();
			tG.Operator = "cwad1";
			tG.ManageCom = "86";
			VData vData = new VData();
			vData.addElement(tG);

			DFSXFExcelBL DFSXFBL = new DFSXFExcelBL();
			DFSXFBL.submitData(vData, "PRINT");

		}

		/**
		 * 传输数据的公共方法
		 */
		public boolean submitData(VData cInputData, String cOperate) {
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}
			if (cOperate.equals("PRINT")) { // 打印提数
				if (!getPrintData()) {
					return false;
				}
			}
			return true;
		}

		/**
		 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		 */
		private boolean getInputData(VData cInputData) { // 打印付费
			// 全局变量
			mDay = (String[]) cInputData.get(0);
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
			tOutXmlPath = (String)cInputData.get(2);
			
			if (mGlobalInput == null) {
				buildError("getInputData", "没有得到足够的信息！");
				return false;
			}
			if (tOutXmlPath == null || tOutXmlPath.equals("")) {
				buildError("getInputData", "没有得到文件路径的信息！");
				return false;
			}
			return true;
		}

		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "DFSXFExcelBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			this.mErrors.addOneError(cError);
		}

		/**
		 * 新的打印数据方法
		 *
		 * @return
		 */
		private boolean getPrintData() {
			
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
//			String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
//			tSSRS = tExeSQL.execSQL(nsql);
//			String manageCom = tSSRS.GetText(1, 1);
			
			
			String mmanagecom=getManagecom(mGlobalInput.ManageCom);
			//明细打印

			msql="select "
				+"		 d.name,"
				+"       a.RiskCode,"
				+"       a.Transmoney,"
				+"       a.chargerate,"
				+"       a.charge,"
				+"		 (select b.ManageCom from Ljfiget b, ljAGet c where a.Receiptno = b.otherno"
				+"   and b.ActuGetNo = c.ActugetNo and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"  and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"   and b.EnterAccDate <= '"+mDay[1]+"' fetch first 1 rows only )"
				+"        ManageCom,"
				+"		(select c.ManageCom from Ljfiget b, ljAGet c where a.Receiptno = b.otherno"
				+"   and b.ActuGetNo = c.ActugetNo and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"   and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"  and b.EnterAccDate <= '"+mDay[1]+"' fetch first 1 rows only )	"	
				+"       ManageCom "	
				+" from LAcharge a, lacom d "
				+" where   1 = 1 and "
				+" a.charge <> 0 and   a.AgentCom = d.AgentCom "
				+" and "
				+"	exists (select 1 from  Ljfiget b, ljAGet c where  "
				+"	a.Receiptno = b.otherno "
				+"   and b.ActuGetNo = c.ActugetNo "
				+"   and a.commisionsn=c.getnoticeno "
				+"   and c.othernoType in ('BC', 'AC') "
				+"   and (c.FinState != 'FC' or c.FinState is null) "
				+"   and (c.confDate is not null and c.EnterAccDate is not null) "
				+"   and b.policycom like '"+mmanagecom+"%'"
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null) "
				+"   and b.EnterAccDate >= '"+mDay[0]+"' "
				+"  and b.EnterAccDate <= '"+mDay[1]+"' "
				+" ) ";
			tSSRS = tExeSQL.execSQL(msql);
            
            String[][] mToExcel = new String[tSSRS.MaxRow+10][7];
            mToExcel[1][0] = "财务代付手续费日结单";
            mToExcel[2][0] = "代付日期：";
            mToExcel[2][1] = mDay[0]+"至"+mDay[1];
            mToExcel[4][0] = "代理机构名称";
            mToExcel[4][1] = "险种";
            mToExcel[4][2] = "保费收入";
            mToExcel[4][3] = "手续费比例";
            mToExcel[4][4] = "代付手续费金额";
            mToExcel[4][5] = "手续费支出所属机构";
            mToExcel[4][6] = "应付机构";
            
            int no = 5; //初始化打印行数
            
			for (int row = 1; row <= tSSRS.MaxRow; row++) {
				for (int col = 1; col <= tSSRS.MaxCol; col++) {
					if (col == 3) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					if (col == 4) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					if (col == 5) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
				}
			}
			no= no+tSSRS.MaxRow;
			//汇总打印
			
			String tsql="select db2inst1.nvl(sum(a.charge),0)  "
				+" from LAcharge a, lacom d"
				+" where   1 = 1 and"
				+" a.charge <> 0 and   a.AgentCom = d.AgentCom "
				+" and"
				+"	exists (select 1 from  Ljfiget b, ljAGet c where "
				+"	a.Receiptno = b.otherno"
				+"  and b.ActuGetNo = c.ActugetNo"
				+"  and a.commisionsn=c.getnoticeno "
				+"   and c.othernoType in ('BC', 'AC')"
				+"   and (c.FinState != 'FC' or c.FinState is null)"
				+"   and (c.confDate is not null and c.EnterAccDate is not null)"
				+"   and b.policycom like '"+mmanagecom+"%' "
				+"   and (c.bankonthewayflag = '0' or c.bankonthewayflag is null)"
				+"   and b.EnterAccDate >= '"+mDay[0]+"'"
				+"   and b.EnterAccDate <= '"+mDay[1]+"')";
			tSSRS = tExeSQL.execSQL(tsql);
			mToExcel[no][0] = "合计：";
			mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
			mToExcel[no+2][0] = "制表员：";
			mToExcel[no+2][4] = "审核员：";
			
			try{
				System.out.println("DFSXF-tOutXmlPath:"+tOutXmlPath);
				WriteToExcel t = new WriteToExcel("");
				t.createExcelFile();
				String[] sheetName = {PubFun.getCurrentDate()};
				t.addSheet(sheetName);
				t.setData(0, mToExcel);
				t.write(tOutXmlPath);
				System.out.println("生成DFSXF文件完成");
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				buildError("getPrintData", e.toString());
			}
			return true;
		}
         
         // *获取 修改之后的机构代码
           
		private String getManagecom(String managecom){
			String tmanagecom=null;
			if(managecom.endsWith("0000")){
				return tmanagecom=managecom.substring(1, 4)	;
			}else{
				return tmanagecom=managecom;
			}
		}
}
