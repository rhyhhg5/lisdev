package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintPauseContNoticeUI {
	 public CErrors mErrors = new CErrors();

	    public PrintPauseContNoticeUI()
	    {
	    }

	    /**
	     *
	     * @param cInputData VData：包含
	     1、	TransferData对象：包括查询Sql
	     2、	GlobalInput对象：完整的操作员信息。
	     * @param operate String：操作方式，可为空串“”
	     * @return XmlExport:打印清单数据，失败为null
	     */
	    public XmlExport getXmlExport(VData cInputData, String cOperate)
	    {
	    	PrintPauseContNoticeBL bl = new PrintPauseContNoticeBL();

	        XmlExport xml = bl.getXmlExport(cInputData, cOperate);
	        if(xml == null)
	        {
	            mErrors = bl.mErrors;
	            return null;
	        }

	        return xml;
	    }
}
