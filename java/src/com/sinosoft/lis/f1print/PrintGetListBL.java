package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * function :
 * @version 1.0
 * @date
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PrintGetListBL
{
    public  CErrors mErrors=new CErrors();
    private GlobalInput mGlobalInput;
    private String mSql;
    private XmlExport mXmlExport=new XmlExport();

    public PrintGetListBL()
    {
    }
    public XmlExport getXmlExport(VData data, String operate)
    {
        if(getInputData(data)==false)
        {
            mXmlExport=null;

        }
        else if(dealData()==false)
             {
                mXmlExport=null;
             }

        return mXmlExport;
    }
    public boolean dealData()
    {
        String sql = "select contno,(select name from lcinsured where contno = a.contno and insuredno = a.insuredno),'',getnoticeno,"
                     + " (select shoulddate from ljaget where actugetno = a.getnoticeno),"
                     + " nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
                     + " where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = a.contno and b.insuredno =a.insuredno ),0),'',sum(getmoney), "
                     + " (case when not exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付确认' else "
                     + "(select case when b.dealstate='0' and c.confdate is null then '待给付' when  c.confdate is not null then '给付完成' "
                     + "when dealstate ='2' then '已撤销' end from ljsget b ,ljaget c " 
                     + "where b.getnoticeno = a.getnoticeno and c.actugetno = a.getnoticeno fetch first 1 row only) end  ), "
                     + " (case when exists (select 1 from loprtmanager where code in ('bq001','MX001','MX002') and standbyflag2 = a.getnoticeno "
                     + " and printtimes >= 1) then '已打印' else '未打印' end),(select codename('paymode',paymode) from ljaget where actugetno = a.getnoticeno ), "
                     + " (select confdate from ljaget where actugetno = a.getnoticeno),(select bankcode from ljaget where actugetno = a.getnoticeno), "
                     + " (select bankACCNO from ljaget where actugetno = a.getnoticeno),(select ACCNAME from ljaget where actugetno = a.getnoticeno),AGENTGROUP,AGENTCODE  "
                     + " from ljsgetdraw a where 1=1 "
                 	 + " and feefinatype != 'YEI' "
                     + " and exists(select 1 from ljsget where a.getnoticeno = getnoticeno and othernotype ='20') "
					 + " and not exists (select 'Z' from ljsget where getnoticeno = a.getnoticeno and paymode = '5') and " 
                     //+ " not exists(select 1 from ljspayb where a.contno = otherno and dealstate ='1') and " //modify by fuxin 原来没有考虑财务数据所以，对已经续保成功产生的给付的保单不提
                     + mSql
                     + " group by contno,polno,insuredno,getnoticeno,agentgroup,agentcode with ur"
                     ;
        System.out.println("打印给付清单后台查询："+sql);
        SSRS tSSRS =new ExeSQL().execSQL(sql);
        if(tSSRS==null||tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到需要的清单数据");
            return false;
        }

        ListTable tlistTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[tSSRS.getMaxCol()+1];
            info[0]=""+i;
            String getnoticeno = tSSRS.GetText(i, 4);
            for(int position=1; position <= tSSRS.getMaxCol(); position++)
            {
                if(position==3)
                {
                    String sqlBnf = "select name from lcbnf where polno in (select polno from ljsgetdraw where getnoticeno ='"+getnoticeno+"')";
                    SSRS BnfSSRS = new ExeSQL().execSQL(sqlBnf);
                    String bnf = "";
                    for (int j = 1; j <= BnfSSRS.getMaxRow(); j++) {
                        bnf += BnfSSRS.GetText(j, 1) + " ";
                    }
                    info[position] = StrTool.cTrim(bnf);
                }else if (position==7)
                {
                    String sqlRisk = "select riskcode,getmoney from ljsgetdraw where getnoticeno ='"+getnoticeno+"'";
                    SSRS RiskSSRS = new ExeSQL().execSQL(sqlRisk);
                    String Risk = "";
                    for (int j = 1; j <= RiskSSRS.getMaxRow(); j++) {
                        Risk += "险种"+RiskSSRS.GetText(j, 1)+ "金额"+RiskSSRS.GetText(j, 2)+" ";
                    }
                    info[position] = StrTool.cTrim(Risk);

                }else
                {
                    info[position] = StrTool.cTrim(tSSRS.GetText(i, position));
                }
            }

            tlistTable.add(info);
        }

        tlistTable.setName("List");
        mXmlExport.createDocument("GetList.vts", "printer");
        mXmlExport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);
        mXmlExport.outputDocumentToFile("d:\\","failList");
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mSql = (String)cInputData.get(0);
        System.out.print("前台传入查询条件:"+mSql);
        if(mSql == null || mSql.equals(""))
        {
            mErrors.addOneError("请您在查询到清单后再执行打印程序。");
            return false;
        }
        mGlobalInput = (GlobalInput) cInputData.get(1);
        return true;
    }
    public static void main(String[] args)
   {
       PrintGetListBL test = new PrintGetListBL();
       test.mSql=args[0];
       test.dealData();
   }


}
