package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PDFRePrintReCheckUI {
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    public CErrors mErrors = new CErrors();
    private String mflag = null;
    /**
     * 提交方法
     * @param cInputData VData
     * @param flag String
     * @return boolean
     * 08/11/17 修改：单打也需要生成清单文件，所以单打也调用本类。
     * submitData(VData cInputData, String flag)中flag参数失去意义。
     */
    public boolean submitData(VData cInputData, String flag) {
        // 数据操作字符串拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = flag;

        System.out.println("---PDFRePrintReCheckBL BEGIN---");
        PDFRePrintReCheckBL tPDFRePrintReCheckBL = new PDFRePrintReCheckBL();

        if (tPDFRePrintReCheckBL.submitData(cInputData, flag) == false)
        {
        	// @@错误处理
            this.mErrors.copyAllErrors(tPDFRePrintReCheckBL.mErrors);
            return false;
        }
        else
        {
            mResult = tPDFRePrintReCheckBL.getResult();
        }
        System.out.println("---PDFRePrintReCheckBL END---");
        return true;
    }
    public VData getResult() {
        return mResult;
    }
}
