package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LBInsuredListDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LBInsuredListSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * 汇交件分单打印
 * 
 * @author zjd
 * 
 */
public class HJGrpSubContPrtBL implements PrintService {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mOperate = "";

	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LCGrpContSchema mGrpContInfo = null;

	private LCContSchema mGrpSubContInfo = null;

	private LBInsuredListSchema mGrpSCInsuListInfo = null;

	private LDComSchema mLDComSchema = null;

	private LCPolSet mGrpSCInsuPolInfo = null;

	private String mflag = null;

	public HJGrpSubContPrtBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;

		try {
			// 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
			if (!getInputData(cInputData)) {
				return false;
			}

			mResult.clear();

			// 准备所有要打印的数据
			if (!getPrintData()) {
				return false;
			}

			return true;

		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("submitData", ex.toString());
			return false;
		}
	}

	private String getDate(String cDate) {
		Date tDate = new FDate().getDate(cDate);
		if (tDate == null) {
			System.out.println("[" + cDate + "]日期转换失败");
			return cDate;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
		return df.format(tDate);
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
				.getObjectByObjectName("LOPRTManagerSchema", 0));
		if (!dealPrintManager()) {
			return false;
		}

		mflag = mOperate;

		return true;
	}

	// 得到返回值
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "CCSFirstPayBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	// 准备所有要打印的数据
	private boolean getPrintData() {
		XmlExport xmlExport = new XmlExport();

		// xmlExport.createDocument("HJGrpSubContPrint", "");
		xmlExport.setDocument(this.createDocument("HJGrpSubContPrint", ""));

		// 预设pdf接口节点。
		if (!dealPdfBaseInfo(xmlExport)) {
			return false;
		}
		// ------------------------------

		// 处理团体分单基本信息。
		if (!dealGrpSubContBaseInfo(xmlExport)) {
			return false;
		}
		// ------------------------------

		if (!getPolList(xmlExport)) {
			return false;
		}
		Document tnewxmlExport = xmlExport.getDocument();
		Element DataSetElement = new Element("DATASETS");
		Element newrootElement = (Element) tnewxmlExport.getRootElement()
				.clone();
		tnewxmlExport.setRootElement(DataSetElement.addContent(newrootElement));
		xmlExport.setDocument(tnewxmlExport);
		mResult.clear();
		mResult.addElement(xmlExport);

		System.out.println("生成的報文" + xmlExport.outputString());
		// 处理打印轨迹。
		mLOPRTManagerSchema.setStateFlag("1");
		mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
		mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
		mLOPRTManagerSchema
				.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);

		mResult.add(mLOPRTManagerSchema);
		// ------------------------------

		return true;
	}

	/**
	 * 获取团单信息
	 * 
	 * @param cGrpContNo
	 * @return
	 */
	private boolean loadGrpContInfo(String cGrpContNo) {
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(cGrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			return false;
		}
		mGrpContInfo = tLCGrpContDB.getSchema();
		return true;
	}

	/**
	 * 获取分单数据。
	 * 
	 * @param cContNo
	 * @return
	 */
	private boolean loadGrpSubContInfo(String cContNo) {
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(cContNo);
		if (!tLCContDB.getInfo()) {
			return false;
		}
		mGrpSubContInfo = tLCContDB.getSchema();
		return true;
	}

	/**
	 * 产生PDF打印相关基本信息。
	 * 
	 * @param cTextTag
	 * @return
	 */
	private boolean dealPdfBaseInfo(XmlExport cXmlExport) {
		TextTag tTmpTextTag = new TextTag();

		// 单证类型标志。
		tTmpTextTag.add("JetFormType", "J201");
		// -------------------------------

		// 四位管理机构代码。
		String sqlusercom = "select comcode from lduser where usercode='"
				+ mGlobalInput.Operator + "' with ur";
		String comcode = new ExeSQL().getOneValue(sqlusercom);
		if (comcode.equals("86") || comcode.equals("8600")
				|| comcode.equals("86000000")) {
			comcode = "86";
		} else if (comcode.length() >= 4) {
			comcode = comcode.substring(0, 4);
		} else {
			buildError("dealGrpContInfo", "操作员机构查询出错!");
			return false;
		}
		String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
				+ comcode + "' with ur";
		String printcode = new ExeSQL().getOneValue(printcom);

		tTmpTextTag.add("ManageComLength4", printcode);
		// -------------------------------

		// 客户端IP。
		tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
		// -------------------------------

		// 预览标志。
		if (mflag.equals("batch")) {
			tTmpTextTag.add("previewflag", "0");
		} else {
			tTmpTextTag.add("previewflag", "1");
		}
		// -------------------------------

		cXmlExport.addTextTag(tTmpTextTag);

		return true;
	}

	/**
	 * 处理团体保单层信息。
	 * 
	 * @param cTextTag
	 * @return
	 */
	private boolean dealGrpSubContBaseInfo(XmlExport cXmlExport) {
		TextTag tTmpTextTag = new TextTag();

		String tStrSql = " select ArchiveNo from ES_DOC_Main where DocCode = '"
				+ mGrpContInfo.getPrtNo()
				+ "' order by DocId fetch first 1 rows only ";
		String mArchiveNo = new ExeSQL().getOneValue(tStrSql);
		tTmpTextTag.add("ArchiveNo", mArchiveNo);
		tTmpTextTag.add("ContNo", mGrpSubContInfo.getContNo());
		tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
		// tTmpTextTag.add("ProposalContNo",
		// mGrpContInfo.getProposalGrpContNo());
		tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
		// tTmpTextTag.add("ContType", "2");//保单类型
		tTmpTextTag.add("MANAGECOM", mGlobalInput.ManageCom);// 签发机构
		tTmpTextTag.add("SUMPREM", getInsuPrem());// 保费
		tTmpTextTag.add("SIGNDATE", getDate(mGrpContInfo.getSignDate()));// 保费

		tTmpTextTag.add("SIGNCOM", mGrpContInfo.getSignCom());// 签发机构
		tTmpTextTag.add("SIGNCOMNAME",
				chgGrpManageCom(mGrpContInfo.getSignCom()));// 签发机构名称

		tTmpTextTag.add("SALECHNL", mGrpContInfo.getSaleChnl());
		tTmpTextTag.add("AGENTCOM", mGrpContInfo.getAgentCom());

		// tTmpTextTag.add("AGENTCODE", mGrpContInfo.getAgentCode());
		// tTmpTextTag.add("AGENTNAME", new
		// ExeSQL().getOneValue("select name from laagent where agentcode ='"+mGrpContInfo.getAgentCode()+"' "));
		//tTmpTextTag.add("AGENTCODE", mGrpContInfo.getAgentCode());
		String tTmpSql = " select laa.Name, labg.BranchAttr, labg.Name,laa.groupagentcode "
				+ " from LAAgent laa "
				+ " inner join LABranchGroup labg on labg.AgentGroup = laa.AgentGroup "
				+ " where laa.AgentCode = '" + mGrpContInfo.getAgentCode()
				+ "' ";
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(tTmpSql);
		if (tSSRS == null || tSSRS.getMaxRow() == 0) {
			buildError("dealGrpSubContBaseInfo", "未找到业务员相关信息。");
			return false;
		}
		String tAgentName = tSSRS.GetText(1, 1);
		tTmpTextTag.add("AGENTCODE", tSSRS.GetText(1, 4));
		tTmpTextTag.add("AGENTNAME", tAgentName);// 业务员姓名
		// 操作员信息
		tTmpTextTag.add("OPERATOR", mGlobalInput.Operator);
		tTmpTextTag.add("OPERATORNAME", getOperatorName(mGlobalInput.Operator));
		// tTmpTextTag.add("MANAGECOM", mGrpContInfo.getManageCom());
		// tTmpTextTag.add("MANAGECOMNAME",
		// chgGrpManageCom(mGrpContInfo.getManageCom()));
		// tTmpTextTag.add("SIGNDATE", mGrpSubContInfo.getSignDate());//签单日期
		// 投保人信息
		tTmpTextTag.add("APPNTNAME", mGrpSCInsuListInfo.getAppntName());
		tTmpTextTag.add("APPNTSEX",
				mGrpSCInsuListInfo.getAppntSex());
		tTmpTextTag.add("APPNTBIRTHDAY",
				getDate(mGrpSCInsuListInfo.getAppntBirthday()));
		tTmpTextTag.add("APPNTIDTYPE",
				mGrpSCInsuListInfo.getAppntIdType());
		tTmpTextTag.add("APPNTIDNO", mGrpSCInsuListInfo.getAppntIdNo());
		// 被保人信息
		// tTmpTextTag.add("InsuredNo", mGrpSCInsuListInfo.getInsuredNo());
		tTmpTextTag.add("INSUREDNAME", mGrpSCInsuListInfo.getInsuredName());
		tTmpTextTag.add("INSUREDSEX", mGrpSCInsuListInfo.getSex());
		tTmpTextTag.add("INSUREDBIRTHDAY",
				getDate(mGrpSCInsuListInfo.getBirthday()));
		tTmpTextTag.add("INSUREDIDTYPE",
				mGrpSCInsuListInfo.getIDType());
		tTmpTextTag.add("INSUREDIDNO", mGrpSCInsuListInfo.getIDNo());
		tTmpTextTag.add(
				"INSUREDAGE",
				getAppntAge(mGrpSCInsuListInfo.getGrpContNo(),
						mGrpSCInsuListInfo.getInsuredNo()) + "周岁");
		tTmpTextTag.add("SCHOOLNMAE", mGrpSCInsuListInfo.getSchoolNmae());
		tTmpTextTag.add("CLASSNAME", mGrpSCInsuListInfo.getClassName());
		tTmpTextTag.add("CVALIDATE", getDate(mGrpSubContInfo.getCValiDate()));// 保险有效起期  更换为分单的生效起期
		String tCInValiDate = PubFun.calDate(mGrpSubContInfo.getCInValiDate(),-1,"D",null);		
		tTmpTextTag.add("CINVALIDATE", getDate(tCInValiDate));// 保险有效止期  更换为分单的生效止期
		tTmpTextTag.add("REMARK", StrTool.cTrim(mGrpContInfo.getRemark())
				.equals("") ? "无" : mGrpContInfo.getRemark());
		// 管理机构信息
		tTmpTextTag.add("SERVICEPHONE", mLDComSchema.getServicePhone());// 客户服务热线
		tTmpTextTag.add("WEBADDRESS", mLDComSchema.getWebAddress());// 公司网址
		tTmpTextTag.add("ADDRESS", mLDComSchema.getAddress());// 公司地址
		tTmpTextTag.add("ZIPCODE", mLDComSchema.getZipCode());// 公司邮编

		cXmlExport.addTextTag(tTmpTextTag);

		return true;
	}

	/**
	 * 获取保障计划险种信息
	 * 
	 * @param cXmlDataset
	 *            XMLDataset
	 * @param cLCPolSet
	 *            LCPolSet
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getPolList(XmlExport cXmlExport) {

		System.out.println("查询险种责任信息开始。。。。。。");

		String[] tCashListInfoTitle = new String[12];
		tCashListInfoTitle[0] = "Row";
		tCashListInfoTitle[1] = "RiskCode";
		tCashListInfoTitle[2] = "RiskName";
		tCashListInfoTitle[3] = "DutyName";
		tCashListInfoTitle[4] = "Amnt";
		tCashListInfoTitle[5] = "Mult";
		tCashListInfoTitle[6] = "Copys";
		tCashListInfoTitle[7] = "MaxMedFee";
		tCashListInfoTitle[8] = "GetLimit";
		tCashListInfoTitle[9] = "GetRate";
		tCashListInfoTitle[10] = "Prem";
		tCashListInfoTitle[11] = "ContPlanCode";

		String tSql = " Select Lc.Riskcode, (Select Lm.Riskname  From Lmrisk Lm  Where Lm.Riskcode = Lc.Riskcode), "
			        + " (Select Dutyname  From Lmduty Lmd  Where Lmd.Dutycode = Lcd.Dutycode), Lcd.Amnt, Lcd.Mult, Lcd.Copys,'', Lcd.Getlimit, Lcd.Getrate, Lcd.Prem, "
			        + " (Select Contplancode  From Lcinsured  Where Contno = Lc.Contno  And Insuredno = Lc.Insuredno)  "
			        + " From Lcpol Lc  "
			        + " Inner Join Lcduty Lcd On Lc.Polno = Lcd.Polno  "
			        + " Inner Join Lmriskapp Lmr On Lc.Riskcode = Lmr.Riskcode  " 
			        + " Inner Join ldcode ldc on ldc.codetype = 'grphjrisk' and code = lc.riskcode "
			        + " Where Lc.Contno = '"+mGrpSubContInfo.getContNo()+"'  "
			        + " Order By ldc.comcode,Lmr.Subriskflag";
		ExeSQL tExeSQL2 = new ExeSQL();
		SSRS tSSRS2 = tExeSQL2.execSQL(tSql);
		if (tSSRS2.MaxRow > 0) {

			ListTable tListTable = new ListTable();
			tListTable.setName("RISKLIST");

			System.out.println("ContPrintType"
					+ mGrpContInfo.getContPrintType());
			if (mGrpContInfo.getContPrintType().equals("5")) {

				for (int j = 1; j <= tSSRS2.MaxRow; j++) {
					String[] oneCashInfo = null;
					oneCashInfo = new String[12];

						oneCashInfo[0] = ""+j;
						if(!"".equals(tSSRS2.GetText(j, 1).trim()) && tSSRS2.GetText(j, 1)!=null){
							oneCashInfo[1] = tSSRS2.GetText(j, 1);
                    	}else{
                    		oneCashInfo[1] = "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 2).trim()) && tSSRS2.GetText(j, 2)!=null){
							oneCashInfo[2] = tSSRS2.GetText(j, 2);
                    	}else{
                    		oneCashInfo[2] = "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 3).trim()) && tSSRS2.GetText(j, 3)!=null){
							oneCashInfo[3] = tSSRS2.GetText(j, 3);
                    	}else{
                    		oneCashInfo[3] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 4).trim()) && tSSRS2.GetText(j, 4)!=null ){
							oneCashInfo[4] = tSSRS2.GetText(j, 4);
							if("121301".equals(oneCashInfo[1])){
								oneCashInfo[4] =  "--";
							}
                    	}else{
                    		oneCashInfo[4] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 5).trim()) && tSSRS2.GetText(j, 5)!=null && !"0".equals(tSSRS2.GetText(j, 5))){
							oneCashInfo[5] = tSSRS2.GetText(j, 5);
                    	}else{
                    		oneCashInfo[5] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 6).trim()) && tSSRS2.GetText(j, 6)!=null && !"0".equals(tSSRS2.GetText(j, 6))){
							oneCashInfo[6] = tSSRS2.GetText(j, 6);
                    	}else{
                    		oneCashInfo[6] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 7).trim()) && tSSRS2.GetText(j, 7)!=null){
							oneCashInfo[7] = tSSRS2.GetText(j,7);
                    	}else{
                    		oneCashInfo[7] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 8).trim()) && tSSRS2.GetText(j, 8)!=null){
							oneCashInfo[8] = tSSRS2.GetText(j, 8);
							if("121301".equals(oneCashInfo[1])){
								oneCashInfo[8] =  "--";
							}
                    	}else{
                    		oneCashInfo[8] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 9).trim()) && tSSRS2.GetText(j, 9)!=null){
							oneCashInfo[9] = tSSRS2.GetText(j, 9);
							System.out.println("计算出的值" + tSSRS2.GetText(j, 9));
							System.out.println("xml存储的值" + oneCashInfo[9]);
							if("121301".equals(oneCashInfo[1])){
								oneCashInfo[9] =  "--";
							}
                    	}else{
                    		oneCashInfo[9] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 10).trim()) && tSSRS2.GetText(j, 10)!=null){
							oneCashInfo[10] = tSSRS2.GetText(j, 10);
                    	}else{
                    		oneCashInfo[10] =  "--";
                    	}
						if(!"".equals(tSSRS2.GetText(j, 11).trim()) && tSSRS2.GetText(j, 11)!=null){
							oneCashInfo[11] = tSSRS2.GetText(j, 11);
                    	}else{
                    		oneCashInfo[11] =  "--";
                    	}
						
						tListTable.add(oneCashInfo);
				}

			}
			cXmlExport.addListTableHJ(tListTable, tCashListInfoTitle);
		} else {
			System.out.println("查询险种信息失败！");
			buildError("getPolList", "查询险种信息失败 !");
			return false;
		}

		return true;

	}

	/**
	 * 获取汇交投保人数据。
	 * 
	 * @param cContNo
	 * @return
	 */
	private boolean loadGrpSCInsuListInfo(String cContNo, String cInsuredNo) {
		LBInsuredListDB tInsuListDB = new LBInsuredListDB();
		tInsuListDB.setGrpContNo(cContNo);
		tInsuListDB.setInsuredNo(cInsuredNo);
		// if (!tInsuListDB.getInfo())
		// {
		// return false;
		// }
		mGrpSCInsuListInfo = tInsuListDB.query().get(1);
		return true;
	}

	/**
	 * 
	 * @return LOPRTManagerDB
	 * @throws Exception
	 */
	private boolean dealPrintManager() {
		String tContNo = mLOPRTManagerSchema.getStandbyFlag1();

		if (!loadGrpSubContInfo(tContNo)) {
			buildError("dealPrintManager", "获取分单数据失败！");
			return false;
		}

		String tGrpContNo = mGrpSubContInfo.getGrpContNo();

		if (!loadGrpContInfo(tGrpContNo)) {
			buildError("dealPrintManager", "获取团单数据失败！");
			return false;
		}

		String tInsuredNo = mLOPRTManagerSchema.getStandbyFlag2();

		if (!loadGrpSCInsuListInfo(tGrpContNo, tInsuredNo)) {
			buildError("dealPrintManager", "获取投保人数据失败！");
			return false;
		}
		// 获取被保人险种信息
		if (!loadGrpSubInsuPol(tContNo, tInsuredNo)) {
			buildError("dealPrintManager", "获取被保人险种数据失败！");
			return false;
		}

		// 管理机构信息
		if (!getLDcom(mGlobalInput.ManageCom)) {
			buildError("dealPrintManager", "获取投保人数据失败！");
			return false;
		}

		String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
		if (tStrPrtSeq == null || tStrPrtSeq.equals("")) {
			buildError("dealPrintManager", "生成打印流水号失败。");
			return false;
		}

		mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

		mLOPRTManagerSchema.setOtherNoType("16");

		mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
		mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
		mLOPRTManagerSchema.setPrtType("0");
		mLOPRTManagerSchema.setStateFlag("1");

		mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

		return true;
	}

	/**
	 * 获取被保人险种信息。
	 * 
	 * @return
	 */
	private boolean loadGrpSubInsuPol(String cContNo, String cInsuredNo) {
		LCPolDB tLCPolDB = new LCPolDB();
		String tStrSql = " select * from LCPol lcp where lcp.ContNo = '"
				+ mGrpSubContInfo.getContNo() + "' " + " and lcp.InsuredNo = '"
				+ mGrpSCInsuListInfo.getInsuredNo() + "' ";
		mGrpSCInsuPolInfo = tLCPolDB.executeQuery(tStrSql);
		if (mGrpSCInsuPolInfo == null || mGrpSCInsuPolInfo.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * 获取证件号对应中文说明
	 * 
	 * @param cIDType
	 * @return
	 */
	private String getIDTypeName(String cIDType) {
		String tStrSql = " select CodeName from LDCode "
				+ " where CodeType = 'idtype' and Code = '" + cIDType + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取性别对应中文说明
	 * 
	 * @param cIDType
	 * @return
	 */
	private String getSexName(String sex) {
		String tStrSql = " select CodeName from LDCode "
				+ " where CodeType = 'sex' and Code = '" + sex + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取被保人投保年龄
	 * 
	 * @param cIDType
	 * @return
	 */
	private String getAppntAge(String cGrpContNO, String cInsuNo) {
		String tStrSql = " select InsuredAppAge from LCPol "
				+ " where GrpContNo = '" + cGrpContNO + "' and InsuredNo = '"
				+ cInsuNo + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取操作员姓名
	 * 
	 * @param usercode
	 * @return
	 */
	private String getOperatorName(String cusercode) {
		String tStrSql = " select UserName from LDUser "
				+ " where UserCode = '" + cusercode + "' ";
		return new ExeSQL().getOneValue(tStrSql);
	}

	/**
	 * 获取被保人险种总保费。
	 * 
	 * @return
	 */
	private String getInsuPrem() {
		LCPolSet tPolInfoSet = mGrpSCInsuPolInfo;
		double tSumPrem = 0d;
		for (int i = 1; i <= tPolInfoSet.size(); i++) {
			LCPolSchema tTmpPol = tPolInfoSet.get(i);
			tSumPrem += tTmpPol.getPrem();
		}

		return format(tSumPrem);
	}

	/**
	 * 获取缴费频次代码对应中文含义。
	 * 
	 * @param cPayIntv
	 * @return
	 */
	private boolean getLDcom(String comcode) {
		LDComDB tLDComDB = new LDComDB();
		tLDComDB.setComCode(comcode);
		if (!tLDComDB.getInfo()) {
			return false;
		}
		mLDComSchema = tLDComDB.getSchema();
		return true;
	}

	/**
	 * 获取机构代码代码对应中文含义。
	 * 
	 * @param cPayIntv
	 * @return
	 */
	private String chgGrpManageCom(String cManageCom) {
		LDComDB tLDComDB=new LDComDB();
		tLDComDB.setComCode(cManageCom);
		if(tLDComDB.getInfo()){
			mLDComSchema=tLDComDB.getSchema();
			if("Y".equals(mLDComSchema.getInteractFlag())){
	        	tLDComDB.setComCode(cManageCom.substring(0, 4));
	        	if (tLDComDB.getInfo())
	            {
	                //buildError("getInfo", "查询管理机构信息失败！");
	                //return false;
	                mLDComSchema.setName(tLDComDB.getSchema().getName());
	            }else{
	            	buildError("getInfo", "查询管理机构信息失败！");
	            }
	        }
		}
		
		
		return mLDComSchema.getName();
	}

	/**
	 * 格式化浮点型数据
	 * 
	 * @param dValue
	 *            double
	 * @return String
	 */
	private static String format(double dValue) {
		return new DecimalFormat("0.00").format(dValue);
	}

	public CErrors getErrors() {
		return null;
	}

	public static void main(String args[]) {
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		tLOPRTManagerSchema.setOtherNo("2320909681" + "_" + "014060198");
		tLOPRTManagerSchema.setStandbyFlag1("2320909681");
		tLOPRTManagerSchema.setStandbyFlag2("014060198");
		tLOPRTManagerSchema.setCode("J201");

		String mOperator = "betch";

		GlobalInput tg = new GlobalInput();
		tg.ManageCom = "86110000";
		tg.Operator = "gzh";
		tg.ComCode = "86110000";
		tg.ServerIP = "localhsot";
		tg.ClientIP = "127.0.0.1";
		VData td = new VData();
		td.add(tLOPRTManagerSchema);
		td.add(tg);
		HJGrpSubContPrtBL tb = new HJGrpSubContPrtBL();
		tb.submitData(td, mOperator);
	}

	// 初始化文件，参数为模板名，打印机名
	public Document createDocument(String templatename, String printername) {

		// Create the root element
		// TemplateName=templatename;
		// Element DataSetsElement = new Element("DATASETS");

		Element DataSetElement = new Element("DATASET");
		// DataSetsElement.addContent(DataSetElement);
		// create the document
		Document myDocument = new Document(DataSetElement);
		// add some child elements

		// Note that this is the first approach to adding an element and
		// textual content. The second approach is commented out.

		Element CONTROL = new Element("CONTROL");
		DataSetElement.addContent(CONTROL);

		Element CONTTYPE = new Element("CONTTYPE");
		Element TEMPLATE = new Element("TEMPLATE");
		Element PRINTER = new Element("PRINTER");

		PRINTER.addContent(printername);
		TEMPLATE.addContent(templatename);
		CONTTYPE.addContent("2");
		CONTROL.addContent(CONTTYPE);
		CONTROL.addContent(TEMPLATE);
		CONTROL.addContent(PRINTER);
		CONTROL.addContent(new Element("DISPLAY"));
		return myDocument;
	}
}
