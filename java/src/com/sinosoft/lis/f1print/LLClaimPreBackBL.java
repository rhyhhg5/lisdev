package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLClaimPreBackBL implements PrintService {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
    private LLPrepaidGrpContSchema mLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
    private int index;
    private String mOperate = "";
    private String mAppntName = "";
    private String mAppntNo = "";
    private String mGrpContNo = "";
    private String mGrpPolNo = "";
    private String mStatement = "";
    private String mOperator = "";
    private String mDate = "";    
    private boolean mNeedStore = false;
    private String mOper = "";    

    public LLClaimPreBackBL() {
    }
    /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
      if (!getInputData(cInputData)) {
          return false;
      }
      mResult.clear();
      if (!dealData()) {
          return false;
      }
      // 准备所有要打印的数据
      if (!getPrintData()) {
          return false;
      }
      if (!dealPrintMag("DELETE&INSERT")) {
          return false;
      }    
      return true;
  }
  
  private boolean getInputData(VData cInputData) {
      mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
      ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
      
      if (ttLOPRTManagerSchema == null) 
      {
          System.out.println("---------为空--初次打印入口！");         
          buildError("getInputData", "为空--初次打印入口没有得到足够的信息！");
          return false;
      } else {
          //PDF入口
          System.out.println("---------不为空--PDF入口！");
          LLPrepaidGrpContDB mLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
          mLLPrepaidGrpContDB.setGrpContNo(ttLOPRTManagerSchema.getOtherNo());          
          if (!mLLPrepaidGrpContDB.getInfo()) {
              mErrors.addOneError("PDF入口LLPrepaidGrpCont传入的数据不完整！");
              return false;
          }
          mLLPrepaidGrpContSchema = mLLPrepaidGrpContDB.getSchema();
          mGrpContNo = mLLPrepaidGrpContSchema.getGrpContNo();
          return true;
      }
  }
  private boolean dealData() 
  {
	  LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
      LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
      //用保单和code（lp007）在打印管理表里查找给付凭证打印记录
      tLOPRTManagerDB.setOtherNo(mGrpContNo);
      tLOPRTManagerDB.setCode("lp007");
      tLOPRTManagerSet = tLOPRTManagerDB.query();
      if (tLOPRTManagerSet == null || tLOPRTManagerSet.size() == 0) {
          index = 0;
          mNeedStore = true;
      } else {
          if (tLOPRTManagerDB.mErrors.needDealError()) {
              mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
              buildError("dealData", "在取得LOPRTManager的数据时发生错误");
              return false;
          }
          //打印次数
          index = (int) tLOPRTManagerSet.get(1).getPrintTimes();
          mNeedStore = false;         
      }
      return true;
  }
  private boolean dealPrintMag(String cOperate) {
      MMap tMap = new MMap();
      tMap.put(mLOPRTManagerSchema, cOperate);
      VData tVData = new VData();
      tVData.add(tMap);
      PubSubmit tPubSubmit = new PubSubmit();
      if (tPubSubmit.submitData(tVData, "") == false) {
          this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
          return false;
      }
      return true;
  }
  private boolean getPrintData() 
  {
	  String tSql ="select GrpName,AppntNo from lcgrpcont where grpcontno='"+ mGrpContNo +"'"
	  			+" union select GrpName,AppntNo from lbgrpcont where grpcontno='"+ mGrpContNo +"'";
	  ExeSQL exeSQL = new ExeSQL();
      SSRS ssrs = exeSQL.execSQL(tSql);
      if(exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||ssrs.getMaxRow()<=0)
      {
    	  buildError("print", "保单"+mGrpContNo+"信息查询失败！");
          return false;
      }
      
      String CurrentDate = PubFun.getCurrentDate();
      int a = CurrentDate.lastIndexOf("-");
      String tYear = CurrentDate.substring(0, 4);
      String tMonth = CurrentDate.substring(5, a);
      String tDay = CurrentDate.substring(a + 1, CurrentDate.length());
      String tDate = tYear + "年" + tMonth + "月" + tDay + "日";
      mStatement="截至"+ tDate +"，保单号为"+ mGrpContNo +"的保单累计预付赔款" 
      			+ mLLPrepaidGrpContSchema.getApplyAmount() +"元，实际发生赔款"
      			+ Math.abs(mLLPrepaidGrpContSchema.getSumPay()) +"元，需收回预付赔款"
      			+ ttLOPRTManagerSchema.getStandbyFlag4()+"元(大写："+PubFun.getChnMoney(Double.parseDouble(ttLOPRTManagerSchema.getStandbyFlag4())) +"元)。";
      
	  TextTag texttag = new TextTag(); 
      XmlExport xmlexport = new XmlExport();
      //借操作员信息中的机构号存储打印所需要配置的机构号  
      String sqlusercom = "select comcode from lduser where usercode='" +
                        mGlobalInput.Operator + "' with ur";
      String comcode = new ExeSQL().getOneValue(sqlusercom);
      texttag.add("JetFormType", "lp007");
       if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
          comcode = "86";
       } else if (comcode.length() >= 4){
          comcode = comcode.substring(0, 4);
       } else {
          buildError("getInputData", "操作员机构查询出错！");
          return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

      texttag.add("ManageComLength4", printcode);
      texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));      
      texttag.add("previewflag", "1");
      xmlexport.createDocument("LLClaimPreGetNotice.vts", "printer"); //最好紧接着就初始化xml文档
      
      texttag.add("AppntName", ssrs.GetText(1, 1));
      texttag.add("AppntNo", ssrs.GetText(1, 2));
      texttag.add("GrpContNo", mGrpContNo);
      texttag.add("Statement", mStatement);
      texttag.add("Operator", mGlobalInput.Operator);     
      texttag.add("Date", PubFun.getCurrentDate());   
     
      if (texttag.size() > 0) {
          xmlexport.addTextTag(texttag);
      }
      xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
      mResult.clear();
      mResult.addElement(xmlexport);

      LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
      tLOPRTManagerDB.setOtherNo(mGrpContNo);
      tLOPRTManagerDB.setCode("lp007");

      String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
      String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
      mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
      mLOPRTManagerSchema.setOtherNo(mGrpContNo);
      mLOPRTManagerSchema.setOtherNoType("Y");
      mLOPRTManagerSchema.setCode("lp007");
      mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
      mLOPRTManagerSchema.setAgentCode("");
      mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
      mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
      mLOPRTManagerSchema.setPrtType("0");
      mLOPRTManagerSchema.setStateFlag("0");
      mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
      mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime()); 

      mResult.addElement(mLOPRTManagerSchema);
      return true;
  }
  public VData getResult() {
      return this.mResult;
  }
  
  private void buildError(String szFunc, String szErrMsg) {
      CError cError = new CError();

      cError.moduleName = "LCPolF1PBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }
  public CErrors getErrors() {
      return mErrors;
  }
  public static void main(String[] args) {
	  LLPrepaidGrpContDB mLLPrepaidGrpContDB= new LLPrepaidGrpContDB();
	  LLPrepaidGrpContSchema mLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
      mLLPrepaidGrpContDB.setGrpContNo("00003112000158");
	  mLLPrepaidGrpContSchema = mLLPrepaidGrpContDB.getSchema();      

	 String mStatement="元(大写："+PubFun.getChnMoney(mLLPrepaidGrpContSchema.getSumPay())+"元)";
	 System.out.println("mStatemen======="+ mStatement);
  }
}
