/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.Statement;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPrintDB;
import com.sinosoft.lis.db.LCContReceiveDB;
import com.sinosoft.lis.db.LCPolPrintDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContPrintSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.ContReceiveUI;
import com.sinosoft.lis.vdb.LCContPrintDBSet;
import com.sinosoft.lis.vdb.LCContReceiveDBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCContPrintSet;
import com.sinosoft.lis.vschema.LCContReceiveSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;

/**
 * <p>Title: </p>
 * <p>Description: 数据提交及xml流处理</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
public class LCContF1PBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private File mFile = null;

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";

    private String mContNo = "";

    private String mPrtNo = "";

    private int mPrintCount = 1;

    private VData mInputData;

    private String mcvalidate;

    private String mCardFlag = "";

    private String mdate;

    private String mtime;

    private MMap map = new MMap();

    private String mManageCom;

    private LCContSchema mLCContSchema = new LCContSchema();

    private GlobalInput mGlobalInput = new GlobalInput();

    //    private VData mResult = new VData();
    private VData mResult = new VData();

    private TransferData mTransferData = null;

    private boolean isULI = false;//个险万能标志

    private boolean isFilialePrint = false;//分公司打印标志
    
    private String PrintType = "";
	
	private String PadXmlFile;

    public LCContF1PBLS()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        try
        {
        	PrintType = (String) mInputData.get(8);
    }
    catch(Exception e)
    {
        //do nothing
    }
		System.out.println(PrintType);

        if (mOperate.equals("PRINT"))
        {
            return save(mInputData);
        }
        else if (mOperate.equals("REPRINT"))
        {
            return update(mInputData);
        }

        return true;
    }

    /**
     * 打印操作
     * @param mInputData VData
     * @return boolean
     */
    private boolean save(VData mInputData)
    {
        VData vData = new VData();
        MMap mMap = new MMap();
        boolean tReturn = true;
        System.out.println("Start Save...");
//        Connection conn = DBConnPool.getConnection();
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
        int picNo = 0;//图片张书，用于没有找到图片时的返回控制

//        if (conn == null)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "LCContF1PBLS";
//            tError.functionName = "saveData";
//            tError.errorMessage = "数据库连接失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        try
        {
//            conn.setAutoCommit(false);
            System.out.println("Start ....");
            // 获取要保存的数据
            XMLDatasets tXMLDatasets = (XMLDatasets) mInputData
                    .getObjectByObjectName("XMLDatasets", 0);
            mTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);

            GlobalInput tGlobalInput = (GlobalInput) mInputData
                    .getObjectByObjectName("GlobalInput", 0);
            mGlobalInput = tGlobalInput;
            //更新LCGrpCont表数据
//            LCContDB tLCContDB = new LCContDB(conn);
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setSchema((LCContSchema) mInputData
                    .getObjectByObjectName("LCContSchema", 0));
            String tWebPath = (String) mInputData.getObjectByObjectName(
                    "String", 0);

            mContNo = tLCContDB.getContNo();
            //@将打印的xml放入lccontprint表中
//            LCContDB ttLCContDB = new LCContDB(conn);
            LCContDB ttLCContDB = new LCContDB();
            ttLCContDB.setContNo(mContNo);
            LCContSet ttLCContSet = ttLCContDB.query();
            mLCContSchema = ttLCContSet.get(1).getSchema();
            mCardFlag = mLCContSchema.getCardFlag();
            mPrtNo = ttLCContSet.get(1).getPrtNo();
            mcvalidate = ttLCContSet.get(1).getCValiDate();
            mManageCom = ttLCContSet.get(1).getManageCom();
//            LCContPrintDB mLCContPrintDB = new LCContPrintDB(conn);
            LCContPrintDB mLCContPrintDB = new LCContPrintDB();
            mLCContPrintDB.setOtherNo(mContNo);
            LCContPrintSet mLCContPrintSet = new LCContPrintSet();
            mLCContPrintSet = mLCContPrintDB.query();
            System.out.println("mLCContPrintSet.size() is "
                    + mLCContPrintSet.size());
            if (mLCContPrintSet.size() > 0)
            {
                mPrintCount = mLCContPrintSet.get(1).getPrintCount();
                mPrintCount = mPrintCount + 1;
//                LCContPrintDBSet mLCContPrintDBSet = new LCContPrintDBSet(conn);
//                LCContPrintDBSet mLCContPrintDBSet = new LCContPrintDBSet();
//                mLCContPrintDBSet.set(mLCContPrintSet);
//                if (!mLCContPrintDBSet.delete())
//                {
//                    conn.rollback();
//                    conn.close();
//                    return false;
//                }
                mMap.put(mLCContPrintSet, "DELETE");    
                
            }
            //校验是否为个险万能保单
            if (mLCContSchema.getContType().equals("1")
                    && (mLCContSchema.getCardFlag().equals("0") || mLCContSchema
                            .getCardFlag().equals("9")|| mLCContSchema.getCardFlag().equals("c")|| mLCContSchema.getCardFlag().equals("d")
                            || mLCContSchema.getCardFlag().equals("e")|| mLCContSchema.getCardFlag().equals("f")))
            {
                String flag = new ExeSQL()
                        .getOneValue("select count(1) from LCPol a, LMRiskApp b where a.ContNo = '"
                                + mLCContSchema.getContNo()
                                + "' and a.RiskCode = b.RiskCode and b.RiskType4 = '4'");
                isULI = Integer.parseInt(flag) > 0;
            }
			 if (mLCContSchema.getContType().equals("1")
                    && (mLCContSchema.getCardFlag().equals("b"))){
            	String flag = new ExeSQL()
                .getOneValue("select count(1) from LCPol a, LMRiskApp b where a.ContNo = '"
                        + mLCContSchema.getContNo()
                        + "' and a.RiskCode = b.RiskCode and b.taxoptimal = 'Y'");
            	isULI = Integer.parseInt(flag) > 0;
            	
            	
            }

            //判断保单是否为分公司打印
            String sql = "select count(1) from LDCode1 a, LCCont b "
                    + "where a.CodeType = 'printchannel' and a.CodeName = '1' "
                    + "and a.Code = substr(b.ManageCom, 1, 4) and a.Code1 = b.ContType "
                    + "and b.ContNo = '"
                    + mLCContSchema.getContNo()
                    + "' and b.ContType = '1' and b.CardFlag in ('0', '9','c','d','e','f') "
                    + "and not exists (select 1 from LCPol c, LMRiskApp d "
                    + "where c.ContNo = b.ContNo and c.RiskCode = d.RiskCode and d.RiskType4 = '4')";
            isFilialePrint = new ExeSQL().getOneValue(sql).equals("1");

            String contPrintFlag = (String) mInputData.get(6);//保单是否打印标记：0，不打印；1，打印
            //将数据流的信息存入blob字段中，更换了数据库，目前只支持db2的blob数据插入
            //saveDataStream(tXMLDatasets, tGlobalInput, conn);
//            if (!tLCContDB.update())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLCContDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "LCContF1PBLS";
//                tError.functionName = "saveData";
//                tError.errorMessage = "更新lccont表信息失败!";
//                this.mErrors.addOneError(tError);
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            mMap.put(tLCContDB.getSchema(), "UPDATE");

            //文件路径
            //管理机构一层
            String FilePath = tWebPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            //根据合同号生成打印数据存放文件夹
            //            FilePath = tWebPath + "/printdata" + "/" + mContNo;
            FilePath = tWebPath + "/printdata" + "/data";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            InputStream ins = tXMLDatasets.getInputStream();
            //根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mContNo + ".xml";
            FileOutputStream fos = new FileOutputStream(XmlFile);
            //此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            //            while ((n = ins.read()) != -1)
            //            {
            //                fos.write(n);
            //            }
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1)
            {
                fos.write(c, 0, n);
            }
            fos.close();
	        if("1".equals(PrintType)){//PAD电子投保书需求modify by lxs
	        	  LCContF1PBLS tLCContF1PBLS = new LCContF1PBLS();
	        	InputStream mIs = new FileInputStream(XmlFile);
				byte[] mInXmlBytes = tLCContF1PBLS.InputStreamToBytes(mIs);
				PadXmlFile = new String(mInXmlBytes, "UTF-8");
				System.out.println(PadXmlFile);
                    }else{
                    	 LCContF1PBLS tLCContF1PBLS = new LCContF1PBLS();
         	        	InputStream mIs = new FileInputStream(XmlFile);
         				byte[] mInXmlBytes = tLCContF1PBLS.InputStreamToBytes(mIs);
         				PadXmlFile = new String(mInXmlBytes, "UTF-8");
         				System.out.println("打印报文===========："+PadXmlFile);
                    }
	     
            System.out.println("开始放入xml");
            System.out.println(XmlFile);
            LCContPrintSet tLCContPrintSet = new LCContPrintSet();
            LCContPrintSchema tLCContPrintSchema = new LCContPrintSchema();
            tLCContPrintSchema.setPrintID(PubFun1.CreateMaxNo("PrintID", 11));
            tLCContPrintSchema.setPrtNo(mPrtNo);
            tLCContPrintSchema.setOtherNo(mContNo);
            tLCContPrintSchema.setOtherNoType("1");
            tLCContPrintSchema.setPrintCount(mPrintCount);
            tLCContPrintSchema
                    .setFilePath("printdata/data/" + mContNo + ".xml");
            tLCContPrintSchema.setCValiDate(mcvalidate);
            tLCContPrintSchema.setManageCom(mManageCom);
            tLCContPrintSchema.setOperator(tGlobalInput.Operator);
            tLCContPrintSchema.setMakeDate(mdate);
            tLCContPrintSchema.setMakeTime(mtime);
            tLCContPrintSchema.setModifyDate(mdate);
            tLCContPrintSchema.setModifyTime(mtime);
            tLCContPrintSchema.setPrintType(PrintType);
            //银保万能、个险万能、电话销售保单，或不需要打印的保单，置为已下载状态
            if (StrTool.cTrim(mCardFlag).equals("8")
                    || StrTool.cTrim(mCardFlag).equals("5")
                    || StrTool.cTrim(mCardFlag).equals("9") 
                    || StrTool.cTrim(mCardFlag).equals("c")
                    || StrTool.cTrim(mCardFlag).equals("d")
                    || StrTool.cTrim(mCardFlag).equals("e")
                    || StrTool.cTrim(mCardFlag).equals("f")
                    || isULI
                    || "0".equals(contPrintFlag))
            {
                tLCContPrintSchema.setDownloadCount(1);
                tLCContPrintSchema.setDownloadDate(mdate);
                tLCContPrintSchema.setDownloadTime(mtime);
                tLCContPrintSchema.setDownOperator(tGlobalInput.Operator);
            }
            //加入set中
            tLCContPrintSet.add(tLCContPrintSchema);
            System.out.println("xml放入完毕");
            //缺少获取影印件信息的程序
            //获取影印件列表信息
            String[][] tScanPic = (String[][]) mInputData.get(4);
           System.out.println("111111111111"+tScanPic);
            if (tScanPic != null)
            {
            	System.out.println("tScanPic.length=="+tScanPic.length);
            	System.out.println("FilePath===="+FilePath);
                for (int i = 0; i < tScanPic.length; i++, picNo++)
                {
                	System.out.println("ScanPic[i][0]==="+tScanPic[i][0]);
                    URL jspUrl = new URL(tScanPic[i][2]);
                    URLConnection uc = jspUrl.openConnection();
                    fos = new FileOutputStream(FilePath + "/" + tScanPic[i][0]);
                    int fn = 0;
                    //                    while ((fn = uc.getInputStream().read()) != -1)
                    //                    {
                    //                        fos.write(fn);
                    //                    }
                    while ((n = uc.getInputStream().read(c)) != -1)
                    {
                    	fos.write(c, 0, n);
                    }
                    fos.close();
                    //循环加入图片
                    System.out.println("开始放入图片");
                    LCContPrintSchema mtLCContPrintSchema = new LCContPrintSchema();
                    mtLCContPrintSchema.setPrintID(PubFun1.CreateMaxNo(
                            "PrintID", 11));
                    mtLCContPrintSchema.setPrtNo(mPrtNo);
                    mtLCContPrintSchema.setOtherNo(mContNo);
                    mtLCContPrintSchema.setOtherNoType("1");
                    mtLCContPrintSchema.setPrintCount(mPrintCount);
                    mtLCContPrintSchema.setFilePath("printdata/data/"
                            + tScanPic[i][0]);
                    mtLCContPrintSchema.setCValiDate(mcvalidate);
                    mtLCContPrintSchema.setManageCom(mManageCom);
                    mtLCContPrintSchema.setOperator(tGlobalInput.Operator);
                    mtLCContPrintSchema.setMakeDate(mdate);
                    mtLCContPrintSchema.setMakeTime(mtime);
                    mtLCContPrintSchema.setModifyDate(mdate);
                    mtLCContPrintSchema.setModifyTime(mtime);
                    //银保万能、个险万能、电话销售保单，或不需要打印的保单，置为已下载状态
                    if (StrTool.cTrim(mCardFlag).equals("8")
                            || StrTool.cTrim(mCardFlag).equals("5")
                            || StrTool.cTrim(mCardFlag).equals("9") 
                            || StrTool.cTrim(mCardFlag).equals("c") 
                            || StrTool.cTrim(mCardFlag).equals("d") 
                            || StrTool.cTrim(mCardFlag).equals("e") 
                            || StrTool.cTrim(mCardFlag).equals("f")
                            || isULI
                            || "0".equals(contPrintFlag))
                    {
                        mtLCContPrintSchema.setDownloadCount(1);
                        mtLCContPrintSchema.setDownloadDate(mdate);
                        mtLCContPrintSchema.setDownloadTime(mtime);
                        mtLCContPrintSchema
                                .setDownOperator(tGlobalInput.Operator);
                    }
                    //加入set中
                    tLCContPrintSet.add(mtLCContPrintSchema);
                }
            }
//            LCContPrintDBSet tLCContPrintDBSet = new LCContPrintDBSet(conn);
//            tLCContPrintDBSet.set(tLCContPrintSet);
//            if (!tLCContPrintDBSet.insert())
//            {
//            	conn.rollback();
//                conn.close();
//                return false;
//            }
            mMap.put(tLCContPrintSet, "INSERT");

            //银保万能、个险万能、意外险平台和电话销售的保单打印需要发送新外包
            if (StrTool.cTrim(mCardFlag).equals("8")
                    || StrTool.cTrim(mCardFlag).equals("2")
                    || StrTool.cTrim(mCardFlag).equals("5")
                    || StrTool.cTrim(mCardFlag).equals("6")
                    || StrTool.cTrim(mCardFlag).equals("9")
                    || StrTool.cTrim(mCardFlag).equals("c") 
                    || StrTool.cTrim(mCardFlag).equals("d") 
                    || StrTool.cTrim(mCardFlag).equals("e") 
                    || StrTool.cTrim(mCardFlag).equals("f") 
                    || isULI
                    || isFilialePrint)
            {
                if ("1".equals(contPrintFlag)&&!("1".equals(PrintType)))
                {
                    if (!sendXML(XmlFile))
                    {
//                        conn.rollback();
//                        conn.close();
                        return false;
                    }else{
                    	File cFile = new File(XmlFile);
                        cFile.delete();
                    }
                }
            }

//            if (!dealreceive(conn))
//            {
//                conn.rollback();
//                conn.close();
//                return false;
//            }
//            conn.commit();
//            conn.close();
            if (!dealreceive())
              {
                  return false;
              }

            if (!receiveCont())
            {
                return false;
            }
            vData.add(mMap);
            PubSubmit pubSubmit = new PubSubmit();
            if(!pubSubmit.submitData(vData, "")){
            	 this.mErrors.addOneError(pubSubmit.mErrors.getContent());
            	 return false;
            }
            

            System.out.println("commit end");
        }
        catch (FileNotFoundException fe)
        {
            String[][] tScanPic = (String[][]) mInputData.get(4);
            String subTypeName = new ExeSQL()
                    .getOneValue("select SubTypeName from ES_DOC_DEF where SubType = '"
                            + tScanPic[picNo][4] + "'");
            System.out.println(subTypeName);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = subTypeName + "的第 " + tScanPic[picNo][5]
                    + " 张图片有问题！";
            this.mErrors.addOneError(tError);
            try
            {
//                conn.rollback();
//                conn.close();
            }
            catch (Exception e)
            {
            }
            tReturn = false;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
//            try
//            {
//                conn.rollback();
//                conn.close();
//            }
//            catch (Exception e)
//            {
//            }
            tReturn = false;
        }

        return tReturn;
    }

	private byte[] InputStreamToBytes(InputStream mIs) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=mIs.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				mIs.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
	public String getPadXmlFile() {
		return PadXmlFile;
	}

	public void setPadXmlFile(String padXmlFile) {
		PadXmlFile = padXmlFile;
	}

	/**
     * receiveCont
     * 如果保单是银保万能、个险万能、意外险平台和电话销售，则自动合同接收
     * @return boolean
     */
    private boolean receiveCont()
    {

        if (StrTool.cTrim(mCardFlag).equals("5")
                || StrTool.cTrim(mCardFlag).equals("2")
                || StrTool.cTrim(mCardFlag).equals("8")
                || StrTool.cTrim(mCardFlag).equals("6")
                || StrTool.cTrim(mCardFlag).equals("9")
                || StrTool.cTrim(mCardFlag).equals("c")
                || StrTool.cTrim(mCardFlag).equals("d")
                || StrTool.cTrim(mCardFlag).equals("e")
                || StrTool.cTrim(mCardFlag).equals("f")
                || isULI
                || isFilialePrint)
        {
            TransferData tTmpTransferData = new TransferData();
            tTmpTransferData.setNameAndValue("ContNo", mLCContSchema
                    .getContNo());

            VData data = new VData();
            data.add(mGlobalInput);
            data.add(tTmpTransferData);

            ContReceiveUI tContReceiveUI = new ContReceiveUI();
            if (!tContReceiveUI.submitData(data, "ReceiveCont"))
            {
                mErrors.copyAllErrors(tContReceiveUI.mErrors);
                return false;
            }
        }

        return true;
    }

    /**
     * sendXML
     * 将xml发送到指定目录
     * @param cXmlFile String
     * @return boolean
     */
    private boolean sendXML(String cXmlFile) throws SocketException,
            IOException
    {
        //银保万能、个险万能和电话销售的保单打印界面触发的打印需要发送新外包
        //if(mTransferData == null || (!StrTool.cTrim(mCardFlag).equals("8") && !StrTool.cTrim(mCardFlag).equals("5")))
        //{
        //    return true;
        //}
    	System.out.println("打印报文："+cXmlFile);
    	String testFlag = (String) mInputData.get(7);
           String code=mGlobalInput.ManageCom;
           if(code.equals("86")){
        	   code="8600";
           }else{
        	   code=code.substring(0, 4);
           }
        String tIP = "";
        String tPort = CommonBL.getCodeName("printserver", "Port");
        String tUserName = CommonBL.getCodeName("printserver", "UserName");
        String tPassword = CommonBL.getCodeName("printserver", "Password");
        String strSQL1 = new ExeSQL().getOneValue("select othersign   from ldcode  where  codetype='pilotconfig' and code='"
			+ code + "'");
    	if (strSQL1.equals("1") && "new".equals(testFlag)) {
    		    tIP= CommonBL.getCodeName("printserver", "NIP");
			System.out.println("试点机构打印IP为+"+tIP);
			}else {
				tIP=CommonBL.getCodeName("printserver", "IP");
				System.out.println("非试点机构打印IP为+"+tIP);
			}
//        tIP="10.253.33.7";
//        tPort="21";
//        tUserName ="picch";
//        tPassword ="picch";
        if (tIP == null || tIP.equals("") || tPort == null || tPort.equals("")
                || tUserName == null || tUserName.equals("")
                || tPassword == null || tPassword.equals(""))
        {
            mErrors.addOneError("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
            return false;
        }

        FTPTool tFTPTool = new FTPTool(tIP, tUserName, tPassword, Integer
                .parseInt(tPort));
        try
        {
            if (!tFTPTool.loginFTP())
            {
                mErrors.addOneError("登录ftp服务器失败");
                return false;
            }
        }
        catch (SocketException ex)
        {
            mErrors.addOneError("无法登录ftp服务器");
            return false;
        }
        catch (IOException ex)
        {
            mErrors.addOneError("无法读取ftp服务器信息");
            return false;
        }

        if (!tFTPTool.upload(".", cXmlFile))
        {
            System.out.println("上载文件失败!");
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "sendZip";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            tFTPTool.logoutFTP();
            System.out.println(tError.errorMessage);

            return false;
        }
        tFTPTool.logoutFTP();

        return true;
    }

    /**
     * 将所有得到的打印数据按照主险保单号存入数据库，为以后的补打保单做准备
     * @param cXMLDataSets XMLDatasets
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private void saveDataStream(XMLDatasets cXMLDataSets,
            GlobalInput cGlobalInput, Connection conn) throws Exception
    {

        InputStream tIns = cXMLDataSets.getInputStream();
        saveOneDataStream(tIns, cGlobalInput, conn);
        //        Document doc = cXMLDataSets.getDocument();
        //        List list = doc.getRootElement().getChildren("DATASET");
        //        //转换xml流数据为document对象
        //        for (int nIndex = 0; nIndex < list.size(); nIndex++)
        //        {
        //            Element ele = (Element) list.get(nIndex);
        //            // Build a new element from the content of old element.
        //            // By doing this, we can detach element from document
        //            ele = new Element("DATASET").setMixedContent(ele.getMixedContent());
        //            // Document newDoc = new Document(new Element("DATASETS").addContent(ele));
        //            Document newDoc = new Document(ele);
        //        }
    }

    /**
     * 保存一条保单打印的数据
     * @param cIns InputStream
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private void saveOneDataStream(InputStream cIns, GlobalInput cGlobalInput,
            Connection conn) throws Exception
    {
        try
        {
            //获得合同单号
            String strCurDate = PubFun.getCurrentDate();
            String strCurTime = PubFun.getCurrentTime();
            //数据库中删除旧有数据，针对保单重打时候的操作
            LCPolPrintDB tLCPolPrintDB = new LCPolPrintDB(conn);
            tLCPolPrintDB.setMainPolNo(mContNo);
            //删除操作改用db的delete方法执行，通用化
            if (!tLCPolPrintDB.delete())
            {
                throw new Exception("删除数据失败！");
            }

            String[] parmStrArr = new String[4];
            StringBuffer tSBql = new StringBuffer(256);
            //插入数据操作
            //            parmStrArr[0] = "INSERT INTO LCPolPrint(MainPolNo, PrtTimes, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime,PolInfo, PolType) VALUES('"
            //                            + mContNo + "', " + "1, '" + cGlobalInput.Operator +
            //                            "', '" + strCurDate + "', '" + strCurTime + "', '" +
            //                            strCurDate + "', '" + strCurTime +
            //                            "',empty_blob() ,'2')";
            tSBql
                    .append("INSERT INTO LCPolPrint(MainPolNo, PrtTimes, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime,PolInfo, PolType) VALUES('");
            tSBql.append(mContNo);
            tSBql.append("', 1, '");
            tSBql.append(cGlobalInput.Operator);
            tSBql.append("', '");
            tSBql.append(strCurDate);
            tSBql.append("', '");
            tSBql.append(strCurTime);
            tSBql.append("', '");
            tSBql.append(strCurDate);
            tSBql.append("', '");
            tSBql.append(strCurTime);
            tSBql.append("',empty_blob() ,'2')");

            parmStrArr[0] = tSBql.toString();
            parmStrArr[1] = "LCPolPrint";
            parmStrArr[2] = "PolInfo";
            parmStrArr[3] = " and MainPolNo = '" + mContNo + "' ";

            //            CBlob tCBlob = new CBlob();
            if (!CBlob.BlobInsert(cIns, parmStrArr, conn))
            {
                throw new Exception("Blob数据操作失败！");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Kevin 2003-03-27
     * 补打保单时对应的保存函数
     * @param vData VData
     * @return boolean
     */
    private boolean update(VData vData)
    {
        LCContSchema tLCContSchema = (LCContSchema) vData
                .getObjectByObjectName("LCContSchema", 0);

        Connection conn = null;
        Statement stmt = null;
        //        String strSQL = "";

        try
        {
            conn = DBConnPool.getConnection();
            stmt = conn.createStatement();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }
            conn.setAutoCommit(false);
            //            strSQL = "UPDATE LCPolPrint SET ModifyDate = '" +
            //                     PubFun.getCurrentDate() + "' , ModifyTime = '" +
            //                     PubFun.getCurrentTime()
            //                     + "' WHERE MainPolNo = '" + tLCContSchema.getGrpContNo() +
            //                     "'";
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("UPDATE LCPolPrint SET ModifyDate = '");
            tSBql.append(PubFun.getCurrentDate());
            tSBql.append("' , ModifyTime = '");
            tSBql.append(PubFun.getCurrentTime());
            tSBql.append("' WHERE MainPolNo = '");
            tSBql.append(tLCContSchema.getContNo());
            tSBql.append("'");

            stmt.executeUpdate(tSBql.toString());
            conn.commit();
            if (stmt != null)
            {
                stmt.close();
            }
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
            buildError("getRePrintData", ex.getMessage());
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 放入合同接收表
     * @param cIns InputStream
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */

    private boolean dealreceive() throws Exception
    {
        System.out.println("in dealreceive");
//        LAAgentDB tLAAgentDB = new LAAgentDB(conn);
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentSet tLAAgentSet = tLAAgentDB
                .executeQuery("select * from laagent where agentcode='"
                        + mLCContSchema.getAgentCode() + "' ");
        tLAAgentSchema = tLAAgentSet.get(1);
        String tAgentName = tLAAgentSchema.getName();
//        LCContReceiveDBSet cLCContReceiveDBSet = new LCContReceiveDBSet(conn);
        LCContReceiveDBSet cLCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet cLCContReceiveSet = new LCContReceiveSet();
//        LCContReceiveDBSet c2LCContReceiveDBSet = new LCContReceiveDBSet(conn);
        LCContReceiveDBSet c2LCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet c2LCContReceiveSet = new LCContReceiveSet();

        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema.setReceiveID(1);
        tLCContReceiveSchema.setContNo(mLCContSchema.getContNo());
        tLCContReceiveSchema.setPrtNo(mLCContSchema.getPrtNo());
        tLCContReceiveSchema.setContType(mLCContSchema.getContType());
        tLCContReceiveSchema.setCValiDate(mLCContSchema.getCValiDate());
        tLCContReceiveSchema.setAppntNo(mLCContSchema.getAppntNo());
        tLCContReceiveSchema.setAppntName(mLCContSchema.getAppntName());
        tLCContReceiveSchema.setPrem(mLCContSchema.getPrem());
        tLCContReceiveSchema.setAgentCode(mLCContSchema.getAgentCode());
        tLCContReceiveSchema.setAgentName(tAgentName);
        tLCContReceiveSchema.setSignDate(mLCContSchema.getSignDate());
        tLCContReceiveSchema.setManageCom(mLCContSchema.getManageCom());
        tLCContReceiveSchema.setPrintManageCom(mGlobalInput.ManageCom);
        tLCContReceiveSchema.setPrintOperator(mGlobalInput.Operator);
        tLCContReceiveSchema.setPrintDate(mdate);
        tLCContReceiveSchema.setPrintTime(mtime);
        tLCContReceiveSchema.setPrintCount(mPrintCount);
        tLCContReceiveSchema.setReceiveState("0");
        tLCContReceiveSchema.setDealState("0");
        tLCContReceiveSchema.setMakeDate(mdate);
        tLCContReceiveSchema.setMakeTime(mtime);
        tLCContReceiveSchema.setModifyDate(mdate);
        tLCContReceiveSchema.setModifyTime(mtime);

//        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB(conn);
        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
        tLCContReceiveDB.setContNo(mLCContSchema.getContNo());
        tLCContReceiveSet = tLCContReceiveDB.query();

        if (tLCContReceiveSet.size() > 0)
        {
            int k = tLCContReceiveSet.size() + 1;
            System.out.println("第" + k + "次生成合同接收记录");
            tLCContReceiveSchema.setReceiveID(tLCContReceiveSet.size() + 1);
//            LCContReceiveDB t2LCContReceiveDB = new LCContReceiveDB(conn);
            LCContReceiveDB t2LCContReceiveDB = new LCContReceiveDB();
            LCContReceiveSet t2LCContReceiveSet = new LCContReceiveSet();
            t2LCContReceiveDB.setContNo(mLCContSchema.getContNo());
            t2LCContReceiveDB.setDealState("0");
            t2LCContReceiveSet = t2LCContReceiveDB.query();
            if (t2LCContReceiveSet.size() > 0)
            {
                for (int i = 1; i <= t2LCContReceiveSet.size(); i++)
                {
                    LCContReceiveSchema t2LCContReceiveSchema = new LCContReceiveSchema();
                    t2LCContReceiveSchema = t2LCContReceiveSet.get(i)
                            .getSchema();
                    t2LCContReceiveSchema.setDealState("1");
                    c2LCContReceiveSet.add(t2LCContReceiveSchema);
                }
            }

        }
        else
        {
            System.out.println("第1次生成合同接收记录成");
        }
        MMap mmMap = new MMap();
        if (c2LCContReceiveSet.size() > 0)
        {
//            c2LCContReceiveDBSet.add(c2LCContReceiveSet);
//            if (!c2LCContReceiveDBSet.update())
//            {
//                conn.rollback();
//                conn.close();
//                return false;
//            }
        	mmMap.put(c2LCContReceiveSet, "UPDATE");
        }
        cLCContReceiveSet.add(tLCContReceiveSchema);
        cLCContReceiveDBSet.add(cLCContReceiveSet);
//        if (!cLCContReceiveDBSet.insert())
//        {
//            conn.rollback();
//            conn.close();
//            return false;
//        }
         mmMap.put(cLCContReceiveSet, "INSERT");
         VData mVData = new VData();
         mVData.add(mmMap);
         PubSubmit pubSubmit = new PubSubmit();
         if(!pubSubmit.submitData(mVData, "")){
             this.mErrors.addOneError(pubSubmit.mErrors.getContent());
        	return false; 
         }
        return true;
    }

    public static void main(String[] args) throws SocketException, IOException
    {
        LCContF1PBLS bls = new LCContF1PBLS();
        if (!bls.sendXML("D:/picch/ui/printdata/data/000917377000001.xml"))
            System.out.println(bls.mErrors.getErrContent());
    }
}
