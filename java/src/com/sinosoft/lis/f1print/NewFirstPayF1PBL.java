package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author z
 * @version 1.0
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
//import com.sinosoft.lis.tb.TempFeeAppBL;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewFirstPayF1PBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    //    private LCContSchema mLCContSchema;

    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();

    private double fPremSum = 0;

    private double fPremAddSum = 0;

    private String fPrem = "";

    private String fPremAdd = "";

    //    private String mOperate = "";

    private String CurrentDate = PubFun.getCurrentDate();

    /** 总行数 */
    private static int sumCount = 11;

    private String mLoadFlag;

    /** 险种缓存 */
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    /** 是否保费相加（主要因为有个别保费为“-”情况，因此增加全局标记是否显示相加保费） */
    private boolean NeedAdd = true;

    private String mflag = null;

    public NewFirstPayF1PBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //        mOperate = cOperate;
        mflag = cOperate;
        try
        {

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            mResult.clear();
            // 准备所有要打印的数据
            getPrintData();
            //            else if (cOperate.equals("PRINT"))
            //            {
            //                if (!saveData(cInputData))
            //                {
            //                    return false;
            //                }
            //            }
            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    //    private boolean dealData()
    //    {
    //        return true;
    //    }
    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        TransferData tTransferData = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        if (tTransferData != null)
        {
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }
        //只赋给schema一个prtseq

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if ("".equals(mLOPRTManagerSchema.getOtherNo())
                || mLOPRTManagerSchema.getOtherNo() == null
                || "null".equals(mLOPRTManagerSchema.getOtherNo()))
        {
            buildError("getprintData", "没有查到该loprtmanager的otherno");
            return false;
        }

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "NewFirstPayF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private void getPrintData() throws Exception
    {
        /** 首先封装打印管理信息数据 */
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setCode(mLOPRTManagerSchema.getCode());
        tLOPRTManagerDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
        mLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
        LCContDB tLCContDB = getContInfo();
        String strContNo = tLCContDB.getContNo();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
                .executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where  agentcode='"
                        + tLCContDB.getAgentCode() + "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            buildError("getprintData", "没有查到该报单的代理人组别");
        }
        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        tLDComSet = tLDComDB.query();

        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            buildError("getprintData", "无效的打印状态");
        }
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
        /** 获取代理人信息 */
        getAgentInfo(tLCContDB);

        // 设置合计项
        fPremSum = 0;
        fPremAddSum = 0;
        double fSupplementaryPrem = 0;
        TextTag texttag1 = new TextTag();
        //      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag1.add("JetFormType", "07");
        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            throw new Exception("操作员机构查询出错!");
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag1.add("ManageComLength4", printcode);
        texttag1.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch"))
        {
            texttag1.add("previewflag", "0");
        }
        else
        {
            texttag1.add("previewflag", "1");
        }

        //不显示绑定型附加险
        String checkAppendRisk = "  and RiskCode not in "
                + "   (select Code from LDCode1 "
                + "   where CodeType = 'checkappendrisk' "
                + "      and RiskWrapPlanName is not null) ";

        // 查出所有的险种投保单
        String strsql = "SELECT a.* FROM LCPol a,LCUWMaster b WHERE a.ContNo = '"
                + strContNo
                + "' and a.polno=b.polno and a.UWFlag in ('4','9') "
                + checkAppendRisk + " order by a.PolNo";
        if (!StrTool.cTrim(tLCContDB.getCardFlag()).equals("")
                && !StrTool.cTrim(tLCContDB.getCardFlag()).equals("0"))
        {
            strsql = "SELECT a.* FROM LCPol a WHERE a.ContNo = '" + strContNo
                    + "' " + checkAppendRisk + "order by a.PolNo";
        }
        LCPolDB tempLCPolDB = new LCPolDB();
        System.out.println("$$ : " + strsql);
        LCPolSet tLCPolSet = tempLCPolDB.executeQuery(strsql);

        if (tempLCPolDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tempLCPolDB.mErrors);
            throw new Exception("在获取附加险投保单时出错！");
        }
        String[] RiskInfoTitle = new String[11];

        RiskInfoTitle[0] = "RiskCode";
        RiskInfoTitle[1] = "InsuredName";
        RiskInfoTitle[2] = "Prem";
        RiskInfoTitle[3] = "PremAdd";
        RiskInfoTitle[4] = "PremSum";
        RiskInfoTitle[5] = "YearsIntv";
        RiskInfoTitle[6] = "PayYearsIntv";
        RiskInfoTitle[7] = "AM";
        RiskInfoTitle[8] = "PayIntv";
        RiskInfoTitle[9] = "SupplementaryPrem";
        RiskInfoTitle[10] = "RiskName";
        ListTable tListTable = new ListTable();
        String StrRiskInfo[] = null;
        tListTable.setName("RiskInfo");

        for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
        {
            NeedAdd = true;
            mLCPolSchema = tLCPolSet.get(nIndex + 1).getSchema();
            StrRiskInfo = new String[11];

            //            LMRiskDB tLMRiskDB = new LMRiskDB();
            //            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
            //            if (!tLMRiskDB.getInfo()) {
            //                mErrors.copyAllErrors(tLMRiskDB.mErrors);
            //                buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
            //
            //            }
            LMRiskAppSchema tLMRiskAppSchema = this.mCRI
                    .findRiskAppByRiskCode(mLCPolSchema.getRiskCode());
            StrRiskInfo[10] = tLMRiskAppSchema.getRiskName();

            String sql = "select RiskWrapPlanName from LDCode1 "
                    + "where CodeType = 'checkappendrisk' " + "  and Code1='"
                    + mLCPolSchema.getRiskCode() + "' ";
            String riskWrapPlanName = new ExeSQL().getOneValue(sql);
            if (!"".equals(riskWrapPlanName)
                    && !"null".equals(riskWrapPlanName))
            {
                StrRiskInfo[10] = riskWrapPlanName;
            }

            // StrRiskInfo[1] = (new Double(mLCPolSchema.getAmnt())).toString();
            //StrRiskInfo[2] = (new Integer(mLCPolSchema.getPayYears())).toString();
            StrRiskInfo[0] = mLCPolSchema.getRiskCode();
            StrRiskInfo[1] = mLCPolSchema.getInsuredName();
            String strSQL = "SELECT SUM(Prem) FROM LCPrem WHERE" + " PolNo = '"
                    + mLCPolSchema.getPolNo() + "'"
                    + " AND PayPlanCode NOT LIKE '000000%'";

            if (tLMRiskAppSchema.getNotPrintPol() != null)
            {
                if (tLMRiskAppSchema.getNotPrintPol().equals("1"))
                {
                    strSQL = "SELECT SUM(Prem) FROM LCPrem WHERE "
                            + "PolNo in (select PolNo from LCPol where "
                            + "ContNo='"
                            + this.mLCPolSchema.getContNo()
                            + "' and insuredno='"
                            + this.mLCPolSchema.getInsuredNo()
                            + "'  and RiskCode in (select RiskCode from  LDRiskParamPrint "
                            + " where ParamType1='sumprem' and ParamValue1='"
                            + mLCPolSchema.getRiskCode() + "')"
                            //+ " and (Polno = '" + mLCPolSchema.getPolNo() + "' or MainPolNo = '" + mLCPolSchema.getPolNo() + "') " 
                            + ")" + " AND PayPlanCode NOT LIKE '000000%'";
                }
                if (tLMRiskAppSchema.getNotPrintPol().equals("0"))
                {
                    strSQL = "select '--' from dual";
                    NeedAdd = false;
                }
            }

            ExeSQL exeSQL = new ExeSQL();

            SSRS ssrs = exeSQL.execSQL(strSQL);

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                throw new Exception("取标准保费时出错");
            }

            if (!(ssrs.GetText(1, 1).equals("0")
                    || ssrs.GetText(1, 1).trim().equals("") || ssrs.GetText(1,
                    1).equals("null")))
            {
                fPrem = ssrs.GetText(1, 1);
            }
            else
            {
                fPrem = "0";
            }

            StrRiskInfo[2] = fPrem;
            //追加保费
            fSupplementaryPrem += mLCPolSchema.getSupplementaryPrem();
            StrRiskInfo[9] = mLCPolSchema.getSupplementaryPrem() + "";

            // 取加费
            String strSql = "SELECT SUM(Prem) FROM LCPrem WHERE" + " PolNo = '"
                    + mLCPolSchema.getPolNo() + "'"
                    + " AND PayPlanCode LIKE '000000%'";

            if (tLMRiskAppSchema.getNotPrintPol() != null)
            {
                if (tLMRiskAppSchema.getNotPrintPol().equals("1"))
                {
                    strSql = "SELECT SUM(Prem) FROM LCPrem WHERE "
                            + "PolNo in (select PolNo from LCPol where "
                            + "ContNo='"
                            + this.mLCPolSchema.getContNo()
                            + "' and insuredno='"
                            + this.mLCPolSchema.getInsuredNo()
                            + "'  and RiskCode in (select RiskCode from LDRiskParamPrint "
                            + " where ParamType1='sumprem' and ParamValue1='"
                            + mLCPolSchema.getRiskCode() + "')"
                            //+ " and (Polno = '" + mLCPolSchema.getPolNo() + "' or MainPolNo = '" + mLCPolSchema.getPolNo() + "') " 
                            + ")" + " AND PayPlanCode LIKE '000000%'";
                }
                if (tLMRiskAppSchema.getNotPrintPol().equals("0"))
                {
                    strSql = "select '--' from dual";
                    NeedAdd = false;
                }
            }

            exeSQL = new ExeSQL();

            ssrs = exeSQL.execSQL(strSql);

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                throw new Exception("取加费时出错");
            }

            if (!(ssrs.GetText(1, 1).equals("0")
                    || ssrs.GetText(1, 1).trim().equals("") || ssrs.GetText(1,
                    1).equals("null")))
            {
                fPremAdd = ssrs.GetText(1, 1);
            }
            else
            {
                fPremAdd = "0";
            }
            StrRiskInfo[3] = StrTool.cTrim(fPremAdd);
            double fSum = 0.00;
            if (NeedAdd)
            {
                fPremSum += PubFun.setPrecision(Double.parseDouble(fPrem),
                        "0.00");
                fPremAddSum += PubFun.setPrecision(
                        Double.parseDouble(fPremAdd), "0.00");
                fSum = PubFun
                        .setPrecision(Double.parseDouble(fPremAdd), "0.00")
                        + PubFun
                                .setPrecision(Double.parseDouble(fPrem), "0.00");
                String strSumPrem = StrTool.cTrim(String.valueOf(fSum));

                StrRiskInfo[4] = (new ExeSQL()).getOneValue("select varchar("
                        + strSumPrem + ") from dual");
            }
            else
            {
                StrRiskInfo[4] = "--";
            }

            //StrRiskInfo[5] = (setPrecision(mLCPolSchema.getPrem(),"0.00")).toString();
            //            StrRiskInfo[5] = fSum;

            strSQL = "select rtrim(char(InsuYear))||case InsuYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                    + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
            exeSQL = new ExeSQL();
            ssrs = exeSQL.execSQL(strSQL);
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A")
                    && (tLCPolSet.get(nIndex + 1).getInsuYear() == 1000 || tLCPolSet
                            .get(nIndex + 1).getInsuYear() == 106))
            {
                StrRiskInfo[5] = "终身";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A")
                    && (tLCPolSet.get(nIndex + 1).getInsuYear() != 1000 && tLCPolSet
                            .get(nIndex + 1).getInsuYear() != 106))
            {
                StrRiskInfo[5] = "至" + tLCPolSet.get(nIndex + 1).getInsuYear()
                        + "岁";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("M"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "月";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("Y"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "年";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("D"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "日";
            }
            if (tLCPolSet.get(nIndex + 1).getPayIntv() == 0)
            {
                StrRiskInfo[6] = "-";
            }
            else
            {
                strSQL = "select rtrim(char(PayEndYear))||case PayEndYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                        + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
                exeSQL = new ExeSQL();
                ssrs = exeSQL.execSQL(strSQL);
                StrRiskInfo[6] = StrTool.cTrim(ssrs.GetText(1, 1));
                if (StrRiskInfo[6].endsWith("岁"))
                {
                    StrRiskInfo[6] = "至" + StrRiskInfo[7];
                }
            }
            strSQL = "select case when (Mult = 0 or Mult = 10) then Amnt else integer(Mult) end from LCPol where"
                    + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
            exeSQL = new ExeSQL();
            ssrs = exeSQL.execSQL(strSQL);
            if (ssrs.GetText(1, 1).length() == 1)
            {
                StrRiskInfo[7] = ssrs.GetText(1, 1) + '档';
            }
            else
            {
                StrRiskInfo[7] = StrTool.cTrim(ssrs.GetText(1, 1)) + "元";
            }
            /**@author:Yangming 添加缴费频次 */
            strSQL = "select codename from ldcode a,lcpol b where b.polno='"
                    + mLCPolSchema.getPolNo()
                    + "' and a.code=char(b.payintv) and a.codetype='payintv' and conttype='1'";
            String tPayIntv = exeSQL.getOneValue(strSQL);
            if (tPayIntv == null || tPayIntv.equals("")
                    || tPayIntv.equals("null"))
            {
                buildError("getPrintData", "查找险种" + mLCPolSchema.getRiskCode()
                        + "缴费频次失败！");
                throw new Exception("查找险种" + mLCPolSchema.getRiskCode()
                        + "缴费频次失败！");
            }
            StrRiskInfo[8] = tPayIntv;
            tListTable.add(StrRiskInfo);
        }
        /** 动态行数，为了将显示等分 */
        ListTable tCountTable = new ListTable();

        String[] tCount = new String[1];
        //tCountTable.setName("Blank");
        // tCount[0] = "Blank";
        int count = sumCount - tLCPolSet.size();
        //  if (count > 0)
        //  {
        //            for (int i = 1; i <= count; i++)
        //            {
        //                String[] blank = { "" };
        //                tCountTable.add(blank);
        //            }
        // }

        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        System.out.println(" mLoadFlag : " + mLoadFlag);
        if (StrTool.cTrim(mLoadFlag).equals("Back"))
        {
            xmlExport.createDocument("FirstPayNoticeBack.vts", "");
        }
        else
        {
            xmlExport.createDocument("FirstPayNotice.vts", ""); //最好紧接着就初始化xml文档
        }
        xmlExport.addTextTag(texttag1);

        TextTag texttag = new TextTag();
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("XI_AppntNo", tLCContDB.getAppntNo());
        texttag.add("AppntName", tLCContDB.getAppntName());
        System.out.println("==============================");
        System.out.println("tLCContDB.getAppntName() : "
                + tLCContDB.getAppntName());
        System.out.println("tLCContDB.getAppntName() : "
                + StrTool.unicodeToGBK(tLCContDB.getAppntName()));
        if (tLCContDB.getAppntSex().equals("1"))
        {
            texttag.add("Title", "女士");
        }
        else
        {
            texttag.add("Title", "先生");
        }
        texttag.add("ContNo", tLCContDB.getContNo());
        /*        texttag.add("Cavidate",
         tLCContDB.getCValiDate().split("-")[0] + "年" +
         tLCContDB.getCValiDate().split("-")[1] + "月" +
         tLCContDB.getCValiDate().split("-")[2] + "日");
         */texttag.add("AppntAddr", getAppAddr(tLCContDB.getContNo()));
        texttag.add("AppntZipcode", getAppZipcode(tLCContDB.getContNo()));

        texttag.add("PrtNo", tLCContDB.getPrtNo());
        texttag.add("prtSeq", mLOPRTManagerSchema.getPrtSeq());

        texttag.add("AgentName", this.tLAAgentSchema.getName());
        //2014-11-2    杨阳
        //业务员号友显示LAagent表中GroupAgentcode字段
        String groupAgentCode = new ExeSQL().getOneValue("select getUniteCode("+tLCContDB.getAgentCode()+") from dual with ur");
        texttag.add("AgentCode", groupAgentCode);	
        //add by zhangxing 增加代理人信息

        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getBranchAttr());
        if (tLDComSet == null || tLDComSet.size() == 0)
        {
            texttag.add("GlobalServicePhone", "");
        }
        else
        {
            texttag.add("GlobalServicePhone", tLDComSet.get(1)
                    .getServicePhone());
        }

        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            throw new Exception("读取管理机构错误!");
        }
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getPhone());
        texttag.add("InsuredName", tLCContDB.getInsuredName());
        System.out.println("=======tLCContDB.getInsuredName() : "
                + tLCContDB.getInsuredName());
        texttag.add("PrtSeq", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("LCPol.prtno", tLCContDB.getPrtNo());
        texttag.add("MangeCom", tLDComDB.getServiceName());
        //2014-11-2    杨阳
        //业务员号友显示LAagent表中GroupAgentcode字段
        texttag.add("Agentcode", groupAgentCode);
        texttag.add("Prem", fPremSum);
        texttag.add("PremAdd", fPremAddSum);
        texttag.add("PayMoneyCHS", PubFun.getChnMoney(fPremSum + fPremAddSum
                + fSupplementaryPrem));
        texttag.add("Bank", getBankName(tLCContDB.getBankCode()));
        texttag.add("BankID", tLCContDB.getBankAccNo());
        texttag.add("Account", tLCContDB.getAccName());
        texttag.add("PayMode", getPayMode(tLCContDB.getPayMode()));
        texttag.add("AgentPhone", getAgentPhone());
        texttag.add("OperatorName", getOperatorName(mGlobalInput.Operator));
        texttag.add("OperatorCode", mGlobalInput.Operator);
        texttag.add("PayDate", getDate((new FDate()).getDate(PubFun
                .getCurrentDate())));

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + tLCContDB.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
            throw new Exception("系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        // 调整缴费通知书，新增节点信息。2008-01-27

        // 先收费标志。0：先收费；""：正常
        texttag.add("PayLocation", StrTool.cTrim(tLCContDB.getPayLocation()));

        // 保单关联的缴费凭证号
        texttag.add("TempFeeNo", StrTool.cTrim(tLCContDB.getTempFeeNo()));

        // 实际到帐保费（对后收费的保单该值为0）
        String tEnterAccFee = getEnterAccFee(tLCContDB.getSchema());
        texttag.add("EnterAccFee", tEnterAccFee);

        texttag.add("DifMoney", getDifMoney(fPremSum, fPremAddSum
                + fSupplementaryPrem, tEnterAccFee));

        // --------------------------------------

        if (StrTool.cTrim(tLCContDB.getPayMode()).equals("4"))
        {
            Date makeDate = (new FDate()).getDate(mLOPRTManagerSchema
                    .getMakeDate());
            texttag.add("Today", getDate(makeDate));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(makeDate);
            calendar.add(Calendar.DATE, 3);
            texttag.add("Cavidate", getDate(calendar.getTime()));
        }
        else
        {
            Date makeDate = (new FDate()).getDate(mLOPRTManagerSchema
                    .getMakeDate());
            texttag.add("Today", getDate(makeDate));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(makeDate);
            calendar.add(Calendar.DATE, 10);
            texttag.add("Cavidate", getDate(calendar.getTime()));
        }
        Date Cavidate1 = (new FDate()).getDate(tLCContDB.getCValiDate());
        texttag.add("Cavidate1", getDate(Cavidate1));

        if (texttag.size() > 0)
        {
            xmlExport.addTextTag(texttag);
        }

        if (tCountTable.size() > 0)
        {
            xmlExport.addListTable(tCountTable, tCount);
        }
        if (tLCContDB.getPayMode() == null || tLCContDB.getPayMode() == "")
        {
            //mErrors.copyAllErrors(tLCContDB.mErrors);
            throw new Exception("没有录入缴费方式！");
        }
        if (tLCContDB.getPayMode().equals("4"))
        {
            xmlExport.addDisplayControl("displaybank");
        }
        else
        {
            xmlExport.addDisplayControl("displaymoney");
        }

        xmlExport.addListTable(tListTable, RiskInfoTitle);

        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlExport.addListTable(tEndTable, tEndTitle);
        TextTag texttag2 = new TextTag();
        texttag2.add("PremSum", String.valueOf(fPremSum + fPremAddSum
                + fSupplementaryPrem));
        if (texttag2.size() > 0)
        {
            xmlExport.addTextTag(texttag2);
        }

        xmlExport.outputDocumentToFile("d:\\", "firtpay");
        mResult.clear();
        mResult.addElement(xmlExport);
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);

    }

    private String getAgentPhone()
    {
        String phone = !StrTool.cTrim(tLAAgentSchema.getMobile()).equals("") ? tLAAgentSchema
                .getMobile()
                : StrTool.cTrim(tLAAgentSchema.getPhone());
        return phone.equals("") ? "          " : phone;
    }

    /**
     * getOperatorName
     *
     * @param tOperatorCode String
     * @return String
     */
    private String getOperatorName(String tOperatorCode)
    {
        String name = (new ExeSQL())
                .getOneValue("select username from lduser where usercode='"
                        + tOperatorCode + "'");
        if (name == null || name.equals("") || name.equals("null"))
        {
            return "";
        }
        return name;
    }

    /**
     * getPayMode
     *
     * @param tPayMode String
     * @return String
     */
    private String getPayMode(String tPayMode)
    {
        return StrTool.cTrim(tPayMode).equals("1") ? "现金" : "银行转帐";
    }

    private void getAgentInfo(LCContDB tLCContDB)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
        }
        tLAAgentSchema = tLAAgentDB.getSchema();
    }

    /**
     *
     * @return LCContDB
     * @throws Exception
     */
    private LCContDB getContInfo() throws Exception
    {
        LCContDB tLCContDB = new LCContDB();
        // 打印时传入的是主险投保单的投保单号
        tLCContDB.setProposalContNo(mLOPRTManagerSchema.getOtherNo());
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0)
        {
            throw new Exception("查询合同信息失败！");
        }
        if (tLCContSet.size() > 1)
        {
            throw new Exception("查询合同信息失败！");
        }
        //        if (!tLCContDB.getInfo()) {
        //            mErrors.copyAllErrors(tLCContDB.mErrors);
        //            throw new Exception("在获取保单信息时出错！");
        //        }
        tLCContDB.setSchema(tLCContSet.get(1).getSchema());
        return tLCContDB;
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    //    private LOPRTManagerDB getPrintManager() throws Exception
    //    {
    //        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    //        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
    //        if (tLOPRTManagerDB.getInfo() == false)
    //        {
    //            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
    //            throw new Exception("在取得打印队列中数据时发生错误");
    //        }
    //        //需要判断是否已经打印？！
    //        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //get all message！
    //        return tLOPRTManagerDB;
    //    }
    //
    //    private boolean saveData(VData mInputData)
    //    {
    //
    //        //根据印刷号查询打印队列中的纪录
    //        //mLOPRTManagerSchema
    //        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    //        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
    //        if (tLOPRTManagerDB.getInfo() == false)
    //        {
    //            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
    //            buildError("outputXML", "在取得打印队列中数据时发生错误");
    //            return false;
    //        }
    //        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
    //        mLOPRTManagerSchema.setStateFlag("1");
    //        mLOPRTManagerSchema.setDoneDate(CurrentDate);
    //        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
    //
    //        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData
    //                .getObjectByObjectName("LOPRTManagerSchema", 0));
    //
    //        mResult.add(mLOPRTManagerSchema);
    //        mResult.add(tLOPRTManagerDB);
    //        FirstPayF1PBLS tFirstPayF1PBLS = new FirstPayF1PBLS();
    //        tFirstPayF1PBLS.submitData(mResult, mOperate);
    //        if (tFirstPayF1PBLS.mErrors.needDealError())
    //        {
    //            mErrors.copyAllErrors(tFirstPayF1PBLS.mErrors);
    //            buildError("saveData", "提交数据库出错！");
    //            return false;
    //        }
    //        return true;
    //
    //    }
    // private String getAgentName(String strAgentCode) throws Exception
    //    {
    //
    //        tLAAgentDB.setAgentCode(strAgentCode);
    //        if (!tLAAgentDB.getInfo())
    //        {
    //            mErrors.copyAllErrors(tLAAgentDB.mErrors);
    //            throw new Exception("在取得LAAgent的数据时发生错误");
    //        }
    //        return tLAAgentDB.getName();
    //    }
    private String getBankName(String strBankCode) throws Exception
    {
        //LDCodeDB tLDCodeDB = new LDCodeDB();
        return ChangeCodeBL.getCodeName("Bank", strBankCode, "BankCode");
        //        tLDCodeDB.setCode(strBankCode);
        //        tLDCodeDB.setCodeType("bank");
        //        if (tLDCodeDB.getInfo()) {
        //            return tLDCodeDB.getCodeName();
        //        } else {
        //            return null;
        //        }
    }

    private String getAppNo(String strContNo) throws Exception
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo())
        {
            return tLCAppntDB.getAppntNo();
        }
        else
        {
            return null;
        }
    }

    private String getAddrNo(String strContNo) throws Exception
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo())
        {
            return tLCAppntDB.getAddressNo();
        }
        else
        {
            return null;
        }
    }

    private String getAppAddr(String strContNo) throws Exception
    {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo())
        {
            return tLCAddressDB.getPostalAddress();
        }
        else
        {
            return null;
        }
    }

    private String getAppZipcode(String strContNo) throws Exception
    {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo())
        {
            return tLCAddressDB.getZipCode();
        }
        else
        {
            return null;
        }
    }

    /**
     * 获取到帐保费。
     * @param tLCContSchema
     * @return  到帐总金额
     */
    private String getEnterAccFee(LCContSchema tLCContSchema)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 获取暂收总保费。
        String tPrtNo = tLCContSchema.getPrtNo();

        tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf "
                + " where ljtf.OtherNoType = '4' " // 个单暂收核销前标志
                // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                + " and ljtf.EnterAccDate is not null " // 财务到帐
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        // --------------------

        return format(Double.parseDouble(tResult));
    }

    /**
     * 计算到帐保费差异。
     * <br />计算方式：到帐保费金额 － (标准承保保费 ＋ 承保加费)
     * @param fPremSum  承保标准保费
     * @param fPremAddSum   承保加费
     * @param tEnterAccFee  到帐保费
     * @return  到帐保费差异金额
     */
    private String getDifMoney(double fPremSum, double fPremAddSum,
            String tEnterAccFee)
    {
        BigDecimal tDifMoney = new BigDecimal(0);

        // 累计标准承保保费
        tDifMoney = tDifMoney.add(new BigDecimal(fPremSum));
        // 累计承保加费
        tDifMoney = tDifMoney.add(new BigDecimal(fPremAddSum));

        tDifMoney = new BigDecimal(tEnterAccFee).subtract(tDifMoney);

        // 精确到2位小数。末位四舍五入。
        tDifMoney = tDifMoney.setScale(2, BigDecimal.ROUND_HALF_UP);

        return tDifMoney.toString();
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }
}
