/**
 * <p>Title:需要输入统计项的的扫描报表</p>
 * <p>Description: 1张报表</p>
 * <p>prt：扫描打印表格</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.utility.*;

public class ScanCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mRiskCode = ""; //个险=0，团险=1，银代=2,简易保单=3
    private String mDay[] = null;
    private String mScanOper = "";
    private String mDefineCode = "";
    private String mManageCom = "";
    /** 业务处理相关变量，在initCommon()中初始化*/
    private String mNO; //序号
    private String mPrtNO; //印刷号
    /**构造函数*/
    public ScanCheckBL()
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    { //打印
        //全局变量
        mRiskCode = (String) cInputData.get(0);
        mDay = (String[]) cInputData.get(1);
        mManageCom = (String) cInputData.get(2);
        mScanOper = (String) cInputData.get(3);
        mDefineCode = (String) cInputData.get(4);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //test:
        System.out.println("大小：" + cInputData.size());
        System.out.println("险种:个险=0？团险=1？银代=2？" + mRiskCode);
        System.out.println("starttime：" + mDay[0]);
        System.out.println("endtime：" + mDay[1]);
        System.out.println("被选择管理机构：" + mManageCom);
        System.out.println("登陆操作员：" + mGlobalInput.Operator);
        System.out.println("登陆操作机构：" + mGlobalInput.ManageCom);
        System.out.println("被统计的扫描操作员：" + mScanOper);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ScanCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**传输数据的公共方法*/
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        if (!getPrintDataPay())
        {
            return false;
        }
        return true;
    }

    private boolean getPrintDataPay()
    {
        System.out.println("报表代码类型：" + mDefineCode);
        String xmlname = "";
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        FinDayTool tFinDayTool = new FinDayTool();
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        //********************************************************************************
         //************************扫描打印清单*********************************************
          //*******************************************************************************
           if (mDefineCode.equals("prt"))
           {
               xmlexport.createDocument("Scanprt.vts", "printer");
               //xmlname="扫描打印表格";
               String RiskClass = ReportPubFun.getRiskClass(mRiskCode);
               System.out.println("险类标志：" + RiskClass);
               String mngcom = mGlobalInput.ManageCom;
               System.out.println("管理机构代码：" + mngcom);
               String Scan_sql = "select * from es_doc_main "
                                 + " where doc_code like '86" + RiskClass +
                                 "%'"
                                 + " and INPUT_DATE>='" + mDay[0] +
                                 "' and INPUT_DATE<='" + mDay[1] + "'"
                                 + " and ManageCom like'" + mngcom + "%' "
                                 +
                                 " and ManageCom is not null and ScanOperator is not null";
               if (mScanOper == null || mScanOper.equals(""))
               {
//                   Scan_sql = Scan_sql;
               }
               else
               {
                   Scan_sql = Scan_sql + " and ScanOperator='" + mScanOper +
                              "'";
               }
               if (mManageCom == null || mManageCom.equals(""))
               {
//                   Scan_sql = Scan_sql;
               }
               else
               {
                   Scan_sql = Scan_sql + " and ManageCom like'" + mManageCom +
                              "%'";
               }
               Scan_sql = Scan_sql + " order by DOC_CODE";
               System.out.println("扫描总数sql:" + Scan_sql);
               ES_DOC_MAINDB tESDB = new ES_DOC_MAINDB();
               ES_DOC_MAINSet tESSet = tESDB.executeQuery(Scan_sql);
               tlistTable.setName("prt");
               System.out.println("大小：" + tESSet.size());
               if (tESSet.size() == 0)
               {
                   System.out.println("查询结果为0");
                   strArr = new String[5];
                   strArr[0] = "";
                   strArr[1] = "";
                   strArr[2] = "";
                   strArr[3] = "";
                   strArr[4] = "";
                   tlistTable.add(strArr);
               }
               for (int i = 1; i <= tESSet.size(); i++)
               {
                   this.initCommon();
                   ES_DOC_MAINSchema tESSchema = tESSet.get(i);
                   this.mNO = String.valueOf(i);
//        this.mPrtNO=tESSchema.getDOC_CODE();
                   System.out.println("序号：" + this.mNO);
                   System.out.println("印刷号：" + this.mPrtNO);
                   strArr = new String[5];
                   strArr[0] = this.mNO;
                   strArr[1] = this.mPrtNO;
                   strArr[2] = "";
                   strArr[3] = "";
                   strArr[4] = "";
                   tlistTable.add(strArr);
               }
               xmlexport.addListTable(tlistTable, strArr);
           }
        String CurrentDate = PubFun.getCurrentDate();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("sManageCom", mManageCom);
        texttag.add("dManageCom", mGlobalInput.ManageCom);
        texttag.add("Operator", mScanOper);
        texttag.add("time", CurrentDate);
        System.out.println("大小" + texttag.size());
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\",xmlname);//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    private void initCommon()
    {
        this.mNO = "";
        this.mPrtNO = "";

    }

}
