package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCPENoticeItemSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.*;

public class PRnewBodyCheckPrintBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的保单号码
    private String mPolNo = "";
    //String InsuredName="";
    //输入的查询sql语句
    private String msql = "";
    //取得的延期承保原因
    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    public PRnewBodyCheckPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
//  mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));

        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        boolean PEFlag = false; //打印体检件部分的判断标志
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());
        int m, i;
        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            buildError("outputXML", "在取得LCPol的数据时发生错误");
            return false;
        }

        mLCPolSchema.setSchema(tLCPolDB.getSchema());

        String InsuredName = mLCPolSchema.getInsuredName(); //保存被保险人名称

        mAgentCode = mLCPolSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息
//1-险种信息：
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
        if (!tLMRiskDB.getInfo())
        {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
            return false;
        }
        String MainRiskName = tLMRiskDB.getRiskName(); //保存主险名称

//2-体检信息
//2-1 查询体检主表
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeDB.setContNo(mLCPolSchema.getPolNo());
        tLCPENoticeDB.setCustomerNo(mLCPolSchema.getInsuredNo());
        if (!tLCPENoticeDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
            buildError("outputXML", "在取得体检通知的数据时发生错误");
            return false;
        }

        String PEDate = tLCPENoticeDB.getPEDate(); //保存体检日期
        String PEOperator = tLCPENoticeDB.getOperator(); //保全核保人
        String NeedLimosis = "";
        if (tLCPENoticeDB.getPEBeforeCond().equals("Y")) //是否需要空腹
        {
            NeedLimosis = "是";
        }
        else
        {
            NeedLimosis = "否";
        }
        String PEAddress = tLCPENoticeDB.getPEAddress(); //体检地点代码

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("hospitalcode");
        tLDCodeDB.setCode(PEAddress);
        if (tLDCodeDB.getInfo())
        {
            PEAddress = tLDCodeDB.getCodeName();
        }
        else
        {
            tLDCodeDB.setCodeType("hospitalcodeuw");
            tLDCodeDB.setCode(PEAddress);
            if (tLDCodeDB.getInfo())
            {
                PEAddress = tLDCodeDB.getCodeName();
            }
            else
            {
                PEAddress = "该医院代码尚未建立，请确认！";
            }
        }

//2-1 查询体检子表
        String[] PETitle = new String[2];
        PETitle[0] = "ID";
        PETitle[1] = "CHECKITEM";
        ListTable tPEListTable = new ListTable();
        String strPE[] = null;
        tPEListTable.setName("CHECKITEM"); //对应模版体检部分的行对象名
        LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
        LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
        tLCPENoticeItemDB.setContNo(mLCPolSchema.getPolNo());
        tLCPENoticeItemDB.setPrtSeq(PrtNo);
        tLCPENoticeItemDB.setFreePE("N");
        tLCPENoticeItemSet.set(tLCPENoticeItemDB.query());
        if (tLCPENoticeItemSet.size() == 0)
        {
            PEFlag = false;
        }
        else
        {
            PEFlag = true;
            LCPENoticeItemSchema tLCPENoticeItemSchema;
            for (i = 1; i <= tLCPENoticeItemSet.size(); i++)
            {
                tLCPENoticeItemSchema = new LCPENoticeItemSchema();
                tLCPENoticeItemSchema.setSchema(tLCPENoticeItemSet.get(i));
                strPE = new String[2];
                strPE[0] = (new Integer(i)).toString(); //序号
                strPE[1] = tLCPENoticeItemSchema.getPEItemName(); //序号对应的内容
                tPEListTable.add(strPE);
            }
        }
//其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PRnewPENotice.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
//        StrTool tSrtTool = new StrTool();
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";
        PEDate = PEDate + "-";
        String CheckDate = StrTool.decodeStr(PEDate, "-", 1) + "年" +
                           StrTool.decodeStr(PEDate, "-", 2) + "月" +
                           StrTool.decodeStr(PEDate, "-", 3) + "日";

        //模版自上而下的元素
        texttag.add("LCPol.AppntName", mLCPolSchema.getAppntName()); //投保人名称
        texttag.add("LCPol.PolNo", mLCPolSchema.getPolNo()); //投保单号
        texttag.add("LCPol.PrtNo", mLCPolSchema.getPrtNo()); //投保单号
        texttag.add("RiskName", MainRiskName); //主险名称
        texttag.add("CheckDate", CheckDate); //体检日期
        texttag.add("InsuredName", InsuredName); //被保险人名称
        texttag.add("NeedLimosis", NeedLimosis); //是否需要空腹
        texttag.add("Hospital", PEAddress); //体检地点
        texttag.add("LAAgent.Name", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("LCPol.AgentCode", mLCPolSchema.getAgentCode()); //代理人业务号
        texttag.add("LCPol.ManageCom", getComName(mLCPolSchema.getManageCom())); //营业机构
        texttag.add("PrtNo", PrtNo); //刘水号
        texttag.add("LCPENotice.Operator", PEOperator); //保全核保人
        texttag.add("SysDate", SysDate);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        if (PEFlag == true)
        {
            xmlexport.addListTable(tPEListTable, PETitle); //保存体检信息
        }

        //保存其它信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode
     * @return
     * @throws Exception
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    /**
     * 根据主险保单号码查询主险保单
     * @param tMainPolNo
     * @return LCPolSchema
     */
    private LCPolSchema queryMainPol(String tMainPolNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tMainPolNo);
        tLCPolDB.setMainPolNo(tMainPolNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet == null)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        if (tLCPolSet.size() == 0)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        return tLCPolSet.get(1);
    }

}
