package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import java.util.Date;

public class GrpPayReportBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mdirect = "";
    private FDate fDate = new FDate();
    String StartDate = "";
    String EndDate = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
    private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new
            LOBatchPRTManagerSchema(); //理赔申诉错误表

    public GrpPayReportBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mLOBatchPRTManagerSchema.setSchema((LOBatchPRTManagerSchema) cInputData.
                                           getObjectByObjectName(
                "LOBatchPRTManagerSchema", 0));
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        System.out.println("wwwwwwwwwwwwwwww" + EndDate);

        if (mLOBatchPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOBatchPRTManagerSchema.getOtherNo() == null) {
            buildError("getInputData", "没有得到足够的信息:立案号不能为空！");
            return false;
        }
        System.out.println(mLOBatchPRTManagerSchema.getStandbyFlag3());
        if (mLOBatchPRTManagerSchema.getStandbyFlag3() == null
            ||mLOBatchPRTManagerSchema.getStandbyFlag3().equals("null")
                ||mLOBatchPRTManagerSchema.getStandbyFlag3().equals("")) {
            buildError("getInputData", "没有得到足够的信息:保单号不能为空！");
            return false;
        }

        mdirect += mLOBatchPRTManagerSchema.getStandbyFlag2();
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //根据印刷号查询打印队列中的纪录
        String aGrpName = "";
        int count = 0;
        String GrpNo = "";
        GrpNo = mLOBatchPRTManagerSchema.getOtherNo();
        String GrpContNo = "";
        GrpContNo = mLOBatchPRTManagerSchema.getStandbyFlag3();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(GrpContNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        String aRiskCode = "";
        String aRiskName = "";
        if (tLCGrpPolSet.size() > 0) {
            aRiskCode += tLCGrpPolSet.get(1).getRiskCode();
        }
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(aRiskCode);
        if (tLMRiskDB.getInfo()) {
            aRiskName += tLMRiskDB.getRiskName();
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(GrpContNo);
        String RecDate = "";
        String Cvalidate = "";
        int DutyDays = 0;
        int Peoples2 = 0;
        double GrpPrem = 0.0;
        if (tLCGrpContDB.getInfo()) {
            if (tLCGrpContDB.getReceiveDate() != null) {
                RecDate = DateFormat(tLCGrpContDB.getReceiveDate());
            } else {
                RecDate = DateFormat(tLCGrpContDB.getHandlerDate());
            }
            Cvalidate = DateFormat(tLCGrpContDB.getCValiDate());
            Date tCvalidate = fDate.getDate(tLCGrpContDB.getCValiDate());
            String strNow = PubFun.getCurrentDate();
            Date aNow = fDate.getDate(strNow);
            DutyDays = PubFun.calInterval(tCvalidate, aNow, "D");
            Peoples2 = tLCGrpContDB.getPeoples2();
            GrpPrem = tLCGrpContDB.getPrem();
            GrpNo = tLCGrpContDB.getAppntNo();
            aGrpName = tLCGrpContDB.getGrpName();
        }

        //ListTable保障计划
        ListTable GetPlanListTable = new ListTable();
        GetPlanListTable.setName("DutyVector");
        String[] Title = {"plancode", "riskcode", "amnt"};
        String dutySql = "select  contplancode,(select riskname from lmrisk where riskcode=g.riskcode),coalesce(sum(amnt),0) " +
                         "from lcpol g where appflag='1' and grpcontno='" +
                         GrpContNo + "' group by contplancode,riskcode with ur";
        ExeSQL texecsql = new ExeSQL();
        SSRS tss = new SSRS();
        SSRS rss = new SSRS();
        tss = texecsql.execSQL(dutySql);
        if (tss != null && tss.getMaxRow() > 0) {
            for (int i = 1; i <= tss.getMaxRow(); i++) {
                String[] strCol = tss.getRowData(i);
                GetPlanListTable.add(strCol);
            }
        }
        //ListTable保单赔付状况
        ListTable GetClaimListTable = new ListTable();
        GetClaimListTable.setName("ClaimVector");
        String[] ClaimTitle = {"RiskCode", "ClaimCount", "RealPay"};
        String ClaimSql1 =
                "select b.riskcode,count(distinct b.caseno),coalesce(sum(realpay),0) from "
                + "llclaimdetail b,llcase c where b.caseno=c.caseno  and c.rgtstate in ('12','11','09') "
                //+ "and NOT EXISTS(SELECT 1 FROM lmriskapp WHERE riskcode=b.riskcode AND risktype3='7' AND risktype <>'M')"
                + " and c.endcasedate between '" + StartDate + "' and '" + EndDate
                + "' and b.grpcontno ='" + GrpContNo + "' group by b.riskcode with ur";
        tss = texecsql.execSQL(ClaimSql1);
        if (tss != null && tss.getMaxRow() > 0) {
            for (int i = 1; i <= tss.getMaxRow(); i++) {
                String[] strCol = tss.getRowData(i);
                GetClaimListTable.add(strCol);
            }
        }

        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
        String ClaimSql = "select count(distinct c.caseno),coalesce(sum(realpay),0) from "
                          + " llclaimdetail b,llcase c where  c.rgtstate in ('12','11','09') and b.caseno=c.caseno "
                          //+ "and NOT EXISTS(SELECT 1 FROM lmriskapp WHERE riskcode=b.riskcode AND risktype3='7' AND risktype <>'M')"
                          + "  and c.endcasedate between '" + StartDate + "' and '" +
                          EndDate
                          + "' and b.grpcontno ='" + GrpContNo + "' with ur";
        int TotalCaseNo = 0;
        double TotalClaimMoney = 0;
        tss = texecsql.execSQL(ClaimSql);
        if (tss != null && tss.getMaxRow() > 0) {
            String[] rowdata = tss.getRowData(1);
            TotalCaseNo = Integer.parseInt(rowdata[0]);
            if (!rowdata[1].equals("null")) {
                TotalClaimMoney = Double.parseDouble(rowdata[1]);
            }
        }
        double CaseRate = 0.0;
        double ClaimRate = 0.0;
        double TemPrem = 0.0;

        ClaimSql = "select coalesce(sum(realpay),0) from "
                   + " llclaimdetail b,llcase c where  c.rgtstate in ('12','11','09') and b.caseno=c.caseno "
                   + "and NOT EXISTS(SELECT 1 FROM lmriskapp WHERE riskcode=b.riskcode AND risktype3='7' AND risktype <>'M')"
                   + "  and c.endcasedate between '" + StartDate + "' and '" +
                   EndDate
                   + "' and b.grpcontno ='" + GrpContNo + "' with ur";
        double ClaimMoney = 0;
        String tClaimMoney = texecsql.getOneValue(ClaimSql);
        if (tClaimMoney != null && tClaimMoney != "") {
            ClaimMoney = Double.parseDouble(tClaimMoney);
        }
        if (GrpPrem != 0.0) {
            //算经过保费
            /*String SrtSQL1 = "select coalesce(sum(B),0) from "
                           + "( select case when payintv<>0 then  (days('"+EndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 "
                           +" when payintv=0 then (days('"+EndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B "
                           +" from lcpol where enddate>'"+EndDate+"' and riskcode  not in (select riskcode from lmriskapp where risktype3='7' and risktype<>'M') "
                           +" and grpcontno='"+GrpContNo+"'union all "
                           +" select case when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/365  when payintv=0 "
                           +" then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where  enddate<='"+EndDate
                           +"' and riskcode  not in (select riskcode from lmriskapp where risktype3='7' and risktype<>'M')  and  grpcontno='"+GrpContNo+"' union all"
                           + " select case when payintv<>0 then  (days('"+EndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 "
                           +" when payintv=0 then (days('"+EndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B "
                           +" from lbpol where enddate>'"+EndDate+"' and riskcode  not in (select riskcode from lmriskapp where risktype3='7' and risktype<>'M') "
                           +" and grpcontno='"+GrpContNo+"'union all "
                           +" select case when payintv<>0  then (days(enddate)-days(cvalidate)+1)*prem*12*0.75/payintv/365  when payintv=0 "
                           +" then (days(enddate)-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lbpol where  enddate<='"+EndDate
                           +"' and riskcode  not in (select riskcode from lmriskapp where risktype3='7' and risktype<>'M')  and  grpcontno='"+GrpContNo+"'"
                           +" ) AS X with ur ";*/
            //实收保费
            String SrtSQL1 = "SELECT "
                             + "(SELECT coalesce(SUM(SumActuPayMoney),0) FROM ljapaygrp WHERE endorsementno IS NULL "
                             + "AND grpcontno='"+GrpContNo+"')+"
                             +
                             "(SELECT coalesce(SUM(getmoney),0) FROM ljagetendorse WHERE "
                             + "grpcontno='"+GrpContNo+"') FROM dual with ur";

            String temPrem = texecsql.getOneValue(SrtSQL1);
            if (temPrem.equals("")||temPrem.equals(null)){
                temPrem = "0";
            }
            TemPrem = Double.parseDouble(temPrem);
            if (TemPrem != 0) {
                ClaimRate = ClaimMoney / TemPrem * 100;
            }
        }
        double payrate = Arith.round(ClaimRate, 2);

        //按费用类新分类

        ListTable ClaimTable = new ListTable();
        ClaimTable.setName("ClaimR");
        String[] ClaimRTitle = {"Name", "Count", "CountRate", "Money",
                               "MoneyRate"};
        String ToSQL =
                "select distinct substr(getdutykind,1,1) from llclaimpolicy a1,llcase c1 " +
                " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09')  " +
                " and c1.endcasedate between '" + StartDate + "' and '" + EndDate +
                "' and a1.grpcontno = '" + GrpContNo + "' with ur";
        tss = texecsql.execSQL(ToSQL);
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            String[] strArr = new String[6];
            String ToNameSQL = "select codename,substr(a.code,1,1) from ldcode a where a.codetype='getdutykind' and a.code = '" +
                               tss.GetText(i, 1) + "00' with ur";
            strArr[0] = texecsql.getOneValue(ToNameSQL);
            String sql1 = "select count(distinct a1.caseno), coalesce(sum(a1.realpay),0) from llclaimdetail a1,llcase c1 " +
                          " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09') and a1.getdutykind in " +
                          "(select code from ldcode where codetype='getdutykind' and code like '" +
                          tss.GetText(i, 1) + "%') " +
                          "and c1.endcasedate between '" + StartDate + "' and '" +
                          EndDate +
                          "' and a1.grpcontno = '" + GrpContNo + "' with ur";
            rss = texecsql.execSQL(sql1);
            strArr[1] = rss.GetText(1, 1);
            double tempCountR = Arith.round(Double.parseDouble(rss.GetText(1, 1)) *
                                            100 / TotalCaseNo, 2);
            strArr[2] = String.valueOf(tempCountR);
            strArr[3] = rss.GetText(1, 2);
            double tempMoneyR = 0;
			if (TotalClaimMoney != 0) {
				tempMoneyR = Arith.round(Double.parseDouble(rss.GetText(1, 2))
						* 100 / TotalClaimMoney, 2);
			}
            strArr[4] = String.valueOf(tempMoneyR);
            ClaimTable.add(strArr);
        }

        //疾病分类

        //疾病合计
        String strSql =
                "select count(B) from ( select b.diseastype A ,a.caseno B from llcasecure a," +
                " ldicddiseastype b,llclaimpolicy c,llcase e where " +
                " c.caserelano=a.caserelano and c.caseno=e.caseno and a.caseno=e.caseno   and e.rgtstate in ('12','11','09') " +
                " and a.diseasecode between icdbegincode and icdendcode and diseastypelevel='1' " +
                " and e.endcasedate between '" + StartDate + "' and '" + EndDate +
                "' " +
                " and c.grpcontno='" + GrpContNo +
                "'  group by b.diseastype,a.caseno,a.diseasecode,a.diseasename )as x with ur";
        tss = texecsql.execSQL(strSql);
        int TotalCureCaseNo = 0;
        if (tss != null && tss.getMaxRow() > 0) {
            TotalCureCaseNo = Integer.parseInt(tss.GetText(1, 1));
        }

        ListTable DisClaimTable = new ListTable();
        DisClaimTable.setName("DisClaim");
        String[] DisClaimTitle = {"Name", "Rate"};

        ClaimSql = "select A,count(B) C from ( select b.diseastype A ,a.caseno B from llcasecure a," +
                   " ldicddiseastype b,llclaimpolicy c,llcase e where " +
                   " c.caserelano=a.caserelano and c.caseno=e.caseno and a.caseno=e.caseno   and e.rgtstate in ('12','11','09') " +
                   " and a.diseasecode between icdbegincode and icdendcode and diseastypelevel='1' " +
                   " and e.endcasedate between '" + StartDate + "' and '" + EndDate +
                   "' " +
                   " and c.grpcontno='" + GrpContNo + "'  group by b.diseastype,a.caseno,a.diseasecode,a.diseasename )as x group by A order by C desc with ur";
        System.out.println(ClaimSql);
        tss = texecsql.execSQL(ClaimSql);
        int tempDisOtherCount = 0;
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            String[] strArr = new String[2];
            if (Integer.parseInt(tss.GetText(i, 2)) * 100.0 / TotalCureCaseNo >
                5) {
                strArr[0] = tss.GetText(i, 1);
                strArr[1] = String.valueOf(Integer.parseInt(tss.GetText(i, 2)) *
                                           100.0 / TotalCureCaseNo);
                DisClaimTable.add(strArr);
            } else {
                tempDisOtherCount += Integer.parseInt(tss.GetText(i, 2));
            }

        }
        if (tss.getMaxRow() > 0 && tss != null) {
            String[] strArrOthDis = new String[2];
            strArrOthDis[0] = "其他";
            strArrOthDis[1] = String.valueOf(tempDisOtherCount * 100.0 /
                                             TotalCureCaseNo);
            DisClaimTable.add(strArrOthDis);
        }
        //意外事件

        //合计
        strSql = "select count(B) C from ( select t.accname A,a.caseno B  from LLAccident a,llclaimdetail c,llcase b,llaccidenttype t  where " +
                 " a.caseno=c.caseno  and c.caseno=b.caseno and b.rgtstate in ('12','11','09') and c.grpcontno='" +
                 GrpContNo + "'" +
                 " and a.code between substr(t.accidentno,1,3) and substr(t.accidentno,1,1)||substr(t.accidentno,4) and  t.accidentclase is null " +
                 " and b.endcasedate between '" + StartDate + "' and '" + EndDate +
                 "' group by a.caseno,t.accidentno,t.accname ) as x with ur";
        tss = texecsql.execSQL(strSql);
        int TotalAccidentCaseNo = 0;
        if (tss != null && tss.getMaxRow() > 0) {
            TotalAccidentCaseNo = Integer.parseInt(tss.GetText(1, 1));
        }

        ListTable AcciClaimTable = new ListTable();
        AcciClaimTable.setName("AcciClaim");
        String[] AcciClaimTitle = {"Name", "Rate"};

        ClaimSql = "select A,count(B) C from ( select t.accname A,a.caseno B  from LLAccident a,llclaimdetail c,llcase b,llaccidenttype t  where " +
                   " a.caseno=c.caseno  and c.caseno=b.caseno and b.rgtstate in ('12','11','09') and c.grpcontno='" +
                   GrpContNo + "'" +
                   " and a.code between substr(t.accidentno,1,3) and substr(t.accidentno,1,1)||substr(t.accidentno,4) and  t.accidentclase is null " +
                   " and b.endcasedate between '" + StartDate + "' and '" + EndDate +
                   "' group by a.caseno,t.accidentno,t.accname ) as x group by A order by C desc with ur";

        tss = texecsql.execSQL(ClaimSql);
        int tempAcciOtherCount = 0;
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            String[] strArr = new String[2];
            if (Integer.parseInt(tss.GetText(i, 2)) * 100.0 /
                TotalAccidentCaseNo > 5) {
                strArr[0] = tss.GetText(i, 1);
                strArr[1] = String.valueOf(Integer.parseInt(tss.GetText(i, 2)) *
                                           100.0 / TotalAccidentCaseNo);
                AcciClaimTable.add(strArr);
            } else {
                tempAcciOtherCount += Integer.parseInt(tss.GetText(i, 2));
            }
        }
        if (tss.getMaxRow() > 0 && tss != null) {
            String[] strArrOthAcci = new String[2];
            strArrOthAcci[0] = "其他";
            strArrOthAcci[1] = String.valueOf(tempAcciOtherCount * 100.0 /
                                              TotalAccidentCaseNo);
            AcciClaimTable.add(strArrOthAcci);
        }

//    End
        ClaimSql =
                "select distinct a.customerno ,a.customername,a.customersex,max(a.customerage)," +
                " count(distinct a.caseno) a,coalesce(sum(c.realpay),0) b," +
                " (select distinct insuredstat from lcinsured where insuredno=a.customerno and grpcontno='" +
                GrpContNo +
                "' union select distinct insuredstat from lbinsured where insuredno=a.customerno and grpcontno='" +
                GrpContNo + "' fetch first 1 rows only )," +
                " (select distinct RelationToMainInsured from lcinsured where insuredno=a.customerno and grpcontno='" +
                GrpContNo + "' union select distinct RelationToMainInsured from lbinsured where insuredno=a.customerno and grpcontno='" +
                GrpContNo + "' fetch first 1 rows only)" +
                " from llcase a,llclaimdetail c where  c.caseno=a.caseno and a.rgtstate in ('12','11','09') " +
                " and c.grpcontno='" + GrpContNo + "' and  a.endcasedate between '" +
                StartDate + "' and '" + EndDate
                +
                "' group by a.customerno,a.customername,a.customersex order by a,b desc with ur";
        System.out.println(ClaimSql);
        tss = texecsql.execSQL(ClaimSql);
        ListTable tCustCaseListTable = new ListTable();
        tCustCaseListTable.setName("CustCaseInfo");
        String[] tCustCaseListTitle = {"CSerial", "CName", "CAge", "CSex",
                                      "CFrequant", "CFRate", "CPayMoney",
                                      "CPRate", "attribute", "sort"};
        if (tss != null && tss.getMaxRow() > 0) {
            for (int i = 1; i <= tss.getMaxRow(); i++) {
                String strLine[] = new String[10];
                String[] rowdata = tss.getRowData(i);
                strLine[0] = i + "";
                strLine[1] = rowdata[1];
                strLine[2] = rowdata[3];
                if (rowdata[2].equals("0")) {
                    strLine[3] = "男";
                } else if (rowdata[2].equals("1")) {
                    strLine[3] = "女";
                } else {
                    strLine[3] = "其他";
                }
                strLine[4] = rowdata[4];
                int ccfrequ = Integer.parseInt(rowdata[4]);
                double ccfrate = 0.0;
                if (TotalCaseNo != 0) {
                    ccfrate = ccfrequ * 100.0 / TotalCaseNo;
                }
                ccfrate = Arith.round(ccfrate, 2);
                strLine[5] = ccfrate + "";
                strLine[6] = rowdata[5];
                double cpfrequ = Double.parseDouble(rowdata[5]);
                double cpfrate = 0.0;
                if (TotalClaimMoney != 0) {
                    cpfrate = cpfrequ * 100.0 / TotalClaimMoney;
                }
                cpfrate = Arith.round(cpfrate, 2);
                strLine[7] = cpfrate + "";
                if (rowdata[6].equals("1")) {
                    strLine[8] = "在职";
                } else if (rowdata[6].equals("2")) {
                    strLine[8] = "退休";
                } else {
                    strLine[8] = "其他";
                }

                if (rowdata[7].equals("00")) {
                    strLine[9] = "主被保人";
                } else {
                    strLine[9] = "连带被保人";
                }
                tCustCaseListTable.add(strLine);
            }
        }
        System.out.println("XmlExport Begin......");
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        //xmlexport.createDocuments("PayColPrt.vts", mGlobalInput); //最好紧接着就初始化xml文档
        System.out.println("XmlExport create Documents Begin......");
        xmlexport.createDocuments("GrpClaimReport.vts", mGlobalInput);
        //生成-年-月-日格式的日期

        //PayColPrtBL模版元素
//    texttag.add("BarCode1", aRgtNo);
//    texttag.add("BarCodeParam1"
//                , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("GrpName", aGrpName);
        texttag.add("GrpContNo", GrpContNo);
        texttag.add("StartDate", StartDate);
        texttag.add("Peoples2", Peoples2);
        texttag.add("EndDate", EndDate);
        texttag.add("TotalCaseNo", TotalCaseNo);
        texttag.add("TotalClaimMoney", TotalClaimMoney);
        texttag.add("payrate", payrate);

        texttag.add("count", count);
        texttag.add("SysDate", SysDate);
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(GetPlanListTable, Title);
        xmlexport.addListTable(GetClaimListTable, ClaimTitle);
        xmlexport.addListTable(ClaimTable, ClaimRTitle);
        xmlexport.addListTable(tCustCaseListTable, tCustCaseListTitle);
        xmlexport.addListTable(DisClaimTable, DisClaimTitle);
        xmlexport.addListTable(AcciClaimTable, AcciClaimTitle);
        System.out.println("xmlexport ending");
        //保存信息
        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println();
        return true;
    }

    private String DateFormat(String ori_date) {
        int sp_pos1 = ori_date.indexOf("-");
        int sp_pos2 = ori_date.lastIndexOf("-");
        String new_Date = ori_date.substring(0, sp_pos1) + "年" +
                          ori_date.substring(sp_pos1 + 1, sp_pos2) + "月" +
                          ori_date.substring(sp_pos2 + 1) + "日";
        return new_Date;
    }

    private String TranNull(double sss) {
        String returnstring = "  ";
        if (sss != 0) {
            returnstring = String.valueOf(sss);
        }
        return returnstring;
    }

    public static void main(String[] args) {

        GrpPayReportBL tGrpPayReportBL = new GrpPayReportBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "xuxin";
        tVData.addElement(tGlobalInput);
        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("00000008");
        tLOBatchPRTManagerSchema.setStandbyFlag3("0000000801");
        tVData.addElement(tLOBatchPRTManagerSchema);
        TransferData tTransferData = new TransferData();
        String StartDate = "2005-5-01";
        String EndDate = "2005-12-31";
        tTransferData.setNameAndValue("StartDate", StartDate);
        tTransferData.setNameAndValue("EndDate", EndDate);
        tVData.addElement(tTransferData);
        tGrpPayReportBL.submitData(tVData, "PRINT");
        VData vdata = tGrpPayReportBL.getResult();

    }

}
