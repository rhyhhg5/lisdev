/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * ClassName: LCGrpContF1PBL
 * </p>
 * <p>
 * Description: 保单xml文件生成，数据准备类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @Database: LIS
 * @CreateDate：2002-11-04 以后增加补充协议时需要将getScanDoc函数,private String[][] mDocFile =
 *                        null;注释取消
 */
public class LCGrpContF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    // 业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

    private LDComSchema mLDComSchema = new LDComSchema();

    private String mTemplatePath = null;

    private String mOutXmlPath = null;

    private XMLDatasets mXMLDatasets = null;

    private String[][] mScanFile = null;

    private String mSQL;

    private String ContPrintType = null;

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    private String printInsureDetail = "1";// 被保险人清单是否打印标记：0，不打印；1，打印

    private String contPrintFlag = "1";// 保单是否打印标记：0，不打印；1，打印
    
    private String testFlag = "";//试点打印标记

    private String NewJetFormType = "0"; //分公司打印使用新模板的标记:0,默认不使用
    
    // private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的， 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    // private Vector m_vGrpPolNo = new Vector();
    public LCGrpContF1PBL()
    {
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日24时止");
        return df.format(date);
    }

    /**
     * 主程序
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            // 判定打印类型
            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 全局变量赋值
            mOperate = cOperate;
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 数据校验

            if (!checkdate())
            {
                return false;
            }

            ExeSQL tExeSQL = new ExeSQL();
            ContPrintType = tExeSQL.getOneValue("select ContPrintType from lcgrpcont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'");
            // 正常打印处理
            if (mOperate.equals("PRINT"))
            {
                // 新建一个xml对象
                mXMLDatasets = new XMLDatasets();
                mXMLDatasets.createDocument();

                // 数据准备
                if (!getPrintData())
                {
                    return false;
                }
            }
            // 重打处理
            if (mOperate.equals("REPRINT"))
            {
                if (!getRePrintData())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 
     * @param cInputData
     *            VData
     * @return boolean 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 正常打印
        if (mOperate.equals("PRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
            mTemplatePath = (String) cInputData.get(2);
            mOutXmlPath = (String) cInputData.get(3);
            try
            {
                printInsureDetail = (String) cInputData.get(4);
                contPrintFlag = (String) cInputData.get(5);
            }
            catch (Exception e)
            {
                // do nothing
            }
        }
        // 重打模式
        if (mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
        }
        return true;
    }

    /**
     * 返回信息
     * 
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 出错处理
     * 
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception
    {
        // 团单Schema
        LCGrpContSchema tLCGrpContSchema = null;
        // 原则上一次只打印一个团单，当然可以一次多打
        tLCGrpContSchema = this.mLCGrpContSchema;

        // 取得该团单下的所有投保险种信息
        LCGrpPolSet tLCGrpPolSet = getRiskList(tLCGrpContSchema.getGrpContNo());
        // 正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(tLCGrpPolSet))
        {
            return false;
        }

        // 将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        // mResult.add(tLCGrpPolSet);//其实没必要更新这个表
        this.mLCGrpContSchema.setPrintCount(1);
        this.mLCGrpContSchema.setAgentCode(new ExeSQL().getOneValue(" select agentcode from lcgrpcont where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'" ));
        mResult.add(this.mLCGrpContSchema); // 只更新这个表就好拉
        mResult.add(mXMLDatasets); // xml数据流
        mResult.add(mGlobalInput); // 全局变量
        mResult.add(mOutXmlPath); // 输出目录
        mResult.add(mScanFile);
        mResult.add(contPrintFlag); // 保单是否打印标记：0，不打印；1，打印
        mResult.add(testFlag);	//新试点打印标记:"new",打印
        // mResult.add(mDocFile); //协议影印件数组

        // 后台提交
        LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        tLCGrpContF1PBLS.submitData(mResult, "PRINT");
        // 判错处理
        if (tLCGrpContF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        // 返回数据容器清空
        mResult.clear();
        System.out.println("add inputstream to mResult");
        // 将xml信息放入返回容器
        mResult.add(mXMLDatasets.getInputStream());
        return true;
    }

    /**
     * 查询协议影印件数据
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    // private boolean getScanDoc(XMLDataset cXmlDataset)
    // throws
    // Exception
    // {
    // XMLDataList tXmlDataList = new XMLDataList();
    // //标签对象，团单需要的影印件有两种类型
    // tXmlDataList.setDataObjectID("DocFile");
    //
    // //标签中的Header
    // tXmlDataList.addColHead("FileUrl");
    // tXmlDataList.addColHead("HttpUrl");
    // tXmlDataList.addColHead("PageIndex");
    // tXmlDataList.buildColHead();
    //
    // VData tVData = new VData();
    // tVData.add(mLCGrpContSchema.getPrtNo());
    // tVData.add("12");
    // tVData.add("TB");
    // tVData.add("TB1002");
    // EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();
    // if (!tEasyScanQueryBL.submitData(tVData, "QUERY||MAIN"))
    // {
    // System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
    // // buildError("getScanDoc", "查询保单影印件出错！");
    // // return false;
    // // return true;
    // }
    // else
    // {
    // tVData.clear();
    // tVData = tEasyScanQueryBL.getResult();
    // //Http信息对象
    // VData tUrl = (VData) tVData.get(0);
    // //页面信息对象
    // VData tPages = (VData) tVData.get(1);
    // String tStrUrl = "";
    // String tStrPages = "";
    // //根据查询结果初始化影印件信息数组
    // mDocFile = new String[tUrl.size()][2];
    // for (int nIndex = 0; nIndex < tUrl.size(); nIndex++)
    // {
    // tStrUrl = (String) tUrl.get(nIndex);
    // tStrPages = (String) tPages.get(nIndex);
    // tStrUrl = tStrUrl.substring(0,
    // tStrUrl.lastIndexOf(".")) + ".tif";
    // mDocFile[nIndex][0] = tStrUrl;
    // //协议影印件信息，采用此命名方法
    // mDocFile[nIndex][1] = "Agreement_" +
    // mLCGrpContSchema.getGrpContNo() +
    // "_" + tStrPages + ".tif";
    // tXmlDataList.setColValue("FileUrl", mDocFile[nIndex][1]);
    // tXmlDataList.setColValue("HttpUrl", tStrUrl);
    // tXmlDataList.setColValue("PageIndex", nIndex);
    // tXmlDataList.insertRow(0);
    // }
    // }
    // cXmlDataset.addDataObject(tXmlDataList);
    // return true;
    // }
    /**
     * 查询影印件数据
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getScanPic(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        // 标签对象，团单需要的影印件有两种类型
        tXmlDataList.setDataObjectID("PicFile");

        // 标签中的Header
        tXmlDataList.addColHead("FileUrl");
        tXmlDataList.addColHead("HttpUrl");
        tXmlDataList.addColHead("PageIndex");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.buildColHead();
        if (ContPrintType.equals("0") || "4".equals(ContPrintType))
            mSQL = "select * from es_doc_main where doccode = '" + this.mLCGrpContSchema.getPrtNo()
                    + "' order by doccode";
        else
            mSQL = "select * from es_doc_main where doccode = '" + this.mLCGrpContSchema.getPrtNo()
                    + "' and busstype<>'HM' order by doccode";
        System.out.println("mSQL is " + mSQL);
        ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINDBSet = new ES_DOC_MAINSet();
        tES_DOC_MAINDBSet = tES_DOC_MAINDB.executeQuery(mSQL);
        int piccount = 1;
        if (tES_DOC_MAINDBSet.size() > 0)
        {
            ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
            ES_SERVER_INFOSet tES_SERVER_INFOSet = new ES_SERVER_INFOSet();
            tES_SERVER_INFOSet = tES_SERVER_INFODB.query();
            String thttphead = "http://" + tES_SERVER_INFOSet.get(1).getServerPort() + "/";
//            String trealpath = this.getClass().getResource("/").getPath();
            String trealpath = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'UIRoot'");
            
            int k = 0;
            for (int i = 0; i < tES_DOC_MAINDBSet.size(); i++)
            {
                k = k + (int) tES_DOC_MAINDBSet.get(i + 1).getNumPages();
            }
            String tStrUrl = "";
            mScanFile = new String[k][3];
            for (int j = 0; j < tES_DOC_MAINDBSet.size(); j++)
            {
                ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
                ES_DOC_PAGESSet tES_DOC_PAGESSet = new ES_DOC_PAGESSet();
                // tES_DOC_PAGESDB.setDocID(tES_DOC_MAINDBSet.get(j +
                // 1).getDocID());
                // tES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
                tES_DOC_PAGESSet = tES_DOC_PAGESDB.executeQuery("select * from es_doc_pages where docid="
                        + tES_DOC_MAINDBSet.get(j + 1).getDocID() + " order by pagecode asc");
                if (tES_DOC_PAGESSet.size() > 0)
                {
                	// 错误影印件明细
            		String errorInf = "";
            		//错误影印件计数
        			int errorCont = 0;
        			//错误标记
        			boolean errorFlag = true;
                	for (int m = 0; m < tES_DOC_PAGESSet.size(); m++)
                    {
                    	
                    	String tpath = tES_DOC_PAGESSet.get(m + 1).getPicPath();
//                    	String tcheckhead = trealpath.replaceAll("WEB-INF/classes/",tpath);
                    	String tcheckhead = trealpath+tpath;
                        String tCheckUrl = tcheckhead
                                + tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        File cScanFile = new File(tCheckUrl);
                        if(!cScanFile.exists()){
                    	  String tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2014");
                          tcheckhead = trealpath+tnewpath;
                       	  tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                        	     tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2012");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                            	 tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                    	         tnewpath = tpath.replaceAll("EasyScan2016","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2014为基础
                                 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan2012");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                            	 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                                 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2012为基础
                                 tnewpath = tpath.replaceAll("EasyScan2012","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                                 tnewpath = tpath.replaceAll("EasyScan2012","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2011为基础
                                 tnewpath = tpath.replaceAll("EasyScan2011","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                                                   System.out.println("新老路径都不存在文件");
                	                            	// 显示明细错误影印件明细页
                	                            	errorInf = errorInf + (m + 1) + "、";
                									// 遇到丢失影印件errorFlag置为false
                	                            	errorFlag = false;
                									// 错误影印件计数
                									errorCont++;
                									continue;
                                                }else{
                                                	tpath=tnewpath;
                                                	System.out.println("2011-EasyScan新路径存在文件");
                                                }
                                                }else{
                                            	tpath=tnewpath;
                                            	System.out.println("2012-EasyScan新路径存在文件");
                                                }
                                                }else{
                                        	    tpath=tnewpath;
                                        	    System.out.println("2012-2011新路径存在文件");
                                                }
                                                }else{
                                    	        tpath=tnewpath;
                                    	        System.out.println("2014-EasyScan新路径存在文件");
                                                }
                                                }else{ 
                                                tpath=tnewpath;
                                	            System.out.println("2014-2011新路径存在文件");
                                                }
                                                }else{
                            	                tpath=tnewpath;
                            	                System.out.println("2014-2012新路径存在文件");
                                                }
                                                }else{	tpath=tnewpath;
                          	                    System.out.println("2016-EasyScan新路径存在文件");
                          	                    } 
                                                }else{
                     		                    tpath=tnewpath;
                            	                System.out.println("2016-2011新路径存在文件");
                            	                }
                                                }else{
                     		                    tpath=tnewpath;
                            	                System.out.println("2016-2012新路径存在文件");
                            	                }
                                                }else{
                    		                    tpath=tnewpath;
                           	                    System.out.println("2016-2014新路径存在文件");
                           	                    }
                                                }else{
                        	System.out.println("未变更路径存在文件");
                        }
                        
                        tStrUrl = thttphead + tpath
                                + tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        mScanFile[piccount - 1][0] = tStrUrl;
                        mScanFile[piccount - 1][1] = this.mLCGrpContSchema.getGrpContNo() + "_" + k + "_" + piccount
                                + ".tif";
                        mScanFile[piccount - 1][2] = tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        piccount++;
                    }
                	
                	if (!errorFlag) {
    					if (errorCont == tES_DOC_PAGESSet.size() && tES_DOC_PAGESSet.size() != 1) {
    						// 去除字符串最后一个顿号
    						buildError("getScanPic","第"+ errorInf.substring(0,errorInf.length() - 1)+ "张扫描件异常，请先保留第一张扫描件，并对其它扫描件进行扫描修改，修改后再对第一张扫描件进行修改。");
    						return false;
    					} else {
    						buildError("getScanPic", "第"+ errorInf.substring(0, errorInf.length() - 1)+ "张影印件异常，请对这" + errorCont + "张影印件进行扫描修改！");
    						return false;
    					}
            		}
                }
            }
        }
        System.out.println("ddddfdf");
        if (mScanFile != null)
        {
            System.out.println("sdsf");
            for (int i = 0; i < mScanFile.length; i++)
            {
                tXmlDataList.setColValue("FileUrl", mScanFile[i][1]);
                tXmlDataList.setColValue("HttpUrl", mScanFile[i][0]);
                tXmlDataList.setColValue("PageIndex", i + 1);
                tXmlDataList.setColValue("FileName", mScanFile[i][2]);
                tXmlDataList.insertRow(0);
            }
        }
        cXmlDataset.addDataObject(tXmlDataList);
        // 添加图片标记节点
        if (piccount > 1)
            cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "1"));
        else
            cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "0"));
        return true;
    }

    /**
     * 条款信息查询
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getSpecDoc(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList xmlDataList = new XMLDataList();
        // 添加xml一个新的对象Term
        xmlDataList.setDataObjectID("LCCGrpSpec");
        // 添加Head单元内的信息
        xmlDataList.addColHead("RowID");
        xmlDataList.addColHead("SpecContent");
        xmlDataList.buildColHead();
        // 查询合同下的特约信息
        LCCGrpSpecDB tLCCGrpSpecDB = new LCCGrpSpecDB();
        tLCCGrpSpecDB.setProposalGrpContNo(this.mLCGrpContSchema.getProposalGrpContNo());
        LCCGrpSpecSet tLCCGrpSpecSet = tLCCGrpSpecDB.query();
        for (int i = 1; i <= tLCCGrpSpecSet.size(); i++)
        {
            xmlDataList.setColValue("RowID", i);
            xmlDataList.setColValue("SpecContent", tLCCGrpSpecSet.get(i).getSpecContent());
            xmlDataList.insertRow(0);
        }
        cXmlDataset.addDataObject(xmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @param cLCGrpPolSet
     *            LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getTerm(XMLDataset cXmlDataset, LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        // 添加xml一个新的对象Term
        tXmlDataList.setDataObjectID("Term");
        // 添加Head单元内的信息
        tXmlDataList.addColHead("PrintIndex");
        tXmlDataList.addColHead("TermName");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.addColHead("DocumentName");
        tXmlDataList.buildColHead();

        // 判断保单是否为分公司打印
        String sql = "select count(1) from LDCode1 where CodeType = 'printchannel' and CodeName = '1' "
                + "and Code = '" + this.mLCGrpContSchema.getManageCom().substring(0, 4) + "' and Code1 = '2' ";
        boolean isFilialePrint = new ExeSQL().getOneValue(sql).equals("1");

        // 设置查询对象
        ExeSQL tExeSQL = new ExeSQL();
        // 查询保单下的险种条款，根据条款级别排序
        // 查询合同下的全部唯一主条款信息
        String tSql;
        if (isFilialePrint)
        {
            // 分公司打印排序规则（投保顺序）为险种编码
            tSql = "select distinct ItemName,FileName,a.RiskCode, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                    + "' and ItemType = '0' order by a.RiskCode";
        }
        else
        {
            tSql = "select distinct ItemName,FileName,a.RiskCode, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                    + "' and ItemType = '0' order by b.GrpProposalNo";
        }
        String TSQL = "select 1 from lcgrppol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
    				+ "' and riskcode like '8201%'";      
        String ttsql = "select 1 from lcgrppol where grpcontno='"
   			 		 + this.mLCGrpContSchema.getGrpContNo()
   			 		 + "' and riskcode ='820101'";
	    SSRS tSSRS = new SSRS();
	    SSRS tSSRS2 = new SSRS();
	    SSRS tSSRS3 = new SSRS();
	    SSRS tSSRS4 = new SSRS();
	    tSSRS3 = tExeSQL.execSQL(TSQL);
	    tSSRS4 = tExeSQL.execSQL(ttsql);
	    if(tSSRS3.getMaxRow() > 0){
	        tSql = "select distinct ItemName,FileName,a.RiskCode " 
	        	 + "from LDRiskPrint a, LCGrpPol b "
	        	 + "where a.RiskCode = b.RiskCode and a.riskcode not like '8201%' and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
	        	 + "' and ItemType = '0' "
	        	 + "union "
	        	 + "select distinct ItemName,FileName,'820101' " 
	        	 + "from LDRiskPrint a, LCGrpPol b "
	        	 + "where a.RiskCode = b.RiskCode and a.riskcode in (select code from ldcode where codetype='risk8201' and othersign='1') and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
	        	 + "' and ItemType = '0' "
	        	 + "union "
	        	 + "select distinct ItemName,FileName,'820129' " 
	        	 + "from LDRiskPrint a, LCGrpPol b "
	        	 + "where a.RiskCode = b.RiskCode and a.riskcode in (select code from ldcode where codetype='risk8201' and othersign='2') and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
	        	 + "' and ItemType = '0' ";
	    }        
        tSSRS = tExeSQL.execSQL(tSql);
        System.out.println("tSSRS" + tSql);
        if (tSSRS.getMaxRow() > 0)
        {
            HashMap map = new HashMap();
            int index = 0;
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
            	SSRS mSSRS = new SSRS();
            	String sqlterm = "select code from ldcode where codetype = 'not_termname' and code = '"+tSSRS.GetText(i, 3)+"'";
            	mSSRS = tExeSQL.execSQL(sqlterm);
                if (map.get(tSSRS.GetText(i, 2)) == null)
                {
                    tXmlDataList.setColValue("PrintIndex", i);
                    tXmlDataList.setColValue("TermName", tSSRS.GetText(i, 1));
                    tXmlDataList.setColValue("FileName", "0");
                    tXmlDataList.setColValue("DocumentName", tSSRS.GetText(i, 2));
                    tXmlDataList.insertRow(0);

                    if (isFilialePrint)
                    {
                        map.put(tSSRS.GetText(i, 2), "1");
                    }
                }
                
                // 查询当前主条款下的责任条款信息
                tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCGrpPol where GrpContNo = '"
                        + this.mLCGrpContSchema.getGrpContNo()
                        + "') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where RiskCode = '"
                        + tSSRS.GetText(i, 3) + "')";
                tSSRS2 = tExeSQL.execSQL(tSql);
                System.out.println("tSSRS2" + tSql);
                for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
                {
                	if(mSSRS.getMaxRow()==0){
                		tXmlDataList.setColValue("PrintIndex", index++);
                        tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                        tXmlDataList.setColValue("FileName", "1");
                        tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                        tXmlDataList.insertRow(0);	
                	}
                    /*tXmlDataList.setColValue("PrintIndex", index++);
                    tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                    tXmlDataList.setColValue("FileName", "1");
                    tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                    tXmlDataList.insertRow(0);*/
                }
            }
            String ttsql1 = "select 1 from lcgrppol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
    					+ "' and riskcode in (select code from ldcode where codetype='risk8201' and othersign='1') and not exists(select 1 from lcgrppol where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='820101')";      
            String ttsql2 = "select 1 from lcgrppol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
						+ "' and riskcode in (select code from ldcode where codetype='risk8201' and othersign='2') and not exists(select 1 from lcgrppol where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='820129')";
		    SSRS ttSSRS3 = new SSRS();
		    SSRS ttSSRS4 = new SSRS();
		    ttSSRS3 = tExeSQL.execSQL(ttsql1);
		    ttSSRS4 = tExeSQL.execSQL(ttsql2);
	        if(ttSSRS3.getMaxRow() > 0){
	        	tXmlDataList.setColValue("PrintIndex", tSSRS.getMaxRow());
	            tXmlDataList.setColValue("TermName", "健康守望补充医疗保险（B款）");
	            tXmlDataList.setColValue("FileName", "1");
	            tXmlDataList.setColValue("DocumentName", "820101");
	            tXmlDataList.insertRow(0);
	        }
	        if(ttSSRS4.getMaxRow() > 0){
	        	tXmlDataList.setColValue("PrintIndex", tSSRS.getMaxRow());
	            tXmlDataList.setColValue("TermName", "健康守望团体补充医疗保险（B款）");
	            tXmlDataList.setColValue("FileName", "1");
	            tXmlDataList.setColValue("DocumentName", "820129");
	            tXmlDataList.insertRow(0);
	        }
	        
        }
        else
        {
            // 查询当前主条款下的责任条款信息
            tSql = "select distinct ItemName,FileName, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode " + "   and b.GrpContNo = '" + mLCGrpContSchema.getGrpContNo()
                    + "' " + "  and ItemType = '1' " + "order by b.GrpProposalNo ";
            tSSRS2 = tExeSQL.execSQL(tSql);
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
            {
                tXmlDataList.setColValue("PrintIndex", j);
                tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                tXmlDataList.setColValue("FileName", "1");
                tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                tXmlDataList.insertRow(0);
            }

        }
        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 正常打印流程
     * 
     * @param cLCGrpPolSet
     *            LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        // xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        // 查询个单合同表信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        // 根据合同号查询
        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpContDB.getInfo())
        {
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();

        // 团单类型节点
        //        if ("86650105".equals(mLCGrpContSchema.getManageCom())) // 如果判断是新疆该机构保单，则采用新疆共保专用模版打印
        //        {
        //            tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J103"));
        //        }
        //        else if (jugdeUliByGrpContNo())
      //是否是内蒙保单
        if(checkRiskcode()){
        	tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J103"));
            testFlag = "new";
        }else{
            if(ManageCom()){
            	tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J107"));
            	testFlag = "new";
            }else{
            	if (jugdeUliByGrpContNo())
            	{
            		tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J102"));
            		testFlag = "new";
            	}
            	else
            	{
            		if(checkJetFormType()){
            			tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J108"));
            			NewJetFormType = "J108";
            		}else{
            			tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J101"));
            		}
            		testFlag = "new";
            	}
            }
        }
        //add by zjd 归档号  2014-11-24
        String ArchiveNo = new ExeSQL().getOneValue("select ArchiveNo from ES_DOC_Main where DocCode = '"
                + mLCGrpContSchema.getPrtNo() + "' order by DocId fetch first 1 rows only");
        tXMLDataset.addDataObject(new XMLDataTag("ArchiveNo", ArchiveNo));
        System.out.println("ArchiveNo归档号是:"+ArchiveNo);
        // 团单四位公司编码节点
        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", mLCGrpContSchema.getManageCom().substring(0, 4)));
        // 被保险人清单是否打印节点：0，不打印；1，打印
        tXMLDataset.addDataObject(new XMLDataTag("LCInsuredDetailFlag", printInsureDetail));

        // 团个单标志，1个单，2团单
        tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));
        if (jugdeUliByGrpContNo())
        {
            tXMLDataset.addDataObject(new XMLDataTag("FinishDate", "每个被保险人老年护理保险金领取起始日止"));
        }
        else
        {
            // 保险合同终止日期
            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            String mSql = "select cinvalidate from LCGrpCont where GrpContNo = '"
                    + this.mLCGrpContSchema.getGrpContNo() + "'";

            mSSRS = mExeSQL.execSQL(mSql);

            if (mSSRS.GetText(1, 1) != null)
            {
                if ("9999".equals(mSSRS.GetText(1, 1).substring(0, 4)))
                {
                    tXMLDataset.addDataObject(new XMLDataTag("FinishDate", "终身"));
                }
                else
                {
                    String EndDate = getDate((new FDate()).getDate(mSSRS.GetText(1, 1)));
                    tXMLDataset.addDataObject(new XMLDataTag("FinishDate", EndDate));
                }
            }
        }

        // 缴费年期
        // 如果是团单终身重疾并且缴费方式不为趸交的时候显示缴费年期
        String sSql = "select max(payyears) from LCPol where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and riskcode in(select code from ldcode where " + " codetype = 'payyears') and payintv != 0";

        String PayYears = new ExeSQL().getOneValue(sSql);

        if (!"".equals(PayYears) && !"null".equals(PayYears) && !"1".equals(PayYears))
        {
            tXMLDataset.addDataObject(new XMLDataTag("PayYears", "缴费年期:" + PayYears + "年"));
        }

        // 获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo())
        {
            // 只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError())
            {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 默认填入空
            tXMLDataset.setTemplate("");
        }
        else
        {
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }

        if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("0"))
        {
            mXMLDatasets.setDTDName(this.mLCGrpContSchema.getGrpContNo() + ".dtd");
        }
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCGrpContSchema.getAgentCode());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo())
        {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        this.mLCGrpContSchema.setAgentCode(mLAAgentSchema.getGroupAgentCode());
        tXMLDataset.addSchema(this.mLCGrpContSchema);
        // 如果是卡单型险种需要查询抵达国家
        if (!StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals(""))
        {
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++)
            {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size())
                {
                    Nation += ",";
                }
            }
            tXMLDataset.addDataObject(new XMLDataTag("Nation", Nation));
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLDGrpDB.getInfo();
            tXMLDataset.addDataObject(new XMLDataTag("GrpEnglithName", tLDGrpDB.getGrpEnglishName()));
        }

        // 查询投保人信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        // 根据合同号查询
        tLCGrpAppntDB.setGrpContNo(tLCGrpContDB.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAppntDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCGrpAppntSchema = tLCGrpAppntDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLCGrpAppntSchema);

        // 查询投保人的地址信息
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        // 根据投保人编码和地址编码查询
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAddressDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人地址信息失败！");
            return false;
        }
        this.mLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLCGrpAddressSchema);

        // 查询管理机构信息
        LDComDB tLDComDB = new LDComDB();
        // 根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDComDB.setComCode(tLCGrpContDB.getManageCom());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDComDB.getInfo())
        {
            buildError("getInfo", "查询管理机构信息失败！");
            return false;
        }
        this.mLDComSchema = tLDComDB.getSchema();
        //add by zjd 打印显示管理结构修改  原显示 人保财险机构名称 
        if("Y".equals(mLDComSchema.getInteractFlag())){
        	tLDComDB.setComCode(tLCGrpContDB.getManageCom().substring(0, 4));
        	if (!tLDComDB.getInfo())
            {
                buildError("getInfo", "查询管理机构信息失败！");
                return false;
            }
        	mLDComSchema.setName(tLDComDB.getSchema().getName());
        }
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLDComSchema);

        
        this.mLAAgentSchema.setAgentCode(mLAAgentSchema.getGroupAgentCode());
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAAgentSchema);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.mLCGrpContSchema.getAgentGroup());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLABranchGroupDB.getInfo())
        {
            buildError("getInfo", "查询销售机构信息失败！");
            return false;
        }
        this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLABranchGroupSchema);
        
        // by gzh 20120521
        //增加中介机构及中介机构销售人员信息
        String tAgentCom = this.mLCGrpContSchema.getAgentCom();
        if(tAgentCom != null && !"".equals(tAgentCom)){
        	LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(tAgentCom);
//          如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAComDB.getInfo())
            {
            	buildError("getInfo", "查询中介机构信息失败！");
                return false;
            }
            this.mLAComSchema = tLAComDB.getSchema();
        }
//      将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAComSchema);
        
        String tAgentSaleCode = this.mLCGrpContSchema.getAgentSaleCode();
        if(tAgentSaleCode != null && !"".equals(tAgentSaleCode)){
        	LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
            tLAAgenttempDB.setAgentCode(tAgentSaleCode);
            // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAAgenttempDB.getInfo())
            {
                buildError("getInfo", "查询中介机构代理业务员信息失败！");
                return false;
            }
            this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
        }
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAAgenttempSchema);
        // by gzh end 20120521

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "select cvalidate,cinvalidate from LCGrpCont where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("BeginDate", tSSRS.GetText(1, 1)));
        tXMLDataset.addDataObject(new XMLDataTag("EndDate", tSSRS.GetText(1, 2)));

        // 暂交费收据号
        tSql = "select distinct TempFeeNo from LJTempFee where OtherNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and OtherNoType = '7'";
        tSSRS = tExeSQL.execSQL(tSql);
        if (!mLCGrpContSchema.getAppFlag().equals("9"))
        {
            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", tSSRS.GetText(1, 1))); // 收据号
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", "")); // 收据号
        }

        //是否是内蒙保单
        if(ManageCom()){      
            String tSqls = "select sum(sumactupaymoney) from ljapay where incomeno='"
            			 + this.mLCGrpContSchema.getGrpContNo() 
            		  	 + "' and duefeetype='0' ";
            tSSRS = tExeSQL.execSQL(tSqls);
            tXMLDataset.addDataObject(new XMLDataTag("Prem", cancelKeXueCount(format(Double.parseDouble(tSSRS.GetText(1,
                    1))))));
            
            String tSqls1 = "select sum(sumactupaymoney) from ljapay where incomeno='"
       			 + this.mLCGrpContSchema.getGrpContNo() 
       		  	 + "'";
            tSSRS = tExeSQL.execSQL(tSqls1);
            tXMLDataset.addDataObject(new XMLDataTag("SumPrem", cancelKeXueCount(format(Double.parseDouble(tSSRS.GetText(1,
               1))))));
        }else{
      
	        // 总保费
	        tSql = "select prem from LCGrpCont where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo() + "'";
	        tSSRS = tExeSQL.execSQL(tSql);
	        tXMLDataset.addDataObject(new XMLDataTag("SumPrem", cancelKeXueCount(format(Double.parseDouble(tSSRS.GetText(1,
	                1))))));
        }
        
        String cDate = PubFun.getCurrentDate().split("-")[0] + "年" + PubFun.getCurrentDate().split("-")[1] + "月"
                + PubFun.getCurrentDate().split("-")[2] + "日";
        tXMLDataset.addDataObject(new XMLDataTag("Today", cDate));

        // 增加：保单打印时间/签单时间/收费时间
        String tTmpGrpContNo = mLCGrpContSchema.getGrpContNo();
        String tTmpPrtNo = mLCGrpContSchema.getPrtNo();

        String tContPrintDate = cDate;
        String tContPrintTime = PubFun.getCurrentTime();
        tContPrintTime = tContPrintTime.split(":")[0] + "时" + tContPrintTime.split(":")[1] + "分";
        tXMLDataset.addDataObject(new XMLDataTag("ContPrintDateTime", tContPrintDate + tContPrintTime));

        String tContSignDate = tExeSQL.getOneValue("select SignDate from LCGrpCont where GrpContNo = '" + tTmpGrpContNo
                + "'");
        if (tContSignDate != null && !tContSignDate.equals(""))
        {
            tContSignDate = tContSignDate.split("-")[0] + "年" + tContSignDate.split("-")[1] + "月"
                    + tContSignDate.split("-")[2] + "日";
            String tContSignTime = tExeSQL
                    .getOneValue("select replace(varchar(SignTime), '.', ':') from LCGrpCont where GrpContNo = '"
                            + tTmpGrpContNo + "'");
            if (tContSignTime == null || tContSignTime.equals(""))
                tContSignTime = PubFun.getCurrentTime();
            tContSignTime = tContSignTime.split(":")[0] + "时" + tContSignTime.split(":")[1] + "分";
            tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime", tContSignDate + tContSignTime));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime", "----"));
        }

        String tPayConfDate = tExeSQL.getOneValue("select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                + tTmpGrpContNo + "' or OtherNo = '" + tTmpPrtNo + "') ");
        if (tPayConfDate != null && !tPayConfDate.equals(""))
        {
            tPayConfDate = tPayConfDate.split("-")[0] + "年" + tPayConfDate.split("-")[1] + "月"
                    + tPayConfDate.split("-")[2] + "日";
            String tPayConfTime = tExeSQL
                    .getOneValue("select replace(varchar(max(confMakeTime)), '.', ':') from LJTempFee where (OtherNo = '"
                            + tTmpGrpContNo
                            + "' or OtherNo = '"
                            + tTmpPrtNo
                            + "') and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                            + tTmpGrpContNo + "'or OtherNo = '" + tTmpPrtNo + "'))");
            if (tPayConfTime == null || tPayConfTime.equals(""))
            {
                tPayConfTime = tExeSQL
                        .getOneValue("select replace(varchar(max(MakeTime)), '.', ':') from LJTempFee where (OtherNo = '"
                                + tTmpGrpContNo
                                + "' or OtherNo = '"
                                + tTmpPrtNo
                                + "') and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                                + tTmpGrpContNo + "'or OtherNo = '" + tTmpPrtNo + "'))");
            }
            tPayConfTime = tPayConfTime.split(":")[0] + "时" + tPayConfTime.split(":")[1] + "分";
            tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime", tPayConfDate + tPayConfTime));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime", "----"));
        }
        // --------------------

        // 操作人员编码和操作人员姓名
        tSql = "select UserCode,UserName from LDUser where UserCode in (Select Operator from LJTempFee where OtherNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "' and OtherNoType = '7')";
        tSSRS = tExeSQL.execSQL(tSql);
        if (!((String) mLCGrpContSchema.getAppFlag()).equals("9"))
        {
            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode", tSSRS.GetText(1, 1)));
            tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS.GetText(1, 2)));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode", mGlobalInput.Operator));
            String tempSql = "select UserName from lduser where usercode='" + mGlobalInput.Operator + "'";
            tSSRS = tExeSQL.execSQL(tempSql);
            tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS.GetText(1, 1)));

        }
        
        if("8644".equals(this.mLCGrpContSchema.getManageCom().substring(0, 4))){
        	tXMLDataset.addDataObject(new XMLDataTag("ManageComSpecFlag", "1"));
        }else{
        	tXMLDataset.addDataObject(new XMLDataTag("ManageComSpecFlag", "0"));
        }
        
        // 产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genInsuredList(tXMLDataset))
        {
            return false;
        }

        // 获取特约信息
        if (!getSpecDoc(tXMLDataset))
        {
            return false;
        }

        // 获取条款信息
        if (!getTerm(tXMLDataset, cLCGrpPolSet))
        {
            return false;
        }

        // 获取协议影印件信息,以后打印协议时用
        // if (!getScanDoc(tXMLDataset))
        // {
        // return false;
        // }

        // 产生团单下被保人清单
        if (!genInsuredDetail(tXMLDataset))
        {
            return false;
        }

        // 获取保单影印件信息
        // 下面4行信息注掉，用于以后的补充协议打印
        if (!getScanPic(tXMLDataset))
        {
            return false;
        }
        // 获取被保险人急救医疗卡信息
        if (!StrTool.cTrim(this.mLCGrpContSchema.getCardFlag()).equals(""))
        {
            if (!genInsuredCard(tXMLDataset))
            {
                return false;
            }
        }
        // 获取定点医院信息
        // if (!getHospital(tXMLDataset))
        // {
        // return false;
        // }
        // System.out.println("定点医院明细准备完毕。。。");
        
        
        
        //获取收款凭证
        if (!getReceiptNote(tXMLDataset)){
        	return false;
        }
        
        
        String tttSql = "select 1 from loprtmanager2 "
        			  + "where code='35' and otherno in ( "
        			  + "select payno from ljapay "
        			  + "where incomeno='"+ this.mLCGrpContSchema.getGrpContNo() + "') ";
        SSRS tttSSRS = new ExeSQL().execSQL(tttSql);
        String tttsql1 = "select 1 from LYOutPayDetail "
        		       + "where insureno='"+ this.mLCGrpContSchema.getPrtNo() +"' and billprintdate is not null ";
        SSRS tttSSRS1 = new ExeSQL().execSQL(tttsql1);
        String tttsql2 = "select 1 from ljapay "
        			   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
        SSRS tttSSRS2 = new ExeSQL().execSQL(tttsql2);
        if(tttSSRS.getMaxRow()>0||tttSSRS1.getMaxRow()>0||tttSSRS2.getMaxRow()==0){
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "0"));
        }else{
        	
        	//保费收据字段标识 0：不打收据 1：打收据
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "1"));
        	
        	 //保单号
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptPolicyNo", this.mLCGrpContSchema.getGrpContNo()));
            
        	//实收号
        	String tStrSql0= "select payno from ljapay "
        				   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
        	SSRS tSSRS0 = new ExeSQL().execSQL(tStrSql0);
        	if(tSSRS0.getMaxRow()>0){
        		tXMLDataset.addDataObject(new XMLDataTag("ReceiptPayNo",  tSSRS0.GetText(1,1)));

        	}
        	
            //保单收费时间
        	    
            String tStrSql1= "select min(ConfMakeDate) from LJTempFee where (OtherNo = '"
            			   +this.mLCGrpContSchema.getGrpContNo() +"' or OtherNo = '" +this.mLCGrpContSchema.getPrtNo() + "') ";       
            String payenddate=tExeSQL.getOneValue(tStrSql1);
            if (payenddate != null && !payenddate.equals("")){
            	payenddate = payenddate.split("-")[0] + "年" + payenddate.split("-")[1] + "月"
            			+ payenddate.split("-")[2] + "日";
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptPayTime", payenddate));
            }
            
            //投保单位/投保人
            String tStrSql2= "select name from lcgrpappnt "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
            SSRS tSSRS2 = new ExeSQL().execSQL(tStrSql2);
            if(tSSRS2.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptAppName",  tSSRS2.GetText(1,1)));
            }
            
            //交费方式
            String tStrSql3= "select codename from ldcode "
            			   + "where codetype='paymode' and code in ("
            			   + "select paymode from lcgrpcont "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "')";
            SSRS tSSRS3 = new ExeSQL().execSQL(tStrSql3);
            if(tSSRS3.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptWayToPay",  tSSRS3.GetText(1,1)));
            }	
            
            //总保费金额 人民币大小写
            String tStrSql4= "select sumactupaymoney from ljapay "
            			   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' and duefeetype='0'";
            String a=tExeSQL.getOneValue(tStrSql4);    
            if(a != null && !a.equals("")){
            	double p=Double.parseDouble(a);
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPrem",  new java.text.DecimalFormat("#.00").format(p)));		
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPremC",  PubFun.getChnMoney(p)));		
            }		
            			  
            //收款方名称和地址
            String tStrSql5= "select name,address from ldcom "
            			   + "where comcode in ( "
            			   + "select managecom from lcgrpcont "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "')";
            SSRS tSSRS5 = new ExeSQL().execSQL(tStrSql5);
            if(tSSRS5.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptReceivingName",  tSSRS5.GetText(1,1)));
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptReceivingAdd",  tSSRS5.GetText(1,2)));
            }
            
            //获取险种名称以及险种保费
            if(!getPremDetail(tXMLDataset)){
            	return false;
            }
        }
        
        if("9".equals(this.mLCGrpContSchema.getAppFlag())){
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPrem",  new java.text.DecimalFormat("#.00").format(this.mLCGrpContSchema.getPrem())));		
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPremC",  PubFun.getChnMoney(this.mLCGrpContSchema.getPrem())));
        }
        
        // 获取中文保费的信息
        if (!getMoney(tXMLDataset))
        {
            return false;
        }
        // 获取发票信息
        if (!this.mLCGrpContSchema.getAppFlag().equals("9"))
        {
            if (!getInvoice(tXMLDataset))
            {
                return false;
            }
        }

        
        // 生成团体保险期间
        if (!getGrpInsuYear(tXMLDataset))
        {
            return false;
        }
        
        // 对于产品370301，生成退保费率
        if (checkRiskcode())
        {
        	if (!getRiskGrpNewCont(tXMLDataset))
            {
                return false;
            }
        	if (!getFCRiskZTFee(tXMLDataset))
            {
                return false;
            }
        }
       
        //如果为真，则执行新增的这三个方法，反之则不执行
        if (jugdeUliByGrpContNo())
        {
            if (!getGrpNewCont(tXMLDataset))
            {
                return false;
            }
            if (!getFCRiskZTFee(tXMLDataset))
            {
                return false;
            }
            // 生成归属规则：
            if (!getGrpascriptionrule(tXMLDataset))
            {
                return false;
            }
        }

        //		 生成归属规则：
        if (!CreateEndDate(tXMLDataset))
        {
            return false;
        }
        return true;
    }

    /**
     * getGrpInsuYear
     * 
     * @return boolean
     */
    private boolean getGrpInsuYear(XMLDataset tXmlDataset)
    {
        String start = this.mLCGrpContSchema.getCValiDate();
        String end = this.mLCGrpContSchema.getCInValiDate();
        StringBuffer sql = new StringBuffer(255);
        sql.append("select (date('").append(end).append("') + 1 day - date('").append(start).append(
                "'))/10000 from dual");
        String insuYear = (new ExeSQL()).getOneValue(sql.toString());
        if (insuYear != null && !insuYear.equals(""))
        {
            double year = PubFun.setPrecision(Double.parseDouble(insuYear), "0.00");
            if (year > 0)
            {
                tXmlDataset.addDataObject(new XMLDataTag("GrpInsuYear", String.valueOf(year)));
            }
        }
        return true;
    }

    /**
     * 根据个单合同号，查询明细信息
     * 
     * @param tXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getHospital(XMLDataset tXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件，mszTemplatePath为模板所在的应用路径
        tTemplateFile = mTemplatePath + "Hospital.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getHospital", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable tHshData = new Hashtable();
            // 将变量_ContNo的值赋给xml文件
            // tHshData.put("_AREACODE",
            // this.mLDComSchema.getRegionalismCode());
            tHshData.put("_AREACODE", "11%%");
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), tHshData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            tXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            // 出错处理
            buildError("genHospital", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询明细信息 责任信息、特约信息、个人险种明细、险种条款信息等
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredList(XMLDataset cXmlDataset) throws Exception
    {
        // String tSql =
        // "select ContPlanCode,ContPlanName from LCContPlan where GrpContNo =
        // '" +
        // this.mLCGrpContSchema.getGrpContNo() +
        // "' and ContPlanCode != '00'";

        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(this.mLCGrpContSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G' and riskcode != '370301')");
        System.out.println("查询险种的啊啊啊啊啊啊啊" + tSBql2.toString());

        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        System.out.println("查询险种" + tExeSQL5.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select ContPlanCode,ContPlanName from LCContPlan where GrpContNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("' and ContPlanCode != '00' and ContPlanCode != '11'");
            tSBql.append(" order by ContPlanCode ");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());

            StringBuffer tContPlanAabstractInfo = new StringBuffer();
            // 循环获取计划信息
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                cXmlDataset.addDataObject(new XMLDataTag(tSSRS.GetText(i, 1), tSSRS.GetText(i, 2)));

                // 保障计划概述
                tContPlanAabstractInfo.append("计划 ");
                tContPlanAabstractInfo.append(tSSRS.GetText(i, 1));
                tContPlanAabstractInfo.append(".");
                tContPlanAabstractInfo.append(tSSRS.GetText(i, 2));
                tContPlanAabstractInfo.append(" ");
                // ----------------------------------
            }

            // 保障计划概述节点：<ContPlanAabstractInfo />
            cXmlDataset.addDataObject(new XMLDataTag("ContPlanAabstractInfo", tContPlanAabstractInfo.toString()));
            // --------------------------------------

            // String flag="1";
            XMLDataList xmlDataList = new XMLDataList();
            // 添加xml一个新的对象Term
            xmlDataList.setDataObjectID("LCPol");
            xmlDataList.addColHead("ContPlanCode");
            xmlDataList.addColHead("InsuredCount");
            xmlDataList.addColHead("RiskCode");
            xmlDataList.addColHead("RiskName");
            xmlDataList.addColHead("DutyCode");
            xmlDataList.addColHead("DutyName");
            xmlDataList.addColHead("Amnt/Mult");
            xmlDataList.addColHead("GetLimit");
            xmlDataList.addColHead("GetRate");
            xmlDataList.addColHead("Prem");

            // 档次
            xmlDataList.addColHead("Mult");
            // --------------------------------------

            // 保额
            xmlDataList.addColHead("Amnt");
            // --------------------------------------

            // 限额
            xmlDataList.addColHead("DutyAmnt");
            // --------------------------------------

            xmlDataList.buildColHead();
            System.out.println("ContPrintType" + ContPrintType);
            if (ContPrintType.equals("0") || "4".equals(ContPrintType))
            {
                // 取险种相关责任
                StringBuffer tSQl1 = new StringBuffer(256);
                tSQl1
                        .append(" select distinct ContPlanCode,(select case  when count(1)=0 then (select peoples2 from LCContPlan where grpcontno=z.grpcontno and ContPlanCode = z.ContPlanCode ) else count(1) end InsuredCount from LCInsured where GrpContNo = z.GrpContNo ");
                tSQl1
                        .append(" and ContPlanCode = z.ContPlanCode and name not like'公共账户%' and name not like'无名单%' ),z.RiskCode,(select RiskName from LMRisk where RiskCode =z.riskcode), ");
                tSQl1.append(" (case when b.riskcode in ('162601','162701','162801','162901') then '' else (select trim(z.riskcode) || trim(outdutycode) from LMduty where dutycode = z.dutycode) end), ");
                tSQl1.append(" (case when b.riskcode in ('162601','162701','162801','162901') then '' else (select outdutyname from LMduty where dutycode = z.dutycode) end), ");
                tSQl1.append(" (select case when code  in  ('162401','162501','170501') then '' when code in ('162601','162701','162801','162901') and  CalFactorValue='1' then concat('998', codename) when code in ('162601','162701','162801','162901') and  CalFactorValue='2' then concat('999', codename) else  concat(CalFactorValue,codename) end ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('Mult') and codetype='printflag' and code=z.RiskCode fetch first 1 rows only), ");
                tSQl1.append(" (select case when dutycode='706002' then concat(CalFactorValue,'B') else concat(CalFactorValue,codename) end ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('Amnt') and codetype='printflag' and code=z.RiskCode), ");
                tSQl1
                        .append(" (select CalFactorValue ParamValue2 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
                tSQl1
                        .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetLimit'), ");
                tSQl1
                        .append(" (select CalFactorValue ParamValue3 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
                tSQl1
                        .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetRate'), ");
                tSQl1.append(" (select n ");
                tSQl1.append(" from ");
                tSQl1.append(" (select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ");
                tSQl1.append(" from lccontplan a,lcpol b,lcinsured c ");
                tSQl1.append("  where ");
                tSQl1.append(" a.grpcontno=b.grpcontno  and b.insuredno=c.insuredno ");
                tSQl1.append(" and b.contno=c.contno ");
                tSQl1.append(" and a.contplancode=c.contplancode ");
                tSQl1.append(" and a.grpcontno=c.grpcontno ");
                tSQl1.append(" and b.grpcontno=c.grpcontno ");
                tSQl1.append(" and b.insuredname not like ('公共账户') ");
                tSQl1.append(" and b.appflag in('1','9') group by a.contplancode,b.RiskCode,a.grpcontno)  as X ");
                tSQl1.append(" where X.contplancode=z.contplancode ");
                tSQl1.append(" and X.RiskCode=z.riskcode  and X.grpcontno=z.grpcontno ) ");
                tSQl1.append("  ,dutycode, ");
                tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('DutyAmnt') and codetype='printflag2' and code=z.RiskCode), ");
                tSQl1.append(" b.GrpProposalNo ");
                tSQl1.append(" ,(Select Othersign from LDCODE  where codetype='printflag' and code = b.RiskCode) ");
                tSQl1.append(" from LCContPlanDutyParam z, LCGrpPol b ");
                tSQl1.append("  where z.GrpPolNo = b.GrpPolNo");
                tSQl1.append("  and z.GrpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' and ContPlanCode != '00'");
                tSQl1.append("  and z.dutycode not in (select dutycode from lmriskduty where riskcode in  ('162601','162701','162801','162901') and dutycode not in ('743001','744001','745001','746001')) ");
                tSQl1.append(" and ContPlanCode != '11' order by ContPlanCode,b.GrpProposalNo,dutycode with ur ");
                System.out.println(tSQl1.toString());

                StringBuffer tsql = new StringBuffer();
                tsql.append(" select 'ALL',(select sum(z.peoples3)  from lccontplanrisk m,lccontplan z ");
                tsql
                        .append(" where m.contplancode not in ('00','11')  and m.grpcontno=z.grpcontno and   m.contplancode=z.contplancode and ");
                tsql.append(" m.riskcode=a.riskcode and m.riskcode=c.riskcode and a.grpcontno=m.grpcontno ");
                tsql
                        .append(" and a.grpcontno=z.grpcontno),a.riskcode,c.riskname,'',(case (Select Othersign from LDCODE  where codetype='printflag' and code = c.RiskCode) when '1' then '特需综合保障责任' else '团体帐户' end),'','','','',(select sum(prem) from lcpol where ");
                tsql.append(" lcpol.poltypeflag='2' and lcpol.insuredname like '公共账户' and lcpol.prtno=a.prtno ");
                tsql.append(" and lcpol.riskcode=a.riskcode) from lcgrppol a,lmrisk c where a.grpcontNo='"
                        + this.mLCGrpContSchema.getGrpContNo() + "' ");
                tsql.append(" and  a.riskcode=c.riskcode and c.riskcode in(select riskcode from lmriskapp where ");
                tsql.append(" risktype9='4' or riskcode in ('162501', '170501') ) order by a.riskcode ");
                System.out.println(tsql.toString());

                ExeSQL tExeSQL2 = new ExeSQL();
                SSRS tSSRS1 = tExeSQL2.execSQL(tSQl1.toString());
                SSRS tSSRS2 = tExeSQL2.execSQL(tsql.toString());
                if (tSSRS2.getMaxRow() > 0)
                {
                    for (int Riskcount = 0; Riskcount < PubRiskcode.length; Riskcount++)
                    {
                        PubRiskcode[Riskcount] = "";
                    }
                }

                int begincount = 0;
                for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
                {
                    // 判断是否存在特殊打印需求。如170301，不打印责任。
                    if ("1".equals(tSSRS1.GetText(j, 15)))
                        continue;
                    // --------------------------------------------

                    if (j > 1 && tSSRS1.GetText(j, 1).equals(tSSRS1.GetText(j - 1, 1))
                            && tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j - 1, 3)))
                    {
                        begincount = j;
                    }
                    xmlDataList.setColValue("ContPlanCode", tSSRS1.GetText(j, 1));
                    xmlDataList.setColValue("InsuredCount", tSSRS1.GetText(j, 2));
                    xmlDataList.setColValue("RiskCode", tSSRS1.GetText(j, 3));
                    xmlDataList.setColValue("RiskName", tSSRS1.GetText(j, 4));
                    xmlDataList.setColValue("DutyCode", tSSRS1.GetText(j, 5));
                    xmlDataList.setColValue("DutyName", tSSRS1.GetText(j, 6));

                    // 处理“保额/限额/档次”节点
                    String tAmntOrMult = "";

                    if (tSSRS1.GetText(j, 13).equals(""))
                    {
                        if (tSSRS1.GetText(j, 7).equals(""))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 8));

                            tAmntOrMult = tSSRS1.GetText(j, 8);
                        }
                        else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 8).equals("")))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 8).replace('D', 'X'));

                            tAmntOrMult = tSSRS1.GetText(j, 8).replace('D', 'X');
                        }
                        else
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 7));

                            tAmntOrMult = tSSRS1.GetText(j, 7);
                        }
                    }
                    else if (!tSSRS1.GetText(j, 13).equals(""))
                    {
                        if (tSSRS1.GetText(j, 7).equals(""))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 13));

                            tAmntOrMult = tSSRS1.GetText(j, 13);
                        }
                        else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 13).equals("")))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 13).replace('D', 'B'));

                            tAmntOrMult = tSSRS1.GetText(j, 13).replace('D', 'B');
                        }
                        else
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 7));

                            tAmntOrMult = tSSRS1.GetText(j, 7);
                        }
                    }

                    xmlDataList.setColValue("Amnt/Mult", tAmntOrMult);
                    dealAmntEx(xmlDataList, tAmntOrMult);
                    // ------------------------------------------

                    xmlDataList.setColValue("GetLimit", tSSRS1.GetText(j, 9));
                    if (!StrTool.cTrim(tSSRS1.GetText(j, 10)).equals(""))
                    {
                        xmlDataList.setColValue("GetRate", Double.parseDouble(tSSRS1.GetText(j, 10)));
                    }
                    else
                    {
                        xmlDataList.setColValue("GetRate", tSSRS1.GetText(j, 10));
                    }
                    if (j == begincount)
                    {
                        xmlDataList.setColValue("Prem", "");
                    }
                    else
                    {
                        String prem = setPrem(mLCGrpContSchema.getGrpContNo(), tSSRS1.GetText(j, 1), tSSRS1.GetText(j,
                                3));
                        if (prem == null)
                        {
                            return false;
                        }

                        xmlDataList.setColValue("Prem", prem);
                    }
                    xmlDataList.insertRow(0);
                }
                // 公共帐户显示,为了避免重复显示公共账户增加PubRiskcode数组缓存已经打印过公共账户的险种代码

                if (tSSRS2.getMaxRow() > 0)
                {

                    for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
                    {
                        for (int tIndex = 1; tIndex <= tSSRS2.getMaxRow(); tIndex++)
                        {
                            System.out.println("+++++");
                            for (int Riskcount1 = 0; Riskcount1 < PubRiskcode.length; Riskcount1++)
                            {
                                if (tSSRS2.GetText(tIndex, 3).equals(PubRiskcode[Riskcount1]))
                                {
                                    PubRiskcodePrintFlag = "1";
                                }
                            }
                            System.out.println("+++++");
                            if (tSSRS1.GetText(j, 3).equals(tSSRS2.GetText(tIndex, 3))
                                    & PubRiskcodePrintFlag.equals("0"))
                            {
                                if (j < tSSRS1.getMaxRow() && !(tSSRS1.GetText(j, 3).equals(tSSRS1.GetText(j + 1, 3))))
                                {
                                    System.out.println("tSSRS2" + tSSRS2.GetText(tIndex, 3));
                                    xmlDataList.setColValue("ContPlanCode", tSSRS2.GetText(tIndex, 1));
                                    xmlDataList.setColValue("InsuredCount", tSSRS2.GetText(tIndex, 2));
                                    xmlDataList.setColValue("RiskCode", tSSRS2.GetText(tIndex, 3));
                                    xmlDataList.setColValue("RiskName", tSSRS2.GetText(tIndex, 4));
                                    xmlDataList.setColValue("DutyCode", tSSRS2.GetText(tIndex, 5));
                                    if("162501".equals(tSSRS2.GetText(tIndex, 3))){
                                    	xmlDataList.setColValue("DutyName", "公共保险金额");
                                    }else{
                                    	xmlDataList.setColValue("DutyName", tSSRS2.GetText(tIndex, 6));
                                    }                                    
                                    xmlDataList.setColValue("Amnt/Mult", tSSRS2.GetText(tIndex, 8));
                                    xmlDataList.setColValue("GetLimit", tSSRS2.GetText(tIndex, 9));
                                    xmlDataList.setColValue("GetRate", tSSRS2.GetText(tIndex, 10));

                                    if (tSSRS2.GetText(tIndex, 11).equals(null)
                                            || tSSRS2.GetText(tIndex, 11).equals("null")
                                            || tSSRS2.GetText(tIndex, 11).equals(""))
                                    {
                                        xmlDataList.setColValue("Prem", "");
                                    }
                                    else
                                    {
                                        xmlDataList.setColValue("Prem", format(Double.parseDouble(tSSRS2.GetText(
                                                tIndex, 11))));
                                    }
                                    PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                                    xmlDataList.insertRow(0);
                                }
                                else if (j == tSSRS1.getMaxRow())
                                {
                                    xmlDataList.setColValue("ContPlanCode", tSSRS2.GetText(tIndex, 1));
                                    xmlDataList.setColValue("InsuredCount", tSSRS2.GetText(tIndex, 2));
                                    xmlDataList.setColValue("RiskCode", tSSRS2.GetText(tIndex, 3));
                                    xmlDataList.setColValue("RiskName", tSSRS2.GetText(tIndex, 4));
                                    xmlDataList.setColValue("DutyCode", tSSRS2.GetText(tIndex, 5));
                                    if("162501".equals(tSSRS2.GetText(tIndex, 3))){
                                    	xmlDataList.setColValue("DutyName", "公共保险金额");
                                    }else{
                                    	xmlDataList.setColValue("DutyName", tSSRS2.GetText(tIndex, 6));
                                    }
                                    xmlDataList.setColValue("Amnt/Mult", tSSRS2.GetText(tIndex, 8));
                                    xmlDataList.setColValue("GetLimit", tSSRS2.GetText(tIndex, 9));
                                    xmlDataList.setColValue("GetRate", tSSRS2.GetText(tIndex, 10));
                                    if (tSSRS2.GetText(tIndex, 11).equals(null)
                                            || tSSRS2.GetText(tIndex, 11).equals("null")
                                            || tSSRS2.GetText(tIndex, 11).equals(""))
                                    {
                                        xmlDataList.setColValue("Prem", "");
                                    }
                                    else
                                    {
                                        xmlDataList.setColValue("Prem", format(Double.parseDouble(tSSRS2.GetText(
                                                tIndex, 11))));
                                    }

                                    PubRiskcode[tIndex - 1] = tSSRS2.GetText(tIndex, 3);
                                    xmlDataList.insertRow(0);
                                }
                            }
                        }
                    }
                    cXmlDataset.addDataObject(xmlDataList);
                }
                else
                {
                    cXmlDataset.addDataObject(xmlDataList);
                }
            }
            else
            {
                StringBuffer tSQl1 = new StringBuffer(256);
                tSQl1
                        .append(" select distinct ContPlanCode,(select case  when count(1)=0 then (select peoples2 from LCContPlan where grpcontno=z.grpcontno and ContPlanCode = z.ContPlanCode ) else count(1) end InsuredCount from LCInsured where GrpContNo = z.GrpContNo ");
                tSQl1
                        .append(" and ContPlanCode = z.ContPlanCode and name not like'公共账户%' and name not like'无名单%' ),z.RiskCode,(select RiskName from LMRisk where RiskCode =z.riskcode), ");
                tSQl1.append(" (select trim(z.riskcode)||trim(outdutycode)  from LMduty where dutycode =z.dutycode), ");
                tSQl1.append(" (select outdutyname from LMduty where dutycode =z.dutycode), ");
                tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('Mult') and codetype='printflag' and code=z.RiskCode), ");
                tSQl1.append(" (select case when dutycode='706002' then concat(CalFactorValue,'B') else concat(CalFactorValue,codename) end ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('Amnt') and codetype='printflag' and code=z.RiskCode), ");
                tSQl1
                        .append(" (select CalFactorValue ParamValue2 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
                tSQl1
                        .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetLimit'), ");
                tSQl1
                        .append(" (select CalFactorValue ParamValue3 from LCContPlanDutyParam where GrpContNo =z.grpcontno and ");
                tSQl1
                        .append(" ContPlanCode = z.ContPlanCode and RiskCode=z.riskcode and DutyCode = z.dutycode and CalFactor = 'GetRate'),DUTYCODE, ");
                tSQl1.append(" (select concat(CalFactorValue,codename) ParamValue1 from LCContPlanDutyParam, ldcode ");
                tSQl1
                        .append(" where GrpContNo =z.grpcontno and ContPlanCode = z.ContPlanCode  and DutyCode = z.dutycode and CalFactor ");
                tSQl1.append(" in ('DutyAmnt') and codetype='printflag2' and code=z.RiskCode), b.GrpProposalNo ");
                tSQl1
                        .append(" from LCContPlanDutyParam z, LCGrpPol b where z.GrpPolNo = b.GrpPolNo and z.GrpContNo = '"
                                + this.mLCGrpContSchema.getGrpContNo() + "' and z.ContPlanCode != '00' ");
                tSQl1
                        .append(" and z.ContPlanCode != '11' and  not exists  (select riskcode from lmriskapp where riskprop='G' and risktype2='5' and riskcode=z.riskcode) order by ContPlanCode,b.GrpProposalNo,dutycode with ur ");
                ExeSQL tExeSQL2 = new ExeSQL();
                System.out.println(tSQl1.toString());
                SSRS tSSRS1 = tExeSQL2.execSQL(tSQl1.toString());
                for (int j = 1; j <= tSSRS1.getMaxRow(); j++)
                {
                    xmlDataList.setColValue("ContPlanCode", tSSRS1.GetText(j, 1));
                    xmlDataList.setColValue("InsuredCount", tSSRS1.GetText(j, 2));
                    xmlDataList.setColValue("RiskCode", tSSRS1.GetText(j, 3));
                    xmlDataList.setColValue("RiskName", tSSRS1.GetText(j, 4));
                    xmlDataList.setColValue("DutyCode", tSSRS1.GetText(j, 5));
                    xmlDataList.setColValue("DutyName", tSSRS1.GetText(j, 6));

                    // 处理“保额/限额/档次”节点
                    String tAmntOrMult = "";
                    if (tSSRS1.GetText(j, 12).equals(""))
                    {
                        if (tSSRS1.GetText(j, 7).equals(""))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 8));

                            tAmntOrMult = tSSRS1.GetText(j, 8);
                        }
                        else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 8).equals("")))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 8).replace('D', 'X'));

                            tAmntOrMult = tSSRS1.GetText(j, 8).replace('D', 'X');
                        }
                        else
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 7));

                            tAmntOrMult = tSSRS1.GetText(j, 7);
                        }
                    }
                    else if (!tSSRS1.GetText(j, 12).equals(""))
                    {
                        if (tSSRS1.GetText(j, 7).equals(""))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 12));

                            tAmntOrMult = tSSRS1.GetText(j, 12);
                        }
                        else if (!(tSSRS1.GetText(j, 7).equals("")) && !(tSSRS1.GetText(j, 12).equals("")))
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 12).replace('D', 'B'));

                            tAmntOrMult = tSSRS1.GetText(j, 12).replace('D', 'B');
                        }
                        else
                        {
                            // xmlDataList.setColValue("Amnt/Mult",
                            // tSSRS1.GetText(j, 7));

                            tAmntOrMult = tSSRS1.GetText(j, 7);
                        }
                    }

                    xmlDataList.setColValue("Amnt/Mult", tAmntOrMult);
                    dealAmntEx(xmlDataList, tAmntOrMult);
                    // ------------------------------------------

                    xmlDataList.setColValue("GetLimit", tSSRS1.GetText(j, 9));
                    if (!StrTool.cTrim(tSSRS1.GetText(j, 10)).equals(""))
                    {
                        xmlDataList.setColValue("GetRate", Double.parseDouble(tSSRS1.GetText(j, 10)));
                    }
                    else
                    {
                        xmlDataList.setColValue("GetRate", tSSRS1.GetText(j, 10));
                    }
                    xmlDataList.setColValue("Prem", "");
                    xmlDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(xmlDataList);
            }
            /*
             * //数据流 // InnerFormater tInnerFormater = null; String
             * tTemplateFile = ""; //团险默认配置文件 if
             * (StrTool.cTrim(this.mLCGrpContSchema.getCardFlag()).equals("")) {
             * tTemplateFile = mTemplatePath + "GrpContPrint.xml"; } else {
             * tTemplateFile = mTemplatePath + "GrpContPrintCard.xml"; }
             * //校验配置文件是否存在 mFile = new File(tTemplateFile); if
             * (!mFile.exists()) { // throw new Exception("缺少配置文件：" +
             * tTemplateFile); buildError("genInsuredList", "XML配置文件不存在！");
             * return false; } try { Hashtable thashData = new Hashtable();
             * //将变量GrpContNo的值赋给xml文件 thashData.put("_GRPCONTNO",
             * this.mLCGrpContSchema.getGrpContNo()); //根据配置文件生成xml数据
             * XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
             * tTemplateFile), thashData); //
             * tXmlDataMine.setDataFormater(tInnerFormater);
             * cXmlDataset.addDataObject(tXmlDataMine); } catch (Exception e) {
             * buildError("genInsuredList", "根据XML文件生成报表数据失败！"); return false; }
             */
        }
        else
        {

        }
        
        if("J108".equals(NewJetFormType)){
        	
            //调整模板之后的总保费小写，大写
            if (!getPrem(cXmlDataset)){
            	return false;
            }
            
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select lcp.riskcode,(select RiskName from LMRiskApp where RiskCode = lcp.riskcode),lcp.amnt ");
//            tSBql.append("(select sum(sumactupaymoney) from ljapaygrp where payno = (select payno from ljapay where incomeno = lcp.grpcontno and duefeetype = '0')and riskcode = lcp.riskcode) ");
            tSBql.append("from LCGrpPol lcp where GrpContNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("'");
            
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
            
            XMLDataList xmlDataList = new XMLDataList();
            // 添加xml一个新的对象Term
            xmlDataList.setDataObjectID("NEWLCPol");
            xmlDataList.addColHead("RiskCode");
            xmlDataList.addColHead("RiskName");
            xmlDataList.addColHead("Amnt");
            xmlDataList.addColHead("Prem");
            
            xmlDataList.buildColHead();
            
            for(int i=1;i<=tSSRS.getMaxRow();i++){
            	String riskcode = tSSRS.GetText(i, 1);
            	String riskname = tSSRS.GetText(i, 2);
            	String amnt = tSSRS.GetText(i, 3);
                xmlDataList.setColValue("RiskCode", riskcode);
                xmlDataList.setColValue("RiskName", riskname);
                int acount=0;
                int dcount=0;
                String sql1 = "select distinct contplancode from lccontplandutyparam where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='"+riskcode+"' and contplancode<>'11'";
                SSRS s1 = new ExeSQL().execSQL(sql1);
                int contplancount = s1.getMaxRow();
                if(contplancount>0){
                	for(int j=1;j<=contplancount;j++){
                		String sql2 = "select distinct dutycode from lccontplandutyparam where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='"+riskcode+"' and contplancode='"+s1.GetText(j, 1)+"'";
                		SSRS s2 = new ExeSQL().execSQL(sql2);
                		int dutycount = s2.getMaxRow();
                		if(dutycount>0){
                			for(int z=1;z<=dutycount;z++){
                				String sql3 = "select calfactorvalue from lccontplandutyparam where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='"+riskcode+"' and contplancode='"+s1.GetText(j, 1)+"' and dutycode='"+s2.GetText(z, 1)+"' and calfactor='Amnt'";
                				SSRS s3 = new ExeSQL().execSQL(sql3);
                				if(s3.getMaxRow()>0){
                					if("0".equals(s3.GetText(1, 1)) || "".equals(s3.GetText(1, 1))){
                						acount++;
                					}
                				}else{
                					acount++;
                				}
                				String sql4 = "select calfactorvalue from lccontplandutyparam where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='"+riskcode+"' and contplancode='"+s1.GetText(j, 1)+"' and dutycode='"+s2.GetText(z, 1)+"' and calfactor='DutyAmnt'";
                				SSRS s4 = new ExeSQL().execSQL(sql4);
                				if(s4.getMaxRow()>0){
                					if("0".equals(s4.GetText(1, 1)) || "".equals(s4.GetText(1, 1))){
                						dcount++;
                					}
                				}else{
                					dcount++;
                				}
                			}
                		}
                	}
                }
                if(acount>0 && dcount>0){
                	amnt="0";
                }
                
                if(!"0".equals(amnt) && amnt != null && !"null".equals(amnt) && !"".equals(amnt) ){
                	xmlDataList.setColValue("Amnt", PubFun.setPrecision2(amnt));
                }else{
                	xmlDataList.setColValue("Amnt", "--");
                }
                
                // 查询非绑定附险种的保费，包括绑定主险的附险保费
                String prem = setNewPrem(this.mLCGrpContSchema.getGrpContNo(), riskcode);

                if(prem != null && !"null".equals(prem) && !"".equals(prem) ){
                	xmlDataList.setColValue("Prem", PubFun.setPrecision2(prem));
                }else{
                	xmlDataList.setColValue("Prem", "--");
                }

                xmlDataList.insertRow(0);
            }
            
            cXmlDataset.addDataObject(xmlDataList);
        }
        
        return true;
    }

    /**
     * setPrem 绑定型主险显示主附险的保费合计，附险不显示保费 若当前险种是绑定型主险，则对主附险进行求和
     * 
     * @param cGrpContNo
     *            String
     * @param cContPlanCode
     *            String
     * @param cRiskCode
     *            String
     * @return String 主附险保费合计，若险种是附险，则返回“”
     */
    private String setPrem(String cGrpContNo, String cContPlanCode, String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "没有查询到险种定义信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("0".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            return "";
        }

        String premSumPart = "  and RiskCode = '" + cRiskCode + "' ";
        if ("1".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            premSumPart = " and (X.RiskCode='" + cRiskCode + "' "
                    + "   or X.RiskCode in(select RiskCode from LDRiskParamPrint "
                    + "              where ParamType1 = 'sumprem' " + "                 and ParamValue1='" + cRiskCode
                    + "')) ";
        }

        // 查询非绑定附险种的保费，包括绑定主险的附险保费
        ExeSQL tExeSQL = new ExeSQL();

        StringBuffer sql2 = new StringBuffer();
        sql2.append(" select nvl(sum(n), 0) from ").append("(").append(
                "    select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ").append(
                "    from lccontplan a,lcpol b,lcinsured c ").append(
                "    where  a.grpcontno=b.grpcontno  and b.insuredno=c.insuredno  and b.contno=c.contno ").append(
                "      and a.contplancode=c.contplancode  and a.grpcontno=c.grpcontno ").append(
                "      and b.grpcontno=c.grpcontno ").append("      and b.insuredname not like ('公共账户') ").append(
                "      and b.appflag in('1','9') ").append("    group by a.contplancode,b.RiskCode,a.grpcontno ")
                .append(" )  as X ").append(" where X.contplancode='" + cContPlanCode + "' ").append(premSumPart)
                .append("   and X.grpcontno='" + cGrpContNo + "' ");
        String prem = tExeSQL.getOneValue(sql2.toString());

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询邦定主险保费出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("".equals(prem) || "null".equals(prem))
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询主险保费失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return format(Double.parseDouble(prem));
    }
    private String setNewPrem(String cGrpContNo, String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "没有查询到险种定义信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("0".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            return "";
        }

        String premSumPart = " RiskCode = '" + cRiskCode + "' ";
        if ("1".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            premSumPart = " (X.RiskCode='" + cRiskCode + "' "
                    + "   or X.RiskCode in(select RiskCode from LDRiskParamPrint "
                    + "              where ParamType1 = 'sumprem' " + "                 and ParamValue1='" + cRiskCode
                    + "')) ";
        }

        // 查询非绑定附险种的保费，包括绑定主险的附险保费
        ExeSQL tExeSQL = new ExeSQL();

        StringBuffer sql2 = new StringBuffer();
        sql2.append(" select nvl(sum(n), 0) from ").append("(").append(
                "    select sum(b.Prem) n,b.RiskCode,b.grpcontno ").append(
                "    from lcpol b,lcinsured c ").append(
                "    where  b.insuredno=c.insuredno  and b.contno=c.contno ").append(
                "      and b.grpcontno=c.grpcontno ").append("      and b.insuredname not like ('公共账户') ").append(
                "      and b.appflag in('1','9') ").append("    group by b.RiskCode,b.grpcontno ")
                .append(" )  as X ").append(" where ").append(premSumPart)
                .append("   and X.grpcontno='" + this.mLCGrpContSchema.getGrpContNo() + "' ");
        String prem = tExeSQL.getOneValue(sql2.toString());

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询邦定主险保费出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("".equals(prem) || "null".equals(prem))
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询主险保费失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return format(Double.parseDouble(prem));
    }

    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(this.mLCGrpContSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G' and riskcode != '370301' )");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            return false;
        }
        return true;
    }

    //增加一个方法用来判断是否是内蒙保单，根据此方法区分
    public boolean ManageCom()
    {
        StringBuffer tSql = new StringBuffer(256);
        tSql.append("select managecom,salechnl from lcgrpcont where GrpContNo = '");
        tSql.append(this.mLCGrpContSchema.getGrpContNo());
        tSql.append("'");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSql.toString());
        if (tSSRS5.getMaxRow()>0)
        {
        	String tmanagecom=tSSRS5.GetText(1, 1);
        	String tsalechnl=tSSRS5.GetText(1, 2);
        	if(tmanagecom.substring(0, 4).equals("8632")&&"16".equals(tsalechnl)){
        		return true;
        	}        
        }
		return false;        
    }
   
    //增加一个方法用来判断是否是370301产品，根据此方法区分
    public boolean checkRiskcode()
    {
        StringBuffer tSql = new StringBuffer(256);
        tSql.append("select 1 from lcgrppol where GrpContNo = '");
        tSql.append(this.mLCGrpContSchema.getGrpContNo());
        tSql.append("' and riskcode='370301' ");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSql.toString());
        if (tSSRS.getMaxRow()>0)
        {
      		return true; 
        }
		return false;        
    }

    private boolean getRiskGrpNewCont(XMLDataset tXmlDataset)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCNewGrpCont");
        // 添加Head单元内的信息
        tXMLDataList.addColHead("SumInsureds");
        tXMLDataList.addColHead("RiskCode");
        tXMLDataList.addColHead("RiskName");
        tXMLDataList.addColHead("PubPrem");
        tXMLDataList.addColHead("SelfPrem");
        tXMLDataList.addColHead("SelfGrp");
        tXMLDataList.addColHead("InitFee");
        tXMLDataList.addColHead("ManageFee");
        tXMLDataList.addColHead("SumPrem1");
        tXMLDataList.buildColHead();
        String str = "select peoples2 from lcgrpcont where grpcontno='" + this.mLCGrpContSchema.getGrpContNo() + "'";
        ExeSQL tExeSQLNewCont = new ExeSQL();
        SSRS tSSRSSNewCont = tExeSQLNewCont.execSQL(str.toString());
        tXMLDataList.setColValue("SumInsureds", tSSRSSNewCont.GetText(1, 1));

        ExeSQL mExeSQLRisk = new ExeSQL();
        SSRS mSSRSRisk = new SSRS();
        String mSqlRisk = "select l.riskcode,m.riskname from LcGrppol l,lmrisk m where l.riskcode=m.riskcode and  GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSRisk = mExeSQLRisk.execSQL(mSqlRisk);

        if (mSSRSRisk.GetText(1, 1) != null & mSSRSRisk.GetText(1, 2) != null)
        {
            tXMLDataList.setColValue("RiskCode", mSSRSRisk.GetText(1, 1));
            tXMLDataList.setColValue("RiskName", mSSRSRisk.GetText(1, 2));
        }

        ExeSQL mExeSQLPub = new ExeSQL();
        SSRS mSSRSPub = new SSRS();
        String mSqlPub = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='3' and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSPub = mExeSQLPub.execSQL(mSqlPub);
        if (mSSRSPub.GetText(1, 1) != null)
        {
        	double PubPrem = Double.parseDouble(mSSRSPub.GetText(1, 1));
            tXMLDataList.setColValue("PubPrem", com.sinosoft.lis.bq.CommonBL.bigDoubleToCommonString(PubPrem,"0.00"));
        }
        tXMLDataList.setColValue("SelfPrem", "0");
        tXMLDataList.setColValue("SelfGrp", "0");

        ExeSQL mExeSQLInit = new ExeSQL();
        SSRS mSSRSInit = new SSRS();
        String mSqlInit = "select feevalue from lcgrpfee where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and feecode in (select feecode from lmriskfee where FeeItemType='01') ";
        mSSRSInit = mExeSQLInit.execSQL(mSqlInit);
        if (mSSRSInit.GetText(1, 1) != null)
        {
        	double pubprem=Double.parseDouble(mSSRSPub.GetText(1, 1));
        	double initfee=Double.parseDouble(mSSRSInit.GetText(1, 1));
        	double a=pubprem*initfee;
            tXMLDataList.setColValue("InitFee", com.sinosoft.lis.bq.CommonBL.bigDoubleToCommonString(a,"0.00"));
        }

        ExeSQL mExeSQLManage = new ExeSQL();
        SSRS mSSRSManage = new SSRS();
        String mSqlManage = "select feevalue from lcgrpfee where feecode in (select feecode from lmriskfee where FeeItemType='03') and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSManage = mExeSQLManage.execSQL(mSqlManage);
        if (mSSRSManage.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("ManageFee", mSSRSManage.GetText(1, 1));
        }
        double SumPrem1 = Double.parseDouble(mSSRSPub.GetText(1, 1));
        tXMLDataList.setColValue("SumPrem1", com.sinosoft.lis.bq.CommonBL.bigDoubleToCommonString(SumPrem1,"0.00"));
        tXMLDataList.insertRow(0);
        tXmlDataset.addDataObject(tXMLDataList);
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        String tTemplateFile = "";
        // 团险默认配置文件

        if (!jugdeUliByGrpContNo())
        {
            tTemplateFile = mTemplatePath + "GrpInsured.xml";
        }
        else
        {
            tTemplateFile = mTemplatePath + "GrpUliInsured.xml";
        }
        // 校验配置文件是否存在
        System.out.println("tTemplateFile" + tTemplateFile);
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            // throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredDetail", "XML配置文件不存在！");
            return false;
        }
        try
        {
            String mGrpContNo = this.mLCGrpContSchema.getGrpContNo();
            // 如不打印被保险人清单，将团体保单号置空
            if ("0".equals(printInsureDetail))
                mGrpContNo = "00000000000000";

            Hashtable thashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            System.out.println("mGrpContNomGrpContNomGrpContNo" + mGrpContNo);
            thashData.put("_GRPCONTNO", mGrpContNo);
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), thashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }

        // 增加被保险人列表节点。0：无被保险人；1：有被保险人
        String sql = "select count(1) from LCInsured where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and name not in ('公共账户','无名单')";
        if ("0".equals(new ExeSQL().getOneValue(sql)))
            cXmlDataset.addDataObject(new XMLDataTag("LCInsuredDetailExistFlag", "0"));
        else
            cXmlDataset.addDataObject(new XMLDataTag("LCInsuredDetailExistFlag", "1"));

        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsuredCard.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            // throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), thashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     *一个空值节点
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getReceiptNote(XMLDataset tXmlDataset){
    	XMLDataList xmlDataList = new XMLDataList();
    	xmlDataList.setDataObjectID("ReceiptNote");
    	tXmlDataset.addDataObject(xmlDataList);
    	return true;
    }
    
    /**
     * 获取险种名称以及保费
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getPremDetail(XMLDataset tXmlDataset)
    {
    	XMLDataList xmlDataList = new XMLDataList();
    	xmlDataList.setDataObjectID("PremDetail");
    	xmlDataList.addColHead("RiskName");
    	xmlDataList.addColHead("RiskPrem");
    	ExeSQL tExeSQL = new ExeSQL();
    	String tStrSql= "select distinct(riskcode) from lcpol "
    				  +	"where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
    	if(tSSRS.getMaxRow()>0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){ 
    			String tStrSql1= "select riskname from lmriskapp "
    						   + "where riskcode='"+tSSRS.GetText(i,1)+"'";
    			String riskname=tExeSQL.getOneValue(tStrSql1);	
                xmlDataList.setColValue("RiskName", riskname);

    			String tStrSql2= "select sum(sumactupaymoney) from ljapaygrp "
     				           + "where payno = (select payno from ljapay "
     				           + "where incomeno='" +this.mLCGrpContSchema.getGrpContNo()+ "' and duefeetype='0') "
     				           + "and riskcode ='"+tSSRS.GetText(i,1)+"'";	 
                 String riskprem = tExeSQL.getOneValue(tStrSql2);
                 xmlDataList.setColValue("RiskPrem", riskprem);
                 xmlDataList.insertRow(0);  
    		}
    	}   
    	tXmlDataset.addDataObject(xmlDataList);
    	return true;
    }
    
    /**
     * 获取中文保费数据
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getMoney(XMLDataset cXmlDataset)
    {
        try
        {
            XMLDataList tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("Money");
            tXMLDataList.addColHead("ChinaMoney");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("ChinaMoney", PubFun.getChnMoney(this.mLCGrpContSchema.getPrem()));
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);
        }
        catch (Exception e)
        {
            buildError("getMoney", "添加保费数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 获取发票信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getInvoice(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpContInvoice.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getInvoice", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable tHashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            tHashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), tHashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);

            // cXmlDataset.addDataObject(new XMLDataTag("RMB",
            // PubFun.getChnMoney(this.
            // mLCGrpContSchema.getPrem()))); //保费大写
            // 收款人，目前系统提供操作员信息

            // 查询缴费方式信息
            // 根据合同号，查询缴费记录信息
            // String tSql =
            // "select distinct TempFeeNo from LJTempFee where OtherNo = '" +
            // this.mLCGrpContSchema.getGrpContNo() +
            // "' and OtherNoType = '7'";

            XMLDataList tXMLDataList = new XMLDataList();

            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("' and OtherNoType = '7'");

            // String tWhere = " where a.TempFeeNo in ('";
            StringBuffer tBWhere = new StringBuffer(" where a.TempFeeNo in ('");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                // 将每笔缴费的缴费号串连起来作为查询条件
                if (i == tSSRS.getMaxRow())
                {
                    // tWhere = tWhere + tSSRS.GetText(i, 1) + "')";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("')");
                }
                else
                {
                    // tWhere = tWhere + tSSRS.GetText(i, 1) + "','";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("','");
                }
            }

            
            // cXmlDataset.addDataObject(new XMLDataTag("TempFeeNo",
            // tSSRS.GetText(1, 1))); //收据号

            // 根据上面的查询条件，查询缴费方式为现金的数据
            // tSql = "select a.ConfMakeDate from LJTempFeeClass a" +
            // tWhere + " and a.PayMode = '1' order by a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select a.ConfMakeDate from LJTempFeeClass a");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode = '1' order by a.ConfMakeDate");

            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0)
            {
                // 添加缴费方式为现金的标签
                tXMLDataList = new XMLDataList();
                // 添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Cash");
                // 添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                // 此缴费方式只需知道确认日期
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tXMLDataList.setColValue("PayMode", "现金");
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 1));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            // 根据上面的查询条件，查询缴费方式不是现金的数据
            // tSql =
            // "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from
            // LJTempFeeClass a,LDCode b" +
            // tWhere + " and a.PayMode <> '1' and a.BankCode = b.Code and
            // b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select b.BankName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a, LDBank b");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode <> '1' and a.BankCode = b.BankCode order by a.BankAccNo,a.ConfMakeDate");
            System.out.println(tSBql.toString());
            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0)
            {
                // 添加缴费方式为支票的标签
                tXMLDataList = new XMLDataList();
                // 添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Check");
                // 添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("Bank");
                tXMLDataList.addColHead("AccName");
                tXMLDataList.addColHead("BankAccNo");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                // 此标签需要获得银行、户名、帐户的信息
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tXMLDataList.setColValue("PayMode", "银行转帐");
                    tXMLDataList.setColValue("Bank", tSSRS.GetText(i, 1));
                    tXMLDataList.setColValue("AccName", tSSRS.GetText(i, 2));
                    tXMLDataList.setColValue("BankAccNo", tSSRS.GetText(i, 3));
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 4));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            // cXmlDataset.addDataObject(new XMLDataTag("End", "1"));
        }
        catch (Exception e)
        {
            buildError("getInvoice", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    private boolean CreateEndDate(XMLDataset cXmlDataset)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("End");
        tXMLDataList.addColHead("Flag");
        tXMLDataList.buildColHead();

        tXMLDataList.setColValue("Flag", "1");
        tXMLDataList.insertRow(0);
        cXmlDataset.addDataObject(tXMLDataList);
        return true;
    }

    /**
     * 取得团单下的全部LCGrpPol表数据
     * 
     * @param cGrpContNo
     *            String
     * @return LCGrpPolSet
     * @throws Exception
     */
    private static LCGrpPolSet getRiskList(String cGrpContNo) throws Exception
    {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();

        tLCGrpPolDB.setGrpContNo(cGrpContNo);
        // 由于LCGrpCont和LCGrpPol为一对多的关系，所以采用query方法
        tLCGrpPolSet = tLCGrpPolDB.query();

        return tLCGrpPolSet;
    }

    /**
     * 获取补打保单的数据
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getRePrintData() throws Exception
    {
        // String tCurDate = PubFun.getCurrentDate();
        // String tCurTime = PubFun.getCurrentTime();

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        // 查询LCPolPrint表，获取要补打的保单数据
        String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
        System.out.println(tSql);
        tSSRS = tExeSQL.execSQL(tSql);

        if (tExeSQL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tExeSQL.mErrors);
            throw new Exception("获取打印数据失败");
        }
        if (tSSRS.MaxRow < 1)
        {
            throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
        }

        mResult.clear();
        mResult.add(mLCGrpContSchema);

        LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        if (!tLCGrpContF1PBLS.submitData(mResult, "REPRINT"))
        {
            if (tLCGrpContF1PBLS.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
            }
            throw new Exception("保存数据失败");
        }

        // 取打印数据
        Connection conn = null;

        try
        {
            DOMBuilder tDOMBuilder = new DOMBuilder();
            Element tRootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            // String tSql = "";
            java.sql.Blob tBlob = null;
            // CDB2Blob tCDB2Blob = new CDB2Blob();

            tSql = " and MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);

            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            // BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            Element tElement = tDOMBuilder.build(tBlob.getBinaryStream()).getRootElement();
            tElement = new Element("DATASET").setMixedContent(tElement.getMixedContent());
            tRootElement.addContent(tElement);

            ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
            XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "UTF-8");
            tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

            // mResult.clear();
            // mResult.add(new
            // ByteArrayInputStream(tByteArrayOutputStream.toByteArray()));
            ByteArrayInputStream tByteArrayInputStream = new ByteArrayInputStream(tByteArrayOutputStream.toByteArray());

            // 仅仅重新生成xml打印数据，影印件信息暂时不重新获取
            String FilePath = mOutXmlPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            // 根据合同号生成打印数据存放文件夹
            FilePath = mOutXmlPath + "/printdata" + "/" + mLCGrpContSchema.getGrpContNo();
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            // 根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mLCGrpContSchema.getGrpContNo() + ".xml";
            System.out.println("XmlFileXmlFile" + XmlFile);
            FileOutputStream fos = new FileOutputStream(XmlFile);
            // 此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            while ((n = tByteArrayInputStream.read()) != -1)
            {
                fos.write(n);
            }
            fos.close();

            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * 
     * @param dValue
     *            double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private boolean checkdate() throws Exception
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
//        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
//        tLCGrpContSet = tLCGrpContDB.query();
        if(!"".equals(this.mLCGrpContSchema.getGrpContNo()) && this.mLCGrpContSchema.getGrpContNo()!=null){
            String tSql=" select * from LCGrpCont where GrpContNo='"+this.mLCGrpContSchema.getGrpContNo()+"'";
            tLCGrpContSet=tLCGrpContDB.executeQuery(tSql); 
        }else{
            buildError("checkdate", "取保单号失败!");
            return false;
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        SSRS tSSRS1 = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL("select distinct Grpname,customerno,payintv from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'");
        tSSRS1 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  appflag in('1','9') ");
        tSSRS2 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lcpol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9') ");
        if (tSSRS.getMaxRow() != 1 || tSSRS1.getMaxRow() != 1 || tSSRS2.getMaxRow() != 1)
        {
            buildError("checkdate", "取投保客户数据失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getGrpName().equals(tSSRS.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS1.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS2.GetText(1, 1))))
        {
            buildError("checkdate", "取投保客户名称失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS1.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS2.GetText(1, 2))))
        {
            buildError("checkdate", "取投保客户号码失败!");
            return false;
        }
        if (!(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS1.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS2.GetText(1, 3))))
        {
            buildError("checkdate", "取缴费频次失败!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select count(insuredno) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
        tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "'  and  name not in ('公共账户','无名单') and relationtomaininsured='00' ");
        if (!tSSRS.GetText(1, 1).equals(tSSRS1.GetText(1, 1)))
        {
            buildError("checkdate", "取被保人数失败!");
            return false;
        }
        tSSRS2 = tExeSQL
                .execSQL("SELECT count(1) FROM LCGrpCont  WHERE AppFlag in ('1','9') and  PrintCount =1   and grpcontno='"
                        + this.mLCGrpContSchema.getGrpContNo() + "' ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (!StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            buildError("checkdate", "该保单已经进行过打印!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS2 = tExeSQL.execSQL("select count(1) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  name in ('无名单') ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            tSSRS = tExeSQL.execSQL("select sum(peoples) from lccont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo()
                    + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
            tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'  and  name not in ('公共账户','无名单') ");
            if (!(String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS.GetText(1, 1)))
                    || !((String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS1.GetText(1, 1)))))
            {
                buildError("checkdate", "取团体被保人数失败!");
                return false;
            }
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getCustomerReceiptNo()).equals(""))
        {
            buildError("checkdate", "取保险合同送达回执号失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getContPrintType()).equals(""))
        {
            buildError("checkdate", "取保单打印类型失败!");
            return false;
        }

        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select sum(prem) from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        tSSRS1 = tExeSQL.execSQL("select sum(prem) from lcpol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9')");
        tSSRS2 = tExeSQL.execSQL("select sum(prem) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        if (tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS1.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS2.GetText(1, 1)))
        {
            System.out.println("zzzzz" + tLCGrpContSet.get(1).getPrem());
            System.out.println("bbbbbb" + tSSRS2.GetText(1, 1));
            buildError("checkdate", "取保单费用失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getPayMode()).equals("4"))
        {
            if (StrTool.cTrim(tLCGrpContSet.get(1).getAccName()).equals(""))
            {
                buildError("checkdate", "取银行帐户名失败！");
                return false;
            }

            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankAccNo()).equals(""))
            {
                buildError("checkdate", "取银行帐号失败！");
                return false;
            }
            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankCode()).equals(""))
            {
                buildError("checkdate", "取银行代码失败！");
                return false;
            }
        }

        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        tLDComDB.setComCode(tLCGrpContSet.get(1).getManageCom());
        tLDComSet = tLDComDB.query();
        if (StrTool.cTrim(tLDComSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取分公司名称失败！");
            return false;
        }
        if (StrTool.cTrim(tLDComSet.get(1).getClaimReportPhone()).equals(""))
        {
            buildError("checkdate", "取理赔报案电话失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServiceName()).equals(""))
        {
            buildError("checkdate", "取信函服务名称失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取地址信函服务地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取地址信函服务邮编失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取客户服务中心地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取客户服务中心邮编失败！");
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(tLCGrpContSet.get(1).getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (StrTool.cTrim(tLAAgentSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取业务员名称失败！");
            return false;
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSet.get(1).getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getBranchAttr()).equals(""))
        {
            buildError("checkdate", "取营业单位代码失败！");
            return false;
        }
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取营业单位名称失败！");
            return false;
        }

        return true;
    }

    /**
     * 根据险种，分别生成对应的：保费/限额/档次，节点
     * 
     * @param cXmlDataList
     * @param cAmntStr
     */
    private void dealAmntEx(XMLDataList cXmlDataList, String cAmntStr)
    {
        if (cAmntStr != null && !cAmntStr.equals(""))
        {

            String tAmntOrMult = cAmntStr.substring(0, cAmntStr.length() - 1);
            String tLastFlagOfAmnt = cAmntStr.substring(cAmntStr.length() - 1, cAmntStr.length());

            if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("ej") || StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("hk")){
            	cXmlDataList.setColValue("Amnt", tAmntOrMult);
            }else{
	            if ("X".equals(tLastFlagOfAmnt))
	            {
	                cXmlDataList.setColValue("DutyAmnt", tAmntOrMult);
	            }
	            else if ("B".equals(tLastFlagOfAmnt))
	            {
	                cXmlDataList.setColValue("Amnt", tAmntOrMult);
	            }
	            else if ("D".equals(tLastFlagOfAmnt))
	            {
	                cXmlDataList.setColValue("Mult", tAmntOrMult);
	            }
            }
        }
    }

    private boolean getGrpascriptionrule(XMLDataset tXmlDataset)
    {
        String str = "select distinct Ascriptionrulecode,Ascriptionrulename from lcAscriptionrulefactory a where a.GrpContNo='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        ExeSQL tExeSQLScri = new ExeSQL();
        SSRS tSSRSScri = tExeSQLScri.execSQL(str.toString());
        XMLDataList tXMLDataList = new XMLDataList();
        if (tSSRSScri != null && tSSRSScri.getMaxRow() > 0)
        {
            tXMLDataList = new XMLDataList();
            // 添加xml一个新的对象Ascriptionrule
            tXMLDataList.setDataObjectID("LCAscriptionRuleFactory");
            // 添加Head单元内的信息
            tXMLDataList.addColHead("AscriptionRuleName");
            tXMLDataList.addColHead("AscriptionRule");
            tXMLDataList.buildColHead();
            for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
            {
                tXMLDataList.setColValue("AscriptionRuleName", tSSRSScri.GetText(i, 1));
                String str1 = "select  Ascriptionrulecode,Ascriptionrulename,Calremark,inerserialno from lcAscriptionrulefactory a where a.GrpContNo='"
                        + this.mLCGrpContSchema.getGrpContNo()
                        + "' and Ascriptionrulecode='"
                        + tSSRSScri.GetText(i, 1)
                        + "' order by InerSerialNo";

                ExeSQL tExeSQLScri1 = new ExeSQL();
                SSRS tSSRSScri1 = tExeSQLScri1.execSQL(str1.toString());
                StringBuffer tAscriptionRuleInfo = new StringBuffer();
                System.out.println("tSSRSScri1.getMaxRow()" + tSSRSScri1.getMaxRow());
                // 循环获取计划信息
                for (int j = 1; j <= tSSRSScri1.getMaxRow(); j++)
                {
                    String str2 = "select  Paramname,param from LCAscriptionRuleParams a where a.GrpContNo='"
                            + this.mLCGrpContSchema.getGrpContNo() + "' and Ascriptionrulecode='"
                            + tSSRSScri.GetText(i, 1) + "'and inerserialno ='" + tSSRSScri1.GetText(j, 4) + "'";
                    ExeSQL tExeSQLScri2 = new ExeSQL();
                    SSRS tSSRSScri2 = tExeSQLScri2.execSQL(str2.toString());
                    String SourceString = tSSRSScri1.GetText(j, 3);
                    String TargetString = "";
                    for (int m = 1; m <= tSSRSScri2.getMaxRow(); m++)
                    {
                        System.out.println(tSSRSScri2.GetText(m, 1) + "代替了" + tSSRSScri2.GetText(m, 2));
                        TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), tSSRSScri2.GetText(m, 2));
                        System.out.println(TargetString);
                        SourceString = TargetString;

                    }
                    tAscriptionRuleInfo.append(SourceString);
                    tAscriptionRuleInfo.append(";");
                    System.out.println("SourceStringSourceString" + SourceString);
                    // 保障计划概述		

                    tXMLDataList.setColValue("AscriptionRule", tAscriptionRuleInfo.toString());

                }
                tXMLDataList.insertRow(0);
            }
            tXmlDataset.addDataObject(tXMLDataList);
        }

        return true;
    }

    private boolean getGrpNewCont(XMLDataset tXmlDataset)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCNewGrpCont");
        // 添加Head单元内的信息
        tXMLDataList.addColHead("SumInsureds");
        tXMLDataList.addColHead("RiskCode");
        tXMLDataList.addColHead("RiskName");
        tXMLDataList.addColHead("PubPrem");
        tXMLDataList.addColHead("SelfPrem");
        tXMLDataList.addColHead("SelfGrp");
        tXMLDataList.addColHead("InitFee");
        tXMLDataList.addColHead("ManageFee");
        tXMLDataList.addColHead("SumPrem1");
        tXMLDataList.buildColHead();
        String str = "select peoples2 from lcgrpcont where grpcontno='" + this.mLCGrpContSchema.getGrpContNo() + "'";
        ExeSQL tExeSQLNewCont = new ExeSQL();
        SSRS tSSRSSNewCont = tExeSQLNewCont.execSQL(str.toString());
        tXMLDataList.setColValue("SumInsureds", tSSRSSNewCont.GetText(1, 1));

        ExeSQL mExeSQLRisk = new ExeSQL();
        SSRS mSSRSRisk = new SSRS();
        String mSqlRisk = "select l.riskcode,m.riskname from LcGrppol l,lmrisk m where l.riskcode=m.riskcode and  GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSRisk = mExeSQLRisk.execSQL(mSqlRisk);

        if (mSSRSRisk.GetText(1, 1) != null & mSSRSRisk.GetText(1, 2) != null)
        {
            tXMLDataList.setColValue("RiskCode", mSSRSRisk.GetText(1, 1));
            tXMLDataList.setColValue("RiskName", mSSRSRisk.GetText(1, 2));
        }

        ExeSQL mExeSQLPub = new ExeSQL();
        SSRS mSSRSPub = new SSRS();
        String mSqlPub = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='3' and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSPub = mExeSQLPub.execSQL(mSqlPub);
        if (mSSRSPub.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("PubPrem", mSSRSPub.GetText(1, 1));
        }
        ExeSQL mExeSQLSelf = new ExeSQL();
        SSRS mSSRSSelf = new SSRS();
        String mSqlSelf = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass in('5','6') and grpcontno='"
            + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSSelf = mExeSQLSelf.execSQL(mSqlSelf);
        if (mSSRSSelf.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("SelfPrem", mSSRSSelf.GetText(1, 1));
        }

        ExeSQL mExeSQLGrp = new ExeSQL();
        SSRS mSSRSGrp = new SSRS();
        String mSqlGrp = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='4' and grpcontno='"
            + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSGrp = mExeSQLGrp.execSQL(mSqlGrp);
        if (mSSRSGrp.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("SelfGrp", mSSRSGrp.GetText(1, 1));
        }

        ExeSQL mExeSQLInit = new ExeSQL();
        SSRS mSSRSInit = new SSRS();
        String mSqlInit = "select nvl(sum(fee),0) from lcinsureaccfeetrace where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSInit = mExeSQLInit.execSQL(mSqlInit);
        if (mSSRSInit.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("InitFee", mSSRSInit.GetText(1, 1));
        }

        ExeSQL mExeSQLManage = new ExeSQL();
        SSRS mSSRSManage = new SSRS();
        String mSqlManage = "select feevalue from lcgrpfee where feecode in (select feecode from lmriskfee where FeeItemType='03') and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSManage = mExeSQLManage.execSQL(mSqlManage);
        if (mSSRSManage.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("ManageFee", mSSRSManage.GetText(1, 1));
        }
        double SumPrem1 = Double.parseDouble(mSSRSGrp.GetText(1, 1)) + Double.parseDouble(mSSRSSelf.GetText(1, 1))
                + Double.parseDouble(mSSRSPub.GetText(1, 1));
        tXMLDataList.setColValue("SumPrem1", com.sinosoft.lis.bq.CommonBL.bigDoubleToCommonString(SumPrem1,"0.00"));
        tXMLDataList.insertRow(0);
        tXmlDataset.addDataObject(tXMLDataList);
        return true;
    }

    private boolean getFCRiskZTFee(XMLDataset tXmlDataset)
    {

        String str2 = "SELECT EXTRACTRATE from LCRISKZTFEE where GrpContNo='" + this.mLCGrpContSchema.getGrpContNo()
                + "' order by Beginpolyear";

        ExeSQL tExeSQLScri = new ExeSQL();
        SSRS tSSRSScri = tExeSQLScri.execSQL(str2.toString());
        XMLDataList tXMLDataList = new XMLDataList();
        /*	if (tSSRSScri.getMaxRow() > 0) {
         // 添加退保利率的标签
         tXMLDataList = new XMLDataList();
         // 添加xml一个新的对象LCRiskZTFee
         tXMLDataList.setDataObjectID("LCRiskZTFee");
         // 添加Head单元内的信息
         tXMLDataList.addColHead("RiskZTRate");
         tXMLDataList.buildColHead();
         for (int i = 1; i <= tSSRSScri.getMaxRow(); i++) {
         tXMLDataList.setColValue("RiskZTRate",tSSRSScri.GetText(i, 1));

         tXMLDataList.insertRow(0);
         }
         tXmlDataset.addDataObject(tXMLDataList);
         
         }*/
        if (tSSRSScri.getMaxRow() > 0)
        {
            // 添加退保利率的标签
            //	tXMLDataList = new XMLDataList();
            // 添加xml一个新的对象LCRiskZTFee
            tXMLDataList.setDataObjectID("LCRiskZTFee");
            // 添加Head单元内的信息
            tXMLDataList.addColHead("RiskZTRate1");
            tXMLDataList.addColHead("RiskZTRate2");
            tXMLDataList.addColHead("RiskZTRate3");
            tXMLDataList.addColHead("RiskZTRate4");
            tXMLDataList.addColHead("RiskZTRate5");
            tXMLDataList.addColHead("RiskZTRate6");
            tXMLDataList.buildColHead();
            for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
            {

                NumberFormat nt = NumberFormat.getPercentInstance();
                nt.setMaximumFractionDigits(6);
                String tRiskZTRate = nt.format(Double.parseDouble(tSSRSScri.GetText(i, 1)));
                if (i == 1)
                {
                    tXMLDataList.setColValue("RiskZTRate1", tRiskZTRate);
                }
                if (i == 2)
                {
                    tXMLDataList.setColValue("RiskZTRate2", tRiskZTRate);
                }
                if (i == 3)
                {
                    tXMLDataList.setColValue("RiskZTRate3", tRiskZTRate);
                }
                if (i == 4)
                {
                    tXMLDataList.setColValue("RiskZTRate4", tRiskZTRate);
                }
                if (i == 5)
                {
                    tXMLDataList.setColValue("RiskZTRate5", tRiskZTRate);
                }
                if (i == 6)
                {
                    tXMLDataList.setColValue("RiskZTRate6", tRiskZTRate);
                }

            }
            tXMLDataList.insertRow(0);
            tXmlDataset.addDataObject(tXMLDataList);
        }
        return true;
    }

    private String cancelKeXueCount(String moneyString)
    {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        BigDecimal b = new BigDecimal(String.valueOf(moneyString));
        return df.format(b.doubleValue());
    }
    
    private boolean checkJetFormType(){
    	String msql = "select 1 from ldcode where codetype='JetFormType' and code='J108' and codealias='1'";
		SSRS s = new ExeSQL().execSQL(msql);
		if(s.getMaxRow()<1){
			return false;
		}
		return true;
    }
    
    private boolean getPrem(XMLDataset tXMLDataset){
    	String msql = "select appflag from lcgrpcont where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
    	SSRS msqls = new ExeSQL().execSQL(msql);
    	if("9".equals(msqls.GetText(1, 1))){
    		tXMLDataset.addDataObject(new XMLDataTag("PremSum", "--"));
			tXMLDataset.addDataObject(new XMLDataTag("PremSumCH", "--"));
			return true;
    	}else{
    		String msql1 = "select payintv from lcgrpcont where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
    		SSRS s = new ExeSQL().execSQL(msql1);
    		if(s.getMaxRow()>0){
    			String PremSum = "";
    			String tPayIntv = s.GetText(1, 1);
    			if("-1".equals(tPayIntv)){
    				String msql2="select PremScope from lcgrpcont where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
    				SSRS s2 = new ExeSQL().execSQL(msql2);
    				PremSum = PubFun.setPrecision2(s2.GetText(1, 1));
    			}else if("0".equals(tPayIntv)){
    				String msql3="select sum(sumactupaymoney) from ljapay where incomeno='"+this.mLCGrpContSchema.getGrpContNo()+"' and duefeetype='0'";
    				SSRS s3 = new ExeSQL().execSQL(msql3);
    				PremSum = PubFun.setPrecision2(s3.GetText(1, 1));
    			}else if("1".equals(tPayIntv) || "3".equals(tPayIntv) || "6".equals(tPayIntv) || "12".equals(tPayIntv)){
    				String msql4="select timestampdiff (64, char(timestamp(PayendDate) - timestamp(Cvalidate))) from lcgrppol where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' fetch first 1 rows only with ur";
    				String msql5="select sum(sumactupaymoney) from ljapay where incomeno='"+this.mLCGrpContSchema.getGrpContNo()+"' and duefeetype='0'";
    				String tDifMonth = new ExeSQL().execSQL(msql4).GetText(1, 1);
    				if("".equals(tDifMonth) || tDifMonth == null){
    					buildError("getPrem", "取保险期间失败！");
    					return false;
    				}
    				String tPrem = new ExeSQL().execSQL(msql5).GetText(1, 1);
    				if("".equals(tPrem) || tPrem == null){
    					buildError("getPrem", "取首期实收保费失败！");
    					return false;
    				}
    				int tPayTimes = 1;
    				tPayTimes = Integer.parseInt(tDifMonth)/Integer.parseInt(tPayIntv);
    				if(tPayTimes < 1){
    					tPayTimes = 1;
    				}
    				PremSum = PubFun.setPrecision2(Double.parseDouble(tPrem)*tPayTimes);
//    			PremSum = String.valueOf(Double.parseDouble(tPrem)*tPayTimes);
    			}else{
    				buildError("getPrem", "取缴费频次失败！");
    				return false;
    			}
    			tXMLDataset.addDataObject(new XMLDataTag("PremSum", PremSum));
    			tXMLDataset.addDataObject(new XMLDataTag("PremSumCH", PubFun.getChnMoney(Double.parseDouble(PremSum))));
    			return true;
    		}else{
    			buildError("getPrem", "取缴费频次失败！");
    			return false;
    		}
    	}
    }
  
    public static void main(String[] args)
    {
    	LCGrpContF1PBL bl = new LCGrpContF1PBL();
        bl.mGlobalInput.ComCode = "86";
        bl.mGlobalInput.ManageCom = bl.mGlobalInput.ComCode;
        bl.mGlobalInput.Operator = "group";
        bl.mTemplatePath = "D:\\SourceCode\\lisdev\\ui\\f1print\\template\\";
        bl.mOutXmlPath = "E:\\TC";

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo("00110186000006");//00107433000006  00114302000003
        tLCGrpContDB.getInfo();
        bl.mLCGrpContSchema = tLCGrpContDB.getSchema();

        XMLDatasets tXMLDatasets = new XMLDatasets();
        tXMLDatasets.createDocument();

        //        XMLDataset tXMLDataset = tXMLDatasets.createDataset();

        //        if (!bl.combineInvioce(tXMLDataset))
        //        {
        //            System.out.println(bl.mErrors.getErrContent());
        //        }
        VData aInputData = new VData();
        aInputData.add(bl.mGlobalInput);
        aInputData.add(bl.mLCGrpContSchema);
        aInputData.add(bl.mTemplatePath);
        aInputData.add(bl.mOutXmlPath);

        if (!bl.submitData(aInputData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
