package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ConFeeF1PUI implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public ConFeeF1PUI()
    {
    }

    public static void main(String[] args)
    {
        LOPRTManagerSchema a = new LOPRTManagerSchema();
        a.setPrtSeq("86110020040810000031");
        GlobalInput g = new GlobalInput();
        VData tVData = new VData();
        tVData.addElement(a);
        tVData.addElement(g);
        ConFeeF1PUI u = new ConFeeF1PUI();
        u.submitData(tVData, "PRINT");
        VData result = new VData();
        result = u.getResult();

    }

    /**
     *
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("cOperate:" + cOperate);
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        ConFeeF1PBL tConFeeF1PBL = new ConFeeF1PBL();
        System.out.println("Start ConFeeF1P UI Submit ...");

        if (!tConFeeF1PBL.submitData(vData, cOperate))
        {
            if (tConFeeF1PBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tConFeeF1PBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "ConFeeF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tConFeeF1PBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLOPRTManagerSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //mLJSPaySchema.setSchema((LJSPaySchema)cInputData.getObjectByObjectName("LJSPaySchema",0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ConFeeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}