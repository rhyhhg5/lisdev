package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function ://打印“银行代扣正确清单包括（首期和续期）”
 * @version 1.0
 * @date 2004-6-28
 */

import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.*;

public class NewPrintBillRightBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mSerialNo = "";
    private String mBankCode = "";
    private String mStation = "";
    private String strMngCom;
    private GlobalInput mG = new GlobalInput();
    private String mFlag = "";
    private String mTFFlag = ""; //查询正确错误清单T--正确,F--错误
    private String mXQFlag = ""; //X---续期S--首期
    // private String mSuccFlag = "";//记录每个银行成功返回值的标志
    String mSuccFlag[] = new String[10];

    private String mChkSuccFlag; //银行校验成功标志；
    private String mChkFailFlag; //银行校验失败标志；
    private int mCount = 0;
    private int mSumQiYueCount=0;
    private int mSumXuQiCount=0;
    private int mSumBaoQuanCount=0;
    private String mMessage = "";
    private double mSumDuePayMoney = 0.00;
    private double mSumQiYuePayMoney=0.00;
    private double mSumXuQiPayMoney=0.00;
    private double mSumBaoQuanMoney=0.00;

    private PremBankPubFun mPremBankPubFun = new PremBankPubFun();
    public NewPrintBillRightBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (mTFFlag.equals("T")) {
            if (!getPrintRightData()) {
                return false;
            }

        }
        if (mTFFlag.equals("F")) {
            if (!getPrintErrorData()) {
                return false;
            }
        }
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mSerialNo = (String) cInputData.get(0);
        mBankCode = (String) cInputData.get(1);
        mFlag = (String) cInputData.get(2); //查询的标准“YF”or“YS”
        //mStation = (String) cInputData.get(3);
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mStation = mG.ManageCom;
        mTFFlag = (String) cInputData.get(3);
        mXQFlag = (String) cInputData.get(4); ;

        mSerialNo = mSerialNo.trim();
        mBankCode = mBankCode.trim();
        mFlag = mFlag.trim();
        mStation = mStation.trim();
        mTFFlag = mTFFlag.trim();
        mXQFlag = mXQFlag.trim();
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrintBillRightBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    //查询出首期或者是续期的银行代收正确清单
    private boolean getPrintRightData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrintBillRight.vts", "printer");
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");

        //转账成功的清单，要查询应收和实收的并集因为在没有进行核销时，仍为应收信息
        //打印成功的清单

        mSuccFlag = this.getSuccFlag(mBankCode);
        System.out.println("数组的长度是" + mSuccFlag.length);
        String bankSql =
                "select bankunitecode from ldbankunite where bankunitecode = '" +
                mBankCode + "' ";
        ExeSQL bankExeSQL = new ExeSQL();
        String UniteBank = bankExeSQL.getOneValue(bankSql);
        for (int l = 0; l < mSuccFlag.length; l++) {
            if (!(mSuccFlag[l] == null || mSuccFlag[l].equals(""))) {
                String main_sql = "";
                if (StrTool.cTrim(UniteBank).equals("")) {
                    main_sql =
                            " select paycode,bankcode,BankSuccFlag from LYReturnFromBank where SerialNo = '" +
                            mSerialNo +
                            "' and DealType = 'S' and BankCode =  '" +
                            mBankCode + "' and ComCode like '" + mStation +
                            "%' and BankSuccFlag ='" + mSuccFlag[l].trim() +
                            "' "
                            + " union all select paycode, BankCode,BankSuccFlag from LYReturnFromBankB where SerialNo = '" +
                            mSerialNo +
                            "' and DealType = 'S' and BankCode =  '" +
                            mBankCode + "' and ComCode like '" + mStation +
                            "%' and BankSuccFlag ='" + mSuccFlag[l].trim() +
                            "' ";
                } else {
                    main_sql =
                            " select paycode,bankcode,BankSuccFlag from LYReturnFromBank where SerialNo = '" +
                            mSerialNo + "' and DealType = 'S' and BankCode in (select bankcode from ldbankunite where bankunitecode = '" +
                            UniteBank + "') and ComCode like '" + mStation +
                            "%' and BankSuccFlag ='" + mSuccFlag[l].trim() +
                            "' "
                            + " union all select paycode, BankCode,BankSuccFlag from LYReturnFromBankB where SerialNo = '" +
                            mSerialNo + "' and DealType = 'S' and BankCode in (select bankcode from ldbankunite where bankunitecode = '" +
                            UniteBank + "') and ComCode like '" + mStation +
                            "%' and BankSuccFlag ='" + mSuccFlag[l].trim() +
                            "' ";
                }
                System.out.println("主查询语句是" + main_sql);
                ExeSQL main_exesql = new ExeSQL();
                SSRS main_ssrs = main_exesql.execSQL(main_sql);
                String tReturnDate=PubFun.getCurrentDate();
                if (main_ssrs.getMaxRow() > 0) {
                    System.out.println("查询的结果是" + main_ssrs.getMaxRow());
                    for (int main_count = 1; main_count <= main_ssrs.getMaxRow();
                                          main_count++) {
                        //判断返回的是标志是正确的还是错误的，此处只取正确的
                        String tf_flag = mPremBankPubFun.getBankSuccFlag(
                                mBankCode);
                        boolean jy_flag_b = mPremBankPubFun.verifyBankSuccFlag(
                                tf_flag, main_ssrs.GetText(main_count, 3));
                        System.out.println("返回值是" + jy_flag_b);
                        if (jy_flag_b) {
                            String addsql = "";
                            String aMessage = "";
                            //若是正确的，则查询其是首期还是续期
                            String sec_sql = "";
                            String add_sql = "";

                            mMessage = "没有银行返回交费成功清单记录";
                            sec_sql =
                                    "select distinct a.tempfeeno,c.agentcode,c.agentgroup,b.AccNo,b.AccName,a.paymoney,"
                                    + "case c.tempfeetype when '1'  then '新契约' when '2' then '续期' when '4' then '保全' when '7' then '保全' when '8' then '新契约' else '其他' end,b.modifydate "
                                    + " from ljtempfeeclass a,lyreturnfrombankb b,ljtempfee c where a.tempfeeno = b.paycode and a.tempfeeno=c.tempfeeno and b.paycode = '" +
                                    main_ssrs.GetText(main_count, 1) + "' " +
                                    "and a.enteraccdate is not null " +
                                    "and b.serialno = '" + mSerialNo +
                                    "' union all select distinct a.tempfeeno,c.agentcode,c.agentgroup,b.AccNo,b.AccName,a.paymoney,"
                                    + "case c.tempfeetype when '1'  then '新契约' when '2' then '续期' when '4' then '保全' when '7' then '保全' when '8' then '新契约' else '其他' end,b.modifydate"
                                    + " from ljtempfeeclass a,lyreturnfrombank b,ljtempfee c where a.tempfeeno = b.paycode and a.tempfeeno=c.tempfeeno  and b.paycode = '" +
                                    main_ssrs.GetText(main_count, 1) + "' " +
                                    "and a.enteraccdate is not null " +
                                    "and b.serialno = '" + mSerialNo + "'";

                            System.out.println("查询的语句是" + sec_sql);

                            ExeSQL sec_exesql = new ExeSQL();
                            SSRS sec_ssrs = sec_exesql.execSQL(sec_sql);
                            if (sec_ssrs.getMaxRow() > 0) {
                                System.out.println("查询的结果是" +
                                        sec_ssrs.getMaxRow());
                                //对其明细信息进行附值
                                String[] cols = new String[9];
                                cols[0] = StrTool.cTrim(sec_ssrs.GetText(1, 2)); //业务员编码
                                if(cols[0]==null || (cols[0]!=null && cols[0].trim().equals(""))){
                                }else{
                                    cols[1] = StrTool.cTrim(mPremBankPubFun.getAgentInfo(sec_ssrs.
                                            GetText(1, 2)).getName()); //业务员姓名
                                    
                                    //将AgentCode转换为GroupAgentCode
                			        //统一工号修改
                			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
                			        ExeSQL aExeSQL = new ExeSQL();
                			        SSRS aSSRS = new SSRS();
                			        aSSRS = aExeSQL.execSQL(SQL);
                				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
                				  		cols[0] = aSSRS.GetText(1, 1);
                				  	}

                                }
                                if(sec_ssrs.GetText(1, 3)==null||sec_ssrs.GetText(1, 3).equals("")){
                                	cols[2]="无组别";
                                	}
                                else{
                                 cols[2] = StrTool.cTrim(mPremBankPubFun.getAgentGroup(	
                                        sec_ssrs.GetText(1, 3)));} //业务员组别
                                if(sec_ssrs.GetText(1, 2)==null||sec_ssrs.GetText(1, 2).equals("")){
                                	cols[8]="无机构";
                                }
                                else{
                                	cols[8]=StrTool.cTrim(mPremBankPubFun.getManagecom(sec_ssrs.GetText(1, 2)));
                                }
                                cols[3] = StrTool.cTrim(sec_ssrs.GetText(1, 4)); //银行账号
                                cols[4] = StrTool.cTrim(sec_ssrs.GetText(1, 5)); //银行户名
                                cols[6] = StrTool.cTrim(sec_ssrs.GetText(1, 7)); //类别
                                cols[7] = StrTool.cTrim(sec_ssrs.GetText(1, 6)); //累计金额
                                cols[5] = StrTool.cTrim(sec_ssrs.GetText(1, 1)); //缴费通知书号/暂收收据号
                                tReturnDate = StrTool.cTrim(sec_ssrs.GetText(1, 8));//日期
                                
                                if(cols[6]!=null && !cols[6].trim().equals("")){
                                    if(cols[6].trim().equals("新契约")){
                                        mSumQiYuePayMoney = mSumQiYuePayMoney +
                                        Double.parseDouble(cols[7]);
                                        mSumQiYueCount++;
                                    }else if(cols[6].trim().equals("续期")){
                                        mSumXuQiPayMoney = mSumXuQiPayMoney +
                                        Double.parseDouble(cols[7]);
                                        mSumXuQiCount++;
                                    }else if(cols[6].trim().equals("保全")){
                                        mSumBaoQuanMoney = mSumBaoQuanMoney +
                                        Double.parseDouble(cols[7]);
                                        mSumBaoQuanCount++;
                                    }
                                }
                                
                                mCount = mCount + 1;
                                for (int i=0; i<cols.length; i++) {
                                	if (cols[i] == null) {
                                
                                	cols[i] = "";
                                	
                                	}
                                }
                            
                                alistTable.add(cols);
                                mSumDuePayMoney = mSumDuePayMoney +
                                                  Double.parseDouble(cols[7]);
                            }
                        }
                    }
                    if (mCount > 0) {
                        System.out.println("开始执行最外部分的循环");
                        String[] b_col = new String[8];
                        xmlexport.addDisplayControl("displayinfo");
                        xmlexport.addListTable(alistTable, b_col);
                        texttag.add("BillNo", mSerialNo);
                        texttag.add("Date", tReturnDate);
                        texttag.add("BankCode", mBankCode);
                        texttag.add("BankName",
                                    mPremBankPubFun.getBankInfo(mBankCode).
                                    getBankName().trim());
                        texttag.add("SumDuePayMoney", mSumDuePayMoney);
                        texttag.add("SumQiYuePayMoney", mSumQiYuePayMoney);
                        texttag.add("SumXuQiPayMoney", mSumXuQiPayMoney);
                        texttag.add("SumBaoQuanMoney", mSumBaoQuanMoney);
                        texttag.add("SumCount", mCount);
                        texttag.add("SumQiYueCount", mSumQiYueCount);
                        texttag.add("SumXuQiCount", mSumXuQiCount);
                        texttag.add("SumBaoQuanCount", mSumBaoQuanCount);
                        if (texttag.size() > 0) {
                            xmlexport.addTextTag(texttag);
                        }
                        xmlexport.outputDocumentToFile("e:\\",
                                "NewPrintBillRightBL"); //输出xml文档到文件
                        mResult.clear();
                        mResult.addElement(xmlexport);
                        return true;
                    } else {
                        this.mErrors.copyAllErrors(this.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = mMessage;
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean getPrintErrorData() {
        String tel = "";
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrintBill.vts", "printer");
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");

        //转账成功的清单，要查询应收和实收的并集因为在没有进行核销时，仍为应收信息
        //打印成功的清单
        String[] mSuccFlag = this.getSuccFlag(mBankCode);
        System.out.println("长度是" + mSuccFlag.length);
        String mSuccFlagSet = " ";
        String bankSql =
                "select bankunitecode from ldbankunite where bankunitecode = '" +
                mBankCode + "' ";
        ExeSQL bankExeSQL = new ExeSQL();
        String UniteBank = bankExeSQL.getOneValue(bankSql);
        for (int p = 0; p < mSuccFlag.length; p++) {
            if (!(mSuccFlag[p] == null || mSuccFlag[p].equals(""))) {
                mSuccFlagSet = mSuccFlagSet + "'" + mSuccFlag[p].trim() + "',";
            }
        }
        mSuccFlagSet = " ( " + mSuccFlagSet + " 'AAAAAAA' ) ";
        System.out.println("最后的成功标识结果集合是" + mSuccFlagSet);
        String main_sql = "";
        if (StrTool.cTrim(UniteBank).equals("")) {
            main_sql =
                    " select paycode,bankcode,BankSuccFlag from LYReturnFromBank where SerialNo = '" +
                    mSerialNo + "' and DealType = 'S' and BankCode =  '" +
                    mBankCode + "' and ComCode like '" + mStation +
                    "%' and BankSuccFlag not in " + mSuccFlagSet + " "
                    + " union all select paycode, BankCode,BankSuccFlag from LYReturnFromBankB where SerialNo = '" +
                    mSerialNo + "' and DealType = 'S' and BankCode =  '" +
                    mBankCode + "' and ComCode like '" + mStation +
                    "%' and (BankSuccFlag not in " + mSuccFlagSet +
                    " or banksuccflag is null )";
        } else {
            main_sql =
                    " select paycode,bankcode,BankSuccFlag from LYReturnFromBank where SerialNo = '" +
                    mSerialNo + "' and DealType = 'S' and BankCode in (select bankcode from ldbankunite where bankunitecode = '" +
                    UniteBank + "') and ComCode like '" + mStation +
                    "%' and BankSuccFlag not in " + mSuccFlagSet + " "
                    + " union all select paycode, BankCode,BankSuccFlag from LYReturnFromBankB where SerialNo = '" +
                    mSerialNo + "' and DealType = 'S' and BankCode in (select bankcode from ldbankunite where bankunitecode = '" +
                    UniteBank + "') and ComCode like '" + mStation +
                    "%' and (BankSuccFlag not in " + mSuccFlagSet +
                    " or banksuccflag is null )";
        }
        System.out.println("主查询语句是" + main_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = main_exesql.execSQL(main_sql);
        if (main_ssrs.getMaxRow() > 0) {
            System.out.println("查询的结果是" + main_ssrs.getMaxRow());
            for (int main_count = 1; main_count <= main_ssrs.getMaxRow();
                                  main_count++) {
                //判断返回的是标志是正确的还是错误的，此处只取正确的
                System.out.println("main_count : " + main_count);
                String tf_flag = mPremBankPubFun.getBankSuccFlag(mBankCode);
                boolean jy_flag_b = mPremBankPubFun.verifyBankSuccFlag(tf_flag,
                        main_ssrs.GetText(main_count, 3));
                System.out.println("返回值是" + jy_flag_b);
                if (!jy_flag_b) {
                    String strSql = "";
                    String appntName = "";
                    String strTel = "";
                    mMessage = "没有银行返回首期交费失败清单记录";
                    //首期采用续期的not in
                    String thr_sql = "";
                    thr_sql = "select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                              + "case a.tempfeetype when '1'  then '新契约' when '2' then '续期' when '4' then '保全' when '7' then '保全' when '8' then '新契约' else '其他' end"
                              + " from ljtempfee a,lyreturnfrombankb b where a.tempfeeno = b.paycode and b.paycode = '" +
                              main_ssrs.GetText(main_count, 1) +
                              "' and b.serialno = '" + mSerialNo +
                              "' union all select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                              + "case a.tempfeetype when '1'  then '新契约' when '2' then '续期' when '4' then '保全' when '7' then '保全' when '8' then '新契约' else '其他' end"
                              + " from ljtempfee a,lyreturnfrombank b where a.tempfeeno = b.paycode and b.paycode = '" +
                              main_ssrs.GetText(main_count, 1) +
                              "' and b.serialno = '" + mSerialNo + "'";

                    System.out.println("查询的语句是" + thr_sql);
                    ExeSQL sec_exesql = new ExeSQL();
                    SSRS sec_ssrs = sec_exesql.execSQL(thr_sql);
                    int tPolFlag=0;
                    if (sec_ssrs.getMaxRow() > 0) {
                        System.out.println("查询的结果是" + sec_ssrs.getMaxRow());

                        String[] cols = new String[13];
                        cols[0] = StrTool.cTrim(sec_ssrs.GetText(1, 1)); //业务员编码
                        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                        if(StrTool.cTrim(sec_ssrs.GetText(1, 1)).equals("")){
                            tLAAgentSchema.setName("无记录");
                        }else{
                            tLAAgentSchema.setSchema(mPremBankPubFun.getAgentInfo(
                                    sec_ssrs.GetText(1, 1)));
                            
                            //将AgentCode转换为GroupAgentCode
        			        //统一工号修改
        			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
        			        ExeSQL aExeSQL = new ExeSQL();
        			        SSRS aSSRS = new SSRS();
        			        aSSRS = aExeSQL.execSQL(SQL);
        				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
        				  		cols[0] = aSSRS.GetText(1, 1);
        				  	}

                        }
                        cols[1] = StrTool.cTrim(tLAAgentSchema.getName());
                        //进行查询
                        cols[2] = StrTool.cTrim(mPremBankPubFun.getAgentGroup(tLAAgentSchema.getAgentGroup())); //业务原组别
                        //cols[3]=mPremBankPubFun.(tLAAgentSchema.getManageCom());
                        cols[3] = StrTool.cTrim(sec_ssrs.GetText(1, 3)); //银行账号
                        cols[4] = StrTool.cTrim(sec_ssrs.GetText(1, 4)); //银行户名
                        //查询ljspay中的数据
                        cols[9]=StrTool.cTrim(sec_ssrs.GetText(1, 9)+" ");
                        LJSPayDB tLJSPayDB = new LJSPayDB();
                        tLJSPayDB.setGetNoticeNo(StrTool.cTrim(sec_ssrs.GetText(1, 9)));
                        if (tLJSPayDB.getInfo()) {
                            cols[5] = String.valueOf(tLJSPayDB.getSendBankCount());
                        } else {
                            cols[5] = "Z";
                        }
                        LDCode1DB tLDCode1DB = new LDCode1DB();
                        tLDCode1DB.setCodeType("bankerror");
                        if (StrTool.cTrim(UniteBank).equals("")) {
                            tLDCode1DB.setCode(mBankCode);
                        } else {
                            tLDCode1DB.setCode(UniteBank);
                        }
                        tLDCode1DB.setCode1(sec_ssrs.GetText(1, 8));
                        LDCode1Set tLDCode1Set = new LDCode1Set();
                        LDCode1Schema tLDCode1Schema = new LDCode1Schema();
                        tLDCode1Set.set(tLDCode1DB.query());
                        int k = tLDCode1Set.size();
                        String b_errorReason;
                        if (k == 0) {
                            b_errorReason = "没有指明错误原因";
                        } else {
                            tLDCode1Schema.setSchema(tLDCode1Set.get(1));
                            b_errorReason = tLDCode1Schema.getCodeName();
                        }
                        cols[6] = StrTool.cTrim(b_errorReason);
                        LCPolDB tLCPolDB = new LCPolDB();
                        LCPolSet tLCPolSet = new LCPolSet();
                        System.out.println(sec_ssrs.GetText(1, 6));
                        
                        if (sec_ssrs.GetText(1, 6).equals("1")) {
                            //集体保单号码
                            tLCPolDB.setGrpContNo(sec_ssrs.GetText(1, 5));
                        }
                        else if (sec_ssrs.GetText(1, 6).equals("2")) {
                            //个人保单号码
                            tLCPolDB.setContNo(sec_ssrs.GetText(1, 5));
                            /** 查询投保人 */
                            strSql =
                                    "select appntname from lccont where contno ='" +
                                    sec_ssrs.GetText(1, 5) + "' union " +
                                    "select appntname from lbcont where contno ='" +
                                    sec_ssrs.GetText(1, 5) + "' union " +
                                    "select appntname from lobcont where contno ='" +
                                    sec_ssrs.GetText(1, 5) + "' ";
                            appntName = sec_exesql.getOneValue(strSql);
                            //投保人的联系方式
                            strTel = "select Mobile from lcaddress  where customerno in(select appntno from lcappnt where contno='" +
                                     sec_ssrs.GetText(1, 5) + "') "
                                     +
                                     "and addressno in(select addressno from lcappnt where contno='" +
                                     sec_ssrs.GetText(1, 5) + "') ";
                            tel = sec_exesql.getOneValue(strTel);
                        }
                        else if (sec_ssrs.GetText(1, 6).equals("6")) {
                            tLCPolDB.setProposalNo(sec_ssrs.GetText(1,
                                    5));
                        }
                        else if (sec_ssrs.GetText(1, 6).equals("9")) {
                            tLCPolDB.setPrtNo(sec_ssrs.GetText(1, 5));
                            /** 查询投保人 */
                            strSql =
                                    "select appntname from lccont where prtno ='" +
                                    sec_ssrs.GetText(1, 5) + "' union " +
                                    "select appntname from lbcont where prtno ='" +
                                    sec_ssrs.GetText(1, 5) + "' union " +
                                    "select appntname from lobcont where prtno ='" +
                                    sec_ssrs.GetText(1, 5) + "' ";
                            appntName = sec_exesql.getOneValue(strSql);
                            //投保人的联系方式
                            strTel = "select Mobile from lcaddress  where customerno in(select appntno from lcappnt where prtno='" +
                                     sec_ssrs.GetText(1, 5) + "') "
                                     +
                                     "and addressno in(select addressno from lcappnt where prtno='" +
                                     sec_ssrs.GetText(1, 5) + "') ";
                            tel = sec_exesql.getOneValue(strTel);
                        }
                        else {
                            tPolFlag=1;
                        }
                        if(tPolFlag==0){
                            tLCPolSet.set(tLCPolDB.query());
                        }

                        if (tLCPolSet.size() > 0) {
                            //cols[9] = tLCPolSet.get(1).getMainPolNo();
                            cols[9] = StrTool.cTrim(tLCPolSet.get(1).getPrtNo());
                            cols[7] = StrTool.cTrim(appntName);
                            //tel = mPremBankPubFun.getAppntAddr(tLCPolSet.
                            //get(1).getAppntNo()).getMobile(); //投保人的联系方式
                            if (StrTool.cTrim(tel).equals("")) {
                                cols[8] = " ";
                            } else {
                                cols[8] = StrTool.cTrim(tel);
                            }
                        } else {
                            LBPolDB tLBPolDB = new LBPolDB();
                            LBPolSet tLBPolSet = new LBPolSet();
                            int tFlag=0;
                            if (sec_ssrs.GetText(1, 6).equals("1")) {
                                //集体保单号码
                                tLBPolDB.setGrpPolNo(sec_ssrs.GetText(1,
                                        5));
                                tFlag=1;
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("2")) {
                                //个人保单号码
                                tLBPolDB.setPolNo(sec_ssrs.GetText(1, 5));
                                tFlag=1;
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("6")) {
                                tLBPolDB.setProposalNo(sec_ssrs.GetText(
                                        1, 5));
                                tFlag=1;
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("9")) {
                                tLBPolDB.setPrtNo(sec_ssrs.GetText(1, 5));
                                tFlag=1;
                            }
                            if(tPolFlag==0 && tFlag==1){
                                tLBPolSet.set(tLBPolDB.query());
                            }
                            if (tLBPolSet.size() > 0) {
                                //cols[9] = tLBPolSet.get(1).getMainPolNo();
                                cols[9] = StrTool.cTrim(tLBPolSet.get(1).getPrtNo());
                                cols[7] = StrTool.cTrim(appntName);
                                //tel = mPremBankPubFun.getAppntAddr(
                                //tLBPolSet.get(1).getAppntNo()).getMobile(); //投保人的联系方式
                                if (StrTool.cTrim(tel).equals("") ) {
                                    cols[8] = " ";
                                } else {
                                    cols[8] = tel;
                                }
                            }
                        }
                        cols[10] = StrTool.cTrim(sec_ssrs.GetText(1, 10)); //类别
                        cols[11] = StrTool.cTrim(sec_ssrs.GetText(1, 7)); //累计金额
                        cols[12]=StrTool.cTrim(mPremBankPubFun.getManagecom(sec_ssrs.GetText(1, 1)));
                        if(tPolFlag==0){
                            mCount = mCount + 1;
                            alistTable.add(cols);
                            mSumDuePayMoney = mSumDuePayMoney +
                                          Double.parseDouble(cols[11]);
                        }
                    }

                    if (sec_ssrs.getMaxRow() == 0 || tPolFlag==1) {
                        tPolFlag=0;
                        String thr_sqlB = "";
                        //modify by yingxl 2008-06-13
                        thr_sqlB = "select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                                   + " '续期' "
                                   + " from ljspay a,lyreturnfrombankb b where a.getnoticeno = b.paycode and b.paycode = '" +
                                   main_ssrs.GetText(main_count, 1) +
                                   "' and b.serialno = '" + mSerialNo +
                                   "' union all select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                                   + " '续期' "
                                   + " from ljspay a,lyreturnfrombank b where a.getnoticeno = b.paycode and b.paycode = '" +
                                   main_ssrs.GetText(main_count, 1) +
                                   "' and b.serialno = '" + mSerialNo + "'";
                        thr_sqlB = thr_sqlB +"union all "
                                   +"select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                                   + " '续期' "
                                   + " from ljspayb a,lyreturnfrombankb b where a.getnoticeno = b.paycode and b.paycode = '" +
                                   main_ssrs.GetText(main_count, 1) +
                                   "' and b.serialno = '" + mSerialNo +
                                   "' union all select distinct b.AgentCode,b.BankCode,b.AccNo,b.AccName,b.polno,b.NoType,b.PayMoney,b.BankSuccFlag,b.PayCode,"
                                   + " '续期' "
                                   + " from ljspayb a,lyreturnfrombank b where a.getnoticeno = b.paycode and b.paycode = '" +
                                   main_ssrs.GetText(main_count, 1) +
                                   "' and b.serialno = '" + mSerialNo + "'";
                        System.out.println(thr_sqlB);
                        //end modify 
                        System.out.println("查询的语句是" + thr_sqlB);
                        sec_exesql = new ExeSQL();
                        sec_ssrs = new SSRS();
                        sec_ssrs = sec_exesql.execSQL(thr_sqlB);
                        if(sec_ssrs.getMaxRow()>0){
                            String[] cols = new String[12];
                            cols[0] = StrTool.cTrim(sec_ssrs.GetText(1, 1)); //业务员编码
                            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                            if(StrTool.cTrim( sec_ssrs.GetText(1, 1)).equals("")){
                                tLAAgentSchema.setName("无记录");
                            }else{
                                tLAAgentSchema.setSchema(mPremBankPubFun.
                                        getAgentInfo(
                                                sec_ssrs.GetText(1, 1)));
                                
                                //将AgentCode转换为GroupAgentCode
            			        //统一工号修改
            			        String SQL = "select getUniteCode("+cols[0]+") from dual where 1=1 with ur";
            			        ExeSQL aExeSQL = new ExeSQL();
            			        SSRS aSSRS = new SSRS();
            			        aSSRS = aExeSQL.execSQL(SQL);
            				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
            				  		cols[0] = aSSRS.GetText(1, 1);
            				  	}

                            }
                            cols[1] = StrTool.cTrim(tLAAgentSchema.getName());
                            //进行查询
                            cols[2] = StrTool.cTrim(mPremBankPubFun.getAgentGroup(
                                    tLAAgentSchema.
                                    getAgentGroup().trim())); //业务原组别
                            cols[3] = StrTool.cTrim(sec_ssrs.GetText(1, 3)); //银行账号
                            cols[4] = StrTool.cTrim(sec_ssrs.GetText(1, 4)); //银行户名
                            //查询ljspay中的数据
                            LJSPayDB tLJSPayDB = new LJSPayDB();
                            tLJSPayDB.setGetNoticeNo(StrTool.cTrim(sec_ssrs.GetText(1, 9)));
                            if (tLJSPayDB.getInfo()) {
                                cols[5] = StrTool.cTrim(String.valueOf(tLJSPayDB.
                                        getSendBankCount()));
                            } else {
                                cols[5] = "Z";
                            }
                            LDCode1DB tLDCode1DB = new LDCode1DB();
                            tLDCode1DB.setCodeType("bankerror");
                            if (StrTool.cTrim(UniteBank).equals("")) {
                                tLDCode1DB.setCode(mBankCode);
                            } else {
                                tLDCode1DB.setCode(UniteBank);
                            }
                            tLDCode1DB.setCode1(StrTool.cTrim(sec_ssrs.GetText(1, 8)));
                            LDCode1Set tLDCode1Set = new LDCode1Set();
                            LDCode1Schema tLDCode1Schema = new LDCode1Schema();
                            tLDCode1Set.set(tLDCode1DB.query());
                            int k = tLDCode1Set.size();
                            String b_errorReason;
                            if (k == 0) {
                                b_errorReason = "没有指明错误原因";
                            } else {
                                tLDCode1Schema.setSchema(tLDCode1Set.get(1));
                                b_errorReason = tLDCode1Schema.getCodeName();
                            }
                            cols[6] = b_errorReason;
                            LCPolDB tLCPolDB = new LCPolDB();
                            LCPolSet tLCPolSet = new LCPolSet();
                            if (sec_ssrs.GetText(1, 6).equals("1")) {
                                //集体保单号码
                                tLCPolDB.setGrpContNo(StrTool.cTrim(sec_ssrs.GetText(1, 5)));
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("2")) {
                                //个人保单号码
                                tLCPolDB.setContNo(sec_ssrs.GetText(1, 5));
                                /** 查询投保人 */
                                strSql =
                                        "select appntname from lccont where contno ='" +
                                        sec_ssrs.GetText(1, 5) + "' union " +
                                        "select appntname from lbcont where contno ='" +
                                        sec_ssrs.GetText(1, 5) + "' union " +
                                        "select appntname from lobcont where contno ='" +
                                        sec_ssrs.GetText(1, 5) + "' ";
                                appntName = sec_exesql.getOneValue(strSql);
                                //投保人的联系方式
                                strTel = "select Mobile from lcaddress  where customerno in(select appntno from lcappnt where contno='" +
                                         sec_ssrs.GetText(1, 5) + "') "
                                         +
                                         "and addressno in(select addressno from lcappnt where contno='" +
                                         sec_ssrs.GetText(1, 5) + "') ";
                                tel = sec_exesql.getOneValue(strTel);
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("6")) {
                                tLCPolDB.setProposalNo(sec_ssrs.GetText(1,
                                        5));
                            }
                            else if (sec_ssrs.GetText(1, 6).equals("9")) {
                                tLCPolDB.setPrtNo(sec_ssrs.GetText(1, 5));
                                /** 查询投保人 */
                                strSql =
                                        "select appntname from lccont where prtno ='" +
                                        sec_ssrs.GetText(1, 5) + "' union " +
                                        "select appntname from lbcont where prtno ='" +
                                        sec_ssrs.GetText(1, 5) + "' union " +
                                        "select appntname from lobcont where prtno ='" +
                                        sec_ssrs.GetText(1, 5) + "' ";
                                appntName = sec_exesql.getOneValue(strSql);
                                //投保人的联系方式
                                strTel = "select Mobile from lcaddress  where customerno in(select appntno from lcappnt where prtno='" +
                                         sec_ssrs.GetText(1, 5) + "') "
                                         +
                                         "and addressno in(select addressno from lcappnt where prtno='" +
                                         sec_ssrs.GetText(1, 5) + "') ";
                                tel = sec_exesql.getOneValue(strTel);
                            }else if(sec_ssrs.GetText(1, 6).equals("10")){
                                strSql="select contno from ljsgetendorse where endorsementno ='"+sec_ssrs.GetText(1, 5)+"' with ur";
                                String tContno = sec_exesql.getOneValue(strSql);
//                              个人保单号码
                                tLCPolDB.setContNo(tContno);
                                /** 查询投保人 */
                                strSql =
                                        "select appntname from lccont where contno ='" +
                                        tContno + "' union " +
                                        "select appntname from lbcont where contno ='" +
                                        tContno + "' union " +
                                        "select appntname from lobcont where contno ='" +
                                        tContno + "' ";
                                appntName = sec_exesql.getOneValue(strSql);
                                //投保人的联系方式
                                strTel = "select Mobile from lcaddress  where customerno in(select appntno from lcappnt where contno='" +
                                tContno + "') "
                                         +
                                         "and addressno in(select addressno from lcappnt where contno='" +
                                         tContno + "') ";
                                tel = sec_exesql.getOneValue(strTel);
                            }else{
                                tPolFlag=1;
                            }
                            if(tPolFlag==0){
                                tLCPolSet.set(tLCPolDB.query());
                            }

                            if ((StrTool.cTrim(appntName).equals("") ||
                                StrTool.cTrim(appntName).equals("null")) && tPolFlag==0) {
                                CError tError = new CError();
                                tError.moduleName = "ClaimBL";
                                tError.functionName = "submitData";
                                tError.errorMessage = "查询投保人姓名失败";
                                this.mErrors.addOneError(tError);
                                return false;
                            }

                            if (tLCPolSet.size() > 0) {
                                //cols[9] = tLCPolSet.get(1).getMainPolNo();
                                cols[9] = StrTool.cTrim(tLCPolSet.get(1).getPrtNo());
                                cols[7] = StrTool.cTrim(appntName);
                                //tel = mPremBankPubFun.getAppntAddr(tLCPolSet.
                                //get(1).getAppntNo()).getMobile(); //投保人的联系方式
                                if (StrTool.cTrim(tel).equals("")) {
                                    cols[8] = " ";
                                } else {
                                    cols[8] = tel;
                                }
                            } else {
                                LBPolDB tLBPolDB = new LBPolDB();
                                LBPolSet tLBPolSet = new LBPolSet();
                                int tFlag=0;
                                if (sec_ssrs.GetText(1, 6).equals("1")) {
                                    //集体保单号码
                                    tLBPolDB.setGrpPolNo(sec_ssrs.GetText(1,
                                            5));
                                    tFlag=1;
                                }
                                if (sec_ssrs.GetText(1, 6).equals("2")) {
                                    //个人保单号码
                                    tLBPolDB.setPolNo(sec_ssrs.GetText(1, 5));
                                    tFlag=1;
                                }
                                if (sec_ssrs.GetText(1, 6).equals("6")) {
                                    tLBPolDB.setProposalNo(sec_ssrs.GetText(
                                            1, 5));
                                    tFlag=1;
                                }
                                if (sec_ssrs.GetText(1, 6).equals("9")) {
                                    tLBPolDB.setPrtNo(sec_ssrs.GetText(1, 5));
                                    tFlag=1;
                                }
                                if(tFlag==1){
                                    tLBPolSet.set(tLBPolDB.query());
                                }
                                if (tLBPolSet.size() > 0) {
                                    //cols[9] = tLBPolSet.get(1).getMainPolNo();
                                    cols[9] = StrTool.cTrim(tLBPolSet.get(1).getPrtNo());
                                    cols[7] = StrTool.cTrim(appntName);
                                    //tel = mPremBankPubFun.getAppntAddr(
                                    //tLBPolSet.get(1).getAppntNo()).getMobile(); //投保人的联系方式
                                    if (StrTool.cTrim(tel).equals("")) {
                                        cols[8] = " ";
                                    } else {
                                        cols[8] = tel;
                                    }
                                }
                            }
                            cols[10] = StrTool.cTrim(sec_ssrs.GetText(1, 10)); //类别
                            cols[11] = StrTool.cTrim(sec_ssrs.GetText(1, 7)); //累计金额
                           // cols[12]
                            mCount = mCount + 1;
                            for (int i=0; i<cols.length; i++) {
                            	if (cols[i] == null) {
                            
                            	cols[i] = "";
                            	
                            	}
                            }
                        

                            alistTable.add(cols);
                            mSumDuePayMoney = mSumDuePayMoney +
                                              Double.parseDouble(cols[11]);

//                        sec_ssrs = sec_exesql.execSQL(thr_sqlB);
                        }
                    }
                    //对其明细信息进行附值
                }
            }
            if (mCount > 0) {
                System.out.println("开始执行最外部分的循环");
                String[] b_col = new String[12];
                xmlexport.addDisplayControl("displayinfo");
                xmlexport.addListTable(alistTable, b_col);
                texttag.add("BillNo", mSerialNo);
                texttag.add("Date", PubFun.getCurrentDate());
                texttag.add("BankCode", mBankCode);
                texttag.add("BankName",
                            mPremBankPubFun.getBankInfo(mBankCode).
                            getBankName().trim());
                texttag.add("SumMoney", mSumDuePayMoney);
                texttag.add("SumCount", mCount);
                if (texttag.size() > 0) {
                    xmlexport.addTextTag(texttag);
                }
                xmlexport.outputDocumentToFile("e:\\", "NewPrintBillErrorBL"); //输出xml文档到文件
                mResult.clear();
                mResult.addElement(xmlexport);
                return true;
            } else {
                this.mErrors.copyAllErrors(this.mErrors);
                CError tError = new CError();
                tError.moduleName = "ClaimBL";
                tError.functionName = "submitData";
                tError.errorMessage = mMessage;
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    public String[] getSuccFlag(String aBankCode) {
        String[] aSuccFlag = new String[10];
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(aBankCode);
        tLDBankDB.getInfo();

        String[] arrSucc = PubFun.split(tLDBankDB.getSchema().
                                        getAgentPaySuccFlag().trim(), ";");
        System.out.println("数组的长度是" + arrSucc.length);
        for (int i = 0; i < arrSucc.length; i++) {
            if (!(arrSucc[i] == null || arrSucc[i].equals(""))) {
                aSuccFlag[i] = arrSucc[i];
                System.out.println("该银行的返回标志是" + arrSucc[i]);
            }
        }
        return aSuccFlag;
    }

    public static void main(String[] args) {
        String aSerialNo = "00000000000000115729";
        String aBankCode = "999401";
        String aFlag = "YS";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8691";
        String aStation = "86";
        String aTFFlag = "F"; //T是正确F是错误
        String aXQFlag = "S";
        VData tVData = new VData();
        tVData.addElement(aSerialNo);
        tVData.addElement(aBankCode);
        tVData.addElement(aFlag);
        tVData.addElement(aTFFlag);
        tVData.addElement(aXQFlag);
        tVData.addElement(aStation);
        tVData.addElement(tG);
        NewPrintBillRightBL tNewPrintBillRightBL = new NewPrintBillRightBL();
        tNewPrintBillRightBL.submitData(tVData, "PRINT");
    }
}
