package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :续期保费银行转账失败清单
 * @version 1.0
 * @date 2004-4-27
 */

import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAppntIndSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ShowXQPremSuccBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();
    public PremBankPubFun mPremBankPubFun = new PremBankPubFun();

    private LYBankLogSet mLYBankLogSet = new LYBankLogSet();
    private LDBankSchema mLDBankSchema = new LDBankSchema();
    private LDCode1Schema mLDCode1Schema = new LDCode1Schema();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();

    //初始化全局变量，从前台承接数据
    private String strStartDate = ""; //开始日期
    private String strEndDate = ""; //结束日期
    private String strAgentState = ""; //业务员的状态(1为在职单，0为孤儿单)
    private String strPremType = ""; //首续期的标志
    private String strFlag = ""; //S or F(S为银行代收，F为银行代付)
    private String strComCode = ""; //系统登陆的机构(查询银行日志表)
    private String strStation = "";

    private String strBillNo = ""; //批次号码
    private String mBankName = ""; //银行名称
    private String mErrorReason = ""; //失败原因
    private String mChkSuccFlag = ""; //银行校验成功标志；
    private String mChkFailFlag = ""; //银行校验失败标志；
    private String mAgentGroup = "";
    private String mAgentState = "";
    private double mMoney = 0.00;
    private int mCount = 0;

    private LCPolSet mLCPolSet = new LCPolSet();
    private VData mInputData;
    public ShowXQPremSuccBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        if (!cOperate.equals("SHOW"))
        {
            mPremBankPubFun.buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!CaseFunPub.checkDate(strStartDate, strEndDate))
        {
            mPremBankPubFun.buildError("submitData", "开始日期比结束日期晚,请从新录入");
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getPrintData()
    {
        //在LJSPayPerson 和LJAPayPerson中查询符合条件的记录
        String main_sql = "";
        main_sql =
                "select a.PolNo,a.getnoticeno ,a.appntno ,b.BankDealDate,b.AccNo,b.AccName "
                + " from ljapayperson a,lyreturnfrombank b   where   a.getnoticeno = b.PayCode  and a.paycount >='2' and a.managecom like '" +
                strStation + "%' "
                + " and a.polno in (select otherno from  LOPRTManager where code = '48' and StateFlag = '0') "
                + " and b.serialno in ( "
                + " select serialno from lybanklog where senddate >='" +
                strStartDate + "' and senddate <='" + strEndDate + "' "
                + " and logtype = 'S' ) union all "
                + " select a.PolNo,a.getnoticeno ,a.appntno  ,b.BankDealDate,b.AccNo,b.AccName from ljapayperson a,lyreturnfrombankb b  "
                +
                " where  a.getnoticeno = b.PayCode  and a.paycount >='2' and a.managecom like '" +
                strStation + "%' "
                + " and a.polno in (select otherno from  LOPRTManager where code = '48' and StateFlag = '0') "
                + " and b.serialno in ( "
                + " select serialno from lybanklog where senddate >='" +
                strStartDate + "' and senddate <='" + strEndDate + "' "
                + " and logtype = 'S' ) union all "
                +
                " select a.PolNo,a.getnoticeno ,a.appntno  ,b.BankDealDate,b.AccNo,b.AccName "
                + " from ljspayperson a,lyreturnfrombank b where  a.getnoticeno = b.PayCode  and a.paycount >='2' and a.managecom like '" +
                strStation + "%' "
                + " and a.polno in (select otherno from  LOPRTManager where code = '48' and StateFlag = '0') "
                + "  and b.serialno in ( "
                + " select serialno from lybanklog where senddate >='" +
                strStartDate + "' and senddate <='" + strEndDate + "' "
                + " and logtype = 'S' ) union all "
                +
                " select a.PolNo,a.getnoticeno ,a.appntno ,b.BankDealDate,b.AccNo,b.AccName "
                + " from ljspayperson a,lyreturnfrombankb b where  a.getnoticeno = b.PayCode  and a.paycount >='2' and a.managecom like '" +
                strStation + "%' "
                + " and a.polno in (select otherno from  LOPRTManager where code = '48' and StateFlag = '0') "
                + " and b.serialno in ( "
                + " select serialno from lybanklog where senddate >='" +
                strStartDate + "' and senddate <='" + strEndDate + "' "
                + " and logtype = 'S' ) ";
        System.out.println("查询的语句是" + main_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = main_exesql.execSQL(main_sql);
        System.out.println("查询的结果是" + main_ssrs.getMaxRow());

        if (main_ssrs.getMaxRow() > 0)
        {
            for (int main_count = 1; main_count <= main_ssrs.getMaxRow();
                                  main_count++)
            {
                //查询出投保人名称的语句是
                LCPolSchema mLCPolSchema = new LCPolSchema();
                LCAppntIndSchema pLCAppntIndSchema = new LCAppntIndSchema();
                pLCAppntIndSchema = mPremBankPubFun.getAppntInfo(main_ssrs.
                        GetText(main_count, 3)); //查询投保人信息
                mLCPolSchema.setMainPolNo(main_ssrs.GetText(main_count, 1)); //主险保单号码；
                mLCPolSchema.setAppntName(pLCAppntIndSchema.getName()); //获得投保人的客户名称
                mLCPolSchema.setMakeDate(main_ssrs.GetText(main_count, 4)); //银行划款日期
                mLCPolSchema.setContNo(main_ssrs.GetText(main_count, 2)); //批次号码
                mLCPolSchema.setInsuredName(main_ssrs.GetText(main_count, 6)); //银行户名
                /*Lis5.3 upgrade set
                 mLCPolSchema.setBankAccNo(main_ssrs.GetText(main_count,5));//银行帐户
                 */
                mLCPolSet.add(mLCPolSchema);
            }
        }

        if (mLCPolSet.size() == 0)
        {
            mPremBankPubFun.buildError("submitData", "没有要进行打印的信息");
            return false;
        }
        mResult.clear();
        mResult.addElement(mLCPolSet);
        return true;
    }

    public boolean getInputData(VData tInputData)
    {
        strStartDate = (String) tInputData.get(0);
        System.out.println("strStartDate" + strStartDate);
        strEndDate = (String) tInputData.get(1);
        strAgentState = (String) tInputData.get(2);
        strPremType = (String) tInputData.get(3); //首期还是续期
        strFlag = (String) tInputData.get(4); //F or S
        strStation = (String) tInputData.get(5);

        System.out.println("strEndDate" + strEndDate);
        System.out.println("strPremType" + strPremType);
        System.out.println("strFlag" + strFlag);

        System.out.println("strComCode" + strComCode);
        System.out.println("strStation" + strStation);

        strStartDate = strStartDate.trim();
        strEndDate = strEndDate.trim();
        strPremType = strPremType.trim();
        strFlag = strFlag.trim();
        return true;
    }

    public static void main(String[] args)
    {
        String strStartDate = "2004-7-10"; //开始日期
        String strEndDate = "2004-7-15"; //结束日期
        String strAgentState = "1"; //业务员的状态(1为在职单，0为孤儿单)
        String strPremType = "X"; //首续期的标志
        String strFlag = "S"; //S or F(S为银行代收，F为银行代付)
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";

        VData tVData = new VData();
        tVData.addElement(strStartDate);
        tVData.addElement(strEndDate);
        tVData.addElement(strAgentState);
        tVData.addElement(strPremType);
        tVData.addElement(strFlag);
        tVData.addElement("86");
        tVData.addElement(tG);
        ShowXQPremSuccBL aShowXQPremSuccBL = new ShowXQPremSuccBL();
        aShowXQPremSuccBL.submitData(tVData, "SHOW");
    }
}
