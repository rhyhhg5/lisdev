package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LILRRecePremPrtBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        private VData mResult = new VData();
        //取得的时间
        private String mDay = "";
        //业务处理相关变量
        /** 全局数据 */
        private String mCurrentDate = PubFun.getCurrentDate();
        private String mCurrentTime = PubFun.getCurrentTime();
    private GlobalInput mGlobalInput = new GlobalInput();

    public LILRRecePremPrtBL() {
    }

    LILRRecePremPrtSchema tLILRRecePremPrtSchema = new LILRRecePremPrtSchema();
    LILRRecePremPrtDB tLILRRecePremPrtDB = new LILRRecePremPrtDB();
    LILRRecePremPrtSet tLILRRecePremPrtSet = new LILRRecePremPrtSet();
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
       
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //判断是否是月初
        if(!monthOfFirst())
        {
            CError tError = new CError();
            tError.moduleName = "LILRRecePremPrtBL";
            tError.functionName = "monthOfFirst";
            tError.errorMessage = "该日期不是月初，长险应收不用提取!";
            this.mErrors.addOneError(tError);
            System.out.println(this.mErrors.getLastError());
            return false;
        }
        //判断是否已经提过该批数
        if(existdata())
        {
            CError tError = new CError();
            tError.moduleName = "LILRRecePremPrtBL";
            tError.functionName = "existdata";
            tError.errorMessage = "该日期的长线计提数在LILRRecePremPrt表已经存在!";
            this.mErrors.addOneError(tError);
            System.out.println(this.mErrors.getLastError());
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        LILRRecePremPrtBLS mLILRRecePremPrtBLS = new LILRRecePremPrtBLS();
        if (!mLILRRecePremPrtBLS.submitData(mResult,"INSERT"))
        {
            this.mErrors.copyAllErrors(mLILRRecePremPrtBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LILRRecePremPrtBL";
            tError.functionName = "saveData";
            tError.errorMessage = "长线计提数据插入失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 在查询数据之前判断LILRRecePremPrt表是否已经存在该批数据
     * @return 
     */
    private boolean existdata()
    {
        String sql="select * from LILRRecePremPrt where chargedate='"+mDay+"' with ur";
        SSRS mSSRS = new SSRS();
        ExeSQL mExeSQL = new ExeSQL();
        mSSRS = mExeSQL.execSQL(sql);
        System.out.println("LILRRecePremPrt表已经存在记录数:"+mSSRS.getMaxRow());
        if(mSSRS.getMaxRow()>0){
            return true;
        }else{
            return false;
        }
        
    }
    /**
     * 在查询数据之前判断是否是月初，因为长险应收计提只在每月1号提数
     * @return
     */
    private boolean monthOfFirst()
    {   
        //String dateSql ="Select 1 from dual where month(date('"+PubFun.getCurrentDate()+"') - 1 days ) <> month(date('"+PubFun.getCurrentDate()+"')) with ur";
        //ExeSQL texeSQL = new ExeSQL();
        //String getDate = texeSQL.getOneValue(dateSql);
        String getDate=PubFun.getCurrentDate();
        if((getDate.substring(8,10).equals("01"))){
            return true;
        }
        return false;
    }
    /**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */

private boolean getInputData(VData cInputData)
{
    //全局变量
    mDay = (String) cInputData.get(0);
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    
    if (mGlobalInput == null)
    {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }

    return true;
}

    private boolean getPrintData()
    {
        String mSerialno ="";
        String SQL = "select case salechnl when '01' then  '个人直销' when '02' then '团险直销' when '03' then '中介' when  "
                     +
                "'04' then '银行代理' when '06' then '个销团' when '07' then '职团开拓'  end as Salechnl, "
                     + "contno as contno,'1' as conttype, sum(prem) as sumMoney,"+mGlobalInput.ManageCom+" as ManageCom, '"+mDay+"' as ChargeDate, "
                     + "paytodate as paytodate,payenddate as payenddate,payintv as payintv,'"+mGlobalInput.Operator+"' as operator,current date as makedate, "
                     + "current time as maketime,current date as modifydate,current time as modifytime "
                     + "from lcpol a where  month(date('"+mDay+"') - 1 days ) <> month(date('"+mDay+"'))   "
                     + "and a.PayToDate < (date('"+mDay+"') - 1 days)  "
                     + "and a.payenddate > (date('"+mDay+"') - 1 days)    "
                     + "and a.ContType in ('1')  "
                     + "and a.Payintv <> 0    "
                     + "and a.StateFlag = '1'  "
                     + "and a.prem <> 0   "
                     + "and managecom like '"+mGlobalInput.ManageCom+"%' "
                     + "and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod in ('L')) "
                     +
                "group by contno,salechnl,ManageCom,paytodate,payenddate,payintv  "
                     + "union all "
                     + "select case salechnl when '01' then  '个人直销' when '02' then '团险直销' when '03' then '中介' when  "
                     +
                "'04' then '银行代理' when '06' then '个销团' when '07' then '职团开拓'  end as salechnl, "
                     + "grpcontno as contno, '2' as conttypy,sum(prem) as summoney,"+mGlobalInput.ManageCom+" as ManageCom, '"+mDay+"' as ChargeDate, "
                     + "paytodate as paytodate,payenddate as payenddate,payintv as payintv,'"+mGlobalInput.Operator+"' as operator,current date as makedate, "
                     + "current time as maketime,current date as modifydate,current time as modifytime "
                     + "from lcgrppol a where  month(date('"+mDay+"') - 1 days ) <> month(date('"+mDay+"'))  "
                     + "and a.PayToDate < (date('"+mDay+"') - 1 days)  "
                     + "and a.payenddate > (date('"+mDay+"') - 1 days)  "
                     + "and a.Payintv <> 0   "
                     + "and a.StateFlag = '1'  "
                     + "and a.prem <> 0   "
                     + "and managecom like '"+mGlobalInput.ManageCom+"%' "
                     + "and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod in ('L')) "
                     +
                "group by grpcontno, salechnl,ManageCom,paytodate,payenddate,payintv with ur";

//        tLILRRecePremPrtSet = tLILRRecePremPrtDB.executeQuery(SQL);
          SSRS mSSRS = new SSRS();
          ExeSQL mExeSQL = new ExeSQL();
          mSSRS = mExeSQL.execSQL(SQL);

          for (int i=1;i<=mSSRS.getMaxRow();i++)
          {
              mSerialno = PubFun1.CreateMaxNo("YSSERIALNO", 20);
              LILRRecePremPrtSchema mLILRRecePremPrtSchema = new LILRRecePremPrtSchema();
              mLILRRecePremPrtSchema.setSerialNo(mSerialno);
              mLILRRecePremPrtSchema.setSaleChnl("02");
              mLILRRecePremPrtSchema.setContNo(mSSRS.GetText(i,2));
              mLILRRecePremPrtSchema.setConttype(mSSRS.GetText(i,3));
              mLILRRecePremPrtSchema.setSumMoney(mSSRS.GetText(i,4));
              mLILRRecePremPrtSchema.setManageCom(mSSRS.GetText(i,5));
              mLILRRecePremPrtSchema.setChargeDate(mSSRS.GetText(i,6));
               mLILRRecePremPrtSchema.setPayToDate(mSSRS.GetText(i,7));
              mLILRRecePremPrtSchema.setpayenddate(mSSRS.GetText(i,8));
              mLILRRecePremPrtSchema.setPayintv(mSSRS.GetText(i,9));
              mLILRRecePremPrtSchema.setOperator(mSSRS.GetText(i,10));
              mLILRRecePremPrtSchema.setMakeDate(mCurrentDate);
              mLILRRecePremPrtSchema.setMakeTime(mCurrentTime);
              mLILRRecePremPrtSchema.setModifyDate(mCurrentDate);
              mLILRRecePremPrtSchema.setModifyTime(mCurrentTime);
              tLILRRecePremPrtSet.add(mLILRRecePremPrtSchema);
          }
          mResult.add(tLILRRecePremPrtSet);
   return true;
    }
    private void buildError(String szFunc, String szErrMsg)
   {
    CError cError = new CError();

    cError.moduleName = "LILRRecePremPrtBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
   }
   public VData getResult()
{
    return this.mResult;
}
public static void main(String[] args)
{
    LILRRecePremPrtBL mLILRRecePremPrtBL = new LILRRecePremPrtBL();
    GlobalInput mG = new GlobalInput();
    String StartDate = "2011-01-01";
//    String mManageCom = "86110000";
    mG.ManageCom="86";
    mG.Operator="000";
    VData mVData = new VData();
    mVData.add(StartDate);
    mVData.add(mG);
    mLILRRecePremPrtBL.submitData(mVData,"PRINT");
 }


}
