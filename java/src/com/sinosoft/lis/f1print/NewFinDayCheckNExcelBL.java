package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys yh
 * @version 1.0
 */

import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class NewFinDayCheckNExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();


	// 取得的时间和操作参数
	private String mDay[] = null;
	private String strOperation="";
	private String tOutXmlPath = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public NewFinDayCheckNExcelBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		strOperation=cOperate;
		
		if (!cOperate.equals("SS") && !cOperate.equals("SF") && !cOperate.equals("SXF")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}		
        if (!dealPrint()) {
			return false;
		}
		return true;
	}
	/** 处理日结单打印函数 */
    private boolean dealPrint(){
    	if(strOperation.equals("SS")){
    		if(!dealPrint_SS()){
    			return false;
    		}
    	}else if(strOperation.equals("SF")){
    		if(!dealPrint_SF()){
    			return false;
    		}
    	}else if(strOperation.equals("SXF")){
    		if(!dealPrint_SXF()){
    			return false;
    		}
    	}else{
    		buildError("dealPrint","错误的操作类型");
    	}
    	return true;
    }
	/** X1实收日结单打印函数-Excel格式 */
    private boolean dealPrint_SS(){
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	
    	String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
    	tSSRS = tExeSQL.execSQL(nsql);
        String tmanageCom = tSSRS.GetText(1, 1);
        
        String[][] mToExcel = new String[50000][9];
        mToExcel[0][0] = "财务收费日结单（方式）";
        mToExcel[1][0] = "统计日期："+mDay[0]+"至"+mDay[1];
        mToExcel[2][0] = "统计机构："+tmanageCom;
        mToExcel[4][0] = "收款方式";
        mToExcel[4][1] = "号码类型";
        mToExcel[4][2] = "号码";
        mToExcel[4][3] = "收费时间";
        mToExcel[4][4] = "发票号码";
        mToExcel[4][5] = "印刷号";
        mToExcel[4][6] = "成本中心";
        mToExcel[4][7] = "金额";
        mToExcel[4][8] = "件数";
        
        int no = 5;//初始化打印行数
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath = "E:/lisdev/ui/";
		//日结单按收费方式打印 收费明细SQL
		String[] getType = new String[] {
  				"X1SSMX"	 // 按方式打印 收费明细SQL
  		 };
		for(int i=0;i<getType.length;i++){
			String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", getType[i]);
			tSSRS = tExeSQL.execSQL(msql);
			int mMaxRow = tSSRS.getMaxRow();
			if(mMaxRow>0){
				for(int row=1; row<=tSSRS.getMaxRow(); row++){
					for(int col=1; col<=tSSRS.getMaxCol(); col++){
						if(col==8){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}else{
							mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
						}
					}
				}
				no = no+mMaxRow;
			}
			
			//收费日结单 明细汇总SQL
			String zsql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", getType[i]+"Z");
			tSSRS = tExeSQL.execSQL(zsql);
			int zMaxRow = tSSRS.getMaxRow();
			System.out.println("行数："+zMaxRow+";列数："+tSSRS.getMaxCol());
			if(zMaxRow>0){
				for(int row=1; row<=tSSRS.getMaxRow(); row++){
					for(int col=1; col<=tSSRS.getMaxCol(); col++){
						if(col==1){
							mToExcel[no+row-1][0] = tSSRS.GetText(row, col);
						}
						if(col==2){
							mToExcel[no+row-1][7] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						if(col==3){
							mToExcel[no+row-1][8] = tSSRS.GetText(row, col);
						}
					}
				}
				no = no+zMaxRow;
			}
		}
		
		//汇总SQL
		String hsql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "X1SSHJ");
		tSSRS = tExeSQL.execSQL(hsql);
		int hmaxRow = tSSRS.getMaxRow();
		if(tSSRS.getMaxRow()>0){
			mToExcel[no+hmaxRow-1][0] = "合计：";
			mToExcel[no+hmaxRow-1][7] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(hmaxRow, 1)));
			mToExcel[no+hmaxRow-1][8] = tSSRS.GetText(hmaxRow, 2);
			no=no+hmaxRow+1;
		}else{
			mToExcel[no+hmaxRow][0] = "合计：";
			mToExcel[no+hmaxRow][7] = "0";
			mToExcel[no+hmaxRow][8] = "0";
			no=no+hmaxRow+2;
		}
		
		System.out.println("X1-SS打印行数："+no);
		//添加制表员和审核员
		mToExcel[no][0] = "制表员";
		mToExcel[no][2] = "审核员";
		try
        {
            System.out.println("X1-SS路径信息："+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X1-SS文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }	
    	return true;
    }
    
    /** X2实收日结单打印函数-Excel格式 */
    private boolean dealPrint_SF(){
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	
    	String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
    	tSSRS = tExeSQL.execSQL(nsql);
        String tmanageCom = tSSRS.GetText(1, 1);
        
        String[][] mToExcel = new String[50000][6];
        mToExcel[0][0] = "财务付费日结单";
        mToExcel[1][0] = "统计日期："+mDay[0]+"至"+mDay[1];
        mToExcel[2][0] = "统计机构："+tmanageCom;
        mToExcel[4][0] = "付款方式";
        mToExcel[4][1] = "给付号码";
        mToExcel[4][2] = "号码类型";
        mToExcel[4][3] = "号码";
        mToExcel[4][4] = "金额";
        mToExcel[4][5] = "件数";
        
        int no = 5;//初始化打印行数
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath = "E:/lisdev/ui/";
    	//X2实付日结单-实付明细打印 
		String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "X2SF");
    	tSSRS = tExeSQL.execSQL(msql);
    	int mMaxRow = tSSRS.getMaxRow();
    	if(mMaxRow>0){
    		for(int row=1; row<=tSSRS.getMaxRow(); row++){
        		for(int col=1;col<=tSSRS.getMaxCol();col++){
        			mToExcel[no+row-1][0] = tSSRS.GetText(row, 2);
        			mToExcel[no+row-1][1] = tSSRS.GetText(row, 3);
        			mToExcel[no+row-1][2] = tSSRS.GetText(row, 1);
        			mToExcel[no+row-1][3] = tSSRS.GetText(row, 6);
        			mToExcel[no+row-1][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 4)));
        			mToExcel[no+row-1][5] = tSSRS.GetText(row, 5);
        		}
        	}
        	no=no+mMaxRow;
    	}
    	
    	//X2实付日结单-实付汇总打印
    	String zsql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "X2SFZ");
    	tSSRS = tExeSQL.execSQL(zsql);
    	int zMaxRow = tSSRS.getMaxRow();
    	if(zMaxRow>0){
    		for(int row=1; row<=tSSRS.getMaxRow(); row++){
        		for(int col=1;col<=tSSRS.getMaxCol();col++){
        			mToExcel[no+row-1][0] = tSSRS.GetText(row, 1);
        			mToExcel[no+row-1][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
        			mToExcel[no+row-1][5] = tSSRS.GetText(row, 2);
        		}
        	}
        	no=no+zMaxRow;
    	}
    	
    	//X2实付日结单-实付合计
    	String hsql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "X2SFHJ");
    	tSSRS = tExeSQL.execSQL(hsql);
    	int hMaxRow = tSSRS.getMaxRow();
    	if(hMaxRow>0){
    		for(int row=1; row<=tSSRS.getMaxRow(); row++){
        		for(int col=1;col<=tSSRS.getMaxCol();col++){
        			mToExcel[no+row-1][0] = "合计：";
        			mToExcel[no+row-1][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 1)));
        			mToExcel[no+row-1][5] = tSSRS.GetText(row, 2);
        		}
        	}	
    		no=no+hMaxRow+1;
    	}else{
    		mToExcel[no+hMaxRow][0] = "合计：";
    		mToExcel[no+hMaxRow][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(hMaxRow, 1)));
			mToExcel[no+hMaxRow][5] = tSSRS.GetText(hMaxRow, 2);
			no=no+hMaxRow+2;
    	}
    	
    	System.out.println("X2-SF打印行数："+no);
		//添加制表员和审核员
		mToExcel[no][0] = "制表员";
		mToExcel[no][2] = "审核员";
		try
        {
            System.out.println("X2-SF路径信息："+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X2-SF文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }	
    	return true;
    }
    
    /** X9实收日结单打印函数-Excel格式 */
    private boolean dealPrint_SXF(){
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	
    	String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
    	tSSRS = tExeSQL.execSQL(nsql);
        String tmanageCom = tSSRS.GetText(1, 1);
        
        String[][] mToExcel = new String[50000][10];
        mToExcel[0][0] = "手续费日结单";
        mToExcel[1][0] = "统计日期："+mDay[0]+"至"+mDay[1];
        mToExcel[2][0] = "统计机构："+tmanageCom;
        mToExcel[4][0] = "中介机构";
        mToExcel[4][1] = "保单号码";
        mToExcel[4][2] = "成本中心";
        mToExcel[4][3] = "结算号";
        mToExcel[4][4] = "险种代码";
        mToExcel[4][5] = "投保人";
        mToExcel[4][6] = "保费收入";
        mToExcel[4][7] = "手续费率";
        mToExcel[4][8] = "手续费金额";
        mToExcel[4][9] = "机构代码";
        
        int no = 5;//初始化打印行数
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath = "E:/lisdev/ui/";
		
		//X9手续费日结单-明细
		String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "SXFDSF");
    	tSSRS = tExeSQL.execSQL(msql);
    	int mMaxRow = tSSRS.getMaxRow();
    	if(mMaxRow>0){
    		for(int row=1; row<=tSSRS.getMaxRow(); row++){
        		for(int col=1; col<=tSSRS.getMaxCol();col++){
        			if(col==7 || col==8 || col==9){
        				mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
        			}else{
        				mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
        			}
        		}
        	}
        	no=no+mMaxRow+1;
    	}
    	
    	//X9手续费日结单-汇总
    	String zsql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/NewFinDayCheckNSql.xml", "SXFZBF");
    	tSSRS = tExeSQL.execSQL(zsql);
    	int zMaxRow = tSSRS.getMaxRow();
    	
    	if(zMaxRow >0){
    		mToExcel[no+zMaxRow-1][0] = "合计：";
    		mToExcel[no+zMaxRow-1][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
    		no=no+zMaxRow+1;
    	}else{
    		mToExcel[no+zMaxRow][0] = "合计：";
    		mToExcel[no+zMaxRow][1] = "0";
    		no=no+zMaxRow+2;
    	}
    	
    	System.out.println("X9-SXF打印行数："+no);
		//添加制表员和审核员
		mToExcel[no][0] = "制表员";
		mToExcel[no][2] = "审核员";
		try
        {
            System.out.println("X9-SXF路径信息："+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X9-SXF文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }	
    	return true;
    }
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		
	    if (mDay == null)
        {
            buildError("getInputData", "没有得到日结单打印起始日期！");
            return false;
        }
    	System.out.println("打印起始日期：" + mDay[0]+";打印终止日期：" + mDay[1]);
    	
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到公共信息！");
            return false;
        }
        if(tOutXmlPath.equals("") ||  tOutXmlPath==null){
        	buildError("getInputData", "没有得到打印路径信息！");
            return false;
        }
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "NewFinDayCheckNExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
}

