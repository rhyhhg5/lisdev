package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author 
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NewPreRecPrintBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;

	private String mSql = null;

	private String mOutXmlPath = null;

	private String StartDate = "";

	private String EndDate = "";
	
	private String type = "";

	private String mCurDate = PubFun.getCurrentDate();
	
	private String[][] mToExcel = null;
	
	private String mMail = "";

	SSRS tSSRS = new SSRS();

	public NewPreRecPrintBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		
		if(!createFile()){
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	public boolean dealData() {
		System.out.println("BL->dealDate()");
		String dSql = "";
		String rSql = "";
		if (StartDate != null && !StartDate.equals("")) {
			dSql = " and gc.cvalidate >='" + StartDate + "'";
		}
		if(!"".equals(mMail) && mMail != null){
        	rSql = " and gc.Markettype not in ('1','9') ";
        }
		ExeSQL manExeSQL = new ExeSQL();
		String manSql = "select distinct managecom from "
				+ ("T".equals(type) ? "lbgrpcont" : "lcgrpcont") + " gc "
				+ "where gc.payintv = -1 and gc.managecom like '"
				+ mGI.ManageCom
				+ "%' and "
				+ "exists (select 1 from LCGrpPayPlan gpp where gpp.prtno = gc.prtno) "
				+ "and gc.cvalidate <= '" + EndDate + "' " + dSql + rSql
				+ " and Gc.signdate is not null "
				+ " order by managecom";
		SSRS manSSRS = manExeSQL.execSQL(manSql);
		if (manExeSQL.mErrors.needDealError()) {
			System.out.println(manExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		if (manSSRS.getMaxRow() == 0) {

			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有符合条件的数据，请重新输入条件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mToExcel = new String[10000][30];
		mToExcel[0][0] = "中国人民健康保险股份有限公司";
		mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
				+ "年"
				+ EndDate.substring(EndDate.indexOf('-') + 1,
						EndDate.lastIndexOf('-')) + "月应收保费统计表";
		mToExcel[2][0] = "编制单位：" + getName();
		mToExcel[2][14] = "金额单位：元";

		mToExcel[3][0] = "序号";
		mToExcel[3][1] = "机构代码";
		mToExcel[3][2] = "机构名称";
		mToExcel[3][3] = "险类";
		mToExcel[3][4] = "项目名称";
		mToExcel[3][5] = "险种代码";
		mToExcel[3][6] = "险种名称";
		mToExcel[3][7] = "投保单号";
		mToExcel[3][8] = "保单号";
		mToExcel[3][9] = "投保单位名称/个人姓名";
		mToExcel[3][10] = "业务序号";
		mToExcel[3][11] = "生效日期";
		mToExcel[3][12] = "终止日期";
		mToExcel[3][13] = "共保比例";
		mToExcel[3][14] = "保费收入金额";
		mToExcel[3][15] = "实收保费金额";
		mToExcel[3][16] = "应收保费金额";
		mToExcel[3][17] = "未逾期应收保费余额";
		mToExcel[3][18] = "逾期在三个月以内应收保费余额";
		mToExcel[3][19] = "逾期超过三个月但在六个月内应收保费余额";
		mToExcel[3][20] = "逾期超过六个月但在一年内应收保费余额";
		mToExcel[3][21] = "逾期超过一年应收保费余额";
		mToExcel[3][22] = "理赔金额";
		mToExcel[3][23] = "市场类型";
		mToExcel[3][24] = "业务员";        
		mToExcel[3][25] = "备注";
		
		
		String polCom = manSSRS.GetText(1, 1).substring(0, 4) + "0000";
		int printNum = 3;
		//总合计
		double sumsFinMoney = 0;// 总合计实收保费
		double sumaFinMoney = 0;// 总合计应收保费		
		double sumFinMoney1 = 0;// 总合计未逾期
		double sumFinMoney3 = 0;// 总合计逾期-3
		double sumFinMoney6 = 0;// 总合计3-6
		double sumFinMoney12 = 0;// 总合计6-12
		double sumFinMoney1y = 0;// 总合计>12
		double sumpFinMoney = 0;   //总合计赔款
		//分公司
		double fsFinMoney = 0;//分公司实收保费金额
		double faFinMoney = 0;//分公司应收保费金额
		double fFinMoney1 = 0;//分公司未逾期应收保费余额
		double fFinMoney3 = 0;//分公司逾期在三个月以内应收保费
		double fFinMoney6 = 0;//分公司逾期三个月至六个月应收保费
		double fFinMoney12 = 0;//分公司逾期六个月至一年应收保费
		double fFinMoney1y = 0;//分公司逾期一年以上应收保费
		double fpFinMoney = 0; //分公司赔款

		for (int comnum = 1; comnum <= manSSRS.getMaxRow(); comnum++) {
			ExeSQL tExeSQL = new ExeSQL();
			mSql = "select * from ("
					+ "select rownumber() over(),gc.managecom as 机构代码,"
					+ "(select name from ldcom where comcode=gc.managecom) as 机构名称,"				
					+ "(select codename from ldcode where codetype='risktype' and code=substr(gp.riskcode,1,4)) as 险类,"
					+ "(select ProjectName from LCGrpContSub where PrtNo = Gc.PrtNo) as 项目名称,"
					+ " gp.riskcode as 险种代码,"	
					+ "(select RiskName from LMRisk where rtrim(RiskCode) = gp.riskcode) as 险种名称,gc.prtno,gc.grpcontno,gc.grpname,'团险渠道'"
					+ ",gc.cvalidate,gc.cinvalidate, "
					+ "( case when gc.CoInsuranceFlag<>'1' then 1 else (select 1-sum(rate) From lccoinsuranceparam  where Grpcontno=Gc.Grpcontno) end ) as 共保比例  "                
					+ ",'' "
					//实收保费
					+ ", (select sum(sumduepaymoney) from ljapaygrp apg where apg.grpcontno = gc.grpcontno and apg.PayType <> 'GB' and apg.riskcode=gp.riskcode and exists (select 1 from ljapay ap where ap.incomeno=apg.grpcontno and ap.payno = apg.payno) )"
					//应收保费
					+ ",(case when (select count(1) from lcgrppayplandetail where prtno = gc.prtno and riskcode = gp.riskcode and state='2')=0 then 0 else ((select sum(prem) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and plancode != '1')  "
		            + "- (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where  d.prtno = gc.prtno and d.riskcode = gp.riskcode)) end ) as totalshouldpay"
					//未逾期应收保费
					+ ", case when current date - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end  >= 0 "
					+ "then  (select NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and paytodate > current date)"
					+ "else  (select   NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and plancode != '1') - (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where  d.prtno = gc.prtno and d.riskcode = gp.riskcode) end "
					//逾期在3个月以内的应收保费余额
					+ ", case when current date - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end  > 0 "
					+ "and current date -3 month -case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end  <= 0 "
					+ "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and plancode != '1' and paytodate < current date) - (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where  d.prtno = gc.prtno and d.riskcode = gp.riskcode) "
					+ "when current date -3 month -case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end > 0  "
					+ "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and paytodate < current date and paytodate >= current date - 3 month ) else 0 end "
					//逾期超过3个月但在6个月内的应收保费余额
					+ ", case when current date -3 month -case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end  >0 "
                    + " and current date -6 month - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end  <= 0 "
                    + "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode and plancode != '1' and paytodate < current date - 3 month ) - (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where d.prtno = gc.prtno and d.riskcode = gp.riskcode) "
					+ "when current date -6 month - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end > 0 "
					+ "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and paytodate < current date - 3 month and paytodate >= current date - 6 month ) else 0 end "
					//逾期超过6个月但在1年内的应收保费余额
					+ ", case when current date -6 month -case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno) else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end > 0 "
                    + " and current date -12 month - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end <= 0 "
                    + "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode and plancode != '1'and paytodate <= current date - 6 month) - (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where d.prtno = gc.prtno and d.riskcode = gp.riskcode)  "
					+ "when current date -6 month - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end > 0 "
					+ "then (select  NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode  and paytodate < current date - 6 month and paytodate >= current date - 12 month ) else 0 end "
					//逾期超过1年的应收保费余额
					+ ", case when  current date -12 month - case when (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  is not null   then (select  max(paytodate) from  LcGrpPayDueDetail  where  prtno = gc.prtno)  else  (select min(paytodate) from lcgrppayplan where prtno = gc.prtno) end > 0 "
                    + "then (select NVL(sum(prem),0) from lcgrppayplandetail p where p.prtno = gc.prtno and p.riskcode=gp.riskcode and plancode != '1' and paytodate <= current date - 12 month )"
					+ "- (select NVL(sum(prem),0) From db2inst1.LcGrpPayDuedetail d where  d.prtno = gc.prtno and d.riskcode = gp.riskcode)  else 0 end  "
					+ ",(Select NVL(Sum(Pay),0) From Ljagetclaim Where Grpcontno = Gc.Grpcontno And Riskcode = Gp.Riskcode And Othernotype In ('5', 'C', 'F'))"
					+ ",(Select Codename From Ldcode Where Codetype = 'markettype' And Code = Gc.Markettype),getUniteCode(gc.agentcode)"					
					+ "from "
					+ ("T".equals(type) ? "lbgrpcont" : "lcgrpcont")
					+ " gc "
					+ "right join "
					+ ("T".equals(type) ? "lbgrppol" : "lcgrppol")
					+ " gp "
					+ "on gc.grpcontno=gp.grpcontno "
					+ "where gc.payintv = -1 and "
					+ ("T".equals(type) ? "" : "Gc.signdate is not null and ")
					+ "exists (select 1 from LCGrpPayPlan gpp where gpp.prtno = gc.prtno) "
					+ "and gc.managecom='"
					+ manSSRS.GetText(comnum, 1)
					+ "' and gc.cvalidate <= '"
					+ EndDate
					+ "' "
					+ dSql
					+ rSql
					+ " order by gc.cvalidate,gc.managecom,gc.grpcontno,gp.riskcode) "
					+ ("T".equals(type) ? "" : " where totalshouldpay <> 0" )
					+ " with ur";
			System.out.println(mSql);
			tSSRS = tExeSQL.execSQL(mSql);
			if(tSSRS.getMaxRow() != 0){			
				System.out.println(polCom + "--"
						+ tSSRS.GetText(tSSRS.getMaxRow(), 2));
				if (!polCom.equals(tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0,
						4)
						+ "0000")) {
					mToExcel[printNum + 1][0] = "分公司合计：";
					mToExcel[printNum + 1][14] = PubFun.format(fsFinMoney+ faFinMoney);
					mToExcel[printNum + 1][15] = PubFun.format(fsFinMoney);
					mToExcel[printNum + 1][16] = PubFun.format(faFinMoney);
					mToExcel[printNum + 1][17] = PubFun.format(fFinMoney1);
					mToExcel[printNum + 1][18] = PubFun.format(fFinMoney3);
					mToExcel[printNum + 1][19] = PubFun.format(fFinMoney6);
					mToExcel[printNum + 1][20] = PubFun.format(fFinMoney12);
					mToExcel[printNum + 1][21] = PubFun.format(fFinMoney1y);
					mToExcel[printNum + 1][22] = PubFun.format(fpFinMoney);
					fsFinMoney = 0;//分公司实收保费金额
					faFinMoney = 0;//分公司应收保费金额
					fFinMoney1 = 0;//分公司未逾期应收保费余额
					fFinMoney3 = 0;//分公司逾期在三个月以内应收保费
					fFinMoney6 = 0;//分公司逾期三个月至六个月应收保费
					fFinMoney12 = 0;//分公司逾期六个月至一年应收保费
					fFinMoney1y = 0;//分公司逾期一年以上应收保费
					fpFinMoney = 0; //分公司赔款
					printNum = printNum + 2;
				}
				polCom = tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0, 4)
						+ "0000";

				double ssFinMoney = 0;// 支公司合计实收保费
				double saFinMoney = 0;// 支公司合计应收保费
				double sFinMoney1 = 0;//未逾期应收保费余额
				double sFinMoney3 = 0;//逾期在3个月以内的应收保费余额
				double sFinMoney6 = 0;//逾期超过3个月但在6个月内的应收保费余额
				double sFinMoney12 = 0;//逾期超过6个月但在1年内的应收保费余额
				double sFinMoney1y = 0;//逾期超过1年的应收保费余额
				double spFinMoney = 0; //支公司赔款
				int no = 1;
				for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
					for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
						if(col < 15 || col > 23){
							if (tSSRS.GetText(row, col).equals("null")) {
								mToExcel[row + printNum][col - 1] = "";
							}else{
								mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
							} 
						}else{
							double rate= Double.parseDouble(tSSRS.GetText(row,14));
							double money=0.0;
							 if (tSSRS.GetText(row, col).equals("null")){
		                        	mToExcel[row + printNum][col - 1] = "";
		                        }else if(col == 15){
		                        	if(!tSSRS.GetText(row,16).equals("null")&&!tSSRS.GetText(row,17).equals("null")){
		                        		money=Double.parseDouble(tSSRS.GetText(row,16))*rate + Double.parseDouble(tSSRS.GetText(row,17))*rate;	
		                        	}else if(tSSRS.GetText(row,16).equals("null")&&!tSSRS.GetText(row,17).equals("null")){
		                        		money=Double.parseDouble(tSSRS.GetText(row,17))*rate;
		                        	}else if(!tSSRS.GetText(row,16).equals("null")&&tSSRS.GetText(row,17).equals("null")){
		                        		money=Double.parseDouble(tSSRS.GetText(row,16))*rate;
		                        	}
		                        	 mToExcel[row + printNum][col - 1] =PubFun.format(money); 
		                        }else if(col == 23){
									money=Double.parseDouble(tSSRS.GetText(row, col));
									 mToExcel[row + printNum][col - 1] =PubFun.format(money); 
								}else{
									money=Double.parseDouble(tSSRS.GetText(row, col))*rate; 
									 mToExcel[row + printNum][col - 1] =PubFun.format(money); 
								}
							//支公司
							if (col == 16) {ssFinMoney = ssFinMoney+money;}
                        	if (col == 17) {saFinMoney = saFinMoney+money;}
                        	if (col == 18) {sFinMoney1 = sFinMoney1+money;}
                        	if (col == 19) {sFinMoney3 = sFinMoney3+money;}
                        	if (col == 20) {sFinMoney6 = sFinMoney6+money;}
                        	if (col == 21) {sFinMoney12 = sFinMoney12+money;}
                        	if (col == 22) {sFinMoney1y = sFinMoney1y+money;}
                        	if (col == 23) {spFinMoney = spFinMoney+money;}
                        	
                        	//分公司
							if (col == 16) {fsFinMoney = fsFinMoney+money;}
                        	if (col == 17) {faFinMoney = faFinMoney+money;}
                        	if (col == 18) {fFinMoney1 = fFinMoney1+money;}
                        	if (col == 19) {fFinMoney3 = fFinMoney3+money;}
                        	if (col == 20) {fFinMoney6 = fFinMoney6+money;}
                        	if (col == 21) {fFinMoney12 = fFinMoney12+money;}
                        	if (col == 22) {fFinMoney1y = fFinMoney1y+money;}
                        	if (col == 23) {fpFinMoney = fpFinMoney+money;}	
                        	//总合计
							if (col == 16) {sumsFinMoney = sumsFinMoney+money;}
                        	if (col == 17) {sumaFinMoney = sumaFinMoney+money;}
                        	if (col == 18) {sumFinMoney1 = sumFinMoney1+money;}
                        	if (col == 19) {sumFinMoney3 = sumFinMoney3+money;}
                        	if (col == 20) {sumFinMoney6 = sumFinMoney6+money;}
                        	if (col == 21) {sumFinMoney12 = sumFinMoney12+money;}
                        	if (col == 22) {sumFinMoney1y = sumFinMoney1y+money;}
                        	if (col == 23) {sumpFinMoney = sumpFinMoney+money;}	
						}
					}
					mToExcel[row + printNum][0] = no + "";
					no++;
				}
				mToExcel[printNum + tSSRS.getMaxRow() + 1][0] = "支公司合计：";
				mToExcel[printNum + tSSRS.getMaxRow() + 1][14] = PubFun.format(ssFinMoney + saFinMoney);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][15] = PubFun.format(ssFinMoney);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][16] = PubFun.format(saFinMoney);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][17] = PubFun.format(sFinMoney1);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][18] = PubFun.format(sFinMoney3);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][19] = PubFun.format(sFinMoney6);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][20] = PubFun.format(sFinMoney12);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][21] = PubFun.format(sFinMoney1y);
				mToExcel[printNum + tSSRS.getMaxRow() + 1][22] = PubFun.format(spFinMoney);
				printNum = printNum + tSSRS.getMaxRow() + 1;
				tSSRS.Clear();
				
			}
			
		}
		mToExcel[printNum + 1][0] = "分公司合计：";
		mToExcel[printNum + 1][14] = PubFun.format(faFinMoney + fsFinMoney);
		mToExcel[printNum + 1][15] = PubFun.format(fsFinMoney);
		mToExcel[printNum + 1][16] = PubFun.format(faFinMoney);
		mToExcel[printNum + 1][17] = PubFun.format(fFinMoney1);
		mToExcel[printNum + 1][18] = PubFun.format(fFinMoney3);
		mToExcel[printNum + 1][19] = PubFun.format(fFinMoney6);
		mToExcel[printNum + 1][20] = PubFun.format(fFinMoney12);
		mToExcel[printNum + 1][21] = PubFun.format(fFinMoney1y);
		mToExcel[printNum + 1][22] = PubFun.format(fpFinMoney);
		mToExcel[printNum + 3][0] = "总合计：";
		mToExcel[printNum + 3][14] = PubFun.format(sumsFinMoney + sumaFinMoney);
		mToExcel[printNum + 3][15] = PubFun.format(sumsFinMoney);
		mToExcel[printNum + 3][16] = PubFun.format(sumaFinMoney);
		mToExcel[printNum + 3][17] = PubFun.format(sumFinMoney1);
		mToExcel[printNum + 3][18] = PubFun.format(sumFinMoney3);
		mToExcel[printNum + 3][19] = PubFun.format(sumFinMoney6);
		mToExcel[printNum + 3][20] = PubFun.format(sumFinMoney12);
		mToExcel[printNum + 3][21] = PubFun.format(sumFinMoney1y);
		mToExcel[printNum + 3][22] = PubFun.format(sumpFinMoney);
		mToExcel[printNum + 5][0] = "制表";
		mToExcel[printNum + 5][14] = "日期：" + mCurDate;
		return true;
	}
	
	private boolean createFile(){
		
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}
	
	public String[][] getMToExcel() {
		return mToExcel;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	public boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);
		System.out.println(mGI.ManageCom);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		StartDate = (String) tf.getValueByName("StartDate");
		EndDate = (String) tf.getValueByName("EndDate");
		type = (String) tf.getValueByName("type");
		mMail = (String) tf.getValueByName("toMail");
		
		System.out.println("type ----" + type);
		System.out.println("mMail ----" + mMail);
		
		if (EndDate == null || mOutXmlPath == null) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}

	private String getName() {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86110000";
		tG.Operator = "cwad";
		tG.ComCode = "86110000";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "F:/a.xls");
		tTransferData.setNameAndValue("EndDate", "2014-3-12");
		tTransferData.setNameAndValue("StartDate", "2016-01-01");
		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);
		NewPreRecPrintUI tPreRecPrintUI = new NewPreRecPrintUI();
		if (!tPreRecPrintUI.submitData(vData, "")) {
			System.out.print("失败！");
		}
	}
}
