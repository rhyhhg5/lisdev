package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author guoly
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class HealthAchiveBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();

    private LHCustomHealthStatusSchema mLHCustomHealthStatusSchema = new
            LHCustomHealthStatusSchema();
    private LHCustomHealthStatusSet mLHCustomHealthStatusSet = new
            LHCustomHealthStatusSet();
    private LHCustomHealthStatusDB mLHCustomHealthStatusDB = new
            LHCustomHealthStatusDB();
    private TransferData mTransferData = new TransferData();
    private LHCustomGymSet mLHCustomGymSet = new LHCustomGymSet();
    private LHCustomFamilyDiseasSet mLHCustomFamilyDiseasSet = new
            LHCustomFamilyDiseasSet();
    private LHCustomInHospitalSchema mLHCustomInHospitalSchema = new
            LHCustomInHospitalSchema();
    private LHDiagnoSet mLHDiagnoSet = new LHDiagnoSet();
    private LHCustomTestSet mLHCustomTestSet = new LHCustomTestSet();
    private LHCustomOPSSet mLHCustomOPSSet = new LHCustomOPSSet();
    private LHCustomOtherCureSet mLHCustomOtherCureSet = new
            LHCustomOtherCureSet();
    private LHFeeInfoSet mLHFeeInfoSet = new LHFeeInfoSet();

    public HealthAchiveBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLDPersonSchema.setSchema((LDPersonSchema)
                                       cInputData.getObjectByObjectName(
                                               "LDPersonSchema", 0));
        System.out.println(mLDPersonSchema.getCustomerNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mLDPersonSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLDPersonSchema.getCustomerNo() == null) {
            buildError("getInputData", "没有得到足够的信息:客户号不能为空！");
            return false;
        }
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "HealthAchiveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String MStartDate = (String) mTransferData.getValueByName("Begin");
        String MEndDate = (String) mTransferData.getValueByName("End");

        //得到客户基本信息
        String aCustomerNo = "";
        String aCustomerName = "";
        String aSex = "";
        String aBirthDay = "";
        String aGrpName = "";
       // String aCorp="";//登陆公司

        //客户基本健康信息
        String aStature = "";//身高
        String aAvoirdupois = "";//体重
        String aAvoirindex = "";//体重指数
        String aBloodPressHigh = "";//血压舒张压（mmHg）
        String aBloodPressLow= "";//血压收缩压（mmHg）
        //String aBlood = "";//血压
        String aNervation= "";//脉压差

        aCustomerNo = mLDPersonSchema.getCustomerNo();
        LDPersonDB mLDPersonDB = new LDPersonDB();
        mLDPersonDB.setCustomerNo(aCustomerNo);

        if (!mLDPersonDB.getInfo()) {
            System.out.println("--------DB_GetInfo_Fail--------");
        }
        aCustomerNo = mLDPersonDB.getCustomerNo();
        aCustomerName = mLDPersonDB.getName();
        aSex = mLDPersonDB.getSex();
        aBirthDay = mLDPersonDB.getBirthday();
        aGrpName = mLDPersonDB.getGrpName();
        System.out.println("aCustomerNo:" + aCustomerNo + "---aCustomerName:" +
                           aCustomerName + "---aSex:" + aSex + "---aBirthDay" +
                          aBirthDay+"aGrpName:"+aGrpName);
        //基本健康信息
        try {

                String sqlStature ="  select a.TestResult from LHCustomTest a where  a.MedicaItemCode='110500009'"
                       +" and a.customerno = '" + aCustomerNo + "' and  "
+" a.testdate =(select max(testdate) from LHCustomTest where  customerno = '" + aCustomerNo + "' and  medicaitemcode = '110500009')"
                       ;
                aStature =  new ExeSQL().getOneValue(sqlStature); //身高
                String sqlAvoirdupois ="  select a.TestResult from LHCustomTest a where  a.MedicaItemCode='110500010'"
                                      +" and a.customerno = '" + aCustomerNo + "' and  "
+" a.testdate =(select max(testdate) from LHCustomTest where  customerno = '" + aCustomerNo + "' and  medicaitemcode = '110500010')"
                                      ;
                aAvoirdupois =  new ExeSQL().getOneValue(sqlAvoirdupois); //体重
                String sqlAvoirindex ="  select a.TestResult from LHCustomTest a where  a.MedicaItemCode='110500014'"
                                      +" and a.customerno = '" + aCustomerNo + "' and  "
+" a.testdate =(select max(testdate) from LHCustomTest where  customerno = '" + aCustomerNo + "' and  medicaitemcode = '110500014')"
                                      ;
                aAvoirindex =  new ExeSQL().getOneValue(sqlAvoirindex); //体重指数
                String sqlBloodPressHigh ="  select a.TestResult from LHCustomTest a where  a.MedicaItemCode='110500011'"
                                      +" and a.customerno = '" + aCustomerNo + "' and  "
+" a.testdate =(select max(testdate) from LHCustomTest where  customerno = '" + aCustomerNo + "' and  medicaitemcode = '110500011')"
                                      ;
                aBloodPressHigh =  new ExeSQL().getOneValue(sqlBloodPressHigh); //收缩压
                String sqlBloodPressLow ="  select a.TestResult from LHCustomTest a where  a.MedicaItemCode='110500012'"
                                      +" and a.customerno = '" + aCustomerNo + "' and  "
+" a.testdate =(select max(testdate) from LHCustomTest where  customerno = '" + aCustomerNo + "' and  medicaitemcode = '110500012')"
                                      ;
                aBloodPressLow =  new ExeSQL().getOneValue(sqlBloodPressLow); //舒张压

              System.out.println("---aStature:" +
                                 aStature + "---aAvoirdupois:" + aAvoirdupois + "---aAvoirindex" +
                                 aAvoirindex+ "---aBloodPressHigh:" +
                                 aBloodPressHigh +"---aBloodPressLow:" +
                          aBloodPressLow+ "---aNervation:" +
                          aNervation );
       } catch (Exception e) {
           CError tError = new CError();
            tError.moduleName = "HealthAchiveBL";
           tError.functionName = "";
           tError.errorMessage = "客户基本信息查询失败";
     }

        //就诊信息

        ListTable DiagListTable = new ListTable();
        DiagListTable.setName("DIAG");
        String[] Title_D = {
                            "CODENAME","INHOSPITDATE",
                            "HOSPITNAME", "MAINITEM", "ICDNAME"
        };

        try {
            SSRS tSSRS_D = new SSRS();
            String sql = "select rownumber() over(),"
                         +
                    " (select e.codename from ldcode e where e.codetype = 'hminhospitalmode' and e.code = a.inhospitmode),"
                         + " a.inhospitdate,"
                         +" (select c.hospitname from ldhospital c where a.hospitcode = c.hospitcode),"
                         +" a.InHospitalDays,"
                         +" (select d.icdname from lddisease d where b.icdcode = d.icdcode), "
                         +"  (select distinct l. codename   from ldcode l, LHDiagno c where l.codetype='diagnosecureeffect' and  l.code=c.CureEffect and  c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
                         +"  (select sum(f.Feeamount) from LHFeeInfo f where f.Customerno=a.Customerno and f.Inhospitno=a.Inhospitno) "
                         + " from   lhcustominhospital a, LHDiagno b "
                         + " where  a.customerno = '" + aCustomerNo + "'"
                         + " and    a.customerno = b.customerno  and b.IsMainDiagno='1' "
                         + " and    a.inhospitno = b.inhospitno "
                         + " and    (a.InHospitDate between '" + MStartDate +
                         "' and '" + MEndDate + "')"
                        // + " order by a.InHospitDate"
                         ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_D = tExeSQL.execSQL(sql);
            int count_D = tSSRS_D.getMaxRow();
            for (int i = 0; i < count_D; i++) {
                String temp_D[][] = tSSRS_D.getAllData();
                String[] strCol;
                strCol = new String[8];
                strCol[0] = temp_D[i][0];System.out.println(strCol[0]);
                strCol[1] = temp_D[i][1];
                strCol[2] = temp_D[i][2];
                strCol[3] = temp_D[i][3];
                strCol[4] = temp_D[i][4];
                strCol[5] = temp_D[i][5];
                strCol[6] = temp_D[i][6];
                strCol[7] = temp_D[i][7];
                System.out.println( "strCol[0]:" + temp_D[i][0] +
                        ",CODENAME:" + temp_D[i][1] + ", INHOSPITDATE:" + temp_D[i][2] +
                        ", HOSPITNAME:" + temp_D[i][3] + ", MAINITEM:" + temp_D[i][4]
                        + ", ICDNAME:" + temp_D[i][5] );
                DiagListTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthAchiveBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "客户就诊信息查询失败";
            this.mErrors.addOneError(tError);
            mLHCustomInHospitalSchema.setInHospitNo("");
        }


        //检查信息

        ListTable TestListTable = new ListTable();
        TestListTable.setName("TEST");
        String[] Title_T = {"MEDICAITEMNAME", "TESTDATE", "TESTRESULT", "HOSPITNAME",
                           "CODENAME"};

        try {
            SSRS tSSRS_T = new SSRS();
            String sql_T = " select rownumber() over(), "
                           + "(select e.codename from ldcode e where e.codetype = 'hminhospitalmode' and e.code = a.inhospitmode),"
                           +
                           " a.InHospitDate,(select HospitName from LDHospital where HospitCode=a.HospitCode) , "
                           +"(select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ), "
                           +" b.TestResult ||' '||(select distinct c.standardmeasureunit from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                           +"  (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ) "
                           +"   from LHCustomInHospital a ,LHCustomTest b  "
                           + "  where 1=1 and  a.inhospitmode not in ('31','32')  "
                           +"  and  a.customerno = '" + aCustomerNo + "' "
                           + " and a.InHospitNo=b.InHospitNo and b.isnormal='异常' "
                           + "  and    a.customerno = b.customerno  "
                           + " and    (b.testdate between '" + MStartDate +
                           "' and '" + MEndDate + "')"
                           ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_T = tExeSQL.execSQL(sql_T);
            int count_T = tSSRS_T.getMaxRow();
            for (int i = 0; i < count_T; i++) {
                String temp_T[][] = tSSRS_T.getAllData();

                String[] strCol;
                strCol = new String[7];
                strCol[0] = temp_T[i][0];
                strCol[1] = temp_T[i][1];
                strCol[2] = temp_T[i][2];
                strCol[3] = temp_T[i][3];
                strCol[4] = temp_T[i][4];
                strCol[5] = temp_T[i][5];
                strCol[6] = temp_T[i][6];

                System.out.println(
                        "MEDICAITEMNAME:" + temp_T[i][1] + ", TESTDATE:" +
                        temp_T[i][2] +
                        ", TESTRESULT:" + temp_T[i][3] + ", HOSPITNAME:" + temp_T[i][4]
                        + ", CODENAME:" + temp_T[i][5]);

                TestListTable.add(strCol);
            }

        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthAchiveBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "客户检查信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //手术信息

        ListTable OpsListTable = new ListTable();
        OpsListTable.setName("OPS");
        String[] Title_O = {"ICDOPSNAME", "EXECUTEDATE", "HOSPITNAME", "CUREEFFECT"};

        try {
            SSRS tSSRS_O = new SSRS();
            String sql_O = "select rownumber() over(),"
                           +
                           "  (select e.codename from ldcode e where e.codetype = 'hminhospitalmode' and e.code = a.inhospitmode), "
                           +
                           "  b.executdate,"
                           +" (select c.hospitname from ldhospital c where a.hospitcode = c.hospitcode),"
                           +"  (select c.icdopsname from ldicdops c where b.icdopscode = c.icdopscode), "
                           +"   b.cureeffect  "
                           + " from   lhcustominhospital a, lhcustomops b  "
                           + " where  a.customerno = '" + aCustomerNo + "' "
                           + " and    a.customerno = b.customerno  "
                           + " and    a.inhospitno = b.inhospitno "
                           + " and    (b.executdate between '" + MStartDate +
                           "' and '" + MEndDate + "')"
                           + " order by b.executdate "
                           ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_O = tExeSQL.execSQL(sql_O);
            int count_O = tSSRS_O.getMaxRow();
            for (int i = 0; i < count_O; i++) {
                String temp_O[][] = tSSRS_O.getAllData();

                String[] strCol;
                strCol = new String[6];
                strCol[0] = temp_O[i][0];
                strCol[1] = temp_O[i][1];
                strCol[2] = temp_O[i][2];
                strCol[3] = temp_O[i][3];
                strCol[4] = temp_O[i][4];
                strCol[5] = temp_O[i][5];
                System.out.println(
                        "ICDOPSNAME:" + temp_O[i][1] + ", EXECUTEDATE:" +
                        temp_O[i][2] +
                        ", HOSPITNAME:" + temp_O[i][3] + ", CUREEFFECT:" + temp_O[i][4]);
                OpsListTable.add(strCol);
            }

        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthAchiveBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "客户手术信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //家族病史

       // ListTable FamilyListTable = new ListTable();
       // FamilyListTable.setName("FAMILY");
       // String[] Title_F = {"RELATION", "DISEASE"};

       // try {
         //   SSRS tSSRS_F = new SSRS();
          //  String sql_F = " select distinct ( select c.codename from ldcode c where c.codetype ='familycode' and c.code = b.familycode),"
            //               +
              //      " (select d.icdname from lddisease d where d.icdcode = b.icdcode) "
                //           +
                 //   " from   LHCustomFamilyDiseas b, lhcustomhealthstatus a "
                   //        + " where  b.customerno = '" + aCustomerNo + "'"
                     //      + " and a.customerno = b.customerno "
                      //     + " and    (a.AddDate between '" + MStartDate +
                        //   "' and '" + MEndDate + "')"
                         //  ;
           // ExeSQL tExeSQL = new ExeSQL();
           // tSSRS_F = tExeSQL.execSQL(sql_F);
           // int count_F = tSSRS_F.getMaxRow();
           // for (int i = 0; i < count_F; i++) {
           //     String temp_F[][] = tSSRS_F.getAllData();

  //              String[] strCol;
    //            strCol = new String[5];
      //          strCol[0] = temp_F[i][0];
        //        strCol[1] = temp_F[i][1];
    //
      //          System.out.println("TESTMODE:" + temp_F[i][0] + ", TESTDATE:" +
        //                           temp_F[i][1]);
          //      FamilyListTable.add(strCol);
            //}
    //
      //  } catch (Exception e) {
        //    CError tError = new CError();
          //  tError.moduleName = "HealthAchiveBL";
            //tError.functionName = "createAddressNo";
            //tError.errorMessage = "客户家族病信息查询失败";
            //this.mErrors.addOneError(tError);
        //}


        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("HealthArchive.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";

        //HealthArchiveBL模版元素
        MStartDate=MStartDate.replaceFirst("-","年");
        MStartDate=MStartDate.replaceFirst("-","月");
        MEndDate=MEndDate.replaceFirst("-","年");
        MEndDate=MEndDate.replaceFirst("-","月");
       texttag.add("MStartDate", MStartDate);
       texttag.add("MEndDate", MEndDate);
        texttag.add("CustomerName", aCustomerName);
        texttag.add("CustomerNo", aCustomerNo);
        if (aSex.equals("0")) {
            aSex = "男";
        } else {
            aSex = "女";
        }
        System.out.println(aSex);
        texttag.add("Sex", aSex);

        String Age = this.getAge(aBirthDay);
        System.out.println("aBirthDay = " + aBirthDay);
        texttag.add("BirthDay", aBirthDay);
        texttag.add("BloodPressHigh", aBloodPressHigh);
        texttag.add("BloodPressLow", aBloodPressLow);
        texttag.add("WorkPlace", aGrpName);
        texttag.add("stature",aStature);
        texttag.add("avoirdupois",aAvoirdupois);
        texttag.add("avoirindex",aAvoirindex);
        texttag.add("nervation",aNervation);

        SSRS tTask = null;
        String sql1="select name  from ldcom where comcode='"+ mGlobalInput.ManageCom +"'";
        ExeSQL tExeSQL= new ExeSQL();
        tTask=tExeSQL.execSQL(sql1);
        String[][] msgInfo = tTask.getAllData();
        texttag.add("Corp",msgInfo[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息

    //    xmlexport.addListTable(appColListTable, Title);
        xmlexport.addListTable(DiagListTable, Title_D);
        xmlexport.addListTable(TestListTable, Title_T);
        xmlexport.addListTable(OpsListTable, Title_O);
        //xmlexport.addListTable(SomiteListTable, Title_F);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }
    private String getAge(String BirthDay) {
        Integer ii = new Integer("0");
        Integer ii2 = new Integer("0");
        int i = (ii2.parseInt(PubFun.getCurrentDate().substring(0, 4)) -
                 ii.parseInt(BirthDay.substring(0, 4)));

        if (ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
            ii.parseInt(BirthDay.substring(5, 7)) > 0)
        {
            BirthDay = i + "";
        }
        else
        {
            if ((ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
                 ii.parseInt(BirthDay.substring(5, 7))) < 0)
            {
                BirthDay = (i - 1) + "";
            }
            else
            {
                if ((ii2.parseInt(PubFun.getCurrentDate().substring(8, 10)) -
                     ii.parseInt(BirthDay.substring(8, 10))) < 0)
                {
                    BirthDay = (i - 1) + "";
                }
                else
                {
                    BirthDay = i + "";
                }
            }
        }
        return BirthDay;
    }

    public static void main(String[] args) {

        HealthAchiveBL tHealthAchiveBL = new HealthAchiveBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("000000142");
        tVData.addElement(tLDPersonSchema);
        tHealthAchiveBL.submitData(tVData, "PRINT");
        VData vdata = tHealthAchiveBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

    private void jbInit() throws Exception {
    }

}
