package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LAWheelPremiumReportBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mDealDay = "";
    private String mYear = "";
    private String[][] mShowDataList = null;

  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }

  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      mYear = mDealDay.substring(0,4);

      return true;
  }

  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL  = "select * from labranchgroup where branchtype='2' and branchtype2='01'";
      tSQL += "   and managecom in (select comcode from ldcom where Sign='1'";
      tSQL += "   and comcode like '86%' and length(trim(comcode))=8";
      tSQL += "   and comcode <> '86000000')";
      tSQL += " order by branchattr";

      LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
      LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
      tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
      if(tLABranchGroupSet.size()==0)
      {
          buildError("queryData", "没有符合条件的机构！");
          return false;
      }

      // 2、查询需要表示的数据
      String[][] tShowDataList = new String[tLABranchGroupSet.size()][5];
      for(int i=1;i<=tLABranchGroupSet.size();i++)
      {// 循环机构进行统计
          LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
          tLABranchGroupSchema = tLABranchGroupSet.get(i);
          tShowDataList[i-1][0] = tLABranchGroupSchema.getName();
          System.out.println(tLABranchGroupSchema.getAgentGroup()+"/"+tShowDataList[i-1][0]);

          // 查询需要表示的数据
          if (!queryOneDataList(tLABranchGroupSchema.getAgentGroup(),
                                tLABranchGroupSchema.getBranchAttr(),
                                tShowDataList[i - 1]))
          {
              System.out.println("[" + tLABranchGroupSchema.getAgentGroup() +
                                 "] 本机构数据查询失败！");
              return false;
          }
      }
      mShowDataList = tShowDataList;

      return true;
  }

  /**
   * 取得累计承保标准保费
   * @param pmAgentGroup String
   * @return String
   */
  private String getTransStandMoney(String pmAgentGroup)
  {
      String tRtValue = "";
      String tSQL = "";
      double tValue = 0.00;
      DecimalFormat tDF = new DecimalFormat("0.##");

//      tSQL  = "select sum(value(TransStandMoney,0.00)) from lacommision";
//      tSQL += " where tmakedate >= '"+this.mYear+"-01-01'";
//      tSQL += "   and tmakedate <='"+this.mDealDay +"'";
//      tSQL += "   and branchtype = '2' and branchtype2 = '01'";
//      tSQL += "   and agentgroup = '" + pmAgentGroup + "'";

      tSQL  = "select sum(standmoney(b.managecom,b.grppolno,b.riskcode,c.sumactupaymoney))";
      tSQL += "  from lcgrpcont a,lcgrppol b,ljapaygrp c";
      tSQL += " where a.grpcontno = b.grpcontno";
      tSQL += "   and a.grpcontno = c.grpcontno";
      tSQL += "   and a.signdate >= '" + this.mYear + "-01-01'";
      tSQL += "   and a.signdate <= '" + this.mDealDay + "'";
      tSQL += "   and a.agentgroup = '" + pmAgentGroup + "'";
      tSQL += "   and a.salechnl='02'";

      try{
          tValue =  execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getTransStandMoney 出错！");
      }

      tRtValue = "" + tDF.format(tValue);

      return tRtValue;
  }

  /**
   * 取得累计承保规模保费
   * @param pmAgentGroup String
   * @return String
   */
  private String getTransMoney(String pmAgentGroup)
  {
      String tRtValue = "";
      String tSQL = "";
      double tValue = 0.00;
      DecimalFormat tDF = new DecimalFormat("0.##");

//      tSQL  = "select sum(value(TransMoney,0.00)) from lacommision";
//      tSQL += " where tmakedate >= '"+this.mYear+"-01-01'";
//      tSQL += "   and tmakedate <='"+this.mDealDay +"'";
//      tSQL += "   and branchtype = '2' and branchtype2 = '01'";
//      tSQL += "   and agentgroup = '" + pmAgentGroup + "'";

      tSQL  = "select sum(value(b.sumactupaymoney,0.00)) from lcgrpcont a,ljapaygrp b";
      tSQL += " where a.signdate >= '" + this.mYear + "-01-01'";
      tSQL += "   and a.grpcontno=b.grpcontno";
      tSQL += "   and a.signdate <= '" + this.mDealDay + "'";
      tSQL += "   and a.agentgroup = '" + pmAgentGroup + "'";
      tSQL += "   and a.salechnl='02'";

      try
      {
          tValue = execQuery(tSQL);
      } catch (Exception ex)
      {
          System.out.println("getTransMoney 出错！");
      }

      tRtValue = "" + tDF.format(tValue);

      return tRtValue;
  }

  /**
   * 取得标准保费计划
   * @param pmBranchAttr String
   * @return String
   */
  private String getPlanMoney(String pmBranchAttr)
  {
      String tRtValue = "";
      String tSQL = "";
      double tValue = 0.00;
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "select Max(planvalue) from LAPlan where plantype = '2'";
      tSQL += "   and planobject = '" + pmBranchAttr + "'";
      tSQL += "   and branchtype = '2' and branchtype2 = '01'";
      tSQL += "   and planperiodunit = 12 and planperiod = "+this.mYear;

      try
      {
          tValue = execQuery(tSQL);
      } catch (Exception ex)
      {
          System.out.println("getPlanMoney 出错！");
      }

      tRtValue = "" + tDF.format(tValue);

      return tRtValue;
  }

  /**
   * 取得标准保费计划完成率
   * @param pmAgentGroup String
   * @return String
   */
  private String getRateTransStandMoney(String pmTransStandMoney,String pmPlanMoney)
  {
      String tRtValue = "";
      String tSQL = "";
      double tValue = 0.00;

      if(0.05 > Double.parseDouble(pmPlanMoney))
      {
          // 如果未设定保费计划计划达成率为100%
          return "100";
      }

      tSQL = "select DECIMAL(DECIMAL(" + pmTransStandMoney + ",12,4) / DECIMAL(" +
             pmPlanMoney + ",12,4),12,4) * 100 from dual";

      try
      {
          tValue = execQuery(tSQL);
      } catch (Exception ex)
      {
          System.out.println("getRateTransStandMoney 出错！");
      }

      tRtValue = String.valueOf(tValue);

      return tRtValue;
  }

  /**
   * 查询填充表示数据
   * @param pmAgentGroup String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList(String pmAgentGroup,String pmBranchAttr,String[] pmOneDataList)
  {
      try{
          // 0、机构名称
          // 1、累计承保标准保费
          pmOneDataList[1] = getTransStandMoney(pmAgentGroup);
          // 2、累计承保规模保费
          pmOneDataList[2] = getTransMoney(pmAgentGroup);
          // 3、年度标准保费计划
          pmOneDataList[3] = getPlanMoney(pmBranchAttr);
          // 4、年度标准保费计划完成率
          pmOneDataList[4] = getRateTransStandMoney(pmOneDataList[1],pmOneDataList[3]);
      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      String tSQL = "";
      String CurrentDate = PubFun.getCurrentDate();//得到当天日期
      //String strArr[] = null;

      try{
          // 1、查询数据
          if(!getDataList())
          {
              return false;
          }

System.out.println(mShowDataList.length);
System.out.println(mShowDataList[0].length);

          // 2、进行排序  先进行保费计划达成率排序 再 进行活动率排名
//          LISComparator tLISComparator = new LISComparator();
//          tLISComparator.setNum(6);
//          Arrays.sort(mShowDataList,tLISComparator);
//          for(int j=0;j<mShowDataList.length;j++)
//          {// 追加序号
//              mShowDataList[j][7] = "" + (j + 1);
//          }
//          tLISComparator = new LISComparator();
//          tLISComparator.setNum(14);
//          Arrays.sort(mShowDataList, tLISComparator);
//          for (int j = 0; j < mShowDataList.length; j++)
//          { // 追加序号
//              mShowDataList[j][15] = "" + (j + 1);
//          }

          // 3、设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Order");

          for(int i=0;i<mShowDataList.length;i++)
          {
              tlistTable.add(mShowDataList[i]);
          }

          TextTag texttag = new TextTag();    //新建一个TextTag的实例
          texttag.add("StartYear", this.mDealDay.substring(0,4));   //输入制表时间
          texttag.add("StartDay", this.mDealDay.substring(8,10));   //输入制表时间
          texttag.add("StartMonth", this.mDealDay.substring(5,7));    //输入制表时间

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAWheelPremiumReport.vts", "printer"); //最好紧接着就初始化xml文档
          if (texttag.size() > 0)
              xmlexport.addTextTag(texttag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          mResult.addElement(xmlexport);
      }catch (Exception ex)
      {
          buildError("queryData", "LAWheelPremiumReportBL发生错误，准备数据时出错！");
          return false;
      }

      return true;
  }

  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
      mDealDay = (String) pmInputData.get(0);
      //mEndDay = (String) pmInputData.get(1);
      System.out.println(mDealDay);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAStatisticReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }

  /**
   * 执行SQL文查询结果
   * @param sql String
   * @return double
   */
  private double execQuery(String sql)
  {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
    }

  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
}
