package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.*;
import java.text.DecimalFormat;

//程序名称： OmDayPrintBL.java
//程序功能： 万能可转投资净额日结报表
//创建日期：2009-6-2
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class OmDayPrintBL implements PrintService 
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    SSRS mSSRS = new SSRS();
    
    private String mOperate = ""; 
    
    private String mCurrentDate = PubFun.getCurrentDate();
    
    private String mPrintName = ""; 

    private String mManageCom = "";
    
    private String mComName = "";

    private String mStartDate = "";

    private String mEndDate = ""; 

    public OmDayPrintBL() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        this.mOperate = cOperate;
        System.out.println("mOperate="+mOperate);
        if(!"null".equals(mOperate) && !"".equals(mOperate) && "save".equals(mOperate))
        {
        	if (!saveData(cInputData))
            {
                return false;
            }
        }
        else
        {
        	System.out.println("-----OmDayPrintBL print start-----");
        	if (!getInputData(cInputData))
            {
                return false;
            }

            mResult.clear();
            if (!getListData()) 
            {
                return false;
            }

            // 准备所有要打印的数据
            if (!getPrintData()) 
            {
                return false;
            }

            //加入到打印列表
            if (!dealPrintMag()) 
            {
                return false;
            }
            
            //删除临时表
            if (!delOmnipotenceIntfData()) 
            {
                return false;
            }
            System.out.println("-----OmDayPrintBL print end-----");
        }
        return true;
    }

    private boolean saveData(VData cInputData) 
    {
    	System.out.println("-----OmDayPrintBL save start-----");
    	TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	OmnipotenceIntfDataSet tOmnipotenceIntfDataSet = (OmnipotenceIntfDataSet)tTransferData.getValueByName("tOmnipotenceIntfDataSet");
    	MMap map = new MMap();
    	map.put(tOmnipotenceIntfDataSet, SysConst.DELETE_AND_INSERT);
    	VData tVData = new VData();
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "")) 
        {
            System.out.println("save fail!");
            return false;
        }
        System.out.println("-----OmDayPrintBL save end-----");
        return true;
	}

	private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if( mGlobalInput==null || mLOPRTManagerSchema==null )
        {
            CError tError = new CError();
            tError.moduleName = "OmDayPrintBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mPrintName = mLOPRTManagerSchema.getOtherNo();
        mManageCom = mLOPRTManagerSchema.getStandbyFlag1();
        mStartDate = mLOPRTManagerSchema.getStandbyFlag2();
        mEndDate = mLOPRTManagerSchema.getStandbyFlag3(); 
        String sql = "select name from ldcom where comcode = '"+mManageCom+"' with ur";
        mComName = new ExeSQL().getOneValue(sql);
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }

    public CErrors getErrors() 
    {
        return this.mErrors;
    }

    private boolean getListData()
    {
        StringBuffer sql = new StringBuffer(128);
        sql.append("select a.managecom, (select name from ldcom where comcode = a.managecom), ")
           .append("a.riskcode, (select riskname from lmrisk where riskcode = a.riskcode), ")
           .append("S06, S07, S09, Remark, Remark1, Remark2, Remark3, Remark4, BussDescribe, ")
           .append("AgentCode, IntfOperater, ErrorInfo from OmnipotenceIntfData  a ")
           .append("where BussType = 'O2' and BussDate = '").append(mCurrentDate).append("' ")
           .append("and otherno = '").append(mPrintName).append("' with ur");
        System.out.println(sql.toString());

        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL(sql.toString());
        if (tExeSQL.mErrors.needDealError()) 
        {
            CError tError = new CError();
            tError.moduleName = "OmDayPrintBL";
            tError.functionName = "getListData";
            tError.errorMessage = "获取打印数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean getPrintData()
    {
    	XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("OMDayPrint", "printer");
        textTag.add("ComName", mComName);
        textTag.add("StartDate", mStartDate);
        textTag.add("EndDate", mEndDate);
        textTag.add("Yuliu1","Yuliu2");//预留1
        textTag.add("Yuliu2","Yuliu2");//预留2
        textTag.add("Yuliu3","Yuliu3");//预留3
        xmlexport.addTextTag(textTag);
        String[] title = {"机构编码","机构名称","险种代码","险种名称","保费收入","持续奖金","保单利息","初始费用","犹豫期退保","退保金","退保费用","保单管理费","部分领取","重疾保险金","身故保险金","可转投资净额"};
        xmlexport.addListTable(getListTable(), title);

        xmlexport.outputDocumentToFile("C:\\","zmgtest");
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    private boolean dealPrintMag()
    {
        String tLimit = PubFun.getNoLimit(mManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNoType("OM");
        mLOPRTManagerSchema.setCode("OM001");
        mLOPRTManagerSchema.setManageCom(mManageCom);
        mLOPRTManagerSchema.setAgentCode(mGlobalInput.AgentCom);
        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) 
        {
            String[] info = new String[16];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) 
            {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("OmnipotenceIntfData");
        return tListTable;
    }
    
    private boolean delOmnipotenceIntfData()
    {
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("delete from OmnipotenceIntfData where otherno = '").append(mPrintName).append("' ");
    	VData tVData = new VData();
    	MMap map = new MMap();
    	map.put(sql.toString(), SysConst.DELETE);
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "")) 
        {
            System.out.println("delOmnipotenceIntfData fail!");
            return false;
        }
        return true;
    }
}
