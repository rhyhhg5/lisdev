package com.sinosoft.lis.f1print;

import java.io.InputStream;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLAccModifyPrintfBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";

	// private String mFileNameB = "";
	// private String mMakeDate = "";
	private String mActuGetNo = "";

	private String mCaseNo = "";

	private String mRgtNo = "";

	private String mContNo = "";

	private String mCustomerNo = "";

	private String mTypeRadio = "";
	
	private String mHandler = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryData()) {
			return false;
		}/*
			 * else{ TransferData tTransferData= new TransferData();
			 * tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			 * tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			 * tTransferData.setNameAndValue("tOperator", mOperator);
			 * LLPrintSave tLLPrintSave = new LLPrintSave(); VData tVData = new
			 * VData(); tVData.addElement(tTransferData);
			 * if(!tLLPrintSave.submitData(tVData,"")){
			 * this.bulidErrorB("LLPrintSave-->submitData", "数据不完整"); return
			 * false; } }
			 */
		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLSurveyBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();

		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		// 页面传入的数据 三个
		mOperator = (String) mTransferData.getValueByName("tOperator");
		// mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
		this.mManageCom = (String) mTransferData.getValueByName("tManageCom");

		System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);
		this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
		this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
		if (mManageCom == null || mManageCom.equals("")) {
			this.mManageCom = "86";
		}
		mActuGetNo = (String) mTransferData.getValueByName("ActuGetNo");
		mCaseNo = (String) mTransferData.getValueByName("CaseNo");
		mRgtNo = (String) mTransferData.getValueByName("RgtNo");
		mContNo = (String) mTransferData.getValueByName("ContNo");
		mCustomerNo = (String) mTransferData.getValueByName("CustomerNo");
		mTypeRadio = (String) mTransferData.getValueByName("typeRadio");
		mHandler = (String) mTransferData.getValueByName("handler");

		return true;

	}

	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLCaseStatistics.vts", "printer");

		String tMakeDate = "";
		tMakeDate = mPubFun.getCurrentDate();

		System.out.print("dayin252");
		String sql = "select name from ldcom where comcode='" + mManageCom
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		String mManageComName = tExeSQL.getOneValue(sql);
		tTextTag.add("ManageCom", mManageComName);
		tTextTag.add("StartDate", mStartDate);
		tTextTag.add("EndDate", mEndDate);
		tTextTag.add("MakeDate", tMakeDate);
		tTextTag.add("operator", mOperator);
		// mMakeDate = tMakeDate;
		System.out.println("1212121" + tMakeDate);
		if (tTextTag.size() < 1) {
			return false;
		}
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "", "", "", "", "", "", "", "", "", "","", ""};
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private boolean getDataList() {

		SSRS tSSRS = new SSRS();
		//add lyc #1990 加失败原因字段 2014-07-16
		String sql = "select  g.otherno,g.sumgetmoney,g.PayMode,g.BankCode,g.BankAccNo,g.AccName,g.actugetno,(select bank_reason(g.actugetno) from dual) from ljaget g where  othernotype='5' and confdate is null and g.managecom like '"
				+ mManageCom + "%' ";
		if ("".equals(mTypeRadio) || mTypeRadio == null
				|| "2".endsWith(mTypeRadio)) {
			String tHandlerSQL = "";
			if (mHandler != null && !"".equals(mHandler)) {
				tHandlerSQL = " and rigister = '" + mHandler + "'";
			}
			sql = sql
					+ "  and g.paymode='4'  and (bankonthewayflag ='0' or bankonthewayflag is null) and g.cansendbank = '1' and exists (select 1 from llcase where caseno=g.otherno "
					+ tHandlerSQL + ")";
		} else if ("3".equals(mTypeRadio)) {
			String tHandlerSQL = "";
			if (mHandler != null && !"".equals(mHandler)) {
				tHandlerSQL = " and handler = '" + mHandler + "'";
			}
			sql = sql
					+ "  and g.paymode in ('3','4','11')  and (bankonthewayflag = '0' or bankonthewayflag is null) and g.cansendbank = 'M' and exists (select 1 from llcase where caseno=g.otherno "
					+ tHandlerSQL + ")";
		} else if ("0".equals(mTypeRadio)) {
			sql = sql
					+ "  and g.paymode='4'  and (bankonthewayflag = '0' or bankonthewayflag is null) and (g.cansendbank = '0' or g.cansendbank is null)  and  (g.BankAccNo is not null) and otherno not like 'P%' ";
		} else if ("1".equals(mTypeRadio)) {
			sql = sql
					+ "  and g.paymode='4'  and (g.cansendbank = '0' or g.cansendbank is null) and g.paymode='4' and bankonthewayflag='1' and otherno not like 'P%' ";
		} else if ("4".equals(mTypeRadio)) {
			sql = sql + "  and g.paymode='4'  and otherno not like 'P%' ";
		} else if ("6".equals(mTypeRadio)){
			sql = sql + " and g.paymode='11' and (bankonthewayflag = '0' or bankonthewayflag is null)";
		} else {
			sql = sql + " and otherno not like 'P%' ";
		}
		if (!"".equals(mActuGetNo) && mActuGetNo != null) {
			sql = sql + " and g.actugetno='" + mActuGetNo + "'";
		}
		if (!"".equals(mStartDate) && mStartDate != null) {
			sql = sql + " and g.makedate>='" + mStartDate + "'";
		}
		if (!"".equals(mEndDate) && mEndDate != null) {
			sql = sql + " and g.makedate<='" + mEndDate + "'";
		}
		String spart1 = "";
		ExeSQL tExeSQL = new ExeSQL();
		if (!"".equals(mCaseNo) && mCaseNo != null) {
				spart1 = spart1 + " and g.otherno='" + mCaseNo + "'";

		}
		if (mRgtNo != null && !"".equals(mRgtNo)) {
				spart1 = spart1
						+ " and exists (select 1 from llcase where caseno=g.otherno and rgtno='"
						+ mRgtNo + "')";
		}
		if (mContNo != null && !"".equals(mContNo)) {
			spart1 = spart1
					+ " and exists (select 1 from llclaimdetail where (contno='"
					+ mContNo + "' or grpcontno='" + mContNo
					+ "') and (caseNo=g.otherno or rgtno=g.otherno))";
		}
		if (mCustomerNo != null && !"".equals(mCustomerNo)) {
			spart1 = spart1
					+ " and exists (select 1 from llcase where customerNo='"
					+ mCustomerNo
					+ "' and (caseNo=g.otherno or rgtno=g.otherno)";
		}
		sql = sql + spart1 + " with ur";
		System.out.println("sql:"+sql);
		tSSRS = tExeSQL.execSQL(sql);
		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql);
		int cont = 0;
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String ListInfo[] = new String[15];// modify lyc
			String mC = tSSRS.GetText(i, 1);
			String s = "";
			String tRgtNo = "";
			if(!"P".equals(mC.substring(0, 1))){
                s = "select rgtno from llcase where caseno='"+mC+"' with ur";
    			tRgtNo = tExeSQL.getOneValue(s);
			}

            if(tRgtNo==null || "".equals(tRgtNo)){
			    ListInfo[0] = mC;
			    ListInfo[1] = "";
				ListInfo[2] = "" ;
				ListInfo[3] = "";
				ListInfo[4] = getGrpContNo(mC) ;
				ListInfo[5] = getGrpNameByRgtNo(mC) ;
            }else{
			    ListInfo[0] = tRgtNo;
			    ListInfo[1] = mC;
				ListInfo[2] = getCustomerName(mC) ;
				ListInfo[3] = getIDNo(mC) ;
				ListInfo[4] = getContNo(mC) ;
				ListInfo[5] = getGrpNameByCaseNo(mC) ;
            }
				//System.out.println("getAgentNameByContNo(ListInfo[4])="+(getAgentNameByContNo(ListInfo[4]))+"   ListInfo[11]="+(ListInfo[11]));
            //modify lyc 为后人不被误导修改了下
            String sumPay = tSSRS.GetText(i, 2);
			if(sumPay==null || "".equals(sumPay)){
				sumPay = "";
            }
			ListInfo[6] = sumPay;
			ListInfo[7] = getPayMode(tSSRS.GetText(i, 3)) ;
			ListInfo[8] = getBankName(tSSRS.GetText(i, 4)) ;
			String insure = tSSRS.GetText(i, 5);
			if(insure==null || "".equals(insure)){
				insure="";
			}
			ListInfo[9] = insure;
			String tel = tSSRS.GetText(i, 6);;
			if(tel==null || "".equals(tel)){
				tel = "";
			}
			ListInfo[10] = tel;
			ListInfo[11] = getAgentNameByNo(mC);
			ListInfo[12] =getPhone(mC);
			ListInfo[13] = tSSRS.GetText(i, 7);
			ListInfo[14] = tSSRS.GetText(i, 8); //add lyc
			++cont;
//			System.out.println("getAgentNameByContNo(ListInfo[4])="+(getAgentNameByContNo(ListInfo[4]))+"   ListInfo[11]="+(ListInfo[11]));
			mListTable.add(ListInfo);
		}

		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql);
		System.out.println(cont+"'''''''''''''''''''");
		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	private String getCustomerName(String tCaseNo) {
		String sql = "select customername from llcase where caseno='" + tCaseNo
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String customerName = tExeSQL.getOneValue(sql);
		if(customerName==null || "".equals(customerName)){
			customerName = "  ";
		}
		return customerName;

	}

	private String getIDNo(String tCaseNo) {
		String sql = "select IDNo from llcase where caseno='" + tCaseNo
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String IDNo = tExeSQL.getOneValue(sql);
		if(IDNo==null || "".equals(IDNo)){
			IDNo = "  ";
		}
		return IDNo;

	}

	private String getContNo(String tCaseNo) {
		String sql = "select contno from llclaimdetail where caseno='"
				+ tCaseNo + "' fetch first rows only with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String contNo = tExeSQL.getOneValue(sql);
		if(contNo==null || "".equals(contNo)){
			contNo = "  ";
		}
		return contNo;
	}
	private String getGrpContNo(String tCaseNo){
    String sql = "select rgtobjno from llregister where rgtno='"
			+ tCaseNo + "' fetch first rows only with ur";
	ExeSQL tExeSQL = new ExeSQL();
	String contNo = tExeSQL.getOneValue(sql);
	if(contNo==null || "".equals(contNo)){
		contNo = "  ";
	}
	return contNo;
	}

	private String getGrpNameByCaseNo(String tCaseNo) {
		String sql = "select grpname from lcgrpcont c,llclaimdetail d where caseno='"
				+ tCaseNo
				+ "' and c.grpcontno=d.grpcontno union select grpname from lbgrpcont c,llclaimdetail d where caseno='" + tCaseNo
				+ "' and c.grpcontno=d.grpcontno fetch first rows only with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String grpName = tExeSQL.getOneValue(sql);
		if(grpName==null || "".equals(grpName)){
			grpName = "  ";
		}
		return grpName;
	}
	private String getGrpNameByRgtNo(String tRgt) {
		String sql = "select distinct grpname from lcgrpcont c,llclaimdetail d where Rgtno='"
				+ tRgt
				+ "' and c.grpcontno=d.grpcontno union select distinct grpname from lbgrpcont c,llclaimdetail d where rgtno='" + tRgt
				+ "' and c.grpcontno=d.grpcontno fetch first rows only with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String grpName = tExeSQL.getOneValue(sql);
		if(grpName==null || "".equals(grpName)){
			grpName = "  ";
		}
		return grpName;
	}

	private String getPayMode(String payMode) {
		String sql = "select codename from ldcode where codetype='paymode' and code='"
				+ payMode + "'with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String payM = tExeSQL.getOneValue(sql);
		if(payM==null || "".equals(payM)){
			payM = "  ";
		}
		return payM;
	}

	private String getBankName(String bankCode) {
		String sql = "select bankname from ldbank where bankcode='" + bankCode
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String bankName = tExeSQL.getOneValue(sql);
		if(bankName==null || "".equals(bankName)){
			bankName = "  ";
		}
		return bankName;
	}
	private String getAgentNameByNo(String no){
		String sql = "select name from laagent a,ljaget c where otherno='"+no+"' and a.agentcode=c.agentcode fetch first rows only with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String agentName = tExeSQL.getOneValue(sql);
		if(agentName==null || "".equals(agentName)){
			agentName = "  ";
		}
		return agentName;
	}
//	private String getAgentPhoneByContNo(String contNo){
//		String sql = "select name from laagent a,lccont c where contno='"+contNo+"' and a.agentcode=c.agentcode union select name from laagent a,lbcont c where contno='"+contNo+"' and a.agentcode=c.agentcode fetch first rows only with ur";
//		ExeSQL tExeSQL = new ExeSQL();
//		String agentName = tExeSQL.getOneValue(sql);
//		if(agentName==null || "".equals(agentName)){
//			agentName = "  ";
//		}
//		return agentName;
//	}
	private String getPhone(String no){
		String sql = "select phone from llcase where caseno='"+no+"' union select rgtantmobile from llregister where rgtno='"+no+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String agentName = tExeSQL.getOneValue(sql);
		if(agentName==null || "".equals(agentName)){
			agentName = "  ";
		}
		return agentName;
		
	}
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }

}
