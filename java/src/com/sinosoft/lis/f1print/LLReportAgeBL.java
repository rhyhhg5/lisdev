package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;

public class LLReportAgeBL
    implements PrintService {

/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

private VData mResult = new VData();


//业务处理相关变量
/** 全局数据 */
private GlobalInput mGlobalInput = new GlobalInput();
private TransferData mTransferData = new TransferData();
private String mOperate = "";
private String StartDate;
private String EndDate;
private String ManageCom;
private String AgeSpan;

public LLReportAgeBL() {
}

/**
 传输数据的公共方法
 */
public boolean submitData(VData cInputData, String cOperate) {
    mOperate = cOperate;
    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
        return false;
    }
    System.out.println("1 ...");
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
        return false;
    }
    return true;
}

/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData() {
    return true;
}

/**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) {
    //全局变量
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    mTransferData = ((TransferData) cInputData.getObjectByObjectName(
            "TransferData", 0));

    if (mTransferData == null) {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }

    return true;
}

public VData getResult() {
    return mResult;
}

public CErrors getErrors() {
    return mErrors;
}

private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

private boolean getPrintData() {
    boolean PEFlag = false; //打印理赔申请回执的判断标志
    boolean shortFlag = false; //打印备注信息
    StartDate = (String) mTransferData.getValueByName("StartDate");
    EndDate = (String) mTransferData.getValueByName("EndDate");
    ManageCom = (String) mTransferData.getValueByName("ManageCom");
    AgeSpan = (String) mTransferData.getValueByName("AgeSpan");
    if(mOperate.equals("STATE"))
        printCaseReport();
    if(mOperate.equals("RESULT"))
        printRsuReport();
    return true;
}
private void printCaseReport(){
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    String[] strCol;
    ListTable appColListTable = new ListTable();
    appColListTable.setName("Report");
    String[] Title = {
                     "AgeSpan", "CaseNoM", "CaseNoF", "CaseNo",
                     "ClaimAmntM", "ClaimAmntF", "ClaimAmnt","AccRateM",
                     "AccRateF","AccRate", "PayRateM","PayRateF","PayRate"};
    int span = Integer.parseInt(AgeSpan);
    int age = span;
    int tempage=0;
    String exeSql = "select max(customerage) from llcase";
    ExeSQL esql = new ExeSQL();
    String tmaxage = esql.getOneValue(exeSql);
    int maxage = Integer.parseInt(tmaxage);
    while(tempage<maxage)
    {
        ExeSQL exesql = new ExeSQL();
        String sqlbase0 = "SELECT COUNT(A.CASENO),SUM(A1.REALPAY) FROM LLCASE A,LLCLAIM A1 WHERE " +
                          " A1.CASENO=A.CASENO and A.ENDCASEDATE IS NOT NULL AND A.CUSTOMERAGE>="+
                          tempage+" AND A.CUSTOMERAGE<"+ age+" AND substr(A.MNGCOM,1,length('"+
                          ManageCom+"'))='"+ManageCom+"' AND DATE(A.MAKEDATE)>=DATE('"+StartDate+
                          "') AND DATE(A1.MAKEDATE)<=DATE('"+EndDate+"')";
        String sql0 = sqlbase0 + " AND A.CUSTOMERSEX='0' ";
        String sql1 = sqlbase0 + " AND A.CUSTOMERSEX='1' ";
        String sqlbase1 = "SELECT COUNT(A.InsuredNo),(1-0.25)*SUM(A.Prem)*(DATE('"+EndDate+"')-DATE('"
                          +StartDate+"'))/365 FROM LCpol A WHERE A.AppFlag='1' AND A.INSUREDAPPAGE>="
                          +tempage+" AND A.INSUREDAPPAGE<"+ age+" AND substr(A.ManageCOM,1,length('"
                          +ManageCom+"'))='"+ManageCom+"' AND DATE(A.EndDATE)>=DATE('"+StartDate+
                          "') AND DATE(A.CVALIDATE)<=DATE('"+EndDate+"')";
        String sql2 = sqlbase1 + " AND A.INSUREDSEX='0' ";
        String sql3 = sqlbase1 + " AND A.INSUREDSEX='1' ";
        SSRS COUNT0 = exesql.execSQL(sql0);
        SSRS COUNT1 = exesql.execSQL(sql1);
        SSRS COUNT2 = exesql.execSQL(sqlbase0);
        SSRS COUNT3 = exesql.execSQL(sql2);
        SSRS COUNT4 = exesql.execSQL(sql3);
        SSRS COUNT5 = exesql.execSQL(sqlbase1);
        System.out.println(COUNT0.getMaxRow());
        String[][] temdata0 = null;
        String[][] temdata1 = null;
        String[][] temdata2 = null;
        String[][] temdata3 = null;
        String[][] temdata4 = null;
        String[][] temdata5 = null;
        if (COUNT0 != null && COUNT0.getMaxRow() > 0) {
            temdata0 = COUNT0.getAllData();
        }
        if (COUNT1 != null && COUNT1.getMaxRow() > 0) {
            temdata1 = COUNT1.getAllData();
        }
        if (COUNT2 != null && COUNT2.getMaxRow() > 0) {
            temdata2 = COUNT2.getAllData();
        }
        if (COUNT3 != null && COUNT3.getMaxRow() > 0) {
            temdata3 = COUNT3.getAllData();
        }
        if (COUNT4 != null && COUNT4.getMaxRow() > 0) {
            temdata4 = COUNT4.getAllData();
        }
        if (COUNT5 != null && COUNT5.getMaxRow() > 0) {
            temdata5 = COUNT5.getAllData();
        }

            strCol = new String[13];
            strCol[0] = temdata0[0][0];
            strCol[1] = temdata1[0][0];
            strCol[2] = temdata2[0][0];
            strCol[3] = temdata0[0][1];
            strCol[4] = temdata1[0][1];
            strCol[5] = temdata2[0][1];
            if (strCol[3].equals("null"))
                strCol[3] = "0";
            if (strCol[4].equals("null"))
                strCol[4] = "0";
            if (strCol[5].equals("null"))
                strCol[5] = "0";
            double mrealpay_M = Double.parseDouble(strCol[3]);
            double mrealpay_F = Double.parseDouble(strCol[4]);
            double mrealpay = Double.parseDouble(strCol[5]);
            if(temdata3[0][0].equals("0"))
                strCol[6]="0";
            else{
                double claimrate = Double.parseDouble(strCol[0])/Double.parseDouble(temdata3[0][0])*100;
                claimrate = Arith.round(claimrate,2);
                strCol[6]=claimrate+"";
            }
            if(temdata4[0][0].equals("0"))
                strCol[7]="0";
            else{
                double claimrate = Double.parseDouble(strCol[1])/Double.parseDouble(temdata4[0][0])*100;
                claimrate = Arith.round(claimrate,2);
                strCol[7]=claimrate+"";
            }
            if(temdata5[0][0].equals("0"))
                strCol[8]="0";
            else{
                double claimrate = Double.parseDouble(strCol[2])/Double.parseDouble(temdata5[0][0])*100;
                claimrate = Arith.round(claimrate,2);
                strCol[8]=claimrate+"";
            }
            double mpassprem = 0.0;
            double mpassprem_M = 0.0;
            double mpassprem_F = 0.0;
            String tmpassprem = "";
            String tmpassprem_M = "";
            String tmpassprem_F = "";
            tmpassprem = temdata5[0][1];
            tmpassprem_M = temdata3[0][1];
            tmpassprem_F = temdata4[0][1];
            if (tmpassprem.equals("null"))
                tmpassprem = "0";
            mpassprem = Double.parseDouble(tmpassprem);
            if (tmpassprem_M.equals("null"))
                tmpassprem_M = "0";
            mpassprem_M = Double.parseDouble(tmpassprem_M);
            if (tmpassprem_F.equals("null"))
                tmpassprem_F = "0";
            mpassprem_F = Double.parseDouble(tmpassprem_F);
            if (mpassprem_M < 0.001)
                strCol[9] = "0";
            else {
                double claimrate = mrealpay_M / mpassprem_M * 100;
                claimrate = Arith.round(claimrate, 2);
                strCol[9] = claimrate + "";
            }
            if (mpassprem_F < 0.001)
                strCol[10] = "0";
            else {
                double claimrate = mrealpay_F / mpassprem_F * 100;
                claimrate = Arith.round(claimrate, 2);
                strCol[10] = claimrate + "";
            }
            if (mpassprem < 0.001)
                strCol[11] = "0";
            else {
                double claimrate = mrealpay / mpassprem * 100;
                claimrate = Arith.round(claimrate, 2);
                strCol[11] = claimrate + "";
            }
            strCol[12] = age+"岁";
            appColListTable.add(strCol);
        tempage =age;
        age+=span;
    }
    //其它模版上单独不成块的信息
    ExeSQL exesql = new ExeSQL();
    String sqlbase0 = "SELECT COUNT(A.CASENO),SUM(A1.REALPAY) FROM LLCASE A,LLCLAIM A1 WHERE " +
                      " A1.CASENO=A.CASENO and A.ENDCASEDATE IS NOT NULL AND substr(A.MNGCOM,1,length('"+
                      ManageCom+"'))='"+ManageCom+"' AND DATE(A.MAKEDATE)>=DATE('"+StartDate+
                      "') AND DATE(A1.MAKEDATE)<=DATE('"+EndDate+"')";
    String sql0 = sqlbase0 + " AND A.CUSTOMERSEX='0' ";
    String sql1 = sqlbase0 + " AND A.CUSTOMERSEX='1' ";
    String sqlbase1 = "SELECT COUNT(A.InsuredNo),(1-0.25)*SUM(A.Prem)*(DATE('"+EndDate+"')-DATE('"
                      +StartDate+"'))/365 FROM LCpol A WHERE A.AppFlag='1' AND substr(A.ManageCOM,1,length('"
                      +ManageCom+"'))='"+ManageCom+"' AND DATE(A.EndDATE)>=DATE('"+StartDate+
                      "') AND DATE(A.CVALIDATE)<=DATE('"+EndDate+"')";
    String sql2 = sqlbase1 + " AND A.INSUREDSEX='0' ";
    String sql3 = sqlbase1 + " AND A.INSUREDSEX='1' ";
    SSRS COUNT0 = exesql.execSQL(sql0);
    SSRS COUNT1 = exesql.execSQL(sql1);
    SSRS COUNT2 = exesql.execSQL(sqlbase0);
    SSRS COUNT3 = exesql.execSQL(sql2);
    SSRS COUNT4 = exesql.execSQL(sql3);
    SSRS COUNT5 = exesql.execSQL(sqlbase1);
    System.out.println(COUNT0.getMaxRow());
    String[][] temdata0 = null;
    String[][] temdata1 = null;
    String[][] temdata2 = null;
    String[][] temdata3 = null;
    String[][] temdata4 = null;
    String[][] temdata5 = null;
    if (COUNT0 != null && COUNT0.getMaxRow() > 0) {
        temdata0 = COUNT0.getAllData();
    }
    if (COUNT1 != null && COUNT1.getMaxRow() > 0) {
        temdata1 = COUNT1.getAllData();
    }
    if (COUNT2 != null && COUNT2.getMaxRow() > 0) {
        temdata2 = COUNT2.getAllData();
    }
    if (COUNT3 != null && COUNT3.getMaxRow() > 0) {
        temdata3 = COUNT3.getAllData();
    }
    if (COUNT4 != null && COUNT4.getMaxRow() > 0) {
        temdata4 = COUNT4.getAllData();
    }
    if (COUNT5 != null && COUNT5.getMaxRow() > 0) {
        temdata5 = COUNT5.getAllData();
    }

        String TCaseNoM = temdata0[0][0];
        String TCaseNoF = temdata1[0][0];
        String TCaseNo = temdata2[0][0];
        String TRealPayM = temdata0[0][1];
        String TRealPayF = temdata1[0][1];
        String TRealPay = temdata2[0][1];
        if (TRealPayM.equals("null"))
            TRealPayM = "0";
        if (TRealPayF.equals("null"))
            TRealPayF = "0";
        if (TRealPay.equals("null"))
            TRealPay = "0";
        double mrealpay_M = Double.parseDouble(TRealPayM);
        double mrealpay_F = Double.parseDouble(TRealPayF);
        double mrealpay = Double.parseDouble(TRealPay);
        String TCaseRateM="";
        String TPayRateM = "";
        if(temdata3[0][0].equals("0"))
            TCaseRateM="0";
        else{
            double claimrate = Double.parseDouble(TCaseNoM)/Double.parseDouble(temdata3[0][0])*100;
            claimrate = Arith.round(claimrate,2);
            TCaseRateM=claimrate+"";
        }
        String TCaseRateF="";
        String TPayRateF = "";
        if(temdata4[0][0].equals("0"))
            TCaseRateF="0";
        else{
            double claimrate = Double.parseDouble(TCaseNoF)/Double.parseDouble(temdata4[0][0])*100;
            claimrate = Arith.round(claimrate,2);
            TCaseRateF=claimrate+"";
        }
        String TCaseRate="";
        String TPayRate = "";
        if(temdata5[0][0].equals("0"))
            TCaseRate="0";
        else{
            double claimrate = Double.parseDouble(TCaseNo)/Double.parseDouble(temdata5[0][0])*100;
            claimrate = Arith.round(claimrate,2);
            TCaseRate=claimrate+"";
        }
        double mpassprem = 0.0;
        double mpassprem_M = 0.0;
        double mpassprem_F = 0.0;
        String tmpassprem = "";
        String tmpassprem_M = "";
        String tmpassprem_F = "";
        tmpassprem = temdata5[0][1];
        tmpassprem_M = temdata3[0][1];
        tmpassprem_F = temdata4[0][1];
        if (tmpassprem.equals("null"))
            tmpassprem = "0";
        mpassprem = Double.parseDouble(tmpassprem);
        if (tmpassprem_M.equals("null"))
            tmpassprem_M = "0";
        mpassprem_M = Double.parseDouble(tmpassprem_M);
        if (tmpassprem_F.equals("null"))
            tmpassprem_F = "0";
        mpassprem_F = Double.parseDouble(tmpassprem_F);
        if (mpassprem_M < 0.001)
            TPayRateM = "0";
        else {
            double claimrate = mrealpay_M / mpassprem_M * 100;
            claimrate = Arith.round(claimrate, 2);
            TPayRateM = claimrate + "";
        }
        if (mpassprem_F < 0.001)
            TPayRateF = "0";
        else {
            double claimrate = mrealpay_F / mpassprem_F * 100;
            claimrate = Arith.round(claimrate, 2);
            TPayRateF = claimrate + "";
        }
        if (mpassprem < 0.001)
            TPayRate = "0";
        else {
            double claimrate = mrealpay / mpassprem * 100;
            claimrate = Arith.round(claimrate, 2);
            TPayRate = claimrate + "";
        }

    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("LLAgeSpanCaseReport.vts", "printer"); //最好紧接着就初始化xml文档
    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                     tSrtTool.getDay() + "日";
    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(ManageCom);
    int pos1 = StartDate.indexOf("-");
    int pos2 = StartDate.lastIndexOf("-");
    String MStartDate = StartDate.substring(0,pos1)+"年"+StartDate.substring(pos1+1,pos2)+"月"+
                        StartDate.substring(pos2+1)+"日";
    pos1 = EndDate.indexOf("-");
    pos2 = EndDate.lastIndexOf("-");
    String MEndDate = EndDate.substring(0,pos1)+"年"+EndDate.substring(pos1+1,pos2)+"月"+
                        EndDate.substring(pos2+1)+"日";
    String ComName="";
    if(tLDComDB.getInfo())
        ComName=tLDComDB.getName();
//    String StaDate = Year+"年"+Month+"月";
    //PayColPrtBL模版元素
    texttag.add("SysDate", SysDate);
    texttag.add("TCaseNoM", TCaseNoM);
    texttag.add("TCaseNoF", TCaseNoF);
    texttag.add("TCaseNo", TCaseNo);
    texttag.add("TRealPayM", TRealPayM);
    texttag.add("TRealPayF", TRealPayF);
    texttag.add("TRealPay", TRealPay);
    texttag.add("TCaseRateM", TCaseRateM);
    texttag.add("TCaseRateF", TCaseRateF);
    texttag.add("TCaseRate", TCaseRate);
    texttag.add("TPayRateM", TPayRateM);
    texttag.add("TPayRateF", TPayRateF);
    texttag.add("TPayRate", TPayRate);
    texttag.add("ManageCom",ComName);
    texttag.add("StartDate",MStartDate);
    texttag.add("EndDate",MEndDate);
//    texttag.add("StaDate",StaDate);
    if (texttag.size() > 0) {
        xmlexport.addTextTag(texttag);
    }
    //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
//    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
    mResult.clear();
    mResult.addElement(xmlexport);
}

private void printRsuReport(){
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    String[] strCol;
    ListTable appColListTable = new ListTable();
    appColListTable.setName("Report");
    String[] Title = {
                     "AgeSpan", "TCASEM1", "TCASEF1", "TCASE1","TAMNTM1","TAMNTF1","TAMNT1",
                     "TCASEM2", "TCASEF2", "TCASE2","TAMNTM2","TAMNTF2","TAMNT2",
                     "TCASEM3", "TCASEF3", "TCASE3","TAMNTM3","TAMNTF3","TAMNT3",
                     "TCASEM4", "TCASEF4", "TCASE4","TAMNTM4","TAMNTF4","TAMNT4",
                     "TCASEM5", "TCASEF5", "TCASE5","TAMNTM5","TAMNTF5","TAMNT5"};
    int span = Integer.parseInt(AgeSpan);
    int age = span;
    int tempage=0;
    String exeSql = "select max(customerage) from llcase";
    ExeSQL esql = new ExeSQL();
    String tmaxage = esql.getOneValue(exeSql);
    int maxage = Integer.parseInt(tmaxage);
    while(tempage<maxage)
    {
        ExeSQL exesql = new ExeSQL();
        String sqlbase = "SELECT COUNT(A.CASENO),SUM(A1.REALPAY) FROM LLCASE A,LLCLAIM A1 WHERE " +
                         " A1.CASENO=A.CASENO and A.ENDCASEDATE IS NOT NULL AND A.CUSTOMERAGE>="+
                          tempage+" AND A.CUSTOMERAGE<"+ age+" AND substr(A.MNGCOM,1,length('"+
                          ManageCom+"'))='"+ManageCom+"' AND DATE(A.MAKEDATE)>=DATE('"+StartDate+
                          "') AND DATE(A1.MAKEDATE)<=DATE('"+EndDate+"')";
        String sqlp0 = " AND A.CUSTOMERSEX='0' ";
        String sqlp1 = " AND A.CUSTOMERSEX='1' ";
        String sqls01 = " AND A1.GIVETYPE='1' ";
        String sqls02 = " AND A1.GIVETYPE='2' ";
        String sqls03 = " AND A1.GIVETYPE='3' ";
        String sqls04 = " AND A1.GIVETYPE='4' ";
        String sqls05 = " AND A1.GIVETYPE='5' ";
        String sql0 = sqlbase + sqlp0 + sqls01;
        String sql1 = sqlbase + sqlp1 + sqls01;
        String sql2 = sqlbase + sqls01;
        String sql3 = sqlbase + sqlp0 + sqls02;
        String sql4 = sqlbase + sqlp1 + sqls02;
        String sql5 = sqlbase + sqls02;
        String sql6 = sqlbase + sqlp0 + sqls03;
        String sql7 = sqlbase + sqlp1 + sqls03;
        String sql8 = sqlbase + sqls03;
        String sql9 = sqlbase + sqlp0 + sqls04;
        String sql10 = sqlbase + sqlp1 + sqls04;
        String sql11 = sqlbase + sqls04;
        String sql12 = sqlbase + sqlp0 + sqls05;
        String sql13 = sqlbase + sqlp1 + sqls05;
        String sql14 = sqlbase + sqls05;
        SSRS COUNT0 = exesql.execSQL(sql0);
        SSRS COUNT1 = exesql.execSQL(sql1);
        SSRS COUNT2 = exesql.execSQL(sql2);
        SSRS COUNT3 = exesql.execSQL(sql3);
        SSRS COUNT4 = exesql.execSQL(sql4);
        SSRS COUNT5 = exesql.execSQL(sql5);
        SSRS COUNT6 = exesql.execSQL(sql6);
        SSRS COUNT7 = exesql.execSQL(sql7);
        SSRS COUNT8 = exesql.execSQL(sql8);
        SSRS COUNT9 = exesql.execSQL(sql9);
        SSRS COUNT10 = exesql.execSQL(sql10);
        SSRS COUNT11 = exesql.execSQL(sql11);
        SSRS COUNT12 = exesql.execSQL(sql12);
        SSRS COUNT13 = exesql.execSQL(sql13);
        SSRS COUNT14 = exesql.execSQL(sql14);
        String[][] temdata0 = null;
        String[][] temdata1 = null;
        String[][] temdata2 = null;
        String[][] temdata3 = null;
        String[][] temdata4 = null;
        String[][] temdata5 = null;
        String[][] temdata6 = null;
        String[][] temdata7 = null;
        String[][] temdata8 = null;
        String[][] temdata9 = null;
        String[][] temdata10 = null;
        String[][] temdata11 = null;
        String[][] temdata12 = null;
        String[][] temdata13 = null;
        String[][] temdata14 = null;
        if (COUNT0 != null && COUNT0.getMaxRow() > 0)
            temdata0 = COUNT0.getAllData();
        if (COUNT1 != null && COUNT1.getMaxRow() > 0)
            temdata1 = COUNT1.getAllData();
        if (COUNT2 != null && COUNT2.getMaxRow() > 0)
            temdata2 = COUNT2.getAllData();
        if (COUNT3 != null && COUNT3.getMaxRow() > 0)
            temdata3 = COUNT3.getAllData();
        if (COUNT4 != null && COUNT4.getMaxRow() > 0)
            temdata4 = COUNT4.getAllData();
        if (COUNT5 != null && COUNT5.getMaxRow() > 0)
            temdata5 = COUNT5.getAllData();
        if (COUNT6 != null && COUNT6.getMaxRow() > 0)
            temdata6 = COUNT6.getAllData();
        if (COUNT7 != null && COUNT7.getMaxRow() > 0)
            temdata7 = COUNT7.getAllData();
        if (COUNT8 != null && COUNT8.getMaxRow() > 0)
            temdata8 = COUNT8.getAllData();
        if (COUNT9 != null && COUNT9.getMaxRow() > 0)
            temdata9 = COUNT9.getAllData();
        if (COUNT10 != null && COUNT10.getMaxRow() > 0)
            temdata10 = COUNT10.getAllData();
        if (COUNT11 != null && COUNT11.getMaxRow() > 0)
            temdata11 = COUNT11.getAllData();
        if (COUNT12 != null && COUNT12.getMaxRow() > 0)
            temdata12 = COUNT12.getAllData();
        if (COUNT13 != null && COUNT13.getMaxRow() > 0)
            temdata13 = COUNT13.getAllData();
        if (COUNT14 != null && COUNT14.getMaxRow() > 0)
            temdata14 = COUNT14.getAllData();

            strCol = new String[31];
            strCol[0] = age+"岁";
            strCol[1] = temdata0[0][0];
            strCol[2] = temdata1[0][0];
            strCol[3] = temdata2[0][0];
            strCol[4] = (temdata0[0][1].equals("null"))?"0.00":temdata0[0][1];
            strCol[5] = (temdata1[0][1].equals("null"))?"0.00":temdata1[0][1];
            strCol[6] = (temdata2[0][1].equals("null"))?"0.00":temdata2[0][1];
            strCol[7] = temdata3[0][0];
            strCol[8] = temdata4[0][0];
            strCol[9] = temdata5[0][0];
            strCol[10] = (temdata3[0][1].equals("null"))?"0.00":temdata3[0][1];
            strCol[11] = (temdata4[0][1].equals("null"))?"0.00":temdata4[0][1];
            strCol[12] = (temdata5[0][1].equals("null"))?"0.00":temdata5[0][1];
            strCol[13] = temdata6[0][0];
            strCol[14] = temdata7[0][0];
            strCol[15] = temdata8[0][0];
            strCol[16] = (temdata6[0][1].equals("null"))?"0.00":temdata6[0][1];
            strCol[17] = (temdata7[0][1].equals("null"))?"0.00":temdata7[0][1];
            strCol[18] = (temdata8[0][1].equals("null"))?"0.00":temdata8[0][1];
            strCol[19] = temdata9[0][0];
            strCol[20] = temdata10[0][0];
            strCol[21] = temdata11[0][0];
            strCol[22] = ( temdata9[0][1].equals("null"))?"0.00":temdata9[0][1];
            strCol[23] = (temdata10[0][1].equals("null"))?"0.00":temdata10[0][1];
            strCol[24] = (temdata11[0][1].equals("null"))?"0.00":temdata11[0][1];
            strCol[25] = temdata12[0][0];
            strCol[26] = temdata13[0][0];
            strCol[27] = temdata14[0][0];
            strCol[28] = (temdata12[0][1].equals("null"))?"0.00":temdata12[0][1];
            strCol[29] = (temdata13[0][1].equals("null"))?"0.00":temdata13[0][1];
            strCol[30] = (temdata14[0][1].equals("null"))?"0.00":temdata14[0][1];
            appColListTable.add(strCol);
        tempage =age;
        age+=span;
    }
    //其它模版上单独不成块的信息
    ExeSQL exesql = new ExeSQL();
    String sqlbase = "SELECT COUNT(A.CASENO),SUM(A1.REALPAY) FROM LLCASE A,LLCLAIM A1 WHERE " +
                     " A1.CASENO=A.CASENO and A.ENDCASEDATE IS NOT NULL AND substr(A.MNGCOM,1,length('"+
                      ManageCom+"'))='"+ManageCom+"' AND DATE(A.MAKEDATE)>=DATE('"+StartDate+
                      "') AND DATE(A1.MAKEDATE)<=DATE('"+EndDate+"')";
    String sqlp0 = " AND A.CUSTOMERSEX='0' ";
    String sqlp1 = " AND A.CUSTOMERSEX='1' ";
    String sqls01 = " AND A1.GIVETYPE='1' ";
    String sqls02 = " AND A1.GIVETYPE='2' ";
    String sqls03 = " AND A1.GIVETYPE='3' ";
    String sqls04 = " AND A1.GIVETYPE='4' ";
    String sqls05 = " AND A1.GIVETYPE='5' ";
    String sql0 = sqlbase + sqlp0 + sqls01;
    String sql1 = sqlbase + sqlp1 + sqls01;
    String sql2 = sqlbase + sqls01;
    String sql3 = sqlbase + sqlp0 + sqls02;
    String sql4 = sqlbase + sqlp1 + sqls02;
    String sql5 = sqlbase + sqls02;
    String sql6 = sqlbase + sqlp0 + sqls03;
    String sql7 = sqlbase + sqlp1 + sqls03;
    String sql8 = sqlbase + sqls03;
    String sql9 = sqlbase + sqlp0 + sqls04;
    String sql10 = sqlbase + sqlp1 + sqls04;
    String sql11 = sqlbase + sqls04;
    String sql12 = sqlbase + sqlp0 + sqls05;
    String sql13 = sqlbase + sqlp1 + sqls05;
    String sql14 = sqlbase + sqls05;
    SSRS COUNT0 = exesql.execSQL(sql0);
    SSRS COUNT1 = exesql.execSQL(sql1);
    SSRS COUNT2 = exesql.execSQL(sql2);
    SSRS COUNT3 = exesql.execSQL(sql3);
    SSRS COUNT4 = exesql.execSQL(sql4);
    SSRS COUNT5 = exesql.execSQL(sql5);
    SSRS COUNT6 = exesql.execSQL(sql6);
    SSRS COUNT7 = exesql.execSQL(sql7);
    SSRS COUNT8 = exesql.execSQL(sql8);
    SSRS COUNT9 = exesql.execSQL(sql9);
    SSRS COUNT10 = exesql.execSQL(sql10);
    SSRS COUNT11 = exesql.execSQL(sql11);
    SSRS COUNT12 = exesql.execSQL(sql12);
    SSRS COUNT13 = exesql.execSQL(sql13);
    SSRS COUNT14 = exesql.execSQL(sql14);
    String[][] temdata0 = null;
    String[][] temdata1 = null;
    String[][] temdata2 = null;
    String[][] temdata3 = null;
    String[][] temdata4 = null;
    String[][] temdata5 = null;
    String[][] temdata6 = null;
    String[][] temdata7 = null;
    String[][] temdata8 = null;
    String[][] temdata9 = null;
    String[][] temdata10 = null;
    String[][] temdata11 = null;
    String[][] temdata12 = null;
    String[][] temdata13 = null;
    String[][] temdata14 = null;
    if (COUNT0 != null && COUNT0.getMaxRow() > 0)
        temdata0 = COUNT0.getAllData();
    if (COUNT1 != null && COUNT1.getMaxRow() > 0)
        temdata1 = COUNT1.getAllData();
    if (COUNT2 != null && COUNT2.getMaxRow() > 0)
        temdata2 = COUNT2.getAllData();
    if (COUNT3 != null && COUNT3.getMaxRow() > 0)
        temdata3 = COUNT3.getAllData();
    if (COUNT4 != null && COUNT4.getMaxRow() > 0)
        temdata4 = COUNT4.getAllData();
    if (COUNT5 != null && COUNT5.getMaxRow() > 0)
        temdata5 = COUNT5.getAllData();
    if (COUNT6 != null && COUNT6.getMaxRow() > 0)
        temdata6 = COUNT6.getAllData();
    if (COUNT7 != null && COUNT7.getMaxRow() > 0)
        temdata7 = COUNT7.getAllData();
    if (COUNT8 != null && COUNT8.getMaxRow() > 0)
        temdata8 = COUNT8.getAllData();
    if (COUNT9 != null && COUNT9.getMaxRow() > 0)
        temdata9 = COUNT9.getAllData();
    if (COUNT10 != null && COUNT10.getMaxRow() > 0)
        temdata10 = COUNT10.getAllData();
    if (COUNT11 != null && COUNT11.getMaxRow() > 0)
        temdata11 = COUNT11.getAllData();
    if (COUNT12 != null && COUNT12.getMaxRow() > 0)
        temdata12 = COUNT12.getAllData();
    if (COUNT13 != null && COUNT13.getMaxRow() > 0)
        temdata13 = COUNT13.getAllData();
    if (COUNT14 != null && COUNT14.getMaxRow() > 0)
        temdata14 = COUNT14.getAllData();

    String TCM1= temdata0[0][0];
    String TCF1= temdata1[0][0];
    String TC1= temdata2[0][0];
    String TMM1= temdata0[0][1];
    String TMF1= temdata1[0][1];
    String TM1= temdata2[0][1];
    String TCM2= temdata3[0][0];
    String TCF2= temdata4[0][0];
    String TC2= temdata5[0][0];
    String TMM2 = temdata3[0][1];
    String TMF2 = temdata4[0][1];
    String TM2 = temdata5[0][1];
    String TCM3 = temdata6[0][0];
    String TCF3 = temdata7[0][0];
    String TC3 = temdata8[0][0];
    String TMM3 = temdata6[0][1];
    String TMF3 = temdata7[0][1];
    String TM3 = temdata8[0][1];
    String TCM4 = temdata9[0][0];
    String TCF4 = temdata10[0][0];
    String TC4 = temdata11[0][0];
    String TMM4 = temdata9[0][1];
    String TMF4 = temdata10[0][1];
    String TM4 = temdata11[0][1];
    String TCM5 = temdata12[0][0];
    String TCF5 = temdata13[0][0];
    String TC5 = temdata14[0][0];
    String TMM5 = temdata12[0][1];
    String TMF5 = temdata13[0][1];
    String TM5 = temdata14[0][1];
    TMM1=(TMM1.equals("null"))?"0.00":TMM1;
    TMF1=(TMF1.equals("null"))?"0.00":TMF1;
    TM1=(TM1.equals("null"))?"0.00":TM1;
    TMM2=(TMM2.equals("null"))?"0.00":TMM2;
    TMF2=(TMF2.equals("null"))?"0.00":TMF2;
    TM2=(TM2.equals("null"))?"0.00":TM2;
    TMM3=(TMM3.equals("null"))?"0.00":TMM3;
    TMF3=(TMF3.equals("null"))?"0.00":TMF3;
    TM3=(TM3.equals("null"))?"0.00":TM3;
    TMM4=(TMM4.equals("null"))?"0.00":TMM4;
    TMF4=(TMF4.equals("null"))?"0.00":TMF4;
    TM4=(TM4.equals("null"))?"0.00":TM4;
    TMM5=(TMM5.equals("null"))?"0.00":TMM5;
    TMF5=(TMF5.equals("null"))?"0.00":TMF5;
    TM5=(TM5.equals("null"))?"0.00":TM5;

    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("LLSexCaseReport.vts", "printer"); //最好紧接着就初始化xml文档
    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                     tSrtTool.getDay() + "日";
    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(ManageCom);
    String ComName="";
    if(tLDComDB.getInfo())
        ComName=tLDComDB.getName();
    int pos1 = StartDate.indexOf("-");
    int pos2 = StartDate.lastIndexOf("-");
    String MStartDate = StartDate.substring(0,pos1)+"年"+StartDate.substring(pos1+1,pos2)+"月"+
                        StartDate.substring(pos2+1)+"日";
    pos1 = EndDate.indexOf("-");
    pos2 = EndDate.lastIndexOf("-");
    String MEndDate = EndDate.substring(0,pos1)+"年"+EndDate.substring(pos1+1,pos2)+"月"+
                        EndDate.substring(pos2+1)+"日";
//    String StaDate = Year+"年"+Month+"月";
    //PayColPrtBL模版元素
    texttag.add("SysDate", SysDate);
    texttag.add("TCM1", TCM1);
    texttag.add("TCF1", TCF1);
    texttag.add("TC1", TC1);
    texttag.add("TMM1", TMM1);
    texttag.add("TMF1", TMF1);
    texttag.add("TM1", TM1);
    texttag.add("TCM2", TCM2);
    texttag.add("TCF2", TCF2);
    texttag.add("TC2", TC2);
    texttag.add("TMM2", TMM2);
    texttag.add("TMF2", TMF2);
    texttag.add("TM2", TM2);
    texttag.add("TCM3", TCM3);
    texttag.add("TCF3", TCF3);
    texttag.add("TC3", TC3);
    texttag.add("TMM3", TMM3);
    texttag.add("TMF3", TMF3);
    texttag.add("TM3", TM3);
    texttag.add("TCM4", TCM4);
    texttag.add("TCF4", TCF4);
    texttag.add("TC4", TC4);
    texttag.add("TMM4", TMM4);
    texttag.add("TMF4", TMF4);
    texttag.add("TM4", TM4);
    texttag.add("TCM5", TCM5);
    texttag.add("TCF5", TCF5);
    texttag.add("TC5", TC5);
    texttag.add("TMM5", TMM5);
    texttag.add("TMF5", TMF5);
    texttag.add("TM5", TM5);
    texttag.add("ManageCom",ComName);
    texttag.add("StartDate",MStartDate);
    texttag.add("EndDate",MEndDate);
//    texttag.add("StaDate",StaDate);
    if (texttag.size() > 0) {
        xmlexport.addTextTag(texttag);
    }
    //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
//    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
    mResult.clear();
    mResult.addElement(xmlexport);

}

public static void main(String[] args) {
    TransferData PrintElement = new TransferData();
    PrintElement.setNameAndValue("StartDate", "2005-7-1");
    PrintElement.setNameAndValue("EndDate", "2005-7-31");
    PrintElement.setNameAndValue("ManageCom", "86110000");
    PrintElement.setNameAndValue("AgeSpan", "10");

    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86110000";
    tG.Operator = "001";
    VData tVData = new VData();
    tVData.addElement(PrintElement);
    tVData.addElement(tG);

    LLReportAgeUI tLLReportAgeUI = new LLReportAgeUI();
    tLLReportAgeUI.submitData(tVData, "RESULT");
}
}
