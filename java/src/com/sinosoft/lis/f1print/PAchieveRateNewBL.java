package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PAchieveRateNewBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 应收起始日期
	private String mStartDate = "";
	private String mEndDate = "";
	// 实收起始日期
	private String mActuStartDate = "";
	private String mActuEndDate = "";
	// 其他要素
	private String mSaleChnlType = "";
	private String mRiskType = "";
	private String mSaleChnl = "";
	private String mOrphans = "";
	private String mPayCount = "";
	private String mManageCom = "";

	private String mCount = "次数不限";

	// 子算法SQL
	private String msubpolsql = "";
	private String msubpaypersonsql = "";
	private String mactupaypersonsql = " 1=1 ";

	// 查询出的问题保单
	private String[][] mExcelData = null;

	// 数据初始化
	double sumInitPrem = 0;
	double sumInitCount = 0;
	double sumActuPrem = 0;
	double sumActuCount = 0;
	double sumCtPrem = 0;
	double sumCtCount = 0;
	double sumHjPrem = 0;
	double sumHjCount = 0;
	double sumPremRate = 0;
	double sumCountRate = 0;
	double sumCtRate = 0;
	double sumHjRate = 0;

	public PAchieveRateNewBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData, String cOperate) {
		// cOperate为1为普通保单,2为万能保单
		if (!cOperate.equals("1") && !cOperate.equals("2") && !cOperate.equals("3")) {
			buildError("submitData", "不支持的操作字符串");
			return null;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}
		
		if (cOperate.equals("1")) {
			// 准备所有要打印的数据，普通单
			mRiskType = "1";
		} else if (cOperate.equals("2")) {
			// 准备所有要打印的数据，万能
			mRiskType = "2";
		} else if (cOperate.equals("3")) {
			// 全部
			mRiskType = "3";
		} else {
			buildError("dealdata", "险种类型错误");
			return null;
		}

		// 获取各项条件SQL
		getsubsql();

		// 判断机构
		if (mManageCom.equals("0")) { // 二级机构 0-二级机构
			if (!getPrintData()) {
					return null;
			}
			
		} else { // 三级机构 1-三级机构
			if (!getPrintThirdData()) {
				return null;
			}			
		}

		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	public static void main(String[] args) {

		PAchieveRateNewBL tbl = new PAchieveRateNewBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2010-10-08");
		tTransferData.setNameAndValue("EndDate", "2010-10-08");
		tTransferData.setNameAndValue("ManageCom", "8644");
		tGlobalInput.ManageCom = "8644";
		tGlobalInput.Operator = "xp";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData, "1");
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\contactcompare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}

	/**
	 * 获取所有条件子SQL
	 */
	private void getsubsql() {
		// 管理机构
		//msubpolsql = " and lcp.managecom like '" + mManageCom + "%' ";
		//msubpaypersonsql = " and ljsp.managecom like '" + mManageCom + "%' ";

		// 应收时间
		msubpolsql += " and lcp.paytodate between '" + mStartDate + "' and '"
				+ mEndDate + "' ";
		msubpaypersonsql += " and ljsp.lastpaytodate between '" + mStartDate
				+ "' and '" + mEndDate + "' ";

		// 实收时间
		if ((!(mActuStartDate.equals("") || mActuStartDate == null))
				&& (!(mActuEndDate.equals("") || mActuEndDate == null))) {
			// msubpolsql=" and exists (select 1 from ljapayperson where polno=lcp.polno and confdate between '"
			// + mActuStartDate + "' and '" + mActuEndDate + "') ";
			// msubpolsql+=" and 1=2 ";
			// msubpaypersonsql+=" and ljsp.confdate between '" + mActuStartDate
			// + "' and '" + mActuEndDate + "' ";
			// 实收的保单仅影响实收费用
			mactupaypersonsql = "  ljsp.confdate between '" + mActuStartDate
					+ "' and '" + mActuEndDate + "' ";
		}

//		if (mSaleChnl.equals("99")) {
//			msubpolsql += " and lcp.salechnl not in ('01','03','04','07','08','11','13') ";
//			msubpaypersonsql += " and exists (select 1 from lccont where contno=ljsp.contno and salechnl not in ('01','03','04','07','08','11','13') union select 1 from lbcont where contno=ljsp.contno and salechnl not in ('01','03','04','07','08','11','13')) ";
//		} else 
		if (mSaleChnl.equals("00")) {
			System.out.println("选择为所有渠道");
			msubpolsql += " and lcp.salechnl in ('01','04','10','13','14','15') ";
			msubpaypersonsql += " and exists (select 1 from lccont where contno=ljsp.contno and salechnl in ('01','04','10','13','14','15') union select 1 from lbcont where contno=ljsp.contno and salechnl in ('01','04','10','13','14','15')) ";
		} else {
			msubpolsql += " and lcp.salechnl='" + mSaleChnl + "' ";
			msubpaypersonsql += " and exists (select 1 from lccont where contno=ljsp.contno and  salechnl='"
					+ mSaleChnl
					+ "' union select 1 from lbcont where contno=ljsp.contno and  salechnl='"
					+ mSaleChnl + "') ";
		}

		if(mSaleChnlType.equals("0")){
	    	msubpolsql+=" and lcp.salechnl in ('01','04','10','13','14','15') ";
	        msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl  in ('01','04','10','13','14','15')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('01','04','10','13','14','15')) ";
        } 
        else if(mSaleChnlType.equals("1")){
    	    msubpolsql+=" and lcp.salechnl in ('01','10') ";
            msubpaypersonsql+="  and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('01','10') union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('01','10'))  ";
        }
        else if(mSaleChnlType.equals("2")){
    	    msubpolsql+=" and lcp.salechnl in ('04','13') ";
            msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('04','13')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('04','13')) ";
        }
        else if(mSaleChnlType.equals("3")){
    	    msubpolsql+=" and lcp.salechnl in ('14','15') ";
            msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl in ('14','15')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('14','15')) ";
        }
		
		
		if (mOrphans.equals("1")) {
			msubpolsql += "  and not exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = lcp.AgentCode)<=(select SignDate from lccont where ContNo=lcp.ContNo union select SignDate from lbcont where ContNo=lcp.ContNo fetch first 1 row only) ";
			msubpaypersonsql += "  and not exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') and not exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') and  (select employdate from laagent where agentcode = ljsp.AgentCode)<=(select SignDate from lccont where ContNo=ljsp.ContNo  union select SignDate from lbcont where ContNo=ljsp.ContNo fetch first 1 row only)  ";
		} else if (mOrphans.equals("2")) {
			msubpolsql += " and (exists (select 1 from LAAscription where ContNo = lcp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = lcp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = lcp.AgentCode)>(select SignDate from lccont where ContNo=lcp.ContNo  union select SignDate from lbcont where ContNo=lcp.ContNo fetch first 1 row only)) ";
			msubpaypersonsql += " and (exists (select 1 from LAAscription where ContNo = ljsp.ContNo and AscripState = '3') or exists (select 1 from laagent where agentcode = ljsp.AgentCode and agentstate>='06') or (select employdate from laagent where agentcode = ljsp.AgentCode)>(select SignDate from lccont where ContNo=ljsp.ContNo  union select SignDate from lbcont where ContNo=ljsp.ContNo fetch first 1 row only)) ";
		}

		if (mPayCount.equals("2")) {
			mCount = "两次";
			msubpolsql += " and payintv > 0 and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=2 ";
			msubpaypersonsql += "  and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=2  ";
		} else if (mPayCount.equals("3")) {
			mCount = "三次";
			msubpolsql += " and payintv > 0 and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )=3 ";
			msubpaypersonsql += " and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )=3 ";
		} else if (mPayCount.equals("4")) {
			mCount = "四次及以上";
			msubpolsql += " and payintv > 0 and (((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 )>3 ";
			msubpaypersonsql += " and (((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo)))*12 + (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=ljsp.PolNo union select d.CValiDate from lbpol d where d.PolNo=ljsp.PolNo))))/ljsp.payintv +1 )>3 ";
		}
		
		if (mRiskType.equals("1")){
			msubpolsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4!='4') ";
			msubpaypersonsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4!='4') ";
		} else if (mRiskType.equals("2")){
			msubpolsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4='4') ";
			msubpaypersonsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L' and risktype4='4') ";
		} else if (mRiskType.equals("3")){
			msubpolsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L') ";
			msubpaypersonsql += " and riskcode in (select riskcode from lmriskapp where riskperiod = 'L') ";
		}

	}

  
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mStartDate = (String) mTransferData.getValueByName("StartDate");
		mEndDate = (String) mTransferData.getValueByName("EndDate");
		if (mStartDate.equals("") || mEndDate.equals("") || mStartDate == null
				|| mEndDate == null) {
			buildError("getInputData", "没有得到足够的信息:应收起始和终止日期不能为空！");
			return false;
		}
		mManageCom = (String) mTransferData.getValueByName("ManageCom");
		if (mManageCom.equals("") || mManageCom == null) {
			buildError("getInputData", "没有得到足够的信息:管理机构不能为空！");
			return false;
		}
		mActuStartDate = (String) mTransferData.getValueByName("ActuStartDate");
		mActuEndDate = (String) mTransferData.getValueByName("ActuEndDate");
		if ((!(mActuStartDate.equals("") || mActuStartDate == null))
				&& (mActuEndDate.equals("") || mActuEndDate == null)) {
			buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
			return false;
		}
		if ((!(mActuEndDate.equals("") || mActuEndDate == null))
				&& (mActuStartDate.equals("") || mActuStartDate == null)) {
			buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
			return false;
		}
		mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
		if (mSaleChnl.equals("") || mSaleChnl == null) {
			buildError("getInputData", "没有得到足够的信息:销售渠道不能为空！");
			return false;
		}
		mRiskType = (String) mTransferData.getValueByName("RiskType");
		if (mRiskType.equals("") || mRiskType == null) {
			buildError("getInputData", "没有得到足够的信息:险种类型不能为空！");
			return false;
		}
		mSaleChnlType = (String) mTransferData.getValueByName("SaleChnlType");
		if (mSaleChnlType.equals("") || mSaleChnlType == null) {
			buildError("getInputData", "没有得到足够的信息:保单类型不能为空！");
			return false;
		}
		mOrphans = (String) mTransferData.getValueByName("Orphans");
		if (mOrphans.equals("") || mOrphans == null) {
			buildError("getInputData", "没有得到足够的信息:保单状态不能为空！");
			return false;
		}
		mPayCount = (String) mTransferData.getValueByName("PayCount");
		if (mPayCount.equals("") || mPayCount == null) {
			buildError("getInputData", "没有得到足够的信息:缴费次数不能为空！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PAchieveRateNewBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = {
				{ "", "", "", "", "", "", "续期达成率统计报表", "", "", "", "", "", "",
						"" },
				{ "应缴开始日期：", mStartDate, "", "", "应缴结束日期：", mEndDate, "", "",
						"", "缴次：", mCount, "", "", "" },
				{ "机构代码", "机构名称", "应收保费（万元）", "应收件数", "实收保费（万元）", "实收件数", "退保保费（万元）",
						"退保件数", "缓缴保费（万元）", "缓缴件数", "保费达成率", "件数达成率", "保费退保率",
						"保费缓缴率" } };
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		String sqlManage = "select comcode from ldcom where comgrade='02' and Sign='1' order by comcode with ur";
		ExeSQL tExeSQL2 = new ExeSQL();
		SSRS tSSRS2 = tExeSQL2.execSQL(sqlManage.toString());

		int tRow = 0;
		tRow = tSSRS2.getMaxRow();

		for (int k = 1; k <= tRow; k++) {
			mManageCom = tSSRS2.GetText(k, 1);
	      
		      //获得EXCEL列信息      
		      String tSQL="select  " +
				" substr(x.managecode,1,4) , " +
		  		" (select name from ldcom where comcode=substr(x.managecode,1,4)) , " +
		  		" sum(x.premsum) , " +
		  		" count(distinct(case when x.premcount<>'' then x.premcount end)) , " +
		  		" sum(x.actusum) , " +
		  		" count(distinct(case when x.actucount<>'' then x.actucount end)) , " +
		  		" sum(x.ctprem) , " +
		  		" count(distinct(case when x.ctcount<>'' then x.ctcount end)) , " +
		  		" sum(x.delaysum) , " +
		  		" count(distinct(case when x.delaycount<>'' then x.delaycount end)) , " +
		  		" 0.0  , " +
		  		" 0.0  , " +
		  		" 0.0  , " +
		  		" 0.0    " +
		  		" from  " +
		  		" (select  " +
		  		" substr(ljsp.managecom,1,4) managecode, " +
		  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate and payplancode=ljsp.payplancode group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
		  		" ljsp.contno premcount, " +
		  		" (case when "+mactupaypersonsql+" then ljsp.sumactupaymoney else 0.0 end) actusum, " +
		  		" (case when "+mactupaypersonsql+" then ljsp.contno else '' end) actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
		  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
		  		" from ljapayperson ljsp " +
		  		" where paytypeflag='0'  " +
		  		msubpaypersonsql +
		  		" and ljsp.managecom like '" + mManageCom + "%' " +
		  		" and sumactupaymoney > 0 " +
		  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno  and paytype not like 'YE%') > 0 " +
		  		" union all " +
		  		" select  " +
		  		" substr(ljsp.managecom,1,4) managecode, " +
		  		" ljsp.sumduepaymoney premsum, " +
		  		" ljsp.contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
		  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
		  		" from ljapayperson ljsp " +
		  		" where paytypeflag is null  " +
		  		" and ljsp.payintv>0 " +
		  		" and exists (select 1 from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate) " +
		  		msubpaypersonsql +
		  		" and ljsp.managecom like '" + mManageCom + "%' " +
		  		" and exists (select 1 from ljapay lja where payno=ljsp.payno and incometype='10' and exists (select 1 from lpedoritem where edorno=lja.incomeno and edortype='FX')) " +
		  		" and sumactupaymoney > 0 " +
		//  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno) > 0 " +
		  		" union all " +
		  		" select  " +
		  		" substr(lcp.managecom,1,4) managecode, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) premsum, " +
		  		" contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" (case when stateflag='2' then prem else 0.0 end) delaysum, " +
		  		" (case when stateflag='2' then contno else '' end) delaycount " +
		  		"  from lcpol lcp  " +
		  		" where 1=1 " +
		  		" and conttype='1' " +
		  		msubpolsql +
		  		" and lcp.managecom like '" + mManageCom + "%' " +
		//  		" and stateflag='1' " +
		  		" and prem <> 0 " +
		  		" and paytodate<payenddate " +
		  		" union all " +
		  		" select  " +
		  		" substr(lcp.managecom,1,4) managecode, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) premsum, " +
		  		" contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) ctprem, " +
		  		" contno ctcount, " +
		  		" 0.0 delaysum, " +
		  		" '' delaycount " +
		  		"  from lbpol lcp where 1=1 " +
		  		msubpolsql +
		  		" and lcp.managecom like '" + mManageCom + "%' " +
		  		" and exists (select 1 from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate) " +
		  		" and exists (select 1 from lpedoritem where edorno=lcp.edorno and edortype in ('XT','WT','CT')) " +
		  		"  and prem <> 0 " +
		  		"  and paytodate<payenddate) as x " +
		  		"  group by x.managecode with ur ";
	      
			System.out.println("查询sql：" + tSQL.toString());
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(tSQL);
			if (tExeSQL.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "PAchieveRateNewBL";
				tError.functionName = "getPrintData";
				tError.errorMessage = "没有查询到需要下载的数据";
				mErrors.addOneError(tError);
				return false;
			}

			String[][] tGetData = null;

			if (tSSRS.MaxRow == 0) {

				String sqlManageName = "select comcode, Name, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' from LDCom where ComCode = '"
						+ mManageCom + "'";
				ExeSQL tExeSQL3 = new ExeSQL();
				SSRS tSSRS3 = tExeSQL3.execSQL(sqlManageName.toString());
				tGetData = tSSRS3.getAllData();
				System.out.println("拢共有这些条：" + tGetData.length);

				// 万元，小数点后两位
				tGetData[0][2] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][4] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][6] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][8] = String.valueOf(PubFun.setPrecision(0, "0.00"));

				// 百分数表示，小数点后1位
				tGetData[0][10] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%"; // 4舍6入5凑偶
				tGetData[0][11] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";
				tGetData[0][12] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";
				tGetData[0][13] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";

			} else {
		
				tGetData = tSSRS.getAllData();
	
				if (tGetData == null) {
					CError tError = new CError();
					tError.moduleName = "PAchieveRateNewBL";
					tError.functionName = "getPrintData";
					tError.errorMessage = "没有查询到需要输出的数据";
					return false;
				}
				
				System.out.println("拢共有这些条：" + tGetData.length);
	
				//机构代码、机构名称、应收保费、应收件数、实收保费、实收件数、退保保费、
				//退保件数、缓缴保费、缓缴件数、保费达成率、件数达成率、保费退保率、保费缓缴率
	
				int j = 0;

				double initPrem = Double.parseDouble(tGetData[j][2]) / 10000; // 应收保费
				double initCount = Double.parseDouble(tGetData[j][3]); // 应收件数
				double actuPrem = Double.parseDouble(tGetData[j][4]) / 10000; // 实收保费
				double actuCount = Double.parseDouble(tGetData[j][5]); // 实收件数
				double ctPrem = Double.parseDouble(tGetData[j][6]) / 10000; // 退保保费
				double ctCount = Double.parseDouble(tGetData[j][7]); // 退保件数
				double hjPrem = Double.parseDouble(tGetData[j][8]) / 10000; // 缓缴保费
				double hjCount = Double.parseDouble(tGetData[j][9]);// 缓缴件数

				double premRate = actuPrem / initPrem * 100;// 保费达成率：续期实收保费/应收保费。
				double countRate = actuCount / initCount * 100;// 件数达成率：续期实收件数/应收件数。
				double ctRate = ctPrem / initPrem * 100; // 退保率
				double hjRate = hjPrem / initPrem * 100;// 缓缴率
				
				System.out.println("有数没有啊：" + initPrem);

				// 万元，两位小数
				tGetData[j][2] = String.valueOf(PubFun.setPrecision(initPrem,
						"0.00"));
				tGetData[j][4] = String.valueOf(PubFun.setPrecision(actuPrem,
						"0.00"));
				tGetData[j][6] = String.valueOf(PubFun.setPrecision(ctPrem,
						"0.00"));
				tGetData[j][8] = String.valueOf(PubFun.setPrecision(hjPrem,
						"0.00"));

				// 百分数表示，一位小数
				tGetData[j][10] = String.valueOf(PubFun.setPrecision(premRate,
						"0.0"))
						+ "%"; // 4舍6入5凑偶
				tGetData[j][11] = String.valueOf(PubFun.setPrecision(countRate,
						"0.0"))
						+ "%";
				tGetData[j][12] = String.valueOf(PubFun.setPrecision(ctRate,
						"0.0"))
						+ "%";
				tGetData[j][13] = String.valueOf(PubFun.setPrecision(hjRate,
						"0.0"))
						+ "%";

				sumInitPrem = sumInitPrem + initPrem;
				sumInitCount = sumInitCount + initCount;
				sumActuPrem = sumActuPrem + actuPrem;
				sumActuCount = sumActuCount + actuCount;
				sumCtPrem = sumCtPrem + ctPrem;
				sumCtCount = sumCtCount + ctCount;
				sumHjPrem = sumHjPrem + hjPrem;
				sumHjCount = sumHjCount + hjCount;
				sumPremRate = sumActuPrem / sumInitPrem * 100;
				sumCountRate = sumActuCount / sumInitCount * 100;
				sumCtRate = sumCtPrem / sumInitPrem * 100;
				sumHjRate = sumHjPrem / sumInitPrem * 100;

			}
			if (mCreateExcelList.setData(tGetData, displayData) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}
		} 
			
		// ---------------------合计测试版
		int[] displayCount = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
		String tSumData[][] = new String[1][14];
		tSumData[0][0] = "合计";
		tSumData[0][1] = "总公司";
		tSumData[0][2] = String.valueOf(PubFun
				.setPrecision(sumInitPrem, "0.00"));
		tSumData[0][3] = String.valueOf(sumInitCount);
		tSumData[0][4] = String.valueOf(PubFun
				.setPrecision(sumActuPrem, "0.00"));
		tSumData[0][5] = String.valueOf(sumActuCount);
		tSumData[0][6] = String.valueOf(PubFun.setPrecision(sumCtPrem, "0.00"));
		tSumData[0][7] = String.valueOf(sumCtCount);
		tSumData[0][8] = String.valueOf(PubFun.setPrecision(sumHjPrem, "0.00"));
		tSumData[0][9] = String.valueOf(sumHjCount);
		tSumData[0][10] = String.valueOf(PubFun
				.setPrecision(sumPremRate, "0.0"))
				+ "%";
		tSumData[0][11] = String.valueOf(PubFun.setPrecision(sumCountRate,
				"0.0"))
				+ "%";
		tSumData[0][12] = String.valueOf(PubFun.setPrecision(sumCtRate, "0.0"))
				+ "%";
		tSumData[0][13] = String.valueOf(PubFun.setPrecision(sumHjRate, "0.0"))
				+ "%";

		// --------------------------------------
		if (mCreateExcelList.setData(tSumData, displayCount) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}
		return true;
	}

	private boolean getPrintThirdData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = {
				{ "", "", "", "", "", "", "续期达成率统计报表", "", "", "", "", "", "",
						"" },
				{ "应缴开始日期：", mStartDate, "", "", "应缴结束日期：", mEndDate, "", "",
						"", "缴次：", mCount, "", "", "" },
				{ "机构代码", "机构名称", "应收保费（万元）", "应收件数", "实收保费（万元）", "实收件数", "退保保费（万元）",
					"退保件数", "缓缴保费（万元）", "缓缴件数", "保费达成率", "件数达成率", "保费退保率",
					"保费缓缴率" } };
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		String sqlManage = "select comcode from ldcom where comgrade='03' and comcode != '86000000' and Sign='1' order by comcode with ur";
		ExeSQL tExeSQL2 = new ExeSQL();
		SSRS tSSRS2 = tExeSQL2.execSQL(sqlManage.toString());

		int tRow = 0;
		tRow = tSSRS2.getMaxRow();

		for (int k = 1; k <= tRow; k++) {
			mManageCom = tSSRS2.GetText(k, 1);

		      //获得EXCEL列信息      
		      String tSQL="select  " +
				" substr(x.managecode,1,8) , " +
		  		" (select name from ldcom where comcode=substr(x.managecode,1,8)) , " +
		  		" sum(x.premsum) , " +
		  		" count(distinct(case when x.premcount<>'' then x.premcount end)) , " +
		  		" sum(x.actusum) , " +
		  		" count(distinct(case when x.actucount<>'' then x.actucount end)) , " +
		  		" sum(x.ctprem) , " +
		  		" count(distinct(case when x.ctcount<>'' then x.ctcount end)) , " +
		  		" sum(x.delaysum) , " +
		  		" count(distinct(case when x.delaycount<>'' then x.delaycount end)) , " +
		  		" 0.0  , " +
		  		" 0.0  , " +
		  		" 0.0  , " +
		  		" 0.0    " +
		  		" from  " +
		  		" (select  " +
		  		" substr(ljsp.managecom,1,8) managecode, " +
		  		" (select sum(sumduepaymoney) from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate and payplancode=ljsp.payplancode group by getnoticeno order by getnoticeno fetch first 1 row only) premsum, " +
		  		" ljsp.contno premcount, " +
		  		" (case when "+mactupaypersonsql+" then ljsp.sumactupaymoney else 0.0 end) actusum, " +
		  		" (case when "+mactupaypersonsql+" then ljsp.contno else '' end) actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
		  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
		  		" from ljapayperson ljsp " +
		  		" where paytypeflag='0'  " +
		  		msubpaypersonsql +
		  		" and ljsp.managecom like '" + mManageCom + "%' " +
		  		" and sumactupaymoney > 0 " +
		  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno  and paytype not like 'YE%') > 0 " +
		  		" union all " +
		  		" select  " +
		  		" substr(ljsp.managecom,1,8) managecode, " +
		  		" ljsp.sumduepaymoney premsum, " +
		  		" ljsp.contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" nvl((select prem from lcpol where polno=ljsp.polno and stateflag ='2'),0.0) delaysum, " +
		  		" nvl((select contno from lcpol where polno=ljsp.polno and stateflag ='2'),'') delaycount " +
		  		" from ljapayperson ljsp " +
		  		" where paytypeflag is null  " +
		  		" and ljsp.payintv>0 " +
		  		" and exists (select 1 from ljspaypersonb where polno=ljsp.polno and lastpaytodate=ljsp.lastpaytodate) " +
		  		msubpaypersonsql +
		  		" and ljsp.managecom like '" + mManageCom + "%' " +
		  		" and exists (select 1 from ljapay lja where payno=ljsp.payno and incometype='10' and exists (select 1 from lpedoritem where edorno=lja.incomeno and edortype='FX')) " +
		  		" and sumactupaymoney > 0 " +
		//  		" and (select sum(sumactupaymoney) from ljapayperson where getnoticeno=ljsp.getnoticeno) > 0 " +
		  		" union all " +
		  		" select  " +
		  		" substr(lcp.managecom,1,8) managecode, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) premsum, " +
		  		" contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" 0.0 ctprem, " +
		  		" '' ctcount, " +
		  		" (case when stateflag='2' then prem else 0.0 end) delaysum, " +
		  		" (case when stateflag='2' then contno else '' end) delaycount " +
		  		"  from lcpol lcp  " +
		  		" where 1=1 " +
		  		" and conttype='1' " +
		  		msubpolsql +
		  		" and lcp.managecom like '" + mManageCom + "%' " +
		//  		" and stateflag='1' " +
		  		" and prem <> 0 " +
		  		" and paytodate<payenddate " +
		  		" union all " +
		  		" select  " +
		  		" substr(lcp.managecom,1,8) managecode, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) premsum, " +
		  		" contno premcount, " +
		  		" 0.0 actusum, " +
		  		" '' actucount, " +
		  		" nvl((select sum(sumduepaymoney) from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate group by getnoticeno order by getnoticeno fetch first 1 row only),prem) ctprem, " +
		  		" contno ctcount, " +
		  		" 0.0 delaysum, " +
		  		" '' delaycount " +
		  		"  from lbpol lcp where 1=1 " +
		  		msubpolsql +
		  		" and lcp.managecom like '" + mManageCom + "%' " +
		  		" and exists (select 1 from ljspaypersonb where polno=lcp.polno and lastpaytodate=lcp.paytodate) " +
		  		" and exists (select 1 from lpedoritem where edorno=lcp.edorno and edortype in ('XT','WT','CT')) " +
		  		"  and prem <> 0 " +
		  		"  and paytodate<payenddate) as x " +
		  		"  group by x.managecode with ur ";
      
			ExeSQL tExeSQL = new ExeSQL();
			System.out.println(tSQL);
			SSRS tSSRS = tExeSQL.execSQL(tSQL);
			if (tExeSQL.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "CreateExcelList";
				tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
				tError.errorMessage = "没有查询到需要下载的数据";
				mErrors.addOneError(tError);
				return false;
			}

			String[][] tGetData = null;

			if (tSSRS.MaxRow == 0) {
	
				String sqlManageName = "select comcode, Name, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' from LDCom where ComCode = '"
						+ mManageCom + "'";
				ExeSQL tExeSQL3 = new ExeSQL();
				SSRS tSSRS3 = tExeSQL3.execSQL(sqlManageName.toString());
				tGetData = tSSRS3.getAllData();
				System.out.println("拢共有这些条：" + tGetData.length);

				// 万元，小数点后两位
				tGetData[0][2] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][4] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][6] = String.valueOf(PubFun.setPrecision(0, "0.00"));
				tGetData[0][8] = String.valueOf(PubFun.setPrecision(0, "0.00"));

				// 百分数表示，小数点后1位
				tGetData[0][10] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%"; // 4舍6入5凑偶
				tGetData[0][11] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";
				tGetData[0][12] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";
				tGetData[0][13] = String.valueOf(PubFun.setPrecision(0, "0.0"))
						+ "%";

			} else {

				tGetData = tSSRS.getAllData();

				if (tGetData == null) {
					CError tError = new CError();
					tError.moduleName = "CreateExcelList";
					tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
					tError.errorMessage = "没有查询到需要输出的数据";
					return false;
				}

				System.out.println("拢共有这些条：" + tGetData.length);

				// 机构代码、机构名称、应收保费、应收件数、实收保费、实收件数、退保保费、
				// 退保件数、缓缴保费、缓缴件数、保费达成率、件数达成率、保费退保率、保费缓缴率

				int j = 0;

				double initPrem = Double.parseDouble(tGetData[j][2]) / 10000; // 应收保费
				double initCount = Double.parseDouble(tGetData[j][3]); // 应收件数
				double actuPrem = Double.parseDouble(tGetData[j][4]) / 10000; // 实收保费
				double actuCount = Double.parseDouble(tGetData[j][5]); // 实收件数
				double ctPrem = Double.parseDouble(tGetData[j][6]) / 10000; // 退保保费
				double ctCount = Double.parseDouble(tGetData[j][7]); // 退保件数
				double hjPrem = Double.parseDouble(tGetData[j][8]) / 10000; // 缓缴保费
				double hjCount = Double.parseDouble(tGetData[j][9]);// 缓缴件数

				double premRate = actuPrem / initPrem * 100;// 保费达成率：续期实收保费/应收保费。
				double countRate = actuCount / initCount * 100;// 件数达成率：续期实收件数/应收件数。
				double ctRate = ctPrem / initPrem * 100; // 退保率
				double hjRate = hjPrem / initPrem * 100;// 缓缴率
				
				// 万元，两位小数
				tGetData[j][2] = String.valueOf(PubFun.setPrecision(initPrem,
						"0.00"));
				tGetData[j][4] = String.valueOf(PubFun.setPrecision(actuPrem,
						"0.00"));
				tGetData[j][6] = String.valueOf(PubFun.setPrecision(ctPrem,
						"0.00"));
				tGetData[j][8] = String.valueOf(PubFun.setPrecision(hjPrem,
						"0.00"));

				// 百分数表示，一位小数
				tGetData[j][10] = String.valueOf(PubFun.setPrecision(premRate,
						"0.0"))
						+ "%"; // 4舍6入5凑偶
				tGetData[j][11] = String.valueOf(PubFun.setPrecision(countRate,
						"0.0"))
						+ "%";
				tGetData[j][12] = String.valueOf(PubFun.setPrecision(ctRate,
						"0.0"))
						+ "%";
				tGetData[j][13] = String.valueOf(PubFun.setPrecision(hjRate,
						"0.0"))
						+ "%";

				sumInitPrem = sumInitPrem + initPrem;
				sumInitCount = sumInitCount + initCount;
				sumActuPrem = sumActuPrem + actuPrem;
				sumActuCount = sumActuCount + actuCount;
				sumCtPrem = sumCtPrem + ctPrem;
				sumCtCount = sumCtCount + ctCount;
				sumHjPrem = sumHjPrem + hjPrem;
				sumHjCount = sumHjCount + hjCount;
				sumPremRate = sumActuPrem / sumInitPrem * 100;
				sumCountRate = sumActuCount / sumInitCount * 100;
				sumCtRate = sumCtPrem / sumInitPrem * 100;
				sumHjRate = sumHjPrem / sumInitPrem * 100;

			}
			if (mCreateExcelList.setData(tGetData, displayData) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}
		}

		// ---------------------合计测试版
		int[] displayCount = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
		String tSumData[][] = new String[1][14];
		tSumData[0][0] = "合计";
		tSumData[0][1] = "总公司";
		tSumData[0][2] = String.valueOf(PubFun
				.setPrecision(sumInitPrem, "0.00"));
		tSumData[0][3] = String.valueOf(sumInitCount);
		tSumData[0][4] = String.valueOf(PubFun
				.setPrecision(sumActuPrem, "0.00"));
		tSumData[0][5] = String.valueOf(sumActuCount);
		tSumData[0][6] = String.valueOf(PubFun.setPrecision(sumCtPrem, "0.00"));
		tSumData[0][7] = String.valueOf(sumCtCount);
		tSumData[0][8] = String.valueOf(PubFun.setPrecision(sumHjPrem, "0.00"));
		tSumData[0][9] = String.valueOf(sumHjCount);
		tSumData[0][10] = String.valueOf(PubFun
				.setPrecision(sumPremRate, "0.0"))
				+ "%";
		tSumData[0][11] = String.valueOf(PubFun.setPrecision(sumCountRate,
				"0.0"))
				+ "%";
		tSumData[0][12] = String.valueOf(PubFun.setPrecision(sumCtRate, "0.0"))
				+ "%";
		tSumData[0][13] = String.valueOf(PubFun.setPrecision(sumHjRate, "0.0"))
				+ "%";

		// --------------------------------------
		if (mCreateExcelList.setData(tSumData, displayCount) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}
		return true;
	}

  
  
	/**
	 * 得到通过传入参数得到相关信息
	 * 
	 * @param strComCode
	 * @return
	 * @throws Exception
	 */
	private boolean getExcelData() {

		String tSQL = "select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'"
				+ " and managecom like '' and signdate between '' and '' with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}
		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			return false;
		}
		return true;
	}

}

