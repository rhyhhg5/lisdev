package com.sinosoft.lis.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Hashtable;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCCGrpSpecDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCNationDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LMRiskFormDB;
import com.sinosoft.lis.pubfun.EasyScanQueryBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.vschema.LCCGrpSpecSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCGrpSuccorBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();

    private LDComSchema mLDComSchema = new LDComSchema();

    private String mTemplatePath = null;

    private String mOutXmlPath = null;

    private XMLDatasets mXMLDatasets = null;

    private String[][] mPicFile = null;

    private TransferData mTransferData;

    private MMap Map = new MMap();

    //  private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    //    private Vector m_vGrpPolNo = new Vector();
    public LCGrpSuccorBL()
    {
    }

    /**
     * 主程序
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            //判定打印类型
            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            //全局变量赋值
            mOperate = cOperate;
            //得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            //正常打印处理
            if (mOperate.equals("PRINT"))
            {
                //新建一个xml对象
                mXMLDatasets = new XMLDatasets();
                mXMLDatasets.createDocument();
                //数据准备
                if (!getPrintData())
                {
                    return false;
                }
            }
            //重打处理
            if (mOperate.equals("REPRINT"))
            {
                if (!getRePrintData())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //正常打印
        if (mOperate.equals("PRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData
                    .getObjectByObjectName("LCGrpContSchema", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mTemplatePath = (String) mTransferData
                    .getValueByName("TemplatePath");
            mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");

        }
        //重打模式
        if (mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData
                    .getObjectByObjectName("LCGrpContSchema", 0));
        }
        return true;
    }

    /**
     * 返回信息
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception
    {
        //团单Schema
        LCGrpContSchema tLCGrpContSchema = null;
        //原则上一次只打印一个团单，当然可以一次多打
        tLCGrpContSchema = this.mLCGrpContSchema;

        //取得该团单下的所有投保险种信息
        LCGrpPolSet tLCGrpPolSet = getRiskList(tLCGrpContSchema.getGrpContNo());

        //正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(tLCGrpPolSet))
        {
            return false;
        }

        //将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        //    mResult.add(tLCGrpPolSet);//其实没必要更新这个表
        this.mLCGrpContSchema.setPrintCount(1);
        mResult.add(this.mLCGrpContSchema); //只更新这个表就好拉
        mResult.add(mXMLDatasets); //xml数据流
        mResult.add(mGlobalInput); //全局变量
        mResult.add(mOutXmlPath); //输出目录
        mResult.add(mPicFile); //保单影印件数组
        //    mResult.add(mDocFile); //协议影印件数组

        String XmlFile = mOutXmlPath + "/printdata/data/brief/J01-"
                + this.mLCGrpContSchema.getGrpContNo() + ".xml";
        String XmlFile_Card = mOutXmlPath + "/printdata/data/brief/J02-"
                + this.mLCGrpContSchema.getGrpContNo() + ".xml";

        // 对不同的打印类别（保险卡，保单），生成相应的文件。
        // 只是针对境外救援而言。其它类型简易件不适用。
        String tPrtFlag = (String) mTransferData.getValueByName("prtFlag");
        //        if (!creatXmlFile(XmlFile))
        //        {
        //            return false;
        //        }
        //        creatXmlFile(XmlFile_Card);
        if ("Notice".equals(tPrtFlag))
        {
            if (!creatXmlFile(XmlFile))
            {
                return false;
            }
        }
        else if ("Card".equals(tPrtFlag))
        {
            creatXmlFile(XmlFile_Card);
        }
        // --------------------------------------------

        if (this.mOperate.equals("REPRINT"))
        {
            if (!deleteFile(XmlFile))
            {
                return false;
            }
            if (!deleteFile(XmlFile_Card))
            {
                return false;
            }
        }
        //更新保单打印次数
        Map.put("update lcgrpcont set printcount=1  where  prtno='"
                + this.mLCGrpContSchema.getPrtNo() + "' and printcount<>1",
                "UPDATE");
        System.out.println("update lcgrpcont set printcount=1  where  prtno='"
                + this.mLCGrpContSchema.getPrtNo() + "' and printcount<>1");
        mResult.clear();
        mResult.add(Map);
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCGrpSuccorBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("ImpartToICDBL end");

        return true;
    }

    /**
     * deleteFile
     *
     * @param XmlFile String
     * @return boolean
     */
    private boolean deleteFile(String XmlFile)
    {
        String file = StrTool.replaceEx(XmlFile, ".xml", ".pdf");
        file = StrTool.replaceEx(file, "brief", "briefpdf");
        try
        {
            File tFile = new File(file);
            if (tFile.exists())
            {
                tFile.delete();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("deleteFile", "重打保单失败，原因是，ｐｄｆ文件存在但是删除原ｐｄｆ文件失败！");
            return false;
        }
        return true;
    }

    /**
     * creatXmlFile
     *
     * @param XmlFile String
     * @throws Exception
     * @return boolean
     */
    private boolean creatXmlFile(String XmlFile)
    {
        InputStream ins = mXMLDatasets.getInputStream();

        try
        {
            FileOutputStream fos = new FileOutputStream(XmlFile);
            int n = 0;
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1)
            {
                fos.write(c, 0, n);
            }
            fos.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("creatXmlFile", "生成Xml文件失败！");
            return false;
        }
        return true;
    }

    /**
     * 查询协议影印件数据
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    //  private boolean getScanDoc(XMLDataset cXmlDataset)
    //          throws
    //          Exception
    //  {
    //    XMLDataList tXmlDataList = new XMLDataList();
    //    //标签对象，团单需要的影印件有两种类型
    //    tXmlDataList.setDataObjectID("DocFile");
    //
    //    //标签中的Header
    //    tXmlDataList.addColHead("FileUrl");
    //    tXmlDataList.addColHead("HttpUrl");
    //    tXmlDataList.addColHead("PageIndex");
    //    tXmlDataList.buildColHead();
    //
    //    VData tVData = new VData();
    //    tVData.add(mLCGrpContSchema.getPrtNo());
    //    tVData.add("12");
    //    tVData.add("TB");
    //    tVData.add("TB1002");
    //    EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();
    //    if (!tEasyScanQueryBL.submitData(tVData, "QUERY||MAIN"))
    //    {
    //      System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
    ////            buildError("getScanDoc", "查询保单影印件出错！");
    ////            return false;
    ////            return true;
    //    }
    //    else
    //    {
    //      tVData.clear();
    //      tVData = tEasyScanQueryBL.getResult();
    //      //Http信息对象
    //      VData tUrl = (VData) tVData.get(0);
    //      //页面信息对象
    //      VData tPages = (VData) tVData.get(1);
    //      String tStrUrl = "";
    //      String tStrPages = "";
    //      //根据查询结果初始化影印件信息数组
    //      mDocFile = new String[tUrl.size()][2];
    //      for (int nIndex = 0; nIndex < tUrl.size(); nIndex++)
    //      {
    //        tStrUrl = (String) tUrl.get(nIndex);
    //        tStrPages = (String) tPages.get(nIndex);
    //        tStrUrl = tStrUrl.substring(0,
    //                                    tStrUrl.lastIndexOf(".")) + ".tif";
    //        mDocFile[nIndex][0] = tStrUrl;
    //        //协议影印件信息，采用此命名方法
    //        mDocFile[nIndex][1] = "Agreement_" +
    //                              mLCGrpContSchema.getGrpContNo() +
    //                              "_" + tStrPages + ".tif";
    //        tXmlDataList.setColValue("FileUrl", mDocFile[nIndex][1]);
    //        tXmlDataList.setColValue("HttpUrl", tStrUrl);
    //        tXmlDataList.setColValue("PageIndex", nIndex);
    //        tXmlDataList.insertRow(0);
    //      }
    //    }
    //    cXmlDataset.addDataObject(tXmlDataList);
    //    return true;
    //  }
    /**
     * 查询影印件数据
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getScanPic(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        //标签对象，团单需要的影印件有两种类型
        tXmlDataList.setDataObjectID("PicFile");

        //标签中的Header
        tXmlDataList.addColHead("FileUrl");
        tXmlDataList.addColHead("HttpUrl");
        tXmlDataList.addColHead("PageIndex");
        tXmlDataList.buildColHead();

        VData tVData = new VData();
        tVData.add(mLCGrpContSchema.getPrtNo());
        tVData.add("12");
        tVData.add("TB");
        tVData.add("TB1002");
        EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();
        if (!tEasyScanQueryBL.submitData(tVData, "QUERY||MAIN"))
        {
            System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
            //            buildError("getScanPic", "查询保单影印件出错！");
            //            return false;
            //            return true;
        }
        else
        {
            tVData.clear();
            tVData = tEasyScanQueryBL.getResult();
            //Http信息对象
            VData tUrl = (VData) tVData.get(0);
            //页面信息对象
            VData tPages = (VData) tVData.get(1);
            String tStrUrl = "";
            String tStrPages = "";
            //根据查询结果初始化影印件信息数组
            mPicFile = new String[tUrl.size()][2];
            for (int nIndex = 0; nIndex < tUrl.size(); nIndex++)
            {
                tStrUrl = (String) tUrl.get(nIndex);
                tStrPages = (String) tPages.get(nIndex);
                tStrUrl = tStrUrl.substring(0, tStrUrl.lastIndexOf("."))
                        + ".tif";
                mPicFile[nIndex][0] = tStrUrl;
                //保单影印件信息，采用此命名方法
                mPicFile[nIndex][1] = mLCGrpContSchema.getGrpContNo() + "_"
                        + tStrPages + ".tif";
                tXmlDataList.setColValue("FileUrl", mPicFile[nIndex][1]);
                tXmlDataList.setColValue("HttpUrl", tStrUrl);
                tXmlDataList.setColValue("PageIndex", nIndex);
                tXmlDataList.insertRow(0);
            }
        }
        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getSpecDoc(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList xmlDataList = new XMLDataList();
        //添加xml一个新的对象Term
        xmlDataList.setDataObjectID("LCCGrpSpec");
        //添加Head单元内的信息
        xmlDataList.addColHead("RowID");
        xmlDataList.addColHead("SpecContent");
        xmlDataList.buildColHead();
        //查询合同下的特约信息
        LCCGrpSpecDB tLCCGrpSpecDB = new LCCGrpSpecDB();
        tLCCGrpSpecDB.setProposalGrpContNo(this.mLCGrpContSchema
                .getProposalGrpContNo());
        LCCGrpSpecSet tLCCGrpSpecSet = tLCCGrpSpecDB.query();
        for (int i = 1; i <= tLCCGrpSpecSet.size(); i++)
        {
            xmlDataList.setColValue("RowID", i);
            xmlDataList.setColValue("SpecContent", tLCCGrpSpecSet.get(i)
                    .getSpecContent());
            xmlDataList.insertRow(0);
        }
        cXmlDataset.addDataObject(xmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * @param cXmlDataset XMLDataset
     * @param cLCGrpPolSet LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getTerm(XMLDataset cXmlDataset, LCGrpPolSet cLCGrpPolSet)
            throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        //添加xml一个新的对象Term
        tXmlDataList.setDataObjectID("Term");
        //添加Head单元内的信息
        tXmlDataList.addColHead("PrintIndex");
        tXmlDataList.addColHead("TermName");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.addColHead("DocumentName");
        tXmlDataList.buildColHead();

        //设置查询对象
        ExeSQL tExeSQL = new ExeSQL();
        //查询保单下的险种条款，根据条款级别排序
        //查询合同下的全部唯一主条款信息
        String tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCGrpPol where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo()
                + "') and ItemType = '0'";
        SSRS tSSRS = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            tXmlDataList.setColValue("PrintIndex", i);
            tXmlDataList.setColValue("TermName", tSSRS.GetText(i, 1));
            tXmlDataList.setColValue("FileName", "0");
            tXmlDataList.setColValue("DocumentName", tSSRS.GetText(i, 2));
            tXmlDataList.insertRow(0);
            //查询当前主条款下的责任条款信息
            tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCGrpPol where GrpContNo = '"
                    + this.mLCGrpContSchema.getGrpContNo()
                    + "') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where FileName = '"
                    + tSSRS.GetText(i, 2) + "')";
            tSSRS2 = tExeSQL.execSQL(tSql);
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
            {
                tXmlDataList.setColValue("PrintIndex", i);
                tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                tXmlDataList.setColValue("FileName", "1");
                tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                tXmlDataList.insertRow(0);
            }
        }

        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 正常打印流程
     * @param cLCGrpPolSet LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        //xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        //团个单标志，1个单，2团单
        tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));

        //获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo())
        {
            //只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError())
            {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //默认填入空
            tXMLDataset.setTemplate("");
        }
        else
        {
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }

        //查询个单合同表信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        //根据合同号查询
        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpContDB.getInfo())
        {
            System.out
                    .println("getInfo" + tLCGrpContDB.mErrors.getErrContent());
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();
        if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("0"))
        {
            mXMLDatasets.setDTDName(this.mLCGrpContSchema.getGrpContNo()
                    + ".dtd");
        }
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCGrpContSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpContNo",
                this.mLCGrpContSchema.getGrpContNo()));
        tXMLDataset.addDataObject(new XMLDataTag("XI_ManageCom", mLCGrpContSchema.getManageCom()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.PrtNo",
                this.mLCGrpContSchema.getPrtNo()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AppntNo",
                this.mLCGrpContSchema.getAppntNo()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Peoples2",
                this.mLCGrpContSchema.getPeoples2()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpName",
                this.mLCGrpContSchema.getGrpName()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SignDate", PubFun
                .getCurrentDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CValiDate",
                this.mLCGrpContSchema.getCValiDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SumPrem", String
                .valueOf(this.mLCGrpContSchema.getSumPrem())));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Remark", StrTool
                .cTrim(this.mLCGrpContSchema.getRemark())));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CInValiDate",
                this.mLCGrpContSchema.getCInValiDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.DegreeType",
                this.mLCGrpContSchema.getDegreeType()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Operator",
                this.mLCGrpContSchema.getOperator()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AgentCode",
                this.mLCGrpContSchema.getAgentCode()));
        //保全补打 qulq 2007-11-28
        tXMLDataset.addDataObject(new XMLDataTag("LostTimes",
                this.mLCGrpContSchema.getLostTimes()));
        tXMLDataset.addDataObject(new XMLDataTag("BQRePrintDate",
                                         com.sinosoft.lis.bq.CommonBL.decodeDate(PubFun.getCurrentDate())
                                         ));


        //tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Uwoperator", this.mLCGrpContSchema.getUWOperator()));
        //如果是卡单型险种需要查询抵达国家
        if (!StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals(""))
        {
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++)
            {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size())
                {
                    Nation += ",";
                }
            }
            tXMLDataset.addDataObject(new XMLDataTag("Nation", Nation));
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLDGrpDB.getInfo();
            tXMLDataset.addDataObject(new XMLDataTag("GrpEnglithName", tLDGrpDB
                    .getGrpEnglishName()));
            /** 增加签单机构 */
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
            if (!tLDComDB.getInfo())
            {
                buildError("getGrpPolDataSet", "查询失败！");
                return false;
            }
            tXMLDataset.addDataObject(new XMLDataTag("SignCom", tLDComDB
                    .getLetterServiceName()));
            tXMLDataset.addDataObject(new XMLDataTag("SignComEnName", tLDComDB
                    .getEName()));
            tXMLDataset.addDataObject(new XMLDataTag("EnPostAddress", tLDComDB
                    .getEAddress()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePhone", tLDComDB
                    .getPhone()));
            //        tXMLDataset.addDataObject(new XMLDataTag("ServicePhone",
            //                                                 tLDComDB.getServicePhone()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePostAddress",
                    tLDComDB.getServicePostAddress()));
        }

        //查询投保人信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        //根据合同号查询
        tLCGrpAppntDB.setGrpContNo(tLCGrpContDB.getGrpContNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAppntDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCGrpAppntSchema = tLCGrpAppntDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCGrpAppntSchema);

        //查询投保人的地址信息
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        //根据投保人编码和地址编码查询
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAddressDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人地址信息失败！");
            return false;
        }
        this.mLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCGrpAddressSchema);

        //查询管理机构信息
        LDComDB tLDComDB = new LDComDB();
        //根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDComDB.getInfo())
        {
            buildError("getInfo", "查询管理机构信息失败！");
            return false;
        }
        this.mLDComSchema = tLDComDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLDComSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.Name",
                this.mLDComSchema.getName()));

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCGrpContSchema.getAgentCode());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo())
        {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        tXMLDataset.addDataObject(new XMLDataTag("LAAgent.Name", mLAAgentSchema
                .getName()));
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLAAgentSchema);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.mLCGrpContSchema.getAgentGroup());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLABranchGroupDB.getInfo())
        {
            buildError("getInfo", "查询销售机构信息失败！");
            return false;
        }
        this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLABranchGroupSchema);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "select cvalidate,cinvalidate from LCGrpCont where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("BeginDate", tSSRS.GetText(1,
                1)));
        tXMLDataset
                .addDataObject(new XMLDataTag("EndDate", tSSRS.GetText(1, 2)));

        //暂交费收据号
        tSql = "select distinct TempFeeNo from LJTempFee where OtherNo = '"
                + this.mLCGrpContSchema.getGrpContNo()
                + "' and OtherNoType = '7'";
        tSSRS = tExeSQL.execSQL(tSql);
        //        if (!mLCGrpContSchema.getAppFlag().equals("9")) {
        //            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo",
        //                    tSSRS.GetText(1, 1))); //收据号
        //        } else {
        tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", "")); //收据号
        //        }

        //总保费
        /*tSql = "select prem from LCGrpCont where GrpContNo = '" +
         this.mLCGrpContSchema.getGrpContNo() + "'";
         tSSRS = tExeSQL.execSQL(tSql);
         tXMLDataset.addDataObject(new XMLDataTag("SumPrem",
         format(Double.
         parseDouble(tSSRS.GetText(1, 1))))); //总保费*/

        //操作人员编码和操作人员姓名
        tSql = "select UserCode,UserName from LDUser where UserCode in (Select Operator from LJTempFee where OtherNo = '"
                + this.mLCGrpContSchema.getGrpContNo()
                + "' and OtherNoType = '7')";
        tSSRS = tExeSQL.execSQL(tSql);
        //        if (!((String) mLCGrpContSchema.getAppFlag()).equals("9")) {
        //            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode",
        //                    tSSRS.GetText(1, 1)));
        //            tXMLDataset.addDataObject(new XMLDataTag("Operator",
        //                    tSSRS.GetText(1, 2)));
        //        } else {
        tXMLDataset.addDataObject(new XMLDataTag("OperatorCode",
                mGlobalInput.Operator));
        String tempSql = "select UserName from lduser where usercode='"
                + mGlobalInput.Operator + "'";
        tSSRS = tExeSQL.execSQL(tempSql);
        tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS
                .GetText(1, 1)));

        //        }
        //产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genInsuredList(tXMLDataset))
        {
            return false;
        }

        //获取特约信息
        if (!getSpecDoc(tXMLDataset))
        {
            return false;
        }

        //获取条款信息
        if (!getTerm(tXMLDataset, cLCGrpPolSet))
        {
            return false;
        }

        //获取协议影印件信息,以后打印协议时用
        //    if (!getScanDoc(tXMLDataset))
        //    {
        //      return false;
        //    }

        //产生团单下被保人清单
        if (!genInsuredDetail(tXMLDataset))
        {
            return false;
        }

        //获取保单影印件信息
        //下面4行信息注掉，用于以后的补充协议打印
        if (!getScanPic(tXMLDataset))
        {
            return false;
        }
        //获取被保险人急救医疗卡信息
        if (!StrTool.cTrim(this.mLCGrpContSchema.getCardFlag()).equals(""))
        {
            if (!genInsuredCard(tXMLDataset))
            {
                return false;
            }
        }
        //获取定点医院信息
        // if (!getHospital(tXMLDataset))
        //{
        //  return false;
        // }
        // System.out.println("定点医院明细准备完毕。。。");

        //获取发票信息
        //      if (!this.mLCGrpContSchema.getAppFlag().equals("9"))
        //      {
        //        if (!getInvoice(tXMLDataset))
        //        {
        //          return false;
        //        }
        //      }
        if (!getEnglishName(tXMLDataset))
        {
            return false;
        }

        return true;
    }

    /**
     * getEnglishName
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean getEnglishName(XMLDataset tXMLDataset)
    {
        String sql = "select riskcode from lmriskapp where  RiskProp='G'  and RiskType8='1' order by RiskCode ";
        SSRS ssrs = (new ExeSQL()).execSQL(sql);
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            String tRiskCode = ssrs.GetText(i, 1);
            String sql_EnName = "select RiskEnName from LMRisk a,LCGrpPol b where a.RiskCode=b.RiskCode and b.GrpContNo='"
                    + mLCGrpContSchema.getGrpContNo()
                    + "' and b.RiskCode='"
                    + tRiskCode + "'";
            String tEnName = (new ExeSQL()).getOneValue(sql_EnName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "EnName", tEnName));
            String sql_ZhName = "select RiskName from LMRisk a,LCGrpPol b where a.RiskCode=b.RiskCode and b.GrpContNo='"
                    + mLCGrpContSchema.getGrpContNo()
                    + "' and b.RiskCode='"
                    + tRiskCode + "'";
            String tZhName = (new ExeSQL()).getOneValue(sql_ZhName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "ZhName", tZhName));
        }
        return true;
    }

    /**
     * 根据个单合同号，查询明细信息
     * @param tXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getHospital(XMLDataset tXmlDataset) throws Exception
    {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件，mszTemplatePath为模板所在的应用路径
        tTemplateFile = mTemplatePath + "Hospital.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getHospital", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable tHshData = new Hashtable();
            //将变量_ContNo的值赋给xml文件
            //            tHshData.put("_AREACODE", this.mLDComSchema.getRegionalismCode());
            tHshData.put("_AREACODE", "11%%");
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), tHshData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            tXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            //出错处理
            buildError("genHospital", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询明细信息
     * 责任信息、特约信息、个人险种明细、险种条款信息等
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredList(XMLDataset cXmlDataset) throws Exception
    {
        //        String tSql =
        //                "select ContPlanCode,ContPlanName from LCContPlan where GrpContNo = '" +
        //                this.mLCGrpContSchema.getGrpContNo() +
        //                "' and ContPlanCode != '00'";
        StringBuffer tSBql = new StringBuffer(256);
        tSBql
                .append("select ContPlanCode,ContPlanName from LCContPlan where GrpContNo = '");
        tSBql.append(this.mLCGrpContSchema.getGrpContNo());
        tSBql.append("' and ContPlanCode != '00' and ContPlanCode != '11'");

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
        //循环获取计划信息
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            cXmlDataset.addDataObject(new XMLDataTag(tSSRS.GetText(i, 1), tSSRS
                    .GetText(i, 2)));
        }

        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        if (StrTool.cTrim(this.mLCGrpContSchema.getCardFlag()).equals(""))
        {
            tTemplateFile = mTemplatePath + "GrpContPrint.xml";
        }
        else
        {
            tTemplateFile = mTemplatePath + "GrpContPrintCard.xml";
        }
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredList", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            //将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception
    {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsured.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredDetail", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            //将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset cXmlDataset) throws Exception
    {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsuredCard.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            //将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 获取发票信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getInvoice(XMLDataset cXmlDataset) throws Exception
    {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpContInvoice.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getInvoice", "XML配置文件不存在！");
            return false;
        }
        try
        {
            XMLDataList tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("Money");
            tXMLDataList.addColHead("ChinaMoney");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("ChinaMoney", PubFun
                    .getChnMoney(this.mLCGrpContSchema.getPrem()));
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);

            Hashtable tHashData = new Hashtable();
            //将变量GrpContNo的值赋给xml文件
            tHashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), tHashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);

            cXmlDataset.addDataObject(new XMLDataTag("Today", PubFun
                    .getCurrentDate())); //签发日期
            //            cXmlDataset.addDataObject(new XMLDataTag("RMB",
            //                                                     PubFun.getChnMoney(this.
            //                    mLCGrpContSchema.getPrem()))); //保费大写
            //收款人，目前系统提供操作员信息

            //查询缴费方式信息
            //根据合同号，查询缴费记录信息
            //            String tSql =
            //                    "select distinct TempFeeNo from LJTempFee where OtherNo = '" +
            //                    this.mLCGrpContSchema.getGrpContNo() +
            //                    "' and OtherNoType = '7'";
            StringBuffer tSBql = new StringBuffer(256);
            tSBql
                    .append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("' and OtherNoType = '7'");

            //            String tWhere = " where a.TempFeeNo in ('";
            StringBuffer tBWhere = new StringBuffer(" where a.TempFeeNo in ('");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                //将每笔缴费的缴费号串连起来作为查询条件
                if (i == tSSRS.getMaxRow())
                {
                    //                    tWhere = tWhere + tSSRS.GetText(i, 1) + "')";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("')");
                }
                else
                {
                    //                    tWhere = tWhere + tSSRS.GetText(i, 1) + "','";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("','");
                }
            }
            //            cXmlDataset.addDataObject(new XMLDataTag("TempFeeNo",
            //                    tSSRS.GetText(1, 1))); //收据号

            //根据上面的查询条件，查询缴费方式为现金的数据
            //            tSql = "select a.ConfMakeDate from LJTempFeeClass a" +
            //                   tWhere + " and a.PayMode = '1' order by a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select a.ConfMakeDate from LJTempFeeClass a");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode = '1' order by a.ConfMakeDate");

            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0)
            {
                //添加缴费方式为现金的标签
                tXMLDataList = new XMLDataList();
                //添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Cash");
                //添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                //此缴费方式只需知道确认日期
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tXMLDataList.setColValue("PayMode", "现金");
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS
                            .GetText(i, 1));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            //根据上面的查询条件，查询缴费方式不是现金的数据
            //            tSql =
            //                    "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a,LDCode b" +
            //                    tWhere + " and a.PayMode <> '1' and a.BankCode = b.Code and b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate";
            //        tSBql.setLength(0);
            //        tSBql.append(
            //                "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a,LDCode b");
            //        tSBql.append(tBWhere);
            //        tSBql.append(" and a.PayMode <> '1' and a.BankCode = b.Code and b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate");
            //
            //        tSSRS = tExeSQL.execSQL(tSBql.toString());
            //        if (tSSRS.getMaxRow() > 0)
            //        {
            //          //添加缴费方式为支票的标签
            //          tXMLDataList = new XMLDataList();
            //          //添加xml一个新的对象Term
            //          tXMLDataList.setDataObjectID("Check");
            //          //添加Head单元内的信息
            //          tXMLDataList.addColHead("PayMode");
            //          tXMLDataList.addColHead("Bank");
            //          tXMLDataList.addColHead("AccName");
            //          tXMLDataList.addColHead("BankAccNo");
            //          tXMLDataList.addColHead("ConfMakeDate");
            //          tXMLDataList.buildColHead();
            //          //此标签需要获得银行、户名、帐户的信息
            //          for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            //          {
            //            tXMLDataList.setColValue("PayMode", "银行转帐");
            //            tXMLDataList.setColValue("Bank", tSSRS.GetText(i, 1));
            //            tXMLDataList.setColValue("AccName", tSSRS.GetText(i, 2));
            //            tXMLDataList.setColValue("BankAccNo", tSSRS.GetText(i, 3));
            //            tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 4));
            //            tXMLDataList.insertRow(0);
            //          }
            //          cXmlDataset.addDataObject(tXMLDataList);
            //        }

            //添加xml结束标签
            tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("End");
            tXMLDataList.addColHead("Flag");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("Flag", "1");
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);
            //            cXmlDataset.addDataObject(new XMLDataTag("End", "1"));
        }
        catch (Exception e)
        {
            buildError("getInvoice", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 取得团单下的全部LCGrpPol表数据
     * @param cGrpContNo String
     * @return LCGrpPolSet
     * @throws Exception
     */
    private static LCGrpPolSet getRiskList(String cGrpContNo) throws Exception
    {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();

        tLCGrpPolDB.setGrpContNo(cGrpContNo);
        //由于LCGrpCont和LCGrpPol为一对多的关系，所以采用query方法
        tLCGrpPolSet = tLCGrpPolDB.query();

        return tLCGrpPolSet;
    }

    /**
     * 获取补打保单的数据
     * @return boolean
     * @throws Exception
     */
    private boolean getRePrintData() throws Exception
    {
        //        String tCurDate = PubFun.getCurrentDate();
        //        String tCurTime = PubFun.getCurrentTime();

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        //查询LCPolPrint表，获取要补打的保单数据
        String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '"
                + mLCGrpContSchema.getGrpContNo() + "'";
        System.out.println(tSql);
        tSSRS = tExeSQL.execSQL(tSql);

        if (tExeSQL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tExeSQL.mErrors);
            throw new Exception("获取打印数据失败");
        }
        if (tSSRS.MaxRow < 1)
        {
            throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
        }

        mResult.clear();
        mResult.add(mLCGrpContSchema);

        //   LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        //   if (!tLCGrpContF1PBLS.submitData(mResult, "REPRINT"))
        //   {
        //     if (tLCGrpContF1PBLS.mErrors.needDealError())
        //     {
        //       mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
        //     }
        //     throw new Exception("保存数据失败");
        //   }

        // 取打印数据
        Connection conn = null;

        try
        {
            DOMBuilder tDOMBuilder = new DOMBuilder();
            Element tRootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            //            String tSql = "";
            java.sql.Blob tBlob = null;
            //            CDB2Blob tCDB2Blob = new CDB2Blob();

            tSql = " and MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);

            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            //BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            Element tElement = tDOMBuilder.build(tBlob.getBinaryStream())
                    .getRootElement();
            tElement = new Element("DATASET").setMixedContent(tElement
                    .getMixedContent());
            tRootElement.addContent(tElement);

            ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
            XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "UTF-8");
            tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

            //            mResult.clear();
            //            mResult.add(new ByteArrayInputStream(tByteArrayOutputStream.toByteArray()));
            ByteArrayInputStream tByteArrayInputStream = new ByteArrayInputStream(
                    tByteArrayOutputStream.toByteArray());

            //仅仅重新生成xml打印数据，影印件信息暂时不重新获取
            String FilePath = mOutXmlPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            //根据合同号生成打印数据存放文件夹
            FilePath = mOutXmlPath + "/printdata" + "/"
                    + mLCGrpContSchema.getGrpContNo();
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            //根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mLCGrpContSchema.getGrpContNo()
                    + ".xml";
            FileOutputStream fos = new FileOutputStream(XmlFile);
            //此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            while ((n = tByteArrayInputStream.read()) != -1)
            {
                fos.write(n);
            }
            fos.close();

            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

}
