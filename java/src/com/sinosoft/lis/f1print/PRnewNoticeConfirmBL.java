package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPENoticeSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PRnewNoticeConfirmBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String mOperate = "";
    private VData mResult = new VData();
    private boolean PatchFlag = false;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    public PRnewNoticeConfirmBL()
    {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!saveData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PRnewNoticeConfirmBL tPRnewNoticeConfirmBL = new PRnewNoticeConfirmBL();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo("86110020030110002734");
        VData tVData = new VData();
        tVData.addElement(tLOPRTManagerSchema);
        tPRnewNoticeConfirmBL.submitData(tVData, "PRINT");
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));

        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BodyCheckConfirmBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean saveData()
    {
        //根据印刷号查询打印队列中的纪录
        //mLOPRTManagerSchema
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (!tLOPRTManagerDB.getInfo())
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");

        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setDoneTime(CurrentTime);

        mLOPRTManagerSchema.setExeCom(mGlobalInput.ComCode);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);

        if (mLOPRTManagerSchema.getPatchFlag() == null)
        {
            PatchFlag = false;
        }
        else if (mLOPRTManagerSchema.getPatchFlag().equals("0"))
        {
            PatchFlag = false;
        }
        else if (mLOPRTManagerSchema.getPatchFlag().equals("1"))
        {
            PatchFlag = true;
        }

        mResult.add(mLOPRTManagerSchema);

        PRnewNoticeConfirmBLS tPRnewNoticeConfirmBLS = new
                PRnewNoticeConfirmBLS();
        tPRnewNoticeConfirmBLS.submitData(mResult, mOperate);
        if (tPRnewNoticeConfirmBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tPRnewNoticeConfirmBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode
     * @return
     * @throws Exception
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    /**
     * 根据主险保单号码查询主险保单
     * @param tMainPolNo
     * @return LCPolSchema
     */
    private LCPolSchema queryMainPol(String tMainPolNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tMainPolNo);
        tLCPolDB.setMainPolNo(tMainPolNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet == null)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        if (tLCPolSet.size() == 0)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        return tLCPolSet.get(1);
    }

}