package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TextTag;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAAnnuityBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mAnnuityYear = "";

    private String mAnnuityMonth = "";

    private String mManageCom = "";

    private String mManageComName = "";

    private XmlExport mXmlExport = null;

    private SSRS mSSRS = new SSRS();

    private ListTable mListTable = new ListTable();

    public LAAnnuityBL() {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        this.mOperate = cOperate;

        System.out.println("1111111111111111111111111111111111" + mOperate);

        if (mOperate.equals("") || (!mOperate.equals("PRINT"))) {
            buildError("submitData", "数据不完整！");
            return false;
        }
        System.out.println("2222222222222222222222222222222");
        if (!getInputData(cInputData)) {
            buildError("getInputData", "数据不完整！");
            return false;
        }

        if (!getListData()) {
            return false;
        }

        if (!getPrintData()) {
            this.buildError("getPrintData", "查询数据失败！");
            return false;
        }
        return true;

    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {
            System.out.println("33333333333333333");
            this.mInputData = (VData) cInputData;
            System.out.println("4444444444444444444444");
            this.mGlobalInput.setSchema((GlobalInput) mInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            System.out.println("55555555555555555555555");
            this.mTransferData = (TransferData) mInputData.
                                 getObjectByObjectName("TransferData", 0);
            System.out.println("2222222222222222222222222222222");
        } catch (Exception ex) {
            this.mErrors.addOneError("数据不完整！");
            return false;
        }

        mAnnuityYear = (String) mTransferData.getValueByName("AnnuityYear");

        mAnnuityMonth = (String) mTransferData.getValueByName("AnnuityMonth");

        mManageCom = (String) mTransferData.getValueByName("ManageCom");

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getListData() {
        StringBuffer sql = new StringBuffer();

        ExeSQL tExeSQL = new ExeSQL();

        String calNo = mAnnuityYear + mAnnuityMonth;

        sql.append(
                "SELECT sum(a.K04),a.managecom FROM lawage a,ldcom b WHERE a.managecom=b.comcode");
        sql.append(" and a.indexcalno ='" + calNo + "'");
        sql.append(" and  a.managecom like '" + mManageCom + "%'");
        sql.append(
                " and  b.sign='1' and a.BranchType='1' and a.BranchType2='01' group by  a.managecom ORDER BY  a.managecom");

        System.out.println(sql.toString());

        this.mSSRS = tExeSQL.execSQL(sql.toString());

        if (tExeSQL.mErrors.needDealError()) {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "查询XML数据出错！";

            this.mErrors.addOneError(tCError);

            return false;
        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {

        TextTag tTextTag = new TextTag();

        this.mXmlExport = new XmlExport();

        this.mXmlExport.createDocument("LAAnnuityReport.vts", "printer");

        String CurrentData = PubFun.getCurrentDate();

        String CurrentTime = PubFun.getCurrentTime();

        if (!this.getManageCom()) {
            return false;
        }

        tTextTag.add("MakeData", CurrentData);
        tTextTag.add("MakeTime", CurrentTime);
        tTextTag.add("tName", mManageComName);
        tTextTag.add("AnnuityYear", mAnnuityYear);
        tTextTag.add("AnnuityMonth", mAnnuityMonth);

        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        if (!getListTable()) {
            return false;
        }

        String[] title = {"销售机构名称", "公司应付养老金"};

        mXmlExport.addListTable(this.mListTable, title);
        mXmlExport.outputDocumentToFile("c:\\", "Test111");
        this.mResult.clear();
        this.mResult.addElement(mXmlExport);

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getListTable() {

        if (this.mSSRS.getMaxRow() > 0) {
            System.out.println("666666666666666666666");
            double dCount = 0;

            System.out.println(mSSRS.getMaxRow());
            System.out.println(mSSRS.getMaxCol());
//            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
//
//                String[] info = new String[2];
//
//                for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
//
//                    if (mSSRS.GetText(i, 1).length() == 2) {
//                        info[0] = this.mManageCom;
//                        for (int k = 1; k <= mSSRS.getMaxRow(); k++) {
//                            double dTwo = 0;
//                            if ((mSSRS.GetText(i, 1).equals(mManageCom)) ||
//                                (
//                                        mSSRS.GetText(i,
//                                    1).equals(mManageCom + "00000000"))) {
//                                dTwo = dTwo +
//                                       Double.parseDouble(mSSRS.GetText(i, 2));
//
//                            }
//                            info[1] = String.valueOf(dTwo);
//                        }
//                    }
//
//                }
//                this.mListTable.add(info);
//                System.out.println("00000000000000000000" + mSSRS.GetText(i, 1));
//                //dCount = dCount + Double.parseDouble(mSSRS.GetText(i, 2));
//                System.out.println("00000000000000000000");
//            }

            if (this.mManageCom.length() == 2) {
//                86000000
//                double dTwo = 0;
                int intTwo = 0;
//                for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
//                    if ((mSSRS.GetText(i, 2).equals(mManageCom)) ||
//                        (mSSRS.GetText(i, 2).equals(mManageCom + "000000"))) {
//                        dTwo = dTwo + Double.parseDouble(mSSRS.GetText(i, 1));
//                        intTwo++;
//                    }
//                }
//                String[] info = new String[2];
//                info[0] = mManageCom;
//                info[1] = String.valueOf(dTwo);
//
//                mListTable.add(info);

                String strSub = "";

                System.out.println("7878787878787878" + String.valueOf(intTwo));
                System.out.println(mSSRS.getMaxRow());

                if (intTwo != mSSRS.getMaxRow()) {
                    for (int j = 1; j <= mSSRS.getMaxRow(); j++) {

                        if (mSSRS.GetText(j, 2).length() >= 4) {

                            if (!strSub.equals(mSSRS.GetText(j, 2).substring(0,
                                    4))) {

                                strSub = mSSRS.GetText(j, 2).substring(0, 4);
                                double dThree = 0;
                                for (int k = 1; k <= mSSRS.getMaxRow(); k++) {


                                    if (mSSRS.GetText(k, 2).length() >= 4) {
                                        if (mSSRS.GetText(j, 2).substring(0, 4).
                                            equals(mSSRS.GetText(k, 2).
                                                substring(0,
                                                4))) {
                                            dThree = dThree +
                                                    Double.parseDouble(mSSRS.
                                                    GetText(k, 1));
                                        }

                                    }

                                }

                                String[] infoTwo = new String[2];
                                infoTwo[0] = strSub;
                                infoTwo[1] = String.valueOf(dThree);

                                mListTable.add(infoTwo);

                            }

                        }

                    }
                }

            } else {

                double dCountOne = 0;
                for (int x = 1; x <= mSSRS.getMaxRow(); x++) {
                    dCountOne = dCountOne +
                                Double.parseDouble(mSSRS.GetText(x, 1));

                    String[] infoThree = new String[2];
                    infoThree[0] = mManageCom;
                    infoThree[1] = String.valueOf(dCountOne);
                    this.mListTable.add(infoThree);

                }
            }
            for (int l = 1; l <= mSSRS.getMaxRow(); l++) {
                dCount = dCount + Double.parseDouble(mSSRS.GetText(l, 1));
            }

            String[] infoCount = new String[2];
            infoCount[0] = "合计";
            infoCount[1] = String.valueOf(dCount);

            this.mListTable.add(infoCount);

            this.mListTable.setName("ZT");

        } else {
            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的数据！";

            this.mErrors.addOneError(tCError);

            return false;

        }

        return true;
    }

    /**
     *
     * @return boolean
     */
    private boolean getManageCom() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "查询XML数据出错！";

            this.mErrors.addOneError(tCError);

            return false;
        }

        if (mManageCom.equals("86")) {
            this.mManageComName = "";
        } else {
            this.mManageComName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;

    }

    /**
     *
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void buildError(String cFunction, String cErrorMsg) {

        CError tError = new CError();

        tError.moduleName = "LAAnnuityBL";
        tError.functionName = cFunction;
        tError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {

    }
}
