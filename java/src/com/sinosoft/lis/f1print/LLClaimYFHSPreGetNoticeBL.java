package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
//import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class LLClaimYFHSPreGetNoticeBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private int index;
    private String mOperate = "";
    private String mPolNo;
    private String mRiskName;
    private String mAppntName;
    private String mInsuredNameLable = "客户姓名";
    private String mInsuredName;
    private String mInsuredNo;
    private String mOtherNo;
    private String mDrawer;
    private String mDrawerID;
    private String mBnfName;
    private String mPayItem;
    private String mCaseNo;
    private String mRgtClass = "";
    private boolean mNeedStore = false;
    private String mOper = "";
    private LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = null;
    private String mflag = null;

    public LLClaimYFHSPreGetNoticeBL() {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
//        if (mOperate.equals("PRINT")) {
            if (!dealData()) {
                return false;
            }
//        }
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        if (cOperate.equals("INSERT") || mNeedStore) {
            if (!dealPrintMag("INSERT")) {
                return false;
            }
        }
        if(mOper.equals("UPDATE")){   //第二次调用批打程序时执行打印次数加一。
            if (!dealPrintMag(mOper)) {
                return false;
            }
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        //用实收号和code（lp009）在打印管理表里查找给付凭证打印记录
        tLOPRTManagerDB.setStandbyFlag2(mLJSPaySchema.getGetNoticeNo());
        tLOPRTManagerDB.setCode("lp009");
        tLOPRTManagerSet = tLOPRTManagerDB.query();
        if (tLOPRTManagerSet == null || tLOPRTManagerSet.size() == 0) {
            index = 0;
            mNeedStore = true;
        } else {
            if (tLOPRTManagerDB.mErrors.needDealError()) {
                mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                buildError("dealData", "在取得LOPRTManager的数据时发生错误");
                return false;
            }
            //打印次数
            index = (int) tLOPRTManagerSet.get(1).getPrintTimes();
            mNeedStore = false;
            if (tLOBatchPRTManagerSchema != null) {
                mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
                mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
                mOper = "UPDATE";
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);

        //批次印管理表信息
        tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema = ((LOBatchPRTManagerSchema)
                                    cInputData.getObjectByObjectName(
                                            "LOBatchPRTManagerSchema", 0));

        if (ttLOPRTManagerSchema == null) {
            if (tLOBatchPRTManagerSchema == null) {
                System.out.println("---------为空--初次打印入口！");
                mLJSPaySchema.setSchema((LJSPaySchema) cInputData.
                                        getObjectByObjectName("LJSPaySchema", 0));
  //              mRgtClass = "";
  //              System.out.println("mRgtClass:"+mRgtClass);

                if (mLJSPaySchema == null) {
                    buildError("getInputData", "为空--初次打印入口没有得到足够的信息！");
                    return false;
                }
            } else {
                //批次打印的调用入口
                mCaseNo = tLOBatchPRTManagerSchema.getOtherNo();
                String tGetNoticeNo = tLOBatchPRTManagerSchema.getStandbyFlag1(); //
                mLJSPaySchema.setOtherNo(mCaseNo);
                mLJSPaySchema.setGetNoticeNo(tGetNoticeNo);
//                mLJSPaySchema.setPayMode("0"); //借用交费方式字段传递申请类型
                mRgtClass = tLOBatchPRTManagerSchema.getStandbyFlag2();

                if (tGetNoticeNo == null || tGetNoticeNo == "") {
                    buildError("getInputData", "为空--批次打印入口没有得到足够的信息！");
                    return false;
                }
            }
            return true;
        } else {
            //PDF入口
            System.out.println("---------不为空--PDF入口！");
            LJSPayDB mLJSPayDB = new LJSPayDB();
            mLJSPayDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
            //取出申请类型
            mRgtClass = ttLOPRTManagerSchema.getStandbyFlag3();
            if (!mLJSPayDB.getInfo()) {
                mErrors.addOneError("PDF入口LJSPayDB传入的数据不完整。");
                return false;
            }
            mLJSPaySchema = mLJSPayDB.getSchema();
            return true;
        }
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean print(String tPolNo) {
        mPolNo = tPolNo;
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mPolNo);
        if (!tLCPolDB.getInfo()) {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(mPolNo);
            if (!tLBPolDB.getInfo()) {
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLCPolSchema, tLBPolDB.getSchema());
        } else {
            tLCPolSchema.setSchema(tLCPolDB.getSchema());
        }
        mAppntName = tLCPolSchema.getAppntName();
        mInsuredName = tLCPolSchema.getInsuredName();
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
        if (!tLMRiskDB.getInfo()) {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            buildError("Print", "在取得险种名称时发生错误");
            return false;
        }
        mRiskName = tLMRiskDB.getRiskName();
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setPolNo(mPolNo);
        tLCBnfDB.setBnfType("0");
        LCBnfSet tLCBnfSet = new LCBnfSet();
        tLCBnfSet.set(tLCBnfDB.query());
        if (tLCBnfDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLCBnfDB.mErrors);
            buildError("Print", "在取得受益人时发生错误");
            return false;
        }
        if (tLCBnfSet.size() == 0) {
            mBnfName = "法定";
        } else {
            mBnfName = tLCBnfSet.get(1).getName();

            for (int i = 1; i < tLCBnfSet.size(); i++) {
                mBnfName += "、" + tLCBnfSet.get(i + 1).getName();
            }
        }

        return true;
    }
    private boolean getPrintData() {
        String tOtherNoType = "";
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setSchema(mLJSPaySchema);
        if (!tLJSPayDB.getInfo()) {

            mErrors.copyAllErrors(tLJSPayDB.mErrors);
            buildError("getPrintData", "案件为团体统一给付或尚未给付确认，不能打印给付凭证");
            return false;
        }
        tOtherNoType = tLJSPayDB.getOtherNoType();
        mOtherNo = tLJSPayDB.getOtherNo();
        System.out.println("otherno:" + tLJSPayDB.getOtherNo() +
                           "  othernotype:" + tLJSPayDB.getOtherNoType());       
       
        if (tOtherNoType.equals("Y")) {
            mPayItem = "预付赔款回收";
            if ("0".equals(mRgtClass)) {
                //个案
            	LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
            	tLLPrepaidClaimDB.setPrepaidNo(tLJSPayDB.getOtherNo());
                if (!tLLPrepaidClaimDB.getInfo()) {
                    mErrors.copyAllErrors(tLLPrepaidClaimDB.mErrors);
                    buildError("getPrintData", "在取得理赔预付信息时发生错误！");
                    return false;
                } 
                String tSql ="select GrpName,AppntNo from lcgrpcont where grpcontno='"+ tLLPrepaidClaimDB.getGrpContNo() +"'"
	  				+" union select GrpName,AppntNo from lbgrpcont where grpcontno='"+ tLLPrepaidClaimDB.getGrpContNo() +"'";
		    	ExeSQL exeSQL = new ExeSQL();
		        SSRS ssrs = exeSQL.execSQL(tSql);
		        if(exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||ssrs.getMaxRow()<=0)
		        {
		        	buildError("print", "保单"+tLLPrepaidClaimDB.getGrpContNo()+"信息查询失败！");
		            return false;
		        }
      
                mInsuredName = ssrs.GetText(1, 1);
                mInsuredNo = ssrs.GetText(1, 2);
                
                mDrawer = tLJSPayDB.getAccName();
                mDrawerID = "";
            }
            if ("1".equals(mRgtClass)) {
                mInsuredNameLable = "单位名称";
                //团体案件LLRegister
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                tLLRegisterDB.setRgtNo(tLJSPayDB.getOtherNo());
                if (!tLLRegisterDB.getInfo()) {
                    mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    buildError("getPrintData", "在取得理赔信息时发生错误");
                    return false;

                } else {
                    mInsuredNo = tLLRegisterDB.getCustomerNo(); //团体客户号
                  
                    mDrawer = tLJSPayDB.getAccName();
            		mDrawerID = "";

                    mInsuredName = tLLRegisterDB.getGrpName();
                }

            }
        String Money = new DecimalFormat("0.00").format((double) (tLJSPayDB.getSumDuePayMoney()));

        ListTable tlistTable = new ListTable();
        System.out.println("money:" + Money);
        String strArr[] = null;
        tlistTable.setName("GET");
        strArr = new String[4];
        strArr[0] = mPayItem;
        strArr[1] = Money;
        strArr[2] = "";
        strArr[3] = "";
        tlistTable.add(strArr);
        String CurrentDate = PubFun.getCurrentDate();
        int a = CurrentDate.lastIndexOf("-");
        String tYear = CurrentDate.substring(0, 4);
        String tMonth = CurrentDate.substring(5, a);
        String tDay = CurrentDate.substring(a + 1, CurrentDate.length());
        String tDate = tYear + "年" + tMonth + "月" + tDay + "日";

        String sql = "select username from lduser where usercode = '" +
                     mGlobalInput.Operator + "'";
        ExeSQL exesql = new ExeSQL();
        String opername = exesql.getOneValue(sql);
        
        String Sql ="select  paymode  from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where  otherno ='"+ tLJSPayDB.getGetNoticeNo()+"') with ur ";                 
        String tpaymode=  new ExeSQL().getOneValue(Sql);//支付方式
       
        String sqlMode ="select CodeName from ldcode where CodeType = 'paymode' and Code='" +tpaymode+ "'";
        String PayMode = exesql.getOneValue(sqlMode);

        String sqlAgent = "select Name from LAAgent where  AgentCode='" + tLJSPayDB.getAgentCode() + "'";
        String AgentName = exesql.getOneValue(sqlAgent);
        
        String AgentCodes="select getUniteCode(agentcode) from LAAgent where AgentCode='" + tLJSPayDB.getAgentCode() + "'";
        String tAgentCodes = exesql.getOneValue(AgentCodes);
        
        String sqlAgentgroup = "select Name from labranchgroup where  Agentgroup='"  + tLJSPayDB.getAgentGroup() + "'";
        String tAgentGroupName = exesql.getOneValue(sqlAgentgroup);

        System.out.println(mInsuredName);
        System.out.println(mInsuredNo);
        System.out.println(mOtherNo);
        System.out.println(mPayItem);
        System.out.println(mDrawer);
        System.out.println(mDrawerID);
        System.out.println(tDate);
        String AppealRemark = "";

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag.add("JetFormType", "lp009");
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
            texttag.add("previewflag", "0");
        }else{
            texttag.add("previewflag", "1");
        }
        xmlexport.createDocument("GetCredence.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("BarCode1", mLJSPaySchema.getGetNoticeNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("BarCode2", mLJSPaySchema.getGetNoticeNo());
        texttag.add("BarCodeParam2"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("PolNo", mPolNo);
        texttag.add("PolNo", mPolNo);
        texttag.add("RiskName", mRiskName);
        texttag.add("AppntName", mAppntName);
        texttag.add("InsuredNameLable", mInsuredNameLable);
        texttag.add("InsuredName", mInsuredName);
        texttag.add("InsuredNo", mInsuredNo);
        texttag.add("Operator", opername);
        texttag.add("OtherNo", mOtherNo);
        texttag.add("PayItem", mPayItem);
        texttag.add("Drawer", mDrawer);
        texttag.add("DrawerID", mDrawerID);
        texttag.add("BnfName", mBnfName);
        texttag.add("PayMode", PayMode);
        texttag.add("AgentName", AgentName);
        texttag.add("AgentCode",tAgentCodes);
        texttag.add("AgentGroupName", tAgentGroupName);

        texttag.add("Year", tYear);
        texttag.add("Month", tMonth);
        texttag.add("Day", tDay);
        texttag.add("GetNoticeNo", tLJSPayDB.getGetNoticeNo());
        texttag.add("Date", tDate);
        texttag.add("Index", index + 1);
        texttag.add("AppealRemark", AppealRemark);
        texttag.add("MoneyCHN", PubFun.getChnMoney(tLJSPayDB.getSumDuePayMoney()));
        texttag.add("Money", Money);

        texttag.add("XI_ContNo","");
        texttag.add("XI_ManageCom",tLJSPayDB.getManageCom());
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("e:\\", "testYFSH"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSPayDB.getGetNoticeNo());
            tLOPRTManagerDB.setCode("lp009");

            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNo(tLJSPayDB.getOtherNo());
            mLOPRTManagerSchema.setOtherNoType("Y");
            mLOPRTManagerSchema.setCode("lp009");
            mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
            mLOPRTManagerSchema.setAgentCode("");
            mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
            mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSchema.setStandbyFlag2(tLJSPayDB.getGetNoticeNo()); //这里存放实付总表的实付号码
            mLOPRTManagerSchema.setStandbyFlag3(mRgtClass);
            if (tLOBatchPRTManagerSchema != null){
                mLOPRTManagerSchema.setPrintTimes("1");
            }
        }

        mResult.addElement(mLOPRTManagerSchema);
        return true;

    }


    public CErrors getErrors() {
        return mErrors;
    }

    private boolean dealPrintMag(String cOperate) {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, cOperate);
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }


    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLJAGetSchema.setActuGetNo("370110000000152");
        tVData.addElement(tLJAGetSchema);
        tVData.addElement(tGlobalInput);
        GetCredenceF1PBL tGetCredenceF1PBL1 = new GetCredenceF1PBL();
        tGetCredenceF1PBL1.submitData(tVData, "PRINT");
    }
}
