package com.sinosoft.lis.f1print;

/**
 * <p>Title: FenClaimDayBalanceExcelUI</p>
 * <p>Description: X5理赔日结单Excel</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: 2013-09-16</p>
 * @author yan
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FenClaimDayBalanceExcelUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = new GlobalInput();

	private String mDay[] = null;
	private String tOutXmlPath = "";

	public FenClaimDayBalanceExcelUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if(!getInputData(cInputData)){
				return false;
			}
			if (!prepareOutputData(cInputData)) {
				return false;
			}
			System.out.println("Start FenClaimDayBalanceExcelBL Submit ...X5");
			FenClaimDayBalanceExcelBL tFenClaimDayBalanceExcelBL = new FenClaimDayBalanceExcelBL();
			if (!tFenClaimDayBalanceExcelBL.submitData(cInputData, cOperate)) {
				if (tFenClaimDayBalanceExcelBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tFenClaimDayBalanceExcelBL.mErrors);
					return false;
				} else {
					buildError("submitData",
							"FenClaimDayBalanceExcelBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			buildError("submitData", "操作失败！");
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData(VData vData) {
		try {
			vData.clear();
			vData.add(mDay);
			vData.add(mGlobalInput);
			vData.add(tOutXmlPath);
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("prepareOutputData", "发生异常");
			return false;
		}
		return true;
	}

	/*
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);

		System.out.println("start  FenClaimDayBalanceExcelUI getInputData...X5" );
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "FenClaimDayBalanceExcelUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
}
