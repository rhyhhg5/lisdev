package com.sinosoft.lis.f1print;


import org.jdom.Element;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppAccTraceDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppAccGetTraceSchema;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCGrpAddressSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要修改理赔金帐户的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class NewPrtAppAccGetBL implements PrintService
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
//    private TransferData mTransferData = null;
    private LCAppAccTraceSchema mLCAppAccTraceSchema = null;
    private LCAppAccGetTraceSchema mLCAppAccGetTraceSchema=null;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private XmlExport xmlexport = null;
    private TextTag mTag = new TextTag();
    private VData mResult = null;
    private int needPrt = -1 ;
    ListTable mtListTable = new ListTable();
    private String mOperate=null;
    public NewPrtAppAccGetBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }
    
    
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
        	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
//            mTransferData = (TransferData) cInputData.
//                            getObjectByObjectName("TransferData", 0);
//            mLCAppAccTraceSchema = (LCAppAccTraceSchema) cInputData.
//                               getObjectByObjectName("LCAppAccTraceSchema", 0);
            
        	ttLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                    getObjectByObjectName("LOPRTManagerSchema", 0));
            if(ttLOPRTManagerSchema == null)
            {//为空--VTS打印入口

                	mErrors.addOneError("getInputData没有得到足够的信息！");
                    return false;

            }
            else
            {//PDF入口
            	
            	String customerno=ttLOPRTManagerSchema.getStandbyFlag3();
            	String SerialNo=ttLOPRTManagerSchema.getStandbyFlag2();
//            	LCAppAccTraceDB mLCAppAccTraceDB = new LCAppAccTraceDB();
//            	mLCAppAccTraceDB.setOtherNo(ttLOPRTManagerSchema.getStandbyFlag2());
//            	mLCAppAccTraceDB.setSerialNo(ttLOPRTManagerSchema.getStandbyFlag2());
//            	mLCAppAccTraceDB.setCustomerNo(ttLOPRTManagerSchema.getStandbyFlag3());
                if(customerno==null||SerialNo==null)
                {
                	buildError("getInputData", "没有得到足够的信息！");
                    return false;
                }
                mOperate=(customerno.length()==9)?("0"):("1");
                
                
//              mLCAppAccTraceSchema = mLCAppAccTraceDB.getSchema();
//            	mLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(customerno,SerialNo);
//                mLCAppAccTraceSchema.setCustomerNo(customerno);
//                mLCAppAccTraceSchema.setSerialNo(SerialNo);

                
                AppAcc tAppAcc=new AppAcc();
                mLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(customerno,SerialNo);
                if(mLCAppAccTraceSchema == null)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tAppAcc.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "PrtAppAccGetBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "查询投保人帐户轨迹表时出错!";
                    System.out.println("------" + tError);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLCAppAccGetTraceSchema = tAppAcc.getLCAppAccGetTrace(mLCAppAccTraceSchema.getCustomerNo(),mLCAppAccTraceSchema.getSerialNo());
                if(mLCAppAccGetTraceSchema == null)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tAppAcc.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "PrtAppAccGetBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "查询投保人帐户领取轨迹表时出错!";
                    System.out.println("------" + tError);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                return true;
            }
            
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

 
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
    	
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtAppAccGet.vts", "printer"); //最好紧接着就初始化xml文档
        
        String sqlusercom = "select comcode from lduser where usercode='" +
        mGlobalInput.Operator + "' with ur";
		String comcode = new ExeSQL().getOneValue(sqlusercom);
		mTag.add("JetFormType", "86");
		if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
		comcode = "86";
		} else if (comcode.length() >= 4) {
		comcode = comcode.substring(0, 4);
		} else {
			mErrors.addOneError("getInputData操作员机构查询出错！");
//			buildError("getInputData", "操作员机构查询出错！");
		return false;
		}
		String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
		String printcode = new ExeSQL().getOneValue(printcom);
		
		mTag.add("ManageComLength4", printcode);
		mTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
//		if(mflag.equals("batch")){
//			mTag.add("previewflag", "0");
//		}else{
			mTag.add("previewflag", "1");
//		}

        
        if(!getCustomerInfo())
        {
            return false;
        }

        setFixedInfo(mLCAppAccGetTraceSchema.getOperator());
        mTag.add("CustomerNo", mLCAppAccGetTraceSchema.getCustomerNo());
        mTag.add("ConfirmDate", CommonBL.decodeDate(mLCAppAccGetTraceSchema.getAccGetDate()));
        mTag.add("AppDate", CommonBL.decodeDate(mLCAppAccGetTraceSchema.getAppDate()));

        mTag.add("AccBala", mLCAppAccGetTraceSchema.getAccBala());
        mTag.add("AccGetMoney", mLCAppAccGetTraceSchema.getAccGetMoney());
        mTag.add("AccGetName", mLCAppAccGetTraceSchema.getName());
        mTag.add("AccGetIDNo", mLCAppAccGetTraceSchema.getIDNo());
        mTag.add("NoticeNo", mLCAppAccGetTraceSchema.getNoticeNo());
        mTag.add("BarCode1", mLCAppAccGetTraceSchema.getNoticeNo()); //受理号
        mTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        mTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));

        xmlexport.addTextTag(mTag);
        
        
        if(mLCAppAccGetTraceSchema.getAccGetMode().equals("4"))
        {
//            xmlexport.addDisplayControl("displayTransfer");
//            mTag.add("BankName", mLCAppAccGetTraceSchema.getBankCode());
//            mTag.add("AccNo", mLCAppAccGetTraceSchema.getBankAccNo());
//            mTag.add("AccName", mLCAppAccGetTraceSchema.getAccName());
//            mTag.add("AccTransferDate", mLCAppAccGetTraceSchema.getTransferDate());
    		Element DataSetElement = xmlexport.getDocument().getRootElement();
            Element table = new Element("ACC");
            Element b = new Element("BankName");
            Element c = new Element("AccNo");
            Element d = new Element("AccName");
            Element e = new Element("AccTransferDate");
            b.addContent(mLCAppAccGetTraceSchema.getBankCode());
            c.addContent(mLCAppAccGetTraceSchema.getBankAccNo());
            d.addContent(mLCAppAccGetTraceSchema.getAccName());
            e.addContent(mLCAppAccGetTraceSchema.getTransferDate());
            table.addContent(b);
            table.addContent(c);
            table.addContent(d);
            table.addContent(e);
            DataSetElement.addContent(table);
            xmlexport.setDocument(DataSetElement.getDocument());
        }else
        {
        	
//            xmlexport.addDisplayControl("displayCash");
    		Element DataSetElement = xmlexport.getDocument().getRootElement();
            Element table = new Element("CASH");
            Element b = new Element("AccGetName");
            Element c = new Element("AccGetIDNo");
            b.addContent(mLCAppAccGetTraceSchema.getName());
            c.addContent(mLCAppAccGetTraceSchema.getIDNo());
            table.addContent(b);
            table.addContent(c);
            DataSetElement.addContent(table);
            xmlexport.setDocument(DataSetElement.getDocument());
        }
        xmlexport.outputDocumentToFile("C:\\", "newPrtAppAccListBL");
        mResult = new VData();
        mResult.addElement(xmlexport);

        //放入打印列表
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setStandbyFlag2(mLCAppAccGetTraceSchema.getNoticeNo());
        tLOPRTManagerDB.setCode("86");
        tLOPRTManagerSet = tLOPRTManagerDB.query();
        needPrt = tLOPRTManagerSet.size();

        if (needPrt == 0)
        { //没有数据，进行封装
            String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNo(mLCAppAccGetTraceSchema.getNoticeNo());
            mLOPRTManagerSchema.setOtherNoType("01");
            mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INFORM);
            mLOPRTManagerSchema.setManageCom(mGlobalInput.ManageCom);
//            mLOPRTManagerSchema.setAgentCode();
            mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
            mLOPRTManagerSchema.setReqOperator(mLCAppAccGetTraceSchema.getOperator());
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSchema.setStandbyFlag2(mLCAppAccGetTraceSchema.getNoticeNo()); //这里存放 （也为交费收据号）
        }else{
        	mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        	mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
        }
      mResult.add(mLOPRTManagerSchema);
    
        
        return true;
    }


    private void setFixedInfo(String operator)
    {
        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(operator);
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        mTag.add("Operator", tLDUserDB.getUserName());
        mTag.add("ServicePhone", tLDComDB.getServicePhone());
        mTag.add("ServiceFax", tLDComDB.getFax());
        mTag.add("ServiceAddress", tLDComDB.getLetterServicePostAddress());
        mTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        mTag.add("ComName", tLDComDB.getLetterServiceName());
        mTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    }

    //查询客户名称
    private boolean getCustomerInfo()
    {
        if(mOperate.equals("0"))
        {
            LCAddressSchema tLCAddressSchema = null;
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLCAppAccTraceSchema.getCustomerNo());
            if (!tLDPersonDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return false;
            }
            mTag.add("CustomerName", tLDPersonDB.getName());

            LCAddressDB tLCAddressDB = new LCAddressDB();
            LCAddressSet tLCAddressSet = new LCAddressSet();
            String tSql = "select * from LCAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "' "
                          + "and int(addressNo) =(select max(int(addressNo)) "
                          + "from LCAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "')";

            tLCAddressSet = tLCAddressDB.executeQuery(tSql);
            if (tLCAddressDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCAddressDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCAddressSet.size() > 0) {
                tLCAddressSchema = tLCAddressSet.get(1);
                mTag.add("AppZipCode", tLCAddressSchema.getZipCode());
                mTag.add("AppAddress", tLCAddressSchema.getPostalAddress());
                mTag.add("AppName", tLDPersonDB.getName());
            } else {
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息无记录!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
        }else if(mOperate.equals("1"))
        {
            LCGrpAddressSchema tLCGrpAddressSchema = null;
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCAppAccTraceSchema.getCustomerNo());
            if (!tLDGrpDB.getInfo()) {
                mErrors.addOneError("没有查询到客户信息。");
                return false;
            }
            mTag.add("CustomerName", tLDGrpDB.getGrpName());

            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            LCGrpAddressSet tLCGrpAddressSet = new LCGrpAddressSet();
            String tSql = "select * from LCGrpAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "' "
                          + "and int(addressNo) =(select max(int(addressNo)) "
                          + "from LCGrpAddress "
                          + "where customerNo='" +
                          mLCAppAccTraceSchema.getCustomerNo() + "')";

            tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(tSql);
            if (tLCGrpAddressDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息出错!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCGrpAddressSet.size() > 0) {
                tLCGrpAddressSchema = tLCGrpAddressSet.get(1);
                mTag.add("AppZipCode", tLCGrpAddressSchema.getGrpZipCode());
                mTag.add("AppAddress", tLCGrpAddressSchema.getGrpAddress());
                mTag.add("AppName", tLCGrpAddressSchema.getLinkMan1());
            } else {
                CError tError = new CError();
                tError.moduleName = "PrtAppAccGetBL";
                tError.functionName = "getCustomerInfo";
                tError.errorMessage = "查询地址信息无记录!";
                System.out.println("------" + tError);
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private boolean checkData()
    {


        return true;
    }
}
