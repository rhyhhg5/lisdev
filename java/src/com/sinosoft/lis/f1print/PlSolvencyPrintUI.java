package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlSolvencyPrintUI {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
	private String strBeginDate,strEndDate,year,strManageCom;

	public PlSolvencyPrintUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			// 只对打印操作进行支持
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}

			if (!getInputData(cInputData)) {
				return false;
			}
			VData vData = new VData();

			if (!prepareOutputData(vData)) {
				return false; 
			}

			PlSolvencyPrintBL tPlSolvencyPrintBL = new PlSolvencyPrintBL();
			System.out.println("Start PlSolvencyPrintUI Submit ...");
			if (!tPlSolvencyPrintBL.submitData(vData, cOperate)) {
				if (tPlSolvencyPrintBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlSolvencyPrintBL.mErrors);
					return false;
				} else {
					buildError("submitData","PlSolvencyPrintBL发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPlSolvencyPrintBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "PlSolvencyPrintBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}

	private boolean prepareOutputData(VData vData) {
		vData.clear();
		vData.add(strBeginDate);
		vData.add(strEndDate);
		vData.add(year);
		vData.add(mGlobalInput);
		vData.add(strManageCom);
		return true;
	}

	private boolean getInputData(VData cInputData) {
		strBeginDate=(String) cInputData.get(0);
		strEndDate=(String) cInputData.get(1);
		year=(String) cInputData.get(2);
		mGlobalInput=(GlobalInput) cInputData.get(3);
		strManageCom=(String) cInputData.get(4);
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PlSolvencyPrintBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {}
}
