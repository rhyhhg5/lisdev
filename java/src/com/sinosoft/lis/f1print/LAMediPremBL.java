package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author shaoax
 * @version 1.0
 */
public class LAMediPremBL {

	/**全局变量*/
    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mStartDate = "";
    private String mRiskcode = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageName = "总公司";

    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();

    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    public LAMediPremBL() {
    }
    public static void main(String[] args)
    {
        //GlobalInput tG = new GlobalInput();
        //tG.Operator = "xxx";
        //tG.ManageCom = "86";

        LAMediPremBL tLAMediPremBL = new LAMediPremBL();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;

        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
      try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
            this.mRiskcode = (String) mTransferData.getValueByName("tRiskCode");
            this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");

            System.out.println("#1#mManageCom"+mManageCom+"tRiskcode"+mRiskcode+"mStartDate"+"mEndDate"+mEndDate);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
	  //如果前台输入的查询条件为空,则默认查询条件
	  if(mManageCom == null||mManageCom.equals(""))
	  {
		  mManageCom = "86";
	  }
	  if(mStartDate == null||mStartDate.equals(""))
	  {
		  mStartDate=mPubFun.getCurrentDate().split("-")[0]+"-01-01";
		  System.out.println("mStartDate"+mStartDate);
	  }
	  if(mEndDate == null||mEndDate.equals(""))
	  {
		  mEndDate = mPubFun.getCurrentDate();
	  }

      return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();
        mXmlExport.createDocument("LAMediaPrem.vts", "printer");

        String tMakeDate = mPubFun.getCurrentDate();
        String tMakeTime = mPubFun.getCurrentTime();

        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("tMakeDate" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);
        String[] title = {"", "", "", "", ""};

        if (!getListTable()) {
            return false;
        }
        mXmlExport.addListTable(mListTable, title);
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();
        mResult.add(mXmlExport);
        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable()
    {
    	System.out.println("#2#mManageCom"+mManageCom+"mRiskcode"+mRiskcode+"mStartDate"+mStartDate+"mEndDate"+mEndDate);
    	String strSql = "";
    	//如果险种编码为空,则默认全查
        if(mRiskcode==null||mRiskcode.equals(""))
        {
	    	strSql = "select l.Managecom, l.Riskcode, r.Riskname,sum(l.Transmoney) as Transmoney,  "
				+"  (select count(1) from laagent where branchtype='2' and branchtype2='02' and agentstate<='04' and Managecom = l.Managecom ) as laagent  "
				+"  from lacommision l,lmriskapp r  "
				+"  where l.Riskcode = r.Riskcode   "
				+"  and l.branchtype='2' and l.branchtype2='02'  "
				+"  and Tmakedate >= '"+mStartDate+"'   "
				+"  and Tmakedate <= '"+mEndDate+"'  "
				+"  and l.Managecom  like '"+mManageCom+"%'  "
                                +"  and l.agentcode in (select agentcode from laagent where branchtype='2' and branchtype2='02') "
                                ;
        }
        else
        {
	    	strSql = "select l.Managecom, l.Riskcode, r.Riskname,sum(l.Transmoney) as Transmoney,  "
				+"  (select count(1) from laagent where branchtype='2' and branchtype2='02' and agentstate<='04' and Managecom = l.Managecom ) as laagent  "
				+"  from lacommision l,lmriskapp r  "
				+"  where l.Riskcode = r.Riskcode   "
				+"  and l.branchtype='2' and l.branchtype2='02'  "
				+"  and Tmakedate > '"+mStartDate+"'   "
				+"  and Tmakedate < '"+mEndDate+"'  "
				+"  and l.Managecom  like '"+mManageCom+"%'  "
	    		+"  and l.Riskcode = '"+mRiskcode+"'  "
                        +"  and l.agentcode in (select agentcode from laagent where branchtype='2' and branchtype2='02') ";

    	}
    	strSql = strSql+"  group by l.Managecom,l.Riskcode,r.Riskname  "
    				+"  order by l.Managecom  ";

    	System.out.println("sql"+strSql);
        SSRS tSSRS2 = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS2 = tExeSQL.execSQL(strSql);

        if (tSSRS2.getMaxRow() >= 1)
        {
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
            {
                String Info[] = new String[5];
                Info[0] = tSSRS2.GetText(j, 1);
                Info[1] = tSSRS2.GetText(j, 2);
                Info[2] = tSSRS2.GetText(j, 3);
                Info[3] = tSSRS2.GetText(j, 4);
                Info[4] = tSSRS2.GetText(j, 5);
                mListTable.add(Info);
            }
        }
        else
        {
            bulidError("getListTable","没有符合条件的信息!");
            return false;
        }
        mListTable.setName("ZT");
        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LADimissionContBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
