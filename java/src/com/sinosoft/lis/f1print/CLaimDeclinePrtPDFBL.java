package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLClaimDeclineDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLClaimDeclineSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLClaimDeclineSet;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;

public class CLaimDeclinePrtPDFBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mdirect;
//    private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
    
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public CLaimDeclinePrtPDFBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {System.out.println("CLaimDeclinePrtBL....");

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        mdirect = "";
        mdirect +=mLOPRTManagerSchema.getStandbyFlag2();
        mLLCaseSchema.setCaseNo(mLOPRTManagerSchema.getOtherNo());
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ComCode);
        tLDComDB.getInfo();
        String CaseNo = mLLCaseSchema.getCaseNo();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(CaseNo);

        if (!tLLCaseDB.getInfo()) {
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            buildError("outputXML", "mLLCaseDB在取得打印队列中数据时发生错误");
            return false;
        }
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        System.out.println("受理日期"+mLLCaseSchema.getRgtDate());
        String RgtDate = getCDate(mLLCaseSchema.getRgtDate());
        System.out.println("中文受理日期"+RgtDate);
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            buildError("outputXML", "mLLRegisterDB在取得打印队列中数据时发生错误");
            return false;
        }
        String Statement0 = "您于"+RgtDate+"提起的理赔申请，本公司已收悉。";

        String Statement = "根据您的申请事由及您提供的相关证明，结合被保险人"
                           +mLLCaseSchema.getCustomerName()
                           +"在本公司的保障范围（";

        String sql = "select distinct coalesce((select distinct grpcontno from lcrnewstatelog where newgrpcontno=a.grpcontno),grpcontno),"
        			+" coalesce((select distinct contno from lcrnewstatelog where newcontno=a.contno),contno)"
        			+" from llclaimdetail a where caseno = '"+CaseNo+"'";
        ExeSQL exesql1 = new ExeSQL();
        SSRS resu = exesql1.execSQL(sql);
        if(resu!=null){
            for (int j = 1; j <= resu.getMaxRow(); j++) {
                String strLine0 = "";
                if(resu.GetText(j, 1).equals("00000000000000000000"))
                    strLine0 = resu.GetText(j, 2);
                else
                    strLine0 = resu.GetText(j, 1);
                strLine0 += "号保险合同项下险种：";
                sql = "select distinct B.RiskName from llclaimdetail A,lmrisk B "
                      +" where B.Riskcode =A.riskcode and caseno = '"+CaseNo
                      +"' and (a.contno='"+resu.GetText(j, 2)+"' or  exists (select 1 from lcrnewstatelog where a.contno=newcontno and contno='"+resu.GetText(j, 2)+"'))";
                SSRS risktb = exesql1.execSQL(sql);
                String RiskName="";
                if(resu!=null){
                    for(int k = 1;k<=risktb.getMaxRow();k++){
                        RiskName += "《"+risktb.GetText(k,1)+"》";
                        if(k!=risktb.getMaxRow())
                            RiskName += "、";
                        else{
                            if(j!=resu.getMaxRow())
                              RiskName += ";";
                        }
                    }
                    strLine0 += RiskName;
                }
                Statement += strLine0;
            }
        }
        Statement += "），经公司审核确认，";
        LLClaimDeclineDB tLLClaimDeclineDB = new LLClaimDeclineDB();
        tLLClaimDeclineDB.setCaseNo(CaseNo);
        LLClaimDeclineSet tLLClaimDeclineSet = new LLClaimDeclineSet();
        tLLClaimDeclineSet.set(tLLClaimDeclineDB.query());
        if (tLLClaimDeclineSet.size() == 0) {
            buildError("tLLClaimDeclineSet", "没有得到足够的拒赔信息！");
            return false;
        }
        LLClaimDeclineSchema tLLClaimDeclineSchema = new LLClaimDeclineSchema();
        tLLClaimDeclineSchema = tLLClaimDeclineSet.get(1).getSchema();
        Statement += tLLClaimDeclineSchema.getReason();
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(CaseNo);
        LLClaimSet tLLClaimSet = new LLClaimSet();
        tLLClaimSet.set(tLLClaimDB.query());
        if (tLLClaimSet.size() == 0) {
            buildError("tLLClaimSet", "没有得到足够的赔案信息！");
            return false;
        }
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema = tLLClaimSet.get(1).getSchema();
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
       // xmlexport.createDocuments("ClaimDecide.vts", mGlobalInput); //最好紧接着就初始化xml文档
       if(mdirect.equals("1"))
           xmlexport.createDocument("ClaimDecline.vts", "print");
       else
           xmlexport.createDocuments("ClaimDecline.vts",mGlobalInput);

        String tCaseGetMode = "";
        //模版自上而下的元素

        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
        
        String sqlusercom = "select comcode from lduser where usercode='" +
        mGlobalInput.Operator + "' with ur";
		String comcode = new ExeSQL().getOneValue(sqlusercom);
		texttag.add("JetFormType", "lp014");
		if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
		comcode = "86";
		} else if (comcode.length() >= 4){
		comcode = comcode.substring(0, 4);
		} else {
		buildError("getInputData", "操作员机构查询出错！");
		return false;
		}
		String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
		String printcode = new ExeSQL().getOneValue(printcom);
        
        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));      
        texttag.add("previewflag", "1");

        texttag.add("BarCode1", mLLCaseSchema.getCaseNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("SysDate", SysDate);
        if (mLLCaseSchema.getCustomerSex().equalsIgnoreCase("0"))
        {
            texttag.add("Name", tLLRegisterDB.getRgtantName() + "先生"); //投保人名称
        }
        else
        {
            texttag.add("Name", tLLRegisterDB.getRgtantName() + "女士"); //投保人名称
        }
        texttag.add("CustomerName", tLLRegisterDB.getRgtantName()); //理赔号
        texttag.add("PostCode", tLLRegisterDB.getPostCode()); //理赔号
        texttag.add("PostalAddress", tLLRegisterDB.getRgtantAddress()); //理赔号
        //System.out.println("mLLCaseSchema.getInsuredNo()="+mLLCaseSchema.getInsuredNo());
        texttag.add("Money", tLLClaimSchema.getRealPay()); //体检日期
        texttag.add("RgtDate", RgtDate); //申请日期
        texttag.add("Statement0",Statement0);
        texttag.add("Statement",Statement);
        texttag.add("ClaimPhone",tLDComDB.getClaimReportPhone());
        texttag.add("Fax",tLDComDB.getFax());
        texttag.add("ClaimAddress",tLDComDB.getLetterServicePostAddress());
        texttag.add("ClaimZipCode",tLDComDB.getLetterServicePostZipcode());
        texttag.add("ComName",tLDComDB.getLetterServiceName());

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tEndListTable);
//        xmlexport.outputDocumentToFile("e:\\", "claimdecide"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

private String getCDate(String rgtdate)
    {
        String year = rgtdate.substring(0,4);
        int dashid1 = rgtdate.indexOf("-")+1;
        int dashid2 = rgtdate.indexOf("-",6)+1;
        String month = rgtdate.substring(dashid1,dashid2-1);
        String day = rgtdate.substring(dashid2);
        String ChineseDate = year+"年"+month+"月"+day+"日";
        return ChineseDate;
    }


private boolean dealPrintMag() {
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
    String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
    tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
    tLOPRTManagerSchema.setOtherNo(mLLCaseSchema.getCaseNo()); //存放前台传来的caseno
    tLOPRTManagerSchema.setOtherNoType("5");
    tLOPRTManagerSchema.setCode("lp014");
    tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
    tLOPRTManagerSchema.setAgentCode("");
    tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
    tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
    tLOPRTManagerSchema.setPrtType("0");
    tLOPRTManagerSchema.setStateFlag("0");
    tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
    tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
    tLOPRTManagerSchema.setStandbyFlag2(mLLCaseSchema.getCaseNo());

    mResult.addElement(tLOPRTManagerSchema);
    return true;
}

    public static void main(String[] args)
    {

        CLaimDeclinePrtPDFBL tCLaimDeclinePrtPDFBL = new CLaimDeclinePrtPDFBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("550000000000098");
        tVData.addElement(tLLCaseSchema);
        tCLaimDeclinePrtPDFBL.submitData(tVData, "PRINT");
        VData vdata = tCLaimDeclinePrtPDFBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }


}
