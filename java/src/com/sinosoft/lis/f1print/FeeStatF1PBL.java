package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import java.util.Hashtable;

import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FeeStatF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FeeStatF1PBL()
    {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        int m;
        String tsql = "";
        //tsql = "select comcode,name from ldcom where length(trim(comcode))=8";
        tsql =
                "select * from ldcom where length(trim(comcode))=8 order by comcode";
        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        LDComSchema tLDComSchema;
        tLDComSet.set(tLDComDB.executeQuery(tsql));
        if (tLDComDB.mErrors.needDealError() == true)
        {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            CError cError = new CError();
            cError.moduleName = "FeeStatF1PBL";
            cError.functionName = "getPrintData";
            cError.errorMessage = "FeeStatF1PBL在读取数据库时发生错误";
            mErrors.addOneError(cError);
            return false;
        }
        Hashtable tHashtable = new Hashtable();
        for (m = 1; m <= tLDComSet.size(); m++)
        {
            tLDComSchema = new LDComSchema();
            tLDComSchema.setSchema(tLDComSet.get(m));
            tHashtable.put(tLDComSchema.getComCode(), new Integer(m));
        }

        //LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        //LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

        //
        //long SumMoney=0;
        //long SumNumber=0;


        SSRS tSSRS;
        msql = "select LMRisk.RiskName,a.ManageCom,sum(a.GetMoney) from (select RiskCode,ManageCom,sum(GetMoney) GetMoney from LJAGetEndorse where MakeDate>='" +
               mDay[0] + "' and MakeDate<='" + mDay[1] + "' Group By RiskCode,ManageCom union select RiskCode,ManageCom,sum(SumActuPayMoney) GetMoney from LJAPayPerson where ConfDate>='" +
               mDay[0] + "' and ConfDate<='" + mDay[1] + "' Group By RiskCode,ManageCom) a,LMRisk where a.riskcode=LMRisk.riskcode Group by LMRisk.RiskName, a.managecom";
        //msql = "select LMRisk.RiskName,LJAPay.ManageCom,sum(LJAPay.SumActuPayMoney) from LJAPay,LMRisk where LJAPay.EnterAccDate>='"+mDay[0]+"' and LJAPay.EnterAccDate<='"+mDay[1]+"' and LJAPay.RiskCode=LMRisk.RiskCode Group By LMRisk.RiskName,LJAPay.ManageCom order by LMRisk.RiskName,LJAPay.ManageCom";
        //msql="select RiskCode,sum(PayMoney),sum(1) from LJTempFee where MakeDate>='"+mDay[0]+"' and MakeDate<='"+mDay[1]+"' and Operator='"+mGlobalInput.Operator+"' Group By RiskCode order by RiskCode";
        //判断操作员位数是否为8位
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("MCOM");
        Integer num;
        String RName = "";
        double tSum = 0;
        String nsql =
                "select count(*) from(select riskcode from LJAPayPerson where MakeDate>='" +
                mDay[0] + "' and MakeDate<='" + mDay[1] +
                "' group by riskcode union select riskcode from LJAGetEndorse where MakeDate>='" +
                mDay[0] + "' and MakeDate<='" + mDay[1] +
                "' group by riskcode)";
        ExeSQL nExeSQL = new ExeSQL();
        SSRS nSSRS = new SSRS();
        nSSRS = nExeSQL.execSQL(nsql);
        int tnumber = Integer.valueOf(nSSRS.GetText(1, 1)).intValue();
        String StringArray[][] = new String[tnumber][36];
        int n = 0;
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            //strArr = new String[36];
            //RName = tSSRS.GetText(i,1);
            if (!RName.equals(tSSRS.GetText(i, 1)))
            {
                if (i != 1)
                {
                    strArr[35] = String.valueOf(tSum);
                    StringArray[n] = strArr;
                    n++;
                    tlistTable.add(strArr);
                }
                tSum = 0;
                strArr = new String[36];
                RName = tSSRS.GetText(i, 1);
                strArr[0] = RName;
                //为数组赋初值
                for (int j = 1; j <= 35; j++)
                {
                    strArr[j] = "0";
                }

            }
            try
            {
                num = (Integer) tHashtable.get(tSSRS.GetText(i, 2));
                strArr[num.intValue()] = tSSRS.GetText(i, 3);
                tSum = tSum + Double.valueOf(tSSRS.GetText(i, 3)).doubleValue();
            }
            catch (Exception ex)
            {
                continue;
            }
        }
        strArr[35] = String.valueOf(tSum);
        StringArray[n] = strArr;
        tlistTable.add(strArr);

        strArr = new String[36];
        strArr[0] = "合计：";
        double TotalSum = 0;
        for (int x = 1; x < 36; x++)
        {
            for (int y = 0; y < tnumber; y++)
            {
                TotalSum = TotalSum +
                           Double.valueOf(StringArray[y][x]).doubleValue();
            }
            strArr[x] = String.valueOf(TotalSum);
            TotalSum = 0;
        }
        tlistTable.add(strArr);

//        strArr = new String[3];
//        strArr[0] = "Risk"; strArr[1] ="Money";strArr[2] = "Mult";
//        nsql="select Name from LDCom where ComCode='" + mGlobalInput.ManageCom +"'";
//        ExeSQL nExeSQL = new ExeSQL();
//        nSSRS=nExeSQL.execSQL(nsql);
//        String manageCom =nSSRS.GetText(1,1);
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FeeStat.vts", "printer"); //最好紧接着就初始化xml文档

        texttag.add("StartDay", mDay[0]);
        texttag.add("EndDay", mDay[1]);

//        // texttag.add("",);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;

    }
}