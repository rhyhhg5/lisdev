package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  hzy lys
 * @date:2003-06-04
 * @version 1.0
 */
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJABonusGetDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAGetDrawDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAGetOtherDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJABonusGetSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetDrawSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetOtherSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJABonusGetSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetDrawSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class ActuPayDayCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private LMRiskAppSet mLMRiskAppSet;
    private String mRiskName = "";
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public ActuPayDayCheckBL()
    {
        mErrors=new CErrors();
        mResult=new VData();
        mDay=null;
        mRiskName="";
        mGlobalInput=new GlobalInput();

    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86110000";
        VData vData = new VData();
        String[] tDay = new String[2];
        tDay[0] = "2007-01-14";
        tDay[1] = "2007-07-14";
        vData.addElement(tDay);
        vData.addElement(tG);

        ActuPayDayCheckBL tF = new ActuPayDayCheckBL();
        tF.submitData(vData, "PRINTGET");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINTGET")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        SSRS nSSRS = new SSRS();
        double ESumMoney = 0;
        double OSumMoney = 0;
        double SumMoney = 0;
        int SumNum=0;
//        String msql ="select '保全' || case a.paymode when '4' then '银行转帐' else '' end, a.otherno,sum(sumgetmoney) a ,a.paymode from LJAGet a,LJAGetEndorse b where 1=1 "+
//        " and ConfDate>='" + mDay[0] +"' and confdate <= '" + mDay[1] +"' and a.ManageCom like '"+ mGlobalInput.ManageCom +"%' " +
//        " and a.OtherNoType in('10','3') "+
//        " and b.feefinatype <> 'YEI' "+
//        "and a.paymode not in ('B','J','13','0') "+
//        "and (b.riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) or  b.riskcode is null) "+
//        " and a.actugetno = b.actugetno  group by a.otherno,a.paymode";
        
        GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "ActuPay1");
        
        
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName("ENDOR");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[4];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    ESumMoney = ESumMoney + Double.parseDouble(strArr[j - 1]);
                }
                if (j == 4)
                {
                    
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && strArr[j - 1].trim().equals("4")){
                        SumNum++;
                    }
                        
                }
            }
            tlistTable.add(strArr);
        }

        strArr = new String[4];
        strArr[0] = "EndorName";
        strArr[1] = "EndorNo";
        strArr[2] = "Money";

//        msql = "select '理赔  ' || case (select paymode from ljaget where actugetno=LJAGetClaim.actugetno) "
//                +"when '4' then '银行转帐' else '' end "
//                +", otherno,sum(pay) a,actugetno from LJAGetClaim where ConfDate>='"+ mDay[0]+"' "+
//               "and confdate <= '"+ mDay[1]+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' "+
//               "and actugetno not in (select actugetno from ljfiget where substr(managecom,1,4) <> substr(policycom,1,4)) and (riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) or (riskcode is null)) group by otherno,actugetno"
//               +" union all select '暂收退费  ' || case (select paymode from ljaget where actugetno=LJAGetTempFee.actugetno) "
//                +"when '4' then '银行转帐' else '' end , tempfeeno,sum(getmoney),actugetno from LJAGetTempFee where MakeDate>='"+mDay[0]+"'"
//               +" and MakeDate<='"+mDay[1]+"' and ManageCom like '"+mGlobalInput.ManageCom+"%' and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode not in ('170101','170301')) group by tempfeeno,actugetno "
//        
//                +" union all select '其他退费  ' || case (select paymode from ljaget where actugetno=LJAGetOther.actugetno) " 
//                +"when '4' then '银行转帐' else '' end ,otherno,sum(getmoney) a,actugetno  from LJAGetOther where 1=1 "
//                +"and ConfDate >='"+ mDay[0]+"'  "
//                +"and confdate <= '"+ mDay[1]+"' " 
//                +"and ManageCom like '"+mGlobalInput.ManageCom+"%' "
//                +"group by otherno,actugetno";
        msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "ActuPay2");
        tSSRS = tExeSQL.execSQL(msql);
        ListTable alistTable = new ListTable();
        String strSumArr[] = null;
        alistTable.setName("CLAIM");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strSumArr = new String[4];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strSumArr[j - 1]));
                    strSumArr[j - 1] = strSum;
                    OSumMoney = OSumMoney + Double.parseDouble(strSumArr[j - 1]);
                }
                if (j == 4)
                {
                    
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    if(strSumArr[j - 1]!=null && !strSumArr[j - 1].trim().equals("")){
                        LJAGetDB tLJAGetDB=new LJAGetDB();
                        tLJAGetDB.setActuGetNo(strSumArr[j - 1]);
                        LJAGetSet tLJAGetSet=tLJAGetDB.query();
                        if(tLJAGetSet!=null && tLJAGetSet.size()>0){
                            LJAGetSchema tLJAGetSchemal=tLJAGetSet.get(1);
                            if(tLJAGetSchemal.getPayMode()!=null && tLJAGetSchemal.getPayMode().equals("4")){
                                System.out.println("========================================"+tSSRS.GetText(i, j));
                                SumNum++;
                            }
                        }
                    }
                }
            }
            alistTable.add(strSumArr);
        }
        strSumArr = new String[4];
        strSumArr[0] = "EndorName";
        strSumArr[1] = "EndorNo";
        strSumArr[2] = "Money";

        SumMoney = ESumMoney + OSumMoney;

        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ActuPayDayCheck.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", SumMoney);
        texttag.add("SumNum", SumNum);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.addListTable(alistTable, strSumArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("E:\\","ShiFu");
        return true;
    }

    private void initCommon()
    {
        this.mRiskCode = "";
        this.mRiskType = "";
        this.mXsqd = "";
        this.mXzdl = "";
        this.mSxq = "";
        this.mDqj = "";
    }


}
