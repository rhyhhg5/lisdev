package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;


public class PlPayMoneyMainPrintBL 
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();
    
    private String tCompanyCode = "";   //机构代码
    private String tBeginDate = "";     //统计起期
    private String tEndDate = "";       //统计止期
//    private String tComCode = "";		//当前登录机构编码

    public PlPayMoneyMainPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(TransferData cInputData, String cOperate) {
        mResult.clear();
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    private boolean getInputData(TransferData cInputData) {
    	try {
    		tCompanyCode=(String)cInputData.getValueByName("CompanyCode");
        	tBeginDate=(String)cInputData.getValueByName("BeginDate");
        	tEndDate=(String)cInputData.getValueByName("EndDate");	
//        	tComCode=(String)cInputData.getValueByName("ComCode");
		} catch (Exception e) {
			e.printStackTrace();
			buildError("getInputData", "执行失败，原因是："+e.toString());
			return false;
		}
		return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PlPayMoneyMainPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean getPrintData() 
    {
    	
        try {
    		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
    		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
    		xmlexport.createDocument("PlPayMoneyMain.vts", "printer"); // 最好紧接着就初始化xml文档
    		
    		String strArr[] = new String[9];
    		String[] detailArr = new String[] {"分公司","合计","应付租金","应付物业费","应付保证金","应付违约金","应收已付款","应退保证金","应付装修费"};
    		//日期格式处理
    		tBeginDate=tBeginDate+"-1";
    		tEndDate=tEndDate+"-1";
    		
            String sql="";
    		sql = "select managecom,(nvl(sum(m1), 0) + nvl(sum(m2), 0) +nvl(sum(m7), 0) +nvl(sum(m4), 0) - nvl(sum(m5), 0) - nvl(sum(m6), 0)),"+
    	    	  " nvl(sum(m1), 0),nvl(sum(m2), 0),nvl(sum(m7), 0),nvl(sum(m4), 0),nvl(sum(m5), 0),nvl(sum(m6), 0),nvl(sum(m3), 0) "+
    	    	  " from (select managecom,placeno,nvl(f1, 0) as m1,nvl(f2, 0) as m2,nvl(f3, 0) as m3,nvl(f4 + s4, 0) as m4,nvl(f5 + s5, 0) as m5,nvl(f6 + s6 + z6, 0) as m6,nvl(f7, 0) as m7"+
    	    	  " from (select a.managecom,a.placeno,"+
    	    	  " (select nvl(sum(money),0) from liplacerentfee where placeno = a.placeno and feetype = '01' and (year(paydate)*100+month(paydate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"'))  and (year(paydate)*100+month(paydate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f1,"+
    	    	  " (select nvl(sum(money),0) from liplacerentfee where placeno = a.placeno and feetype = '02' and (year(paydate)*100+month(paydate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and (year(paydate)*100+month(paydate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f2,"+
    	    	  " (select nvl(sum(money),0) from liplacerentfee where placeno = a.placeno and feetype = '03' and (year(paydate)*100+month(paydate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and (year(paydate)*100+month(paydate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f3,"+
    	    	  " (select nvl(sum(Apenaltyfee),0) from LIPlaceChangeInfo where preplacno = a.placeno  and (year(Apenaltyfeegetdate)*100+month(Apenaltyfeegetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and (year(APenaltyfeegetdate)*100+month(APenaltyfeegetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f4,"+
    	    	  " (select nvl(sum(Apenaltyfee),0) from LIPlaceEndInfo where placeno = a.placeno  and (year(Apenaltyfeegetdate)*100+month(Apenaltyfeegetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"'))  and  (year(APenaltyfeegetdate)*100+month(APenaltyfeegetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as s4,"+
    	    	  " (select nvl(sum(Shouldgetfee),0) from LIPlaceChangeInfo where preplacno = a.placeno  and (year(shouldgetdate)*100+month(shouldgetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and  (year(shouldgetdate)*100+month(shouldgetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f5,"+
    	    	  " (select nvl(sum(Shouldgetfee),0) from LIPlaceEndInfo where placeno = a.placeno   and (year(shouldgetdate)*100+month(shouldgetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and  (year(shouldgetdate)*100+month(shouldgetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as s5,"+
    	    	  " (select nvl(sum(Marginfee),0) from LIPlaceChangeInfo where preplacno = a.placeno  and (year(Marginfeegetdate)*100+month(Marginfeegetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and  (year(Marginfeegetdate)*100+month(Marginfeegetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f6,"+
    	    	  " (select nvl(sum(Marginfee),0) from LIPlaceEndInfo where placeno = a.placeno   and (year(Marginfeegetdate)*100+month(Marginfeegetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"'))and  (year(Marginfeegetdate)*100+month(Marginfeegetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as s6,"+
    	    	  " (select nvl(sum(Marginfee),0) from LIPlaceNextInfo where placeno = a.placeno  and (year(Marginfeegetdate)*100+month(Marginfeegetdate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"')) and  (year(Marginfeegetdate)*100+month(Marginfeegetdate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as z6,"+
    	    	  " (select nvl(sum(Marginfee),0) from liplacerentinfo where placeno=a.placeno and (year(Marginpaydate)*100+month(Marginpaydate)) between (year('"+tBeginDate+"')*100+month('"+tBeginDate+"')) and (year('"+tEndDate+"')*100+month('"+tEndDate+"'))and  (year(Marginpaydate)*100+month(Marginpaydate))<=(year(a.Actualenddate)*100+month(a.Actualenddate))) as f7"+
    	    	  " from liplacerentinfo a where state <>'02'";
    		if (!tCompanyCode.equals("") && tCompanyCode!=null) {
    			sql=sql+" and a.managecom like'"+tCompanyCode+"%'";
    		}
//    		if(tCompanyCode.equals("") || tCompanyCode == null)
//    		{
//    			sql=sql+" and a.managecom like '"+tComCode+"%'";
//    		}
    		sql=sql+") as bb) as cc where 1 = 1 group by managecom with ur";
    		
    		System.out.println("sql:"+sql);
    		ExeSQL main_exesql = new ExeSQL();
    	    SSRS main_ssrs = main_exesql.execSQL(sql);
    		ListTable tDetailListTable = new ListTable(); // 明细ListTable
    		tDetailListTable.setName("INFO");
    		String strSum="";
    		for(int i=1;i<=main_ssrs.MaxRow;i++)
    		{
    			strArr = new String[9];
    			for(int j=1;j<=main_ssrs.MaxCol;j++)
    			{
    			if (j==1) {
    				strArr[j-1]=main_ssrs.GetText(i,j);
    			} else {
        			strArr[j - 1] = main_ssrs.GetText(i, j);
    				if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1]))){
	    				strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
	    				strArr[j - 1] = strSum;
    				}
    			}	
    			}
    			tDetailListTable.add(strArr);
    		}
    		xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable

    		if (texttag.size() > 0) 
    		{
    			xmlexport.addTextTag(texttag);
    		}
    		mResult.clear();
    		mResult.addElement(xmlexport);
    		return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			buildError("getPrintData", "运行错误，原因是"+e.toString());
			return false;
		}
    }

}
