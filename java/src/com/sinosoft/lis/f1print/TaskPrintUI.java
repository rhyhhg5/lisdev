package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LGWorkSchema;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单打印BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Yang Yalin
 * @version 1.0
 * @dat e 2005-02-25
 */

public class TaskPrintUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public TaskPrintUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        TaskPrintBL tTaskPrintBL = new TaskPrintBL();
        if (!tTaskPrintBL.submitData(cInputData, cOperate))
        {
            if (tTaskPrintBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tTaskPrintBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "tTaskPrintBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tTaskPrintBL.getResult();
            return true;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args)
    {
        TaskPrintUI tTaskPrintUI = new TaskPrintUI();

        LGWorkSchema tLGWorkScmeha = new LGWorkSchema();
        tLGWorkScmeha.setAcceptNo("20050524000051");
        tLGWorkScmeha.setWorkNo("20050524000051");

        VData tVData = new VData();
        tVData.addElement(tLGWorkScmeha);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        tVData.addElement(tG);
        tTaskPrintUI.submitData(tVData, "PRINT");
    }


}







