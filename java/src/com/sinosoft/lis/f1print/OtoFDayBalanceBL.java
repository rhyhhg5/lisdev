package com.sinosoft.lis.f1print;

/**
 * <p>Title: OtoFDayBalance</p>
 * <p>Description:财务提数对帐单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : zhangjun
 * @date:2006-03-23
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class OtoFDayBalanceBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    private int mOtofNum;

    public OtoFDayBalanceBL() {
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86110000";
        VData vData = new VData();
        String[] tDay = new String[4];
        tDay[0] = "2006-05-01";
        tDay[1] = "2006-05-05";
        Integer itemp = new Integer("5");
        vData.addElement(tDay);
        vData.addElement(itemp);
        vData.addElement(tG);

        OtoFDayBalanceBL tF = new OtoFDayBalanceBL();
        tF.submitData(vData, "PRINT");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT") ) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINT")) { //打印提数
            if (!getPrintData(mOtofNum)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mDay = (String[]) cInputData.get(0);
        Integer itemp = (Integer) cInputData.get(1);
        mOtofNum = itemp.intValue();
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData(int mNum) {
        switch (mNum){
        case 1: //暂收
            getTempFeeData();
            break;
        case 2: //预收(保全)
            getEndorseFeeAdvance();
            break;
        case 3:
            getFeeDataPrem();//保费收入
            break;
        case 4:
            payFeeDataJ(); //其他业务应付
            break;
        case 5:
            payFeeDataEndordseJ();//保全应付
            break;
        case 6:
            payFeeDataD();//业务实付
            break;
        case 7:
            getEndorsePrem();//保全收入
            break;
        case 8:
            getFeeDataPremtl();//应收保费
            break;
        case 9:
            getFeeDataPremadvanceconf();//应收保费转保费收入
            break;
        case 10:
            PayCommionJ();//佣金计算 “佣金”
            break;

        }
        return true;
    }

    //暂收
    private boolean getTempFeeData(){
        String msql = "select distinct '暂收',Confmakedate,sum(paymoney),ManageCom from LJTempFee where Confmakedate>='"
                      + mDay[0] + "' and Confmakedate<='" + mDay[1] +"' and ManageCom like '"+ mGlobalInput.ManageCom
                      + "%' and TempFeeType in('1','5','6') group by Confmakedate,ManageCom";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //预收(保全)
    private boolean getEndorseFeeAdvance()
    {
       String msql = "select distinct '预收',Confmakedate,sum(paymoney),ManageCom from LJTempFee where Confmakedate>='"
                          + mDay[0] + "' and Confmakedate<='" + mDay[1] +"' and ManageCom like '"+ mGlobalInput.ManageCom
                          + "%' and TempFeeType = '4' group by Confmakedate,ManageCom";
       if (!productXml(msql))
             return false;
       else
             return true;
    }
    //保费收入
    private boolean getFeeDataPrem()
    {
        String msql = "select distinct '保费',ConfDate,sum(Sumactupaymoney),ManageCom from LJAPayPerson where ConfDate>='"
                      + mDay[0] + "' and ConfDate <= '"+ mDay[1] +"'and ManageCom like '"+ mGlobalInput.ManageCom
                      +"%' and payno not in(select actugetno from ljaget where othernotype in('10','3')) "
                      +" and payno not in(select payno from ljapay where incometype in('10','3','9')) "
                      +"group by ConfDate,ManageCom";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //其他业务应付
    private boolean payFeeDataJ()
    {
        String mEndSql = "where MakeDate>='"+ mDay[0] +"' "
                       + "and MakeDate <='"+ mDay[1] +"' and ManageCom like '"
                       + mGlobalInput.ManageCom+"%' "
                       + "group by makedate,managecom ";

        String msql = "select a, b, sum(c), d from "
                       +"(select distinct '其他业务应付' a,MakeDate b,sum(GetMoney) c ,managecom d from LJAGetTempFee "
                       + mEndSql + "union all "
                       +"select distinct '其他业务应付' a,MakeDate b,sum(pay) c,managecom d from LJAGetClaim "
                       + mEndSql + "union all "
                       +"select distinct '其他业务应付' a,MakeDate b,sum(GetMoney) c,ManageCom d from LJAGetOther "
                       + mEndSql+") y group by a,b,d";
         if (!productXml(msql))
               return false;
         else
              return true;


    }
    //保全应付
    private boolean payFeeDataEndordseJ()
    {
        String msql = "select  '保全应付',a.makedate,-sum(a.getmoney),a.managecom from LJAGetEndorse a,ljaget b where a.MakeDate>='"
                      + mDay[0]+"' and a.MakeDate <= '"+ mDay[1] +"' and a.ManageCom like '"+ mGlobalInput.ManageCom
                      +"%' and FeeFinaType!='LX' and FeeFinaType!='BF' and FeeFinaType!='GB' "
                      +" and FeeFinaType!='HK' and FeeFinaType!='HD' and FeeFinaType!='YHSL' and a.actugetno = b.actugetno and b.sumgetmoney <> 0 "
                      +" group by a.makedate,a.managecom "
                      +" union all "
                       +" select  '保全应付',a.makedate,-sum(a.getmoney),a.managecom from LJAGetEndorse a,ljapay b where a.MakeDate>='"
                       + mDay[0]+"' and a.MakeDate <= '"+ mDay[1] +"' and a.ManageCom like '"+ mGlobalInput.ManageCom
                       +"%' and FeeFinaType!='LX' and FeeFinaType!='BF' and FeeFinaType!='GB' "
                       +" and FeeFinaType!='HK' and FeeFinaType!='HD' and FeeFinaType!='YHSL' and a.actugetno = b.payno and b.sumactupaymoney <> 0 "
                       +" group by a.makedate,a.managecom " ;
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //业务实付
    private boolean payFeeDataD()
    {
        String msql = "select '实付',ConfDate,sum(sumgetmoney),managecom from LJAGet where ConfDate>='"
                      +mDay[0]+"' and ConfDate <= '"+ mDay[1] +"'and ManageCom like '"+mGlobalInput.ManageCom
                      +"%' group by confdate,managecom";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //保全收入
    private boolean getEndorsePrem()
    {
        String msql = "select '保全收入',modifydate,sum(getmoney),managecom from ljagetendorse where endorsementno in "
                +"(select otherno from LJAGet where ConfDate>='"+mDay[0]+"' "
                +"and confDate<='"+mDay[1]+"' "
                +"and ManageCom like '"+mGlobalInput.ManageCom+"%' and Othernotype in ('10','3') "
                +"union all "
                +"select incomeno from LJAPay where ConfDate>='"+mDay[0]+"' "
                +"and confDate<='"+mDay[1]+"' "
                +"and ManageCom like '"+mGlobalInput.ManageCom+"%' and IncomeType in('10','3') "
                +")and feefinatype in ( 'BF','GB','TJ') "
                +"group by modifydate,managecom "
                +"order by modifydate,managecom";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //应收保费
    private boolean getFeeDataPremtl()
    {
        String msql = "select '应付保费',makedate,sum(sumduepaymoney),managecom from ljspayb "
                    + "where makedate>='"+mDay[0]+"' "
                    + "and makedate <= '"+mDay[1]+"' "
                    + "and managecom like '"+mGlobalInput.ManageCom+"%' "
                    + "and othernotype in ('1','2') "
                    + "and dealstate <> '2' "
                    + "group by makedate,managecom ";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //应收保费转保费收入
    private boolean getFeeDataPremadvanceconf()
    {
        String msql = "select '应收保费转保费收入',Confmakedate,sum(paymoney),managecom from LJTempFee "
                      +"where Confmakedate>='"+mDay[0]+"' and Confmakedate <= '"+mDay[1]
                      +"'and ManageCom like '"+mGlobalInput.ManageCom
                      +"%' and TempFeeType = '2'group by confmakedate,managecom";
        if (!productXml(msql))
              return false;
        else
              return true;
    }
    //佣金计算 “佣金”
    private boolean PayCommionJ()
    {
        String msql = "";
        return true;
    }
    //生成XML函数
    private boolean productXml(String msql)
    {
        SSRS tSSRS = new SSRS();
        SSRS nSSRS = new SSRS();
        double ESumMoney = 0;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum = "";
        String strArr[] = null;
        tlistTable.setName("ENDOR");
        for (int i = 1; i <= tSSRS.MaxRow; i++) {
            strArr = new String[4];
            for (int j = 1; j <= tSSRS.MaxCol; j++) {
                if (j == 1) {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2) {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3) {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    ESumMoney = ESumMoney + Double.parseDouble(strArr[j - 1]);
                }
                if (j == 4) {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
            }
            tlistTable.add(strArr);
        }
        ESumMoney = PubFun.setPrecision(ESumMoney,"0.00");
        strArr = new String[5];
        strArr[0] = "EndorName";
        strArr[1] = "EndorDate";
        strArr[2] = "Money";
        strArr[3] = "ManageCom";

        String nsql = "select Name from LDCom where ComCode='" +
                      mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("OtoFDayBalance.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", ESumMoney);


        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("E:\\","zanshou");
        return true;
    }
}
