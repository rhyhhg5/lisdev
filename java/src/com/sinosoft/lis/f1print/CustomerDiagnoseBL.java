package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;
import java.lang.*;

public class CustomerDiagnoseBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData;
    private String ManageCom; //管理机构
    private String MakeDate1; //入机日期,起始日期
    private String MakeDate2; //入机日期,终止日期
    private String FeeType; //费用类型
    private String hospital;
    private String ContType; //保单类型（个单or团单）

    public CustomerDiagnoseBL() {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "CustomerDiagnoseBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        MakeDate1 = (String) mTransferData.getValueByName("MakeDate1"); //统计日期,起始日期
        MakeDate2 = (String) mTransferData.getValueByName("MakeDate2"); //统计日期,终止日期
        FeeType = (String) mTransferData.getValueByName("FeeType"); //费用类型
        hospital = (String) mTransferData.getValueByName("hospital"); //给付一家医院，给付一家以上医院
        ManageCom = (String) mTransferData.getValueByName("ManageCom");
        ContType = (String) mTransferData.getValueByName("ContType"); //保单类型

        String md1 = "";
        String md2 = "";
        String makedate = ""; //统计时间,结案日期
        String strFeeType = ""; //费用类型
        String strManageCom = "";
        String conttype = "";
        if (MakeDate1 != null && MakeDate2 != null && !MakeDate2.equals("") &&
            !MakeDate1.equals("")) {
            makedate = " and m.feedate between '" + MakeDate1 + "' and '" +
                       MakeDate2 + "'";
            md1 = CommonBL.decodeDate(MakeDate1);
            md2 = CommonBL.decodeDate(MakeDate2);
            texttag.add("Date1", md1);
            texttag.add("Date2", md2);
        }
        if ((MakeDate1.equals("") || MakeDate1 == null) &&
            (MakeDate2.equals("") || MakeDate2 == null)) {
            MakeDate2 = new PubFun().getCurrentDate();
            md2 = CommonBL.decodeDate(MakeDate2);
            texttag.add("Date2", md2);
        }
        if (ManageCom != null && !ManageCom.equals("")) {
            strManageCom = " and c.managecom like '" + ManageCom + "%'";
        }
        if (!FeeType.equals("0")) {
            strFeeType = " and m.FeeType='" + FeeType + "'";
        }
        if (ContType != null && !ContType.equals("")) {
            conttype = " and c.conttype='" + ContType + "'";
            if(ContType.equals("1")){
                texttag.add("ContType", "个险");
            }else if(ContType.equals("2")){
                texttag.add("ContType","团险");
            }else{
                texttag.add("ContType","");
            }
        }

        ListTable tListTable = new ListTable();
        tListTable.setName("table1");

        String strhospital = "";
        if (hospital.equals("1")) {
            strhospital = " and not exists (select 1 from llfeemain a where a.caseno=m.caseno and a.hospitalcode<>m.hospitalcode)";
        } else if (hospital.equals("2")) {
            strhospital = " and exists (select 1 from llfeemain a where a.caseno=m.caseno and a.hospitalcode<>m.hospitalcode)";
        } else if (hospital.equals("12")) {
            strhospital = "";
        }

        String sql = " select num 客户号,ming 姓名,pol 险种,sex 性别,nian 年龄,feedate 就诊日期,hospitalcode 就诊医院,levelcode 医院等级,classcode 医院合作级别,diseasename 住院疾病,realdate 住院天数,"
                      + " sum(sumfee) 住院费用 from "
                      + " (select X.num,X.ming,X.sex,X.nian,X.pol,X.feedate,X.hospitalcode,X.realdate,"
                      + " l.diseasename,"
                      + " X.levelcode,X.classcode,X.sumfee from  "
                      + " (select distinct a.caseno caseno,a.caserelano caserelano,m.customerno num,d.name ming,d.sex sex,year(current date -d.birthday) nian,"
                      + " (select riskname from lmriskapp where riskcode=a.riskcode) pol,"
                      + " m.feedate feedate,"
                      + " (select HospitName from ldhospital h where h.HospitCode=m.hospitalcode) hospitalcode,"
                      + " m.realhospdate realdate,"
                      +" (select LevelCode from ldhospital h where h.HospitCode=m.hospitalcode) levelcode,"
                      + " (select AssociateClass from ldhospital h where h.HospitCode=m.hospitalcode) classcode,"
                      + " m.sumfee sumfee"
                      + " from LLClaimDetail a,ldperson d,llfeemain m,lcpol c where 1=1 "
                      + strManageCom + makedate + strFeeType + conttype
                      + "  and c.polno=a.polno and a.caserelano = m.caserelano and m.caseno= a.caseno and d.customerno=m.CustomerNo "
                      + strhospital
                      + " ) as X,llcasecure l where X.caseno=l.caseno and X.caserelano=l.caserelano) as Y"
                      + " group by num,ming,sex,nian,pol,feedate,hospitalcode,realdate,diseasename,levelcode,classcode"
                      + " union all "
                      + " select num 客户号,ming 姓名,pol 险种,sex 性别,nian 年龄,feedate 就诊日期,hospitalcode 就诊医院,levelcode 医院等级,classcode 医院合作级别,'' as 住院疾病,realdate 住院天数,"
                      + " sum(sumfee) 住院费用 "
                      + " from  "
                      + " (select distinct a.caseno caseno,a.caserelano caserelano,m.customerno num,d.name ming,d.sex sex,year(current date -d.birthday) nian,"
                      + " (select riskname from lmriskapp where riskcode=a.riskcode) pol,"
                      + " m.feedate feedate,"
                      + " (select HospitName from ldhospital h where h.HospitCode=m.hospitalcode) hospitalcode,"
                      + " m.realhospdate realdate,"
                      + " (select LevelCode from ldhospital h where h.HospitCode=m.hospitalcode) levelcode,"
                      + " (select AssociateClass from ldhospital h where h.HospitCode=m.hospitalcode) classcode,"
                      + " m.sumfee sumfee"
                      + " from LLClaimDetail a,ldperson d,llfeemain m,lcpol c where 1=1 "
                      + strManageCom + makedate + strFeeType + conttype
                      + " and c.polno=a.polno "
                      + " and a.caserelano = m.caserelano and m.caseno= a.caseno and d.customerno=m.CustomerNo "
                      + strhospital
                      + " ) as X where not exists (select 1 from llcasecure l where l.caseno=X.caseno and l.caserelano=X.caserelano)"
                      + " group by num,ming,sex,nian,pol,feedate,hospitalcode,realdate,levelcode,classcode"
                      + " with ur ";

        System.out.println(sql);
        SSRS sbb = new ExeSQL().execSQL(sql);
        if (sbb != null) {
            int len = sbb.getMaxRow();
            for(int m=1;m<=len;m++){
                String v[] = {"", "", "", "", "", "", "", "", "", "", "", ""};
                for (int i = 0; i < sbb.getMaxCol(); i++) {
                    if (sbb.GetText(m, i + 1) != "null") {
                        v[i] = sbb.GetText(m, i + 1);
                    }
                }
                tListTable.add(v);
            }
        }

        String sqlm = "select distinct substr(Comcode,1,4) as comcode from LDcom where 1 = 1 and Sign='1' and comcode like '" +
                      ManageCom + "%' order by comcode with ur";
        String manage = new ExeSQL().getOneValue(sqlm); //机构号
        String sqln = "select name from LdCom  where ComCode='" +
                      manage + "' with ur";
        String name = new ExeSQL().getOneValue(sqln); //机构名
        texttag.add("ManageCom", name);

        XmlExport xmlexport = new XmlExport();

        xmlexport.createDocument("CustomerDiagnose.vts", "printer"); //分机构

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tListTable, new String[12]);
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {

        CustomerDiagnoseBL tHospitalClaimBL = new CustomerDiagnoseBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        tHospitalClaimBL.getPrintData();

    }
}
