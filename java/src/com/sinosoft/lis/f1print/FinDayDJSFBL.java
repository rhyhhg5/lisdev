package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinDayDJSFBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //业务处理相关变量
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private LMRiskAppSet mLMRiskAppSet;
    private String mRiskName = "";

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public FinDayDJSFBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        if (cOperate.equals("PRINTPAY")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86110000";
        VData vData = new VData();
        String[] tDay = new String[2];
        tDay[0] = "2004-1-13";
        tDay[1] = "2004-1-13";
        vData.addElement(tDay);
        vData.addElement(tG);

        FinDayDJSFBL tF = new FinDayDJSFBL();
        tF.submitData(vData, "PRINTPAY");

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        //11-26
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayDJPremBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
                private boolean getPrintDataPay()
                {
                          SSRS tSSRS = new SSRS();
        double SumMoney = 0;

        String msql = "select a.a,a.b from (select otherno a,sum(paymoney) b from ljtempfee"+
                      "where tempfeeno in(select getnoticeno from ljapay where incometype='13' "+
                      "and sumactupaymoney <> 0) and confmakedate>='2006-01-02' and ManageCom like '86%'"+
                      "and riskcode not in (Select RiskCode From LMRiskApp Where "+
                      "Risktype3 = '7' and riskcode <> '170101') group by otherno "+
                      "union "+
                      "Select endorsementno a,sum(getmoney) b from ljagetendorse "+
                      "where  actugetno in (  Select Btactuno from ljaedorbaldetail "+
                      "where  actuno in "+
                      "(  Select actugetno from LJAGet where  othernotype='13' and sumgetmoney <> 0 "+
                      "and confdate>='2006-01-01' "+
                      "and ManageCom like '86%' ))  "+
                      "and riskcode not in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and "+
                      "riskcode <> '170101') group by endorsementno) as a";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("ENDOR");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[4];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]  == null){
                  System.out.println("什么阿什么啊???");
                    }
                  System.out.println(strArr[j - 1]);
                }
                if (j == 3)
                {
                     strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1] == null){
                    System.out.println("为什么不能转化呢????");
                    }
                     System.out.println(strArr[j - 1]);
                }
                if (j == 4)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
//                   String strSum = new DecimalFormat("0.00").format(Double.
//                         valueOf(strArr[j - 1]));
//                    strArr[j - 1] = strSum;
                    SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);

                }
            }
            tlistTable.add(strArr);
        }

        strArr = new String[4];
        strArr[0] = "ENDORNO";
        strArr[1] = "POSITIVE";
        strArr[2] = "NEGATIVE";
        strArr[3] = "Money";

        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FinDayDJPrem.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", SumMoney);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
                }

}
