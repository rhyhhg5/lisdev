package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInformationDB;
import com.sinosoft.lis.db.LCInformationItemDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInformationItemSchema;
import com.sinosoft.lis.schema.LCInformationSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInformationItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.schema.LAComSchema;

public class AskInfoPrintBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的保单号码
    private String mContNo = "";
    //String InsuredName="";
    //输入的查询sql语句
    private String msql = "";
    //取得的延期承保原因
    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    public AskInfoPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq(); //打印流水号

        System.out.println("PrtNo = " + PrtNo);
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        boolean PEFlag = false; //打印体检件部分的判断标志
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        int m, i;
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();

        mAgentCode = mLCGrpContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);

        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(mLCGrpContSchema.getAgentCom());
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema=tLAComDB.getSchema();

        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

//2-补充资料信息
//2-1 查询补充材料主表
        LCInformationDB tLCInformationDB = new LCInformationDB();
        LCInformationSchema tLCInformationSchema = new LCInformationSchema();
        tLCInformationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCInformationDB.setPrtSeq(PrtNo);
        //tLCInformationDB.setCustomerNo(mLOPRTManagerSchema.getStandbyFlag1()) ;

        if (!tLCInformationDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得体检通知的数据时发生错误");
            return false;
        }

        tLCInformationSchema.setSchema(tLCInformationDB.getSchema());

//2-1 查询体检子表
        String[] PETitle = new String[3];
        PETitle[0] = "ID";
        PETitle[1] = "CHECKITEM";
        PETitle[2] = "Remark";
        ListTable tPEListTable = new ListTable();
        String strPE[] = null;
        tPEListTable.setName("INFO"); //对应模版体检部分的行对象名
        LCInformationItemDB tLCInformationItemDB = new LCInformationItemDB();
        LCInformationItemSet tLCInformationItemSet = new LCInformationItemSet();
        tLCInformationItemDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCInformationItemDB.setPrtSeq(PrtNo);

        tLCInformationItemSet.set(tLCInformationItemDB.query());
        if (tLCInformationItemSet == null || tLCInformationItemSet.size() == 0)
        {
            PEFlag = false;
        }
        else
        {
            PEFlag = true;
            LCInformationItemSchema tLCInformationItemSchema;
            for (i = 1; i <= tLCInformationItemSet.size(); i++)
            {
                tLCInformationItemSchema = new LCInformationItemSchema();
                tLCInformationItemSchema.setSchema(tLCInformationItemSet.get(i));
                strPE = new String[3];
                strPE[0] = (new Integer(i)).toString(); //序号
                strPE[1] = tLCInformationItemSchema.getInformationName(); //序号对应的内容
                strPE[2] = tLCInformationItemSchema.getRemark();
                tPEListTable.add(strPE);
            }
        }
        //险种信息

        String[] RiskInfoTitle = new String[4];

        RiskInfoTitle[0] = "RiskName"; //险种名称

        RiskInfoTitle[1] = "Peoples2"; //总投保人数
        RiskInfoTitle[2] = "Prem"; //保费

        RiskInfoTitle[3] = "SumPrem"; //累计保费
        ListTable tRiskListTable = new ListTable();
        tRiskListTable.setName("MAIN"); //对应模版投保信息部分的行对象名
        String strRisk[] = new String[4];

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCGrpPolSet = tLCGrpPolDB.query();
        if (tLCGrpPolSet == null)
        {

        }
        else
        {

            for (i = 1; i <= tLCGrpPolSet.size(); i++)
            {
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(tLCGrpPolSet.get(i).getRiskCode());
                if (tLMRiskDB.getInfo() == false)
                {
                    return false;
                }

                strRisk[0] = tLMRiskDB.getRiskName();

                Double Peoples2 = new Double(tLCGrpPolSet.get(i).getPeoples2());
                strRisk[1] = Peoples2.toString();
                Double Prem = new Double(tLCGrpPolSet.get(i).getPrem());
                strRisk[2] = Prem.toString();

                Double Amnt = new Double(tLCGrpPolSet.get(i).getAmnt());
                strRisk[3] = Amnt.toString();
                tRiskListTable.add(strRisk);
            }
        }
//其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("DateSupply.vts", "printer"); //最好紧接着就初始化xml文档

        //模版自上而下的元素
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("LCGrpCont.AppntName", mLCGrpContSchema.getGrpName()); //投保人名称
        texttag.add("LCGrpCont.ContNo", mLCGrpContSchema.getGrpContNo()); //投保单号
        texttag.add("LCGrpCont.AskGrpContNo", mLCGrpContSchema.getAskGrpContNo()); //询价号码
        if (mLCGrpContSchema.getSaleChnl().equals("03")) {
            texttag.add("LCGrpCont.AgentCom", mLCGrpContSchema.getAgentCom()); //代理机构编码
            texttag.add("LACom.Name", tLAComSchema.getName()); //代理机构名称
        } else {
            texttag.add("LCGrpCont.AgentCom",""); //代理机构编码
            texttag.add("LACom.Name", "");
        }
        texttag.add("SysDate", SysDate);

        texttag.add("LAAgent.Name", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("LCGrpCont.AgentCode", mLCGrpContSchema.getAgentCode()); //代理人业务号
        texttag.add("LCGrpCont.ManageCom",
                    getComName(mLCGrpContSchema.getManageCom())); //营业机构
        texttag.add("LCGrpCont.ManageCode", mLCGrpContSchema.getManageCom());
        texttag.add("PrtNo", PrtNo); //刘水号
        texttag.add("LCGrpCont.PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号
        texttag.add("LCGrpCont.UWCode", tLCInformationDB.getOperator()); // 核保师代码

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存补充资料信息
        if (PEFlag == true)
        {
            xmlexport.addListTable(tPEListTable, PETitle); //保存补充资料信息
        }
        //保存保险计划调整
        //保存险种信息

        xmlexport.addListTable(tRiskListTable, RiskInfoTitle);

       // xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode
     * @return
     * @throws Exception
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }
}
