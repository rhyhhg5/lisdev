package com.sinosoft.lis.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.EasyScanQueryBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDataList;
import com.sinosoft.utility.XMLDataMine;
import com.sinosoft.utility.XMLDataTag;
import com.sinosoft.utility.XMLDataset;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.utility.XmlExport;

import org.jdom.Element; 
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.pubfun.PubFun1; 

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCGrpSuccorNewECBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCAppntSchema mLCAppntSchema = new LCAppntSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LDComSchema mLDComSchema = new LDComSchema();

    private String mTemplatePath = "";

    private String mOutXmlPath = null;

    private XMLDatasets mXMLDatasets = null;

    private String[][] mPicFile = null;

    private TransferData mTransferData;

    private MMap Map = new MMap();

    //  private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    //    private Vector m_vGrpPolNo = new Vector();
    public LCGrpSuccorNewECBL() {
    }

    /**
     * 主程序
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            //全局变量赋值
            mOperate = cOperate;
            //得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }
            //新建一个xml对象
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            //数据准备
            if (!getPrintData()) {
                return false;
            }

            if (!dealPrintMag()) {
                return false;
            }

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //正常打印
//        if (mOperate.equals("PRINT"))
//        {
        mGlobalInput.setSchema((GlobalInput) cInputData
                               .getObjectByObjectName("GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));

        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='ServerRoot'";//生成文件的存放路径
        mTemplatePath = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
//        mTemplatePath = "F:\\application\\lisdev\\ui\\";
        System.out.println("文件的存放路径   "+mTemplatePath);//调试用－－－－－
        if(mTemplatePath == null ||mTemplatePath.equals("")){
            buildError("getFileUrlName","获取文件存放路径出错");
            return false;
        }
        mTemplatePath += "f1print/template/";

        return true;
    }

    /**
     * 返回信息
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception {
        //团单Schema
//        LCContSchema tLCContSchema = null;
        //原则上一次只打印一个团单，当然可以一次多打
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo()) {
            System.out
                    .println("getInfo" + tLCContDB.mErrors.getErrContent());
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();

        //取得该团单下的所有投保险种信息
        LCPolSet tLCPolSet = getRiskList(mLCContSchema.getContNo());

        //正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(tLCPolSet)) {
            return false;
        }

        //将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        //    mResult.add(tLCPolSet);//其实没必要更新这个表
        this.mLCContSchema.setPrintCount(1);
//        mResult.add(this.mLCContSchema); //只更新这个表就好拉
        mResult.add(mXMLDatasets); //xml数据流
        mResult.add(mGlobalInput); //全局变量
//        mResult.add(mOutXmlPath); //输出目录
        //更新保单打印次数
        Map.put("update LCCont set printcount=1  where  prtno='"
                + this.mLCContSchema.getPrtNo() + "' and printcount<>1",
                "UPDATE");
        System.out.println("update LCCont set printcount=1  where  prtno='"
                           + this.mLCContSchema.getPrtNo() +
                           "' and printcount<>1");
//        mResult.clear();
        VData tVData = new VData();
        tVData.add(Map);
        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(tVData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LCGrpSuccorBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("ImpartToICDBL end");

        return true;
    }

    /**
     * deleteFile
     *
     * @param XmlFile String
     * @return boolean
     */
    private boolean deleteFile(String XmlFile) {
        String file = StrTool.replaceEx(XmlFile, ".xml", ".pdf");
        file = StrTool.replaceEx(file, "brief", "briefpdf");
        try {
            File tFile = new File(file);
            if (tFile.exists()) {
                tFile.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("deleteFile", "重打保单失败，原因是，ｐｄｆ文件存在但是删除原ｐｄｆ文件失败！");
            return false;
        }
        return true;
    }

    /**
     * creatXmlFile
     *
     * @param XmlFile String
     * @throws Exception
     * @return boolean
     */
    private boolean creatXmlFile(String XmlFile) {
        InputStream ins = mXMLDatasets.getInputStream();

        try {
            FileOutputStream fos = new FileOutputStream(XmlFile);
            int n = 0;
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1) {
                fos.write(c, 0, n);
            }
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("creatXmlFile", "生成Xml文件失败！");
            return false;
        }
        return true;
    }

    /**
     * 查询协议影印件数据
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    //  private boolean getScanDoc(XMLDataset cXmlDataset)
    //          throws
    //          Exception
    //  {
    //    XMLDataList tXmlDataList = new XMLDataList();
    //    //标签对象，团单需要的影印件有两种类型
    //    tXmlDataList.setDataObjectID("DocFile");
    //
    //    //标签中的Header
    //    tXmlDataList.addColHead("FileUrl");
    //    tXmlDataList.addColHead("HttpUrl");
    //    tXmlDataList.addColHead("PageIndex");
    //    tXmlDataList.buildColHead();
    //
    //    VData tVData = new VData();
    //    tVData.add(mLCContSchema.getPrtNo());
    //    tVData.add("12");
    //    tVData.add("TB");
    //    tVData.add("TB1002");
    //    EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();
    //    if (!tEasyScanQueryBL.submitData(tVData, "QUERY||MAIN"))
    //    {
    //      System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
    ////            buildError("getScanDoc", "查询保单影印件出错！");
    ////            return false;
    ////            return true;
    //    }
    //    else
    //    {
    //      tVData.clear();
    //      tVData = tEasyScanQueryBL.getResult();
    //      //Http信息对象
    //      VData tUrl = (VData) tVData.get(0);
    //      //页面信息对象
    //      VData tPages = (VData) tVData.get(1);
    //      String tStrUrl = "";
    //      String tStrPages = "";
    //      //根据查询结果初始化影印件信息数组
    //      mDocFile = new String[tUrl.size()][2];
    //      for (int nIndex = 0; nIndex < tUrl.size(); nIndex++)
    //      {
    //        tStrUrl = (String) tUrl.get(nIndex);
    //        tStrPages = (String) tPages.get(nIndex);
    //        tStrUrl = tStrUrl.substring(0,
    //                                    tStrUrl.lastIndexOf(".")) + ".tif";
    //        mDocFile[nIndex][0] = tStrUrl;
    //        //协议影印件信息，采用此命名方法
    //        mDocFile[nIndex][1] = "Agreement_" +
    //                              mLCContSchema.getContNo() +
    //                              "_" + tStrPages + ".tif";
    //        tXmlDataList.setColValue("FileUrl", mDocFile[nIndex][1]);
    //        tXmlDataList.setColValue("HttpUrl", tStrUrl);
    //        tXmlDataList.setColValue("PageIndex", nIndex);
    //        tXmlDataList.insertRow(0);
    //      }
    //    }
    //    cXmlDataset.addDataObject(tXmlDataList);
    //    return true;
    //  }
    /**
     * 查询影印件数据
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getScanPic(XMLDataset cXmlDataset) throws Exception {
        XMLDataList tXmlDataList = new XMLDataList();
        //标签对象，团单需要的影印件有两种类型
        tXmlDataList.setDataObjectID("PicFile");

        //标签中的Header
        tXmlDataList.addColHead("FileUrl");
        tXmlDataList.addColHead("HttpUrl");
        tXmlDataList.addColHead("PageIndex");
        tXmlDataList.buildColHead();

        VData tVData = new VData();
        tVData.add(mLCContSchema.getPrtNo());
        tVData.add("12");
        tVData.add("TB");
        tVData.add("TB1002");
        EasyScanQueryBL tEasyScanQueryBL = new EasyScanQueryBL();
        if (!tEasyScanQueryBL.submitData(tVData, "QUERY||MAIN")) {
            System.out.println(tEasyScanQueryBL.mErrors.getFirstError());
            //            buildError("getScanPic", "查询保单影印件出错！");
            //            return false;
            //            return true;
        } else {
            tVData.clear();
            tVData = tEasyScanQueryBL.getResult();
            //Http信息对象
            VData tUrl = (VData) tVData.get(0);
            //页面信息对象
            VData tPages = (VData) tVData.get(1);
            String tStrUrl = "";
            String tStrPages = "";
            //根据查询结果初始化影印件信息数组
            mPicFile = new String[tUrl.size()][2];
            for (int nIndex = 0; nIndex < tUrl.size(); nIndex++) {
                tStrUrl = (String) tUrl.get(nIndex);
                tStrPages = (String) tPages.get(nIndex);
                tStrUrl = tStrUrl.substring(0, tStrUrl.lastIndexOf("."))
                          + ".tif";
                mPicFile[nIndex][0] = tStrUrl;
                //保单影印件信息，采用此命名方法
                mPicFile[nIndex][1] = mLCContSchema.getContNo() + "_"
                                      + tStrPages + ".tif";
                tXmlDataList.setColValue("FileUrl", mPicFile[nIndex][1]);
                tXmlDataList.setColValue("HttpUrl", tStrUrl);
                tXmlDataList.setColValue("PageIndex", nIndex);
                tXmlDataList.insertRow(0);
            }
        }
        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getSpecDoc(XMLDataset cXmlDataset) throws Exception {
        XMLDataList xmlDataList = new XMLDataList();
        //添加xml一个新的对象Term
        xmlDataList.setDataObjectID("LCCGrpSpec");
        //添加Head单元内的信息
        xmlDataList.addColHead("RowID");
        xmlDataList.addColHead("SpecContent");
        xmlDataList.buildColHead();
/*        //查询合同下的特约信息
        LCCGrpSpecDB tLCCGrpSpecDB = new LCCGrpSpecDB();
        tLCCGrpSpecDB.setProposalContNo(this.mLCContSchema
                                           .getProposalContNo());
        LCCGrpSpecSet tLCCGrpSpecSet = tLCCGrpSpecDB.query();
        for (int i = 1; i <= tLCCGrpSpecSet.size(); i++) {
            xmlDataList.setColValue("RowID", i);
            xmlDataList.setColValue("SpecContent", tLCCGrpSpecSet.get(i)
                                    .getSpecContent());
            xmlDataList.insertRow(0);
        }*/
        cXmlDataset.addDataObject(xmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * @param cXmlDataset XMLDataset
     * @param cLCPolSet LCPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getTerm(XMLDataset cXmlDataset, LCPolSet cLCPolSet) throws
            Exception {
        XMLDataList tXmlDataList = new XMLDataList();
        //添加xml一个新的对象Term
        tXmlDataList.setDataObjectID("Term");
        //添加Head单元内的信息
        tXmlDataList.addColHead("PrintIndex");
        tXmlDataList.addColHead("TermName");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.addColHead("DocumentName");
        tXmlDataList.buildColHead();

        //设置查询对象
        ExeSQL tExeSQL = new ExeSQL();
        //查询保单下的险种条款，根据条款级别排序
        //查询合同下的全部唯一主条款信息
        String tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '"
                      + this.mLCContSchema.getContNo()
                      + "') and ItemType = '0'";
        SSRS tSSRS = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            tXmlDataList.setColValue("PrintIndex", i);
            tXmlDataList.setColValue("TermName", tSSRS.GetText(i, 1));
            tXmlDataList.setColValue("FileName", "0");
            tXmlDataList.setColValue("DocumentName", tSSRS.GetText(i, 2));
            tXmlDataList.insertRow(0);
            //查询当前主条款下的责任条款信息
            tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCPol where ContNo = '"
                   + this.mLCContSchema.getContNo()
                   + "') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where FileName = '"
                   + tSSRS.GetText(i, 2) + "')";
            tSSRS2 = tExeSQL.execSQL(tSql);
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
                tXmlDataList.setColValue("PrintIndex", i);
                tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                tXmlDataList.setColValue("FileName", "1");
                tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                tXmlDataList.insertRow(0);
            }
        }

        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 正常打印流程
     * @param cLCPolSet LCPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(LCPolSet cLCPolSet) throws Exception {
        //xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();
        ExeSQL typeExeSQL = new ExeSQL();
        String typeSql = "select othersign from ldcode where codetype='ECRescue'";

        String JetFormType = typeExeSQL.getOneValue(typeSql);
        //团个单标志，1个单，2团单
        tXMLDataset.addDataObject(new XMLDataTag("JetFormType",
        		JetFormType));
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", printcode));
        tXMLDataset.addDataObject(new XMLDataTag("userIP",
                                                 mGlobalInput.ClientIP.
                                                 replaceAll("\\.", "_")));
        if ("batch".equals(mOperate)) {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "0"));
        } else {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "1"));
        }

        tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));

        //获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo()) {
            //只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError()) {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //默认填入空
            tXMLDataset.setTemplate("");
        } else {
            //团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            //打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }

        //查询个单合同表信息
//        LCContDB tLCContDB = new LCContDB();
//        //根据合同号查询
//        tLCContDB.setContNo(this.mLCContSchema.getContNo());
//        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
//        if (!tLCContDB.getInfo()) {
//            System.out
//                    .println("getInfo" + tLCContDB.mErrors.getErrContent());
//            buildError("getInfo", "查询个单合同信息失败！");
//            return false;
//        }
//        this.mLCContSchema = tLCContDB.getSchema();
//        if (StrTool.cTrim(mLCContSchema.getCardFlag()).equals("0")) {
//            mXMLDatasets.setDTDName(this.mLCContSchema.getContNo()
//                                    + ".dtd");
//        }
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCContSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpContNo",
                                                 this.mLCContSchema.
                                                 getContNo()));
        tXMLDataset.addDataObject(new XMLDataTag("XI_ManageCom",
                                                 mLCContSchema.getManageCom()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.PrtNo",
                                                 this.mLCContSchema.getPrtNo()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AppntNo",
                                                 this.mLCContSchema.
                                                 getAppntNo()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Peoples2",
                                                 this.mLCContSchema.
                                                 getPeoples()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.GrpName",
                                                 this.mLCContSchema.getAppntName()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SignDate", PubFun
                                                 .getCurrentDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CValiDate",
                                                 this.mLCContSchema.
                                                 getCValiDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.SumPrem", String
                                                 .valueOf(this.mLCContSchema.
                getSumPrem())));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Remark", StrTool
                                                 .cTrim(this.mLCContSchema.
                getRemark())));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.CInValiDate",
                                                 this.mLCContSchema.
                                                 getCInValiDate()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.DegreeType",
                                                 this.mLCContSchema.
                                                 getDegreeType()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.Operator",
                                                 this.mLCContSchema.
                                                 getOperator()));
        tXMLDataset.addDataObject(new XMLDataTag("LCGrpCont.AgentCode",
                                                 this.mLCContSchema.
                                                 getAgentCode()));
        //保全补打 qulq 2007-11-28
        tXMLDataset.addDataObject(new XMLDataTag("LostTimes",
                                                 this.mLCContSchema.
                                                 getLostTimes()));
        tXMLDataset.addDataObject(new XMLDataTag("BQRePrintDate",
                                                 com.sinosoft.lis.bq.CommonBL.
                                                 decodeDate(PubFun.
                getCurrentDate())
                                  ));

        //tXMLDataset.addDataObject(new XMLDataTag("LCCont.Uwoperator", this.mLCContSchema.getUWOperator()));
        //如果是卡单型险种需要查询抵达国家
        if (!StrTool.cTrim(mLCContSchema.getCardFlag()).equals("")) {
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setContNo(mLCContSchema.getContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++) {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size()) {
                    Nation += ",";
                }
            }
            tXMLDataset.addDataObject(new XMLDataTag("Nation", Nation));
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCContSchema.getAppntNo());
            tLDGrpDB.getInfo();
            tXMLDataset.addDataObject(new XMLDataTag("GrpEnglithName", tLDGrpDB
                    .getGrpEnglishName()));
            /** 增加签单机构 */
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(mLCContSchema.getManageCom());
            if (!tLDComDB.getInfo()) {
                buildError("getGrpPolDataSet", "查询失败！");
                return false;
            }
            tXMLDataset.addDataObject(new XMLDataTag("SignCom", tLDComDB
                    .getLetterServiceName()));
            tXMLDataset.addDataObject(new XMLDataTag("SignComEnName", tLDComDB
                    .getEName()));
            tXMLDataset.addDataObject(new XMLDataTag("EnPostAddress", tLDComDB
                    .getEAddress()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePhone", tLDComDB
                    .getPhone()));
            //        tXMLDataset.addDataObject(new XMLDataTag("ServicePhone",
            //                                                 tLDComDB.getServicePhone()));
            tXMLDataset.addDataObject(new XMLDataTag("ServicePostAddress",
                    tLDComDB.getServicePostAddress()));
        }

        //查询投保人信息
        LCAppntDB tLCAppntDB = new LCAppntDB();
        //根据合同号查询
        tLCAppntDB.setContNo(mLCContSchema.getContNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCAppntDB.getInfo()) {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCAppntSchema = tLCAppntDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCAppntSchema);

        //查询投保人的地址信息
        LCAddressDB tLCAddressDB = new LCAddressDB();
        //根据投保人编码和地址编码查询
        tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCAddressDB.getInfo()) {
            buildError("getInfo", "查询个单投保人地址信息失败！");
            return false;
        }
        this.mLCAddressSchema = tLCAddressDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLCAddressSchema);

        //查询管理机构信息
        LDComDB tLDComDB = new LDComDB();
        //根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDComDB.getInfo()) {
            buildError("getInfo", "查询管理机构信息失败！");
            return false;
        }
        this.mLDComSchema = tLDComDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLDComSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LDCom.Name",
                                                 this.mLDComSchema.getName()));

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo()) {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        tXMLDataset.addDataObject(new XMLDataTag("LAAgent.Name", mLAAgentSchema
                                                 .getName()));
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLAAgentSchema);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.mLCContSchema.getAgentGroup());
        //如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLABranchGroupDB.getInfo()) {
            buildError("getInfo", "查询销售机构信息失败！");
            return false;
        }
        this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
        //将查询出的schema信息放入到xml对象中
        //tXMLDataset.addSchema(this.mLABranchGroupSchema);
        
        //by gzh 20120522 增加对中介机构及业务员的处理
//      by gzh 20120521
        //增加中介机构及中介机构销售人员信息
        String tAgentCom = this.mLCContSchema.getAgentCom();
        String tAgentComName = "";
        if(tAgentCom != null && !"".equals(tAgentCom)){
        	LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(tAgentCom);
//          如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAComDB.getInfo())
            {
            	buildError("getInfo", "查询中介机构信息失败！");
                return false;
            }
            this.mLAComSchema = tLAComDB.getSchema();
            tAgentComName = this.mLAComSchema.getName();
        }
//      将查询出的schema信息放入到xml对象中
//        tXMLDataset.addSchema(this.mLAComSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LACom.AgentCom", tAgentCom));
        tXMLDataset.addDataObject(new XMLDataTag("LACom.Name", tAgentComName));
        
        String tAgentSaleCode = this.mLCContSchema.getAgentSaleCode();
        String tAgentSaleName = "";
        if(tAgentSaleCode != null && !"".equals(tAgentSaleCode)){
        	LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
            tLAAgenttempDB.setAgentCode(tAgentSaleCode);
            // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAAgenttempDB.getInfo())
            {
                buildError("getInfo", "查询中介机构代理业务员信息失败！");
                return false;
            }
            this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
            tAgentSaleName = this.mLAAgenttempSchema.getName();
        }
        // 将查询出的schema信息放入到xml对象中
//        tXMLDataset.addSchema(this.mLAAgenttempSchema);
        tXMLDataset.addDataObject(new XMLDataTag("LAAgenttemp.AgentCode", tAgentSaleCode));
        tXMLDataset.addDataObject(new XMLDataTag("LAAgenttemp.Name", tAgentSaleName));
        // by gzh end 20120521

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql =
                "select cvalidate,cinvalidate from LCCont where ContNo = '"
                + this.mLCContSchema.getContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("BeginDate", tSSRS.GetText(1,
                1)));
        tXMLDataset
                .addDataObject(new XMLDataTag("EndDate", tSSRS.GetText(1, 2)));

        //暂交费收据号
        tSql = "select distinct TempFeeNo from LJTempFee where OtherNo = '"
               + this.mLCContSchema.getContNo()
               + "' ";
        tSSRS = tExeSQL.execSQL(tSql);
        //        if (!mLCContSchema.getAppFlag().equals("9")) {
        //            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo",
        //                    tSSRS.GetText(1, 1))); //收据号
        //        } else {
        tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", "")); //收据号
        //        }

        //总保费
        /*tSql = "select prem from LCCont where ContNo = '" +
         this.mLCContSchema.getContNo() + "'";
         tSSRS = tExeSQL.execSQL(tSql);
         tXMLDataset.addDataObject(new XMLDataTag("SumPrem",
         format(Double.
         parseDouble(tSSRS.GetText(1, 1))))); //总保费*/

        //操作人员编码和操作人员姓名
        tSql = "select UserCode,UserName from LDUser where UserCode in (Select Operator from LJTempFee where OtherNo = '"
               + this.mLCContSchema.getContNo()
               + "' )";
        tSSRS = tExeSQL.execSQL(tSql);
        //        if (!((String) mLCContSchema.getAppFlag()).equals("9")) {
        //            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode",
        //                    tSSRS.GetText(1, 1)));
        //            tXMLDataset.addDataObject(new XMLDataTag("Operator",
        //                    tSSRS.GetText(1, 2)));
        //        } else {
        tXMLDataset.addDataObject(new XMLDataTag("OperatorCode",
                                                 mGlobalInput.Operator));
        String tempSql = "select UserName from lduser where usercode='"
                         + mGlobalInput.Operator + "'";
        tSSRS = tExeSQL.execSQL(tempSql);
        tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS
                                                 .GetText(1, 1)));
        SSRS tRescueSSRS = new SSRS();
        String tRescueSql = "select code,codename from ldcode where codetype='ECRescue'";
        tRescueSSRS = tExeSQL.execSQL(tRescueSql);
        tXMLDataset.addDataObject(new XMLDataTag("RescueCom", tRescueSSRS
                .GetText(1, 2)));

        tXMLDataset.addDataObject(new XMLDataTag("RescueTel", tRescueSSRS
                .GetText(1, 1)));

        //        }
        //产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genInsuredList(tXMLDataset)) {  //需要调整xml模板取数sql
            return false;
        }

        //获取特约信息
        if (!getSpecDoc(tXMLDataset)) {
            return false;
        }

        //获取条款信息
        if (!getTerm(tXMLDataset, cLCPolSet)) {
            return false;
        }

        //获取协议影印件信息,以后打印协议时用
        //    if (!getScanDoc(tXMLDataset))
        //    {
        //      return false;
        //    }

        //产生团单下被保人清单
        if (!genInsuredDetail(tXMLDataset)) {  //需要调整xml模板取数sql
            return false;
        }

        //获取保单影印件信息
        //下面4行信息注掉，用于以后的补充协议打印
        if (!getScanPic(tXMLDataset)) {
            return false;
        }
        //获取被保险人急救医疗卡信息
        if (!StrTool.cTrim(this.mLCContSchema.getCardFlag()).equals("")) {
            if (!genInsuredCard(tXMLDataset)) {
                return false;
            }
        }
        //获取定点医院信息
        // if (!getHospital(tXMLDataset))
        //{
        //  return false;
        // }
        // System.out.println("定点医院明细准备完毕。。。");

        //获取发票信息
        //      if (!this.mLCContSchema.getAppFlag().equals("9"))
        //      {
        //        if (!getInvoice(tXMLDataset))
        //        {
        //          return false;
        //        }
        //      }
        if (!getEnglishName(tXMLDataset)) {
            return false;
        }

        return true;
    }

    /**
     * getEnglishName
     *
     * @param tXMLDataset XMLDataset
     * @return boolean
     */
    private boolean getEnglishName(XMLDataset tXMLDataset) {
    	String sql = "select riskcode from lmriskapp where riskcode in ('5102','1101')order by RiskCode ";
        SSRS ssrs = (new ExeSQL()).execSQL(sql);
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            String tRiskCode = ssrs.GetText(i, 1);
            String sql_EnName = "select RiskEnName from LMRisk a,LCPol b where a.RiskCode=b.RiskCode and b.ContNo='"
                                + mLCContSchema.getContNo()
                                + "' and b.RiskCode='"
                                + tRiskCode + "'";
            String tEnName = (new ExeSQL()).getOneValue(sql_EnName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "EnName", tEnName));
            String sql_ZhName = "select RiskName from LMRisk a,LCPol b where a.RiskCode=b.RiskCode and b.ContNo='"
                                + mLCContSchema.getContNo()
                                + "' and b.RiskCode='"
                                + tRiskCode + "'";
            String tZhName = (new ExeSQL()).getOneValue(sql_ZhName);
            tXMLDataset.addDataObject(new XMLDataTag("Risk" + tRiskCode
                    + "ZhName", tZhName));
        }
        return true;
    }

    /**
     * 根据个单合同号，查询明细信息
     * @param tXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getHospital(XMLDataset tXmlDataset) throws Exception {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件，mszTemplatePath为模板所在的应用路径
        tTemplateFile = mTemplatePath + "Hospital.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            buildError("getHospital", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable tHshData = new Hashtable();
            //将变量_ContNo的值赋给xml文件
            //            tHshData.put("_AREACODE", this.mLDComSchema.getRegionalismCode());
            tHshData.put("_AREACODE", "11%%");
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), tHshData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            tXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            //出错处理
            buildError("genHospital", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询明细信息
     * 责任信息、特约信息、个人险种明细、险种条款信息等
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredList(XMLDataset cXmlDataset) throws Exception {
//        StringBuffer tSBql = new StringBuffer(256);
//        tSBql.append("select b.RiskCode,b.RiskName from LCPol a, LMRisk b where a.RiskCode = b.riskcode and a.ContNo = '");
//        tSBql.append(this.mLCContSchema.getContNo());
//        tSBql.append("'");
//
//        ExeSQL tExeSQL = new ExeSQL();
//        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
//        //循环获取计划信息
//        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            cXmlDataset.addDataObject(new XMLDataTag(tSSRS.GetText(i, 1), tSSRS
//                    .GetText(i, 2)));
//        }

        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        if (StrTool.cTrim(this.mLCContSchema.getCardFlag()).equals("")) {
            tTemplateFile = mTemplatePath + "GrpContPrint.xml";
        } else {
            tTemplateFile = mTemplatePath + "ECContPrintCard.xml";
        }
        System.out.println(tTemplateFile);
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredList", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "ECInsured.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredDetail", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset cXmlDataset) throws Exception {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsuredCard.xml";

        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            //            throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        } catch (Exception e) {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 获取发票信息
     * @param cXmlDataset XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getInvoice(XMLDataset cXmlDataset) throws Exception {
        //数据流
        //        InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        //团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpContInvoice.xml";
        //校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists()) {
            buildError("getInvoice", "XML配置文件不存在！");
            return false;
        }
        try {
            XMLDataList tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("Money");
            tXMLDataList.addColHead("ChinaMoney");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("ChinaMoney", PubFun
                                     .getChnMoney(this.mLCContSchema.getPrem()));
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);

            Hashtable tHashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            tHashData.put("_GrpContNo", this.mLCContSchema.getContNo());
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), tHashData);
            //            tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);

            cXmlDataset.addDataObject(new XMLDataTag("Today", PubFun
                    .getCurrentDate())); //签发日期
            //            cXmlDataset.addDataObject(new XMLDataTag("RMB",
            //                                                     PubFun.getChnMoney(this.
            //                    mLCContSchema.getPrem()))); //保费大写
            //收款人，目前系统提供操作员信息

            //查询缴费方式信息
            //根据合同号，查询缴费记录信息
            //            String tSql =
            //                    "select distinct TempFeeNo from LJTempFee where OtherNo = '" +
            //                    this.mLCContSchema.getContNo() +
            //                    "' and OtherNoType = '7'";
            StringBuffer tSBql = new StringBuffer(256);
            tSBql
                    .append(
                            "select distinct TempFeeNo from LJTempFee where OtherNo = '");
            tSBql.append(this.mLCContSchema.getContNo());
            tSBql.append("' and OtherNoType = '7'");

            //            String tWhere = " where a.TempFeeNo in ('";
            StringBuffer tBWhere = new StringBuffer(" where a.TempFeeNo in ('");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                //将每笔缴费的缴费号串连起来作为查询条件
                if (i == tSSRS.getMaxRow()) {
                    //                    tWhere = tWhere + tSSRS.GetText(i, 1) + "')";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("')");
                } else {
                    //                    tWhere = tWhere + tSSRS.GetText(i, 1) + "','";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("','");
                }
            }
            //            cXmlDataset.addDataObject(new XMLDataTag("TempFeeNo",
            //                    tSSRS.GetText(1, 1))); //收据号

            //根据上面的查询条件，查询缴费方式为现金的数据
            //            tSql = "select a.ConfMakeDate from LJTempFeeClass a" +
            //                   tWhere + " and a.PayMode = '1' order by a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select a.ConfMakeDate from LJTempFeeClass a");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode = '1' order by a.ConfMakeDate");

            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0) {
                //添加缴费方式为现金的标签
                tXMLDataList = new XMLDataList();
                //添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Cash");
                //添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                //此缴费方式只需知道确认日期
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                    tXMLDataList.setColValue("PayMode", "现金");
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS
                                             .GetText(i, 1));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            //根据上面的查询条件，查询缴费方式不是现金的数据
            //            tSql =
            //                    "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a,LDCode b" +
            //                    tWhere + " and a.PayMode <> '1' and a.BankCode = b.Code and b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate";
            //        tSBql.setLength(0);
            //        tSBql.append(
            //                "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a,LDCode b");
            //        tSBql.append(tBWhere);
            //        tSBql.append(" and a.PayMode <> '1' and a.BankCode = b.Code and b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate");
            //
            //        tSSRS = tExeSQL.execSQL(tSBql.toString());
            //        if (tSSRS.getMaxRow() > 0)
            //        {
            //          //添加缴费方式为支票的标签
            //          tXMLDataList = new XMLDataList();
            //          //添加xml一个新的对象Term
            //          tXMLDataList.setDataObjectID("Check");
            //          //添加Head单元内的信息
            //          tXMLDataList.addColHead("PayMode");
            //          tXMLDataList.addColHead("Bank");
            //          tXMLDataList.addColHead("AccName");
            //          tXMLDataList.addColHead("BankAccNo");
            //          tXMLDataList.addColHead("ConfMakeDate");
            //          tXMLDataList.buildColHead();
            //          //此标签需要获得银行、户名、帐户的信息
            //          for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            //          {
            //            tXMLDataList.setColValue("PayMode", "银行转帐");
            //            tXMLDataList.setColValue("Bank", tSSRS.GetText(i, 1));
            //            tXMLDataList.setColValue("AccName", tSSRS.GetText(i, 2));
            //            tXMLDataList.setColValue("BankAccNo", tSSRS.GetText(i, 3));
            //            tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 4));
            //            tXMLDataList.insertRow(0);
            //          }
            //          cXmlDataset.addDataObject(tXMLDataList);
            //        }

            //添加xml结束标签
            tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("End");
            tXMLDataList.addColHead("Flag");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("Flag", "1");
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);
            //            cXmlDataset.addDataObject(new XMLDataTag("End", "1"));
        } catch (Exception e) {
            buildError("getInvoice", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 取得团单下的全部LCPol表数据
     * @param cContNo String
     * @return LCPolSet
     * @throws Exception
     */
    private static LCPolSet getRiskList(String cContNo) throws Exception {
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();

        tLCPolDB.setContNo(cContNo);
        //由于LCCont和LCPol为一对多的关系，所以采用query方法
        tLCPolSet = tLCPolDB.query();

        return tLCPolSet;
    }


    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue) {
        return new DecimalFormat("0.00").format(dValue);
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("02");
        mLOPRTManagerSchema.setCode("J01");
        mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }
    public static void main(String[] args) {
    	XMLDatasets mXMLDatasets = new XMLDatasets();
        mXMLDatasets.createDocument();
    	XMLDataset tXMLDataset = mXMLDatasets.createDataset();
//    	String tTemplateFile  = "F:\\电子商务\\网销境外救援\\ECContPrintCard.xml";
    	String tTemplateFile  = "F:\\电子商务\\网销境外救援\\ECInsured.xml";
//    	String tTemplateFile  = "F:\\电子商务\\网销境外救援\\GrpContPrintCard.xml";
        System.out.println(tTemplateFile);
    	try {
            Hashtable thashData = new Hashtable();
            //将变量ContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", "013936587000019"); //团单号：00000002000006 个单号：013936587000019 
            //根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(
                    tTemplateFile), thashData);
            tXMLDataset.addDataObject(tXmlDataMine);
            VData mResult = new VData();
            mResult.add(mXMLDatasets); //xml数据流
            String mStrUrl = "F:\\电子商务\\网销境外救援\\";
//            String mFileName = "testEC";
            String mFileName = "testEC1";
            XmlExport txmlExport = (XmlExport) mResult.getObjectByObjectName(
                    "XmlExport", 0);
            if (txmlExport == null) {
                System.out.println("没有所需的打印数据文件");
                XMLDatasets tXMLDatasets = (XMLDatasets) mResult.getObjectByObjectName("XMLDatasets",0);
                tXMLDatasets.outputCode(mStrUrl+mFileName+".xml","GB2312");
            } else {
            	System.out.println("Successful");
                txmlExport.outputDocumentToFile(mStrUrl, mFileName);
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}

}
