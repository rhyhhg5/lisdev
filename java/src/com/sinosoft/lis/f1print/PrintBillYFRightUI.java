package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;

public class PrintBillYFRightUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;

    public PrintBillYFRightUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            /*if (!getInputData(cInputData))
            {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }*/

            PrintBillYFRightBL tPrintBillYFRightBL = new PrintBillYFRightBL();

            System.out.println("Start PrintBillYFRight UI Submit ...");

            if (!tPrintBillYFRightBL.submitData(cInputData, cOperate))
            {
                if (tPrintBillYFRightBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPrintBillYFRightBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "FinChargeDayModeF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPrintBillYFRightBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PrintBillYFRightUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        vData.clear();
        vData.add(strBillNo);
        vData.add(strBankCode);
        vData.add(strMngCom);
        return true;
    }

    /*private boolean getInputData(VData cInputData)
    {
        //全局变量
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        strMngCom = (String) cInputData.get(2);
        return true;
    }*/

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PrintBillYFRightUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        String tBillNo = "00000000000000000079";
        String tBankCode = "0102";
        tG.ManageCom = "8694";
        VData tVData = new VData();
        tVData.addElement(tBillNo);
        tVData.addElement(tBankCode);
        tVData.addElement(tG);
        PrintBillYFRightUI tPrintBillUI = new PrintBillYFRightUI();
        tPrintBillUI.submitData(tVData, "PRINT");
    }
}
