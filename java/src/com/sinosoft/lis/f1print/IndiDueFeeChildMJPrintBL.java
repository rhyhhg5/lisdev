package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IndiDueFeeChildMJPrintBL implements PrintService{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCContSchema tLCContSchema = new LCContSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
    private LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private Object tExeSQL;
    public IndiDueFeeChildMJPrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("IndiDueFeeChildMJPrintBL begin");
        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT") )
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        if(cOperate.equals("INSERT") && needPrt==0)
        {
            if (!dealPrintMag())
            {
                return false;
            }
        }

        System.out.println("IndiDueFeeChildMJPrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if(ttLOPRTManagerSchema == null)
        {//为空--VTS打印入口
            System.out.println("---------为空--VTS打印入口！");
            tLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);
            tLJSPayBDB = (LJSPayBDB) cInputData.getObjectByObjectName("LJSPayBDB", 0);
            if (tLCContSchema == null)
            {
                buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
                return false;
            }
            return true;
        }
        else
        {//PDF入口
            //取得LJSPayBDB
            System.out.println("---------不为空--PDF入口！");
            LJSPayBDB mLJSPayBDB = new LJSPayBDB();
            mLJSPayBDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
            if(!mLJSPayBDB.getInfo())
            {
                mErrors.addOneError("PDF入口LJSPayBDB传入的数据不完整。");
                return false;
            }
            tLJSPayBDB = mLJSPayBDB.getDB();

            //取得tLCContSchema
            LCContDB mLCContDB = new LCContDB();
            mLCContDB.setContNo(mLJSPayBDB.getOtherNo());
            if(!mLCContDB.getInfo())
            {
                buildError("getInputData", "LCContDB没有得到足够的信息！");
                return false;
            }
            tLCContSchema = mLCContDB.getSchema();
            return true;
        }

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        String ContNo = tLCContSchema.getContNo();
        System.out.println("ContNo=" + ContNo);
        String GetNoticeNo = "";
        String sql = "";
        String sqlone = "";
        String sqltwo = "";
        String sql3 = "";
        String s1 = "";

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(ContNo);

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='" +
              ContNo + "') AND AddressNo = '" + tLCAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        tLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).
                           getSchema();

        sqlone = "select * from lccont where contno='" + ContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sqlone);
        tLCContSchema = tLCContSet.get(1).getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrtChildMJNotice.vts", "printer"); //最好紧接着就初始化xml文档
			  setFixedInfo();
        textTag.add("AppZipCode", tLCAddressSchema.getZipCode());
        textTag.add("AppAddress", tLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if(tLCAddressSchema.getPhone() != null&&!tLCAddressSchema.getPhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getPhone() + "、";
        }
        if (tLCAddressSchema.getHomePhone() != null&&!tLCAddressSchema.getHomePhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getHomePhone() + "、";
        }
        if(tLCAddressSchema.getCompanyPhone() != null&&!tLCAddressSchema.getCompanyPhone().equals("")){
            appntPhoneStr += tLCAddressSchema.getCompanyPhone() + "、";
        }
        if (tLCAddressSchema.getMobile() != null&&!tLCAddressSchema.getMobile().equals("")){
            appntPhoneStr += tLCAddressSchema.getMobile() + "、";
        }
        appntPhoneStr = appntPhoneStr.substring(0,appntPhoneStr.length()-1);
        textTag.add("Title", "满期终止批单");
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("AppntNo", tLCContSchema.getAppntNo());
        textTag.add("CustomerNo", tLCContSchema.getAppntNo());
        textTag.add("ContNo", ContNo);
        textTag.add("AppName", tLCContSchema.getAppntName());
        textTag.add("GetNoticeNo", tLJSPayBDB.getGetNoticeNo());
        textTag.add("CreatDate", tLJSPayBDB.getMakeDate());
        //应收日期
        FDate tD = new FDate();

        Date PayToDate = tD.getDate(getPayToDate());
        strPayToDate = tD.getString(PayToDate);

//        textTag.add("PayToDate", strPayToDate);
        textTag.add("PayToDate", CommonBL.decodeDate(PubFun.calDate(strPayToDate,-1,"D",null)));//转换格式，显示为24时
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("AgentPhone", agntPhone);
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
					+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
					+" (select agentgroup from laagent where agentcode ='"
      		+ tLaAgentDB.getAgentCode()+"'))"
					;
				LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());
        // 保单到期日
//        textTag.add("CinValidate", tLCContSchema.getCInValiDate());
        // modify by fuxin 长期险种和少儿险在一起的时候lccont中的CinValidate取长期险的，打印通知书有问题，现在取险种表的paytodate。
        String cCinValidate = new ExeSQL().getOneValue(" select distinct paytodate From lcpol where contno ='"+tLCContSchema.getContNo()+"' and riskcode ='320106'") ;
        textTag.add("CinValidate", CommonBL.decodeDate(PubFun.calDate(cCinValidate,-1,"D",null)));//转换格式，显示为24时
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLJSPayBDB.getGetNoticeNo(),"XB");
        tEdorItemSpecialData.query();
        textTag.add("BackDate",tEdorItemSpecialData.getEdorValue("BackDate"));

        textTag.add("BarCode1", tLJSPayBDB.getGetNoticeNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        // 满期险种信息


        xmlexport.addDisplayControl("displayMJ");

//        xmlexport.outputDocumentToFile("C:\\","indiDueFeeChildXB");
        mResult.clear();
        mResult.addElement(xmlexport);


        if(mOperate.equals("INSERT"))
        {
            //放入打印列表
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
            tLOPRTManagerDB.setCode("XB003");
            needPrt = tLOPRTManagerDB.query().size();
System.out.println("-------------------------the size is "+needPrt+"--------------------------");
            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
                mLOPRTManagerSchema.setOtherNoType("00");
                mLOPRTManagerSchema.setCode("XB003");
                mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo()); //这里存放 （也为交费收据号）
            }
        }


        return true;
    }

    private boolean setFeedBackInfo(XmlExport xmlexport)
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("FeedBack");

        String[] feedBaceInfo = new String[2];
        feedBaceInfo[0] = tLCContSchema.getContNo();
        feedBaceInfo[1] = "续保口         保单终止口";

        tlistTable.add(feedBaceInfo);
        String[] infoTitle = {"保单号", "续保选择（在相应方框打√）"};
        xmlexport.addListTable(tlistTable, infoTitle);
        return true;
    }

    /**
     * 得到险种未续期续保之前的交至日期
     * @return String
     */
    private String getPayToDate()
    {
        //当前应收记录的lastPayToDate即是未续期续保之前的险种交至日期
        String sql = "  select lastPayToDate "
                     + "from LJSPayPersonB "
                     + "where getNoticeNo = '" + tLJSPayBDB.getGetNoticeNo()
                     + "' ";
        ExeSQL e = new ExeSQL();
        String payToDate = e.getOneValue(sql);
        if(payToDate.equals("") || payToDate.equals("null"))
        {
            payToDate = tLCContSchema.getPaytoDate();
        }
        return payToDate;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[3];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("MJ");
        return tListTable;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
        sql.append("select riskcode,(select riskname from lmrisk where riskcode = a.riskcode),sum(sumactupaymoney)")
            .append(" from ljspayperson a where getnoticeno ='")
            .append(tLJSPayBDB.getGetNoticeNo())
            .append("' group by riskcode")
            ;

        String tSQL = sql.toString();
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public static void main(String[] a)
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000001965");
        tLJSPayBDB.getInfo();

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo(tLJSPayBDB.getOtherNo());

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tLCContSchema);
        tVData.addElement(tG);
        tVData.addElement(tLJSPayBDB);

        IndiDueFeeChildMJPrintBL bl = new IndiDueFeeChildMJPrintBL();
        if(bl.submitData(tVData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
