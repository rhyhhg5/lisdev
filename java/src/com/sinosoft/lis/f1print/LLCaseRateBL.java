package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLCaseRateBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDate = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageComName = "";
    private String mContType = "";
    private String mCaseRateNo = "";
    private String mCon = "";
    private String moperator = "";
    private String[][] mShowDataList = null;
    private XmlExport mXmlExport = null;
    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }


  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL = "select  b.comcode,b.name,a.riskcode  from ldcom B,lmriskapp A where B.sign='1' and  comgrade='02' "
             + "  AND  B.comcode like '" + mManageCom + "%' and A.riskprop='" +
             mContType + "' order by b.comcode,a.riskcode";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if (tSSRS.getMaxRow() <= 0) {
          buildError("queryData", "没有附和条件的机构和险种信息！");
          return false;
      }

      // 2、查询需要表示的数据
  //      String[][] tShowDataList = new String[tSSRS.getMaxRow()][4];
      String tManageCom ="";
      String tComName = "";
      for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环机构进行统计
          String Info[] = new String[4];
          tManageCom = tSSRS.GetText(i, 1);
          tComName = tSSRS.GetText(i, 2);
          String tRiskCode = tSSRS.GetText(i, 3);
          if ( i!=1 && !tManageCom.equals(tSSRS.GetText(i-1, 1)) ) {
              String sumInfo[] = new String[4];
              sumInfo[0] = tComName;
              sumInfo[1] = "合计";
              sumInfo[2] = getSumRate1(tManageCom); //机构所有险种合计发生率
              sumInfo[3] = getSumRate2(tManageCom); //机构所有合计赔付率
              mListTable.add(sumInfo);
          }
          Info[0] = tComName;
          Info[1] = tRiskCode;
          // 查询需要表示的数据
          Info[2] = getRate1(tManageCom, tRiskCode);
          Info[3] = getRate2(tManageCom, tRiskCode);
          mListTable.add(Info);
      }
      //最后一个分公司的合计
      String sumInfo[] = new String[4];
      sumInfo[0] = tComName;
      sumInfo[1] = "合计";
      sumInfo[2] = getSumRate1(tManageCom); //机构所有险种合计发生率
      sumInfo[3] = getSumRate2(tManageCom); //机构所有合计赔付率
      mListTable.add(sumInfo);
      tSQL = "select riskcode from  lmriskapp A where  A.riskprop='" + mContType +
             "' order by riskcode ";
      tSSRS = new SSRS();
      tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环 险种进行统计
          String tRiskCode = tSSRS.GetText(i, 1);
          String Info[] = new String[4];
          Info[0] = "合计";
          Info[1] = tRiskCode;
          Info[2] = getRiskSumRate1(tRiskCode);
          Info[3] = getRiskSumRate2(tRiskCode);
          mListTable.add(Info);
      }
      //合计所有公司所有险种
      String Info[] = new String[4];
      Info[0] = "合计";
      Info[1] = "合计";
      Info[2] = getAllSumRate1();
      Info[3] = getAllSumRate2();
      mListTable.add(Info);

      return true;
  }


  /**
   *  明细发生率
   */
  private String getRate1(String cManageCom,String cRiskCode)
  {
      String tRtValue = "0";
      String tSql = "SELECT COUNT(DISTINCT A1.CASENO) FROM LLCLAIMDETAIL A1 ,LLCASE B1 WHERE "
                   +" B1.CASENO=A1.CASENO   AND A1.RISKCODE='"+cRiskCode+"'  AND "
                  + " B1.MNGCOM like '"+cManageCom+"%' AND B1.ENDCASEDATE>='"+mStartDate+"' "
                  + " AND  B1.ENDCASEDATE<='"+mEndDate+"' AND B1.RGTTYPE='1'";
      ExeSQL tExeSQL = new ExeSQL();
      String tCount1=tExeSQL.getOneValue(tSql);
      tSql= "SELECT COUNT(A3.POLNO) FROM LCPOL A3 WHERE A3.APPFLAG='1' AND A3.RISKCODE='"+cRiskCode+"' "
           +" AND  A3.MANAGECOM like '"+cManageCom+"%' AND A3.ENDDATE>='"+mStartDate+"' AND  "
           +" A3.CVALIDATE<='"+mEndDate+"'";
     String tCount2=tExeSQL.getOneValue(tSql);
     double tValue=0;
     if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
     {
         return "0";
     }
     if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
     {
         tValue=0;
     }
     else
     {
         tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
     }
     DecimalFormat tDF = new DecimalFormat("0.##");
     String tRturn=tDF.format(tValue);
     return tRturn;
  }
  /**
    *  机构和计发生率
    */
   private String getSumRate1(String cManageCom)
   {
       String tRtValue = "0";
       String tSql = "SELECT COUNT(DISTINCT A1.CASENO) FROM LLCLAIMDETAIL A1 ,LLCASE B1 WHERE "
                  + " B1.CASENO=A1.CASENO    AND B1.MNGCOM like '"+cManageCom+"%' AND  "
                  + " B1.ENDCASEDATE >='"+mStartDate+"' AND B1.ENDCASEDATE<='"+mEndDate+"' AND B1.RGTTYPE='1' "
                 +" AND A1.riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"')";
       ExeSQL tExeSQL = new ExeSQL();
       String tCount1=tExeSQL.getOneValue(tSql);
       tSql= "SELECT COUNT(A3.POLNO) FROM LCPOL A3 WHERE A3.APPFLAG='1'  AND "
             +" A3.MANAGECOM like '"+cManageCom+"%' AND A3.ENDDATE>='"+mStartDate+"' and "
             + " A3.CVALIDATE<='"+mEndDate+"' "
             +" AND A3.riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"')";
      String tCount2=tExeSQL.getOneValue(tSql);
      double tValue=0;
      if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
      {
          return "0";
     }
      if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
      {
          tValue=0;
      }
      else
      {
          tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
      }
      DecimalFormat tDF = new DecimalFormat("0.##");
      String tRturn=tDF.format(tValue);
      return tRturn;
  }
  /**
   *  明细赔付率
   */
  private String getRate2(String cManageCom,String cRiskCode)
  {
      String tRtValue = "0";
      String tSql = "SELECT SUM(A2.REALPAY) FROM LLCLAIMDETAIL A2 ,LLCASE B2 WHERE B2.CASENO=A2.CASENO"
                 +" AND A2.RISKCODE='"+cRiskCode+"'  AND  B2.MNGCOM like '"+cManageCom+"%' AND "
                 +" B2.ENDCASEDATE>='"+mStartDate+"' AND B2.ENDCASEDATE<='"+mEndDate+"' AND B2.RGTTYPE='1'";
      ExeSQL tExeSQL = new ExeSQL();
      String tCount1=tExeSQL.getOneValue(tSql);
      tSql= "select sum(rate) from ldcaserate   where caserateno='"+mCaseRateNo+"' "
          + " and  ManageCom  like '"+cManageCom+"%'   and riskcode='"+cRiskCode+"'";
     String tCount2=tExeSQL.getOneValue(tSql);
     double tValue=0;
     if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
     {
         return "0";
     }
     if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
     {
         tValue=0;
     }
     else
     {
         tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
     }
     DecimalFormat tDF = new DecimalFormat("0.##");
     String tRturn=tDF.format(tValue);
     return tRturn;
  }
  /**
 * 机构赔付率和计
 */
private String getSumRate2(String cManageCom)
{
    String tRtValue = "0";
    String tSql = "SELECT SUM(A2.REALPAY) FROM LLCLAIMDETAIL A2 ,LLCASE B2 WHERE B2.CASENO=A2.CASENO"
               +" AND A2.riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"')  AND  B2.MNGCOM like '"+cManageCom+"%' AND "
               +" B2.ENDCASEDATE>='"+mStartDate+"' AND B2.ENDCASEDATE<='"+mEndDate+"' AND B2.RGTTYPE='1'";
    ExeSQL tExeSQL = new ExeSQL();
    String tCount1=tExeSQL.getOneValue(tSql);
    tSql= "select sum(rate) from ldcaserate   where caserateno='"+mCaseRateNo+"' "
        + " and  ManageCom  like '"+cManageCom+"%' and riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"')";
   String tCount2=tExeSQL.getOneValue(tSql);
   double tValue=0;
   if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
   {
       return "0";
     }
   if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
   {
       tValue=0;
   }
   else
   {
       tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
   }
   DecimalFormat tDF = new DecimalFormat("0.##");
   String tRturn=tDF.format(tValue);
   return tRturn;
}
/**
     * 险种的和计 发生率
 */

private String getRiskSumRate1(String cRiskCode)
{
        String tRtValue = "0";
        String tSql = "SELECT COUNT(DISTINCT A1.CASENO) FROM LLCLAIMDETAIL A1 ,LLCASE B1 WHERE "
                     +"  B1.CASENO=A1.CASENO   AND A1.RISKCODE='"+cRiskCode+"'  AND "
                     +"B1.ENDCASEDATE>='"+mStartDate+"' AND B1.ENDCASEDATE<='"+mEndDate+"' AND B1.RGTTYPE='1'  "
                    +" AND  B1.MNGCOM like '"+mManageCom+"%'";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1=tExeSQL.getOneValue(tSql);
        tSql= "SELECT COUNT(A3.POLNO) FROM LCPOL A3 WHERE A3.APPFLAG='1' AND A3.RISKCODE='"+cRiskCode+"' "
              +" AND A3.ENDDATE>='"+mStartDate+"' AND A3.CVALIDATE<='"+mEndDate+"' AND  ManageCom like '"+mManageCom+"%'";
       String tCount2=tExeSQL.getOneValue(tSql);
       double tValue=0;
       if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
       {
           return "0";
     }
       if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
       {
           tValue=0;
       }
       else
       {
           tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
       }
       DecimalFormat tDF = new DecimalFormat("0.##");
       String tRturn=tDF.format(tValue);
       return tRturn;
  }
    /**
   *  险种的和计 赔付率
   */
  private String getRiskSumRate2(String cRiskCode)
  {
      String tRtValue = "0";
      String tSql = "SELECT SUM(A2.REALPAY) FROM LLCLAIMDETAIL A2 ,LLCASE B2 WHERE B2.CASENO=A2.CASENO"
                 +" AND A2.RISKCODE='"+cRiskCode+"' AND "
                 +"B2.ENDCASEDATE>='"+mStartDate+"' AND B2.ENDCASEDATE<='"+mEndDate+"' AND "
                 +" B2.RGTTYPE='1'   AND B2.MNGCOM like '"+mManageCom+"%'";
      ExeSQL tExeSQL = new ExeSQL();
      String tCount1=tExeSQL.getOneValue(tSql);
      tSql= "select sum(rate) from ldcaserate    where caserateno='"+mCaseRateNo+"' "
            +" and riskcode='"+cRiskCode+"'    AND  ManageCom like '"+mManageCom+"%' ";
     String tCount2=tExeSQL.getOneValue(tSql);
     double tValue=0;
     if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
     {
         return "0";
     }
     if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
     {
         tValue=0;
     }
     else
     {
         tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2) ;
     }
     DecimalFormat tDF = new DecimalFormat("0.##");
     String tRturn=tDF.format(tValue);
     return tRturn;
  }

  /**
       * 险种的和计 发生率
   */

  private String getAllSumRate1()
  {
          String tRtValue = "0";
          String tSql = " SELECT COUNT(DISTINCT A1.CASENO) FROM LLCLAIMDETAIL A1 ,LLCASE B1 WHERE "
                        +" B1.CASENO=A1.CASENO    AND "
                        +" B1.ENDCASEDATE>='"+mStartDate+"' AND B1.ENDCASEDATE<='"+mEndDate+"' AND "
                        +" B1.RGTTYPE='1' AND A1.riskcode IN "
                        +" (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"') AND "
                        +" B1.MNGCOM like '"+mManageCom+"%'";
          ExeSQL tExeSQL = new ExeSQL();
          String tCount1=tExeSQL.getOneValue(tSql);
          tSql= "SELECT COUNT(A3.POLNO) FROM LCPOL A3 WHERE A3.APPFLAG='1'  AND "
                +" A3.ENDDATE>='"+mStartDate+"' AND A3.CVALIDATE<='"+mEndDate+"' AND A3.riskcode IN "
                +" (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"') AND  ManageCom "
                +" like '"+mManageCom+"%' ";
         String tCount2=tExeSQL.getOneValue(tSql);
         double tValue=0;
         if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
         {
             return "0";
         }
         if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
         {
             tValue=0;
         }
         else
         {
            tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2);
         }
         DecimalFormat tDF = new DecimalFormat("0.##");
         String tRturn=tDF.format(tValue);
         return tRturn;
    }
      /**
     *  险种的和计 赔付率
     */
    private String getAllSumRate2()
    {
        String tRtValue = "0";
        String tSql = " SELECT SUM(A2.REALPAY) FROM LLCLAIMDETAIL A2 ,LLCASE B2 WHERE "
                      +" B2.CASENO=A2.CASENO AND    "
                      +" B2.ENDCASEDATE>='"+mStartDate+"' AND  B2.ENDCASEDATE<='"+mEndDate+"' "
                      +" AND B2.RGTTYPE='1' AND A2.riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"') AND  B2.MNGCOM like '"+mManageCom+"%'";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1=tExeSQL.getOneValue(tSql);
        tSql= " select sum(rate) from ldcaserate   WHERE  caserateno='"+mCaseRateNo+"' and "
            +" riskcode IN (SELECT RISKCODE FROM LMRISKAPP WHERE riskprop='"+mContType+"') AND  ManageCom like '"+mManageCom+"%' ";
       String tCount2=tExeSQL.getOneValue(tSql);
       double tValue=0;
       if(tCount1==null ||tCount1.equals("") ||tCount1.equals("0"))
       {
           return "0";
     }
       if(tCount2==null ||tCount2.equals("") ||tCount2.equals("0"))
       {
           tValue=0;
       }
       else
       {
           tValue=Arith.div(Double.parseDouble(tCount1)*100,Double.parseDouble(tCount2),2);
          // tValue=Arith.round(Double.parseDouble(tCount1)/Double.parseDouble(tCount2),2) ;
       }
       DecimalFormat tDF = new DecimalFormat("0.##");
       String tRturn=tDF.format(tValue);
       return tRturn;
    }


  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      ExeSQL tExeSQL = new ExeSQL();
      //执行存储过程
      if (!tExeSQL.callProcedure("call getcase1('" + mStartDate + "','" +
                                  mEndDate + "','" + mManageCom + "','" +
                                  currentDate + "','" + mCaseRateNo + "')")) {
           return false;
      }

      TextTag tTextTag = new TextTag();
      mXmlExport = new XmlExport();
      //设置模版名称
      mXmlExport.createDocument("LLRiskCaseRateReport1.vts", "printer");
      System.out.print("dayin252");
      tTextTag.add("ManageComName", mManageComName);
      tTextTag.add("StartDate", mStartDate);
      tTextTag.add("EndDate", mEndDate);
      tTextTag.add("Con", mCon);
      tTextTag.add("Operator", moperator);
      tTextTag.add("MakeDate", currentDate);
      if (tTextTag.size() < 1) {
          return false;
      }

      mXmlExport.addTextTag(tTextTag);

      String[] title = {"", "", "", ""};

      if (!getDataList()) {
          return false;
      }
      mListTable.setName("Order");
      System.out.println("111");
      mXmlExport.addListTable(mListTable, title);
      System.out.println("121");
      mXmlExport.outputDocumentToFile("c:\\", "new1");
      this.mResult.clear();

      mResult.addElement(mXmlExport);

      return true;
  }
  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
      mStartDate = (String) pmInputData.get(0);
      mEndDate = (String) pmInputData.get(1);
      mManageCom=(String) pmInputData.get(2);
      moperator=(String) pmInputData.get(3);
      mManageComName=(String) pmInputData.get(4);
      mContType=(String) pmInputData.get(5);
      mCon=(String) pmInputData.get(6);
      mCaseRateNo=(String) pmInputData.get(7);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LLCaseRateBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }


  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
}
