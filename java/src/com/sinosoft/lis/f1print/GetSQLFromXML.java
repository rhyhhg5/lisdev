package com.sinosoft.lis.f1print;

import java.io.*;
import org.jdom.*;
import org.jdom.input.*;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LMCalFactorSchema;
import com.sinosoft.utility.StrTool;

import java.util.*;

public class GetSQLFromXML
{
    private  Hashtable mHashtable=new Hashtable();
    public  void setParameters(String name, String value){
        if(name!=null && value!=null)
        mHashtable.put(name.trim().toLowerCase(), value.trim().toLowerCase());
    }
    public  String getSql(String path, String nodeName)
    {
        String result="";
        try
        {
            SAXBuilder sb = new SAXBuilder();
            Document doc = sb.build(new FileInputStream(path));
            Element tElement = doc.getRootElement();
            List tList = tElement.getChildren(nodeName);
            Iterator tIterator = tList.iterator();
            if (tIterator.hasNext())
            {
                Element subElement = (Element) tIterator.next();
                result= subElement.getText();
                result=result.replace('#', '>');
                result=result.replace('@', '<'); 
                result=result.replace('$', '/');
                while (true)
                {
                    String paremeter=PubFun.getStr(result, 2, "?");
                    if (paremeter.equals(""))
                    {
                        break;
                    }
                    String paremeterValue=(String)mHashtable.get(paremeter.trim().toLowerCase());
                    if(paremeterValue!=null){
                        result=StrTool.replaceEx(result, "?"+paremeter+"?", paremeterValue);
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }
    public static void main(String[] args)
    {
        GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", "116");
        tGetSQLFromXML.setParameters("BeginDate", "2007-1-8");
        tGetSQLFromXML.setParameters("EndDate", "2007-1-9");
        String result = tGetSQLFromXML.getSql(
                "D:/V1.1/ui/f1print/picctemplate/FeePrintSql.xml", "TempFee");
        System.out.println(result);
        
    }
}
