package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class LCSuccorUI {
    public static String EEEEE = "1";
    private LCContSchema mLCContSchema;
    public CErrors mErrors = new CErrors();
    public LCSuccorUI() {
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args) {
//        Propertie p = new Propertie();
        System.setProperty("OS", "XP");
        System.out.println("System "+System.getProperty("application"));
        GlobalInput tG = new GlobalInput();
        tG.Operator = "pc";
        tG.ComCode = "86";
        LCSuccorUI tZhuF1PUI = new LCSuccorUI();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("13000086087"); //合同单号
        tLCContSchema.setCardFlag("2"); //合同单号
        //        tLCContSchema.setGrpContNo("240110000000024"); //合同单号
        VData vData = new VData();
        vData.addElement(tG);
        vData.addElement(tLCContSchema);
        String szTemplatePath = "Y:/ui/f1print/template/";
        String sOutXmlPath = "Y:/ui/";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TemplatePath", szTemplatePath);
        tTransferData.setNameAndValue("OutXmlPath", sOutXmlPath);
        vData.add(tTransferData);
        if (true) {
            String a = "&fjfi";
        } else {
            String a;
        }
        vData.addElement("Y:/ui/f1print/template/");
        vData.addElement("Y:/ui/");
        if (!tZhuF1PUI.submitData(vData, "PRINT")) {
            System.out.println(tZhuF1PUI.mErrors.getFirstError());
            System.out.println("fail to get print data");
        }
    }

    /**
     * submitData
     *
     * @param vData VData
     * @param string String
     * @return boolean
     */
    public boolean submitData(VData vData, String string) {

        if (!getInputData(vData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData(vData, string)) {
            return false;
        }

        return true;
    }

    /**
     * dealData
     *
     * @param vData VData
     * @return boolean
     */
    private boolean dealData(VData vData, String string) {
        /** 境外救援的保单打印 */
        System.out.println("************");
        System.out.println(this.mLCContSchema.getCardFlag());
        System.out.println("************");
        if (this.mLCContSchema.getCardFlag().equals("1")) {
            LCSuccorBL tLCSuccorBL = new LCSuccorBL();
            if (!tLCSuccorBL.submitData(vData, string)) {
                this.mErrors.copyAllErrors(tLCSuccorBL.mErrors);
                return false;
            }
        }
        /** 交通意外保单打印 */
        if (this.mLCContSchema.getCardFlag().equals("2") ||
            this.mLCContSchema.getCardFlag().equals("5")) {
            LCContPrintPdfBL tLCContPringPdfBL = new LCContPrintPdfBL();
            if (!tLCContPringPdfBL.submitData(vData, string)) {
                this.mErrors.copyAllErrors(tLCContPringPdfBL.mErrors);
                return false;
            }
        }
        if (this.mLCContSchema.getCardFlag().equals("6")) {
            BriefContPdfBL tBriefContPdfBL = new BriefContPdfBL();
            if (!tBriefContPdfBL.submitData(vData, string)) {
                this.mErrors.copyAllErrors(tBriefContPdfBL.mErrors);
                return false;
            }
        }
        else if("1".equals(mLCContSchema.getIntlFlag()))
        {
            LCContPrintIntlPdfBL bl = new LCContPrintIntlPdfBL();
            if (!bl.submitData(vData, string)) {
                this.mErrors.copyAllErrors(bl.mErrors);
                return false;
            }
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mLCContSchema == null) {
            buildError("checkData", "传入合同信息为null！");
            return false;
        }
        if (this.mLCContSchema.getContNo() == null ||
            this.mLCContSchema.getContNo().equals("")) {
            buildError("checkData", "传入合同号码为null！");
            return false;
        }
        if (this.mLCContSchema.getCardFlag() == null ||
            this.mLCContSchema.getCardFlag().equals("")) {
            buildError("checkData", "传入保单类型为null！");
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData(VData vData) {
        mLCContSchema = (LCContSchema) vData.getObjectByObjectName(
                "LCContSchema", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCSuccorUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
