package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class AskTrackPrintBL implements PrintService {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  //取得的保单号码
  private String mContNo="";
  //String InsuredName="";
  //输入的查询sql语句
  private String msql="";
  //取得的延期承保原因
  private String mUWError="";
  //取得的代理人编码
  private String mAgentCode="";

  //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput();
  private LOPRTManagerSchema mLOPRTManagerSchema=new LOPRTManagerSchema();
  private LCGrpContSchema mLCGrpContSchema=new LCGrpContSchema();
  private LCGrpContSet mLCGrpContSet=new LCGrpContSet();
  private LCGrpPolSet mLCGrpPolSet=new LCGrpPolSet();
  private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
  private LCGCUWMasterSchema mLCGCUWMasterSchema = new LCGCUWMasterSchema();
  public AskTrackPrintBL() {
  }

  /**
传输数据的公共方法
*/
  public boolean submitData(VData cInputData, String cOperate)
  {
    if( !cOperate.equals("PRINT") ) {
      buildError("submitData", "不支持的操作字符串");
      return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if( !getInputData(cInputData) ) {
      return false;
    }

    mResult.clear();

    // 准备所有要打印的数据
    if( !getPrintData() ) {
      return false;
    }

    return true;
  }

  public static void main(String[] args)
  {
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
    mLOPRTManagerSchema.setSchema((LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema",0));
    if( mLOPRTManagerSchema==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    if( mLOPRTManagerSchema.getPrtSeq()==null ) {
      buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData()
  {

    //根据印刷号查询打印队列中的纪录
    String PrtNo=mLOPRTManagerSchema.getPrtSeq();//打印流水号

    System.out.println("PrtNo = "+PrtNo);
    LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
    if(tLOPRTManagerDB.getInfo()==false)
    {
      mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
      buildError("outputXML", "在取得打印队列中数据时发生错误");
      return false;
    }
    mLOPRTManagerSchema=tLOPRTManagerDB.getSchema();

    boolean PEFlag = false; //打印体检件部分的判断标志
    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
    int m,i;
    if( !tLCGrpContDB.getInfo() ) {
      mErrors.copyAllErrors(tLCGrpContDB.mErrors);
      buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
      return false;
    }
    mLCGrpContSchema=tLCGrpContDB.getSchema() ;

    mAgentCode=mLCGrpContSchema.getAgentCode();
    LAAgentDB tLAAgentDB = new LAAgentDB();
    tLAAgentDB.setAgentCode(mAgentCode);
    if (!tLAAgentDB.getInfo())
    {
      mErrors.copyAllErrors(tLAAgentDB.mErrors);
      buildError("outputXML", "在取得LAAgent的数据时发生错误");
      return false;
    }
    mLAAgentSchema=tLAAgentDB.getSchema();//保存代理人信息
//核保结论
   LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
   tLCGCUWMasterDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo());
   if(!tLCGCUWMasterDB.getInfo())
   {
     mErrors.copyAllErrors(tLCGCUWMasterDB.mErrors);
     buildError("outputXML", "在取得核保结论的数据时发生错误");
     return false;
   }
   mLCGCUWMasterSchema = tLCGCUWMasterDB.getSchema();//保存核保结论信息

    //险种信息



//其它模版上单独不成块的信息
    TextTag texttag=new TextTag();//新建一个TextTag的实例
    XmlExport xmlexport=new XmlExport();//新建一个XmlExport的实例
    xmlexport.createDocument("GrpAskAppFollow.vts","printer");//最好紧接着就初始化xml文档
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" + tSrtTool.getDay() + "日";
    //模版自上而下的元素
    texttag.add("BarCode1","1234234");
    texttag.add("BarCodeParam1","BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
    texttag.add("LCCont.AppntName",mLCGrpContSchema.getGrpName()); //投保人名称
    texttag.add("LCGrpCont.AppntNo",mLCGrpContSchema.getAppntNo()); //团体保障号
    texttag.add("LCGrpCont.ContNo",mLCGrpContSchema.getGrpContNo()); //投保单号
    texttag.add("LCGrpCont.AskGrpContNo",mLCGrpContSchema.getAskGrpContNo()); //询价号

    texttag.add("LAAgent.Name",mLAAgentSchema.getName());       //代理人姓名
    texttag.add("LCGrpCont.AgentCode",mLCGrpContSchema.getAgentCode()); //代理人业务号
    texttag.add("ManageCom",getComName(mLCGrpContSchema.getManageCom())); //营业机构
    texttag.add("ManageCode",mLCGrpContSchema.getManageCom());//营业机构代码
    texttag.add("PrtNo",PrtNo); //刘水号
    texttag.add("LCGrpCont.PrtNo",mLCGrpContSchema.getPrtNo()); //印刷号
    texttag.add("SysDate",SysDate);




    if (texttag.size()>0)
    {
      xmlexport.addTextTag(texttag);
    }
   //保存补充资料信息




    xmlexport.outputDocumentToFile("e:\\","AskTrack");//输出xml文档到文件
    mResult.clear();
    mResult.addElement(xmlexport);

    return true;
  }

  /**
   * 得到通过机构代码得到机构名称
   * @param strComCode
   * @return
   * @throws Exception
   */
  private String getComName(String strComCode)
  {
    LDCodeDB tLDCodeDB = new LDCodeDB();

    tLDCodeDB.setCode(strComCode);
    tLDCodeDB.setCodeType("station");

    if (!tLDCodeDB.getInfo()) {
      mErrors.copyAllErrors(tLDCodeDB.mErrors);
      return "";
    }
    return tLDCodeDB.getCodeName();
  }
}

