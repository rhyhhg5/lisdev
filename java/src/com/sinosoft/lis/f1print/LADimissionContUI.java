package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LADimissionContBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LADimissionContUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LADimissionContUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LADimissionContBL tLADimissionContBL = new LADimissionContBL();

        if (!tLADimissionContBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLADimissionContBL.mErrors);

            return false;
        } else {
            this.mResult = tLADimissionContBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
