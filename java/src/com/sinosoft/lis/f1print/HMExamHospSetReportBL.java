/**
 * <p>ClassName: HMExamHospSetReportBL.java </p>
 * <p>Description: 核保体检医院设置情况统计 </p>
 * <p>Copyright: Copyright (c) 2010 </p>
 * <p>Company: </p>
 * @author chenxw
 * @version 1.0
 * @CreateDate：2010-4-1
 */

package com.sinosoft.lis.f1print;

import java.lang.reflect.Array;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class HMExamHospSetReportBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData = new TransferData();

	/** 统计开始日期 */
	private String mStartDate = "";

	/** 统计结束日期 */
	private String mEndDate = "";

	/** 统计机构编码 */
	private String mManageComCode = "";

	/** 统计机构名称 */
	private String mManageComName = "";
	
	/** 数据操作字符串 */
	private String mOperate;


	public HMExamHospSetReportBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start CorreResuReportBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(this.mErrors);
			return false;
		}

		mInputData = null;
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {

		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			CError cError = new CError();
			cError.moduleName = "PrintOrderReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mGlobalInput 为空值";
			mErrors.addOneError(cError);
			return false;
		}
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			CError cError = new CError();
			cError.moduleName = "PrintOrderReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mTransferData 为空值";
			mErrors.addOneError(cError);
			return false;
		}
		/** 统计开始日期 */
		mStartDate = (String) mTransferData.getValueByName("StartDate");
		/** 统计结束日期 */
		mEndDate = (String) mTransferData.getValueByName("EndDate");
		/** 统计机构编码 */
		mManageComCode = (String) mTransferData.getValueByName("ManageCom");
		/** 统计机构名称 */
		mManageComName = (String) mTransferData.getValueByName("ComName");
		
		return true;
	}

	private boolean checkData() {
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		//报表打印
		if (!PrintData()) {
			return false;
		}
		return true;
	}

	/**
	 * 报表打印主程序
	 * 
	 * @return
	 */
	private boolean PrintData() {
		
		
		XmlExport xmlExport = new XmlExport(); // 新建一个XmlExport的实例
		xmlExport.createDocument("HMExamHospSetReport.vts", ""); // 最好紧接着就初始化xml文档
		ListTable tlistTable = new ListTable();
		String strArr[] = null;
		
		tlistTable.setName("HMReport");
		//
		String tStartDateCondition = "";
		if(mStartDate != null && !mStartDate.equals("")) {
			tStartDateCondition = " and X.ModifyDate >='" + mStartDate + "'";
		}
		String tEndDateCondition = "";
		if(mEndDate != null && !mEndDate.equals("")) {
			tEndDateCondition = " and X.ModifyDate <='" + mEndDate + "'";
		}
		
		String tManageComCondition = "";
		if(mManageComCode != null && !"".equals(mManageComCode)) {
			
			if(mManageComCode.equals("86")) {
				tManageComCondition = " and lm.ComCode like '" + mManageComCode + "%' and lm.Comgrade='02' ";
			} else if(mManageComCode.length() == 4) {
				tManageComCondition = " and lm.ComCode like '" + mManageComCode + "%' and lm.Comgrade='03'";
			} else {
				tManageComCondition = " and lm.ComCode like '" + mManageComCode + "%' ";
			}
		}
		
		String sql = " select XX.ComCode, XX.Name,sum(XX.sj),sum(XX.fsj),sum(XX.ej),sum(XX.fej),"
			  +" sum(XX.yj),sum(XX.fyj),sum(XX.wfj),sum(XX.total)"
			  +" from (select lm.ComCode ComCode,lm.Name Name, "
			  +" (select count(1)from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '31' "
			  +" and aa.hospitcode = X.hospitcode) as sj, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '30' "
			  +" and aa.hospitcode = X.hospitcode) as fsj, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '21' "
			  +" and aa.hospitcode = X.hospitcode) as ej, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '20' "
			  +" and aa.hospitcode = X.hospitcode) as fej, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '11' "
			  +" and aa.hospitcode = X.hospitcode) as yj, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '10' "
			  +" and aa.hospitcode = X.hospitcode) as fyj, "
			  +" (select count(1) from ldhospital aa "
			  +" where 1 = 1 "
			  +" and aa.LevelCode = '00' "
			  +" and aa.hospitcode = X.hospitcode) as wfj ,"
			  +"(select count(1) from ldhospital aa where 1 = 1 and aa.hospitcode = X.hospitcode) as total"			        
			  +" from ldcom lm,ldhospital X "
			  +" where 1=1 and lm.comcode=X.managecom" 
			  + " and lm.Sign='1' "
			  + tManageComCondition
			  + tStartDateCondition
			  + tEndDateCondition
			  + ") as XX "
			  +" group by XX.ComCode, XX.Name order by XX.ComCode desc with ur";
		 
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		int sjCount = 0; //三甲
		int fsjCount = 0; //非三甲
		int ejCount = 0; //二甲
		int fejCount = 0; //非二甲
		int yjCount = 0; //一甲
		int fyjCount = 0; //非一甲
		int wfjCount = 0; //未分级
		int count = 0; //总计
		
		int NO = 0; //序号
		try {
			
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				NO = NO + 1;
				strArr = new String[10];
				strArr[0] = String.valueOf(NO);
				strArr[1] = tSSRS.GetText(i, 2);
				strArr[2] = tSSRS.GetText(i, 3);//三甲
				strArr[3] = tSSRS.GetText(i, 4);//
				strArr[4] = tSSRS.GetText(i, 5);//
				strArr[5] = tSSRS.GetText(i, 6);//
				strArr[6] = tSSRS.GetText(i, 7);//
				strArr[7] = tSSRS.GetText(i, 8);//
				strArr[8] = tSSRS.GetText(i, 9);//
				strArr[9] = tSSRS.GetText(i, 10);//
				
				sjCount += Integer.parseInt(strArr[2]);
				fsjCount += Integer.parseInt(strArr[3]);
				ejCount += Integer.parseInt(strArr[4]);
				fejCount += Integer.parseInt(strArr[5]);
				yjCount += Integer.parseInt(strArr[6]);
				fyjCount += Integer.parseInt(strArr[7]);
				wfjCount += Integer.parseInt(strArr[8]);
				count += Integer.parseInt(strArr[9]);
				
				tlistTable.add(strArr);
			}
		} catch(Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "PrintOrderReportBL";
			cError.functionName = "printData";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
		
		strArr = new String[10];
		
		TextTag texttag = new TextTag();
		if(mStartDate != null && !mStartDate.equals("")) {
			mStartDate = mStartDate.replaceFirst("-","年").replaceFirst("-", "月") + "日";
		}
		if(mEndDate != null && !mEndDate.equals("")) {
			mEndDate = mEndDate.replaceFirst("-","年").replaceFirst("-", "月") + "日";
		}
		
		mManageComName = new ExeSQL().getOneValue("select Name from LDCom where ComCode='" + mManageComCode + "'");
		
		texttag.add("StartDate", mStartDate);// 开始日期
		texttag.add("EndDate", mEndDate);// 结束日期
		texttag.add("ManageComName", mManageComName);// 统计机构
		texttag.add("Operator",mGlobalInput.Operator);
		texttag.add("MakeDate",PubFun.getCurrentDate());
		
		texttag.add("SJCount",sjCount);
		texttag.add("FSJCount",fsjCount);
		texttag.add("EJCount",ejCount);
		texttag.add("FEJCount",fejCount);
		texttag.add("YJCount",yjCount);
		texttag.add("FYJCount",fyjCount);
		texttag.add("WFJCount",wfjCount);
		texttag.add("Count",count);
		
		//System.out.println("**********mManageComName= " + mManageComName);
		if (texttag.size() > 0) {
			xmlExport.addTextTag(texttag);
		}
		xmlExport.addListTable(tlistTable, strArr);
		xmlExport.outputDocumentToFile("e:\\", "test");
		mResult.clear();
		mResult.addElement(xmlExport);
		mResult.addElement(mTransferData);
		return true;
	}
	
	private boolean prepareOutputData() {
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}
	
	public static void main(String[] args) {

	}
}
