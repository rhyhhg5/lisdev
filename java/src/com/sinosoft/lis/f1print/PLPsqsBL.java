package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :理赔申请书的打印
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCasePolicyDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PLPsqsBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mRgtNo;
    private String mAccidentType;
    private String mIDNo;
    private String mIDType;
    private String mCustomerSex;
    private float mCustomerAge;
    private String mAccidentReason;
    private String mRemark;

    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PLPsqsBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("getInputData begin");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema = (LLRegisterSchema) cInputData.getObjectByObjectName(
                "LLRegisterSchema", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mRgtNo = tLLRegisterSchema.getRgtNo();
        System.out.println("该次案件的立案号码是＝＝＝＝" + mRgtNo);

        if (mRgtNo == null)
        {
            buildError("getInputData", "立案号码是空，没有本次立案！");
            return false;
        }
        System.out.println("getInputData end");
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PayNoticeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean checkData()
    {
        System.out.println("checkData begin");
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo())
        {
            CError cError = new CError();
            cError.moduleName = "PayNoticeF1PUI";
            cError.functionName = "checkData";
            cError.errorMessage = "立案信息查询失败";
            this.mErrors.addOneError(cError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        //mLLRegisterSchema 中存放的是tLLRegisterDB中查询出的数据
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseDB.setRgtNo(mRgtNo);
        mLLCaseSet.set(tLLCaseDB.query());
        tLLCaseSchema.setSchema(mLLCaseSet.get(1));
        mAccidentType = tLLCaseSchema.getAccidentType();
        mIDNo = tLLCaseSchema.getIDNo();
        mIDType = tLLCaseSchema.getIDType();
        mCustomerSex = tLLCaseSchema.getCustomerSex();
//    mCustomerAge = tLLCaseSchema.getCustomerAge();

        System.out.println("该客户的事故类型是====" + mAccidentType);
        System.out.println("checkData end");
        return true;
    }

    private boolean getPrintData()
    {
        System.out.println("getPrintData begin");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");
        String strLKType = "未确定";
        String strAccidentCourse = "";
        String strRemark = "";
        String strPolNo1 = "";
        String strPolNo2 = "";
        String strPolNo3 = "";
        String strPolNo4 = "";
        String strApplyType = "";
        String strIDType = "";
        //申请人的证件类型

        LLRegisterDB yLLRegisterDB = new LLRegisterDB();
        yLLRegisterDB.setRgtNo(mRgtNo);
        System.out.println("测试数据＝＝＝＝＝＝＝＝＝");
        if (yLLRegisterDB.getInfo())
        {
            //申请人的证件类型
            strIDType = "无证件";
            if (yLLRegisterDB.getIDType() == null ||
                yLLRegisterDB.getIDType().length() == 0)
            {
                strIDType = "无证件";
            }
            else
            {
                if (yLLRegisterDB.getIDType().equals("0"))
                {
                    strIDType = "身份证";
                }
                if (yLLRegisterDB.getIDType().equals("1"))
                {
                    strIDType = "护照";
                }
                if (yLLRegisterDB.getIDType().equals("2"))
                {
                    strIDType = "军官证";
                }
                if (yLLRegisterDB.getIDType().equals("3"))
                {
                    strIDType = "驾照";
                }
                if (yLLRegisterDB.getIDType().equals("4"))
                {
                    strIDType = "户口本";
                }
                if (yLLRegisterDB.getIDType().equals("5"))
                {
                    strIDType = "学生证";
                }
                if (yLLRegisterDB.getIDType().equals("6"))
                {
                    strIDType = "工作证";
                }
                //转换AccidentReason,需要调用函数
                mAccidentReason = CaseFunPub.getStr(yLLRegisterDB.
                        getAccidentReason().trim());
            }

            System.out.println("申请人的证件类型是=======" + strIDType);

            //申请人的身份处理
            strApplyType = "其他";
//      if(yLLRegisterDB.getApplyType()==null||yLLRegisterDB.getApplyType().length()==0)
//        strApplyType = "其他";
//      else
//      {
//        if(yLLRegisterDB.getApplyType().equals("0"))
//          strApplyType = "被保险人";
//        if(yLLRegisterDB.getApplyType().equals("1"))
//          strApplyType = "事故受益人/继承人";
//        if(yLLRegisterDB.getApplyType().equals("2"))
//          strApplyType = "监护人";
//      }
            System.out.println("申请人的身份是========" + strApplyType);

            //处理出险原因(事故者现状)
            if (yLLRegisterDB.getAccidentCourse() == null ||
                yLLRegisterDB.getAccidentCourse().length() == 0)
            {
                strAccidentCourse = "无";
            }
            else
            {
                strAccidentCourse = CaseFunPub.getStr(yLLRegisterDB.
                        getAccidentCourse());
            }
            //处理备注
            if (yLLRegisterDB.getRemark() == null ||
                yLLRegisterDB.getRemark().length() == 0)
            {
                strRemark = "无";
            }
            else
            {
                strRemark = CaseFunPub.getStr(yLLRegisterDB.getRemark());
            }

            //处理领款方式

            if (!(yLLRegisterDB.getCaseGetMode() == null ||
                  yLLRegisterDB.getCaseGetMode().equals("")))
            {
                if (yLLRegisterDB.getCaseGetMode().trim().equals("1"))
                {
                    strLKType = "现金";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("2"))
                {
                    strLKType = "现金支票";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("3"))
                {
                    strLKType = "转账支票";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("4"))
                {
                    strLKType = "银行转账";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("5"))
                {
                    strLKType = "内部转账";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("6"))
                {
                    strLKType = "银行托收";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("7"))
                {
                    strLKType = "银行电汇";
                }
                if (yLLRegisterDB.getCaseGetMode().trim().equals("8"))
                {
                    strLKType = "其他";
                }
            }
        }
        //对索赔保单的查询LLCasePolicy表的操作
        LLCasePolicyDB yLLCasePolicyDB = new LLCasePolicyDB();
        yLLCasePolicyDB.setRgtNo(mRgtNo);
        LLCasePolicySet yLLCasePolicySet = new LLCasePolicySet();
        yLLCasePolicySet.set(yLLCasePolicyDB.query());
        int y = yLLCasePolicySet.size();
        if (y == 1)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo();
        }
        if (y == 2)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo();
        }
        if (y == 3)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
        }
        if (y == 4)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo();
        }
        if (y == 5)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo();
        }
        if (y == 6)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
        }
        if (y == 7)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo();
        }
        if (y == 8)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo() + "，" +
                        yLLCasePolicySet.get(8);
        }
        if (y == 9)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo() + "，" +
                        yLLCasePolicySet.get(8).getPolNo() + "，" +
                        yLLCasePolicySet.get(9).getPolNo();
        }
        if (y == 10)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo() + "，" +
                        yLLCasePolicySet.get(8).getPolNo() + "，" +
                        yLLCasePolicySet.get(9).getPolNo();
            strPolNo4 = yLLCasePolicySet.get(10).getPolNo();
        }
        if (y == 11)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo() + "，" +
                        yLLCasePolicySet.get(8).getPolNo() + "，" +
                        yLLCasePolicySet.get(9).getPolNo();
            strPolNo4 = yLLCasePolicySet.get(10).getPolNo() + "，" +
                        yLLCasePolicySet.get(11).getPolNo();
        }
        if (y == 12)
        {
            strPolNo1 = yLLCasePolicySet.get(1).getPolNo() + "，" +
                        yLLCasePolicySet.get(2).getPolNo() + "，" +
                        yLLCasePolicySet.get(3).getPolNo();
            strPolNo2 = yLLCasePolicySet.get(4).getPolNo() + "，" +
                        yLLCasePolicySet.get(5).getPolNo() + "，" +
                        yLLCasePolicySet.get(6).getPolNo();
            strPolNo3 = yLLCasePolicySet.get(7).getPolNo() + "，" +
                        yLLCasePolicySet.get(8).getPolNo() + "，" +
                        yLLCasePolicySet.get(9).getPolNo();
            strPolNo4 = yLLCasePolicySet.get(10).getPolNo() + "，" +
                        yLLCasePolicySet.get(11).getPolNo() + "，" +
                        yLLCasePolicySet.get(12).getPolNo();
        }

        LLCaseDB yLLCaseDB = new LLCaseDB();
        LLCaseSchema yLLCaseSchema = new LLCaseSchema();
        LLCaseSet yLLCaseSet = new LLCaseSet();
        yLLCaseDB.setRgtNo(mRgtNo);
        yLLCaseSet.set(yLLCaseDB.query());
        yLLCaseSchema.setSchema(mLLCaseSet.get(1));
        mAccidentType = yLLCaseSchema.getAccidentType();
        String str2CustomerName = yLLCaseSchema.getCustomerName();
        String str2AccidentType = "";
        if (yLLCaseSchema.getAccidentType() == null ||
            yLLCaseSchema.getAccidentType().length() == 0)
        {
            str2AccidentType = "其他";
        }
        else
        {
            if (yLLCaseSchema.getAccidentType().equals("0"))
            {
                str2AccidentType = "死亡";
            }
            if (yLLCaseSchema.getAccidentType().equals("1"))
            {
                str2AccidentType = "其他";
            }
        }
        System.out.println("现状是＝＝＝＝＝＝＝＝" + str2AccidentType);
        String str2IDNo = yLLCaseSchema.getIDNo();

        String str2CustomerSex = "不详";
        if (yLLCaseSchema.getCustomerSex() == null ||
            yLLCaseSchema.getCustomerSex().length() == 0)
        {
            str2CustomerSex = "不详";
        }
        else
        {
            if (yLLCaseSchema.getCustomerSex().equals("0"))
            {
                str2CustomerSex = "男";
            }
            if (yLLCaseSchema.getCustomerSex().equals("1"))
            {
                str2CustomerSex = "女";
            }
        }
//    int str2CustomerAge = yLLCaseSchema.getCustomerAge();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NewLPsqs.vts", "printer"); //最好紧接着就初始化xml文档
        //对变量进行附值
        texttag.add("InsuredName", str2CustomerName);
        texttag.add("Sex", str2CustomerSex);
        texttag.add("Age", yLLCaseSchema.getCustomerAge());
        texttag.add("mIDNo", yLLCaseSchema.getIDNo());
        texttag.add("AccidentDate", yLLRegisterDB.getAccidentDate());
        texttag.add("AccidentSite", yLLRegisterDB.getAccidentSite());
        texttag.add("AccidentType", str2AccidentType);
        texttag.add("AccidentReason", mAccidentReason);
        texttag.add("AccidentCourse", strAccidentCourse);
        texttag.add("RgtantName", yLLRegisterDB.getRgtantName());
        texttag.add("IDType", strIDType);
        texttag.add("IDNo", yLLRegisterDB.getIDNo());
        texttag.add("RgtantAddress", yLLRegisterDB.getRgtantAddress());
        texttag.add("RgtantPhone", yLLRegisterDB.getRgtantPhone());
        texttag.add("LKType", strLKType);
        texttag.add("BankCode", yLLRegisterDB.getBankCode());
        texttag.add("AccName", yLLRegisterDB.getAccName());
        texttag.add("BankAccNo", yLLRegisterDB.getBankAccNo());
        texttag.add("Remark", strRemark);
        texttag.add("PolNo1", strPolNo1);
        texttag.add("PolNo2", strPolNo2);
        texttag.add("PolNo3", strPolNo3);
        texttag.add("PolNo4", strPolNo4);
        texttag.add("ApplyType", strApplyType);
        texttag.add("Handler1", yLLRegisterDB.getHandler1());
        texttag.add("Handler1Phone", yLLRegisterDB.getHandler1Phone());
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println("getPrintData end");
        return true;
    }
}
