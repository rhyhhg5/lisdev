package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author GUOXIANG
 * @version 1.0
 */

import com.f1j.ss.BookModelImpl;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class ShowGrpInsured
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();
    private String mOperate = "";
    private String mGrpPolNo = "";
    private BookModelImpl m_book = new BookModelImpl();

    //D-Display后客户自己生成execl;-用于小数据量
    //C-程序生成execl,客户下载--用于大数据量
    private String mPath = "D";
    public ShowGrpInsured()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        mOperate = cOperate;

        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        if (getPrintData() == false)
        {
            return false;
        }

        return true;

    }

    public static void main(String[] args)
    {
        BookModelImpl test = new BookModelImpl();
        try
        {
            test.read("e:\\200.xls", new com.f1j.ss.ReadParams());
            test.setSheetSelected(0, true);
            test.setCol(0);

            test.setEntry(1, 0, "hzm1");
            test.setEntry(1, 1, "xx");
            test.write("e:\\100.xls",
                       new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.
                                                  eFileExcel97));
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGrpPolNo = (String) cInputData.getObjectByObjectName("String", 0);
        mPath = (String) cInputData.getObjectByObjectName("String", 1);
        if (mPath == null)
        {
            mPath = "D";
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ShowGrpInsured";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

// 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        xmlExport.createDocument("InsuredList.vts", ""); //最好紧接着就初始化xml文档

        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mGrpPolNo);
        if (tLCGrpPolDB.getInfo() == false)
        {
            buildError("getPrintData", "在获取团体保单信息时出错");
            return false;
        }
        tLCGrpPolSchema = tLCGrpPolDB.getSchema();

        //查询集体单下的个人数目
        String strSql = "select count(*) from lcpol where grppolno='" +
                        mGrpPolNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSql);
        String strCount = tSSRS.GetText(1, 1);
        int iCount = Integer.parseInt(strCount);
        int NUM = 1;
        strSql = "select PolNo,InsuredNo from lcpol where grppolno='" +
                 mGrpPolNo + "'";

        ListTable listTable = new ListTable();
        ListTable listTable2 = new ListTable();
        String[] cols = new String[8];

        String tPolNo = "";
        String tInsuredNo = "";

        if (mPath.equals("D"))
        {
            while (NUM <= iCount)
            {
                System.out.println("Num:" + NUM);
                tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(strSql, NUM, 100);
                if (tSSRS == null)
                {
                    buildError("getPrintData", "集体下个人投保单读取失败");
                    return false;
                }
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tPolNo = tSSRS.GetText(i, 1);
                    tInsuredNo = tSSRS.GetText(i, 2);
                    cols = new String[8];
                    if (getOneRow(cols, tPolNo, tInsuredNo) == false)
                    {
                        return false;
                    }
                    listTable.add(cols);
                }
                NUM = NUM + 100;
            }
        }
        else
        {
            try
            {
                //m_book.read("e:\\InsuredMode.xls",new com.f1j.ss.ReadParams());
                //m_book.read("/weblogic/bea/config/mydomain/applications/DefaultWebApp/Xml/bq/InsuredMode.xls",new com.f1j.ss.ReadParams());
                LDSysVarDB tx = new LDSysVarDB();
                tx.setSysVar("ShowInsuredListPath");
                if (tx.getInfo() == false)
                {
                    buildError("getPrintData",
                               "LDSysVar取ShowInsuredListPath描述失败");
                    return false;
                }
                String path = tx.getSysVarValue();
                String filemode = path + "InsuredMode.xls";
                m_book.read(filemode, new com.f1j.ss.ReadParams());
                m_book.setSheetSelected(0, true);
                m_book.setCol(0);
                while (NUM <= iCount)
                {
                    System.out.println("Num:" + NUM);
                    tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(strSql, NUM, 100);
                    if (tSSRS == null)
                    {
                        buildError("getPrintData", "集体下个人投保单读取失败");
                        return false;
                    }
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                    {
                        tPolNo = tSSRS.GetText(i, 1);
                        tInsuredNo = tSSRS.GetText(i, 2);
                        cols = new String[8];
                        if (getOneRow(cols, tPolNo, tInsuredNo) == false)
                        {
                            return false;
                        }
                        m_book.setEntry(NUM + i - 1, 0, cols[0]);
                        m_book.setEntry(NUM + i - 1, 1, cols[1]);
                        m_book.setEntry(NUM + i - 1, 2, cols[2]);
                        m_book.setEntry(NUM + i - 1, 3, cols[3]);
                        m_book.setEntry(NUM + i - 1, 4, cols[4]);
                        m_book.setEntry(NUM + i - 1, 5, cols[5]);
                        m_book.setEntry(NUM + i - 1, 6, cols[6]);
                        m_book.setEntry(NUM + i - 1, 7, cols[7]);
                    }
                    NUM = NUM + 100;
                }
                m_book.write(path + "InsuredList.xls.z",
                             new com.f1j.ss.
                             WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
            }
            catch (Exception ex)
            {
                System.out.println(ex);
            }
        }

        System.out.println("over:");
        // 设置列名
        cols = new String[8];
        cols[0] = "Name";
        cols[1] = "Sex";
        cols[2] = "Birthday";
        cols[3] = "IDType";
        cols[4] = "IDNo";
        cols[5] = "WorkType";
        cols[6] = "OccupationType";
        cols[7] = "OccupationCode";

        listTable.setName("InsuInfo");
        xmlExport.addListTable(listTable, cols);
        TextTag texttag = new TextTag();
        texttag.add("GrpPolNo", tLCGrpPolSchema.getGrpPolNo());
        texttag.add("GrpName", tLCGrpPolSchema.getGrpName());
        texttag.add("RiskCode", tLCGrpPolSchema.getRiskCode());
        if (texttag.size() > 0)
        {
            xmlExport.addTextTag(texttag);
        }

        mResult.clear();
        mResult.addElement(xmlExport);
        return true;

    }

    private boolean getOneRow(String[] cols, String PolNo, String InsuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        /*Lis5.3 upgrade set
             tLCInsuredDB.setPolNo(PolNo);
             tLCInsuredDB.setCustomerNo(InsuredNo);
         */
        if (tLCInsuredDB.getInfo() == false)
        {
            buildError("getOneRow", "没有找到个人保单对应的被保人信息");
            return false;
        }
        /*Lis5.3 upgrade set
             cols[0] = tLCInsuredDB.getName();
             cols[1] = tLCInsuredDB.getSex();
             cols[2] = tLCInsuredDB.getBirthday();
             cols[3] = tLCInsuredDB.getIDType();
             cols[4] = tLCInsuredDB.getIDNo();
         */
        cols[0] = "";
        cols[1] = "";
        cols[2] = "";
        cols[3] = "";
        cols[4] = "";

        cols[5] = tLCInsuredDB.getWorkType();
        cols[6] = tLCInsuredDB.getOccupationType();
        cols[7] = tLCInsuredDB.getOccupationCode();
        return true;
    }

}
