package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FeeInvoiceListPrintBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 查询要素
	private String mEnterAccDate = "";

	private String mEnterAccDateEnd = "";

	private String mIncomeType = "";

	private String mSaleChnl = "";

	private String mPrtNo = "";

	private String mIncomeNo = "";

	private String mMngCom = "";

	private String mAgentCode = "";

	private String mInvoiceState = "";

	private String mCardFlag = "";

	private String mAgentCom = "";

	private String mAgentComAgentCode = "";

	private String mBankContFlag = "";

	private String wherePart = "";

	// 查询出的结果集
	private String[][] mExcelData = null;

	public FeeInvoiceListPrintBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}
		if (mIncomeType.equals("1")) {
			// 准备所有要打印的数据
			if (!GgetPrintData()) {
				return null;
			}
		} else if (mIncomeType.equals("2")) {
			// 准备所有要打印的数据
			if (!PgetPrintData()) {
				return null;
			}
		} else {
			if (!OgetPrintData()) {
				return null;
			}
		}

		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	public static void main(String[] args) {

		FeeInvoiceListPrintBL tbl = new FeeInvoiceListPrintBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2010-10-08");
		tTransferData.setNameAndValue("EndDate", "2010-10-08");
		tTransferData.setNameAndValue("ManageCom", "8644");
		tGlobalInput.ManageCom = "8644";
		tGlobalInput.Operator = "xp";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData, "PRINT");
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\contactcompare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mEnterAccDate = (String) mTransferData.getValueByName("EnterAccDate");
		mEnterAccDateEnd = (String) mTransferData
				.getValueByName("EnterAccDateEnd");
		mIncomeType = (String) mTransferData.getValueByName("IncomeType");
		mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mIncomeNo = (String) mTransferData.getValueByName("IncomeNo");
		mMngCom = (String) mTransferData.getValueByName("MngCom");
		mAgentCode = (String) mTransferData.getValueByName("AgentCode");
		mInvoiceState = (String) mTransferData.getValueByName("InvoiceState");
		mCardFlag = (String) mTransferData.getValueByName("CardFlag");
		mAgentCom = (String) mTransferData.getValueByName("AgentCom");
		mAgentComAgentCode = (String) mTransferData
				.getValueByName("AgentComAgentCode");
		mBankContFlag = (String) mTransferData.getValueByName("BankContFlag");

		if (mInvoiceState.equals("") || mInvoiceState == null) {
			CError.buildErr(this, "没有得到足够的信息:发票状态不能为空！");
			System.out.println("FeeInvoiceListPrintBL+getInputData++--");
			return false;
		}
		// if(
		// mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null
		// ) {
		// buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
		// return false;
		// }
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LCGrpContF1PBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean GgetPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = { { "付款方名称", "付款方纳税人识别号", "项目摘要", "合计金额人民币小写",
				"开票日期", "发票代码", "发票号码", "开票方名称", "开票方纳税人识别号", "发票状态" } };
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}
		System.out.println("mAgentCom...."+mAgentCom);
		if (!( mAgentCom == null||mAgentCom.equals(""))) {
			wherePart += " and b.AgentCom='" + mAgentCom + "' ";
		}
		if (!(mAgentComAgentCode == null||mAgentComAgentCode.equals(""))) {
			wherePart += " and  b.AgentCom=(select agentcom from lacomtoagent where relatype='1' and agentcode='"
					+ mAgentComAgentCode + "') ";
		}

		if (!(mSaleChnl == null||mSaleChnl.equals("") )) {
			wherePart += " and  b.SaleChnl='" + mSaleChnl + "' ";
		}
		if (!(mPrtNo == null||mPrtNo.equals("") )) {
			if (mIncomeType == "1") {
				wherePart += " and exists (select '1' from lcgrpcont where grpcontno=a.incomeno and prtno = '"
						+ mPrtNo + "')";
			} else if (mIncomeType == "2") {
				wherePart += " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"
						+ mPrtNo + "')";
			}
		}
		if (!( mIncomeType == null||mIncomeType.equals("") )) {
			wherePart += " and  a.IncomeType='" + mIncomeType + "' ";
		}
		if (!(mIncomeNo == null||mIncomeNo.equals(""))) {
			wherePart += " and  a.IncomeNo='" + mIncomeNo + "' ";
		}
		if (!(mAgentCode == null||mAgentCode.equals(""))) {
			wherePart += " and  b.AgentCode='" + mAgentCode + "' ";
		}
		if (mInvoiceState.equals("1")) {
			wherePart += " and (select count(1) from LOPRTManager2 b where otherno=a.payno and code in('35','36') and stateflag='1') = 0";
		}
		if (mInvoiceState.equals("2")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		} else if (mInvoiceState.equals("4")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='36' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		}
		if (!(mEnterAccDate == null||mEnterAccDate.equals("") )) {
			wherePart += " and a.ConfDate>='" + mEnterAccDate + "' ";
		}
		if (!(mEnterAccDateEnd == null||mEnterAccDateEnd.equals("") )) {
			wherePart += " and a.ConfDate<='" + mEnterAccDateEnd + "' ";
		}
		if (!( mCardFlag == null||mCardFlag.equals("") )) {
			wherePart += " and b.cardflag='" + mCardFlag + "' ";
		}
		if (!( mMngCom == null||mMngCom.equals(""))) {
			wherePart += " and a.ManageCom like '" + mMngCom + "%' ";
		}

		// 获得EXCEL列信息
		String strSQL = " select b.grpname,"
				+ " (select organcomcode from lcgrpappnt where grpcontno=b.grpcontno),"
				+ " b.prem,"
				+ " a.SumActuPayMoney,"
				+ " (select max(makedate) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " (select max(oldprtseq) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " '',"
				// 开票方名称
				+ " (select LetterServiceName from ldcom where comcode=a.managecom),"
				// 开票方纳税人识别号
				+ " (select TaxRegistryNo from ldcom where comcode=a.managecom),"
				// 发票状态
				+ " '',"
				+ " a.payno "
				+ " from LJAPay a,lcgrpcont b where 1=1 and a.makedate>'2007-06-27' and a.IncomeNo=b.grpcontno "
				+ wherePart;
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(strSQL);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}
		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		for (int j = 0; j < mExcelData.length; j++) {
			// 获取发票号码
			mExcelData[j][6] = getinvoiceno(mExcelData[j][10]);
			// 获取发票状态
			mExcelData[j][9] = getinvoicestate(mExcelData[j][10]);
		}
		if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}

		return true;
	}
	
	private boolean PgetPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = { { "付款方名称", "付款方纳税人识别号", "项目摘要", "合计金额人民币小写",
				"开票日期", "发票代码", "发票号码", "开票方名称", "开票方纳税人识别号", "发票状态" } };
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}
		if(mBankContFlag.equals("1")){
			wherePart+= " and b.salechnl='04' and b.cardflag='9' ";
		}
		if(mBankContFlag.equals("2")){
			wherePart+= " and (b.cardflag<>'9' or b.cardflag is null) ";
		}		
		if (!( mAgentCom == null||mAgentCom.equals("") )) {
			wherePart += " and b.AgentCom='" + mAgentCom + "' ";
		}
		if (!(mAgentComAgentCode == null||mAgentComAgentCode.equals("") )) {
			wherePart += " and  b.AgentCom=(select agentcom from lacomtoagent where relatype='1' and agentcode='"
					+ mAgentComAgentCode + "') ";
		}

		if (!(mSaleChnl == null||mSaleChnl.equals("") )) {
			wherePart += " and  b.SaleChnl='" + mSaleChnl + "' ";
		}
		if (!(mPrtNo == null||mPrtNo.equals("") )) {
			if (mIncomeType == "1") {
				wherePart += " and exists (select '1' from lcgrpcont where grpcontno=a.incomeno and prtno = '"
						+ mPrtNo + "')";
			} else if (mIncomeType == "2") {
				wherePart += " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"
						+ mPrtNo + "')";
			}
		}
		if (!( mIncomeType == null||mIncomeType.equals("") )) {
			wherePart += " and  a.IncomeType='" + mIncomeType + "' ";
		}
		if (!(mIncomeNo == null||mIncomeNo.equals(""))) {
			wherePart += " and  a.IncomeNo='" + mIncomeNo + "' ";
		}
		if (!(mAgentCode == null||mAgentCode.equals(""))) {
			wherePart += " and  b.AgentCode='" + mAgentCode + "' ";
		}
		if (mInvoiceState.equals("1")) {
			wherePart += " and (select count(1) from LOPRTManager2 b where otherno=a.payno and code in('35','36') and stateflag='1') = 0";
		}
		if (mInvoiceState.equals("2")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		} else if (mInvoiceState.equals("4")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='36' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		}
		if (!(mEnterAccDate == null||mEnterAccDate.equals("") )) {
			wherePart += " and a.ConfDate>='" + mEnterAccDate + "' ";
		}
		if (!(mEnterAccDateEnd == null||mEnterAccDateEnd.equals("") )) {
			wherePart += " and a.ConfDate<='" + mEnterAccDateEnd + "' ";
		}
		if (!( mCardFlag == null||mCardFlag.equals("") )) {
			wherePart += " and b.cardflag='" + mCardFlag + "' ";
		}
		if (!( mMngCom == null||mMngCom.equals(""))) {
			wherePart += " and a.ManageCom like '" + mMngCom + "%' ";
		}

		// 获得EXCEL列信息
		String strSQL = " select b.appntname,"
				+ " b.appntidno,"
				+ " b.prem,"
				+ " a.SumActuPayMoney,"
				+ " (select max(makedate) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " (select max(oldprtseq) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " '',"
				// 开票方名称
				+ " (select LetterServiceName from ldcom where comcode=a.managecom),"
				// 开票方纳税人识别号
				+ " (select TaxRegistryNo from ldcom where comcode=a.managecom),"
				// 发票状态
				+ " '',"
				+ " a.payno "
				+ " from LJAPay a,lccont b where 1=1 and a.makedate>'2007-06-27' and a.IncomeNo=b.contno  "
				+ wherePart;
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(strSQL);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}
		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		for (int j = 0; j < mExcelData.length; j++) {
			// 获取发票号码
			mExcelData[j][6] = getinvoiceno(mExcelData[j][10]);
			// 获取发票状态
			mExcelData[j][9] = getinvoicestate(mExcelData[j][10]);
		}
		if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}

		return true;
	}
	
	
	private boolean OgetPrintData() {

		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = { { "付款方名称", "付款方纳税人识别号", "项目摘要", "合计金额人民币小写",
				"开票日期", "发票代码", "发票号码", "开票方名称", "开票方纳税人识别号", "发票状态" } };
		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		// 获得EXCEL列信息
		String strSQL = " select "
				+ " (select distinct appntname from lccont where exists (select '1' from ljapayperson where contno=lccont.contno and payno =a.payno) "
			  	+ " union  (select b.grpname from lcgrpcont b,ljagetendorse c where b.grpcontno = c.grpcontno and a.payno = c.actugetno fetch first 1 rows only)  "
			  	+ " union (select b.grpname from lcgrpcont b,lgwork c where b.grpcontno = c.contno and c.workno=a.incomeno  fetch first 1 rows only) fetch first 1 rows only   ), "
			  	+ " (select distinct appntidno from lccont where exists (select '1' from ljapayperson where contno=lccont.contno and payno =a.payno) "
			  	+ " union  (select (select organcomcode from lcgrpappnt where grpcontno=b.grpcontno) from lcgrpcont b,ljagetendorse c where b.grpcontno = c.grpcontno and a.payno = c.actugetno fetch first 1 rows only)  "
			  	+ " union (select (select organcomcode from lcgrpappnt where grpcontno=b.grpcontno) from lcgrpcont b,lgwork c where b.grpcontno = c.contno and c.workno=a.incomeno  fetch first 1 rows only) fetch first 1 rows only  ), "
				+ " (select distinct prem from lccont where exists (select '1' from ljapayperson where contno=lccont.contno and payno =a.payno) "
			  	+ " union  (select b.prem from lcgrpcont b,ljagetendorse c where b.grpcontno = c.grpcontno and a.payno = c.actugetno fetch first 1 rows only)  "
			  	+ " union (select b.prem from lcgrpcont b,lgwork c where b.grpcontno = c.contno and c.workno=a.incomeno  fetch first 1 rows only) fetch first 1 rows only  ), "
				+ " a.SumActuPayMoney,"
				+ " (select max(makedate) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " (select max(oldprtseq) from loprtmanager where otherno=a.payno and code='invoice'),"
				+ " '',"
				// 开票方名称
				+ " (select LetterServiceName from ldcom where comcode=a.managecom),"
				// 开票方纳税人识别号
				+ " (select TaxRegistryNo from ldcom where comcode=a.managecom),"
				// 发票状态
				+ " '',"
				+ " a.payno "
				+ " from LJAPay a where 1=1 and a.makedate>'2007-06-27'  ";
		
		if (!( mAgentCom == null||mAgentCom.equals("") )) {
			strSQL += " and AgentCom='" + mAgentCom + "' ";
		}
		if (!(mAgentComAgentCode == null||mAgentComAgentCode.equals("") )) {
			strSQL += " and  AgentCom=(select agentcom from lacomtoagent where relatype='1' and agentcode='"
					+ mAgentComAgentCode + "') ";
		}
		
		if(mIncomeType.equals("15"))
		{			
			strSQL = " "+
			
			" select distinct (select appntname from lccont where contno=b.contno),"
			+ " (select appntidno from lccont where contno=b.contno),"
			+ " (select prem from lccont where contno=b.contno),"
			+ " a.SumActuPayMoney,"
			+ " (select max(makedate) from loprtmanager where otherno=a.payno and code='invoice'),"
			+ " (select max(oldprtseq) from loprtmanager where otherno=a.payno and code='invoice'),"
			+ " '',"
			// 开票方名称
			+ " (select LetterServiceName from ldcom where comcode=a.managecom),"
			// 开票方纳税人识别号
			+ " (select TaxRegistryNo from ldcom where comcode=a.managecom),"
			// 发票状态
			+ " '',"
			+ " a.payno "
			+ " from LJAPay a,LJAPayPerson b where 1=1 and a.makedate>'2007-06-27' and a.PayNo=b.PayNo ";
			if (!(mPrtNo == null||mPrtNo.equals("") )) {
				strSQL+=" and b.ContNo='"+mPrtNo+"' ";
			}
			
		}		 
		if(mIncomeType.equals("3"))
		{
			strSQL += " and a.IncomeType in('3','13') ";
		}
		else
		{	
			strSQL += " and a.IncomeType ='"+mIncomeType+"' ";
		}
		
		if (!(mIncomeNo == null||mIncomeNo.equals(""))) {
			wherePart += " and  a.IncomeNo='" + mIncomeNo + "' ";
		}
		if (!(mAgentCode == null||mAgentCode.equals(""))) {
			wherePart += " and  a.AgentCode='" + mAgentCode + "' ";
		}
		if (mInvoiceState.equals("1")) {
			wherePart += " and (select count(1) from LOPRTManager2 b where otherno=a.payno and code in('35','36') and stateflag='1') = 0";
		}
		if (mInvoiceState.equals("2")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		} else if (mInvoiceState.equals("4")) {
			wherePart += " and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='36' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1' )";
		}
		if (!(mEnterAccDate == null||mEnterAccDate.equals("") )) {
			wherePart += " and a.ConfDate>='" + mEnterAccDate + "' ";
		}
		if (!(mEnterAccDateEnd == null||mEnterAccDateEnd.equals("") )) {
			wherePart += " and a.ConfDate<='" + mEnterAccDateEnd + "' ";
		}
		if (!( mMngCom == null||mMngCom.equals(""))) {
			if(!mIncomeType.equals("15"))
			  {			
				wherePart += " and a.ManageCom like '" + mMngCom + "%' ";
			  }else
			  {
				  wherePart += " and a.ManageCom = '" + mMngCom + "' ";
			  }
			
		}
		strSQL += wherePart;
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(strSQL);
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}
		mExcelData = tSSRS.getAllData();
		if (mExcelData == null) {
			CError tError = new CError();
			tError.moduleName = "CreateExcelList";
			tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		for (int j = 0; j < mExcelData.length; j++) {
			// 获取发票号码
			mExcelData[j][6] = getinvoiceno(mExcelData[j][10]);
			// 获取发票状态
			mExcelData[j][9] = getinvoicestate(mExcelData[j][10]);
		}
		if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}

		return true;
	}

	private String getinvoicestate(String tpayno) {
		String tState = "";
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
		tLOPRTManagerDB.setOtherNo(tpayno);
		tLOPRTManagerDB.setCode("invoice");
		if(tLOPRTManagerDB.query().size()==0){
			return "";
		}
		tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
		LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
		LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
		tLOPRTManager2DB.setOtherNo(tpayno);
		if(tLOPRTManager2DB.query().size()==0){
			return "";
		}
		tLOPRTManager2Schema = tLOPRTManager2DB.query().get(1);
		tState=new ExeSQL().getOneValue("select case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end from lzcard where certifycode='"+tLOPRTManagerSchema.getStandbyFlag1()+"' and startno='"+tLOPRTManager2Schema.getStandbyFlag4()+"'");
		return tState;

	}

	private String getinvoiceno(String tpayno) {
		String tStartNo = "";
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
		tLOPRTManagerDB.setOtherNo(tpayno);
		tLOPRTManagerDB.setCode("invoice");
		if(tLOPRTManagerDB.query().size()==0){
			return "";
		}
		tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
		String tStandbyFlag1 = tLOPRTManagerSchema.getStandbyFlag1();
		try{
		int tNum = tStandbyFlag1.lastIndexOf(",");
		String tCertifyCode = tStandbyFlag1.substring(0, tNum);
		tLOPRTManagerSchema.setStandbyFlag1(tCertifyCode);
		tStartNo = tStandbyFlag1.substring(tNum + 1);
		if ("00000000".equals(tStartNo)) {

			String tMinSql = "select min(StartNo) from LZCARD WHERE CertifyCode='"
					+ tCertifyCode
					+ "' and ReceiveCom='B"
					+ this.mGlobalInput.Operator
					+ "' and StateFlag in ('0','7') ";
			SSRS tSSRS = new ExeSQL().execSQL(tMinSql);
			if (tSSRS.getMaxRow() > 0) {
				tStartNo = tSSRS.GetText(1, 1);
				return tStartNo;
			}
		}
		}
		catch(Exception e){
			LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
			LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
			tLOPRTManager2DB.setOtherNo(tpayno);
			if(tLOPRTManager2DB.query().size()==0){
				return "";
			}
			tLOPRTManager2Schema = tLOPRTManager2DB.query().get(1);
			return tLOPRTManager2Schema.getStandbyFlag4();
		}
		return tStartNo;

	}
}
