 
package com.sinosoft.lis.f1print;

/**
 * <p>Title: YBTydDayBL</p>
 * <p>Description:X7-银保通异地预收保费日结提数 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : yan
 * @date:2013-09-16
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class YBTydDayExcelBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
         private String mDay[] = null; //获取时间
         private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
         private String tOutXmlPath = ""; //文件路径
         private String sum1="0";
         private double sum2=0;
         private String sum3="0";
         private double sum4=0;
         private String sum5="0";
         
    public YBTydDayExcelBL() {
    }
    
    public static void main(String[] args) {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT") ) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate.equals("PRINT")) { //打印提数
            if (!getPrintData()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (tOutXmlPath == null || tOutXmlPath.equals("")) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "YBTydDayExcelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){
        int no=5;  //初始打印行数

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
       
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        tGetSQLFromXML.setParameters("SumMoney", sum1);
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//        tServerPath= "C:/Users/db2admin.qqing-pc/Picch/ui/";
            
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[]{
                "YBTYDDS",    // 银保通代收日结单
                };
        
        for(int i=0; i<getTypes.length;i++){
            String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = tExeSQL.execSQL(msql);
            int maxrow = tSSRS.getMaxRow();

            no=no+maxrow;
            
            // 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
            msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]+"Z");
            tSSRS = tExeSQL.execSQL(msql);
            int zmaxrow = tSSRS.getMaxRow();
            if(zmaxrow == 0){
                no=no+1;
            }else{
                no=no+zmaxrow+2; 
            }
        }
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

    /**
     * 新的打印数据方法
     * @return
     */
    private boolean getPrintData(){
    	
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        
        int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+10][7];
        mToExcel[0][0] = "银保通日结单";
        mToExcel[1][0] = "统计日期：";
        mToExcel[1][1] = mDay[0]+"至"+mDay[1];
        mToExcel[2][0] = "管理机构：";
        mToExcel[2][1] = manageCom;
        mToExcel[4][0] = "类型";
        mToExcel[4][1] = "渠道";
        mToExcel[4][2] = "保单号码";
        mToExcel[4][3] = "金额";
        mToExcel[4][4] = "净保费";
        mToExcel[4][5] = "税额";
        mToExcel[4][6] = "对方代码";
        mToExcel[3][1] = "健康服务管理费应总公司财务要求不在日结单显示";
        
        int no=5;  //初始打印行数
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        tGetSQLFromXML.setParameters("SumMoney", sum1);
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//tServerPath= "E:/lisdev/ui/";
//		tServerPath ="C:/Users/db2admin.qqing-pc/Picch/ui/";
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[]{
        		"YBTYDDS",    // 银保通代收日结单
        		};
        
        for(int i=0; i<getTypes.length;i++){
        	String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
        	System.out.println("我在上面");
        	tSSRS = tExeSQL.execSQL(msql);
        	System.out.println("我在下面");
        	int maxrow = tSSRS.getMaxRow();
        	for(int row = 1; row <= maxrow; row++){
        		for(int col = 1; col <= 7; col++){
        			if (row >= 2 && ((tSSRS.GetText(row, 3)).equals(tSSRS.GetText(row-1, 3)))){
        				System.out.println("重复啦！！！");
                   	 mToExcel[no+row-1][4]= new DecimalFormat("0.00").format(Double.valueOf("0.00"));
                   	 mToExcel[no+row-1][5]= new DecimalFormat("0.00").format(Double.valueOf("0.00"));           
                }else{
        			if(col==4||col==5||col==6){
        				mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
        				System.out.println("第"+row+"行，第"+col+"列的值是："+tSSRS.GetText(row, col));
        			}else{
        				mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
        				System.out.println("第"+row+"行，第"+col+"列的值是："+tSSRS.GetText(row, col));
        			}
                }
        		}
        	}
        	no=no+maxrow;
        	
        	// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
        	msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]+"Z");
        	tSSRS = tExeSQL.execSQL(msql);
        	int zmaxrow = tSSRS.getMaxRow();
        	if(zmaxrow < 0){
        		String [] str = this.getChineseNameWithPY(getTypes[i]+"Z");
        		mToExcel[no][0] = str[0];
        		mToExcel[no][3] = str[2];
        		mToExcel[no][4] = str[2];
        		mToExcel[no][5] = str[2];
        		no=no+1;
        	}else if (zmaxrow == 0)
        	{
        		mToExcel[no][2] = "总计金额：";
                mToExcel[no][3] = sum1;
                mToExcel[no][4] = sum3;
                mToExcel[no][5] = sum5;
                

              
        	}else{
        		double sum = 0;
        		for(int row = 1; row <= zmaxrow; row++){
        			mToExcel[no+row-1][0] = tSSRS.GetText(row, 1);
            		mToExcel[no+row-1][3] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
            		mToExcel[no+row-1][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
            		mToExcel[no+row-1][5] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 4)));
//            		保费
            		sum += Double.parseDouble((tSSRS.GetText(row, 2)));
    	            sum1=new DecimalFormat("0.00").format(Double.valueOf(String.valueOf(sum)));
//    	                             净保费
    	            sum2 += Double.parseDouble((tSSRS.GetText(row, 3)));
    	            sum3=new DecimalFormat("0.00").format(Double.valueOf(String.valueOf(sum2)));
//                  税额
    	            sum4 += Double.parseDouble((tSSRS.GetText(row, 4)));
    	            sum5=new DecimalFormat("0.00").format(Double.valueOf(String.valueOf(sum4)));
            	}
        		no=no+zmaxrow+2; 
        		
        		mToExcel[no-2][2] = "总计金额：";
            mToExcel[no-2][3] = sum1;
            mToExcel[no-2][4] = sum3;
            mToExcel[no-2][5] = sum5;
        	}
            
        }
        mToExcel[no+1][0] = "制表员：";
        mToExcel[no+1][2] = "审核员：";
        
        try{
        	System.out.println("X7--tOutXmlPath:"+tOutXmlPath);
        	WriteToExcel t = new WriteToExcel("");
        	t.createExcelFile();
        	String [] sheetName = {PubFun.getCurrentDate()};
        	t.addSheet(sheetName);
        	t.setData(0, mToExcel);
        	t.write(tOutXmlPath);
        	System.out.println("生成X7文件完成");
        }catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
        	buildError("getPrintData", e.toString());
		}
    	return true;
    }

	/**
	 * 显示无金额的情况
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName){
		String[] result = new String[2];
	    if("YBTYDDSZ".equals(pName)){
			result[0] = "银保通代收日结单 合计";
		}
		result[1] = "0";
		return result;
	}

}
