package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.InterfaceTableSchema;

public class DataBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String agentcom = "";
    private String accountdate = "";
    private String mCurDate = PubFun.getCurrentDate();
    private String newaccountdate="";
    	
	private String[][] mToExcel = null;
	
       

    public DataBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {

    	System.out.println("Start DataBL Submit ...");
        try{
//        if (!cOperate.equals("UPDATEE") &&!cOperate.equals("UPDATES")) {
//        	buiError("submitData", "不支持的操作字符串");
//            return false;
//        }
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }
        
//        置空数据
            if (!emptyData()) {
             return false;
            }         
            }           
            catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "XqqGrpCrsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean emptyData()
    {
        System.out.println("BL->供应商为空数据维护"); 
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
        System.out.println("agentcom : "+ agentcom);    
        tmap.put("update fiabstandarddata a set supplierno=LF_getBankCodeForAgentCom(agentcom,'A',managecom) where 1=1 and agentcom = '"+agentcom+"' and Accountdate = '"+accountdate+"' and (bustypeid in('YF-SX-000002','YF-SX-000001','YF-SX-000003','YF-SX-000004','F-CG-000001','F-CG-000003') or (bustypeid in ('DF-CG-000001','DF-CG-000002') and costid='F000000000P')) and exists(select 1 from fivoucherdatadetail b where a.serialno=b.serialno and checkflag='01' and accountdate='"+accountdate+"') and db2inst1.LF_getBankCodeForAgentCom(agentcom,'A',managecom) is not null","UPDATE");
        tmap.put("update fiabstandarddata a set state='00', readstate='0' where 1=1 and agentcom = '"+agentcom+"' and Accountdate = '"+accountdate+"' and bustypeid in('YF-SX-000002','YF-SX-000001','YF-SX-000003','YF-SX-000004','F-CG-000001','DF-CG-000001','DF-CG-000002','F-CG-000002','F-CG-000003','F-CG-000004') and exists(select 1 from fivoucherdatadetail b where a.serialno=b.serialno and checkflag='01' and accountdate='"+accountdate+"') and db2inst1.LF_getBankCodeForAgentCom(agentcom,'A',managecom) is not null", "UPDATE");
        tmap.put("delete from fivoucherdatadetail a where Accountdate = '"+accountdate+"'  and checkflag='01' and (classtype like 'OS%' or classtype like 'S%') and exists(select 1 from fiabstandarddata where agentcom = '"+agentcom+"' and serialno=a.serialno and (bustypeid in('YF-SX-000002','YF-SX-000001','YF-SX-000003','YF-SX-000004','F-CG-000001','DF-CG-000001','DF-CG-000002','F-CG-000002','F-CG-000003','F-CG-000004')))", "DELETE");
        tInputData.add(tmap);
        System.out.println(tmap);       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "中介机构为" + agentcom + "维护数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        agentcom = (String) tf.getValueByName("AgentCom");      
        accountdate = (String) tf.getValueByName("AccountDate");
        newaccountdate = (String) tf.getValueByName("NewAccountDate");
        if (agentcom == null || accountdate == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "DataBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
//    public static void main(String[] args)
//    {
//        GlobalInput tG = new GlobalInput();
//        tG.ManageCom = "86";
//        tG.Operator = "cwad";
//        tG.ComCode = "86";
//        TransferData tTransferData = new TransferData();
//        VData vData = new VData();
//        vData.add(tG);
//        vData.add(tTransferData);
//       DataUI tDataUI = new DataUI();
//        if (!tDataUI.submitData(vData, ""))
//        {
//            System.out.print("失败！");
//        }
//    }
}

