package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NewPreRecAgePrintBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;

    private String mOutXmlPath = null;

    private String StartDate = "";

    private String EndDate = "";

    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();

    public NewPreRecAgePrintBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        System.out.println("BL->dealDate()");
        String dSql = "";
        if (StartDate != null && !StartDate.equals(""))
        {
            dSql = " and gc.cvalidate >='" + StartDate + "'";
        }
        ExeSQL manExeSQL = new ExeSQL();
        String manSql = "Select Distinct Gc.Managecom Managecom "
                + "From Lcgrpcont Gc, Lcgrppayplandetail Gp "
                + "Where Gc.Payintv = -1 And Gc.Prtno = Gp.Prtno and gc.managecom like '"
                + mGI.ManageCom
                + "%' And Exists "
                + "(Select 1 From Lcgrppayplan Gpp Where Gpp.Prtno = Gc.Prtno) and "
                + "exists (select 1 from lcgrppayplandetail gppd where gppd.prtno = gc.prtno) "
                + "and gc.cvalidate <= '" + EndDate + "' " + dSql
                + " and Gc.appflag = '1' and Gc.signdate is not null with ur";
        SSRS manSSRS = manExeSQL.execSQL(manSql);
        if (manExeSQL.mErrors.needDealError())
        {
            System.out.println(manExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if (manSSRS.getMaxRow() == 0)
        {

            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有符合条件的数据，请重新输入条件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String[][] mToExcel = new String[10000][30];
        mToExcel[0][0] = "中国人民健康保险股份有限公司";
        mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))
                + "年"
                + EndDate.substring(EndDate.indexOf('-') + 1, EndDate
                        .lastIndexOf('-')) + "月应收保费账龄统计表";
        mToExcel[2][0] = "编制单位：" + getName();
        mToExcel[2][10] = "金额单位：元";

        mToExcel[3][0] = "序号";
        mToExcel[3][1] = "机构代码";
        mToExcel[3][2] = "机构名称";
        mToExcel[3][3] = "险类";
        mToExcel[3][4] = "险种代码";
        mToExcel[3][5] = "险种名称";
        mToExcel[3][6] = "业务序号";
        mToExcel[3][7] = "应收保费余额";
        mToExcel[3][8] = "未逾期应收保费余额";
        mToExcel[3][9] = "逾期在3个月以内的应收保费余额";
        mToExcel[3][10] = "逾期超过3个月但在6个月内的应收保费余额";
        mToExcel[3][11] = "逾期超过6个月但在1年内的应收保费余额";
        mToExcel[3][12] = "逾期超过1年的应收保费余额";
        String polCom = manSSRS.GetText(1, 1).substring(0, 4) + "0000";

        int printNum = 3;
        double sumMoney[] = { 0, 0, 0, 0, 0, 0 }; //总合计
        double sMoney[] = { 0, 0, 0, 0, 0, 0 }; //分公司合计

        for (int comnum = 1; comnum <= manSSRS.getMaxRow(); comnum++)
 {
			ExeSQL tExeSQL = new ExeSQL();
			mSql = "select '1',managecom,(select name from ldcom where comcode=managecom),(select codename from ldcode where codetype='risktype' and code=substr(pay.riskcode,1,4)),riskcode,(select RiskName from LMRisk where rtrim(RiskCode) = pay.riskcode),'团险渠道',sum(shouldpay),sum(shouldpay0),sum(shouldpay1),sum(shouldpay3),sum(shouldpay6),sum(shouldpay12) from ("
					+ "select distinct"
					+ " gc.managecom managecom,gc.prtno prtno,riskcode"
					//应收保费
					+ ",(select SUM(shouldpaya) "
					+ "from (select p.prtno,"
					+ "                      p.riskcode,"
					+ "                      p.prem -"
					+ "                      (select (case"
					+ "                                when sum(d.prem) is not null then"
					+ "                                 sum(d.prem)"
					+ "                                when sum(d.prem) is null and"
					+ "                                     p.state = '1' then"
					+ "                                 p.prem"
					+ "                                when sum(d.prem) is null and"
					+ "                                     p.state = '2' then"
					+ "                                 0"
					+ "                              end)"
					+ "                         from db2inst1.LcGrpPayDueDetail d"
					+ "                        where d.prtno = p.prtno"
					+ "                          and p.paytodate = d.paytodate"
					+ "                          and d.contplancode = p.contplancode"
					+ "                          and d.plancode = p.plancode"
					+ "                          and p.riskcode = d.riskcode) as shouldpaya"
					+ "                 from lcgrppayplandetail p) as insFee1"
					+ "        where prtno = gc.prtno"
					+ "          and riskcode = gp.riskcode"
					+ "        group by prtno) shouldpay"
					//未逾期应收保费
					+ ",(select SUM(shouldpaya)"
					+ "          from (select p.prtno,"
					+ "                       p.riskcode,"
					+ "                       p.paytodate,"
					+ "                       p.prem -"
					+ "                       (select (case"
					+ "                                 when sum(d.prem) is not null then"
					+ "                                  sum(d.prem)"
					+ "                                 when sum(d.prem) is null and"
					+ "                                      p.state = '1' then"
					+ "                                  p.prem"
					+ "                                 when sum(d.prem) is null and"
					+ "                                      p.state = '2' then"
					+ "                                  0"
					+ "                               end)"
					+ "                          from db2inst1.LcGrpPayDueDetail d"
					+ "                         where d.prtno = p.prtno"
					+ "                           and p.paytodate = d.paytodate"
					+ "                           and d.contplancode = p.contplancode"
					+ "                           and d.plancode = p.plancode"
					+ "                           and p.riskcode = d.riskcode) as shouldpaya"
					+ "                  from lcgrppayplandetail p) as insFee2"
					+ "         where prtno = gc.prtno"
					+ "           and riskcode = gp.riskcode"
					+ "           and current date <= paytodate + 1 month"
					+ "         group by prtno) shouldpay0"
					// --逾期在3个月以内的应收保费余额
					+ ",(select SUM(shouldpaya)"
					+ "   from (select p.prtno,"
					+ "                p.riskcode,"
					+ "                p.paytodate,"
					+ "                p.prem -"
					+ "                (select (case"
					+ "                          when sum(d.prem) is not null then"
					+ "                           sum(d.prem)"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '1' then"
					+ "                           p.prem"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '2' then"
					+ "                           0"
					+ "                        end)"
					+ "                   from db2inst1.LcGrpPayDueDetail d"
					+ "                  where d.prtno = p.prtno"
					+ "                    and p.paytodate = d.paytodate"
					+ "                    and d.contplancode = p.contplancode"
					+ "                    and d.plancode = p.plancode"
					+ "                    and p.riskcode = d.riskcode) as shouldpaya"
					+ "           from lcgrppayplandetail p) as insFee3 "
					+ "  where prtno = gc.prtno"
					+ "    and riskcode = gp.riskcode"
					+ "    and current date > paytodate + 1 month"
					+ "    and current date <= paytodate + 3 month"
					+ "  group by prtno) shouldpay1"
					// --逾期超过3个月但在6个月内的应收保费余额
					+ ",(select SUM(shouldpaya)"
					+ "   from (select p.prtno,"
					+ "                p.riskcode,"
					+ "                p.paytodate,"
					+ "                p.prem -"
					+ "                (select (case"
					+ "                          when sum(d.prem) is not null then"
					+ "                           sum(d.prem)"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '1' then"
					+ "                           p.prem"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '2' then"
					+ "                           0"
					+ "                        end)"
					+ "                   from db2inst1.LcGrpPayDueDetail d"
					+ "                  where d.prtno = p.prtno"
					+ "                    and p.paytodate = d.paytodate"
					+ "                    and d.contplancode = p.contplancode"
					+ "                    and d.plancode = p.plancode"
					+ "                    and p.riskcode = d.riskcode) as shouldpaya"
					+ "           from lcgrppayplandetail p) as insFee4"
					+ "  where prtno = gc.prtno"
					+ "    and riskcode = gp.riskcode"
					+ "    and current date > paytodate + 3 month"
					+ "    and current date <= paytodate + 6 month"
					+ "  group by prtno) shouldpay3"
					// --逾期超过6个月但在1年内的应收保费余额
					+ ",(select SUM(shouldpaya)"
					+ "   from (select p.prtno,"
					+ "                p.riskcode,"
					+ "                p.paytodate,"
					+ "                p.prem -"
					+ "                (select (case"
					+ "                          when sum(d.prem) is not null then"
					+ "                           sum(d.prem)"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '1' then"
					+ "                           p.prem"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '2' then"
					+ "                           0"
					+ "                        end)"
					+ "                   from db2inst1.LcGrpPayDueDetail d"
					+ "                  where d.prtno = p.prtno"
					+ "                    and p.paytodate = d.paytodate"
					+ "                    and d.contplancode = p.contplancode"
					+ "                    and d.plancode = p.plancode"
					+ "                    and p.riskcode = d.riskcode) as shouldpaya"
					+ "           from lcgrppayplandetail p) as insFee5 "
					+ "  where prtno = gc.prtno"
					+ "    and riskcode = gp.riskcode"
					+ "    and current date > paytodate + 6 month"
					+ "    and current date <= paytodate + 12 month"
					+ "  group by prtno) shouldpay6"
					// --逾期超过1年的应收保费余额
					+ ",(select SUM(shouldpaya)"
					+ "   from (select p.prtno,"
					+ "                p.riskcode,"
					+ "                p.paytodate,"
					+ "                p.prem -"
					+ "                (select (case"
					+ "                          when sum(d.prem) is not null then"
					+ "                           sum(d.prem)"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '1' then"
					+ "                           p.prem"
					+ "                          when sum(d.prem) is null and"
					+ "                               p.state = '2' then"
					+ "                           0"
					+ "                        end)"
					+ "                   from db2inst1.LcGrpPayDueDetail d"
					+ "                  where d.prtno = p.prtno"
					+ "                    and p.paytodate = d.paytodate"
					+ "                    and d.contplancode = p.contplancode"
					+ "                    and d.plancode = p.plancode"
					+ "                    and p.riskcode = d.riskcode) as shouldpaya"
					+ "           from lcgrppayplandetail p) as insFee6"
					+ "  where prtno = gc.prtno"
					+ "    and riskcode = gp.riskcode"
					+ "    and current date > paytodate + 1 years"
					+ "  group by prtno) shouldpay12"
					+ " from lcgrpcont gc,lcgrppayplandetail gp "
					+ " where gc.payintv = -1 and Gc.appflag = '1' and Gc.signdate is not null and gc.prtno = gp.prtno and gc.managecom like '"
					+ manSSRS.GetText(comnum, 1)
					+ "%' and gc.cvalidate <= '"
					+ EndDate
					+ "' "
					+ dSql
					+ " and "
					+ " exists (select 1 from LCGrpPayPlan gpp where gpp.prtno = gc.prtno) and "
					+ " exists (select 1 from lcgrppayplandetail gppd where gppd.prtno = gc.prtno)) as pay" 
					+ " where shouldpay <> 0"
					+ " group by managecom,riskcode with ur";
			System.out.println(mSql);
			tSSRS = tExeSQL.execSQL(mSql);
			if(tSSRS.getMaxRow() != 0){
				if (!polCom.equals(tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0,
						4)
						+ "0000")) {
					mToExcel[printNum + 1][0] = "分公司合计：";
					for (int i = 0; i < sMoney.length; i++) {
						mToExcel[printNum + 1][i + 7] = PubFun.format(sMoney[i])
								+ "";
						sMoney[i] = 0;
					}
					printNum = printNum + 2;
				}

				polCom = tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0, 4)
						+ "0000";

				double money[] = { 0, 0, 0, 0, 0, 0 }; // 支公司合计

				int no = 1;
				for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
					for (int i = 0; i < sumMoney.length; i++) {
						if (tSSRS.GetText(row, i + 8).trim() != null
								&& !tSSRS.GetText(row, i + 8).trim().equals("")
								&& !tSSRS.GetText(row, i + 8).trim().equals("null")) {
							sumMoney[i] = sumMoney[i]
									+ Double.parseDouble(tSSRS.GetText(row, i + 8));
							sMoney[i] = sMoney[i]
									+ Double.parseDouble(tSSRS.GetText(row, i + 8));
							money[i] = money[i]
									+ Double.parseDouble(tSSRS.GetText(row, i + 8));
						}
					}
					for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
						if (tSSRS.GetText(row, col).equals("null")) {
							mToExcel[row + printNum][col - 1] = "";
						} else {
							mToExcel[row + printNum][col - 1] = tSSRS.GetText(row,
									col);
						}
					}
					mToExcel[row + printNum][0] = no + "";
					no++;
				}
				mToExcel[printNum + tSSRS.getMaxRow() + 1][0] = "支公司合计：";
				for (int i = 0; i < sMoney.length; i++) {
					mToExcel[printNum + tSSRS.getMaxRow() + 1][i + 7] = PubFun
							.format(money[i]) + "";
				}
				printNum = printNum + tSSRS.getMaxRow() + 1;
				tSSRS.Clear();
			}
		}
			
        mToExcel[printNum + 1][0] = "分公司合计：";
        for (int i = 0; i < sMoney.length; i++)
        {
            mToExcel[printNum + 1][i + 7] = PubFun.format(sMoney[i]) + "";
            sMoney[i] = 0;
        }
        mToExcel[printNum + 3][0] = "总合计：";
        for (int i = 0; i < sMoney.length; i++)
        {
            mToExcel[printNum + 3][i + 7] = PubFun.format(sumMoney[i]) + "";
        }

        mToExcel[printNum + 6][0] = "制表";
        mToExcel[printNum + 6][10] = "日期：" + mCurDate;

        try
        {
            System.out.println(mOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
            System.out.println("生成文件完成");
        }
        catch (Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        StartDate = (String) tf.getValueByName("StartDate");
        EndDate = (String) tf.getValueByName("EndDate");

        if (EndDate == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\b.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        NewPreRecAgePrintUI tPreRecAgePrintUI = new NewPreRecAgePrintUI();
        if (!tPreRecAgePrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}
