package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author 
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewLCISPullReportBL {
	/** 错误信息容器 */
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private String mManageCom = "";

	private String moperator = "";

	private String mOperatorName = "";

	private String mBatchNo = "";

	private String mAgentComName = "";

	private String mType = "";

	private String mTypeName = "";

	private String mAgentCom = "";

	private String mStartDate = "2008-11-1";

	private String mEndDate = "";

	private double mSumActuMoney = 0;

	private double mClaimMoney = 0;

	private String mCount = "";

	private String mMoneyType = "";

	private String currentDate = PubFun.getCurrentDate();

	DecimalFormat tDF = new DecimalFormat("0.00");

	private String[][] mToExcel = null;

	SSRS tSSRS = new SSRS();
	
	private String mOutXmlPath = null;

	public NewLCISPullReportBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		if (!createFile()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	public boolean dealData() {
		System.out.println("BL->dealDate()");
		String tSQL = null;
		ExeSQL tExeSQL = new ExeSQL();
		tSQL = "select name from lacom where agentcom='" + mAgentCom
				+ "' with ur";
		mAgentComName = tExeSQL.getOneValue(tSQL);

		if ("01".equals(mType)) {
			mTypeName = "新单";
		} else if ("02".equals(mType)) {
			mTypeName = "续期";
		} else if ("03".equals(mType)) {
			mTypeName = "保全";
		} else if ("04".equals(mType)) {
			mTypeName = "理赔";
		} else if ("05".equals(mType)) {
			mTypeName = "理赔";
		} else if ("06".equals(mType)) {
			mTypeName = "理赔";
		}

		tSQL = "select username from lduser where usercode='" + moperator
				+ "' with ur";
		mOperatorName = tExeSQL.getOneValue(tSQL);

		tSQL = "select count(distinct otherno) from lcispayget where batchno='"
				+ mBatchNo + "' with ur";
		mCount = tExeSQL.getOneValue(tSQL);

		String tOtherNoType = mType;
		if ("LP".equals(mType)) {
			tOtherNoType = "C','F','5";
		}

		tSQL = "select max(a.makedate)+1 days from lcispayget a where a.agentcom='"
				+ mAgentCom
				+ "' and a.makedate<(select makedate from lcispayget where batchno='"
				+ mBatchNo
				+ "' fetch first 1 rows only ) and a.batchno<>'"
				+ mBatchNo
				+ "' and a.othernotype in ('"
				+ tOtherNoType
				+ "') fetch first 1 rows only with ur";
		mStartDate = tExeSQL.getOneValue(tSQL);
		if ("".equals(mStartDate) || "null".equals(mStartDate)) {
			mStartDate = "2008-11-1";
		}

		tSQL = "select a.otherno,b.codename,a.grpcontno,c.grpname,a.riskcode,a.sumactumoney,a.rate,a.claimmoney,a.makedate "
				+ " from lcispayget a,ldcode b,lcgrpcont c where a.batchno = '"
				+ mBatchNo
				+ "' and a.othernotype = b.code and a.grpcontno=c.grpcontno "
				+ " and b.codetype = 'CISTYPE' union "
				+ "select a.otherno,b.codename,a.grpcontno,c.grpname,a.riskcode,a.sumactumoney,a.rate,a.claimmoney,a.makedate "
				+ " from lcispayget a,ldcode b,lbgrpcont c where a.batchno = '"
				+ mBatchNo
				+ "' and a.othernotype = b.code and a.grpcontno=c.grpcontno "
				+ " and b.codetype = 'CISTYPE' with ur ";
		SSRS tSSRS = new SSRS();

		tSSRS = tExeSQL.execSQL(tSQL);
		String tDateSQL = "";
		int count = tSSRS.getMaxRow();
		mToExcel = new String[count + 20][15];
		mToExcel[0][0] = "共保" + mTypeName + "结算单";
		mToExcel[2][0] = "主共保公司：中国人民健康保险股份有限公司";
		mToExcel[3][0] = "从共保公司:"+mAgentComName;
		mToExcel[3][4] = "从共保公司代码："+mAgentCom;//需要改
		
		mToExcel[6][0] = "抽档号："+mBatchNo;
		mToExcel[6][2] = "业务数量："+mCount;
		
		mToExcel[8][0] = "业务号码";
		mToExcel[8][1] = "业务类型";
		mToExcel[8][2] = "保单号";
		mToExcel[8][3] = "投保人";
		mToExcel[8][4] = "险种编码";
		mToExcel[8][5] = "总金额";
		mToExcel[8][6] = "主共保方比例";
		mToExcel[8][7] = "主共保方金额";
		mToExcel[8][8] = "从共保方比例";
		mToExcel[8][9] = "从共保方金额";
		mToExcel[8][10] = "时间";
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            String Info[] = new String[11];
            Info[0] = tSSRS.GetText(i, 1);
            Info[1] = tSSRS.GetText(i, 2);
            Info[2] = tSSRS.GetText(i, 3);
            Info[3] = tSSRS.GetText(i, 4);
            Info[4] = tSSRS.GetText(i, 5);
            Info[5] = tSSRS.GetText(i, 6);
            String tRateSQl =
                    "select 1-sum(b.rate) from LCIGrpCont a,LCCoInsuranceParam b "
                    + " where a.grpcontno=b.grpcontno and a.grpcontno='" +
                    Info[2] + "' fetch first 1 rows only with ur";
            String tRate = tExeSQL.getOneValue(tRateSQl);
            Info[6] = tRate;
            double tMoney = Arith.round(Double.parseDouble(Info[5]) *
                                        Double.parseDouble(tRate), 2);
            Info[7] = tDF.format(tMoney);
            Info[8] = tSSRS.GetText(i, 7);
            Info[9] = tDF.format(Double.parseDouble(tSSRS.GetText(i, 8)));
            mEndDate = tSSRS.GetText(i, 9);
            mSumActuMoney += Double.parseDouble(Info[5]);
            mClaimMoney += Double.parseDouble(Info[9]);

            if ("01".equals(mType)) {
                tDateSQL = "select signdate from lcgrpcont where prtno='" +
                           Info[0] +
                           "' union select signdate from lbgrpcont where prtno='" +
                           Info[0] + "' fetch first 1 rows only with ur";
            } else if ("02".equals(mType)) {
                tDateSQL = "select confdate from ljapay where payno = '" +
                           Info[0] + "' fetch first 1 rows only with ur";
            } else if ("03".equals(mType)) {
                tDateSQL =
                        "select confdate from lpedorapp where edoracceptno = '" +
                        Info[0] + "' fetch first 1 rows only with ur";
            } else if ("04".equals(mType)) {
                tDateSQL = "select makedate from ljagetclaim where otherno='" +
                           Info[0] + "' fetch first 1 rows only with ur";
            }
            if("".equals(tDateSQL)){
            	Info[10] = "";
            }else{
            	Info[10] = tExeSQL.getOneValue(tDateSQL);
            }
            
            mToExcel[8+i]=Info;
            
        }
		if (mClaimMoney > 0) {
            mMoneyType = "支付";
        } else {
            mMoneyType = "收取";
        }
		mToExcel[count+9][0]="打印人："+mOperatorName;
		mToExcel[count+9][9]="打印时间："+currentDate;
		mToExcel[count+10][0]="交接人：";
		
		mToExcel[1][0] = "结算时间"+mStartDate+"——"+mEndDate;//后续改
		mToExcel[4][0] = "我方需向从共保方"+mMoneyType+"金额：";//需要改
		mToExcel[4][2] = "大写:"+PubFun.getChnMoney(Math.abs(Arith.round(mClaimMoney, 2)));
		mToExcel[5][2] = "小写:"+tDF.format(Math.abs(Arith.round(mClaimMoney, 2)))+"(￥)";

		return true;
	}

	private boolean createFile() {

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	public boolean getInputData(VData data) {
		mManageCom = (String) data.get(0);
		moperator = (String) data.get(1);
		mBatchNo = (String) data.get(2);
		mType = (String) data.get(3);
		mAgentCom = (String) data.get(4);
		mOutXmlPath = (String) data.get(5);

		return true;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86110000";
		tG.Operator = "cwad";
		tG.ComCode = "86110000";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
		tTransferData.setNameAndValue("EndDate", "2013-3-12");
		tTransferData.setNameAndValue("StartDate", "2013-01-01");
		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);
		NewPreRecPrintUI tPreRecPrintUI = new NewPreRecPrintUI();
		if (!tPreRecPrintUI.submitData(vData, "")) {
			System.out.print("失败！");
		}
		// System.out.println(PubFun.format(111111111111111.0));
	}
}
