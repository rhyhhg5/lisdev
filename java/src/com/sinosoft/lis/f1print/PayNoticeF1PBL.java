package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimPolicyDB;
import com.sinosoft.lis.db.LMDutyGetClmDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LMDutyGetClmSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PayNoticeF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mClmNo;

    //业务处理相关变量
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PayNoticeF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LLClaimSchema aLLClaimSchema = new LLClaimSchema();
        aLLClaimSchema.setClmNo("86330020040520000043");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "8633";

        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(aLLClaimSchema);
        PayNoticeF1PBL aPayNoticeF1PBL = new PayNoticeF1PBL();
        if (aPayNoticeF1PBL.submitData(tVData, "PRINT"))
        {
            System.out.println("执行完毕");
        }
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        System.out.println("getInputData begin");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema = (LLClaimSchema) cInputData.getObjectByObjectName(
                "LLClaimSchema", 0);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mClmNo = tLLClaimSchema.getClmNo();
        if (mClmNo == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        System.out.println("getInputData end");
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PayNoticeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean checkData()
    {
        System.out.println("checkData begin");
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        if (!tLLClaimDB.getInfo())
        {
            CError cError = new CError();
            cError.moduleName = "PayNoticeF1PUI";
            cError.functionName = "checkData";
            cError.errorMessage = "赔案信息查询失败";
            this.mErrors.addOneError(cError);
            return false;
        }
        mLLClaimSchema.setSchema(tLLClaimDB.getSchema());
        if (!mLLClaimSchema.getClmState().equals("3"))
        {
            CError cError = new CError();
            cError.moduleName = "PayNoticeF1PUI";
            cError.functionName = "checkData";
            cError.errorMessage = "案件状态不允许打印理赔批单";
            this.mErrors.addOneError(cError);
            return false;
        }

        if (mLLClaimSchema.getRgtNo() == null)
        {
            CError cError = new CError();
            cError.moduleName = "PayNoticeF1PUI";
            cError.functionName = "checkData";
            cError.errorMessage = "无案件号";
            this.mErrors.addOneError(cError);
            return false;
        }
        System.out.println("checkData end");

        return true;
    }

    private boolean getPrintData()
    {
        System.out.println("getPrintData begin");
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PayNotice1.vts", "printer"); //最好紧接着就初始化xml文档

        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        tLLClaimPolicySet = tLLClaimPolicyDB.query();
        String tStrArr[] = new String[tLLClaimPolicySet.size()];
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++)
        {
            LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
            tLLClaimPolicySchema.setSchema(tLLClaimPolicySet.get(i));
            String tStr = "    ";
            if (tLLClaimPolicySet.size() > 1)
            {
                tStr = tStr + String.valueOf(i) + ".";
            }
            tStr = tStr + "客户之" + tLLClaimPolicySchema.getPolNo().trim() +
                   "号保险单(投保人：" + tLLClaimPolicySchema.getAppntName().trim() +
                   " 被保险人：" + tLLClaimPolicySchema.getInsuredName().trim() +
                   "):";
            LMRiskDB tLMRiskDB = new LMRiskDB();
            LMRiskSchema tLMRiskSchema = new LMRiskSchema();
            tLMRiskDB.setRiskCode(tLLClaimPolicySchema.getRiskCode());
            tLMRiskDB.getInfo();
            tLMRiskSchema.setSchema(tLMRiskDB.getSchema());
            if (tLMRiskSchema.getRiskName() == null)
            {
                continue;
            }
            tStr = tStr + "根据《" + tLMRiskSchema.getRiskName().trim() + "》，";

            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            tLLClaimDetailDB.setClmNo(mClmNo);
            tLLClaimDetailDB.setPolNo(tLLClaimPolicySchema.getPolNo());
            tLLClaimDetailSet = tLLClaimDetailDB.query();
            String tStrAdd = "自批改之日起，被保险人" +
                             tLLClaimPolicySchema.getInsuredName().trim() +
                             "的" + tLMRiskSchema.getRiskName().trim() +
                             "有效保险金额经核减为:";
            for (int j = 1; j <= tLLClaimDetailSet.size(); j++)
            {
                LLClaimDetailSchema tLLClaimDetailSchema = new
                        LLClaimDetailSchema();
                tLLClaimDetailSchema.setSchema(tLLClaimDetailSet.get(j));
                LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
                tLMDutyGetClmDB.setGetDutyCode(tLLClaimDetailSchema.
                                               getGetDutyCode());
                tLMDutyGetClmDB.setGetDutyKind(tLLClaimDetailSchema.
                                               getGetDutyKind());
                tLMDutyGetClmDB.getInfo();
                LMDutyGetClmSchema tLMDutyGetClmSchema = new LMDutyGetClmSchema();
                tLMDutyGetClmSchema.setSchema(tLMDutyGetClmDB.getSchema());
                double tRealPay = Double.parseDouble(new DecimalFormat("0.00").
                        format(tLLClaimDetailSchema.getRealPay()));
                System.out.println("Trealpay=" + tRealPay);
                String PayMoneyStr = PubFun.getChnMoney(tRealPay);
                tStr = tStr + "给付" + tLMDutyGetClmSchema.getGetDutyName().trim() +
                       "人民币" +
                       PayMoneyStr.trim() + "（RMB" + tRealPay +
                       "），";
                tStrAdd = tStrAdd + tLMDutyGetClmSchema.getGetDutyName().trim() +
                          "人民币";
                LCGetDB tLCGetDB = new LCGetDB();
                tLCGetDB.setPolNo(tLLClaimDetailSchema.getPolNo());
                tLCGetDB.setDutyCode(tLLClaimDetailSchema.getDutyCode());
                tLCGetDB.setGetDutyCode(tLLClaimDetailSchema.getGetDutyCode());
                tLCGetDB.getInfo();
                LCGetSchema tLCGetSchema = new LCGetSchema();

                tLCGetSchema.setSchema(tLCGetDB.getSchema());
                double tSySum = 0;
                // tSySum = tLCGetSchema.getStandMoney()-tLLClaimDetailSchema.getRealPay();
                //刘岩松2004-2-23
                tSySum = tLCGetSchema.getStandMoney() -
                         tLCGetSchema.getSumMoney();
                System.out.println("2004-2-23修改后的金额是" + tSySum);
                if (tSySum < 0)
                {
                    tSySum = 0;
                }
                double tSyMoney = Double.parseDouble(new DecimalFormat("0.00").
                        format(tSySum));
                String tSySumStr = PubFun.getChnMoney(tSyMoney);
                tStrAdd = tStrAdd + tSySumStr + "（RMB" + tSyMoney + ")，";

            }
            tStrAdd = tStrAdd + "其它不变。";
            tStr = tStr + tStrAdd;
            tStr = tStr + "该保险单合计给付人民币" +
                   PubFun.getChnMoney(tLLClaimPolicySchema.getRealPay()) +
                   "(RMB" +
                   tLLClaimPolicySchema.getRealPay() + ")";
            tStrArr[i - 1] = tStr;
            String[] strArr = new String[1];
            strArr[0] = tStr;
            tlistTable.add(strArr);
            System.out.println("str===" + tStrArr[i - 1]);
        }

        String[] strArr1 = new String[1];
        strArr1[0] = "Mode";
        xmlexport.addListTable(tlistTable, strArr1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        LJAGetDB tLjaGetDB = new LJAGetDB();
        tLjaGetDB.setOtherNo(mClmNo);
        LJAGetSet tLJAGetSet = new LJAGetSet();
        tLJAGetSet = tLjaGetDB.query();
        String tPayNoticeNo = "";
        if (tLJAGetSet.get(1) != null)
        {
            tPayNoticeNo = tLJAGetSet.get(1).getActuGetNo();
        }
        texttag.add("PayNoticeNo", tPayNoticeNo);
        texttag.add("ClmNo", mLLClaimSchema.getRgtNo());
        String tPayMoney = PubFun.getChnMoney(mLLClaimSchema.getRealPay()) +
                           ",(RMB" + mLLClaimSchema.getRealPay() + ").";
        texttag.add("PaySumStr", tPayMoney);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        System.out.println("getPrintData end");
        return true;

    }

}