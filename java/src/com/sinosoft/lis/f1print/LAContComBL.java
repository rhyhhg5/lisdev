package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.Arith;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAContComBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mManageCom = "";
    private String mBranchAttr = "";
    private String mAgentGroup ="";
    private String mStartDate1 = "";
    private String mEndDate1 = "";
    private String mStartDate2 = "";
    private String mEndDate2 = "";

    private double mSumPrem=0;
        private String  mAgentStateMin ;
    private String  mAgentStateMax ;
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();
    private SSRS mSSRS2 = new SSRS();
    private SSRS mSSRS3 = new SSRS();
    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAContComBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";

        LAContComBL tLAContComBL = new LAContComBL();
       // tLAContComBL.submitData(tVData, "");

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
      try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
            this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
            this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
            this.mStartDate1 = (String) mTransferData.getValueByName("tStartDate1");
            this.mEndDate1 = (String) mTransferData.getValueByName("tEndDate1");
            this.mStartDate2 = (String) mTransferData.getValueByName("tStartDate2");
            this.mEndDate2 = (String) mTransferData.getValueByName("tEndDate2");
        } catch (Exception ex)
        {
            this.mErrors.addOneError("");
            return false;
        }

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if(!getAgent())
      {
          return false;
      }

      return true;
   }
   /**
   * 得到人员
   * @return boolean
   */
   private boolean getAgent()
   {
       String tSQL = "select distinct a.agentcode,b.name from  lacommision a,laagent b "
                     + " where a.agentcode=b.agentcode  "
                     + " and a.branchtype='1' and a.branchtype2='01'  "
                     + " and b.branchtype='1' and b.branchtype2='01'  "
                     +
                     " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and a.managecom like '" + mManageCom + "%' "
                     + " and a.signdate<='" + mEndDate2 + "' ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " order by a.agentcode  ";
       ExeSQL tExeSQL = new ExeSQL();
       mSSRS1 = tExeSQL.execSQL(tSQL);
       if(mSSRS1.getMaxRow()<=0)
       {
           bulidError("getagent","没有符合条件的信息!");
           return false;
       }

       return true;

   }
   /**
   * 得到
   * 一个人的保费
   * @return boolean
   */
   private double getPrem(String tAgentCode)
   {
       //查询正常保单
       String tSQL = "select sum(a.transmoney) "
                     +
                     " from  lacommision a,lccont b where  a.p14=b.prtno   "
                     + "and a.branchtype='1' and a.branchtype2='01'  "
                     +
                     " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and (b.cardflag='0' or b.cardflag is null ) " //正常保单
                     + " and a.managecom like '" + mManageCom +
                     "%' and a.agentcode='" + tAgentCode + "'   "
                     + " and a.scandate>='" + mStartDate1 +
                     "' and a.scandate<='" + mEndDate1 + "' "
                     + " and a.signdate<='" + mEndDate2 + "'  and "
                     + " b.contno not in (select aa.contno from LCRnewStateLog aa "
                     + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }

       ExeSQL tExeSQL = new ExeSQL();
       String tPrem1 = tExeSQL.getOneValue(tSQL);
       //查询lcbont备份表
       tSQL = "select sum(a.transmoney) "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and (b.cardflag='0' or b.cardflag is null )  " //正常保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and a.scandate>='" + mStartDate1 + "' and a.scandate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tExeSQL = new ExeSQL();
       String tPrem2 = tExeSQL.getOneValue(tSQL);
       if(tPrem1==null || tPrem1.equals(""))
       {
           tPrem1="0";
       }
       if(tPrem2==null || tPrem2.equals(""))
       {
           tPrem2="0";
       }

       double tsumprem1 = Arith.add(Double.parseDouble(tPrem1),
                                Double.parseDouble(tPrem2));
       //查询简易保单------------------------------------------------------------
       tSQL = "select sum(a.transmoney) "
              + " from  lacommision a,lccont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and  "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tExeSQL = new ExeSQL();
       String tPrem3 = tExeSQL.getOneValue(tSQL);
       //简易保单备份信息
       tSQL = "select sum(a.transmoney) "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tExeSQL = new ExeSQL();
       String tPrem4 = tExeSQL.getOneValue(tSQL);
       if(tPrem3==null || tPrem3.equals(""))
       {
           tPrem3="0";
       }
       if(tPrem4==null || tPrem4.equals(""))
       {
           tPrem4="0";
       }

       double tsumprem2 = Arith.add(Double.parseDouble(tPrem3),
                                 Double.parseDouble(tPrem4));
       double tprem = Arith.add(tsumprem1,tsumprem2);
       return tprem ;
   }
   /**
   * 得到客户数
   * @return boolean
   */
   private int getCustomerNo(String tAgentCode)
   {
       //查询正常保单
       String tSQL = " select count(*)  from  "
                     + "(select  distinct a.P12,a.contno "
                     +
                     " from  lacommision a,lccont b where  a.p14=b.prtno     "
                     + " and a.branchtype='1' and a.branchtype2='01'  "
                     + " and (b.cardflag='0' or b.cardflag is null )  " //正常保单
                     + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
                     + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and a.managecom like '" + mManageCom +
                     "%' and a.agentcode='" + tAgentCode + "'   "
                     + " and a.scandate>='" + mStartDate1 +
                     "' and a.scandate<='" + mEndDate1 + "' "
                     + " and a.signdate<='" + mEndDate2 + "' and "
                     + " b.contno not in (select aa.contno from LCRnewStateLog aa "
                     + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       ExeSQL tExeSQL = new ExeSQL();
       String tPrem1 = tExeSQL.getOneValue(tSQL);
       //查询lcbont备份表
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12,a.contno "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and (b.cardflag='0' or b.cardflag is null )  " //正常保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and a.scandate>='" + mStartDate1 + "' and a.scandate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem2 = tExeSQL.getOneValue(tSQL);
       if(tPrem1==null || tPrem1.equals(""))
       {
           tPrem1="0";
       }
       if(tPrem2==null || tPrem2.equals(""))
       {
           tPrem2="0";
       }

       int tsumprem1 = Integer.parseInt(tPrem1)+Integer.parseInt(tPrem2) ;
       //查询简易保单------------------------------------------------------------
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12,a.contno "
              + " from  lacommision a,lccont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem3 = tExeSQL.getOneValue(tSQL);
       //简易保单备份信息
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12,a.contno "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem4 = tExeSQL.getOneValue(tSQL);
       if(tPrem3==null || tPrem3.equals(""))
       {
           tPrem3="0";
       }
       if(tPrem4==null || tPrem4.equals(""))
       {
           tPrem4="0";
       }

       int tsumprem2 = Integer.parseInt(tPrem3) + Integer.parseInt(tPrem4) ;
       int tprem = tsumprem1 + tsumprem2 ;
       return tprem;
   }

   /**
   * 得到有效客户数
   * @return boolean
   */
   private int getValidNo(String tAgentCode)
   {
       //查询正常保单
       String tSQL = " select count(*)  from  "
                     + "(select  distinct a.P12  "
                     +
                     " from  lacommision a,lccont b where  a.p14=b.prtno     "
                     + " and a.branchtype='1' and a.branchtype2='01'  "
                     + " and (b.cardflag='0' or b.cardflag is null )  " //正常保单
                     + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
                     + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and a.managecom like '" + mManageCom +
                     "%' and a.agentcode='" + tAgentCode + "'   "
                     + " and a.scandate>='" + mStartDate1 +
                     "' and a.scandate<='" + mEndDate1 + "' "
                     + " and a.signdate<='" + mEndDate2 + "' and "
                     + " b.contno not in (select aa.contno from LCRnewStateLog aa "
                     + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       ExeSQL tExeSQL = new ExeSQL();
       String tPrem1 = tExeSQL.getOneValue(tSQL);
       //查询lbont备份表
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12  "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and (b.cardflag='0' or b.cardflag is null ) " //正常保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and a.scandate>='" + mStartDate1 + "' and a.scandate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem2 = tExeSQL.getOneValue(tSQL);
       if(tPrem1==null || tPrem1.equals(""))
       {
           tPrem1="0";
       }
       if(tPrem2==null || tPrem2.equals(""))
       {
           tPrem2="0";
       }

       int tsumprem1 = Integer.parseInt(tPrem1) + Integer.parseInt(tPrem2);
       //查询简易保单------------------------------------------------------------
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12 "
              + " from  lacommision a,lccont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem3 = tExeSQL.getOneValue(tSQL);
       //简易保单备份信息
       tSQL = " select count(*)  from  "
              + "(select  distinct a.P12  "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.riskcode not in   ( select c.riskcode from LMRiskApp c where c.RiskType = 'A'    and c.RiskPeriod <> 'L' ) "
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " ) as x  ";
       tExeSQL = new ExeSQL();
       String tPrem4 = tExeSQL.getOneValue(tSQL);
       if(tPrem3==null || tPrem3.equals(""))
       {
           tPrem3="0";
       }
       if(tPrem4==null || tPrem4.equals(""))
       {
           tPrem4="0";
       }

       int tsumprem2 = Integer.parseInt(tPrem3) + Integer.parseInt(tPrem4);
       int tprem = tsumprem1 + tsumprem2;
       return tprem;
   }
    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LAContComReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }



        String[] title = {"", "", "", "", ""  };

        if (!getListTable()) {
            return false;
        }
        tTextTag.add("tSumPrem", mSumPrem);
        mXmlExport.addTextTag(tTextTag);
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable()
    {
        if (mSSRS1.getMaxRow() >= 1)
        {
            //此人有职级变更
            boolean tFlag = false;//判断是否所有人业绩为0
            double tSumprem=0;
            int tSumCustomerNo=0;
            int tSumValidNo =0;
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
            {
                String tAgentCode = mSSRS1.GetText(i,1);
                double tprem = getPrem(tAgentCode);
                tprem = Arith.div(tprem,10000,6);//单位为万元

                int tCustomerNo = getCustomerNo(tAgentCode);
                int tValidNo = getValidNo(tAgentCode);

                tSumprem = Arith.add(tSumprem,tprem);
                tSumCustomerNo=tSumCustomerNo+tCustomerNo;
                tSumValidNo=tSumValidNo+tValidNo;

                String Info[] = new String[5];
                Info[0] = mSSRS1.GetText(i, 1);
                Info[1] = mSSRS1.GetText(i, 2);
                Info[2] = String.valueOf(tprem);
                Info[3] = String.valueOf(tCustomerNo) ;
                Info[4] = String.valueOf(tValidNo)   ;
                if(tprem==0 && tCustomerNo==0 && tValidNo==0 )
                {
                    //业务员没有业绩不在报表中显示
                    continue;
                }
                tFlag =true ; //
                mListTable.add(Info);

            }
            mSumPrem=tSumprem;
            String Info[] = new String[5];
            Info[0] = "合计";
            Info[1] = "";
            Info[2] = String.valueOf(tSumprem) ;
            Info[3] = String.valueOf(tSumCustomerNo) ;
            Info[4] = String.valueOf(tSumValidNo)   ;
            //mListTable.add(Info);
            if(tFlag==false)
            {   //所有人的业绩为0
                bulidError("getListTable","没有符合条件的信息!");
                return false;
            }
        }
        else
        {
            bulidError("getListTable","没有符合条件的信息!");
            return false;
        }
        mListTable.setName("ZT");

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LAContComBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
