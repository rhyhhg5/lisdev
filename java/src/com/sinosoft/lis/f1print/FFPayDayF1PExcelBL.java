package com.sinosoft.lis.f1print;

/**
 * <p>Title:FFPayDayF1PExcelBL </p>
 * <p>Description:X2-日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author yan
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FFPayDayF1PExcelBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String tOutXmlPath = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FFPayDayF1PExcelBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        // 准备所有要打印的数据
         if (!getPrintData())
         {
            return false;
         }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        
        if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFPayDayF1PExcelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){
        int no=5;  //初始打印行数

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
 
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//        tServerPath= "E:/lisdev/ui/";
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        
//      要提取的类型，对应FeePrintSql.xml里的SQL语句节点
        String[] getTypes = new String[] { 
        		"X2SF",
        		"X2GBBFCWFC"
        };
        
        //X2-实付日结单-实付明细-数据量
        for(int node=0;node<getTypes.length;node++){
        	String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[node]);

            tSSRS = tExeSQL.execSQL(msql);
            no = no+tSSRS.MaxRow;
        }
        
        //X2-实付日结单-实付汇总-数据量
        for(int hz=0;hz<getTypes.length;hz++){
        	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[hz]+"Z");
            tSSRS = tExeSQL.execSQL(msql);
            no = no+tSSRS.MaxRow;
        }
        
        //X2实付日结单-实付合计
        msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X2SFHJ"); 
        
        tSSRS = tExeSQL.execSQL(msql);
        no = no+tSSRS.MaxRow;
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

    private boolean getPrintData()
    {
    	ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
// 		tServerPath= "E:/lisdev/ui/";
		
		String Msql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(Msql);
		String manageCom = tSSRS.GetText(1, 1);
		
        int tMaxRowNum = getNumber();
        
		String[][] mToExcel = new String[tMaxRowNum+10][5];
		mToExcel[0][0] = "财务付费日结单";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mDay[0]+"至"+mDay[1];
		mToExcel[2][0] = "统计机构：";
		mToExcel[2][1] = manageCom;
		mToExcel[4][0] = "付款方式";
		mToExcel[4][1] = "号码类型";
		mToExcel[4][2] = "号码";
		mToExcel[4][3] = "金额";
		mToExcel[4][4] = "件数";
		
		int no=5;  //初始打印行数

        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
		
        //要提取的类型，对应FeePrintSql.xml里的SQL语句节点
        String[] getTypes = new String[] { 
        		"X2SF",
        		"X2GBBFCWFC"
        };
        
		//X2-实付日结单-实付明细
        for(int node=0;node<getTypes.length;node++){
        	String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[node]);
            tSSRS = tExeSQL.execSQL(msql);
            for (int row = 1; row <= tSSRS.MaxRow; row++)
            {
            	 mToExcel[no+row-1][0]=tSSRS.GetText(row, 2);
                 mToExcel[no+row-1][1]=tSSRS.GetText(row, 1);
                 mToExcel[no+row-1][2]=tSSRS.GetText(row, 5);
                 mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
                 mToExcel[no+row-1][4]=tSSRS.GetText(row, 4);
            }
            no = no+tSSRS.MaxRow;
        }
        
        //X2-实付日结单-实付汇总
        for(int hz=0;hz<getTypes.length;hz++){
        	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[hz]+"Z");
        	tSSRS = tExeSQL.execSQL(msql);
            for (int row = 1; row <= tSSRS.MaxRow; row++)
            {
            	 mToExcel[no+row-1][0]=tSSRS.GetText(row, 1);
                 mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
                 mToExcel[no+row-1][4]=tSSRS.GetText(row, 2);
            }
            no = no+tSSRS.MaxRow;
        }
        
        //X2实付日结单-实付合计
    	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X2SFHJ"); 
        tSSRS = tExeSQL.execSQL(msql);
        for (int row = 1; row <= tSSRS.MaxRow; row++)
        {
        	 mToExcel[no+row-1][0]="合计：";
        	 mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
             mToExcel[no+row-1][4]=tSSRS.GetText(row, 3);
        }
        no = no+tSSRS.MaxRow;
        
        mToExcel[no+2][0] = "制表员：";
		mToExcel[no+2][2] = "审核员：";
		 try
	        {
	            System.out.println("X2--tOutXmlPath:"+tOutXmlPath);
	            WriteToExcel t = new WriteToExcel("");
	            t.createExcelFile();
	            String[] sheetName = { PubFun.getCurrentDate() };
	            t.addSheet(sheetName);
	            t.setData(0, mToExcel);
	            t.write(tOutXmlPath);
	            System.out.println("生成X2文件完成");
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	            buildError("dealData", ex.toString());
	            return false;
	        }
		return true;
    }

}
