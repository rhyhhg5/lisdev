package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class LAAscriptTraceQueryBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";


    //取得的代理人姓名
    private String mName = "";

    //取得的保单号
    private String mContNo = "";

    //取得的管理机构代码
    private String mManageCom = "";
    //取得的外部代码
    private String mBranchAttr= "";
    //取得的分配日期代码
    private String mMakeDate= "";
    //取得当前日期
    private String mdate = PubFun.getCurrentDate();
    //取得当期时间
    private String mtime = PubFun.getCurrentTime();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	 mManageCom = (String)cInputData.get(0);
         mContNo = (String)cInputData.get(1);
       //mBranchAttr = (String)cInputData.get(4);
         mMakeDate = (String)cInputData.get(2);
         mAgentCode = (String)cInputData.get(3);
    	 mName = (String)cInputData.get(4);
         return true;
    }
    /*
     * 根据生日求年龄
     */
 private String getage(String birthday)
 {
	 String age="";
	 int age1=0;
     //计算年份差值
	 String oldyear = birthday.substring(0,4);
     System.out.println("oldyear"+oldyear);
     String year = mdate.substring(0,4);
     System.out.println("year"+year);
     //判断月份
    if(Integer.parseInt(birthday.substring(5,7))==Integer.parseInt(mdate.substring(5,7)))
    {
    	//判断日期
    	if(Integer.parseInt(birthday.substring(8,10))>=Integer.parseInt(mdate.substring(8,10)))
    			{
    		       age1 = Integer.parseInt(year)-Integer.parseInt(oldyear)-1;
    			}
    	else{
    	  age1 = Integer.parseInt(year)-Integer.parseInt(oldyear);
    	}
    }
    else if(Integer.parseInt(birthday.substring(5,7))>Integer.parseInt(mdate.substring(5,7)))
    {
    	age1 = Integer.parseInt(year)-Integer.parseInt(oldyear)-1;
    }
    else
    {
    	age1 = Integer.parseInt(year)-Integer.parseInt(oldyear);
    }
	 age = String.valueOf(age1);
	 return age;
 }
    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAAscriptTrace.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAAscript");
        String[] title = {"", "", "", "", "" ,"","","","","","","",""};

       //查询所有的记录
       msql ="select a.ascriptioncount,a.contno,b.appntname"
          +", case when b.appntsex='0' then '男' else '女' end ,b.appntbirthday,"
    	    + "  (select e.mobile from lcaddress e where customerno = b.appntno and addressno=(select addressno from lcappnt where a.contno=lcappnt.contno)),"
    	    + " c.name,a.agentold ,a.branchattr,"
    	    +" (select name from laagent where agentcode=a.agentnew),a.agentnew,"
    	    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select branchcode from laagent where agentcode=a.agentnew)) h ,a.modifydate"
    	   +"  ,(select e.postaladdress from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))"
          + " from laascription a ,lccont b,laagent c where b.contno=a.contno  and a.agentold=c.agentcode and a.validflag='N' and a.ascripstate='3' and a.branchtype='1' and a.branchtype2='01'";
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))
       {
    	   msql += " and a.managecom like '"+this.mManageCom+"%'";
       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and a.agentold='"+this.mAgentCode+"'";
       }
       if(this.mContNo!=null&&!this.mContNo.equals(""))
       {
    	   msql+= " and a.contno='"+this.mContNo+"'";
       }
       if(this.mName!=null&&!this.mName.equals(""))
       {
    	   msql+=" and a.name='"+this.mName+"'";
       }
//       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
//       {
//           msql+=" and a.BranchAttr='"+this.mBranchAttr+"'";
//       }
       if(this.mMakeDate!=null&&!this.mMakeDate.equals(""))
       {
           msql+=" and a.modifydate='"+this.mMakeDate+"'";
       }



       msql += "order by a.contno,a.ascriptioncount,a.ascriptiondate,a.makedate,a.modifydate,a.modifytime";
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的孤儿单信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[14];
    	  Info[0] = mSSRS.GetText(i, 1);
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = getage(mSSRS.GetText(i, 5));
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);
    	  Info[7] = mSSRS.GetText(i, 8);
    	  Info[8] = mSSRS.GetText(i, 9);
    	  Info[9] = mSSRS.GetText(i, 10);
    	  Info[10] = mSSRS.GetText(i, 11);
    	  Info[11] = mSSRS.GetText(i, 12);
          Info[12] = mSSRS.GetText(i, 13);
          Info[13] = mSSRS.GetText(i, 14);
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的孤儿单信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }

         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
